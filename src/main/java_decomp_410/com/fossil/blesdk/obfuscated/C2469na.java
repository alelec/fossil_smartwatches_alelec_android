package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.na */
public abstract class C2469na {
    @DexIgnore
    /* renamed from: a */
    public abstract androidx.databinding.ViewDataBinding mo13885a(com.fossil.blesdk.obfuscated.C2638pa paVar, android.view.View view, int i);

    @DexIgnore
    /* renamed from: a */
    public abstract androidx.databinding.ViewDataBinding mo13886a(com.fossil.blesdk.obfuscated.C2638pa paVar, android.view.View[] viewArr, int i);

    @DexIgnore
    /* renamed from: a */
    public java.util.List<com.fossil.blesdk.obfuscated.C2469na> mo13887a() {
        return java.util.Collections.emptyList();
    }
}
