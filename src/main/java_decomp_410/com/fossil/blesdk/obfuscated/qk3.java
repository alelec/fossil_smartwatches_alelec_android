package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qk3 implements Factory<ok3> {
    @DexIgnore
    public static ok3 a(pk3 pk3) {
        ok3 a = pk3.a();
        n44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
