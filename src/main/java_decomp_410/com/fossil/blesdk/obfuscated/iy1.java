package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class iy1 implements Runnable {
    @DexIgnore
    public /* final */ hy1 e;
    @DexIgnore
    public /* final */ Intent f;

    @DexIgnore
    public iy1(hy1 hy1, Intent intent) {
        this.e = hy1;
        this.f = intent;
    }

    @DexIgnore
    public final void run() {
        hy1 hy1 = this.e;
        String action = this.f.getAction();
        StringBuilder sb = new StringBuilder(String.valueOf(action).length() + 61);
        sb.append("Service took too long to process intent: ");
        sb.append(action);
        sb.append(" App may get closed.");
        Log.w("EnhancedIntentService", sb.toString());
        hy1.a();
    }
}
