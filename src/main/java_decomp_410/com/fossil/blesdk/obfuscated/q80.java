package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.LegacyFileControlStatusCode;
import com.fossil.blesdk.device.logic.request.legacy.LegacyFileControlOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class q80 extends f70 {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId I;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId J;
    @DexIgnore
    public /* final */ short K;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q80(LegacyFileControlOperationCode legacyFileControlOperationCode, short s, RequestId requestId, Peripheral peripheral, int i) {
        super(requestId, peripheral, i);
        kd4.b(legacyFileControlOperationCode, "operationCode");
        kd4.b(requestId, "requestId");
        kd4.b(peripheral, "peripheral");
        this.K = s;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(legacyFileControlOperationCode.getCode()).putShort(this.K).array();
        kd4.a((Object) array, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.G = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(legacyFileControlOperationCode.responseCode()).putShort(this.K).array();
        kd4.a((Object) array2, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.H = array2;
        GattCharacteristic.CharacteristicId characteristicId = GattCharacteristic.CharacteristicId.FTC;
        this.I = characteristicId;
        this.J = characteristicId;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId B() {
        return this.J;
    }

    @DexIgnore
    public byte[] D() {
        return this.G;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId E() {
        return this.I;
    }

    @DexIgnore
    public byte[] G() {
        return this.H;
    }

    @DexIgnore
    public final short I() {
        return this.K;
    }

    @DexIgnore
    public void b(byte[] bArr) {
        kd4.b(bArr, "<set-?>");
        this.H = bArr;
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(super.t(), JSONKey.FILE_HANDLE, n90.a(this.K));
    }

    @DexIgnore
    public final o70 b(byte b) {
        return LegacyFileControlStatusCode.Companion.a(b);
    }
}
