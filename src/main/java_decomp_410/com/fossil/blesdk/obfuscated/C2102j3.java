package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.j3 */
public abstract class C2102j3 {
    @DexIgnore
    /* renamed from: a */
    public abstract void mo11563a(java.lang.Runnable runnable);

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo11564a();

    @DexIgnore
    /* renamed from: b */
    public void mo12211b(java.lang.Runnable runnable) {
        if (mo11564a()) {
            runnable.run();
        } else {
            mo11565c(runnable);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public abstract void mo11565c(java.lang.Runnable runnable);
}
