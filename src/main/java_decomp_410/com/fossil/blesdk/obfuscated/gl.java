package com.fossil.blesdk.obfuscated;

import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gl implements fl {
    @DexIgnore
    public /* final */ RoomDatabase a;
    @DexIgnore
    public /* final */ lf b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends lf<el> {
        @DexIgnore
        public a(gl glVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(kg kgVar, el elVar) {
            String str = elVar.a;
            if (str == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, str);
            }
            String str2 = elVar.b;
            if (str2 == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, str2);
            }
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkName`(`name`,`work_spec_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public gl(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
    }

    @DexIgnore
    public void a(el elVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(elVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }
}
