package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class oi2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;

    @DexIgnore
    public oi2(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, View view2, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
        this.u = flexibleTextView3;
        this.v = flexibleTextView4;
        this.w = flexibleTextView6;
        this.x = flexibleTextView8;
    }

    @DexIgnore
    public static oi2 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, qa.a());
    }

    @DexIgnore
    @Deprecated
    public static oi2 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (oi2) ViewDataBinding.a(layoutInflater, (int) R.layout.item_sleep_day, viewGroup, z, obj);
    }
}
