package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vb2 extends ub2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j s; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray t; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public long r;

    /*
    static {
        t.put(R.id.progressBar, 1);
        t.put(R.id.tv_title, 2);
        t.put(R.id.iv_email, 3);
        t.put(R.id.ftv_email_notification, 4);
        t.put(R.id.ftv_email, 5);
        t.put(R.id.ftv_enter_codes_guide, 6);
        t.put(R.id.bt_continue_sign_up, 7);
    }
    */

    @DexIgnore
    public vb2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 8, s, t));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.r = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.r != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.r = 1;
        }
        g();
    }

    @DexIgnore
    public vb2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[7], objArr[5], objArr[4], objArr[6], objArr[3], objArr[1], objArr[2]);
        this.r = -1;
        this.q = objArr[0];
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
