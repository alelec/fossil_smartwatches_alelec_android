package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xu */
public class C3314xu extends androidx.fragment.app.Fragment {

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2160ju f11077e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C3141vu f11078f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.util.Set<com.fossil.blesdk.obfuscated.C3314xu> f11079g;

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C3314xu f11080h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C3299xn f11081i;

    @DexIgnore
    /* renamed from: j */
    public androidx.fragment.app.Fragment f11082j;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xu$a")
    /* renamed from: com.fossil.blesdk.obfuscated.xu$a */
    public class C3315a implements com.fossil.blesdk.obfuscated.C3141vu {
        @DexIgnore
        public C3315a() {
        }

        @DexIgnore
        /* renamed from: a */
        public java.util.Set<com.fossil.blesdk.obfuscated.C3299xn> mo14860a() {
            java.util.Set<com.fossil.blesdk.obfuscated.C3314xu> N0 = com.fossil.blesdk.obfuscated.C3314xu.this.mo17878N0();
            java.util.HashSet hashSet = new java.util.HashSet(N0.size());
            for (com.fossil.blesdk.obfuscated.C3314xu next : N0) {
                if (next.mo17881Q0() != null) {
                    hashSet.add(next.mo17881Q0());
                }
            }
            return hashSet;
        }

        @DexIgnore
        public java.lang.String toString() {
            return super.toString() + "{fragment=" + com.fossil.blesdk.obfuscated.C3314xu.this + "}";
        }
    }

    @DexIgnore
    public C3314xu() {
        this(new com.fossil.blesdk.obfuscated.C2160ju());
    }

    @DexIgnore
    /* renamed from: c */
    public static androidx.fragment.app.FragmentManager m16515c(androidx.fragment.app.Fragment fragment) {
        while (fragment.getParentFragment() != null) {
            fragment = fragment.getParentFragment();
        }
        return fragment.getFragmentManager();
    }

    @DexIgnore
    /* renamed from: N0 */
    public java.util.Set<com.fossil.blesdk.obfuscated.C3314xu> mo17878N0() {
        com.fossil.blesdk.obfuscated.C3314xu xuVar = this.f11080h;
        if (xuVar == null) {
            return java.util.Collections.emptySet();
        }
        if (equals(xuVar)) {
            return java.util.Collections.unmodifiableSet(this.f11079g);
        }
        java.util.HashSet hashSet = new java.util.HashSet();
        for (com.fossil.blesdk.obfuscated.C3314xu next : this.f11080h.mo17878N0()) {
            if (mo17887a(next.mo17880P0())) {
                hashSet.add(next);
            }
        }
        return java.util.Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    /* renamed from: O0 */
    public com.fossil.blesdk.obfuscated.C2160ju mo17879O0() {
        return this.f11077e;
    }

    @DexIgnore
    /* renamed from: P0 */
    public final androidx.fragment.app.Fragment mo17880P0() {
        androidx.fragment.app.Fragment parentFragment = getParentFragment();
        return parentFragment != null ? parentFragment : this.f11082j;
    }

    @DexIgnore
    /* renamed from: Q0 */
    public com.fossil.blesdk.obfuscated.C3299xn mo17881Q0() {
        return this.f11081i;
    }

    @DexIgnore
    /* renamed from: R0 */
    public com.fossil.blesdk.obfuscated.C3141vu mo17882R0() {
        return this.f11078f;
    }

    @DexIgnore
    /* renamed from: S0 */
    public final void mo17883S0() {
        com.fossil.blesdk.obfuscated.C3314xu xuVar = this.f11080h;
        if (xuVar != null) {
            xuVar.mo17889b(this);
            this.f11080h = null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17885a(com.fossil.blesdk.obfuscated.C3299xn xnVar) {
        this.f11081i = xnVar;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo17889b(com.fossil.blesdk.obfuscated.C3314xu xuVar) {
        this.f11079g.remove(xuVar);
    }

    @DexIgnore
    public void onAttach(android.content.Context context) {
        super.onAttach(context);
        androidx.fragment.app.FragmentManager c = m16515c(this);
        if (c != null) {
            try {
                mo17884a(getContext(), c);
            } catch (java.lang.IllegalStateException e) {
                if (android.util.Log.isLoggable("SupportRMFragment", 5)) {
                    android.util.Log.w("SupportRMFragment", "Unable to register fragment with root", e);
                }
            }
        } else if (android.util.Log.isLoggable("SupportRMFragment", 5)) {
            android.util.Log.w("SupportRMFragment", "Unable to register fragment with root, ancestor detached");
        }
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        this.f11077e.mo12478a();
        mo17883S0();
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        this.f11082j = null;
        mo17883S0();
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        this.f11077e.mo12480b();
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        this.f11077e.mo12482c();
    }

    @DexIgnore
    public java.lang.String toString() {
        return super.toString() + "{parent=" + mo17880P0() + "}";
    }

    @DexIgnore
    @android.annotation.SuppressLint({"ValidFragment"})
    public C3314xu(com.fossil.blesdk.obfuscated.C2160ju juVar) {
        this.f11078f = new com.fossil.blesdk.obfuscated.C3314xu.C3315a();
        this.f11079g = new java.util.HashSet();
        this.f11077e = juVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo17886a(com.fossil.blesdk.obfuscated.C3314xu xuVar) {
        this.f11079g.add(xuVar);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17888b(androidx.fragment.app.Fragment fragment) {
        this.f11082j = fragment;
        if (fragment != null && fragment.getContext() != null) {
            androidx.fragment.app.FragmentManager c = m16515c(fragment);
            if (c != null) {
                mo17884a(fragment.getContext(), c);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo17887a(androidx.fragment.app.Fragment fragment) {
        androidx.fragment.app.Fragment P0 = mo17880P0();
        while (true) {
            androidx.fragment.app.Fragment parentFragment = fragment.getParentFragment();
            if (parentFragment == null) {
                return false;
            }
            if (parentFragment.equals(P0)) {
                return true;
            }
            fragment = fragment.getParentFragment();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo17884a(android.content.Context context, androidx.fragment.app.FragmentManager fragmentManager) {
        mo17883S0();
        this.f11080h = com.fossil.blesdk.obfuscated.C2815rn.m13272a(context).mo15650h().mo16899a(context, fragmentManager);
        if (!equals(this.f11080h)) {
            this.f11080h.mo17886a(this);
        }
    }
}
