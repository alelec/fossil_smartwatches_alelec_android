package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ty3 extends uy3 {
    @DexIgnore
    public ty3(Context context) {
        super(context);
    }

    @DexIgnore
    public final void a(String str) {
        synchronized (this) {
            Log.i("MID", "write mid to Settings.System");
            Settings.System.putString(this.a.getContentResolver(), wy3.c("4kU71lN96TJUomD1vOU9lgj9Tw=="), str);
        }
    }

    @DexIgnore
    public final boolean a() {
        return wy3.a(this.a, "android.permission.WRITE_SETTINGS");
    }

    @DexIgnore
    public final String b() {
        String string;
        synchronized (this) {
            Log.i("MID", "read mid from Settings.System");
            string = Settings.System.getString(this.a.getContentResolver(), wy3.c("4kU71lN96TJUomD1vOU9lgj9Tw=="));
        }
        return string;
    }
}
