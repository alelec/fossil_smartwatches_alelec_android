package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yp */
public class C3376yp<Data, ResourceType, Transcode> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>> f11306a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.List<? extends com.fossil.blesdk.obfuscated.C2582op<Data, ResourceType, Transcode>> f11307b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.String f11308c;

    @DexIgnore
    public C3376yp(java.lang.Class<Data> cls, java.lang.Class<ResourceType> cls2, java.lang.Class<Transcode> cls3, java.util.List<com.fossil.blesdk.obfuscated.C2582op<Data, ResourceType, Transcode>> list, com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>> g8Var) {
        this.f11306a = g8Var;
        com.fossil.blesdk.obfuscated.C2992tw.m14460a(list);
        this.f11307b = list;
        this.f11308c = "Failed LoadPath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<Transcode> mo18138a(com.fossil.blesdk.obfuscated.C2978to<Data> toVar, com.fossil.blesdk.obfuscated.C2337lo loVar, int i, int i2, com.fossil.blesdk.obfuscated.C2582op.C2583a<ResourceType> aVar) throws com.bumptech.glide.load.engine.GlideException {
        java.util.List<java.lang.Throwable> a = this.f11306a.mo11162a();
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(a);
        java.util.List list = a;
        try {
            com.fossil.blesdk.obfuscated.C1438aq<Transcode> a2 = mo18139a(toVar, loVar, i, i2, aVar, list);
            return a2;
        } finally {
            this.f11306a.mo11163a(list);
        }
    }

    @DexIgnore
    public java.lang.String toString() {
        return "LoadPath{decodePaths=" + java.util.Arrays.toString(this.f11307b.toArray()) + '}';
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C1438aq<Transcode> mo18139a(com.fossil.blesdk.obfuscated.C2978to<Data> toVar, com.fossil.blesdk.obfuscated.C2337lo loVar, int i, int i2, com.fossil.blesdk.obfuscated.C2582op.C2583a<ResourceType> aVar, java.util.List<java.lang.Throwable> list) throws com.bumptech.glide.load.engine.GlideException {
        java.util.List<java.lang.Throwable> list2 = list;
        int size = this.f11307b.size();
        com.fossil.blesdk.obfuscated.C1438aq<Transcode> aqVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            try {
                aqVar = ((com.fossil.blesdk.obfuscated.C2582op) this.f11307b.get(i3)).mo14465a(toVar, i, i2, loVar, aVar);
            } catch (com.bumptech.glide.load.engine.GlideException e) {
                list2.add(e);
            }
            if (aqVar != null) {
                break;
            }
        }
        if (aqVar != null) {
            return aqVar;
        }
        throw new com.bumptech.glide.load.engine.GlideException(this.f11308c, (java.util.List<java.lang.Throwable>) new java.util.ArrayList(list2));
    }
}
