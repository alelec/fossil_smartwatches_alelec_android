package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface fc4 {
    @DexIgnore
    fc4 getCallerFrame();

    @DexIgnore
    StackTraceElement getStackTraceElement();
}
