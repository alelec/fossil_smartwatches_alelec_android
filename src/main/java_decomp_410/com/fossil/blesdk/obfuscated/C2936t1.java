package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.t1 */
public interface C2936t1 {
    @DexIgnore
    /* renamed from: c */
    void mo724c();

    @DexIgnore
    /* renamed from: d */
    boolean mo862d();

    @DexIgnore
    void dismiss();

    @DexIgnore
    /* renamed from: e */
    android.widget.ListView mo864e();
}
