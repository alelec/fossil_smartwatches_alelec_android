package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface tv0 extends uv0, Cloneable {
    @DexIgnore
    tv0 a(sv0 sv0);

    @DexIgnore
    sv0 s();

    @DexIgnore
    sv0 v();
}
