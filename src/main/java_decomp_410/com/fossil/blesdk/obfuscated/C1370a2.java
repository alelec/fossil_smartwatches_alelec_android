package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.a2 */
public class C1370a2 extends android.database.DataSetObservable {

    @DexIgnore
    /* renamed from: n */
    public static /* final */ java.lang.String f3299n; // = com.fossil.blesdk.obfuscated.C1370a2.class.getSimpleName();

    @DexIgnore
    /* renamed from: o */
    public static /* final */ java.lang.Object f3300o; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: p */
    public static /* final */ java.util.Map<java.lang.String, com.fossil.blesdk.obfuscated.C1370a2> f3301p; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.Object f3302a; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C1370a2.C1372b> f3303b; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C1370a2.C1375e> f3304c; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: d */
    public /* final */ android.content.Context f3305d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.String f3306e;

    @DexIgnore
    /* renamed from: f */
    public android.content.Intent f3307f;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C1370a2.C1373c f3308g; // = new com.fossil.blesdk.obfuscated.C1370a2.C1374d();

    @DexIgnore
    /* renamed from: h */
    public int f3309h; // = 50;

    @DexIgnore
    /* renamed from: i */
    public boolean f3310i; // = true;

    @DexIgnore
    /* renamed from: j */
    public boolean f3311j; // = false;

    @DexIgnore
    /* renamed from: k */
    public boolean f3312k; // = true;

    @DexIgnore
    /* renamed from: l */
    public boolean f3313l; // = false;

    @DexIgnore
    /* renamed from: m */
    public com.fossil.blesdk.obfuscated.C1370a2.C1376f f3314m;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.a2$a */
    public interface C1371a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a2$b")
    /* renamed from: com.fossil.blesdk.obfuscated.a2$b */
    public static final class C1372b implements java.lang.Comparable<com.fossil.blesdk.obfuscated.C1370a2.C1372b> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ android.content.pm.ResolveInfo f3315e;

        @DexIgnore
        /* renamed from: f */
        public float f3316f;

        @DexIgnore
        public C1372b(android.content.pm.ResolveInfo resolveInfo) {
            this.f3315e = resolveInfo;
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(com.fossil.blesdk.obfuscated.C1370a2.C1372b bVar) {
            return java.lang.Float.floatToIntBits(bVar.f3316f) - java.lang.Float.floatToIntBits(this.f3316f);
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            return obj != null && com.fossil.blesdk.obfuscated.C1370a2.C1372b.class == obj.getClass() && java.lang.Float.floatToIntBits(this.f3316f) == java.lang.Float.floatToIntBits(((com.fossil.blesdk.obfuscated.C1370a2.C1372b) obj).f3316f);
        }

        @DexIgnore
        public int hashCode() {
            return java.lang.Float.floatToIntBits(this.f3316f) + 31;
        }

        @DexIgnore
        public java.lang.String toString() {
            return "[" + "resolveInfo:" + this.f3315e.toString() + "; weight:" + new java.math.BigDecimal((double) this.f3316f) + "]";
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.a2$c */
    public interface C1373c {
        @DexIgnore
        /* renamed from: a */
        void mo8439a(android.content.Intent intent, java.util.List<com.fossil.blesdk.obfuscated.C1370a2.C1372b> list, java.util.List<com.fossil.blesdk.obfuscated.C1370a2.C1375e> list2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a2$d")
    /* renamed from: com.fossil.blesdk.obfuscated.a2$d */
    public static final class C1374d implements com.fossil.blesdk.obfuscated.C1370a2.C1373c {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.Map<android.content.ComponentName, com.fossil.blesdk.obfuscated.C1370a2.C1372b> f3317a; // = new java.util.HashMap();

        @DexIgnore
        /* renamed from: a */
        public void mo8439a(android.content.Intent intent, java.util.List<com.fossil.blesdk.obfuscated.C1370a2.C1372b> list, java.util.List<com.fossil.blesdk.obfuscated.C1370a2.C1375e> list2) {
            java.util.Map<android.content.ComponentName, com.fossil.blesdk.obfuscated.C1370a2.C1372b> map = this.f3317a;
            map.clear();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                com.fossil.blesdk.obfuscated.C1370a2.C1372b bVar = list.get(i);
                bVar.f3316f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                map.put(new android.content.ComponentName(bVar.f3315e.activityInfo.packageName, bVar.f3315e.activityInfo.name), bVar);
            }
            float f = 1.0f;
            for (int size2 = list2.size() - 1; size2 >= 0; size2--) {
                com.fossil.blesdk.obfuscated.C1370a2.C1375e eVar = list2.get(size2);
                com.fossil.blesdk.obfuscated.C1370a2.C1372b bVar2 = map.get(eVar.f3318a);
                if (bVar2 != null) {
                    bVar2.f3316f += eVar.f3320c * f;
                    f *= 0.95f;
                }
            }
            java.util.Collections.sort(list);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a2$e")
    /* renamed from: com.fossil.blesdk.obfuscated.a2$e */
    public static final class C1375e {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.ComponentName f3318a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ long f3319b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ float f3320c;

        @DexIgnore
        public C1375e(java.lang.String str, long j, float f) {
            this(android.content.ComponentName.unflattenFromString(str), j, f);
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || com.fossil.blesdk.obfuscated.C1370a2.C1375e.class != obj.getClass()) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C1370a2.C1375e eVar = (com.fossil.blesdk.obfuscated.C1370a2.C1375e) obj;
            android.content.ComponentName componentName = this.f3318a;
            if (componentName == null) {
                if (eVar.f3318a != null) {
                    return false;
                }
            } else if (!componentName.equals(eVar.f3318a)) {
                return false;
            }
            return this.f3319b == eVar.f3319b && java.lang.Float.floatToIntBits(this.f3320c) == java.lang.Float.floatToIntBits(eVar.f3320c);
        }

        @DexIgnore
        public int hashCode() {
            android.content.ComponentName componentName = this.f3318a;
            int hashCode = componentName == null ? 0 : componentName.hashCode();
            long j = this.f3319b;
            return ((((hashCode + 31) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + java.lang.Float.floatToIntBits(this.f3320c);
        }

        @DexIgnore
        public java.lang.String toString() {
            return "[" + "; activity:" + this.f3318a + "; time:" + this.f3319b + "; weight:" + new java.math.BigDecimal((double) this.f3320c) + "]";
        }

        @DexIgnore
        public C1375e(android.content.ComponentName componentName, long j, float f) {
            this.f3318a = componentName;
            this.f3319b = j;
            this.f3320c = f;
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.a2$f */
    public interface C1376f {
        @DexIgnore
        /* renamed from: a */
        boolean mo988a(com.fossil.blesdk.obfuscated.C1370a2 a2Var, android.content.Intent intent);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a2$g")
    /* renamed from: com.fossil.blesdk.obfuscated.a2$g */
    public final class C1377g extends android.os.AsyncTask<java.lang.Object, java.lang.Void, java.lang.Void> {
        @DexIgnore
        public C1377g() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x006d, code lost:
            if (r15 != null) goto L_0x006f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
            r15.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0092, code lost:
            if (r15 == null) goto L_0x00d5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00b2, code lost:
            if (r15 == null) goto L_0x00d5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x00d2, code lost:
            if (r15 == null) goto L_0x00d5;
         */
        @DexIgnore
        public java.lang.Void doInBackground(java.lang.Object... objArr) {
            java.util.List list = objArr[0];
            java.lang.String str = objArr[1];
            try {
                java.io.FileOutputStream openFileOutput = com.fossil.blesdk.obfuscated.C1370a2.this.f3305d.openFileOutput(str, 0);
                org.xmlpull.v1.XmlSerializer newSerializer = android.util.Xml.newSerializer();
                try {
                    newSerializer.setOutput(openFileOutput, (java.lang.String) null);
                    newSerializer.startDocument("UTF-8", true);
                    newSerializer.startTag((java.lang.String) null, "historical-records");
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        com.fossil.blesdk.obfuscated.C1370a2.C1375e eVar = (com.fossil.blesdk.obfuscated.C1370a2.C1375e) list.remove(0);
                        newSerializer.startTag((java.lang.String) null, "historical-record");
                        newSerializer.attribute((java.lang.String) null, com.misfit.frameworks.common.constants.Constants.ACTIVITY, eVar.f3318a.flattenToString());
                        newSerializer.attribute((java.lang.String) null, com.sina.weibo.sdk.statistic.LogBuilder.KEY_TIME, java.lang.String.valueOf(eVar.f3319b));
                        newSerializer.attribute((java.lang.String) null, com.misfit.frameworks.common.constants.Constants.PROFILE_KEY_UNITS_WEIGHT, java.lang.String.valueOf(eVar.f3320c));
                        newSerializer.endTag((java.lang.String) null, "historical-record");
                    }
                    newSerializer.endTag((java.lang.String) null, "historical-records");
                    newSerializer.endDocument();
                    com.fossil.blesdk.obfuscated.C1370a2.this.f3310i = true;
                } catch (java.lang.IllegalArgumentException e) {
                    java.lang.String str2 = com.fossil.blesdk.obfuscated.C1370a2.f3299n;
                    android.util.Log.e(str2, "Error writing historical record file: " + com.fossil.blesdk.obfuscated.C1370a2.this.f3306e, e);
                    com.fossil.blesdk.obfuscated.C1370a2.this.f3310i = true;
                } catch (java.lang.IllegalStateException e2) {
                    java.lang.String str3 = com.fossil.blesdk.obfuscated.C1370a2.f3299n;
                    android.util.Log.e(str3, "Error writing historical record file: " + com.fossil.blesdk.obfuscated.C1370a2.this.f3306e, e2);
                    com.fossil.blesdk.obfuscated.C1370a2.this.f3310i = true;
                } catch (java.io.IOException e3) {
                    java.lang.String str4 = com.fossil.blesdk.obfuscated.C1370a2.f3299n;
                    android.util.Log.e(str4, "Error writing historical record file: " + com.fossil.blesdk.obfuscated.C1370a2.this.f3306e, e3);
                    com.fossil.blesdk.obfuscated.C1370a2.this.f3310i = true;
                } catch (Throwable th) {
                    com.fossil.blesdk.obfuscated.C1370a2.this.f3310i = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (java.io.IOException unused) {
                        }
                    }
                    throw th;
                }
                return null;
            } catch (java.io.FileNotFoundException e4) {
                java.lang.String str5 = com.fossil.blesdk.obfuscated.C1370a2.f3299n;
                android.util.Log.e(str5, "Error writing historical record file: " + str, e4);
                return null;
            }
        }
    }

    @DexIgnore
    public C1370a2(android.content.Context context, java.lang.String str) {
        this.f3305d = context.getApplicationContext();
        if (android.text.TextUtils.isEmpty(str) || str.endsWith(".xml")) {
            this.f3306e = str;
            return;
        }
        this.f3306e = str + ".xml";
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1370a2 m4102a(android.content.Context context, java.lang.String str) {
        com.fossil.blesdk.obfuscated.C1370a2 a2Var;
        synchronized (f3300o) {
            a2Var = f3301p.get(str);
            if (a2Var == null) {
                a2Var = new com.fossil.blesdk.obfuscated.C1370a2(context, str);
                f3301p.put(str, a2Var);
            }
        }
        return a2Var;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo8423b() {
        int size;
        synchronized (this.f3302a) {
            mo8419a();
            size = this.f3303b.size();
        }
        return size;
    }

    @DexIgnore
    /* renamed from: c */
    public android.content.pm.ResolveInfo mo8425c() {
        synchronized (this.f3302a) {
            mo8419a();
            if (this.f3303b.isEmpty()) {
                return null;
            }
            android.content.pm.ResolveInfo resolveInfo = this.f3303b.get(0).f3315e;
            return resolveInfo;
        }
    }

    @DexIgnore
    /* renamed from: d */
    public int mo8427d() {
        int size;
        synchronized (this.f3302a) {
            mo8419a();
            size = this.f3304c.size();
        }
        return size;
    }

    @DexIgnore
    /* renamed from: e */
    public final boolean mo8428e() {
        if (!this.f3313l || this.f3307f == null) {
            return false;
        }
        this.f3313l = false;
        this.f3303b.clear();
        java.util.List<android.content.pm.ResolveInfo> queryIntentActivities = this.f3305d.getPackageManager().queryIntentActivities(this.f3307f, 0);
        int size = queryIntentActivities.size();
        for (int i = 0; i < size; i++) {
            this.f3303b.add(new com.fossil.blesdk.obfuscated.C1370a2.C1372b(queryIntentActivities.get(i)));
        }
        return true;
    }

    @DexIgnore
    /* renamed from: f */
    public final void mo8429f() {
        if (!this.f3311j) {
            throw new java.lang.IllegalStateException("No preceding call to #readHistoricalData");
        } else if (this.f3312k) {
            this.f3312k = false;
            if (!android.text.TextUtils.isEmpty(this.f3306e)) {
                new com.fossil.blesdk.obfuscated.C1370a2.C1377g().executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR, new java.lang.Object[]{new java.util.ArrayList(this.f3304c), this.f3306e});
            }
        }
    }

    @DexIgnore
    /* renamed from: g */
    public final void mo8430g() {
        int size = this.f3304c.size() - this.f3309h;
        if (size > 0) {
            this.f3312k = true;
            for (int i = 0; i < size; i++) {
                com.fossil.blesdk.obfuscated.C1370a2.C1375e remove = this.f3304c.remove(0);
            }
        }
    }

    @DexIgnore
    /* renamed from: h */
    public final boolean mo8431h() {
        if (!this.f3310i || !this.f3312k || android.text.TextUtils.isEmpty(this.f3306e)) {
            return false;
        }
        this.f3310i = false;
        this.f3311j = true;
        mo8432i();
        return true;
    }

    @DexIgnore
    /* renamed from: i */
    public final void mo8432i() {
        try {
            java.io.FileInputStream openFileInput = this.f3305d.openFileInput(this.f3306e);
            try {
                org.xmlpull.v1.XmlPullParser newPullParser = android.util.Xml.newPullParser();
                newPullParser.setInput(openFileInput, "UTF-8");
                int i = 0;
                while (i != 1 && i != 2) {
                    i = newPullParser.next();
                }
                if ("historical-records".equals(newPullParser.getName())) {
                    java.util.List<com.fossil.blesdk.obfuscated.C1370a2.C1375e> list = this.f3304c;
                    list.clear();
                    while (true) {
                        int next = newPullParser.next();
                        if (next == 1) {
                            if (openFileInput == null) {
                                return;
                            }
                        } else if (!(next == 3 || next == 4)) {
                            if ("historical-record".equals(newPullParser.getName())) {
                                list.add(new com.fossil.blesdk.obfuscated.C1370a2.C1375e(newPullParser.getAttributeValue((java.lang.String) null, com.misfit.frameworks.common.constants.Constants.ACTIVITY), java.lang.Long.parseLong(newPullParser.getAttributeValue((java.lang.String) null, com.sina.weibo.sdk.statistic.LogBuilder.KEY_TIME)), java.lang.Float.parseFloat(newPullParser.getAttributeValue((java.lang.String) null, com.misfit.frameworks.common.constants.Constants.PROFILE_KEY_UNITS_WEIGHT))));
                            } else {
                                throw new org.xmlpull.v1.XmlPullParserException("Share records file not well-formed.");
                            }
                        }
                    }
                    try {
                        openFileInput.close();
                    } catch (java.io.IOException unused) {
                    }
                } else {
                    throw new org.xmlpull.v1.XmlPullParserException("Share records file does not start with historical-records tag.");
                }
            } catch (org.xmlpull.v1.XmlPullParserException e) {
                java.lang.String str = f3299n;
                android.util.Log.e(str, "Error reading historical recrod file: " + this.f3306e, e);
                if (openFileInput == null) {
                }
            } catch (java.io.IOException e2) {
                java.lang.String str2 = f3299n;
                android.util.Log.e(str2, "Error reading historical recrod file: " + this.f3306e, e2);
                if (openFileInput == null) {
                }
            } catch (Throwable th) {
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (java.io.IOException unused2) {
                    }
                }
                throw th;
            }
        } catch (java.io.FileNotFoundException unused3) {
        }
    }

    @DexIgnore
    /* renamed from: j */
    public final boolean mo8433j() {
        if (this.f3308g == null || this.f3307f == null || this.f3303b.isEmpty() || this.f3304c.isEmpty()) {
            return false;
        }
        this.f3308g.mo8439a(this.f3307f, this.f3303b, java.util.Collections.unmodifiableList(this.f3304c));
        return true;
    }

    @DexIgnore
    /* renamed from: b */
    public android.content.pm.ResolveInfo mo8424b(int i) {
        android.content.pm.ResolveInfo resolveInfo;
        synchronized (this.f3302a) {
            mo8419a();
            resolveInfo = this.f3303b.get(i).f3315e;
        }
        return resolveInfo;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo8426c(int i) {
        synchronized (this.f3302a) {
            mo8419a();
            com.fossil.blesdk.obfuscated.C1370a2.C1372b bVar = this.f3303b.get(i);
            com.fossil.blesdk.obfuscated.C1370a2.C1372b bVar2 = this.f3303b.get(0);
            mo8422a(new com.fossil.blesdk.obfuscated.C1370a2.C1375e(new android.content.ComponentName(bVar.f3315e.activityInfo.packageName, bVar.f3315e.activityInfo.name), java.lang.System.currentTimeMillis(), bVar2 != null ? (bVar2.f3316f - bVar.f3316f) + 5.0f : 1.0f));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8420a(android.content.Intent intent) {
        synchronized (this.f3302a) {
            if (this.f3307f != intent) {
                this.f3307f = intent;
                this.f3313l = true;
                mo8419a();
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int mo8417a(android.content.pm.ResolveInfo resolveInfo) {
        synchronized (this.f3302a) {
            mo8419a();
            java.util.List<com.fossil.blesdk.obfuscated.C1370a2.C1372b> list = this.f3303b;
            int size = list.size();
            for (int i = 0; i < size; i++) {
                if (list.get(i).f3315e == resolveInfo) {
                    return i;
                }
            }
            return -1;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public android.content.Intent mo8418a(int i) {
        synchronized (this.f3302a) {
            if (this.f3307f == null) {
                return null;
            }
            mo8419a();
            com.fossil.blesdk.obfuscated.C1370a2.C1372b bVar = this.f3303b.get(i);
            android.content.ComponentName componentName = new android.content.ComponentName(bVar.f3315e.activityInfo.packageName, bVar.f3315e.activityInfo.name);
            android.content.Intent intent = new android.content.Intent(this.f3307f);
            intent.setComponent(componentName);
            if (this.f3314m != null) {
                if (this.f3314m.mo988a(this, new android.content.Intent(intent))) {
                    return null;
                }
            }
            mo8422a(new com.fossil.blesdk.obfuscated.C1370a2.C1375e(componentName, java.lang.System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8421a(com.fossil.blesdk.obfuscated.C1370a2.C1376f fVar) {
        synchronized (this.f3302a) {
            this.f3314m = fVar;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo8419a() {
        boolean e = mo8428e() | mo8431h();
        mo8430g();
        if (e) {
            mo8433j();
            notifyChanged();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo8422a(com.fossil.blesdk.obfuscated.C1370a2.C1375e eVar) {
        boolean add = this.f3304c.add(eVar);
        if (add) {
            this.f3312k = true;
            mo8430g();
            mo8429f();
            mo8433j();
            notifyChanged();
        }
        return add;
    }
}
