package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x24 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;
    @DexIgnore
    public /* final */ /* synthetic */ k04 f;
    @DexIgnore
    public /* final */ /* synthetic */ m04 g;

    @DexIgnore
    public x24(Context context, k04 k04, m04 m04) {
        this.e = context;
        this.f = k04;
        this.g = m04;
    }

    @DexIgnore
    public final void run() {
        try {
            l04 l04 = new l04(this.e, j04.a(this.e, false, this.f), this.g.a, this.f);
            l04.g().c = this.g.c;
            new c14(l04).a();
        } catch (Throwable th) {
            j04.m.a(th);
            j04.a(this.e, th);
        }
    }
}
