package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface qp4 extends rp4 {
    @DexIgnore
    String d();

    @DexIgnore
    void writeTo(OutputStream outputStream) throws IOException;
}
