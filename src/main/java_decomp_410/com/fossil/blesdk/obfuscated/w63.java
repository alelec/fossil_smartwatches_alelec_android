package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w63 implements Factory<MicroAppPresenter> {
    @DexIgnore
    public static MicroAppPresenter a(u63 u63, CategoryRepository categoryRepository) {
        return new MicroAppPresenter(u63, categoryRepository);
    }
}
