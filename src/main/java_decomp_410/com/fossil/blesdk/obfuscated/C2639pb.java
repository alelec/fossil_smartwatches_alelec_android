package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pb */
public abstract class C2639pb<T> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.concurrent.Executor f8321a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ androidx.lifecycle.LiveData<T> f8322b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.concurrent.atomic.AtomicBoolean f8323c; // = new java.util.concurrent.atomic.AtomicBoolean(true);

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.concurrent.atomic.AtomicBoolean f8324d; // = new java.util.concurrent.atomic.AtomicBoolean(false);

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.Runnable f8325e; // = new com.fossil.blesdk.obfuscated.C2639pb.C2641b();

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.lang.Runnable f8326f; // = new com.fossil.blesdk.obfuscated.C2639pb.C2642c();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pb$a")
    /* renamed from: com.fossil.blesdk.obfuscated.pb$a */
    public class C2640a extends androidx.lifecycle.LiveData<T> {
        @DexIgnore
        public C2640a() {
        }

        @DexIgnore
        /* renamed from: d */
        public void mo2286d() {
            com.fossil.blesdk.obfuscated.C2639pb pbVar = com.fossil.blesdk.obfuscated.C2639pb.this;
            pbVar.f8321a.execute(pbVar.f8325e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pb$b")
    /* renamed from: com.fossil.blesdk.obfuscated.pb$b */
    public class C2641b implements java.lang.Runnable {
        @DexIgnore
        public C2641b() {
        }

        @DexIgnore
        public void run() {
            boolean z;
            do {
                if (com.fossil.blesdk.obfuscated.C2639pb.this.f8324d.compareAndSet(false, true)) {
                    java.lang.Object obj = null;
                    z = false;
                    while (com.fossil.blesdk.obfuscated.C2639pb.this.f8323c.compareAndSet(true, false)) {
                        try {
                            obj = com.fossil.blesdk.obfuscated.C2639pb.this.mo13929a();
                            z = true;
                        } finally {
                            com.fossil.blesdk.obfuscated.C2639pb.this.f8324d.set(false);
                        }
                    }
                    if (z) {
                        com.fossil.blesdk.obfuscated.C2639pb.this.f8322b.mo2280a(obj);
                    }
                } else {
                    z = false;
                }
                if (!z) {
                    return;
                }
            } while (com.fossil.blesdk.obfuscated.C2639pb.this.f8323c.get());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pb$c")
    /* renamed from: com.fossil.blesdk.obfuscated.pb$c */
    public class C2642c implements java.lang.Runnable {
        @DexIgnore
        public C2642c() {
        }

        @DexIgnore
        public void run() {
            boolean c = com.fossil.blesdk.obfuscated.C2639pb.this.f8322b.mo2285c();
            if (com.fossil.blesdk.obfuscated.C2639pb.this.f8323c.compareAndSet(false, true) && c) {
                com.fossil.blesdk.obfuscated.C2639pb pbVar = com.fossil.blesdk.obfuscated.C2639pb.this;
                pbVar.f8321a.execute(pbVar.f8325e);
            }
        }
    }

    @DexIgnore
    public C2639pb(java.util.concurrent.Executor executor) {
        this.f8321a = executor;
        this.f8322b = new com.fossil.blesdk.obfuscated.C2639pb.C2640a();
    }

    @DexIgnore
    /* renamed from: a */
    public abstract T mo13929a();

    @DexIgnore
    /* renamed from: b */
    public androidx.lifecycle.LiveData<T> mo14653b() {
        return this.f8322b;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo14654c() {
        com.fossil.blesdk.obfuscated.C1919h3.m7768c().mo12211b(this.f8326f);
    }
}
