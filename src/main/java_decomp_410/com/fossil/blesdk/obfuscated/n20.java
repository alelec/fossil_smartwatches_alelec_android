package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.notification.AppNotification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n20 extends s20<AppNotification, qa4> {
    @DexIgnore
    public static /* final */ k20<AppNotification>[] a; // = {new a(), new b()};
    @DexIgnore
    public static /* final */ l20<qa4>[] b; // = new l20[0];
    @DexIgnore
    public static /* final */ n20 c; // = new n20();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends q20<AppNotification> {
        @DexIgnore
        public byte[] a(AppNotification appNotification) {
            kd4.b(appNotification, "entries");
            return n20.c.a(appNotification);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends r20<AppNotification> {
        @DexIgnore
        public byte[] a(AppNotification appNotification) {
            kd4.b(appNotification, "entries");
            return n20.c.a(appNotification);
        }
    }

    @DexIgnore
    public l20<qa4>[] b() {
        return b;
    }

    @DexIgnore
    public k20<AppNotification>[] a() {
        return a;
    }

    @DexIgnore
    public final byte[] a(AppNotification appNotification) {
        return appNotification.getData$blesdk_productionRelease();
    }
}
