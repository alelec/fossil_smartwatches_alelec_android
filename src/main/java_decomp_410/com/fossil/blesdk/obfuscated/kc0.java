package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.de0;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kc0 extends oc0<Status> {
    @DexIgnore
    public kc0(ge0 ge0) {
        super(ge0);
    }

    @DexIgnore
    public final /* synthetic */ me0 a(Status status) {
        return status;
    }

    @DexIgnore
    public final /* synthetic */ void a(de0.b bVar) throws RemoteException {
        ic0 ic0 = (ic0) bVar;
        ((uc0) ic0.x()).a(new lc0(this), ic0.G());
    }
}
