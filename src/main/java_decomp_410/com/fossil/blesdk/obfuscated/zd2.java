package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zd2 extends yd2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j u; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray v; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public long t;

    /*
    static {
        v.put(R.id.iv_back, 1);
        v.put(R.id.ftv_title, 2);
        v.put(R.id.ftv_get_notified, 3);
        v.put(R.id.ftv_get_notified_description, 4);
        v.put(R.id.nsdv, 5);
        v.put(R.id.ftv_tap_to_assign, 6);
    }
    */

    @DexIgnore
    public zd2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 7, u, v));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.t = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.t != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.t = 1;
        }
        g();
    }

    @DexIgnore
    public zd2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[3], objArr[4], objArr[6], objArr[2], objArr[1], objArr[5]);
        this.t = -1;
        this.s = objArr[0];
        this.s.setTag((Object) null);
        a(view);
        f();
    }
}
