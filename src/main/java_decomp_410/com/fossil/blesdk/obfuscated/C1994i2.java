package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.i2 */
public interface C1994i2 {
    @DexIgnore
    /* renamed from: a */
    void mo439a(int i);

    @DexIgnore
    /* renamed from: a */
    void mo441a(android.view.Menu menu, com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar);

    @DexIgnore
    /* renamed from: a */
    boolean mo442a();

    @DexIgnore
    /* renamed from: b */
    void mo445b();

    @DexIgnore
    /* renamed from: c */
    boolean mo446c();

    @DexIgnore
    /* renamed from: d */
    boolean mo448d();

    @DexIgnore
    /* renamed from: e */
    boolean mo450e();

    @DexIgnore
    /* renamed from: f */
    boolean mo451f();

    @DexIgnore
    /* renamed from: g */
    void mo453g();

    @DexIgnore
    void setWindowCallback(android.view.Window.Callback callback);

    @DexIgnore
    void setWindowTitle(java.lang.CharSequence charSequence);
}
