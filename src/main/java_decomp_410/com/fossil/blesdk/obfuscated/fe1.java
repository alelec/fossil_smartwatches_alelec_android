package com.fossil.blesdk.obfuscated;

import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fe1 {
    @DexIgnore
    public static /* final */ int[] MapAttrs; // = {R.attr.ambientEnabled, R.attr.cameraBearing, R.attr.cameraMaxZoomPreference, R.attr.cameraMinZoomPreference, R.attr.cameraTargetLat, R.attr.cameraTargetLng, R.attr.cameraTilt, R.attr.cameraZoom, R.attr.latLngBoundsNorthEastLatitude, R.attr.latLngBoundsNorthEastLongitude, R.attr.latLngBoundsSouthWestLatitude, R.attr.latLngBoundsSouthWestLongitude, R.attr.liteMode, R.attr.mapType, R.attr.uiCompass, R.attr.uiMapToolbar, R.attr.uiRotateGestures, R.attr.uiScrollGestures, R.attr.uiScrollGesturesDuringRotateOrZoom, R.attr.uiTiltGestures, R.attr.uiZoomControls, R.attr.uiZoomGestures, R.attr.useViewLifecycle, R.attr.zOrderOnTop};
    @DexIgnore
    public static /* final */ int MapAttrs_ambientEnabled; // = 0;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraBearing; // = 1;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraMaxZoomPreference; // = 2;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraMinZoomPreference; // = 3;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraTargetLat; // = 4;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraTargetLng; // = 5;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraTilt; // = 6;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraZoom; // = 7;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsNorthEastLatitude; // = 8;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsNorthEastLongitude; // = 9;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsSouthWestLatitude; // = 10;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsSouthWestLongitude; // = 11;
    @DexIgnore
    public static /* final */ int MapAttrs_liteMode; // = 12;
    @DexIgnore
    public static /* final */ int MapAttrs_mapType; // = 13;
    @DexIgnore
    public static /* final */ int MapAttrs_uiCompass; // = 14;
    @DexIgnore
    public static /* final */ int MapAttrs_uiMapToolbar; // = 15;
    @DexIgnore
    public static /* final */ int MapAttrs_uiRotateGestures; // = 16;
    @DexIgnore
    public static /* final */ int MapAttrs_uiScrollGestures; // = 17;
    @DexIgnore
    public static /* final */ int MapAttrs_uiScrollGesturesDuringRotateOrZoom; // = 18;
    @DexIgnore
    public static /* final */ int MapAttrs_uiTiltGestures; // = 19;
    @DexIgnore
    public static /* final */ int MapAttrs_uiZoomControls; // = 20;
    @DexIgnore
    public static /* final */ int MapAttrs_uiZoomGestures; // = 21;
    @DexIgnore
    public static /* final */ int MapAttrs_useViewLifecycle; // = 22;
    @DexIgnore
    public static /* final */ int MapAttrs_zOrderOnTop; // = 23;
}
