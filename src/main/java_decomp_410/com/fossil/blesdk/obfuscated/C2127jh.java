package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jh */
public class C2127jh extends com.fossil.blesdk.obfuscated.C1876gi {

    @DexIgnore
    /* renamed from: b */
    public float f6504b; // = 3.0f;

    @DexIgnore
    /* renamed from: c */
    public int f6505c; // = 80;

    @DexIgnore
    /* renamed from: a */
    public void mo12356a(int i) {
        this.f6505c = i;
    }

    @DexIgnore
    /* renamed from: a */
    public long mo12355a(android.view.ViewGroup viewGroup, androidx.transition.Transition transition, com.fossil.blesdk.obfuscated.C2654ph phVar, com.fossil.blesdk.obfuscated.C2654ph phVar2) {
        int i;
        int i2;
        int i3;
        com.fossil.blesdk.obfuscated.C2654ph phVar3 = phVar;
        if (phVar3 == null && phVar2 == null) {
            return 0;
        }
        android.graphics.Rect c = transition.mo3546c();
        if (phVar2 == null || mo11244b(phVar3) == 0) {
            i = -1;
        } else {
            phVar3 = phVar2;
            i = 1;
        }
        int c2 = mo11245c(phVar3);
        int d = mo11246d(phVar3);
        int[] iArr = new int[2];
        viewGroup.getLocationOnScreen(iArr);
        int round = iArr[0] + java.lang.Math.round(viewGroup.getTranslationX());
        int round2 = iArr[1] + java.lang.Math.round(viewGroup.getTranslationY());
        int width = round + viewGroup.getWidth();
        int height = round2 + viewGroup.getHeight();
        if (c != null) {
            i3 = c.centerX();
            i2 = c.centerY();
        } else {
            i3 = (round + width) / 2;
            i2 = (round2 + height) / 2;
        }
        float a = ((float) mo12353a(viewGroup, c2, d, i3, i2, round, round2, width, height)) / ((float) mo12354a(viewGroup));
        long b = transition.mo3539b();
        if (b < 0) {
            b = 300;
        }
        return (long) java.lang.Math.round((((float) (b * ((long) i))) / this.f6504b) * a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0026, code lost:
        if (r4 != false) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r4 != false) goto L_0x0015;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0017, code lost:
        r0 = 3;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0051  */
    /* renamed from: a */
    public final int mo12353a(android.view.View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        int i9 = this.f6505c;
        boolean z = true;
        if (i9 != 8388611) {
            if (i9 == 8388613) {
                if (com.fossil.blesdk.obfuscated.C1776f9.m6845k(view) != 1) {
                    z = false;
                }
            }
            if (i9 != 3) {
                return (i7 - i) + java.lang.Math.abs(i4 - i2);
            }
            if (i9 == 5) {
                return (i - i5) + java.lang.Math.abs(i4 - i2);
            }
            if (i9 == 48) {
                return (i8 - i2) + java.lang.Math.abs(i3 - i);
            }
            if (i9 != 80) {
                return 0;
            }
            return (i2 - i6) + java.lang.Math.abs(i3 - i);
        } else if (com.fossil.blesdk.obfuscated.C1776f9.m6845k(view) != 1) {
            z = false;
        }
        i9 = 5;
        if (i9 != 3) {
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo12354a(android.view.ViewGroup viewGroup) {
        int i = this.f6505c;
        if (i == 3 || i == 5 || i == 8388611 || i == 8388613) {
            return viewGroup.getWidth();
        }
        return viewGroup.getHeight();
    }
}
