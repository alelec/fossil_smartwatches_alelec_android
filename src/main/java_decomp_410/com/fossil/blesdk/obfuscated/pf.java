package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pf {
    @DexIgnore
    public static /* final */ String[] m; // = {"UPDATE", "DELETE", "INSERT"};
    @DexIgnore
    public /* final */ g4<String, Integer> a;
    @DexIgnore
    public /* final */ String[] b;
    @DexIgnore
    public Map<String, Set<String>> c;
    @DexIgnore
    public /* final */ RoomDatabase d;
    @DexIgnore
    public AtomicBoolean e; // = new AtomicBoolean(false);
    @DexIgnore
    public volatile boolean f; // = false;
    @DexIgnore
    public volatile kg g;
    @DexIgnore
    public b h;
    @DexIgnore
    public /* final */ of i;
    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public /* final */ l3<c, d> j; // = new l3<>();
    @DexIgnore
    public qf k;
    @DexIgnore
    public Runnable l; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public final Set<Integer> a() {
            h4 h4Var = new h4();
            Cursor query = pf.this.d.query(new fg("SELECT * FROM room_table_modification_log WHERE invalidated = 1;"));
            while (query.moveToNext()) {
                try {
                    h4Var.add(Integer.valueOf(query.getInt(0)));
                } catch (Throwable th) {
                    query.close();
                    throw th;
                }
            }
            query.close();
            if (!h4Var.isEmpty()) {
                pf.this.g.n();
            }
            return h4Var;
        }

        @DexIgnore
        public void run() {
            gg a;
            Lock closeLock = pf.this.d.getCloseLock();
            Set<Integer> set = null;
            try {
                closeLock.lock();
                if (!pf.this.a()) {
                    closeLock.unlock();
                } else if (!pf.this.e.compareAndSet(true, false)) {
                    closeLock.unlock();
                } else if (pf.this.d.inTransaction()) {
                    closeLock.unlock();
                } else {
                    if (pf.this.d.mWriteAheadLoggingEnabled) {
                        a = pf.this.d.getOpenHelper().a();
                        a.s();
                        set = a();
                        a.u();
                        a.v();
                    } else {
                        set = a();
                    }
                    closeLock.unlock();
                    if (set != null && !set.isEmpty()) {
                        synchronized (pf.this.j) {
                            Iterator<Map.Entry<c, d>> it = pf.this.j.iterator();
                            while (it.hasNext()) {
                                ((d) it.next().getValue()).a(set);
                            }
                        }
                    }
                }
            } catch (SQLiteException | IllegalStateException e2) {
                try {
                    Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", e2);
                } catch (Throwable th) {
                    closeLock.unlock();
                    throw th;
                }
            } catch (Throwable th2) {
                a.v();
                throw th2;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends c {
        @DexIgnore
        public /* final */ pf a;
        @DexIgnore
        public /* final */ WeakReference<c> b;

        @DexIgnore
        public e(pf pfVar, c cVar) {
            super(cVar.mTables);
            this.a = pfVar;
            this.b = new WeakReference<>(cVar);
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            c cVar = (c) this.b.get();
            if (cVar == null) {
                this.a.c((c) this);
            } else {
                cVar.onInvalidated(set);
            }
        }
    }

    @DexIgnore
    public pf(RoomDatabase roomDatabase, Map<String, String> map, Map<String, Set<String>> map2, String... strArr) {
        this.d = roomDatabase;
        this.h = new b(strArr.length);
        this.a = new g4<>();
        this.c = map2;
        this.i = new of(this.d);
        int length = strArr.length;
        this.b = new String[length];
        for (int i2 = 0; i2 < length; i2++) {
            String lowerCase = strArr[i2].toLowerCase(Locale.US);
            this.a.put(lowerCase, Integer.valueOf(i2));
            String str = map.get(strArr[i2]);
            if (str != null) {
                this.b[i2] = str.toLowerCase(Locale.US);
            } else {
                this.b[i2] = lowerCase;
            }
        }
        for (Map.Entry next : map.entrySet()) {
            String lowerCase2 = ((String) next.getValue()).toLowerCase(Locale.US);
            if (this.a.containsKey(lowerCase2)) {
                String lowerCase3 = ((String) next.getKey()).toLowerCase(Locale.US);
                g4<String, Integer> g4Var = this.a;
                g4Var.put(lowerCase3, g4Var.get(lowerCase2));
            }
        }
    }

    @DexIgnore
    public void a(gg ggVar) {
        synchronized (this) {
            if (this.f) {
                Log.e("ROOM", "Invalidation tracker is initialized twice :/.");
                return;
            }
            ggVar.b("PRAGMA temp_store = MEMORY;");
            ggVar.b("PRAGMA recursive_triggers='ON';");
            ggVar.b("CREATE TEMP TABLE room_table_modification_log(table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)");
            b(ggVar);
            this.g = ggVar.c("UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1 ");
            this.f = true;
        }
    }

    @DexIgnore
    public final void b(gg ggVar, int i2) {
        String str = this.b[i2];
        StringBuilder sb = new StringBuilder();
        for (String a2 : m) {
            sb.setLength(0);
            sb.append("DROP TRIGGER IF EXISTS ");
            a(sb, str, a2);
            ggVar.b(sb.toString());
        }
    }

    @DexIgnore
    public final String[] c(String[] strArr) {
        String[] b2 = b(strArr);
        int length = b2.length;
        int i2 = 0;
        while (i2 < length) {
            String str = b2[i2];
            if (this.a.containsKey(str.toLowerCase(Locale.US))) {
                i2++;
            } else {
                throw new IllegalArgumentException("There is no table with name " + str);
            }
        }
        return b2;
    }

    @DexIgnore
    public void d() {
        qf qfVar = this.k;
        if (qfVar != null) {
            qfVar.a();
            this.k = null;
        }
    }

    @DexIgnore
    public void e() {
        if (this.d.isOpen()) {
            b(this.d.getOpenHelper().a());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c {
        @DexIgnore
        public /* final */ String[] mTables;

        @DexIgnore
        public c(String str, String... strArr) {
            this.mTables = (String[]) Arrays.copyOf(strArr, strArr.length + 1);
            this.mTables[strArr.length] = str;
        }

        @DexIgnore
        public boolean isRemote() {
            return false;
        }

        @DexIgnore
        public abstract void onInvalidated(Set<String> set);

        @DexIgnore
        public c(String[] strArr) {
            this.mTables = (String[]) Arrays.copyOf(strArr, strArr.length);
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public void c(c cVar) {
        d remove;
        synchronized (this.j) {
            remove = this.j.remove(cVar);
        }
        if (remove != null && this.h.b(remove.a)) {
            e();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ long[] a;
        @DexIgnore
        public /* final */ boolean[] b;
        @DexIgnore
        public /* final */ int[] c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public boolean e;

        @DexIgnore
        public b(int i) {
            this.a = new long[i];
            this.b = new boolean[i];
            this.c = new int[i];
            Arrays.fill(this.a, 0);
            Arrays.fill(this.b, false);
        }

        @DexIgnore
        public boolean a(int... iArr) {
            boolean z;
            synchronized (this) {
                z = false;
                for (int i : iArr) {
                    long j = this.a[i];
                    this.a[i] = 1 + j;
                    if (j == 0) {
                        this.d = true;
                        z = true;
                    }
                }
            }
            return z;
        }

        @DexIgnore
        public boolean b(int... iArr) {
            boolean z;
            synchronized (this) {
                z = false;
                for (int i : iArr) {
                    long j = this.a[i];
                    this.a[i] = j - 1;
                    if (j == 1) {
                        this.d = true;
                        z = true;
                    }
                }
            }
            return z;
        }

        @DexIgnore
        public int[] a() {
            synchronized (this) {
                if (this.d) {
                    if (!this.e) {
                        int length = this.a.length;
                        int i = 0;
                        while (true) {
                            int i2 = 1;
                            if (i < length) {
                                boolean z = this.a[i] > 0;
                                if (z != this.b[i]) {
                                    int[] iArr = this.c;
                                    if (!z) {
                                        i2 = 2;
                                    }
                                    iArr[i] = i2;
                                } else {
                                    this.c[i] = 0;
                                }
                                this.b[i] = z;
                                i++;
                            } else {
                                this.e = true;
                                this.d = false;
                                int[] iArr2 = this.c;
                                return iArr2;
                            }
                        }
                    }
                }
                return null;
            }
        }

        @DexIgnore
        public void b() {
            synchronized (this) {
                this.e = false;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public /* final */ int[] a;
        @DexIgnore
        public /* final */ String[] b;
        @DexIgnore
        public /* final */ c c;
        @DexIgnore
        public /* final */ Set<String> d;

        @DexIgnore
        public d(c cVar, int[] iArr, String[] strArr) {
            this.c = cVar;
            this.a = iArr;
            this.b = strArr;
            if (iArr.length == 1) {
                h4 h4Var = new h4();
                h4Var.add(this.b[0]);
                this.d = Collections.unmodifiableSet(h4Var);
                return;
            }
            this.d = null;
        }

        @DexIgnore
        public void a(Set<Integer> set) {
            int length = this.a.length;
            Set set2 = null;
            for (int i = 0; i < length; i++) {
                if (set.contains(Integer.valueOf(this.a[i]))) {
                    if (length == 1) {
                        set2 = this.d;
                    } else {
                        if (set2 == null) {
                            set2 = new h4(length);
                        }
                        set2.add(this.b[i]);
                    }
                }
            }
            if (set2 != null) {
                this.c.onInvalidated(set2);
            }
        }

        @DexIgnore
        public void a(String[] strArr) {
            Set<String> set = null;
            if (this.b.length == 1) {
                int length = strArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (strArr[i].equalsIgnoreCase(this.b[0])) {
                        set = this.d;
                        break;
                    } else {
                        i++;
                    }
                }
            } else {
                h4 h4Var = new h4();
                for (String str : strArr) {
                    String[] strArr2 = this.b;
                    int length2 = strArr2.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length2) {
                            break;
                        }
                        String str2 = strArr2[i2];
                        if (str2.equalsIgnoreCase(str)) {
                            h4Var.add(str2);
                            break;
                        }
                        i2++;
                    }
                }
                if (h4Var.size() > 0) {
                    set = h4Var;
                }
            }
            if (set != null) {
                this.c.onInvalidated(set);
            }
        }
    }

    @DexIgnore
    public final String[] b(String[] strArr) {
        h4 h4Var = new h4();
        for (String str : strArr) {
            String lowerCase = str.toLowerCase(Locale.US);
            if (this.c.containsKey(lowerCase)) {
                h4Var.addAll(this.c.get(lowerCase));
            } else {
                h4Var.add(str);
            }
        }
        return (String[]) h4Var.toArray(new String[h4Var.size()]);
    }

    @DexIgnore
    public void c() {
        e();
        this.l.run();
    }

    @DexIgnore
    public void a(Context context, String str) {
        this.k = new qf(context, str, this, this.d.getQueryExecutor());
    }

    @DexIgnore
    public static void a(StringBuilder sb, String str, String str2) {
        sb.append("`");
        sb.append("room_table_modification_trigger_");
        sb.append(str);
        sb.append("_");
        sb.append(str2);
        sb.append("`");
    }

    @DexIgnore
    public void b(c cVar) {
        a((c) new e(this, cVar));
    }

    @DexIgnore
    public void b() {
        if (this.e.compareAndSet(false, true)) {
            this.d.getQueryExecutor().execute(this.l);
        }
    }

    @DexIgnore
    public void b(gg ggVar) {
        if (!ggVar.x()) {
            while (true) {
                try {
                    Lock closeLock = this.d.getCloseLock();
                    closeLock.lock();
                    try {
                        int[] a2 = this.h.a();
                        if (a2 == null) {
                            closeLock.unlock();
                            return;
                        }
                        int length = a2.length;
                        ggVar.s();
                        for (int i2 = 0; i2 < length; i2++) {
                            int i3 = a2[i2];
                            if (i3 == 1) {
                                a(ggVar, i2);
                            } else if (i3 == 2) {
                                b(ggVar, i2);
                            }
                        }
                        ggVar.u();
                        ggVar.v();
                        this.h.b();
                        closeLock.unlock();
                    } catch (Throwable th) {
                        closeLock.unlock();
                        throw th;
                    }
                } catch (SQLiteException | IllegalStateException e2) {
                    Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", e2);
                    return;
                }
            }
        }
    }

    @DexIgnore
    public final void a(gg ggVar, int i2) {
        ggVar.b("INSERT OR IGNORE INTO room_table_modification_log VALUES(" + i2 + ", 0)");
        String str = this.b[i2];
        StringBuilder sb = new StringBuilder();
        for (String str2 : m) {
            sb.setLength(0);
            sb.append("CREATE TEMP TRIGGER IF NOT EXISTS ");
            a(sb, str, str2);
            sb.append(" AFTER ");
            sb.append(str2);
            sb.append(" ON `");
            sb.append(str);
            sb.append("` BEGIN UPDATE ");
            sb.append("room_table_modification_log");
            sb.append(" SET ");
            sb.append("invalidated");
            sb.append(" = 1");
            sb.append(" WHERE ");
            sb.append("table_id");
            sb.append(" = ");
            sb.append(i2);
            sb.append(" AND ");
            sb.append("invalidated");
            sb.append(" = 0");
            sb.append("; END");
            ggVar.b(sb.toString());
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public void a(c cVar) {
        d b2;
        String[] b3 = b(cVar.mTables);
        int[] iArr = new int[b3.length];
        int length = b3.length;
        int i2 = 0;
        while (i2 < length) {
            Integer num = this.a.get(b3[i2].toLowerCase(Locale.US));
            if (num != null) {
                iArr[i2] = num.intValue();
                i2++;
            } else {
                throw new IllegalArgumentException("There is no table with name " + b3[i2]);
            }
        }
        d dVar = new d(cVar, iArr, b3);
        synchronized (this.j) {
            b2 = this.j.b(cVar, dVar);
        }
        if (b2 == null && this.h.a(iArr)) {
            e();
        }
    }

    @DexIgnore
    public boolean a() {
        if (!this.d.isOpen()) {
            return false;
        }
        if (!this.f) {
            this.d.getOpenHelper().a();
        }
        if (this.f) {
            return true;
        }
        Log.e("ROOM", "database is not initialized even though it is open");
        return false;
    }

    @DexIgnore
    public void a(String... strArr) {
        synchronized (this.j) {
            Iterator<Map.Entry<c, d>> it = this.j.iterator();
            while (it.hasNext()) {
                Map.Entry next = it.next();
                if (!((c) next.getKey()).isRemote()) {
                    ((d) next.getValue()).a(strArr);
                }
            }
        }
    }

    @DexIgnore
    public <T> LiveData<T> a(String[] strArr, boolean z, Callable<T> callable) {
        return this.i.a(c(strArr), z, callable);
    }
}
