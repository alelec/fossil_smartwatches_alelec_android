package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class c5 {
    @DexIgnore
    public static boolean[] a; // = new boolean[3];

    @DexIgnore
    public static void a(y4 y4Var, q4 q4Var, ConstraintWidget constraintWidget) {
        if (y4Var.C[0] != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && constraintWidget.C[0] == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            int i = constraintWidget.s.e;
            int t = y4Var.t() - constraintWidget.u.e;
            ConstraintAnchor constraintAnchor = constraintWidget.s;
            constraintAnchor.i = q4Var.a((Object) constraintAnchor);
            ConstraintAnchor constraintAnchor2 = constraintWidget.u;
            constraintAnchor2.i = q4Var.a((Object) constraintAnchor2);
            q4Var.a(constraintWidget.s.i, i);
            q4Var.a(constraintWidget.u.i, t);
            constraintWidget.a = 2;
            constraintWidget.a(i, t);
        }
        if (y4Var.C[1] != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && constraintWidget.C[1] == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            int i2 = constraintWidget.t.e;
            int j = y4Var.j() - constraintWidget.v.e;
            ConstraintAnchor constraintAnchor3 = constraintWidget.t;
            constraintAnchor3.i = q4Var.a((Object) constraintAnchor3);
            ConstraintAnchor constraintAnchor4 = constraintWidget.v;
            constraintAnchor4.i = q4Var.a((Object) constraintAnchor4);
            q4Var.a(constraintWidget.t.i, i2);
            q4Var.a(constraintWidget.v.i, j);
            if (constraintWidget.Q > 0 || constraintWidget.s() == 8) {
                ConstraintAnchor constraintAnchor5 = constraintWidget.w;
                constraintAnchor5.i = q4Var.a((Object) constraintAnchor5);
                q4Var.a(constraintWidget.w.i, constraintWidget.Q + i2);
            }
            constraintWidget.b = 2;
            constraintWidget.e(i2, j);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003b A[RETURN] */
    public static boolean a(ConstraintWidget constraintWidget, int i) {
        ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = constraintWidget.C;
        if (dimensionBehaviourArr[i] != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            return false;
        }
        char c = 1;
        if (constraintWidget.G != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            if (i != 0) {
                c = 0;
            }
            if (dimensionBehaviourArr[c] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            }
            return false;
        }
        if (i == 0) {
            if (constraintWidget.e == 0 && constraintWidget.h == 0 && constraintWidget.i == 0) {
                return true;
            }
            return false;
        } else if (constraintWidget.f != 0 || constraintWidget.k != 0 || constraintWidget.l != 0) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static void a(int i, ConstraintWidget constraintWidget) {
        ConstraintWidget constraintWidget2 = constraintWidget;
        constraintWidget.J();
        e5 d = constraintWidget2.s.d();
        e5 d2 = constraintWidget2.t.d();
        e5 d3 = constraintWidget2.u.d();
        e5 d4 = constraintWidget2.v.d();
        boolean z = (i & 8) == 8;
        boolean z2 = constraintWidget2.C[0] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && a(constraintWidget2, 0);
        if (!(d.h == 4 || d3.h == 4)) {
            if (constraintWidget2.C[0] == ConstraintWidget.DimensionBehaviour.FIXED || (z2 && constraintWidget.s() == 8)) {
                if (constraintWidget2.s.d == null && constraintWidget2.u.d == null) {
                    d.b(1);
                    d3.b(1);
                    if (z) {
                        d3.a(d, 1, constraintWidget.n());
                    } else {
                        d3.a(d, constraintWidget.t());
                    }
                } else if (constraintWidget2.s.d != null && constraintWidget2.u.d == null) {
                    d.b(1);
                    d3.b(1);
                    if (z) {
                        d3.a(d, 1, constraintWidget.n());
                    } else {
                        d3.a(d, constraintWidget.t());
                    }
                } else if (constraintWidget2.s.d == null && constraintWidget2.u.d != null) {
                    d.b(1);
                    d3.b(1);
                    d.a(d3, -constraintWidget.t());
                    if (z) {
                        d.a(d3, -1, constraintWidget.n());
                    } else {
                        d.a(d3, -constraintWidget.t());
                    }
                } else if (!(constraintWidget2.s.d == null || constraintWidget2.u.d == null)) {
                    d.b(2);
                    d3.b(2);
                    if (z) {
                        constraintWidget.n().a(d);
                        constraintWidget.n().a(d3);
                        d.b(d3, -1, constraintWidget.n());
                        d3.b(d, 1, constraintWidget.n());
                    } else {
                        d.b(d3, (float) (-constraintWidget.t()));
                        d3.b(d, (float) constraintWidget.t());
                    }
                }
            } else if (z2) {
                int t = constraintWidget.t();
                d.b(1);
                d3.b(1);
                if (constraintWidget2.s.d == null && constraintWidget2.u.d == null) {
                    if (z) {
                        d3.a(d, 1, constraintWidget.n());
                    } else {
                        d3.a(d, t);
                    }
                } else if (constraintWidget2.s.d == null || constraintWidget2.u.d != null) {
                    if (constraintWidget2.s.d != null || constraintWidget2.u.d == null) {
                        if (!(constraintWidget2.s.d == null || constraintWidget2.u.d == null)) {
                            if (z) {
                                constraintWidget.n().a(d);
                                constraintWidget.n().a(d3);
                            }
                            if (constraintWidget2.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                d.b(3);
                                d3.b(3);
                                d.b(d3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                d3.b(d, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            } else {
                                d.b(2);
                                d3.b(2);
                                d.b(d3, (float) (-t));
                                d3.b(d, (float) t);
                                constraintWidget2.p(t);
                            }
                        }
                    } else if (z) {
                        d.a(d3, -1, constraintWidget.n());
                    } else {
                        d.a(d3, -t);
                    }
                } else if (z) {
                    d3.a(d, 1, constraintWidget.n());
                } else {
                    d3.a(d, t);
                }
            }
        }
        boolean z3 = constraintWidget2.C[1] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && a(constraintWidget2, 1);
        if (d2.h != 4 && d4.h != 4) {
            if (constraintWidget2.C[1] == ConstraintWidget.DimensionBehaviour.FIXED || (z3 && constraintWidget.s() == 8)) {
                if (constraintWidget2.t.d == null && constraintWidget2.v.d == null) {
                    d2.b(1);
                    d4.b(1);
                    if (z) {
                        d4.a(d2, 1, constraintWidget.m());
                    } else {
                        d4.a(d2, constraintWidget.j());
                    }
                    ConstraintAnchor constraintAnchor = constraintWidget2.w;
                    if (constraintAnchor.d != null) {
                        constraintAnchor.d().b(1);
                        d2.a(1, constraintWidget2.w.d(), -constraintWidget2.Q);
                    }
                } else if (constraintWidget2.t.d != null && constraintWidget2.v.d == null) {
                    d2.b(1);
                    d4.b(1);
                    if (z) {
                        d4.a(d2, 1, constraintWidget.m());
                    } else {
                        d4.a(d2, constraintWidget.j());
                    }
                    if (constraintWidget2.Q > 0) {
                        constraintWidget2.w.d().a(1, d2, constraintWidget2.Q);
                    }
                } else if (constraintWidget2.t.d == null && constraintWidget2.v.d != null) {
                    d2.b(1);
                    d4.b(1);
                    if (z) {
                        d2.a(d4, -1, constraintWidget.m());
                    } else {
                        d2.a(d4, -constraintWidget.j());
                    }
                    if (constraintWidget2.Q > 0) {
                        constraintWidget2.w.d().a(1, d2, constraintWidget2.Q);
                    }
                } else if (constraintWidget2.t.d != null && constraintWidget2.v.d != null) {
                    d2.b(2);
                    d4.b(2);
                    if (z) {
                        d2.b(d4, -1, constraintWidget.m());
                        d4.b(d2, 1, constraintWidget.m());
                        constraintWidget.m().a(d2);
                        constraintWidget.n().a(d4);
                    } else {
                        d2.b(d4, (float) (-constraintWidget.j()));
                        d4.b(d2, (float) constraintWidget.j());
                    }
                    if (constraintWidget2.Q > 0) {
                        constraintWidget2.w.d().a(1, d2, constraintWidget2.Q);
                    }
                }
            } else if (z3) {
                int j = constraintWidget.j();
                d2.b(1);
                d4.b(1);
                if (constraintWidget2.t.d == null && constraintWidget2.v.d == null) {
                    if (z) {
                        d4.a(d2, 1, constraintWidget.m());
                    } else {
                        d4.a(d2, j);
                    }
                } else if (constraintWidget2.t.d == null || constraintWidget2.v.d != null) {
                    if (constraintWidget2.t.d != null || constraintWidget2.v.d == null) {
                        if (constraintWidget2.t.d != null && constraintWidget2.v.d != null) {
                            if (z) {
                                constraintWidget.m().a(d2);
                                constraintWidget.n().a(d4);
                            }
                            if (constraintWidget2.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                d2.b(3);
                                d4.b(3);
                                d2.b(d4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                d4.b(d2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                return;
                            }
                            d2.b(2);
                            d4.b(2);
                            d2.b(d4, (float) (-j));
                            d4.b(d2, (float) j);
                            constraintWidget2.h(j);
                            if (constraintWidget2.Q > 0) {
                                constraintWidget2.w.d().a(1, d2, constraintWidget2.Q);
                            }
                        }
                    } else if (z) {
                        d2.a(d4, -1, constraintWidget.m());
                    } else {
                        d2.a(d4, -j);
                    }
                } else if (z) {
                    d4.a(d2, 1, constraintWidget.m());
                } else {
                    d4.a(d2, j);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        if (r7.e0 == 2) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0036, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0048, code lost:
        if (r7.f0 == 2) goto L_0x0034;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:124:0x01d6  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0109  */
    public static boolean a(y4 y4Var, q4 q4Var, int i, int i2, x4 x4Var) {
        boolean z;
        boolean z2;
        float f;
        int i3;
        int i4;
        float f2;
        ConstraintWidget constraintWidget;
        boolean z3;
        int i5;
        q4 q4Var2 = q4Var;
        int i6 = i;
        x4 x4Var2 = x4Var;
        ConstraintWidget constraintWidget2 = x4Var2.a;
        ConstraintWidget constraintWidget3 = x4Var2.c;
        ConstraintWidget constraintWidget4 = x4Var2.b;
        ConstraintWidget constraintWidget5 = x4Var2.d;
        ConstraintWidget constraintWidget6 = x4Var2.e;
        float f3 = x4Var2.k;
        ConstraintWidget constraintWidget7 = x4Var2.f;
        ConstraintWidget constraintWidget8 = x4Var2.g;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour = y4Var.C[i6];
        ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        if (i6 == 0) {
            z2 = constraintWidget6.e0 == 0;
            z = constraintWidget6.e0 == 1;
        } else {
            z2 = constraintWidget6.f0 == 0;
            z = constraintWidget6.f0 == 1;
        }
        boolean z4 = true;
        ConstraintWidget constraintWidget9 = constraintWidget2;
        int i7 = 0;
        boolean z5 = false;
        int i8 = 0;
        float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f5 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (!z5) {
            if (constraintWidget9.s() != 8) {
                i8++;
                if (i6 == 0) {
                    i5 = constraintWidget9.t();
                } else {
                    i5 = constraintWidget9.j();
                }
                f4 += (float) i5;
                if (constraintWidget9 != constraintWidget4) {
                    f4 += (float) constraintWidget9.A[i2].b();
                }
                if (constraintWidget9 != constraintWidget5) {
                    f4 += (float) constraintWidget9.A[i2 + 1].b();
                }
                f5 = f5 + ((float) constraintWidget9.A[i2].b()) + ((float) constraintWidget9.A[i2 + 1].b());
            }
            ConstraintAnchor constraintAnchor = constraintWidget9.A[i2];
            if (constraintWidget9.s() != 8 && constraintWidget9.C[i6] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                i7++;
                if (i6 != 0) {
                    z3 = false;
                    if (constraintWidget9.f != 0) {
                        return false;
                    }
                    if (constraintWidget9.k == 0) {
                        if (constraintWidget9.l != 0) {
                        }
                    }
                    return z3;
                } else if (constraintWidget9.e != 0) {
                    return false;
                } else {
                    z3 = false;
                    if (!(constraintWidget9.h == 0 && constraintWidget9.i == 0)) {
                        return false;
                    }
                }
                if (constraintWidget9.G != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    return z3;
                }
            }
            ConstraintAnchor constraintAnchor2 = constraintWidget9.A[i2 + 1].d;
            if (constraintAnchor2 != null) {
                ConstraintWidget constraintWidget10 = constraintAnchor2.b;
                ConstraintAnchor[] constraintAnchorArr = constraintWidget10.A;
                ConstraintWidget constraintWidget11 = constraintWidget10;
                if (constraintAnchorArr[i2].d != null && constraintAnchorArr[i2].d.b == constraintWidget9) {
                    constraintWidget = constraintWidget11;
                    if (constraintWidget == null) {
                        constraintWidget9 = constraintWidget;
                    } else {
                        z5 = true;
                    }
                }
            }
            constraintWidget = null;
            if (constraintWidget == null) {
            }
        }
        e5 d = constraintWidget2.A[i2].d();
        int i9 = i2 + 1;
        e5 d2 = constraintWidget3.A[i9].d();
        e5 e5Var = d.d;
        if (e5Var == null) {
            return false;
        }
        ConstraintWidget constraintWidget12 = constraintWidget2;
        e5 e5Var2 = d2.d;
        if (e5Var2 == null || e5Var.b != 1 || e5Var2.b != 1) {
            return false;
        }
        if (i7 > 0 && i7 != i8) {
            return false;
        }
        if (z4 || z2 || z) {
            f = constraintWidget4 != null ? (float) constraintWidget4.A[i2].b() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (constraintWidget5 != null) {
                f += (float) constraintWidget5.A[i9].b();
            }
        } else {
            f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        float f6 = d.d.g;
        float f7 = d2.d.g;
        float f8 = (f6 < f7 ? f7 - f6 : f6 - f7) - f4;
        if (i7 <= 0 || i7 != i8) {
            q4 q4Var3 = q4Var;
            if (f8 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z4 = true;
                z2 = false;
                z = false;
            }
            if (z4) {
                ConstraintWidget constraintWidget13 = constraintWidget12;
                float b = f6 + ((f8 - f) * constraintWidget13.b(i6));
                while (constraintWidget13 != null) {
                    r4 r4Var = q4.q;
                    if (r4Var != null) {
                        r4Var.z--;
                        r4Var.r++;
                        r4Var.x++;
                    }
                    ConstraintWidget constraintWidget14 = constraintWidget13.i0[i6];
                    if (constraintWidget14 != null || constraintWidget13 == constraintWidget3) {
                        if (i6 == 0) {
                            i4 = constraintWidget13.t();
                        } else {
                            i4 = constraintWidget13.j();
                        }
                        float b2 = b + ((float) constraintWidget13.A[i2].b());
                        constraintWidget13.A[i2].d().a(d.f, b2);
                        float f9 = b2 + ((float) i4);
                        constraintWidget13.A[i9].d().a(d.f, f9);
                        constraintWidget13.A[i2].d().a(q4Var3);
                        constraintWidget13.A[i9].d().a(q4Var3);
                        b = f9 + ((float) constraintWidget13.A[i9].b());
                    }
                    constraintWidget13 = constraintWidget14;
                }
                return true;
            }
            ConstraintWidget constraintWidget15 = constraintWidget12;
            if (!z2 && !z) {
                return true;
            }
            if (z2 || z) {
                f8 -= f;
            }
            float f10 = f8 / ((float) (i8 + 1));
            if (z) {
                f10 = f8 / (i8 > 1 ? (float) (i8 - 1) : 2.0f);
            }
            float f11 = constraintWidget15.s() != 8 ? f6 + f10 : f6;
            if (z && i8 > 1) {
                f11 = ((float) constraintWidget4.A[i2].b()) + f6;
            }
            if (z2 && constraintWidget4 != null) {
                f11 += (float) constraintWidget4.A[i2].b();
            }
            while (constraintWidget15 != null) {
                r4 r4Var2 = q4.q;
                if (r4Var2 != null) {
                    r4Var2.z--;
                    r4Var2.r++;
                    r4Var2.x++;
                }
                ConstraintWidget constraintWidget16 = constraintWidget15.i0[i6];
                if (constraintWidget16 != null || constraintWidget15 == constraintWidget3) {
                    if (i6 == 0) {
                        i3 = constraintWidget15.t();
                    } else {
                        i3 = constraintWidget15.j();
                    }
                    float f12 = (float) i3;
                    if (constraintWidget15 != constraintWidget4) {
                        f11 += (float) constraintWidget15.A[i2].b();
                    }
                    constraintWidget15.A[i2].d().a(d.f, f11);
                    constraintWidget15.A[i9].d().a(d.f, f11 + f12);
                    constraintWidget15.A[i2].d().a(q4Var3);
                    constraintWidget15.A[i9].d().a(q4Var3);
                    f11 += f12 + ((float) constraintWidget15.A[i9].b());
                    if (constraintWidget16 != null) {
                        if (constraintWidget16.s() != 8) {
                            f11 += f10;
                        }
                        constraintWidget15 = constraintWidget16;
                    }
                }
                constraintWidget15 = constraintWidget16;
            }
            return true;
        } else if (constraintWidget9.l() != null && constraintWidget9.l().C[i6] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
            return false;
        } else {
            float f13 = (f8 + f4) - f5;
            float f14 = f6;
            ConstraintWidget constraintWidget17 = constraintWidget12;
            while (constraintWidget17 != null) {
                r4 r4Var3 = q4.q;
                if (r4Var3 != null) {
                    r4Var3.z--;
                    r4Var3.r++;
                    r4Var3.x++;
                }
                ConstraintWidget constraintWidget18 = constraintWidget17.i0[i6];
                if (constraintWidget18 != null || constraintWidget17 == constraintWidget3) {
                    float f15 = f13 / ((float) i7);
                    if (f3 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        float[] fArr = constraintWidget17.g0;
                        if (fArr[i6] == -1.0f) {
                            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                            if (constraintWidget17.s() == 8) {
                                f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                            }
                            float b3 = f14 + ((float) constraintWidget17.A[i2].b());
                            constraintWidget17.A[i2].d().a(d.f, b3);
                            float f16 = b3 + f2;
                            constraintWidget17.A[i9].d().a(d.f, f16);
                            q4 q4Var4 = q4Var;
                            constraintWidget17.A[i2].d().a(q4Var4);
                            constraintWidget17.A[i9].d().a(q4Var4);
                            f14 = f16 + ((float) constraintWidget17.A[i9].b());
                        } else {
                            f15 = (fArr[i6] * f13) / f3;
                        }
                    }
                    f2 = f15;
                    if (constraintWidget17.s() == 8) {
                    }
                    float b32 = f14 + ((float) constraintWidget17.A[i2].b());
                    constraintWidget17.A[i2].d().a(d.f, b32);
                    float f162 = b32 + f2;
                    constraintWidget17.A[i9].d().a(d.f, f162);
                    q4 q4Var42 = q4Var;
                    constraintWidget17.A[i2].d().a(q4Var42);
                    constraintWidget17.A[i9].d().a(q4Var42);
                    f14 = f162 + ((float) constraintWidget17.A[i9].b());
                } else {
                    q4 q4Var5 = q4Var;
                }
                constraintWidget17 = constraintWidget18;
            }
            return true;
        }
    }

    @DexIgnore
    public static void a(ConstraintWidget constraintWidget, int i, int i2) {
        int i3 = i * 2;
        int i4 = i3 + 1;
        constraintWidget.A[i3].d().f = constraintWidget.l().s.d();
        constraintWidget.A[i3].d().g = (float) i2;
        constraintWidget.A[i3].d().b = 1;
        constraintWidget.A[i4].d().f = constraintWidget.A[i3].d();
        constraintWidget.A[i4].d().g = (float) constraintWidget.d(i);
        constraintWidget.A[i4].d().b = 1;
    }
}
