package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i12 extends l12 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;

    @DexIgnore
    public i12() {
        a();
    }

    @DexIgnore
    public i12 a() {
        this.a = "";
        this.b = "";
        return this;
    }

    @DexIgnore
    public i12 a(j12 j12) throws IOException {
        while (true) {
            int j = j12.j();
            if (j == 0) {
                return this;
            }
            if (j == 18) {
                this.a = j12.i();
            } else if (j == 26) {
                this.b = j12.i();
            } else if (j == 50) {
                j12.i();
            } else if (!n12.b(j12, j)) {
                return this;
            }
        }
    }
}
