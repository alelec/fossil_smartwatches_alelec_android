package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ju */
public class C2160ju implements com.fossil.blesdk.obfuscated.C2770qu {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.Set<com.fossil.blesdk.obfuscated.C2835ru> f6585a; // = java.util.Collections.newSetFromMap(new java.util.WeakHashMap());

    @DexIgnore
    /* renamed from: b */
    public boolean f6586b;

    @DexIgnore
    /* renamed from: c */
    public boolean f6587c;

    @DexIgnore
    /* renamed from: a */
    public void mo12479a(com.fossil.blesdk.obfuscated.C2835ru ruVar) {
        this.f6585a.add(ruVar);
        if (this.f6587c) {
            ruVar.mo14096b();
        } else if (this.f6586b) {
            ruVar.mo14094a();
        } else {
            ruVar.mo14097c();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12481b(com.fossil.blesdk.obfuscated.C2835ru ruVar) {
        this.f6585a.remove(ruVar);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo12482c() {
        this.f6586b = false;
        for (T c : com.fossil.blesdk.obfuscated.C3066uw.m14927a(this.f6585a)) {
            c.mo14097c();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12480b() {
        this.f6586b = true;
        for (T a : com.fossil.blesdk.obfuscated.C3066uw.m14927a(this.f6585a)) {
            a.mo14094a();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12478a() {
        this.f6587c = true;
        for (T b : com.fossil.blesdk.obfuscated.C3066uw.m14927a(this.f6585a)) {
            b.mo14096b();
        }
    }
}
