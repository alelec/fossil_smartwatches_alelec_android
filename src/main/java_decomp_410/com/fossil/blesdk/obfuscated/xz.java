package com.fossil.blesdk.obfuscated;

import com.crashlytics.android.core.CrashlyticsController;
import com.crashlytics.android.core.Report;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.places.internal.LocationScannerImpl;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xz {
    @DexIgnore
    public static /* final */ Map<String, String> g; // = Collections.singletonMap("X-CRASHLYTICS-INVALID-SESSION", AppEventsConstants.EVENT_PARAM_VALUE_YES);
    @DexIgnore
    public static /* final */ short[] h; // = {10, 20, 30, 60, 120, 300};
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ cz b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ c d;
    @DexIgnore
    public /* final */ b e;
    @DexIgnore
    public Thread f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements d {
        @DexIgnore
        public boolean a() {
            return true;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        boolean a();
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        File[] a();

        @DexIgnore
        File[] b();

        @DexIgnore
        File[] c();
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        boolean a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends l54 {
        @DexIgnore
        public /* final */ float e;
        @DexIgnore
        public /* final */ d f;

        @DexIgnore
        public e(float f2, d dVar) {
            this.e = f2;
            this.f = dVar;
        }

        @DexIgnore
        public void a() {
            try {
                b();
            } catch (Exception e2) {
                q44.g().e("CrashlyticsCore", "An unexpected error occurred while attempting to upload crash reports.", e2);
            }
            Thread unused = xz.this.f = null;
        }

        @DexIgnore
        public final void b() {
            y44 g2 = q44.g();
            g2.d("CrashlyticsCore", "Starting report processing in " + this.e + " second(s)...");
            float f2 = this.e;
            if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                try {
                    Thread.sleep((long) (f2 * 1000.0f));
                } catch (InterruptedException unused) {
                    Thread.currentThread().interrupt();
                    return;
                }
            }
            List<Report> a = xz.this.a();
            if (!xz.this.e.a()) {
                if (a.isEmpty() || this.f.a()) {
                    int i = 0;
                    while (!a.isEmpty() && !xz.this.e.a()) {
                        y44 g3 = q44.g();
                        g3.d("CrashlyticsCore", "Attempting to send " + a.size() + " report(s)");
                        for (Report a2 : a) {
                            xz.this.a(a2);
                        }
                        a = xz.this.a();
                        if (!a.isEmpty()) {
                            int i2 = i + 1;
                            long j = (long) xz.h[Math.min(i, xz.h.length - 1)];
                            y44 g4 = q44.g();
                            g4.d("CrashlyticsCore", "Report submisson: scheduling delayed retry in " + j + " seconds");
                            try {
                                Thread.sleep(j * 1000);
                                i = i2;
                            } catch (InterruptedException unused2) {
                                Thread.currentThread().interrupt();
                                return;
                            }
                        }
                    }
                    return;
                }
                y44 g5 = q44.g();
                g5.d("CrashlyticsCore", "User declined to send. Removing " + a.size() + " Report(s).");
                for (Report remove : a) {
                    remove.remove();
                }
            }
        }
    }

    @DexIgnore
    public xz(String str, cz czVar, c cVar, b bVar) {
        if (czVar != null) {
            this.b = czVar;
            this.c = str;
            this.d = cVar;
            this.e = bVar;
            return;
        }
        throw new IllegalArgumentException("createReportCall must not be null.");
    }

    @DexIgnore
    public synchronized void a(float f2, d dVar) {
        if (this.f != null) {
            q44.g().d("CrashlyticsCore", "Report upload has already been started.");
            return;
        }
        this.f = new Thread(new e(f2, dVar), "Crashlytics Report Uploader");
        this.f.start();
    }

    @DexIgnore
    public boolean a(Report report) {
        boolean z;
        synchronized (this.a) {
            z = false;
            try {
                boolean a2 = this.b.a(new bz(this.c, report));
                y44 g2 = q44.g();
                StringBuilder sb = new StringBuilder();
                sb.append("Crashlytics report upload ");
                sb.append(a2 ? "complete: " : "FAILED: ");
                sb.append(report.b());
                g2.i("CrashlyticsCore", sb.toString());
                if (a2) {
                    report.remove();
                    z = true;
                }
            } catch (Exception e2) {
                y44 g3 = q44.g();
                g3.e("CrashlyticsCore", "Error occurred sending report " + report, e2);
            }
        }
        return z;
    }

    @DexIgnore
    public List<Report> a() {
        File[] c2;
        File[] b2;
        File[] a2;
        q44.g().d("CrashlyticsCore", "Checking for crash reports...");
        synchronized (this.a) {
            c2 = this.d.c();
            b2 = this.d.b();
            a2 = this.d.a();
        }
        LinkedList linkedList = new LinkedList();
        if (c2 != null) {
            for (File file : c2) {
                q44.g().d("CrashlyticsCore", "Found crash report " + file.getPath());
                linkedList.add(new a00(file));
            }
        }
        HashMap hashMap = new HashMap();
        if (b2 != null) {
            for (File file2 : b2) {
                String c3 = CrashlyticsController.c(file2);
                if (!hashMap.containsKey(c3)) {
                    hashMap.put(c3, new LinkedList());
                }
                ((List) hashMap.get(c3)).add(file2);
            }
        }
        for (String str : hashMap.keySet()) {
            q44.g().d("CrashlyticsCore", "Found invalid session: " + str);
            List list = (List) hashMap.get(str);
            linkedList.add(new iz(str, (File[]) list.toArray(new File[list.size()])));
        }
        if (a2 != null) {
            for (File qzVar : a2) {
                linkedList.add(new qz(qzVar));
            }
        }
        if (linkedList.isEmpty()) {
            q44.g().d("CrashlyticsCore", "No reports found.");
        }
        return linkedList;
    }
}
