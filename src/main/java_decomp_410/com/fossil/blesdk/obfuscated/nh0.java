package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nh0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ me0 e;
    @DexIgnore
    public /* final */ /* synthetic */ mh0 f;

    @DexIgnore
    public nh0(mh0 mh0, me0 me0) {
        this.f = mh0;
        this.e = me0;
    }

    @DexIgnore
    public final void run() {
        try {
            BasePendingResult.p.set(true);
            this.f.g.sendMessage(this.f.g.obtainMessage(0, this.f.a.a(this.e)));
            BasePendingResult.p.set(false);
            mh0.a(this.e);
            ge0 ge0 = (ge0) this.f.f.get();
            if (ge0 != null) {
                ge0.a(this.f);
            }
        } catch (RuntimeException e2) {
            this.f.g.sendMessage(this.f.g.obtainMessage(1, e2));
            BasePendingResult.p.set(false);
            mh0.a(this.e);
            ge0 ge02 = (ge0) this.f.f.get();
            if (ge02 != null) {
                ge02.a(this.f);
            }
        } catch (Throwable th) {
            BasePendingResult.p.set(false);
            mh0.a(this.e);
            ge0 ge03 = (ge0) this.f.f.get();
            if (ge03 != null) {
                ge03.a(this.f);
            }
            throw th;
        }
    }
}
