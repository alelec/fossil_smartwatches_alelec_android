package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uu2 implements MembersInjector<su2> {
    @DexIgnore
    public static void a(su2 su2, DashboardActivityPresenter dashboardActivityPresenter) {
        su2.k = dashboardActivityPresenter;
    }

    @DexIgnore
    public static void a(su2 su2, DashboardActiveTimePresenter dashboardActiveTimePresenter) {
        su2.l = dashboardActiveTimePresenter;
    }

    @DexIgnore
    public static void a(su2 su2, DashboardCaloriesPresenter dashboardCaloriesPresenter) {
        su2.m = dashboardCaloriesPresenter;
    }

    @DexIgnore
    public static void a(su2 su2, DashboardHeartRatePresenter dashboardHeartRatePresenter) {
        su2.n = dashboardHeartRatePresenter;
    }

    @DexIgnore
    public static void a(su2 su2, DashboardSleepPresenter dashboardSleepPresenter) {
        su2.o = dashboardSleepPresenter;
    }

    @DexIgnore
    public static void a(su2 su2, DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter) {
        su2.p = dashboardGoalTrackingPresenter;
    }
}
