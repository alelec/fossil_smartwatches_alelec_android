package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hb0 {
    @DexIgnore
    public static /* final */ hb0 a; // = new hb0();

    @DexIgnore
    public final Handler a() {
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        if (myLooper != null) {
            return new Handler(myLooper);
        }
        kd4.a();
        throw null;
    }
}
