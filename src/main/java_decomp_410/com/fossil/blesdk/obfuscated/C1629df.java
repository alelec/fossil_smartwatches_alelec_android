package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.df */
public class C1629df {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1855g4<androidx.recyclerview.widget.RecyclerView.ViewHolder, com.fossil.blesdk.obfuscated.C1629df.C1630a> f4365a; // = new com.fossil.blesdk.obfuscated.C1855g4<>();

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2103j4<androidx.recyclerview.widget.RecyclerView.ViewHolder> f4366b; // = new com.fossil.blesdk.obfuscated.C2103j4<>();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.df$a")
    /* renamed from: com.fossil.blesdk.obfuscated.df$a */
    public static class C1630a {

        @DexIgnore
        /* renamed from: d */
        public static com.fossil.blesdk.obfuscated.C1862g8<com.fossil.blesdk.obfuscated.C1629df.C1630a> f4367d; // = new com.fossil.blesdk.obfuscated.C1928h8(20);

        @DexIgnore
        /* renamed from: a */
        public int f4368a;

        @DexIgnore
        /* renamed from: b */
        public androidx.recyclerview.widget.RecyclerView.C0230j.C0233c f4369b;

        @DexIgnore
        /* renamed from: c */
        public androidx.recyclerview.widget.RecyclerView.C0230j.C0233c f4370c;

        @DexIgnore
        /* renamed from: a */
        public static void m5836a(com.fossil.blesdk.obfuscated.C1629df.C1630a aVar) {
            aVar.f4368a = 0;
            aVar.f4369b = null;
            aVar.f4370c = null;
            f4367d.mo11163a(aVar);
        }

        @DexIgnore
        /* renamed from: b */
        public static com.fossil.blesdk.obfuscated.C1629df.C1630a m5837b() {
            com.fossil.blesdk.obfuscated.C1629df.C1630a a = f4367d.mo11162a();
            return a == null ? new com.fossil.blesdk.obfuscated.C1629df.C1630a() : a;
        }

        @DexIgnore
        /* renamed from: a */
        public static void m5835a() {
            do {
            } while (f4367d.mo11162a() != null);
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.df$b */
    public interface C1631b {
        @DexIgnore
        /* renamed from: a */
        void mo2787a(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder);

        @DexIgnore
        /* renamed from: a */
        void mo2788a(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar2);

        @DexIgnore
        /* renamed from: b */
        void mo2789b(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar2);

        @DexIgnore
        /* renamed from: c */
        void mo2790c(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar2);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9924a() {
        this.f4365a.clear();
        this.f4366b.mo12213a();
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo9931b(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        com.fossil.blesdk.obfuscated.C1629df.C1630a aVar = this.f4365a.get(viewHolder);
        if (aVar == null || (aVar.f4368a & 1) == 0) {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo9932c(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar) {
        com.fossil.blesdk.obfuscated.C1629df.C1630a aVar = this.f4365a.get(viewHolder);
        if (aVar == null) {
            aVar = com.fossil.blesdk.obfuscated.C1629df.C1630a.m5837b();
            this.f4365a.put(viewHolder, aVar);
        }
        aVar.f4369b = cVar;
        aVar.f4368a |= 4;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo9934d(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        mo9937g(viewHolder);
    }

    @DexIgnore
    /* renamed from: e */
    public androidx.recyclerview.widget.RecyclerView.C0230j.C0233c mo9935e(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        return mo9923a(viewHolder, 8);
    }

    @DexIgnore
    /* renamed from: f */
    public androidx.recyclerview.widget.RecyclerView.C0230j.C0233c mo9936f(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        return mo9923a(viewHolder, 4);
    }

    @DexIgnore
    /* renamed from: g */
    public void mo9937g(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        com.fossil.blesdk.obfuscated.C1629df.C1630a aVar = this.f4365a.get(viewHolder);
        if (aVar != null) {
            aVar.f4368a &= -2;
        }
    }

    @DexIgnore
    /* renamed from: h */
    public void mo9938h(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        int c = this.f4366b.mo12220c() - 1;
        while (true) {
            if (c < 0) {
                break;
            } else if (viewHolder == this.f4366b.mo12222c(c)) {
                this.f4366b.mo12219b(c);
                break;
            } else {
                c--;
            }
        }
        com.fossil.blesdk.obfuscated.C1629df.C1630a remove = this.f4365a.remove(viewHolder);
        if (remove != null) {
            com.fossil.blesdk.obfuscated.C1629df.C1630a.m5836a(remove);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final androidx.recyclerview.widget.RecyclerView.C0230j.C0233c mo9923a(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int i) {
        androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar;
        int b = this.f4365a.mo1221b((java.lang.Object) viewHolder);
        if (b < 0) {
            return null;
        }
        com.fossil.blesdk.obfuscated.C1629df.C1630a e = this.f4365a.mo1229e(b);
        if (e != null) {
            int i2 = e.f4368a;
            if ((i2 & i) != 0) {
                e.f4368a = (~i) & i2;
                if (i == 4) {
                    cVar = e.f4369b;
                } else if (i == 8) {
                    cVar = e.f4370c;
                } else {
                    throw new java.lang.IllegalArgumentException("Must provide flag PRE or POST");
                }
                if ((e.f4368a & 12) == 0) {
                    this.f4365a.mo1228d(b);
                    com.fossil.blesdk.obfuscated.C1629df.C1630a.m5836a(e);
                }
                return cVar;
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9930b(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar) {
        com.fossil.blesdk.obfuscated.C1629df.C1630a aVar = this.f4365a.get(viewHolder);
        if (aVar == null) {
            aVar = com.fossil.blesdk.obfuscated.C1629df.C1630a.m5837b();
            this.f4365a.put(viewHolder, aVar);
        }
        aVar.f4370c = cVar;
        aVar.f4368a |= 8;
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo9933c(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        com.fossil.blesdk.obfuscated.C1629df.C1630a aVar = this.f4365a.get(viewHolder);
        return (aVar == null || (aVar.f4368a & 4) == 0) ? false : true;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9929b() {
        com.fossil.blesdk.obfuscated.C1629df.C1630a.m5835a();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9925a(long j, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        this.f4366b.mo12223c(j, viewHolder);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9927a(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar) {
        com.fossil.blesdk.obfuscated.C1629df.C1630a aVar = this.f4365a.get(viewHolder);
        if (aVar == null) {
            aVar = com.fossil.blesdk.obfuscated.C1629df.C1630a.m5837b();
            this.f4365a.put(viewHolder, aVar);
        }
        aVar.f4368a |= 2;
        aVar.f4369b = cVar;
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.recyclerview.widget.RecyclerView.ViewHolder mo9922a(long j) {
        return this.f4366b.mo12216b(j);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9926a(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        com.fossil.blesdk.obfuscated.C1629df.C1630a aVar = this.f4365a.get(viewHolder);
        if (aVar == null) {
            aVar = com.fossil.blesdk.obfuscated.C1629df.C1630a.m5837b();
            this.f4365a.put(viewHolder, aVar);
        }
        aVar.f4368a |= 1;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9928a(com.fossil.blesdk.obfuscated.C1629df.C1631b bVar) {
        for (int size = this.f4365a.size() - 1; size >= 0; size--) {
            androidx.recyclerview.widget.RecyclerView.ViewHolder c = this.f4365a.mo1224c(size);
            com.fossil.blesdk.obfuscated.C1629df.C1630a d = this.f4365a.mo1228d(size);
            int i = d.f4368a;
            if ((i & 3) == 3) {
                bVar.mo2787a(c);
            } else if ((i & 1) != 0) {
                androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar = d.f4369b;
                if (cVar == null) {
                    bVar.mo2787a(c);
                } else {
                    bVar.mo2789b(c, cVar, d.f4370c);
                }
            } else if ((i & 14) == 14) {
                bVar.mo2788a(c, d.f4369b, d.f4370c);
            } else if ((i & 12) == 12) {
                bVar.mo2790c(c, d.f4369b, d.f4370c);
            } else if ((i & 4) != 0) {
                bVar.mo2789b(c, d.f4369b, (androidx.recyclerview.widget.RecyclerView.C0230j.C0233c) null);
            } else if ((i & 8) != 0) {
                bVar.mo2788a(c, d.f4369b, d.f4370c);
            }
            com.fossil.blesdk.obfuscated.C1629df.C1630a.m5836a(d);
        }
    }
}
