package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.h0 */
public class C1914h0 {

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Field f5597a;

    @DexIgnore
    /* renamed from: b */
    public static boolean f5598b;

    @DexIgnore
    /* renamed from: c */
    public static java.lang.Class f5599c;

    @DexIgnore
    /* renamed from: d */
    public static boolean f5600d;

    @DexIgnore
    /* renamed from: e */
    public static java.lang.reflect.Field f5601e;

    @DexIgnore
    /* renamed from: f */
    public static boolean f5602f;

    @DexIgnore
    /* renamed from: g */
    public static java.lang.reflect.Field f5603g;

    @DexIgnore
    /* renamed from: h */
    public static boolean f5604h;

    @DexIgnore
    /* renamed from: a */
    public static void m7650a(android.content.res.Resources resources) {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i < 28) {
            if (i >= 24) {
                m7654d(resources);
            } else if (i >= 23) {
                m7653c(resources);
            } else if (i >= 21) {
                m7652b(resources);
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m7652b(android.content.res.Resources resources) {
        java.util.Map map;
        if (!f5598b) {
            try {
                f5597a = android.content.res.Resources.class.getDeclaredField("mDrawableCache");
                f5597a.setAccessible(true);
            } catch (java.lang.NoSuchFieldException e) {
                android.util.Log.e("ResourcesFlusher", "Could not retrieve Resources#mDrawableCache field", e);
            }
            f5598b = true;
        }
        java.lang.reflect.Field field = f5597a;
        if (field != null) {
            try {
                map = (java.util.Map) field.get(resources);
            } catch (java.lang.IllegalAccessException e2) {
                android.util.Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mDrawableCache", e2);
                map = null;
            }
            if (map != null) {
                map.clear();
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static void m7653c(android.content.res.Resources resources) {
        if (!f5598b) {
            try {
                f5597a = android.content.res.Resources.class.getDeclaredField("mDrawableCache");
                f5597a.setAccessible(true);
            } catch (java.lang.NoSuchFieldException e) {
                android.util.Log.e("ResourcesFlusher", "Could not retrieve Resources#mDrawableCache field", e);
            }
            f5598b = true;
        }
        java.lang.Object obj = null;
        java.lang.reflect.Field field = f5597a;
        if (field != null) {
            try {
                obj = field.get(resources);
            } catch (java.lang.IllegalAccessException e2) {
                android.util.Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mDrawableCache", e2);
            }
        }
        if (obj != null) {
            m7651a(obj);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public static void m7654d(android.content.res.Resources resources) {
        java.lang.Object obj;
        if (!f5604h) {
            try {
                f5603g = android.content.res.Resources.class.getDeclaredField("mResourcesImpl");
                f5603g.setAccessible(true);
            } catch (java.lang.NoSuchFieldException e) {
                android.util.Log.e("ResourcesFlusher", "Could not retrieve Resources#mResourcesImpl field", e);
            }
            f5604h = true;
        }
        java.lang.reflect.Field field = f5603g;
        if (field != null) {
            java.lang.Object obj2 = null;
            try {
                obj = field.get(resources);
            } catch (java.lang.IllegalAccessException e2) {
                android.util.Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mResourcesImpl", e2);
                obj = null;
            }
            if (obj != null) {
                if (!f5598b) {
                    try {
                        f5597a = obj.getClass().getDeclaredField("mDrawableCache");
                        f5597a.setAccessible(true);
                    } catch (java.lang.NoSuchFieldException e3) {
                        android.util.Log.e("ResourcesFlusher", "Could not retrieve ResourcesImpl#mDrawableCache field", e3);
                    }
                    f5598b = true;
                }
                java.lang.reflect.Field field2 = f5597a;
                if (field2 != null) {
                    try {
                        obj2 = field2.get(obj);
                    } catch (java.lang.IllegalAccessException e4) {
                        android.util.Log.e("ResourcesFlusher", "Could not retrieve value from ResourcesImpl#mDrawableCache", e4);
                    }
                }
                if (obj2 != null) {
                    m7651a(obj2);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m7651a(java.lang.Object obj) {
        android.util.LongSparseArray longSparseArray;
        if (!f5600d) {
            try {
                f5599c = java.lang.Class.forName("android.content.res.ThemedResourceCache");
            } catch (java.lang.ClassNotFoundException e) {
                android.util.Log.e("ResourcesFlusher", "Could not find ThemedResourceCache class", e);
            }
            f5600d = true;
        }
        java.lang.Class cls = f5599c;
        if (cls != null) {
            if (!f5602f) {
                try {
                    f5601e = cls.getDeclaredField("mUnthemedEntries");
                    f5601e.setAccessible(true);
                } catch (java.lang.NoSuchFieldException e2) {
                    android.util.Log.e("ResourcesFlusher", "Could not retrieve ThemedResourceCache#mUnthemedEntries field", e2);
                }
                f5602f = true;
            }
            java.lang.reflect.Field field = f5601e;
            if (field != null) {
                try {
                    longSparseArray = (android.util.LongSparseArray) field.get(obj);
                } catch (java.lang.IllegalAccessException e3) {
                    android.util.Log.e("ResourcesFlusher", "Could not retrieve value from ThemedResourceCache#mUnthemedEntries", e3);
                    longSparseArray = null;
                }
                if (longSparseArray != null) {
                    longSparseArray.clear();
                }
            }
        }
    }
}
