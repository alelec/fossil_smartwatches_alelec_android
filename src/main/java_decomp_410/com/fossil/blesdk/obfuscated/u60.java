package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u60 extends g70 {
    @DexIgnore
    public String A; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u60(Peripheral peripheral) {
        super(RequestId.READ_FIRMWARE_VERSION, peripheral);
        kd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new i10(GattCharacteristic.CharacteristicId.FIRMWARE_VERSION, i().h());
    }

    @DexIgnore
    public final String B() {
        return this.A;
    }

    @DexIgnore
    public void a(BluetoothCommand bluetoothCommand) {
        kd4.b(bluetoothCommand, Constants.COMMAND);
        this.A = new String(((i10) bluetoothCommand).j(), bf4.a);
        a(new Request.ResponseInfo(0, (GattCharacteristic.CharacteristicId) null, (byte[]) null, wa0.a(new JSONObject(), JSONKey.FIRMWARE_VERSION, this.A), 7, (fd4) null));
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.FIRMWARE_VERSION, this.A);
    }
}
