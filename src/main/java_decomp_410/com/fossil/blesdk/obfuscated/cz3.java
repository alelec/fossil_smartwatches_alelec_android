package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.os.Looper;
import android.os.Process;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cz3 {
    @DexIgnore
    public static int a; // = 6;
    @DexIgnore
    public static a b; // = new dz3();

    @DexIgnore
    public interface a {
        @DexIgnore
        int a();

        @DexIgnore
        void a(String str, String str2);

        @DexIgnore
        void b(String str, String str2);

        @DexIgnore
        void c(String str, String str2);

        @DexIgnore
        void i(String str, String str2);
    }

    /*
    static {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("VERSION.RELEASE:[" + Build.VERSION.RELEASE);
            sb.append("] VERSION.CODENAME:[" + Build.VERSION.CODENAME);
            sb.append("] VERSION.INCREMENTAL:[" + Build.VERSION.INCREMENTAL);
            sb.append("] BOARD:[" + Build.BOARD);
            sb.append("] DEVICE:[" + Build.DEVICE);
            sb.append("] DISPLAY:[" + Build.DISPLAY);
            sb.append("] FINGERPRINT:[" + Build.FINGERPRINT);
            sb.append("] HOST:[" + Build.HOST);
            sb.append("] MANUFACTURER:[" + Build.MANUFACTURER);
            sb.append("] MODEL:[" + Build.MODEL);
            sb.append("] PRODUCT:[" + Build.PRODUCT);
            sb.append("] TAGS:[" + Build.TAGS);
            sb.append("] TYPE:[" + Build.TYPE);
            sb.append("] USER:[" + Build.USER + "]");
        } catch (Throwable th) {
            th.printStackTrace();
        }
        sb.toString();
    }
    */

    @DexIgnore
    public static void a(String str, String str2) {
        a(str, str2, (Object[]) null);
    }

    @DexIgnore
    public static void a(String str, String str2, Object... objArr) {
        a aVar = b;
        if (aVar != null && aVar.a() <= 4) {
            if (objArr != null) {
                str2 = String.format(str2, objArr);
            }
            if (str2 == null) {
                str2 = "";
            }
            a aVar2 = b;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            aVar2.i(str, str2);
        }
    }

    @DexIgnore
    public static void b(String str, String str2) {
        a aVar = b;
        if (aVar != null && aVar.a() <= 3) {
            a aVar2 = b;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            aVar2.c(str, str2);
        }
    }

    @DexIgnore
    public static void c(String str, String str2) {
        a aVar = b;
        if (aVar != null && aVar.a() <= 2) {
            a aVar2 = b;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            aVar2.b(str, str2);
        }
    }

    @DexIgnore
    public static void d(String str, String str2) {
        a aVar = b;
        if (aVar != null && aVar.a() <= 1) {
            if (str2 == null) {
                str2 = "";
            }
            a aVar2 = b;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            aVar2.a(str, str2);
        }
    }
}
