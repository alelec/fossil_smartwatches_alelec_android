package com.fossil.blesdk.obfuscated;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h4<E> implements Collection<E>, Set<E> {
    @DexIgnore
    public static /* final */ int[] i; // = new int[0];
    @DexIgnore
    public static /* final */ Object[] j; // = new Object[0];
    @DexIgnore
    public static Object[] k;
    @DexIgnore
    public static int l;
    @DexIgnore
    public static Object[] m;
    @DexIgnore
    public static int n;
    @DexIgnore
    public int[] e;
    @DexIgnore
    public Object[] f;
    @DexIgnore
    public int g;
    @DexIgnore
    public l4<E, E> h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends l4<E, E> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public Object a(int i, int i2) {
            return h4.this.f[i];
        }

        @DexIgnore
        public int b(Object obj) {
            return h4.this.indexOf(obj);
        }

        @DexIgnore
        public int c() {
            return h4.this.g;
        }

        @DexIgnore
        public int a(Object obj) {
            return h4.this.indexOf(obj);
        }

        @DexIgnore
        public Map<E, E> b() {
            throw new UnsupportedOperationException("not a map");
        }

        @DexIgnore
        public void a(E e, E e2) {
            h4.this.add(e);
        }

        @DexIgnore
        public E a(int i, E e) {
            throw new UnsupportedOperationException("not a map");
        }

        @DexIgnore
        public void a(int i) {
            h4.this.g(i);
        }

        @DexIgnore
        public void a() {
            h4.this.clear();
        }
    }

    @DexIgnore
    public h4() {
        this(0);
    }

    @DexIgnore
    public final int a(Object obj, int i2) {
        int i3 = this.g;
        if (i3 == 0) {
            return -1;
        }
        int a2 = i4.a(this.e, i3, i2);
        if (a2 < 0 || obj.equals(this.f[a2])) {
            return a2;
        }
        int i4 = a2 + 1;
        while (i4 < i3 && this.e[i4] == i2) {
            if (obj.equals(this.f[i4])) {
                return i4;
            }
            i4++;
        }
        int i5 = a2 - 1;
        while (i5 >= 0 && this.e[i5] == i2) {
            if (obj.equals(this.f[i5])) {
                return i5;
            }
            i5--;
        }
        return ~i4;
    }

    @DexIgnore
    public boolean add(E e2) {
        int i2;
        int i3;
        if (e2 == null) {
            i3 = b();
            i2 = 0;
        } else {
            int hashCode = e2.hashCode();
            i2 = hashCode;
            i3 = a(e2, hashCode);
        }
        if (i3 >= 0) {
            return false;
        }
        int i4 = ~i3;
        int i5 = this.g;
        if (i5 >= this.e.length) {
            int i6 = 4;
            if (i5 >= 8) {
                i6 = (i5 >> 1) + i5;
            } else if (i5 >= 4) {
                i6 = 8;
            }
            int[] iArr = this.e;
            Object[] objArr = this.f;
            a(i6);
            int[] iArr2 = this.e;
            if (iArr2.length > 0) {
                System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
                System.arraycopy(objArr, 0, this.f, 0, objArr.length);
            }
            a(iArr, objArr, this.g);
        }
        int i7 = this.g;
        if (i4 < i7) {
            int[] iArr3 = this.e;
            int i8 = i4 + 1;
            System.arraycopy(iArr3, i4, iArr3, i8, i7 - i4);
            Object[] objArr2 = this.f;
            System.arraycopy(objArr2, i4, objArr2, i8, this.g - i4);
        }
        this.e[i4] = i2;
        this.f[i4] = e2;
        this.g++;
        return true;
    }

    @DexIgnore
    public boolean addAll(Collection<? extends E> collection) {
        f(this.g + collection.size());
        boolean z = false;
        for (Object add : collection) {
            z |= add(add);
        }
        return z;
    }

    @DexIgnore
    public final int b() {
        int i2 = this.g;
        if (i2 == 0) {
            return -1;
        }
        int a2 = i4.a(this.e, i2, 0);
        if (a2 < 0 || this.f[a2] == null) {
            return a2;
        }
        int i3 = a2 + 1;
        while (i3 < i2 && this.e[i3] == 0) {
            if (this.f[i3] == null) {
                return i3;
            }
            i3++;
        }
        int i4 = a2 - 1;
        while (i4 >= 0 && this.e[i4] == 0) {
            if (this.f[i4] == null) {
                return i4;
            }
            i4--;
        }
        return ~i3;
    }

    @DexIgnore
    public void clear() {
        int i2 = this.g;
        if (i2 != 0) {
            a(this.e, this.f, i2);
            this.e = i;
            this.f = j;
            this.g = 0;
        }
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    @DexIgnore
    public boolean containsAll(Collection<?> collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set = (Set) obj;
            if (size() != set.size()) {
                return false;
            }
            int i2 = 0;
            while (i2 < this.g) {
                try {
                    if (!set.contains(h(i2))) {
                        return false;
                    }
                    i2++;
                } catch (ClassCastException | NullPointerException unused) {
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public void f(int i2) {
        int[] iArr = this.e;
        if (iArr.length < i2) {
            Object[] objArr = this.f;
            a(i2);
            int i3 = this.g;
            if (i3 > 0) {
                System.arraycopy(iArr, 0, this.e, 0, i3);
                System.arraycopy(objArr, 0, this.f, 0, this.g);
            }
            a(iArr, objArr, this.g);
        }
    }

    @DexIgnore
    public E g(int i2) {
        E[] eArr = this.f;
        E e2 = eArr[i2];
        int i3 = this.g;
        if (i3 <= 1) {
            a(this.e, eArr, i3);
            this.e = i;
            this.f = j;
            this.g = 0;
        } else {
            int[] iArr = this.e;
            int i4 = 8;
            if (iArr.length <= 8 || i3 >= iArr.length / 3) {
                this.g--;
                int i5 = this.g;
                if (i2 < i5) {
                    int[] iArr2 = this.e;
                    int i6 = i2 + 1;
                    System.arraycopy(iArr2, i6, iArr2, i2, i5 - i2);
                    Object[] objArr = this.f;
                    System.arraycopy(objArr, i6, objArr, i2, this.g - i2);
                }
                this.f[this.g] = null;
            } else {
                if (i3 > 8) {
                    i4 = i3 + (i3 >> 1);
                }
                int[] iArr3 = this.e;
                Object[] objArr2 = this.f;
                a(i4);
                this.g--;
                if (i2 > 0) {
                    System.arraycopy(iArr3, 0, this.e, 0, i2);
                    System.arraycopy(objArr2, 0, this.f, 0, i2);
                }
                int i7 = this.g;
                if (i2 < i7) {
                    int i8 = i2 + 1;
                    System.arraycopy(iArr3, i8, this.e, i2, i7 - i2);
                    System.arraycopy(objArr2, i8, this.f, i2, this.g - i2);
                }
            }
        }
        return e2;
    }

    @DexIgnore
    public E h(int i2) {
        return this.f[i2];
    }

    @DexIgnore
    public int hashCode() {
        int[] iArr = this.e;
        int i2 = this.g;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            i3 += iArr[i4];
        }
        return i3;
    }

    @DexIgnore
    public int indexOf(Object obj) {
        return obj == null ? b() : a(obj, obj.hashCode());
    }

    @DexIgnore
    public boolean isEmpty() {
        return this.g <= 0;
    }

    @DexIgnore
    public Iterator<E> iterator() {
        return a().e().iterator();
    }

    @DexIgnore
    public boolean remove(Object obj) {
        int indexOf = indexOf(obj);
        if (indexOf < 0) {
            return false;
        }
        g(indexOf);
        return true;
    }

    @DexIgnore
    public boolean removeAll(Collection<?> collection) {
        boolean z = false;
        for (Object remove : collection) {
            z |= remove(remove);
        }
        return z;
    }

    @DexIgnore
    public boolean retainAll(Collection<?> collection) {
        boolean z = false;
        for (int i2 = this.g - 1; i2 >= 0; i2--) {
            if (!collection.contains(this.f[i2])) {
                g(i2);
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public int size() {
        return this.g;
    }

    @DexIgnore
    public Object[] toArray() {
        int i2 = this.g;
        Object[] objArr = new Object[i2];
        System.arraycopy(this.f, 0, objArr, 0, i2);
        return objArr;
    }

    @DexIgnore
    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.g * 14);
        sb.append('{');
        for (int i2 = 0; i2 < this.g; i2++) {
            if (i2 > 0) {
                sb.append(", ");
            }
            Object h2 = h(i2);
            if (h2 != this) {
                sb.append(h2);
            } else {
                sb.append("(this Set)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public h4(int i2) {
        if (i2 == 0) {
            this.e = i;
            this.f = j;
        } else {
            a(i2);
        }
        this.g = 0;
    }

    @DexIgnore
    public <T> T[] toArray(T[] tArr) {
        if (tArr.length < this.g) {
            tArr = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), this.g);
        }
        System.arraycopy(this.f, 0, tArr, 0, this.g);
        int length = tArr.length;
        int i2 = this.g;
        if (length > i2) {
            tArr[i2] = null;
        }
        return tArr;
    }

    @DexIgnore
    public final void a(int i2) {
        if (i2 == 8) {
            synchronized (h4.class) {
                if (m != null) {
                    Object[] objArr = m;
                    this.f = objArr;
                    m = (Object[]) objArr[0];
                    this.e = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    n--;
                    return;
                }
            }
        } else if (i2 == 4) {
            synchronized (h4.class) {
                if (k != null) {
                    Object[] objArr2 = k;
                    this.f = objArr2;
                    k = (Object[]) objArr2[0];
                    this.e = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    l--;
                    return;
                }
            }
        }
        this.e = new int[i2];
        this.f = new Object[i2];
    }

    @DexIgnore
    public static void a(int[] iArr, Object[] objArr, int i2) {
        if (iArr.length == 8) {
            synchronized (h4.class) {
                if (n < 10) {
                    objArr[0] = m;
                    objArr[1] = iArr;
                    for (int i3 = i2 - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    m = objArr;
                    n++;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (h4.class) {
                if (l < 10) {
                    objArr[0] = k;
                    objArr[1] = iArr;
                    for (int i4 = i2 - 1; i4 >= 2; i4--) {
                        objArr[i4] = null;
                    }
                    k = objArr;
                    l++;
                }
            }
        }
    }

    @DexIgnore
    public final l4<E, E> a() {
        if (this.h == null) {
            this.h = new a();
        }
        return this.h;
    }
}
