package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.h1 */
public class C1915h1 implements com.fossil.blesdk.obfuscated.C1927h7 {

    @DexIgnore
    /* renamed from: A */
    public static /* final */ int[] f5608A; // = {1, 4, 5, 3, 2, 0};

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f5609a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.content.res.Resources f5610b;

    @DexIgnore
    /* renamed from: c */
    public boolean f5611c;

    @DexIgnore
    /* renamed from: d */
    public boolean f5612d;

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1915h1.C1916a f5613e;

    @DexIgnore
    /* renamed from: f */
    public java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> f5614f;

    @DexIgnore
    /* renamed from: g */
    public java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> f5615g;

    @DexIgnore
    /* renamed from: h */
    public boolean f5616h;

    @DexIgnore
    /* renamed from: i */
    public java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> f5617i;

    @DexIgnore
    /* renamed from: j */
    public java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> f5618j;

    @DexIgnore
    /* renamed from: k */
    public boolean f5619k;

    @DexIgnore
    /* renamed from: l */
    public int f5620l; // = 0;

    @DexIgnore
    /* renamed from: m */
    public android.view.ContextMenu.ContextMenuInfo f5621m;

    @DexIgnore
    /* renamed from: n */
    public java.lang.CharSequence f5622n;

    @DexIgnore
    /* renamed from: o */
    public android.graphics.drawable.Drawable f5623o;

    @DexIgnore
    /* renamed from: p */
    public android.view.View f5624p;

    @DexIgnore
    /* renamed from: q */
    public boolean f5625q; // = false;

    @DexIgnore
    /* renamed from: r */
    public boolean f5626r; // = false;

    @DexIgnore
    /* renamed from: s */
    public boolean f5627s; // = false;

    @DexIgnore
    /* renamed from: t */
    public boolean f5628t; // = false;

    @DexIgnore
    /* renamed from: u */
    public boolean f5629u; // = false;

    @DexIgnore
    /* renamed from: v */
    public java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> f5630v; // = new java.util.ArrayList<>();

    @DexIgnore
    /* renamed from: w */
    public java.util.concurrent.CopyOnWriteArrayList<java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C2618p1>> f5631w; // = new java.util.concurrent.CopyOnWriteArrayList<>();

    @DexIgnore
    /* renamed from: x */
    public com.fossil.blesdk.obfuscated.C2179k1 f5632x;

    @DexIgnore
    /* renamed from: y */
    public boolean f5633y; // = false;

    @DexIgnore
    /* renamed from: z */
    public boolean f5634z;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.h1$a */
    public interface C1916a {
        @DexIgnore
        /* renamed from: a */
        void mo535a(com.fossil.blesdk.obfuscated.C1915h1 h1Var);

        @DexIgnore
        /* renamed from: a */
        boolean mo536a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.MenuItem menuItem);
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.h1$b */
    public interface C1917b {
        @DexIgnore
        /* renamed from: a */
        boolean mo375a(com.fossil.blesdk.obfuscated.C2179k1 k1Var);
    }

    @DexIgnore
    public C1915h1(android.content.Context context) {
        this.f5609a = context;
        this.f5610b = context.getResources();
        this.f5614f = new java.util.ArrayList<>();
        this.f5615g = new java.util.ArrayList<>();
        this.f5616h = true;
        this.f5617i = new java.util.ArrayList<>();
        this.f5618j = new java.util.ArrayList<>();
        this.f5619k = true;
        mo11501e(true);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11461a(com.fossil.blesdk.obfuscated.C2618p1 p1Var) {
        mo11462a(p1Var, this.f5609a);
    }

    @DexIgnore
    public android.view.MenuItem add(java.lang.CharSequence charSequence) {
        return mo11449a(0, 0, 0, charSequence);
    }

    @DexIgnore
    public int addIntentOptions(int i, int i2, int i3, android.content.ComponentName componentName, android.content.Intent[] intentArr, android.content.Intent intent, int i4, android.view.MenuItem[] menuItemArr) {
        android.content.pm.PackageManager packageManager = this.f5609a.getPackageManager();
        java.util.List<android.content.pm.ResolveInfo> queryIntentActivityOptions = packageManager.queryIntentActivityOptions(componentName, intentArr, intent, 0);
        int size = queryIntentActivityOptions != null ? queryIntentActivityOptions.size() : 0;
        if ((i4 & 1) == 0) {
            removeGroup(i);
        }
        for (int i5 = 0; i5 < size; i5++) {
            android.content.pm.ResolveInfo resolveInfo = queryIntentActivityOptions.get(i5);
            int i6 = resolveInfo.specificIndex;
            android.content.Intent intent2 = new android.content.Intent(i6 < 0 ? intent : intentArr[i6]);
            intent2.setComponent(new android.content.ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name));
            android.view.MenuItem intent3 = add(i, i2, i3, resolveInfo.loadLabel(packageManager)).setIcon(resolveInfo.loadIcon(packageManager)).setIntent(intent2);
            if (menuItemArr != null) {
                int i7 = resolveInfo.specificIndex;
                if (i7 >= 0) {
                    menuItemArr[i7] = intent3;
                }
            }
        }
        return size;
    }

    @DexIgnore
    public android.view.SubMenu addSubMenu(java.lang.CharSequence charSequence) {
        return addSubMenu(0, 0, 0, charSequence);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11482b(com.fossil.blesdk.obfuscated.C2618p1 p1Var) {
        java.util.Iterator<java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C2618p1>> it = this.f5631w.iterator();
        while (it.hasNext()) {
            java.lang.ref.WeakReference next = it.next();
            com.fossil.blesdk.obfuscated.C2618p1 p1Var2 = (com.fossil.blesdk.obfuscated.C2618p1) next.get();
            if (p1Var2 == null || p1Var2 == p1Var) {
                this.f5631w.remove(next);
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C1915h1 mo11485c(int i) {
        this.f5620l = i;
        return this;
    }

    @DexIgnore
    public void clear() {
        com.fossil.blesdk.obfuscated.C2179k1 k1Var = this.f5632x;
        if (k1Var != null) {
            mo11468a(k1Var);
        }
        this.f5614f.clear();
        mo11489c(true);
    }

    @DexIgnore
    public void clearHeader() {
        this.f5623o = null;
        this.f5622n = null;
        this.f5624p = null;
        mo11489c(false);
    }

    @DexIgnore
    public void close() {
        mo11464a(true);
    }

    @DexIgnore
    /* renamed from: d */
    public java.lang.String mo11494d() {
        return "android:menu:actionviewstates";
    }

    @DexIgnore
    /* renamed from: d */
    public void mo11495d(android.os.Bundle bundle) {
        mo11458a(bundle);
    }

    @DexIgnore
    /* renamed from: e */
    public void mo11500e(android.os.Bundle bundle) {
        int size = size();
        android.util.SparseArray sparseArray = null;
        for (int i = 0; i < size; i++) {
            android.view.MenuItem item = getItem(i);
            android.view.View actionView = item.getActionView();
            if (!(actionView == null || actionView.getId() == -1)) {
                if (sparseArray == null) {
                    sparseArray = new android.util.SparseArray();
                }
                actionView.saveHierarchyState(sparseArray);
                if (item.isActionViewExpanded()) {
                    bundle.putInt("android:menu:expandedactionview", item.getItemId());
                }
            }
            if (item.hasSubMenu()) {
                ((com.fossil.blesdk.obfuscated.C3078v1) item.getSubMenu()).mo11500e(bundle);
            }
        }
        if (sparseArray != null) {
            bundle.putSparseParcelableArray(mo11494d(), sparseArray);
        }
    }

    @DexIgnore
    /* renamed from: f */
    public void mo11503f(android.os.Bundle bundle) {
        mo11481b(bundle);
    }

    @DexIgnore
    public android.view.MenuItem findItem(int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            com.fossil.blesdk.obfuscated.C2179k1 k1Var = this.f5614f.get(i2);
            if (k1Var.getItemId() == i) {
                return k1Var;
            }
            if (k1Var.hasSubMenu()) {
                android.view.MenuItem findItem = k1Var.getSubMenu().findItem(i);
                if (findItem != null) {
                    return findItem;
                }
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: g */
    public android.graphics.drawable.Drawable mo11505g() {
        return this.f5623o;
    }

    @DexIgnore
    public android.view.MenuItem getItem(int i) {
        return this.f5614f.get(i);
    }

    @DexIgnore
    /* renamed from: h */
    public java.lang.CharSequence mo11507h() {
        return this.f5622n;
    }

    @DexIgnore
    public boolean hasVisibleItems() {
        if (this.f5634z) {
            return true;
        }
        int size = size();
        for (int i = 0; i < size; i++) {
            if (this.f5614f.get(i).isVisible()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: i */
    public android.view.View mo11509i() {
        return this.f5624p;
    }

    @DexIgnore
    public boolean isShortcutKey(int i, android.view.KeyEvent keyEvent) {
        return mo11454a(i, keyEvent) != null;
    }

    @DexIgnore
    /* renamed from: j */
    public java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> mo11511j() {
        mo11480b();
        return this.f5618j;
    }

    @DexIgnore
    /* renamed from: k */
    public boolean mo11512k() {
        return this.f5628t;
    }

    @DexIgnore
    /* renamed from: l */
    public android.content.res.Resources mo11513l() {
        return this.f5610b;
    }

    @DexIgnore
    /* renamed from: m */
    public com.fossil.blesdk.obfuscated.C1915h1 mo11514m() {
        return this;
    }

    @DexIgnore
    /* renamed from: n */
    public java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> mo11515n() {
        if (!this.f5616h) {
            return this.f5615g;
        }
        this.f5615g.clear();
        int size = this.f5614f.size();
        for (int i = 0; i < size; i++) {
            com.fossil.blesdk.obfuscated.C2179k1 k1Var = this.f5614f.get(i);
            if (k1Var.isVisible()) {
                this.f5615g.add(k1Var);
            }
        }
        this.f5616h = false;
        this.f5619k = true;
        return this.f5615g;
    }

    @DexIgnore
    /* renamed from: o */
    public boolean mo11516o() {
        return this.f5633y;
    }

    @DexIgnore
    /* renamed from: p */
    public boolean mo11517p() {
        return this.f5611c;
    }

    @DexIgnore
    public boolean performIdentifierAction(int i, int i2) {
        return mo11465a(findItem(i), i2);
    }

    @DexIgnore
    public boolean performShortcut(int i, android.view.KeyEvent keyEvent, int i2) {
        com.fossil.blesdk.obfuscated.C2179k1 a = mo11454a(i, keyEvent);
        boolean a2 = a != null ? mo11465a((android.view.MenuItem) a, i2) : false;
        if ((i2 & 2) != 0) {
            mo11464a(true);
        }
        return a2;
    }

    @DexIgnore
    /* renamed from: q */
    public boolean mo11520q() {
        return this.f5612d;
    }

    @DexIgnore
    /* renamed from: r */
    public void mo11521r() {
        this.f5625q = false;
        if (this.f5626r) {
            this.f5626r = false;
            mo11489c(this.f5627s);
        }
    }

    @DexIgnore
    public void removeGroup(int i) {
        int a = mo11447a(i);
        if (a >= 0) {
            int size = this.f5614f.size() - a;
            int i2 = 0;
            while (true) {
                int i3 = i2 + 1;
                if (i2 >= size || this.f5614f.get(a).getGroupId() != i) {
                    mo11489c(true);
                } else {
                    mo11457a(a, false);
                    i2 = i3;
                }
            }
            mo11489c(true);
        }
    }

    @DexIgnore
    public void removeItem(int i) {
        mo11457a(mo11479b(i), true);
    }

    @DexIgnore
    /* renamed from: s */
    public void mo11524s() {
        if (!this.f5625q) {
            this.f5625q = true;
            this.f5626r = false;
            this.f5627s = false;
        }
    }

    @DexIgnore
    public void setGroupCheckable(int i, boolean z, boolean z2) {
        int size = this.f5614f.size();
        for (int i2 = 0; i2 < size; i2++) {
            com.fossil.blesdk.obfuscated.C2179k1 k1Var = this.f5614f.get(i2);
            if (k1Var.getGroupId() == i) {
                k1Var.mo12553c(z2);
                k1Var.setCheckable(z);
            }
        }
    }

    @DexIgnore
    public void setGroupDividerEnabled(boolean z) {
        this.f5633y = z;
    }

    @DexIgnore
    public void setGroupEnabled(int i, boolean z) {
        int size = this.f5614f.size();
        for (int i2 = 0; i2 < size; i2++) {
            com.fossil.blesdk.obfuscated.C2179k1 k1Var = this.f5614f.get(i2);
            if (k1Var.getGroupId() == i) {
                k1Var.setEnabled(z);
            }
        }
    }

    @DexIgnore
    public void setGroupVisible(int i, boolean z) {
        int size = this.f5614f.size();
        boolean z2 = false;
        for (int i2 = 0; i2 < size; i2++) {
            com.fossil.blesdk.obfuscated.C2179k1 k1Var = this.f5614f.get(i2);
            if (k1Var.getGroupId() == i && k1Var.mo12557e(z)) {
                z2 = true;
            }
        }
        if (z2) {
            mo11489c(true);
        }
    }

    @DexIgnore
    public void setQwertyMode(boolean z) {
        this.f5611c = z;
        mo11489c(false);
    }

    @DexIgnore
    public int size() {
        return this.f5614f.size();
    }

    @DexIgnore
    /* renamed from: f */
    public static int m7661f(int i) {
        int i2 = (-65536 & i) >> 16;
        if (i2 >= 0) {
            int[] iArr = f5608A;
            if (i2 < iArr.length) {
                return (i & 65535) | (iArr[i2] << 16);
            }
        }
        throw new java.lang.IllegalArgumentException("order does not contain a valid category.");
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11462a(com.fossil.blesdk.obfuscated.C2618p1 p1Var, android.content.Context context) {
        this.f5631w.add(new java.lang.ref.WeakReference(p1Var));
        p1Var.mo1158a(context, this);
        this.f5619k = true;
    }

    @DexIgnore
    public android.view.MenuItem add(int i) {
        return mo11449a(0, 0, 0, this.f5610b.getString(i));
    }

    @DexIgnore
    public android.view.SubMenu addSubMenu(int i) {
        return addSubMenu(0, 0, 0, (java.lang.CharSequence) this.f5610b.getString(i));
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11487c(android.os.Bundle bundle) {
        if (bundle != null) {
            android.util.SparseArray sparseParcelableArray = bundle.getSparseParcelableArray(mo11494d());
            int size = size();
            for (int i = 0; i < size; i++) {
                android.view.MenuItem item = getItem(i);
                android.view.View actionView = item.getActionView();
                if (!(actionView == null || actionView.getId() == -1)) {
                    actionView.restoreHierarchyState(sparseParcelableArray);
                }
                if (item.hasSubMenu()) {
                    ((com.fossil.blesdk.obfuscated.C3078v1) item.getSubMenu()).mo11487c(bundle);
                }
            }
            int i2 = bundle.getInt("android:menu:expandedactionview");
            if (i2 > 0) {
                android.view.MenuItem findItem = findItem(i2);
                if (findItem != null) {
                    findItem.expandActionView();
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo11496d(com.fossil.blesdk.obfuscated.C2179k1 k1Var) {
        this.f5616h = true;
        mo11489c(true);
    }

    @DexIgnore
    public android.view.MenuItem add(int i, int i2, int i3, java.lang.CharSequence charSequence) {
        return mo11449a(i, i2, i3, charSequence);
    }

    @DexIgnore
    public android.view.SubMenu addSubMenu(int i, int i2, int i3, java.lang.CharSequence charSequence) {
        com.fossil.blesdk.obfuscated.C2179k1 k1Var = (com.fossil.blesdk.obfuscated.C2179k1) mo11449a(i, i2, i3, charSequence);
        com.fossil.blesdk.obfuscated.C3078v1 v1Var = new com.fossil.blesdk.obfuscated.C3078v1(this.f5609a, this, k1Var);
        k1Var.mo12548a(v1Var);
        return v1Var;
    }

    @DexIgnore
    public android.view.MenuItem add(int i, int i2, int i3, int i4) {
        return mo11449a(i, i2, i3, this.f5610b.getString(i4));
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo11483b(boolean z) {
        if (!this.f5631w.isEmpty()) {
            mo11524s();
            java.util.Iterator<java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C2618p1>> it = this.f5631w.iterator();
            while (it.hasNext()) {
                java.lang.ref.WeakReference next = it.next();
                com.fossil.blesdk.obfuscated.C2618p1 p1Var = (com.fossil.blesdk.obfuscated.C2618p1) next.get();
                if (p1Var == null) {
                    this.f5631w.remove(next);
                } else {
                    p1Var.mo1161a(z);
                }
            }
            mo11521r();
        }
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C1915h1 mo11493d(int i) {
        mo11456a(0, (java.lang.CharSequence) null, i, (android.graphics.drawable.Drawable) null, (android.view.View) null);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo11469a(com.fossil.blesdk.obfuscated.C3078v1 v1Var, com.fossil.blesdk.obfuscated.C2618p1 p1Var) {
        boolean z = false;
        if (this.f5631w.isEmpty()) {
            return false;
        }
        if (p1Var != null) {
            z = p1Var.mo1164a(v1Var);
        }
        java.util.Iterator<java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C2618p1>> it = this.f5631w.iterator();
        while (it.hasNext()) {
            java.lang.ref.WeakReference next = it.next();
            com.fossil.blesdk.obfuscated.C2618p1 p1Var2 = (com.fossil.blesdk.obfuscated.C2618p1) next.get();
            if (p1Var2 == null) {
                this.f5631w.remove(next);
            } else if (!z) {
                z = p1Var2.mo1164a(v1Var);
            }
        }
        return z;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo11497d(boolean z) {
        this.f5634z = z;
    }

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C2179k1 mo11502f() {
        return this.f5632x;
    }

    @DexIgnore
    public android.view.SubMenu addSubMenu(int i, int i2, int i3, int i4) {
        return addSubMenu(i, i2, i3, (java.lang.CharSequence) this.f5610b.getString(i4));
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11458a(android.os.Bundle bundle) {
        android.util.SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:presenters");
        if (sparseParcelableArray != null && !this.f5631w.isEmpty()) {
            java.util.Iterator<java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C2618p1>> it = this.f5631w.iterator();
            while (it.hasNext()) {
                java.lang.ref.WeakReference next = it.next();
                com.fossil.blesdk.obfuscated.C2618p1 p1Var = (com.fossil.blesdk.obfuscated.C2618p1) next.get();
                if (p1Var == null) {
                    this.f5631w.remove(next);
                } else {
                    int id = p1Var.getId();
                    if (id > 0) {
                        android.os.Parcelable parcelable = (android.os.Parcelable) sparseParcelableArray.get(id);
                        if (parcelable != null) {
                            p1Var.mo1159a(parcelable);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo11481b(android.os.Bundle bundle) {
        if (!this.f5631w.isEmpty()) {
            android.util.SparseArray sparseArray = new android.util.SparseArray();
            java.util.Iterator<java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C2618p1>> it = this.f5631w.iterator();
            while (it.hasNext()) {
                java.lang.ref.WeakReference next = it.next();
                com.fossil.blesdk.obfuscated.C2618p1 p1Var = (com.fossil.blesdk.obfuscated.C2618p1) next.get();
                if (p1Var == null) {
                    this.f5631w.remove(next);
                } else {
                    int id = p1Var.getId();
                    if (id > 0) {
                        android.os.Parcelable b = p1Var.mo1165b();
                        if (b != null) {
                            sparseArray.put(id, b);
                        }
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:presenters", sparseArray);
        }
    }

    @DexIgnore
    /* renamed from: e */
    public final void mo11501e(boolean z) {
        boolean z2 = true;
        if (!z || this.f5610b.getConfiguration().keyboard == 1 || !com.fossil.blesdk.obfuscated.C1863g9.m7313d(android.view.ViewConfiguration.get(this.f5609a), this.f5609a)) {
            z2 = false;
        }
        this.f5612d = z2;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11489c(boolean z) {
        if (!this.f5625q) {
            if (z) {
                this.f5616h = true;
                this.f5619k = true;
            }
            mo11483b(z);
            return;
        }
        this.f5626r = true;
        if (z) {
            this.f5627s = true;
        }
    }

    @DexIgnore
    /* renamed from: e */
    public android.content.Context mo11498e() {
        return this.f5609a;
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1915h1 mo11499e(int i) {
        mo11456a(i, (java.lang.CharSequence) null, 0, (android.graphics.drawable.Drawable) null, (android.view.View) null);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11460a(com.fossil.blesdk.obfuscated.C1915h1.C1916a aVar) {
        this.f5613e = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public android.view.MenuItem mo11449a(int i, int i2, int i3, java.lang.CharSequence charSequence) {
        int f = m7661f(i3);
        com.fossil.blesdk.obfuscated.C2179k1 a = mo11453a(i, i2, i3, f, charSequence, this.f5620l);
        android.view.ContextMenu.ContextMenuInfo contextMenuInfo = this.f5621m;
        if (contextMenuInfo != null) {
            a.mo12547a(contextMenuInfo);
        }
        java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> arrayList = this.f5614f;
        arrayList.add(m7660a(arrayList, f), a);
        mo11489c(true);
        return a;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo11479b(int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            if (this.f5614f.get(i2).getItemId() == i) {
                return i2;
            }
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11488c(com.fossil.blesdk.obfuscated.C2179k1 k1Var) {
        this.f5619k = true;
        mo11489c(true);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11480b() {
        java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> n = mo11515n();
        if (this.f5619k) {
            java.util.Iterator<java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C2618p1>> it = this.f5631w.iterator();
            boolean z = false;
            while (it.hasNext()) {
                java.lang.ref.WeakReference next = it.next();
                com.fossil.blesdk.obfuscated.C2618p1 p1Var = (com.fossil.blesdk.obfuscated.C2618p1) next.get();
                if (p1Var == null) {
                    this.f5631w.remove(next);
                } else {
                    z |= p1Var.mo1162a();
                }
            }
            if (z) {
                this.f5617i.clear();
                this.f5618j.clear();
                int size = n.size();
                for (int i = 0; i < size; i++) {
                    com.fossil.blesdk.obfuscated.C2179k1 k1Var = n.get(i);
                    if (k1Var.mo12572h()) {
                        this.f5617i.add(k1Var);
                    } else {
                        this.f5618j.add(k1Var);
                    }
                }
            } else {
                this.f5617i.clear();
                this.f5618j.clear();
                this.f5618j.addAll(mo11515n());
            }
            this.f5619k = false;
        }
    }

    @DexIgnore
    /* renamed from: c */
    public java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> mo11486c() {
        mo11480b();
        return this.f5617i;
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C2179k1 mo11453a(int i, int i2, int i3, int i4, java.lang.CharSequence charSequence, int i5) {
        com.fossil.blesdk.obfuscated.C2179k1 k1Var = new com.fossil.blesdk.obfuscated.C2179k1(this, i, i2, i3, i4, charSequence, i5);
        return k1Var;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11457a(int i, boolean z) {
        if (i >= 0 && i < this.f5614f.size()) {
            this.f5614f.remove(i);
            if (z) {
                mo11489c(true);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11459a(android.view.MenuItem menuItem) {
        int groupId = menuItem.getGroupId();
        int size = this.f5614f.size();
        mo11524s();
        for (int i = 0; i < size; i++) {
            com.fossil.blesdk.obfuscated.C2179k1 k1Var = this.f5614f.get(i);
            if (k1Var.getGroupId() == groupId && k1Var.mo12574i() && k1Var.isCheckable()) {
                k1Var.mo12551b(k1Var == menuItem);
            }
        }
        mo11521r();
    }

    @DexIgnore
    /* renamed from: a */
    public int mo11447a(int i) {
        return mo11448a(i, 0);
    }

    @DexIgnore
    /* renamed from: a */
    public int mo11448a(int i, int i2) {
        int size = size();
        if (i2 < 0) {
            i2 = 0;
        }
        while (i2 < size) {
            if (this.f5614f.get(i2).getGroupId() == i) {
                return i2;
            }
            i2++;
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo11484b(com.fossil.blesdk.obfuscated.C2179k1 k1Var) {
        boolean z = false;
        if (this.f5631w.isEmpty()) {
            return false;
        }
        mo11524s();
        java.util.Iterator<java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C2618p1>> it = this.f5631w.iterator();
        while (it.hasNext()) {
            java.lang.ref.WeakReference next = it.next();
            com.fossil.blesdk.obfuscated.C2618p1 p1Var = (com.fossil.blesdk.obfuscated.C2618p1) next.get();
            if (p1Var == null) {
                this.f5631w.remove(next);
            } else {
                z = p1Var.mo1166b(this, k1Var);
                if (z) {
                    break;
                }
            }
        }
        mo11521r();
        if (z) {
            this.f5632x = k1Var;
        }
        return z;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11467a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.MenuItem menuItem) {
        com.fossil.blesdk.obfuscated.C1915h1.C1916a aVar = this.f5613e;
        return aVar != null && aVar.mo536a(h1Var, menuItem);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11455a() {
        com.fossil.blesdk.obfuscated.C1915h1.C1916a aVar = this.f5613e;
        if (aVar != null) {
            aVar.mo535a(this);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static int m7660a(java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> arrayList, int i) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (arrayList.get(size).mo12552c() <= i) {
                return size + 1;
            }
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11463a(java.util.List<com.fossil.blesdk.obfuscated.C2179k1> list, int i, android.view.KeyEvent keyEvent) {
        boolean p = mo11517p();
        int modifiers = keyEvent.getModifiers();
        android.view.KeyCharacterMap.KeyData keyData = new android.view.KeyCharacterMap.KeyData();
        if (keyEvent.getKeyData(keyData) || i == 67) {
            int size = this.f5614f.size();
            for (int i2 = 0; i2 < size; i2++) {
                com.fossil.blesdk.obfuscated.C2179k1 k1Var = this.f5614f.get(i2);
                if (k1Var.hasSubMenu()) {
                    ((com.fossil.blesdk.obfuscated.C1915h1) k1Var.getSubMenu()).mo11463a(list, i, keyEvent);
                }
                char alphabeticShortcut = p ? k1Var.getAlphabeticShortcut() : k1Var.getNumericShortcut();
                if (((modifiers & 69647) == ((p ? k1Var.getAlphabeticModifiers() : k1Var.getNumericModifiers()) & 69647)) && alphabeticShortcut != 0) {
                    char[] cArr = keyData.meta;
                    if ((alphabeticShortcut == cArr[0] || alphabeticShortcut == cArr[2] || (p && alphabeticShortcut == 8 && i == 67)) && k1Var.isEnabled()) {
                        list.add(k1Var);
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2179k1 mo11454a(int i, android.view.KeyEvent keyEvent) {
        char c;
        java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> arrayList = this.f5630v;
        arrayList.clear();
        mo11463a((java.util.List<com.fossil.blesdk.obfuscated.C2179k1>) arrayList, i, keyEvent);
        if (arrayList.isEmpty()) {
            return null;
        }
        int metaState = keyEvent.getMetaState();
        android.view.KeyCharacterMap.KeyData keyData = new android.view.KeyCharacterMap.KeyData();
        keyEvent.getKeyData(keyData);
        int size = arrayList.size();
        if (size == 1) {
            return arrayList.get(0);
        }
        boolean p = mo11517p();
        for (int i2 = 0; i2 < size; i2++) {
            com.fossil.blesdk.obfuscated.C2179k1 k1Var = arrayList.get(i2);
            if (p) {
                c = k1Var.getAlphabeticShortcut();
            } else {
                c = k1Var.getNumericShortcut();
            }
            if ((c == keyData.meta[0] && (metaState & 2) == 0) || ((c == keyData.meta[2] && (metaState & 2) != 0) || (p && c == 8 && i == 67))) {
                return k1Var;
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11465a(android.view.MenuItem menuItem, int i) {
        return mo11466a(menuItem, (com.fossil.blesdk.obfuscated.C2618p1) null, i);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11466a(android.view.MenuItem menuItem, com.fossil.blesdk.obfuscated.C2618p1 p1Var, int i) {
        com.fossil.blesdk.obfuscated.C2179k1 k1Var = (com.fossil.blesdk.obfuscated.C2179k1) menuItem;
        if (k1Var == null || !k1Var.isEnabled()) {
            return false;
        }
        boolean g = k1Var.mo12559g();
        com.fossil.blesdk.obfuscated.C2382m8 a = k1Var.mo8358a();
        boolean z = a != null && a.hasSubMenu();
        if (k1Var.mo12558f()) {
            g |= k1Var.expandActionView();
            if (g) {
                mo11464a(true);
            }
        } else if (k1Var.hasSubMenu() || z) {
            if ((i & 4) == 0) {
                mo11464a(false);
            }
            if (!k1Var.hasSubMenu()) {
                k1Var.mo12548a(new com.fossil.blesdk.obfuscated.C3078v1(mo11498e(), this, k1Var));
            }
            com.fossil.blesdk.obfuscated.C3078v1 v1Var = (com.fossil.blesdk.obfuscated.C3078v1) k1Var.getSubMenu();
            if (z) {
                a.onPrepareSubMenu(v1Var);
            }
            g |= mo11469a(v1Var, p1Var);
            if (!g) {
                mo11464a(true);
            }
        } else if ((i & 1) == 0) {
            mo11464a(true);
        }
        return g;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11464a(boolean z) {
        if (!this.f5629u) {
            this.f5629u = true;
            java.util.Iterator<java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C2618p1>> it = this.f5631w.iterator();
            while (it.hasNext()) {
                java.lang.ref.WeakReference next = it.next();
                com.fossil.blesdk.obfuscated.C2618p1 p1Var = (com.fossil.blesdk.obfuscated.C2618p1) next.get();
                if (p1Var == null) {
                    this.f5631w.remove(next);
                } else {
                    p1Var.mo1160a(this, z);
                }
            }
            this.f5629u = false;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11456a(int i, java.lang.CharSequence charSequence, int i2, android.graphics.drawable.Drawable drawable, android.view.View view) {
        android.content.res.Resources l = mo11513l();
        if (view != null) {
            this.f5624p = view;
            this.f5622n = null;
            this.f5623o = null;
        } else {
            if (i > 0) {
                this.f5622n = l.getText(i);
            } else if (charSequence != null) {
                this.f5622n = charSequence;
            }
            if (i2 > 0) {
                this.f5623o = com.fossil.blesdk.obfuscated.C2185k6.m9358c(mo11498e(), i2);
            } else if (drawable != null) {
                this.f5623o = drawable;
            }
            this.f5624p = null;
        }
        mo11489c(false);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1915h1 mo11452a(java.lang.CharSequence charSequence) {
        mo11456a(0, charSequence, 0, (android.graphics.drawable.Drawable) null, (android.view.View) null);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1915h1 mo11450a(android.graphics.drawable.Drawable drawable) {
        mo11456a(0, (java.lang.CharSequence) null, 0, drawable, (android.view.View) null);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1915h1 mo11451a(android.view.View view) {
        mo11456a(0, (java.lang.CharSequence) null, 0, (android.graphics.drawable.Drawable) null, view);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11468a(com.fossil.blesdk.obfuscated.C2179k1 k1Var) {
        boolean z = false;
        if (!this.f5631w.isEmpty() && this.f5632x == k1Var) {
            mo11524s();
            java.util.Iterator<java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C2618p1>> it = this.f5631w.iterator();
            while (it.hasNext()) {
                java.lang.ref.WeakReference next = it.next();
                com.fossil.blesdk.obfuscated.C2618p1 p1Var = (com.fossil.blesdk.obfuscated.C2618p1) next.get();
                if (p1Var == null) {
                    this.f5631w.remove(next);
                } else {
                    z = p1Var.mo1163a(this, k1Var);
                    if (z) {
                        break;
                    }
                }
            }
            mo11521r();
            if (z) {
                this.f5632x = null;
            }
        }
        return z;
    }
}
