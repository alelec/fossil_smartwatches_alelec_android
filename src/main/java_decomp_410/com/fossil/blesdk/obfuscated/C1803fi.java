package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fi */
public class C1803fi {

    @DexIgnore
    /* renamed from: a */
    public float[] f5192a;

    @DexIgnore
    /* renamed from: a */
    public void mo9528a(android.view.View view, float f) {
        java.lang.Float f2 = (java.lang.Float) view.getTag(com.fossil.blesdk.obfuscated.C1875gh.save_non_transition_alpha);
        if (f2 != null) {
            view.setAlpha(f2.floatValue() * f);
        } else {
            view.setAlpha(f);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public float mo9529b(android.view.View view) {
        java.lang.Float f = (java.lang.Float) view.getTag(com.fossil.blesdk.obfuscated.C1875gh.save_non_transition_alpha);
        if (f != null) {
            return view.getAlpha() / f.floatValue();
        }
        return view.getAlpha();
    }

    @DexIgnore
    /* renamed from: c */
    public void mo9531c(android.view.View view) {
        if (view.getTag(com.fossil.blesdk.obfuscated.C1875gh.save_non_transition_alpha) == null) {
            view.setTag(com.fossil.blesdk.obfuscated.C1875gh.save_non_transition_alpha, java.lang.Float.valueOf(view.getAlpha()));
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo9957c(android.view.View view, android.graphics.Matrix matrix) {
        android.view.ViewParent parent = view.getParent();
        if (parent instanceof android.view.View) {
            android.view.View view2 = (android.view.View) parent;
            mo9957c(view2, matrix);
            matrix.postTranslate((float) view2.getScrollX(), (float) view2.getScrollY());
        }
        matrix.postTranslate((float) view.getLeft(), (float) view.getTop());
        android.graphics.Matrix matrix2 = view.getMatrix();
        if (!matrix2.isIdentity()) {
            android.graphics.Matrix matrix3 = new android.graphics.Matrix();
            if (matrix2.invert(matrix3)) {
                matrix.postConcat(matrix3);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9527a(android.view.View view) {
        if (view.getVisibility() == 0) {
            view.setTag(com.fossil.blesdk.obfuscated.C1875gh.save_non_transition_alpha, (java.lang.Object) null);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9955b(android.view.View view, android.graphics.Matrix matrix) {
        android.view.ViewParent parent = view.getParent();
        if (parent instanceof android.view.View) {
            android.view.View view2 = (android.view.View) parent;
            mo9955b(view2, matrix);
            matrix.preTranslate((float) (-view2.getScrollX()), (float) (-view2.getScrollY()));
        }
        matrix.preTranslate((float) view.getLeft(), (float) view.getTop());
        android.graphics.Matrix matrix2 = view.getMatrix();
        if (!matrix2.isIdentity()) {
            matrix.preConcat(matrix2);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9954a(android.view.View view, android.graphics.Matrix matrix) {
        if (matrix == null || matrix.isIdentity()) {
            view.setPivotX((float) (view.getWidth() / 2));
            view.setPivotY((float) (view.getHeight() / 2));
            view.setTranslationX(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            view.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            view.setScaleX(1.0f);
            view.setScaleY(1.0f);
            view.setRotation(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            return;
        }
        float[] fArr = this.f5192a;
        if (fArr == null) {
            fArr = new float[9];
            this.f5192a = fArr;
        }
        matrix.getValues(fArr);
        float f = fArr[3];
        float sqrt = ((float) java.lang.Math.sqrt((double) (1.0f - (f * f)))) * ((float) (fArr[0] < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? -1 : 1));
        float degrees = (float) java.lang.Math.toDegrees(java.lang.Math.atan2((double) f, (double) sqrt));
        float f2 = fArr[0] / sqrt;
        float f3 = fArr[4] / sqrt;
        float f4 = fArr[2];
        float f5 = fArr[5];
        view.setPivotX(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setPivotY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setTranslationX(f4);
        view.setTranslationY(f5);
        view.setRotation(degrees);
        view.setScaleX(f2);
        view.setScaleY(f3);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10528a(android.view.View view, int i, int i2, int i3, int i4) {
        view.setLeft(i);
        view.setTop(i2);
        view.setRight(i3);
        view.setBottom(i4);
    }
}
