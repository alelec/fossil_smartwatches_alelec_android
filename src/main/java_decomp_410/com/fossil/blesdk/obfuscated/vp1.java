package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface vp1 extends IInterface {
    @DexIgnore
    void a(cq1 cq1) throws RemoteException;

    @DexIgnore
    void a(eq1 eq1) throws RemoteException;

    @DexIgnore
    void a(kp1 kp1) throws RemoteException;

    @DexIgnore
    void a(op1 op1) throws RemoteException;

    @DexIgnore
    void a(xp1 xp1) throws RemoteException;

    @DexIgnore
    void a(zp1 zp1) throws RemoteException;

    @DexIgnore
    void a(DataHolder dataHolder) throws RemoteException;

    @DexIgnore
    void b(zp1 zp1) throws RemoteException;

    @DexIgnore
    void b(List<zp1> list) throws RemoteException;
}
