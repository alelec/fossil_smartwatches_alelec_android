package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class sa2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ImageView r;

    @DexIgnore
    public sa2(Object obj, View view, int i, FlexibleButton flexibleButton, ImageView imageView) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = imageView;
    }
}
