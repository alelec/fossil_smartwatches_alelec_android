package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qm2 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnTouchListener {
        @DexIgnore
        public long e;
        @DexIgnore
        public float f;
        @DexIgnore
        public /* final */ /* synthetic */ b g;
        @DexIgnore
        public /* final */ /* synthetic */ View h;

        @DexIgnore
        public a(qm2 qm2, b bVar, View view) {
            this.g = bVar;
            this.h = view;
        }

        @DexIgnore
        @SuppressLint({"ClickableViewAccessibility"})
        public boolean onTouch(View view, MotionEvent motionEvent) {
            int action = motionEvent.getAction();
            if (action != 0) {
                boolean z = false;
                if (action != 1) {
                    if (action == 2) {
                        if (System.currentTimeMillis() - this.e > 300) {
                            this.g.a(this.h);
                        }
                        return true;
                    } else if (action != 3) {
                        return false;
                    }
                }
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - this.e > 300) {
                    this.g.b(this.h);
                } else {
                    this.g.onClick(this.h);
                }
                this.g.d(this.h);
                float x = motionEvent.getX() - this.f;
                if (x < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    z = true;
                }
                float a = (float) (fl2.b().a() / 3);
                float f2 = x / ((float) (currentTimeMillis - this.e));
                float f3 = x / 200.0f;
                if ((Math.abs(x) > ((float) (fl2.b().a() / 10)) && Math.abs(f2) > f3) || x > a) {
                    this.g.a(this.h, Boolean.valueOf(z));
                }
                this.f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                this.e = 0;
                return true;
            }
            this.e = System.currentTimeMillis();
            this.g.c(this.h);
            this.f = motionEvent.getX();
            return true;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(View view);

        @DexIgnore
        void a(View view, Boolean bool);

        @DexIgnore
        void b(View view);

        @DexIgnore
        void c(View view);

        @DexIgnore
        void d(View view);

        @DexIgnore
        void onClick(View view);
    }

    @DexIgnore
    public void a(View view, b bVar) {
        view.setOnTouchListener(new a(this, bVar, view));
    }
}
