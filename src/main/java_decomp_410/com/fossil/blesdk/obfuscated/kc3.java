package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kc3 {
    @DexIgnore
    public /* final */ fc3 a;
    @DexIgnore
    public /* final */ uc3 b;
    @DexIgnore
    public /* final */ pc3 c;

    @DexIgnore
    public kc3(fc3 fc3, uc3 uc3, pc3 pc3) {
        kd4.b(fc3, "mHeartRateOverviewDayView");
        kd4.b(uc3, "mHeartRateOverviewWeekView");
        kd4.b(pc3, "mHeartRateOverviewMonthView");
        this.a = fc3;
        this.b = uc3;
        this.c = pc3;
    }

    @DexIgnore
    public final fc3 a() {
        return this.a;
    }

    @DexIgnore
    public final pc3 b() {
        return this.c;
    }

    @DexIgnore
    public final uc3 c() {
        return this.b;
    }
}
