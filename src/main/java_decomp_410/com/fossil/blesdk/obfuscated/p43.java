package com.fossil.blesdk.obfuscated;

import android.os.Parcelable;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class p43 extends u52 {
    @DexIgnore
    public abstract void a(Category category);

    @DexIgnore
    public abstract void a(DianaCustomizeViewModel dianaCustomizeViewModel);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(String str, Parcelable parcelable);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract void i();

    @DexIgnore
    public abstract void j();

    @DexIgnore
    public abstract void k();
}
