package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ny */
public class C2510ny {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f7893a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2510ny.C2511a f7894b;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.ny$a */
    public interface C2511a {
        @DexIgnore
        /* renamed from: a */
        java.lang.String mo9005a(java.io.File file) throws java.io.IOException;
    }

    @DexIgnore
    public C2510ny(android.content.Context context, com.fossil.blesdk.obfuscated.C2510ny.C2511a aVar) {
        this.f7893a = context;
        this.f7894b = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public byte[] mo14147a(java.lang.String str) throws java.io.IOException {
        return m11491a(mo14151d(str));
    }

    @DexIgnore
    /* renamed from: b */
    public final org.json.JSONArray mo14149b(java.io.BufferedReader bufferedReader) throws java.io.IOException {
        org.json.JSONArray jSONArray = new org.json.JSONArray();
        while (true) {
            java.lang.String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return jSONArray;
            }
            org.json.JSONObject c = mo14150c(readLine);
            if (c != null) {
                jSONArray.put(c);
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final org.json.JSONObject mo14150c(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C2997tz a = com.fossil.blesdk.obfuscated.C3075uz.m14982a(str);
        if (a != null && m11490a(a)) {
            try {
                try {
                    return m11489a(this.f7894b.mo9005a(mo14148b(a.f9796d)), a);
                } catch (org.json.JSONException e) {
                    com.fossil.blesdk.obfuscated.q44.m26805g().mo30058b("CrashlyticsCore", "Could not create a binary image json string", e);
                    return null;
                }
            } catch (java.io.IOException e2) {
                com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
                g.mo30058b("CrashlyticsCore", "Could not generate ID for file " + a.f9796d, e2);
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: d */
    public final org.json.JSONArray mo14151d(java.lang.String str) {
        org.json.JSONArray jSONArray = new org.json.JSONArray();
        try {
            java.lang.String[] split = m11492b(new org.json.JSONObject(str).getJSONArray("maps")).split(com.facebook.internal.FetchedAppSettings.DialogFeatureConfig.DIALOG_CONFIG_DIALOG_NAME_FEATURE_NAME_SEPARATOR);
            for (java.lang.String c : split) {
                org.json.JSONObject c2 = mo14150c(c);
                if (c2 != null) {
                    jSONArray.put(c2);
                }
            }
            return jSONArray;
        } catch (org.json.JSONException e) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30056a("CrashlyticsCore", "Unable to parse proc maps string", (java.lang.Throwable) e);
            return jSONArray;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public byte[] mo14146a(java.io.BufferedReader bufferedReader) throws java.io.IOException {
        return m11491a(mo14149b(bufferedReader));
    }

    @DexIgnore
    /* renamed from: a */
    public final java.io.File mo14145a(java.io.File file) {
        if (android.os.Build.VERSION.SDK_INT < 9 || !file.getAbsolutePath().startsWith("/data")) {
            return file;
        }
        try {
            return new java.io.File(this.f7893a.getPackageManager().getApplicationInfo(this.f7893a.getPackageName(), 0).nativeLibraryDir, file.getName());
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Error getting ApplicationInfo", e);
            return file;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final java.io.File mo14148b(java.lang.String str) {
        java.io.File file = new java.io.File(str);
        return !file.exists() ? mo14145a(file) : file;
    }

    @DexIgnore
    /* renamed from: b */
    public static java.lang.String m11492b(org.json.JSONArray jSONArray) throws org.json.JSONException {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        for (int i = 0; i < jSONArray.length(); i++) {
            sb.append(jSONArray.getString(i));
        }
        return sb.toString();
    }

    @DexIgnore
    /* renamed from: a */
    public static byte[] m11491a(org.json.JSONArray jSONArray) {
        org.json.JSONObject jSONObject = new org.json.JSONObject();
        try {
            jSONObject.put("binary_images", jSONArray);
            return jSONObject.toString().getBytes();
        } catch (org.json.JSONException e) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30056a("CrashlyticsCore", "Binary images string is null", (java.lang.Throwable) e);
            return new byte[0];
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static org.json.JSONObject m11489a(java.lang.String str, com.fossil.blesdk.obfuscated.C2997tz tzVar) throws org.json.JSONException {
        org.json.JSONObject jSONObject = new org.json.JSONObject();
        jSONObject.put("base_address", tzVar.f9793a);
        jSONObject.put("size", tzVar.f9794b);
        jSONObject.put("name", tzVar.f9796d);
        jSONObject.put("uuid", str);
        return jSONObject;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m11490a(com.fossil.blesdk.obfuscated.C2997tz tzVar) {
        return (tzVar.f9795c.indexOf(120) == -1 || tzVar.f9796d.indexOf(47) == -1) ? false : true;
    }
}
