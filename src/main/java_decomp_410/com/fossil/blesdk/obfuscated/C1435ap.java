package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ap */
public abstract class C1435ap<T> implements com.fossil.blesdk.obfuscated.C2902so<T> {

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.net.Uri f3555e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ android.content.ContentResolver f3556f;

    @DexIgnore
    /* renamed from: g */
    public T f3557g;

    @DexIgnore
    public C1435ap(android.content.ContentResolver contentResolver, android.net.Uri uri) {
        this.f3556f = contentResolver;
        this.f3555e = uri;
    }

    @DexIgnore
    /* renamed from: a */
    public abstract T mo8868a(android.net.Uri uri, android.content.ContentResolver contentResolver) throws java.io.FileNotFoundException;

    @DexIgnore
    /* renamed from: a */
    public final void mo8870a(com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2902so.C2903a<? super T> aVar) {
        try {
            this.f3557g = mo8868a(this.f3555e, this.f3556f);
            aVar.mo9252a(this.f3557g);
        } catch (java.io.FileNotFoundException e) {
            if (android.util.Log.isLoggable("LocalUriFetcher", 3)) {
                android.util.Log.d("LocalUriFetcher", "Failed to open Uri", e);
            }
            aVar.mo9251a((java.lang.Exception) e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo8871a(T t) throws java.io.IOException;

    @DexIgnore
    /* renamed from: b */
    public com.bumptech.glide.load.DataSource mo8872b() {
        return com.bumptech.glide.load.DataSource.LOCAL;
    }

    @DexIgnore
    public void cancel() {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8869a() {
        T t = this.f3557g;
        if (t != null) {
            try {
                mo8871a(t);
            } catch (java.io.IOException unused) {
            }
        }
    }
}
