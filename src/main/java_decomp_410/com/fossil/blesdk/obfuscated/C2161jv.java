package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jv */
public class C2161jv {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.List<java.lang.String> f6590a; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.Map<java.lang.String, java.util.List<com.fossil.blesdk.obfuscated.C2161jv.C2162a<?, ?>>> f6591b; // = new java.util.HashMap();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jv$a")
    /* renamed from: com.fossil.blesdk.obfuscated.jv$a */
    public static class C2162a<T, R> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.Class<T> f6592a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.Class<R> f6593b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ com.fossil.blesdk.obfuscated.C2427mo<T, R> f6594c;

        @DexIgnore
        public C2162a(java.lang.Class<T> cls, java.lang.Class<R> cls2, com.fossil.blesdk.obfuscated.C2427mo<T, R> moVar) {
            this.f6592a = cls;
            this.f6593b = cls2;
            this.f6594c = moVar;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo12488a(java.lang.Class<?> cls, java.lang.Class<?> cls2) {
            return this.f6592a.isAssignableFrom(cls) && cls2.isAssignableFrom(this.f6593b);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo12486a(java.util.List<java.lang.String> list) {
        java.util.ArrayList<java.lang.String> arrayList = new java.util.ArrayList<>(this.f6590a);
        this.f6590a.clear();
        this.f6590a.addAll(list);
        for (java.lang.String str : arrayList) {
            if (!list.contains(str)) {
                this.f6590a.add(str);
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public synchronized <T, R> java.util.List<java.lang.Class<R>> mo12487b(java.lang.Class<T> cls, java.lang.Class<R> cls2) {
        java.util.ArrayList arrayList;
        arrayList = new java.util.ArrayList();
        for (java.lang.String str : this.f6590a) {
            java.util.List<com.fossil.blesdk.obfuscated.C2161jv.C2162a> list = this.f6591b.get(str);
            if (list != null) {
                for (com.fossil.blesdk.obfuscated.C2161jv.C2162a aVar : list) {
                    if (aVar.mo12488a(cls, cls2) && !arrayList.contains(aVar.f6593b)) {
                        arrayList.add(aVar.f6593b);
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized <T, R> java.util.List<com.fossil.blesdk.obfuscated.C2427mo<T, R>> mo12483a(java.lang.Class<T> cls, java.lang.Class<R> cls2) {
        java.util.ArrayList arrayList;
        arrayList = new java.util.ArrayList();
        for (java.lang.String str : this.f6590a) {
            java.util.List<com.fossil.blesdk.obfuscated.C2161jv.C2162a> list = this.f6591b.get(str);
            if (list != null) {
                for (com.fossil.blesdk.obfuscated.C2161jv.C2162a aVar : list) {
                    if (aVar.mo12488a(cls, cls2)) {
                        arrayList.add(aVar.f6594c);
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized <T, R> void mo12485a(java.lang.String str, com.fossil.blesdk.obfuscated.C2427mo<T, R> moVar, java.lang.Class<T> cls, java.lang.Class<R> cls2) {
        mo12484a(str).add(new com.fossil.blesdk.obfuscated.C2161jv.C2162a(cls, cls2, moVar));
    }

    @DexIgnore
    /* renamed from: a */
    public final synchronized java.util.List<com.fossil.blesdk.obfuscated.C2161jv.C2162a<?, ?>> mo12484a(java.lang.String str) {
        java.util.List<com.fossil.blesdk.obfuscated.C2161jv.C2162a<?, ?>> list;
        if (!this.f6590a.contains(str)) {
            this.f6590a.add(str);
        }
        list = this.f6591b.get(str);
        if (list == null) {
            list = new java.util.ArrayList<>();
            this.f6591b.put(str, list);
        }
        return list;
    }
}
