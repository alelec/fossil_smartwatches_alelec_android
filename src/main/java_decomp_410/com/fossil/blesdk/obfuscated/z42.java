package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.remote.GoogleApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z42 implements Factory<GoogleApiService> {
    @DexIgnore
    public /* final */ n42 a;
    @DexIgnore
    public /* final */ Provider<xo2> b;
    @DexIgnore
    public /* final */ Provider<bp2> c;

    @DexIgnore
    public z42(n42 n42, Provider<xo2> provider, Provider<bp2> provider2) {
        this.a = n42;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static z42 a(n42 n42, Provider<xo2> provider, Provider<bp2> provider2) {
        return new z42(n42, provider, provider2);
    }

    @DexIgnore
    public static GoogleApiService b(n42 n42, Provider<xo2> provider, Provider<bp2> provider2) {
        return a(n42, provider.get(), provider2.get());
    }

    @DexIgnore
    public static GoogleApiService a(n42 n42, xo2 xo2, bp2 bp2) {
        GoogleApiService c2 = n42.c(xo2, bp2);
        n44.a(c2, "Cannot return null from a non-@Nullable @Provides method");
        return c2;
    }

    @DexIgnore
    public GoogleApiService get() {
        return b(this.a, this.b, this.c);
    }
}
