package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lt */
public final class C2342lt {

    @DexIgnore
    /* renamed from: a */
    public static volatile boolean f7278a; // = true;

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.drawable.Drawable m10273a(android.content.Context context, android.content.Context context2, int i) {
        return m10274a(context, context2, i, (android.content.res.Resources.Theme) null);
    }

    @DexIgnore
    /* renamed from: b */
    public static android.graphics.drawable.Drawable m10275b(android.content.Context context, int i, android.content.res.Resources.Theme theme) {
        return com.fossil.blesdk.obfuscated.C2785r6.m13075a(context.getResources(), i, theme);
    }

    @DexIgnore
    /* renamed from: c */
    public static android.graphics.drawable.Drawable m10276c(android.content.Context context, int i, android.content.res.Resources.Theme theme) {
        if (theme != null) {
            context = new com.fossil.blesdk.obfuscated.C2999u0(context, theme);
        }
        return com.fossil.blesdk.obfuscated.C2364m0.m10497c(context, i);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.drawable.Drawable m10272a(android.content.Context context, int i, android.content.res.Resources.Theme theme) {
        return m10274a(context, context, i, theme);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.drawable.Drawable m10274a(android.content.Context context, android.content.Context context2, int i, android.content.res.Resources.Theme theme) {
        try {
            if (f7278a) {
                return m10276c(context2, i, theme);
            }
        } catch (java.lang.NoClassDefFoundError unused) {
            f7278a = false;
        } catch (java.lang.IllegalStateException e) {
            if (!context.getPackageName().equals(context2.getPackageName())) {
                return com.fossil.blesdk.obfuscated.C2185k6.m9358c(context2, i);
            }
            throw e;
        } catch (android.content.res.Resources.NotFoundException unused2) {
        }
        if (theme == null) {
            theme = context2.getTheme();
        }
        return m10275b(context2, i, theme);
    }
}
