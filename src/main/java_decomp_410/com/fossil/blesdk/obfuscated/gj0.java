package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.os.Binder;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.blesdk.obfuscated.tj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gj0 extends tj0.a {
    @DexIgnore
    public static Account a(tj0 tj0) {
        if (tj0 != null) {
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                return tj0.h();
            } catch (RemoteException unused) {
                Log.w("AccountAccessor", "Remote account accessor probably died");
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
        return null;
    }
}
