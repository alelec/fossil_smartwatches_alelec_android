package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.wm4;
import java.lang.ref.Reference;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ol4 {
    @DexIgnore
    public static /* final */ Executor g; // = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), jm4.a("OkHttp ConnectionPool", true));
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ Runnable c;
    @DexIgnore
    public /* final */ Deque<tm4> d;
    @DexIgnore
    public /* final */ um4 e;
    @DexIgnore
    public boolean f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x002b */
        public void run() {
            while (true) {
                long a = ol4.this.a(System.nanoTime());
                if (a != -1) {
                    if (a > 0) {
                        long j = a / 1000000;
                        long j2 = a - (1000000 * j);
                        synchronized (ol4.this) {
                            ol4.this.wait(j, (int) j2);
                        }
                    }
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public ol4() {
        this(5, 5, TimeUnit.MINUTES);
    }

    @DexIgnore
    public tm4 a(gl4 gl4, wm4 wm4, fm4 fm4) {
        for (tm4 next : this.d) {
            if (next.a(gl4, fm4)) {
                wm4.a(next, true);
                return next;
            }
        }
        return null;
    }

    @DexIgnore
    public void b(tm4 tm4) {
        if (!this.f) {
            this.f = true;
            g.execute(this.c);
        }
        this.d.add(tm4);
    }

    @DexIgnore
    public ol4(int i, long j, TimeUnit timeUnit) {
        this.c = new a();
        this.d = new ArrayDeque();
        this.e = new um4();
        this.a = i;
        this.b = timeUnit.toNanos(j);
        if (j <= 0) {
            throw new IllegalArgumentException("keepAliveDuration <= 0: " + j);
        }
    }

    @DexIgnore
    public Socket a(gl4 gl4, wm4 wm4) {
        for (tm4 next : this.d) {
            if (next.a(gl4, (fm4) null) && next.e() && next != wm4.c()) {
                return wm4.b(next);
            }
        }
        return null;
    }

    @DexIgnore
    public boolean a(tm4 tm4) {
        if (tm4.k || this.a == 0) {
            this.d.remove(tm4);
            return true;
        }
        notifyAll();
        return false;
    }

    @DexIgnore
    public long a(long j) {
        synchronized (this) {
            long j2 = Long.MIN_VALUE;
            tm4 tm4 = null;
            int i = 0;
            int i2 = 0;
            for (tm4 next : this.d) {
                if (a(next, j) > 0) {
                    i2++;
                } else {
                    i++;
                    long j3 = j - next.o;
                    if (j3 > j2) {
                        tm4 = next;
                        j2 = j3;
                    }
                }
            }
            if (j2 < this.b) {
                if (i <= this.a) {
                    if (i > 0) {
                        long j4 = this.b - j2;
                        return j4;
                    } else if (i2 > 0) {
                        long j5 = this.b;
                        return j5;
                    } else {
                        this.f = false;
                        return -1;
                    }
                }
            }
            this.d.remove(tm4);
            jm4.a(tm4.g());
            return 0;
        }
    }

    @DexIgnore
    public final int a(tm4 tm4, long j) {
        List<Reference<wm4>> list = tm4.n;
        int i = 0;
        while (i < list.size()) {
            Reference reference = list.get(i);
            if (reference.get() != null) {
                i++;
            } else {
                ao4.d().a("A connection to " + tm4.f().a().k() + " was leaked. Did you forget to close a response body?", ((wm4.a) reference).a);
                list.remove(i);
                tm4.k = true;
                if (list.isEmpty()) {
                    tm4.o = j - this.b;
                    return 0;
                }
            }
        }
        return list.size();
    }
}
