package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class eu implements gu<Drawable, byte[]> {
    @DexIgnore
    public /* final */ jq a;
    @DexIgnore
    public /* final */ gu<Bitmap, byte[]> b;
    @DexIgnore
    public /* final */ gu<ut, byte[]> c;

    @DexIgnore
    public eu(jq jqVar, gu<Bitmap, byte[]> guVar, gu<ut, byte[]> guVar2) {
        this.a = jqVar;
        this.b = guVar;
        this.c = guVar2;
    }

    @DexIgnore
    public static aq<ut> a(aq<Drawable> aqVar) {
        return aqVar;
    }

    @DexIgnore
    public aq<byte[]> a(aq<Drawable> aqVar, lo loVar) {
        Drawable drawable = aqVar.get();
        if (drawable instanceof BitmapDrawable) {
            return this.b.a(ps.a(((BitmapDrawable) drawable).getBitmap(), this.a), loVar);
        }
        if (!(drawable instanceof ut)) {
            return null;
        }
        gu<ut, byte[]> guVar = this.c;
        a(aqVar);
        return guVar.a(aqVar, loVar);
    }
}
