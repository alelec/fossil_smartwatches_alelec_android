package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.tj */
public class C2968tj extends com.fossil.blesdk.obfuscated.C2129jj {

    @DexIgnore
    /* renamed from: j */
    public static com.fossil.blesdk.obfuscated.C2968tj f9685j;

    @DexIgnore
    /* renamed from: k */
    public static com.fossil.blesdk.obfuscated.C2968tj f9686k;

    @DexIgnore
    /* renamed from: l */
    public static /* final */ java.lang.Object f9687l; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: a */
    public android.content.Context f9688a;

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C3290xi f9689b;

    @DexIgnore
    /* renamed from: c */
    public androidx.work.impl.WorkDatabase f9690c;

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C3444zl f9691d;

    @DexIgnore
    /* renamed from: e */
    public java.util.List<com.fossil.blesdk.obfuscated.C2656pj> f9692e;

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C2572oj f9693f;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C2896sl f9694g;

    @DexIgnore
    /* renamed from: h */
    public boolean f9695h;

    @DexIgnore
    /* renamed from: i */
    public android.content.BroadcastReceiver.PendingResult f9696i;

    @DexIgnore
    public C2968tj(android.content.Context context, com.fossil.blesdk.obfuscated.C3290xi xiVar, com.fossil.blesdk.obfuscated.C3444zl zlVar) {
        this(context, xiVar, zlVar, context.getResources().getBoolean(com.fossil.blesdk.obfuscated.C1952hj.workmanager_test_configuration));
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2968tj m14287a() {
        synchronized (f9687l) {
            if (f9685j != null) {
                com.fossil.blesdk.obfuscated.C2968tj tjVar = f9685j;
                return tjVar;
            }
            com.fossil.blesdk.obfuscated.C2968tj tjVar2 = f9686k;
            return tjVar2;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public android.content.Context mo16456b() {
        return this.f9688a;
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C3290xi mo16458c() {
        return this.f9689b;
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C2896sl mo16459d() {
        return this.f9694g;
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C2572oj mo16460e() {
        return this.f9693f;
    }

    @DexIgnore
    /* renamed from: f */
    public java.util.List<com.fossil.blesdk.obfuscated.C2656pj> mo16461f() {
        return this.f9692e;
    }

    @DexIgnore
    /* renamed from: g */
    public androidx.work.impl.WorkDatabase mo16462g() {
        return this.f9690c;
    }

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C3444zl mo16463h() {
        return this.f9691d;
    }

    @DexIgnore
    /* renamed from: i */
    public void mo16464i() {
        synchronized (f9687l) {
            this.f9695h = true;
            if (this.f9696i != null) {
                this.f9696i.finish();
                this.f9696i = null;
            }
        }
    }

    @DexIgnore
    /* renamed from: j */
    public void mo16465j() {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            com.fossil.blesdk.obfuscated.C1735ek.m6501a(mo16456b());
        }
        mo16462g().mo3780d().mo12027d();
        com.fossil.blesdk.obfuscated.C2744qj.m12826a(mo16458c(), mo16462g(), mo16461f());
    }

    @DexIgnore
    /* renamed from: b */
    public void mo16457b(java.lang.String str) {
        this.f9691d.mo8788a(new com.fossil.blesdk.obfuscated.C3123vl(this, str));
    }

    @DexIgnore
    public C2968tj(android.content.Context context, com.fossil.blesdk.obfuscated.C3290xi xiVar, com.fossil.blesdk.obfuscated.C3444zl zlVar, boolean z) {
        this(context, xiVar, zlVar, androidx.work.impl.WorkDatabase.m2479a(context.getApplicationContext(), zlVar.mo8789b(), z));
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2968tj m14288a(android.content.Context context) {
        com.fossil.blesdk.obfuscated.C2968tj a;
        synchronized (f9687l) {
            a = m14287a();
            if (a == null) {
                android.content.Context applicationContext = context.getApplicationContext();
                if (applicationContext instanceof com.fossil.blesdk.obfuscated.C3290xi.C3292b) {
                    m14289a(applicationContext, ((com.fossil.blesdk.obfuscated.C3290xi.C3292b) applicationContext).mo17756a());
                    a = m14288a(applicationContext);
                } else {
                    throw new java.lang.IllegalStateException("WorkManager is not initialized properly.  You have explicitly disabled WorkManagerInitializer in your manifest, have not manually called WorkManager#initialize at this point, and your Application does not implement Configuration.Provider.");
                }
            }
        }
        return a;
    }

    @DexIgnore
    public C2968tj(android.content.Context context, com.fossil.blesdk.obfuscated.C3290xi xiVar, com.fossil.blesdk.obfuscated.C3444zl zlVar, androidx.work.impl.WorkDatabase workDatabase) {
        android.content.Context applicationContext = context.getApplicationContext();
        com.fossil.blesdk.obfuscated.C1635dj.m5872a((com.fossil.blesdk.obfuscated.C1635dj) new com.fossil.blesdk.obfuscated.C1635dj.C1636a(xiVar.mo17751g()));
        android.content.Context context2 = context;
        com.fossil.blesdk.obfuscated.C3290xi xiVar2 = xiVar;
        com.fossil.blesdk.obfuscated.C3444zl zlVar2 = zlVar;
        androidx.work.impl.WorkDatabase workDatabase2 = workDatabase;
        java.util.List<com.fossil.blesdk.obfuscated.C2656pj> a = mo16451a(applicationContext, zlVar);
        com.fossil.blesdk.obfuscated.C2572oj ojVar = new com.fossil.blesdk.obfuscated.C2572oj(context2, xiVar2, zlVar2, workDatabase2, a);
        mo16453a(context2, xiVar2, zlVar2, workDatabase2, a, ojVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m14289a(android.content.Context context, com.fossil.blesdk.obfuscated.C3290xi xiVar) {
        synchronized (f9687l) {
            if (f9685j != null) {
                if (f9686k != null) {
                    throw new java.lang.IllegalStateException("WorkManager is already initialized.  Did you try to initialize it manually without disabling WorkManagerInitializer? See WorkManager#initialize(Context, Configuration) or the class levelJavadoc for more information.");
                }
            }
            if (f9685j == null) {
                android.content.Context applicationContext = context.getApplicationContext();
                if (f9686k == null) {
                    f9686k = new com.fossil.blesdk.obfuscated.C2968tj(applicationContext, xiVar, new com.fossil.blesdk.obfuscated.C1430am(xiVar.mo17752h()));
                }
                f9685j = f9686k;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1804fj mo12365a(java.lang.String str, androidx.work.ExistingWorkPolicy existingWorkPolicy, java.util.List<com.fossil.blesdk.obfuscated.C1733ej> list) {
        return new com.fossil.blesdk.obfuscated.C2811rj(this, str, existingWorkPolicy, list).mo15591a();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16454a(java.lang.String str) {
        mo16455a(str, (androidx.work.WorkerParameters.C0359a) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16455a(java.lang.String str, androidx.work.WorkerParameters.C0359a aVar) {
        this.f9691d.mo8788a(new com.fossil.blesdk.obfuscated.C3046ul(this, str, aVar));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16452a(android.content.BroadcastReceiver.PendingResult pendingResult) {
        synchronized (f9687l) {
            this.f9696i = pendingResult;
            if (this.f9695h) {
                this.f9696i.finish();
                this.f9696i = null;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16453a(android.content.Context context, com.fossil.blesdk.obfuscated.C3290xi xiVar, com.fossil.blesdk.obfuscated.C3444zl zlVar, androidx.work.impl.WorkDatabase workDatabase, java.util.List<com.fossil.blesdk.obfuscated.C2656pj> list, com.fossil.blesdk.obfuscated.C2572oj ojVar) {
        android.content.Context applicationContext = context.getApplicationContext();
        this.f9688a = applicationContext;
        this.f9689b = xiVar;
        this.f9691d = zlVar;
        this.f9690c = workDatabase;
        this.f9692e = list;
        this.f9693f = ojVar;
        this.f9694g = new com.fossil.blesdk.obfuscated.C2896sl(this.f9688a);
        this.f9695h = false;
        this.f9691d.mo8788a(new androidx.work.impl.utils.ForceStopRunnable(applicationContext, this));
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<com.fossil.blesdk.obfuscated.C2656pj> mo16451a(android.content.Context context, com.fossil.blesdk.obfuscated.C3444zl zlVar) {
        return java.util.Arrays.asList(new com.fossil.blesdk.obfuscated.C2656pj[]{com.fossil.blesdk.obfuscated.C2744qj.m12825a(context, this), new com.fossil.blesdk.obfuscated.C3118vj(context, zlVar, this)});
    }
}
