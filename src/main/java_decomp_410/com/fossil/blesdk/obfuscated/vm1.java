package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.de0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vm1 implements de0.d.e {
    @DexIgnore
    public static /* final */ vm1 m; // = new vm1(false, false, (String) null, false, (String) null, false, (Long) null, (Long) null);
    @DexIgnore
    public /* final */ boolean e; // = false;
    @DexIgnore
    public /* final */ boolean f; // = false;
    @DexIgnore
    public /* final */ String g; // = null;
    @DexIgnore
    public /* final */ boolean h; // = false;
    @DexIgnore
    public /* final */ String i; // = null;
    @DexIgnore
    public /* final */ boolean j; // = false;
    @DexIgnore
    public /* final */ Long k; // = null;
    @DexIgnore
    public /* final */ Long l; // = null;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    public vm1(boolean z, boolean z2, String str, boolean z3, String str2, boolean z4, Long l2, Long l3) {
    }

    @DexIgnore
    public final Long a() {
        return this.k;
    }

    @DexIgnore
    public final String b() {
        return this.i;
    }

    @DexIgnore
    public final Long c() {
        return this.l;
    }

    @DexIgnore
    public final String d() {
        return this.g;
    }

    @DexIgnore
    public final boolean e() {
        return this.h;
    }

    @DexIgnore
    public final boolean f() {
        return this.f;
    }

    @DexIgnore
    public final boolean g() {
        return this.e;
    }

    @DexIgnore
    public final boolean h() {
        return this.j;
    }
}
