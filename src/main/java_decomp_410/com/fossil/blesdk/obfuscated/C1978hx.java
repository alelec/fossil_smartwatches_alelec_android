package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hx */
public class C1978hx extends com.fossil.blesdk.obfuscated.o44.C4784b {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1451ay f5876a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2356lx f5877b;

    @DexIgnore
    public C1978hx(com.fossil.blesdk.obfuscated.C1451ay ayVar, com.fossil.blesdk.obfuscated.C2356lx lxVar) {
        this.f5876a = ayVar;
        this.f5877b = lxVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11767a(android.app.Activity activity) {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11768a(android.app.Activity activity, android.os.Bundle bundle) {
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11769b(android.app.Activity activity) {
        this.f5876a.mo8976a(activity, com.crashlytics.android.answers.SessionEvent.Type.PAUSE);
        this.f5877b.mo13478b();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11770b(android.app.Activity activity, android.os.Bundle bundle) {
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11771c(android.app.Activity activity) {
        this.f5876a.mo8976a(activity, com.crashlytics.android.answers.SessionEvent.Type.RESUME);
        this.f5877b.mo13479c();
    }

    @DexIgnore
    /* renamed from: d */
    public void mo11772d(android.app.Activity activity) {
        this.f5876a.mo8976a(activity, com.crashlytics.android.answers.SessionEvent.Type.START);
    }

    @DexIgnore
    /* renamed from: e */
    public void mo11773e(android.app.Activity activity) {
        this.f5876a.mo8976a(activity, com.crashlytics.android.answers.SessionEvent.Type.STOP);
    }
}
