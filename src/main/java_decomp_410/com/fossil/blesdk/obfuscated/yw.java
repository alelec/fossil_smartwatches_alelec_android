package com.fossil.blesdk.obfuscated;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yw extends v44<Void> implements w44 {
    @DexIgnore
    public /* final */ uy k;
    @DexIgnore
    public /* final */ Collection<? extends v44> l;

    @DexIgnore
    public yw() {
        this(new bx(), new ky(), new uy());
    }

    @DexIgnore
    public static void a(int i, String str, String str2) {
        v();
        w().k.b(i, str, str2);
    }

    @DexIgnore
    public static void v() {
        if (w() == null) {
            throw new IllegalStateException("Crashlytics must be initialized by calling Fabric.with(Context) prior to calling Crashlytics.getInstance()");
        }
    }

    @DexIgnore
    public static yw w() {
        return (yw) q44.a(yw.class);
    }

    @DexIgnore
    public Collection<? extends v44> i() {
        return this.l;
    }

    @DexIgnore
    public Void k() {
        return null;
    }

    @DexIgnore
    public String p() {
        return "com.crashlytics.sdk.android:crashlytics";
    }

    @DexIgnore
    public String r() {
        return "2.10.1.34";
    }

    @DexIgnore
    public yw(bx bxVar, ky kyVar, uy uyVar) {
        this.k = uyVar;
        this.l = Collections.unmodifiableCollection(Arrays.asList(new v44[]{bxVar, kyVar, uyVar}));
    }
}
