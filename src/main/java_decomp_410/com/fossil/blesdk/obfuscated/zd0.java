package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.UserManager;
import android.util.Log;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zd0 {
    @DexIgnore
    public static /* final */ int GMS_AVAILABILITY_NOTIFICATION_ID; // = 10436;
    @DexIgnore
    public static /* final */ int GMS_GENERAL_ERROR_NOTIFICATION_ID; // = 39789;
    @DexIgnore
    public static /* final */ String GOOGLE_PLAY_GAMES_PACKAGE; // = "com.google.android.play.games";
    @DexIgnore
    @Deprecated
    public static /* final */ String GOOGLE_PLAY_SERVICES_PACKAGE; // = "com.google.android.gms";
    @DexIgnore
    @Deprecated
    public static /* final */ int GOOGLE_PLAY_SERVICES_VERSION_CODE; // = 12451000;
    @DexIgnore
    public static /* final */ String GOOGLE_PLAY_STORE_PACKAGE; // = "com.android.vending";
    @DexIgnore
    public static /* final */ AtomicBoolean sCanceledAvailabilityNotification; // = new AtomicBoolean();
    @DexIgnore
    public static boolean zzah;
    @DexIgnore
    public static boolean zzai;
    @DexIgnore
    public static boolean zzaj;
    @DexIgnore
    public static boolean zzak;
    @DexIgnore
    public static /* final */ AtomicBoolean zzal; // = new AtomicBoolean();

    @DexIgnore
    @Deprecated
    public static void cancelAvailabilityErrorNotifications(Context context) {
        if (!sCanceledAvailabilityNotification.getAndSet(true)) {
            try {
                NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
                if (notificationManager != null) {
                    notificationManager.cancel(GMS_AVAILABILITY_NOTIFICATION_ID);
                }
            } catch (SecurityException unused) {
            }
        }
    }

    @DexIgnore
    public static void enableUsingApkIndependentContext() {
        zzal.set(true);
    }

    @DexIgnore
    @Deprecated
    public static void ensurePlayServicesAvailable(Context context, int i) throws GooglePlayServicesRepairableException, GooglePlayServicesNotAvailableException {
        int a = yd0.a().a(context, i);
        if (a != 0) {
            Intent a2 = yd0.a().a(context, a, "e");
            StringBuilder sb = new StringBuilder(57);
            sb.append("GooglePlayServices not available due to error ");
            sb.append(a);
            Log.e("GooglePlayServicesUtil", sb.toString());
            if (a2 == null) {
                throw new GooglePlayServicesNotAvailableException(a);
            }
            throw new GooglePlayServicesRepairableException(a, "Google Play Services not available", a2);
        }
    }

    @DexIgnore
    @Deprecated
    public static int getApkVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode;
        } catch (PackageManager.NameNotFoundException unused) {
            Log.w("GooglePlayServicesUtil", "Google Play services is missing.");
            return 0;
        }
    }

    @DexIgnore
    @Deprecated
    public static int getClientVersion(Context context) {
        bk0.b(true);
        return gm0.a(context, context.getPackageName());
    }

    @DexIgnore
    @Deprecated
    public static PendingIntent getErrorPendingIntent(int i, Context context, int i2) {
        return yd0.a().a(context, i, i2);
    }

    @DexIgnore
    @Deprecated
    public static String getErrorString(int i) {
        return ud0.zza(i);
    }

    @DexIgnore
    @Deprecated
    public static Intent getGooglePlayServicesAvailabilityRecoveryIntent(int i) {
        return yd0.a().a((Context) null, i, (String) null);
    }

    @DexIgnore
    public static Context getRemoteContext(Context context) {
        try {
            return context.createPackageContext("com.google.android.gms", 3);
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public static Resources getRemoteResource(Context context) {
        try {
            return context.getPackageManager().getResourcesForApplication("com.google.android.gms");
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public static boolean honorsDebugCertificates(Context context) {
        if (!zzak) {
            try {
                PackageInfo b = bn0.b(context).b("com.google.android.gms", 64);
                ae0.a(context);
                if (b == null || ae0.a(b, false) || !ae0.a(b, true)) {
                    zzaj = false;
                } else {
                    zzaj = true;
                }
            } catch (PackageManager.NameNotFoundException e) {
                Log.w("GooglePlayServicesUtil", "Cannot find Google Play services package name.", e);
            } finally {
                zzak = true;
            }
        }
        return zzaj || !lm0.a();
    }

    @DexIgnore
    @Deprecated
    public static int isGooglePlayServicesAvailable(Context context) {
        return isGooglePlayServicesAvailable(context, GOOGLE_PLAY_SERVICES_VERSION_CODE);
    }

    @DexIgnore
    @Deprecated
    public static boolean isGooglePlayServicesUid(Context context, int i) {
        return sm0.a(context, i);
    }

    @DexIgnore
    @Deprecated
    public static boolean isPlayServicesPossiblyUpdating(Context context, int i) {
        if (i == 18) {
            return true;
        }
        if (i == 1) {
            return isUninstalledAppPossiblyUpdating(context, "com.google.android.gms");
        }
        return false;
    }

    @DexIgnore
    @Deprecated
    public static boolean isPlayStorePossiblyUpdating(Context context, int i) {
        if (i == 9) {
            return isUninstalledAppPossiblyUpdating(context, "com.android.vending");
        }
        return false;
    }

    @DexIgnore
    @TargetApi(18)
    public static boolean isRestrictedUserProfile(Context context) {
        if (!pm0.d()) {
            return false;
        }
        Bundle applicationRestrictions = ((UserManager) context.getSystemService("user")).getApplicationRestrictions(context.getPackageName());
        return applicationRestrictions != null && ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equals(applicationRestrictions.getString("restricted_profile"));
    }

    @DexIgnore
    @Deprecated
    public static boolean isSidewinderDevice(Context context) {
        return lm0.a(context);
    }

    @DexIgnore
    @TargetApi(21)
    public static boolean isUninstalledAppPossiblyUpdating(Context context, String str) {
        boolean equals = str.equals("com.google.android.gms");
        if (pm0.g()) {
            try {
                for (PackageInstaller.SessionInfo appPackageName : context.getPackageManager().getPackageInstaller().getAllSessions()) {
                    if (str.equals(appPackageName.getAppPackageName())) {
                        return true;
                    }
                }
            } catch (Exception unused) {
                return false;
            }
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(str, 8192);
            if (equals) {
                return applicationInfo.enabled;
            }
            return applicationInfo.enabled && !isRestrictedUserProfile(context);
        } catch (PackageManager.NameNotFoundException unused2) {
        }
    }

    @DexIgnore
    @Deprecated
    public static boolean isUserRecoverableError(int i) {
        return i == 1 || i == 2 || i == 3 || i == 9;
    }

    @DexIgnore
    @TargetApi(19)
    @Deprecated
    public static boolean uidHasPackageName(Context context, int i, String str) {
        return sm0.a(context, i, str);
    }

    @DexIgnore
    public static int zza(Context context, boolean z, int i) {
        bk0.a(i >= 0);
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo = null;
        if (z) {
            try {
                packageInfo = packageManager.getPackageInfo("com.android.vending", 8256);
            } catch (PackageManager.NameNotFoundException unused) {
                Log.w("GooglePlayServicesUtil", "Google Play Store is missing.");
                return 9;
            }
        }
        try {
            PackageInfo packageInfo2 = packageManager.getPackageInfo("com.google.android.gms", 64);
            ae0.a(context);
            if (!ae0.a(packageInfo2, true)) {
                Log.w("GooglePlayServicesUtil", "Google Play services signature invalid.");
                return 9;
            } else if (z && (!ae0.a(packageInfo, true) || !packageInfo.signatures[0].equals(packageInfo2.signatures[0]))) {
                Log.w("GooglePlayServicesUtil", "Google Play Store signature invalid.");
                return 9;
            } else if (xm0.a(packageInfo2.versionCode) < xm0.a(i)) {
                int i2 = packageInfo2.versionCode;
                StringBuilder sb = new StringBuilder(77);
                sb.append("Google Play services out of date.  Requires ");
                sb.append(i);
                sb.append(" but found ");
                sb.append(i2);
                Log.w("GooglePlayServicesUtil", sb.toString());
                return 2;
            } else {
                ApplicationInfo applicationInfo = packageInfo2.applicationInfo;
                if (applicationInfo == null) {
                    try {
                        applicationInfo = packageManager.getApplicationInfo("com.google.android.gms", 0);
                    } catch (PackageManager.NameNotFoundException e) {
                        Log.wtf("GooglePlayServicesUtil", "Google Play services missing when getting application info.", e);
                        return 1;
                    }
                }
                if (!applicationInfo.enabled) {
                    return 3;
                }
                return 0;
            }
        } catch (PackageManager.NameNotFoundException unused2) {
            Log.w("GooglePlayServicesUtil", "Google Play services is missing.");
            return 1;
        }
    }

    @DexIgnore
    @Deprecated
    public static int isGooglePlayServicesAvailable(Context context, int i) {
        try {
            context.getResources().getString(be0.common_google_play_services_unknown_issue);
        } catch (Throwable unused) {
            Log.e("GooglePlayServicesUtil", "The Google Play services resources were not found. Check your project configuration to ensure that the resources are included.");
        }
        if (!"com.google.android.gms".equals(context.getPackageName()) && !zzal.get()) {
            int b = am0.b(context);
            if (b != 0) {
                int i2 = GOOGLE_PLAY_SERVICES_VERSION_CODE;
                if (b != i2) {
                    StringBuilder sb = new StringBuilder(320);
                    sb.append("The meta-data tag in your app's AndroidManifest.xml does not have the right value.  Expected ");
                    sb.append(i2);
                    sb.append(" but found ");
                    sb.append(b);
                    sb.append(".  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />");
                    throw new IllegalStateException(sb.toString());
                }
            } else {
                throw new IllegalStateException("A required meta-data tag in your app's AndroidManifest.xml does not exist.  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />");
            }
        }
        return zza(context, !lm0.c(context) && !lm0.d(context), i);
    }
}
