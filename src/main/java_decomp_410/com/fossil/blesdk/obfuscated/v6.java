package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Handler;
import com.fossil.blesdk.obfuscated.o6;
import com.fossil.blesdk.obfuscated.r6;
import com.fossil.blesdk.obfuscated.u7;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class v6 {
    @DexIgnore
    public static /* final */ a7 a;
    @DexIgnore
    public static /* final */ k4<String, Typeface> b; // = new k4<>(16);

    /*
    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            a = new z6();
        } else if (i >= 26) {
            a = new y6();
        } else if (i >= 24 && x6.a()) {
            a = new x6();
        } else if (Build.VERSION.SDK_INT >= 21) {
            a = new w6();
        } else {
            a = new a7();
        }
    }
    */

    @DexIgnore
    public static String a(Resources resources, int i, int i2) {
        return resources.getResourcePackageName(i) + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + i + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + i2;
    }

    @DexIgnore
    public static Typeface b(Resources resources, int i, int i2) {
        return b.b(a(resources, i, i2));
    }

    @DexIgnore
    public static Typeface a(Context context, o6.a aVar, Resources resources, int i, int i2, r6.a aVar2, Handler handler, boolean z) {
        Typeface typeface;
        if (aVar instanceof o6.d) {
            o6.d dVar = (o6.d) aVar;
            boolean z2 = false;
            if (!z ? aVar2 == null : dVar.a() == 0) {
                z2 = true;
            }
            typeface = u7.a(context, dVar.b(), aVar2, handler, z2, z ? dVar.c() : -1, i2);
        } else {
            typeface = a.a(context, (o6.b) aVar, resources, i2);
            if (aVar2 != null) {
                if (typeface != null) {
                    aVar2.a(typeface, handler);
                } else {
                    aVar2.a(-3, handler);
                }
            }
        }
        if (typeface != null) {
            b.a(a(resources, i, i2), typeface);
        }
        return typeface;
    }

    @DexIgnore
    public static Typeface a(Context context, Resources resources, int i, String str, int i2) {
        Typeface a2 = a.a(context, resources, i, str, i2);
        if (a2 != null) {
            b.a(a(resources, i, i2), a2);
        }
        return a2;
    }

    @DexIgnore
    public static Typeface a(Context context, CancellationSignal cancellationSignal, u7.f[] fVarArr, int i) {
        return a.a(context, cancellationSignal, fVarArr, i);
    }
}
