package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.remote.ShortcutApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m52 implements Factory<ShortcutApiService> {
    @DexIgnore
    public /* final */ n42 a;
    @DexIgnore
    public /* final */ Provider<xo2> b;
    @DexIgnore
    public /* final */ Provider<bp2> c;

    @DexIgnore
    public m52(n42 n42, Provider<xo2> provider, Provider<bp2> provider2) {
        this.a = n42;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static m52 a(n42 n42, Provider<xo2> provider, Provider<bp2> provider2) {
        return new m52(n42, provider, provider2);
    }

    @DexIgnore
    public static ShortcutApiService b(n42 n42, Provider<xo2> provider, Provider<bp2> provider2) {
        return a(n42, provider.get(), provider2.get());
    }

    @DexIgnore
    public static ShortcutApiService a(n42 n42, xo2 xo2, bp2 bp2) {
        ShortcutApiService d = n42.d(xo2, bp2);
        n44.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }

    @DexIgnore
    public ShortcutApiService get() {
        return b(this.a, this.b, this.c);
    }
}
