package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.maps.MapView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vf1 extends te1 {
    @DexIgnore
    public /* final */ /* synthetic */ de1 e;

    @DexIgnore
    public vf1(MapView.a aVar, de1 de1) {
        this.e = de1;
    }

    @DexIgnore
    public final void a(je1 je1) throws RemoteException {
        this.e.a(new be1(je1));
    }
}
