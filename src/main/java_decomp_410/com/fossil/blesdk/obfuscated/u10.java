package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u10 extends GattOperationResult {
    @DexIgnore
    public /* final */ Peripheral.BondState b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u10(GattOperationResult.GattResult gattResult, Peripheral.BondState bondState, Peripheral.BondState bondState2) {
        super(gattResult);
        kd4.b(gattResult, "gattResult");
        kd4.b(bondState, "previousBondState");
        kd4.b(bondState2, "newBondState");
        this.b = bondState2;
    }

    @DexIgnore
    public final Peripheral.BondState b() {
        return this.b;
    }
}
