package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.p1;
import com.google.android.material.internal.NavigationMenuItemView;
import com.google.android.material.internal.NavigationMenuView;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class os1 implements p1 {
    @DexIgnore
    public NavigationMenuView e;
    @DexIgnore
    public LinearLayout f;
    @DexIgnore
    public p1.a g;
    @DexIgnore
    public h1 h;
    @DexIgnore
    public int i;
    @DexIgnore
    public c j;
    @DexIgnore
    public LayoutInflater k;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public ColorStateList n;
    @DexIgnore
    public ColorStateList o;
    @DexIgnore
    public Drawable p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public /* final */ View.OnClickListener u; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            os1.this.b(true);
            k1 itemData = ((NavigationMenuItemView) view).getItemData();
            os1 os1 = os1.this;
            boolean a = os1.h.a((MenuItem) itemData, (p1) os1, 0);
            if (itemData != null && itemData.isCheckable() && a) {
                os1.this.j.a(itemData);
            }
            os1.this.b(false);
            os1.this.a(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends k {
        @DexIgnore
        public b(View view) {
            super(view);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.g<k> {
        @DexIgnore
        public /* final */ ArrayList<e> a; // = new ArrayList<>();
        @DexIgnore
        public k1 b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public c() {
            d();
        }

        @DexIgnore
        /* renamed from: a */
        public void onBindViewHolder(k kVar, int i) {
            int itemViewType = getItemViewType(i);
            if (itemViewType == 0) {
                NavigationMenuItemView navigationMenuItemView = (NavigationMenuItemView) kVar.itemView;
                navigationMenuItemView.setIconTintList(os1.this.o);
                os1 os1 = os1.this;
                if (os1.m) {
                    navigationMenuItemView.setTextAppearance(os1.l);
                }
                ColorStateList colorStateList = os1.this.n;
                if (colorStateList != null) {
                    navigationMenuItemView.setTextColor(colorStateList);
                }
                Drawable drawable = os1.this.p;
                f9.a((View) navigationMenuItemView, drawable != null ? drawable.getConstantState().newDrawable() : null);
                g gVar = (g) this.a.get(i);
                navigationMenuItemView.setNeedsEmptyIcon(gVar.b);
                navigationMenuItemView.setHorizontalPadding(os1.this.q);
                navigationMenuItemView.setIconPadding(os1.this.r);
                navigationMenuItemView.a(gVar.a(), 0);
            } else if (itemViewType == 1) {
                ((TextView) kVar.itemView).setText(((g) this.a.get(i)).a().getTitle());
            } else if (itemViewType == 2) {
                f fVar = (f) this.a.get(i);
                kVar.itemView.setPadding(0, fVar.b(), 0, fVar.a());
            }
        }

        @DexIgnore
        public Bundle b() {
            Bundle bundle = new Bundle();
            k1 k1Var = this.b;
            if (k1Var != null) {
                bundle.putInt("android:menu:checked", k1Var.getItemId());
            }
            SparseArray sparseArray = new SparseArray();
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                e eVar = this.a.get(i);
                if (eVar instanceof g) {
                    k1 a2 = ((g) eVar).a();
                    View actionView = a2 != null ? a2.getActionView() : null;
                    if (actionView != null) {
                        qs1 qs1 = new qs1();
                        actionView.saveHierarchyState(qs1);
                        sparseArray.put(a2.getItemId(), qs1);
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:action_views", sparseArray);
            return bundle;
        }

        @DexIgnore
        public k1 c() {
            return this.b;
        }

        @DexIgnore
        public final void d() {
            if (!this.c) {
                this.c = true;
                this.a.clear();
                this.a.add(new d());
                int size = os1.this.h.n().size();
                int i = -1;
                boolean z = false;
                int i2 = 0;
                for (int i3 = 0; i3 < size; i3++) {
                    k1 k1Var = os1.this.h.n().get(i3);
                    if (k1Var.isChecked()) {
                        a(k1Var);
                    }
                    if (k1Var.isCheckable()) {
                        k1Var.c(false);
                    }
                    if (k1Var.hasSubMenu()) {
                        SubMenu subMenu = k1Var.getSubMenu();
                        if (subMenu.hasVisibleItems()) {
                            if (i3 != 0) {
                                this.a.add(new f(os1.this.t, 0));
                            }
                            this.a.add(new g(k1Var));
                            int size2 = this.a.size();
                            int size3 = subMenu.size();
                            boolean z2 = false;
                            for (int i4 = 0; i4 < size3; i4++) {
                                k1 k1Var2 = (k1) subMenu.getItem(i4);
                                if (k1Var2.isVisible()) {
                                    if (!z2 && k1Var2.getIcon() != null) {
                                        z2 = true;
                                    }
                                    if (k1Var2.isCheckable()) {
                                        k1Var2.c(false);
                                    }
                                    if (k1Var.isChecked()) {
                                        a(k1Var);
                                    }
                                    this.a.add(new g(k1Var2));
                                }
                            }
                            if (z2) {
                                a(size2, this.a.size());
                            }
                        }
                    } else {
                        int groupId = k1Var.getGroupId();
                        if (groupId != i) {
                            i2 = this.a.size();
                            boolean z3 = k1Var.getIcon() != null;
                            if (i3 != 0) {
                                i2++;
                                ArrayList<e> arrayList = this.a;
                                int i5 = os1.this.t;
                                arrayList.add(new f(i5, i5));
                            }
                            z = z3;
                        } else if (!z && k1Var.getIcon() != null) {
                            a(i2, this.a.size());
                            z = true;
                        }
                        g gVar = new g(k1Var);
                        gVar.b = z;
                        this.a.add(gVar);
                        i = groupId;
                    }
                }
                this.c = false;
            }
        }

        @DexIgnore
        public void e() {
            d();
            notifyDataSetChanged();
        }

        @DexIgnore
        public int getItemCount() {
            return this.a.size();
        }

        @DexIgnore
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        public int getItemViewType(int i) {
            e eVar = this.a.get(i);
            if (eVar instanceof f) {
                return 2;
            }
            if (eVar instanceof d) {
                return 3;
            }
            if (eVar instanceof g) {
                return ((g) eVar).a().hasSubMenu() ? 1 : 0;
            }
            throw new RuntimeException("Unknown item type.");
        }

        @DexIgnore
        public k onCreateViewHolder(ViewGroup viewGroup, int i) {
            if (i == 0) {
                os1 os1 = os1.this;
                return new h(os1.k, viewGroup, os1.u);
            } else if (i == 1) {
                return new j(os1.this.k, viewGroup);
            } else {
                if (i == 2) {
                    return new i(os1.this.k, viewGroup);
                }
                if (i != 3) {
                    return null;
                }
                return new b(os1.this.f);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void onViewRecycled(k kVar) {
            if (kVar instanceof h) {
                ((NavigationMenuItemView) kVar.itemView).d();
            }
        }

        @DexIgnore
        public final void a(int i, int i2) {
            while (i < i2) {
                ((g) this.a.get(i)).b = true;
                i++;
            }
        }

        @DexIgnore
        public void a(k1 k1Var) {
            if (this.b != k1Var && k1Var.isCheckable()) {
                k1 k1Var2 = this.b;
                if (k1Var2 != null) {
                    k1Var2.setChecked(false);
                }
                this.b = k1Var;
                k1Var.setChecked(true);
            }
        }

        @DexIgnore
        public void a(Bundle bundle) {
            int i = bundle.getInt("android:menu:checked", 0);
            if (i != 0) {
                this.c = true;
                int size = this.a.size();
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    }
                    e eVar = this.a.get(i2);
                    if (eVar instanceof g) {
                        k1 a2 = ((g) eVar).a();
                        if (a2 != null && a2.getItemId() == i) {
                            a(a2);
                            break;
                        }
                    }
                    i2++;
                }
                this.c = false;
                d();
            }
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:action_views");
            if (sparseParcelableArray != null) {
                int size2 = this.a.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    e eVar2 = this.a.get(i3);
                    if (eVar2 instanceof g) {
                        k1 a3 = ((g) eVar2).a();
                        if (a3 != null) {
                            View actionView = a3.getActionView();
                            if (actionView != null) {
                                qs1 qs1 = (qs1) sparseParcelableArray.get(a3.getItemId());
                                if (qs1 != null) {
                                    actionView.restoreHierarchyState(qs1);
                                }
                            }
                        }
                    }
                }
            }
        }

        @DexIgnore
        public void a(boolean z) {
            this.c = z;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements e {
    }

    @DexIgnore
    public interface e {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements e {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public f(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        @DexIgnore
        public int a() {
            return this.b;
        }

        @DexIgnore
        public int b() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g implements e {
        @DexIgnore
        public /* final */ k1 a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public g(k1 k1Var) {
            this.a = k1Var;
        }

        @DexIgnore
        public k1 a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h extends k {
        @DexIgnore
        public h(LayoutInflater layoutInflater, ViewGroup viewGroup, View.OnClickListener onClickListener) {
            super(layoutInflater.inflate(zq1.design_navigation_item, viewGroup, false));
            this.itemView.setOnClickListener(onClickListener);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i extends k {
        @DexIgnore
        public i(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(zq1.design_navigation_item_separator, viewGroup, false));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class j extends k {
        @DexIgnore
        public j(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(zq1.design_navigation_item_subheader, viewGroup, false));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class k extends RecyclerView.ViewHolder {
        @DexIgnore
        public k(View view) {
            super(view);
        }
    }

    @DexIgnore
    public void a(Context context, h1 h1Var) {
        this.k = LayoutInflater.from(context);
        this.h = h1Var;
        this.t = context.getResources().getDimensionPixelOffset(vq1.design_navigation_separator_vertical_padding);
    }

    @DexIgnore
    public boolean a() {
        return false;
    }

    @DexIgnore
    public boolean a(h1 h1Var, k1 k1Var) {
        return false;
    }

    @DexIgnore
    public boolean a(v1 v1Var) {
        return false;
    }

    @DexIgnore
    public void b(int i2) {
        this.i = i2;
    }

    @DexIgnore
    public boolean b(h1 h1Var, k1 k1Var) {
        return false;
    }

    @DexIgnore
    public k1 c() {
        return this.j.c();
    }

    @DexIgnore
    public int d() {
        return this.f.getChildCount();
    }

    @DexIgnore
    public void e(int i2) {
        this.l = i2;
        this.m = true;
        a(false);
    }

    @DexIgnore
    public int f() {
        return this.q;
    }

    @DexIgnore
    public int g() {
        return this.r;
    }

    @DexIgnore
    public int getId() {
        return this.i;
    }

    @DexIgnore
    public ColorStateList h() {
        return this.n;
    }

    @DexIgnore
    public ColorStateList i() {
        return this.o;
    }

    @DexIgnore
    public Parcelable b() {
        Bundle bundle = new Bundle();
        if (this.e != null) {
            SparseArray sparseArray = new SparseArray();
            this.e.saveHierarchyState(sparseArray);
            bundle.putSparseParcelableArray("android:menu:list", sparseArray);
        }
        c cVar = this.j;
        if (cVar != null) {
            bundle.putBundle("android:menu:adapter", cVar.b());
        }
        if (this.f != null) {
            SparseArray sparseArray2 = new SparseArray();
            this.f.saveHierarchyState(sparseArray2);
            bundle.putSparseParcelableArray("android:menu:header", sparseArray2);
        }
        return bundle;
    }

    @DexIgnore
    public void c(int i2) {
        this.q = i2;
        a(false);
    }

    @DexIgnore
    public void d(int i2) {
        this.r = i2;
        a(false);
    }

    @DexIgnore
    public Drawable e() {
        return this.p;
    }

    @DexIgnore
    public q1 a(ViewGroup viewGroup) {
        if (this.e == null) {
            this.e = (NavigationMenuView) this.k.inflate(zq1.design_navigation_menu, viewGroup, false);
            if (this.j == null) {
                this.j = new c();
            }
            this.f = (LinearLayout) this.k.inflate(zq1.design_navigation_item_header, this.e, false);
            this.e.setAdapter(this.j);
        }
        return this.e;
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        this.n = colorStateList;
        a(false);
    }

    @DexIgnore
    public void a(boolean z) {
        c cVar = this.j;
        if (cVar != null) {
            cVar.e();
        }
    }

    @DexIgnore
    public void b(boolean z) {
        c cVar = this.j;
        if (cVar != null) {
            cVar.a(z);
        }
    }

    @DexIgnore
    public void a(h1 h1Var, boolean z) {
        p1.a aVar = this.g;
        if (aVar != null) {
            aVar.a(h1Var, z);
        }
    }

    @DexIgnore
    public void a(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
            if (sparseParcelableArray != null) {
                this.e.restoreHierarchyState(sparseParcelableArray);
            }
            Bundle bundle2 = bundle.getBundle("android:menu:adapter");
            if (bundle2 != null) {
                this.j.a(bundle2);
            }
            SparseArray sparseParcelableArray2 = bundle.getSparseParcelableArray("android:menu:header");
            if (sparseParcelableArray2 != null) {
                this.f.restoreHierarchyState(sparseParcelableArray2);
            }
        }
    }

    @DexIgnore
    public void a(k1 k1Var) {
        this.j.a(k1Var);
    }

    @DexIgnore
    public View a(int i2) {
        View inflate = this.k.inflate(i2, this.f, false);
        a(inflate);
        return inflate;
    }

    @DexIgnore
    public void a(View view) {
        this.f.addView(view);
        NavigationMenuView navigationMenuView = this.e;
        navigationMenuView.setPadding(0, 0, 0, navigationMenuView.getPaddingBottom());
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        this.o = colorStateList;
        a(false);
    }

    @DexIgnore
    public void a(Drawable drawable) {
        this.p = drawable;
        a(false);
    }

    @DexIgnore
    public void a(n9 n9Var) {
        int e2 = n9Var.e();
        if (this.s != e2) {
            this.s = e2;
            if (this.f.getChildCount() == 0) {
                NavigationMenuView navigationMenuView = this.e;
                navigationMenuView.setPadding(0, this.s, 0, navigationMenuView.getPaddingBottom());
            }
        }
        f9.a((View) this.f, n9Var);
    }
}
