package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.view.MotionEvent;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.FacebookWebFallbackDialog;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class me extends RecyclerView.l implements RecyclerView.p {
    @DexIgnore
    public static /* final */ int[] D; // = {16842919};
    @DexIgnore
    public static /* final */ int[] E; // = new int[0];
    @DexIgnore
    public int A; // = 0;
    @DexIgnore
    public /* final */ Runnable B; // = new a();
    @DexIgnore
    public /* final */ RecyclerView.q C; // = new b();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ StateListDrawable c;
    @DexIgnore
    public /* final */ Drawable d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ StateListDrawable g;
    @DexIgnore
    public /* final */ Drawable h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public float m;
    @DexIgnore
    public int n;
    @DexIgnore
    public int o;
    @DexIgnore
    public float p;
    @DexIgnore
    public int q; // = 0;
    @DexIgnore
    public int r; // = 0;
    @DexIgnore
    public RecyclerView s;
    @DexIgnore
    public boolean t; // = false;
    @DexIgnore
    public boolean u; // = false;
    @DexIgnore
    public int v; // = 0;
    @DexIgnore
    public int w; // = 0;
    @DexIgnore
    public /* final */ int[] x; // = new int[2];
    @DexIgnore
    public /* final */ int[] y; // = new int[2];
    @DexIgnore
    public /* final */ ValueAnimator z; // = ValueAnimator.ofFloat(new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f});

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            me.this.a(500);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends RecyclerView.q {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            me.this.a(recyclerView.computeHorizontalScrollOffset(), recyclerView.computeVerticalScrollOffset());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends AnimatorListenerAdapter {
        @DexIgnore
        public boolean a; // = false;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            this.a = true;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            if (this.a) {
                this.a = false;
            } else if (((Float) me.this.z.getAnimatedValue()).floatValue() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                me meVar = me.this;
                meVar.A = 0;
                meVar.c(0);
            } else {
                me meVar2 = me.this;
                meVar2.A = 2;
                meVar2.f();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            int floatValue = (int) (((Float) valueAnimator.getAnimatedValue()).floatValue() * 255.0f);
            me.this.c.setAlpha(floatValue);
            me.this.d.setAlpha(floatValue);
            me.this.f();
        }
    }

    @DexIgnore
    public me(RecyclerView recyclerView, StateListDrawable stateListDrawable, Drawable drawable, StateListDrawable stateListDrawable2, Drawable drawable2, int i2, int i3, int i4) {
        this.c = stateListDrawable;
        this.d = drawable;
        this.g = stateListDrawable2;
        this.h = drawable2;
        this.e = Math.max(i2, stateListDrawable.getIntrinsicWidth());
        this.f = Math.max(i2, drawable.getIntrinsicWidth());
        this.i = Math.max(i2, stateListDrawable2.getIntrinsicWidth());
        this.j = Math.max(i2, drawable2.getIntrinsicWidth());
        this.a = i3;
        this.b = i4;
        this.c.setAlpha(255);
        this.d.setAlpha(255);
        this.z.addListener(new c());
        this.z.addUpdateListener(new d());
        a(recyclerView);
    }

    @DexIgnore
    public void a(RecyclerView recyclerView) {
        RecyclerView recyclerView2 = this.s;
        if (recyclerView2 != recyclerView) {
            if (recyclerView2 != null) {
                b();
            }
            this.s = recyclerView;
            if (this.s != null) {
                g();
            }
        }
    }

    @DexIgnore
    public void a(boolean z2) {
    }

    @DexIgnore
    public final void b() {
        this.s.b((RecyclerView.l) this);
        this.s.b((RecyclerView.p) this);
        this.s.b(this.C);
        a();
    }

    @DexIgnore
    public void c(int i2) {
        if (i2 == 2 && this.v != 2) {
            this.c.setState(D);
            a();
        }
        if (i2 == 0) {
            f();
        } else {
            h();
        }
        if (this.v == 2 && i2 != 2) {
            this.c.setState(E);
            b(1200);
        } else if (i2 == 1) {
            b((int) FacebookWebFallbackDialog.OS_BACK_BUTTON_RESPONSE_TIMEOUT_MILLISECONDS);
        }
        this.v = i2;
    }

    @DexIgnore
    public final int[] d() {
        int[] iArr = this.x;
        int i2 = this.b;
        iArr[0] = i2;
        iArr[1] = this.r - i2;
        return iArr;
    }

    @DexIgnore
    public final boolean e() {
        return f9.k(this.s) == 1;
    }

    @DexIgnore
    public void f() {
        this.s.invalidate();
    }

    @DexIgnore
    public final void g() {
        this.s.a((RecyclerView.l) this);
        this.s.a((RecyclerView.p) this);
        this.s.a(this.C);
    }

    @DexIgnore
    public void h() {
        int i2 = this.A;
        if (i2 != 0) {
            if (i2 == 3) {
                this.z.cancel();
            } else {
                return;
            }
        }
        this.A = 1;
        ValueAnimator valueAnimator = this.z;
        valueAnimator.setFloatValues(new float[]{((Float) valueAnimator.getAnimatedValue()).floatValue(), 1.0f});
        this.z.setDuration(500);
        this.z.setStartDelay(0);
        this.z.start();
    }

    @DexIgnore
    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        if (this.q != this.s.getWidth() || this.r != this.s.getHeight()) {
            this.q = this.s.getWidth();
            this.r = this.s.getHeight();
            c(0);
        } else if (this.A != 0) {
            if (this.t) {
                b(canvas);
            }
            if (this.u) {
                a(canvas);
            }
        }
    }

    @DexIgnore
    public final void b(int i2) {
        a();
        this.s.postDelayed(this.B, (long) i2);
    }

    @DexIgnore
    public void a(int i2) {
        int i3 = this.A;
        if (i3 == 1) {
            this.z.cancel();
        } else if (i3 != 2) {
            return;
        }
        this.A = 3;
        ValueAnimator valueAnimator = this.z;
        valueAnimator.setFloatValues(new float[]{((Float) valueAnimator.getAnimatedValue()).floatValue(), 0.0f});
        this.z.setDuration((long) i2);
        this.z.start();
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        int i2 = this.q;
        int i3 = this.e;
        int i4 = i2 - i3;
        int i5 = this.l;
        int i6 = this.k;
        int i7 = i5 - (i6 / 2);
        this.c.setBounds(0, 0, i3, i6);
        this.d.setBounds(0, 0, this.f, this.r);
        if (e()) {
            this.d.draw(canvas);
            canvas.translate((float) this.e, (float) i7);
            canvas.scale(-1.0f, 1.0f);
            this.c.draw(canvas);
            canvas.scale(1.0f, 1.0f);
            canvas.translate((float) (-this.e), (float) (-i7));
            return;
        }
        canvas.translate((float) i4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.d.draw(canvas);
        canvas.translate(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i7);
        this.c.draw(canvas);
        canvas.translate((float) (-i4), (float) (-i7));
    }

    @DexIgnore
    public final int[] c() {
        int[] iArr = this.y;
        int i2 = this.b;
        iArr[0] = i2;
        iArr[1] = this.q - i2;
        return iArr;
    }

    @DexIgnore
    public final void a() {
        this.s.removeCallbacks(this.B);
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        int i2 = this.r;
        int i3 = this.i;
        int i4 = i2 - i3;
        int i5 = this.o;
        int i6 = this.n;
        int i7 = i5 - (i6 / 2);
        this.g.setBounds(0, 0, i6, i3);
        this.h.setBounds(0, 0, this.q, this.j);
        canvas.translate(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i4);
        this.h.draw(canvas);
        canvas.translate((float) i7, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.g.draw(canvas);
        canvas.translate((float) (-i7), (float) (-i4));
    }

    @DexIgnore
    public void a(int i2, int i3) {
        int computeVerticalScrollRange = this.s.computeVerticalScrollRange();
        int i4 = this.r;
        this.t = computeVerticalScrollRange - i4 > 0 && i4 >= this.a;
        int computeHorizontalScrollRange = this.s.computeHorizontalScrollRange();
        int i5 = this.q;
        this.u = computeHorizontalScrollRange - i5 > 0 && i5 >= this.a;
        if (this.t || this.u) {
            if (this.t) {
                float f2 = (float) i4;
                this.l = (int) ((f2 * (((float) i3) + (f2 / 2.0f))) / ((float) computeVerticalScrollRange));
                this.k = Math.min(i4, (i4 * i4) / computeVerticalScrollRange);
            }
            if (this.u) {
                float f3 = (float) i5;
                this.o = (int) ((f3 * (((float) i2) + (f3 / 2.0f))) / ((float) computeHorizontalScrollRange));
                this.n = Math.min(i5, (i5 * i5) / computeHorizontalScrollRange);
            }
            int i6 = this.v;
            if (i6 == 0 || i6 == 1) {
                c(1);
            }
        } else if (this.v != 0) {
            c(0);
        }
    }

    @DexIgnore
    public boolean b(RecyclerView recyclerView, MotionEvent motionEvent) {
        int i2 = this.v;
        if (i2 == 1) {
            boolean b2 = b(motionEvent.getX(), motionEvent.getY());
            boolean a2 = a(motionEvent.getX(), motionEvent.getY());
            if (motionEvent.getAction() != 0) {
                return false;
            }
            if (!b2 && !a2) {
                return false;
            }
            if (a2) {
                this.w = 1;
                this.p = (float) ((int) motionEvent.getX());
            } else if (b2) {
                this.w = 2;
                this.m = (float) ((int) motionEvent.getY());
            }
            c(2);
        } else if (i2 == 2) {
            return true;
        } else {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void b(float f2) {
        int[] d2 = d();
        float max = Math.max((float) d2[0], Math.min((float) d2[1], f2));
        if (Math.abs(((float) this.l) - max) >= 2.0f) {
            int a2 = a(this.m, max, d2, this.s.computeVerticalScrollRange(), this.s.computeVerticalScrollOffset(), this.r);
            if (a2 != 0) {
                this.s.scrollBy(0, a2);
            }
            this.m = max;
        }
    }

    @DexIgnore
    public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
        if (this.v != 0) {
            if (motionEvent.getAction() == 0) {
                boolean b2 = b(motionEvent.getX(), motionEvent.getY());
                boolean a2 = a(motionEvent.getX(), motionEvent.getY());
                if (b2 || a2) {
                    if (a2) {
                        this.w = 1;
                        this.p = (float) ((int) motionEvent.getX());
                    } else if (b2) {
                        this.w = 2;
                        this.m = (float) ((int) motionEvent.getY());
                    }
                    c(2);
                }
            } else if (motionEvent.getAction() == 1 && this.v == 2) {
                this.m = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                this.p = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                c(1);
                this.w = 0;
            } else if (motionEvent.getAction() == 2 && this.v == 2) {
                h();
                if (this.w == 1) {
                    a(motionEvent.getX());
                }
                if (this.w == 2) {
                    b(motionEvent.getY());
                }
            }
        }
    }

    @DexIgnore
    public boolean b(float f2, float f3) {
        if (!e() ? f2 >= ((float) (this.q - this.e)) : f2 <= ((float) (this.e / 2))) {
            int i2 = this.l;
            int i3 = this.k;
            return f3 >= ((float) (i2 - (i3 / 2))) && f3 <= ((float) (i2 + (i3 / 2)));
        }
    }

    @DexIgnore
    public final void a(float f2) {
        int[] c2 = c();
        float max = Math.max((float) c2[0], Math.min((float) c2[1], f2));
        if (Math.abs(((float) this.o) - max) >= 2.0f) {
            int a2 = a(this.p, max, c2, this.s.computeHorizontalScrollRange(), this.s.computeHorizontalScrollOffset(), this.q);
            if (a2 != 0) {
                this.s.scrollBy(a2, 0);
            }
            this.p = max;
        }
    }

    @DexIgnore
    public final int a(float f2, float f3, int[] iArr, int i2, int i3, int i4) {
        int i5 = iArr[1] - iArr[0];
        if (i5 == 0) {
            return 0;
        }
        int i6 = i2 - i4;
        int i7 = (int) (((f3 - f2) / ((float) i5)) * ((float) i6));
        int i8 = i3 + i7;
        if (i8 >= i6 || i8 < 0) {
            return 0;
        }
        return i7;
    }

    @DexIgnore
    public boolean a(float f2, float f3) {
        if (f3 >= ((float) (this.r - this.i))) {
            int i2 = this.o;
            int i3 = this.n;
            return f2 >= ((float) (i2 - (i3 / 2))) && f2 <= ((float) (i2 + (i3 / 2)));
        }
    }
}
