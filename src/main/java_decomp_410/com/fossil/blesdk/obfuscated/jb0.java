package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jb0 {
    @DexIgnore
    public static /* final */ jb0 a; // = new jb0();

    @DexIgnore
    public final boolean a(Context context, String[] strArr) {
        kd4.b(context, "context");
        kd4.b(strArr, "permissions");
        boolean z = true;
        for (String a2 : strArr) {
            z = z && k6.a(context, a2) == 0;
        }
        return z;
    }
}
