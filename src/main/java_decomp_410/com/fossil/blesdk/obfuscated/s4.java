package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface s4<T> {
    @DexIgnore
    T a();

    @DexIgnore
    void a(T[] tArr, int i);

    @DexIgnore
    boolean a(T t);
}
