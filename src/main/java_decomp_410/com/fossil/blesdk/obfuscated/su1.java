package com.fossil.blesdk.obfuscated;

import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class su1 {
    @DexIgnore
    public static /* final */ Object[] a; // = new Object[0];

    @DexIgnore
    public static <T> T[] a(T[] tArr, int i) {
        T[] c = c(tArr, i);
        System.arraycopy(tArr, 0, c, 0, Math.min(tArr.length, i));
        return c;
    }

    @DexIgnore
    public static Object[] b(Object[] objArr, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            a(objArr[i2], i2);
        }
        return objArr;
    }

    @DexIgnore
    public static <T> T[] c(T[] tArr, int i) {
        return uu1.a(tArr, i);
    }

    @DexIgnore
    public static <T> T[] a(Collection<?> collection, T[] tArr) {
        int size = collection.size();
        if (tArr.length < size) {
            tArr = c(tArr, size);
        }
        a((Iterable<?>) collection, (Object[]) tArr);
        if (tArr.length > size) {
            tArr[size] = null;
        }
        return tArr;
    }

    @DexIgnore
    public static Object[] a(Iterable<?> iterable, Object[] objArr) {
        int i = 0;
        for (Object obj : iterable) {
            objArr[i] = obj;
            i++;
        }
        return objArr;
    }

    @DexIgnore
    public static Object[] a(Object... objArr) {
        b(objArr, objArr.length);
        return objArr;
    }

    @DexIgnore
    public static Object a(Object obj, int i) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException("at index " + i);
    }
}
