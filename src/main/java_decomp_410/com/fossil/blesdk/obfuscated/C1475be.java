package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@java.lang.Deprecated
/* renamed from: com.fossil.blesdk.obfuscated.be */
public class C1475be {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.view.ViewGroup f3706a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.be$a")
    @java.lang.Deprecated
    /* renamed from: com.fossil.blesdk.obfuscated.be$a */
    public static class C1476a {

        @DexIgnore
        /* renamed from: a */
        public float f3707a; // = -1.0f;

        @DexIgnore
        /* renamed from: b */
        public float f3708b; // = -1.0f;

        @DexIgnore
        /* renamed from: c */
        public float f3709c; // = -1.0f;

        @DexIgnore
        /* renamed from: d */
        public float f3710d; // = -1.0f;

        @DexIgnore
        /* renamed from: e */
        public float f3711e; // = -1.0f;

        @DexIgnore
        /* renamed from: f */
        public float f3712f; // = -1.0f;

        @DexIgnore
        /* renamed from: g */
        public float f3713g; // = -1.0f;

        @DexIgnore
        /* renamed from: h */
        public float f3714h; // = -1.0f;

        @DexIgnore
        /* renamed from: i */
        public float f3715i;

        @DexIgnore
        /* renamed from: j */
        public /* final */ com.fossil.blesdk.obfuscated.C1475be.C1478c f3716j; // = new com.fossil.blesdk.obfuscated.C1475be.C1478c(0, 0);

        @DexIgnore
        /* renamed from: a */
        public void mo9152a(android.view.ViewGroup.LayoutParams layoutParams, int i, int i2) {
            com.fossil.blesdk.obfuscated.C1475be.C1478c cVar = this.f3716j;
            cVar.width = layoutParams.width;
            cVar.height = layoutParams.height;
            boolean z = false;
            boolean z2 = (cVar.f3718b || cVar.width == 0) && this.f3707a < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            com.fossil.blesdk.obfuscated.C1475be.C1478c cVar2 = this.f3716j;
            if ((cVar2.f3717a || cVar2.height == 0) && this.f3708b < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z = true;
            }
            float f = this.f3707a;
            if (f >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                layoutParams.width = java.lang.Math.round(((float) i) * f);
            }
            float f2 = this.f3708b;
            if (f2 >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                layoutParams.height = java.lang.Math.round(((float) i2) * f2);
            }
            float f3 = this.f3715i;
            if (f3 >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                if (z2) {
                    layoutParams.width = java.lang.Math.round(((float) layoutParams.height) * f3);
                    this.f3716j.f3718b = true;
                }
                if (z) {
                    layoutParams.height = java.lang.Math.round(((float) layoutParams.width) / this.f3715i);
                    this.f3716j.f3717a = true;
                }
            }
        }

        @DexIgnore
        public java.lang.String toString() {
            return java.lang.String.format("PercentLayoutInformation width: %f height %f, margins (%f, %f,  %f, %f, %f, %f)", new java.lang.Object[]{java.lang.Float.valueOf(this.f3707a), java.lang.Float.valueOf(this.f3708b), java.lang.Float.valueOf(this.f3709c), java.lang.Float.valueOf(this.f3710d), java.lang.Float.valueOf(this.f3711e), java.lang.Float.valueOf(this.f3712f), java.lang.Float.valueOf(this.f3713g), java.lang.Float.valueOf(this.f3714h)});
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9150a(android.view.View view, android.view.ViewGroup.MarginLayoutParams marginLayoutParams, int i, int i2) {
            mo9152a(marginLayoutParams, i, i2);
            com.fossil.blesdk.obfuscated.C1475be.C1478c cVar = this.f3716j;
            cVar.leftMargin = marginLayoutParams.leftMargin;
            cVar.topMargin = marginLayoutParams.topMargin;
            cVar.rightMargin = marginLayoutParams.rightMargin;
            cVar.bottomMargin = marginLayoutParams.bottomMargin;
            com.fossil.blesdk.obfuscated.C2865s8.m13558c(cVar, com.fossil.blesdk.obfuscated.C2865s8.m13556b(marginLayoutParams));
            com.fossil.blesdk.obfuscated.C2865s8.m13557b(this.f3716j, com.fossil.blesdk.obfuscated.C2865s8.m13554a(marginLayoutParams));
            float f = this.f3709c;
            if (f >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.leftMargin = java.lang.Math.round(((float) i) * f);
            }
            float f2 = this.f3710d;
            if (f2 >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.topMargin = java.lang.Math.round(((float) i2) * f2);
            }
            float f3 = this.f3711e;
            if (f3 >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.rightMargin = java.lang.Math.round(((float) i) * f3);
            }
            float f4 = this.f3712f;
            if (f4 >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.bottomMargin = java.lang.Math.round(((float) i2) * f4);
            }
            boolean z = false;
            float f5 = this.f3713g;
            if (f5 >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                com.fossil.blesdk.obfuscated.C2865s8.m13558c(marginLayoutParams, java.lang.Math.round(((float) i) * f5));
                z = true;
            }
            float f6 = this.f3714h;
            if (f6 >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                com.fossil.blesdk.obfuscated.C2865s8.m13557b(marginLayoutParams, java.lang.Math.round(((float) i) * f6));
                z = true;
            }
            if (z && view != null) {
                com.fossil.blesdk.obfuscated.C2865s8.m13555a(marginLayoutParams, com.fossil.blesdk.obfuscated.C1776f9.m6845k(view));
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9153a(android.view.ViewGroup.MarginLayoutParams marginLayoutParams) {
            mo9151a((android.view.ViewGroup.LayoutParams) marginLayoutParams);
            com.fossil.blesdk.obfuscated.C1475be.C1478c cVar = this.f3716j;
            marginLayoutParams.leftMargin = cVar.leftMargin;
            marginLayoutParams.topMargin = cVar.topMargin;
            marginLayoutParams.rightMargin = cVar.rightMargin;
            marginLayoutParams.bottomMargin = cVar.bottomMargin;
            com.fossil.blesdk.obfuscated.C2865s8.m13558c(marginLayoutParams, com.fossil.blesdk.obfuscated.C2865s8.m13556b(cVar));
            com.fossil.blesdk.obfuscated.C2865s8.m13557b(marginLayoutParams, com.fossil.blesdk.obfuscated.C2865s8.m13554a(this.f3716j));
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9151a(android.view.ViewGroup.LayoutParams layoutParams) {
            com.fossil.blesdk.obfuscated.C1475be.C1478c cVar = this.f3716j;
            if (!cVar.f3718b) {
                layoutParams.width = cVar.width;
            }
            com.fossil.blesdk.obfuscated.C1475be.C1478c cVar2 = this.f3716j;
            if (!cVar2.f3717a) {
                layoutParams.height = cVar2.height;
            }
            com.fossil.blesdk.obfuscated.C1475be.C1478c cVar3 = this.f3716j;
            cVar3.f3718b = false;
            cVar3.f3717a = false;
        }
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: com.fossil.blesdk.obfuscated.be$b */
    public interface C1477b {
        @DexIgnore
        /* renamed from: a */
        com.fossil.blesdk.obfuscated.C1475be.C1476a mo2339a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.be$c")
    /* renamed from: com.fossil.blesdk.obfuscated.be$c */
    public static class C1478c extends android.view.ViewGroup.MarginLayoutParams {

        @DexIgnore
        /* renamed from: a */
        public boolean f3717a;

        @DexIgnore
        /* renamed from: b */
        public boolean f3718b;

        @DexIgnore
        public C1478c(int i, int i2) {
            super(i, i2);
        }
    }

    @DexIgnore
    public C1475be(android.view.ViewGroup viewGroup) {
        if (viewGroup != null) {
            this.f3706a = viewGroup;
            return;
        }
        throw new java.lang.IllegalArgumentException("host must be non-null");
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4919a(android.view.ViewGroup.LayoutParams layoutParams, android.content.res.TypedArray typedArray, int i, int i2) {
        layoutParams.width = typedArray.getLayoutDimension(i, 0);
        layoutParams.height = typedArray.getLayoutDimension(i2, 0);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9149b() {
        int childCount = this.f3706a.getChildCount();
        for (int i = 0; i < childCount; i++) {
            android.view.ViewGroup.LayoutParams layoutParams = this.f3706a.getChildAt(i).getLayoutParams();
            if (layoutParams instanceof com.fossil.blesdk.obfuscated.C1475be.C1477b) {
                com.fossil.blesdk.obfuscated.C1475be.C1476a a = ((com.fossil.blesdk.obfuscated.C1475be.C1477b) layoutParams).mo2339a();
                if (a != null) {
                    if (layoutParams instanceof android.view.ViewGroup.MarginLayoutParams) {
                        a.mo9153a((android.view.ViewGroup.MarginLayoutParams) layoutParams);
                    } else {
                        a.mo9151a(layoutParams);
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9147a(int i, int i2) {
        int size = (android.view.View.MeasureSpec.getSize(i) - this.f3706a.getPaddingLeft()) - this.f3706a.getPaddingRight();
        int size2 = (android.view.View.MeasureSpec.getSize(i2) - this.f3706a.getPaddingTop()) - this.f3706a.getPaddingBottom();
        int childCount = this.f3706a.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            android.view.View childAt = this.f3706a.getChildAt(i3);
            android.view.ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if (layoutParams instanceof com.fossil.blesdk.obfuscated.C1475be.C1477b) {
                com.fossil.blesdk.obfuscated.C1475be.C1476a a = ((com.fossil.blesdk.obfuscated.C1475be.C1477b) layoutParams).mo2339a();
                if (a != null) {
                    if (layoutParams instanceof android.view.ViewGroup.MarginLayoutParams) {
                        a.mo9150a(childAt, (android.view.ViewGroup.MarginLayoutParams) layoutParams, size, size2);
                    } else {
                        a.mo9152a(layoutParams, size, size2);
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m4921b(android.view.View view, com.fossil.blesdk.obfuscated.C1475be.C1476a aVar) {
        return (view.getMeasuredWidthAndState() & -16777216) == 16777216 && aVar.f3707a >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && aVar.f3716j.width == -2;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1475be.C1476a m4918a(android.content.Context context, android.util.AttributeSet attributeSet) {
        com.fossil.blesdk.obfuscated.C1475be.C1476a aVar;
        android.content.res.TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, com.fossil.blesdk.obfuscated.C1414ae.PercentLayout_Layout);
        float fraction = obtainStyledAttributes.getFraction(com.fossil.blesdk.obfuscated.C1414ae.PercentLayout_Layout_layout_widthPercent, 1, 1, -1.0f);
        if (fraction != -1.0f) {
            aVar = new com.fossil.blesdk.obfuscated.C1475be.C1476a();
            aVar.f3707a = fraction;
        } else {
            aVar = null;
        }
        float fraction2 = obtainStyledAttributes.getFraction(com.fossil.blesdk.obfuscated.C1414ae.PercentLayout_Layout_layout_heightPercent, 1, 1, -1.0f);
        if (fraction2 != -1.0f) {
            if (aVar == null) {
                aVar = new com.fossil.blesdk.obfuscated.C1475be.C1476a();
            }
            aVar.f3708b = fraction2;
        }
        float fraction3 = obtainStyledAttributes.getFraction(com.fossil.blesdk.obfuscated.C1414ae.PercentLayout_Layout_layout_marginPercent, 1, 1, -1.0f);
        if (fraction3 != -1.0f) {
            if (aVar == null) {
                aVar = new com.fossil.blesdk.obfuscated.C1475be.C1476a();
            }
            aVar.f3709c = fraction3;
            aVar.f3710d = fraction3;
            aVar.f3711e = fraction3;
            aVar.f3712f = fraction3;
        }
        float fraction4 = obtainStyledAttributes.getFraction(com.fossil.blesdk.obfuscated.C1414ae.PercentLayout_Layout_layout_marginLeftPercent, 1, 1, -1.0f);
        if (fraction4 != -1.0f) {
            if (aVar == null) {
                aVar = new com.fossil.blesdk.obfuscated.C1475be.C1476a();
            }
            aVar.f3709c = fraction4;
        }
        float fraction5 = obtainStyledAttributes.getFraction(com.fossil.blesdk.obfuscated.C1414ae.PercentLayout_Layout_layout_marginTopPercent, 1, 1, -1.0f);
        if (fraction5 != -1.0f) {
            if (aVar == null) {
                aVar = new com.fossil.blesdk.obfuscated.C1475be.C1476a();
            }
            aVar.f3710d = fraction5;
        }
        float fraction6 = obtainStyledAttributes.getFraction(com.fossil.blesdk.obfuscated.C1414ae.PercentLayout_Layout_layout_marginRightPercent, 1, 1, -1.0f);
        if (fraction6 != -1.0f) {
            if (aVar == null) {
                aVar = new com.fossil.blesdk.obfuscated.C1475be.C1476a();
            }
            aVar.f3711e = fraction6;
        }
        float fraction7 = obtainStyledAttributes.getFraction(com.fossil.blesdk.obfuscated.C1414ae.PercentLayout_Layout_layout_marginBottomPercent, 1, 1, -1.0f);
        if (fraction7 != -1.0f) {
            if (aVar == null) {
                aVar = new com.fossil.blesdk.obfuscated.C1475be.C1476a();
            }
            aVar.f3712f = fraction7;
        }
        float fraction8 = obtainStyledAttributes.getFraction(com.fossil.blesdk.obfuscated.C1414ae.PercentLayout_Layout_layout_marginStartPercent, 1, 1, -1.0f);
        if (fraction8 != -1.0f) {
            if (aVar == null) {
                aVar = new com.fossil.blesdk.obfuscated.C1475be.C1476a();
            }
            aVar.f3713g = fraction8;
        }
        float fraction9 = obtainStyledAttributes.getFraction(com.fossil.blesdk.obfuscated.C1414ae.PercentLayout_Layout_layout_marginEndPercent, 1, 1, -1.0f);
        if (fraction9 != -1.0f) {
            if (aVar == null) {
                aVar = new com.fossil.blesdk.obfuscated.C1475be.C1476a();
            }
            aVar.f3714h = fraction9;
        }
        float fraction10 = obtainStyledAttributes.getFraction(com.fossil.blesdk.obfuscated.C1414ae.PercentLayout_Layout_layout_aspectRatio, 1, 1, -1.0f);
        if (fraction10 != -1.0f) {
            if (aVar == null) {
                aVar = new com.fossil.blesdk.obfuscated.C1475be.C1476a();
            }
            aVar.f3715i = fraction10;
        }
        obtainStyledAttributes.recycle();
        return aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9148a() {
        int childCount = this.f3706a.getChildCount();
        boolean z = false;
        for (int i = 0; i < childCount; i++) {
            android.view.View childAt = this.f3706a.getChildAt(i);
            android.view.ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if (layoutParams instanceof com.fossil.blesdk.obfuscated.C1475be.C1477b) {
                com.fossil.blesdk.obfuscated.C1475be.C1476a a = ((com.fossil.blesdk.obfuscated.C1475be.C1477b) layoutParams).mo2339a();
                if (a != null) {
                    if (m4921b(childAt, a)) {
                        layoutParams.width = -2;
                        z = true;
                    }
                    if (m4920a(childAt, a)) {
                        layoutParams.height = -2;
                        z = true;
                    }
                }
            }
        }
        return z;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m4920a(android.view.View view, com.fossil.blesdk.obfuscated.C1475be.C1476a aVar) {
        return (view.getMeasuredHeightAndState() & -16777216) == 16777216 && aVar.f3708b >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && aVar.f3716j.height == -2;
    }
}
