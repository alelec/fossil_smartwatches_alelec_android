package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nc */
public class C2474nc extends androidx.loader.app.LoaderManager {

    @DexIgnore
    /* renamed from: c */
    public static boolean f7702c; // = false;

    @DexIgnore
    /* renamed from: a */
    public /* final */ androidx.lifecycle.LifecycleOwner f7703a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2474nc.C2477c f7704b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.nc$a")
    /* renamed from: com.fossil.blesdk.obfuscated.nc$a */
    public static class C2475a<D> extends androidx.lifecycle.MutableLiveData<D> implements com.fossil.blesdk.obfuscated.C2719qc.C2722c<D> {

        @DexIgnore
        /* renamed from: k */
        public /* final */ int f7705k;

        @DexIgnore
        /* renamed from: l */
        public /* final */ android.os.Bundle f7706l;

        @DexIgnore
        /* renamed from: m */
        public /* final */ com.fossil.blesdk.obfuscated.C2719qc<D> f7707m;

        @DexIgnore
        /* renamed from: n */
        public androidx.lifecycle.LifecycleOwner f7708n;

        @DexIgnore
        /* renamed from: o */
        public com.fossil.blesdk.obfuscated.C2474nc.C2476b<D> f7709o;

        @DexIgnore
        /* renamed from: p */
        public com.fossil.blesdk.obfuscated.C2719qc<D> f7710p;

        @DexIgnore
        public C2475a(int i, android.os.Bundle bundle, com.fossil.blesdk.obfuscated.C2719qc<D> qcVar, com.fossil.blesdk.obfuscated.C2719qc<D> qcVar2) {
            this.f7705k = i;
            this.f7706l = bundle;
            this.f7707m = qcVar;
            this.f7710p = qcVar2;
            this.f7707m.registerListener(i, this);
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2719qc<D> mo13907a(androidx.lifecycle.LifecycleOwner lifecycleOwner, androidx.loader.app.LoaderManager.C0205a<D> aVar) {
            com.fossil.blesdk.obfuscated.C2474nc.C2476b<D> bVar = new com.fossil.blesdk.obfuscated.C2474nc.C2476b<>(this.f7707m, aVar);
            mo2277a(lifecycleOwner, bVar);
            com.fossil.blesdk.obfuscated.C2474nc.C2476b<D> bVar2 = this.f7709o;
            if (bVar2 != null) {
                mo2283b(bVar2);
            }
            this.f7708n = lifecycleOwner;
            this.f7709o = bVar;
            return this.f7707m;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo2283b(com.fossil.blesdk.obfuscated.C1548cc<? super D> ccVar) {
            super.mo2283b(ccVar);
            this.f7708n = null;
            this.f7709o = null;
        }

        @DexIgnore
        /* renamed from: d */
        public void mo2286d() {
            if (com.fossil.blesdk.obfuscated.C2474nc.f7702c) {
                android.util.Log.v("LoaderManager", "  Starting: " + this);
            }
            this.f7707m.startLoading();
        }

        @DexIgnore
        /* renamed from: e */
        public void mo2287e() {
            if (com.fossil.blesdk.obfuscated.C2474nc.f7702c) {
                android.util.Log.v("LoaderManager", "  Stopping: " + this);
            }
            this.f7707m.stopLoading();
        }

        @DexIgnore
        /* renamed from: f */
        public com.fossil.blesdk.obfuscated.C2719qc<D> mo13911f() {
            return this.f7707m;
        }

        @DexIgnore
        /* renamed from: g */
        public void mo13912g() {
            androidx.lifecycle.LifecycleOwner lifecycleOwner = this.f7708n;
            com.fossil.blesdk.obfuscated.C2474nc.C2476b<D> bVar = this.f7709o;
            if (lifecycleOwner != null && bVar != null) {
                super.mo2283b(bVar);
                mo2277a(lifecycleOwner, bVar);
            }
        }

        @DexIgnore
        public java.lang.String toString() {
            java.lang.StringBuilder sb = new java.lang.StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(java.lang.Integer.toHexString(java.lang.System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.f7705k);
            sb.append(" : ");
            com.fossil.blesdk.obfuscated.C1539c8.m5321a(this.f7707m, sb);
            sb.append("}}");
            return sb.toString();
        }

        @DexIgnore
        /* renamed from: b */
        public void mo2284b(D d) {
            super.mo2284b(d);
            com.fossil.blesdk.obfuscated.C2719qc<D> qcVar = this.f7710p;
            if (qcVar != null) {
                qcVar.reset();
                this.f7710p = null;
            }
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2719qc<D> mo13908a(boolean z) {
            if (com.fossil.blesdk.obfuscated.C2474nc.f7702c) {
                android.util.Log.v("LoaderManager", "  Destroying: " + this);
            }
            this.f7707m.cancelLoad();
            this.f7707m.abandon();
            com.fossil.blesdk.obfuscated.C2474nc.C2476b<D> bVar = this.f7709o;
            if (bVar != null) {
                mo2283b(bVar);
                if (z) {
                    bVar.mo13916b();
                }
            }
            this.f7707m.unregisterListener(this);
            if ((bVar == null || bVar.mo13915a()) && !z) {
                return this.f7707m;
            }
            this.f7707m.reset();
            return this.f7710p;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13909a(com.fossil.blesdk.obfuscated.C2719qc<D> qcVar, D d) {
            if (com.fossil.blesdk.obfuscated.C2474nc.f7702c) {
                android.util.Log.v("LoaderManager", "onLoadComplete: " + this);
            }
            if (android.os.Looper.myLooper() == android.os.Looper.getMainLooper()) {
                mo2284b(d);
                return;
            }
            if (com.fossil.blesdk.obfuscated.C2474nc.f7702c) {
                android.util.Log.w("LoaderManager", "onLoadComplete was incorrectly called on a background thread");
            }
            mo2280a(d);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13910a(java.lang.String str, java.io.FileDescriptor fileDescriptor, java.io.PrintWriter printWriter, java.lang.String[] strArr) {
            printWriter.print(str);
            printWriter.print("mId=");
            printWriter.print(this.f7705k);
            printWriter.print(" mArgs=");
            printWriter.println(this.f7706l);
            printWriter.print(str);
            printWriter.print("mLoader=");
            printWriter.println(this.f7707m);
            com.fossil.blesdk.obfuscated.C2719qc<D> qcVar = this.f7707m;
            qcVar.dump(str + "  ", fileDescriptor, printWriter, strArr);
            if (this.f7709o != null) {
                printWriter.print(str);
                printWriter.print("mCallbacks=");
                printWriter.println(this.f7709o);
                com.fossil.blesdk.obfuscated.C2474nc.C2476b<D> bVar = this.f7709o;
                bVar.mo13914a(str + "  ", printWriter);
            }
            printWriter.print(str);
            printWriter.print("mData=");
            printWriter.println(mo13911f().dataToString(mo2275a()));
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.println(mo2285c());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.nc$b")
    /* renamed from: com.fossil.blesdk.obfuscated.nc$b */
    public static class C2476b<D> implements com.fossil.blesdk.obfuscated.C1548cc<D> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2719qc<D> f7711a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ androidx.loader.app.LoaderManager.C0205a<D> f7712b;

        @DexIgnore
        /* renamed from: c */
        public boolean f7713c; // = false;

        @DexIgnore
        public C2476b(com.fossil.blesdk.obfuscated.C2719qc<D> qcVar, androidx.loader.app.LoaderManager.C0205a<D> aVar) {
            this.f7711a = qcVar;
            this.f7712b = aVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8689a(D d) {
            if (com.fossil.blesdk.obfuscated.C2474nc.f7702c) {
                android.util.Log.v("LoaderManager", "  onLoadFinished in " + this.f7711a + ": " + this.f7711a.dataToString(d));
            }
            this.f7712b.mo2309a(this.f7711a, d);
            this.f7713c = true;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo13916b() {
            if (this.f7713c) {
                if (com.fossil.blesdk.obfuscated.C2474nc.f7702c) {
                    android.util.Log.v("LoaderManager", "  Resetting: " + this.f7711a);
                }
                this.f7712b.mo2308a(this.f7711a);
            }
        }

        @DexIgnore
        public java.lang.String toString() {
            return this.f7712b.toString();
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo13915a() {
            return this.f7713c;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13914a(java.lang.String str, java.io.PrintWriter printWriter) {
            printWriter.print(str);
            printWriter.print("mDeliveredData=");
            printWriter.println(this.f7713c);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.nc$c")
    /* renamed from: com.fossil.blesdk.obfuscated.nc$c */
    public static class C2477c extends com.fossil.blesdk.obfuscated.C2010ic {

        @DexIgnore
        /* renamed from: e */
        public static /* final */ com.fossil.blesdk.obfuscated.C2200kc.C2202b f7714e; // = new com.fossil.blesdk.obfuscated.C2474nc.C2477c.C2478a();

        @DexIgnore
        /* renamed from: c */
        public androidx.collection.SparseArrayCompat<com.fossil.blesdk.obfuscated.C2474nc.C2475a> f7715c; // = new androidx.collection.SparseArrayCompat<>();

        @DexIgnore
        /* renamed from: d */
        public boolean f7716d; // = false;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.nc$c$a")
        /* renamed from: com.fossil.blesdk.obfuscated.nc$c$a */
        public static class C2478a implements com.fossil.blesdk.obfuscated.C2200kc.C2202b {
            @DexIgnore
            /* renamed from: a */
            public <T extends com.fossil.blesdk.obfuscated.C2010ic> T mo12712a(java.lang.Class<T> cls) {
                return new com.fossil.blesdk.obfuscated.C2474nc.C2477c();
            }
        }

        @DexIgnore
        /* renamed from: a */
        public static com.fossil.blesdk.obfuscated.C2474nc.C2477c m11181a(androidx.lifecycle.ViewModelStore viewModelStore) {
            return (com.fossil.blesdk.obfuscated.C2474nc.C2477c) new com.fossil.blesdk.obfuscated.C2200kc(viewModelStore, f7714e).mo12710a(com.fossil.blesdk.obfuscated.C2474nc.C2477c.class);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo13921b(int i) {
            this.f7715c.mo1250e(i);
        }

        @DexIgnore
        /* renamed from: c */
        public void mo13922c() {
            this.f7716d = false;
        }

        @DexIgnore
        /* renamed from: d */
        public boolean mo13923d() {
            return this.f7716d;
        }

        @DexIgnore
        /* renamed from: e */
        public void mo13924e() {
            int c = this.f7715c.mo1245c();
            for (int i = 0; i < c; i++) {
                this.f7715c.mo1251f(i).mo13912g();
            }
        }

        @DexIgnore
        /* renamed from: f */
        public void mo13925f() {
            this.f7716d = true;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13919a(int i, com.fossil.blesdk.obfuscated.C2474nc.C2475a aVar) {
            this.f7715c.mo1247c(i, aVar);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo11903b() {
            super.mo11903b();
            int c = this.f7715c.mo1245c();
            for (int i = 0; i < c; i++) {
                this.f7715c.mo1251f(i).mo13908a(true);
            }
            this.f7715c.mo1239a();
        }

        @DexIgnore
        /* renamed from: a */
        public <D> com.fossil.blesdk.obfuscated.C2474nc.C2475a<D> mo13918a(int i) {
            return this.f7715c.mo1242b(i);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13920a(java.lang.String str, java.io.FileDescriptor fileDescriptor, java.io.PrintWriter printWriter, java.lang.String[] strArr) {
            if (this.f7715c.mo1245c() > 0) {
                printWriter.print(str);
                printWriter.println("Loaders:");
                java.lang.String str2 = str + "    ";
                for (int i = 0; i < this.f7715c.mo1245c(); i++) {
                    com.fossil.blesdk.obfuscated.C2474nc.C2475a f = this.f7715c.mo1251f(i);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(this.f7715c.mo1249d(i));
                    printWriter.print(": ");
                    printWriter.println(f.toString());
                    f.mo13910a(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
    }

    @DexIgnore
    public C2474nc(androidx.lifecycle.LifecycleOwner lifecycleOwner, androidx.lifecycle.ViewModelStore viewModelStore) {
        this.f7703a = lifecycleOwner;
        this.f7704b = com.fossil.blesdk.obfuscated.C2474nc.C2477c.m11181a(viewModelStore);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public final <D> com.fossil.blesdk.obfuscated.C2719qc<D> mo13905a(int i, android.os.Bundle bundle, androidx.loader.app.LoaderManager.C0205a<D> aVar, com.fossil.blesdk.obfuscated.C2719qc<D> qcVar) {
        try {
            this.f7704b.mo13925f();
            com.fossil.blesdk.obfuscated.C2719qc<D> a = aVar.mo2307a(i, bundle);
            if (a != null) {
                if (a.getClass().isMemberClass()) {
                    if (!java.lang.reflect.Modifier.isStatic(a.getClass().getModifiers())) {
                        throw new java.lang.IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + a);
                    }
                }
                com.fossil.blesdk.obfuscated.C2474nc.C2475a aVar2 = new com.fossil.blesdk.obfuscated.C2474nc.C2475a(i, bundle, a, qcVar);
                if (f7702c) {
                    android.util.Log.v("LoaderManager", "  Created new loader " + aVar2);
                }
                this.f7704b.mo13919a(i, aVar2);
                this.f7704b.mo13922c();
                return aVar2.mo13907a(this.f7703a, aVar);
            }
            throw new java.lang.IllegalArgumentException("Object returned from onCreateLoader must not be null");
        } catch (Throwable th) {
            this.f7704b.mo13922c();
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public <D> com.fossil.blesdk.obfuscated.C2719qc<D> mo2306b(int i, android.os.Bundle bundle, androidx.loader.app.LoaderManager.C0205a<D> aVar) {
        if (this.f7704b.mo13923d()) {
            throw new java.lang.IllegalStateException("Called while creating a loader");
        } else if (android.os.Looper.getMainLooper() == android.os.Looper.myLooper()) {
            if (f7702c) {
                android.util.Log.v("LoaderManager", "restartLoader in " + this + ": args=" + bundle);
            }
            com.fossil.blesdk.obfuscated.C2474nc.C2475a a = this.f7704b.mo13918a(i);
            com.fossil.blesdk.obfuscated.C2719qc qcVar = null;
            if (a != null) {
                qcVar = a.mo13908a(false);
            }
            return mo13905a(i, bundle, aVar, qcVar);
        } else {
            throw new java.lang.IllegalStateException("restartLoader must be called on the main thread");
        }
    }

    @DexIgnore
    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(java.lang.Integer.toHexString(java.lang.System.identityHashCode(this)));
        sb.append(" in ");
        com.fossil.blesdk.obfuscated.C1539c8.m5321a(this.f7703a, sb);
        sb.append("}}");
        return sb.toString();
    }

    @DexIgnore
    /* renamed from: a */
    public <D> com.fossil.blesdk.obfuscated.C2719qc<D> mo2302a(int i, android.os.Bundle bundle, androidx.loader.app.LoaderManager.C0205a<D> aVar) {
        if (this.f7704b.mo13923d()) {
            throw new java.lang.IllegalStateException("Called while creating a loader");
        } else if (android.os.Looper.getMainLooper() == android.os.Looper.myLooper()) {
            com.fossil.blesdk.obfuscated.C2474nc.C2475a a = this.f7704b.mo13918a(i);
            if (f7702c) {
                android.util.Log.v("LoaderManager", "initLoader in " + this + ": args=" + bundle);
            }
            if (a == null) {
                return mo13905a(i, bundle, aVar, (com.fossil.blesdk.obfuscated.C2719qc) null);
            }
            if (f7702c) {
                android.util.Log.v("LoaderManager", "  Re-using existing loader " + a);
            }
            return a.mo13907a(this.f7703a, aVar);
        } else {
            throw new java.lang.IllegalStateException("initLoader must be called on the main thread");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo2304a(int i) {
        if (this.f7704b.mo13923d()) {
            throw new java.lang.IllegalStateException("Called while creating a loader");
        } else if (android.os.Looper.getMainLooper() == android.os.Looper.myLooper()) {
            if (f7702c) {
                android.util.Log.v("LoaderManager", "destroyLoader in " + this + " of " + i);
            }
            com.fossil.blesdk.obfuscated.C2474nc.C2475a a = this.f7704b.mo13918a(i);
            if (a != null) {
                a.mo13908a(true);
                this.f7704b.mo13921b(i);
            }
        } else {
            throw new java.lang.IllegalStateException("destroyLoader must be called on the main thread");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo2303a() {
        this.f7704b.mo13924e();
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: a */
    public void mo2305a(java.lang.String str, java.io.FileDescriptor fileDescriptor, java.io.PrintWriter printWriter, java.lang.String[] strArr) {
        this.f7704b.mo13920a(str, fileDescriptor, printWriter, strArr);
    }
}
