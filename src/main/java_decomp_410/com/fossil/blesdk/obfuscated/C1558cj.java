package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.cj */
public abstract class C1558cj {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.cj$a")
    /* renamed from: com.fossil.blesdk.obfuscated.cj$a */
    public static class C1559a extends com.fossil.blesdk.obfuscated.C1558cj {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1488bj mo9535a(java.lang.String str) {
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1558cj m5428a() {
        return new com.fossil.blesdk.obfuscated.C1558cj.C1559a();
    }

    @DexIgnore
    /* renamed from: a */
    public abstract com.fossil.blesdk.obfuscated.C1488bj mo9535a(java.lang.String str);

    @DexIgnore
    /* renamed from: b */
    public final com.fossil.blesdk.obfuscated.C1488bj mo9536b(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C1488bj a = mo9535a(str);
        return a == null ? com.fossil.blesdk.obfuscated.C1488bj.m5008a(str) : a;
    }
}
