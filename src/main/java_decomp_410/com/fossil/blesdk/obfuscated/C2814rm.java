package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rm */
public class C2814rm extends java.lang.Thread {

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.concurrent.BlockingQueue<com.android.volley.Request<?>> f9038e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C2748qm f9039f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C2334lm f9040g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C3124vm f9041h;

    @DexIgnore
    /* renamed from: i */
    public volatile boolean f9042i; // = false;

    @DexIgnore
    public C2814rm(java.util.concurrent.BlockingQueue<com.android.volley.Request<?>> blockingQueue, com.fossil.blesdk.obfuscated.C2748qm qmVar, com.fossil.blesdk.obfuscated.C2334lm lmVar, com.fossil.blesdk.obfuscated.C3124vm vmVar) {
        this.f9038e = blockingQueue;
        this.f9039f = qmVar;
        this.f9040g = lmVar;
        this.f9041h = vmVar;
    }

    @DexIgnore
    @android.annotation.TargetApi(14)
    /* renamed from: a */
    public final void mo15633a(com.android.volley.Request<?> request) {
        if (android.os.Build.VERSION.SDK_INT >= 14) {
            android.net.TrafficStats.setThreadStatsTag(request.getTrafficStatsTag());
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15635b() {
        this.f9042i = true;
        interrupt();
    }

    @DexIgnore
    public void run() {
        android.os.Process.setThreadPriority(10);
        while (true) {
            try {
                mo15632a();
            } catch (java.lang.InterruptedException unused) {
                if (this.f9042i) {
                    java.lang.Thread.currentThread().interrupt();
                    return;
                }
                com.fossil.blesdk.obfuscated.C3296xm.m16422c("Ignoring spurious interrupt of NetworkDispatcher thread; use quit() to terminate it", new java.lang.Object[0]);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo15632a() throws java.lang.InterruptedException {
        mo15636b(this.f9038e.take());
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15636b(com.android.volley.Request<?> request) {
        long elapsedRealtime = android.os.SystemClock.elapsedRealtime();
        try {
            request.addMarker("network-queue-take");
            if (request.isCanceled()) {
                request.finish("network-discard-cancelled");
                request.notifyListenerResponseNotUsable();
                return;
            }
            mo15633a(request);
            com.fossil.blesdk.obfuscated.C2897sm a = this.f9039f.mo8843a(request);
            request.addMarker("network-http-complete");
            if (!a.f9394e || !request.hasHadResponseDelivered()) {
                com.fossil.blesdk.obfuscated.C3047um<?> parseNetworkResponse = request.parseNetworkResponse(a);
                request.addMarker("network-parse-complete");
                if (request.shouldCache() && parseNetworkResponse.f10028b != null) {
                    this.f9040g.mo9572a(request.getCacheKey(), parseNetworkResponse.f10028b);
                    request.addMarker("network-cache-written");
                }
                request.markDelivered();
                this.f9041h.mo14456a(request, parseNetworkResponse);
                request.notifyListenerResponseReceived(parseNetworkResponse);
                return;
            }
            request.finish("not-modified");
            request.notifyListenerResponseNotUsable();
        } catch (com.android.volley.VolleyError e) {
            e.setNetworkTimeMs(android.os.SystemClock.elapsedRealtime() - elapsedRealtime);
            mo15634a(request, e);
            request.notifyListenerResponseNotUsable();
        } catch (java.lang.Exception e2) {
            com.fossil.blesdk.obfuscated.C3296xm.m16420a(e2, "Unhandled exception %s", e2.toString());
            com.android.volley.VolleyError volleyError = new com.android.volley.VolleyError((java.lang.Throwable) e2);
            volleyError.setNetworkTimeMs(android.os.SystemClock.elapsedRealtime() - elapsedRealtime);
            this.f9041h.mo14455a(request, volleyError);
            request.notifyListenerResponseNotUsable();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo15634a(com.android.volley.Request<?> request, com.android.volley.VolleyError volleyError) {
        this.f9041h.mo14455a(request, request.parseNetworkError(volleyError));
    }
}
