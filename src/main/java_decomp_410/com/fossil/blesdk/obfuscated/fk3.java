package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fossil.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.login.AppleAuthorizationActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fk3 extends zr2 implements ek3 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public dk3 j;
    @DexIgnore
    public tr3<mf2> k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return fk3.m;
        }

        @DexIgnore
        public final fk3 b() {
            return new fk3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ fk3 f;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ mf2 e;

            @DexIgnore
            public a(mf2 mf2) {
                this.e = mf2;
            }

            @DexIgnore
            public final void run() {
                FlexibleTextView flexibleTextView = this.e.C;
                kd4.a((Object) flexibleTextView, "it.tvHaveAccount");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = this.e.D;
                kd4.a((Object) flexibleTextView2, "it.tvSignup");
                flexibleTextView2.setVisibility(0);
            }
        }

        @DexIgnore
        public b(View view, fk3 fk3) {
            this.e = view;
            this.f = fk3;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.e.getWindowVisibleDisplayFrame(rect);
            int height = this.e.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                mf2 mf2 = (mf2) fk3.a(this.f).a();
                if (mf2 != null) {
                    try {
                        FlexibleTextView flexibleTextView = mf2.C;
                        kd4.a((Object) flexibleTextView, "it.tvHaveAccount");
                        flexibleTextView.setVisibility(8);
                        FlexibleTextView flexibleTextView2 = mf2.D;
                        kd4.a((Object) flexibleTextView2, "it.tvSignup");
                        flexibleTextView2.setVisibility(8);
                        qa4 qa4 = qa4.a;
                    } catch (Exception e2) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = fk3.n.a();
                        local.d(a2, "onCreateView - e=" + e2);
                        qa4 qa42 = qa4.a;
                    }
                }
            } else {
                mf2 mf22 = (mf2) fk3.a(this.f).a();
                if (mf22 != null) {
                    try {
                        kd4.a((Object) mf22, "it");
                        Boolean.valueOf(mf22.d().postDelayed(new a(mf22), 100));
                    } catch (Exception e3) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a3 = fk3.n.a();
                        local2.d(a3, "onCreateView - e=" + e3);
                        qa4 qa43 = qa4.a;
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fk3 e;

        @DexIgnore
        public c(fk3 fk3) {
            this.e = fk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().m();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fk3 e;

        @DexIgnore
        public d(fk3 fk3) {
            this.e = fk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                SignUpActivity.a aVar = SignUpActivity.G;
                kd4.a((Object) activity, "it");
                aVar.a(activity);
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fk3 e;

        @DexIgnore
        public e(fk3 fk3) {
            this.e = fk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextView.OnEditorActionListener {
        @DexIgnore
        public /* final */ /* synthetic */ fk3 a;

        @DexIgnore
        public f(fk3 fk3) {
            this.a = fk3;
        }

        @DexIgnore
        public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((i & 255) != 6) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d(fk3.n.a(), "Password DONE key, trigger login flow");
            this.a.U0();
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mf2 e;
        @DexIgnore
        public /* final */ /* synthetic */ fk3 f;

        @DexIgnore
        public g(mf2 mf2, fk3 fk3) {
            this.e = mf2;
            this.f = fk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            TextInputLayout textInputLayout = this.e.u;
            kd4.a((Object) textInputLayout, "binding.inputPassword");
            textInputLayout.setErrorEnabled(false);
            TextInputLayout textInputLayout2 = this.e.t;
            kd4.a((Object) textInputLayout2, "binding.inputEmail");
            textInputLayout2.setErrorEnabled(false);
            this.f.U0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ fk3 e;

        @DexIgnore
        public h(fk3 fk3) {
            this.e = fk3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            this.e.T0().a(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ fk3 e;

        @DexIgnore
        public i(fk3 fk3) {
            this.e = fk3;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.e.T0().a(z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ fk3 e;

        @DexIgnore
        public j(fk3 fk3) {
            this.e = fk3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            this.e.T0().b(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fk3 e;

        @DexIgnore
        public k(fk3 fk3) {
            this.e = fk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fk3 e;

        @DexIgnore
        public l(fk3 fk3) {
            this.e = fk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fk3 e;

        @DexIgnore
        public m(fk3 fk3) {
            this.e = fk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fk3 e;

        @DexIgnore
        public n(fk3 fk3) {
            this.e = fk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fk3 e;

        @DexIgnore
        public o(fk3 fk3) {
            this.e = fk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().l();
        }
    }

    /*
    static {
        String simpleName = fk3.class.getSimpleName();
        if (simpleName != null) {
            kd4.a((Object) simpleName, "LoginFragment::class.java.simpleName!!");
            m = simpleName;
            return;
        }
        kd4.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ tr3 a(fk3 fk3) {
        tr3<mf2> tr3 = fk3.k;
        if (tr3 != null) {
            return tr3;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void F(boolean z) {
        tr3<mf2> tr3 = this.k;
        if (tr3 != null) {
            mf2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                FloatingActionButton floatingActionButton = a2.A;
                kd4.a((Object) floatingActionButton, "it.ivWeibo");
                floatingActionButton.setVisibility(0);
                FloatingActionButton floatingActionButton2 = a2.z;
                kd4.a((Object) floatingActionButton2, "it.ivWechat");
                floatingActionButton2.setVisibility(0);
                ImageView imageView = a2.y;
                kd4.a((Object) imageView, "it.ivGoogle");
                imageView.setVisibility(8);
                ImageView imageView2 = a2.x;
                kd4.a((Object) imageView2, "it.ivFacebook");
                imageView2.setVisibility(8);
                return;
            }
            FloatingActionButton floatingActionButton3 = a2.A;
            kd4.a((Object) floatingActionButton3, "it.ivWeibo");
            floatingActionButton3.setVisibility(8);
            FloatingActionButton floatingActionButton4 = a2.z;
            kd4.a((Object) floatingActionButton4, "it.ivWechat");
            floatingActionButton4.setVisibility(8);
            ImageView imageView3 = a2.y;
            kd4.a((Object) imageView3, "it.ivGoogle");
            imageView3.setVisibility(0);
            ImageView imageView4 = a2.x;
            kd4.a((Object) imageView4, "it.ivFacebook");
            imageView4.setVisibility(0);
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void J(String str) {
        kd4.b(str, "email");
        Context context = getContext();
        if (context != null) {
            ForgotPasswordActivity.a aVar = ForgotPasswordActivity.C;
            kd4.a((Object) context, "it");
            aVar.a(context, str);
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return m;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final dk3 T0() {
        dk3 dk3 = this.j;
        if (dk3 != null) {
            return dk3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void U0() {
        tr3<mf2> tr3 = this.k;
        if (tr3 != null) {
            mf2 a2 = tr3.a();
            if (a2 != null) {
                dk3 dk3 = this.j;
                if (dk3 != null) {
                    TextInputEditText textInputEditText = a2.r;
                    kd4.a((Object) textInputEditText, "etEmail");
                    String valueOf = String.valueOf(textInputEditText.getText());
                    TextInputEditText textInputEditText2 = a2.s;
                    kd4.a((Object) textInputEditText2, "etPassword");
                    dk3.a(valueOf, String.valueOf(textInputEditText2.getText()));
                    return;
                }
                kd4.d("mPresenter");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void Y() {
        tr3<mf2> tr3 = this.k;
        if (tr3 != null) {
            mf2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.q;
                if (flexibleButton != null) {
                    flexibleButton.setEnabled(false);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void c(SignUpSocialAuth signUpSocialAuth) {
        kd4.b(signUpSocialAuth, "socialAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.C;
            kd4.a((Object) activity, "it");
            aVar.a((Context) activity, signUpSocialAuth);
            activity.finish();
        }
    }

    @DexIgnore
    public void h() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.C;
            kd4.a((Object) activity, "it");
            aVar.a(activity);
            activity.finish();
        }
    }

    @DexIgnore
    public void i() {
        a();
    }

    @DexIgnore
    public void i0() {
        if (isActive()) {
            tr3<mf2> tr3 = this.k;
            if (tr3 != null) {
                mf2 a2 = tr3.a();
                if (a2 != null) {
                    TextInputLayout textInputLayout = a2.t;
                    kd4.a((Object) textInputLayout, "it.inputEmail");
                    textInputLayout.setError(" ");
                    TextInputLayout textInputLayout2 = a2.u;
                    kd4.a((Object) textInputLayout2, "it.inputPassword");
                    textInputLayout2.setError(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Login_WrongEmailPasswordError_Text__WrongEmailOrPassword));
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void k() {
        String string = getString(R.string.Onboarding_Login_LoggingIn_Text__PleaseWait);
        kd4.a((Object) string, "getString(R.string.Onboa\u2026ggingIn_Text__PleaseWait)");
        S(string);
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 3535) {
            return;
        }
        if (i3 != -1) {
            FLogger.INSTANCE.getLocal().e(m, "Something went wrong with Apple login");
        } else if (intent != null) {
            SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) intent.getParcelableExtra("USER_INFO_EXTRA");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            local.d(str, "Apple Auth Info = " + signUpSocialAuth);
            dk3 dk3 = this.j;
            if (dk3 != null) {
                kd4.a((Object) signUpSocialAuth, "authCode");
                dk3.a(signUpSocialAuth);
                return;
            }
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        mf2 mf2 = (mf2) qa.a(layoutInflater, R.layout.fragment_signin, viewGroup, false, O0());
        kd4.a((Object) mf2, "binding");
        View d2 = mf2.d();
        kd4.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, this));
        this.k = new tr3<>(this, mf2);
        tr3<mf2> tr3 = this.k;
        if (tr3 != null) {
            mf2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        dk3 dk3 = this.j;
        if (dk3 != null) {
            dk3.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        dk3 dk3 = this.j;
        if (dk3 != null) {
            dk3.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<mf2> tr3 = this.k;
        if (tr3 != null) {
            mf2 a2 = tr3.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new g(a2, this));
                a2.r.addTextChangedListener(new h(this));
                a2.r.setOnFocusChangeListener(new i(this));
                a2.s.addTextChangedListener(new j(this));
                a2.w.setOnClickListener(new k(this));
                a2.x.setOnClickListener(new l(this));
                a2.y.setOnClickListener(new m(this));
                a2.v.setOnClickListener(new n(this));
                a2.z.setOnClickListener(new o(this));
                a2.A.setOnClickListener(new c(this));
                a2.D.setOnClickListener(new d(this));
                a2.B.setOnClickListener(new e(this));
                a2.s.setOnEditorActionListener(new f(this));
            }
            R("login_view");
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void q0() {
        tr3<mf2> tr3 = this.k;
        if (tr3 != null) {
            mf2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.q;
                if (flexibleButton != null) {
                    flexibleButton.setEnabled(true);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(int i2, String str) {
        kd4.b(str, "errorMessage");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(boolean z, String str) {
        kd4.b(str, "errorMessage");
        if (isActive()) {
            tr3<mf2> tr3 = this.k;
            if (tr3 != null) {
                mf2 a2 = tr3.a();
                if (a2 != null) {
                    TextInputLayout textInputLayout = a2.t;
                    if (textInputLayout != null) {
                        kd4.a((Object) textInputLayout, "it");
                        textInputLayout.setErrorEnabled(z);
                        textInputLayout.setError(str);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(dk3 dk3) {
        kd4.b(dk3, "presenter");
        this.j = dk3;
    }

    @DexIgnore
    public void F() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            AppleAuthorizationActivity.a aVar = AppleAuthorizationActivity.F;
            kd4.a((Object) activity, "it");
            aVar.a(activity, "file:///android_asset/signinwithapple.html");
        }
    }
}
