package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z {
    @DexIgnore
    public static /* final */ int AlertDialog_AppCompat; // = 2131886081;
    @DexIgnore
    public static /* final */ int AlertDialog_AppCompat_Light; // = 2131886082;
    @DexIgnore
    public static /* final */ int Animation_AppCompat_Dialog; // = 2131886086;
    @DexIgnore
    public static /* final */ int Animation_AppCompat_DropDownUp; // = 2131886087;
    @DexIgnore
    public static /* final */ int Animation_AppCompat_Tooltip; // = 2131886088;
    @DexIgnore
    public static /* final */ int Base_AlertDialog_AppCompat; // = 2131886220;
    @DexIgnore
    public static /* final */ int Base_AlertDialog_AppCompat_Light; // = 2131886221;
    @DexIgnore
    public static /* final */ int Base_Animation_AppCompat_Dialog; // = 2131886222;
    @DexIgnore
    public static /* final */ int Base_Animation_AppCompat_DropDownUp; // = 2131886223;
    @DexIgnore
    public static /* final */ int Base_Animation_AppCompat_Tooltip; // = 2131886224;
    @DexIgnore
    public static /* final */ int Base_DialogWindowTitleBackground_AppCompat; // = 2131886227;
    @DexIgnore
    public static /* final */ int Base_DialogWindowTitle_AppCompat; // = 2131886226;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat; // = 2131886228;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Body1; // = 2131886229;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Body2; // = 2131886230;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Button; // = 2131886231;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Caption; // = 2131886232;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display1; // = 2131886233;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display2; // = 2131886234;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display3; // = 2131886235;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display4; // = 2131886236;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Headline; // = 2131886237;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Inverse; // = 2131886238;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Large; // = 2131886239;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Large_Inverse; // = 2131886240;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large; // = 2131886241;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small; // = 2131886242;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Medium; // = 2131886243;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Medium_Inverse; // = 2131886244;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Menu; // = 2131886245;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_SearchResult; // = 2131886246;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_SearchResult_Subtitle; // = 2131886247;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_SearchResult_Title; // = 2131886248;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Small; // = 2131886249;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Small_Inverse; // = 2131886250;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Subhead; // = 2131886251;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Subhead_Inverse; // = 2131886252;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Title; // = 2131886253;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Title_Inverse; // = 2131886254;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Tooltip; // = 2131886255;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Menu; // = 2131886256;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle; // = 2131886257;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse; // = 2131886258;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Title; // = 2131886259;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse; // = 2131886260;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle; // = 2131886261;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionMode_Title; // = 2131886262;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button; // = 2131886263;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button_Borderless_Colored; // = 2131886264;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button_Colored; // = 2131886265;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button_Inverse; // = 2131886266;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_DropDownItem; // = 2131886267;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_PopupMenu_Header; // = 2131886268;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_PopupMenu_Large; // = 2131886269;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_PopupMenu_Small; // = 2131886270;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Switch; // = 2131886271;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_TextView_SpinnerItem; // = 2131886272;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item; // = 2131886273;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle; // = 2131886274;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_Widget_AppCompat_Toolbar_Title; // = 2131886275;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat; // = 2131886307;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_ActionBar; // = 2131886308;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dark; // = 2131886309;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dark_ActionBar; // = 2131886310;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dialog; // = 2131886311;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dialog_Alert; // = 2131886312;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Light; // = 2131886313;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat; // = 2131886276;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_CompactMenu; // = 2131886277;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog; // = 2131886278;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_DialogWhenLarge; // = 2131886282;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog_Alert; // = 2131886279;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog_FixedSize; // = 2131886280;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog_MinWidth; // = 2131886281;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light; // = 2131886283;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_DarkActionBar; // = 2131886284;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog; // = 2131886285;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_DialogWhenLarge; // = 2131886289;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog_Alert; // = 2131886286;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog_FixedSize; // = 2131886287;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog_MinWidth; // = 2131886288;
    @DexIgnore
    public static /* final */ int Base_V21_ThemeOverlay_AppCompat_Dialog; // = 2131886329;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat; // = 2131886325;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat_Dialog; // = 2131886326;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat_Light; // = 2131886327;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat_Light_Dialog; // = 2131886328;
    @DexIgnore
    public static /* final */ int Base_V22_Theme_AppCompat; // = 2131886330;
    @DexIgnore
    public static /* final */ int Base_V22_Theme_AppCompat_Light; // = 2131886331;
    @DexIgnore
    public static /* final */ int Base_V23_Theme_AppCompat; // = 2131886332;
    @DexIgnore
    public static /* final */ int Base_V23_Theme_AppCompat_Light; // = 2131886333;
    @DexIgnore
    public static /* final */ int Base_V26_Theme_AppCompat; // = 2131886334;
    @DexIgnore
    public static /* final */ int Base_V26_Theme_AppCompat_Light; // = 2131886335;
    @DexIgnore
    public static /* final */ int Base_V26_Widget_AppCompat_Toolbar; // = 2131886336;
    @DexIgnore
    public static /* final */ int Base_V28_Theme_AppCompat; // = 2131886337;
    @DexIgnore
    public static /* final */ int Base_V28_Theme_AppCompat_Light; // = 2131886338;
    @DexIgnore
    public static /* final */ int Base_V7_ThemeOverlay_AppCompat_Dialog; // = 2131886343;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat; // = 2131886339;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat_Dialog; // = 2131886340;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat_Light; // = 2131886341;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat_Light_Dialog; // = 2131886342;
    @DexIgnore
    public static /* final */ int Base_V7_Widget_AppCompat_AutoCompleteTextView; // = 2131886344;
    @DexIgnore
    public static /* final */ int Base_V7_Widget_AppCompat_EditText; // = 2131886345;
    @DexIgnore
    public static /* final */ int Base_V7_Widget_AppCompat_Toolbar; // = 2131886346;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar; // = 2131886347;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_Solid; // = 2131886348;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_TabBar; // = 2131886349;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_TabText; // = 2131886350;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_TabView; // = 2131886351;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionButton; // = 2131886352;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionButton_CloseMode; // = 2131886353;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionButton_Overflow; // = 2131886354;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionMode; // = 2131886355;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActivityChooserView; // = 2131886356;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_AutoCompleteTextView; // = 2131886357;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button; // = 2131886358;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ButtonBar; // = 2131886364;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ButtonBar_AlertDialog; // = 2131886365;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Borderless; // = 2131886359;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Borderless_Colored; // = 2131886360;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_ButtonBar_AlertDialog; // = 2131886361;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Colored; // = 2131886362;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Small; // = 2131886363;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_CompoundButton_CheckBox; // = 2131886366;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_CompoundButton_RadioButton; // = 2131886367;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_CompoundButton_Switch; // = 2131886368;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_DrawerArrowToggle; // = 2131886369;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_DrawerArrowToggle_Common; // = 2131886370;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_DropDownItem_Spinner; // = 2131886371;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_EditText; // = 2131886372;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ImageButton; // = 2131886373;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar; // = 2131886374;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_Solid; // = 2131886375;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabBar; // = 2131886376;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabText; // = 2131886377;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse; // = 2131886378;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabView; // = 2131886379;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_PopupMenu; // = 2131886380;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_PopupMenu_Overflow; // = 2131886381;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListMenuView; // = 2131886382;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListPopupWindow; // = 2131886383;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListView; // = 2131886384;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListView_DropDown; // = 2131886385;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListView_Menu; // = 2131886386;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_PopupMenu; // = 2131886387;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_PopupMenu_Overflow; // = 2131886388;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_PopupWindow; // = 2131886389;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ProgressBar; // = 2131886390;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ProgressBar_Horizontal; // = 2131886391;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_RatingBar; // = 2131886392;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_RatingBar_Indicator; // = 2131886393;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_RatingBar_Small; // = 2131886394;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SearchView; // = 2131886395;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SearchView_ActionBar; // = 2131886396;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SeekBar; // = 2131886397;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SeekBar_Discrete; // = 2131886398;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Spinner; // = 2131886399;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Spinner_Underlined; // = 2131886400;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_TextView_SpinnerItem; // = 2131886401;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Toolbar; // = 2131886402;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Toolbar_Button_Navigation; // = 2131886403;
    @DexIgnore
    public static /* final */ int Platform_AppCompat; // = 2131886509;
    @DexIgnore
    public static /* final */ int Platform_AppCompat_Light; // = 2131886510;
    @DexIgnore
    public static /* final */ int Platform_ThemeOverlay_AppCompat; // = 2131886515;
    @DexIgnore
    public static /* final */ int Platform_ThemeOverlay_AppCompat_Dark; // = 2131886516;
    @DexIgnore
    public static /* final */ int Platform_ThemeOverlay_AppCompat_Light; // = 2131886517;
    @DexIgnore
    public static /* final */ int Platform_V21_AppCompat; // = 2131886518;
    @DexIgnore
    public static /* final */ int Platform_V21_AppCompat_Light; // = 2131886519;
    @DexIgnore
    public static /* final */ int Platform_V25_AppCompat; // = 2131886520;
    @DexIgnore
    public static /* final */ int Platform_V25_AppCompat_Light; // = 2131886521;
    @DexIgnore
    public static /* final */ int Platform_Widget_AppCompat_Spinner; // = 2131886522;
    @DexIgnore
    public static /* final */ int RtlOverlay_DialogWindowTitle_AppCompat; // = 2131886531;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_ActionBar_TitleItem; // = 2131886532;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_DialogTitle_Icon; // = 2131886533;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem; // = 2131886534;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup; // = 2131886535;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_Shortcut; // = 2131886536;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_SubmenuArrow; // = 2131886537;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_Text; // = 2131886538;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_Title; // = 2131886539;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_SearchView_MagIcon; // = 2131886545;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown; // = 2131886540;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1; // = 2131886541;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2; // = 2131886542;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Query; // = 2131886543;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Text; // = 2131886544;
    @DexIgnore
    public static /* final */ int RtlUnderlay_Widget_AppCompat_ActionButton; // = 2131886546;
    @DexIgnore
    public static /* final */ int RtlUnderlay_Widget_AppCompat_ActionButton_Overflow; // = 2131886547;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat; // = 2131886558;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Body1; // = 2131886559;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Body2; // = 2131886560;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Button; // = 2131886561;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Caption; // = 2131886562;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display1; // = 2131886563;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display2; // = 2131886564;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display3; // = 2131886565;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display4; // = 2131886566;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Headline; // = 2131886567;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Inverse; // = 2131886568;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Large; // = 2131886569;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Large_Inverse; // = 2131886570;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_SearchResult_Subtitle; // = 2131886571;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_SearchResult_Title; // = 2131886572;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_Widget_PopupMenu_Large; // = 2131886573;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_Widget_PopupMenu_Small; // = 2131886574;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Medium; // = 2131886575;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Medium_Inverse; // = 2131886576;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Menu; // = 2131886577;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_SearchResult_Subtitle; // = 2131886578;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_SearchResult_Title; // = 2131886579;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Small; // = 2131886580;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Small_Inverse; // = 2131886581;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Subhead; // = 2131886582;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Subhead_Inverse; // = 2131886583;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Title; // = 2131886584;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Title_Inverse; // = 2131886585;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Tooltip; // = 2131886586;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Menu; // = 2131886587;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Subtitle; // = 2131886588;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse; // = 2131886589;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Title; // = 2131886590;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse; // = 2131886591;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Subtitle; // = 2131886592;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse; // = 2131886593;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Title; // = 2131886594;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse; // = 2131886595;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button; // = 2131886596;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button_Borderless_Colored; // = 2131886597;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button_Colored; // = 2131886598;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button_Inverse; // = 2131886599;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_DropDownItem; // = 2131886600;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_PopupMenu_Header; // = 2131886601;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_PopupMenu_Large; // = 2131886602;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_PopupMenu_Small; // = 2131886603;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Switch; // = 2131886604;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_TextView_SpinnerItem; // = 2131886605;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification; // = 2131886606;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Info; // = 2131886607;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Line2; // = 2131886609;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Time; // = 2131886612;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Title; // = 2131886614;
    @DexIgnore
    public static /* final */ int TextAppearance_Widget_AppCompat_ExpandedMenu_Item; // = 2131886639;
    @DexIgnore
    public static /* final */ int TextAppearance_Widget_AppCompat_Toolbar_Subtitle; // = 2131886640;
    @DexIgnore
    public static /* final */ int TextAppearance_Widget_AppCompat_Toolbar_Title; // = 2131886641;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat; // = 2131886699;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_ActionBar; // = 2131886700;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dark; // = 2131886701;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dark_ActionBar; // = 2131886702;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dialog; // = 2131886703;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dialog_Alert; // = 2131886704;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Light; // = 2131886705;
    @DexIgnore
    public static /* final */ int Theme_AppCompat; // = 2131886651;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_CompactMenu; // = 2131886652;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight; // = 2131886653;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_DarkActionBar; // = 2131886654;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_Dialog; // = 2131886655;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_DialogWhenLarge; // = 2131886658;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_Dialog_Alert; // = 2131886656;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_Dialog_MinWidth; // = 2131886657;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_NoActionBar; // = 2131886659;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Dialog; // = 2131886660;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DialogWhenLarge; // = 2131886663;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Dialog_Alert; // = 2131886661;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Dialog_MinWidth; // = 2131886662;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light; // = 2131886664;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_DarkActionBar; // = 2131886665;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_Dialog; // = 2131886666;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_DialogWhenLarge; // = 2131886669;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_Dialog_Alert; // = 2131886667;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_Dialog_MinWidth; // = 2131886668;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_NoActionBar; // = 2131886670;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_NoActionBar; // = 2131886671;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar; // = 2131886730;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_Solid; // = 2131886731;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_TabBar; // = 2131886732;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_TabText; // = 2131886733;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_TabView; // = 2131886734;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionButton; // = 2131886735;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionButton_CloseMode; // = 2131886736;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionButton_Overflow; // = 2131886737;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionMode; // = 2131886738;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActivityChooserView; // = 2131886739;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_AutoCompleteTextView; // = 2131886740;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button; // = 2131886741;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ButtonBar; // = 2131886747;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ButtonBar_AlertDialog; // = 2131886748;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Borderless; // = 2131886742;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Borderless_Colored; // = 2131886743;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_ButtonBar_AlertDialog; // = 2131886744;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Colored; // = 2131886745;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Small; // = 2131886746;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_CompoundButton_CheckBox; // = 2131886749;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_CompoundButton_RadioButton; // = 2131886750;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_CompoundButton_Switch; // = 2131886751;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_DrawerArrowToggle; // = 2131886752;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_DropDownItem_Spinner; // = 2131886753;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_EditText; // = 2131886754;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ImageButton; // = 2131886755;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar; // = 2131886756;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_Solid; // = 2131886757;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_Solid_Inverse; // = 2131886758;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabBar; // = 2131886759;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabBar_Inverse; // = 2131886760;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabText; // = 2131886761;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabText_Inverse; // = 2131886762;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabView; // = 2131886763;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabView_Inverse; // = 2131886764;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionButton; // = 2131886765;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionButton_CloseMode; // = 2131886766;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionButton_Overflow; // = 2131886767;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionMode_Inverse; // = 2131886768;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActivityChooserView; // = 2131886769;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_AutoCompleteTextView; // = 2131886770;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_DropDownItem_Spinner; // = 2131886771;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ListPopupWindow; // = 2131886772;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ListView_DropDown; // = 2131886773;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_PopupMenu; // = 2131886774;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_PopupMenu_Overflow; // = 2131886775;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_SearchView; // = 2131886776;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_Spinner_DropDown_ActionBar; // = 2131886777;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListMenuView; // = 2131886778;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListPopupWindow; // = 2131886779;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListView; // = 2131886780;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListView_DropDown; // = 2131886781;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListView_Menu; // = 2131886782;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_PopupMenu; // = 2131886783;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_PopupMenu_Overflow; // = 2131886784;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_PopupWindow; // = 2131886785;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ProgressBar; // = 2131886786;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ProgressBar_Horizontal; // = 2131886787;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_RatingBar; // = 2131886788;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_RatingBar_Indicator; // = 2131886789;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_RatingBar_Small; // = 2131886790;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SearchView; // = 2131886791;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SearchView_ActionBar; // = 2131886792;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SeekBar; // = 2131886793;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SeekBar_Discrete; // = 2131886794;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner; // = 2131886795;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner_DropDown; // = 2131886796;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner_DropDown_ActionBar; // = 2131886797;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner_Underlined; // = 2131886798;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_TextView_SpinnerItem; // = 2131886799;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Toolbar; // = 2131886800;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Toolbar_Button_Navigation; // = 2131886801;
    @DexIgnore
    public static /* final */ int Widget_Compat_NotificationActionContainer; // = 2131886802;
    @DexIgnore
    public static /* final */ int Widget_Compat_NotificationActionText; // = 2131886803;
    @DexIgnore
    public static /* final */ int Widget_Support_CoordinatorLayout; // = 2131886850;
}
