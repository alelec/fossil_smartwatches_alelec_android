package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xs */
public final class C3310xs {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C2149jq f11060a; // = new com.fossil.blesdk.obfuscated.C3310xs.C3311a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xs$a")
    /* renamed from: com.fossil.blesdk.obfuscated.xs$a */
    public class C3311a extends com.fossil.blesdk.obfuscated.C2236kq {
        @DexIgnore
        /* renamed from: a */
        public void mo12446a(android.graphics.Bitmap bitmap) {
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> m16497a(com.fossil.blesdk.obfuscated.C2149jq jqVar, android.graphics.drawable.Drawable drawable, int i, int i2) {
        android.graphics.Bitmap bitmap;
        android.graphics.drawable.Drawable current = drawable.getCurrent();
        boolean z = false;
        if (current instanceof android.graphics.drawable.BitmapDrawable) {
            bitmap = ((android.graphics.drawable.BitmapDrawable) current).getBitmap();
        } else if (!(current instanceof android.graphics.drawable.Animatable)) {
            bitmap = m16498b(jqVar, current, i, i2);
            z = true;
        } else {
            bitmap = null;
        }
        if (!z) {
            jqVar = f11060a;
        }
        return com.fossil.blesdk.obfuscated.C2675ps.m12395a(bitmap, jqVar);
    }

    @DexIgnore
    /* renamed from: b */
    public static android.graphics.Bitmap m16498b(com.fossil.blesdk.obfuscated.C2149jq jqVar, android.graphics.drawable.Drawable drawable, int i, int i2) {
        if (i == Integer.MIN_VALUE && drawable.getIntrinsicWidth() <= 0) {
            if (android.util.Log.isLoggable("DrawableToBitmap", 5)) {
                android.util.Log.w("DrawableToBitmap", "Unable to draw " + drawable + " to Bitmap with Target.SIZE_ORIGINAL because the Drawable has no intrinsic width");
            }
            return null;
        } else if (i2 != Integer.MIN_VALUE || drawable.getIntrinsicHeight() > 0) {
            if (drawable.getIntrinsicWidth() > 0) {
                i = drawable.getIntrinsicWidth();
            }
            if (drawable.getIntrinsicHeight() > 0) {
                i2 = drawable.getIntrinsicHeight();
            }
            java.util.concurrent.locks.Lock a = com.fossil.blesdk.obfuscated.C1904gt.m7580a();
            a.lock();
            android.graphics.Bitmap a2 = jqVar.mo12443a(i, i2, android.graphics.Bitmap.Config.ARGB_8888);
            try {
                android.graphics.Canvas canvas = new android.graphics.Canvas(a2);
                drawable.setBounds(0, 0, i, i2);
                drawable.draw(canvas);
                canvas.setBitmap((android.graphics.Bitmap) null);
                return a2;
            } finally {
                a.unlock();
            }
        } else {
            if (android.util.Log.isLoggable("DrawableToBitmap", 5)) {
                android.util.Log.w("DrawableToBitmap", "Unable to draw " + drawable + " to Bitmap with Target.SIZE_ORIGINAL because the Drawable has no intrinsic height");
            }
            return null;
        }
    }
}
