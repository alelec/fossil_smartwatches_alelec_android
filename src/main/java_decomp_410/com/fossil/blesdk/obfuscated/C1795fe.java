package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fe */
public class C1795fe implements com.fossil.blesdk.obfuscated.C3106ve.C3107a {

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1862g8<com.fossil.blesdk.obfuscated.C1795fe.C1797b> f5138a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.ArrayList<com.fossil.blesdk.obfuscated.C1795fe.C1797b> f5139b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.ArrayList<com.fossil.blesdk.obfuscated.C1795fe.C1797b> f5140c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C1795fe.C1796a f5141d;

    @DexIgnore
    /* renamed from: e */
    public java.lang.Runnable f5142e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ boolean f5143f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C3106ve f5144g;

    @DexIgnore
    /* renamed from: h */
    public int f5145h;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.fe$a */
    public interface C1796a {
        @DexIgnore
        /* renamed from: a */
        androidx.recyclerview.widget.RecyclerView.ViewHolder mo2802a(int i);

        @DexIgnore
        /* renamed from: a */
        void mo2803a(int i, int i2);

        @DexIgnore
        /* renamed from: a */
        void mo2804a(int i, int i2, java.lang.Object obj);

        @DexIgnore
        /* renamed from: a */
        void mo2805a(com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar);

        @DexIgnore
        /* renamed from: b */
        void mo2806b(int i, int i2);

        @DexIgnore
        /* renamed from: b */
        void mo2807b(com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar);

        @DexIgnore
        /* renamed from: c */
        void mo2808c(int i, int i2);

        @DexIgnore
        /* renamed from: d */
        void mo2810d(int i, int i2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fe$b")
    /* renamed from: com.fossil.blesdk.obfuscated.fe$b */
    public static class C1797b {

        @DexIgnore
        /* renamed from: a */
        public int f5146a;

        @DexIgnore
        /* renamed from: b */
        public int f5147b;

        @DexIgnore
        /* renamed from: c */
        public java.lang.Object f5148c;

        @DexIgnore
        /* renamed from: d */
        public int f5149d;

        @DexIgnore
        public C1797b(int i, int i2, int i3, java.lang.Object obj) {
            this.f5146a = i;
            this.f5147b = i2;
            this.f5149d = i3;
            this.f5148c = obj;
        }

        @DexIgnore
        /* renamed from: a */
        public java.lang.String mo10877a() {
            int i = this.f5146a;
            if (i == 1) {
                return "add";
            }
            if (i == 2) {
                return "rm";
            }
            if (i != 4) {
                return i != 8 ? "??" : "mv";
            }
            return "up";
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || com.fossil.blesdk.obfuscated.C1795fe.C1797b.class != obj.getClass()) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar = (com.fossil.blesdk.obfuscated.C1795fe.C1797b) obj;
            int i = this.f5146a;
            if (i != bVar.f5146a) {
                return false;
            }
            if (i == 8 && java.lang.Math.abs(this.f5149d - this.f5147b) == 1 && this.f5149d == bVar.f5147b && this.f5147b == bVar.f5149d) {
                return true;
            }
            if (this.f5149d != bVar.f5149d || this.f5147b != bVar.f5147b) {
                return false;
            }
            java.lang.Object obj2 = this.f5148c;
            if (obj2 != null) {
                if (!obj2.equals(bVar.f5148c)) {
                    return false;
                }
            } else if (bVar.f5148c != null) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            return (((this.f5146a * 31) + this.f5147b) * 31) + this.f5149d;
        }

        @DexIgnore
        public java.lang.String toString() {
            return java.lang.Integer.toHexString(java.lang.System.identityHashCode(this)) + "[" + mo10877a() + ",s:" + this.f5147b + "c:" + this.f5149d + ",p:" + this.f5148c + "]";
        }
    }

    @DexIgnore
    public C1795fe(com.fossil.blesdk.obfuscated.C1795fe.C1796a aVar) {
        this(aVar, false);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10854a() {
        int size = this.f5140c.size();
        for (int i = 0; i < size; i++) {
            this.f5141d.mo2807b(this.f5140c.get(i));
        }
        mo10857a((java.util.List<com.fossil.blesdk.obfuscated.C1795fe.C1797b>) this.f5140c);
        this.f5145h = 0;
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo10862b(int i) {
        int size = this.f5140c.size();
        for (int i2 = 0; i2 < size; i2++) {
            com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar = this.f5140c.get(i2);
            int i3 = bVar.f5146a;
            if (i3 == 8) {
                if (mo10852a(bVar.f5149d, i2 + 1) == i) {
                    return true;
                }
            } else if (i3 == 1) {
                int i4 = bVar.f5147b;
                int i5 = bVar.f5149d + i4;
                while (i4 < i5) {
                    if (mo10852a(i4, i2 + 1) == i) {
                        return true;
                    }
                    i4++;
                }
                continue;
            } else {
                continue;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo10865c(com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar) {
        mo10876g(bVar);
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo10869d(com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar) {
        char c;
        boolean z;
        boolean z2;
        int i = bVar.f5147b;
        int i2 = bVar.f5149d + i;
        int i3 = 0;
        char c2 = 65535;
        int i4 = i;
        while (i4 < i2) {
            if (this.f5141d.mo2802a(i4) != null || mo10862b(i4)) {
                if (c2 == 0) {
                    mo10875f(mo10853a(2, i, i3, (java.lang.Object) null));
                    z2 = true;
                } else {
                    z2 = false;
                }
                c = 1;
            } else {
                if (c2 == 1) {
                    mo10876g(mo10853a(2, i, i3, (java.lang.Object) null));
                    z = true;
                } else {
                    z = false;
                }
                c = 0;
            }
            if (z) {
                i4 -= i3;
                i2 -= i3;
                i3 = 1;
            } else {
                i3++;
            }
            i4++;
            c2 = c;
        }
        if (i3 != bVar.f5149d) {
            mo10855a(bVar);
            bVar = mo10853a(2, i, i3, (java.lang.Object) null);
        }
        if (c2 == 0) {
            mo10875f(bVar);
        } else {
            mo10876g(bVar);
        }
    }

    @DexIgnore
    /* renamed from: e */
    public void mo10872e() {
        this.f5144g.mo17038b(this.f5139b);
        int size = this.f5139b.size();
        for (int i = 0; i < size; i++) {
            com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar = this.f5139b.get(i);
            int i2 = bVar.f5146a;
            if (i2 == 1) {
                mo10861b(bVar);
            } else if (i2 == 2) {
                mo10869d(bVar);
            } else if (i2 == 4) {
                mo10873e(bVar);
            } else if (i2 == 8) {
                mo10865c(bVar);
            }
            java.lang.Runnable runnable = this.f5142e;
            if (runnable != null) {
                runnable.run();
            }
        }
        this.f5139b.clear();
    }

    @DexIgnore
    /* renamed from: f */
    public void mo10874f() {
        mo10857a((java.util.List<com.fossil.blesdk.obfuscated.C1795fe.C1797b>) this.f5139b);
        mo10857a((java.util.List<com.fossil.blesdk.obfuscated.C1795fe.C1797b>) this.f5140c);
        this.f5145h = 0;
    }

    @DexIgnore
    /* renamed from: g */
    public final void mo10876g(com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar) {
        this.f5140c.add(bVar);
        int i = bVar.f5146a;
        if (i == 1) {
            this.f5141d.mo2808c(bVar.f5147b, bVar.f5149d);
        } else if (i == 2) {
            this.f5141d.mo2806b(bVar.f5147b, bVar.f5149d);
        } else if (i == 4) {
            this.f5141d.mo2804a(bVar.f5147b, bVar.f5149d, bVar.f5148c);
        } else if (i == 8) {
            this.f5141d.mo2803a(bVar.f5147b, bVar.f5149d);
        } else {
            throw new java.lang.IllegalArgumentException("Unknown update op type for " + bVar);
        }
    }

    @DexIgnore
    public C1795fe(com.fossil.blesdk.obfuscated.C1795fe.C1796a aVar, boolean z) {
        this.f5138a = new com.fossil.blesdk.obfuscated.C1928h8(30);
        this.f5139b = new java.util.ArrayList<>();
        this.f5140c = new java.util.ArrayList<>();
        this.f5145h = 0;
        this.f5141d = aVar;
        this.f5143f = z;
        this.f5144g = new com.fossil.blesdk.obfuscated.C3106ve(this);
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo10866c() {
        return this.f5139b.size() > 0;
    }

    @DexIgnore
    /* renamed from: c */
    public int mo10864c(int i) {
        return mo10852a(i, 0);
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo10867c(int i, int i2) {
        if (i2 < 1) {
            return false;
        }
        this.f5139b.add(mo10853a(2, i, i2, (java.lang.Object) null));
        this.f5145h |= 2;
        if (this.f5139b.size() == 1) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: f */
    public final void mo10875f(com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar) {
        int i;
        int i2 = bVar.f5146a;
        if (i2 == 1 || i2 == 8) {
            throw new java.lang.IllegalArgumentException("should not dispatch add or move for pre layout");
        }
        int d = mo10868d(bVar.f5147b, i2);
        int i3 = bVar.f5147b;
        int i4 = bVar.f5146a;
        if (i4 == 2) {
            i = 0;
        } else if (i4 == 4) {
            i = 1;
        } else {
            throw new java.lang.IllegalArgumentException("op should be remove or update." + bVar);
        }
        int i5 = d;
        int i6 = i3;
        int i7 = 1;
        for (int i8 = 1; i8 < bVar.f5149d; i8++) {
            int d2 = mo10868d(bVar.f5147b + (i * i8), bVar.f5146a);
            int i9 = bVar.f5146a;
            if (i9 == 2 ? d2 == i5 : i9 == 4 && d2 == i5 + 1) {
                i7++;
            } else {
                com.fossil.blesdk.obfuscated.C1795fe.C1797b a = mo10853a(bVar.f5146a, i5, i7, bVar.f5148c);
                mo10856a(a, i6);
                mo10855a(a);
                if (bVar.f5146a == 4) {
                    i6 += i7;
                }
                i5 = d2;
                i7 = 1;
            }
        }
        java.lang.Object obj = bVar.f5148c;
        mo10855a(bVar);
        if (i7 > 0) {
            com.fossil.blesdk.obfuscated.C1795fe.C1797b a2 = mo10853a(bVar.f5146a, i5, i7, obj);
            mo10856a(a2, i6);
            mo10855a(a2);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10856a(com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar, int i) {
        this.f5141d.mo2805a(bVar);
        int i2 = bVar.f5146a;
        if (i2 == 2) {
            this.f5141d.mo2810d(i, bVar.f5149d);
        } else if (i2 == 4) {
            this.f5141d.mo2804a(i, bVar.f5149d, bVar.f5148c);
        } else {
            throw new java.lang.IllegalArgumentException("only remove and update ops can be dispatched in first pass");
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo10861b(com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar) {
        mo10876g(bVar);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo10863b(int i, int i2) {
        if (i2 < 1) {
            return false;
        }
        this.f5139b.add(mo10853a(1, i, i2, (java.lang.Object) null));
        this.f5145h |= 1;
        if (this.f5139b.size() == 1) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public int mo10852a(int i, int i2) {
        int size = this.f5140c.size();
        while (i2 < size) {
            com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar = this.f5140c.get(i2);
            int i3 = bVar.f5146a;
            if (i3 == 8) {
                int i4 = bVar.f5147b;
                if (i4 == i) {
                    i = bVar.f5149d;
                } else {
                    if (i4 < i) {
                        i--;
                    }
                    if (bVar.f5149d <= i) {
                        i++;
                    }
                }
            } else {
                int i5 = bVar.f5147b;
                if (i5 > i) {
                    continue;
                } else if (i3 == 2) {
                    int i6 = bVar.f5149d;
                    if (i < i5 + i6) {
                        return -1;
                    }
                    i -= i6;
                } else if (i3 == 1) {
                    i += bVar.f5149d;
                }
            }
            i2++;
        }
        return i;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10860b() {
        mo10854a();
        int size = this.f5139b.size();
        for (int i = 0; i < size; i++) {
            com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar = this.f5139b.get(i);
            int i2 = bVar.f5146a;
            if (i2 == 1) {
                this.f5141d.mo2807b(bVar);
                this.f5141d.mo2808c(bVar.f5147b, bVar.f5149d);
            } else if (i2 == 2) {
                this.f5141d.mo2807b(bVar);
                this.f5141d.mo2810d(bVar.f5147b, bVar.f5149d);
            } else if (i2 == 4) {
                this.f5141d.mo2807b(bVar);
                this.f5141d.mo2804a(bVar.f5147b, bVar.f5149d, bVar.f5148c);
            } else if (i2 == 8) {
                this.f5141d.mo2807b(bVar);
                this.f5141d.mo2803a(bVar.f5147b, bVar.f5149d);
            }
            java.lang.Runnable runnable = this.f5142e;
            if (runnable != null) {
                runnable.run();
            }
        }
        mo10857a((java.util.List<com.fossil.blesdk.obfuscated.C1795fe.C1797b>) this.f5139b);
        this.f5145h = 0;
    }

    @DexIgnore
    /* renamed from: e */
    public final void mo10873e(com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar) {
        int i = bVar.f5147b;
        int i2 = bVar.f5149d + i;
        int i3 = i;
        int i4 = 0;
        char c = 65535;
        while (i < i2) {
            if (this.f5141d.mo2802a(i) != null || mo10862b(i)) {
                if (c == 0) {
                    mo10875f(mo10853a(4, i3, i4, bVar.f5148c));
                    i3 = i;
                    i4 = 0;
                }
                c = 1;
            } else {
                if (c == 1) {
                    mo10876g(mo10853a(4, i3, i4, bVar.f5148c));
                    i3 = i;
                    i4 = 0;
                }
                c = 0;
            }
            i4++;
            i++;
        }
        if (i4 != bVar.f5149d) {
            java.lang.Object obj = bVar.f5148c;
            mo10855a(bVar);
            bVar = mo10853a(4, i3, i4, obj);
        }
        if (c == 0) {
            mo10875f(bVar);
        } else {
            mo10876g(bVar);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public final int mo10868d(int i, int i2) {
        for (int size = this.f5140c.size() - 1; size >= 0; size--) {
            com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar = this.f5140c.get(size);
            int i3 = bVar.f5146a;
            if (i3 == 8) {
                int i4 = bVar.f5147b;
                int i5 = bVar.f5149d;
                if (i4 >= i5) {
                    int i6 = i5;
                    i5 = i4;
                    i4 = i6;
                }
                if (i < i4 || i > i5) {
                    int i7 = bVar.f5147b;
                    if (i < i7) {
                        if (i2 == 1) {
                            bVar.f5147b = i7 + 1;
                            bVar.f5149d++;
                        } else if (i2 == 2) {
                            bVar.f5147b = i7 - 1;
                            bVar.f5149d--;
                        }
                    }
                } else {
                    int i8 = bVar.f5147b;
                    if (i4 == i8) {
                        if (i2 == 1) {
                            bVar.f5149d++;
                        } else if (i2 == 2) {
                            bVar.f5149d--;
                        }
                        i++;
                    } else {
                        if (i2 == 1) {
                            bVar.f5147b = i8 + 1;
                        } else if (i2 == 2) {
                            bVar.f5147b = i8 - 1;
                        }
                        i--;
                    }
                }
            } else {
                int i9 = bVar.f5147b;
                if (i9 <= i) {
                    if (i3 == 1) {
                        i -= bVar.f5149d;
                    } else if (i3 == 2) {
                        i += bVar.f5149d;
                    }
                } else if (i2 == 1) {
                    bVar.f5147b = i9 + 1;
                } else if (i2 == 2) {
                    bVar.f5147b = i9 - 1;
                }
            }
        }
        for (int size2 = this.f5140c.size() - 1; size2 >= 0; size2--) {
            com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar2 = this.f5140c.get(size2);
            if (bVar2.f5146a == 8) {
                int i10 = bVar2.f5149d;
                if (i10 == bVar2.f5147b || i10 < 0) {
                    this.f5140c.remove(size2);
                    mo10855a(bVar2);
                }
            } else if (bVar2.f5149d <= 0) {
                this.f5140c.remove(size2);
                mo10855a(bVar2);
            }
        }
        return i;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo10859a(int i, int i2, java.lang.Object obj) {
        if (i2 < 1) {
            return false;
        }
        this.f5139b.add(mo10853a(4, i, i2, obj));
        this.f5145h |= 4;
        if (this.f5139b.size() == 1) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo10858a(int i, int i2, int i3) {
        if (i == i2) {
            return false;
        }
        if (i3 == 1) {
            this.f5139b.add(mo10853a(8, i, i2, (java.lang.Object) null));
            this.f5145h |= 8;
            if (this.f5139b.size() == 1) {
                return true;
            }
            return false;
        }
        throw new java.lang.IllegalArgumentException("Moving more than 1 item is not supported yet");
    }

    @DexIgnore
    /* renamed from: a */
    public int mo10851a(int i) {
        int size = this.f5139b.size();
        for (int i2 = 0; i2 < size; i2++) {
            com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar = this.f5139b.get(i2);
            int i3 = bVar.f5146a;
            if (i3 != 1) {
                if (i3 == 2) {
                    int i4 = bVar.f5147b;
                    if (i4 <= i) {
                        int i5 = bVar.f5149d;
                        if (i4 + i5 > i) {
                            return -1;
                        }
                        i -= i5;
                    } else {
                        continue;
                    }
                } else if (i3 == 8) {
                    int i6 = bVar.f5147b;
                    if (i6 == i) {
                        i = bVar.f5149d;
                    } else {
                        if (i6 < i) {
                            i--;
                        }
                        if (bVar.f5149d <= i) {
                            i++;
                        }
                    }
                }
            } else if (bVar.f5147b <= i) {
                i += bVar.f5149d;
            }
        }
        return i;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1795fe.C1797b mo10853a(int i, int i2, int i3, java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.C1795fe.C1797b a = this.f5138a.mo11162a();
        if (a == null) {
            return new com.fossil.blesdk.obfuscated.C1795fe.C1797b(i, i2, i3, obj);
        }
        a.f5146a = i;
        a.f5147b = i2;
        a.f5149d = i3;
        a.f5148c = obj;
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10855a(com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar) {
        if (!this.f5143f) {
            bVar.f5148c = null;
            this.f5138a.mo11163a(bVar);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo10871d(int i) {
        return (i & this.f5145h) != 0;
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo10870d() {
        return !this.f5140c.isEmpty() && !this.f5139b.isEmpty();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10857a(java.util.List<com.fossil.blesdk.obfuscated.C1795fe.C1797b> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            mo10855a(list.get(i));
        }
        list.clear();
    }
}
