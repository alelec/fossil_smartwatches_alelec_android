package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ih {
    @DexIgnore
    public ViewGroup a;
    @DexIgnore
    public Runnable b;

    @DexIgnore
    public void a() {
        if (a(this.a) == this) {
            Runnable runnable = this.b;
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    @DexIgnore
    public static void a(View view, ih ihVar) {
        view.setTag(gh.transition_current_scene, ihVar);
    }

    @DexIgnore
    public static ih a(View view) {
        return (ih) view.getTag(gh.transition_current_scene);
    }
}
