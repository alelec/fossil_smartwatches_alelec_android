package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.g0 */
public class C1850g0 extends com.fossil.blesdk.obfuscated.C3269xa {
    @DexIgnore
    public android.app.Dialog onCreateDialog(android.os.Bundle bundle) {
        return new com.fossil.blesdk.obfuscated.C1761f0(getContext(), getTheme());
    }

    @DexIgnore
    public void setupDialog(android.app.Dialog dialog, int i) {
        if (dialog instanceof com.fossil.blesdk.obfuscated.C1761f0) {
            com.fossil.blesdk.obfuscated.C1761f0 f0Var = (com.fossil.blesdk.obfuscated.C1761f0) dialog;
            if (!(i == 1 || i == 2)) {
                if (i == 3) {
                    dialog.getWindow().addFlags(24);
                } else {
                    return;
                }
            }
            f0Var.mo10668a(1);
            return;
        }
        super.setupDialog(dialog, i);
    }
}
