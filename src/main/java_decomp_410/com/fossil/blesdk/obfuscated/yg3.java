package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yg3 extends tg3 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public /* final */ ug3 f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = yg3.class.getSimpleName();
        kd4.a((Object) simpleName, "AboutPresenter::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public yg3(ug3 ug3) {
        kd4.b(ug3, "mView");
        this.f = ug3;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(g, "presenter start");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(g, "presenter stop");
    }

    @DexIgnore
    public void h() {
        this.f.a(this);
    }
}
