package com.fossil.blesdk.obfuscated;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.facebook.appevents.FacebookTimeSpentData;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ge0;
import com.fossil.blesdk.obfuscated.ij0;
import com.fossil.blesdk.obfuscated.se0;
import com.fossil.blesdk.obfuscated.ze0;
import com.fossil.blesdk.obfuscated.zj0;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.UnsupportedApiCallException;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ve0 implements Handler.Callback {
    @DexIgnore
    public static /* final */ Status n; // = new Status(4, "Sign-out occurred while this API call was in progress.");
    @DexIgnore
    public static /* final */ Status o; // = new Status(4, "The user must be signed in to make this API call.");
    @DexIgnore
    public static /* final */ Object p; // = new Object();
    @DexIgnore
    public static ve0 q;
    @DexIgnore
    public long a; // = 5000;
    @DexIgnore
    public long b; // = ScanService.BLE_SCAN_TIMEOUT;
    @DexIgnore
    public long c; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ xd0 e;
    @DexIgnore
    public /* final */ sj0 f;
    @DexIgnore
    public /* final */ AtomicInteger g; // = new AtomicInteger(1);
    @DexIgnore
    public /* final */ AtomicInteger h; // = new AtomicInteger(0);
    @DexIgnore
    public /* final */ Map<yh0<?>, a<?>> i; // = new ConcurrentHashMap(5, 0.75f, 1);
    @DexIgnore
    public mf0 j; // = null;
    @DexIgnore
    public /* final */ Set<yh0<?>> k; // = new h4();
    @DexIgnore
    public /* final */ Set<yh0<?>> l; // = new h4();
    @DexIgnore
    public /* final */ Handler m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements kh0, ij0.c {
        @DexIgnore
        public /* final */ de0.f a;
        @DexIgnore
        public /* final */ yh0<?> b;
        @DexIgnore
        public tj0 c; // = null;
        @DexIgnore
        public Set<Scope> d; // = null;
        @DexIgnore
        public boolean e; // = false;

        @DexIgnore
        public c(de0.f fVar, yh0<?> yh0) {
            this.a = fVar;
            this.b = yh0;
        }

        @DexIgnore
        public final void a(ud0 ud0) {
            ve0.this.m.post(new xg0(this, ud0));
        }

        @DexIgnore
        public final void b(ud0 ud0) {
            ((a) ve0.this.i.get(this.b)).b(ud0);
        }

        @DexIgnore
        public final void a(tj0 tj0, Set<Scope> set) {
            if (tj0 == null || set == null) {
                Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", new Exception());
                b(new ud0(4));
                return;
            }
            this.c = tj0;
            this.d = set;
            a();
        }

        @DexIgnore
        public final void a() {
            if (this.e) {
                tj0 tj0 = this.c;
                if (tj0 != null) {
                    this.a.a(tj0, this.d);
                }
            }
        }
    }

    @DexIgnore
    public ve0(Context context, Looper looper, xd0 xd0) {
        this.d = context;
        this.m = new ss0(looper, this);
        this.e = xd0;
        this.f = new sj0(xd0);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(6));
    }

    @DexIgnore
    public static ve0 a(Context context) {
        ve0 ve0;
        synchronized (p) {
            if (q == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                q = new ve0(context.getApplicationContext(), handlerThread.getLooper(), xd0.a());
            }
            ve0 = q;
        }
        return ve0;
    }

    @DexIgnore
    public static void d() {
        synchronized (p) {
            if (q != null) {
                ve0 ve0 = q;
                ve0.h.incrementAndGet();
                ve0.m.sendMessageAtFrontOfQueue(ve0.m.obtainMessage(10));
            }
        }
    }

    @DexIgnore
    public static ve0 e() {
        ve0 ve0;
        synchronized (p) {
            bk0.a(q, (Object) "Must guarantee manager is non-null before using getInstance");
            ve0 = q;
        }
        return ve0;
    }

    @DexIgnore
    public final int b() {
        return this.g.getAndIncrement();
    }

    @DexIgnore
    public final void c() {
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(3));
    }

    @DexIgnore
    public boolean handleMessage(Message message) {
        a aVar;
        int i2 = message.what;
        long j2 = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
        switch (i2) {
            case 1:
                if (((Boolean) message.obj).booleanValue()) {
                    j2 = ButtonService.CONNECT_TIMEOUT;
                }
                this.c = j2;
                this.m.removeMessages(12);
                for (yh0<?> obtainMessage : this.i.keySet()) {
                    Handler handler = this.m;
                    handler.sendMessageDelayed(handler.obtainMessage(12, obtainMessage), this.c);
                }
                break;
            case 2:
                ai0 ai0 = (ai0) message.obj;
                Iterator<yh0<?>> it = ai0.b().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    } else {
                        yh0 next = it.next();
                        a aVar2 = this.i.get(next);
                        if (aVar2 == null) {
                            ai0.a(next, new ud0(13), (String) null);
                            break;
                        } else if (aVar2.c()) {
                            ai0.a(next, ud0.i, aVar2.f().f());
                        } else if (aVar2.n() != null) {
                            ai0.a(next, aVar2.n(), (String) null);
                        } else {
                            aVar2.a(ai0);
                            aVar2.a();
                        }
                    }
                }
            case 3:
                for (a next2 : this.i.values()) {
                    next2.m();
                    next2.a();
                }
                break;
            case 4:
            case 8:
            case 13:
                dh0 dh0 = (dh0) message.obj;
                a aVar3 = this.i.get(dh0.c.h());
                if (aVar3 == null) {
                    b(dh0.c);
                    aVar3 = this.i.get(dh0.c.h());
                }
                if (aVar3.d() && this.h.get() != dh0.b) {
                    dh0.a.a(n);
                    aVar3.k();
                    break;
                } else {
                    aVar3.a(dh0.a);
                    break;
                }
                break;
            case 5:
                int i3 = message.arg1;
                ud0 ud0 = (ud0) message.obj;
                Iterator<a<?>> it2 = this.i.values().iterator();
                while (true) {
                    if (it2.hasNext()) {
                        aVar = it2.next();
                        if (aVar.b() == i3) {
                        }
                    } else {
                        aVar = null;
                    }
                }
                if (aVar == null) {
                    StringBuilder sb = new StringBuilder(76);
                    sb.append("Could not find API instance ");
                    sb.append(i3);
                    sb.append(" while trying to fail enqueued calls.");
                    Log.wtf("GoogleApiManager", sb.toString(), new Exception());
                    break;
                } else {
                    String b2 = this.e.b(ud0.H());
                    String I = ud0.I();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b2).length() + 69 + String.valueOf(I).length());
                    sb2.append("Error resolution was canceled by the user, original error message: ");
                    sb2.append(b2);
                    sb2.append(": ");
                    sb2.append(I);
                    aVar.a(new Status(17, sb2.toString()));
                    break;
                }
            case 6:
                if (pm0.a() && (this.d.getApplicationContext() instanceof Application)) {
                    se0.a((Application) this.d.getApplicationContext());
                    se0.b().a((se0.a) new rg0(this));
                    if (!se0.b().b(true)) {
                        this.c = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
                        break;
                    }
                }
                break;
            case 7:
                b((fe0<?>) (fe0) message.obj);
                break;
            case 9:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).e();
                    break;
                }
                break;
            case 10:
                for (yh0<?> remove : this.l) {
                    this.i.remove(remove).k();
                }
                this.l.clear();
                break;
            case 11:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).g();
                    break;
                }
                break;
            case 12:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).q();
                    break;
                }
                break;
            case 14:
                nf0 nf0 = (nf0) message.obj;
                yh0<?> b3 = nf0.b();
                if (this.i.containsKey(b3)) {
                    nf0.a().a(Boolean.valueOf(this.i.get(b3).a(false)));
                    break;
                } else {
                    nf0.a().a(false);
                    break;
                }
            case 15:
                b bVar = (b) message.obj;
                if (this.i.containsKey(bVar.a)) {
                    this.i.get(bVar.a).a(bVar);
                    break;
                }
                break;
            case 16:
                b bVar2 = (b) message.obj;
                if (this.i.containsKey(bVar2.a)) {
                    this.i.get(bVar2.a).b(bVar2);
                    break;
                }
                break;
            default:
                StringBuilder sb3 = new StringBuilder(31);
                sb3.append("Unknown message id: ");
                sb3.append(i2);
                Log.w("GoogleApiManager", sb3.toString());
                return false;
        }
        return true;
    }

    @DexIgnore
    public final void b(fe0<?> fe0) {
        yh0<?> h2 = fe0.h();
        a aVar = this.i.get(h2);
        if (aVar == null) {
            aVar = new a(fe0);
            this.i.put(h2, aVar);
        }
        if (aVar.d()) {
            this.l.add(h2);
        }
        aVar.a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a<O extends de0.d> implements ge0.b, ge0.c, hi0 {
        @DexIgnore
        public /* final */ Queue<ig0> e; // = new LinkedList();
        @DexIgnore
        public /* final */ de0.f f;
        @DexIgnore
        public /* final */ de0.b g;
        @DexIgnore
        public /* final */ yh0<O> h;
        @DexIgnore
        public /* final */ jf0 i;
        @DexIgnore
        public /* final */ Set<ai0> j; // = new HashSet();
        @DexIgnore
        public /* final */ Map<ze0.a<?>, eh0> k; // = new HashMap();
        @DexIgnore
        public /* final */ int l;
        @DexIgnore
        public /* final */ hh0 m;
        @DexIgnore
        public boolean n;
        @DexIgnore
        public /* final */ List<b> o; // = new ArrayList();
        @DexIgnore
        public ud0 p; // = null;

        @DexIgnore
        public a(fe0<O> fe0) {
            this.f = fe0.a(ve0.this.m.getLooper(), (a<O>) this);
            de0.f fVar = this.f;
            if (fVar instanceof gk0) {
                this.g = ((gk0) fVar).G();
            } else {
                this.g = fVar;
            }
            this.h = fe0.h();
            this.i = new jf0();
            this.l = fe0.f();
            if (this.f.l()) {
                this.m = fe0.a(ve0.this.d, ve0.this.m);
            } else {
                this.m = null;
            }
        }

        @DexIgnore
        public final void a(ud0 ud0, de0<?> de0, boolean z) {
            if (Looper.myLooper() == ve0.this.m.getLooper()) {
                a(ud0);
            } else {
                ve0.this.m.post(new ug0(this, ud0));
            }
        }

        @DexIgnore
        public final void b(ud0 ud0) {
            bk0.a(ve0.this.m);
            this.f.a();
            a(ud0);
        }

        @DexIgnore
        public final boolean c(ud0 ud0) {
            synchronized (ve0.p) {
                if (ve0.this.j == null || !ve0.this.k.contains(this.h)) {
                    return false;
                }
                ve0.this.j.b(ud0, this.l);
                return true;
            }
        }

        @DexIgnore
        public final void d(ud0 ud0) {
            for (ai0 next : this.j) {
                String str = null;
                if (zj0.a(ud0, ud0.i)) {
                    str = this.f.f();
                }
                next.a(this.h, ud0, str);
            }
            this.j.clear();
        }

        @DexIgnore
        public final void e(Bundle bundle) {
            if (Looper.myLooper() == ve0.this.m.getLooper()) {
                h();
            } else {
                ve0.this.m.post(new sg0(this));
            }
        }

        @DexIgnore
        public final void f(int i2) {
            if (Looper.myLooper() == ve0.this.m.getLooper()) {
                i();
            } else {
                ve0.this.m.post(new tg0(this));
            }
        }

        @DexIgnore
        public final void g() {
            Status status;
            bk0.a(ve0.this.m);
            if (this.n) {
                o();
                if (ve0.this.e.c(ve0.this.d) == 18) {
                    status = new Status(8, "Connection timed out while waiting for Google Play services update to complete.");
                } else {
                    status = new Status(8, "API failed to connect while resuming due to an unknown error.");
                }
                a(status);
                this.f.a();
            }
        }

        @DexIgnore
        public final void h() {
            m();
            d(ud0.i);
            o();
            Iterator<eh0> it = this.k.values().iterator();
            while (it.hasNext()) {
                eh0 next = it.next();
                if (a(next.a.c()) != null) {
                    it.remove();
                } else {
                    try {
                        next.a.a(this.g, new xn1());
                    } catch (DeadObjectException unused) {
                        f(1);
                        this.f.a();
                    } catch (RemoteException unused2) {
                        it.remove();
                    }
                }
            }
            j();
            p();
        }

        @DexIgnore
        public final void i() {
            m();
            this.n = true;
            this.i.c();
            ve0.this.m.sendMessageDelayed(Message.obtain(ve0.this.m, 9, this.h), ve0.this.a);
            ve0.this.m.sendMessageDelayed(Message.obtain(ve0.this.m, 11, this.h), ve0.this.b);
            ve0.this.f.a();
        }

        @DexIgnore
        public final void j() {
            ArrayList arrayList = new ArrayList(this.e);
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                ig0 ig0 = (ig0) obj;
                if (!this.f.c()) {
                    return;
                }
                if (b(ig0)) {
                    this.e.remove(ig0);
                }
            }
        }

        @DexIgnore
        public final void k() {
            bk0.a(ve0.this.m);
            a(ve0.n);
            this.i.b();
            for (ze0.a xh0 : (ze0.a[]) this.k.keySet().toArray(new ze0.a[this.k.size()])) {
                a((ig0) new xh0(xh0, new xn1()));
            }
            d(new ud0(4));
            if (this.f.c()) {
                this.f.a((ij0.e) new vg0(this));
            }
        }

        @DexIgnore
        public final Map<ze0.a<?>, eh0> l() {
            return this.k;
        }

        @DexIgnore
        public final void m() {
            bk0.a(ve0.this.m);
            this.p = null;
        }

        @DexIgnore
        public final ud0 n() {
            bk0.a(ve0.this.m);
            return this.p;
        }

        @DexIgnore
        public final void o() {
            if (this.n) {
                ve0.this.m.removeMessages(11, this.h);
                ve0.this.m.removeMessages(9, this.h);
                this.n = false;
            }
        }

        @DexIgnore
        public final void p() {
            ve0.this.m.removeMessages(12, this.h);
            ve0.this.m.sendMessageDelayed(ve0.this.m.obtainMessage(12, this.h), ve0.this.c);
        }

        @DexIgnore
        public final boolean q() {
            return a(true);
        }

        @DexIgnore
        public final ln1 r() {
            hh0 hh0 = this.m;
            if (hh0 == null) {
                return null;
            }
            return hh0.o();
        }

        @DexIgnore
        public final void a(ud0 ud0) {
            bk0.a(ve0.this.m);
            hh0 hh0 = this.m;
            if (hh0 != null) {
                hh0.p();
            }
            m();
            ve0.this.f.a();
            d(ud0);
            if (ud0.H() == 4) {
                a(ve0.o);
            } else if (this.e.isEmpty()) {
                this.p = ud0;
            } else if (!c(ud0) && !ve0.this.b(ud0, this.l)) {
                if (ud0.H() == 18) {
                    this.n = true;
                }
                if (this.n) {
                    ve0.this.m.sendMessageDelayed(Message.obtain(ve0.this.m, 9, this.h), ve0.this.a);
                    return;
                }
                String a = this.h.a();
                StringBuilder sb = new StringBuilder(String.valueOf(a).length() + 38);
                sb.append("API: ");
                sb.append(a);
                sb.append(" is not available on this device.");
                a(new Status(17, sb.toString()));
            }
        }

        @DexIgnore
        public final boolean b(ig0 ig0) {
            if (!(ig0 instanceof fh0)) {
                c(ig0);
                return true;
            }
            fh0 fh0 = (fh0) ig0;
            wd0 a = a(fh0.b(this));
            if (a == null) {
                c(ig0);
                return true;
            } else if (fh0.c(this)) {
                b bVar = new b(this.h, a, (rg0) null);
                int indexOf = this.o.indexOf(bVar);
                if (indexOf >= 0) {
                    b bVar2 = this.o.get(indexOf);
                    ve0.this.m.removeMessages(15, bVar2);
                    ve0.this.m.sendMessageDelayed(Message.obtain(ve0.this.m, 15, bVar2), ve0.this.a);
                    return false;
                }
                this.o.add(bVar);
                ve0.this.m.sendMessageDelayed(Message.obtain(ve0.this.m, 15, bVar), ve0.this.a);
                ve0.this.m.sendMessageDelayed(Message.obtain(ve0.this.m, 16, bVar), ve0.this.b);
                ud0 ud0 = new ud0(2, (PendingIntent) null);
                if (c(ud0)) {
                    return false;
                }
                ve0.this.b(ud0, this.l);
                return false;
            } else {
                fh0.a((RuntimeException) new UnsupportedApiCallException(a));
                return false;
            }
        }

        @DexIgnore
        public final void e() {
            bk0.a(ve0.this.m);
            if (this.n) {
                a();
            }
        }

        @DexIgnore
        public final de0.f f() {
            return this.f;
        }

        @DexIgnore
        public final boolean d() {
            return this.f.l();
        }

        @DexIgnore
        public final void c(ig0 ig0) {
            ig0.a(this.i, d());
            try {
                ig0.a((a<?>) this);
            } catch (DeadObjectException unused) {
                f(1);
                this.f.a();
            }
        }

        @DexIgnore
        public final boolean c() {
            return this.f.c();
        }

        @DexIgnore
        public final void a(ig0 ig0) {
            bk0.a(ve0.this.m);
            if (!this.f.c()) {
                this.e.add(ig0);
                ud0 ud0 = this.p;
                if (ud0 == null || !ud0.K()) {
                    a();
                } else {
                    a(this.p);
                }
            } else if (b(ig0)) {
                p();
            } else {
                this.e.add(ig0);
            }
        }

        @DexIgnore
        public final int b() {
            return this.l;
        }

        @DexIgnore
        public final void b(b bVar) {
            if (this.o.remove(bVar)) {
                ve0.this.m.removeMessages(15, bVar);
                ve0.this.m.removeMessages(16, bVar);
                wd0 b = bVar.b;
                ArrayList arrayList = new ArrayList(this.e.size());
                for (ig0 ig0 : this.e) {
                    if (ig0 instanceof fh0) {
                        wd0[] b2 = ((fh0) ig0).b(this);
                        if (b2 != null && fm0.a((T[]) b2, b)) {
                            arrayList.add(ig0);
                        }
                    }
                }
                int size = arrayList.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList.get(i2);
                    i2++;
                    ig0 ig02 = (ig0) obj;
                    this.e.remove(ig02);
                    ig02.a((RuntimeException) new UnsupportedApiCallException(b));
                }
            }
        }

        @DexIgnore
        public final void a(Status status) {
            bk0.a(ve0.this.m);
            for (ig0 a : this.e) {
                a.a(status);
            }
            this.e.clear();
        }

        @DexIgnore
        public final boolean a(boolean z) {
            bk0.a(ve0.this.m);
            if (!this.f.c() || this.k.size() != 0) {
                return false;
            }
            if (this.i.a()) {
                if (z) {
                    p();
                }
                return false;
            }
            this.f.a();
            return true;
        }

        @DexIgnore
        public final void a() {
            bk0.a(ve0.this.m);
            if (!this.f.c() && !this.f.e()) {
                int a = ve0.this.f.a(ve0.this.d, this.f);
                if (a != 0) {
                    a(new ud0(a, (PendingIntent) null));
                    return;
                }
                c cVar = new c(this.f, this.h);
                if (this.f.l()) {
                    this.m.a((kh0) cVar);
                }
                this.f.a((ij0.c) cVar);
            }
        }

        @DexIgnore
        public final void a(ai0 ai0) {
            bk0.a(ve0.this.m);
            this.j.add(ai0);
        }

        @DexIgnore
        public final wd0 a(wd0[] wd0Arr) {
            if (!(wd0Arr == null || wd0Arr.length == 0)) {
                wd0[] j2 = this.f.j();
                if (j2 == null) {
                    j2 = new wd0[0];
                }
                g4 g4Var = new g4(j2.length);
                for (wd0 wd0 : j2) {
                    g4Var.put(wd0.H(), Long.valueOf(wd0.I()));
                }
                for (wd0 wd02 : wd0Arr) {
                    if (!g4Var.containsKey(wd02.H()) || ((Long) g4Var.get(wd02.H())).longValue() < wd02.I()) {
                        return wd02;
                    }
                }
            }
            return null;
        }

        @DexIgnore
        public final void a(b bVar) {
            if (!this.o.contains(bVar) || this.n) {
                return;
            }
            if (!this.f.c()) {
                a();
            } else {
                j();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ yh0<?> a;
        @DexIgnore
        public /* final */ wd0 b;

        @DexIgnore
        public b(yh0<?> yh0, wd0 wd0) {
            this.a = yh0;
            this.b = wd0;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (obj != null && (obj instanceof b)) {
                b bVar = (b) obj;
                if (!zj0.a(this.a, bVar.a) || !zj0.a(this.b, bVar.b)) {
                    return false;
                }
                return true;
            }
            return false;
        }

        @DexIgnore
        public final int hashCode() {
            return zj0.a(this.a, this.b);
        }

        @DexIgnore
        public final String toString() {
            zj0.a a2 = zj0.a((Object) this);
            a2.a("key", this.a);
            a2.a("feature", this.b);
            return a2.toString();
        }

        @DexIgnore
        public /* synthetic */ b(yh0 yh0, wd0 wd0, rg0 rg0) {
            this(yh0, wd0);
        }
    }

    @DexIgnore
    public final void b(mf0 mf0) {
        synchronized (p) {
            if (this.j == mf0) {
                this.j = null;
                this.k.clear();
            }
        }
    }

    @DexIgnore
    public final void a(fe0<?> fe0) {
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(7, fe0));
    }

    @DexIgnore
    public final void a(mf0 mf0) {
        synchronized (p) {
            if (this.j != mf0) {
                this.j = mf0;
                this.k.clear();
            }
            this.k.addAll(mf0.h());
        }
    }

    @DexIgnore
    public final boolean b(ud0 ud0, int i2) {
        return this.e.a(this.d, ud0, i2);
    }

    @DexIgnore
    public final wn1<Map<yh0<?>, String>> a(Iterable<? extends fe0<?>> iterable) {
        ai0 ai0 = new ai0(iterable);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(2, ai0));
        return ai0.a();
    }

    @DexIgnore
    public final void a() {
        this.h.incrementAndGet();
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(10));
    }

    @DexIgnore
    public final <O extends de0.d> void a(fe0<O> fe0, int i2, te0<? extends me0, de0.b> te0) {
        uh0 uh0 = new uh0(i2, te0);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(4, new dh0(uh0, this.h.get(), fe0)));
    }

    @DexIgnore
    public final <O extends de0.d, ResultT> void a(fe0<O> fe0, int i2, ff0<de0.b, ResultT> ff0, xn1<ResultT> xn1, df0 df0) {
        wh0 wh0 = new wh0(i2, ff0, xn1, df0);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(4, new dh0(wh0, this.h.get(), fe0)));
    }

    @DexIgnore
    public final <O extends de0.d> wn1<Void> a(fe0<O> fe0, bf0<de0.b, ?> bf0, hf0<de0.b, ?> hf0) {
        xn1 xn1 = new xn1();
        vh0 vh0 = new vh0(new eh0(bf0, hf0), xn1);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(8, new dh0(vh0, this.h.get(), fe0)));
        return xn1.a();
    }

    @DexIgnore
    public final <O extends de0.d> wn1<Boolean> a(fe0<O> fe0, ze0.a<?> aVar) {
        xn1 xn1 = new xn1();
        xh0 xh0 = new xh0(aVar, xn1);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(13, new dh0(xh0, this.h.get(), fe0)));
        return xn1.a();
    }

    @DexIgnore
    public final PendingIntent a(yh0<?> yh0, int i2) {
        a aVar = this.i.get(yh0);
        if (aVar == null) {
            return null;
        }
        ln1 r = aVar.r();
        if (r == null) {
            return null;
        }
        return PendingIntent.getActivity(this.d, i2, r.k(), 134217728);
    }

    @DexIgnore
    public final void a(ud0 ud0, int i2) {
        if (!b(ud0, i2)) {
            Handler handler = this.m;
            handler.sendMessage(handler.obtainMessage(5, i2, 0, ud0));
        }
    }
}
