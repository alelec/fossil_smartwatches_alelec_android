package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hs */
public class C1969hs implements com.fossil.blesdk.obfuscated.C2912sr<java.net.URL, java.io.InputStream> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2912sr<com.fossil.blesdk.obfuscated.C2340lr, java.io.InputStream> f5832a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hs$a")
    /* renamed from: com.fossil.blesdk.obfuscated.hs$a */
    public static class C1970a implements com.fossil.blesdk.obfuscated.C2984tr<java.net.URL, java.io.InputStream> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<java.net.URL, java.io.InputStream> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C1969hs(wrVar.mo17509a(com.fossil.blesdk.obfuscated.C2340lr.class, java.io.InputStream.class));
        }
    }

    @DexIgnore
    public C1969hs(com.fossil.blesdk.obfuscated.C2912sr<com.fossil.blesdk.obfuscated.C2340lr, java.io.InputStream> srVar) {
        this.f5832a = srVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(java.net.URL url) {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<java.io.InputStream> mo8911a(java.net.URL url, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return this.f5832a.mo8911a(new com.fossil.blesdk.obfuscated.C2340lr(url), i, i2, loVar);
    }
}
