package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.h8 */
public class C1928h8<T> implements com.fossil.blesdk.obfuscated.C1862g8<T> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.Object[] f5702a;

    @DexIgnore
    /* renamed from: b */
    public int f5703b;

    @DexIgnore
    public C1928h8(int i) {
        if (i > 0) {
            this.f5702a = new java.lang.Object[i];
            return;
        }
        throw new java.lang.IllegalArgumentException("The max pool size must be > 0");
    }

    @DexIgnore
    /* renamed from: a */
    public T mo11162a() {
        int i = this.f5703b;
        if (i <= 0) {
            return null;
        }
        int i2 = i - 1;
        T[] tArr = this.f5702a;
        T t = tArr[i2];
        tArr[i2] = null;
        this.f5703b = i - 1;
        return t;
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo11604b(T t) {
        for (int i = 0; i < this.f5703b; i++) {
            if (this.f5702a[i] == t) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11163a(T t) {
        if (!mo11604b(t)) {
            int i = this.f5703b;
            java.lang.Object[] objArr = this.f5702a;
            if (i >= objArr.length) {
                return false;
            }
            objArr[i] = t;
            this.f5703b = i + 1;
            return true;
        }
        throw new java.lang.IllegalStateException("Already in the pool!");
    }
}
