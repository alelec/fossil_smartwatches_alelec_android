package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class z00 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[Peripheral.State.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[Peripheral.State.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c; // = new int[Peripheral.HIDState.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d; // = new int[Peripheral.HIDState.values().length];

    /*
    static {
        a[Peripheral.State.DISCONNECTED.ordinal()] = 1;
        a[Peripheral.State.CONNECTING.ordinal()] = 2;
        a[Peripheral.State.CONNECTED.ordinal()] = 3;
        a[Peripheral.State.DISCONNECTING.ordinal()] = 4;
        b[Peripheral.State.DISCONNECTED.ordinal()] = 1;
        b[Peripheral.State.CONNECTED.ordinal()] = 2;
        b[Peripheral.State.CONNECTING.ordinal()] = 3;
        b[Peripheral.State.DISCONNECTING.ordinal()] = 4;
        c[Peripheral.HIDState.DISCONNECTED.ordinal()] = 1;
        c[Peripheral.HIDState.CONNECTING.ordinal()] = 2;
        c[Peripheral.HIDState.CONNECTED.ordinal()] = 3;
        c[Peripheral.HIDState.DISCONNECTING.ordinal()] = 4;
        d[Peripheral.HIDState.DISCONNECTED.ordinal()] = 1;
        d[Peripheral.HIDState.CONNECTING.ordinal()] = 2;
        d[Peripheral.HIDState.CONNECTED.ordinal()] = 3;
        d[Peripheral.HIDState.DISCONNECTING.ordinal()] = 4;
    }
    */
}
