package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ik */
public class C2035ik implements com.fossil.blesdk.obfuscated.C2331lk.C2332a {

    @DexIgnore
    /* renamed from: d */
    public static /* final */ java.lang.String f6100d; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("WorkConstraintsTracker");

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1953hk f6101a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2331lk<?>[] f6102b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.Object f6103c; // = new java.lang.Object();

    @DexIgnore
    public C2035ik(android.content.Context context, com.fossil.blesdk.obfuscated.C3444zl zlVar, com.fossil.blesdk.obfuscated.C1953hk hkVar) {
        android.content.Context applicationContext = context.getApplicationContext();
        this.f6101a = hkVar;
        this.f6102b = new com.fossil.blesdk.obfuscated.C2331lk[]{new com.fossil.blesdk.obfuscated.C2130jk(applicationContext, zlVar), new com.fossil.blesdk.obfuscated.C2228kk(applicationContext, zlVar), new com.fossil.blesdk.obfuscated.C2746qk(applicationContext, zlVar), new com.fossil.blesdk.obfuscated.C2420mk(applicationContext, zlVar), new com.fossil.blesdk.obfuscated.C2658pk(applicationContext, zlVar), new com.fossil.blesdk.obfuscated.C2574ok(applicationContext, zlVar), new com.fossil.blesdk.obfuscated.C2493nk(applicationContext, zlVar)};
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12009a() {
        synchronized (this.f6103c) {
            for (com.fossil.blesdk.obfuscated.C2331lk<?> a : this.f6102b) {
                a.mo13300a();
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12012b(java.util.List<java.lang.String> list) {
        synchronized (this.f6103c) {
            if (this.f6101a != null) {
                this.f6101a.mo3844a(list);
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo12013c(java.util.List<com.fossil.blesdk.obfuscated.C1954hl> list) {
        synchronized (this.f6103c) {
            for (com.fossil.blesdk.obfuscated.C2331lk<?> a : this.f6102b) {
                a.mo13301a((com.fossil.blesdk.obfuscated.C2331lk.C2332a) null);
            }
            for (com.fossil.blesdk.obfuscated.C2331lk<?> a2 : this.f6102b) {
                a2.mo13302a(list);
            }
            for (com.fossil.blesdk.obfuscated.C2331lk<?> a3 : this.f6102b) {
                a3.mo13301a((com.fossil.blesdk.obfuscated.C2331lk.C2332a) this);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12011a(java.lang.String str) {
        synchronized (this.f6103c) {
            for (com.fossil.blesdk.obfuscated.C2331lk<?> lkVar : this.f6102b) {
                if (lkVar.mo13303a(str)) {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f6100d, java.lang.String.format("Work %s constrained by %s", new java.lang.Object[]{str, lkVar.getClass().getSimpleName()}), new java.lang.Throwable[0]);
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12010a(java.util.List<java.lang.String> list) {
        synchronized (this.f6103c) {
            java.util.ArrayList arrayList = new java.util.ArrayList();
            for (java.lang.String next : list) {
                if (mo12011a(next)) {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f6100d, java.lang.String.format("Constraints met for %s", new java.lang.Object[]{next}), new java.lang.Throwable[0]);
                    arrayList.add(next);
                }
            }
            if (this.f6101a != null) {
                this.f6101a.mo3845b(arrayList);
            }
        }
    }
}
