package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qh4 extends ki4<fi4> {
    @DexIgnore
    public /* final */ oh4 i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qh4(fi4 fi4, oh4 oh4) {
        super(fi4);
        kd4.b(fi4, "job");
        kd4.b(oh4, "handle");
        this.i = oh4;
    }

    @DexIgnore
    public void b(Throwable th) {
        this.i.dispose();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return qa4.a;
    }

    @DexIgnore
    public String toString() {
        return "DisposeOnCompletion[" + this.i + ']';
    }
}
