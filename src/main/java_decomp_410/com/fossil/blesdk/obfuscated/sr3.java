package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sr3<T> extends LiveData<T> {
    @DexIgnore
    public static /* final */ a k; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final <T> LiveData<T> a() {
            return new sr3((fd4) null);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public sr3() {
        a(null);
    }

    @DexIgnore
    public /* synthetic */ sr3(fd4 fd4) {
        this();
    }
}
