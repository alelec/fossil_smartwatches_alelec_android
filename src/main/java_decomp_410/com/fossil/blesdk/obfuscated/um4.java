package com.fossil.blesdk.obfuscated;

import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class um4 {
    @DexIgnore
    public /* final */ Set<fm4> a; // = new LinkedHashSet();

    @DexIgnore
    public synchronized void a(fm4 fm4) {
        this.a.remove(fm4);
    }

    @DexIgnore
    public synchronized void b(fm4 fm4) {
        this.a.add(fm4);
    }

    @DexIgnore
    public synchronized boolean c(fm4 fm4) {
        return this.a.contains(fm4);
    }
}
