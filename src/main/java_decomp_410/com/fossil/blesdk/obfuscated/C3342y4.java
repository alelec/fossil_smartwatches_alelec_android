package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.y4 */
public class C3342y4 extends com.fossil.blesdk.obfuscated.C1998i5 {

    @DexIgnore
    /* renamed from: A0 */
    public int f11157A0; // = 0;

    @DexIgnore
    /* renamed from: B0 */
    public int f11158B0; // = 0;

    @DexIgnore
    /* renamed from: C0 */
    public int f11159C0; // = 7;

    @DexIgnore
    /* renamed from: D0 */
    public boolean f11160D0; // = false;

    @DexIgnore
    /* renamed from: E0 */
    public boolean f11161E0; // = false;

    @DexIgnore
    /* renamed from: F0 */
    public boolean f11162F0; // = false;

    @DexIgnore
    /* renamed from: l0 */
    public boolean f11163l0; // = false;

    @DexIgnore
    /* renamed from: m0 */
    public com.fossil.blesdk.obfuscated.C2703q4 f11164m0; // = new com.fossil.blesdk.obfuscated.C2703q4();

    @DexIgnore
    /* renamed from: n0 */
    public com.fossil.blesdk.obfuscated.C1924h5 f11165n0;

    @DexIgnore
    /* renamed from: o0 */
    public int f11166o0;

    @DexIgnore
    /* renamed from: p0 */
    public int f11167p0;

    @DexIgnore
    /* renamed from: q0 */
    public int f11168q0;

    @DexIgnore
    /* renamed from: r0 */
    public int f11169r0;

    @DexIgnore
    /* renamed from: s0 */
    public int f11170s0; // = 0;

    @DexIgnore
    /* renamed from: t0 */
    public int f11171t0; // = 0;

    @DexIgnore
    /* renamed from: u0 */
    public com.fossil.blesdk.obfuscated.C3261x4[] f11172u0; // = new com.fossil.blesdk.obfuscated.C3261x4[4];

    @DexIgnore
    /* renamed from: v0 */
    public com.fossil.blesdk.obfuscated.C3261x4[] f11173v0; // = new com.fossil.blesdk.obfuscated.C3261x4[4];

    @DexIgnore
    /* renamed from: w0 */
    public java.util.List<com.fossil.blesdk.obfuscated.C3413z4> f11174w0; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: x0 */
    public boolean f11175x0; // = false;

    @DexIgnore
    /* renamed from: y0 */
    public boolean f11176y0; // = false;

    @DexIgnore
    /* renamed from: z0 */
    public boolean f11177z0; // = false;

    @DexIgnore
    /* renamed from: E */
    public void mo1278E() {
        this.f11164m0.mo15035i();
        this.f11166o0 = 0;
        this.f11168q0 = 0;
        this.f11167p0 = 0;
        this.f11169r0 = 0;
        this.f11174w0.clear();
        this.f11160D0 = false;
        super.mo1278E();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v15, types: [boolean] */
    /* JADX WARNING: type inference failed for: r8v16 */
    /* JADX WARNING: type inference failed for: r8v17 */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0267  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0283  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0290  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x0293  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0186  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x018f  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01da  */
    /* renamed from: L */
    public void mo11826L() {
        int i;
        int i2;
        boolean z;
        boolean z2;
        char c;
        int i3;
        boolean z3;
        int max;
        int max2;
        Object r8;
        boolean z4;
        int i4 = this.f671I;
        int i5 = this.f672J;
        int max3 = java.lang.Math.max(0, mo1352t());
        int max4 = java.lang.Math.max(0, mo1332j());
        this.f11161E0 = false;
        this.f11162F0 = false;
        if (this.f666D != null) {
            if (this.f11165n0 == null) {
                this.f11165n0 = new com.fossil.blesdk.obfuscated.C1924h5(this);
            }
            this.f11165n0.mo11595b(this);
            mo1351s(this.f11166o0);
            mo1353t(this.f11167p0);
            mo1279F();
            mo1296a(this.f11164m0.mo15031e());
        } else {
            this.f671I = 0;
            this.f672J = 0;
        }
        int i6 = 32;
        if (this.f11159C0 != 0) {
            if (!mo18016u(8)) {
                mo18005T();
            }
            if (!mo18016u(32)) {
                mo18004S();
            }
            this.f11164m0.f8538g = true;
        } else {
            this.f11164m0.f8538g = false;
        }
        androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = this.f665C;
        androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour dimensionBehaviour = dimensionBehaviourArr[1];
        androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = dimensionBehaviourArr[0];
        mo18007V();
        if (this.f11174w0.size() == 0) {
            this.f11174w0.clear();
            this.f11174w0.add(0, new com.fossil.blesdk.obfuscated.C3413z4(this.f5944k0));
        }
        int size = this.f11174w0.size();
        java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> arrayList = this.f5944k0;
        boolean z5 = mo1334k() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || mo1348r() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        boolean z6 = false;
        int i7 = 0;
        while (i7 < size && !this.f11160D0) {
            if (this.f11174w0.get(i7).f11490d) {
                i = size;
            } else {
                if (mo18016u(i6)) {
                    if (mo1334k() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED && mo1348r() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED) {
                        this.f5944k0 = (java.util.ArrayList) this.f11174w0.get(i7).mo18435a();
                    } else {
                        this.f5944k0 = (java.util.ArrayList) this.f11174w0.get(i7).f11487a;
                    }
                }
                mo18007V();
                int size2 = this.f5944k0.size();
                for (int i8 = 0; i8 < size2; i8++) {
                    androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget = this.f5944k0.get(i8);
                    if (constraintWidget instanceof com.fossil.blesdk.obfuscated.C1998i5) {
                        ((com.fossil.blesdk.obfuscated.C1998i5) constraintWidget).mo11826L();
                    }
                }
                boolean z7 = z6;
                int i9 = 0;
                boolean z8 = true;
                while (z8) {
                    boolean z9 = z8;
                    int i10 = i9 + 1;
                    try {
                        this.f11164m0.mo15035i();
                        mo18007V();
                        mo1308b(this.f11164m0);
                        int i11 = 0;
                        while (i11 < size2) {
                            z = z7;
                            try {
                                this.f5944k0.get(i11).mo1308b(this.f11164m0);
                                i11++;
                                z7 = z;
                            } catch (java.lang.Exception e) {
                                e = e;
                                z4 = z9;
                                e.printStackTrace();
                                java.io.PrintStream printStream = java.lang.System.out;
                                z2 = z4;
                                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                                i2 = size;
                                sb.append("EXCEPTION : ");
                                sb.append(e);
                                printStream.println(sb.toString());
                                if (z2) {
                                }
                                c = 2;
                                if (z5) {
                                }
                                i3 = i10;
                                z7 = z;
                                z3 = false;
                                max = java.lang.Math.max(this.f680R, mo1352t());
                                if (max > mo1352t()) {
                                }
                                max2 = java.lang.Math.max(this.f681S, mo1332j());
                                if (max2 <= mo1332j()) {
                                }
                                if (!z7) {
                                }
                                z8 = z3;
                                i9 = i3;
                                size = i2;
                            }
                        }
                        z = z7;
                        z4 = mo18013d(this.f11164m0);
                        if (z4) {
                            try {
                                this.f11164m0.mo15033g();
                            } catch (java.lang.Exception e2) {
                                e = e2;
                            }
                        }
                        z2 = z4;
                        i2 = size;
                    } catch (java.lang.Exception e3) {
                        e = e3;
                        z = z7;
                        z4 = z9;
                        e.printStackTrace();
                        java.io.PrintStream printStream2 = java.lang.System.out;
                        z2 = z4;
                        java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
                        i2 = size;
                        sb2.append("EXCEPTION : ");
                        sb2.append(e);
                        printStream2.println(sb2.toString());
                        if (z2) {
                        }
                        c = 2;
                        if (z5) {
                        }
                        i3 = i10;
                        z7 = z;
                        z3 = false;
                        max = java.lang.Math.max(this.f680R, mo1352t());
                        if (max > mo1352t()) {
                        }
                        max2 = java.lang.Math.max(this.f681S, mo1332j());
                        if (max2 <= mo1332j()) {
                        }
                        if (!z7) {
                        }
                        z8 = z3;
                        i9 = i3;
                        size = i2;
                    }
                    if (z2) {
                        mo18010a(this.f11164m0, com.fossil.blesdk.obfuscated.C1536c5.f4002a);
                    } else {
                        mo1316c(this.f11164m0);
                        int i12 = 0;
                        while (true) {
                            if (i12 >= size2) {
                                break;
                            }
                            androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget2 = this.f5944k0.get(i12);
                            if (constraintWidget2.f665C[0] != androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget2.mo1352t() >= constraintWidget2.mo1356v()) {
                                if (constraintWidget2.f665C[1] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget2.mo1332j() < constraintWidget2.mo1355u()) {
                                    c = 2;
                                    com.fossil.blesdk.obfuscated.C1536c5.f4002a[2] = true;
                                    break;
                                }
                                i12++;
                            } else {
                                com.fossil.blesdk.obfuscated.C1536c5.f4002a[2] = true;
                                break;
                            }
                        }
                        if (z5 || i10 >= 8 || !com.fossil.blesdk.obfuscated.C1536c5.f4002a[c]) {
                            i3 = i10;
                            z7 = z;
                            z3 = false;
                        } else {
                            int i13 = 0;
                            int i14 = 0;
                            int i15 = 0;
                            while (i13 < size2) {
                                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget3 = this.f5944k0.get(i13);
                                i14 = java.lang.Math.max(i14, constraintWidget3.f671I + constraintWidget3.mo1352t());
                                i15 = java.lang.Math.max(i15, constraintWidget3.f672J + constraintWidget3.mo1332j());
                                i13++;
                                i10 = i10;
                            }
                            i3 = i10;
                            int max5 = java.lang.Math.max(this.f680R, i14);
                            int max6 = java.lang.Math.max(this.f681S, i15);
                            if (dimensionBehaviour2 != androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || mo1352t() >= max5) {
                                z3 = false;
                            } else {
                                mo1345p(max5);
                                this.f665C[0] = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                                z3 = true;
                                z = true;
                            }
                            if (dimensionBehaviour != androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || mo1332j() >= max6) {
                                z7 = z;
                            } else {
                                mo1329h(max6);
                                this.f665C[1] = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                                z3 = true;
                                z7 = true;
                            }
                        }
                        max = java.lang.Math.max(this.f680R, mo1352t());
                        if (max > mo1352t()) {
                            mo1345p(max);
                            this.f665C[0] = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED;
                            z3 = true;
                            z7 = true;
                        }
                        max2 = java.lang.Math.max(this.f681S, mo1332j());
                        if (max2 <= mo1332j()) {
                            mo1329h(max2);
                            r8 = 1;
                            this.f665C[1] = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED;
                            z3 = true;
                            z7 = true;
                        } else {
                            r8 = 1;
                        }
                        if (!z7) {
                            if (this.f665C[0] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && max3 > 0 && mo1352t() > max3) {
                                this.f11161E0 = r8;
                                this.f665C[0] = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED;
                                mo1345p(max3);
                                z3 = true;
                                z7 = true;
                            }
                            if (this.f665C[r8] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && max4 > 0 && mo1332j() > max4) {
                                this.f11162F0 = r8;
                                this.f665C[r8] = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED;
                                mo1329h(max4);
                                z8 = true;
                                z7 = true;
                                i9 = i3;
                                size = i2;
                            }
                        }
                        z8 = z3;
                        i9 = i3;
                        size = i2;
                    }
                    c = 2;
                    if (z5) {
                    }
                    i3 = i10;
                    z7 = z;
                    z3 = false;
                    max = java.lang.Math.max(this.f680R, mo1352t());
                    if (max > mo1352t()) {
                    }
                    max2 = java.lang.Math.max(this.f681S, mo1332j());
                    if (max2 <= mo1332j()) {
                    }
                    if (!z7) {
                    }
                    z8 = z3;
                    i9 = i3;
                    size = i2;
                }
                i = size;
                this.f11174w0.get(i7).mo18441b();
                z6 = z7;
            }
            i7++;
            size = i;
            i6 = 32;
        }
        this.f5944k0 = arrayList;
        if (this.f666D != null) {
            int max7 = java.lang.Math.max(this.f680R, mo1352t());
            int max8 = java.lang.Math.max(this.f681S, mo1332j());
            this.f11165n0.mo11594a(this);
            mo1345p(max7 + this.f11166o0 + this.f11168q0);
            mo1329h(max8 + this.f11167p0 + this.f11169r0);
        } else {
            this.f671I = i4;
            this.f672J = i5;
        }
        if (z6) {
            androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr2 = this.f665C;
            dimensionBehaviourArr2[0] = dimensionBehaviour2;
            dimensionBehaviourArr2[1] = dimensionBehaviour;
        }
        mo1296a(this.f11164m0.mo15031e());
        if (this == mo11825K()) {
            mo1282I();
        }
    }

    @DexIgnore
    /* renamed from: N */
    public int mo17999N() {
        return this.f11159C0;
    }

    @DexIgnore
    /* renamed from: O */
    public boolean mo18000O() {
        return false;
    }

    @DexIgnore
    /* renamed from: P */
    public boolean mo18001P() {
        return this.f11162F0;
    }

    @DexIgnore
    /* renamed from: Q */
    public boolean mo18002Q() {
        return this.f11163l0;
    }

    @DexIgnore
    /* renamed from: R */
    public boolean mo18003R() {
        return this.f11161E0;
    }

    @DexIgnore
    /* renamed from: S */
    public void mo18004S() {
        if (!mo18016u(8)) {
            mo1287a(this.f11159C0);
        }
        mo18008W();
    }

    @DexIgnore
    /* renamed from: T */
    public void mo18005T() {
        int size = this.f5944k0.size();
        mo1280G();
        for (int i = 0; i < size; i++) {
            this.f5944k0.get(i).mo1280G();
        }
    }

    @DexIgnore
    /* renamed from: U */
    public void mo18006U() {
        mo18005T();
        mo1287a(this.f11159C0);
    }

    @DexIgnore
    /* renamed from: V */
    public final void mo18007V() {
        this.f11170s0 = 0;
        this.f11171t0 = 0;
    }

    @DexIgnore
    /* renamed from: W */
    public void mo18008W() {
        com.fossil.blesdk.obfuscated.C1703e5 d = mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT).mo1266d();
        com.fossil.blesdk.obfuscated.C1703e5 d2 = mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP).mo1266d();
        d.mo10325a((com.fossil.blesdk.obfuscated.C1703e5) null, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        d2.mo10325a((com.fossil.blesdk.obfuscated.C1703e5) null, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18010a(com.fossil.blesdk.obfuscated.C2703q4 q4Var, boolean[] zArr) {
        zArr[2] = false;
        mo1316c(q4Var);
        int size = this.f5944k0.size();
        for (int i = 0; i < size; i++) {
            androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget = this.f5944k0.get(i);
            constraintWidget.mo1316c(q4Var);
            if (constraintWidget.f665C[0] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.mo1352t() < constraintWidget.mo1356v()) {
                zArr[2] = true;
            }
            if (constraintWidget.f665C[1] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.mo1332j() < constraintWidget.mo1355u()) {
                zArr[2] = true;
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo18011c(boolean z) {
        this.f11163l0 = z;
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo18013d(com.fossil.blesdk.obfuscated.C2703q4 q4Var) {
        mo1297a(q4Var);
        int size = this.f5944k0.size();
        for (int i = 0; i < size; i++) {
            androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget = this.f5944k0.get(i);
            if (constraintWidget instanceof com.fossil.blesdk.obfuscated.C3342y4) {
                androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = constraintWidget.f665C;
                androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour dimensionBehaviour = dimensionBehaviourArr[0];
                androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = dimensionBehaviourArr[1];
                if (dimensionBehaviour == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    constraintWidget.mo1293a(androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED);
                }
                if (dimensionBehaviour2 == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    constraintWidget.mo1307b(androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED);
                }
                constraintWidget.mo1297a(q4Var);
                if (dimensionBehaviour == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    constraintWidget.mo1293a(dimensionBehaviour);
                }
                if (dimensionBehaviour2 == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    constraintWidget.mo1307b(dimensionBehaviour2);
                }
            } else {
                com.fossil.blesdk.obfuscated.C1536c5.m5285a(this, q4Var, constraintWidget);
                constraintWidget.mo1297a(q4Var);
            }
        }
        if (this.f11170s0 > 0) {
            com.fossil.blesdk.obfuscated.C3167w4.m15576a(this, q4Var, 0);
        }
        if (this.f11171t0 > 0) {
            com.fossil.blesdk.obfuscated.C3167w4.m15576a(this, q4Var, 1);
        }
        return true;
    }

    @DexIgnore
    /* renamed from: e */
    public final void mo18014e(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
        int i = this.f11171t0 + 1;
        com.fossil.blesdk.obfuscated.C3261x4[] x4VarArr = this.f11172u0;
        if (i >= x4VarArr.length) {
            this.f11172u0 = (com.fossil.blesdk.obfuscated.C3261x4[]) java.util.Arrays.copyOf(x4VarArr, x4VarArr.length * 2);
        }
        this.f11172u0[this.f11171t0] = new com.fossil.blesdk.obfuscated.C3261x4(constraintWidget, 1, mo18002Q());
        this.f11171t0++;
    }

    @DexIgnore
    /* renamed from: f */
    public void mo18015f(int i, int i2) {
        if (this.f665C[0] != androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
            com.fossil.blesdk.obfuscated.C1770f5 f5Var = this.f693c;
            if (f5Var != null) {
                f5Var.mo10739a(i);
            }
        }
        if (this.f665C[1] != androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
            com.fossil.blesdk.obfuscated.C1770f5 f5Var2 = this.f695d;
            if (f5Var2 != null) {
                f5Var2.mo10739a(i2);
            }
        }
    }

    @DexIgnore
    /* renamed from: u */
    public boolean mo18016u(int i) {
        return (this.f11159C0 & i) == i;
    }

    @DexIgnore
    /* renamed from: v */
    public void mo18017v(int i) {
        this.f11159C0 = i;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1287a(int i) {
        super.mo1287a(i);
        int size = this.f5944k0.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.f5944k0.get(i2).mo1287a(i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18009a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget, int i) {
        if (i == 0) {
            mo18012d(constraintWidget);
        } else if (i == 1) {
            mo18014e(constraintWidget);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo18012d(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
        int i = this.f11170s0 + 1;
        com.fossil.blesdk.obfuscated.C3261x4[] x4VarArr = this.f11173v0;
        if (i >= x4VarArr.length) {
            this.f11173v0 = (com.fossil.blesdk.obfuscated.C3261x4[]) java.util.Arrays.copyOf(x4VarArr, x4VarArr.length * 2);
        }
        this.f11173v0[this.f11170s0] = new com.fossil.blesdk.obfuscated.C3261x4(constraintWidget, 0, mo18002Q());
        this.f11170s0++;
    }
}
