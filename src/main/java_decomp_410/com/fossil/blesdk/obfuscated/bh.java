package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Matrix;
import android.os.Build;
import android.util.Log;
import android.widget.ImageView;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bh {
    @DexIgnore
    public static Method a;
    @DexIgnore
    public static boolean b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ ImageView a;

        @DexIgnore
        public a(ImageView imageView) {
            this.a = imageView;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ImageView.ScaleType scaleType = (ImageView.ScaleType) this.a.getTag(gh.save_scale_type);
            this.a.setScaleType(scaleType);
            this.a.setTag(gh.save_scale_type, (Object) null);
            if (scaleType == ImageView.ScaleType.MATRIX) {
                ImageView imageView = this.a;
                imageView.setImageMatrix((Matrix) imageView.getTag(gh.save_image_matrix));
                this.a.setTag(gh.save_image_matrix, (Object) null);
            }
            animator.removeListener(this);
        }
    }

    @DexIgnore
    public static void a(ImageView imageView) {
        if (Build.VERSION.SDK_INT < 21) {
            ImageView.ScaleType scaleType = imageView.getScaleType();
            imageView.setTag(gh.save_scale_type, scaleType);
            ImageView.ScaleType scaleType2 = ImageView.ScaleType.MATRIX;
            if (scaleType == scaleType2) {
                imageView.setTag(gh.save_image_matrix, imageView.getImageMatrix());
            } else {
                imageView.setScaleType(scaleType2);
            }
            imageView.setImageMatrix(ch.a);
        }
    }

    @DexIgnore
    public static void a(ImageView imageView, Matrix matrix) {
        if (Build.VERSION.SDK_INT < 21) {
            imageView.setImageMatrix(matrix);
            return;
        }
        a();
        Method method = a;
        if (method != null) {
            try {
                method.invoke(imageView, new Object[]{matrix});
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e.getCause());
            }
        }
    }

    @DexIgnore
    public static void a() {
        if (!b) {
            try {
                a = ImageView.class.getDeclaredMethod("animateTransform", new Class[]{Matrix.class});
                a.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ImageViewUtils", "Failed to retrieve animateTransform method", e);
            }
            b = true;
        }
    }

    @DexIgnore
    public static void a(ImageView imageView, Animator animator) {
        if (Build.VERSION.SDK_INT < 21) {
            animator.addListener(new a(imageView));
        }
    }
}
