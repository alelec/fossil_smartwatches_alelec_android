package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.uf */
public class C3035uf implements com.fossil.blesdk.obfuscated.C2125jg, com.fossil.blesdk.obfuscated.C2019ig {

    @DexIgnore
    /* renamed from: m */
    public static /* final */ java.util.TreeMap<java.lang.Integer, com.fossil.blesdk.obfuscated.C3035uf> f9948m; // = new java.util.TreeMap<>();

    @DexIgnore
    /* renamed from: e */
    public volatile java.lang.String f9949e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ long[] f9950f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ double[] f9951g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ java.lang.String[] f9952h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ byte[][] f9953i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ int[] f9954j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ int f9955k;

    @DexIgnore
    /* renamed from: l */
    public int f9956l;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uf$a")
    /* renamed from: com.fossil.blesdk.obfuscated.uf$a */
    public static class C3036a implements com.fossil.blesdk.obfuscated.C2019ig {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3035uf f9957e;

        @DexIgnore
        public C3036a(com.fossil.blesdk.obfuscated.C3035uf ufVar) {
            this.f9957e = ufVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11930a(int i) {
            this.f9957e.mo11930a(i);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo11934b(int i, long j) {
            this.f9957e.mo11934b(i, j);
        }

        @DexIgnore
        public void close() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11931a(int i, double d) {
            this.f9957e.mo11931a(i, d);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11932a(int i, java.lang.String str) {
            this.f9957e.mo11932a(i, str);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11933a(int i, byte[] bArr) {
            this.f9957e.mo11933a(i, bArr);
        }
    }

    @DexIgnore
    public C3035uf(int i) {
        this.f9955k = i;
        int i2 = i + 1;
        this.f9954j = new int[i2];
        this.f9950f = new long[i2];
        this.f9951g = new double[i2];
        this.f9952h = new java.lang.String[i2];
        this.f9953i = new byte[i2][];
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3035uf m14728a(com.fossil.blesdk.obfuscated.C2125jg jgVar) {
        com.fossil.blesdk.obfuscated.C3035uf b = m14729b(jgVar.mo10920b(), jgVar.mo10918a());
        jgVar.mo10919a(new com.fossil.blesdk.obfuscated.C3035uf.C3036a(b));
        return b;
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C3035uf m14729b(java.lang.String str, int i) {
        synchronized (f9948m) {
            java.util.Map.Entry<java.lang.Integer, com.fossil.blesdk.obfuscated.C3035uf> ceilingEntry = f9948m.ceilingEntry(java.lang.Integer.valueOf(i));
            if (ceilingEntry != null) {
                f9948m.remove(ceilingEntry.getKey());
                com.fossil.blesdk.obfuscated.C3035uf value = ceilingEntry.getValue();
                value.mo16766a(str, i);
                return value;
            }
            com.fossil.blesdk.obfuscated.C3035uf ufVar = new com.fossil.blesdk.obfuscated.C3035uf(i);
            ufVar.mo16766a(str, i);
            return ufVar;
        }
    }

    @DexIgnore
    /* renamed from: d */
    public static void m14730d() {
        if (f9948m.size() > 15) {
            int size = f9948m.size() - 10;
            java.util.Iterator<java.lang.Integer> it = f9948m.descendingKeySet().iterator();
            while (true) {
                int i = size - 1;
                if (size > 0) {
                    it.next();
                    it.remove();
                    size = i;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo16767c() {
        synchronized (f9948m) {
            f9948m.put(java.lang.Integer.valueOf(this.f9955k), this);
            m14730d();
        }
    }

    @DexIgnore
    public void close() {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16766a(java.lang.String str, int i) {
        this.f9949e = str;
        this.f9956l = i;
    }

    @DexIgnore
    /* renamed from: a */
    public int mo10918a() {
        return this.f9956l;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10919a(com.fossil.blesdk.obfuscated.C2019ig igVar) {
        for (int i = 1; i <= this.f9956l; i++) {
            int i2 = this.f9954j[i];
            if (i2 == 1) {
                igVar.mo11930a(i);
            } else if (i2 == 2) {
                igVar.mo11934b(i, this.f9950f[i]);
            } else if (i2 == 3) {
                igVar.mo11931a(i, this.f9951g[i]);
            } else if (i2 == 4) {
                igVar.mo11932a(i, this.f9952h[i]);
            } else if (i2 == 5) {
                igVar.mo11933a(i, this.f9953i[i]);
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.String mo10920b() {
        return this.f9949e;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11934b(int i, long j) {
        this.f9954j[i] = 2;
        this.f9950f[i] = j;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11930a(int i) {
        this.f9954j[i] = 1;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11931a(int i, double d) {
        this.f9954j[i] = 3;
        this.f9951g[i] = d;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11932a(int i, java.lang.String str) {
        this.f9954j[i] = 4;
        this.f9952h[i] = str;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11933a(int i, byte[] bArr) {
        this.f9954j[i] = 5;
        this.f9953i[i] = bArr;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16765a(com.fossil.blesdk.obfuscated.C3035uf ufVar) {
        int a = ufVar.mo10918a() + 1;
        java.lang.System.arraycopy(ufVar.f9954j, 0, this.f9954j, 0, a);
        java.lang.System.arraycopy(ufVar.f9950f, 0, this.f9950f, 0, a);
        java.lang.System.arraycopy(ufVar.f9952h, 0, this.f9952h, 0, a);
        java.lang.System.arraycopy(ufVar.f9953i, 0, this.f9953i, 0, a);
        java.lang.System.arraycopy(ufVar.f9951g, 0, this.f9951g, 0, a);
    }
}
