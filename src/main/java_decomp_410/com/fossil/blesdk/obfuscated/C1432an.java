package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.an */
public class C1432an implements com.fossil.blesdk.obfuscated.C2748qm {

    @DexIgnore
    /* renamed from: c */
    public static /* final */ boolean f3542c; // = com.fossil.blesdk.obfuscated.C3296xm.f11007b;

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C3445zm f3543a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1494bn f3544b;

    @DexIgnore
    @java.lang.Deprecated
    public C1432an(com.fossil.blesdk.obfuscated.C1882gn gnVar) {
        this(gnVar, new com.fossil.blesdk.obfuscated.C1494bn(4096));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005d, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005e, code lost:
        r15 = null;
        r19 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a9, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b2, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00b3, code lost:
        r1 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b5, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b6, code lost:
        r19 = r1;
        r15 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ba, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00bb, code lost:
        r19 = r1;
        r12 = null;
        r15 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00c1, code lost:
        r0 = r12.mo10973d();
        com.fossil.blesdk.obfuscated.C3296xm.m16422c("Unexpected response code %d for %s", java.lang.Integer.valueOf(r0), r29.getUrl());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00da, code lost:
        if (r15 != null) goto L_0x00dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00dc, code lost:
        r13 = new com.fossil.blesdk.obfuscated.C2897sm(r0, r15, false, android.os.SystemClock.elapsedRealtime() - r9, r19);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00ed, code lost:
        if (r0 == 401) goto L_0x0129;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f6, code lost:
        if (r0 < 400) goto L_0x0103;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0102, code lost:
        throw new com.android.volley.ClientError(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0105, code lost:
        if (r0 < 500) goto L_0x0123;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x010f, code lost:
        if (r29.shouldRetryServerErrors() != false) goto L_0x0111;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0111, code lost:
        m4558a("server", r8, new com.android.volley.ServerError(r13));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0122, code lost:
        throw new com.android.volley.ServerError(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0128, code lost:
        throw new com.android.volley.ServerError(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0129, code lost:
        m4558a("auth", r8, new com.android.volley.AuthFailureError(r13));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0135, code lost:
        m4558a("network", r8, new com.android.volley.NetworkError());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0146, code lost:
        throw new com.android.volley.NoConnectionError(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0147, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0162, code lost:
        throw new java.lang.RuntimeException("Bad URL " + r29.getUrl(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0163, code lost:
        m4558a("socket", r8, new com.android.volley.TimeoutError());
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0147 A[ExcHandler: MalformedURLException (r0v1 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:2:0x000e] */
    /* JADX WARNING: Removed duplicated region for block: B:79:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), SYNTHETIC, Splitter:B:2:0x000e] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0141 A[SYNTHETIC] */
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2897sm mo8843a(com.android.volley.Request<?> request) throws com.android.volley.VolleyError {
        com.fossil.blesdk.obfuscated.C1815fn fnVar;
        java.util.List<com.fossil.blesdk.obfuscated.C2660pm> c;
        byte[] a;
        java.util.List<com.fossil.blesdk.obfuscated.C2660pm> list;
        com.android.volley.Request<?> request2 = request;
        long elapsedRealtime = android.os.SystemClock.elapsedRealtime();
        while (true) {
            java.util.List<com.fossil.blesdk.obfuscated.C2660pm> emptyList = java.util.Collections.emptyList();
            try {
                fnVar = this.f3543a.mo11693b(request2, mo8844a(request.getCacheEntry()));
                int d = fnVar.mo10973d();
                c = fnVar.mo10972c();
                if (d == 304) {
                    com.fossil.blesdk.obfuscated.C2334lm.C2335a cacheEntry = request.getCacheEntry();
                    if (cacheEntry == null) {
                        com.fossil.blesdk.obfuscated.C2897sm smVar = new com.fossil.blesdk.obfuscated.C2897sm(304, (byte[]) null, true, android.os.SystemClock.elapsedRealtime() - elapsedRealtime, c);
                        return smVar;
                    }
                    com.fossil.blesdk.obfuscated.C2897sm smVar2 = new com.fossil.blesdk.obfuscated.C2897sm(304, cacheEntry.f7239a, true, android.os.SystemClock.elapsedRealtime() - elapsedRealtime, m4557a(c, cacheEntry));
                    return smVar2;
                }
                java.io.InputStream a2 = fnVar.mo10970a();
                a = a2 != null ? mo8846a(a2, fnVar.mo10971b()) : new byte[0];
                mo8845a(android.os.SystemClock.elapsedRealtime() - elapsedRealtime, request, a, d);
                if (d < 200 || d > 299) {
                    java.util.List<com.fossil.blesdk.obfuscated.C2660pm> list2 = c;
                } else {
                    list = c;
                    r13 = r13;
                    com.fossil.blesdk.obfuscated.C2897sm smVar3 = new com.fossil.blesdk.obfuscated.C2897sm(d, a, false, android.os.SystemClock.elapsedRealtime() - elapsedRealtime, list);
                    return smVar3;
                }
            } catch (java.net.SocketTimeoutException unused) {
            } catch (java.net.MalformedURLException e) {
            } catch (java.io.IOException e2) {
                e = e2;
                list = c;
                java.util.List<com.fossil.blesdk.obfuscated.C2660pm> list3 = list;
                byte[] bArr = a;
                if (fnVar == null) {
                }
            }
        }
        java.util.List<com.fossil.blesdk.obfuscated.C2660pm> list22 = c;
        throw new java.io.IOException();
    }

    @DexIgnore
    @java.lang.Deprecated
    public C1432an(com.fossil.blesdk.obfuscated.C1882gn gnVar, com.fossil.blesdk.obfuscated.C1494bn bnVar) {
        this.f3543a = new com.fossil.blesdk.obfuscated.C3370ym(gnVar);
        this.f3544b = bnVar;
    }

    @DexIgnore
    public C1432an(com.fossil.blesdk.obfuscated.C3445zm zmVar) {
        this(zmVar, new com.fossil.blesdk.obfuscated.C1494bn(4096));
    }

    @DexIgnore
    public C1432an(com.fossil.blesdk.obfuscated.C3445zm zmVar, com.fossil.blesdk.obfuscated.C1494bn bnVar) {
        this.f3543a = zmVar;
        this.f3544b = bnVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo8845a(long j, com.android.volley.Request<?> request, byte[] bArr, int i) {
        if (f3542c || j > 3000) {
            java.lang.Object[] objArr = new java.lang.Object[5];
            objArr[0] = request;
            objArr[1] = java.lang.Long.valueOf(j);
            objArr[2] = bArr != null ? java.lang.Integer.valueOf(bArr.length) : "null";
            objArr[3] = java.lang.Integer.valueOf(i);
            objArr[4] = java.lang.Integer.valueOf(request.getRetryPolicy().mo14042b());
            com.fossil.blesdk.obfuscated.C3296xm.m16421b("HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]", objArr);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4558a(java.lang.String str, com.android.volley.Request<?> request, com.android.volley.VolleyError volleyError) throws com.android.volley.VolleyError {
        com.fossil.blesdk.obfuscated.C3229wm retryPolicy = request.getRetryPolicy();
        int timeoutMs = request.getTimeoutMs();
        try {
            retryPolicy.mo14041a(volleyError);
            request.addMarker(java.lang.String.format("%s-retry [timeout=%s]", new java.lang.Object[]{str, java.lang.Integer.valueOf(timeoutMs)}));
        } catch (com.android.volley.VolleyError e) {
            request.addMarker(java.lang.String.format("%s-timeout-giveup [timeout=%s]", new java.lang.Object[]{str, java.lang.Integer.valueOf(timeoutMs)}));
            throw e;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final java.util.Map<java.lang.String, java.lang.String> mo8844a(com.fossil.blesdk.obfuscated.C2334lm.C2335a aVar) {
        if (aVar == null) {
            return java.util.Collections.emptyMap();
        }
        java.util.HashMap hashMap = new java.util.HashMap();
        java.lang.String str = aVar.f7240b;
        if (str != null) {
            hashMap.put("If-None-Match", str);
        }
        long j = aVar.f7242d;
        if (j > 0) {
            hashMap.put("If-Modified-Since", com.fossil.blesdk.obfuscated.C1738en.m6519a(j));
        }
        return hashMap;
    }

    @DexIgnore
    /* renamed from: a */
    public final byte[] mo8846a(java.io.InputStream inputStream, int i) throws java.io.IOException, com.android.volley.ServerError {
        com.fossil.blesdk.obfuscated.C2426mn mnVar = new com.fossil.blesdk.obfuscated.C2426mn(this.f3544b, i);
        byte[] bArr = null;
        if (inputStream != null) {
            try {
                bArr = this.f3544b.mo9233a(1024);
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    mnVar.write(bArr, 0, read);
                }
                return mnVar.toByteArray();
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (java.io.IOException unused) {
                        com.fossil.blesdk.obfuscated.C3296xm.m16423d("Error occurred when closing InputStream", new java.lang.Object[0]);
                    }
                }
                this.f3544b.mo9232a(bArr);
                mnVar.close();
            }
        } else {
            throw new com.android.volley.ServerError();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.List<com.fossil.blesdk.obfuscated.C2660pm> m4557a(java.util.List<com.fossil.blesdk.obfuscated.C2660pm> list, com.fossil.blesdk.obfuscated.C2334lm.C2335a aVar) {
        java.util.TreeSet treeSet = new java.util.TreeSet(java.lang.String.CASE_INSENSITIVE_ORDER);
        if (!list.isEmpty()) {
            for (com.fossil.blesdk.obfuscated.C2660pm a : list) {
                treeSet.add(a.mo14804a());
            }
        }
        java.util.ArrayList arrayList = new java.util.ArrayList(list);
        java.util.List<com.fossil.blesdk.obfuscated.C2660pm> list2 = aVar.f7246h;
        if (list2 != null) {
            if (!list2.isEmpty()) {
                for (com.fossil.blesdk.obfuscated.C2660pm next : aVar.f7246h) {
                    if (!treeSet.contains(next.mo14804a())) {
                        arrayList.add(next);
                    }
                }
            }
        } else if (!aVar.f7245g.isEmpty()) {
            for (java.util.Map.Entry next2 : aVar.f7245g.entrySet()) {
                if (!treeSet.contains(next2.getKey())) {
                    arrayList.add(new com.fossil.blesdk.obfuscated.C2660pm((java.lang.String) next2.getKey(), (java.lang.String) next2.getValue()));
                }
            }
        }
        return arrayList;
    }
}
