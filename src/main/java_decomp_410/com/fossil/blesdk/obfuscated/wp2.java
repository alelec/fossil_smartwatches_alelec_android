package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.service.notification.HybridMessageNotificationComponent;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wp2 implements Factory<HybridMessageNotificationComponent> {
    @DexIgnore
    public static /* final */ wp2 a; // = new wp2();

    @DexIgnore
    public static wp2 a() {
        return a;
    }

    @DexIgnore
    public static HybridMessageNotificationComponent b() {
        return new HybridMessageNotificationComponent();
    }

    @DexIgnore
    public HybridMessageNotificationComponent get() {
        return b();
    }
}
