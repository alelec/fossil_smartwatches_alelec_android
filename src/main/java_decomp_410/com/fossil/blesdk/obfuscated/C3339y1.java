package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.y1 */
public class C3339y1 extends android.graphics.drawable.Drawable {

    @DexIgnore
    /* renamed from: a */
    public /* final */ androidx.appcompat.widget.ActionBarContainer f11141a;

    @DexIgnore
    public C3339y1(androidx.appcompat.widget.ActionBarContainer actionBarContainer) {
        this.f11141a = actionBarContainer;
    }

    @DexIgnore
    public void draw(android.graphics.Canvas canvas) {
        androidx.appcompat.widget.ActionBarContainer actionBarContainer = this.f11141a;
        if (actionBarContainer.f275l) {
            android.graphics.drawable.Drawable drawable = actionBarContainer.f274k;
            if (drawable != null) {
                drawable.draw(canvas);
                return;
            }
            return;
        }
        android.graphics.drawable.Drawable drawable2 = actionBarContainer.f272i;
        if (drawable2 != null) {
            drawable2.draw(canvas);
        }
        androidx.appcompat.widget.ActionBarContainer actionBarContainer2 = this.f11141a;
        android.graphics.drawable.Drawable drawable3 = actionBarContainer2.f273j;
        if (drawable3 != null && actionBarContainer2.f276m) {
            drawable3.draw(canvas);
        }
    }

    @DexIgnore
    public int getOpacity() {
        return 0;
    }

    @DexIgnore
    public void getOutline(android.graphics.Outline outline) {
        androidx.appcompat.widget.ActionBarContainer actionBarContainer = this.f11141a;
        if (actionBarContainer.f275l) {
            android.graphics.drawable.Drawable drawable = actionBarContainer.f274k;
            if (drawable != null) {
                drawable.getOutline(outline);
                return;
            }
            return;
        }
        android.graphics.drawable.Drawable drawable2 = actionBarContainer.f272i;
        if (drawable2 != null) {
            drawable2.getOutline(outline);
        }
    }

    @DexIgnore
    public void setAlpha(int i) {
    }

    @DexIgnore
    public void setColorFilter(android.graphics.ColorFilter colorFilter) {
    }
}
