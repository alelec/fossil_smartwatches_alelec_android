package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.mp;
import com.fossil.blesdk.obfuscated.so;
import com.fossil.blesdk.obfuscated.sr;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jp implements mp, so.a<Object> {
    @DexIgnore
    public /* final */ List<jo> e;
    @DexIgnore
    public /* final */ np<?> f;
    @DexIgnore
    public /* final */ mp.a g;
    @DexIgnore
    public int h;
    @DexIgnore
    public jo i;
    @DexIgnore
    public List<sr<File, ?>> j;
    @DexIgnore
    public int k;
    @DexIgnore
    public volatile sr.a<?> l;
    @DexIgnore
    public File m;

    @DexIgnore
    public jp(np<?> npVar, mp.a aVar) {
        this(npVar.c(), npVar, aVar);
    }

    @DexIgnore
    public boolean a() {
        while (true) {
            boolean z = false;
            if (this.j == null || !b()) {
                this.h++;
                if (this.h >= this.e.size()) {
                    return false;
                }
                jo joVar = this.e.get(this.h);
                this.m = this.f.d().a(new kp(joVar, this.f.l()));
                File file = this.m;
                if (file != null) {
                    this.i = joVar;
                    this.j = this.f.a(file);
                    this.k = 0;
                }
            } else {
                this.l = null;
                while (!z && b()) {
                    List<sr<File, ?>> list = this.j;
                    int i2 = this.k;
                    this.k = i2 + 1;
                    this.l = list.get(i2).a(this.m, this.f.n(), this.f.f(), this.f.i());
                    if (this.l != null && this.f.c(this.l.c.getDataClass())) {
                        this.l.c.a(this.f.j(), this);
                        z = true;
                    }
                }
                return z;
            }
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.k < this.j.size();
    }

    @DexIgnore
    public void cancel() {
        sr.a<?> aVar = this.l;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @DexIgnore
    public jp(List<jo> list, np<?> npVar, mp.a aVar) {
        this.h = -1;
        this.e = list;
        this.f = npVar;
        this.g = aVar;
    }

    @DexIgnore
    public void a(Object obj) {
        this.g.a(this.i, obj, this.l.c, DataSource.DATA_DISK_CACHE, this.i);
    }

    @DexIgnore
    public void a(Exception exc) {
        this.g.a(this.i, exc, this.l.c, DataSource.DATA_DISK_CACHE);
    }
}
