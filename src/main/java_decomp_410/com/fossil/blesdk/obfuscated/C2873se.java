package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.se */
public class C2873se extends androidx.recyclerview.widget.RecyclerView.C0251v {

    @DexIgnore
    /* renamed from: i */
    public /* final */ android.view.animation.LinearInterpolator f9258i; // = new android.view.animation.LinearInterpolator();

    @DexIgnore
    /* renamed from: j */
    public /* final */ android.view.animation.DecelerateInterpolator f9259j; // = new android.view.animation.DecelerateInterpolator();

    @DexIgnore
    /* renamed from: k */
    public android.graphics.PointF f9260k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ float f9261l;

    @DexIgnore
    /* renamed from: m */
    public int f9262m; // = 0;

    @DexIgnore
    /* renamed from: n */
    public int f9263n; // = 0;

    @DexIgnore
    public C2873se(android.content.Context context) {
        this.f9261l = mo9171a(context.getResources().getDisplayMetrics());
    }

    @DexIgnore
    /* renamed from: a */
    public void mo3016a(android.view.View view, androidx.recyclerview.widget.RecyclerView.State state, androidx.recyclerview.widget.RecyclerView.C0251v.C0252a aVar) {
        int a = mo15947a(view, mo15953i());
        int b = mo15950b(view, mo15954j());
        int d = mo15951d((int) java.lang.Math.sqrt((double) ((a * a) + (b * b))));
        if (d > 0) {
            aVar.mo3029a(-a, -b, d, this.f9259j);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final int mo15949b(int i, int i2) {
        int i3 = i - i2;
        if (i * i3 <= 0) {
            return 0;
        }
        return i3;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo15950b(android.view.View view, int i) {
        androidx.recyclerview.widget.RecyclerView.C0236m b = mo3019b();
        if (b == null || !b.mo2437b()) {
            return 0;
        }
        androidx.recyclerview.widget.RecyclerView.LayoutParams layoutParams = (androidx.recyclerview.widget.RecyclerView.LayoutParams) view.getLayoutParams();
        return mo15946a(b.mo2958j(view) - layoutParams.topMargin, b.mo2942e(view) + layoutParams.bottomMargin, b.mo2970q(), b.mo2952h() - b.mo2965n(), i);
    }

    @DexIgnore
    /* renamed from: d */
    public int mo15951d(int i) {
        return (int) java.lang.Math.ceil(((double) mo15952e(i)) / 0.3356d);
    }

    @DexIgnore
    /* renamed from: e */
    public int mo15952e(int i) {
        return (int) java.lang.Math.ceil((double) (((float) java.lang.Math.abs(i)) * this.f9261l));
    }

    @DexIgnore
    /* renamed from: f */
    public void mo3025f() {
    }

    @DexIgnore
    /* renamed from: g */
    public void mo3026g() {
        this.f9263n = 0;
        this.f9262m = 0;
        this.f9260k = null;
    }

    @DexIgnore
    /* renamed from: i */
    public int mo15953i() {
        android.graphics.PointF pointF = this.f9260k;
        if (pointF != null) {
            float f = pointF.x;
            if (f != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                return f > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : -1;
            }
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: j */
    public int mo15954j() {
        android.graphics.PointF pointF = this.f9260k;
        if (pointF != null) {
            float f = pointF.y;
            if (f != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                return f > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : -1;
            }
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo3014a(int i, int i2, androidx.recyclerview.widget.RecyclerView.State state, androidx.recyclerview.widget.RecyclerView.C0251v.C0252a aVar) {
        if (mo3010a() == 0) {
            mo3027h();
            return;
        }
        this.f9262m = mo15949b(this.f9262m, i);
        this.f9263n = mo15949b(this.f9263n, i2);
        if (this.f9262m == 0 && this.f9263n == 0) {
            mo15948a(aVar);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public float mo9171a(android.util.DisplayMetrics displayMetrics) {
        return 25.0f / ((float) displayMetrics.densityDpi);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15948a(androidx.recyclerview.widget.RecyclerView.C0251v.C0252a aVar) {
        android.graphics.PointF a = mo3012a(mo3021c());
        if (a == null || (a.x == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && a.y == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
            aVar.mo3028a(mo3021c());
            mo3027h();
            return;
        }
        mo3015a(a);
        this.f9260k = a;
        this.f9262m = (int) (a.x * 10000.0f);
        this.f9263n = (int) (a.y * 10000.0f);
        aVar.mo3029a((int) (((float) this.f9262m) * 1.2f), (int) (((float) this.f9263n) * 1.2f), (int) (((float) mo15952e(10000)) * 1.2f), this.f9258i);
    }

    @DexIgnore
    /* renamed from: a */
    public int mo15946a(int i, int i2, int i3, int i4, int i5) {
        if (i5 == -1) {
            return i3 - i;
        }
        if (i5 == 0) {
            int i6 = i3 - i;
            if (i6 > 0) {
                return i6;
            }
            int i7 = i4 - i2;
            if (i7 < 0) {
                return i7;
            }
            return 0;
        } else if (i5 == 1) {
            return i4 - i2;
        } else {
            throw new java.lang.IllegalArgumentException("snap preference should be one of the constants defined in SmoothScroller, starting with SNAP_");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int mo15947a(android.view.View view, int i) {
        androidx.recyclerview.widget.RecyclerView.C0236m b = mo3019b();
        if (b == null || !b.mo2426a()) {
            return 0;
        }
        androidx.recyclerview.widget.RecyclerView.LayoutParams layoutParams = (androidx.recyclerview.widget.RecyclerView.LayoutParams) view.getLayoutParams();
        return mo15946a(b.mo2945f(view) - layoutParams.leftMargin, b.mo2956i(view) + layoutParams.rightMargin, b.mo2967o(), b.mo2971r() - b.mo2969p(), i);
    }
}
