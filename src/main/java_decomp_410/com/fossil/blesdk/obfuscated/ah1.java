package com.fossil.blesdk.obfuscated;

import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ah1 implements Runnable {
    @DexIgnore
    public /* final */ zg1 e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ Throwable g;
    @DexIgnore
    public /* final */ byte[] h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ Map<String, List<String>> j;

    @DexIgnore
    public ah1(String str, zg1 zg1, int i2, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        bk0.a(zg1);
        this.e = zg1;
        this.f = i2;
        this.g = th;
        this.h = bArr;
        this.i = str;
        this.j = map;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.i, this.f, this.g, this.h, this.j);
    }
}
