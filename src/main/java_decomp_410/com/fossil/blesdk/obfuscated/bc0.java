package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bc0 {
    @DexIgnore
    public static int b; // = 31;
    @DexIgnore
    public int a; // = 1;

    @DexIgnore
    public bc0 a(Object obj) {
        this.a = (b * this.a) + (obj == null ? 0 : obj.hashCode());
        return this;
    }

    @DexIgnore
    public final bc0 a(boolean z) {
        this.a = (b * this.a) + (z ? 1 : 0);
        return this;
    }

    @DexIgnore
    public int a() {
        return this.a;
    }
}
