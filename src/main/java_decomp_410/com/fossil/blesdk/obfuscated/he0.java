package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.me0;
import com.google.android.gms.common.api.Status;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class he0<R extends me0> {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Status status);
    }

    @DexIgnore
    public abstract R a(long j, TimeUnit timeUnit);

    @DexIgnore
    public abstract void a(a aVar);

    @DexIgnore
    public abstract void a(ne0<? super R> ne0);
}
