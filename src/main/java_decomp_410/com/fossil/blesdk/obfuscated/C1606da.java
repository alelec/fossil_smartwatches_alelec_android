package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.da */
public interface C1606da {
    @DexIgnore
    android.content.res.ColorStateList getSupportImageTintList();

    @DexIgnore
    android.graphics.PorterDuff.Mode getSupportImageTintMode();

    @DexIgnore
    void setSupportImageTintList(android.content.res.ColorStateList colorStateList);

    @DexIgnore
    void setSupportImageTintMode(android.graphics.PorterDuff.Mode mode);
}
