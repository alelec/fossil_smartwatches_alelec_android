package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m33 {
    @DexIgnore
    public /* final */ k33 a;

    @DexIgnore
    public m33(k33 k33) {
        kd4.b(k33, "mView");
        this.a = k33;
    }

    @DexIgnore
    public final k33 a() {
        return this.a;
    }
}
