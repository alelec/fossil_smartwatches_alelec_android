package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ej */
public final class C1733ej extends com.fossil.blesdk.obfuscated.C2224kj {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ej$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ej$a */
    public static final class C1734a extends com.fossil.blesdk.obfuscated.C2224kj.C2225a<com.fossil.blesdk.obfuscated.C1733ej.C1734a, com.fossil.blesdk.obfuscated.C1733ej> {
        @DexIgnore
        public C1734a(java.lang.Class<? extends androidx.work.ListenableWorker> cls) {
            super(cls);
            this.f6887c.f5774d = androidx.work.OverwritingInputMerger.class.getName();
        }

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C1733ej.C1734a mo10532c() {
            return this;
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C1733ej m6494b() {
            if (!this.f6885a || android.os.Build.VERSION.SDK_INT < 23 || !this.f6887c.f5780j.mo18118h()) {
                return new com.fossil.blesdk.obfuscated.C1733ej(this);
            }
            throw new java.lang.IllegalArgumentException("Cannot set backoff criteria on an idle mode job");
        }
    }

    @DexIgnore
    public C1733ej(com.fossil.blesdk.obfuscated.C1733ej.C1734a aVar) {
        super(aVar.f6886b, aVar.f6887c, aVar.f6888d);
    }
}
