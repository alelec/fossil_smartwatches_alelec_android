package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.l6 */
public final class C2296l6 {
    @DexIgnore
    /* renamed from: a */
    public static int m10075a(android.content.Context context, java.lang.String str, int i, int i2, java.lang.String str2) {
        if (context.checkPermission(str, i, i2) == -1) {
            return -1;
        }
        java.lang.String a = com.fossil.blesdk.obfuscated.C3343y5.m16713a(str);
        if (a == null) {
            return 0;
        }
        if (str2 == null) {
            java.lang.String[] packagesForUid = context.getPackageManager().getPackagesForUid(i2);
            if (packagesForUid == null || packagesForUid.length <= 0) {
                return -1;
            }
            str2 = packagesForUid[0];
        }
        if (com.fossil.blesdk.obfuscated.C3343y5.m16712a(context, a, str2) != 0) {
            return -2;
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: b */
    public static int m10076b(android.content.Context context, java.lang.String str) {
        return m10075a(context, str, android.os.Process.myPid(), android.os.Process.myUid(), context.getPackageName());
    }

    @DexIgnore
    /* renamed from: a */
    public static int m10074a(android.content.Context context, java.lang.String str) {
        return m10075a(context, str, android.os.Binder.getCallingPid(), android.os.Binder.getCallingUid(), android.os.Binder.getCallingPid() == android.os.Process.myPid() ? context.getPackageName() : null);
    }
}
