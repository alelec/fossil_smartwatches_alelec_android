package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.Log;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class m8 {
    @DexIgnore
    public static /* final */ String TAG; // = "ActionProvider(support)";
    @DexIgnore
    public /* final */ Context mContext;
    @DexIgnore
    public a mSubUiVisibilityListener;
    @DexIgnore
    public b mVisibilityListener;

    @DexIgnore
    public interface a {
        @DexIgnore
        void b(boolean z);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void onActionProviderVisibilityChanged(boolean z);
    }

    @DexIgnore
    public m8(Context context) {
        this.mContext = context;
    }

    @DexIgnore
    public Context getContext() {
        return this.mContext;
    }

    @DexIgnore
    public boolean hasSubMenu() {
        return false;
    }

    @DexIgnore
    public boolean isVisible() {
        return true;
    }

    @DexIgnore
    public abstract View onCreateActionView();

    @DexIgnore
    public View onCreateActionView(MenuItem menuItem) {
        return onCreateActionView();
    }

    @DexIgnore
    public boolean onPerformDefaultAction() {
        return false;
    }

    @DexIgnore
    public void onPrepareSubMenu(SubMenu subMenu) {
    }

    @DexIgnore
    public boolean overridesItemVisibility() {
        return false;
    }

    @DexIgnore
    public void refreshVisibility() {
        if (this.mVisibilityListener != null && overridesItemVisibility()) {
            this.mVisibilityListener.onActionProviderVisibilityChanged(isVisible());
        }
    }

    @DexIgnore
    public void reset() {
        this.mVisibilityListener = null;
        this.mSubUiVisibilityListener = null;
    }

    @DexIgnore
    public void setSubUiVisibilityListener(a aVar) {
        this.mSubUiVisibilityListener = aVar;
    }

    @DexIgnore
    public void setVisibilityListener(b bVar) {
        if (!(this.mVisibilityListener == null || bVar == null)) {
            Log.w(TAG, "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this " + getClass().getSimpleName() + " instance while it is still in use somewhere else?");
        }
        this.mVisibilityListener = bVar;
    }

    @DexIgnore
    public void subUiVisibilityChanged(boolean z) {
        a aVar = this.mSubUiVisibilityListener;
        if (aVar != null) {
            aVar.b(z);
        }
    }
}
