package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p60 extends g70 {
    @DexIgnore
    public Peripheral.HIDState A; // = Peripheral.HIDState.DISCONNECTED;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p60(Peripheral peripheral) {
        super(RequestId.DISCONNECT_HID, peripheral);
        kd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new g10(i().h());
    }

    @DexIgnore
    public void a(BluetoothCommand bluetoothCommand) {
        kd4.b(bluetoothCommand, Constants.COMMAND);
        this.A = ((g10) bluetoothCommand).i();
        a(new Request.ResponseInfo(0, (GattCharacteristic.CharacteristicId) null, (byte[]) null, wa0.a(new JSONObject(), JSONKey.NEW_HID_STATE, this.A.getLogName$blesdk_productionRelease()), 7, (fd4) null));
    }

    @DexIgnore
    public void a(d20 d20) {
        kd4.b(d20, "connectionStateChangedNotification");
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(wa0.a(super.t(), JSONKey.CURRENT_HID_STATE, i().getState().getLogName$blesdk_productionRelease()), JSONKey.MAC_ADDRESS, i().k());
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.NEW_HID_STATE, this.A.getLogName$blesdk_productionRelease());
    }
}
