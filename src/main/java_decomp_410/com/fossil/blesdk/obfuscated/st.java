package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.blesdk.obfuscated.co;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class st implements mo<ByteBuffer, ut> {
    @DexIgnore
    public static /* final */ a f; // = new a();
    @DexIgnore
    public static /* final */ b g; // = new b();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ List<ImageHeaderParser> b;
    @DexIgnore
    public /* final */ b c;
    @DexIgnore
    public /* final */ a d;
    @DexIgnore
    public /* final */ tt e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public co a(co.a aVar, eo eoVar, ByteBuffer byteBuffer, int i) {
            return new go(aVar, eoVar, byteBuffer, i);
        }
    }

    @DexIgnore
    public st(Context context, List<ImageHeaderParser> list, jq jqVar, gq gqVar) {
        this(context, list, jqVar, gqVar, g, f);
    }

    @DexIgnore
    public st(Context context, List<ImageHeaderParser> list, jq jqVar, gq gqVar, b bVar, a aVar) {
        this.a = context.getApplicationContext();
        this.b = list;
        this.d = aVar;
        this.e = new tt(jqVar, gqVar);
        this.c = bVar;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Queue<fo> a; // = uw.a(0);

        @DexIgnore
        public synchronized fo a(ByteBuffer byteBuffer) {
            fo poll;
            poll = this.a.poll();
            if (poll == null) {
                poll = new fo();
            }
            poll.a(byteBuffer);
            return poll;
        }

        @DexIgnore
        public synchronized void a(fo foVar) {
            foVar.a();
            this.a.offer(foVar);
        }
    }

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer, lo loVar) throws IOException {
        return !((Boolean) loVar.a(au.b)).booleanValue() && io.a(this.b, byteBuffer) == ImageHeaderParser.ImageType.GIF;
    }

    @DexIgnore
    public wt a(ByteBuffer byteBuffer, int i, int i2, lo loVar) {
        fo a2 = this.c.a(byteBuffer);
        try {
            return a(byteBuffer, i, i2, a2, loVar);
        } finally {
            this.c.a(a2);
        }
    }

    @DexIgnore
    public final wt a(ByteBuffer byteBuffer, int i, int i2, fo foVar, lo loVar) {
        Bitmap.Config config;
        long a2 = pw.a();
        try {
            eo c2 = foVar.c();
            if (c2.b() > 0) {
                if (c2.c() == 0) {
                    if (loVar.a(au.a) == DecodeFormat.PREFER_RGB_565) {
                        config = Bitmap.Config.RGB_565;
                    } else {
                        config = Bitmap.Config.ARGB_8888;
                    }
                    Bitmap.Config config2 = config;
                    co a3 = this.d.a(this.e, c2, byteBuffer, a(c2, i, i2));
                    a3.a(config2);
                    a3.b();
                    Bitmap a4 = a3.a();
                    if (a4 == null) {
                        if (Log.isLoggable("BufferGifDecoder", 2)) {
                            Log.v("BufferGifDecoder", "Decoded GIF from stream in " + pw.a(a2));
                        }
                        return null;
                    }
                    wt wtVar = new wt(new ut(this.a, a3, ks.a(), i, i2, a4));
                    if (Log.isLoggable("BufferGifDecoder", 2)) {
                        Log.v("BufferGifDecoder", "Decoded GIF from stream in " + pw.a(a2));
                    }
                    return wtVar;
                }
            }
            return null;
        } finally {
            if (Log.isLoggable("BufferGifDecoder", 2)) {
                Log.v("BufferGifDecoder", "Decoded GIF from stream in " + pw.a(a2));
            }
        }
    }

    @DexIgnore
    public static int a(eo eoVar, int i, int i2) {
        int i3;
        int min = Math.min(eoVar.a() / i2, eoVar.d() / i);
        if (min == 0) {
            i3 = 0;
        } else {
            i3 = Integer.highestOneBit(min);
        }
        int max = Math.max(1, i3);
        if (Log.isLoggable("BufferGifDecoder", 2) && max > 1) {
            Log.v("BufferGifDecoder", "Downsampling GIF, sampleSize: " + max + ", target dimens: [" + i + "x" + i2 + "], actual dimens: [" + eoVar.d() + "x" + eoVar.a() + "]");
        }
        return max;
    }
}
