package com.fossil.blesdk.obfuscated;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s02 extends JsonWriter {
    @DexIgnore
    public static /* final */ Writer s; // = new a();
    @DexIgnore
    public static /* final */ zz1 t; // = new zz1("closed");
    @DexIgnore
    public /* final */ List<JsonElement> p; // = new ArrayList();
    @DexIgnore
    public String q;
    @DexIgnore
    public JsonElement r; // = wz1.a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Writer {
        @DexIgnore
        public void close() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        public void flush() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        public void write(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public s02() {
        super(s);
    }

    @DexIgnore
    public JsonWriter A() throws IOException {
        tz1 tz1 = new tz1();
        a((JsonElement) tz1);
        this.p.add(tz1);
        return this;
    }

    @DexIgnore
    public JsonWriter B() throws IOException {
        xz1 xz1 = new xz1();
        a((JsonElement) xz1);
        this.p.add(xz1);
        return this;
    }

    @DexIgnore
    public JsonWriter C() throws IOException {
        if (this.p.isEmpty() || this.q != null) {
            throw new IllegalStateException();
        } else if (J() instanceof tz1) {
            List<JsonElement> list = this.p;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public JsonWriter D() throws IOException {
        if (this.p.isEmpty() || this.q != null) {
            throw new IllegalStateException();
        } else if (J() instanceof xz1) {
            List<JsonElement> list = this.p;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public JsonWriter I() throws IOException {
        a((JsonElement) wz1.a);
        return this;
    }

    @DexIgnore
    public final JsonElement J() {
        List<JsonElement> list = this.p;
        return list.get(list.size() - 1);
    }

    @DexIgnore
    public JsonElement L() {
        if (this.p.isEmpty()) {
            return this.r;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.p);
    }

    @DexIgnore
    public final void a(JsonElement jsonElement) {
        if (this.q != null) {
            if (!jsonElement.h() || E()) {
                ((xz1) J()).a(this.q, jsonElement);
            }
            this.q = null;
        } else if (this.p.isEmpty()) {
            this.r = jsonElement;
        } else {
            JsonElement J = J();
            if (J instanceof tz1) {
                ((tz1) J).a(jsonElement);
                return;
            }
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public void close() throws IOException {
        if (this.p.isEmpty()) {
            this.p.add(t);
            return;
        }
        throw new IOException("Incomplete document");
    }

    @DexIgnore
    public JsonWriter d(boolean z) throws IOException {
        a((JsonElement) new zz1(Boolean.valueOf(z)));
        return this;
    }

    @DexIgnore
    public JsonWriter e(String str) throws IOException {
        if (this.p.isEmpty() || this.q != null) {
            throw new IllegalStateException();
        } else if (J() instanceof xz1) {
            this.q = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public void flush() throws IOException {
    }

    @DexIgnore
    public JsonWriter h(String str) throws IOException {
        if (str == null) {
            I();
            return this;
        }
        a((JsonElement) new zz1(str));
        return this;
    }

    @DexIgnore
    public JsonWriter h(long j) throws IOException {
        a((JsonElement) new zz1((Number) Long.valueOf(j)));
        return this;
    }

    @DexIgnore
    public JsonWriter a(Boolean bool) throws IOException {
        if (bool == null) {
            I();
            return this;
        }
        a((JsonElement) new zz1(bool));
        return this;
    }

    @DexIgnore
    public JsonWriter a(Number number) throws IOException {
        if (number == null) {
            I();
            return this;
        }
        if (!G()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        a((JsonElement) new zz1(number));
        return this;
    }
}
