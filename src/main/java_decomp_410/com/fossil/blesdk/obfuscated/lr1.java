package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lr1 {
    @DexIgnore
    public long a; // = 0;
    @DexIgnore
    public long b; // = 300;
    @DexIgnore
    public TimeInterpolator c; // = null;
    @DexIgnore
    public int d; // = 0;
    @DexIgnore
    public int e; // = 1;

    @DexIgnore
    public lr1(long j, long j2) {
        this.a = j;
        this.b = j2;
    }

    @DexIgnore
    public void a(Animator animator) {
        animator.setStartDelay(a());
        animator.setDuration(b());
        animator.setInterpolator(c());
        if (animator instanceof ValueAnimator) {
            ValueAnimator valueAnimator = (ValueAnimator) animator;
            valueAnimator.setRepeatCount(d());
            valueAnimator.setRepeatMode(e());
        }
    }

    @DexIgnore
    public long b() {
        return this.b;
    }

    @DexIgnore
    public TimeInterpolator c() {
        TimeInterpolator timeInterpolator = this.c;
        return timeInterpolator != null ? timeInterpolator : dr1.b;
    }

    @DexIgnore
    public int d() {
        return this.d;
    }

    @DexIgnore
    public int e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || lr1.class != obj.getClass()) {
            return false;
        }
        lr1 lr1 = (lr1) obj;
        if (a() == lr1.a() && b() == lr1.b() && d() == lr1.d() && e() == lr1.e()) {
            return c().getClass().equals(lr1.c().getClass());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((int) (a() ^ (a() >>> 32))) * 31) + ((int) (b() ^ (b() >>> 32)))) * 31) + c().getClass().hashCode()) * 31) + d()) * 31) + e();
    }

    @DexIgnore
    public String toString() {
        return 10 + lr1.class.getName() + '{' + Integer.toHexString(System.identityHashCode(this)) + " delay: " + a() + " duration: " + b() + " interpolator: " + c().getClass() + " repeatCount: " + d() + " repeatMode: " + e() + "}\n";
    }

    @DexIgnore
    public static TimeInterpolator b(ValueAnimator valueAnimator) {
        TimeInterpolator interpolator = valueAnimator.getInterpolator();
        if ((interpolator instanceof AccelerateDecelerateInterpolator) || interpolator == null) {
            return dr1.b;
        }
        if (interpolator instanceof AccelerateInterpolator) {
            return dr1.c;
        }
        return interpolator instanceof DecelerateInterpolator ? dr1.d : interpolator;
    }

    @DexIgnore
    public long a() {
        return this.a;
    }

    @DexIgnore
    public static lr1 a(ValueAnimator valueAnimator) {
        lr1 lr1 = new lr1(valueAnimator.getStartDelay(), valueAnimator.getDuration(), b(valueAnimator));
        lr1.d = valueAnimator.getRepeatCount();
        lr1.e = valueAnimator.getRepeatMode();
        return lr1;
    }

    @DexIgnore
    public lr1(long j, long j2, TimeInterpolator timeInterpolator) {
        this.a = j;
        this.b = j2;
        this.c = timeInterpolator;
    }
}
