package com.fossil.blesdk.obfuscated;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rw extends FilterInputStream {
    @DexIgnore
    public int e; // = Integer.MIN_VALUE;

    @DexIgnore
    public rw(InputStream inputStream) {
        super(inputStream);
    }

    @DexIgnore
    public int available() throws IOException {
        int i = this.e;
        if (i == Integer.MIN_VALUE) {
            return super.available();
        }
        return Math.min(i, super.available());
    }

    @DexIgnore
    public final long h(long j) {
        int i = this.e;
        if (i == 0) {
            return -1;
        }
        return (i == Integer.MIN_VALUE || j <= ((long) i)) ? j : (long) i;
    }

    @DexIgnore
    public final void i(long j) {
        int i = this.e;
        if (i != Integer.MIN_VALUE && j != -1) {
            this.e = (int) (((long) i) - j);
        }
    }

    @DexIgnore
    public synchronized void mark(int i) {
        super.mark(i);
        this.e = i;
    }

    @DexIgnore
    public int read() throws IOException {
        if (h(1) == -1) {
            return -1;
        }
        int read = super.read();
        i(1);
        return read;
    }

    @DexIgnore
    public synchronized void reset() throws IOException {
        super.reset();
        this.e = Integer.MIN_VALUE;
    }

    @DexIgnore
    public long skip(long j) throws IOException {
        long h = h(j);
        if (h == -1) {
            return 0;
        }
        long skip = super.skip(h);
        i(skip);
        return skip;
    }

    @DexIgnore
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int h = (int) h((long) i2);
        if (h == -1) {
            return -1;
        }
        int read = super.read(bArr, i, h);
        i((long) read);
        return read;
    }
}
