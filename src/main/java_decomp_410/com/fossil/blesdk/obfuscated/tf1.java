package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tf1 implements Parcelable.Creator<mf1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            if (SafeParcelReader.a(a) != 2) {
                SafeParcelReader.v(parcel, a);
            } else {
                i = SafeParcelReader.q(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new mf1(i);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new mf1[i];
    }
}
