package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.underamour.UAWebViewActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lg3 extends zr2 implements ju2 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public tr3<oa2> j;
    @DexIgnore
    public iu2 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return lg3.m;
        }

        @DexIgnore
        public final lg3 b() {
            return new lg3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ lg3 a;

        @DexIgnore
        public b(lg3 lg3) {
            this.a = lg3;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            kd4.b(compoundButton, "button");
            if (compoundButton.isPressed()) {
                lg3.a(this.a).h();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ lg3 a;

        @DexIgnore
        public c(lg3 lg3) {
            this.a = lg3;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            kd4.b(compoundButton, "button");
            if (compoundButton.isPressed()) {
                lg3.a(this.a).j();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lg3 e;

        @DexIgnore
        public d(lg3 lg3) {
            this.e = lg3;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.e.getActivity() != null) {
                FragmentActivity activity = this.e.getActivity();
                if (activity != null) {
                    activity.finish();
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }
    }

    /*
    static {
        String simpleName = lg3.class.getSimpleName();
        if (simpleName != null) {
            kd4.a((Object) simpleName, "ConnectedAppsFragment::class.java.simpleName!!");
            m = simpleName;
            return;
        }
        kd4.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ iu2 a(lg3 lg3) {
        iu2 iu2 = lg3.k;
        if (iu2 != null) {
            return iu2;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void G(String str) {
        kd4.b(str, "authorizationUrl");
        UAWebViewActivity.a aVar = UAWebViewActivity.H;
        FragmentActivity activity = getActivity();
        if (activity != null) {
            kd4.a((Object) activity, "activity!!");
            aVar.a(activity, str);
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return m;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void i() {
        a();
    }

    @DexIgnore
    public void k() {
        b();
    }

    @DexIgnore
    public void o(int i) {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(i, "", childFragmentManager);
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 3534) {
            if (i2 == -1) {
                String stringExtra = intent != null ? intent.getStringExtra("code") : null;
                if (stringExtra != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = m;
                    local.d(str, "We got a UA code! " + stringExtra);
                    iu2 iu2 = this.k;
                    if (iu2 != null) {
                        iu2.a(stringExtra);
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                }
            } else {
                FLogger.INSTANCE.getLocal().e(m, "Something went wrong with UA login");
            }
        } else if (i == 1 && i2 == -1) {
            iu2 iu22 = this.k;
            if (iu22 != null) {
                iu22.i();
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new tr3<>(this, (oa2) qa.a(layoutInflater, R.layout.fragment_connected_apps, viewGroup, false, O0()));
        tr3<oa2> tr3 = this.j;
        if (tr3 != null) {
            oa2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        iu2 iu2 = this.k;
        if (iu2 != null) {
            iu2.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        iu2 iu2 = this.k;
        if (iu2 != null) {
            iu2.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<oa2> tr3 = this.j;
        if (tr3 != null) {
            oa2 a2 = tr3.a();
            if (a2 != null) {
                a2.q.setOnCheckedChangeListener(new b(this));
                a2.r.setOnCheckedChangeListener(new c(this));
                a2.s.setOnClickListener(new d(this));
            }
            R("connected_app_view");
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void x(boolean z) {
        if (isActive()) {
            tr3<oa2> tr3 = this.j;
            if (tr3 != null) {
                oa2 a2 = tr3.a();
                if (a2 != null) {
                    SwitchCompat switchCompat = a2.r;
                    kd4.a((Object) switchCompat, "it.cbUnderArmour");
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void y(boolean z) {
        if (isActive()) {
            tr3<oa2> tr3 = this.j;
            if (tr3 != null) {
                oa2 a2 = tr3.a();
                if (a2 != null) {
                    SwitchCompat switchCompat = a2.q;
                    kd4.a((Object) switchCompat, "it.cbGooglefit");
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(iu2 iu2) {
        kd4.b(iu2, "presenter");
        this.k = iu2;
    }

    @DexIgnore
    public void a(ud0 ud0) {
        kd4.b(ud0, "connectionResult");
        ud0.a(getActivity(), 1);
    }
}
