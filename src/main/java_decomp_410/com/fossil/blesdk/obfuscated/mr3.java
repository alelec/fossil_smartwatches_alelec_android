package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.util.Base64;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import java.nio.charset.Charset;
import java.security.KeyPair;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mr3 extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public /* final */ en2 d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(String str, String str2) {
            kd4.b(str, "aliasName");
            kd4.b(str2, "value");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public c(int i, String str) {
            kd4.b(str, "errorMessage");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public mr3(en2 en2) {
        kd4.b(en2, "mSharedPreferencesManager");
        this.d = en2;
    }

    @DexIgnore
    public String c() {
        return "EncryptValueKeyStoreUseCase";
    }

    @DexIgnore
    public Object a(b bVar, yb4<Object> yb4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("Start encrypt ");
        sb.append(bVar != null ? bVar.b() : null);
        sb.append(" with alias ");
        sb.append(bVar != null ? bVar.a() : null);
        local.d("EncryptValueKeyStoreUseCase", sb.toString());
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                is3 is3 = is3.b;
                if (bVar != null) {
                    SecretKey d2 = is3.d(bVar.a());
                    Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                    instance.init(1, d2);
                    String b2 = bVar.b();
                    Charset charset = bf4.a;
                    if (b2 != null) {
                        byte[] bytes = b2.getBytes(charset);
                        kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                        byte[] doFinal = instance.doFinal(bytes);
                        kd4.a((Object) instance, "cipher");
                        String encodeToString = Base64.encodeToString(instance.getIV(), 0);
                        String encodeToString2 = Base64.encodeToString(doFinal, 0);
                        this.d.b(bVar.a(), encodeToString);
                        this.d.c(bVar.a(), encodeToString2);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("EncryptValueKeyStoreUseCase", "Encryption done iv " + encodeToString + " value " + encodeToString2);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                is3 is32 = is3.b;
                if (bVar != null) {
                    KeyPair c2 = is32.c(bVar.a());
                    if (c2 == null) {
                        c2 = is3.b.a(bVar.a());
                    }
                    Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                    instance2.init(1, c2.getPublic());
                    String b3 = bVar.b();
                    Charset charset2 = bf4.a;
                    if (b3 != null) {
                        byte[] bytes2 = b3.getBytes(charset2);
                        kd4.a((Object) bytes2, "(this as java.lang.String).getBytes(charset)");
                        String encodeToString3 = Base64.encodeToString(instance2.doFinal(bytes2), 0);
                        this.d.c(bVar.a(), encodeToString3);
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        local3.d("EncryptValueKeyStoreUseCase", "Encryption done value " + encodeToString3);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
            a(new d());
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.e("EncryptValueKeyStoreUseCase", "Exception when encrypt values " + e);
            a(new c(600, ""));
        }
        return new Object();
    }
}
