package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.et */
public class C1747et implements com.fossil.blesdk.obfuscated.C2427mo<android.net.Uri, android.graphics.Bitmap> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2602ot f4925a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2149jq f4926b;

    @DexIgnore
    public C1747et(com.fossil.blesdk.obfuscated.C2602ot otVar, com.fossil.blesdk.obfuscated.C2149jq jqVar) {
        this.f4925a = otVar;
        this.f4926b = jqVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9301a(android.net.Uri uri, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return "android.resource".equals(uri.getScheme());
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> mo9299a(android.net.Uri uri, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        com.fossil.blesdk.obfuscated.C1438aq<android.graphics.drawable.Drawable> a = this.f4925a.mo9299a(uri, i, i2, loVar);
        if (a == null) {
            return null;
        }
        return com.fossil.blesdk.obfuscated.C3310xs.m16497a(this.f4926b, a.get(), i, i2);
    }
}
