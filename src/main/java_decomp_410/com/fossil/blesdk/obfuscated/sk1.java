package com.fossil.blesdk.obfuscated;

import android.app.job.JobParameters;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface sk1 {
    @DexIgnore
    void a(JobParameters jobParameters, boolean z);

    @DexIgnore
    void a(Intent intent);

    @DexIgnore
    boolean a(int i);
}
