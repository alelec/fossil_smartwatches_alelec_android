package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l41 implements Parcelable.Creator<k41> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        i41 i41 = null;
        IBinder iBinder = null;
        PendingIntent pendingIntent = null;
        IBinder iBinder2 = null;
        IBinder iBinder3 = null;
        int i = 1;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 1:
                    i = SafeParcelReader.q(parcel, a);
                    break;
                case 2:
                    i41 = SafeParcelReader.a(parcel, a, i41.CREATOR);
                    break;
                case 3:
                    iBinder = SafeParcelReader.p(parcel, a);
                    break;
                case 4:
                    pendingIntent = SafeParcelReader.a(parcel, a, PendingIntent.CREATOR);
                    break;
                case 5:
                    iBinder2 = SafeParcelReader.p(parcel, a);
                    break;
                case 6:
                    iBinder3 = SafeParcelReader.p(parcel, a);
                    break;
                default:
                    SafeParcelReader.v(parcel, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel, b);
        return new k41(i, i41, iBinder, pendingIntent, iBinder2, iBinder3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new k41[i];
    }
}
