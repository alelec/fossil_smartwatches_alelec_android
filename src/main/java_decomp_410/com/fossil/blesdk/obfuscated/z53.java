package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface z53 extends bs2<y53> {
    @DexIgnore
    void a(boolean z);

    @DexIgnore
    void c(int i);

    @DexIgnore
    void c(List<g13> list);

    @DexIgnore
    void d(int i);

    @DexIgnore
    void d(boolean z);

    @DexIgnore
    void e(int i);

    @DexIgnore
    int getItemCount();

    @DexIgnore
    void j();

    @DexIgnore
    void l();

    @DexIgnore
    void m();

    @DexIgnore
    void q(String str);

    @DexIgnore
    void v();
}
