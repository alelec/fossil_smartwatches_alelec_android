package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mh */
public class C2412mh {

    @DexIgnore
    /* renamed from: a */
    public static androidx.transition.Transition f7495a; // = new androidx.transition.AutoTransition();

    @DexIgnore
    /* renamed from: b */
    public static java.lang.ThreadLocal<java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C1855g4<android.view.ViewGroup, java.util.ArrayList<androidx.transition.Transition>>>> f7496b; // = new java.lang.ThreadLocal<>();

    @DexIgnore
    /* renamed from: c */
    public static java.util.ArrayList<android.view.ViewGroup> f7497c; // = new java.util.ArrayList<>();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mh$a")
    /* renamed from: com.fossil.blesdk.obfuscated.mh$a */
    public static class C2413a implements android.view.ViewTreeObserver.OnPreDrawListener, android.view.View.OnAttachStateChangeListener {

        @DexIgnore
        /* renamed from: e */
        public androidx.transition.Transition f7498e;

        @DexIgnore
        /* renamed from: f */
        public android.view.ViewGroup f7499f;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mh$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.mh$a$a */
        public class C2414a extends com.fossil.blesdk.obfuscated.C2326lh {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1855g4 f7500a;

            @DexIgnore
            public C2414a(com.fossil.blesdk.obfuscated.C1855g4 g4Var) {
                this.f7500a = g4Var;
            }

            @DexIgnore
            /* renamed from: c */
            public void mo3464c(androidx.transition.Transition transition) {
                ((java.util.ArrayList) this.f7500a.get(com.fossil.blesdk.obfuscated.C2412mh.C2413a.this.f7499f)).remove(transition);
            }
        }

        @DexIgnore
        public C2413a(androidx.transition.Transition transition, android.view.ViewGroup viewGroup) {
            this.f7498e = transition;
            this.f7499f = viewGroup;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo13623a() {
            this.f7499f.getViewTreeObserver().removeOnPreDrawListener(this);
            this.f7499f.removeOnAttachStateChangeListener(this);
        }

        @DexIgnore
        public boolean onPreDraw() {
            mo13623a();
            if (!com.fossil.blesdk.obfuscated.C2412mh.f7497c.remove(this.f7499f)) {
                return true;
            }
            com.fossil.blesdk.obfuscated.C1855g4<android.view.ViewGroup, java.util.ArrayList<androidx.transition.Transition>> a = com.fossil.blesdk.obfuscated.C2412mh.m10753a();
            java.util.ArrayList arrayList = a.get(this.f7499f);
            java.util.ArrayList arrayList2 = null;
            if (arrayList == null) {
                arrayList = new java.util.ArrayList();
                a.put(this.f7499f, arrayList);
            } else if (arrayList.size() > 0) {
                arrayList2 = new java.util.ArrayList(arrayList);
            }
            arrayList.add(this.f7498e);
            this.f7498e.mo3519a((androidx.transition.Transition.C0328f) new com.fossil.blesdk.obfuscated.C2412mh.C2413a.C2414a(a));
            this.f7498e.mo3527a(this.f7499f, false);
            if (arrayList2 != null) {
                java.util.Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    ((androidx.transition.Transition) it.next()).mo3553e(this.f7499f);
                }
            }
            this.f7498e.mo3525a(this.f7499f);
            return true;
        }

        @DexIgnore
        public void onViewAttachedToWindow(android.view.View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(android.view.View view) {
            mo13623a();
            com.fossil.blesdk.obfuscated.C2412mh.f7497c.remove(this.f7499f);
            java.util.ArrayList arrayList = com.fossil.blesdk.obfuscated.C2412mh.m10753a().get(this.f7499f);
            if (arrayList != null && arrayList.size() > 0) {
                java.util.Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((androidx.transition.Transition) it.next()).mo3553e(this.f7499f);
                }
            }
            this.f7498e.mo3536a(true);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1855g4<android.view.ViewGroup, java.util.ArrayList<androidx.transition.Transition>> m10753a() {
        java.lang.ref.WeakReference weakReference = f7496b.get();
        if (weakReference != null) {
            com.fossil.blesdk.obfuscated.C1855g4<android.view.ViewGroup, java.util.ArrayList<androidx.transition.Transition>> g4Var = (com.fossil.blesdk.obfuscated.C1855g4) weakReference.get();
            if (g4Var != null) {
                return g4Var;
            }
        }
        com.fossil.blesdk.obfuscated.C1855g4<android.view.ViewGroup, java.util.ArrayList<androidx.transition.Transition>> g4Var2 = new com.fossil.blesdk.obfuscated.C1855g4<>();
        f7496b.set(new java.lang.ref.WeakReference(g4Var2));
        return g4Var2;
    }

    @DexIgnore
    /* renamed from: b */
    public static void m10755b(android.view.ViewGroup viewGroup, androidx.transition.Transition transition) {
        if (transition != null && viewGroup != null) {
            com.fossil.blesdk.obfuscated.C2412mh.C2413a aVar = new com.fossil.blesdk.obfuscated.C2412mh.C2413a(transition, viewGroup);
            viewGroup.addOnAttachStateChangeListener(aVar);
            viewGroup.getViewTreeObserver().addOnPreDrawListener(aVar);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static void m10756c(android.view.ViewGroup viewGroup, androidx.transition.Transition transition) {
        java.util.ArrayList arrayList = m10753a().get(viewGroup);
        if (arrayList != null && arrayList.size() > 0) {
            java.util.Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                ((androidx.transition.Transition) it.next()).mo3548c((android.view.View) viewGroup);
            }
        }
        if (transition != null) {
            transition.mo3527a(viewGroup, true);
        }
        com.fossil.blesdk.obfuscated.C2020ih a = com.fossil.blesdk.obfuscated.C2020ih.m8391a(viewGroup);
        if (a != null) {
            a.mo11941a();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m10754a(android.view.ViewGroup viewGroup, androidx.transition.Transition transition) {
        if (!f7497c.contains(viewGroup) && com.fossil.blesdk.obfuscated.C1776f9.m6860z(viewGroup)) {
            f7497c.add(viewGroup);
            if (transition == null) {
                transition = f7495a;
            }
            androidx.transition.Transition clone = transition.clone();
            m10756c(viewGroup, clone);
            com.fossil.blesdk.obfuscated.C2020ih.m8392a(viewGroup, (com.fossil.blesdk.obfuscated.C2020ih) null);
            m10755b(viewGroup, clone);
        }
    }
}
