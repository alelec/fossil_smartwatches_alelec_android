package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.format.DateFormat;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nl2 {
    @DexIgnore
    public static int a(int i) {
        if (i >= 60) {
            return i / 60;
        }
        return 0;
    }

    @DexIgnore
    public static int b(int i) {
        return i >= 60 ? i - ((i / 60) * 60) : i;
    }

    @DexIgnore
    public static int c(int i) {
        if (i < 1 || i > 12) {
            return -1;
        }
        return (i * 30) - 1;
    }

    @DexIgnore
    public static String d(int i) {
        int i2 = i / 60;
        int i3 = i % 60;
        if (i2 == 0) {
            return i3 + " " + sm2.a((Context) PortfolioApp.R, (int) R.string.AlertsDiana_MoveAlerts_Main_Label__Min);
        } else if (i2 <= 0 || i3 != 0) {
            return String.format(sm2.a((Context) PortfolioApp.R, (int) R.string.DashboardDiana_Sleep_DetailPage_Label__NumberHrsNumberMins), new Object[]{Integer.valueOf(i2), Integer.valueOf(i3)});
        } else {
            return i2 + " " + sm2.a((Context) PortfolioApp.R, (int) R.string.AlertsDiana_MoveAlerts_Main_Label__Hr);
        }
    }

    @DexIgnore
    public static SpannableString e(int i) {
        int i2 = i / 60;
        int i3 = i % 60;
        if (DateFormat.is24HourFormat(PortfolioApp.R)) {
            return new SpannableString(String.format(Locale.US, "%02d", new Object[]{Integer.valueOf(i2)}) + ":" + String.format(Locale.US, "%02d", new Object[]{Integer.valueOf(i3)}));
        }
        int i4 = 12;
        if (i < 720) {
            if (i2 == 0) {
                i2 = 12;
            }
            return ll2.b.a(String.format(Locale.US, "%02d", new Object[]{Integer.valueOf(i2)}) + ":" + String.format(Locale.US, "%02d", new Object[]{Integer.valueOf(i3)}), sm2.a((Context) PortfolioApp.R, (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Am), 1.0f);
        }
        if (i2 > 12) {
            i4 = i2 - 12;
        }
        return ll2.b.a(String.format(Locale.US, "%02d", new Object[]{Integer.valueOf(i4)}) + ":" + String.format(Locale.US, "%02d", new Object[]{Integer.valueOf(i3)}), sm2.a((Context) PortfolioApp.R, (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Pm), 1.0f);
    }

    @DexIgnore
    public static int a() {
        return TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()) / 1000;
    }

    @DexIgnore
    public static String a(Date date) {
        return DateFormat.format("MMMM dd", date).toString();
    }

    @DexIgnore
    public static int a(String str) {
        if (TextUtils.isEmpty(str)) {
            return 1024;
        }
        TimeZone timeZone = TimeZone.getTimeZone(str);
        if (timeZone.inDaylightTime(Calendar.getInstance().getTime())) {
            return (timeZone.getRawOffset() + timeZone.getDSTSavings()) / 60000;
        }
        return timeZone.getRawOffset() / 60000;
    }
}
