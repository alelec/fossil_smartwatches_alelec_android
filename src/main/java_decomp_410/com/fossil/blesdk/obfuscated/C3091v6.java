package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.v6 */
public class C3091v6 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C1394a7 f10161a;

    @DexIgnore
    /* renamed from: b */
    public static /* final */ com.fossil.blesdk.obfuscated.C2183k4<java.lang.String, android.graphics.Typeface> f10162b; // = new com.fossil.blesdk.obfuscated.C2183k4<>(16);

    /*
    static {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 28) {
            f10161a = new com.fossil.blesdk.obfuscated.C3416z6();
        } else if (i >= 26) {
            f10161a = new com.fossil.blesdk.obfuscated.C3344y6();
        } else if (i >= 24 && com.fossil.blesdk.obfuscated.C3264x6.m16160a()) {
            f10161a = new com.fossil.blesdk.obfuscated.C3264x6();
        } else if (android.os.Build.VERSION.SDK_INT >= 21) {
            f10161a = new com.fossil.blesdk.obfuscated.C3169w6();
        } else {
            f10161a = new com.fossil.blesdk.obfuscated.C1394a7();
        }
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m15084a(android.content.res.Resources resources, int i, int i2) {
        return resources.getResourcePackageName(i) + com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + i + com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + i2;
    }

    @DexIgnore
    /* renamed from: b */
    public static android.graphics.Typeface m15085b(android.content.res.Resources resources, int i, int i2) {
        return f10162b.mo12627b(m15084a(resources, i, i2));
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Typeface m15083a(android.content.Context context, com.fossil.blesdk.obfuscated.C2528o6.C2529a aVar, android.content.res.Resources resources, int i, int i2, com.fossil.blesdk.obfuscated.C2785r6.C2786a aVar2, android.os.Handler handler, boolean z) {
        android.graphics.Typeface typeface;
        if (aVar instanceof com.fossil.blesdk.obfuscated.C2528o6.C2532d) {
            com.fossil.blesdk.obfuscated.C2528o6.C2532d dVar = (com.fossil.blesdk.obfuscated.C2528o6.C2532d) aVar;
            boolean z2 = false;
            if (!z ? aVar2 == null : dVar.mo14266a() == 0) {
                z2 = true;
            }
            typeface = com.fossil.blesdk.obfuscated.C3012u7.m14598a(context, dVar.mo14267b(), aVar2, handler, z2, z ? dVar.mo14268c() : -1, i2);
        } else {
            typeface = f10161a.mo8578a(context, (com.fossil.blesdk.obfuscated.C2528o6.C2530b) aVar, resources, i2);
            if (aVar2 != null) {
                if (typeface != null) {
                    aVar2.mo15443a(typeface, handler);
                } else {
                    aVar2.mo15442a(-3, handler);
                }
            }
        }
        if (typeface != null) {
            f10162b.mo12623a(m15084a(resources, i, i2), typeface);
        }
        return typeface;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Typeface m15081a(android.content.Context context, android.content.res.Resources resources, int i, java.lang.String str, int i2) {
        android.graphics.Typeface a = f10161a.mo8576a(context, resources, i, str, i2);
        if (a != null) {
            f10162b.mo12623a(m15084a(resources, i, i2), a);
        }
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Typeface m15082a(android.content.Context context, android.os.CancellationSignal cancellationSignal, com.fossil.blesdk.obfuscated.C3012u7.C3018f[] fVarArr, int i) {
        return f10161a.mo8577a(context, cancellationSignal, fVarArr, i);
    }
}
