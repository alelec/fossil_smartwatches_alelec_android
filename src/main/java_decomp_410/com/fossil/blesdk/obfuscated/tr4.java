package com.fossil.blesdk.obfuscated;

import java.lang.annotation.Annotation;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tr4 implements sr4 {
    @DexIgnore
    public static /* final */ sr4 a; // = new tr4();

    @DexIgnore
    public static Annotation[] a(Annotation[] annotationArr) {
        if (ur4.a(annotationArr, (Class<? extends Annotation>) sr4.class)) {
            return annotationArr;
        }
        Annotation[] annotationArr2 = new Annotation[(annotationArr.length + 1)];
        annotationArr2[0] = a;
        System.arraycopy(annotationArr, 0, annotationArr2, 1, annotationArr.length);
        return annotationArr2;
    }

    @DexIgnore
    public Class<? extends Annotation> annotationType() {
        return sr4.class;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj instanceof sr4;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "@" + sr4.class.getName() + "()";
    }
}
