package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ib2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RecyclerViewPager A;
    @DexIgnore
    public /* final */ ImageView B;
    @DexIgnore
    public /* final */ TextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ CustomizeWidget G;
    @DexIgnore
    public /* final */ CustomizeWidget H;
    @DexIgnore
    public /* final */ CustomizeWidget I;
    @DexIgnore
    public /* final */ CustomizeWidget J;
    @DexIgnore
    public /* final */ CustomizeWidget K;
    @DexIgnore
    public /* final */ CustomizeWidget L;
    @DexIgnore
    public /* final */ CustomizeWidget M;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ CardView t;
    @DexIgnore
    public /* final */ ImageButton u;
    @DexIgnore
    public /* final */ ImageView v;
    @DexIgnore
    public /* final */ ImageView w;
    @DexIgnore
    public /* final */ View x;
    @DexIgnore
    public /* final */ View y;
    @DexIgnore
    public /* final */ View z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ib2(Object obj, View view, int i, View view2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, CardView cardView, ImageButton imageButton, View view3, View view4, ImageView imageView, ImageView imageView2, View view5, View view6, View view7, RecyclerViewPager recyclerViewPager, ImageView imageView3, TextView textView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, View view8, CustomizeWidget customizeWidget, CustomizeWidget customizeWidget2, CustomizeWidget customizeWidget3, CustomizeWidget customizeWidget4, CustomizeWidget customizeWidget5, CustomizeWidget customizeWidget6, CustomizeWidget customizeWidget7) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = constraintLayout4;
        this.t = cardView;
        this.u = imageButton;
        this.v = imageView;
        this.w = imageView2;
        this.x = view5;
        this.y = view6;
        this.z = view7;
        this.A = recyclerViewPager;
        this.B = imageView3;
        this.C = textView;
        this.D = flexibleTextView;
        this.E = flexibleTextView2;
        this.F = flexibleTextView3;
        this.G = customizeWidget;
        this.H = customizeWidget2;
        this.I = customizeWidget3;
        this.J = customizeWidget4;
        this.K = customizeWidget5;
        this.L = customizeWidget6;
        this.M = customizeWidget7;
    }
}
