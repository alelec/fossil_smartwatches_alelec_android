package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b33 implements Factory<CommuteTimeSettingsDefaultAddressPresenter> {
    @DexIgnore
    public static CommuteTimeSettingsDefaultAddressPresenter a(y23 y23, en2 en2, UserRepository userRepository) {
        return new CommuteTimeSettingsDefaultAddressPresenter(y23, en2, userRepository);
    }
}
