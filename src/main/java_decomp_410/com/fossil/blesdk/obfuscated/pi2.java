package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pi2 extends oi2 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(R.id.ftv_day_of_week, 1);
        A.put(R.id.ftv_day_of_month, 2);
        A.put(R.id.line, 3);
        A.put(R.id.cl_daily_value, 4);
        A.put(R.id.tv_hour, 5);
        A.put(R.id.tv_hour_unit, 6);
        A.put(R.id.tv_min, 7);
        A.put(R.id.tv_min_unit, 8);
        A.put(R.id.ftv_daily_unit, 9);
        A.put(R.id.ftv_est, 10);
        A.put(R.id.ftv_indicator, 11);
    }
    */

    @DexIgnore
    public pi2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 12, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    public pi2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[4], objArr[0], objArr[9], objArr[2], objArr[1], objArr[10], objArr[11], objArr[3], objArr[5], objArr[6], objArr[7], objArr[8]);
        this.y = -1;
        this.r.setTag((Object) null);
        a(view);
        f();
    }
}
