package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o91 implements v91 {
    @DexIgnore
    public final u91 a(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }

    @DexIgnore
    public final boolean b(Class<?> cls) {
        return false;
    }
}
