package com.portfolio.platform.manager;

import android.os.Handler;
import android.os.Looper;
import android.provider.Telephony;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.bf4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ms3;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.ym2;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.HybridNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.AppFilterHistory;
import com.portfolio.platform.data.AppType;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.NotificationType;
import com.portfolio.platform.data.model.LightAndHaptics;
import com.portfolio.platform.data.model.MessageComparator;
import com.portfolio.platform.data.model.NotificationInfo;
import com.portfolio.platform.data.model.NotificationPriority;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.util.NotificationAppHelper;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.zip.CRC32;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LightAndHapticsManager {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static LightAndHapticsManager h;
    @DexIgnore
    public static /* final */ a i; // = new a((fd4) null);
    @DexIgnore
    public DeviceRepository a;
    @DexIgnore
    public en2 b;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<LightAndHaptics> c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ Handler e;
    @DexIgnore
    public /* final */ Runnable f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(LightAndHapticsManager lightAndHapticsManager) {
            LightAndHapticsManager.h = lightAndHapticsManager;
        }

        @DexIgnore
        public final LightAndHapticsManager b() {
            return LightAndHapticsManager.h;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final LightAndHapticsManager a() {
            if (b() == null) {
                a(new LightAndHapticsManager((fd4) null));
            }
            LightAndHapticsManager b = b();
            if (b != null) {
                return b;
            }
            kd4.a();
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ LightAndHapticsManager e;

        @DexIgnore
        public b(LightAndHapticsManager lightAndHapticsManager) {
            this.e = lightAndHapticsManager;
        }

        @DexIgnore
        public final void run() {
            this.e.b();
        }
    }

    /*
    static {
        String simpleName = LightAndHapticsManager.class.getSimpleName();
        kd4.a((Object) simpleName, "LightAndHapticsManager::class.java.simpleName");
        g = simpleName;
        kd4.a((Object) Arrays.asList(new CalibrationEnums.HandId[]{CalibrationEnums.HandId.MINUTE, CalibrationEnums.HandId.HOUR}), "Arrays.asList(Calibratio\u2026brationEnums.HandId.HOUR)");
        kd4.a((Object) Arrays.asList(new CalibrationEnums.HandId[]{CalibrationEnums.HandId.MINUTE, CalibrationEnums.HandId.HOUR, CalibrationEnums.HandId.SUB_EYE}), "Arrays.asList(Calibratio\u2026tionEnums.HandId.SUB_EYE)");
    }
    */

    @DexIgnore
    public LightAndHapticsManager() {
        PortfolioApp.W.c().g().a(this);
        this.c = new PriorityBlockingQueue<>(5, new MessageComparator());
        this.e = new Handler(Looper.getMainLooper());
        this.f = new b(this);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x006e A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0095 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0178  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x017c A[RETURN] */
    public final boolean c(NotificationInfo notificationInfo) {
        List<ContactGroup> list;
        List<ContactGroup> list2;
        NotificationSource source;
        LightAndHaptics lightAndHaptics = null;
        if (notificationInfo.getSource() == NotificationSource.TEXT) {
            list2 = ms3.a().b("-5678", MFDeviceFamily.DEVICE_FAMILY_SAM);
            if (!TextUtils.isEmpty(notificationInfo.getSenderInfo())) {
                ms3 a2 = ms3.a();
                String senderInfo = notificationInfo.getSenderInfo();
                if (senderInfo != null) {
                    list = a2.b(senderInfo, MFDeviceFamily.DEVICE_FAMILY_SAM);
                    if (list2 == null && list != null && list.isEmpty() && list2.isEmpty()) {
                        if (a()) {
                            b(notificationInfo);
                            FLogger.INSTANCE.getLocal().d(g, ".Inside checkAgainstContacts, NO assigned contact with this phone number, NO ALL-TEXT, let Message fire");
                        }
                        return false;
                    } else if (list != null && list2 == null) {
                        return false;
                    } else {
                        if (list != null || !(!list.isEmpty())) {
                            source = notificationInfo.getSource();
                            if (source != null) {
                                int i2 = ym2.a[source.ordinal()];
                                if (i2 == 1) {
                                    NotificationSource source2 = notificationInfo.getSource();
                                    kd4.a((Object) source2, "info.source");
                                    lightAndHaptics = a("-5678", (List<? extends ContactGroup>) list2, source2);
                                    FLogger.INSTANCE.getLocal().d(g, "ALL TEXT with " + notificationInfo.getSenderInfo() + " body " + notificationInfo.getBody() + " matchcontact is " + lightAndHaptics);
                                } else if (i2 == 2) {
                                    NotificationSource source3 = notificationInfo.getSource();
                                    kd4.a((Object) source3, "info.source");
                                    lightAndHaptics = a("-1234", (List<? extends ContactGroup>) list2, source3);
                                    FLogger.INSTANCE.getLocal().d(g, "ALL CALL with " + notificationInfo.getSenderInfo() + " body " + notificationInfo.getBody() + " matchcontact is " + lightAndHaptics);
                                }
                            }
                        } else {
                            String senderInfo2 = notificationInfo.getSenderInfo();
                            NotificationSource source4 = notificationInfo.getSource();
                            kd4.a((Object) source4, "info.source");
                            lightAndHaptics = a(senderInfo2, (List<? extends ContactGroup>) list, source4);
                        }
                        FLogger.INSTANCE.getLocal().d(g, "checkAgainstContacts with " + notificationInfo.getSenderInfo() + " body " + notificationInfo.getBody() + " matchcontact is " + lightAndHaptics);
                        if (lightAndHaptics == null) {
                            return false;
                        }
                        a(lightAndHaptics);
                        return true;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
        } else {
            if (notificationInfo.getSource() == NotificationSource.CALL) {
                list2 = ms3.a().a("-1234", MFDeviceFamily.DEVICE_FAMILY_SAM);
                if (!TextUtils.isEmpty(notificationInfo.getSenderInfo())) {
                    ms3 a3 = ms3.a();
                    String senderInfo3 = notificationInfo.getSenderInfo();
                    if (senderInfo3 != null) {
                        list = a3.a(senderInfo3, MFDeviceFamily.DEVICE_FAMILY_SAM);
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            } else {
                list2 = null;
                list = null;
            }
            if (list2 == null) {
            }
            if (list != null) {
            }
            if (list != null) {
            }
            source = notificationInfo.getSource();
            if (source != null) {
            }
            FLogger.INSTANCE.getLocal().d(g, "checkAgainstContacts with " + notificationInfo.getSenderInfo() + " body " + notificationInfo.getBody() + " matchcontact is " + lightAndHaptics);
            if (lightAndHaptics == null) {
            }
        }
        list = null;
        if (list2 == null) {
        }
        if (list != null) {
        }
        if (list != null) {
        }
        source = notificationInfo.getSource();
        if (source != null) {
        }
        FLogger.INSTANCE.getLocal().d(g, "checkAgainstContacts with " + notificationInfo.getSenderInfo() + " body " + notificationInfo.getBody() + " matchcontact is " + lightAndHaptics);
        if (lightAndHaptics == null) {
        }
    }

    @DexIgnore
    public final fi4 f(NotificationInfo notificationInfo) {
        return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new LightAndHapticsManager$checkAgainstSMS$Anon1(this, notificationInfo, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void b(NotificationInfo notificationInfo) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "PACKAGE CHECK : " + notificationInfo.getPackageName());
        if (!TextUtils.isEmpty(notificationInfo.getPackageName())) {
            fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new LightAndHapticsManager$checkAgainstAppFilter$Anon1(this, notificationInfo, (yb4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public final fi4 d(NotificationInfo notificationInfo) {
        return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new LightAndHapticsManager$checkAgainstEmail$Anon1(this, notificationInfo, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final fi4 e(NotificationInfo notificationInfo) {
        return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new LightAndHapticsManager$checkAgainstPhone$Anon1(this, notificationInfo, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(NotificationInfo notificationInfo) {
        kd4.b(notificationInfo, "info");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "notification info : " + notificationInfo.getSource());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = g;
        local2.d(str2, "notification body : " + notificationInfo.getBody());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = g;
        local3.d(str3, "notification type : " + notificationInfo.getSource());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = g;
        local4.e(str4, "notification - info=" + notificationInfo);
        String packageName = notificationInfo.getPackageName();
        kd4.a((Object) packageName, "info.packageName");
        if (!a(packageName)) {
            if (notificationInfo.getSource() == NotificationSource.CALL || notificationInfo.getSource() == NotificationSource.TEXT || notificationInfo.getSource() == NotificationSource.MAIL) {
                if (!c(notificationInfo)) {
                    if (notificationInfo.getSource() == NotificationSource.CALL) {
                        e(notificationInfo);
                    } else if (notificationInfo.getSource() == NotificationSource.TEXT) {
                        f(notificationInfo);
                    } else if (notificationInfo.getSource() == NotificationSource.MAIL) {
                        d(notificationInfo);
                    }
                }
            } else if (notificationInfo.getSource() == NotificationSource.OS) {
                b(notificationInfo);
            }
        }
    }

    @DexIgnore
    public final LightAndHaptics b(String str) {
        Contact a2 = ms3.a().a(str, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        if (a2 == null || !a2.isUseSms()) {
            return null;
        }
        NotificationType notificationType = NotificationType.SMS;
        String displayName = a2.getDisplayName();
        ContactGroup contactGroup = a2.getContactGroup();
        kd4.a((Object) contactGroup, "contact.contactGroup");
        return new LightAndHaptics("", notificationType, displayName, contactGroup.getHour(), NotificationPriority.ENTOURAGE_SMS);
    }

    @DexIgnore
    public /* synthetic */ LightAndHapticsManager(fd4 fd4) {
        this();
    }

    @DexIgnore
    public final void b() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "Check the  : " + this.c.size());
        if (!this.c.isEmpty()) {
            this.d = true;
            c();
            return;
        }
        this.d = false;
    }

    @DexIgnore
    public final void b(LightAndHaptics lightAndHaptics) {
        FLogger.INSTANCE.getLocal().d(g, "doPlayHandsNotification");
        NotificationType type = lightAndHaptics.getType();
        if (type != null) {
            int i2 = ym2.c[type.ordinal()];
            if (i2 == 1) {
                PortfolioApp c2 = PortfolioApp.W.c();
                String e2 = PortfolioApp.W.c().e();
                NotificationBaseObj.ANotificationType aNotificationType = NotificationBaseObj.ANotificationType.INCOMING_CALL;
                DianaNotificationObj.AApplicationName aApplicationName = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                String senderName = lightAndHaptics.getSenderName();
                kd4.a((Object) senderName, "item.senderName");
                String senderName2 = lightAndHaptics.getSenderName();
                kd4.a((Object) senderName2, "item.senderName");
                String senderName3 = lightAndHaptics.getSenderName();
                kd4.a((Object) senderName3, "item.senderName");
                c2.a(e2, (NotificationBaseObj) new DianaNotificationObj(0, aNotificationType, aApplicationName, senderName, senderName2, senderName3, cb4.d(NotificationBaseObj.ANotificationFlag.IMPORTANT)));
            } else if (i2 == 2) {
                PortfolioApp c3 = PortfolioApp.W.c();
                String e3 = PortfolioApp.W.c().e();
                NotificationBaseObj.ANotificationType aNotificationType2 = NotificationBaseObj.ANotificationType.TEXT;
                DianaNotificationObj.AApplicationName aApplicationName2 = DianaNotificationObj.AApplicationName.MESSAGES;
                String senderName4 = lightAndHaptics.getSenderName();
                kd4.a((Object) senderName4, "item.senderName");
                String senderName5 = lightAndHaptics.getSenderName();
                kd4.a((Object) senderName5, "item.senderName");
                String senderName6 = lightAndHaptics.getSenderName();
                kd4.a((Object) senderName6, "item.senderName");
                c3.a(e3, (NotificationBaseObj) new DianaNotificationObj(0, aNotificationType2, aApplicationName2, senderName4, senderName5, senderName6, cb4.d(NotificationBaseObj.ANotificationFlag.IMPORTANT)));
            } else if (i2 == 3) {
                String senderName7 = lightAndHaptics.getSenderName();
                String packageName = senderName7 == null || qf4.a(senderName7) ? lightAndHaptics.getPackageName() : lightAndHaptics.getSenderName();
                CRC32 crc32 = new CRC32();
                String packageName2 = lightAndHaptics.getPackageName();
                kd4.a((Object) packageName2, "item.packageName");
                Charset charset = bf4.a;
                if (packageName2 != null) {
                    byte[] bytes = packageName2.getBytes(charset);
                    kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                    crc32.update(bytes);
                    long value = crc32.getValue();
                    kd4.a((Object) packageName, "appName");
                    String packageName3 = lightAndHaptics.getPackageName();
                    kd4.a((Object) packageName3, "item.packageName");
                    FNotification fNotification = new FNotification(packageName, value, (byte) 2, packageName3, -1, "", NotificationBaseObj.ANotificationType.NOTIFICATION);
                    PortfolioApp c4 = PortfolioApp.W.c();
                    String e4 = PortfolioApp.W.c().e();
                    NotificationBaseObj.ANotificationType aNotificationType3 = NotificationBaseObj.ANotificationType.NOTIFICATION;
                    String packageName4 = lightAndHaptics.getPackageName();
                    kd4.a((Object) packageName4, "item.packageName");
                    String packageName5 = lightAndHaptics.getPackageName();
                    kd4.a((Object) packageName5, "item.packageName");
                    String packageName6 = lightAndHaptics.getPackageName();
                    kd4.a((Object) packageName6, "item.packageName");
                    c4.a(e4, (NotificationBaseObj) new HybridNotificationObj(0, aNotificationType3, fNotification, packageName4, packageName5, packageName6, cb4.d(NotificationBaseObj.ANotificationFlag.IMPORTANT)));
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
    }

    @DexIgnore
    public final void a(NotificationInfo notificationInfo, boolean z) {
        kd4.b(notificationInfo, "info");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "PACKAGE CHECK : " + notificationInfo.getPackageName());
        LightAndHaptics b2 = b(notificationInfo.getSenderInfo());
        if (b2 == null && z) {
            String packageName = notificationInfo.getPackageName();
            kd4.a((Object) packageName, "info.packageName");
            if (a(packageName)) {
                String packageName2 = notificationInfo.getPackageName();
                kd4.a((Object) packageName2, "info.packageName");
                b2 = a(packageName2, notificationInfo.getBody());
            }
            if (b2 == null) {
                List<ContactGroup> b3 = ms3.a().b("-5678", MFDeviceFamily.DEVICE_FAMILY_SAM);
                NotificationSource source = notificationInfo.getSource();
                kd4.a((Object) source, "info.source");
                b2 = a("-5678", (List<? extends ContactGroup>) b3, source);
            }
        }
        if (b2 != null) {
            a(b2);
        }
    }

    @DexIgnore
    public final void a(LightAndHaptics lightAndHaptics) {
        if (this.c.size() < 5) {
            this.c.add(lightAndHaptics);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.d(str, "addEventToQueue : " + lightAndHaptics.getType());
            if (!this.d) {
                this.d = true;
                c();
            }
        }
    }

    @DexIgnore
    public final void c() {
        if (!this.c.isEmpty()) {
            try {
                LightAndHaptics lightAndHaptics = (LightAndHaptics) this.c.remove();
                kd4.a((Object) lightAndHaptics, "nextItem");
                b(lightAndHaptics);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = g;
                local.d(str, "Total duration: " + 500);
                this.e.postDelayed(this.f, 500);
            } catch (Exception e2) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = g;
                local2.d(str2, "exception when startQueue " + e2);
            }
        } else {
            this.d = false;
        }
    }

    @DexIgnore
    public final LightAndHaptics a(String str, List<? extends ContactGroup> list, NotificationSource notificationSource) {
        if (list != null && (!list.isEmpty())) {
            ContactGroup contactGroup = (ContactGroup) list.get(0);
            Contact contactWithPhoneNumber = contactGroup.getContactWithPhoneNumber(str);
            kd4.a((Object) contactWithPhoneNumber, "contact");
            String firstName = contactWithPhoneNumber.getFirstName();
            if (!TextUtils.isEmpty(contactWithPhoneNumber.getLastName()) && !qf4.b(contactWithPhoneNumber.getLastName(), "null", true)) {
                firstName = firstName + " " + contactWithPhoneNumber.getLastName();
            }
            String str2 = firstName;
            int i2 = ym2.b[notificationSource.ordinal()];
            if (i2 == 1) {
                return new LightAndHaptics("", NotificationType.CALL, str2, contactGroup.getHour(), NotificationPriority.ENTOURAGE_CALL);
            }
            if (i2 == 2) {
                return new LightAndHaptics("", NotificationType.SMS, str2, contactGroup.getHour(), NotificationPriority.ENTOURAGE_SMS);
            }
        }
        return null;
    }

    @DexIgnore
    public final boolean a(String str) {
        if (TextUtils.equals(Telephony.Sms.getDefaultSmsPackage(PortfolioApp.W.c()), str)) {
            en2 en2 = this.b;
            if (en2 == null) {
                kd4.d("mSharePref");
                throw null;
            } else if (en2.M()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final LightAndHaptics a(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = g;
        local.d(str3, "Attepting to match app " + str);
        AppFilter a2 = NotificationAppHelper.b.a(str, MFDeviceFamily.DEVICE_FAMILY_SAM);
        if (a2 == null) {
            return null;
        }
        AppFilterHistory appFilterHistory = new AppFilterHistory("");
        appFilterHistory.setColor(a2.getColor());
        appFilterHistory.setHaptic(a2.getHaptic());
        appFilterHistory.setTitle(a2.getName());
        appFilterHistory.setSubTitle(str2);
        appFilterHistory.setType(a2.getType());
        NotificationType notificationType = NotificationType.APP_FILTER;
        if (kd4.a((Object) str, (Object) AppType.ALL_CALLS.name())) {
            notificationType = NotificationType.CALL;
        } else if (kd4.a((Object) str, (Object) AppType.ALL_SMS.name())) {
            notificationType = NotificationType.SMS;
        } else if (kd4.a((Object) str, (Object) AppType.ALL_EMAIL.name())) {
            notificationType = NotificationType.EMAIL;
        }
        return new LightAndHaptics(str, notificationType, "", a2.getHour(), NotificationPriority.APP_FILTER);
    }

    @DexIgnore
    public final boolean a() {
        for (AppFilter next : dn2.p.a().a().getAllAppFilters()) {
            kd4.a((Object) next, "item");
            if (qf4.b(next.getType(), Telephony.Sms.getDefaultSmsPackage(PortfolioApp.W.c()), true)) {
                FLogger.INSTANCE.getLocal().d(g, ".Inside hasMessageApp, return true");
                return true;
            }
        }
        FLogger.INSTANCE.getLocal().d(g, ".Inside hasMessageApp, return false");
        return false;
    }
}
