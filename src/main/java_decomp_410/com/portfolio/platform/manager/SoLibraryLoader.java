package com.portfolio.platform.manager;

import android.content.Context;
import android.util.Base64;
import com.google.gson.Gson;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.AccessGroup;
import java.nio.charset.StandardCharsets;
import java.util.Random;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class SoLibraryLoader {
    @DexIgnore
    public char[][] a; // = {new char[]{'A', 1, '1', 'D', 'E', 'F'}, new char[]{'F', 2, 'H', 'L', 'K', '~'}, new char[]{'#', 'O', 'P', 'Q', 'R', 'S'}, new char[]{'T', '1', 'V', 'W', ')', 'Y'}, new char[]{'$', 'I', 'D', 'O', '0', 'E'}, new char[]{'X', 'V', '%', '^', 'G', '3'}, new char[]{'a', 'b', 'c', '&', 'e', 'f'}, new char[]{'g', 'h', 'i', 'j', 'l', 'k'}, new char[]{'k', 'm', 'n', 'o', 'p', 'q'}, new char[]{'1', '2', 8, '4', '5', '6'}, new char[]{'7', '(', '9', '0', '+', '-'}, new char[]{',', '.', '!', '@', '#', '$'}, new char[]{'^', '&', '|', ']', '[', ';'}};
    @DexIgnore
    public Gson b; // = new Gson();

    /*
    static {
        System.loadLibrary("res-c");
    }
    */

    @DexIgnore
    public Access a(Context context) {
        int i;
        Random random = new Random();
        int nextInt = random.nextInt(1000);
        while (true) {
            i = nextInt + 1;
            if (a(i)) {
                break;
            }
            nextInt = random.nextInt(1000);
        }
        int hashCode = "release".hashCode();
        char c = 65535;
        char c2 = (hashCode == -1897523141 || hashCode != 1090594823) ? (char) 65535 : 0;
        try {
            AccessGroup accessGroup = (AccessGroup) this.b.a(a(a(new String(getData(i, c2 != 0 ? c2 != 1 ? 2 : 1 : 0), StandardCharsets.UTF_8), i), a(new String(find(context, i), StandardCharsets.UTF_8), i).trim()), AccessGroup.class);
            if (accessGroup == null) {
                return null;
            }
            int hashCode2 = "release".hashCode();
            if (hashCode2 != -1897523141) {
                if (hashCode2 == 1090594823) {
                    c = 0;
                }
            }
            if (c == 0) {
                return accessGroup.getPie();
            }
            if (c != 1) {
                return accessGroup.getLollipop();
            }
            return accessGroup.getOreo();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final native byte[] find(Context context, int i);

    @DexIgnore
    public final native byte[] getData(int i, int i2);

    @DexIgnore
    public final boolean a(int i) {
        int i2 = 3;
        if (i > 3 && i % 2 != 0) {
            while (((double) i2) <= Math.sqrt((double) i) && i % i2 != 0) {
                i2 += 2;
            }
            if (i % i2 != 0) {
                return true;
            }
            return false;
        } else if (i == 2 || i == 3) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public final String a(String str, int i) {
        char[][] cArr = this.a;
        char[] cArr2 = {cArr[1][5], cArr[0][1], cArr[1][1], cArr[2][0], cArr[4][0], cArr[5][2], cArr[5][3], cArr[6][3], cArr[9][2], cArr[10][1], cArr[3][4], 0};
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < str.length(); i2++) {
            sb.append((char) (str.charAt(i2) ^ cArr2[(i2 + i) % cArr2.length]));
        }
        return sb.toString();
    }

    @DexIgnore
    public final String a(String str, String str2) throws Exception {
        String[] split = str.split("]");
        if (split.length == 3) {
            try {
                byte[] decode = Base64.decode(split[0], 0);
                byte[] decode2 = Base64.decode(split[1], 0);
                byte[] decode3 = Base64.decode(split[2], 0);
                SecretKey a2 = a(decode, str2);
                Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                instance.init(2, a2, new GCMParameterSpec(128, decode2));
                return new String(instance.doFinal(decode3), "UTF-8");
            } catch (Throwable th) {
                throw new Exception("Error while decryption", th);
            }
        } else {
            throw new IllegalArgumentException("Invalid encypted text format");
        }
    }

    @DexIgnore
    public final SecretKey a(byte[] bArr, String str) throws Exception {
        try {
            return new SecretKeySpec(SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1", "BC").generateSecret(new PBEKeySpec(str.toCharArray(), bArr, 1000, 128)).getEncoded(), "AES");
        } catch (Throwable th) {
            throw new Exception("Error while generating key", th);
        }
    }
}
