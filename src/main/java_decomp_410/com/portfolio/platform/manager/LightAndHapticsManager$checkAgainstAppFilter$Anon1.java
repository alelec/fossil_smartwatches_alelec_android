package com.portfolio.platform.manager;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.NotificationType;
import com.portfolio.platform.data.model.LightAndHaptics;
import com.portfolio.platform.data.model.NotificationInfo;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstAppFilter$Anon1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
public final class LightAndHapticsManager$checkAgainstAppFilter$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationInfo $info;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LightAndHapticsManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LightAndHapticsManager$checkAgainstAppFilter$Anon1(LightAndHapticsManager lightAndHapticsManager, NotificationInfo notificationInfo, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = lightAndHapticsManager;
        this.$info = notificationInfo;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        LightAndHapticsManager$checkAgainstAppFilter$Anon1 lightAndHapticsManager$checkAgainstAppFilter$Anon1 = new LightAndHapticsManager$checkAgainstAppFilter$Anon1(this.this$Anon0, this.$info, yb4);
        lightAndHapticsManager$checkAgainstAppFilter$Anon1.p$ = (zg4) obj;
        return lightAndHapticsManager$checkAgainstAppFilter$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LightAndHapticsManager$checkAgainstAppFilter$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            LightAndHapticsManager lightAndHapticsManager = this.this$Anon0;
            String packageName = this.$info.getPackageName();
            kd4.a((Object) packageName, "info.packageName");
            LightAndHaptics a = lightAndHapticsManager.a(packageName, this.$info.getBody());
            if (a != null) {
                a.setNotificationType(NotificationType.APP_FILTER);
                this.this$Anon0.a(a);
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
