package com.portfolio.platform.manager.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ap2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hz3;
import com.fossil.blesdk.obfuscated.iz3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ln2;
import com.fossil.blesdk.obfuscated.ud0;
import com.fossil.blesdk.obfuscated.us3;
import com.fossil.blesdk.obfuscated.xz3;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.source.remote.WechatApiService;
import okhttp3.Interceptor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFLoginWechatManager implements xz3 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a((fd4) null);
    @DexIgnore
    public WechatApiService e; // = ((WechatApiService) ap2.g.a(WechatApiService.class));
    @DexIgnore
    public boolean f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return MFLoginWechatManager.g;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MFLoginWechatManager e;
        @DexIgnore
        public /* final */ /* synthetic */ ln2 f;

        @DexIgnore
        public b(MFLoginWechatManager mFLoginWechatManager, ln2 ln2) {
            this.e = mFLoginWechatManager;
            this.f = ln2;
        }

        @DexIgnore
        public final void run() {
            if (!this.e.b()) {
                this.f.a(500, (ud0) null, "");
            }
        }
    }

    /*
    static {
        String simpleName = MFLoginWechatManager.class.getSimpleName();
        kd4.a((Object) simpleName, "MFLoginWechatManager::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public MFLoginWechatManager() {
        ap2.g.a("https://api.weixin.qq.com/sns/");
        ap2.g.a((Interceptor) null);
    }

    @DexIgnore
    public final WechatApiService a() {
        return this.e;
    }

    @DexIgnore
    public final void a(String str) {
    }

    @DexIgnore
    public final boolean b() {
        return this.f;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.f = z;
    }

    @DexIgnore
    public final void b(String str) {
        kd4.b(str, "appId");
        us3.a().a(str);
    }

    @DexIgnore
    public final void a(Intent intent) {
        kd4.b(intent, "intent");
        us3.a().a(intent, this);
    }

    @DexIgnore
    public final void a(Activity activity, ln2 ln2) {
        throw null;
        // kd4.b(activity, Constants.ACTIVITY);
        // kd4.b(ln2, Constants.CALLBACK);
        // us3.a().a((Context) activity);
        // us3.a().a(activity.getIntent(), this);
        // this.f = false;
        // Handler handler = new Handler(Looper.getMainLooper());
        // handler.postDelayed(new b(this, ln2), ScanService.BLE_SCAN_TIMEOUT);
        // us3.a().a((us3.a) new MFLoginWechatManager$loginWithWechat$Anon2(this, handler, ln2));
    }

    @DexIgnore
    public void a(hz3 hz3) {
        kd4.b(hz3, "baseReq");
        us3.a().a(hz3);
    }

    @DexIgnore
    public void a(iz3 iz3) {
        kd4.b(iz3, "baseResp");
        us3.a().a(iz3);
    }
}
