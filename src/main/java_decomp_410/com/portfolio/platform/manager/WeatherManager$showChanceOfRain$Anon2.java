package com.portfolio.platform.manager;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.CustomizeRealData;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.manager.WeatherManager$showChanceOfRain$Anon2", f = "WeatherManager.kt", l = {}, m = "invokeSuspend")
public final class WeatherManager$showChanceOfRain$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $rainProbabilityPercent;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$showChanceOfRain$Anon2(WeatherManager weatherManager, int i, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = weatherManager;
        this.$rainProbabilityPercent = i;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WeatherManager$showChanceOfRain$Anon2 weatherManager$showChanceOfRain$Anon2 = new WeatherManager$showChanceOfRain$Anon2(this.this$Anon0, this.$rainProbabilityPercent, yb4);
        weatherManager$showChanceOfRain$Anon2.p$ = (zg4) obj;
        return weatherManager$showChanceOfRain$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WeatherManager$showChanceOfRain$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            this.this$Anon0.b().upsertCustomizeRealData(new CustomizeRealData("chance_of_rain", String.valueOf(this.$rainProbabilityPercent)));
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
