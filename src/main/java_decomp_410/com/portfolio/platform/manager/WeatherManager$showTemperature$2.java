package com.portfolio.platform.manager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.manager.WeatherManager$showTemperature$2", mo27670f = "WeatherManager.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class WeatherManager$showTemperature$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ float $tempInCelsius;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21286p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.manager.WeatherManager this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$showTemperature$2(com.portfolio.platform.manager.WeatherManager weatherManager, float f, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = weatherManager;
        this.$tempInCelsius = f;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.manager.WeatherManager$showTemperature$2 weatherManager$showTemperature$2 = new com.portfolio.platform.manager.WeatherManager$showTemperature$2(this.this$0, this.$tempInCelsius, yb4);
        weatherManager$showTemperature$2.f21286p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return weatherManager$showTemperature$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.manager.WeatherManager$showTemperature$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            this.this$0.mo39692b().upsertCustomizeRealData(new com.portfolio.platform.data.model.CustomizeRealData("temperature", java.lang.String.valueOf(this.$tempInCelsius)));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
