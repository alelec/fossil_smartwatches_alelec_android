package com.portfolio.platform.manager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.manager.WeatherManager$getWeatherForWatchApp$1", mo27670f = "WeatherManager.kt", mo27671l = {120, 128, 132}, mo27672m = "invokeSuspend")
public final class WeatherManager$getWeatherForWatchApp$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int I$0;
    @DexIgnore
    public int I$1;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$10;
    @DexIgnore
    public java.lang.Object L$11;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public java.lang.Object L$7;
    @DexIgnore
    public java.lang.Object L$8;
    @DexIgnore
    public java.lang.Object L$9;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21284p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.manager.WeatherManager this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$getWeatherForWatchApp$1(com.portfolio.platform.manager.WeatherManager weatherManager, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = weatherManager;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.manager.WeatherManager$getWeatherForWatchApp$1 weatherManager$getWeatherForWatchApp$1 = new com.portfolio.platform.manager.WeatherManager$getWeatherForWatchApp$1(this.this$0, yb4);
        weatherManager$getWeatherForWatchApp$1.f21284p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return weatherManager$getWeatherForWatchApp$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.manager.WeatherManager$getWeatherForWatchApp$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0260  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x02ab  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0314  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0382  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0387  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x038e  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x03a3  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x03ba  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x03ca  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object obj2;
        com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting;
        com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset;
        com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset2;
        com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting2;
        java.lang.String str;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        com.portfolio.platform.data.model.MFUser mFUser;
        java.util.ArrayList arrayList;
        com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper weatherLocationWrapper;
        int i;
        kotlin.Pair pair;
        kotlin.Pair pair2;
        kotlin.Pair pair3;
        com.portfolio.platform.manager.WeatherManager$getWeatherForWatchApp$1 weatherManager$getWeatherForWatchApp$1;
        int i2;
        int i3;
        int i4;
        kotlin.Pair pair4;
        java.lang.Object obj3;
        java.lang.String address;
        com.portfolio.platform.data.model.microapp.weather.Weather weather;
        kotlin.Pair pair5;
        java.lang.Object obj4;
        com.portfolio.platform.data.model.microapp.weather.Weather weather2;
        java.lang.Object obj5;
        com.portfolio.platform.data.model.microapp.weather.Weather weather3;
        java.lang.Object obj6;
        com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting3;
        int i5;
        java.lang.Object obj7;
        int i6;
        com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting4;
        java.lang.String str2;
        java.lang.String b;
        T t;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i7 = this.label;
        int i8 = 1;
        if (i7 == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f21284p$;
            com.portfolio.platform.data.model.MFUser currentUser = this.this$0.mo39698e().getCurrentUser();
            if (currentUser != null) {
                com.portfolio.platform.enums.Unit temperatureUnit = currentUser.getTemperatureUnit();
                if (temperatureUnit != null) {
                    int i9 = com.fossil.blesdk.obfuscated.gn2.f15149a[temperatureUnit.ordinal()];
                    if (i9 == 1) {
                        str2 = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.CELSIUS.getValue();
                    } else if (i9 == 2) {
                        str2 = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.FAHRENHEIT.getValue();
                    }
                    str = str2;
                    com.portfolio.platform.data.source.DianaPresetRepository c = this.this$0.mo39695c();
                    b = this.this$0.f21275i;
                    if (b == null) {
                        com.portfolio.platform.data.model.diana.preset.DianaPreset activePresetBySerial = c.getActivePresetBySerial(b);
                        if (activePresetBySerial != null) {
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String h = com.portfolio.platform.manager.WeatherManager.f21264l;
                            local.mo33255d(h, "getWeatherForWatchApp activePreset " + activePresetBySerial);
                            java.util.Iterator<T> it = activePresetBySerial.getWatchapps().iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    t = null;
                                    break;
                                }
                                t = it.next();
                                if (com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) t).getId(), (java.lang.Object) "weather")).booleanValue()) {
                                    break;
                                }
                            }
                            com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting5 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) t;
                            if (dianaPresetWatchAppSetting5 != null) {
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                java.lang.String h2 = com.portfolio.platform.manager.WeatherManager.f21264l;
                                local2.mo33255d(h2, "getWeatherForWatchApp settings=" + dianaPresetWatchAppSetting5.getSettings());
                                if (!com.fossil.blesdk.obfuscated.oj2.m26116a(dianaPresetWatchAppSetting5.getSettings())) {
                                    this.this$0.f21274h = (com.portfolio.platform.data.model.setting.WeatherWatchAppSetting) new com.google.gson.Gson().mo23093a(dianaPresetWatchAppSetting5.getSettings(), com.portfolio.platform.data.model.setting.WeatherWatchAppSetting.class);
                                    com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper weatherLocationWrapper2 = new com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper("", 0.0d, 0.0d, (java.lang.String) null, (java.lang.String) null, true, false, 94, (com.fossil.blesdk.obfuscated.fd4) null);
                                    com.portfolio.platform.data.model.setting.WeatherWatchAppSetting c2 = this.this$0.f21274h;
                                    if (c2 != null) {
                                        c2.getLocations().add(0, weatherLocationWrapper2);
                                        java.util.ArrayList arrayList2 = new java.util.ArrayList();
                                        com.portfolio.platform.data.model.setting.WeatherWatchAppSetting c3 = this.this$0.f21274h;
                                        if (c3 != null) {
                                            for (com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper weatherManager$getWeatherForWatchApp$1$invokeSuspend$$inlined$let$lambda$1 : c3.getLocations()) {
                                                java.util.ArrayList arrayList3 = arrayList2;
                                                com.portfolio.platform.manager.C5973x576ed115 weatherManager$getWeatherForWatchApp$1$invokeSuspend$$inlined$let$lambda$12 = new com.portfolio.platform.manager.C5973x576ed115(weatherManager$getWeatherForWatchApp$1$invokeSuspend$$inlined$let$lambda$1, (com.fossil.blesdk.obfuscated.yb4) null, str, this, zg42);
                                                arrayList3.add(com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, weatherManager$getWeatherForWatchApp$1$invokeSuspend$$inlined$let$lambda$12, 3, (java.lang.Object) null));
                                                arrayList2 = arrayList3;
                                            }
                                            arrayList = arrayList2;
                                            i3 = arrayList.size();
                                            weatherManager$getWeatherForWatchApp$1 = this;
                                            obj2 = a;
                                            dianaPresetWatchAppSetting = dianaPresetWatchAppSetting5;
                                            weatherLocationWrapper = weatherLocationWrapper2;
                                            i2 = 0;
                                            pair3 = null;
                                            pair2 = null;
                                            pair = null;
                                            dianaPreset = activePresetBySerial;
                                            dianaPreset2 = dianaPreset;
                                            dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting;
                                            com.portfolio.platform.data.model.MFUser mFUser2 = currentUser;
                                            zg4 = zg42;
                                            mFUser = mFUser2;
                                        } else {
                                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                                            throw null;
                                        }
                                    } else {
                                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                                        throw null;
                                    }
                                }
                            }
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                }
                str2 = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.CELSIUS.getValue();
                str = str2;
                com.portfolio.platform.data.source.DianaPresetRepository c4 = this.this$0.mo39695c();
                b = this.this$0.f21275i;
                if (b == null) {
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i7 == 1) {
            i5 = this.I$1;
            int i10 = this.I$0;
            pair3 = (kotlin.Pair) this.L$10;
            kotlin.Pair pair6 = (kotlin.Pair) this.L$7;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            i4 = i10;
            dianaPresetWatchAppSetting3 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) this.L$9;
            obj6 = a;
            weatherLocationWrapper = (com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper) this.L$6;
            obj3 = obj;
            arrayList = (java.util.List) this.L$8;
            pair4 = (kotlin.Pair) this.L$11;
            weatherManager$getWeatherForWatchApp$1 = this;
            java.lang.String str3 = (java.lang.String) this.L$2;
            dianaPresetWatchAppSetting2 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) this.L$5;
            mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
            dianaPreset2 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) this.L$4;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            dianaPreset = (com.portfolio.platform.data.model.diana.preset.DianaPreset) this.L$3;
            str = str3;
            kotlin.Pair pair7 = (kotlin.Pair) obj3;
            com.portfolio.platform.data.model.microapp.weather.Weather weather4 = (com.portfolio.platform.data.model.microapp.weather.Weather) pair7.getFirst();
            address = weather4 == null ? weather4.getAddress() : null;
            if (android.text.TextUtils.isEmpty(address)) {
                address = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.Customization_Buttons_DianaWeatherAddLocation_Text__CurrentLocation);
            }
            weather = (com.portfolio.platform.data.model.microapp.weather.Weather) pair7.getFirst();
            if (weather != null) {
                java.lang.String a2 = com.fossil.blesdk.obfuscated.ts3.m28439a(address);
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a2, "Utils.formatLocationName(name)");
                weather.setAddress(a2);
            }
            pair2 = pair4;
            i = 1;
            pair = pair7;
            i2 = i4;
            i2 += i;
            i8 = 1;
        } else if (i7 == 2) {
            i6 = this.I$1;
            i2 = this.I$0;
            kotlin.Pair pair8 = (kotlin.Pair) this.L$10;
            pair = (kotlin.Pair) this.L$7;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            obj4 = obj;
            pair5 = (kotlin.Pair) this.L$11;
            dianaPresetWatchAppSetting4 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) this.L$9;
            weatherManager$getWeatherForWatchApp$1 = this;
            obj7 = a;
            weatherLocationWrapper = (com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper) this.L$6;
            arrayList = (java.util.List) this.L$8;
            java.lang.String str4 = (java.lang.String) this.L$2;
            dianaPresetWatchAppSetting2 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) this.L$5;
            mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
            dianaPreset2 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) this.L$4;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            dianaPreset = (com.portfolio.platform.data.model.diana.preset.DianaPreset) this.L$3;
            str = str4;
            kotlin.Pair pair9 = (kotlin.Pair) obj4;
            weather2 = (com.portfolio.platform.data.model.microapp.weather.Weather) pair9.getFirst();
            if (weather2 != null) {
                com.portfolio.platform.data.model.setting.WeatherWatchAppSetting c5 = weatherManager$getWeatherForWatchApp$1.this$0.f21274h;
                if (c5 != null) {
                    weather2.setAddress(c5.getLocations().get(i2).getName());
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
            pair3 = pair9;
            pair2 = pair5;
            i = 1;
            i2 += i;
            i8 = 1;
        } else if (i7 == 3) {
            i3 = this.I$1;
            i2 = this.I$0;
            kotlin.Pair pair10 = (kotlin.Pair) this.L$11;
            pair = (kotlin.Pair) this.L$7;
            dianaPresetWatchAppSetting2 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) this.L$5;
            dianaPreset2 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) this.L$4;
            dianaPreset = (com.portfolio.platform.data.model.diana.preset.DianaPreset) this.L$3;
            str = (java.lang.String) this.L$2;
            mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            obj5 = obj;
            dianaPresetWatchAppSetting = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) this.L$9;
            obj2 = a;
            weatherLocationWrapper = (com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper) this.L$6;
            pair3 = (kotlin.Pair) this.L$10;
            arrayList = (java.util.List) this.L$8;
            weatherManager$getWeatherForWatchApp$1 = this;
            kotlin.Pair pair11 = (kotlin.Pair) obj5;
            weather3 = (com.portfolio.platform.data.model.microapp.weather.Weather) pair11.getFirst();
            if (weather3 != null) {
                com.portfolio.platform.data.model.setting.WeatherWatchAppSetting c6 = weatherManager$getWeatherForWatchApp$1.this$0.f21274h;
                if (c6 != null) {
                    weather3.setAddress(c6.getLocations().get(i2).getName());
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
            pair2 = pair11;
            i = 1;
            i2 += i;
            i8 = 1;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (i2 >= i3) {
            weatherManager$getWeatherForWatchApp$1.this$0.mo39691a((kotlin.Pair<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>) pair, (kotlin.Pair<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>) pair3, (kotlin.Pair<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>) pair2);
        } else {
            if (i2 == 0) {
                int i11 = i3;
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting6 = dianaPresetWatchAppSetting;
                java.lang.Object obj8 = obj2;
                weatherManager$getWeatherForWatchApp$1.L$0 = zg4;
                weatherManager$getWeatherForWatchApp$1.L$1 = mFUser;
                weatherManager$getWeatherForWatchApp$1.L$2 = str;
                weatherManager$getWeatherForWatchApp$1.L$3 = dianaPreset;
                weatherManager$getWeatherForWatchApp$1.L$4 = dianaPreset2;
                weatherManager$getWeatherForWatchApp$1.L$5 = dianaPresetWatchAppSetting2;
                weatherManager$getWeatherForWatchApp$1.L$6 = weatherLocationWrapper;
                weatherManager$getWeatherForWatchApp$1.L$7 = pair;
                weatherManager$getWeatherForWatchApp$1.L$8 = arrayList;
                weatherManager$getWeatherForWatchApp$1.L$9 = dianaPresetWatchAppSetting6;
                weatherManager$getWeatherForWatchApp$1.L$10 = pair3;
                pair4 = pair2;
                weatherManager$getWeatherForWatchApp$1.L$11 = pair4;
                weatherManager$getWeatherForWatchApp$1.I$0 = i2;
                weatherManager$getWeatherForWatchApp$1.I$1 = i11;
                i4 = i2;
                weatherManager$getWeatherForWatchApp$1.label = 1;
                obj3 = ((com.fossil.blesdk.obfuscated.gh4) arrayList.get(i2)).mo27693a(weatherManager$getWeatherForWatchApp$1);
                if (obj3 == obj8) {
                    return obj8;
                }
                obj6 = obj8;
                dianaPresetWatchAppSetting3 = dianaPresetWatchAppSetting6;
                i5 = i11;
                kotlin.Pair pair72 = (kotlin.Pair) obj3;
                com.portfolio.platform.data.model.microapp.weather.Weather weather42 = (com.portfolio.platform.data.model.microapp.weather.Weather) pair72.getFirst();
                if (weather42 == null) {
                }
                if (android.text.TextUtils.isEmpty(address)) {
                }
                weather = (com.portfolio.platform.data.model.microapp.weather.Weather) pair72.getFirst();
                if (weather != null) {
                }
                pair2 = pair4;
                i = 1;
                pair = pair72;
                i2 = i4;
                i2 += i;
                i8 = 1;
                if (i2 >= i3) {
                }
                return obj8;
            }
            if (i2 == i8) {
                int i12 = i3;
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting7 = dianaPresetWatchAppSetting;
                java.lang.Object obj9 = obj2;
                weatherManager$getWeatherForWatchApp$1.L$0 = zg4;
                weatherManager$getWeatherForWatchApp$1.L$1 = mFUser;
                weatherManager$getWeatherForWatchApp$1.L$2 = str;
                weatherManager$getWeatherForWatchApp$1.L$3 = dianaPreset;
                weatherManager$getWeatherForWatchApp$1.L$4 = dianaPreset2;
                weatherManager$getWeatherForWatchApp$1.L$5 = dianaPresetWatchAppSetting2;
                weatherManager$getWeatherForWatchApp$1.L$6 = weatherLocationWrapper;
                weatherManager$getWeatherForWatchApp$1.L$7 = pair;
                weatherManager$getWeatherForWatchApp$1.L$8 = arrayList;
                weatherManager$getWeatherForWatchApp$1.L$9 = dianaPresetWatchAppSetting7;
                weatherManager$getWeatherForWatchApp$1.L$10 = pair3;
                weatherManager$getWeatherForWatchApp$1.L$11 = pair2;
                weatherManager$getWeatherForWatchApp$1.I$0 = i2;
                int i13 = i12;
                weatherManager$getWeatherForWatchApp$1.I$1 = i13;
                pair5 = pair2;
                weatherManager$getWeatherForWatchApp$1.label = 2;
                obj4 = ((com.fossil.blesdk.obfuscated.gh4) arrayList.get(i2)).mo27693a(weatherManager$getWeatherForWatchApp$1);
                if (obj4 == obj9) {
                    return obj9;
                }
                obj7 = obj9;
                i6 = i13;
                dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting7;
                kotlin.Pair pair92 = (kotlin.Pair) obj4;
                weather2 = (com.portfolio.platform.data.model.microapp.weather.Weather) pair92.getFirst();
                if (weather2 != null) {
                }
                pair3 = pair92;
                pair2 = pair5;
                i = 1;
                i2 += i;
                i8 = 1;
                if (i2 >= i3) {
                }
                return obj9;
            }
            if (i2 == 2) {
                weatherManager$getWeatherForWatchApp$1.L$0 = zg4;
                weatherManager$getWeatherForWatchApp$1.L$1 = mFUser;
                weatherManager$getWeatherForWatchApp$1.L$2 = str;
                weatherManager$getWeatherForWatchApp$1.L$3 = dianaPreset;
                weatherManager$getWeatherForWatchApp$1.L$4 = dianaPreset2;
                weatherManager$getWeatherForWatchApp$1.L$5 = dianaPresetWatchAppSetting2;
                weatherManager$getWeatherForWatchApp$1.L$6 = weatherLocationWrapper;
                weatherManager$getWeatherForWatchApp$1.L$7 = pair;
                weatherManager$getWeatherForWatchApp$1.L$8 = arrayList;
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting8 = dianaPresetWatchAppSetting;
                weatherManager$getWeatherForWatchApp$1.L$9 = dianaPresetWatchAppSetting8;
                weatherManager$getWeatherForWatchApp$1.L$10 = pair3;
                weatherManager$getWeatherForWatchApp$1.L$11 = pair2;
                weatherManager$getWeatherForWatchApp$1.I$0 = i2;
                weatherManager$getWeatherForWatchApp$1.I$1 = i3;
                weatherManager$getWeatherForWatchApp$1.label = 3;
                obj5 = ((com.fossil.blesdk.obfuscated.gh4) arrayList.get(i2)).mo27693a(weatherManager$getWeatherForWatchApp$1);
                int i14 = i3;
                java.lang.Object obj10 = obj2;
                if (obj5 == obj10) {
                    return obj10;
                }
                obj2 = obj10;
                dianaPresetWatchAppSetting = dianaPresetWatchAppSetting8;
                i3 = i14;
                kotlin.Pair pair112 = (kotlin.Pair) obj5;
                weather3 = (com.portfolio.platform.data.model.microapp.weather.Weather) pair112.getFirst();
                if (weather3 != null) {
                }
                pair2 = pair112;
                return obj10;
            }
            i = 1;
            i2 += i;
            i8 = 1;
            if (i2 >= i3) {
            }
        }
        weatherManager$getWeatherForWatchApp$1.this$0.mo39691a((kotlin.Pair<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>) pair, (kotlin.Pair<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>) pair3, (kotlin.Pair<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>) pair2);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
