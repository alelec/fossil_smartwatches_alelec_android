package com.portfolio.platform.manager;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Pair<? extends Weather, ? extends Boolean>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ WeatherLocationWrapper $location;
    @DexIgnore
    public /* final */ /* synthetic */ String $tempUnit$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ zg4 $this_launch$inlined;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager$getWeatherForWatchApp$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(WeatherLocationWrapper weatherLocationWrapper, yb4 yb4, String str, WeatherManager$getWeatherForWatchApp$Anon1 weatherManager$getWeatherForWatchApp$Anon1, zg4 zg4) {
        super(2, yb4);
        this.$location = weatherLocationWrapper;
        this.$tempUnit$inlined = str;
        this.this$Anon0 = weatherManager$getWeatherForWatchApp$Anon1;
        this.$this_launch$inlined = zg4;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WeatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 weatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new WeatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(this.$location, yb4, this.$tempUnit$inlined, this.this$Anon0, this.$this_launch$inlined);
        weatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return weatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WeatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            if (this.$location.isUseCurrentLocation()) {
                WeatherManager weatherManager = this.this$Anon0.this$Anon0;
                String str = this.$tempUnit$inlined;
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = weatherManager.a("weather", str, (yb4<? super Pair<Weather, Boolean>>) this);
                if (obj == a) {
                    return a;
                }
            } else {
                WeatherManager weatherManager2 = this.this$Anon0.this$Anon0;
                double lat = this.$location.getLat();
                double lng = this.$location.getLng();
                String str2 = this.$tempUnit$inlined;
                this.L$Anon0 = zg4;
                this.label = 2;
                obj = weatherManager2.a(lat, lng, str2, (yb4<? super Pair<Weather, Boolean>>) this);
                if (obj == a) {
                    return a;
                }
            }
        } else if (i == 1 || i == 2) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return (Pair) obj;
    }
}
