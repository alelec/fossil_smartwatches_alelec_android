package com.portfolio.platform.manager;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.AppType;
import com.portfolio.platform.data.NotificationType;
import com.portfolio.platform.data.model.LightAndHaptics;
import com.portfolio.platform.data.model.NotificationInfo;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstSMS$Anon1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
public final class LightAndHapticsManager$checkAgainstSMS$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationInfo $info;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LightAndHapticsManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LightAndHapticsManager$checkAgainstSMS$Anon1(LightAndHapticsManager lightAndHapticsManager, NotificationInfo notificationInfo, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = lightAndHapticsManager;
        this.$info = notificationInfo;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        LightAndHapticsManager$checkAgainstSMS$Anon1 lightAndHapticsManager$checkAgainstSMS$Anon1 = new LightAndHapticsManager$checkAgainstSMS$Anon1(this.this$Anon0, this.$info, yb4);
        lightAndHapticsManager$checkAgainstSMS$Anon1.p$ = (zg4) obj;
        return lightAndHapticsManager$checkAgainstSMS$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LightAndHapticsManager$checkAgainstSMS$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = LightAndHapticsManager.g;
            local.e(e, "Check again sms with senderInfo " + this.$info.getSenderInfo() + " and body " + this.$info.getBody());
            LightAndHaptics a = this.this$Anon0.a(AppType.ALL_SMS.name(), this.$info.getBody());
            if (a != null) {
                a.setNotificationType(NotificationType.SMS);
                this.this$Anon0.a(a);
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
