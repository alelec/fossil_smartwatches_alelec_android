package com.portfolio.platform.manager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.manager.WeatherManager$getWeatherForTemperature$1", mo27670f = "WeatherManager.kt", mo27671l = {174}, mo27672m = "invokeSuspend")
public final class WeatherManager$getWeatherForTemperature$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21283p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.manager.WeatherManager this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$getWeatherForTemperature$1(com.portfolio.platform.manager.WeatherManager weatherManager, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = weatherManager;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.manager.WeatherManager$getWeatherForTemperature$1 weatherManager$getWeatherForTemperature$1 = new com.portfolio.platform.manager.WeatherManager$getWeatherForTemperature$1(this.this$0, yb4);
        weatherManager$getWeatherForTemperature$1.f21283p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return weatherManager$getWeatherForTemperature$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.manager.WeatherManager$getWeatherForTemperature$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006e A[RETURN] */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.String str;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21283p$;
            com.portfolio.platform.data.model.MFUser currentUser = this.this$0.mo39698e().getCurrentUser();
            if (currentUser != null) {
                com.portfolio.platform.enums.Unit temperatureUnit = currentUser.getTemperatureUnit();
                if (temperatureUnit != null) {
                    int i2 = com.fossil.blesdk.obfuscated.gn2.f15151c[temperatureUnit.ordinal()];
                    if (i2 == 1) {
                        str = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.CELSIUS.getValue();
                    } else if (i2 == 2) {
                        str = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.FAHRENHEIT.getValue();
                    }
                    com.portfolio.platform.manager.WeatherManager weatherManager = this.this$0;
                    this.L$0 = zg4;
                    this.L$1 = currentUser;
                    this.L$2 = str;
                    this.label = 1;
                    obj = weatherManager.mo39687a("weather", str, (com.fossil.blesdk.obfuscated.yb4<? super kotlin.Pair<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>>) this);
                    if (obj == a) {
                        return a;
                    }
                }
                str = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.CELSIUS.getValue();
                com.portfolio.platform.manager.WeatherManager weatherManager2 = this.this$0;
                this.L$0 = zg4;
                this.L$1 = currentUser;
                this.L$2 = str;
                this.label = 1;
                obj = weatherManager2.mo39687a("weather", str, (com.fossil.blesdk.obfuscated.yb4<? super kotlin.Pair<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>>) this);
                if (obj == a) {
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            java.lang.String str2 = (java.lang.String) this.L$2;
            com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        kotlin.Pair pair = (kotlin.Pair) obj;
        com.portfolio.platform.data.model.microapp.weather.Weather weather = (com.portfolio.platform.data.model.microapp.weather.Weather) pair.component1();
        boolean booleanValue = ((java.lang.Boolean) pair.component2()).booleanValue();
        if (weather != null) {
            this.this$0.mo39693b(weather, booleanValue);
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
