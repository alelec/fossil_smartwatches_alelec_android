package com.portfolio.platform.manager;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {249}, m = "getWeather")
public final class WeatherManager$getWeather$Anon1 extends ContinuationImpl {
    @DexIgnore
    public double D$Anon0;
    @DexIgnore
    public double D$Anon1;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$getWeather$Anon1(WeatherManager weatherManager, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = weatherManager;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a(0.0d, 0.0d, (String) null, (yb4<? super Pair<Weather, Boolean>>) this);
    }
}
