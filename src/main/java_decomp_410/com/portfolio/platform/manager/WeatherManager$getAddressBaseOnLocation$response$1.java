package com.portfolio.platform.manager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.manager.WeatherManager$getAddressBaseOnLocation$response$1", mo27670f = "WeatherManager.kt", mo27671l = {221}, mo27672m = "invokeSuspend")
public final class WeatherManager$getAddressBaseOnLocation$response$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.fossil.blesdk.obfuscated.xz1>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ double $lat;
    @DexIgnore
    public /* final */ /* synthetic */ double $lng;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $type;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.manager.WeatherManager this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$getAddressBaseOnLocation$response$1(com.portfolio.platform.manager.WeatherManager weatherManager, double d, double d2, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(1, yb4);
        this.this$0 = weatherManager;
        this.$lat = d;
        this.$lng = d2;
        this.$type = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.manager.WeatherManager$getAddressBaseOnLocation$response$1 weatherManager$getAddressBaseOnLocation$response$1 = new com.portfolio.platform.manager.WeatherManager$getAddressBaseOnLocation$response$1(this.this$0, this.$lat, this.$lng, this.$type, yb4);
        return weatherManager$getAddressBaseOnLocation$response$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj) {
        return ((com.portfolio.platform.manager.WeatherManager$getAddressBaseOnLocation$response$1) create((com.fossil.blesdk.obfuscated.yb4) obj)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.lang.String str = this.$type;
            this.label = 1;
            obj = this.this$0.mo39697d().getAddressWithType(this.$lat + ',' + this.$lng, str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
