package com.portfolio.platform.manager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstSMS$1", mo27670f = "LightAndHapticsManager.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class LightAndHapticsManager$checkAgainstSMS$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.NotificationInfo $info;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21248p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.manager.LightAndHapticsManager this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LightAndHapticsManager$checkAgainstSMS$1(com.portfolio.platform.manager.LightAndHapticsManager lightAndHapticsManager, com.portfolio.platform.data.model.NotificationInfo notificationInfo, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = lightAndHapticsManager;
        this.$info = notificationInfo;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstSMS$1 lightAndHapticsManager$checkAgainstSMS$1 = new com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstSMS$1(this.this$0, this.$info, yb4);
        lightAndHapticsManager$checkAgainstSMS$1.f21248p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return lightAndHapticsManager$checkAgainstSMS$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstSMS$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String e = com.portfolio.platform.manager.LightAndHapticsManager.f21235g;
            local.mo33256e(e, "Check again sms with senderInfo " + this.$info.getSenderInfo() + " and body " + this.$info.getBody());
            com.portfolio.platform.data.model.LightAndHaptics a = this.this$0.mo39642a(com.portfolio.platform.data.AppType.ALL_SMS.name(), this.$info.getBody());
            if (a != null) {
                a.setNotificationType(com.portfolio.platform.data.NotificationType.SMS);
                this.this$0.mo39644a(a);
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
