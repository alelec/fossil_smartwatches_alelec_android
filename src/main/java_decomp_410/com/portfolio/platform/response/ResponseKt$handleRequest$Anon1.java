package com.portfolio.platform.response;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.response.ResponseKt", f = "Response.kt", l = {15}, m = "handleRequest")
public final class ResponseKt$handleRequest$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;

    @DexIgnore
    public ResponseKt$handleRequest$Anon1(yb4 yb4) {
        super(yb4);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return ResponseKt.a((xc4) null, this);
    }
}
