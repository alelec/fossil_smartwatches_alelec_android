package com.portfolio.platform.response;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.em4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.za4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.model.ServerError;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ResponseKt {
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public static final <T> Object a(xc4<? super yb4<? super qr4<T>>, ? extends Object> xc4, yb4<? super qo2<T>> yb4) {
        throw null;
        // ResponseKt$handleRequest$Anon1 responseKt$handleRequest$Anon1;
        // int i;
        // if (yb4 instanceof ResponseKt$handleRequest$Anon1) {
        //     responseKt$handleRequest$Anon1 = (ResponseKt$handleRequest$Anon1) yb4;
        //     int i2 = responseKt$handleRequest$Anon1.label;
        //     if ((i2 & Integer.MIN_VALUE) != 0) {
        //         responseKt$handleRequest$Anon1.label = i2 - Integer.MIN_VALUE;
        //         Object obj = responseKt$handleRequest$Anon1.result;
        //         Object a = cc4.a();
        //         i = responseKt$handleRequest$Anon1.label;
        //         if (i != 0) {
        //             na4.a((Object) obj);
        //             responseKt$handleRequest$Anon1.L$Anon0 = xc4;
        //             responseKt$handleRequest$Anon1.label = 1;
        //             obj = xc4.invoke(responseKt$handleRequest$Anon1);
        //             if (obj == a) {
        //                 return a;
        //             }
        //         } else if (i == 1) {
        //             xc4 xc42 = (xc4) responseKt$handleRequest$Anon1.L$Anon0;
        //             try {
        //                 na4.a((Object) obj);
        //             } catch (Exception e) {
        //                 Exception exc = e;
        //                 if (exc instanceof SocketTimeoutException) {
        //                     return new po2(MFNetworkReturnCode.CLIENT_TIMEOUT, (ServerError) null, exc, (String) null, 8, (fd4) null);
        //                 }
        //                 if (exc instanceof UnknownHostException) {
        //                     return new po2(601, (ServerError) null, exc, (String) null, 8, (fd4) null);
        //                 }
        //                 return new po2(600, (ServerError) null, exc, (String) null, 8, (fd4) null);
        //             }
        //         } else {
        //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        //         }
        //         return a((qr4) obj);
        //     }
        // }
        // responseKt$handleRequest$Anon1 = new ResponseKt$handleRequest$Anon1(yb4);
        // Object obj2 = responseKt$handleRequest$Anon1.result;
        // Object a2 = cc4.a();
        // i = responseKt$handleRequest$Anon1.label;
        // if (i != 0) {
        // }
        // return a((qr4) obj2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0102, code lost:
        if (r1 != null) goto L_0x0109;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0123, code lost:
        if (r1 != null) goto L_0x012a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0171, code lost:
        if (r1 != null) goto L_0x0178;
     */
    @DexIgnore
    public static final <T> qo2<T> b(qr4<T> qr4) {
        throw null;
        // String str;
        // String str2;
        // po2 po2;
        // String str3;
        // boolean z = true;
        // Integer num = null;
        // if (qr4.d()) {
        //     T a = qr4.a();
        //     if (qr4.f().A() != null) {
        //         FLogger.INSTANCE.getLocal().d("RepoResponse", "cacheResponse valid");
        //     } else {
        //         z = false;
        //     }
        //     if (qr4.f().G() != null) {
        //         Response G = qr4.f().G();
        //         if (G == null) {
        //             kd4.a();
        //             throw null;
        //         } else if (G.B() != 304) {
        //             ILocalFLogger local = FLogger.INSTANCE.getLocal();
        //             StringBuilder sb = new StringBuilder();
        //             sb.append("networkResponse valid httpCode ");
        //             Response G2 = qr4.f().G();
        //             if (G2 != null) {
        //                 num = Integer.valueOf(G2.B());
        //             }
        //             sb.append(num);
        //             local.d("RepoResponse", sb.toString());
        //             z = false;
        //         }
        //     }
        //     ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        //     local2.d("RepoResponse", "isFromCache=" + z);
        //     return new ro2(a, z);
        // }
        // int b = qr4.b();
        // if (za4.b((T[]) new Integer[]{504, 503, 500, 401, Integer.valueOf(MFNetworkReturnCode.RATE_LIMIT_EXEEDED), 601, Integer.valueOf(MFNetworkReturnCode.CLIENT_TIMEOUT), 413}, Integer.valueOf(b))) {
        //     ServerError serverError = new ServerError();
        //     serverError.setCode(Integer.valueOf(b));
        //     em4 c = qr4.c();
        //     if (c != null) {
        //         str3 = c.F();
        //     }
        //     str3 = qr4.e();
        //     serverError.setMessage(str3);
        //     return new po2(b, serverError, (Throwable) null, (String) null, 8, (fd4) null);
        // }
        // em4 c2 = qr4.c();
        // if (c2 != null) {
        //     str = c2.F();
        // }
        // str = qr4.e();
        // try {
        //     ServerError serverError2 = (ServerError) new Gson().a(str, ServerError.class);
        //     if (serverError2 != null) {
        //         Integer code = serverError2.getCode();
        //         if (code != null) {
        //             if (code.intValue() == 0) {
        //             }
        //         }
        //         po2 = new po2(qr4.b(), serverError2, (Throwable) null, (String) null, 8, (fd4) null);
        //         return po2;
        //     }
        //     po2 = new po2(qr4.b(), (ServerError) null, (Throwable) null, str);
        //     return po2;
        // } catch (Exception unused) {
        //     em4 c3 = qr4.c();
        //     if (c3 != null) {
        //         str2 = c3.F();
        //     }
        //     str2 = qr4.e();
        //     return new po2(qr4.b(), new ServerError(b, str2), (Throwable) null, (String) null, 8, (fd4) null);
        // }
    }

    @DexIgnore
    public static final <T> qo2<T> a(qr4<T> qr4) {
        kd4.b(qr4, "$this$createRepoResponse");
        try {
            return b(qr4);
        } catch (Throwable th) {
            return a(th);
        }
    }

    @DexIgnore
    public static final <T> po2<T> a(Throwable th) {
        throw null;
        // ILocalFLogger local = FLogger.INSTANCE.getLocal();
        // local.e("RepoResponse", "create=" + th.getMessage());
        // if (th instanceof SocketTimeoutException) {
        //     return new po2(MFNetworkReturnCode.CLIENT_TIMEOUT, (ServerError) null, th, (String) null, 8, (fd4) null);
        // }
        // if (th instanceof UnknownHostException) {
        //     return new po2(601, (ServerError) null, th, (String) null, 8, (fd4) null);
        // }
        // return new po2(600, (ServerError) null, th, (String) null, 8, (fd4) null);
    }
}
