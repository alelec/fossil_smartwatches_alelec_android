package com.portfolio.platform.p007ui.goaltracking.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries */
public final class FetchDailyGoalTrackingSummaries extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries.C6179b, com.portfolio.platform.CoroutineUseCase.C5605d, com.portfolio.platform.CoroutineUseCase.C5602a> {

    @DexIgnore
    /* renamed from: f */
    public static /* final */ java.lang.String f21918f;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.GoalTrackingRepository f21919d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.portfolio.platform.data.source.UserRepository f21920e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries$a")
    /* renamed from: com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries$a */
    public static final class C6178a {
        @DexIgnore
        public C6178a() {
        }

        @DexIgnore
        public /* synthetic */ C6178a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries$b")
    /* renamed from: com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries$b */
    public static final class C6179b implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.Date f21921a;

        @DexIgnore
        public C6179b(java.util.Date date) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(date, "date");
            this.f21921a = date;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.util.Date mo40380a() {
            return this.f21921a;
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries.C6178a((com.fossil.blesdk.obfuscated.fd4) null);
        java.lang.String simpleName = com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "FetchDailyGoalTrackingSu\u2026es::class.java.simpleName");
        f21918f = simpleName;
    }
    */

    @DexIgnore
    public FetchDailyGoalTrackingSummaries(com.portfolio.platform.data.source.GoalTrackingRepository goalTrackingRepository, com.portfolio.platform.data.source.UserRepository userRepository) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(goalTrackingRepository, "mGoalTrackingRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(userRepository, "mUserRepository");
        this.f21919d = goalTrackingRepository;
        this.f21920e = userRepository;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f21918f;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries.C6179b bVar, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4> yb4) {
        com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries$run$1 fetchDailyGoalTrackingSummaries$run$1;
        int i;
        java.util.Date date;
        java.util.Date date2;
        if (yb4 instanceof com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries$run$1) {
            fetchDailyGoalTrackingSummaries$run$1 = (com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries$run$1) yb4;
            int i2 = fetchDailyGoalTrackingSummaries$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fetchDailyGoalTrackingSummaries$run$1.label = i2 - Integer.MIN_VALUE;
                java.lang.Object obj = fetchDailyGoalTrackingSummaries$run$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = fetchDailyGoalTrackingSummaries$run$1.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    if (bVar == null) {
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    java.util.Date a2 = bVar.mo40380a();
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String str = f21918f;
                    local.mo33255d(str, "executeUseCase - date=" + com.fossil.blesdk.obfuscated.ft3.m22452a(a2));
                    com.portfolio.platform.data.model.MFUser currentUser = this.f21920e.getCurrentUser();
                    if (currentUser == null || android.text.TextUtils.isEmpty(currentUser.getCreatedAt())) {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String str2 = f21918f;
                        local2.mo33255d(str2, "executeUseCase - FAILED!!! with user=" + currentUser);
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    java.util.Date d = com.fossil.blesdk.obfuscated.rk2.m27394d(currentUser.getCreatedAt());
                    java.util.Calendar instance = java.util.Calendar.getInstance();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "calendar");
                    instance.setTime(a2);
                    instance.set(5, 1);
                    long timeInMillis = instance.getTimeInMillis();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) d, "createdDate");
                    if (com.fossil.blesdk.obfuscated.rk2.m27370a(timeInMillis, d.getTime())) {
                        date = d;
                    } else {
                        java.util.Calendar e = com.fossil.blesdk.obfuscated.rk2.m27398e(instance);
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.getStartOfMonth(calendar)");
                        date = e.getTime();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date, "DateHelper.getStartOfMonth(calendar).time");
                        if (com.fossil.blesdk.obfuscated.rk2.m27383b(d, date)) {
                            return com.fossil.blesdk.obfuscated.qa4.f17909a;
                        }
                    }
                    java.lang.Boolean r = com.fossil.blesdk.obfuscated.rk2.m27413r(date);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r, "DateHelper.isThisMonth(startDate)");
                    if (r.booleanValue()) {
                        date2 = new java.util.Date();
                    } else {
                        java.util.Calendar j = com.fossil.blesdk.obfuscated.rk2.m27405j(date);
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) j, "DateHelper.getEndOfMonth(startDate)");
                        date2 = j.getTime();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date2, "DateHelper.getEndOfMonth(startDate).time");
                    }
                    java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> pendingGoalTrackingDataList = this.f21919d.getPendingGoalTrackingDataList(date, date2);
                    if (pendingGoalTrackingDataList.isEmpty()) {
                        com.portfolio.platform.data.source.GoalTrackingRepository goalTrackingRepository = this.f21919d;
                        fetchDailyGoalTrackingSummaries$run$1.L$0 = this;
                        fetchDailyGoalTrackingSummaries$run$1.L$1 = bVar;
                        fetchDailyGoalTrackingSummaries$run$1.L$2 = a2;
                        fetchDailyGoalTrackingSummaries$run$1.L$3 = currentUser;
                        fetchDailyGoalTrackingSummaries$run$1.L$4 = d;
                        fetchDailyGoalTrackingSummaries$run$1.L$5 = instance;
                        fetchDailyGoalTrackingSummaries$run$1.L$6 = date2;
                        fetchDailyGoalTrackingSummaries$run$1.L$7 = date;
                        fetchDailyGoalTrackingSummaries$run$1.L$8 = pendingGoalTrackingDataList;
                        fetchDailyGoalTrackingSummaries$run$1.label = 1;
                        if (goalTrackingRepository.loadSummaries(date, date2, fetchDailyGoalTrackingSummaries$run$1) == a) {
                            return a;
                        }
                    }
                } else if (i == 1) {
                    java.util.List list = (java.util.List) fetchDailyGoalTrackingSummaries$run$1.L$8;
                    java.util.Date date3 = (java.util.Date) fetchDailyGoalTrackingSummaries$run$1.L$7;
                    java.util.Date date4 = (java.util.Date) fetchDailyGoalTrackingSummaries$run$1.L$6;
                    java.util.Calendar calendar = (java.util.Calendar) fetchDailyGoalTrackingSummaries$run$1.L$5;
                    java.util.Date date5 = (java.util.Date) fetchDailyGoalTrackingSummaries$run$1.L$4;
                    com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) fetchDailyGoalTrackingSummaries$run$1.L$3;
                    java.util.Date date6 = (java.util.Date) fetchDailyGoalTrackingSummaries$run$1.L$2;
                    com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries.C6179b bVar2 = (com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries.C6179b) fetchDailyGoalTrackingSummaries$run$1.L$1;
                    com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries fetchDailyGoalTrackingSummaries = (com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries) fetchDailyGoalTrackingSummaries$run$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }
        fetchDailyGoalTrackingSummaries$run$1 = new com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries$run$1(this, yb4);
        java.lang.Object obj2 = fetchDailyGoalTrackingSummaries$run$1.result;
        java.lang.Object a3 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = fetchDailyGoalTrackingSummaries$run$1.label;
        if (i != 0) {
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
