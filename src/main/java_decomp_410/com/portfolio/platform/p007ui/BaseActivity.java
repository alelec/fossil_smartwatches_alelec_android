package com.portfolio.platform.p007ui;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@android.annotation.SuppressLint({"Registered"})
/* renamed from: com.portfolio.platform.ui.BaseActivity */
public class BaseActivity extends androidx.appcompat.app.AppCompatActivity implements com.fossil.blesdk.obfuscated.ws3.C5363g, com.fossil.blesdk.obfuscated.pq4.C4854a {

    @DexIgnore
    /* renamed from: A */
    public static /* final */ com.portfolio.platform.p007ui.BaseActivity.C6101a f21700A; // = new com.portfolio.platform.p007ui.BaseActivity.C6101a((com.fossil.blesdk.obfuscated.fd4) null);

    @DexIgnore
    /* renamed from: z */
    public static com.misfit.frameworks.buttonservice.IButtonConnectivity f21701z;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.String f21702e;

    @DexIgnore
    /* renamed from: f */
    public boolean f21703f;

    @DexIgnore
    /* renamed from: g */
    public boolean f21704g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ android.os.Handler f21705h; // = new android.os.Handler();

    @DexIgnore
    /* renamed from: i */
    public android.view.View f21706i;

    @DexIgnore
    /* renamed from: j */
    public android.widget.TextView f21707j;

    @DexIgnore
    /* renamed from: k */
    public android.widget.TextView f21708k;

    @DexIgnore
    /* renamed from: l */
    public android.widget.TextView f21709l;

    @DexIgnore
    /* renamed from: m */
    public android.widget.TextView f21710m;

    @DexIgnore
    /* renamed from: n */
    public android.widget.TextView f21711n;

    @DexIgnore
    /* renamed from: o */
    public boolean f21712o;

    @DexIgnore
    /* renamed from: p */
    public com.portfolio.platform.data.source.UserRepository f21713p;

    @DexIgnore
    /* renamed from: q */
    public com.fossil.blesdk.obfuscated.en2 f21714q;

    @DexIgnore
    /* renamed from: r */
    public com.portfolio.platform.data.source.DeviceRepository f21715r;

    @DexIgnore
    /* renamed from: s */
    public com.portfolio.platform.MigrationManager f21716s;

    @DexIgnore
    /* renamed from: t */
    public com.fossil.blesdk.obfuscated.gr2 f21717t;

    @DexIgnore
    /* renamed from: u */
    public java.lang.String f21718u;

    @DexIgnore
    /* renamed from: v */
    public com.fossil.blesdk.obfuscated.bt3 f21719v;

    @DexIgnore
    /* renamed from: w */
    public /* final */ com.portfolio.platform.p007ui.BaseActivity$mButtonServiceConnection$1 f21720w; // = new com.portfolio.platform.p007ui.BaseActivity$mButtonServiceConnection$1(this);

    @DexIgnore
    /* renamed from: x */
    public /* final */ com.portfolio.platform.p007ui.BaseActivity.C6105d f21721x; // = new com.portfolio.platform.p007ui.BaseActivity.C6105d(this);

    @DexIgnore
    /* renamed from: y */
    public /* final */ java.lang.Runnable f21722y; // = new com.portfolio.platform.p007ui.BaseActivity.C6102b(this);

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.BaseActivity$a")
    /* renamed from: com.portfolio.platform.ui.BaseActivity$a */
    public static final class C6101a {
        @DexIgnore
        public C6101a() {
        }

        @DexIgnore
        /* renamed from: a */
        public final com.misfit.frameworks.buttonservice.IButtonConnectivity mo40217a() {
            return com.portfolio.platform.p007ui.BaseActivity.f21701z;
        }

        @DexIgnore
        public /* synthetic */ C6101a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo40218a(com.misfit.frameworks.buttonservice.IButtonConnectivity iButtonConnectivity) {
            com.portfolio.platform.p007ui.BaseActivity.f21701z = iButtonConnectivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.BaseActivity$b")
    /* renamed from: com.portfolio.platform.ui.BaseActivity$b */
    public static final class C6102b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.BaseActivity f21723e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.BaseActivity$b$a")
        /* renamed from: com.portfolio.platform.ui.BaseActivity$b$a */
        public static final class C6103a implements com.portfolio.platform.CoroutineUseCase.C5606e<com.fossil.blesdk.obfuscated.gr2.C4349e, com.fossil.blesdk.obfuscated.gr2.C4346b> {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.portfolio.platform.p007ui.BaseActivity.C6102b f21724a;

            @DexIgnore
            public C6103a(com.portfolio.platform.p007ui.BaseActivity.C6102b bVar) {
                this.f21724a = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(com.fossil.blesdk.obfuscated.gr2.C4349e eVar) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(eVar, "responseValue");
                this.f21724a.f21723e.mo40195b(java.lang.String.valueOf(eVar.mo27790a()));
            }

            @DexIgnore
            /* renamed from: a */
            public void mo29641a(com.fossil.blesdk.obfuscated.gr2.C4346b bVar) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(bVar, "errorValue");
                this.f21724a.f21723e.mo40195b("Disconnected");
            }
        }

        @DexIgnore
        public C6102b(com.portfolio.platform.p007ui.BaseActivity baseActivity) {
            this.f21723e = baseActivity;
        }

        @DexIgnore
        public final void run() {
            if (!android.text.TextUtils.isEmpty(this.f21723e.f21718u)) {
                com.fossil.blesdk.obfuscated.gr2 c = this.f21723e.mo40198c();
                java.lang.String a = this.f21723e.f21718u;
                if (a != null) {
                    c.mo34435a(new com.fossil.blesdk.obfuscated.gr2.C4348d(a), new com.portfolio.platform.p007ui.BaseActivity.C6102b.C6103a(this));
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.BaseActivity$c")
    /* renamed from: com.portfolio.platform.ui.BaseActivity$c */
    public static final class C6104c implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.BaseActivity f21725e;

        @DexIgnore
        public C6104c(com.portfolio.platform.p007ui.BaseActivity baseActivity) {
            this.f21725e = baseActivity;
        }

        @DexIgnore
        public final void run() {
            try {
                if (this.f21725e.f21719v == null) {
                    androidx.fragment.app.Fragment a = this.f21725e.getSupportFragmentManager().mo2077a("ProgressDialogFragment");
                    if (a != null) {
                        this.f21725e.f21719v = (com.fossil.blesdk.obfuscated.bt3) a;
                    }
                }
                if (this.f21725e.f21719v != null) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("ProgressDialogFragment", "hideLoadingDialog dismissAllowingStateLoss");
                    com.fossil.blesdk.obfuscated.bt3 b = this.f21725e.f21719v;
                    if (b != null) {
                        b.dismissAllowingStateLoss();
                        this.f21725e.f21719v = null;
                        return;
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            } catch (java.lang.Exception e) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String f = this.f21725e.mo40202f();
                local.mo33255d(f, "Exception when dismiss progress dialog=" + e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.BaseActivity$d")
    /* renamed from: com.portfolio.platform.ui.BaseActivity$d */
    public static final class C6105d implements android.content.ServiceConnection {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.BaseActivity f21726a;

        @DexIgnore
        public C6105d(com.portfolio.platform.p007ui.BaseActivity baseActivity) {
            this.f21726a = baseActivity;
        }

        @DexIgnore
        public void onServiceConnected(android.content.ComponentName componentName, android.os.IBinder iBinder) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(componentName, "name");
            com.fossil.blesdk.obfuscated.kd4.m24411b(iBinder, com.misfit.frameworks.common.constants.Constants.SERVICE);
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f21726a.mo40202f(), "Misfit service connected");
            com.portfolio.platform.service.MFDeviceService.C6006b bVar = (com.portfolio.platform.service.MFDeviceService.C6006b) iBinder;
            this.f21726a.mo40188a(bVar.mo39863a());
            com.portfolio.platform.PortfolioApp.f20941W.mo34587b(bVar);
            this.f21726a.mo40199c(true);
        }

        @DexIgnore
        public void onServiceDisconnected(android.content.ComponentName componentName) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(componentName, "name");
            this.f21726a.mo40199c(false);
            this.f21726a.mo40188a((com.portfolio.platform.service.MFDeviceService) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.BaseActivity$e")
    /* renamed from: com.portfolio.platform.ui.BaseActivity$e */
    public static final class C6106e implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.BaseActivity f21727e;

        @DexIgnore
        public C6106e(com.portfolio.platform.p007ui.BaseActivity baseActivity) {
            this.f21727e = baseActivity;
        }

        @DexIgnore
        public final void run() {
            androidx.fragment.app.FragmentManager supportFragmentManager = this.f21727e.getSupportFragmentManager();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) supportFragmentManager, "supportFragmentManager");
            if (supportFragmentManager.mo2085c() > 1) {
                this.f21727e.getSupportFragmentManager().mo2088f();
            } else {
                this.f21727e.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.BaseActivity$f")
    /* renamed from: com.portfolio.platform.ui.BaseActivity$f */
    public static final class C6107f implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.BaseActivity f21728e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.lang.String f21729f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ boolean f21730g;

        @DexIgnore
        public C6107f(com.portfolio.platform.p007ui.BaseActivity baseActivity, java.lang.String str, boolean z) {
            this.f21728e = baseActivity;
            this.f21729f = str;
            this.f21730g = z;
        }

        @DexIgnore
        public final void run() {
            try {
                if (!this.f21728e.isDestroyed()) {
                    if (!this.f21728e.isFinishing()) {
                        this.f21728e.f21719v = com.fossil.blesdk.obfuscated.bt3.f13710g.mo26053a(this.f21729f);
                        com.fossil.blesdk.obfuscated.bt3 b = this.f21728e.f21719v;
                        if (b != null) {
                            b.setCancelable(this.f21730g);
                            com.fossil.blesdk.obfuscated.C1472bb a = this.f21728e.getSupportFragmentManager().mo2078a();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "supportFragmentManager.beginTransaction()");
                            com.fossil.blesdk.obfuscated.bt3 b2 = this.f21728e.f21719v;
                            if (b2 != null) {
                                a.mo9127a((androidx.fragment.app.Fragment) b2, "ProgressDialogFragment");
                                a.mo9129b();
                                return;
                            }
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                }
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f21728e.mo40202f(), "Activity is destroy or finishing, no need to show dialog");
            } catch (java.lang.Exception e) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String f = this.f21728e.mo40202f();
                local.mo33255d(f, "Exception when showing progress dialog=" + e);
            }
        }
    }

    @DexIgnore
    public BaseActivity() {
        java.lang.String simpleName = getClass().getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "this.javaClass.simpleName");
        this.f21702e = simpleName;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo27620a(int i, java.util.List<java.lang.String> list) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "perms");
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40188a(com.portfolio.platform.service.MFDeviceService mFDeviceService) {
    }

    @DexIgnore
    public void attachBaseContext(android.content.Context context) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(context, "newBase");
        super.attachBaseContext(p014uk.p015co.chrisjenx.calligraphy.CalligraphyContextWrapper.wrap(context));
    }

    @DexIgnore
    /* renamed from: b */
    public void mo27625b(int i, java.util.List<java.lang.String> list) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "perms");
    }

    @DexIgnore
    public void finish() {
        super.finish();
        if (mo40206i()) {
            overridePendingTransition(com.fossil.wearables.fossil.R.anim.slide_in_left, com.fossil.wearables.fossil.R.anim.slide_out_right);
        }
    }

    @DexIgnore
    /* renamed from: g */
    public final int mo40204g() {
        try {
            java.lang.reflect.Method method = android.content.Context.class.getMethod("getThemeResId", new java.lang.Class[0]);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) method, "currentClass.getMethod(\"getThemeResId\")");
            method.setAccessible(true);
            java.lang.Object invoke = method.invoke(this, new java.lang.Object[0]);
            if (invoke != null) {
                return ((java.lang.Integer) invoke).intValue();
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Int");
        } catch (java.lang.Exception unused) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f21702e, "Failed to get theme resource ID");
            return 0;
        }
    }

    @DexIgnore
    /* renamed from: h */
    public final void mo40205h() {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("ProgressDialogFragment", "hideLoadingDialog");
        this.f21705h.post(new com.portfolio.platform.p007ui.BaseActivity.C6104c(this));
    }

    @DexIgnore
    /* renamed from: i */
    public final boolean mo40206i() {
        return mo40204g() == 2131886094;
    }

    @DexIgnore
    /* renamed from: j */
    public final void mo40207j() {
        try {
            com.fossil.blesdk.obfuscated.C1465b6.m4795c(this);
        } catch (java.lang.IllegalArgumentException unused) {
            finish();
        }
    }

    @DexIgnore
    /* renamed from: k */
    public final void mo40208k() {
        startActivity(new android.content.Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
    }

    @DexIgnore
    /* renamed from: l */
    public final void mo40209l() {
        android.content.Intent intent = new android.content.Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(android.net.Uri.fromParts(com.facebook.applinks.FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, getPackageName(), (java.lang.String) null));
        startActivity(intent);
    }

    @DexIgnore
    /* renamed from: m */
    public final void mo40210m() {
        startActivity(new android.content.Intent("android.settings.SETTINGS"));
    }

    @DexIgnore
    /* renamed from: n */
    public final synchronized void mo40211n() {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str = this.f21702e;
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("needToUpdateBLEWhenUpgradeLegacy - isNeedToUpdateBLE=");
        com.fossil.blesdk.obfuscated.en2 en2 = this.f21714q;
        if (en2 != null) {
            sb.append(en2.mo26974L());
            local.mo33255d(str, sb.toString());
            com.fossil.blesdk.obfuscated.en2 en22 = this.f21714q;
            if (en22 == null) {
                com.fossil.blesdk.obfuscated.kd4.m24414d("mSharePrefs");
                throw null;
            } else if (en22.mo26974L()) {
                try {
                    com.portfolio.platform.MigrationManager migrationManager = this.f21716s;
                    if (migrationManager != null) {
                        migrationManager.mo34449c();
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mMigrationManager");
                        throw null;
                    }
                } catch (java.lang.Exception e) {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String str2 = this.f21702e;
                    local2.mo33256e(str2, "needToUpdateBLEWhenUpgradeLegacy - e=" + e);
                }
            }
        } else {
            com.fossil.blesdk.obfuscated.kd4.m24414d("mSharePrefs");
            throw null;
        }
        return;
    }

    @DexIgnore
    /* renamed from: o */
    public final void mo40212o() {
        android.view.View view = this.f21706i;
        if (view != null && this.f21712o) {
            if (view != null) {
                android.view.ViewParent parent = view.getParent();
                if (parent != null) {
                    ((android.view.ViewGroup) parent).removeView(this.f21706i);
                    this.f21712o = false;
                    this.f21705h.removeCallbacks(this.f21722y);
                } else {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
        try {
            com.fossil.blesdk.obfuscated.gr2 gr2 = this.f21717t;
            if (gr2 != null) {
                gr2.mo27787g();
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24414d("mGetRssi");
                throw null;
            }
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    public void onBackPressed() {
        runOnUiThread(new com.portfolio.platform.p007ui.BaseActivity.C6106e(this));
    }

    @DexIgnore
    public void onCreate(android.os.Bundle bundle) {
        com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34538g().mo29083a(this);
        super.onCreate(bundle);
        if (mo40206i()) {
            overridePendingTransition(com.fossil.wearables.fossil.R.anim.slide_in_right, com.fossil.wearables.fossil.R.anim.slide_out_left);
        }
        com.fossil.blesdk.obfuscated.fn2.f14849o.mo27449a().mo27445b();
        android.view.Window window = getWindow();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) window, "window");
        android.view.View decorView = window.getDecorView();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) decorView, "window.decorView");
        decorView.setSystemUiVisibility(3328);
        com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39517c();
    }

    @DexIgnore
    public boolean onOptionsItemSelected(android.view.MenuItem menuItem) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(menuItem, "item");
        if (menuItem.getItemId() == 16908332) {
            mo40207j();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        try {
            com.portfolio.platform.PortfolioApp.f20941W.mo34590c(this);
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str = this.f21702e;
            local.mo33256e(str, "Inside " + this.f21702e + ".onPause - exception=" + e);
        }
        if (!com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34457D()) {
            com.fossil.blesdk.obfuscated.en2 en2 = this.f21714q;
            if (en2 == null) {
                com.fossil.blesdk.obfuscated.kd4.m24414d("mSharePrefs");
                throw null;
            } else if (en2.mo26972J()) {
                mo40212o();
            }
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        com.portfolio.platform.PortfolioApp.f20941W.mo34588b((java.lang.Object) this);
        mo40190a(false);
        if (com.portfolio.platform.PortfolioApp.f20941W.mo34593f()) {
            com.fossil.blesdk.obfuscated.fn2.f14849o.mo27449a().mo27445b();
        }
        if (!com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34457D()) {
            com.fossil.blesdk.obfuscated.en2 en2 = this.f21714q;
            if (en2 == null) {
                com.fossil.blesdk.obfuscated.kd4.m24414d("mSharePrefs");
                throw null;
            } else if (en2.mo26972J()) {
                mo40185a();
            }
        }
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f21702e, "onStart()");
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f21702e, "onStop()");
    }

    @DexIgnore
    public void onTrimMemory(int i) {
        super.onTrimMemory(i);
        java.lang.System.runFinalization();
    }

    @DexIgnore
    /* renamed from: p */
    public final void mo40215p() {
        m32685a(this, false, (java.lang.String) null, 2, (java.lang.Object) null);
    }

    @DexIgnore
    /* renamed from: q */
    public final void mo40216q() {
        this.f21718u = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e();
        if (!android.text.TextUtils.isEmpty(this.f21718u)) {
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.BaseActivity$updateDeviceInfo$1(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            mo40195b("Disconnected");
        }
    }

    @DexIgnore
    public void setContentView(int i) {
        super.setContentView(i);
    }

    @DexIgnore
    public void startActivityForResult(android.content.Intent intent, int i) {
        if (intent == null) {
            intent = new android.content.Intent();
        }
        super.startActivityForResult(intent, i);
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo40196b(boolean z) {
        this.f21704g = z;
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo40199c(boolean z) {
        this.f21703f = z;
    }

    @DexIgnore
    /* renamed from: d */
    public final com.portfolio.platform.data.source.UserRepository mo40200d() {
        com.portfolio.platform.data.source.UserRepository userRepository = this.f21713p;
        if (userRepository != null) {
            return userRepository;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mUserRepository");
        throw null;
    }

    @DexIgnore
    /* renamed from: e */
    public final int mo40201e() {
        android.content.res.Resources resources = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().getResources();
        int identifier = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (identifier > 0) {
            return resources.getDimensionPixelSize(identifier);
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: f */
    public final java.lang.String mo40202f() {
        return this.f21702e;
    }

    @DexIgnore
    /* renamed from: b */
    public final com.portfolio.platform.data.source.DeviceRepository mo40194b() {
        com.portfolio.platform.data.source.DeviceRepository deviceRepository = this.f21715r;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    /* renamed from: c */
    public final com.fossil.blesdk.obfuscated.gr2 mo40198c() {
        com.fossil.blesdk.obfuscated.gr2 gr2 = this.f21717t;
        if (gr2 != null) {
            return gr2;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mGetRssi");
        throw null;
    }

    @DexIgnore
    /* renamed from: b */
    public final <T extends android.app.Service> void mo40197b(java.lang.Class<? extends T>... clsArr) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(clsArr, "services");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f21702e, "unbindServices()");
        for (java.lang.Class<? extends T> cls : clsArr) {
            if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) cls, (java.lang.Object) com.portfolio.platform.service.MFDeviceService.class)) {
                if (this.f21703f) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f21702e, "Unbinding from mIsMisfitServiceBound");
                    com.fossil.blesdk.obfuscated.ps3.f17731a.mo30228a((android.content.Context) this, (android.content.ServiceConnection) this.f21721x);
                    this.f21703f = false;
                }
            } else if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) cls, (java.lang.Object) com.misfit.frameworks.buttonservice.ButtonService.class) && this.f21704g) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f21702e, "Unbinding from mButtonServiceBound");
                com.fossil.blesdk.obfuscated.ps3.f17731a.mo30228a((android.content.Context) this, (android.content.ServiceConnection) this.f21720w);
                this.f21704g = false;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public <T extends android.app.Service> void mo40192a(java.lang.Class<? extends T>... clsArr) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(clsArr, "services");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f21702e, "executeServices()");
        for (java.lang.Class<? extends T> cls : clsArr) {
            if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) cls, (java.lang.Object) com.portfolio.platform.service.MFDeviceService.class)) {
                com.fossil.blesdk.obfuscated.ps3.f17731a.mo30231b(this, com.portfolio.platform.service.MFDeviceService.class, this.f21721x, 0);
            } else if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) cls, (java.lang.Object) com.misfit.frameworks.buttonservice.ButtonService.class)) {
                com.fossil.blesdk.obfuscated.ps3.f17731a.mo30231b(this, com.misfit.frameworks.buttonservice.ButtonService.class, this.f21720w, 1);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40185a() {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str = this.f21702e;
        local.mo33255d(str, "displayDeviceInfoView() called: isDeviceInfoViewAdded = " + this.f21712o);
        if (!this.f21712o) {
            java.lang.Object systemService = getSystemService("layout_inflater");
            if (systemService != null) {
                this.f21706i = ((android.view.LayoutInflater) systemService).inflate(com.fossil.wearables.fossil.R.layout.view_device_info, (android.view.ViewGroup) null);
                android.view.View view = this.f21706i;
                if (view != null) {
                    view.setY((float) mo40201e());
                    android.view.View view2 = this.f21706i;
                    if (view2 != null) {
                        this.f21707j = (android.widget.TextView) view2.findViewById(com.fossil.wearables.fossil.R.id.tv_device_serial);
                        android.view.View view3 = this.f21706i;
                        if (view3 != null) {
                            this.f21708k = (android.widget.TextView) view3.findViewById(com.fossil.wearables.fossil.R.id.tv_device_battery);
                            android.view.View view4 = this.f21706i;
                            if (view4 != null) {
                                this.f21709l = (android.widget.TextView) view4.findViewById(com.fossil.wearables.fossil.R.id.tv_device_rssi);
                                android.view.View view5 = this.f21706i;
                                if (view5 != null) {
                                    this.f21710m = (android.widget.TextView) view5.findViewById(com.fossil.wearables.fossil.R.id.tv_device_firmware);
                                    android.view.View view6 = this.f21706i;
                                    if (view6 != null) {
                                        android.widget.TextView textView = (android.widget.TextView) view6.findViewById(com.fossil.wearables.fossil.R.id.tv_device_top_button);
                                        android.view.View view7 = this.f21706i;
                                        if (view7 != null) {
                                            android.widget.TextView textView2 = (android.widget.TextView) view7.findViewById(com.fossil.wearables.fossil.R.id.tv_device_middle_button);
                                            android.view.View view8 = this.f21706i;
                                            if (view8 != null) {
                                                android.widget.TextView textView3 = (android.widget.TextView) view8.findViewById(com.fossil.wearables.fossil.R.id.tv_device_bottom_button);
                                                android.view.View view9 = this.f21706i;
                                                if (view9 != null) {
                                                    this.f21711n = (android.widget.TextView) view9.findViewById(com.fossil.wearables.fossil.R.id.tv_access_token_expired_at);
                                                    getWindow().addContentView(this.f21706i, new androidx.constraintlayout.widget.ConstraintLayout.LayoutParams((int) com.fossil.blesdk.obfuscated.ts3.m28434a(170, (android.content.Context) this), (int) com.fossil.blesdk.obfuscated.ts3.m28434a(130, (android.content.Context) this)));
                                                    this.f21712o = true;
                                                    mo40216q();
                                                } else {
                                                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                                                    throw null;
                                                }
                                            } else {
                                                com.fossil.blesdk.obfuscated.kd4.m24405a();
                                                throw null;
                                            }
                                        } else {
                                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                                            throw null;
                                        }
                                    } else {
                                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                                        throw null;
                                    }
                                } else {
                                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                                    throw null;
                                }
                            } else {
                                com.fossil.blesdk.obfuscated.kd4.m24405a();
                                throw null;
                            }
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            } else {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.LayoutInflater");
            }
        }
        com.fossil.blesdk.obfuscated.gr2 gr2 = this.f21717t;
        if (gr2 != null) {
            gr2.mo27786f();
        } else {
            com.fossil.blesdk.obfuscated.kd4.m24414d("mGetRssi");
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo40195b(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "rssiInfo");
        android.widget.TextView textView = this.f21709l;
        if (textView != null) {
            textView.setText(str);
            this.f21705h.postDelayed(this.f21722y, 1000);
            return;
        }
        com.fossil.blesdk.obfuscated.kd4.m24405a();
        throw null;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40189a(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "title");
        mo40191a(false, str);
    }

    @DexIgnore
    /* renamed from: a */
    public static /* synthetic */ void m32685a(com.portfolio.platform.p007ui.BaseActivity baseActivity, boolean z, java.lang.String str, int i, java.lang.Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                str = "";
            }
            baseActivity.mo40191a(z, str);
            return;
        }
        throw new java.lang.UnsupportedOperationException("Super calls with default arguments not supported in this target, function: showLoadingDialog");
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40191a(boolean z, java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "title");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d("ProgressDialogFragment", "showLoadingDialog: cancelable = " + z);
        mo40205h();
        this.f21705h.post(new com.portfolio.platform.p007ui.BaseActivity.C6107f(this, str, z));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo25776a(java.lang.String str, int i, android.content.Intent intent) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "tag");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str2 = this.f21702e;
        local.mo33255d(str2, "Inside .onDialogFragmentResult tag=" + str);
        if (str.hashCode() == 2009556792 && str.equals(com.portfolio.platform.data.InAppPermission.NOTIFICATION_ACCESS) && i == com.fossil.wearables.fossil.R.id.ftv_go_to_setting) {
            mo40208k();
        }
    }

    @DexIgnore
    @android.annotation.TargetApi(23)
    /* renamed from: a */
    public final void mo40190a(boolean z) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            android.view.Window window = getWindow();
            window.clearFlags(67108864);
            window.addFlags(Integer.MIN_VALUE);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) window, "window");
            android.view.View decorView = window.getDecorView();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) decorView, "window.decorView");
            int systemUiVisibility = decorView.getSystemUiVisibility();
            if (z) {
                decorView.setSystemUiVisibility(systemUiVisibility & -8193);
            } else {
                decorView.setSystemUiVisibility(systemUiVisibility | 8192);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40186a(androidx.fragment.app.Fragment fragment, int i) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(fragment, "fragment");
        mo40187a(fragment, (java.lang.String) null, i);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40187a(androidx.fragment.app.Fragment fragment, java.lang.String str, int i) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(fragment, "fragment");
        com.fossil.blesdk.obfuscated.C1472bb a = getSupportFragmentManager().mo2078a();
        a.mo9130b(i, fragment, str);
        a.mo9128a(str);
        a.mo9129b();
    }
}
