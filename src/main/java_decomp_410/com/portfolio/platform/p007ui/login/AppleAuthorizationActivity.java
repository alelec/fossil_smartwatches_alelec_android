package com.portfolio.platform.p007ui.login;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.login.AppleAuthorizationActivity */
public final class AppleAuthorizationActivity extends com.portfolio.platform.BaseWebViewActivity {

    @DexIgnore
    /* renamed from: F */
    public static /* final */ com.portfolio.platform.p007ui.login.AppleAuthorizationActivity.C6186a f21936F; // = new com.portfolio.platform.p007ui.login.AppleAuthorizationActivity.C6186a((com.fossil.blesdk.obfuscated.fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.login.AppleAuthorizationActivity$a")
    /* renamed from: com.portfolio.platform.ui.login.AppleAuthorizationActivity$a */
    public static final class C6186a {
        @DexIgnore
        public C6186a() {
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo40387a(androidx.fragment.app.FragmentActivity fragmentActivity, java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(fragmentActivity, com.misfit.frameworks.common.constants.Constants.ACTIVITY);
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "authorizationUrl");
            android.content.Intent intent = new android.content.Intent(fragmentActivity, com.portfolio.platform.p007ui.login.AppleAuthorizationActivity.class);
            intent.putExtra("urlToLoad", str);
            fragmentActivity.startActivityForResult(intent, 3535);
        }

        @DexIgnore
        public /* synthetic */ C6186a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.login.AppleAuthorizationActivity$b")
    /* renamed from: com.portfolio.platform.ui.login.AppleAuthorizationActivity$b */
    public static final class C6187b extends android.webkit.WebViewClient {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.login.AppleAuthorizationActivity f21937a;

        @DexIgnore
        public C6187b(com.portfolio.platform.p007ui.login.AppleAuthorizationActivity appleAuthorizationActivity) {
            this.f21937a = appleAuthorizationActivity;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0040 A[Catch:{ Exception -> 0x00c7 }] */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00b6 A[Catch:{ Exception -> 0x00c7 }] */
        public boolean shouldOverrideUrlLoading(android.webkit.WebView webView, android.webkit.WebResourceRequest webResourceRequest) {
            boolean z;
            boolean z2 = false;
            if (webResourceRequest != null) {
                try {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String a = this.f21937a.mo40202f();
                    local.mo33255d(a, "request = " + webResourceRequest.getUrl());
                    java.lang.String queryParameter = webResourceRequest.getUrl().getQueryParameter("id_token");
                    if (queryParameter != null) {
                        if (!com.fossil.blesdk.obfuscated.qf4.m26950a(queryParameter)) {
                            z = false;
                            if (z) {
                                if (this.f21937a.getIntent() == null) {
                                    this.f21937a.setIntent(new android.content.Intent());
                                }
                                com.portfolio.platform.data.SignUpSocialAuth signUpSocialAuth = new com.portfolio.platform.data.SignUpSocialAuth();
                                signUpSocialAuth.setService("apple");
                                signUpSocialAuth.setToken(queryParameter);
                                java.lang.String queryParameter2 = webResourceRequest.getUrl().getQueryParameter("user");
                                if (queryParameter2 == null || com.fossil.blesdk.obfuscated.qf4.m26950a(queryParameter2)) {
                                    z2 = true;
                                }
                                if (!z2) {
                                    com.portfolio.platform.data.AppleAuth appleAuth = (com.portfolio.platform.data.AppleAuth) new com.google.gson.Gson().mo23093a(queryParameter2, com.portfolio.platform.data.AppleAuth.class);
                                    signUpSocialAuth.setEmail(appleAuth.getEmail());
                                    signUpSocialAuth.setLastName(appleAuth.getName().getLastName());
                                    signUpSocialAuth.setFirstName(appleAuth.getName().getFirstName());
                                }
                                this.f21937a.getIntent().putExtra("USER_INFO_EXTRA", signUpSocialAuth);
                                this.f21937a.setResult(-1, this.f21937a.getIntent());
                                z2 = true;
                            } else {
                                z2 = com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) webResourceRequest.getUrl().getQueryParameter("error"), (java.lang.Object) "user_cancelled_authorize");
                            }
                        }
                    }
                    z = true;
                    if (z) {
                    }
                } catch (java.lang.Exception e) {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String a2 = this.f21937a.mo40202f();
                    local2.mo33255d(a2, "Get authorization info with error: " + e.getMessage());
                }
            }
            if (!z2) {
                return super.shouldOverrideUrlLoading(webView, webResourceRequest);
            }
            this.f21937a.finish();
            return true;
        }
    }

    @DexIgnore
    /* renamed from: s */
    public android.webkit.WebViewClient mo34425s() {
        return new com.portfolio.platform.p007ui.login.AppleAuthorizationActivity.C6187b(this);
    }
}
