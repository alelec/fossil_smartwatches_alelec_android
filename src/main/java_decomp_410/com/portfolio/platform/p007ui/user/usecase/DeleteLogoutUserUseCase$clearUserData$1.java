package com.portfolio.platform.p007ui.user.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$1", mo27670f = "DeleteLogoutUserUseCase.kt", mo27671l = {148}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$1 */
public final class DeleteLogoutUserUseCase$clearUserData$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22001p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeleteLogoutUserUseCase$clearUserData$1(com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase deleteLogoutUserUseCase, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = deleteLogoutUserUseCase;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$1 deleteLogoutUserUseCase$clearUserData$1 = new com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$1(this.this$0, yb4);
        deleteLogoutUserUseCase$clearUserData$1.f22001p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return deleteLogoutUserUseCase$clearUserData$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22001p$;
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().flush();
            try {
                com.fossil.blesdk.obfuscated.is3.f15828b.mo28396f(com.misfit.frameworks.buttonservice.ButtonService.DEVICE_SECRET_KEY);
            } catch (java.lang.Exception e) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a2 = com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.f21967I.mo40405a();
                local.mo33256e(a2, "exception when remove alias in keystore " + e);
            }
            this.this$0.f21982k.clearAllNotificationSetting();
            this.this$0.f21984m.cleanUp();
            this.this$0.f21988q.cleanUp();
            this.this$0.f21968A.cleanUp();
            this.this$0.f21977f.mo26984V();
            this.this$0.f21975d.clearAllUser();
            com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34472S();
            com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase deleteLogoutUserUseCase = this.this$0;
            this.L$0 = zg4;
            this.label = 1;
            if (deleteLogoutUserUseCase.mo40402a((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.f21995x.cleanUp();
        this.this$0.f21996y.cleanUp();
        this.this$0.f21997z.cleanUp();
        this.this$0.f21978g.cleanUp();
        this.this$0.f21994w.cleanUp();
        this.this$0.f21979h.cleanUp();
        this.this$0.f21980i.cleanUp();
        this.this$0.f21985n.cleanUp();
        this.this$0.f21989r.cleanUp();
        this.this$0.f21990s.cleanUp();
        this.this$0.f21991t.cleanUp();
        this.this$0.f21981j.clearData();
        this.this$0.f21976e.cleanUp();
        com.fossil.blesdk.obfuscated.dn2.f14174p.mo26576a().mo26575o();
        com.fossil.blesdk.obfuscated.ap2.f13436g.mo25747a();
        com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34525c();
        com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34459F();
        this.this$0.f21992u.getNotificationSettingsDao().delete();
        this.this$0.f21993v.getDNDScheduledTimeDao().delete();
        this.this$0.f21972E.getInactivityNudgeTimeDao().delete();
        this.this$0.f21972E.getRemindTimeDao().delete();
        this.this$0.f21969B.cleanUp();
        this.this$0.f21970C.cleanUp();
        this.this$0.f21971D.cleanUp();
        com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase w = this.this$0.f21973F;
        w.getGFitSampleDao().clearAll();
        w.getGFitActiveTimeDao().clearAll();
        w.getGFitWorkoutSessionDao().clearAll();
        w.getUASampleDao().clearAll();
        this.this$0.mo34436a(new com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6203d());
        this.this$0.f21983l.cleanUp();
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
