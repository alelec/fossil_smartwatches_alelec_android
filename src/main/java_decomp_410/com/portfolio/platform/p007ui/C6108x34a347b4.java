package com.portfolio.platform.p007ui;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.BaseActivity$updateDeviceInfo$1$invokeSuspend$$inlined$let$lambda$1 */
public final class C6108x34a347b4 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.Device $activeDevice$inlined;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21731p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.BaseActivity$updateDeviceInfo$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6108x34a347b4(com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.p007ui.BaseActivity$updateDeviceInfo$1 baseActivity$updateDeviceInfo$1, com.portfolio.platform.data.model.Device device) {
        super(2, yb4);
        this.this$0 = baseActivity$updateDeviceInfo$1;
        this.$activeDevice$inlined = device;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.p007ui.C6108x34a347b4 baseActivity$updateDeviceInfo$1$invokeSuspend$$inlined$let$lambda$1 = new com.portfolio.platform.p007ui.C6108x34a347b4(yb4, this.this$0, this.$activeDevice$inlined);
        baseActivity$updateDeviceInfo$1$invokeSuspend$$inlined$let$lambda$1.f21731p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return baseActivity$updateDeviceInfo$1$invokeSuspend$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.p007ui.C6108x34a347b4) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            android.widget.TextView f = this.this$0.this$0.f21707j;
            if (f != null) {
                f.setText(this.$activeDevice$inlined.getDeviceId());
                android.widget.TextView d = this.this$0.this$0.f21708k;
                if (d != null) {
                    d.setText(java.lang.String.valueOf(this.$activeDevice$inlined.getBatteryLevel()));
                    android.widget.TextView e = this.this$0.this$0.f21710m;
                    if (e != null) {
                        e.setText(this.$activeDevice$inlined.getFirmwareRevision());
                        this.this$0.this$0.f21718u = this.$activeDevice$inlined.getDeviceId();
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
