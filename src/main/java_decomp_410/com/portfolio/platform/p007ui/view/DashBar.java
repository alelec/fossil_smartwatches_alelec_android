package com.portfolio.platform.p007ui.view;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.view.DashBar */
public final class DashBar extends android.view.View {

    @DexIgnore
    /* renamed from: e */
    public android.graphics.Paint f22034e; // = new android.graphics.Paint(1);

    @DexIgnore
    /* renamed from: f */
    public android.graphics.Paint f22035f; // = new android.graphics.Paint(1);

    @DexIgnore
    /* renamed from: g */
    public float f22036g; // = 10.0f;

    @DexIgnore
    /* renamed from: h */
    public float f22037h; // = 6.0f;

    @DexIgnore
    /* renamed from: i */
    public int f22038i; // = 4;

    @DexIgnore
    /* renamed from: j */
    public int f22039j;

    @DexIgnore
    /* renamed from: k */
    public int f22040k;

    @DexIgnore
    /* renamed from: l */
    public int f22041l;

    @DexIgnore
    /* renamed from: m */
    public int f22042m; // = 50;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.DashBar$a")
    /* renamed from: com.portfolio.platform.ui.view.DashBar$a */
    public static final class C6227a {
        @DexIgnore
        public C6227a() {
        }

        @DexIgnore
        public /* synthetic */ C6227a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.view.DashBar.C6227a((com.fossil.blesdk.obfuscated.fd4) null);
    }
    */

    @DexIgnore
    public DashBar(android.content.Context context) {
        super(context, (android.util.AttributeSet) null);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40435a(float f) {
        this.f22034e.setDither(true);
        this.f22034e.setStyle(android.graphics.Paint.Style.STROKE);
        this.f22034e.setStrokeWidth(f);
        this.f22034e.setAntiAlias(true);
        this.f22034e.setStrokeCap(android.graphics.Paint.Cap.ROUND);
        this.f22034e.setStrokeJoin(android.graphics.Paint.Join.ROUND);
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo40436b(float f) {
        android.graphics.Paint paint = this.f22034e;
        android.graphics.LinearGradient linearGradient = new android.graphics.LinearGradient(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.f22039j, this.f22040k, android.graphics.Shader.TileMode.CLAMP);
        paint.setShader(linearGradient);
        android.graphics.Paint paint2 = this.f22034e;
        int i = this.f22038i;
        float f2 = this.f22037h;
        paint2.setPathEffect(new android.graphics.DashPathEffect(new float[]{((f - 20.0f) - (((float) (i - 1)) * f2)) / ((float) i), f2}, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.f22035f = new android.graphics.Paint(this.f22034e);
        android.graphics.Paint paint3 = this.f22035f;
        int i2 = this.f22041l;
        android.graphics.LinearGradient linearGradient2 = new android.graphics.LinearGradient(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, i2, i2, android.graphics.Shader.TileMode.CLAMP);
        paint3.setShader(linearGradient2);
    }

    @DexIgnore
    public void onDraw(android.graphics.Canvas canvas) {
        mo40436b((float) getWidth());
        if (canvas != null) {
            float height = ((float) canvas.getHeight()) / 2.0f;
            float width = (float) canvas.getWidth();
            float f = height;
            float f2 = height;
            canvas.drawLine(10.0f, f, width - 10.0f, f2, this.f22035f);
            canvas.drawLine(10.0f, f, ((width * ((float) this.f22042m)) / ((float) 100)) - 10.0f, f2, this.f22034e);
        }
    }

    @DexIgnore
    public void onMeasure(int i, int i2) {
        int mode = android.view.View.MeasureSpec.getMode(i);
        int size = android.view.View.MeasureSpec.getSize(i);
        int mode2 = android.view.View.MeasureSpec.getMode(i2);
        int size2 = android.view.View.MeasureSpec.getSize(i2);
        if (!(mode == Integer.MIN_VALUE || mode == 0 || mode == 1073741824)) {
            size = 0;
        }
        if (mode2 != Integer.MIN_VALUE) {
            if (mode2 == 0) {
                size2 = ((int) this.f22036g) * 2;
            } else if (mode2 != 1073741824) {
                size2 = 0;
            }
        }
        setMeasuredDimension(size, size2);
    }

    @DexIgnore
    @androidx.annotation.Keep
    public final void setLength(int i) {
        this.f22038i = i;
        invalidate();
    }

    @DexIgnore
    @androidx.annotation.Keep
    public final void setProgress(int i) {
        this.f22042m = i;
        invalidate();
    }

    @DexIgnore
    public DashBar(android.content.Context context, android.util.AttributeSet attributeSet) {
        super(context, attributeSet);
        android.content.res.TypedArray obtainStyledAttributes = context != null ? context.obtainStyledAttributes(attributeSet, com.fossil.blesdk.obfuscated.h62.DashBar) : null;
        if (obtainStyledAttributes != null) {
            this.f22036g = obtainStyledAttributes.getDimension(6, 10.0f);
            this.f22037h = obtainStyledAttributes.getDimension(3, 6.0f);
            this.f22038i = obtainStyledAttributes.getInteger(4, 4);
            this.f22039j = obtainStyledAttributes.getColor(2, 0);
            this.f22040k = obtainStyledAttributes.getColor(1, 0);
            this.f22041l = obtainStyledAttributes.getColor(0, 0);
            this.f22042m = obtainStyledAttributes.getInt(5, 0);
        }
        mo40435a(this.f22036g);
        setLayerType(1, (android.graphics.Paint) null);
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.recycle();
        }
    }
}
