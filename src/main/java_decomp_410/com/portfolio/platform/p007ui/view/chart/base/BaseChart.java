package com.portfolio.platform.p007ui.view.chart.base;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.view.chart.base.BaseChart */
public abstract class BaseChart extends android.view.ViewGroup {

    @DexIgnore
    /* renamed from: t */
    public static /* final */ com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6239b f22112t; // = new com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6239b((com.fossil.blesdk.obfuscated.fd4) null);

    @DexIgnore
    /* renamed from: e */
    public int f22113e;

    @DexIgnore
    /* renamed from: f */
    public int f22114f;

    @DexIgnore
    /* renamed from: g */
    public int f22115g;

    @DexIgnore
    /* renamed from: h */
    public int f22116h;

    @DexIgnore
    /* renamed from: i */
    public int f22117i;

    @DexIgnore
    /* renamed from: j */
    public int f22118j;

    @DexIgnore
    /* renamed from: k */
    public int f22119k;

    @DexIgnore
    /* renamed from: l */
    public int f22120l;

    @DexIgnore
    /* renamed from: m */
    public int f22121m;

    @DexIgnore
    /* renamed from: n */
    public int f22122n;

    @DexIgnore
    /* renamed from: o */
    public com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6240c f22123o;

    @DexIgnore
    /* renamed from: p */
    public com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6242e f22124p;

    @DexIgnore
    /* renamed from: q */
    public com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6238a f22125q;

    @DexIgnore
    /* renamed from: r */
    public com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6241d f22126r;

    @DexIgnore
    /* renamed from: s */
    public /* final */ java.lang.String f22127s;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.base.BaseChart$a")
    /* renamed from: com.portfolio.platform.ui.view.chart.base.BaseChart$a */
    public final class C6238a extends android.view.View {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.view.chart.base.BaseChart f22128e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C6238a(com.portfolio.platform.p007ui.view.chart.base.BaseChart baseChart, android.content.Context context) {
            super(context);
            com.fossil.blesdk.obfuscated.kd4.m24411b(context, "context");
            this.f22128e = baseChart;
        }

        @DexIgnore
        public void onDraw(android.graphics.Canvas canvas) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
            super.onDraw(canvas);
            this.f22128e.mo40584a(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.f22128e.mo40583a(i, i2, i3, i4);
        }

        @DexIgnore
        public boolean performClick() {
            return super.performClick();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.base.BaseChart$b")
    /* renamed from: com.portfolio.platform.ui.view.chart.base.BaseChart$b */
    public static final class C6239b {
        @DexIgnore
        public C6239b() {
        }

        @DexIgnore
        /* renamed from: a */
        public final boolean mo40624a(android.graphics.PointF pointF, android.graphics.RectF rectF) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(pointF, "point");
            com.fossil.blesdk.obfuscated.kd4.m24411b(rectF, "rect");
            float f = pointF.x;
            if (f <= rectF.right && f >= rectF.left) {
                float f2 = pointF.y;
                return f2 <= rectF.bottom && f2 >= rectF.top;
            }
        }

        @DexIgnore
        public /* synthetic */ C6239b(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.base.BaseChart$c")
    /* renamed from: com.portfolio.platform.ui.view.chart.base.BaseChart$c */
    public final class C6240c extends android.view.View {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.view.chart.base.BaseChart f22129e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C6240c(com.portfolio.platform.p007ui.view.chart.base.BaseChart baseChart, android.content.Context context) {
            super(context);
            com.fossil.blesdk.obfuscated.kd4.m24411b(context, "context");
            this.f22129e = baseChart;
        }

        @DexIgnore
        public void onDraw(android.graphics.Canvas canvas) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
            super.onDraw(canvas);
            this.f22129e.mo40455b(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.f22129e.setMGraphWidth(i);
            this.f22129e.setMGraphHeight(i2);
            this.f22129e.mo40588c(i, i2, i3, i4);
        }

        @DexIgnore
        public boolean performClick() {
            return super.performClick();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.base.BaseChart$d")
    /* renamed from: com.portfolio.platform.ui.view.chart.base.BaseChart$d */
    public final class C6241d extends android.view.View {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.view.chart.base.BaseChart f22130e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C6241d(com.portfolio.platform.p007ui.view.chart.base.BaseChart baseChart, android.content.Context context) {
            super(context);
            com.fossil.blesdk.obfuscated.kd4.m24411b(context, "context");
            this.f22130e = baseChart;
        }

        @DexIgnore
        public void onDraw(android.graphics.Canvas canvas) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
            super.onDraw(canvas);
            this.f22130e.mo40589c(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.f22130e.mo40586b(i, i2, i3, i4);
        }

        @DexIgnore
        public boolean onTouchEvent(android.view.MotionEvent motionEvent) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(motionEvent, com.misfit.frameworks.common.constants.Constants.EVENT);
            return this.f22130e.mo40454a(motionEvent);
        }

        @DexIgnore
        public boolean performClick() {
            return super.performClick();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.base.BaseChart$e")
    /* renamed from: com.portfolio.platform.ui.view.chart.base.BaseChart$e */
    public final class C6242e extends android.view.View {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.view.chart.base.BaseChart f22131e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C6242e(com.portfolio.platform.p007ui.view.chart.base.BaseChart baseChart, android.content.Context context) {
            super(context);
            com.fossil.blesdk.obfuscated.kd4.m24411b(context, "context");
            this.f22131e = baseChart;
        }

        @DexIgnore
        public void onDraw(android.graphics.Canvas canvas) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
            super.onDraw(canvas);
            this.f22131e.mo40458d(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.f22131e.setMLegendWidth(i);
            this.f22131e.mo40590d(i, i2, i3, i4);
        }
    }

    @DexIgnore
    public BaseChart(android.content.Context context) {
        super(context);
        this.f22127s = getClass().getSimpleName() == null ? "BaseChart" : getClass().getSimpleName();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo40449a() {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f22127s, "initGraph");
        android.content.Context context = getContext();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) context, "context");
        this.f22123o = new com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6240c(this, context);
        com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6240c cVar = this.f22123o;
        if (cVar != null) {
            addView(cVar);
            android.content.Context context2 = getContext();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) context2, "context");
            this.f22124p = new com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6242e(this, context2);
            com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6242e eVar = this.f22124p;
            if (eVar != null) {
                addView(eVar);
                android.content.Context context3 = getContext();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) context3, "context");
                this.f22125q = new com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6238a(this, context3);
                com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6238a aVar = this.f22125q;
                if (aVar != null) {
                    addView(aVar);
                    android.content.Context context4 = getContext();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) context4, "context");
                    this.f22126r = new com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6241d(this, context4);
                    com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6241d dVar = this.f22126r;
                    if (dVar != null) {
                        addView(dVar);
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphOverlay");
                        throw null;
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24414d("mBackground");
                    throw null;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24414d("mLegend");
                throw null;
            }
        } else {
            com.fossil.blesdk.obfuscated.kd4.m24414d("mGraph");
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo40583a(int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo40584a(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo40585b() {
        com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6240c cVar = this.f22123o;
        if (cVar != null) {
            cVar.invalidate();
            com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6242e eVar = this.f22124p;
            if (eVar != null) {
                eVar.invalidate();
                com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6238a aVar = this.f22125q;
                if (aVar != null) {
                    aVar.invalidate();
                    com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6241d dVar = this.f22126r;
                    if (dVar != null) {
                        dVar.invalidate();
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphOverlay");
                        throw null;
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24414d("mBackground");
                    throw null;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24414d("mLegend");
                throw null;
            }
        } else {
            com.fossil.blesdk.obfuscated.kd4.m24414d("mGraph");
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo40586b(int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    /* renamed from: b */
    public void mo40455b(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo40587c() {
        mo40585b();
    }

    @DexIgnore
    /* renamed from: c */
    public void mo40588c(int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    /* renamed from: c */
    public void mo40589c(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
    }

    @DexIgnore
    /* renamed from: d */
    public void mo40590d(int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    /* renamed from: d */
    public void mo40458d(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
    }

    @DexIgnore
    public final com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6238a getMBackground() {
        com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6238a aVar = this.f22125q;
        if (aVar != null) {
            return aVar;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mBackground");
        throw null;
    }

    @DexIgnore
    public final int getMBottomPadding() {
        return this.f22118j;
    }

    @DexIgnore
    public final com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6240c getMGraph() {
        com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6240c cVar = this.f22123o;
        if (cVar != null) {
            return cVar;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mGraph");
        throw null;
    }

    @DexIgnore
    public final int getMGraphHeight() {
        return this.f22120l;
    }

    @DexIgnore
    public final com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6241d getMGraphOverlay() {
        com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6241d dVar = this.f22126r;
        if (dVar != null) {
            return dVar;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphOverlay");
        throw null;
    }

    @DexIgnore
    public final int getMGraphWidth() {
        return this.f22119k;
    }

    @DexIgnore
    public final int getMHeight() {
        return this.f22113e;
    }

    @DexIgnore
    public final int getMLeftPadding() {
        return this.f22115g;
    }

    @DexIgnore
    public final com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6242e getMLegend() {
        com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6242e eVar = this.f22124p;
        if (eVar != null) {
            return eVar;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mLegend");
        throw null;
    }

    @DexIgnore
    public final int getMLegendHeight() {
        return this.f22122n;
    }

    @DexIgnore
    public final int getMLegendWidth() {
        return this.f22121m;
    }

    @DexIgnore
    public final int getMRightPadding() {
        return this.f22117i;
    }

    @DexIgnore
    public final int getMTopPadding() {
        return this.f22116h;
    }

    @DexIgnore
    public final int getMWidth() {
        return this.f22114f;
    }

    @DexIgnore
    public final java.lang.String getTAG() {
        return this.f22127s;
    }

    @DexIgnore
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.f22114f = i;
        this.f22113e = i2;
        this.f22115g = getPaddingLeft();
        this.f22116h = getPaddingTop();
        this.f22117i = getPaddingRight();
        this.f22118j = getPaddingBottom();
        com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6240c cVar = this.f22123o;
        if (cVar != null) {
            cVar.layout(this.f22115g, this.f22116h, i - this.f22117i, (i2 - this.f22122n) - this.f22118j);
            com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6242e eVar = this.f22124p;
            if (eVar != null) {
                int i5 = this.f22115g;
                int i6 = this.f22118j;
                eVar.layout(i5, (i2 - this.f22122n) - i6, i - this.f22117i, i2 - i6);
                com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6238a aVar = this.f22125q;
                if (aVar != null) {
                    aVar.layout(this.f22115g, this.f22116h, i - this.f22117i, i2 - this.f22118j);
                    com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6241d dVar = this.f22126r;
                    if (dVar != null) {
                        dVar.layout(this.f22115g, this.f22116h, i - this.f22117i, (i2 - this.f22122n) - this.f22118j);
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphOverlay");
                        throw null;
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24414d("mBackground");
                    throw null;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24414d("mLegend");
                throw null;
            }
        } else {
            com.fossil.blesdk.obfuscated.kd4.m24414d("mGraph");
            throw null;
        }
    }

    @DexIgnore
    public final void setMBackground(com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6238a aVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(aVar, "<set-?>");
        this.f22125q = aVar;
    }

    @DexIgnore
    public final void setMBottomPadding(int i) {
        this.f22118j = i;
    }

    @DexIgnore
    public final void setMGraph(com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6240c cVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(cVar, "<set-?>");
        this.f22123o = cVar;
    }

    @DexIgnore
    public final void setMGraphHeight(int i) {
        this.f22120l = i;
    }

    @DexIgnore
    public final void setMGraphOverlay(com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6241d dVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "<set-?>");
        this.f22126r = dVar;
    }

    @DexIgnore
    public final void setMGraphWidth(int i) {
        this.f22119k = i;
    }

    @DexIgnore
    public final void setMHeight(int i) {
        this.f22113e = i;
    }

    @DexIgnore
    public final void setMLeftPadding(int i) {
        this.f22115g = i;
    }

    @DexIgnore
    public final void setMLegend(com.portfolio.platform.p007ui.view.chart.base.BaseChart.C6242e eVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(eVar, "<set-?>");
        this.f22124p = eVar;
    }

    @DexIgnore
    public final void setMLegendHeight(int i) {
        this.f22122n = i;
    }

    @DexIgnore
    public final void setMLegendWidth(int i) {
        this.f22121m = i;
    }

    @DexIgnore
    public final void setMRightPadding(int i) {
        this.f22117i = i;
    }

    @DexIgnore
    public final void setMTopPadding(int i) {
        this.f22116h = i;
    }

    @DexIgnore
    public final void setMWidth(int i) {
        this.f22114f = i;
    }

    @DexIgnore
    public BaseChart(android.content.Context context, android.util.AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f22127s = getClass().getSimpleName() == null ? "BaseChart" : getClass().getSimpleName();
    }

    @DexIgnore
    public BaseChart(android.content.Context context, android.util.AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f22127s = getClass().getSimpleName() == null ? "BaseChart" : getClass().getSimpleName();
    }

    @DexIgnore
    public BaseChart(android.content.Context context, android.util.AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.f22127s = getClass().getSimpleName() == null ? "BaseChart" : getClass().getSimpleName();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo40454a(android.view.MotionEvent motionEvent) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(motionEvent, com.misfit.frameworks.common.constants.Constants.EVENT);
        return super.onTouchEvent(motionEvent);
    }
}
