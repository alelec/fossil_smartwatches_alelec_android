package com.portfolio.platform.p007ui.view;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.view.SleepQualityChart */
public final class SleepQualityChart extends android.view.View {

    @DexIgnore
    /* renamed from: e */
    public android.graphics.Paint f22043e; // = new android.graphics.Paint(1);

    @DexIgnore
    /* renamed from: f */
    public float f22044f; // = 10.0f;

    @DexIgnore
    /* renamed from: g */
    public int f22045g;

    @DexIgnore
    /* renamed from: h */
    public int f22046h;

    @DexIgnore
    /* renamed from: i */
    public android.graphics.drawable.Drawable f22047i;

    @DexIgnore
    /* renamed from: j */
    public android.graphics.Bitmap f22048j;

    @DexIgnore
    /* renamed from: k */
    public int f22049k; // = 50;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.SleepQualityChart$a")
    /* renamed from: com.portfolio.platform.ui.view.SleepQualityChart$a */
    public static final class C6228a {
        @DexIgnore
        public C6228a() {
        }

        @DexIgnore
        public /* synthetic */ C6228a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.view.SleepQualityChart.C6228a((com.fossil.blesdk.obfuscated.fd4) null);
    }
    */

    @DexIgnore
    public SleepQualityChart(android.content.Context context) {
        super(context, (android.util.AttributeSet) null);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40441a(float f) {
        this.f22043e.setDither(true);
        this.f22043e.setStyle(android.graphics.Paint.Style.STROKE);
        this.f22043e.setStrokeWidth(f);
        this.f22043e.setAntiAlias(true);
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo40442b(float f) {
        android.graphics.Paint paint = this.f22043e;
        android.graphics.LinearGradient linearGradient = new android.graphics.LinearGradient(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.f22045g, this.f22046h, android.graphics.Shader.TileMode.CLAMP);
        paint.setShader(linearGradient);
        this.f22043e.setPathEffect(new android.graphics.DashPathEffect(new float[]{f / ((float) 5), 6.0f}, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
    }

    @DexIgnore
    public void onDraw(android.graphics.Canvas canvas) {
        mo40442b((float) getWidth());
        if (canvas != null) {
            android.graphics.Bitmap bitmap = this.f22048j;
            float f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (bitmap != null) {
                float width = (float) bitmap.getWidth();
                float height = (float) bitmap.getHeight();
                float width2 = ((float) (canvas.getWidth() * this.f22049k)) / 100.0f;
                if (width2 + width > ((float) canvas.getWidth())) {
                    width2 = ((float) canvas.getWidth()) - width;
                }
                canvas.drawBitmap(bitmap, width2, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.f22043e);
                f = height;
            }
            float f2 = f + (this.f22044f / ((float) 2)) + 2.0f;
            canvas.drawLine(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, (float) canvas.getWidth(), f2, this.f22043e);
        }
    }

    @DexIgnore
    public void onMeasure(int i, int i2) {
        android.graphics.Bitmap bitmap = this.f22048j;
        if (bitmap != null) {
            bitmap.getHeight();
        }
        int mode = android.view.View.MeasureSpec.getMode(i);
        int size = android.view.View.MeasureSpec.getSize(i);
        int mode2 = android.view.View.MeasureSpec.getMode(i2);
        int size2 = android.view.View.MeasureSpec.getSize(i2);
        int i3 = 0;
        if (!(mode == Integer.MIN_VALUE || mode == 0 || mode == 1073741824)) {
            size = 0;
        }
        if (mode2 != Integer.MIN_VALUE) {
            if (mode2 == 0) {
                android.graphics.Bitmap bitmap2 = this.f22048j;
                if (bitmap2 != null) {
                    i3 = bitmap2.getHeight();
                }
                size2 = (((int) this.f22044f) * 2) + i3;
            } else if (mode2 != 1073741824) {
                size2 = 0;
            }
        }
        setMeasuredDimension(size, size2);
    }

    @DexIgnore
    public final void setPercent(int i) {
        this.f22049k = i;
        invalidate();
    }

    @DexIgnore
    public SleepQualityChart(android.content.Context context, android.util.AttributeSet attributeSet) {
        super(context, attributeSet);
        android.graphics.Bitmap bitmap = null;
        android.content.res.TypedArray obtainStyledAttributes = context != null ? context.obtainStyledAttributes(attributeSet, com.fossil.blesdk.obfuscated.h62.SleepQualityChart) : null;
        if (obtainStyledAttributes != null) {
            this.f22044f = obtainStyledAttributes.getDimension(4, 10.0f);
            this.f22045g = obtainStyledAttributes.getColor(1, 0);
            this.f22046h = obtainStyledAttributes.getColor(0, 0);
            this.f22047i = obtainStyledAttributes.getDrawable(3);
            this.f22049k = obtainStyledAttributes.getInt(2, 0);
        }
        mo40441a(this.f22044f);
        android.graphics.drawable.BitmapDrawable bitmapDrawable = (android.graphics.drawable.BitmapDrawable) this.f22047i;
        this.f22048j = bitmapDrawable != null ? bitmapDrawable.getBitmap() : bitmap;
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.recycle();
        }
    }
}
