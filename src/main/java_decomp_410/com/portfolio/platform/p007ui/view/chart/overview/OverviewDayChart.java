package com.portfolio.platform.p007ui.view.chart.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewDayChart */
public class OverviewDayChart extends com.portfolio.platform.p007ui.view.chart.base.BarChart {

    @DexIgnore
    /* renamed from: u0 */
    public android.animation.ObjectAnimator f22132u0;

    @DexIgnore
    /* renamed from: v0 */
    public android.animation.ObjectAnimator f22133v0;

    @DexIgnore
    /* renamed from: w0 */
    public com.fossil.blesdk.obfuscated.wr2 f22134w0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.overview.OverviewDayChart$a")
    /* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewDayChart$a */
    public static final class C6243a {
        @DexIgnore
        public C6243a() {
        }

        @DexIgnore
        public /* synthetic */ C6243a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.overview.OverviewDayChart$b")
    /* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewDayChart$b */
    public static final class C6244b implements android.animation.Animator.AnimatorListener {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.view.chart.overview.OverviewDayChart f22135a;

        @DexIgnore
        public C6244b(com.portfolio.platform.p007ui.view.chart.overview.OverviewDayChart overviewDayChart) {
            this.f22135a = overviewDayChart;
        }

        @DexIgnore
        public void onAnimationCancel(android.animation.Animator animator) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f22135a.getTAG(), "changeModel - onAnimationCancel");
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tag = this.f22135a.getTAG();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("changeModel - onAnimationEnd -- isRunning=");
            android.animation.ObjectAnimator b = this.f22135a.f22132u0;
            sb.append(b != null ? java.lang.Boolean.valueOf(b.isRunning()) : null);
            local.mo33255d(tag, sb.toString());
            com.portfolio.platform.p007ui.view.chart.overview.OverviewDayChart overviewDayChart = this.f22135a;
            com.fossil.blesdk.obfuscated.wr2 c = overviewDayChart.f22134w0;
            if (c != null) {
                overviewDayChart.mo40456b(c);
                android.animation.ObjectAnimator a = this.f22135a.f22133v0;
                if (a != null) {
                    a.start();
                    return;
                }
                return;
            }
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        }

        @DexIgnore
        public void onAnimationRepeat(android.animation.Animator animator) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f22135a.getTAG(), "changeModel - onAnimationRepeat");
        }

        @DexIgnore
        public void onAnimationStart(android.animation.Animator animator) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f22135a.getTAG(), "changeModel - onAnimationStart");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.overview.OverviewDayChart$c")
    /* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewDayChart$c */
    public static final class C6245c<T> implements java.util.Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return com.fossil.blesdk.obfuscated.wb4.m29575a(((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t).mo40563c(), ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t2).mo40563c());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.overview.OverviewDayChart$d")
    /* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewDayChart$d */
    public static final class C6246d<T> implements java.util.Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return com.fossil.blesdk.obfuscated.wb4.m29575a(((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t).mo40563c(), ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t2).mo40563c());
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.view.chart.overview.OverviewDayChart.C6243a((com.fossil.blesdk.obfuscated.fd4) null);
    }
    */

    @DexIgnore
    public OverviewDayChart(android.content.Context context) {
        this(context, (android.util.AttributeSet) null);
    }

    @DexIgnore
    /* renamed from: e */
    public void mo40460e(android.graphics.Canvas canvas) {
        java.lang.Object obj;
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        getMGraphPaint().setColor(getMDefaultColor());
        int a = mo40635a((java.util.List<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a>) getMChartModel().mo40570a());
        java.util.Iterator<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> it = getMChartModel().mo40570a().iterator();
        while (it.hasNext()) {
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a next = it.next();
            java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList = next.mo40552c().get(0);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList, "item.mListOfBarPoints[0]");
            java.util.List a2 = com.fossil.blesdk.obfuscated.kb4.m24368a(arrayList, new com.portfolio.platform.p007ui.view.chart.overview.OverviewDayChart.C6245c());
            android.graphics.Paint mGraphPaint = getMGraphPaint();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) next, "item");
            mGraphPaint.setAlpha(mo40634a(next, a));
            java.util.Iterator it2 = a2.iterator();
            if (!it2.hasNext()) {
                obj = null;
            } else {
                obj = it2.next();
                if (it2.hasNext()) {
                    int e = ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) obj).mo40566e();
                    do {
                        java.lang.Object next2 = it2.next();
                        int e2 = ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2).mo40566e();
                        if (e < e2) {
                            obj = next2;
                            e = e2;
                        }
                    } while (it2.hasNext());
                }
            }
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) obj;
            if (bVar != null) {
                canvas.drawRoundRect(bVar.mo40557a(), getMBarRadius(), getMBarRadius(), getMGraphPaint());
            }
        }
    }

    @DexIgnore
    /* renamed from: f */
    public final android.animation.ObjectAnimator mo40637f(int i, int i2, int i3, int i4) {
        android.animation.ObjectAnimator ofPropertyValuesHolder = android.animation.ObjectAnimator.ofPropertyValuesHolder(this, new android.animation.PropertyValuesHolder[]{android.animation.PropertyValuesHolder.ofInt("maxValue", new int[]{i, i4 * i}), android.animation.PropertyValuesHolder.ofInt("barAlpha", new int[]{i2, i3})});
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026s, outMaxValue, outAlpha)");
        ofPropertyValuesHolder.setDuration(200);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    /* renamed from: h */
    public void mo40502h(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        float width = (float) canvas.getWidth();
        java.util.Iterator<kotlin.Pair<java.lang.Integer, android.graphics.PointF>> it = getMGraphLegendPoint().iterator();
        while (it.hasNext()) {
            kotlin.Pair next = it.next();
            android.graphics.Rect rect = new android.graphics.Rect();
            float intValue = (float) ((java.lang.Number) next.getFirst()).intValue();
            java.lang.String valueOf = java.lang.String.valueOf((int) intValue);
            if (intValue >= ((float) 1000)) {
                valueOf = com.fossil.blesdk.obfuscated.il2.m23576a(intValue / r4, 1) + com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.DashboardDiana_Main_StepsToday_Label__K);
            }
            java.lang.String str = valueOf;
            getMLegendPaint().getTextBounds(str, 0, str.length(), rect);
            float f = ((android.graphics.PointF) next.getSecond()).y;
            canvas.drawLine(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f, width, f, getMLegendLinePaint());
            canvas.drawText(str, (width - getMGraphLegendMargin()) - ((float) rect.width()), f + getMGraphLegendMargin() + ((float) rect.height()), getMLegendPaint());
        }
    }

    @DexIgnore
    /* renamed from: i */
    public void mo40503i(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
    }

    @DexIgnore
    public OverviewDayChart(android.content.Context context, android.util.AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo40449a() {
        super.mo40449a();
        setMNumberBar(24);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo40456b(com.fossil.blesdk.obfuscated.wr2 wr2) {
        int i;
        com.fossil.blesdk.obfuscated.kd4.m24411b(wr2, com.facebook.devicerequests.internal.DeviceRequestsHelper.DEVICE_INFO_MODEL);
        setMChartModel((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c) wr2);
        setMMaxValue(getMChartModel().mo40575c());
        if (getMChartModel().mo40570a().size() <= 1) {
            i = 24;
        } else {
            i = getMChartModel().mo40570a().size();
        }
        setMNumberBar(i);
    }

    @DexIgnore
    public OverviewDayChart(android.content.Context context, android.util.AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewDayChart(android.content.Context context, android.util.AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0202, code lost:
        return;
     */
    @DexIgnore
    /* renamed from: a */
    public synchronized void mo40452a(com.fossil.blesdk.obfuscated.wr2 wr2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(wr2, com.facebook.devicerequests.internal.DeviceRequestsHelper.DEVICE_INFO_MODEL);
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String tag = getTAG();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("changeModel - model=");
        sb.append(wr2);
        sb.append(", mOutAnim.isRunning=");
        android.animation.ObjectAnimator objectAnimator = this.f22132u0;
        sb.append(objectAnimator != null ? java.lang.Boolean.valueOf(objectAnimator.isRunning()) : null);
        sb.append(", mInAnim.isRunning=");
        android.animation.ObjectAnimator objectAnimator2 = this.f22133v0;
        sb.append(objectAnimator2 != null ? java.lang.Boolean.valueOf(objectAnimator2.isRunning()) : null);
        local.mo33255d(tag, sb.toString());
        android.animation.ObjectAnimator objectAnimator3 = this.f22132u0;
        java.lang.Boolean valueOf = objectAnimator3 != null ? java.lang.Boolean.valueOf(objectAnimator3.isRunning()) : null;
        android.animation.ObjectAnimator objectAnimator4 = this.f22133v0;
        java.lang.Boolean valueOf2 = objectAnimator4 != null ? java.lang.Boolean.valueOf(objectAnimator4.isRunning()) : null;
        if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) valueOf, (java.lang.Object) true)) {
            if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) valueOf2, (java.lang.Object) true)) {
                if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) getMChartModel(), (java.lang.Object) wr2)) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(getTAG(), "changeModel - mChartModel == model");
                    return;
                }
                this.f22134w0 = wr2;
                this.f22132u0 = m33113b(this, getMMaxValue(), 255, 0, 0, 8, (java.lang.Object) null);
                com.fossil.blesdk.obfuscated.wr2 wr22 = this.f22134w0;
                if (wr22 != null) {
                    this.f22133v0 = m33111a(this, ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c) wr22).mo40575c(), 0, 255, 0, 8, (java.lang.Object) null);
                    android.animation.ObjectAnimator objectAnimator5 = this.f22132u0;
                    if (objectAnimator5 != null) {
                        objectAnimator5.addListener(new com.portfolio.platform.p007ui.view.chart.overview.OverviewDayChart.C6244b(this));
                    }
                    android.animation.ObjectAnimator objectAnimator6 = this.f22132u0;
                    if (objectAnimator6 != null) {
                        objectAnimator6.start();
                    }
                } else {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                }
            }
        }
        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) wr2, (java.lang.Object) this.f22134w0)) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(getTAG(), "changeModel - model == mTempModel");
            return;
        }
        this.f22134w0 = wr2;
        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) valueOf, (java.lang.Object) true)) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(getTAG(), "changeModel - outRunning == true");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tag2 = getTAG();
            java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
            sb2.append("changeModel - outRunning == true - mMaxValue=");
            com.fossil.blesdk.obfuscated.wr2 wr23 = this.f22134w0;
            if (wr23 != null) {
                sb2.append(((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c) wr23).mo40575c());
                local2.mo33255d(tag2, sb2.toString());
                com.fossil.blesdk.obfuscated.wr2 wr24 = this.f22134w0;
                if (wr24 != null) {
                    this.f22133v0 = m33111a(this, ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c) wr24).mo40575c(), 0, 255, 0, 8, (java.lang.Object) null);
                } else {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                }
            } else {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
            }
        } else {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(getTAG(), "changeModel - inRunning == true");
            android.animation.ObjectAnimator objectAnimator7 = this.f22133v0;
            if (objectAnimator7 != null) {
                objectAnimator7.cancel();
            }
            int mMaxValue = getMMaxValue();
            int c = getMChartModel().mo40575c();
            int mBarAlpha = getMBarAlpha();
            if (c <= 0) {
                c = 1;
            }
            int i = mMaxValue / c;
            com.fossil.blesdk.obfuscated.wr2 wr25 = this.f22134w0;
            if (wr25 != null) {
                mo40456b(wr25);
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String tag3 = getTAG();
                local3.mo33255d(tag3, "changeModel - inRunning == true -- tempStartMaxValue=" + mMaxValue + ", " + "tempEndMaxValue=" + mMaxValue + ", tempAlpha=" + mMaxValue + ", tempMaxRate=" + mMaxValue + ", newMaxValue=" + getMChartModel().mo40575c());
                this.f22133v0 = mo40636e(getMChartModel().mo40575c(), mBarAlpha, 255, i);
                android.animation.ObjectAnimator objectAnimator8 = this.f22133v0;
                if (objectAnimator8 != null) {
                    objectAnimator8.start();
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static /* synthetic */ android.animation.ObjectAnimator m33113b(com.portfolio.platform.p007ui.view.chart.overview.OverviewDayChart overviewDayChart, int i, int i2, int i3, int i4, int i5, java.lang.Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewDayChart.mo40637f(i, i2, i3, i4);
        }
        throw new java.lang.UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createOutAnim");
    }

    @DexIgnore
    /* renamed from: b */
    public void mo40455b(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        mo40457d();
        mo40459e();
        mo40502h(canvas);
        mo40460e(canvas);
    }

    @DexIgnore
    /* renamed from: e */
    public final android.animation.ObjectAnimator mo40636e(int i, int i2, int i3, int i4) {
        android.animation.ObjectAnimator ofPropertyValuesHolder = android.animation.ObjectAnimator.ofPropertyValuesHolder(this, new android.animation.PropertyValuesHolder[]{android.animation.PropertyValuesHolder.ofInt("maxValue", new int[]{i4 * i, i}), android.animation.PropertyValuesHolder.ofInt("barAlpha", new int[]{i2, i3})});
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026his, inMaxValue, inAlpha)");
        ofPropertyValuesHolder.setDuration(200);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    /* renamed from: a */
    public static /* synthetic */ android.animation.ObjectAnimator m33111a(com.portfolio.platform.p007ui.view.chart.overview.OverviewDayChart overviewDayChart, int i, int i2, int i3, int i4, int i5, java.lang.Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewDayChart.mo40636e(i, i2, i3, i4);
        }
        throw new java.lang.UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createInAnim");
    }

    @DexIgnore
    /* renamed from: a */
    public void mo40584a(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        super.mo40584a(canvas);
        java.util.Iterator<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> it = getMChartModel().mo40570a().iterator();
        while (it.hasNext()) {
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a next = it.next();
            java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList = next.mo40552c().get(0);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList, "item.mListOfBarPoints[0]");
            java.util.List a = com.fossil.blesdk.obfuscated.kb4.m24368a(arrayList, new com.portfolio.platform.p007ui.view.chart.overview.OverviewDayChart.C6246d());
            if ((!a.isEmpty()) && next.mo40553d()) {
                android.graphics.Bitmap a2 = com.portfolio.platform.p007ui.view.chart.base.BarChart.m33049a((com.portfolio.platform.p007ui.view.chart.base.BarChart) this, getMLegendIconRes(), 0, 2, (java.lang.Object) null);
                if (a2 != null) {
                    android.graphics.RectF a3 = ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) a.get(0)).mo40557a();
                    canvas.drawBitmap(a2, a3.left + ((a3.width() - ((float) a2.getWidth())) * 0.5f), a3.bottom + ((float) getMTextMargin()), new android.graphics.Paint(1));
                    a2.recycle();
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo40450a(float f, float f2) {
        android.graphics.Rect rect = new android.graphics.Rect();
        getMLegendPaint().getTextBounds("gh", 0, 2, rect);
        float mLegendHeight = ((float) (getMLegendHeight() + rect.height())) * 0.5f;
        int size = getMLegendTexts().size();
        if (size < 1) {
            getMTextPoint().clear();
            return;
        }
        float f3 = size == 1 ? com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : (f2 - f) / ((float) (size - 1));
        float mBarWidth = f + (getMBarWidth() * 0.5f);
        int i = 0;
        for (T next : getMLegendTexts()) {
            int i2 = i + 1;
            if (i >= 0) {
                java.lang.String str = (java.lang.String) next;
                if (kotlin.text.StringsKt__StringsKt.m37073a((java.lang.CharSequence) str, (java.lang.CharSequence) org.slf4j.Marker.ANY_NON_NULL_MARKER, false, 2, (java.lang.Object) null)) {
                    java.lang.String str2 = (java.lang.String) kotlin.text.StringsKt__StringsKt.m37069a((java.lang.CharSequence) str, new java.lang.String[]{org.slf4j.Marker.ANY_NON_NULL_MARKER}, false, 0, 6, (java.lang.Object) null).get(0);
                    getMLegendPaint().getTextBounds(str2, 0, str2.length(), rect);
                    getMTextPoint().add(new kotlin.Pair(str, new android.graphics.PointF(((((float) i) * f3) + mBarWidth) - (((float) rect.width()) * 0.5f), mLegendHeight)));
                } else {
                    getMLegendPaint().getTextBounds(str, 0, str.length(), rect);
                    getMTextPoint().add(new kotlin.Pair(str, new android.graphics.PointF(((((float) i) * f3) + mBarWidth) - (((float) rect.width()) * 0.5f), mLegendHeight)));
                }
                i = i2;
            } else {
                com.fossil.blesdk.obfuscated.cb4.m20543c();
                throw null;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo40635a(java.util.List<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> list) {
        if (list.isEmpty()) {
            return 0;
        }
        int i = Integer.MIN_VALUE;
        for (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a aVar : list) {
            if (!aVar.mo40552c().isEmpty()) {
                java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList = aVar.mo40552c().get(0);
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList, "model.mListOfBarPoints[0]");
                for (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b e : arrayList) {
                    int e2 = e.mo40566e();
                    if (e2 > i) {
                        i = e2;
                    }
                }
            }
        }
        return i;
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo40634a(com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a aVar, int i) {
        java.lang.Object obj;
        if (i < 0) {
            return 255;
        }
        int i2 = 0;
        java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList = aVar.mo40552c().get(0);
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList, "barModel.mListOfBarPoints[0]");
        java.util.Iterator it = arrayList.iterator();
        if (!it.hasNext()) {
            obj = null;
        } else {
            obj = it.next();
            if (it.hasNext()) {
                int e = ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) obj).mo40566e();
                do {
                    java.lang.Object next = it.next();
                    int e2 = ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next).mo40566e();
                    if (e < e2) {
                        obj = next;
                        e = e2;
                    }
                } while (it.hasNext());
            }
        }
        com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) obj;
        if (bVar != null) {
            i2 = bVar.mo40566e();
        }
        double d = (double) (((float) i2) / ((float) i));
        if (d >= 0.75d) {
            return 255;
        }
        return (int) ((d < 0.5d || d >= 0.75d) ? (d < 0.25d || d >= 0.5d) ? 63.75d : 127.5d : 191.25d);
    }
}
