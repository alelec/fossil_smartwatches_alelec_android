package com.portfolio.platform.p007ui.debug;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.debug.DebugActivity */
public final class DebugActivity extends com.portfolio.platform.p007ui.BaseActivity implements com.fossil.blesdk.obfuscated.jq2.C4553b {

    @DexIgnore
    /* renamed from: P */
    public static /* final */ com.portfolio.platform.p007ui.debug.DebugActivity.C6109a f21735P; // = new com.portfolio.platform.p007ui.debug.DebugActivity.C6109a((com.fossil.blesdk.obfuscated.fd4) null);

    @DexIgnore
    /* renamed from: B */
    public com.fossil.blesdk.obfuscated.en2 f21736B;

    @DexIgnore
    /* renamed from: C */
    public com.fossil.blesdk.obfuscated.cr2 f21737C;

    @DexIgnore
    /* renamed from: D */
    public com.misfit.frameworks.buttonservice.source.FirmwareFileRepository f21738D;

    @DexIgnore
    /* renamed from: E */
    public com.portfolio.platform.data.source.remote.GuestApiService f21739E;

    @DexIgnore
    /* renamed from: F */
    public com.portfolio.platform.data.source.DianaPresetRepository f21740F;

    @DexIgnore
    /* renamed from: G */
    public com.portfolio.platform.service.ShakeFeedbackService f21741G;

    @DexIgnore
    /* renamed from: H */
    public com.fossil.blesdk.obfuscated.vj2 f21742H;

    @DexIgnore
    /* renamed from: I */
    public /* final */ com.fossil.blesdk.obfuscated.jq2 f21743I; // = new com.fossil.blesdk.obfuscated.jq2();

    @DexIgnore
    /* renamed from: J */
    public /* final */ java.util.ArrayList<com.fossil.blesdk.obfuscated.pq2> f21744J; // = new java.util.ArrayList<>();

    @DexIgnore
    /* renamed from: K */
    public com.portfolio.platform.viewmodel.FirmwareDebugViewModel f21745K;

    @DexIgnore
    /* renamed from: L */
    public java.lang.String f21746L; // = "";

    @DexIgnore
    /* renamed from: M */
    public com.misfit.frameworks.buttonservice.enums.HeartRateMode f21747M;

    @DexIgnore
    /* renamed from: N */
    public boolean f21748N;

    @DexIgnore
    /* renamed from: O */
    public /* final */ com.portfolio.platform.p007ui.debug.DebugActivity.C6111c f21749O; // = new com.portfolio.platform.p007ui.debug.DebugActivity.C6111c(this);

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$a")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$a */
    public static final class C6109a {
        @DexIgnore
        public C6109a() {
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo40249a(android.content.Context context) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(context, "context");
            context.startActivity(new android.content.Intent(context, com.portfolio.platform.p007ui.debug.DebugActivity.class));
        }

        @DexIgnore
        public /* synthetic */ C6109a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$b")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$b */
    public final class C6110b implements android.text.InputFilter {

        @DexIgnore
        /* renamed from: e */
        public double f21750e;

        @DexIgnore
        /* renamed from: f */
        public double f21751f;

        @DexIgnore
        public C6110b(com.portfolio.platform.p007ui.debug.DebugActivity debugActivity, double d, double d2) {
            this.f21750e = d;
            this.f21751f = d2;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0018 A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* renamed from: a */
        public final boolean mo40250a(double d, double d2, double d3) {
            if (d2 > d) {
                return d3 >= d && d3 <= d2;
            }
            if (d3 >= d2 && d3 <= d) {
                return true;
            }
        }

        @DexIgnore
        public java.lang.CharSequence filter(java.lang.CharSequence charSequence, int i, int i2, android.text.Spanned spanned, int i3, int i4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(charSequence, "source");
            com.fossil.blesdk.obfuscated.kd4.m24411b(spanned, "dest");
            try {
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                java.lang.String obj = spanned.toString();
                if (obj != null) {
                    java.lang.String substring = obj.substring(0, i3);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    sb.append(substring);
                    java.lang.String obj2 = spanned.toString();
                    int length = spanned.toString().length();
                    if (obj2 != null) {
                        java.lang.String substring2 = obj2.substring(i4, length);
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                        sb.append(substring2);
                        java.lang.String sb2 = sb.toString();
                        java.lang.StringBuilder sb3 = new java.lang.StringBuilder();
                        if (sb2 != null) {
                            java.lang.String substring3 = sb2.substring(0, i3);
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) substring3, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                            sb3.append(substring3);
                            sb3.append(charSequence.toString());
                            int length2 = sb2.length();
                            if (sb2 != null) {
                                java.lang.String substring4 = sb2.substring(i3, length2);
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) substring4, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                                sb3.append(substring4);
                                if (mo40250a(this.f21750e, this.f21751f, java.lang.Double.parseDouble(sb3.toString()))) {
                                    return null;
                                }
                                return "";
                            }
                            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
                throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
            } catch (java.lang.Exception e) {
                e.printStackTrace();
                return "";
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$c")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$c */
    public static final class C6111c implements com.portfolio.platform.service.BleCommandResultManager.C5999b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.debug.DebugActivity f21752a;

        @DexIgnore
        public C6111c(com.portfolio.platform.p007ui.debug.DebugActivity debugActivity) {
            this.f21752a = debugActivity;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo27136a(com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode, android.content.Intent intent) {
            T t;
            T t2;
            T t3;
            T t4;
            com.fossil.blesdk.obfuscated.kd4.m24411b(communicateMode, "communicateMode");
            com.fossil.blesdk.obfuscated.kd4.m24411b(intent, "intent");
            com.fossil.blesdk.obfuscated.lq2 lq2 = null;
            if (communicateMode == com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_HEART_RATE_MODE) {
                if (intent.getIntExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.f21752a.mo40231C().mo26995a(this.f21752a.mo40248z());
                    java.util.Iterator<T> it = this.f21752a.mo40243u().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t3 = null;
                            break;
                        }
                        t3 = it.next();
                        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.fossil.blesdk.obfuscated.pq2) t3).mo30205b(), (java.lang.Object) "OTHER")) {
                            break;
                        }
                    }
                    com.fossil.blesdk.obfuscated.pq2 pq2 = (com.fossil.blesdk.obfuscated.pq2) t3;
                    if (pq2 != null) {
                        java.util.ArrayList<com.fossil.blesdk.obfuscated.lq2> a = pq2.mo30203a();
                        if (a != null) {
                            java.util.Iterator<T> it2 = a.iterator();
                            while (true) {
                                if (!it2.hasNext()) {
                                    t4 = null;
                                    break;
                                }
                                t4 = it2.next();
                                if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.fossil.blesdk.obfuscated.lq2) t4).mo28062a(), (java.lang.Object) "SWITCH HEART RATE MODE")) {
                                    break;
                                }
                            }
                            lq2 = (com.fossil.blesdk.obfuscated.lq2) t4;
                        }
                    }
                    com.fossil.blesdk.obfuscated.oq2 oq2 = (com.fossil.blesdk.obfuscated.oq2) lq2;
                    if (oq2 != null) {
                        oq2.mo29946b(this.f21752a.mo40248z().name());
                    }
                    this.f21752a.mo40241s().notifyDataSetChanged();
                } else {
                    com.portfolio.platform.p007ui.debug.DebugActivity debugActivity = this.f21752a;
                    com.misfit.frameworks.buttonservice.enums.HeartRateMode d = debugActivity.mo40231C().mo27027d(com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e());
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) d, "mSharedPreferencesManage\u2026tance.activeDeviceSerial)");
                    debugActivity.mo40234a(d);
                }
                this.f21752a.mo40205h();
            } else if (communicateMode == com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_FRONT_LIGHT_ENABLE) {
                if (intent.getIntExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.f21752a.mo40231C().mo27040g(this.f21752a.mo40229A());
                } else {
                    com.portfolio.platform.p007ui.debug.DebugActivity debugActivity2 = this.f21752a;
                    debugActivity2.mo40240d(debugActivity2.mo40231C().mo27056l(com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e()));
                    java.util.Iterator<T> it3 = this.f21752a.mo40243u().iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it3.next();
                        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.fossil.blesdk.obfuscated.pq2) t).mo30205b(), (java.lang.Object) "OTHER")) {
                            break;
                        }
                    }
                    com.fossil.blesdk.obfuscated.pq2 pq22 = (com.fossil.blesdk.obfuscated.pq2) t;
                    if (pq22 != null) {
                        java.util.ArrayList<com.fossil.blesdk.obfuscated.lq2> a2 = pq22.mo30203a();
                        if (a2 != null) {
                            java.util.Iterator<T> it4 = a2.iterator();
                            while (true) {
                                if (!it4.hasNext()) {
                                    t2 = null;
                                    break;
                                }
                                t2 = it4.next();
                                if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.fossil.blesdk.obfuscated.lq2) t2).mo28062a(), (java.lang.Object) "FRONT LIGHT ENABLE")) {
                                    break;
                                }
                            }
                            lq2 = (com.fossil.blesdk.obfuscated.lq2) t2;
                        }
                    }
                    com.fossil.blesdk.obfuscated.nq2 nq2 = (com.fossil.blesdk.obfuscated.nq2) lq2;
                    if (nq2 != null) {
                        nq2.mo29692a(this.f21752a.mo40229A());
                    }
                    this.f21752a.mo40241s().notifyDataSetChanged();
                }
                this.f21752a.mo40205h();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$d")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$d */
    public static final class C6112d<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.portfolio.platform.data.model.Firmware> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.debug.DebugActivity f21753a;

        @DexIgnore
        public C6112d(com.portfolio.platform.p007ui.debug.DebugActivity debugActivity) {
            this.f21753a = debugActivity;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(com.portfolio.platform.data.model.Firmware firmware) {
            com.fossil.blesdk.obfuscated.lq2 lq2;
            T t;
            T t2;
            if (firmware != null) {
                java.util.Iterator<T> it = this.f21753a.mo40243u().iterator();
                while (true) {
                    lq2 = null;
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.fossil.blesdk.obfuscated.pq2) t).mo30205b(), (java.lang.Object) "OTHER")) {
                        break;
                    }
                }
                com.fossil.blesdk.obfuscated.pq2 pq2 = (com.fossil.blesdk.obfuscated.pq2) t;
                if (pq2 != null) {
                    java.util.ArrayList<com.fossil.blesdk.obfuscated.lq2> a = pq2.mo30203a();
                    if (a != null) {
                        java.util.Iterator<T> it2 = a.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                t2 = null;
                                break;
                            }
                            t2 = it2.next();
                            if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.fossil.blesdk.obfuscated.lq2) t2).mo28062a(), (java.lang.Object) "CONSIDER AS LATEST BUNDLE FIRMWARE")) {
                                break;
                            }
                        }
                        lq2 = (com.fossil.blesdk.obfuscated.lq2) t2;
                    }
                }
                if (lq2 != null) {
                    lq2.mo29253a("CONSIDER AS LATEST BUNDLE FIRMWARE: " + firmware.getVersionNumber());
                }
                this.f21753a.mo40241s().notifyDataSetChanged();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$e")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$e */
    public static final class C6113e<T> implements com.fossil.blesdk.obfuscated.C1548cc<java.util.List<? extends com.portfolio.platform.data.model.DebugFirmwareData>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.debug.DebugActivity f21754a;

        @DexIgnore
        public C6113e(com.portfolio.platform.p007ui.debug.DebugActivity debugActivity) {
            this.f21754a = debugActivity;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(java.util.List<com.portfolio.platform.data.model.DebugFirmwareData> list) {
            T t;
            if (list != null && (!list.isEmpty())) {
                java.util.Iterator<T> it = this.f21754a.mo40243u().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.fossil.blesdk.obfuscated.pq2) t).mo30205b(), (java.lang.Object) "FIRMWARE")) {
                        break;
                    }
                }
                com.fossil.blesdk.obfuscated.pq2 pq2 = (com.fossil.blesdk.obfuscated.pq2) t;
                if (pq2 != null) {
                    java.util.ArrayList<com.fossil.blesdk.obfuscated.lq2> a = pq2.mo30203a();
                    if (a != null) {
                        a.clear();
                    }
                }
                for (com.portfolio.platform.data.model.DebugFirmwareData debugFirmwareData : list) {
                    com.portfolio.platform.data.model.Firmware firmware = debugFirmwareData.getFirmware();
                    int state = debugFirmwareData.getState();
                    java.lang.String str = state != 1 ? state != 2 ? "" : "Downloaded" : "Downloading";
                    java.lang.String versionNumber = firmware.getVersionNumber();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) versionNumber, "firmware.versionNumber");
                    com.fossil.blesdk.obfuscated.oq2 oq2 = new com.fossil.blesdk.obfuscated.oq2("FIRMWARE", versionNumber, str);
                    oq2.mo28063a(debugFirmwareData);
                    if (pq2 != null) {
                        java.util.ArrayList<com.fossil.blesdk.obfuscated.lq2> a2 = pq2.mo30203a();
                        if (a2 != null) {
                            a2.add(oq2);
                        }
                    }
                }
                this.f21754a.mo40241s().notifyDataSetChanged();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$f")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$f */
    public static final class C6114f implements android.view.View.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.debug.DebugActivity f21755e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ android.widget.EditText f21756f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ android.widget.EditText f21757g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ /* synthetic */ android.widget.EditText f21758h;

        @DexIgnore
        /* renamed from: i */
        public /* final */ /* synthetic */ android.widget.EditText f21759i;

        @DexIgnore
        /* renamed from: j */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1523c0 f21760j;

        @DexIgnore
        public C6114f(com.portfolio.platform.p007ui.debug.DebugActivity debugActivity, android.widget.EditText editText, android.widget.EditText editText2, android.widget.EditText editText3, android.widget.EditText editText4, com.fossil.blesdk.obfuscated.C1523c0 c0Var) {
            this.f21755e = debugActivity;
            this.f21756f = editText;
            this.f21757g = editText2;
            this.f21758h = editText3;
            this.f21759i = editText4;
            this.f21760j = c0Var;
        }

        @DexIgnore
        public final void onClick(android.view.View view) {
            int i;
            int i2;
            int i3;
            int i4;
            android.widget.EditText editText = this.f21756f;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) editText, "etDelay");
            java.lang.String obj = editText.getText().toString();
            android.widget.EditText editText2 = this.f21757g;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) editText2, "etDuration");
            java.lang.String obj2 = editText2.getText().toString();
            android.widget.EditText editText3 = this.f21758h;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) editText3, "etRepeat");
            java.lang.String obj3 = editText3.getText().toString();
            android.widget.EditText editText4 = this.f21759i;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) editText4, "etDelayEachTime");
            java.lang.String obj4 = editText4.getText().toString();
            try {
                i = java.lang.Integer.parseInt(obj);
            } catch (java.lang.Exception unused) {
                i = 0;
            }
            try {
                i2 = java.lang.Integer.parseInt(obj2);
            } catch (java.lang.Exception unused2) {
                i2 = 0;
            }
            try {
                i3 = java.lang.Integer.parseInt(obj3);
            } catch (java.lang.Exception unused3) {
                i3 = 0;
            }
            try {
                i4 = java.lang.Integer.parseInt(obj4);
            } catch (java.lang.Exception unused4) {
                i4 = 0;
            }
            if (i > 65535 || i2 > 65535 || i3 > 65535 || i4 > 65535) {
                android.widget.Toast.makeText(this.f21755e, "Value should be in range [0, 65535]", 0).show();
                return;
            }
            com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34484a(i, i2, i3, i4);
            this.f21760j.dismiss();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$g")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$g */
    public static final class C6115g implements android.content.DialogInterface.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.debug.DebugActivity f21761e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ android.widget.EditText f21762f;

        @DexIgnore
        public C6115g(com.portfolio.platform.p007ui.debug.DebugActivity debugActivity, android.widget.EditText editText) {
            this.f21761e = debugActivity;
            this.f21762f = editText;
        }

        @DexIgnore
        public final void onClick(android.content.DialogInterface dialogInterface, int i) {
            java.lang.String obj = this.f21762f.getText().toString();
            if (obj != null) {
                java.lang.Integer valueOf = java.lang.Integer.valueOf(kotlin.text.StringsKt__StringsKt.m37091d(obj).toString());
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) valueOf, "Integer.valueOf(input.text.toString().trim())");
                int intValue = valueOf.intValue();
                this.f21761e.mo40231C().mo27029d(intValue);
                for (com.fossil.blesdk.obfuscated.pq2 pq2 : this.f21761e.mo40243u()) {
                    if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) pq2.mo30205b(), (java.lang.Object) "SIMULATION")) {
                        for (T next : pq2.mo30203a()) {
                            if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.fossil.blesdk.obfuscated.lq2) next).mo29254c(), (java.lang.Object) "TRIGGER LOW BATTERY EVENT")) {
                                if (next != null) {
                                    com.fossil.blesdk.obfuscated.pd4 pd4 = com.fossil.blesdk.obfuscated.pd4.f17594a;
                                    java.lang.Object[] objArr = {java.lang.Integer.valueOf(intValue)};
                                    java.lang.String format = java.lang.String.format("Battery level: %d", java.util.Arrays.copyOf(objArr, objArr.length));
                                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) format, "java.lang.String.format(format, *args)");
                                    ((com.fossil.blesdk.obfuscated.oq2) next).mo29946b(format);
                                    this.f21761e.mo40241s().notifyDataSetChanged();
                                    return;
                                }
                                throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithText");
                            }
                        }
                        throw new java.util.NoSuchElementException("Collection contains no element matching the predicate.");
                    }
                }
                throw new java.util.NoSuchElementException("Collection contains no element matching the predicate.");
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$h")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$h */
    public static final class C6116h implements android.text.TextWatcher {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1523c0 f21763e;

        @DexIgnore
        public C6116h(com.fossil.blesdk.obfuscated.C1523c0 c0Var) {
            this.f21763e = c0Var;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0065, code lost:
            if (com.fossil.blesdk.obfuscated.kd4.m24401a(java.lang.Integer.valueOf(kotlin.text.StringsKt__StringsKt.m37091d(r6).toString()).intValue(), 100) <= 0) goto L_0x006f;
         */
        @DexIgnore
        public void afterTextChanged(android.text.Editable editable) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(editable, "s");
            boolean z = true;
            if (editable.length() == 0) {
                android.widget.Button b = this.f21763e.mo9356b(-1);
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) b, "dialog.getButton(AlertDialog.BUTTON_POSITIVE)");
                b.setEnabled(false);
                return;
            }
            android.widget.Button b2 = this.f21763e.mo9356b(-1);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) b2, "dialog.getButton(AlertDialog.BUTTON_POSITIVE)");
            java.lang.String obj = editable.toString();
            if (obj != null) {
                if (com.fossil.blesdk.obfuscated.kd4.m24401a(java.lang.Integer.valueOf(kotlin.text.StringsKt__StringsKt.m37091d(obj).toString()).intValue(), 1) >= 0) {
                    java.lang.String obj2 = editable.toString();
                    if (obj2 == null) {
                        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
                z = false;
                b2.setEnabled(z);
                return;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }

        @DexIgnore
        public void beforeTextChanged(java.lang.CharSequence charSequence, int i, int i2, int i3) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(java.lang.CharSequence charSequence, int i, int i2, int i3) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(charSequence, "s");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$i")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$i */
    public static final class C6117i implements android.content.DialogInterface.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public static /* final */ com.portfolio.platform.p007ui.debug.DebugActivity.C6117i f21764e; // = new com.portfolio.platform.p007ui.debug.DebugActivity.C6117i();

        @DexIgnore
        public final void onClick(android.content.DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$j")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$j */
    public static final class C6118j implements android.content.DialogInterface.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.debug.DebugActivity f21765e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.portfolio.platform.data.model.Firmware f21766f;

        @DexIgnore
        public C6118j(com.portfolio.platform.p007ui.debug.DebugActivity debugActivity, com.portfolio.platform.data.model.Firmware firmware) {
            this.f21765e = debugActivity;
            this.f21766f = firmware;
        }

        @DexIgnore
        public final void onClick(android.content.DialogInterface dialogInterface, int i) {
            this.f21765e.mo40236a(this.f21766f);
            dialogInterface.dismiss();
            this.f21765e.finish();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$k")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$k */
    public static final class C6119k implements android.content.DialogInterface.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public static /* final */ com.portfolio.platform.p007ui.debug.DebugActivity.C6119k f21767e; // = new com.portfolio.platform.p007ui.debug.DebugActivity.C6119k();

        @DexIgnore
        public final void onClick(android.content.DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$l")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$l */
    public static final class C6120l implements android.content.DialogInterface.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.debug.DebugActivity f21768e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.portfolio.platform.data.model.Firmware f21769f;

        @DexIgnore
        public C6120l(com.portfolio.platform.p007ui.debug.DebugActivity debugActivity, com.portfolio.platform.data.model.Firmware firmware) {
            this.f21768e = debugActivity;
            this.f21769f = firmware;
        }

        @DexIgnore
        public final void onClick(android.content.DialogInterface dialogInterface, int i) {
            this.f21768e.mo40231C().mo26996a(this.f21769f, this.f21768e.f21746L);
            com.portfolio.platform.p007ui.debug.DebugActivity.m32731b(this.f21768e).mo42928a(this.f21769f);
            dialogInterface.dismiss();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$m")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$m */
    public static final class C6121m implements android.content.DialogInterface.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public static /* final */ com.portfolio.platform.p007ui.debug.DebugActivity.C6121m f21770e; // = new com.portfolio.platform.p007ui.debug.DebugActivity.C6121m();

        @DexIgnore
        public final void onClick(android.content.DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static final /* synthetic */ com.portfolio.platform.viewmodel.FirmwareDebugViewModel m32731b(com.portfolio.platform.p007ui.debug.DebugActivity debugActivity) {
        com.portfolio.platform.viewmodel.FirmwareDebugViewModel firmwareDebugViewModel = debugActivity.f21745K;
        if (firmwareDebugViewModel != null) {
            return firmwareDebugViewModel;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mFirmwareViewModel");
        throw null;
    }

    @DexIgnore
    /* renamed from: A */
    public final boolean mo40229A() {
        return this.f21748N;
    }

    @DexIgnore
    /* renamed from: B */
    public final com.portfolio.platform.service.ShakeFeedbackService mo40230B() {
        com.portfolio.platform.service.ShakeFeedbackService shakeFeedbackService = this.f21741G;
        if (shakeFeedbackService != null) {
            return shakeFeedbackService;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mShakeFeedbackService");
        throw null;
    }

    @DexIgnore
    /* renamed from: C */
    public final com.fossil.blesdk.obfuscated.en2 mo40231C() {
        com.fossil.blesdk.obfuscated.en2 en2 = this.f21736B;
        if (en2 != null) {
            return en2;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo40239c(com.portfolio.platform.data.model.Firmware firmware) {
        com.fossil.blesdk.obfuscated.C1523c0.C1524a aVar = new com.fossil.blesdk.obfuscated.C1523c0.C1524a(this);
        aVar.mo9371b((java.lang.CharSequence) "Confirm Latest Firmware");
        aVar.mo9365a((java.lang.CharSequence) "Are you sure you want to use firmware " + firmware.getVersionNumber() + " as the bundle latest firmware?");
        aVar.mo9372b("Confirm", new com.portfolio.platform.p007ui.debug.DebugActivity.C6120l(this, firmware));
        aVar.mo9366a((java.lang.CharSequence) "Cancel", (android.content.DialogInterface.OnClickListener) com.portfolio.platform.p007ui.debug.DebugActivity.C6121m.f21770e);
        aVar.mo9368a();
        aVar.mo9373c();
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo40240d(boolean z) {
        this.f21748N = z;
    }

    @DexIgnore
    public void onCreate(android.os.Bundle bundle) {
        super.onCreate(bundle);
        setContentView(com.fossil.wearables.fossil.R.layout.activity_debug);
        com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34538g().mo29084a(this);
        java.lang.String e = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e();
        com.fossil.blesdk.obfuscated.en2 en2 = this.f21736B;
        if (en2 != null) {
            com.misfit.frameworks.buttonservice.enums.HeartRateMode d = en2.mo27027d(e);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) d, "mSharedPreferencesManage\u2026eMode(activeDeviceSerial)");
            this.f21747M = d;
            com.fossil.blesdk.obfuscated.en2 en22 = this.f21736B;
            if (en22 != null) {
                this.f21748N = en22.mo27056l(e);
                java.util.ArrayList arrayList = new java.util.ArrayList();
                arrayList.add(new com.fossil.blesdk.obfuscated.lq2("VIEW LOG", "VIEW LOG"));
                arrayList.add(new com.fossil.blesdk.obfuscated.lq2("SEND LOG", "SEND LOG"));
                arrayList.add(new com.fossil.blesdk.obfuscated.lq2("RESET UAPP LOG FILES", "RESET UAPP LOG FILES"));
                java.util.ArrayList<com.fossil.blesdk.obfuscated.pq2> arrayList2 = this.f21744J;
                com.fossil.blesdk.obfuscated.pq2 pq2 = new com.fossil.blesdk.obfuscated.pq2("LOG", "LOG", arrayList, false, 8, (com.fossil.blesdk.obfuscated.fd4) null);
                arrayList2.add(pq2);
                java.util.ArrayList arrayList3 = new java.util.ArrayList();
                arrayList3.add(new com.fossil.blesdk.obfuscated.lq2("CLEAR DATA", "CLEAR DATA"));
                arrayList3.add(new com.fossil.blesdk.obfuscated.lq2("GENERATE PRESET DATA", "GENERATE PRESET DATA"));
                java.util.ArrayList<com.fossil.blesdk.obfuscated.pq2> arrayList4 = this.f21744J;
                com.fossil.blesdk.obfuscated.pq2 pq22 = new com.fossil.blesdk.obfuscated.pq2("DATA", "DATA", arrayList3, false, 8, (com.fossil.blesdk.obfuscated.fd4) null);
                arrayList4.add(pq22);
                java.util.ArrayList arrayList5 = new java.util.ArrayList();
                arrayList5.add(new com.fossil.blesdk.obfuscated.lq2("BLUETOOTH SETTING", "BLUETOOTH SETTING"));
                java.util.ArrayList<com.fossil.blesdk.obfuscated.pq2> arrayList6 = this.f21744J;
                com.fossil.blesdk.obfuscated.pq2 pq23 = new com.fossil.blesdk.obfuscated.pq2("ANDROID SETTINGS", "ANDROID SETTINGS", arrayList5, false, 8, (com.fossil.blesdk.obfuscated.fd4) null);
                arrayList6.add(pq23);
                java.util.ArrayList arrayList7 = new java.util.ArrayList();
                arrayList7.add(new com.fossil.blesdk.obfuscated.lq2("START MINDFULNESS PRACTICE", "START MINDFULNESS PRACTICE"));
                arrayList7.add(new com.fossil.blesdk.obfuscated.lq2("WATCH APP MUSIC CONTROL", "WATCH APP MUSIC CONTROL"));
                arrayList7.add(new com.fossil.blesdk.obfuscated.lq2("SIMULATE DISCONNECTION", "SIMULATE DISCONNECTION"));
                com.fossil.blesdk.obfuscated.pd4 pd4 = com.fossil.blesdk.obfuscated.pd4.f17594a;
                java.lang.Object[] objArr = new java.lang.Object[1];
                com.fossil.blesdk.obfuscated.en2 en23 = this.f21736B;
                if (en23 != null) {
                    objArr[0] = java.lang.Integer.valueOf(en23.mo27087w());
                    java.lang.String format = java.lang.String.format("Battery level: %d", java.util.Arrays.copyOf(objArr, objArr.length));
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) format, "java.lang.String.format(format, *args)");
                    arrayList7.add(new com.fossil.blesdk.obfuscated.oq2("TRIGGER LOW BATTERY EVENT", "TRIGGER LOW BATTERY EVENT", format));
                    java.util.ArrayList<com.fossil.blesdk.obfuscated.pq2> arrayList8 = this.f21744J;
                    com.fossil.blesdk.obfuscated.pq2 pq24 = new com.fossil.blesdk.obfuscated.pq2("SIMULATION", "SIMULATION", arrayList7, false, 8, (com.fossil.blesdk.obfuscated.fd4) null);
                    arrayList8.add(pq24);
                    java.util.ArrayList arrayList9 = new java.util.ArrayList();
                    arrayList9.add(new com.fossil.blesdk.obfuscated.lq2("FIRMWARE_LOADING", "FIRMWARE_LOADING"));
                    java.util.ArrayList<com.fossil.blesdk.obfuscated.pq2> arrayList10 = this.f21744J;
                    com.fossil.blesdk.obfuscated.pq2 pq25 = new com.fossil.blesdk.obfuscated.pq2("FIRMWARE", "FIRMWARE", arrayList9, false, 8, (com.fossil.blesdk.obfuscated.fd4) null);
                    arrayList10.add(pq25);
                    java.util.ArrayList arrayList11 = new java.util.ArrayList();
                    arrayList11.add(new com.fossil.blesdk.obfuscated.lq2("SYNC", "SYNC"));
                    java.util.List<com.portfolio.platform.data.model.DebugForceBackgroundRequestData> d2 = com.fossil.blesdk.obfuscated.cb4.m20544d(new com.portfolio.platform.data.model.DebugForceBackgroundRequestData("Notification Filter Background", com.misfit.frameworks.buttonservice.model.customrequest.ForceBackgroundRequest.BackgroundRequestType.SET_NOTIFICATION_FILTER), new com.portfolio.platform.data.model.DebugForceBackgroundRequestData("Alarms Background", com.misfit.frameworks.buttonservice.model.customrequest.ForceBackgroundRequest.BackgroundRequestType.SET_MULTI_ALARM), new com.portfolio.platform.data.model.DebugForceBackgroundRequestData("Device Configuration Background", com.misfit.frameworks.buttonservice.model.customrequest.ForceBackgroundRequest.BackgroundRequestType.SET_CONFIG_FILE));
                    java.util.ArrayList arrayList12 = new java.util.ArrayList(com.fossil.blesdk.obfuscated.db4.m20831a(d2, 10));
                    for (com.portfolio.platform.data.model.DebugForceBackgroundRequestData visibleName : d2) {
                        arrayList12.add(visibleName.getVisibleName());
                    }
                    com.fossil.blesdk.obfuscated.mq2 mq2 = new com.fossil.blesdk.obfuscated.mq2("FORCE_BACKGROUND_REQUEST", "FORCE_BACKGROUND_REQUEST", com.fossil.blesdk.obfuscated.kb4.m24381d(arrayList12), "Send");
                    mq2.mo28063a(d2);
                    com.fossil.blesdk.obfuscated.qa4 qa4 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                    arrayList11.add(mq2);
                    arrayList11.add(new com.fossil.blesdk.obfuscated.lq2("WEATHER_WATCH_APP_TAP", "WEATHER_WATCH_APP_TAP"));
                    arrayList11.add(new com.fossil.blesdk.obfuscated.lq2("RESET DEVICE SETTING IN FIRMWARE TO DEFAULT", "RESET DEVICE SETTING IN FIRMWARE TO DEFAULT"));
                    arrayList11.add(new com.fossil.blesdk.obfuscated.lq2("RESET_DELAY_OTA_TIMESTAMP", "RESET_DELAY_OTA_TIMESTAMP"));
                    com.fossil.blesdk.obfuscated.en2 en24 = this.f21736B;
                    if (en24 != null) {
                        arrayList11.add(new com.fossil.blesdk.obfuscated.nq2("SKIP OTA", "SKIP OTA", en24.mo26982T()));
                        com.fossil.blesdk.obfuscated.en2 en25 = this.f21736B;
                        if (en25 != null) {
                            arrayList11.add(new com.fossil.blesdk.obfuscated.nq2("SHOW ALL DEVICES", "SHOW ALL DEVICES", en25.mo26981S()));
                            com.fossil.blesdk.obfuscated.en2 en26 = this.f21736B;
                            if (en26 != null) {
                                arrayList11.add(new com.fossil.blesdk.obfuscated.nq2("DISABLE HW_LOG SYNC", "DISABLE HW_LOG SYNC", !en26.mo26971I()));
                                com.fossil.blesdk.obfuscated.en2 en27 = this.f21736B;
                                if (en27 != null) {
                                    arrayList11.add(new com.fossil.blesdk.obfuscated.nq2("DISABLE AUTO SYNC", "DISABLE AUTO SYNC", !en27.mo26964B()));
                                    com.fossil.blesdk.obfuscated.en2 en28 = this.f21736B;
                                    if (en28 != null) {
                                        arrayList11.add(new com.fossil.blesdk.obfuscated.nq2("SHOW DISPLAY DEVICE INFO", "SHOW DISPLAY DEVICE INFO", en28.mo26972J()));
                                        com.fossil.blesdk.obfuscated.en2 en29 = this.f21736B;
                                        if (en29 != null) {
                                            arrayList11.add(new com.fossil.blesdk.obfuscated.nq2("CONSIDER AS LATEST BUNDLE FIRMWARE", "CONSIDER AS LATEST BUNDLE FIRMWARE:No Firmware", en29.mo26965C()));
                                            com.fossil.blesdk.obfuscated.en2 en210 = this.f21736B;
                                            if (en210 != null) {
                                                arrayList11.add(new com.fossil.blesdk.obfuscated.nq2("APPLY NEW NOTIFICATION FILTER", "APPLY NEW NOTIFICATION FILTER", en210.mo26975M()));
                                                if (com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(e)) {
                                                    arrayList11.add(new com.fossil.blesdk.obfuscated.nq2("FRONT LIGHT ENABLE", "FRONT LIGHT ENABLE", this.f21748N));
                                                    com.misfit.frameworks.buttonservice.enums.HeartRateMode heartRateMode = this.f21747M;
                                                    if (heartRateMode != null) {
                                                        arrayList11.add(new com.fossil.blesdk.obfuscated.oq2("SWITCH HEART RATE MODE", "SWITCH HEART RATE MODE", heartRateMode.name()));
                                                    } else {
                                                        com.fossil.blesdk.obfuscated.kd4.m24414d("mHeartRateMode");
                                                        throw null;
                                                    }
                                                }
                                                this.f21744J.add(new com.fossil.blesdk.obfuscated.pq2("OTHER", "OTHER", arrayList11, true));
                                                com.fossil.blesdk.obfuscated.jq2 jq2 = this.f21743I;
                                                jq2.mo28652a(this.f21744J);
                                                jq2.mo28651a((com.fossil.blesdk.obfuscated.jq2.C4553b) this);
                                                com.fossil.blesdk.obfuscated.qa4 qa42 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                                                androidx.recyclerview.widget.RecyclerView recyclerView = (androidx.recyclerview.widget.RecyclerView) findViewById(com.fossil.wearables.fossil.R.id.rv_debug_parent);
                                                recyclerView.setLayoutManager(new androidx.recyclerview.widget.LinearLayoutManager(getBaseContext()));
                                                recyclerView.setAdapter(this.f21743I);
                                                androidx.recyclerview.widget.RecyclerView.C0227g adapter = recyclerView.getAdapter();
                                                if (adapter != null) {
                                                    adapter.notifyDataSetChanged();
                                                    com.fossil.blesdk.obfuscated.qa4 qa43 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                                                    com.fossil.blesdk.obfuscated.C2010ic a = com.fossil.blesdk.obfuscated.C2306lc.m10130a((androidx.fragment.app.FragmentActivity) this).mo12710a(com.portfolio.platform.viewmodel.FirmwareDebugViewModel.class);
                                                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "ViewModelProviders.of(th\u2026bugViewModel::class.java)");
                                                    this.f21745K = (com.portfolio.platform.viewmodel.FirmwareDebugViewModel) a;
                                                    com.portfolio.platform.p007ui.debug.DebugActivity.C6113e eVar = new com.portfolio.platform.p007ui.debug.DebugActivity.C6113e(this);
                                                    com.portfolio.platform.viewmodel.FirmwareDebugViewModel firmwareDebugViewModel = this.f21745K;
                                                    if (firmwareDebugViewModel != null) {
                                                        firmwareDebugViewModel.mo42929c().mo2277a(this, eVar);
                                                        com.portfolio.platform.p007ui.debug.DebugActivity.C6112d dVar = new com.portfolio.platform.p007ui.debug.DebugActivity.C6112d(this);
                                                        com.portfolio.platform.viewmodel.FirmwareDebugViewModel firmwareDebugViewModel2 = this.f21745K;
                                                        if (firmwareDebugViewModel2 != null) {
                                                            firmwareDebugViewModel2.mo42930d().mo2277a(this, dVar);
                                                            if (!android.text.TextUtils.isEmpty(e)) {
                                                                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.debug.DebugActivity$onCreate$4(this, e, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                                                                return;
                                                            }
                                                            return;
                                                        }
                                                        com.fossil.blesdk.obfuscated.kd4.m24414d("mFirmwareViewModel");
                                                        throw null;
                                                    }
                                                    com.fossil.blesdk.obfuscated.kd4.m24414d("mFirmwareViewModel");
                                                    throw null;
                                                }
                                                com.fossil.blesdk.obfuscated.kd4.m24405a();
                                                throw null;
                                            }
                                            com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                                            throw null;
                                        }
                                        com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                                        throw null;
                                    }
                                    com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                                    throw null;
                                }
                                com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                                throw null;
                            }
                            com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                            throw null;
                        }
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                        throw null;
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                    throw null;
                }
                com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                throw null;
            }
            com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
            throw null;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39775b((com.portfolio.platform.service.BleCommandResultManager.C5999b) this.f21749O, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_HEART_RATE_MODE, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_FRONT_LIGHT_ENABLE);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39771a((com.portfolio.platform.service.BleCommandResultManager.C5999b) this.f21749O, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_HEART_RATE_MODE, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_FRONT_LIGHT_ENABLE);
        com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39773a(com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_HEART_RATE_MODE, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_FRONT_LIGHT_ENABLE);
    }

    @DexIgnore
    /* renamed from: s */
    public final com.fossil.blesdk.obfuscated.jq2 mo40241s() {
        return this.f21743I;
    }

    @DexIgnore
    /* renamed from: t */
    public final java.util.LinkedList<kotlin.Pair<java.lang.String, java.lang.Long>> mo40242t() {
        java.util.Calendar instance = java.util.Calendar.getInstance();
        java.util.LinkedList<kotlin.Pair<java.lang.String, java.lang.Long>> linkedList = new java.util.LinkedList<>();
        for (int i = 0; i <= 31; i++) {
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "calendar");
            java.util.Date time = instance.getTime();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) time, "calendar.time");
            linkedList.add(mo40233a(time));
            instance.add(5, -1);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String f = mo40202f();
            local.mo33255d(f, "getListDay - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    /* renamed from: u */
    public final java.util.ArrayList<com.fossil.blesdk.obfuscated.pq2> mo40243u() {
        return this.f21744J;
    }

    @DexIgnore
    /* renamed from: v */
    public final java.util.LinkedList<kotlin.Pair<java.lang.String, java.lang.Long>> mo40244v() {
        java.util.Calendar instance = java.util.Calendar.getInstance();
        java.util.LinkedList<kotlin.Pair<java.lang.String, java.lang.Long>> linkedList = new java.util.LinkedList<>();
        for (int i = 0; i <= 12; i++) {
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "calendar");
            java.util.Date time = instance.getTime();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) time, "calendar.time");
            linkedList.add(mo40233a(time));
            instance.add(2, -1);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String f = mo40202f();
            local.mo33255d(f, "getListMonth - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    /* renamed from: w */
    public final java.util.LinkedList<kotlin.Pair<java.lang.String, java.lang.Long>> mo40245w() {
        java.util.Calendar instance = java.util.Calendar.getInstance();
        java.util.LinkedList<kotlin.Pair<java.lang.String, java.lang.Long>> linkedList = new java.util.LinkedList<>();
        for (int i = 0; i <= 52; i++) {
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "calendar");
            java.util.Date time = instance.getTime();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) time, "calendar.time");
            linkedList.add(mo40237b(time));
            instance.add(5, -7);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String f = mo40202f();
            local.mo33255d(f, "getListWeek - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    /* renamed from: x */
    public final com.misfit.frameworks.buttonservice.source.FirmwareFileRepository mo40246x() {
        com.misfit.frameworks.buttonservice.source.FirmwareFileRepository firmwareFileRepository = this.f21738D;
        if (firmwareFileRepository != null) {
            return firmwareFileRepository;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mFirmwareFileRepository");
        throw null;
    }

    @DexIgnore
    /* renamed from: y */
    public final com.portfolio.platform.data.source.remote.GuestApiService mo40247y() {
        com.portfolio.platform.data.source.remote.GuestApiService guestApiService = this.f21739E;
        if (guestApiService != null) {
            return guestApiService;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mGuestApiService");
        throw null;
    }

    @DexIgnore
    /* renamed from: z */
    public final com.misfit.frameworks.buttonservice.enums.HeartRateMode mo40248z() {
        com.misfit.frameworks.buttonservice.enums.HeartRateMode heartRateMode = this.f21747M;
        if (heartRateMode != null) {
            return heartRateMode;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mHeartRateMode");
        throw null;
    }

    @DexIgnore
    /* renamed from: b */
    public final kotlin.Pair<java.lang.String, java.lang.Long> mo40237b(java.util.Date date) {
        java.util.Calendar instance = java.util.Calendar.getInstance(java.util.Locale.US);
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "calendar");
        instance.setTime(date);
        instance.set(7, instance.getFirstDayOfWeek());
        int i = instance.get(5);
        instance.set(7, instance.getFirstDayOfWeek() + 6);
        int i2 = instance.get(5);
        return new kotlin.Pair<>(i + '-' + i2, java.lang.Long.valueOf(date.getTime()));
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40234a(com.misfit.frameworks.buttonservice.enums.HeartRateMode heartRateMode) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(heartRateMode, "<set-?>");
        this.f21747M = heartRateMode;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* renamed from: a */
    public final /* synthetic */ java.lang.Object mo40232a(java.lang.String str, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<com.portfolio.platform.data.model.DebugFirmwareData>> yb4) {
        com.portfolio.platform.p007ui.debug.DebugActivity$loadFirmware$1 debugActivity$loadFirmware$1;
        int i;
        java.util.ArrayList arrayList;
        java.lang.Object obj;
        com.portfolio.platform.p007ui.debug.DebugActivity debugActivity;
        com.fossil.blesdk.obfuscated.qo2 qo2;
        if (yb4 instanceof com.portfolio.platform.p007ui.debug.DebugActivity$loadFirmware$1) {
            debugActivity$loadFirmware$1 = (com.portfolio.platform.p007ui.debug.DebugActivity$loadFirmware$1) yb4;
            int i2 = debugActivity$loadFirmware$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                debugActivity$loadFirmware$1.label = i2 - Integer.MIN_VALUE;
                java.lang.Object obj2 = debugActivity$loadFirmware$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = debugActivity$loadFirmware$1.label;
                java.lang.String str2 = null;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj2);
                    if (str.length() > 0) {
                        arrayList = new java.util.ArrayList();
                        com.portfolio.platform.p007ui.debug.DebugActivity$loadFirmware$repoResponse$1 debugActivity$loadFirmware$repoResponse$1 = new com.portfolio.platform.p007ui.debug.DebugActivity$loadFirmware$repoResponse$1(this, str, (com.fossil.blesdk.obfuscated.yb4) null);
                        debugActivity$loadFirmware$1.L$0 = this;
                        debugActivity$loadFirmware$1.L$1 = str;
                        debugActivity$loadFirmware$1.L$2 = arrayList;
                        debugActivity$loadFirmware$1.label = 1;
                        obj = com.portfolio.platform.response.ResponseKt.m32232a(debugActivity$loadFirmware$repoResponse$1, debugActivity$loadFirmware$1);
                        if (obj == a) {
                            return a;
                        }
                        debugActivity = this;
                    } else {
                        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(mo40202f(), ".loadFirmware(), device model is empty. Return empty list");
                        return new java.util.ArrayList();
                    }
                } else if (i == 1) {
                    java.lang.String str3 = (java.lang.String) debugActivity$loadFirmware$1.L$1;
                    debugActivity = (com.portfolio.platform.p007ui.debug.DebugActivity) debugActivity$loadFirmware$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj2);
                    java.lang.Object obj3 = obj2;
                    arrayList = (java.util.ArrayList) debugActivity$loadFirmware$1.L$2;
                    obj = obj3;
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String f = debugActivity.mo40202f();
                local.mo33255d(f, ".loadFirmware(), response" + qo2);
                if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
                    com.portfolio.platform.data.source.remote.ApiResponse apiResponse = (com.portfolio.platform.data.source.remote.ApiResponse) ((com.fossil.blesdk.obfuscated.ro2) qo2).mo30673a();
                    java.util.List<com.portfolio.platform.data.model.Firmware> list = apiResponse != null ? apiResponse.get_items() : null;
                    if (list != null) {
                        for (com.portfolio.platform.data.model.Firmware firmware : list) {
                            com.misfit.frameworks.buttonservice.source.FirmwareFileRepository firmwareFileRepository = debugActivity.f21738D;
                            if (firmwareFileRepository != null) {
                                java.lang.String versionNumber = firmware.getVersionNumber();
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) versionNumber, "it.versionNumber");
                                java.lang.String checksum = firmware.getChecksum();
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) checksum, "it.checksum");
                                arrayList.add(new com.portfolio.platform.data.model.DebugFirmwareData(firmware, firmwareFileRepository.isDownloaded(versionNumber, checksum) ? 2 : 0));
                            } else {
                                com.fossil.blesdk.obfuscated.kd4.m24414d("mFirmwareFileRepository");
                                throw null;
                            }
                        }
                    }
                } else if (qo2 instanceof com.fossil.blesdk.obfuscated.po2) {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String f2 = debugActivity.mo40202f();
                    java.lang.StringBuilder sb = new java.lang.StringBuilder();
                    sb.append(".loadFirmware(), fail with code=");
                    com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
                    sb.append(po2.mo30176a());
                    sb.append(", message=");
                    com.portfolio.platform.data.model.ServerError c = po2.mo30178c();
                    if (c != null) {
                        str2 = c.getMessage();
                    }
                    sb.append(str2);
                    local2.mo33255d(f2, sb.toString());
                }
                return arrayList;
            }
        }
        debugActivity$loadFirmware$1 = new com.portfolio.platform.p007ui.debug.DebugActivity$loadFirmware$1(this, yb4);
        java.lang.Object obj22 = debugActivity$loadFirmware$1.result;
        java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = debugActivity$loadFirmware$1.label;
        java.lang.String str22 = null;
        if (i != 0) {
        }
        qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String f3 = debugActivity.mo40202f();
        local3.mo33255d(f3, ".loadFirmware(), response" + qo2);
        if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
        }
        return arrayList;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo28662b(java.lang.String str, int i, int i2, java.lang.Object obj, android.os.Bundle bundle) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "tagName");
        if (str.hashCode() == 227289531 && str.equals("FIRMWARE")) {
            com.portfolio.platform.data.model.DebugFirmwareData debugFirmwareData = (com.portfolio.platform.data.model.DebugFirmwareData) obj;
            if (debugFirmwareData != null) {
                com.misfit.frameworks.buttonservice.source.FirmwareFileRepository firmwareFileRepository = this.f21738D;
                if (firmwareFileRepository != null) {
                    java.lang.String versionNumber = debugFirmwareData.getFirmware().getVersionNumber();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) versionNumber, "it.firmware.versionNumber");
                    java.lang.String checksum = debugFirmwareData.getFirmware().getChecksum();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) checksum, "it.firmware.checksum");
                    if (firmwareFileRepository.isDownloaded(versionNumber, checksum)) {
                        mo40239c(debugFirmwareData.getFirmware());
                    } else {
                        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(mo40202f(), "Firmware hasn't been downloaded. Could not be set as latest firmware.");
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24414d("mFirmwareFileRepository");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo40238b(com.portfolio.platform.data.model.Firmware firmware) {
        com.fossil.blesdk.obfuscated.C1523c0.C1524a aVar = new com.fossil.blesdk.obfuscated.C1523c0.C1524a(this);
        aVar.mo9371b((java.lang.CharSequence) "Confirm OTA");
        aVar.mo9365a((java.lang.CharSequence) "Are you sure you want OTA to firmware " + firmware.getVersionNumber() + '?');
        aVar.mo9372b("Confirm", new com.portfolio.platform.p007ui.debug.DebugActivity.C6118j(this, firmware));
        aVar.mo9366a((java.lang.CharSequence) "Cancel", (android.content.DialogInterface.OnClickListener) com.portfolio.platform.p007ui.debug.DebugActivity.C6119k.f21767e);
        aVar.mo9368a();
        aVar.mo9373c();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo28661a(java.lang.String str, int i, int i2, java.lang.Object obj, android.os.Bundle bundle) {
        java.lang.String str2 = str;
        android.os.Bundle bundle2 = bundle;
        com.fossil.blesdk.obfuscated.kd4.m24411b(str2, "tagName");
        switch (str.hashCode()) {
            case -2017027426:
                if (str2.equals("BLUETOOTH SETTING")) {
                    android.content.Intent intent = new android.content.Intent();
                    intent.setAction("android.settings.BLUETOOTH_SETTINGS");
                    startActivity(intent);
                    com.fossil.blesdk.obfuscated.qa4 qa4 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                    return;
                }
                return;
            case -1935069302:
                if (str2.equals("WEATHER_WATCH_APP_TAP")) {
                    com.portfolio.platform.PortfolioApp.C5609a aVar = com.portfolio.platform.PortfolioApp.f20941W;
                    aVar.mo34583a((java.lang.Object) new com.fossil.blesdk.obfuscated.gj2(aVar.mo34589c().mo34532e(), com.fossil.blesdk.device.event.DeviceEventId.WEATHER_WATCH_APP.ordinal(), new android.os.Bundle()));
                    return;
                }
                return;
            case -1822588397:
                if (str2.equals("TRIGGER LOW BATTERY EVENT")) {
                    com.fossil.blesdk.obfuscated.C1523c0.C1524a aVar2 = new com.fossil.blesdk.obfuscated.C1523c0.C1524a(this);
                    android.widget.EditText editText = new android.widget.EditText(this);
                    editText.setLayoutParams(new android.widget.LinearLayout.LayoutParams(-1, -1));
                    editText.setInputType(2);
                    aVar2.mo9370b((android.view.View) editText);
                    aVar2.mo9371b((java.lang.CharSequence) "Custom dialog");
                    aVar2.mo9365a((java.lang.CharSequence) "Enter battery level between 1-100");
                    aVar2.mo9372b("Done", new com.portfolio.platform.p007ui.debug.DebugActivity.C6115g(this, editText));
                    com.fossil.blesdk.obfuscated.C1523c0 a = aVar2.mo9368a();
                    a.show();
                    android.widget.Button b = a.mo9356b(-1);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) b, "dialog.getButton(AlertDialog.BUTTON_POSITIVE)");
                    b.setEnabled(false);
                    editText.addTextChangedListener(new com.portfolio.platform.p007ui.debug.DebugActivity.C6116h(a));
                    return;
                }
                return;
            case -1367489317:
                if (str2.equals("SKIP OTA")) {
                    com.fossil.blesdk.obfuscated.en2 en2 = this.f21736B;
                    if (en2 != null) {
                        java.lang.Boolean valueOf = bundle2 != null ? java.lang.Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf != null) {
                            en2.mo27094y(valueOf.booleanValue());
                            return;
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -1337201641:
                if (str2.equals("SHOW DISPLAY DEVICE INFO")) {
                    com.fossil.blesdk.obfuscated.en2 en22 = this.f21736B;
                    if (en22 != null) {
                        java.lang.Boolean valueOf2 = bundle2 != null ? java.lang.Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf2 != null) {
                            en22.mo27068p(valueOf2.booleanValue());
                            com.fossil.blesdk.obfuscated.en2 en23 = this.f21736B;
                            if (en23 == null) {
                                com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                                throw null;
                            } else if (en23.mo26972J()) {
                                mo40185a();
                                return;
                            } else {
                                mo40212o();
                                return;
                            }
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -1276486892:
                if (str2.equals("DISABLE AUTO SYNC")) {
                    com.fossil.blesdk.obfuscated.en2 en24 = this.f21736B;
                    if (en24 != null) {
                        if ((bundle2 != null ? java.lang.Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null) != null) {
                            en24.mo27018b(!r1.booleanValue());
                            return;
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -907588345:
                if (str2.equals("CONSIDER AS LATEST BUNDLE FIRMWARE")) {
                    com.fossil.blesdk.obfuscated.en2 en25 = this.f21736B;
                    if (en25 != null) {
                        java.lang.Boolean valueOf3 = bundle2 != null ? java.lang.Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf3 != null) {
                            en25.mo27026c(valueOf3.booleanValue());
                            return;
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -833549689:
                if (str2.equals("RESET_DELAY_OTA_TIMESTAMP")) {
                    com.fossil.blesdk.obfuscated.en2 en26 = this.f21736B;
                    if (en26 != null) {
                        en26.mo26998a(com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e(), -1);
                        return;
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -558521180:
                if (str2.equals("FRONT LIGHT ENABLE") && bundle2 != null && bundle2.containsKey("DEBUG_BUNDLE_IS_CHECKED")) {
                    boolean z = bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false);
                    mo40215p();
                    if (com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34516b(com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e(), z) == com.misfit.frameworks.buttonservice.ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD()) {
                        mo40205h();
                        return;
                    }
                    return;
                }
                return;
            case -276828739:
                if (str2.equals("CLEAR DATA")) {
                    com.fossil.blesdk.obfuscated.ws3.C5362f fVar = new com.fossil.blesdk.obfuscated.ws3.C5362f(com.fossil.wearables.fossil.R.layout.dialog_confirm);
                    fVar.mo32132a((int) com.fossil.wearables.fossil.R.id.ftv_confirm_title, "ARE YOU SURE?");
                    fVar.mo32132a((int) com.fossil.wearables.fossil.R.id.ftv_confirm_desc, "** This will NOT happen on production **");
                    fVar.mo32132a((int) com.fossil.wearables.fossil.R.id.fb_ok, "OK");
                    fVar.mo32129a((int) com.fossil.wearables.fossil.R.id.fb_ok);
                    com.fossil.blesdk.obfuscated.ws3 a2 = fVar.mo32139a("CONFIRM_CLEAR_DATA");
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a2, "AlertDialogFragment.Buil\u2026Utils.CONFIRM_CLEAR_DATA)");
                    a2.setCancelable(true);
                    a2.setStyle(0, com.fossil.wearables.fossil.R.style.DialogNotFullScreen);
                    a2.show(getSupportFragmentManager(), "CONFIRM_CLEAR_DATA");
                    return;
                }
                return;
            case -75588064:
                if (str2.equals("GENERATE PRESET DATA")) {
                    mo40242t();
                    mo40245w();
                    mo40244v();
                    return;
                }
                return;
            case -62411653:
                if (str2.equals("SHOW ALL DEVICES")) {
                    com.fossil.blesdk.obfuscated.en2 en27 = this.f21736B;
                    if (en27 != null) {
                        java.lang.Boolean valueOf4 = bundle2 != null ? java.lang.Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf4 != null) {
                            en27.mo27092x(valueOf4.booleanValue());
                            com.portfolio.platform.helper.DeviceHelper.f21188o.mo39565e().mo39546a();
                            return;
                        }
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                    throw null;
                }
                return;
            case 2560667:
                if (str2.equals("SYNC")) {
                    com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
                    com.fossil.blesdk.obfuscated.vj2 vj2 = this.f21742H;
                    if (vj2 != null) {
                        c.mo34485a(vj2, false, 12);
                        return;
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mDeviceSettingFactory");
                        throw null;
                    }
                } else {
                    return;
                }
            case 227289531:
                if (str2.equals("FIRMWARE")) {
                    com.portfolio.platform.data.model.DebugFirmwareData debugFirmwareData = (com.portfolio.platform.data.model.DebugFirmwareData) obj;
                    if (debugFirmwareData != null) {
                        if (debugFirmwareData.getState() == 2) {
                            mo40238b(debugFirmwareData.getFirmware());
                        } else if (debugFirmwareData.getState() == 0) {
                            mo40235a(debugFirmwareData);
                        }
                        com.fossil.blesdk.obfuscated.qa4 qa42 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                        return;
                    }
                    return;
                }
                return;
            case 334768719:
                if (str2.equals("RESET DEVICE SETTING IN FIRMWARE TO DEFAULT")) {
                    com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34467N();
                    return;
                }
                return;
            case 437897810:
                if (str2.equals("FORCE_BACKGROUND_REQUEST") && bundle2 != null && bundle2.containsKey("DEBUG_BUNDLE_SPINNER_SELECTED_POS")) {
                    int i3 = bundle2.getInt("DEBUG_BUNDLE_SPINNER_SELECTED_POS");
                    java.util.List c2 = com.fossil.blesdk.obfuscated.qd4.m26936c(obj);
                    java.lang.Object obj2 = c2 != null ? c2.get(i3) : null;
                    if (!(obj2 instanceof com.portfolio.platform.data.model.DebugForceBackgroundRequestData)) {
                        obj2 = null;
                    }
                    com.portfolio.platform.data.model.DebugForceBackgroundRequestData debugForceBackgroundRequestData = (com.portfolio.platform.data.model.DebugForceBackgroundRequestData) obj2;
                    if (debugForceBackgroundRequestData != null) {
                        java.lang.Long.valueOf(com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34478a(com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e(), (com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest) new com.misfit.frameworks.buttonservice.model.customrequest.ForceBackgroundRequest(debugForceBackgroundRequestData.getBackgroundRequestType())));
                        return;
                    }
                    return;
                }
                return;
            case 596314607:
                if (str2.equals("DISABLE HW_LOG SYNC")) {
                    com.fossil.blesdk.obfuscated.en2 en28 = this.f21736B;
                    if (en28 != null) {
                        if ((bundle2 != null ? java.lang.Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null) != null) {
                            en28.mo27046i(!r1.booleanValue());
                            return;
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case 967694544:
                if (str2.equals("SIMULATE DISCONNECTION")) {
                    com.fossil.blesdk.obfuscated.C1523c0.C1524a aVar3 = new com.fossil.blesdk.obfuscated.C1523c0.C1524a(this);
                    java.lang.Object systemService = getSystemService("layout_inflater");
                    if (systemService != null) {
                        android.view.View inflate = ((android.view.LayoutInflater) systemService).inflate(com.fossil.wearables.fossil.R.layout.simulate_disconnection_input, (android.view.ViewGroup) null);
                        aVar3.mo9370b(inflate);
                        com.fossil.blesdk.obfuscated.C1523c0 a3 = aVar3.mo9368a();
                        a3.show();
                        android.widget.EditText editText2 = (android.widget.EditText) inflate.findViewById(com.fossil.wearables.fossil.R.id.et_delay);
                        android.widget.EditText editText3 = (android.widget.EditText) inflate.findViewById(com.fossil.wearables.fossil.R.id.et_duration);
                        android.widget.EditText editText4 = (android.widget.EditText) inflate.findViewById(com.fossil.wearables.fossil.R.id.et_repeat);
                        android.widget.EditText editText5 = (android.widget.EditText) inflate.findViewById(com.fossil.wearables.fossil.R.id.et_delay_each_time);
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) editText2, "etDelay");
                        double d = (double) 0;
                        double d2 = d;
                        double d3 = d;
                        double d4 = (double) 65535;
                        com.portfolio.platform.p007ui.debug.DebugActivity.C6110b bVar = new com.portfolio.platform.p007ui.debug.DebugActivity.C6110b(this, d2, d4);
                        editText2.setFilters(new android.text.InputFilter[]{bVar});
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) editText3, "etDuration");
                        double d5 = d3;
                        com.portfolio.platform.p007ui.debug.DebugActivity.C6110b bVar2 = new com.portfolio.platform.p007ui.debug.DebugActivity.C6110b(this, d5, d4);
                        editText3.setFilters(new android.text.InputFilter[]{bVar2});
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) editText4, "etRepeat");
                        com.portfolio.platform.p007ui.debug.DebugActivity.C6110b bVar3 = new com.portfolio.platform.p007ui.debug.DebugActivity.C6110b(this, d5, d4);
                        editText4.setFilters(new android.text.InputFilter[]{bVar3});
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) editText5, "etDelayEachTime");
                        com.portfolio.platform.p007ui.debug.DebugActivity.C6110b bVar4 = new com.portfolio.platform.p007ui.debug.DebugActivity.C6110b(this, d5, (double) 4294967);
                        editText5.setFilters(new android.text.InputFilter[]{bVar4});
                        com.portfolio.platform.p007ui.debug.DebugActivity.C6114f fVar2 = new com.portfolio.platform.p007ui.debug.DebugActivity.C6114f(this, editText2, editText3, editText4, editText5, a3);
                        ((com.portfolio.platform.view.FlexibleButton) inflate.findViewById(com.fossil.wearables.fossil.R.id.btn_done)).setOnClickListener(fVar2);
                        return;
                    }
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.LayoutInflater");
                }
                return;
            case 999402770:
                if (str2.equals("WATCH APP MUSIC CONTROL")) {
                    finish();
                    return;
                }
                return;
            case 1053818587:
                if (str2.equals("APPLY NEW NOTIFICATION FILTER")) {
                    com.fossil.blesdk.obfuscated.en2 en29 = this.f21736B;
                    if (en29 != null) {
                        java.lang.Boolean valueOf5 = bundle2 != null ? java.lang.Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf5 != null) {
                            en29.mo27077s(valueOf5.booleanValue());
                            return;
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case 1484036056:
                if (str2.equals("RESET UAPP LOG FILES")) {
                    com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger.resetLogFiles();
                    return;
                }
                return;
            case 1642625733:
                if (str2.equals("GENERATE HEART RATE DATA")) {
                    finish();
                    return;
                }
                return;
            case 1977879337:
                if (str2.equals("VIEW LOG")) {
                    com.portfolio.platform.p007ui.debug.LogcatActivity.m32763a((android.content.Context) this);
                    return;
                }
                return;
            case 2029483660:
                if (str2.equals("SEND LOG")) {
                    com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25691a()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.debug.DebugActivity$onItemClicked$1(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                    return;
                }
                return;
            default:
                return;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final kotlin.Pair<java.lang.String, java.lang.Long> mo40233a(java.util.Date date) {
        java.util.Calendar instance = java.util.Calendar.getInstance();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "calendar");
        instance.setTime(date);
        long timeInMillis = instance.getTimeInMillis();
        switch (instance.get(7)) {
            case 1:
                return new kotlin.Pair<>(com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.DashboardHybrid_Main_Steps7days_Label__S), java.lang.Long.valueOf(timeInMillis));
            case 2:
                return new kotlin.Pair<>(com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.DashboardHybrid_Main_Steps7days_Label__M), java.lang.Long.valueOf(timeInMillis));
            case 3:
                return new kotlin.Pair<>(com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.DashboardHybrid_Main_Steps7days_Label__T), java.lang.Long.valueOf(timeInMillis));
            case 4:
                return new kotlin.Pair<>(com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.DashboardHybrid_Main_Steps7days_Label__W), java.lang.Long.valueOf(timeInMillis));
            case 5:
                return new kotlin.Pair<>(com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.DashboardHybrid_Main_Steps7days_Label__T_1), java.lang.Long.valueOf(timeInMillis));
            case 6:
                return new kotlin.Pair<>(com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.DashboardHybrid_Main_Steps7days_Label__F), java.lang.Long.valueOf(timeInMillis));
            case 7:
                return new kotlin.Pair<>(com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.DashboardHybrid_Main_Steps7days_Label__S_1), java.lang.Long.valueOf(timeInMillis));
            default:
                return new kotlin.Pair<>(com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.DashboardHybrid_Main_Steps7days_Label__S), java.lang.Long.valueOf(timeInMillis));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40235a(com.portfolio.platform.data.model.DebugFirmwareData debugFirmwareData) {
        com.fossil.blesdk.obfuscated.C1523c0.C1524a aVar = new com.fossil.blesdk.obfuscated.C1523c0.C1524a(this);
        com.portfolio.platform.data.model.Firmware firmware = debugFirmwareData.getFirmware();
        aVar.mo9371b((java.lang.CharSequence) "Confirm Download");
        aVar.mo9365a((java.lang.CharSequence) "Are you sure you want download to firmware " + firmware.getVersionNumber() + '?');
        aVar.mo9372b("Confirm", new com.portfolio.platform.p007ui.debug.C6122x36bc0f7f(firmware, this, debugFirmwareData));
        aVar.mo9366a((java.lang.CharSequence) "Cancel", (android.content.DialogInterface.OnClickListener) com.portfolio.platform.p007ui.debug.DebugActivity.C6117i.f21764e);
        aVar.mo9368a();
        aVar.mo9373c();
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40236a(com.portfolio.platform.data.model.Firmware firmware) {
        java.lang.String e = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e();
        com.misfit.frameworks.buttonservice.model.FirmwareData createFirmwareData = com.misfit.frameworks.buttonservice.model.FirmwareFactory.getInstance().createFirmwareData(firmware.getVersionNumber(), firmware.getDeviceModel(), firmware.getChecksum());
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) createFirmwareData, "FirmwareFactory.getInsta\u2026Model, firmware.checksum)");
        com.fossil.blesdk.obfuscated.cr2.C4081b bVar = new com.fossil.blesdk.obfuscated.cr2.C4081b(e, createFirmwareData);
        com.fossil.blesdk.obfuscated.cr2 cr2 = this.f21737C;
        if (cr2 != null) {
            cr2.mo34435a(bVar, (com.portfolio.platform.CoroutineUseCase.C5606e) null);
        } else {
            com.fossil.blesdk.obfuscated.kd4.m24414d("mOTAToSpecificFirmwareUseCase");
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo25776a(java.lang.String str, int i, android.content.Intent intent) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "tag");
        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) str, (java.lang.Object) "CONFIRM_CLEAR_DATA") && i == com.fossil.wearables.fossil.R.id.fb_ok) {
            com.portfolio.platform.p007ui.debug.DebugClearDataWarningActivity.m32762a(this);
        } else if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) str, (java.lang.Object) "SWITCH HEART RATE MODE") && i == com.fossil.wearables.fossil.R.id.fb_ok && intent != null && intent.hasExtra("EXTRA_RADIO_GROUPS_RESULTS")) {
            java.util.HashMap hashMap = (java.util.HashMap) intent.getSerializableExtra("EXTRA_RADIO_GROUPS_RESULTS");
        }
    }
}
