package com.portfolio.platform.p007ui.debug;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.ui.debug.DebugActivity$onCreate$4", mo27670f = "DebugActivity.kt", mo27671l = {}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.ui.debug.DebugActivity$onCreate$4 */
public final class DebugActivity$onCreate$4 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $activeDeviceSerial;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21776p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.debug.DebugActivity this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DebugActivity$onCreate$4(com.portfolio.platform.p007ui.debug.DebugActivity debugActivity, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = debugActivity;
        this.$activeDeviceSerial = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.p007ui.debug.DebugActivity$onCreate$4 debugActivity$onCreate$4 = new com.portfolio.platform.p007ui.debug.DebugActivity$onCreate$4(this.this$0, this.$activeDeviceSerial, yb4);
        debugActivity$onCreate$4.f21776p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return debugActivity$onCreate$4;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.p007ui.debug.DebugActivity$onCreate$4) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.model.Device deviceBySerial = this.this$0.mo40194b().getDeviceBySerial(this.$activeDeviceSerial);
            if (deviceBySerial != null) {
                com.portfolio.platform.p007ui.debug.DebugActivity debugActivity = this.this$0;
                java.lang.String sku = deviceBySerial.getSku();
                if (sku == null) {
                    sku = "";
                }
                debugActivity.f21746L = sku;
                com.portfolio.platform.data.model.Firmware a = this.this$0.mo40231C().mo26989a(this.this$0.f21746L);
                if (a != null) {
                    com.portfolio.platform.p007ui.debug.DebugActivity.m32731b(this.this$0).mo42928a(a);
                }
                if (!com.portfolio.platform.p007ui.debug.DebugActivity.m32731b(this.this$0).mo42931e()) {
                    com.portfolio.platform.p007ui.debug.DebugActivity.m32731b(this.this$0).mo42927a((com.fossil.blesdk.obfuscated.xc4<? super com.fossil.blesdk.obfuscated.yb4<? super java.util.List<com.portfolio.platform.data.model.DebugFirmwareData>>, ? extends java.lang.Object>) new com.portfolio.platform.p007ui.debug.DebugActivity$onCreate$4$invokeSuspend$$inlined$let$lambda$1((com.fossil.blesdk.obfuscated.yb4) null, this));
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
