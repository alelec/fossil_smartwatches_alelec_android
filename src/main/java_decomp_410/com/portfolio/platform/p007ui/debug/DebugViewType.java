package com.portfolio.platform.p007ui.debug;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.debug.DebugViewType */
public enum DebugViewType {
    CHILD,
    CHILD_ITEM_WITH_SWITCH,
    CHILD_ITEM_WITH_TEXT,
    CHILD_ITEM_WITH_SPINNER
}
