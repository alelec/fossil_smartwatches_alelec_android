package com.portfolio.platform.p007ui.debug;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.debug.LogcatActivity */
public class LogcatActivity extends com.portfolio.platform.p007ui.BaseActivity {

    @DexIgnore
    /* renamed from: E */
    public static boolean f21781E;

    @DexIgnore
    /* renamed from: B */
    public com.portfolio.platform.p007ui.debug.LogcatActivity.C6130c f21782B;

    @DexIgnore
    /* renamed from: C */
    public androidx.recyclerview.widget.RecyclerView f21783C;

    @DexIgnore
    /* renamed from: D */
    public android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> f21784D;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.LogcatActivity$a")
    /* renamed from: com.portfolio.platform.ui.debug.LogcatActivity$a */
    public class C6128a implements android.widget.AdapterView.OnItemSelectedListener {
        @DexIgnore
        public C6128a() {
        }

        @DexIgnore
        public void onItemSelected(android.widget.AdapterView<?> adapterView, android.view.View view, int i, long j) {
            com.portfolio.platform.p007ui.debug.LogcatActivity.this.mo40267a(i);
        }

        @DexIgnore
        public void onNothingSelected(android.widget.AdapterView<?> adapterView) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.LogcatActivity$b")
    /* renamed from: com.portfolio.platform.ui.debug.LogcatActivity$b */
    public static class C6129b extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f21786a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.portfolio.platform.p007ui.debug.LogcatActivity.C6130c f21787b;

        @DexIgnore
        public C6129b(int i, com.portfolio.platform.p007ui.debug.LogcatActivity.C6130c cVar) {
            this.f21786a = i;
            this.f21787b = cVar;
        }

        @DexIgnore
        /* renamed from: a */
        public java.lang.Void doInBackground(java.lang.Void... voidArr) {
            int i;
            java.lang.String str = "";
            try {
                if (this.f21786a == 0) {
                    str = "logcat -v time -d " + com.portfolio.platform.PortfolioApp.f20936R.getPackageName() + ":V";
                } else if (this.f21786a == 1) {
                    str = "logcat -v time -d ButtonService:V BaseProfile:V TrackerProfile:V LinkProfile:V *:S";
                }
                java.io.BufferedReader bufferedReader = new java.io.BufferedReader(new java.io.InputStreamReader(java.lang.Runtime.getRuntime().exec(str).getInputStream()));
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                loop0:
                while (true) {
                    i = 0;
                    do {
                        if (bufferedReader.readLine() == null) {
                            break loop0;
                        }
                        i++;
                        sb.insert(0, r3 + "<br>");
                    } while (i < 50);
                    this.f21787b.mo40275a(com.portfolio.platform.p007ui.debug.LogcatActivity.m32764c(sb.toString()));
                    sb.setLength(0);
                    sb = new java.lang.StringBuilder();
                }
                if (i > 0) {
                    this.f21787b.mo40275a(com.portfolio.platform.p007ui.debug.LogcatActivity.m32764c(sb.toString()));
                }
                java.lang.Runtime.getRuntime().exec("logcat -c");
                return null;
            } catch (java.lang.Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void onPostExecute(java.lang.Void voidR) {
            com.portfolio.platform.p007ui.debug.LogcatActivity.f21781E = false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.LogcatActivity$c")
    /* renamed from: com.portfolio.platform.ui.debug.LogcatActivity$c */
    public class C6130c extends androidx.recyclerview.widget.RecyclerView.C0227g<com.portfolio.platform.p007ui.debug.LogcatActivity.C6132d> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.List<java.lang.String> f21788a; // = new java.util.ArrayList();

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.LogcatActivity$c$a")
        /* renamed from: com.portfolio.platform.ui.debug.LogcatActivity$c$a */
        public class C6131a implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ java.lang.String f21790e;

            @DexIgnore
            public C6131a(java.lang.String str) {
                this.f21790e = str;
            }

            @DexIgnore
            public void run() {
                if (!com.portfolio.platform.p007ui.debug.LogcatActivity.C6130c.this.f21788a.contains(this.f21790e)) {
                    com.portfolio.platform.p007ui.debug.LogcatActivity.C6130c.this.f21788a.add(0, this.f21790e);
                    com.portfolio.platform.p007ui.debug.LogcatActivity.C6130c.this.notifyItemInserted(0);
                    com.portfolio.platform.p007ui.debug.LogcatActivity.this.f21783C.mo2616i(0);
                }
            }
        }

        @DexIgnore
        public C6130c() {
        }

        @DexIgnore
        /* renamed from: a */
        public void onBindViewHolder(com.portfolio.platform.p007ui.debug.LogcatActivity.C6132d dVar, int i) {
            java.lang.String str = this.f21788a.get(i);
            if (str != null && dVar != null) {
                dVar.f21792a.setText(android.text.Html.fromHtml(str));
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void mo40276b() {
            this.f21788a.clear();
            notifyDataSetChanged();
        }

        @DexIgnore
        public int getItemCount() {
            return this.f21788a.size();
        }

        @DexIgnore
        public com.portfolio.platform.p007ui.debug.LogcatActivity.C6132d onCreateViewHolder(android.view.ViewGroup viewGroup, int i) {
            return new com.portfolio.platform.p007ui.debug.LogcatActivity.C6132d(com.portfolio.platform.p007ui.debug.LogcatActivity.this, android.view.LayoutInflater.from(com.portfolio.platform.p007ui.debug.LogcatActivity.this).inflate(com.fossil.wearables.fossil.R.layout.logcat_item, viewGroup, false));
        }

        @DexIgnore
        /* renamed from: a */
        public void mo40275a(java.lang.String str) {
            if (!com.portfolio.platform.p007ui.debug.LogcatActivity.this.isFinishing() && !com.portfolio.platform.p007ui.debug.LogcatActivity.this.isDestroyed()) {
                com.portfolio.platform.p007ui.debug.LogcatActivity.this.runOnUiThread(new com.portfolio.platform.p007ui.debug.LogcatActivity.C6130c.C6131a(str));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.LogcatActivity$d")
    /* renamed from: com.portfolio.platform.ui.debug.LogcatActivity$d */
    public class C6132d extends androidx.recyclerview.widget.RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.widget.TextView f21792a;

        @DexIgnore
        public C6132d(com.portfolio.platform.p007ui.debug.LogcatActivity logcatActivity, android.view.View view) {
            super(view);
            this.f21792a = (android.widget.TextView) view.findViewById(com.fossil.wearables.fossil.R.id.tv_logcat_item);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m32763a(android.content.Context context) {
        context.startActivity(new android.content.Intent(context, com.portfolio.platform.p007ui.debug.LogcatActivity.class));
    }

    @DexIgnore
    /* renamed from: c */
    public static java.lang.String m32764c(java.lang.String str) {
        java.util.regex.Pattern compile = java.util.regex.Pattern.compile("(E/[A-Za-z0-9_-]+)");
        java.util.regex.Pattern compile2 = java.util.regex.Pattern.compile("(I/[A-Za-z0-9_-]+)");
        java.util.regex.Pattern compile3 = java.util.regex.Pattern.compile("(W/[A-Za-z0-9_-]+)");
        java.lang.StringBuffer stringBuffer = new java.lang.StringBuffer(str.length());
        java.util.regex.Matcher matcher = compile.matcher(str);
        while (matcher.find()) {
            matcher.appendReplacement(stringBuffer, "<font color=\"#d8152a\">" + matcher.group(1) + "</font>");
        }
        matcher.appendTail(stringBuffer);
        java.util.regex.Matcher matcher2 = compile2.matcher(stringBuffer);
        java.lang.StringBuffer stringBuffer2 = new java.lang.StringBuffer(str.length());
        while (matcher2.find()) {
            matcher2.appendReplacement(stringBuffer2, "<font color=\"#46a924\">" + matcher2.group(1) + "</font>");
        }
        matcher2.appendTail(stringBuffer2);
        java.util.regex.Matcher matcher3 = compile3.matcher(stringBuffer2);
        java.lang.StringBuffer stringBuffer3 = new java.lang.StringBuffer(str.length());
        while (matcher3.find()) {
            matcher3.appendReplacement(stringBuffer3, "<font color=\"#f0da23\">" + matcher3.group(1) + "</font>");
        }
        matcher3.appendTail(stringBuffer3);
        return stringBuffer3.toString();
    }

    @DexIgnore
    public void onCreate(android.os.Bundle bundle) {
        super.onCreate(bundle);
        setContentView(com.fossil.wearables.fossil.R.layout.activity_logcat);
        android.widget.Spinner spinner = (android.widget.Spinner) findViewById(com.fossil.wearables.fossil.R.id.spinner_filter);
        spinner.getBackground().setColorFilter(android.graphics.Color.parseColor("#AA7744"), android.graphics.PorterDuff.Mode.SRC_ATOP);
        android.widget.ArrayAdapter<java.lang.CharSequence> createFromResource = android.widget.ArrayAdapter.createFromResource(this, com.fossil.wearables.fossil.R.array.debug_log_filter, 17367048);
        createFromResource.setDropDownViewResource(17367049);
        spinner.setAdapter(createFromResource);
        spinner.setOnItemSelectedListener(new com.portfolio.platform.p007ui.debug.LogcatActivity.C6128a());
        this.f21783C = (androidx.recyclerview.widget.RecyclerView) findViewById(com.fossil.wearables.fossil.R.id.logcat_text);
        this.f21783C.setHasFixedSize(true);
        androidx.recyclerview.widget.LinearLayoutManager linearLayoutManager = new androidx.recyclerview.widget.LinearLayoutManager(this);
        linearLayoutManager.mo2460k(1);
        this.f21783C.setLayoutManager(linearLayoutManager);
        this.f21782B = new com.portfolio.platform.p007ui.debug.LogcatActivity.C6130c();
        this.f21783C.setAdapter(this.f21782B);
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> asyncTask = this.f21784D;
        if (asyncTask != null) {
            asyncTask.cancel(true);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo40267a(int i) {
        if (!f21781E) {
            this.f21782B.mo40276b();
            f21781E = true;
            android.widget.Toast.makeText(this, "Collecting log...", 0).show();
            this.f21784D = new com.portfolio.platform.p007ui.debug.LogcatActivity.C6129b(i, this.f21782B);
            this.f21784D.execute(new java.lang.Void[0]);
        }
    }
}
