package com.portfolio.platform.p007ui.device.locate.map.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.device.locate.map.usecase.GetAddress */
public final class GetAddress extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress.C6175b, com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress.C6177d, com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress.C6176c> {

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.remote.GoogleApiService f21914d;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$a")
    /* renamed from: com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$a */
    public static final class C6174a {
        @DexIgnore
        public C6174a() {
        }

        @DexIgnore
        public /* synthetic */ C6174a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$b")
    /* renamed from: com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$b */
    public static final class C6175b implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ double f21915a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ double f21916b;

        @DexIgnore
        public C6175b(double d, double d2) {
            this.f21915a = d;
            this.f21916b = d2;
        }

        @DexIgnore
        /* renamed from: a */
        public final double mo40376a() {
            return this.f21915a;
        }

        @DexIgnore
        /* renamed from: b */
        public final double mo40377b() {
            return this.f21916b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$c")
    /* renamed from: com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$c */
    public static final class C6176c implements com.portfolio.platform.CoroutineUseCase.C5602a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$d")
    /* renamed from: com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$d */
    public static final class C6177d implements com.portfolio.platform.CoroutineUseCase.C5605d {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f21917a;

        @DexIgnore
        public C6177d(java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "address");
            this.f21917a = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40378a() {
            return this.f21917a;
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress.C6174a((com.fossil.blesdk.obfuscated.fd4) null);
    }
    */

    @DexIgnore
    public GetAddress(com.portfolio.platform.data.source.remote.GoogleApiService googleApiService) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(googleApiService, "mGoogleApiService");
        this.f21914d = googleApiService;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return "GetAddress";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress.C6175b bVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress$run$1 getAddress$run$1;
        int i;
        com.fossil.blesdk.obfuscated.qo2 qo2;
        if (yb4 instanceof com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress$run$1) {
            getAddress$run$1 = (com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress$run$1) yb4;
            int i2 = getAddress$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                getAddress$run$1.label = i2 - Integer.MIN_VALUE;
                java.lang.Object obj = getAddress$run$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = getAddress$run$1.label;
                com.google.gson.JsonElement jsonElement = null;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("GetAddress", "executeUseCase");
                    com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress$run$response$1 getAddress$run$response$1 = new com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress$run$response$1(this, bVar, (com.fossil.blesdk.obfuscated.yb4) null);
                    getAddress$run$1.L$0 = this;
                    getAddress$run$1.L$1 = bVar;
                    getAddress$run$1.label = 1;
                    obj = com.portfolio.platform.response.ResponseKt.m32232a(getAddress$run$response$1, getAddress$run$1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress.C6175b bVar2 = (com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress.C6175b) getAddress$run$1.L$1;
                    com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress getAddress = (com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress) getAddress$run$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
                if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
                    com.fossil.blesdk.obfuscated.xz1 xz1 = (com.fossil.blesdk.obfuscated.xz1) ((com.fossil.blesdk.obfuscated.ro2) qo2).mo30673a();
                    com.fossil.blesdk.obfuscated.tz1 b = xz1 != null ? xz1.mo17941b("results") : null;
                    if (b == null || b.size() <= 0) {
                        return new com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress.C6176c();
                    }
                    com.fossil.blesdk.obfuscated.xz1 xz12 = (com.fossil.blesdk.obfuscated.xz1) b.get(0);
                    if (xz12 != null) {
                        jsonElement = xz12.mo17936a("formatted_address");
                    }
                    if (jsonElement == null) {
                        return new com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress.C6176c();
                    }
                    java.lang.String f = jsonElement.mo16617f();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) f, "value.asString");
                    return new com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress.C6177d(f);
                } else if (qo2 instanceof com.fossil.blesdk.obfuscated.po2) {
                    return new com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress.C6176c();
                } else {
                    throw new kotlin.NoWhenBranchMatchedException();
                }
            }
        }
        getAddress$run$1 = new com.portfolio.platform.p007ui.device.locate.map.usecase.GetAddress$run$1(this, yb4);
        java.lang.Object obj2 = getAddress$run$1.result;
        java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = getAddress$run$1.label;
        com.google.gson.JsonElement jsonElement2 = null;
        if (i != 0) {
        }
        qo2 = (com.fossil.blesdk.obfuscated.qo2) obj2;
        if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
        }
    }
}
