package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase */
public final class UpdateFirmwareUsecase extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6171b, com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6173d, com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6172c> {

    @DexIgnore
    /* renamed from: f */
    public static /* final */ java.lang.String f21908f;

    @DexIgnore
    /* renamed from: g */
    public static /* final */ com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6170a f21909g; // = new com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6170a((com.fossil.blesdk.obfuscated.fd4) null);

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.DeviceRepository f21910d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.en2 f21911e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase$a")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase$a */
    public static final class C6170a {
        @DexIgnore
        public C6170a() {
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40372a() {
            return com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.f21908f;
        }

        @DexIgnore
        public /* synthetic */ C6170a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }

        @DexIgnore
        /* renamed from: a */
        public final com.misfit.frameworks.buttonservice.model.FirmwareData mo40371a(com.fossil.blesdk.obfuscated.en2 en2, java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(en2, "sharedPreferencesManager");
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "deviceSKU");
            if (com.fossil.blesdk.obfuscated.qf4.m26954b("release", "release", true) || !en2.mo26965C()) {
                com.portfolio.platform.data.model.Firmware a = com.fossil.blesdk.obfuscated.dn2.f14174p.mo26576a().mo26565e().mo31582a(str);
                if (a != null) {
                    return com.misfit.frameworks.buttonservice.model.FirmwareFactory.getInstance().createFirmwareData(a.getVersionNumber(), a.getDeviceModel(), a.getChecksum());
                }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a2 = mo40372a();
                local.mo33256e(a2, "Error when update firmware, can't find latest fw of model=" + str);
                return null;
            }
            com.portfolio.platform.data.model.Firmware a3 = en2.mo26989a(str);
            if (a3 != null) {
                return com.misfit.frameworks.buttonservice.model.FirmwareFactory.getInstance().createFirmwareData(a3.getVersionNumber(), a3.getDeviceModel(), a3.getChecksum());
            }
            com.portfolio.platform.data.model.Firmware a4 = com.fossil.blesdk.obfuscated.dn2.f14174p.mo26576a().mo26565e().mo31582a(str);
            if (a4 != null) {
                return com.misfit.frameworks.buttonservice.model.FirmwareFactory.getInstance().createFirmwareData(a4.getVersionNumber(), a4.getDeviceModel(), a4.getChecksum());
            }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a5 = mo40372a();
            local2.mo33256e(a5, "Error when update firmware, can't find latest fw of model=" + str);
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase$b")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase$b */
    public static final class C6171b implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f21912a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ boolean f21913b;

        @DexIgnore
        public C6171b(java.lang.String str, boolean z) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, com.fossil.wearables.fsl.location.DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.f21912a = str;
            this.f21913b = z;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40373a() {
            return this.f21912a;
        }

        @DexIgnore
        /* renamed from: b */
        public final boolean mo40374b() {
            return this.f21913b;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ C6171b(java.lang.String str, boolean z, int i, com.fossil.blesdk.obfuscated.fd4 fd4) {
            this(str, (i & 2) != 0 ? false : z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase$c")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase$c */
    public static final class C6172c implements com.portfolio.platform.CoroutineUseCase.C5602a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase$d")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase$d */
    public static final class C6173d implements com.portfolio.platform.CoroutineUseCase.C5605d {
    }

    /*
    static {
        java.lang.String simpleName = com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "UpdateFirmwareUsecase::class.java.simpleName");
        f21908f = simpleName;
    }
    */

    @DexIgnore
    public UpdateFirmwareUsecase(com.portfolio.platform.data.source.DeviceRepository deviceRepository, com.fossil.blesdk.obfuscated.en2 en2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceRepository, "mDeviceRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(en2, "mSharedPreferencesManager");
        this.f21910d = deviceRepository;
        this.f21911e = en2;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f21908f;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00f0 A[Catch:{ Exception -> 0x0173 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f1 A[Catch:{ Exception -> 0x0173 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f8 A[Catch:{ Exception -> 0x0173 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x016d A[Catch:{ Exception -> 0x0173 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0180  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6171b bVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase$run$1 updateFirmwareUsecase$run$1;
        int i;
        java.lang.String str;
        com.portfolio.platform.data.model.Device device;
        com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase updateFirmwareUsecase;
        java.lang.String sku;
        com.misfit.frameworks.buttonservice.model.FirmwareData a;
        if (yb4 instanceof com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase$run$1) {
            updateFirmwareUsecase$run$1 = (com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase$run$1) yb4;
            int i2 = updateFirmwareUsecase$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                updateFirmwareUsecase$run$1.label = i2 - Integer.MIN_VALUE;
                java.lang.Object obj = updateFirmwareUsecase$run$1.result;
                java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = updateFirmwareUsecase$run$1.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(f21908f, "running UseCase");
                    if (bVar == null) {
                        try {
                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(f21908f, "Error when update firmware, requestValues is NULL");
                            return new com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6172c();
                        } catch (java.lang.Exception e) {
                            e = e;
                            com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                            com.misfit.frameworks.buttonservice.log.FLogger.Component component = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                            com.misfit.frameworks.buttonservice.log.FLogger.Session session = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER;
                            if (bVar != null) {
                            }
                            str = "";
                            java.lang.String str2 = f21908f;
                            remote.mo33266i(component, session, str, str2, "[OTA Start] Exception " + e);
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String str3 = f21908f;
                            local.mo33255d(str3, "Inside .run failed with exception=" + e);
                            e.printStackTrace();
                            return new com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6172c();
                        }
                    } else {
                        device = this.f21910d.getDeviceBySerial(bVar.mo40373a());
                        if (device == null) {
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String str4 = f21908f;
                            local2.mo33256e(str4, "Error when update firmware, can't find latest device on db serial=" + bVar.mo40373a());
                            return new com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6172c();
                        }
                        if (bVar.mo40374b()) {
                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(f21908f, "force download latest fw before udpate fw");
                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().mo33266i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER, com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e(), com.portfolio.platform.ApplicationEventListener.f20882p.mo34421a(), "[OTA Start] Download FW");
                            com.portfolio.platform.util.DeviceUtils a3 = com.portfolio.platform.util.DeviceUtils.f24406g.mo41910a();
                            updateFirmwareUsecase$run$1.L$0 = this;
                            updateFirmwareUsecase$run$1.L$1 = bVar;
                            updateFirmwareUsecase$run$1.L$2 = device;
                            updateFirmwareUsecase$run$1.label = 1;
                            if (a3.mo41904a((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) updateFirmwareUsecase$run$1) == a2) {
                                return a2;
                            }
                        }
                        updateFirmwareUsecase = this;
                    }
                } else if (i == 1) {
                    com.portfolio.platform.data.model.Device device2 = (com.portfolio.platform.data.model.Device) updateFirmwareUsecase$run$1.L$2;
                    com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6171b bVar2 = (com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6171b) updateFirmwareUsecase$run$1.L$1;
                    updateFirmwareUsecase = (com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase) updateFirmwareUsecase$run$1.L$0;
                    try {
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        device = device2;
                        bVar = bVar2;
                    } catch (java.lang.Exception e2) {
                        e = e2;
                        bVar = bVar2;
                        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                        com.misfit.frameworks.buttonservice.log.FLogger.Component component2 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                        com.misfit.frameworks.buttonservice.log.FLogger.Session session2 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER;
                        if (bVar != null) {
                            java.lang.String a4 = bVar.mo40373a();
                            if (a4 != null) {
                                str = a4;
                                java.lang.String str22 = f21908f;
                                remote2.mo33266i(component2, session2, str, str22, "[OTA Start] Exception " + e);
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                java.lang.String str32 = f21908f;
                                local3.mo33255d(str32, "Inside .run failed with exception=" + e);
                                e.printStackTrace();
                                return new com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6172c();
                            }
                        }
                        str = "";
                        java.lang.String str222 = f21908f;
                        remote2.mo33266i(component2, session2, str, str222, "[OTA Start] Exception " + e);
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local32 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String str322 = f21908f;
                        local32.mo33255d(str322, "Inside .run failed with exception=" + e);
                        e.printStackTrace();
                        return new com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6172c();
                    }
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6170a aVar = f21909g;
                com.fossil.blesdk.obfuscated.en2 en2 = updateFirmwareUsecase.f21911e;
                sku = device.getSku();
                if (sku != null) {
                    sku = "";
                }
                a = aVar.mo40371a(en2, sku);
                if (a != null) {
                    return new com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6172c();
                }
                updateFirmwareUsecase.f21911e.mo27000a(device.getDeviceId(), device.getFirmwareRevision());
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String str5 = f21908f;
                local4.mo33255d(str5, "Start update firmware with version=" + a.getFirmwareVersion() + ", currentVersion=" + device.getFirmwareRevision());
                com.misfit.frameworks.buttonservice.model.UserProfile j = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34543j();
                if (j != null) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().mo33266i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER, bVar.mo40373a(), f21908f, "[OTA Start] Calling OTA from SDK");
                    com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34500a(bVar.mo40373a(), a, j);
                    com.fossil.blesdk.obfuscated.qa4 qa4 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                } else {
                    new com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6172c();
                }
                return new com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6173d();
            }
        }
        updateFirmwareUsecase$run$1 = new com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase$run$1(this, yb4);
        java.lang.Object obj2 = updateFirmwareUsecase$run$1.result;
        java.lang.Object a22 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = updateFirmwareUsecase$run$1.label;
        if (i != 0) {
        }
        com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6170a aVar2 = f21909g;
        com.fossil.blesdk.obfuscated.en2 en22 = updateFirmwareUsecase.f21911e;
        sku = device.getSku();
        if (sku != null) {
        }
        a = aVar2.mo40371a(en22, sku);
        if (a != null) {
        }
    }
}
