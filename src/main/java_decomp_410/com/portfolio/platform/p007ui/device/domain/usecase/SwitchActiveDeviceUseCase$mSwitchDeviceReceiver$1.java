package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1 */
public final class SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1 implements com.portfolio.platform.service.BleCommandResultManager.C5999b {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase f21894a;

    @DexIgnore
    public SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1(com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase) {
        this.f21894a = switchActiveDeviceUseCase;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo27136a(com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode, android.content.Intent intent) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(communicateMode, "communicateMode");
        com.fossil.blesdk.obfuscated.kd4.m24411b(intent, "intent");
        int intExtra = intent.getIntExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
        java.lang.String stringExtra = intent.getStringExtra(com.misfit.frameworks.common.constants.Constants.SERIAL_NUMBER);
        if (communicateMode == com.misfit.frameworks.buttonservice.communite.CommunicateMode.SWITCH_DEVICE) {
            com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6162b f = this.f21894a.mo40350f();
            if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) stringExtra, (java.lang.Object) f != null ? f.mo40356b() : null)) {
                boolean z = true;
                if (intExtra == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.ASK_FOR_LINK_SERVER.ordinal()) {
                    if (intent.getExtras() == null) {
                        z = false;
                    }
                    if (!com.fossil.blesdk.obfuscated.ra4.f18216a || z) {
                        android.os.Bundle extras = intent.getExtras();
                        if (extras != null) {
                            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1(this, stringExtra, (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) extras.getParcelable("device"), (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                    } else {
                        throw new java.lang.AssertionError("Assertion failed");
                    }
                } else if (intExtra == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.f21894a.mo40353i();
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.f21875n.mo40354a(), "Switch device  success");
                    if (intent.getExtras() == null) {
                        z = false;
                    }
                    if (!com.fossil.blesdk.obfuscated.ra4.f18216a || z) {
                        android.os.Bundle extras2 = intent.getExtras();
                        if (extras2 != null) {
                            com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile misfitDeviceProfile = (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) extras2.getParcelable("device");
                            if (misfitDeviceProfile != null) {
                                if (misfitDeviceProfile.getHeartRateMode() != com.misfit.frameworks.buttonservice.enums.HeartRateMode.NONE) {
                                    this.f21894a.f21884l.mo26995a(misfitDeviceProfile.getHeartRateMode());
                                }
                                com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.f21894a;
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) stringExtra, "serial");
                                switchActiveDeviceUseCase.mo40347b(stringExtra);
                                com.fossil.blesdk.obfuscated.fi4 unused2 = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2(this, misfitDeviceProfile, stringExtra, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                                com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34544j(stringExtra);
                                com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase2 = this.f21894a;
                                switchActiveDeviceUseCase2.mo34436a(new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6164d(switchActiveDeviceUseCase2.mo40349e()));
                                return;
                            }
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                    throw new java.lang.AssertionError("Assertion failed");
                } else if (intExtra == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED.ordinal()) {
                    this.f21894a.mo40353i();
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    java.util.ArrayList<java.lang.Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new java.util.ArrayList<>();
                    }
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String a = com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.f21875n.mo40354a();
                    local.mo33255d(a, "stop current workout fail due to " + intExtra2);
                    if (intExtra2 != 1101) {
                        if (intExtra2 == 1928) {
                            com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6163c g = this.f21894a.mo40351g();
                            if (g == null || this.f21894a.mo34434a(g) == null) {
                                this.f21894a.mo34434a(new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6163c(116, (java.util.ArrayList<java.lang.Integer>) null, ""));
                                return;
                            }
                            return;
                        } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                            this.f21894a.mo34434a(new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6163c(117, (java.util.ArrayList<java.lang.Integer>) null, ""));
                            return;
                        }
                    }
                    this.f21894a.mo34434a(new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6163c(113, integerArrayListExtra, ""));
                }
            }
        }
    }
}
