package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1$onSuccess$1", mo27670f = "DianaSyncUseCase.kt", mo27671l = {}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1$onSuccess$1 */
public final class DianaSyncUseCase$verifySecretKey$1$onSuccess$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21814p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaSyncUseCase$verifySecretKey$1$onSuccess$1(com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1 dianaSyncUseCase$verifySecretKey$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = dianaSyncUseCase$verifySecretKey$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1$onSuccess$1 dianaSyncUseCase$verifySecretKey$1$onSuccess$1 = new com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1$onSuccess$1(this.this$0, yb4);
        dianaSyncUseCase$verifySecretKey$1$onSuccess$1.f21814p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dianaSyncUseCase$verifySecretKey$1$onSuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1$onSuccess$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1 dianaSyncUseCase$verifySecretKey$1 = this.this$0;
            dianaSyncUseCase$verifySecretKey$1.f21809a.mo40281a(dianaSyncUseCase$verifySecretKey$1.f21811c, dianaSyncUseCase$verifySecretKey$1.f21812d, dianaSyncUseCase$verifySecretKey$1.f21813e, dianaSyncUseCase$verifySecretKey$1.f21810b);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
