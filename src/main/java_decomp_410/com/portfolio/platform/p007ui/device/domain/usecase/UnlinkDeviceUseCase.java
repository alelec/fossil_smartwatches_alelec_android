package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase */
public final class UnlinkDeviceUseCase extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.C6167b, com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.C6169d, com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.C6168c> {

    @DexIgnore
    /* renamed from: h */
    public static /* final */ java.lang.String f21897h;

    @DexIgnore
    /* renamed from: i */
    public static /* final */ com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.C6166a f21898i; // = new com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.C6166a((com.fossil.blesdk.obfuscated.fd4) null);

    @DexIgnore
    /* renamed from: d */
    public com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.C6167b f21899d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.portfolio.platform.data.source.DeviceRepository f21900e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.portfolio.platform.PortfolioApp f21901f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.portfolio.platform.data.source.WatchFaceRepository f21902g;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$a")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$a */
    public static final class C6166a {
        @DexIgnore
        public C6166a() {
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40365a() {
            return com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.f21897h;
        }

        @DexIgnore
        public /* synthetic */ C6166a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$b")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$b */
    public static final class C6167b implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f21903a;

        @DexIgnore
        public C6167b(java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, com.fossil.wearables.fsl.location.DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.f21903a = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40366a() {
            return this.f21903a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$c")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$c */
    public static final class C6168c implements com.portfolio.platform.CoroutineUseCase.C5602a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f21904a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.util.ArrayList<java.lang.Integer> f21905b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ java.lang.String f21906c;

        @DexIgnore
        public C6168c(java.lang.String str, java.util.ArrayList<java.lang.Integer> arrayList, java.lang.String str2) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "lastErrorCode");
            this.f21904a = str;
            this.f21905b = arrayList;
            this.f21906c = str2;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40367a() {
            return this.f21906c;
        }

        @DexIgnore
        /* renamed from: b */
        public final java.lang.String mo40368b() {
            return this.f21904a;
        }

        @DexIgnore
        /* renamed from: c */
        public final java.util.ArrayList<java.lang.Integer> mo40369c() {
            return this.f21905b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$d")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$d */
    public static final class C6169d implements com.portfolio.platform.CoroutineUseCase.C5605d {
    }

    /*
    static {
        java.lang.String simpleName = com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "UnlinkDeviceUseCase::class.java.simpleName");
        f21897h = simpleName;
    }
    */

    @DexIgnore
    public UnlinkDeviceUseCase(com.portfolio.platform.data.source.DeviceRepository deviceRepository, com.fossil.blesdk.obfuscated.vj2 vj2, com.portfolio.platform.PortfolioApp portfolioApp, com.portfolio.platform.data.source.WatchFaceRepository watchFaceRepository) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceRepository, "mDeviceRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(vj2, "mDeviceSettingFactory");
        com.fossil.blesdk.obfuscated.kd4.m24411b(portfolioApp, "mApp");
        com.fossil.blesdk.obfuscated.kd4.m24411b(watchFaceRepository, "watchFaceRepository");
        this.f21900e = deviceRepository;
        this.f21901f = portfolioApp;
        this.f21902g = watchFaceRepository;
    }

    @DexIgnore
    /* renamed from: d */
    public final com.fossil.blesdk.obfuscated.fi4 mo40364d() {
        return com.fossil.blesdk.obfuscated.ag4.m19844b(mo34439b(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$1(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f21897h;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.C6167b bVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(f21897h, "running UseCase");
        if (bVar == null) {
            return new com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.C6168c("", (java.util.ArrayList<java.lang.Integer>) null, "");
        }
        this.f21899d = bVar;
        java.lang.String a = bVar.mo40366a();
        java.lang.String e = this.f21901f.mo34532e();
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str = f21897h;
        local.mo33255d(str, "remove device " + a + " currentActive " + e);
        mo40364d();
        return new java.lang.Object();
    }
}
