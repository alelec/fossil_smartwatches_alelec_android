package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase */
public final class HybridSyncUseCase extends com.portfolio.platform.CoroutineUseCase<com.fossil.blesdk.obfuscated.zq2, com.fossil.blesdk.obfuscated.ar2, com.fossil.blesdk.obfuscated.yq2> {

    @DexIgnore
    /* renamed from: m */
    public static /* final */ java.lang.String f21819m;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.p007ui.device.domain.usecase.HybridSyncUseCase.C6141b f21820d; // = new com.portfolio.platform.p007ui.device.domain.usecase.HybridSyncUseCase.C6141b(this);

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.portfolio.platform.data.source.MicroAppRepository f21821e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.en2 f21822f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.portfolio.platform.data.source.DeviceRepository f21823g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.portfolio.platform.PortfolioApp f21824h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.portfolio.platform.data.source.HybridPresetRepository f21825i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ com.portfolio.platform.data.source.NotificationsRepository f21826j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ com.portfolio.platform.helper.AnalyticsHelper f21827k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ com.portfolio.platform.data.source.AlarmsRepository f21828l;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase$a")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase$a */
    public static final class C6140a {
        @DexIgnore
        public C6140a() {
        }

        @DexIgnore
        public /* synthetic */ C6140a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase$b")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase$b */
    public static final class C6141b implements com.portfolio.platform.service.BleCommandResultManager.C5999b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.device.domain.usecase.HybridSyncUseCase f21829a;

        @DexIgnore
        public C6141b(com.portfolio.platform.p007ui.device.domain.usecase.HybridSyncUseCase hybridSyncUseCase) {
            this.f21829a = hybridSyncUseCase;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo27136a(com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode, android.content.Intent intent) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(communicateMode, "communicateMode");
            com.fossil.blesdk.obfuscated.kd4.m24411b(intent, "intent");
            java.lang.String stringExtra = intent.getStringExtra("SERIAL");
            if (!android.text.TextUtils.isEmpty(stringExtra) && com.fossil.blesdk.obfuscated.qf4.m26954b(stringExtra, this.f21829a.f21824h.mo34532e(), true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                if (intExtra == 1) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.p007ui.device.domain.usecase.HybridSyncUseCase.f21819m, "sync success, remove device now");
                    com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39775b((com.portfolio.platform.service.BleCommandResultManager.C5999b) this, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC);
                    this.f21829a.mo34436a(new com.fossil.blesdk.obfuscated.ar2());
                } else if (intExtra == 2 || intExtra == 4) {
                    com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39775b((com.portfolio.platform.service.BleCommandResultManager.C5999b) this, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC);
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    java.util.ArrayList<java.lang.Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new java.util.ArrayList<>();
                    }
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String d = com.portfolio.platform.p007ui.device.domain.usecase.HybridSyncUseCase.f21819m;
                    local.mo33255d(d, "sync fail due to " + intExtra2);
                    if (intExtra2 == 1101 || intExtra2 == 1112 || intExtra2 == 1113) {
                        this.f21829a.mo34434a(new com.fossil.blesdk.obfuscated.yq2(com.portfolio.platform.enums.SyncResponseCode.FAIL_DUE_TO_LACK_PERMISSION, integerArrayListExtra));
                    } else {
                        this.f21829a.mo34434a(new com.fossil.blesdk.obfuscated.yq2(com.portfolio.platform.enums.SyncResponseCode.FAIL_DUE_TO_SYNC_FAIL, (java.util.ArrayList<java.lang.Integer>) null));
                    }
                }
            }
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.device.domain.usecase.HybridSyncUseCase.C6140a((com.fossil.blesdk.obfuscated.fd4) null);
        java.lang.String simpleName = com.portfolio.platform.p007ui.device.domain.usecase.HybridSyncUseCase.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "HybridSyncUseCase::class.java.simpleName");
        f21819m = simpleName;
    }
    */

    @DexIgnore
    public HybridSyncUseCase(com.portfolio.platform.data.source.MicroAppRepository microAppRepository, com.fossil.blesdk.obfuscated.en2 en2, com.portfolio.platform.data.source.DeviceRepository deviceRepository, com.portfolio.platform.PortfolioApp portfolioApp, com.portfolio.platform.data.source.HybridPresetRepository hybridPresetRepository, com.portfolio.platform.data.source.NotificationsRepository notificationsRepository, com.portfolio.platform.helper.AnalyticsHelper analyticsHelper, com.portfolio.platform.data.source.AlarmsRepository alarmsRepository) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(microAppRepository, "mMicroAppRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(en2, "mSharedPrefs");
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceRepository, "mDeviceRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(portfolioApp, "mApp");
        com.fossil.blesdk.obfuscated.kd4.m24411b(hybridPresetRepository, "mPresetRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(notificationsRepository, "mNotificationsRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(analyticsHelper, "mAnalyticsHelper");
        com.fossil.blesdk.obfuscated.kd4.m24411b(alarmsRepository, "mAlarmsRepository");
        this.f21821e = microAppRepository;
        this.f21822f = en2;
        this.f21823g = deviceRepository;
        this.f21824h = portfolioApp;
        this.f21825i = hybridPresetRepository;
        this.f21826j = notificationsRepository;
        this.f21827k = analyticsHelper;
        this.f21828l = alarmsRepository;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return "HybridSyncUseCase";
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Object mo26310a(com.fossil.blesdk.obfuscated.zq2 zq2, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        if (zq2 == null) {
            return new com.fossil.blesdk.obfuscated.yq2(com.portfolio.platform.enums.SyncResponseCode.FAIL_DUE_TO_INVALID_REQUEST, (java.util.ArrayList<java.lang.Integer>) null);
        }
        int b = zq2.mo33110b();
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str = f21819m;
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("start on thread=");
        java.lang.Thread currentThread = java.lang.Thread.currentThread();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        local.mo33255d(str, sb.toString());
        com.misfit.frameworks.buttonservice.model.UserProfile j = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34543j();
        if (j == null) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str2 = f21819m;
            local2.mo33256e(str2, "Error inside " + f21819m + ".startDeviceSync - user is null");
            mo40294a(zq2.mo33109a());
            return new com.fossil.blesdk.obfuscated.yq2(com.portfolio.platform.enums.SyncResponseCode.FAIL_DUE_TO_INVALID_REQUEST, (java.util.ArrayList<java.lang.Integer>) null);
        } else if (android.text.TextUtils.isEmpty(zq2.mo33109a())) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str3 = f21819m;
            local3.mo33256e(str3, "Error inside " + f21819m + ".startDeviceSync - serial is null");
            mo40294a(zq2.mo33109a());
            return new com.fossil.blesdk.obfuscated.yq2(com.portfolio.platform.enums.SyncResponseCode.FAIL_DUE_TO_INVALID_REQUEST, (java.util.ArrayList<java.lang.Integer>) null);
        } else {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str4 = f21819m;
            local4.mo33255d(str4, "Inside " + f21819m + ".startDeviceSync - serial=" + zq2.mo33109a() + "," + " weightInKg=" + j.getUserBiometricData().getWeightInKilogram() + ", heightInMeter=" + j.getUserBiometricData().getHeightInMeter() + ", goal=" + j.getGoalSteps() + ", isNewDevice=" + zq2.mo33111c() + ", SyncMode=" + b);
            j.setOriginalSyncMode(b);
            if (!this.f21822f.mo27053k(zq2.mo33109a()) || b == 13) {
                b = 13;
            }
            if (b == 13 || !com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34540h(zq2.mo33109a())) {
                if (b == 13) {
                    try {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String str5 = f21819m;
                        local5.mo33255d(str5, "Inside " + f21819m + ".startDeviceSync - Start full-sync");
                        mo40291a(this.f21826j.getAllNotificationsByHour(zq2.mo33109a(), com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()), zq2.mo33109a(), com.portfolio.platform.util.NotificationAppHelper.f24430b.mo41923a(this.f21823g.getSkuModelBySerialPrefix(com.portfolio.platform.helper.DeviceHelper.f21188o.mo39559b(zq2.mo33109a())), zq2.mo33109a()));
                        java.util.List activeAlarms = this.f21828l.getActiveAlarms();
                        if (activeAlarms == null) {
                            activeAlarms = new java.util.ArrayList();
                        }
                        com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34508a((java.util.List<? extends com.misfit.frameworks.buttonservice.model.Alarm>) com.fossil.blesdk.obfuscated.nj2.m25699a(activeAlarms));
                        com.portfolio.platform.data.model.room.microapp.HybridPreset activePresetBySerial = this.f21825i.getActivePresetBySerial(zq2.mo33109a());
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String c = mo26311c();
                        local6.mo33255d(c, "startDeviceSync activePreset=" + activePresetBySerial);
                        if (activePresetBySerial != null) {
                            for (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting hybridPresetAppSetting : activePresetBySerial.getButtons()) {
                                if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) hybridPresetAppSetting.getAppId(), (java.lang.Object) com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                                    java.lang.String settings = hybridPresetAppSetting.getSettings();
                                    if (!android.text.TextUtils.isEmpty(settings)) {
                                        try {
                                            com.portfolio.platform.data.model.setting.SecondTimezoneSetting secondTimezoneSetting = (com.portfolio.platform.data.model.setting.SecondTimezoneSetting) new com.google.gson.Gson().mo23093a(settings, com.portfolio.platform.data.model.setting.SecondTimezoneSetting.class);
                                            if (secondTimezoneSetting != null && !android.text.TextUtils.isEmpty(secondTimezoneSetting.getTimeZoneId())) {
                                                com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34552n(secondTimezoneSetting.getTimeZoneId());
                                            }
                                        } catch (java.lang.Exception e) {
                                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                            java.lang.String str6 = f21819m;
                                            local7.mo33256e(str6, "Inside " + f21819m + ".startDeviceSync - parse secondTimezone, ex=" + e);
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                            java.util.List<com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping> a = com.fossil.blesdk.obfuscated.sj2.m27757a(activePresetBySerial, zq2.mo33109a(), this.f21823g, this.f21821e);
                            if (!a.isEmpty()) {
                                com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34520b(zq2.mo33109a(), (java.util.List<? extends com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping>) a);
                            }
                        }
                    } catch (java.lang.Exception e2) {
                        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                        com.misfit.frameworks.buttonservice.log.FLogger.Component component = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                        com.misfit.frameworks.buttonservice.log.FLogger.Session session = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER;
                        java.lang.String a2 = zq2.mo33109a();
                        java.lang.String str7 = f21819m;
                        remote.mo33266i(component, session, a2, str7, "[Sync] Exception when prepare settings " + e2);
                    }
                }
                mo40293a(b, zq2.mo33111c(), j, zq2.mo33109a());
                return new java.lang.Object();
            }
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(f21819m, "Device is syncing, returning...");
            return new com.fossil.blesdk.obfuscated.ar2();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.fi4 mo40291a(android.util.SparseArray<java.util.List<com.fossil.wearables.fsl.shared.BaseFeatureModel>> sparseArray, java.lang.String str, boolean z) {
        com.fossil.blesdk.obfuscated.zg4 a = com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25691a());
        com.portfolio.platform.p007ui.device.domain.usecase.HybridSyncUseCase$saveNotificationSettingToDevice$1 hybridSyncUseCase$saveNotificationSettingToDevice$1 = new com.portfolio.platform.p007ui.device.domain.usecase.HybridSyncUseCase$saveNotificationSettingToDevice$1(this, sparseArray, z, str, (com.fossil.blesdk.obfuscated.yb4) null);
        return com.fossil.blesdk.obfuscated.ag4.m19844b(a, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, hybridSyncUseCase$saveNotificationSettingToDevice$1, 3, (java.lang.Object) null);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40293a(int i, boolean z, com.misfit.frameworks.buttonservice.model.UserProfile userProfile, java.lang.String str) {
        if (mo34437a() != null) {
            com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39771a((com.portfolio.platform.service.BleCommandResultManager.C5999b) this.f21820d, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC);
        }
        userProfile.setNewDevice(z);
        userProfile.setSyncMode(i);
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str2 = f21819m;
        local.mo33255d(str2, ".startDeviceSync - syncMode=" + i);
        if (!this.f21822f.mo26983U()) {
            com.portfolio.platform.data.model.SKUModel skuModelBySerialPrefix = this.f21823g.getSkuModelBySerialPrefix(com.portfolio.platform.helper.DeviceHelper.f21188o.mo39559b(str));
            this.f21822f.mo26997a((java.lang.Boolean) true);
            this.f21827k.mo39493a(i, skuModelBySerialPrefix);
            com.fossil.blesdk.obfuscated.ul2 b = com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39514b("sync_session");
            com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39513a("sync_session", b);
            b.mo31559d();
        }
        if (!com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34512a(str, userProfile)) {
            mo40294a(str);
        } else if (i == 13) {
            this.f21822f.mo26999a(str, java.lang.System.currentTimeMillis(), false);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40294a(java.lang.String str) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str2 = f21819m;
        local.mo33255d(str2, "broadcastSyncFail serial=" + str);
        android.content.Intent intent = new android.content.Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", 2);
        intent.putExtra("SERIAL", str);
    }
}
