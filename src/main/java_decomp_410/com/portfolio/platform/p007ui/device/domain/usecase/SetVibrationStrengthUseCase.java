package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase */
public final class SetVibrationStrengthUseCase extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.C6157b, com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.C6159d, com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.C6158c> {

    @DexIgnore
    /* renamed from: i */
    public static /* final */ java.lang.String f21860i;

    @DexIgnore
    /* renamed from: j */
    public static /* final */ com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.C6156a f21861j; // = new com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.C6156a((com.fossil.blesdk.obfuscated.fd4) null);

    @DexIgnore
    /* renamed from: d */
    public boolean f21862d;

    @DexIgnore
    /* renamed from: e */
    public int f21863e;

    @DexIgnore
    /* renamed from: f */
    public java.lang.String f21864f; // = "";

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.SetVibrationStrengthBroadcastReceiver f21865g; // = new com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.SetVibrationStrengthBroadcastReceiver();

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.portfolio.platform.data.source.DeviceRepository f21866h;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver */
    public final class SetVibrationStrengthBroadcastReceiver implements com.portfolio.platform.service.BleCommandResultManager.C5999b {
        @DexIgnore
        public SetVibrationStrengthBroadcastReceiver() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo27136a(com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode, android.content.Intent intent) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(communicateMode, "communicateMode");
            com.fossil.blesdk.obfuscated.kd4.m24411b(intent, "intent");
            int intExtra = intent.getIntExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a = com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.f21861j.mo40336a();
            local.mo33255d(a, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.this.mo40333f() + ", isSuccess=" + intExtra);
            if (communicateMode == com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_VIBRATION_STRENGTH && com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.this.mo40333f()) {
                boolean z = false;
                com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.this.mo40330a(false);
                if (intExtra == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.this.mo34439b(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.device.domain.usecase.C6160xe9993bed(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                    return;
                }
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.f21861j.mo40336a(), "onReceive failed");
                int intExtra2 = intent.getIntExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                java.util.ArrayList<java.lang.Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new java.util.ArrayList<>(intExtra2);
                }
                com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.this.mo34434a(new com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.C6158c(com.misfit.frameworks.buttonservice.log.FailureCode.FAILED_TO_CONNECT, intExtra2, integerArrayListExtra));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$a")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$a */
    public static final class C6156a {
        @DexIgnore
        public C6156a() {
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40336a() {
            return com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.f21860i;
        }

        @DexIgnore
        public /* synthetic */ C6156a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$b")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$b */
    public static final class C6157b implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f21868a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f21869b;

        @DexIgnore
        public C6157b(java.lang.String str, int i) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, com.fossil.wearables.fsl.location.DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.f21868a = str;
            this.f21869b = i;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40337a() {
            return this.f21868a;
        }

        @DexIgnore
        /* renamed from: b */
        public final int mo40338b() {
            return this.f21869b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$c")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$c */
    public static final class C6158c implements com.portfolio.platform.CoroutineUseCase.C5602a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f21870a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f21871b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ java.util.ArrayList<java.lang.Integer> f21872c;

        @DexIgnore
        public C6158c(int i, int i2, java.util.ArrayList<java.lang.Integer> arrayList) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(arrayList, "errorCodes");
            this.f21870a = i;
            this.f21871b = i2;
            this.f21872c = arrayList;
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo40339a() {
            return this.f21870a;
        }

        @DexIgnore
        /* renamed from: b */
        public final java.util.ArrayList<java.lang.Integer> mo40340b() {
            return this.f21872c;
        }

        @DexIgnore
        /* renamed from: c */
        public final int mo40341c() {
            return this.f21871b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$d")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$d */
    public static final class C6159d implements com.portfolio.platform.CoroutineUseCase.C5605d {
    }

    /*
    static {
        java.lang.String simpleName = com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "SetVibrationStrengthUseCase::class.java.simpleName");
        f21860i = simpleName;
    }
    */

    @DexIgnore
    public SetVibrationStrengthUseCase(com.portfolio.platform.data.source.DeviceRepository deviceRepository, com.fossil.blesdk.obfuscated.en2 en2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceRepository, "mDeviceRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(en2, "mSharedPreferencesManager");
        this.f21866h = deviceRepository;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f21860i;
    }

    @DexIgnore
    /* renamed from: d */
    public final int mo40331d() {
        return this.f21863e;
    }

    @DexIgnore
    /* renamed from: e */
    public final java.lang.String mo40332e() {
        return this.f21864f;
    }

    @DexIgnore
    /* renamed from: f */
    public final boolean mo40333f() {
        return this.f21862d;
    }

    @DexIgnore
    /* renamed from: g */
    public final void mo40334g() {
        com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39771a((com.portfolio.platform.service.BleCommandResultManager.C5999b) this.f21865g, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_VIBRATION_STRENGTH);
    }

    @DexIgnore
    /* renamed from: h */
    public final void mo40335h() {
        com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39775b((com.portfolio.platform.service.BleCommandResultManager.C5999b) this.f21865g, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_VIBRATION_STRENGTH);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40330a(boolean z) {
        this.f21862d = z;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.C6157b bVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        try {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(f21860i, "running UseCase");
            this.f21862d = true;
            java.lang.Integer a = bVar != null ? com.fossil.blesdk.obfuscated.dc4.m20843a(bVar.mo40338b()) : null;
            if (a != null) {
                this.f21863e = a.intValue();
                this.f21864f = bVar.mo40337a();
                com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34503a(bVar.mo40337a(), new com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj(com.fossil.blesdk.obfuscated.yk2.m31032a(bVar.mo40338b()), false, 2, (com.fossil.blesdk.obfuscated.fd4) null));
                return new java.lang.Object();
            }
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str = f21860i;
            local.mo33256e(str, "Error inside " + f21860i + ".connectDevice - e=" + e);
            return new com.portfolio.platform.p007ui.device.domain.usecase.SetVibrationStrengthUseCase.C6158c(600, -1, new java.util.ArrayList());
        }
    }
}
