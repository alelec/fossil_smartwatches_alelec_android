package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase */
public final class SwitchActiveDeviceUseCase extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6162b, com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6164d, com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6163c> {

    @DexIgnore
    /* renamed from: m */
    public static /* final */ java.lang.String f21874m;

    @DexIgnore
    /* renamed from: n */
    public static /* final */ com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6161a f21875n; // = new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6161a((com.fossil.blesdk.obfuscated.fd4) null);

    @DexIgnore
    /* renamed from: d */
    public com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6162b f21876d;

    @DexIgnore
    /* renamed from: e */
    public com.portfolio.platform.data.model.Device f21877e;

    @DexIgnore
    /* renamed from: f */
    public com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6163c f21878f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.portfolio.platform.service.BleCommandResultManager.C5999b f21879g; // = new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1(this);

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.portfolio.platform.data.source.UserRepository f21880h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.portfolio.platform.data.source.DeviceRepository f21881i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ com.fossil.blesdk.obfuscated.vj2 f21882j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ com.portfolio.platform.PortfolioApp f21883k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ com.fossil.blesdk.obfuscated.en2 f21884l;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$a")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$a */
    public static final class C6161a {
        @DexIgnore
        public C6161a() {
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40354a() {
            return com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.f21874m;
        }

        @DexIgnore
        public /* synthetic */ C6161a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$b")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$b */
    public static final class C6162b implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f21885a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f21886b;

        @DexIgnore
        public C6162b(java.lang.String str, int i) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "newActiveSerial");
            this.f21885a = str;
            this.f21886b = i;
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo40355a() {
            return this.f21886b;
        }

        @DexIgnore
        /* renamed from: b */
        public final java.lang.String mo40356b() {
            return this.f21885a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$c")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$c */
    public static final class C6163c implements com.portfolio.platform.CoroutineUseCase.C5602a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f21887a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.util.ArrayList<java.lang.Integer> f21888b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ java.lang.String f21889c;

        @DexIgnore
        public C6163c(int i, java.util.ArrayList<java.lang.Integer> arrayList, java.lang.String str) {
            this.f21887a = i;
            this.f21888b = arrayList;
            this.f21889c = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40357a() {
            return this.f21889c;
        }

        @DexIgnore
        /* renamed from: b */
        public final int mo40358b() {
            return this.f21887a;
        }

        @DexIgnore
        /* renamed from: c */
        public final java.util.ArrayList<java.lang.Integer> mo40359c() {
            return this.f21888b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$d")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$d */
    public static final class C6164d implements com.portfolio.platform.CoroutineUseCase.C5605d {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.portfolio.platform.data.model.Device f21890a;

        @DexIgnore
        public C6164d(com.portfolio.platform.data.model.Device device) {
            this.f21890a = device;
        }

        @DexIgnore
        /* renamed from: a */
        public final com.portfolio.platform.data.model.Device mo40360a() {
            return this.f21890a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$e")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$e */
    public static final class C6165e implements com.portfolio.platform.CoroutineUseCase.C5606e<com.fossil.blesdk.obfuscated.pn3, com.fossil.blesdk.obfuscated.nn3> {
        @DexIgnore
        /* renamed from: a */
        public void onSuccess(com.fossil.blesdk.obfuscated.pn3 pn3) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(pn3, "responseValue");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.f21875n.mo40354a(), "getDeviceSetting success");
        }

        @DexIgnore
        /* renamed from: a */
        public void mo29641a(com.fossil.blesdk.obfuscated.nn3 nn3) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(nn3, "errorValue");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.f21875n.mo40354a(), "getDeviceSetting fail");
        }
    }

    /*
    static {
        java.lang.String simpleName = com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "SwitchActiveDeviceUseCase::class.java.simpleName");
        f21874m = simpleName;
    }
    */

    @DexIgnore
    public SwitchActiveDeviceUseCase(com.portfolio.platform.data.source.UserRepository userRepository, com.portfolio.platform.data.source.DeviceRepository deviceRepository, com.fossil.blesdk.obfuscated.vj2 vj2, com.portfolio.platform.PortfolioApp portfolioApp, com.fossil.blesdk.obfuscated.en2 en2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(userRepository, "mUserRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceRepository, "mDeviceRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(vj2, "mDeviceSettingFactory");
        com.fossil.blesdk.obfuscated.kd4.m24411b(portfolioApp, "mApp");
        com.fossil.blesdk.obfuscated.kd4.m24411b(en2, "mSharePrefs");
        this.f21880h = userRepository;
        this.f21881i = deviceRepository;
        this.f21882j = vj2;
        this.f21883k = portfolioApp;
        this.f21884l = en2;
    }

    @DexIgnore
    /* renamed from: e */
    public final com.portfolio.platform.data.model.Device mo40349e() {
        return this.f21877e;
    }

    @DexIgnore
    /* renamed from: f */
    public final com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6162b mo40350f() {
        return this.f21876d;
    }

    @DexIgnore
    /* renamed from: g */
    public final com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6163c mo40351g() {
        return this.f21878f;
    }

    @DexIgnore
    /* renamed from: h */
    public final void mo40352h() {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(f21874m, "registerReceiver ");
        com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39775b(this.f21879g, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SWITCH_DEVICE);
        com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39771a(this.f21879g, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SWITCH_DEVICE);
    }

    @DexIgnore
    /* renamed from: i */
    public final void mo40353i() {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(f21874m, "unregisterReceiver ");
        com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39775b(this.f21879g, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SWITCH_DEVICE);
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo40347b(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(f21874m, "getDeviceSetting start");
        this.f21882j.mo31816a(str).mo34435a(new com.fossil.blesdk.obfuscated.on3(str), new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6165e());
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f21874m;
    }

    @DexIgnore
    /* renamed from: d */
    public final com.fossil.blesdk.obfuscated.fi4 mo40348d() {
        return com.fossil.blesdk.obfuscated.ag4.m19844b(mo34439b(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$1(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40345a(com.portfolio.platform.data.model.Device device) {
        this.f21877e = device;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40346a(com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6163c cVar) {
        this.f21878f = cVar;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6162b bVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        if (bVar == null) {
            return new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6163c(600, (java.util.ArrayList<java.lang.Integer>) null, "");
        }
        this.f21876d = bVar;
        java.lang.String e = this.f21883k.mo34532e();
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str = f21874m;
        local.mo33255d(str, "run with mode " + bVar.mo40355a() + " currentActive " + e);
        if (bVar.mo40355a() != 4) {
            mo40348d();
        } else {
            mo40342a(bVar.mo40356b());
        }
        return new java.lang.Object();
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.fi4 mo40342a(java.lang.String str) {
        return com.fossil.blesdk.obfuscated.ag4.m19844b(mo34439b(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1(this, str, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    /* renamed from: a */
    public final /* synthetic */ java.lang.Object mo40344a(java.lang.String str, com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile misfitDeviceProfile, com.fossil.blesdk.obfuscated.yb4<? super kotlin.Pair<java.lang.Boolean, java.lang.Integer>> yb4) {
        return com.fossil.blesdk.obfuscated.yf4.m30997a(com.fossil.blesdk.obfuscated.nh4.m25692b(), new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$linkServer$2(this, misfitDeviceProfile, str, (com.fossil.blesdk.obfuscated.yb4) null), yb4);
    }
}
