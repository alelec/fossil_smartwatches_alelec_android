package com.portfolio.platform.util;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2", mo27670f = "NotificationAppHelper.kt", mo27671l = {152, 153}, mo27672m = "invokeSuspend")
public final class NotificationAppHelper$buildNotificationAppFilters$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.px2 $getAllContactGroup;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.vy2 $getApps;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase $notificationSettingsDatabase;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.en2 $sharedPrefs;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public java.lang.Object L$7;
    @DexIgnore
    public java.lang.Object L$8;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24431p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationAppHelper$buildNotificationAppFilters$2(com.fossil.blesdk.obfuscated.en2 en2, com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase notificationSettingsDatabase, com.fossil.blesdk.obfuscated.px2 px2, com.fossil.blesdk.obfuscated.vy2 vy2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$sharedPrefs = en2;
        this.$notificationSettingsDatabase = notificationSettingsDatabase;
        this.$getAllContactGroup = px2;
        this.$getApps = vy2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2 notificationAppHelper$buildNotificationAppFilters$2 = new com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2(this.$sharedPrefs, this.$notificationSettingsDatabase, this.$getAllContactGroup, this.$getApps, yb4);
        notificationAppHelper$buildNotificationAppFilters$2.f24431p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationAppHelper$buildNotificationAppFilters$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.util.List list;
        java.lang.Object obj2;
        java.util.List list2;
        com.fossil.blesdk.obfuscated.gh4 gh4;
        com.fossil.blesdk.obfuscated.gh4 gh42;
        kotlin.jvm.internal.Ref$IntRef ref$IntRef;
        kotlin.jvm.internal.Ref$IntRef ref$IntRef2;
        com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel;
        boolean z;
        com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel2;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object obj3;
        java.util.List list3;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f24431p$;
            ref$IntRef = new kotlin.jvm.internal.Ref$IntRef();
            ref$IntRef.element = 0;
            ref$IntRef2 = new kotlin.jvm.internal.Ref$IntRef();
            ref$IntRef2.element = 0;
            list = new java.util.ArrayList();
            z = this.$sharedPrefs.mo26963A();
            notificationSettingsModel2 = this.$notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(true);
            notificationSettingsModel = this.$notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.util.NotificationAppHelper.f24429a;
            local.mo33255d(a2, "buildNotificationAppFilters(), callSettingsTypeModel = " + notificationSettingsModel2 + ", messageSettingsTypeModel = " + notificationSettingsModel + ", isAllAppToggleEnabled = " + z);
            if (notificationSettingsModel2 != null) {
                ref$IntRef.element = notificationSettingsModel2.getSettingsType();
            } else {
                this.$notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new com.portfolio.platform.data.model.NotificationSettingsModel("AllowCallsFrom", 0, true));
            }
            if (notificationSettingsModel != null) {
                ref$IntRef2.element = notificationSettingsModel.getSettingsType();
            } else {
                this.$notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new com.portfolio.platform.data.model.NotificationSettingsModel("AllowMessagesFrom", 0, false));
            }
            com.fossil.blesdk.obfuscated.gh4 a3 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.util.C6940xc381eed5(this, ref$IntRef, ref$IntRef2, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            gh42 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.util.C6941x311fec40(this, list, z, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            this.L$0 = zg42;
            this.L$1 = ref$IntRef;
            this.L$2 = ref$IntRef2;
            this.L$3 = list;
            this.Z$0 = z;
            this.L$4 = notificationSettingsModel2;
            this.L$5 = notificationSettingsModel;
            this.L$6 = a3;
            this.L$7 = gh42;
            this.L$8 = list;
            this.label = 1;
            obj3 = a3.mo27693a(this);
            if (obj3 == a) {
                return a;
            }
            gh4 = a3;
            zg4 = zg42;
            list3 = list;
        } else if (i == 1) {
            list3 = (java.util.List) this.L$8;
            notificationSettingsModel2 = (com.portfolio.platform.data.model.NotificationSettingsModel) this.L$4;
            z = this.Z$0;
            ref$IntRef2 = (kotlin.jvm.internal.Ref$IntRef) this.L$2;
            ref$IntRef = (kotlin.jvm.internal.Ref$IntRef) this.L$1;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$6;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            gh42 = (com.fossil.blesdk.obfuscated.gh4) this.L$7;
            obj3 = obj;
            java.util.List list4 = (java.util.List) this.L$3;
            notificationSettingsModel = (com.portfolio.platform.data.model.NotificationSettingsModel) this.L$5;
            list = list4;
        } else if (i == 2) {
            list2 = (java.util.List) this.L$8;
            com.fossil.blesdk.obfuscated.gh4 gh43 = (com.fossil.blesdk.obfuscated.gh4) this.L$7;
            com.fossil.blesdk.obfuscated.gh4 gh44 = (com.fossil.blesdk.obfuscated.gh4) this.L$6;
            com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel3 = (com.portfolio.platform.data.model.NotificationSettingsModel) this.L$5;
            com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel4 = (com.portfolio.platform.data.model.NotificationSettingsModel) this.L$4;
            kotlin.jvm.internal.Ref$IntRef ref$IntRef3 = (kotlin.jvm.internal.Ref$IntRef) this.L$2;
            kotlin.jvm.internal.Ref$IntRef ref$IntRef4 = (kotlin.jvm.internal.Ref$IntRef) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            list = (java.util.List) this.L$3;
            obj2 = obj;
            list2.addAll((java.util.Collection) obj2);
            return list;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        list3.addAll((java.util.Collection) obj3);
        this.L$0 = zg4;
        this.L$1 = ref$IntRef;
        this.L$2 = ref$IntRef2;
        this.L$3 = list;
        this.Z$0 = z;
        this.L$4 = notificationSettingsModel2;
        this.L$5 = notificationSettingsModel;
        this.L$6 = gh4;
        this.L$7 = gh42;
        this.L$8 = list;
        this.label = 2;
        obj2 = gh42.mo27693a(this);
        if (obj2 == a) {
            return a;
        }
        list2 = list;
        list2.addAll((java.util.Collection) obj2);
        return list;
    }
}
