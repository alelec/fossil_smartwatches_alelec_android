package com.portfolio.platform.util;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1", mo27670f = "NetworkBoundResource.kt", mo27671l = {62, 69, 72, 92, 102, 115}, mo27672m = "invokeSuspend")
public final class NetworkBoundResource$fetchFromNetwork$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ androidx.lifecycle.LiveData $dbSource;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24418p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.util.NetworkBoundResource this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$1", mo27670f = "NetworkBoundResource.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$1 */
    public static final class C69301 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24419p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$1$a")
        /* renamed from: com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$1$a */
        public static final class C6931a<T> implements com.fossil.blesdk.obfuscated.C1548cc<S> {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69301 f24420a;

            @DexIgnore
            public C6931a(com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69301 r1) {
                this.f24420a = r1;
            }

            @DexIgnore
            /* renamed from: a */
            public final void mo8689a(ResultType resulttype) {
                this.f24420a.this$0.this$0.setValue(com.fossil.blesdk.obfuscated.os3.f17482e.mo29983b(resulttype));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C69301(com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1 networkBoundResource$fetchFromNetwork$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = networkBoundResource$fetchFromNetwork$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69301 r0 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69301(this.this$0, yb4);
            r0.f24419p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69301) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.result.mo8687a(this.this$0.$dbSource, new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69301.C6931a(this));
                this.this$0.this$0.result.mo8686a(this.this$0.$dbSource);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$2")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$2", mo27670f = "NetworkBoundResource.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$2 */
    public static final class C69322 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24421p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$2$a")
        /* renamed from: com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$2$a */
        public static final class C6933a<T> implements com.fossil.blesdk.obfuscated.C1548cc<S> {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69322 f24422a;

            @DexIgnore
            public C6933a(com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69322 r1) {
                this.f24422a = r1;
            }

            @DexIgnore
            /* renamed from: a */
            public final void mo8689a(ResultType resulttype) {
                this.f24422a.this$0.this$0.setValue(com.fossil.blesdk.obfuscated.os3.f17482e.mo29984c(resulttype));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C69322(com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1 networkBoundResource$fetchFromNetwork$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = networkBoundResource$fetchFromNetwork$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69322 r0 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69322(this.this$0, yb4);
            r0.f24421p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69322) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("NetworkBoundResource", "set value dbSource fetched from network response null");
                this.this$0.this$0.result.mo8687a(this.this$0.this$0.loadFromDb(), new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69322.C6933a(this));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$3")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$3", mo27670f = "NetworkBoundResource.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$3 */
    public static final class C69343 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24423p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$3$a")
        /* renamed from: com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$3$a */
        public static final class C6935a<T> implements com.fossil.blesdk.obfuscated.C1548cc<S> {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69343 f24424a;

            @DexIgnore
            public C6935a(com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69343 r1) {
                this.f24424a = r1;
            }

            @DexIgnore
            /* renamed from: a */
            public final void mo8689a(ResultType resulttype) {
                this.f24424a.this$0.this$0.setValue(com.fossil.blesdk.obfuscated.os3.f17482e.mo29984c(resulttype));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C69343(com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1 networkBoundResource$fetchFromNetwork$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = networkBoundResource$fetchFromNetwork$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69343 r0 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69343(this.this$0, yb4);
            r0.f24423p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69343) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("NetworkBoundResource", "set value dbSource fetched from network success");
                this.this$0.this$0.result.mo8687a(this.this$0.this$0.loadFromDb(), new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69343.C6935a(this));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$4")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$4", mo27670f = "NetworkBoundResource.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$4 */
    public static final class C69364 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24425p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$4$a")
        /* renamed from: com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$4$a */
        public static final class C6937a<T> implements com.fossil.blesdk.obfuscated.C1548cc<S> {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69364 f24426a;

            @DexIgnore
            public C6937a(com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69364 r1) {
                this.f24426a = r1;
            }

            @DexIgnore
            /* renamed from: a */
            public final void mo8689a(ResultType resulttype) {
                this.f24426a.this$0.this$0.setValue(com.fossil.blesdk.obfuscated.os3.f17482e.mo29983b(resulttype));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C69364(com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1 networkBoundResource$fetchFromNetwork$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = networkBoundResource$fetchFromNetwork$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69364 r0 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69364(this.this$0, yb4);
            r0.f24425p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69364) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.result.mo8687a(this.this$0.this$0.loadFromDb(), new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69364.C6937a(this));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$5")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$5", mo27670f = "NetworkBoundResource.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$5 */
    public static final class C69385 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.qo2 $response;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24427p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$5$a")
        /* renamed from: com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$5$a */
        public static final class C6939a<T> implements com.fossil.blesdk.obfuscated.C1548cc<S> {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69385 f24428a;

            @DexIgnore
            public C6939a(com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69385 r1) {
                this.f24428a = r1;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:3:0x0020, code lost:
                if (r3 != null) goto L_0x0035;
             */
            @DexIgnore
            /* renamed from: a */
            public final void mo8689a(ResultType resulttype) {
                java.lang.String str;
                com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69385 r0 = this.f24428a;
                com.portfolio.platform.util.NetworkBoundResource networkBoundResource = r0.this$0.this$0;
                com.fossil.blesdk.obfuscated.os3.C4811a aVar = com.fossil.blesdk.obfuscated.os3.f17482e;
                int a = ((com.fossil.blesdk.obfuscated.po2) r0.$response).mo30176a();
                com.portfolio.platform.data.model.ServerError c = ((com.fossil.blesdk.obfuscated.po2) this.f24428a.$response).mo30178c();
                if (c != null) {
                    str = c.getUserMessage();
                }
                com.portfolio.platform.data.model.ServerError c2 = ((com.fossil.blesdk.obfuscated.po2) this.f24428a.$response).mo30178c();
                str = c2 != null ? c2.getMessage() : null;
                if (str == null) {
                    str = "";
                }
                networkBoundResource.setValue(aVar.mo29981a(a, str, resulttype));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C69385(com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1 networkBoundResource$fetchFromNetwork$1, com.fossil.blesdk.obfuscated.qo2 qo2, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = networkBoundResource$fetchFromNetwork$1;
            this.$response = qo2;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69385 r0 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69385(this.this$0, this.$response, yb4);
            r0.f24427p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69385) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.result.mo8687a(this.this$0.$dbSource, new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69385.C6939a(this));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NetworkBoundResource$fetchFromNetwork$1(com.portfolio.platform.util.NetworkBoundResource networkBoundResource, androidx.lifecycle.LiveData liveData, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = networkBoundResource;
        this.$dbSource = liveData;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1 networkBoundResource$fetchFromNetwork$1 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1(this.this$0, this.$dbSource, yb4);
        networkBoundResource$fetchFromNetwork$1.f24418p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return networkBoundResource$fetchFromNetwork$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x004a, code lost:
        r7 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$response$1(r6, (com.fossil.blesdk.obfuscated.yb4) null);
        r6.L$0 = r1;
        r6.label = 2;
        r7 = com.portfolio.platform.response.ResponseKt.m32232a(r7, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0058, code lost:
        if (r7 != r0) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005a, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x005b, code lost:
        r7 = (com.fossil.blesdk.obfuscated.qo2) r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005f, code lost:
        if ((r7 instanceof com.fossil.blesdk.obfuscated.ro2) == false) goto L_0x010c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0061, code lost:
        r3 = (com.fossil.blesdk.obfuscated.ro2) r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0068, code lost:
        if (r3.mo30673a() != null) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006a, code lost:
        r3 = com.fossil.blesdk.obfuscated.nh4.m25693c();
        r4 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69322(r6, (com.fossil.blesdk.obfuscated.yb4) null);
        r6.L$0 = r1;
        r6.L$1 = r7;
        r6.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x007e, code lost:
        if (com.fossil.blesdk.obfuscated.yf4.m30997a(r3, r4, r6) != r0) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0080, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0081, code lost:
        r4 = r6.this$0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x008b, code lost:
        if (r4.processContinueFetching(r4.processResponse(r3)) == false) goto L_0x00b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0093, code lost:
        if (r6.this$0.isFromCache == false) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0095, code lost:
        r6.this$0.isFromCache = r3.mo30674b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a2, code lost:
        if (r3.mo30674b() != false) goto L_0x00ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a4, code lost:
        r7 = r6.this$0;
        r7.saveCallResult(r7.processResponse(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ad, code lost:
        r6.this$0.fetchFromNetwork(r6.$dbSource);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00bc, code lost:
        if (r6.this$0.isFromCache == false) goto L_0x00c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00be, code lost:
        r6.this$0.isFromCache = r3.mo30674b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00cb, code lost:
        if (r3.mo30674b() != false) goto L_0x00d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00cd, code lost:
        r4 = r6.this$0;
        r4.saveCallResult(r4.processResponse(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00dc, code lost:
        if (r6.this$0.isFromCache != false) goto L_0x00f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00de, code lost:
        r3 = com.fossil.blesdk.obfuscated.nh4.m25693c();
        r4 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69343(r6, (com.fossil.blesdk.obfuscated.yb4) null);
        r6.L$0 = r1;
        r6.L$1 = r7;
        r6.label = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00f2, code lost:
        if (com.fossil.blesdk.obfuscated.yf4.m30997a(r3, r4, r6) != r0) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00f4, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00f5, code lost:
        r3 = com.fossil.blesdk.obfuscated.nh4.m25693c();
        r4 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69364(r6, (com.fossil.blesdk.obfuscated.yb4) null);
        r6.L$0 = r1;
        r6.L$1 = r7;
        r6.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0109, code lost:
        if (com.fossil.blesdk.obfuscated.yf4.m30997a(r3, r4, r6) != r0) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x010b, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x010e, code lost:
        if ((r7 instanceof com.fossil.blesdk.obfuscated.po2) == false) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0110, code lost:
        r6.this$0.onFetchFailed(((com.fossil.blesdk.obfuscated.po2) r7).mo30179d());
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("NetworkBoundResource", "set value dbSource fetched from network failed");
        r3 = com.fossil.blesdk.obfuscated.nh4.m25693c();
        r4 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69385(r6, r7, (com.fossil.blesdk.obfuscated.yb4) null);
        r6.L$0 = r1;
        r6.L$1 = r7;
        r6.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x013d, code lost:
        if (com.fossil.blesdk.obfuscated.yf4.m30997a(r3, r4, r6) != r0) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x013f, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0142, code lost:
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        switch (this.label) {
            case 0:
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg42 = this.f24418p$;
                com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
                com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69301 r3 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1.C69301(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg42;
                this.label = 1;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r3, this) != a) {
                    zg4 = zg42;
                    break;
                } else {
                    return a;
                }
            case 1:
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 2:
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 3:
            case 4:
            case 5:
            case 6:
                com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            default:
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
