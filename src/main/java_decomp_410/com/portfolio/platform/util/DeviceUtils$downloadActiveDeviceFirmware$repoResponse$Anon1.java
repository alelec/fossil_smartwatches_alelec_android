package com.portfolio.platform.util;

import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$repoResponse$Anon1", f = "DeviceUtils.kt", l = {76}, m = "invokeSuspend")
public final class DeviceUtils$downloadActiveDeviceFirmware$repoResponse$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<ApiResponse<Firmware>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $model;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceUtils this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceUtils$downloadActiveDeviceFirmware$repoResponse$Anon1(DeviceUtils deviceUtils, String str, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = deviceUtils;
        this.$model = str;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new DeviceUtils$downloadActiveDeviceFirmware$repoResponse$Anon1(this.this$Anon0, this.$model, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((DeviceUtils$downloadActiveDeviceFirmware$repoResponse$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            GuestApiService a2 = this.this$Anon0.a();
            String h = PortfolioApp.W.c().h();
            String str = this.$model;
            kd4.a((Object) str, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            this.label = 1;
            obj = a2.getFirmwares(h, str, "android", this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
