package com.portfolio.platform.util;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$contactMessageDefer$1", f = "NotificationAppHelper.kt", l = {101}, m = "invokeSuspend")
public final class NotificationAppHelper$buildNotificationAppFilters$2$contactMessageDefer$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $callSettingsType;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $messageSettingsType;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationAppHelper$buildNotificationAppFilters$2$contactMessageDefer$1(com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2 notificationAppHelper$buildNotificationAppFilters$2, kotlin.jvm.internal.Ref$IntRef ref$IntRef, kotlin.jvm.internal.Ref$IntRef ref$IntRef2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = notificationAppHelper$buildNotificationAppFilters$2;
        this.$callSettingsType = ref$IntRef;
        this.$messageSettingsType = ref$IntRef2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$contactMessageDefer$1 notificationAppHelper$buildNotificationAppFilters$2$contactMessageDefer$1 = new com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$contactMessageDefer$1(this.this$0, this.$callSettingsType, this.$messageSettingsType, yb4);
        notificationAppHelper$buildNotificationAppFilters$2$contactMessageDefer$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationAppHelper$buildNotificationAppFilters$2$contactMessageDefer$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$contactMessageDefer$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object obj2;
        java.util.List list;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.p$;
            java.util.ArrayList arrayList = new java.util.ArrayList();
            com.fossil.blesdk.obfuscated.px2 px2 = this.this$0.$getAllContactGroup;
            this.L$0 = zg4;
            this.L$1 = arrayList;
            this.label = 1;
            obj2 = com.fossil.blesdk.obfuscated.w52.a(px2, null, this);
            if (obj2 == a) {
                return a;
            }
            list = arrayList;
        } else if (i == 1) {
            list = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
            obj2 = obj;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.CoroutineUseCase.c cVar = (com.portfolio.platform.CoroutineUseCase.c) obj2;
        if (cVar instanceof com.fossil.blesdk.obfuscated.px2.d) {
            java.util.List<T> d = com.fossil.blesdk.obfuscated.kb4.d(((com.fossil.blesdk.obfuscated.px2.d) cVar).a());
            int i2 = this.$callSettingsType.element;
            if (i2 == 0) {
                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification = r8;
                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification2 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName.getAppName(), aApplicationName.getBundleCrc(), aApplicationName.getGroupId(), aApplicationName.getPackageName(), aApplicationName.getIconResourceId(), aApplicationName.getIconFwPath(), aApplicationName.getNotificationType());
                list.add(new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification));
            } else if (i2 == 1) {
                int size = d.size();
                for (int i3 = 0; i3 < size; i3++) {
                    com.fossil.wearables.fsl.contact.ContactGroup contactGroup = (com.fossil.wearables.fsl.contact.ContactGroup) d.get(i3);
                    com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName2 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                    com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification3 = r10;
                    com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification4 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName2.getAppName(), aApplicationName2.getBundleCrc(), aApplicationName2.getGroupId(), aApplicationName2.getPackageName(), aApplicationName2.getIconResourceId(), aApplicationName2.getIconFwPath(), aApplicationName2.getNotificationType());
                    com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter appNotificationFilter = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification3);
                    java.util.List<com.fossil.wearables.fsl.contact.Contact> contacts = contactGroup.getContacts();
                    com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) contacts, "item.contacts");
                    if (!contacts.isEmpty()) {
                        com.fossil.wearables.fsl.contact.Contact contact = contactGroup.getContacts().get(0);
                        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) contact, "item.contacts[0]");
                        appNotificationFilter.setSender(contact.getDisplayName());
                        list.add(appNotificationFilter);
                    }
                }
            }
            int i4 = this.$messageSettingsType.element;
            if (i4 == 0) {
                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName3 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.MESSAGES;
                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification5 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName3.getAppName(), aApplicationName3.getBundleCrc(), aApplicationName3.getGroupId(), aApplicationName3.getPackageName(), aApplicationName3.getIconResourceId(), aApplicationName3.getIconFwPath(), aApplicationName3.getNotificationType());
                list.add(new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification5));
            } else if (i4 == 1) {
                int size2 = d.size();
                int i5 = 0;
                while (i5 < size2) {
                    com.fossil.wearables.fsl.contact.ContactGroup contactGroup2 = (com.fossil.wearables.fsl.contact.ContactGroup) d.get(i5);
                    com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName4 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.MESSAGES;
                    com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification6 = r9;
                    com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification7 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName4.getAppName(), aApplicationName4.getBundleCrc(), aApplicationName4.getGroupId(), aApplicationName4.getPackageName(), aApplicationName4.getIconResourceId(), aApplicationName4.getIconFwPath(), aApplicationName4.getNotificationType());
                    java.util.List<com.fossil.wearables.fsl.contact.Contact> contacts2 = contactGroup2.getContacts();
                    com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) contacts2, "item.contacts");
                    if (!contacts2.isEmpty()) {
                        com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter appNotificationFilter2 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification6);
                        com.fossil.wearables.fsl.contact.Contact contact2 = contactGroup2.getContacts().get(0);
                        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) contact2, "item.contacts[0]");
                        appNotificationFilter2.setSender(contact2.getDisplayName());
                        list.add(appNotificationFilter2);
                    }
                    i5++;
                }
            }
        }
        return list;
    }
}
