package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.util.DeviceUtils$isDeviceDianaEV1Java$Anon1", f = "DeviceUtils.kt", l = {}, m = "invokeSuspend")
public final class DeviceUtils$isDeviceDianaEV1Java$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Boolean>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceSerial;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceUtils this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceUtils$isDeviceDianaEV1Java$Anon1(DeviceUtils deviceUtils, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = deviceUtils;
        this.$deviceSerial = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DeviceUtils$isDeviceDianaEV1Java$Anon1 deviceUtils$isDeviceDianaEV1Java$Anon1 = new DeviceUtils$isDeviceDianaEV1Java$Anon1(this.this$Anon0, this.$deviceSerial, yb4);
        deviceUtils$isDeviceDianaEV1Java$Anon1.p$ = (zg4) obj;
        return deviceUtils$isDeviceDianaEV1Java$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DeviceUtils$isDeviceDianaEV1Java$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return dc4.a(this.this$Anon0.a(this.$deviceSerial));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
