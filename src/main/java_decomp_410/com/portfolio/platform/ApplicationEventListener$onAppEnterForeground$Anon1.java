package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$Anon1", f = "ApplicationEventListener.kt", l = {60}, m = "invokeSuspend")
public final class ApplicationEventListener$onAppEnterForeground$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ApplicationEventListener this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ApplicationEventListener$onAppEnterForeground$Anon1(ApplicationEventListener applicationEventListener, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = applicationEventListener;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ApplicationEventListener$onAppEnterForeground$Anon1 applicationEventListener$onAppEnterForeground$Anon1 = new ApplicationEventListener$onAppEnterForeground$Anon1(this.this$Anon0, yb4);
        applicationEventListener$onAppEnterForeground$Anon1.p$ = (zg4) obj;
        return applicationEventListener$onAppEnterForeground$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ApplicationEventListener$onAppEnterForeground$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            PortfolioApp c = this.this$Anon0.a;
            this.L$Anon0 = zg4;
            this.label = 1;
            if (c.a((yb4<? super qa4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
