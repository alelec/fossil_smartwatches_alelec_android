package com.portfolio.platform.uirenew.home.details.sleep;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1", mo27670f = "SleepDetailPresenter.kt", mo27671l = {146, 170, 171}, mo27672m = "invokeSuspend")
public final class SleepDetailPresenter$setDate$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $date;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23758p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$1", mo27670f = "SleepDetailPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$1 */
    public static final class C67341 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.Date>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23759p$;

        @DexIgnore
        public C67341(com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1.C67341 r0 = new com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1.C67341(yb4);
            r0.f23759p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1.C67341) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34546k();
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepDetailPresenter$setDate$1(com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter sleepDetailPresenter, java.util.Date date, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = sleepDetailPresenter;
        this.$date = date;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1 sleepDetailPresenter$setDate$1 = new com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1(this.this$0, this.$date, yb4);
        sleepDetailPresenter$setDate$1.f23758p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return sleepDetailPresenter$setDate$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:28:0x019e A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x019f  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x01b0  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x01c2  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object obj2;
        com.portfolio.platform.data.model.room.sleep.MFSleepDay mFSleepDay;
        java.util.List list;
        kotlin.Pair pair;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Boolean bool;
        android.util.Pair<java.util.Date, java.util.Date> pair2;
        java.lang.Object obj3;
        boolean z;
        com.fossil.blesdk.obfuscated.zg4 zg42;
        android.util.Pair<java.util.Date, java.util.Date> a;
        java.lang.Object obj4;
        com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter sleepDetailPresenter;
        java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg42 = this.f23758p$;
            if (this.this$0.f23741f == null) {
                sleepDetailPresenter = this.this$0;
                com.fossil.blesdk.obfuscated.ug4 a3 = sleepDetailPresenter.mo31440b();
                com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1.C67341 r9 = new com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1.C67341((com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg42;
                this.L$1 = sleepDetailPresenter;
                this.label = 1;
                obj4 = com.fossil.blesdk.obfuscated.yf4.m30997a(a3, r9, this);
                if (obj4 == a2) {
                    return a2;
                }
            }
            zg4 = zg42;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("SleepDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.f23741f);
            this.this$0.f23742g = this.$date;
            z = com.fossil.blesdk.obfuscated.rk2.m27391c(this.this$0.f23741f, this.$date);
            java.lang.Boolean s = com.fossil.blesdk.obfuscated.rk2.m27414s(this.$date);
            com.fossil.blesdk.obfuscated.wf3 n = this.this$0.f23753r;
            java.util.Date date = this.$date;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) s, "isToday");
            n.mo32016a(date, z, s.booleanValue(), !com.fossil.blesdk.obfuscated.rk2.m27391c(new java.util.Date(), this.$date));
            a = com.fossil.blesdk.obfuscated.rk2.m27355a(this.$date, this.this$0.f23741f);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            pair = (kotlin.Pair) this.this$0.f23743h.mo2275a();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local2.mo33255d("SleepDetailPresenter", "setDate - rangeDateValue=" + pair + ", newRange=" + new kotlin.Pair(a.first, a.second));
            if (pair != null || !com.fossil.blesdk.obfuscated.rk2.m27396d((java.util.Date) pair.getFirst(), (java.util.Date) a.first) || !com.fossil.blesdk.obfuscated.rk2.m27396d((java.util.Date) pair.getSecond(), (java.util.Date) a.second)) {
                this.this$0.f23745j = false;
                this.this$0.f23746k = false;
                this.this$0.f23743h.mo2280a(new kotlin.Pair(a.first, a.second));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            com.fossil.blesdk.obfuscated.ug4 a4 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$summary$1 sleepDetailPresenter$setDate$1$summary$1 = new com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$summary$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.Z$0 = z;
            this.L$1 = s;
            this.L$2 = a;
            this.L$3 = pair;
            this.label = 2;
            obj3 = com.fossil.blesdk.obfuscated.yf4.m30997a(a4, sleepDetailPresenter$setDate$1$summary$1, this);
            if (obj3 == a2) {
                return a2;
            }
            pair2 = a;
            bool = s;
            com.portfolio.platform.data.model.room.sleep.MFSleepDay mFSleepDay2 = (com.portfolio.platform.data.model.room.sleep.MFSleepDay) obj3;
            com.fossil.blesdk.obfuscated.ug4 a5 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$sessions$1 sleepDetailPresenter$setDate$1$sessions$1 = new com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$sessions$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.Z$0 = z;
            this.L$1 = bool;
            this.L$2 = pair2;
            this.L$3 = pair;
            this.L$4 = mFSleepDay2;
            this.label = 3;
            obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(a5, sleepDetailPresenter$setDate$1$sessions$1, this);
            if (obj2 != a2) {
            }
        } else if (i == 1) {
            sleepDetailPresenter = (com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter) this.L$1;
            zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            obj4 = obj;
        } else if (i == 2) {
            bool = (java.lang.Boolean) this.L$1;
            boolean z2 = this.Z$0;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            pair = (kotlin.Pair) this.L$3;
            pair2 = (android.util.Pair) this.L$2;
            z = z2;
            obj3 = obj;
            com.portfolio.platform.data.model.room.sleep.MFSleepDay mFSleepDay22 = (com.portfolio.platform.data.model.room.sleep.MFSleepDay) obj3;
            com.fossil.blesdk.obfuscated.ug4 a52 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$sessions$1 sleepDetailPresenter$setDate$1$sessions$12 = new com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$sessions$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.Z$0 = z;
            this.L$1 = bool;
            this.L$2 = pair2;
            this.L$3 = pair;
            this.L$4 = mFSleepDay22;
            this.label = 3;
            obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(a52, sleepDetailPresenter$setDate$1$sessions$12, this);
            if (obj2 != a2) {
                return a2;
            }
            mFSleepDay = mFSleepDay22;
            list = (java.util.List) obj2;
            if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23749n, (java.lang.Object) mFSleepDay)) {
            }
            if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23750o, (java.lang.Object) list)) {
            }
            this.this$0.f23753r.mo32015a(mFSleepDay);
            com.fossil.blesdk.obfuscated.fi4 unused = this.this$0.mo41436k();
            com.fossil.blesdk.obfuscated.fi4 unused2 = this.this$0.mo41437l();
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 3) {
            mFSleepDay = (com.portfolio.platform.data.model.room.sleep.MFSleepDay) this.L$4;
            kotlin.Pair pair3 = (kotlin.Pair) this.L$3;
            android.util.Pair pair4 = (android.util.Pair) this.L$2;
            java.lang.Boolean bool2 = (java.lang.Boolean) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            obj2 = obj;
            list = (java.util.List) obj2;
            if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23749n, (java.lang.Object) mFSleepDay)) {
                this.this$0.f23749n = mFSleepDay;
            }
            if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23750o, (java.lang.Object) list)) {
                this.this$0.f23750o = list;
            }
            this.this$0.f23753r.mo32015a(mFSleepDay);
            if (this.this$0.f23745j && this.this$0.f23746k) {
                com.fossil.blesdk.obfuscated.fi4 unused3 = this.this$0.mo41436k();
                com.fossil.blesdk.obfuscated.fi4 unused4 = this.this$0.mo41437l();
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        sleepDetailPresenter.f23741f = (java.util.Date) obj4;
        zg4 = zg42;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local3.mo33255d("SleepDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.f23741f);
        this.this$0.f23742g = this.$date;
        z = com.fossil.blesdk.obfuscated.rk2.m27391c(this.this$0.f23741f, this.$date);
        java.lang.Boolean s2 = com.fossil.blesdk.obfuscated.rk2.m27414s(this.$date);
        com.fossil.blesdk.obfuscated.wf3 n2 = this.this$0.f23753r;
        java.util.Date date2 = this.$date;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) s2, "isToday");
        n2.mo32016a(date2, z, s2.booleanValue(), !com.fossil.blesdk.obfuscated.rk2.m27391c(new java.util.Date(), this.$date));
        a = com.fossil.blesdk.obfuscated.rk2.m27355a(this.$date, this.this$0.f23741f);
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
        pair = (kotlin.Pair) this.this$0.f23743h.mo2275a();
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local22 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local22.mo33255d("SleepDetailPresenter", "setDate - rangeDateValue=" + pair + ", newRange=" + new kotlin.Pair(a.first, a.second));
        if (pair != null) {
        }
        this.this$0.f23745j = false;
        this.this$0.f23746k = false;
        this.this$0.f23743h.mo2280a(new kotlin.Pair(a.first, a.second));
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
