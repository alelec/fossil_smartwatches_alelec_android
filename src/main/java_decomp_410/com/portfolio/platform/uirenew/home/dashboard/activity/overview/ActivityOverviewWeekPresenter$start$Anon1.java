package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.u93;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.v93;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.enums.Status;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$Anon1", f = "ActivityOverviewWeekPresenter.kt", l = {50}, m = "invokeSuspend")
public final class ActivityOverviewWeekPresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivityOverviewWeekPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2<T> implements cc<os3<? extends List<ActivitySummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewWeekPresenter$start$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @gc4(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$Anon1$Anon2$Anon1", f = "ActivityOverviewWeekPresenter.kt", l = {62}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public Object L$Anon1;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon2 this$Anon0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$Anon1$Anon2$Anon1$Anon1")
            @gc4(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$Anon1$Anon2$Anon1$Anon1", f = "ActivityOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$Anon1$Anon2$Anon1$Anon1  reason: collision with other inner class name */
            public static final class C0149Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super BarChart.c>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public zg4 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Anon1 this$Anon0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0149Anon1(Anon1 anon1, yb4 yb4) {
                    super(2, yb4);
                    this.this$Anon0 = anon1;
                }

                @DexIgnore
                public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                    kd4.b(yb4, "completion");
                    C0149Anon1 anon1 = new C0149Anon1(this.this$Anon0, yb4);
                    anon1.p$ = (zg4) obj;
                    return anon1;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0149Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    cc4.a();
                    if (this.label == 0) {
                        na4.a(obj);
                        ActivityOverviewWeekPresenter activityOverviewWeekPresenter = this.this$Anon0.this$Anon0.a.this$Anon0;
                        return activityOverviewWeekPresenter.a(ActivityOverviewWeekPresenter.e(activityOverviewWeekPresenter), (List<ActivitySummary>) this.this$Anon0.$data);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon2 anon2, List list, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon2;
                this.$data = list;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, this.$data, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ActivityOverviewWeekPresenter activityOverviewWeekPresenter;
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 zg4 = this.p$;
                    ActivityOverviewWeekPresenter activityOverviewWeekPresenter2 = this.this$Anon0.a.this$Anon0;
                    ug4 a2 = activityOverviewWeekPresenter2.b();
                    C0149Anon1 anon1 = new C0149Anon1(this, (yb4) null);
                    this.L$Anon0 = zg4;
                    this.L$Anon1 = activityOverviewWeekPresenter2;
                    this.label = 1;
                    obj = yf4.a(a2, anon1, this);
                    if (obj == a) {
                        return a;
                    }
                    activityOverviewWeekPresenter = activityOverviewWeekPresenter2;
                } else if (i == 1) {
                    activityOverviewWeekPresenter = (ActivityOverviewWeekPresenter) this.L$Anon1;
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                activityOverviewWeekPresenter.h = (BarChart.c) obj;
                u93 g = this.this$Anon0.a.this$Anon0.i;
                BarChart.c c = this.this$Anon0.a.this$Anon0.h;
                if (c == null) {
                    c = new BarChart.c(0, 0, (ArrayList) null, 7, (fd4) null);
                }
                g.a(c);
                return qa4.a;
            }
        }

        @DexIgnore
        public Anon2(ActivityOverviewWeekPresenter$start$Anon1 activityOverviewWeekPresenter$start$Anon1) {
            this.a = activityOverviewWeekPresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(os3<? extends List<ActivitySummary>> os3) {
            Status a2 = os3.a();
            List list = (List) os3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mActivitySummaries -- activitySummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("ActivityOverviewWeekPresenter", sb.toString());
            if (a2 != Status.DATABASE_LOADING) {
                fi4 unused = ag4.b(this.a.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, list, (yb4) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityOverviewWeekPresenter$start$Anon1(ActivityOverviewWeekPresenter activityOverviewWeekPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = activityOverviewWeekPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ActivityOverviewWeekPresenter$start$Anon1 activityOverviewWeekPresenter$start$Anon1 = new ActivityOverviewWeekPresenter$start$Anon1(this.this$Anon0, yb4);
        activityOverviewWeekPresenter$start$Anon1.p$ = (zg4) obj;
        return activityOverviewWeekPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActivityOverviewWeekPresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00e0  */
    public final Object invokeSuspend(Object obj) {
        u93 g;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            if (this.this$Anon0.f == null || !rk2.s(ActivityOverviewWeekPresenter.e(this.this$Anon0)).booleanValue()) {
                this.this$Anon0.f = new Date();
                ug4 a2 = this.this$Anon0.b();
                ActivityOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1 activityOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1 = new ActivityOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = yf4.a(a2, activityOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1, this);
                if (obj == a) {
                    return a;
                }
            }
            LiveData b = this.this$Anon0.g;
            g = this.this$Anon0.i;
            if (g == null) {
                b.a((v93) g, new Anon2(this));
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ActivityOverviewWeekPresenter", "start - mDate=" + ActivityOverviewWeekPresenter.e(this.this$Anon0));
                return qa4.a;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekFragment");
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair = (Pair) obj;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("ActivityOverviewWeekPresenter", "start - startDate=" + ((Date) pair.getFirst()) + ", endDate=" + ((Date) pair.getSecond()));
        ActivityOverviewWeekPresenter activityOverviewWeekPresenter = this.this$Anon0;
        activityOverviewWeekPresenter.g = activityOverviewWeekPresenter.k.getSummaries((Date) pair.getFirst(), (Date) pair.getSecond(), false);
        LiveData b2 = this.this$Anon0.g;
        g = this.this$Anon0.i;
        if (g == null) {
        }
    }
}
