package com.portfolio.platform.uirenew.home.alerts.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$otherAppDeffer$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
public final class HomeAlertsPresenter$setNotificationFilterToDevice$1$otherAppDeffer$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<? extends com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter>>, java.lang.Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeAlertsPresenter$setNotificationFilterToDevice$1$otherAppDeffer$1(com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1 homeAlertsPresenter$setNotificationFilterToDevice$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homeAlertsPresenter$setNotificationFilterToDevice$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$otherAppDeffer$1 homeAlertsPresenter$setNotificationFilterToDevice$1$otherAppDeffer$1 = new com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$otherAppDeffer$1(this.this$0, yb4);
        homeAlertsPresenter$setNotificationFilterToDevice$1$otherAppDeffer$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeAlertsPresenter$setNotificationFilterToDevice$1$otherAppDeffer$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$otherAppDeffer$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            return com.fossil.blesdk.obfuscated.ls3.a(this.this$0.this$0.i(), false, 1, (java.lang.Object) null);
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
