package com.portfolio.platform.uirenew.home.customize.diana.watchapps.search;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.r53;
import com.fossil.blesdk.obfuscated.s53;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppSearchPresenter extends r53 {
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h; // = "empty";
    @DexIgnore
    public String i;
    @DexIgnore
    public ArrayList<WatchApp> j; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<WatchApp> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ s53 l;
    @DexIgnore
    public /* final */ WatchAppRepository m;
    @DexIgnore
    public /* final */ en2 n;

    @DexIgnore
    public WatchAppSearchPresenter(s53 s53, WatchAppRepository watchAppRepository, en2 en2) {
        kd4.b(s53, "mView");
        kd4.b(watchAppRepository, "mWatchAppRepository");
        kd4.b(en2, "sharedPreferencesManager");
        this.l = s53;
        this.m = watchAppRepository;
        this.n = en2;
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.i = "";
        this.l.u();
        i();
    }

    @DexIgnore
    public final void i() {
        if (this.k.isEmpty()) {
            this.l.b(a((List<WatchApp>) kb4.d(this.j)));
        } else {
            this.l.e(a((List<WatchApp>) kb4.d(this.k)));
        }
        if (!TextUtils.isEmpty(this.i)) {
            s53 s53 = this.l;
            String str = this.i;
            if (str != null) {
                s53.a(str);
                String str2 = this.i;
                if (str2 != null) {
                    a(str2);
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void j() {
        this.l.a(this);
    }

    @DexIgnore
    public void f() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new WatchAppSearchPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(String str, String str2, String str3) {
        kd4.b(str, "watchAppTop");
        kd4.b(str2, "watchAppMiddle");
        kd4.b(str3, "watchAppBottom");
        this.f = str;
        this.h = str3;
        this.g = str2;
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new WatchAppSearchPresenter$search$Anon1(this, str, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(WatchApp watchApp) {
        kd4.b(watchApp, "selectedWatchApp");
        List<String> y = this.n.y();
        kd4.a((Object) y, "sharedPreferencesManager.watchAppSearchedIdsRecent");
        if (!y.contains(watchApp.getWatchappId())) {
            y.add(0, watchApp.getWatchappId());
            if (y.size() > 5) {
                y = y.subList(0, 5);
            }
            this.n.d(y);
        }
        this.l.a(watchApp);
    }

    @DexIgnore
    public final List<Pair<WatchApp, String>> a(List<WatchApp> list) {
        ArrayList arrayList = new ArrayList();
        for (WatchApp next : list) {
            if (!kd4.a((Object) next.getWatchappId(), (Object) "empty")) {
                String watchappId = next.getWatchappId();
                if (kd4.a((Object) watchappId, (Object) this.f)) {
                    arrayList.add(new Pair(next, ViewHierarchy.DIMENSION_TOP_KEY));
                } else if (kd4.a((Object) watchappId, (Object) this.g)) {
                    arrayList.add(new Pair(next, "middle"));
                } else if (kd4.a((Object) watchappId, (Object) this.h)) {
                    arrayList.add(new Pair(next, "bottom"));
                } else {
                    arrayList.add(new Pair(next, ""));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        if (bundle != null) {
            bundle.putString(ViewHierarchy.DIMENSION_TOP_KEY, this.f);
            bundle.putString("middle", this.g);
            bundle.putString("bottom", this.h);
        }
        return bundle;
    }
}
