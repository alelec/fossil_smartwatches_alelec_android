package com.portfolio.platform.uirenew.home.profile.edit;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.ck2;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lv;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nk2;
import com.fossil.blesdk.obfuscated.oo;
import com.fossil.blesdk.obfuscated.pp;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rv;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.PortfolioApp;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1", f = "ProfileEditViewModel.kt", l = {}, m = "invokeSuspend")
public final class ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Bitmap>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Uri $imageUri;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1(Uri uri, yb4 yb4) {
        super(2, yb4);
        this.$imageUri = uri;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1 profileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1 = new ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1(this.$imageUri, yb4);
        profileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1.p$ = (zg4) obj;
        return profileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return ck2.a((Context) PortfolioApp.W.c()).e().a(this.$imageUri).a((lv<?>) ((rv) ((rv) new rv().a(pp.a)).a(true)).a((oo<Bitmap>) new nk2())).c(200, 200).get();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
