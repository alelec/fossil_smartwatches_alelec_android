package com.portfolio.platform.uirenew.home.details.activetime;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.ce3;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$setDate$Anon1", f = "ActiveTimeDetailPresenter.kt", l = {173, 196, 197}, m = "invokeSuspend")
public final class ActiveTimeDetailPresenter$setDate$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActiveTimeDetailPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$setDate$Anon1$Anon1", f = "ActiveTimeDetailPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Date>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;

        @DexIgnore
        public Anon1(yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return PortfolioApp.W.c().k();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActiveTimeDetailPresenter$setDate$Anon1(ActiveTimeDetailPresenter activeTimeDetailPresenter, Date date, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = activeTimeDetailPresenter;
        this.$date = date;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ActiveTimeDetailPresenter$setDate$Anon1 activeTimeDetailPresenter$setDate$Anon1 = new ActiveTimeDetailPresenter$setDate$Anon1(this.this$Anon0, this.$date, yb4);
        activeTimeDetailPresenter$setDate$Anon1.p$ = (zg4) obj;
        return activeTimeDetailPresenter$setDate$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActiveTimeDetailPresenter$setDate$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:28:0x019e A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x019f  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x01b0  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x01c2  */
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        ActivitySummary activitySummary;
        List list;
        Pair pair;
        zg4 zg4;
        Boolean bool;
        android.util.Pair<Date, Date> pair2;
        Object obj3;
        boolean z;
        zg4 zg42;
        android.util.Pair<Date, Date> a;
        Object obj4;
        ActiveTimeDetailPresenter activeTimeDetailPresenter;
        Object a2 = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg42 = this.p$;
            if (this.this$Anon0.f == null) {
                activeTimeDetailPresenter = this.this$Anon0;
                ug4 a3 = activeTimeDetailPresenter.b();
                Anon1 anon1 = new Anon1((yb4) null);
                this.L$Anon0 = zg42;
                this.L$Anon1 = activeTimeDetailPresenter;
                this.label = 1;
                obj4 = yf4.a(a3, anon1, this);
                if (obj4 == a2) {
                    return a2;
                }
            }
            zg4 = zg42;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActiveTimeDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$Anon0.f);
            this.this$Anon0.g = this.$date;
            z = rk2.c(this.this$Anon0.f, this.$date);
            Boolean s = rk2.s(this.$date);
            ce3 o = this.this$Anon0.s;
            Date date = this.$date;
            kd4.a((Object) s, "isToday");
            o.a(date, z, s.booleanValue(), !rk2.c(new Date(), this.$date));
            a = rk2.a(this.$date, this.this$Anon0.f);
            kd4.a((Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            pair = (Pair) this.this$Anon0.h.a();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("ActiveTimeDetailPresenter", "setDate - rangeDateValue=" + pair + ", newRange=" + new Pair(a.first, a.second));
            if (pair != null || !rk2.d((Date) pair.getFirst(), (Date) a.first) || !rk2.d((Date) pair.getSecond(), (Date) a.second)) {
                this.this$Anon0.i = false;
                this.this$Anon0.j = false;
                this.this$Anon0.h.a(new Pair(a.first, a.second));
                return qa4.a;
            }
            ug4 a4 = this.this$Anon0.b();
            ActiveTimeDetailPresenter$setDate$Anon1$summary$Anon1 activeTimeDetailPresenter$setDate$Anon1$summary$Anon1 = new ActiveTimeDetailPresenter$setDate$Anon1$summary$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.Z$Anon0 = z;
            this.L$Anon1 = s;
            this.L$Anon2 = a;
            this.L$Anon3 = pair;
            this.label = 2;
            obj3 = yf4.a(a4, activeTimeDetailPresenter$setDate$Anon1$summary$Anon1, this);
            if (obj3 == a2) {
                return a2;
            }
            pair2 = a;
            bool = s;
            ActivitySummary activitySummary2 = (ActivitySummary) obj3;
            ug4 a5 = this.this$Anon0.b();
            ActiveTimeDetailPresenter$setDate$Anon1$samples$Anon1 activeTimeDetailPresenter$setDate$Anon1$samples$Anon1 = new ActiveTimeDetailPresenter$setDate$Anon1$samples$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.Z$Anon0 = z;
            this.L$Anon1 = bool;
            this.L$Anon2 = pair2;
            this.L$Anon3 = pair;
            this.L$Anon4 = activitySummary2;
            this.label = 3;
            obj2 = yf4.a(a5, activeTimeDetailPresenter$setDate$Anon1$samples$Anon1, this);
            if (obj2 != a2) {
            }
        } else if (i == 1) {
            activeTimeDetailPresenter = (ActiveTimeDetailPresenter) this.L$Anon1;
            zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            obj4 = obj;
        } else if (i == 2) {
            bool = (Boolean) this.L$Anon1;
            boolean z2 = this.Z$Anon0;
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            pair = (Pair) this.L$Anon3;
            pair2 = (android.util.Pair) this.L$Anon2;
            z = z2;
            obj3 = obj;
            ActivitySummary activitySummary22 = (ActivitySummary) obj3;
            ug4 a52 = this.this$Anon0.b();
            ActiveTimeDetailPresenter$setDate$Anon1$samples$Anon1 activeTimeDetailPresenter$setDate$Anon1$samples$Anon12 = new ActiveTimeDetailPresenter$setDate$Anon1$samples$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.Z$Anon0 = z;
            this.L$Anon1 = bool;
            this.L$Anon2 = pair2;
            this.L$Anon3 = pair;
            this.L$Anon4 = activitySummary22;
            this.label = 3;
            obj2 = yf4.a(a52, activeTimeDetailPresenter$setDate$Anon1$samples$Anon12, this);
            if (obj2 != a2) {
                return a2;
            }
            activitySummary = activitySummary22;
            list = (List) obj2;
            if (!kd4.a((Object) this.this$Anon0.m, (Object) activitySummary)) {
            }
            if (!kd4.a((Object) this.this$Anon0.n, (Object) list)) {
            }
            this.this$Anon0.s.a(this.this$Anon0.o, this.this$Anon0.m);
            ActiveTimeDetailPresenter activeTimeDetailPresenter2 = this.this$Anon0;
            activeTimeDetailPresenter2.c(activeTimeDetailPresenter2.g);
            fi4 unused = this.this$Anon0.l();
            return qa4.a;
        } else if (i == 3) {
            activitySummary = (ActivitySummary) this.L$Anon4;
            Pair pair3 = (Pair) this.L$Anon3;
            android.util.Pair pair4 = (android.util.Pair) this.L$Anon2;
            Boolean bool2 = (Boolean) this.L$Anon1;
            zg4 zg43 = (zg4) this.L$Anon0;
            na4.a(obj);
            obj2 = obj;
            list = (List) obj2;
            if (!kd4.a((Object) this.this$Anon0.m, (Object) activitySummary)) {
                this.this$Anon0.m = activitySummary;
            }
            if (!kd4.a((Object) this.this$Anon0.n, (Object) list)) {
                this.this$Anon0.n = list;
            }
            this.this$Anon0.s.a(this.this$Anon0.o, this.this$Anon0.m);
            ActiveTimeDetailPresenter activeTimeDetailPresenter22 = this.this$Anon0;
            activeTimeDetailPresenter22.c(activeTimeDetailPresenter22.g);
            if (this.this$Anon0.i && this.this$Anon0.j) {
                fi4 unused2 = this.this$Anon0.l();
            }
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        activeTimeDetailPresenter.f = (Date) obj4;
        zg4 = zg42;
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        local3.d("ActiveTimeDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$Anon0.f);
        this.this$Anon0.g = this.$date;
        z = rk2.c(this.this$Anon0.f, this.$date);
        Boolean s2 = rk2.s(this.$date);
        ce3 o2 = this.this$Anon0.s;
        Date date2 = this.$date;
        kd4.a((Object) s2, "isToday");
        o2.a(date2, z, s2.booleanValue(), !rk2.c(new Date(), this.$date));
        a = rk2.a(this.$date, this.this$Anon0.f);
        kd4.a((Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
        pair = (Pair) this.this$Anon0.h.a();
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        local22.d("ActiveTimeDetailPresenter", "setDate - rangeDateValue=" + pair + ", newRange=" + new Pair(a.first, a.second));
        if (pair != null) {
        }
        this.this$Anon0.i = false;
        this.this$Anon0.j = false;
        this.this$Anon0.h.a(new Pair(a.first, a.second));
        return qa4.a;
    }
}
