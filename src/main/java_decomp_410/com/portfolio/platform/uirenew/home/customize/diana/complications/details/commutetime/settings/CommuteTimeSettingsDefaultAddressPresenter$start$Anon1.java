package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.y23;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.google.android.libraries.places.api.Places;
import com.portfolio.platform.PortfolioApp;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$start$Anon1", f = "CommuteTimeSettingsDefaultAddressPresenter.kt", l = {40}, m = "invokeSuspend")
public final class CommuteTimeSettingsDefaultAddressPresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsDefaultAddressPresenter$start$Anon1(CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = commuteTimeSettingsDefaultAddressPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CommuteTimeSettingsDefaultAddressPresenter$start$Anon1 commuteTimeSettingsDefaultAddressPresenter$start$Anon1 = new CommuteTimeSettingsDefaultAddressPresenter$start$Anon1(this.this$Anon0, yb4);
        commuteTimeSettingsDefaultAddressPresenter$start$Anon1.p$ = (zg4) obj;
        return commuteTimeSettingsDefaultAddressPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CommuteTimeSettingsDefaultAddressPresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 a2 = this.this$Anon0.c();
            CommuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1 commuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1 = new CommuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a2, commuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        kd4.a(obj, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
        this.this$Anon0.k.clear();
        this.this$Anon0.k.addAll((List) obj);
        this.this$Anon0.f = Places.createClient(PortfolioApp.W.c());
        this.this$Anon0.j().a(this.this$Anon0.f);
        if (kd4.a((Object) this.this$Anon0.h, (Object) "Home")) {
            y23 j = this.this$Anon0.j();
            String d = this.this$Anon0.i;
            kd4.a((Object) d, "mHomeTitle");
            j.setTitle(d);
        } else {
            y23 j2 = this.this$Anon0.j();
            String g = this.this$Anon0.j;
            kd4.a((Object) g, "mWorkTitle");
            j2.setTitle(g);
        }
        this.this$Anon0.j().v(this.this$Anon0.g);
        this.this$Anon0.j().j(this.this$Anon0.k);
        return qa4.a;
    }
}
