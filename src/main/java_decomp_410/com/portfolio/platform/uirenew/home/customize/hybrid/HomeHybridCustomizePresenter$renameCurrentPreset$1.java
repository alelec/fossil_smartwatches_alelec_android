package com.portfolio.platform.uirenew.home.customize.hybrid;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$renameCurrentPreset$1", mo27670f = "HomeHybridCustomizePresenter.kt", mo27671l = {144}, mo27672m = "invokeSuspend")
public final class HomeHybridCustomizePresenter$renameCurrentPreset$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $name;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $presetId;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22968p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeHybridCustomizePresenter$renameCurrentPreset$1(com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter homeHybridCustomizePresenter, java.lang.String str, java.lang.String str2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homeHybridCustomizePresenter;
        this.$presetId = str;
        this.$name = str2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$renameCurrentPreset$1 homeHybridCustomizePresenter$renameCurrentPreset$1 = new com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$renameCurrentPreset$1(this.this$0, this.$presetId, this.$name, yb4);
        homeHybridCustomizePresenter$renameCurrentPreset$1.f22968p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeHybridCustomizePresenter$renameCurrentPreset$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$renameCurrentPreset$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object obj2;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22968p$;
            java.util.Iterator it = this.this$0.f22945h.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj2 = null;
                    break;
                }
                obj2 = it.next();
                if (com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.portfolio.platform.data.model.room.microapp.HybridPreset) obj2).getId(), (java.lang.Object) this.$presetId)).booleanValue()) {
                    break;
                }
            }
            com.portfolio.platform.data.model.room.microapp.HybridPreset hybridPreset = (com.portfolio.platform.data.model.room.microapp.HybridPreset) obj2;
            com.portfolio.platform.data.model.room.microapp.HybridPreset clone = hybridPreset != null ? hybridPreset.clone() : null;
            if (clone != null) {
                clone.setName(this.$name);
                com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
                com.portfolio.platform.uirenew.home.customize.hybrid.C6521x16756079 homeHybridCustomizePresenter$renameCurrentPreset$1$invokeSuspend$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.customize.hybrid.C6521x16756079(clone, (com.fossil.blesdk.obfuscated.yb4) null, this);
                this.L$0 = zg4;
                this.L$1 = clone;
                this.L$2 = clone;
                this.label = 1;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, homeHybridCustomizePresenter$renameCurrentPreset$1$invokeSuspend$$inlined$let$lambda$1, this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            com.portfolio.platform.data.model.room.microapp.HybridPreset hybridPreset2 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) this.L$2;
            com.portfolio.platform.data.model.room.microapp.HybridPreset hybridPreset3 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
