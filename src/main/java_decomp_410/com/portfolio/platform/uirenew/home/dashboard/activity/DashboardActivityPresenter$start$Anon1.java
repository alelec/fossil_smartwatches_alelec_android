package com.portfolio.platform.uirenew.home.dashboard.activity;

import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.Unit;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$start$Anon1", f = "DashboardActivityPresenter.kt", l = {48}, m = "invokeSuspend")
public final class DashboardActivityPresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DashboardActivityPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardActivityPresenter$start$Anon1(DashboardActivityPresenter dashboardActivityPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = dashboardActivityPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DashboardActivityPresenter$start$Anon1 dashboardActivityPresenter$start$Anon1 = new DashboardActivityPresenter$start$Anon1(this.this$Anon0, yb4);
        dashboardActivityPresenter$start$Anon1.p$ = (zg4) obj;
        return dashboardActivityPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DashboardActivityPresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 a2 = this.this$Anon0.c();
            DashboardActivityPresenter$start$Anon1$currentUser$Anon1 dashboardActivityPresenter$start$Anon1$currentUser$Anon1 = new DashboardActivityPresenter$start$Anon1$currentUser$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a2, dashboardActivityPresenter$start$Anon1$currentUser$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser = (MFUser) obj;
        if (mFUser != null) {
            if (this.this$Anon0.h == null) {
                this.this$Anon0.h = mFUser.getDistanceUnit();
            } else {
                Unit distanceUnit = mFUser.getDistanceUnit();
                if (!(distanceUnit == null || this.this$Anon0.h == distanceUnit)) {
                    this.this$Anon0.h = distanceUnit;
                    this.this$Anon0.i.b(distanceUnit);
                }
            }
        }
        FLogger.INSTANCE.getLocal().d("DashboardActivityPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!rk2.s(this.this$Anon0.f).booleanValue()) {
            this.this$Anon0.f = new Date();
            Listing b = this.this$Anon0.g;
            if (b != null) {
                b.getRefresh();
            }
        }
        return qa4.a;
    }
}
