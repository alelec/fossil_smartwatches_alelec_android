package com.portfolio.platform.uirenew.home.dashboard.calories.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.cu3;
import com.fossil.blesdk.obfuscated.f9;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.ga3;
import com.fossil.blesdk.obfuscated.ka3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.qa;
import com.fossil.blesdk.obfuscated.qa3;
import com.fossil.blesdk.obfuscated.tr3;
import com.fossil.blesdk.obfuscated.u92;
import com.fossil.blesdk.obfuscated.va3;
import com.fossil.blesdk.obfuscated.xe;
import com.fossil.blesdk.obfuscated.zr2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CaloriesOverviewFragment extends zr2 {
    @DexIgnore
    public tr3<u92> j;
    @DexIgnore
    public CaloriesOverviewDayPresenter k;
    @DexIgnore
    public CaloriesOverviewWeekPresenter l;
    @DexIgnore
    public CaloriesOverviewMonthPresenter m;
    @DexIgnore
    public ga3 n;
    @DexIgnore
    public va3 o;
    @DexIgnore
    public qa3 p;
    @DexIgnore
    public int q; // = 7;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewFragment e;

        @DexIgnore
        public b(CaloriesOverviewFragment caloriesOverviewFragment) {
            this.e = caloriesOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CaloriesOverviewFragment caloriesOverviewFragment = this.e;
            tr3 a = caloriesOverviewFragment.j;
            caloriesOverviewFragment.a(7, a != null ? (u92) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewFragment e;

        @DexIgnore
        public c(CaloriesOverviewFragment caloriesOverviewFragment) {
            this.e = caloriesOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CaloriesOverviewFragment caloriesOverviewFragment = this.e;
            tr3 a = caloriesOverviewFragment.j;
            caloriesOverviewFragment.a(4, a != null ? (u92) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewFragment e;

        @DexIgnore
        public d(CaloriesOverviewFragment caloriesOverviewFragment) {
            this.e = caloriesOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CaloriesOverviewFragment caloriesOverviewFragment = this.e;
            tr3 a = caloriesOverviewFragment.j;
            caloriesOverviewFragment.a(2, a != null ? (u92) a.a() : null);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "CaloriesOverviewFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewFragment", "onCreateView");
        u92 u92 = (u92) qa.a(layoutInflater, R.layout.fragment_calories_overview, viewGroup, false, O0());
        f9.c((View) u92.t, false);
        if (bundle != null) {
            this.q = bundle.getInt("CURRENT_TAB", 7);
        }
        kd4.a((Object) u92, "binding");
        a(u92);
        this.j = new tr3<>(this, u92);
        tr3<u92> tr3 = this.j;
        if (tr3 != null) {
            u92 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        kd4.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.q);
    }

    @DexIgnore
    public final void a(u92 u92) {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewFragment", "initUI");
        this.n = (ga3) getChildFragmentManager().a("CaloriesOverviewDayFragment");
        this.o = (va3) getChildFragmentManager().a("CaloriesOverviewWeekFragment");
        this.p = (qa3) getChildFragmentManager().a("CaloriesOverviewMonthFragment");
        if (this.n == null) {
            this.n = new ga3();
        }
        if (this.o == null) {
            this.o = new va3();
        }
        if (this.p == null) {
            this.p = new qa3();
        }
        ArrayList arrayList = new ArrayList();
        ga3 ga3 = this.n;
        if (ga3 != null) {
            arrayList.add(ga3);
            va3 va3 = this.o;
            if (va3 != null) {
                arrayList.add(va3);
                qa3 qa3 = this.p;
                if (qa3 != null) {
                    arrayList.add(qa3);
                    RecyclerView recyclerView = u92.t;
                    kd4.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new cu3(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new CaloriesOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new xe().a(recyclerView);
                    a(this.q, u92);
                    l42 g = PortfolioApp.W.c().g();
                    ga3 ga32 = this.n;
                    if (ga32 != null) {
                        va3 va32 = this.o;
                        if (va32 != null) {
                            qa3 qa32 = this.p;
                            if (qa32 != null) {
                                g.a(new ka3(ga32, va32, qa32)).a(this);
                                u92.s.setOnClickListener(new b(this));
                                u92.q.setOnClickListener(new c(this));
                                u92.r.setOnClickListener(new d(this));
                                return;
                            }
                            kd4.a();
                            throw null;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i, u92 u92) {
        if (u92 != null) {
            FlexibleTextView flexibleTextView = u92.s;
            kd4.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = u92.q;
            kd4.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = u92.r;
            kd4.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = u92.s;
            kd4.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = u92.q;
            kd4.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = u92.r;
            kd4.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i == 2) {
                FlexibleTextView flexibleTextView7 = u92.r;
                kd4.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = u92.r;
                kd4.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = u92.q;
                kd4.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                tr3<u92> tr3 = this.j;
                if (tr3 != null) {
                    u92 a2 = tr3.a();
                    if (a2 != null) {
                        RecyclerView recyclerView = a2.t;
                        if (recyclerView != null) {
                            recyclerView.i(2);
                        }
                    }
                }
            } else if (i == 4) {
                FlexibleTextView flexibleTextView10 = u92.q;
                kd4.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = u92.q;
                kd4.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = u92.q;
                kd4.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                tr3<u92> tr32 = this.j;
                if (tr32 != null) {
                    u92 a3 = tr32.a();
                    if (a3 != null) {
                        RecyclerView recyclerView2 = a3.t;
                        if (recyclerView2 != null) {
                            recyclerView2.i(1);
                        }
                    }
                }
            } else if (i != 7) {
                FlexibleTextView flexibleTextView13 = u92.s;
                kd4.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = u92.s;
                kd4.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = u92.q;
                kd4.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                tr3<u92> tr33 = this.j;
                if (tr33 != null) {
                    u92 a4 = tr33.a();
                    if (a4 != null) {
                        RecyclerView recyclerView3 = a4.t;
                        if (recyclerView3 != null) {
                            recyclerView3.i(0);
                        }
                    }
                }
            } else {
                FlexibleTextView flexibleTextView16 = u92.s;
                kd4.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = u92.s;
                kd4.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = u92.q;
                kd4.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                tr3<u92> tr34 = this.j;
                if (tr34 != null) {
                    u92 a5 = tr34.a();
                    if (a5 != null) {
                        RecyclerView recyclerView4 = a5.t;
                        if (recyclerView4 != null) {
                            recyclerView4.i(0);
                        }
                    }
                }
            }
        }
    }
}
