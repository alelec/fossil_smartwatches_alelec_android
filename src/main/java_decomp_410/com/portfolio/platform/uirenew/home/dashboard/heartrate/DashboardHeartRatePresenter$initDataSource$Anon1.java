package com.portfolio.platform.uirenew.home.dashboard.heartrate;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ac3;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zb3;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter$initDataSource$Anon1", f = "DashboardHeartRatePresenter.kt", l = {57}, m = "invokeSuspend")
public final class DashboardHeartRatePresenter$initDataSource$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DashboardHeartRatePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements cc<qd<DailyHeartRateSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ DashboardHeartRatePresenter$initDataSource$Anon1 a;

        @DexIgnore
        public a(DashboardHeartRatePresenter$initDataSource$Anon1 dashboardHeartRatePresenter$initDataSource$Anon1) {
            this.a = dashboardHeartRatePresenter$initDataSource$Anon1;
        }

        @DexIgnore
        public final void a(qd<DailyHeartRateSummary> qdVar) {
            StringBuilder sb = new StringBuilder();
            sb.append("getSummariesPaging observer size=");
            sb.append(qdVar != null ? Integer.valueOf(qdVar.size()) : null);
            MFLogger.d("DashboardHeartRatePresenter", sb.toString());
            if (qdVar != null) {
                this.a.this$Anon0.h.b(qdVar);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardHeartRatePresenter$initDataSource$Anon1(DashboardHeartRatePresenter dashboardHeartRatePresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = dashboardHeartRatePresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DashboardHeartRatePresenter$initDataSource$Anon1 dashboardHeartRatePresenter$initDataSource$Anon1 = new DashboardHeartRatePresenter$initDataSource$Anon1(this.this$Anon0, yb4);
        dashboardHeartRatePresenter$initDataSource$Anon1.p$ = (zg4) obj;
        return dashboardHeartRatePresenter$initDataSource$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DashboardHeartRatePresenter$initDataSource$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 a3 = this.this$Anon0.c();
            DashboardHeartRatePresenter$initDataSource$Anon1$user$Anon1 dashboardHeartRatePresenter$initDataSource$Anon1$user$Anon1 = new DashboardHeartRatePresenter$initDataSource$Anon1$user$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a3, dashboardHeartRatePresenter$initDataSource$Anon1$user$Anon1, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser = (MFUser) obj;
        if (mFUser != null) {
            Date d = rk2.d(mFUser.getCreatedAt());
            DashboardHeartRatePresenter dashboardHeartRatePresenter = this.this$Anon0;
            HeartRateSummaryRepository g = dashboardHeartRatePresenter.i;
            HeartRateSummaryRepository g2 = this.this$Anon0.i;
            FitnessDataRepository c = this.this$Anon0.j;
            HeartRateDailySummaryDao e = this.this$Anon0.k;
            FitnessDatabase d2 = this.this$Anon0.l;
            kd4.a((Object) d, "createdDate");
            dashboardHeartRatePresenter.g = g.getSummariesPaging(g2, c, e, d2, d, this.this$Anon0.n, this.this$Anon0);
            Listing f = this.this$Anon0.g;
            if (f != null) {
                LiveData pagedList = f.getPagedList();
                if (pagedList != null) {
                    zb3 i2 = this.this$Anon0.h;
                    if (i2 != null) {
                        pagedList.a((ac3) i2, new a(this));
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRateFragment");
                    }
                }
            }
        }
        return qa4.a;
    }
}
