package com.portfolio.platform.uirenew.home.customize.hybrid;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.z53;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import java.util.Arrays;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1 implements CoroutineUseCase.e<SetHybridPresetToWatchUseCase.d, SetHybridPresetToWatchUseCase.b> {
    @DexIgnore
    public /* final */ /* synthetic */ HybridPreset a;
    @DexIgnore
    public /* final */ /* synthetic */ HybridPreset b;
    @DexIgnore
    public /* final */ /* synthetic */ HomeHybridCustomizePresenter c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0143Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0143Anon1(Anon1 anon1, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0143Anon1 anon1 = new C0143Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0143Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 zg4 = this.p$;
                    HybridPresetRepository e = this.this$Anon0.this$Anon0.c.q;
                    HybridPreset hybridPreset = this.this$Anon0.this$Anon0.a;
                    if (hybridPreset != null) {
                        String id = hybridPreset.getId();
                        this.L$Anon0 = zg4;
                        this.label = 1;
                        if (e.deletePresetById(id, this) == a) {
                            return a;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else if (i == 1) {
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return qa4.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1 homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                ug4 b = this.this$Anon0.c.c();
                C0143Anon1 anon1 = new C0143Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                if (yf4.a(b, anon1, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$Anon0.c.o.m();
            this.this$Anon0.c.o.c(this.this$Anon0.c.j() - 1);
            this.this$Anon0.c.s.q(true);
            HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1 homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1 = this.this$Anon0;
            boolean unused = homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1.c.b(homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1.b);
            return qa4.a;
        }
    }

    @DexIgnore
    public HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1(HybridPreset hybridPreset, HybridPreset hybridPreset2, HomeHybridCustomizePresenter homeHybridCustomizePresenter, String str) {
        this.a = hybridPreset;
        this.b = hybridPreset2;
        this.c = homeHybridCustomizePresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(SetHybridPresetToWatchUseCase.d dVar) {
        kd4.b(dVar, "responseValue");
        fi4 unused = ag4.b(this.c.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(SetHybridPresetToWatchUseCase.b bVar) {
        kd4.b(bVar, "errorValue");
        this.c.o.m();
        int b2 = bVar.b();
        if (b2 == 1101 || b2 == 1112 || b2 == 1113) {
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(bVar.a());
            kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            z53 l = this.c.o;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                l.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        this.c.o.j();
    }
}
