package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$Anon1", f = "DianaCustomizeViewModel.kt", l = {155, 160}, m = "invokeSuspend")
public final class DianaCustomizeViewModel$init$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $complicationPos;
    @DexIgnore
    public /* final */ /* synthetic */ String $presetId;
    @DexIgnore
    public /* final */ /* synthetic */ String $watchAppPos;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaCustomizeViewModel this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$Anon1$Anon1", f = "DianaCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel$init$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(DianaCustomizeViewModel$init$Anon1 dianaCustomizeViewModel$init$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = dianaCustomizeViewModel$init$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.this$Anon0.d.a(this.this$Anon0.this$Anon0.x);
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaCustomizeViewModel$init$Anon1(DianaCustomizeViewModel dianaCustomizeViewModel, String str, String str2, String str3, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = dianaCustomizeViewModel;
        this.$presetId = str;
        this.$watchAppPos = str2;
        this.$complicationPos = str3;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DianaCustomizeViewModel$init$Anon1 dianaCustomizeViewModel$init$Anon1 = new DianaCustomizeViewModel$init$Anon1(this.this$Anon0, this.$presetId, this.$watchAppPos, this.$complicationPos, yb4);
        dianaCustomizeViewModel$init$Anon1.p$ = (zg4) obj;
        return dianaCustomizeViewModel$init$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DianaCustomizeViewModel$init$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = DianaCustomizeViewModel.G.a();
            local.d(a2, "init presetId=" + this.$presetId + " originalPreset=" + this.this$Anon0.c + " watchAppPos=" + this.$watchAppPos + " complicationPos=" + this.$complicationPos);
            if (this.this$Anon0.c == null) {
                DianaCustomizeViewModel dianaCustomizeViewModel = this.this$Anon0;
                String str = this.$presetId;
                this.L$Anon0 = zg4;
                this.label = 1;
                if (dianaCustomizeViewModel.a(str, (yb4<? super qa4>) this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.i.a(this.$complicationPos);
        this.this$Anon0.l.a(this.$watchAppPos);
        pi4 c = nh4.c();
        Anon1 anon1 = new Anon1(this, (yb4) null);
        this.L$Anon0 = zg4;
        this.label = 2;
        if (yf4.a(c, anon1, this) == a) {
            return a;
        }
        return qa4.a;
    }
}
