package com.portfolio.platform.uirenew.home.dashboard.sleep;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ad3;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc3;
import com.fossil.blesdk.obfuscated.zc3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DashboardSleepPresenter extends yc3 implements PagingRequestHelper.a {
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public Listing<SleepSummary> g;
    @DexIgnore
    public /* final */ zc3 h;
    @DexIgnore
    public /* final */ SleepSummariesRepository i;
    @DexIgnore
    public /* final */ SleepSessionsRepository j;
    @DexIgnore
    public /* final */ FitnessDataRepository k;
    @DexIgnore
    public /* final */ SleepDao l;
    @DexIgnore
    public /* final */ SleepDatabase m;
    @DexIgnore
    public /* final */ UserRepository n;
    @DexIgnore
    public /* final */ h42 o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public DashboardSleepPresenter(zc3 zc3, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDao sleepDao, SleepDatabase sleepDatabase, UserRepository userRepository, h42 h42) {
        kd4.b(zc3, "mView");
        kd4.b(sleepSummariesRepository, "mSleepSummariesRepository");
        kd4.b(sleepSessionsRepository, "mSleepSessionsRepository");
        kd4.b(fitnessDataRepository, "mFitnessDataRepository");
        kd4.b(sleepDao, "mSleepDao");
        kd4.b(sleepDatabase, "mSleepDatabase");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(h42, "mAppExecutors");
        this.h = zc3;
        this.i = sleepSummariesRepository;
        this.j = sleepSessionsRepository;
        this.k = fitnessDataRepository;
        this.l = sleepDao;
        this.m = sleepDatabase;
        this.n = userRepository;
        this.o = h42;
    }

    @DexIgnore
    public void j() {
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", "retry all failed request");
        Listing<SleepSummary> listing = this.g;
        if (listing != null) {
            wc4<qa4> retry = listing.getRetry();
            if (retry != null) {
                qa4 invoke = retry.invoke();
            }
        }
    }

    @DexIgnore
    public final zc3 k() {
        return this.h;
    }

    @DexIgnore
    public void l() {
        this.h.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!rk2.s(this.f).booleanValue()) {
            this.f = new Date();
            Listing<SleepSummary> listing = this.g;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", "stop");
    }

    @DexIgnore
    public void h() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DashboardSleepPresenter$initDataSource$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        try {
            Listing<SleepSummary> listing = this.g;
            if (listing != null) {
                LiveData<qd<SleepSummary>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    zc3 zc3 = this.h;
                    if (zc3 != null) {
                        pagedList.a((LifecycleOwner) (ad3) zc3);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment");
                    }
                }
            }
            this.i.removePagingListener();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(qa4.a);
            local.e("DashboardSleepPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        kd4.b(eVar, "report");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardSleepPresenter", "onStatusChange status=" + eVar);
        if (eVar.a()) {
            this.h.f();
        }
    }
}
