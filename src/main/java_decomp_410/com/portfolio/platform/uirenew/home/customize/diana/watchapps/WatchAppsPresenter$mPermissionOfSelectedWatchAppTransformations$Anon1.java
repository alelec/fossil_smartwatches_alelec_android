package com.portfolio.platform.uirenew.home.customize.diana.watchapps;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.ArrayList;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppsPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1", f = "WatchAppsPresenter.kt", l = {135, 136}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchApp $it;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1 watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1, WatchApp watchApp, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1;
            this.$it = watchApp;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$it, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0118, code lost:
            if (r0 <= 0) goto L_0x011a;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00b0  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00e2  */
        public final Object invokeSuspend(Object obj) {
            List<String> list;
            ArrayList<Pair> arrayList;
            zg4 zg4;
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 = this.p$;
                ug4 a2 = this.this$Anon0.a.b();
                WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1 watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1 = new WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = yf4.a(a2, watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else if (i == 2) {
                list = (List) this.L$Anon1;
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
                String[] strArr = (String[]) obj;
                arrayList = new ArrayList<>();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppsPresenter", "checkPermissionOf watchAppId=" + this.$it.getWatchappId() + ' ' + "grantedPermission " + strArr.length + " requiredPermission " + list.size());
                if (!list.isEmpty()) {
                    for (String str : list) {
                        arrayList.add(new Pair(str, dc4.a(za4.b((T[]) strArr, str))));
                    }
                }
                this.this$Anon0.a.j.a(arrayList);
                if (!arrayList.isEmpty()) {
                    int i2 = 0;
                    if (!arrayList.isEmpty()) {
                        for (Pair second : arrayList) {
                            if (dc4.a(!((Boolean) second.getSecond()).booleanValue()).booleanValue()) {
                                i2++;
                                if (i2 < 0) {
                                    cb4.b();
                                    throw null;
                                }
                            }
                        }
                    }
                }
                fi4 unused = this.this$Anon0.a.b(this.$it.getWatchappId());
                return qa4.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list2 = (List) obj;
            ug4 a3 = this.this$Anon0.a.b();
            WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1 watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1 = new WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1((yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = list2;
            this.label = 2;
            Object a4 = yf4.a(a3, watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1, this);
            if (a4 == a) {
                return a;
            }
            list = list2;
            obj = a4;
            String[] strArr2 = (String[]) obj;
            arrayList = new ArrayList<>();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("WatchAppsPresenter", "checkPermissionOf watchAppId=" + this.$it.getWatchappId() + ' ' + "grantedPermission " + strArr2.length + " requiredPermission " + list.size());
            if (!list.isEmpty()) {
            }
            this.this$Anon0.a.j.a(arrayList);
            if (!arrayList.isEmpty()) {
            }
            fi4 unused2 = this.this$Anon0.a.b(this.$it.getWatchappId());
            return qa4.a;
        }
    }

    @DexIgnore
    public WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1(WatchAppsPresenter watchAppsPresenter) {
        this.a = watchAppsPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final MutableLiveData<List<Pair<String, Boolean>>> apply(WatchApp watchApp) {
        fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, watchApp, (yb4) null), 3, (Object) null);
        return this.a.j;
    }
}
