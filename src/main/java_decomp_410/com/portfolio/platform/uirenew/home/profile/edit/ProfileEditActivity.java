package com.portfolio.platform.uirenew.home.profile.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.mh3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ProfileEditActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            kd4.b(context, "context");
            Intent intent = new Intent(context, ProfileEditActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        if (((mh3) getSupportFragmentManager().a((int) R.id.content)) == null) {
            a((Fragment) mh3.p.a(), (int) R.id.content);
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        a(false);
    }
}
