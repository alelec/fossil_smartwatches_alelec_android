package com.portfolio.platform.uirenew.home.details.activetime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$showDetailChart$1$pairData$1", mo27670f = "ActiveTimeDetailPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class ActiveTimeDetailPresenter$showDetailChart$1$pairData$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super kotlin.Pair<? extends java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a>, ? extends java.util.ArrayList<java.lang.String>>>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23555p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$showDetailChart$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActiveTimeDetailPresenter$showDetailChart$1$pairData$1(com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$showDetailChart$1 activeTimeDetailPresenter$showDetailChart$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = activeTimeDetailPresenter$showDetailChart$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$showDetailChart$1$pairData$1 activeTimeDetailPresenter$showDetailChart$1$pairData$1 = new com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$showDetailChart$1$pairData$1(this.this$0, yb4);
        activeTimeDetailPresenter$showDetailChart$1$pairData$1.f23555p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return activeTimeDetailPresenter$showDetailChart$1$pairData$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$showDetailChart$1$pairData$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.eg3.f14494a.mo26891a(this.this$0.this$0.f23526g, (java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>) this.this$0.this$0.f23531l, 1);
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
