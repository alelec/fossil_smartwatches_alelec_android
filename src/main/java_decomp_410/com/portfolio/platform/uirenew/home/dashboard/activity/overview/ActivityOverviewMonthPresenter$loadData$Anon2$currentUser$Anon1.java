package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.MFUser;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1", f = "ActivityOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
public final class ActivityOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super MFUser>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivityOverviewMonthPresenter$loadData$Anon2 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1(ActivityOverviewMonthPresenter$loadData$Anon2 activityOverviewMonthPresenter$loadData$Anon2, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = activityOverviewMonthPresenter$loadData$Anon2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ActivityOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1 activityOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1 = new ActivityOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1(this.this$Anon0, yb4);
        activityOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1.p$ = (zg4) obj;
        return activityOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActivityOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return this.this$Anon0.this$Anon0.p.getCurrentUser();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
