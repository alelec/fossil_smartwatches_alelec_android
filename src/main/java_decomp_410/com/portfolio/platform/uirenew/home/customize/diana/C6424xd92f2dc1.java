package com.portfolio.platform.uirenew.home.customize.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$2$$special$$inlined$let$lambda$1 */
public final class C6424xd92f2dc1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $it;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22694p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1.C64282 this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$2$$special$$inlined$let$lambda$1$1")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$2$$special$$inlined$let$lambda$1$1 */
    public static final class C64251 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.MFUser>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22695p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64251(com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1 homeDianaCustomizePresenter$start$1$2$$special$$inlined$let$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = homeDianaCustomizePresenter$start$1$2$$special$$inlined$let$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1.C64251 r0 = new com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1.C64251(this.this$0, yb4);
            r0.f22695p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1.C64251) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return this.this$0.this$0.f22703a.this$0.f22662A.getCurrentUser();
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$2$$special$$inlined$let$lambda$1$2")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$2$$special$$inlined$let$lambda$1$2 */
    public static final class C64262 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<? extends com.fossil.blesdk.obfuscated.f13>>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public java.lang.Object L$1;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22696p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64262(com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1 homeDianaCustomizePresenter$start$1$2$$special$$inlined$let$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = homeDianaCustomizePresenter$start$1$2$$special$$inlined$let$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1.C64262 r0 = new com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1.C64262(this.this$0, yb4);
            r0.f22696p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1.C64262) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.dl4 dl4;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22696p$;
                com.fossil.blesdk.obfuscated.dl4 l = this.this$0.this$0.f22703a.this$0.f22675q;
                this.L$0 = zg4;
                this.L$1 = l;
                this.label = 1;
                if (l.mo26535a((java.lang.Object) null, this) == a) {
                    return a;
                }
                dl4 = l;
            } else if (i == 1) {
                dl4 = (com.fossil.blesdk.obfuscated.dl4) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            try {
                java.util.concurrent.CopyOnWriteArrayList<com.portfolio.platform.data.model.diana.preset.DianaPreset> m = this.this$0.this$0.f22703a.this$0.f22668j;
                java.util.ArrayList arrayList = new java.util.ArrayList(com.fossil.blesdk.obfuscated.db4.m20831a(m, 10));
                for (com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset : m) {
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.this$0.this$0.f22703a.this$0;
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) dianaPreset, "it");
                    arrayList.add(homeDianaCustomizePresenter.mo40993a(dianaPreset));
                }
                return arrayList;
            } finally {
                dl4.mo26536a((java.lang.Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6424xd92f2dc1(java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1.C64282 r3) {
        super(2, yb4);
        this.$it = list;
        this.this$0 = r3;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1 homeDianaCustomizePresenter$start$1$2$$special$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1(this.$it, yb4, this.this$0);
        homeDianaCustomizePresenter$start$1$2$$special$$inlined$let$lambda$1.f22694p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeDianaCustomizePresenter$start$1$2$$special$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f22694p$;
            com.fossil.blesdk.obfuscated.ug4 c = this.this$0.f22703a.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1.C64251 r5 = new com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1.C64251(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(c, r5, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            this.this$0.f22703a.this$0.f22678t.mo26425c((java.util.List<com.fossil.blesdk.obfuscated.f13>) (java.util.List) obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.model.MFUser mFUser2 = (com.portfolio.platform.data.model.MFUser) obj;
        if ((!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f22703a.this$0.f22672n, (java.lang.Object) this.$it)) || (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f22703a.this$0.f22674p, (java.lang.Object) mFUser2))) {
            this.this$0.f22703a.this$0.f22674p = mFUser2;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("HomeDianaCustomizePresenter", "on real data change " + this.$it);
            this.this$0.f22703a.this$0.f22672n.clear();
            this.this$0.f22703a.this$0.f22672n.addAll(this.$it);
            if (true ^ this.this$0.f22703a.this$0.f22668j.isEmpty()) {
                com.fossil.blesdk.obfuscated.ug4 c2 = this.this$0.f22703a.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1.C64262 r52 = new com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1.C64262(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = mFUser2;
                this.label = 2;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(c2, r52, this);
                if (obj == a) {
                    return a;
                }
                this.this$0.f22703a.this$0.f22678t.mo26425c((java.util.List<com.fossil.blesdk.obfuscated.f13>) (java.util.List) obj);
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
