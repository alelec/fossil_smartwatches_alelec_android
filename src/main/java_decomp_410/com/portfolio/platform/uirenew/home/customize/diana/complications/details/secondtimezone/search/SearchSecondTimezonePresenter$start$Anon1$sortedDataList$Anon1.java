package com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gb4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import java.util.ArrayList;
import java.util.Comparator;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1", f = "SearchSecondTimezonePresenter.kt", l = {}, m = "invokeSuspend")
public final class SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super ArrayList<SecondTimezoneSetting>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $rawDataList;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<SecondTimezoneSetting> {
        @DexIgnore
        public static /* final */ a e; // = new a();

        @DexIgnore
        /* renamed from: a */
        public final int compare(SecondTimezoneSetting secondTimezoneSetting, SecondTimezoneSetting secondTimezoneSetting2) {
            return secondTimezoneSetting.component1().compareTo(secondTimezoneSetting2.component1());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1(ArrayList arrayList, yb4 yb4) {
        super(2, yb4);
        this.$rawDataList = arrayList;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1 searchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1 = new SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1(this.$rawDataList, yb4);
        searchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1.p$ = (zg4) obj;
        return searchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            gb4.a(this.$rawDataList, a.e);
            return this.$rawDataList;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
