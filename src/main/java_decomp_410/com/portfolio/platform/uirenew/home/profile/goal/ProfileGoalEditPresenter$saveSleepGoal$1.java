package com.portfolio.platform.uirenew.home.profile.goal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveSleepGoal$1", mo27670f = "ProfileGoalEditPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class ProfileGoalEditPresenter$saveSleepGoal$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23877p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveSleepGoal$1$a")
    /* renamed from: com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveSleepGoal$1$a */
    public static final class C6766a<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.lang.Integer>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveSleepGoal$1 f23878a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ androidx.lifecycle.LiveData f23879b;

        @DexIgnore
        public C6766a(com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveSleepGoal$1 profileGoalEditPresenter$saveSleepGoal$1, androidx.lifecycle.LiveData liveData) {
            this.f23878a = profileGoalEditPresenter$saveSleepGoal$1;
            this.f23879b = liveData;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(com.fossil.blesdk.obfuscated.os3<java.lang.Integer> os3) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter.f23850u, "saveSleepGoal observe");
            if ((os3 != null ? os3.mo29978f() : null) != com.portfolio.platform.enums.Status.SUCCESS || os3.mo29975d() == null) {
                if ((os3 != null ? os3.mo29978f() : null) == com.portfolio.platform.enums.Status.ERROR) {
                    com.fossil.blesdk.obfuscated.vh3 m = this.f23878a.this$0.mo41511m();
                    java.lang.Integer c = os3.mo29974c();
                    if (c != null) {
                        int intValue = c.intValue();
                        java.lang.String e = os3.mo29976e();
                        if (e != null) {
                            m.mo31810d(intValue, e);
                            this.f23879b.mo2276a((androidx.lifecycle.LifecycleOwner) this.f23878a.this$0.mo41511m());
                            this.f23878a.this$0.mo41513o();
                            return;
                        }
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
                return;
            }
            this.f23878a.this$0.mo41514p();
            this.f23879b.mo2276a((androidx.lifecycle.LifecycleOwner) this.f23878a.this$0.mo41511m());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileGoalEditPresenter$saveSleepGoal$1(com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter profileGoalEditPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = profileGoalEditPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveSleepGoal$1 profileGoalEditPresenter$saveSleepGoal$1 = new com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveSleepGoal$1(this.this$0, yb4);
        profileGoalEditPresenter$saveSleepGoal$1.f23877p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return profileGoalEditPresenter$saveSleepGoal$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveSleepGoal$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            androidx.lifecycle.LiveData<com.fossil.blesdk.obfuscated.os3<java.lang.Integer>> updateLastSleepGoal = this.this$0.f23863r.updateLastSleepGoal(this.this$0.f23855j);
            com.fossil.blesdk.obfuscated.vh3 m = this.this$0.mo41511m();
            if (m != null) {
                updateLastSleepGoal.mo2277a((com.fossil.blesdk.obfuscated.zr2) m, new com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveSleepGoal$1.C6766a(this, updateLastSleepGoal));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
