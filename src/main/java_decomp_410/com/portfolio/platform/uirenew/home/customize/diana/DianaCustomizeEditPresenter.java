package com.portfolio.platform.uirenew.home.customize.diana;

import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.bn2;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.f13;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.h13;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.s13;
import com.fossil.blesdk.obfuscated.sl2;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.t13;
import com.fossil.blesdk.obfuscated.u13;
import com.fossil.blesdk.obfuscated.wm2;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaCustomizeEditPresenter extends s13 {
    @DexIgnore
    public DianaCustomizeViewModel f;
    @DexIgnore
    public MutableLiveData<DianaPreset> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<f13> h; // = new MutableLiveData<>();
    @DexIgnore
    public ArrayList<CustomizeRealData> i; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Gson j; // = new Gson();
    @DexIgnore
    public MFUser k;
    @DexIgnore
    public WatchFaceWrapper l;
    @DexIgnore
    public /* final */ LiveData<f13> m;
    @DexIgnore
    public /* final */ t13 n;
    @DexIgnore
    public /* final */ UserRepository o;
    @DexIgnore
    public int p;
    @DexIgnore
    public /* final */ SetDianaPresetToWatchUseCase q;
    @DexIgnore
    public /* final */ wm2 r;
    @DexIgnore
    public /* final */ en2 s;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter a;

        @DexIgnore
        public b(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            this.a = dianaCustomizeEditPresenter;
        }

        @DexIgnore
        public final void a(DianaPreset dianaPreset) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "start - observe current preset value=" + dianaPreset);
            DianaCustomizeEditPresenter dianaCustomizeEditPresenter = this.a;
            dianaCustomizeEditPresenter.k = dianaCustomizeEditPresenter.o.getCurrentUser();
            MutableLiveData b = this.a.g;
            if (dianaPreset != null) {
                b.a(dianaPreset.clone());
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements cc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter a;

        @DexIgnore
        public c(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            this.a = dianaCustomizeEditPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "start - observe selected watchApp value=" + str);
            t13 k = this.a.n;
            if (str != null) {
                k.r(str);
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements cc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter a;

        @DexIgnore
        public d(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            this.a = dianaCustomizeEditPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "observe selected complication pos=" + str);
            t13 k = this.a.n;
            if (str != null) {
                k.p(str);
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements cc<f13> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter a;

        @DexIgnore
        public e(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            this.a = dianaCustomizeEditPresenter;
        }

        @DexIgnore
        public final void a(f13 f13) {
            if (f13 != null) {
                this.a.n.a(f13);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements cc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter a;

        @DexIgnore
        public f(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            this.a = dianaCustomizeEditPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "onLiveDataChanged SelectedCustomizeTheme value=" + str);
            if (str != null) {
                WatchFaceWrapper f = DianaCustomizeEditPresenter.g(this.a).f(str);
                this.a.l = f;
                this.a.n.a(f);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements cc<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter a;

        @DexIgnore
        public g(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            this.a = dianaCustomizeEditPresenter;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "update preset status isChanged=" + bool);
            t13 k = this.a.n;
            if (bool != null) {
                k.f(bool.booleanValue());
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter a;

        @DexIgnore
        public h(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            this.a = dianaCustomizeEditPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<f13> apply(DianaPreset dianaPreset) {
            String str;
            String str2;
            DianaPreset dianaPreset2 = dianaPreset;
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(dianaPreset.getComplications());
            ArrayList arrayList2 = new ArrayList();
            arrayList2.addAll(dianaPreset.getWatchapps());
            WatchFaceWrapper f = DianaCustomizeEditPresenter.g(this.a).f(dianaPreset.getWatchFaceId());
            ArrayList arrayList3 = new ArrayList();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "wrapperPresetTransformations presetChanged complications=" + arrayList + " watchapps=" + arrayList2 + " backgroundWrapper=" + f);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) it.next();
                String component1 = dianaPresetComplicationSetting.component1();
                String component2 = dianaPresetComplicationSetting.component2();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DianaCustomizeEditPresenter", "wrapperPresetTransformations find complicationId=" + component2);
                Complication c = DianaCustomizeEditPresenter.g(this.a).c(component2);
                if (c != null) {
                    String complicationId = c.getComplicationId();
                    String icon = c.getIcon();
                    if (icon != null) {
                        str2 = icon;
                    } else {
                        str2 = "";
                    }
                    String a2 = sm2.a(PortfolioApp.W.c(), c.getNameKey(), c.getName());
                    wm2 f2 = this.a.r;
                    MFUser c2 = this.a.k;
                    ArrayList e = this.a.i;
                    String complicationId2 = c.getComplicationId();
                    kd4.a((Object) dianaPreset2, "preset");
                    arrayList3.add(new h13(complicationId, str2, a2, component1, f2.a(c2, e, complicationId2, dianaPreset2)));
                }
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("DianaCustomizeEditPresenter", "wrapperPresetTransformations presetChanged complicationsDetails=" + arrayList3);
            ArrayList arrayList4 = new ArrayList();
            Iterator it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) it2.next();
                String component12 = dianaPresetWatchAppSetting.component1();
                WatchApp e2 = DianaCustomizeEditPresenter.g(this.a).e(dianaPresetWatchAppSetting.component2());
                if (e2 != null) {
                    String watchappId = e2.getWatchappId();
                    String icon2 = e2.getIcon();
                    if (icon2 != null) {
                        str = icon2;
                    } else {
                        str = "";
                    }
                    arrayList4.add(new h13(watchappId, str, sm2.a(PortfolioApp.W.c(), e2.getNameKey(), e2.getName()), component12, (String) null, 16, (fd4) null));
                }
            }
            f13 f13 = (f13) this.a.h.a();
            if (f13 != null) {
                this.a.a((List<h13>) f13.a(), (List<h13>) arrayList3);
                this.a.b((List<h13>) f13.e(), (List<h13>) arrayList4);
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DianaCustomizeEditPresenter", "wrapperPresetTransformations presetChanged watchAppsDetail=" + arrayList4);
            this.a.h.a(new f13(dianaPreset.getId(), dianaPreset.getName(), arrayList3, arrayList4, dianaPreset.isActive(), f));
            return this.a.h;
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public DianaCustomizeEditPresenter(t13 t13, UserRepository userRepository, int i2, SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase, wm2 wm2, en2 en2) {
        kd4.b(t13, "mView");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(setDianaPresetToWatchUseCase, "mSetDianaPresetToWatchUseCase");
        kd4.b(wm2, "mCustomizeRealDataManager");
        kd4.b(en2, "mSharedPreferencesManager");
        this.n = t13;
        this.o = userRepository;
        this.p = i2;
        this.q = setDianaPresetToWatchUseCase;
        this.r = wm2;
        this.s = en2;
        LiveData<f13> b2 = hc.b(this.g, new h(this));
        kd4.a((Object) b2, "Transformations.switchMa\u2026urrentWrapperPreset\n    }");
        this.m = b2;
    }

    @DexIgnore
    public static final /* synthetic */ DianaCustomizeViewModel g(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
        DianaCustomizeViewModel dianaCustomizeViewModel = dianaCustomizeEditPresenter.f;
        if (dianaCustomizeViewModel != null) {
            return dianaCustomizeViewModel;
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final void l() {
        DianaPreset a2 = this.g.a();
        if (a2 != null) {
            DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
            if (dianaCustomizeViewModel != null) {
                DianaPreset e2 = dianaCustomizeViewModel.e();
                if (e2 != null && (!kd4.a((Object) a2.getWatchFaceId(), (Object) e2.getWatchFaceId()))) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("DianaCustomizeEditPresenter", "Watch face changed from " + e2.getWatchFaceId() + " to " + a2.getWatchFaceId());
                    AnalyticsHelper.f.c().a("set_background_manually");
                    return;
                }
                return;
            }
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void m() {
        this.n.a(this);
    }

    @DexIgnore
    public void c(String str, String str2) {
        T t;
        kd4.b(str, "fromPosition");
        kd4.b(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "swapComplication - fromPosition=" + str + ", toPosition=" + str2);
        if (!kd4.a((Object) str, (Object) str2)) {
            DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
            T t2 = null;
            if (dianaCustomizeViewModel != null) {
                DianaPreset a2 = dianaCustomizeViewModel.c().a();
                if (a2 != null) {
                    DianaPreset clone = a2.clone();
                    Iterator<T> it = clone.getComplications().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        if (kd4.a((Object) ((DianaPresetComplicationSetting) t).getPosition(), (Object) str)) {
                            break;
                        }
                    }
                    DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t;
                    Iterator<T> it2 = clone.getComplications().iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        T next = it2.next();
                        if (kd4.a((Object) ((DianaPresetComplicationSetting) next).getPosition(), (Object) str2)) {
                            t2 = next;
                            break;
                        }
                    }
                    DianaPresetComplicationSetting dianaPresetComplicationSetting2 = (DianaPresetComplicationSetting) t2;
                    if (!(dianaPresetComplicationSetting == null || dianaPresetComplicationSetting2 == null)) {
                        dianaPresetComplicationSetting.setPosition(str2);
                        dianaPresetComplicationSetting2.setPosition(str);
                    }
                    FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "swapComplication - update preset " + clone);
                    a(clone);
                    return;
                }
                return;
            }
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void d(String str, String str2) {
        T t;
        kd4.b(str, "fromPosition");
        kd4.b(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "swapWatchApp - fromPosition=" + str + ", toPosition=" + str2);
        if (!kd4.a((Object) str, (Object) str2)) {
            DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
            T t2 = null;
            if (dianaCustomizeViewModel != null) {
                DianaPreset a2 = dianaCustomizeViewModel.c().a();
                if (a2 != null) {
                    Iterator<T> it = a2.getWatchapps().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        if (kd4.a((Object) ((DianaPresetWatchAppSetting) t).getPosition(), (Object) str)) {
                            break;
                        }
                    }
                    DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t;
                    Iterator<T> it2 = a2.getWatchapps().iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        T next = it2.next();
                        if (kd4.a((Object) ((DianaPresetWatchAppSetting) next).getPosition(), (Object) str2)) {
                            t2 = next;
                            break;
                        }
                    }
                    DianaPresetWatchAppSetting dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) t2;
                    if (dianaPresetWatchAppSetting != null) {
                        dianaPresetWatchAppSetting.setPosition(str2);
                    }
                    if (dianaPresetWatchAppSetting2 != null) {
                        dianaPresetWatchAppSetting2.setPosition(str);
                    }
                    FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "swapWatchApp - update preset");
                    kd4.a((Object) a2, "currentPreset");
                    a(a2);
                    return;
                }
                return;
            }
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void f() {
        this.n.f(this.p);
        this.q.f();
        BleCommandResultManager.d.a(CommunicateMode.SET_COMPLICATION_APPS, CommunicateMode.SET_WATCH_APPS, CommunicateMode.SET_PRESET_APPS_DATA);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            MutableLiveData<DianaPreset> c2 = dianaCustomizeViewModel.c();
            t13 t13 = this.n;
            if (t13 != null) {
                c2.a((u13) t13, new b(this));
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
                if (dianaCustomizeViewModel2 != null) {
                    dianaCustomizeViewModel2.j().a((LifecycleOwner) this.n, new c(this));
                    DianaCustomizeViewModel dianaCustomizeViewModel3 = this.f;
                    if (dianaCustomizeViewModel3 != null) {
                        dianaCustomizeViewModel3.g().a((LifecycleOwner) this.n, new d(this));
                        this.m.a((LifecycleOwner) this.n, new e(this));
                        DianaCustomizeViewModel dianaCustomizeViewModel4 = this.f;
                        if (dianaCustomizeViewModel4 != null) {
                            dianaCustomizeViewModel4.h().a((LifecycleOwner) this.n, new f(this));
                            DianaCustomizeViewModel dianaCustomizeViewModel5 = this.f;
                            if (dianaCustomizeViewModel5 != null) {
                                dianaCustomizeViewModel5.d().a((LifecycleOwner) this.n, new g(this));
                            } else {
                                kd4.d("mDianaCustomizeViewModel");
                                throw null;
                            }
                        } else {
                            kd4.d("mDianaCustomizeViewModel");
                            throw null;
                        }
                    } else {
                        kd4.d("mDianaCustomizeViewModel");
                        throw null;
                    }
                } else {
                    kd4.d("mDianaCustomizeViewModel");
                    throw null;
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditFragment");
            }
        } else {
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void g() {
        this.q.g();
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            MutableLiveData<DianaPreset> c2 = dianaCustomizeViewModel.c();
            t13 t13 = this.n;
            if (t13 != null) {
                c2.a((LifecycleOwner) (u13) t13);
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
                if (dianaCustomizeViewModel2 != null) {
                    dianaCustomizeViewModel2.j().a((LifecycleOwner) this.n);
                    DianaCustomizeViewModel dianaCustomizeViewModel3 = this.f;
                    if (dianaCustomizeViewModel3 != null) {
                        dianaCustomizeViewModel3.g().a((LifecycleOwner) this.n);
                        this.h.a((LifecycleOwner) this.n);
                        return;
                    }
                    kd4.d("mDianaCustomizeViewModel");
                    throw null;
                }
                kd4.d("mDianaCustomizeViewModel");
                throw null;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditFragment");
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void h() {
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            boolean m2 = dianaCustomizeViewModel.m();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "isPresetChanged " + m2);
            if (m2) {
                this.n.p();
            } else {
                this.n.g(false);
            }
        } else {
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        this.n.a(this.l);
    }

    @DexIgnore
    public void j() {
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            DianaPreset a2 = dianaCustomizeViewModel.c().a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "setPresetToWatch currentPreset=" + a2);
            Set<Integer> a3 = bn2.d.a(a2);
            bn2 bn2 = bn2.d;
            t13 t13 = this.n;
            if (t13 == null) {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditFragment");
            } else if (bn2.a(((u13) t13).getContext(), a3) && a2 != null) {
                fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1(a2, (yb4) null, this, a2), 3, (Object) null);
            }
        } else {
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final int k() {
        return this.p;
    }

    @DexIgnore
    public final void b(int i2) {
        this.p = i2;
    }

    @DexIgnore
    public void b(String str) {
        kd4.b(str, "watchAppPos");
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            dianaCustomizeViewModel.l(str);
        } else {
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(DianaCustomizeViewModel dianaCustomizeViewModel) {
        kd4.b(dianaCustomizeViewModel, "viewModel");
        this.f = dianaCustomizeViewModel;
    }

    @DexIgnore
    public void b(String str, String str2) {
        kd4.b(str, "watchAppId");
        kd4.b(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditPresenter", "dropWatchApp - watchAppId=" + str + ", toPosition=" + str2);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        T t = null;
        if (dianaCustomizeViewModel != null) {
            if (!dianaCustomizeViewModel.j(str)) {
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
                if (dianaCustomizeViewModel2 != null) {
                    DianaPreset a2 = dianaCustomizeViewModel2.c().a();
                    if (a2 != null) {
                        Iterator<T> it = a2.getWatchapps().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            T next = it.next();
                            if (kd4.a((Object) ((DianaPresetWatchAppSetting) next).getPosition(), (Object) str2)) {
                                t = next;
                                break;
                            }
                        }
                        DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t;
                        if (dianaPresetWatchAppSetting != null) {
                            dianaPresetWatchAppSetting.setId(str);
                        }
                        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "dropWatchApp - update preset");
                        kd4.a((Object) a2, "currentPreset");
                        a(a2);
                    }
                } else {
                    kd4.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
            if (str.hashCode() == -829740640 && str.equals("commute-time") && !this.s.h()) {
                this.s.o(true);
                this.n.c("commute-time");
                return;
            }
            return;
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void a(int i2) {
        this.p = i2;
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, "complicationPos");
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            dianaCustomizeViewModel.k(str);
        } else {
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(ArrayList<CustomizeRealData> arrayList) {
        kd4.b(arrayList, "customizeRealDataList");
        this.i = arrayList;
    }

    @DexIgnore
    public void a(String str, String str2) {
        kd4.b(str, "complicationId");
        kd4.b(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditPresenter", "dropComplication - complicationId=" + str + ", toPosition=" + str2);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        T t = null;
        if (dianaCustomizeViewModel == null) {
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        } else if (!dianaCustomizeViewModel.i(str)) {
            DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
            if (dianaCustomizeViewModel2 != null) {
                DianaPreset a2 = dianaCustomizeViewModel2.c().a();
                if (a2 != null) {
                    DianaPreset clone = a2.clone();
                    Iterator<T> it = clone.getComplications().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        T next = it.next();
                        if (kd4.a((Object) ((DianaPresetComplicationSetting) next).getPosition(), (Object) str2)) {
                            t = next;
                            break;
                        }
                    }
                    DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t;
                    if (dianaPresetComplicationSetting != null) {
                        dianaPresetComplicationSetting.setId(str);
                        dianaPresetComplicationSetting.setSettings("");
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("DianaCustomizeEditPresenter", "dropComplication - newPreset=" + clone);
                    a(clone);
                    return;
                }
                return;
            }
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void b(List<h13> list, List<h13> list2) {
        boolean z;
        if (list.size() == list2.size()) {
            int size = list2.size();
            int i2 = 0;
            while (true) {
                z = true;
                if (i2 >= size) {
                    z = false;
                    break;
                } else if (!kd4.a((Object) list.get(i2).d(), (Object) list2.get(i2).d())) {
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Process watch app list, old and new list are not different, no need to send logs");
                return;
            }
            int size2 = list2.size();
            for (int i3 = 0; i3 < size2; i3++) {
                h13 h13 = list.get(i3);
                h13 h132 = list2.get(i3);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DianaCustomizeEditPresenter", "Process watch app list, item at " + i3 + " position: " + h13.f() + ", oldId: " + h13.d() + ", newId: " + h132.d());
                String f2 = h132.f();
                if (f2 != null) {
                    int hashCode = f2.hashCode();
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && f2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            b(ViewHierarchy.DIMENSION_TOP_KEY, h13.d(), h132.d());
                        }
                    } else if (f2.equals("middle")) {
                        b("middle", h13.d(), h132.d());
                    }
                }
                b("bottom", h13.d(), h132.d());
            }
            return;
        }
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Process watch app list, old and new list sizes are not the same, logs won't be sent");
    }

    @DexIgnore
    public final void a(DianaPreset dianaPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditPresenter", "updateCurrentPreset=" + dianaPreset);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            dianaCustomizeViewModel.a(dianaPreset);
        } else {
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        if (bundle != null) {
            bundle.putInt("KEY_CUSTOMIZE_TAB", this.p);
        }
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            DianaPreset a2 = dianaCustomizeViewModel.c().a();
            if (!(a2 == null || bundle == null)) {
                bundle.putString("KEY_CURRENT_PRESET", new Gson().a((Object) a2));
            }
            DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
            if (dianaCustomizeViewModel2 != null) {
                DianaPreset e2 = dianaCustomizeViewModel2.e();
                if (!(e2 == null || bundle == null)) {
                    bundle.putString("KEY_ORIGINAL_PRESET", new Gson().a((Object) e2));
                }
                DianaCustomizeViewModel dianaCustomizeViewModel3 = this.f;
                if (dianaCustomizeViewModel3 != null) {
                    String a3 = dianaCustomizeViewModel3.g().a();
                    if (!(a3 == null || bundle == null)) {
                        bundle.putString("KEY_PRESET_COMPLICATION_POS_SELECTED", a3);
                    }
                    DianaCustomizeViewModel dianaCustomizeViewModel4 = this.f;
                    if (dianaCustomizeViewModel4 != null) {
                        String a4 = dianaCustomizeViewModel4.j().a();
                        if (!(a4 == null || bundle == null)) {
                            bundle.putString("KEY_PRESET_WATCH_APP_POS_SELECTED", a4);
                        }
                        DianaCustomizeViewModel dianaCustomizeViewModel5 = this.f;
                        if (dianaCustomizeViewModel5 != null) {
                            String a5 = dianaCustomizeViewModel5.j().a();
                            if (!(a5 == null || bundle == null)) {
                                bundle.putString("KEY_PRESET_WATCH_APP_POS_SELECTED", a5);
                            }
                            return bundle;
                        }
                        kd4.d("mDianaCustomizeViewModel");
                        throw null;
                    }
                    kd4.d("mDianaCustomizeViewModel");
                    throw null;
                }
                kd4.d("mDianaCustomizeViewModel");
                throw null;
            }
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final void b(String str, String str2, String str3) {
        sl2 a2 = AnalyticsHelper.f.a("set_watch_apps_manually");
        a2.a("button", str);
        a2.a("old_app", str2);
        a2.a("new_app", str3);
        a2.a();
    }

    @DexIgnore
    public final void a(List<h13> list, List<h13> list2) {
        boolean z;
        if (list.size() == list2.size()) {
            int size = list2.size();
            int i2 = 0;
            while (true) {
                z = true;
                if (i2 >= size) {
                    z = false;
                    break;
                } else if (!kd4.a((Object) list.get(i2).d(), (Object) list2.get(i2).d())) {
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Process complication list, old and new list are not different, no need to send logs");
                return;
            }
            int size2 = list2.size();
            for (int i3 = 0; i3 < size2; i3++) {
                h13 h13 = list.get(i3);
                h13 h132 = list2.get(i3);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DianaCustomizeEditPresenter", "Process complication list, item at " + i3 + " position: " + h13.f() + ", oldId: " + h13.d() + ", newId: " + h132.d());
                String f2 = h132.f();
                if (f2 != null) {
                    int hashCode = f2.hashCode();
                    if (hashCode != -1383228885) {
                        if (hashCode != 115029) {
                            if (hashCode == 3317767 && f2.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                a(ViewHierarchy.DIMENSION_LEFT_KEY, h13.d(), h132.d());
                            }
                        } else if (f2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a(ViewHierarchy.DIMENSION_TOP_KEY, h13.d(), h132.d());
                        }
                    } else if (f2.equals("bottom")) {
                        a("bottom", h13.d(), h132.d());
                    }
                }
                a("right", h13.d(), h132.d());
            }
            return;
        }
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Process complication list, old and new list sizes are not the same, logs won't be sent");
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        sl2 a2 = AnalyticsHelper.f.a("set_complication_manually");
        a2.a("view", str);
        a2.a("old_complication", str2);
        a2.a("new_complication", str3);
        a2.a();
    }
}
