package com.portfolio.platform.uirenew.home.details.sleep;

import android.graphics.RectF;
import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.device.data.config.TimeConfig;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cg3;
import com.fossil.blesdk.obfuscated.dg3;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qt3;
import com.fossil.blesdk.obfuscated.re4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.vf3;
import com.fossil.blesdk.obfuscated.wf3;
import com.fossil.blesdk.obfuscated.xf3;
import com.fossil.blesdk.obfuscated.xk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepDetailPresenter extends vf3 {
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public MutableLiveData<Pair<Date, Date>> h; // = new MutableLiveData<>();
    @DexIgnore
    public String i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public List<MFSleepDay> l; // = new ArrayList();
    @DexIgnore
    public List<MFSleepSession> m; // = new ArrayList();
    @DexIgnore
    public MFSleepDay n;
    @DexIgnore
    public List<MFSleepSession> o;
    @DexIgnore
    public LiveData<os3<List<MFSleepDay>>> p;
    @DexIgnore
    public LiveData<os3<List<MFSleepSession>>> q;
    @DexIgnore
    public /* final */ wf3 r;
    @DexIgnore
    public /* final */ SleepSummariesRepository s;
    @DexIgnore
    public /* final */ SleepSessionsRepository t;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepDetailPresenter a;

        @DexIgnore
        public b(SleepDetailPresenter sleepDetailPresenter) {
            this.a = sleepDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<os3<List<MFSleepSession>>> apply(Pair<? extends Date, ? extends Date> pair) {
            return this.a.t.getSleepSessionList((Date) pair.component1(), (Date) pair.component2(), true);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepDetailPresenter a;

        @DexIgnore
        public c(SleepDetailPresenter sleepDetailPresenter) {
            this.a = sleepDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<os3<List<MFSleepDay>>> apply(Pair<? extends Date, ? extends Date> pair) {
            return this.a.s.getSleepSummaries((Date) pair.component1(), (Date) pair.component2(), true);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public SleepDetailPresenter(wf3 wf3, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        kd4.b(wf3, "mView");
        kd4.b(sleepSummariesRepository, "mSummariesRepository");
        kd4.b(sleepSessionsRepository, "mSessionsRepository");
        this.r = wf3;
        this.s = sleepSummariesRepository;
        this.t = sleepSessionsRepository;
        LiveData<os3<List<MFSleepDay>>> b2 = hc.b(this.h, new c(this));
        kd4.a((Object) b2, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.p = b2;
        LiveData<os3<List<MFSleepSession>>> b3 = hc.b(this.h, new b(this));
        kd4.a((Object) b3, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.q = b3;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("SleepDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.i = PortfolioApp.W.c().e();
        LiveData<os3<List<MFSleepDay>>> liveData = this.p;
        wf3 wf3 = this.r;
        if (wf3 != null) {
            liveData.a((xf3) wf3, new SleepDetailPresenter$start$Anon1(this));
            this.q.a((LifecycleOwner) this.r, new SleepDetailPresenter$start$Anon2(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.sleep.SleepDetailFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("SleepDetailPresenter", "stop");
        LiveData<os3<List<MFSleepDay>>> liveData = this.p;
        wf3 wf3 = this.r;
        if (wf3 != null) {
            liveData.a((LifecycleOwner) (xf3) wf3);
            this.q.a((LifecycleOwner) this.r);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.sleep.SleepDetailFragment");
    }

    @DexIgnore
    public void h() {
        Date l2 = rk2.l(this.g);
        kd4.a((Object) l2, "DateHelper.getNextDate(mDate)");
        a(l2);
    }

    @DexIgnore
    public void i() {
        Date m2 = rk2.m(this.g);
        kd4.a((Object) m2, "DateHelper.getPrevDate(mDate)");
        a(m2);
    }

    @DexIgnore
    public void j() {
        this.r.a(this);
    }

    @DexIgnore
    public final fi4 k() {
        return ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SleepDetailPresenter$showDetailChart$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final fi4 l() {
        return ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SleepDetailPresenter$showHeartRateSleepSessionChart$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final List<dg3.b> b(List<MFSleepSession> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSleepSessionsToDetailChart - sleepSessions.size=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("SleepDetailPresenter", sb.toString());
        int a2 = xk2.d.a(this.n);
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            Iterator<T> it = list.iterator();
            while (it.hasNext()) {
                MFSleepSession mFSleepSession = (MFSleepSession) it.next();
                BarChart.c cVar = new BarChart.c(0, 0, (ArrayList) null, 7, (fd4) null);
                ArrayList arrayList2 = new ArrayList();
                List<WrapperSleepStateChange> sleepStateChange = mFSleepSession.getSleepStateChange();
                ArrayList arrayList3 = new ArrayList();
                SleepDistribution sleepState = mFSleepSession.getSleepState();
                int realStartTime = mFSleepSession.getRealStartTime();
                int totalMinuteBySleepDistribution = sleepState.getTotalMinuteBySleepDistribution();
                if (sleepStateChange != null) {
                    for (WrapperSleepStateChange wrapperSleepStateChange : sleepStateChange) {
                        BarChart.b bVar = new BarChart.b(0, (BarChart.State) null, 0, 0, (RectF) null, 31, (fd4) null);
                        Iterator<T> it2 = it;
                        bVar.a((int) wrapperSleepStateChange.index);
                        bVar.c(realStartTime);
                        bVar.b(totalMinuteBySleepDistribution);
                        int i2 = wrapperSleepStateChange.state;
                        if (i2 == 0) {
                            bVar.a(BarChart.State.LOWEST);
                        } else if (i2 == 1) {
                            bVar.a(BarChart.State.DEFAULT);
                        } else if (i2 == 2) {
                            bVar.a(BarChart.State.HIGHEST);
                        }
                        arrayList3.add(bVar);
                        it = it2;
                    }
                }
                Iterator<T> it3 = it;
                arrayList2.add(arrayList3);
                ArrayList a3 = arrayList2.size() != 0 ? arrayList2 : cb4.a((T[]) new ArrayList[]{cb4.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.State) null, 0, 0, (RectF) null, 23, (fd4) null)})});
                ArrayList<BarChart.a> a4 = cVar.a();
                BarChart.a aVar = r3;
                BarChart.a aVar2 = new BarChart.a(a2, a3, 0, false, 12, (fd4) null);
                a4.add(aVar);
                cVar.b(a2);
                cVar.a(a2);
                int awake = sleepState.getAwake();
                int light = sleepState.getLight();
                int deep = sleepState.getDeep();
                if (totalMinuteBySleepDistribution > 0) {
                    float f2 = (float) totalMinuteBySleepDistribution;
                    float f3 = ((float) awake) / f2;
                    float f4 = ((float) light) / f2;
                    arrayList.add(new dg3.b(cVar, f3, f4, ((float) 1) - (f3 + f4), awake, light, deep, mFSleepSession.getTimezoneOffset()));
                }
                it = it3;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public void a(Date date) {
        kd4.b(date, "date");
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SleepDetailPresenter$setDate$Anon1(this, date, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        kd4.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.g.getTime());
    }

    @DexIgnore
    public final ArrayList<cg3.a> a(List<MFSleepSession> list) {
        short s2;
        short s3;
        short s4;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("extractHeartRateDataFromSleepSession - sleepSessions.size=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("SleepDetailPresenter", sb.toString());
        ArrayList<cg3.a> arrayList = new ArrayList<>();
        if (list != null) {
            for (MFSleepSession mFSleepSession : list) {
                List<WrapperSleepStateChange> sleepStateChange = mFSleepSession.getSleepStateChange();
                try {
                    SleepSessionHeartRate heartRate = mFSleepSession.getHeartRate();
                    if (heartRate != null) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("extractHeartRateDataFromSleepSession - sleepStates.size=");
                        sb2.append(sleepStateChange != null ? Integer.valueOf(sleepStateChange.size()) : null);
                        sb2.append(", heartRateData.size=");
                        sb2.append(heartRate.getValues().size());
                        local2.d("SleepDetailPresenter", sb2.toString());
                        ArrayList arrayList2 = new ArrayList();
                        int resolutionInSecond = heartRate.getResolutionInSecond();
                        int i2 = 0;
                        short s5 = TimeConfig.MAXIMUM_TIMEZONE_OFFSET_IN_MINUTE;
                        short s6 = TimeConfig.MINIMUM_TIMEZONE_OFFSET_IN_MINUTE;
                        int i3 = 0;
                        for (T next : heartRate.getValues()) {
                            int i4 = i3 + 1;
                            if (i3 >= 0) {
                                short shortValue = ((Number) next).shortValue();
                                if (s5 > shortValue && shortValue != ((short) i2)) {
                                    s5 = shortValue;
                                }
                                if (s6 < shortValue) {
                                    s6 = shortValue;
                                }
                                qt3 qt3 = new qt3(0, 0, 0, 7, (fd4) null);
                                qt3.b((i3 * resolutionInSecond) / 60);
                                qt3.c(shortValue);
                                if (sleepStateChange != null) {
                                    int size = sleepStateChange.size();
                                    int i5 = 0;
                                    while (true) {
                                        if (i5 >= size) {
                                            break;
                                        }
                                        if (i5 < cb4.a(sleepStateChange)) {
                                            s4 = s5;
                                            s3 = s6;
                                            if (sleepStateChange.get(i5).index <= ((long) qt3.e()) && ((long) qt3.e()) < sleepStateChange.get(i5 + 1).index) {
                                                qt3.a(sleepStateChange.get(i5).state);
                                                break;
                                            }
                                        } else {
                                            s4 = s5;
                                            s3 = s6;
                                            qt3.a(sleepStateChange.get(i5).state);
                                        }
                                        i5++;
                                        s5 = s4;
                                        s6 = s3;
                                    }
                                }
                                s4 = s5;
                                s3 = s6;
                                arrayList2.add(qt3);
                                i3 = i4;
                                s5 = s4;
                                s6 = s3;
                                i2 = 0;
                            } else {
                                cb4.c();
                                throw null;
                            }
                        }
                        if (s5 == Short.MAX_VALUE) {
                            s2 = TimeConfig.MINIMUM_TIMEZONE_OFFSET_IN_MINUTE;
                            s5 = 0;
                        } else {
                            s2 = TimeConfig.MINIMUM_TIMEZONE_OFFSET_IN_MINUTE;
                        }
                        if (s6 == s2) {
                            s6 = 100;
                        }
                        try {
                            arrayList.add(new cg3.a(arrayList2, mFSleepSession.getRealSleepMinutes(), s5, s6));
                        } catch (Exception e) {
                            e = e;
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("SleepDetailPresenter", "extractHeartRateDataFromSleepSession - e=" + e);
                    e.printStackTrace();
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<MFSleepSession> a(Date date, List<MFSleepSession> list) {
        if (list != null) {
            re4<T> b2 = kb4.b(list);
            if (b2 != null) {
                re4<T> a2 = SequencesKt___SequencesKt.a(b2, new SleepDetailPresenter$findSleepSessions$Anon1(date));
                if (a2 != null) {
                    return SequencesKt___SequencesKt.g(a2);
                }
            }
        }
        return null;
    }

    @DexIgnore
    public final MFSleepDay b(Date date, List<MFSleepDay> list) {
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "calendar");
        instance.setTime(date);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailPresenter", "findSleepSummary - date=" + date);
        T t2 = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (rk2.d(instance.getTime(), ((MFSleepDay) next).getDate())) {
                t2 = next;
                break;
            }
        }
        return (MFSleepDay) t2;
    }
}
