package com.portfolio.platform.uirenew.home.alerts.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeAlertsPresenter$start$1<T> implements com.fossil.blesdk.obfuscated.C1548cc<java.lang.String> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter f22330a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1", mo27670f = "HomeAlertsPresenter.kt", mo27671l = {83}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1 */
    public static final class C63071 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22331p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3")
        /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3 */
        public static final class C63083<T> implements com.fossil.blesdk.obfuscated.C1548cc<java.util.List<? extends com.portfolio.platform.data.model.DNDScheduledTimeModel>> {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071 f22332a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3$1")
            @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3$1", mo27670f = "HomeAlertsPresenter.kt", mo27671l = {167}, mo27672m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3$1 */
            public static final class C63091 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
                @DexIgnore
                public /* final */ /* synthetic */ java.util.List $lDndScheduledTimeModel;
                @DexIgnore
                public java.lang.Object L$0;
                @DexIgnore
                public int label;

                @DexIgnore
                /* renamed from: p$ */
                public com.fossil.blesdk.obfuscated.zg4 f22333p$;
                @DexIgnore
                public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C63083 this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3$1$1")
                @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3$1$1", mo27670f = "HomeAlertsPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
                /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3$1$1 */
                public static final class C63101 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
                    @DexIgnore
                    public int label;

                    @DexIgnore
                    /* renamed from: p$ */
                    public com.fossil.blesdk.obfuscated.zg4 f22334p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C63083.C63091 this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C63101(com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C63083.C63091 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                        super(2, yb4);
                        this.this$0 = r1;
                    }

                    @DexIgnore
                    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                        com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C63083.C63091.C63101 r0 = new com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C63083.C63091.C63101(this.this$0, yb4);
                        r0.f22334p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                        return r0;
                    }

                    @DexIgnore
                    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                        return ((com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C63083.C63091.C63101) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
                    }

                    @DexIgnore
                    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                        com.fossil.blesdk.obfuscated.cc4.m20546a();
                        if (this.label == 0) {
                            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                            this.this$0.this$0.f22332a.this$0.f22330a.f22317v.getDNDScheduledTimeDao().insertListDNDScheduledTime(this.this$0.$lDndScheduledTimeModel);
                            return com.fossil.blesdk.obfuscated.qa4.f17909a;
                        }
                        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C63091(com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C63083 r1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
                    super(2, yb4);
                    this.this$0 = r1;
                    this.$lDndScheduledTimeModel = list;
                }

                @DexIgnore
                public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                    com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                    com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C63083.C63091 r0 = new com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C63083.C63091(this.this$0, this.$lDndScheduledTimeModel, yb4);
                    r0.f22333p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                    return r0;
                }

                @DexIgnore
                public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                    return ((com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C63083.C63091) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
                }

                @DexIgnore
                public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                    java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                    int i = this.label;
                    if (i == 0) {
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22333p$;
                        com.fossil.blesdk.obfuscated.ug4 c = this.this$0.f22332a.this$0.f22330a.mo31441c();
                        com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C63083.C63091.C63101 r3 = new com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C63083.C63091.C63101(this, (com.fossil.blesdk.obfuscated.yb4) null);
                        this.L$0 = zg4;
                        this.label = 1;
                        if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r3, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    } else {
                        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
            }

            @DexIgnore
            public C63083(com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071 r1) {
                this.f22332a = r1;
            }

            @DexIgnore
            /* renamed from: a */
            public final void mo8689a(java.util.List<com.portfolio.platform.data.model.DNDScheduledTimeModel> list) {
                if (list == null || list.isEmpty()) {
                    java.util.ArrayList arrayList = new java.util.ArrayList();
                    com.portfolio.platform.data.model.DNDScheduledTimeModel dNDScheduledTimeModel = new com.portfolio.platform.data.model.DNDScheduledTimeModel("Start", 1380, 0);
                    com.portfolio.platform.data.model.DNDScheduledTimeModel dNDScheduledTimeModel2 = new com.portfolio.platform.data.model.DNDScheduledTimeModel("End", 1140, 1);
                    arrayList.add(dNDScheduledTimeModel);
                    arrayList.add(dNDScheduledTimeModel2);
                    com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f22332a.this$0.f22330a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C63083.C63091(this, arrayList, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                    return;
                }
                for (com.portfolio.platform.data.model.DNDScheduledTimeModel dNDScheduledTimeModel3 : list) {
                    if (dNDScheduledTimeModel3.getScheduledTimeType() == 0) {
                        this.f22332a.this$0.f22330a.f22307l.mo31012b(this.f22332a.this$0.f22330a.mo40762b(dNDScheduledTimeModel3.getMinutes()));
                    } else {
                        this.f22332a.this$0.f22330a.f22307l.mo31008a(this.f22332a.this$0.f22330a.mo40762b(dNDScheduledTimeModel3.getMinutes()));
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$a")
        /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$a */
        public static final class C6311a implements com.portfolio.platform.CoroutineUseCase.C5606e<com.fossil.blesdk.obfuscated.vy2.C5306a, com.portfolio.platform.CoroutineUseCase.C5602a> {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071 f22335a;

            @DexIgnore
            public C6311a(com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071 r1) {
                this.f22335a = r1;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(com.fossil.blesdk.obfuscated.vy2.C5306a aVar) {
                java.lang.String str;
                com.fossil.blesdk.obfuscated.kd4.m24411b(aVar, "responseValue");
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a(), "GetApps onSuccess");
                java.util.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper> arrayList = new java.util.ArrayList<>();
                for (com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper next : aVar.mo31940a()) {
                    com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion companion = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion;
                    com.portfolio.platform.data.model.InstalledApp installedApp = next.getInstalledApp();
                    if (installedApp != null) {
                        java.lang.String identifier = installedApp.getIdentifier();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) identifier, "app.installedApp!!.identifier");
                        if (companion.isPackageNameSupportedBySDK(identifier)) {
                            arrayList.add(next);
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                }
                if (this.f22335a.this$0.f22330a.f22316u.mo26963A()) {
                    java.util.ArrayList arrayList2 = new java.util.ArrayList();
                    for (com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper appWrapper : arrayList) {
                        com.portfolio.platform.data.model.InstalledApp installedApp2 = appWrapper.getInstalledApp();
                        java.lang.Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
                        if (isSelected == null) {
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        } else if (!isSelected.booleanValue()) {
                            arrayList2.add(appWrapper);
                        }
                    }
                    this.f22335a.this$0.f22330a.mo40764i().clear();
                    this.f22335a.this$0.f22330a.mo40764i().addAll(arrayList);
                    if (!arrayList2.isEmpty()) {
                        this.f22335a.this$0.f22330a.mo40760a((java.util.List<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper>) arrayList2);
                        return;
                    }
                    java.lang.String a = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.Customization_Complications_Elements_Label__All);
                    com.fossil.blesdk.obfuscated.sv2 m = this.f22335a.this$0.f22330a.f22307l;
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "notificationAppOverView");
                    m.mo31015h(a);
                    if (com.fossil.blesdk.obfuscated.bn2.m20275a(com.fossil.blesdk.obfuscated.bn2.f13679d, ((com.fossil.blesdk.obfuscated.tv2) this.f22335a.this$0.f22330a.f22307l).getContext(), "NOTIFICATION_APPS", false, 4, (java.lang.Object) null)) {
                        com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter homeAlertsPresenter = this.f22335a.this$0.f22330a;
                        if (homeAlertsPresenter.mo40761a(homeAlertsPresenter.mo40764i(), (java.util.List<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper>) arrayList)) {
                            this.f22335a.this$0.f22330a.mo40766k();
                            return;
                        }
                        return;
                    }
                    return;
                }
                this.f22335a.this$0.f22330a.mo40764i().clear();
                int i = 0;
                for (com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper appWrapper2 : arrayList) {
                    com.portfolio.platform.data.model.InstalledApp installedApp3 = appWrapper2.getInstalledApp();
                    java.lang.Boolean isSelected2 = installedApp3 != null ? installedApp3.isSelected() : null;
                    if (isSelected2 == null) {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    } else if (isSelected2.booleanValue()) {
                        this.f22335a.this$0.f22330a.mo40764i().add(appWrapper2);
                        i++;
                    }
                }
                if (i == aVar.mo31940a().size()) {
                    str = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.Customization_Complications_Elements_Label__All);
                } else if (i == 0) {
                    str = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.Onboarding_Sections_without_Device_Profile_Without_Watch_None);
                } else {
                    str = i + '/' + arrayList.size();
                }
                com.fossil.blesdk.obfuscated.sv2 m2 = this.f22335a.this$0.f22330a.f22307l;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str, "notificationAppOverView");
                m2.mo31015h(str);
                if (i > 0) {
                    com.fossil.blesdk.obfuscated.bn2.m20275a(com.fossil.blesdk.obfuscated.bn2.f13679d, ((com.fossil.blesdk.obfuscated.tv2) this.f22335a.this$0.f22330a.f22307l).getContext(), "NOTIFICATION_APPS", false, 4, (java.lang.Object) null);
                }
            }

            @DexIgnore
            /* renamed from: a */
            public void mo29641a(com.portfolio.platform.CoroutineUseCase.C5602a aVar) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(aVar, "errorValue");
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a(), "GetApps onError");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$b")
        /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$b */
        public static final class C6312b<T> implements java.util.Comparator<T> {
            @DexIgnore
            public final int compare(T t, T t2) {
                return com.fossil.blesdk.obfuscated.wb4.m29575a(java.lang.Integer.valueOf(((com.portfolio.platform.data.source.local.alarm.Alarm) t).getTotalMinutes()), java.lang.Integer.valueOf(((com.portfolio.platform.data.source.local.alarm.Alarm) t2).getTotalMinutes()));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C63071(com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1 homeAlertsPresenter$start$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = homeAlertsPresenter$start$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071 r0 = new com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071(this.this$0, yb4);
            r0.f22331p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object obj2;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22331p$;
                com.fossil.blesdk.obfuscated.ug4 c = this.this$0.f22330a.mo31441c();
                com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$allAlarms$1 homeAlertsPresenter$start$1$1$allAlarms$1 = new com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$allAlarms$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(c, homeAlertsPresenter$start$1$1$allAlarms$1, this);
                if (obj2 == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                obj2 = obj;
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            java.util.List<com.portfolio.platform.data.source.local.alarm.Alarm> list = (java.util.List) obj2;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a();
            local.mo33255d(a2, "GetAlarms onSuccess: size = " + list.size());
            this.this$0.f22330a.f22303h.clear();
            for (com.portfolio.platform.data.source.local.alarm.Alarm copy$default : list) {
                this.this$0.f22330a.f22303h.add(com.portfolio.platform.data.source.local.alarm.Alarm.copy$default(copy$default, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, 0, 0, (int[]) null, false, false, (java.lang.String) null, (java.lang.String) null, 0, 2047, (java.lang.Object) null));
            }
            java.util.ArrayList e = this.this$0.f22330a.f22303h;
            if (e.size() > 1) {
                com.fossil.blesdk.obfuscated.gb4.m22644a(e, new com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C6312b());
            }
            this.this$0.f22330a.f22307l.mo31014d(this.this$0.f22330a.f22303h);
            this.this$0.f22330a.f22310o.mo34435a(null, new com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C6311a(this));
            com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter homeAlertsPresenter = this.this$0.f22330a;
            homeAlertsPresenter.f22304i = homeAlertsPresenter.f22316u.mo26967E();
            this.this$0.f22330a.f22307l.mo31016m(this.this$0.f22330a.f22304i);
            this.this$0.f22330a.f22302g.mo2277a((androidx.lifecycle.LifecycleOwner) this.this$0.f22330a.f22307l, new com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071.C63083(this));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public HomeAlertsPresenter$start$1(com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter homeAlertsPresenter) {
        this.f22330a = homeAlertsPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo8689a(java.lang.String str) {
        if (str == null || str.length() == 0) {
            this.f22330a.f22307l.mo31010a(true);
        } else {
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f22330a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        }
    }
}
