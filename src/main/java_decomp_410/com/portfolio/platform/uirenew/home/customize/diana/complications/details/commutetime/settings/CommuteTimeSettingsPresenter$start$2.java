package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2", mo27670f = "CommuteTimeSettingsPresenter.kt", mo27671l = {73, 74}, mo27672m = "invokeSuspend")
public final class CommuteTimeSettingsPresenter$start$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22771p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2$1", mo27670f = "CommuteTimeSettingsPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2$1 */
    public static final class C64551 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.MFUser>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22772p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64551(com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2 commuteTimeSettingsPresenter$start$2, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = commuteTimeSettingsPresenter$start$2;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2.C64551 r0 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2.C64551(this.this$0, yb4);
            r0.f22772p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2.C64551) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return this.this$0.this$0.f22767n.getCurrentUser();
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsPresenter$start$2(com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter commuteTimeSettingsPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = commuteTimeSettingsPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2 commuteTimeSettingsPresenter$start$2 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2(this.this$0, yb4);
        commuteTimeSettingsPresenter$start$2.f22771p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return commuteTimeSettingsPresenter$start$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0087  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.data.model.MFUser e;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter commuteTimeSettingsPresenter;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f22771p$;
            commuteTimeSettingsPresenter = this.this$0;
            com.fossil.blesdk.obfuscated.ug4 a2 = commuteTimeSettingsPresenter.mo31441c();
            com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2.C64551 r6 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2.C64551(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg42;
            this.L$1 = commuteTimeSettingsPresenter;
            this.label = 1;
            java.lang.Object a3 = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r6, this);
            if (a3 == a) {
                return a;
            }
            java.lang.Object obj2 = a3;
            zg4 = zg42;
            obj = obj2;
        } else if (i == 1) {
            commuteTimeSettingsPresenter = (com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.kd4.m24407a(obj, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
            this.this$0.f22762i.clear();
            this.this$0.f22762i.addAll((java.util.List) obj);
            e = this.this$0.f22763j;
            if (e != null) {
                com.fossil.blesdk.obfuscated.u23 g = this.this$0.f22765l;
                java.lang.String home = e.getHome();
                if (home == null) {
                    home = "";
                }
                g.mo30028w(home);
                com.fossil.blesdk.obfuscated.u23 g2 = this.this$0.f22765l;
                java.lang.String work = e.getWork();
                if (work == null) {
                    work = "";
                }
                g2.mo30016D(work);
            }
            this.this$0.f22765l.mo30026j(this.this$0.f22762i);
            this.this$0.f22765l.mo30024a(this.this$0.f22761h);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        commuteTimeSettingsPresenter.f22763j = (com.portfolio.platform.data.model.MFUser) obj;
        com.fossil.blesdk.obfuscated.ug4 a4 = this.this$0.mo31441c();
        com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2$recentSearchedAddress$1 commuteTimeSettingsPresenter$start$2$recentSearchedAddress$1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2$recentSearchedAddress$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.label = 2;
        obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a4, commuteTimeSettingsPresenter$start$2$recentSearchedAddress$1, this);
        if (obj == a) {
            return a;
        }
        com.fossil.blesdk.obfuscated.kd4.m24407a(obj, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
        this.this$0.f22762i.clear();
        this.this$0.f22762i.addAll((java.util.List) obj);
        e = this.this$0.f22763j;
        if (e != null) {
        }
        this.this$0.f22765l.mo30026j(this.this$0.f22762i);
        this.this$0.f22765l.mo30024a(this.this$0.f22761h);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
