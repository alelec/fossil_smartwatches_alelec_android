package com.portfolio.platform.uirenew.home.customize.diana.watchapps.search;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$Anon1", f = "WatchAppSearchPresenter.kt", l = {73}, m = "invokeSuspend")
public final class WatchAppSearchPresenter$search$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $query;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppSearchPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$Anon1$Anon1", f = "WatchAppSearchPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<WatchApp>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppSearchPresenter$search$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WatchAppSearchPresenter$search$Anon1 watchAppSearchPresenter$search$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = watchAppSearchPresenter$search$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return kb4.d(this.this$Anon0.this$Anon0.m.queryWatchAppByName(this.this$Anon0.$query));
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppSearchPresenter$search$Anon1(WatchAppSearchPresenter watchAppSearchPresenter, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = watchAppSearchPresenter;
        this.$query = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WatchAppSearchPresenter$search$Anon1 watchAppSearchPresenter$search$Anon1 = new WatchAppSearchPresenter$search$Anon1(this.this$Anon0, this.$query, yb4);
        watchAppSearchPresenter$search$Anon1.p$ = (zg4) obj;
        return watchAppSearchPresenter$search$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WatchAppSearchPresenter$search$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        List list;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            list = new ArrayList();
            if (this.$query.length() > 0) {
                ug4 a2 = this.this$Anon0.c();
                Anon1 anon1 = new Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = list;
                this.label = 1;
                obj = yf4.a(a2, anon1, this);
                if (obj == a) {
                    return a;
                }
            }
            this.this$Anon0.l.b(this.this$Anon0.a((List<WatchApp>) list));
            this.this$Anon0.i = this.$query;
            return qa4.a;
        } else if (i == 1) {
            List list2 = (List) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        list = (List) obj;
        this.this$Anon0.l.b(this.this$Anon0.a((List<WatchApp>) list));
        this.this$Anon0.i = this.$query;
        return qa4.a;
    }
}
