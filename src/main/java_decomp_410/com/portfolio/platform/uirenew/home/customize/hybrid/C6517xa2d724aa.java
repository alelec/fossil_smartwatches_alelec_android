package com.portfolio.platform.uirenew.home.customize.hybrid;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1 */
public final class C6517xa2d724aa implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase.C6512d, com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase.C6510b> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.room.microapp.HybridPreset f22959a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.room.microapp.HybridPreset f22960b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter f22961c;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1$1")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1$1 */
    public static final class C65181 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22962p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.C6517xa2d724aa this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1$1$1")
        /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1$1$1 */
        public static final class C65191 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f22963p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.C6517xa2d724aa.C65181 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C65191(com.portfolio.platform.uirenew.home.customize.hybrid.C6517xa2d724aa.C65181 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.customize.hybrid.C6517xa2d724aa.C65181.C65191 r0 = new com.portfolio.platform.uirenew.home.customize.hybrid.C6517xa2d724aa.C65181.C65191(this.this$0, yb4);
                r0.f22963p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.customize.hybrid.C6517xa2d724aa.C65181.C65191) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22963p$;
                    com.portfolio.platform.data.source.HybridPresetRepository e = this.this$0.this$0.f22961c.f22954q;
                    com.portfolio.platform.data.model.room.microapp.HybridPreset hybridPreset = this.this$0.this$0.f22959a;
                    if (hybridPreset != null) {
                        java.lang.String id = hybridPreset.getId();
                        this.L$0 = zg4;
                        this.label = 1;
                        if (e.deletePresetById(id, this) == a) {
                            return a;
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                } else if (i == 1) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C65181(com.portfolio.platform.uirenew.home.customize.hybrid.C6517xa2d724aa homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.hybrid.C6517xa2d724aa.C65181 r0 = new com.portfolio.platform.uirenew.home.customize.hybrid.C6517xa2d724aa.C65181(this.this$0, yb4);
            r0.f22962p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.hybrid.C6517xa2d724aa.C65181) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22962p$;
                com.fossil.blesdk.obfuscated.ug4 b = this.this$0.f22961c.mo31441c();
                com.portfolio.platform.uirenew.home.customize.hybrid.C6517xa2d724aa.C65181.C65191 r3 = new com.portfolio.platform.uirenew.home.customize.hybrid.C6517xa2d724aa.C65181.C65191(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r3, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.f22961c.f22952o.mo33011m();
            this.this$0.f22961c.f22952o.mo33003c(this.this$0.f22961c.mo41136j() - 1);
            this.this$0.f22961c.f22956s.mo27071q(true);
            com.portfolio.platform.uirenew.home.customize.hybrid.C6517xa2d724aa homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1 = this.this$0;
            boolean unused = homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1.f22961c.mo41135b(homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1.f22960b);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public C6517xa2d724aa(com.portfolio.platform.data.model.room.microapp.HybridPreset hybridPreset, com.portfolio.platform.data.model.room.microapp.HybridPreset hybridPreset2, com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter homeHybridCustomizePresenter, java.lang.String str) {
        this.f22959a = hybridPreset;
        this.f22960b = hybridPreset2;
        this.f22961c = homeHybridCustomizePresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase.C6512d dVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f22961c.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.customize.hybrid.C6517xa2d724aa.C65181(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo29641a(com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase.C6510b bVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bVar, "errorValue");
        this.f22961c.f22952o.mo33011m();
        int b = bVar.mo41128b();
        if (b == 1101 || b == 1112 || b == 1113) {
            java.util.List<com.portfolio.platform.enums.PermissionCodes> convertBLEPermissionErrorCode = com.portfolio.platform.enums.PermissionCodes.convertBLEPermissionErrorCode(bVar.mo41127a());
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            com.fossil.blesdk.obfuscated.z53 l = this.f22961c.f22952o;
            java.lang.Object[] array = convertBLEPermissionErrorCode.toArray(new com.portfolio.platform.enums.PermissionCodes[0]);
            if (array != null) {
                com.portfolio.platform.enums.PermissionCodes[] permissionCodesArr = (com.portfolio.platform.enums.PermissionCodes[]) array;
                l.mo26045a((com.portfolio.platform.enums.PermissionCodes[]) java.util.Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                return;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        this.f22961c.f22952o.mo33009j();
    }
}
