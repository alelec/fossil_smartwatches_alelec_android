package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.c92;
import com.fossil.blesdk.obfuscated.cu3;
import com.fossil.blesdk.obfuscated.f9;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.g93;
import com.fossil.blesdk.obfuscated.k93;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.q93;
import com.fossil.blesdk.obfuscated.qa;
import com.fossil.blesdk.obfuscated.tr3;
import com.fossil.blesdk.obfuscated.v93;
import com.fossil.blesdk.obfuscated.xe;
import com.fossil.blesdk.obfuscated.zr2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityOverviewFragment extends zr2 {
    @DexIgnore
    public tr3<c92> j;
    @DexIgnore
    public ActivityOverviewDayPresenter k;
    @DexIgnore
    public ActivityOverviewWeekPresenter l;
    @DexIgnore
    public ActivityOverviewMonthPresenter m;
    @DexIgnore
    public g93 n;
    @DexIgnore
    public v93 o;
    @DexIgnore
    public q93 p;
    @DexIgnore
    public int q; // = 7;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewFragment e;

        @DexIgnore
        public b(ActivityOverviewFragment activityOverviewFragment) {
            this.e = activityOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActivityOverviewFragment activityOverviewFragment = this.e;
            tr3 a = activityOverviewFragment.j;
            activityOverviewFragment.a(7, a != null ? (c92) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewFragment e;

        @DexIgnore
        public c(ActivityOverviewFragment activityOverviewFragment) {
            this.e = activityOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActivityOverviewFragment activityOverviewFragment = this.e;
            tr3 a = activityOverviewFragment.j;
            activityOverviewFragment.a(4, a != null ? (c92) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewFragment e;

        @DexIgnore
        public d(ActivityOverviewFragment activityOverviewFragment) {
            this.e = activityOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActivityOverviewFragment activityOverviewFragment = this.e;
            tr3 a = activityOverviewFragment.j;
            activityOverviewFragment.a(2, a != null ? (c92) a.a() : null);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ActivityOverviewFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewFragment", "onCreateView");
        c92 c92 = (c92) qa.a(layoutInflater, R.layout.fragment_activity_overview, viewGroup, false, O0());
        f9.c((View) c92.t, false);
        if (bundle != null) {
            this.q = bundle.getInt("CURRENT_TAB", 7);
        }
        kd4.a((Object) c92, "binding");
        a(c92);
        this.j = new tr3<>(this, c92);
        tr3<c92> tr3 = this.j;
        if (tr3 != null) {
            c92 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        kd4.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.q);
    }

    @DexIgnore
    public final void a(c92 c92) {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewFragment", "initUI");
        this.n = (g93) getChildFragmentManager().a("ActivityOverviewDayFragment");
        this.o = (v93) getChildFragmentManager().a("ActivityOverviewWeekFragment");
        this.p = (q93) getChildFragmentManager().a("ActivityOverviewMonthFragment");
        if (this.n == null) {
            this.n = new g93();
        }
        if (this.o == null) {
            this.o = new v93();
        }
        if (this.p == null) {
            this.p = new q93();
        }
        ArrayList arrayList = new ArrayList();
        g93 g93 = this.n;
        if (g93 != null) {
            arrayList.add(g93);
            v93 v93 = this.o;
            if (v93 != null) {
                arrayList.add(v93);
                q93 q93 = this.p;
                if (q93 != null) {
                    arrayList.add(q93);
                    RecyclerView recyclerView = c92.t;
                    kd4.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new cu3(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new ActivityOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new xe().a(recyclerView);
                    a(this.q, c92);
                    l42 g = PortfolioApp.W.c().g();
                    g93 g932 = this.n;
                    if (g932 != null) {
                        v93 v932 = this.o;
                        if (v932 != null) {
                            q93 q932 = this.p;
                            if (q932 != null) {
                                g.a(new k93(g932, v932, q932)).a(this);
                                c92.s.setOnClickListener(new b(this));
                                c92.q.setOnClickListener(new c(this));
                                c92.r.setOnClickListener(new d(this));
                                return;
                            }
                            kd4.a();
                            throw null;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i, c92 c92) {
        if (c92 != null) {
            FlexibleTextView flexibleTextView = c92.s;
            kd4.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = c92.q;
            kd4.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = c92.r;
            kd4.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = c92.s;
            kd4.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = c92.q;
            kd4.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = c92.r;
            kd4.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i == 2) {
                FlexibleTextView flexibleTextView7 = c92.r;
                kd4.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = c92.r;
                kd4.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = c92.q;
                kd4.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                tr3<c92> tr3 = this.j;
                if (tr3 != null) {
                    c92 a2 = tr3.a();
                    if (a2 != null) {
                        RecyclerView recyclerView = a2.t;
                        if (recyclerView != null) {
                            recyclerView.i(2);
                        }
                    }
                }
            } else if (i == 4) {
                FlexibleTextView flexibleTextView10 = c92.q;
                kd4.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = c92.q;
                kd4.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = c92.q;
                kd4.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                tr3<c92> tr32 = this.j;
                if (tr32 != null) {
                    c92 a3 = tr32.a();
                    if (a3 != null) {
                        RecyclerView recyclerView2 = a3.t;
                        if (recyclerView2 != null) {
                            recyclerView2.i(1);
                        }
                    }
                }
            } else if (i != 7) {
                FlexibleTextView flexibleTextView13 = c92.s;
                kd4.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = c92.s;
                kd4.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = c92.q;
                kd4.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                tr3<c92> tr33 = this.j;
                if (tr33 != null) {
                    c92 a4 = tr33.a();
                    if (a4 != null) {
                        RecyclerView recyclerView3 = a4.t;
                        if (recyclerView3 != null) {
                            recyclerView3.i(0);
                        }
                    }
                }
            } else {
                FlexibleTextView flexibleTextView16 = c92.s;
                kd4.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = c92.s;
                kd4.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = c92.q;
                kd4.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                tr3<c92> tr34 = this.j;
                if (tr34 != null) {
                    c92 a5 = tr34.a();
                    if (a5 != null) {
                        RecyclerView recyclerView4 = a5.t;
                        if (recyclerView4 != null) {
                            recyclerView4.i(0);
                        }
                    }
                }
            }
        }
    }
}
