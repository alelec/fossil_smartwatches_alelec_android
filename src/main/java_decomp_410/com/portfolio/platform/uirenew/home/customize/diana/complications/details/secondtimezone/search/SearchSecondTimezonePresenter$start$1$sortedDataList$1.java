package com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1", mo27670f = "SearchSecondTimezonePresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class SearchSecondTimezonePresenter$start$1$sortedDataList$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.ArrayList<com.portfolio.platform.data.model.setting.SecondTimezoneSetting>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.ArrayList $rawDataList;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22791p$;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1$a")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1$a */
    public static final class C6461a<T> implements java.util.Comparator<com.portfolio.platform.data.model.setting.SecondTimezoneSetting> {

        @DexIgnore
        /* renamed from: e */
        public static /* final */ com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1.C6461a f22792e; // = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1.C6461a();

        @DexIgnore
        /* renamed from: a */
        public final int compare(com.portfolio.platform.data.model.setting.SecondTimezoneSetting secondTimezoneSetting, com.portfolio.platform.data.model.setting.SecondTimezoneSetting secondTimezoneSetting2) {
            return secondTimezoneSetting.component1().compareTo(secondTimezoneSetting2.component1());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SearchSecondTimezonePresenter$start$1$sortedDataList$1(java.util.ArrayList arrayList, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$rawDataList = arrayList;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1 searchSecondTimezonePresenter$start$1$sortedDataList$1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1(this.$rawDataList, yb4);
        searchSecondTimezonePresenter$start$1$sortedDataList$1.f22791p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return searchSecondTimezonePresenter$start$1$sortedDataList$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.gb4.m22644a(this.$rawDataList, com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1.C6461a.f22792e);
            return this.$rawDataList;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
