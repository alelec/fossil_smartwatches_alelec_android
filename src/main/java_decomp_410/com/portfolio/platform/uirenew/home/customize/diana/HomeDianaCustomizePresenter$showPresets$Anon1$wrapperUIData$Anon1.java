package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.dl4;
import com.fossil.blesdk.obfuscated.f13;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1", f = "HomeDianaCustomizePresenter.kt", l = {394}, m = "invokeSuspend")
public final class HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<? extends f13>>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter$showPresets$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1(HomeDianaCustomizePresenter$showPresets$Anon1 homeDianaCustomizePresenter$showPresets$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = homeDianaCustomizePresenter$showPresets$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1 homeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1 = new HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1(this.this$Anon0, yb4);
        homeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1.p$ = (zg4) obj;
        return homeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        dl4 dl4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            dl4 l = this.this$Anon0.this$Anon0.q;
            this.L$Anon0 = zg4;
            this.L$Anon1 = l;
            this.label = 1;
            if (l.a((Object) null, this) == a) {
                return a;
            }
            dl4 = l;
        } else if (i == 1) {
            dl4 = (dl4) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            CopyOnWriteArrayList<DianaPreset> m = this.this$Anon0.this$Anon0.j;
            ArrayList arrayList = new ArrayList(db4.a(m, 10));
            for (DianaPreset dianaPreset : m) {
                HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.this$Anon0.this$Anon0;
                kd4.a((Object) dianaPreset, "it");
                arrayList.add(homeDianaCustomizePresenter.a(dianaPreset));
            }
            return arrayList;
        } finally {
            dl4.a((Object) null);
        }
    }
}
