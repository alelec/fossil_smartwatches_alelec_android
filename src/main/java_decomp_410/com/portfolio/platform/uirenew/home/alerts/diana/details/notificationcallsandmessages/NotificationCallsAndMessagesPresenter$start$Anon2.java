package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.vy2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$Anon2", f = "NotificationCallsAndMessagesPresenter.kt", l = {143, 150}, m = "invokeSuspend")
public final class NotificationCallsAndMessagesPresenter$start$Anon2 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$Anon2$Anon1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;

        @DexIgnore
        public Anon1(yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                PortfolioApp.W.c().J();
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.e<px2.d, px2.b> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter$start$Anon2 a;

        @DexIgnore
        public a(NotificationCallsAndMessagesPresenter$start$Anon2 notificationCallsAndMessagesPresenter$start$Anon2) {
            this.a = notificationCallsAndMessagesPresenter$start$Anon2;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(px2.d dVar) {
            kd4.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationCallsAndMessagesPresenter.C.a();
            local.d(a2, "GetAllContactGroup onSuccess, size = " + dVar.a().size());
            this.a.this$Anon0.t.l(kb4.d(dVar.a()));
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = NotificationCallsAndMessagesPresenter.C.a();
            local2.d(a3, "mFlagGetListFavoriteFirstLoad=" + this.a.this$Anon0.l());
            if (this.a.this$Anon0.l()) {
                this.a.this$Anon0.c((List<ContactGroup>) kb4.d(dVar.a()));
                for (ContactGroup contactGroup : dVar.a()) {
                    List<Contact> contacts = contactGroup.getContacts();
                    kd4.a((Object) contacts, "contactGroup.contacts");
                    if (!contacts.isEmpty()) {
                        Contact contact = contactGroup.getContacts().get(0);
                        ContactWrapper contactWrapper = new ContactWrapper(contact, (String) null, 2, (fd4) null);
                        contactWrapper.setAdded(true);
                        Contact contact2 = contactWrapper.getContact();
                        if (contact2 != null) {
                            kd4.a((Object) contact, "contact");
                            contact2.setDbRowId(contact.getDbRowId());
                        }
                        Contact contact3 = contactWrapper.getContact();
                        if (contact3 != null) {
                            kd4.a((Object) contact, "contact");
                            contact3.setUseSms(contact.isUseSms());
                        }
                        Contact contact4 = contactWrapper.getContact();
                        if (contact4 != null) {
                            kd4.a((Object) contact, "contact");
                            contact4.setUseCall(contact.isUseCall());
                        }
                        kd4.a((Object) contact, "contact");
                        List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                        kd4.a((Object) phoneNumbers, "contact.phoneNumbers");
                        if (!phoneNumbers.isEmpty()) {
                            PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                            kd4.a((Object) phoneNumber, "contact.phoneNumbers[0]");
                            if (!TextUtils.isEmpty(phoneNumber.getNumber())) {
                                contactWrapper.setHasPhoneNumber(true);
                                PhoneNumber phoneNumber2 = contact.getPhoneNumbers().get(0);
                                kd4.a((Object) phoneNumber2, "contact.phoneNumbers[0]");
                                contactWrapper.setPhoneNumber(phoneNumber2.getNumber());
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String a4 = NotificationCallsAndMessagesPresenter.C.a();
                                StringBuilder sb = new StringBuilder();
                                sb.append(".Inside loadContactData filter selected contact, phoneNumber=");
                                PhoneNumber phoneNumber3 = contact.getPhoneNumbers().get(0);
                                kd4.a((Object) phoneNumber3, "contact.phoneNumbers[0]");
                                sb.append(phoneNumber3.getNumber());
                                local3.d(a4, sb.toString());
                            }
                        }
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String a5 = NotificationCallsAndMessagesPresenter.C.a();
                        local4.d(a5, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                        this.a.this$Anon0.n().add(contactWrapper);
                    }
                }
                this.a.this$Anon0.b(false);
            }
            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            String a6 = NotificationCallsAndMessagesPresenter.C.a();
            local5.d(a6, "start, mListFavoriteContactWrapperFirstLoad=" + this.a.this$Anon0.n() + " size=" + this.a.this$Anon0.n().size());
        }

        @DexIgnore
        public void a(px2.b bVar) {
            kd4.b(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), "GetAllContactGroup onError");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<vy2.a, CoroutineUseCase.a> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter$start$Anon2 a;

        @DexIgnore
        public b(NotificationCallsAndMessagesPresenter$start$Anon2 notificationCallsAndMessagesPresenter$start$Anon2) {
            this.a = notificationCallsAndMessagesPresenter$start$Anon2;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(vy2.a aVar) {
            kd4.b(aVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), "GetApps onSuccess");
            this.a.this$Anon0.m().clear();
            this.a.this$Anon0.m().addAll(aVar.a());
        }

        @DexIgnore
        public void a(CoroutineUseCase.a aVar) {
            kd4.b(aVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), "GetApps onError");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationCallsAndMessagesPresenter$start$Anon2(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = notificationCallsAndMessagesPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NotificationCallsAndMessagesPresenter$start$Anon2 notificationCallsAndMessagesPresenter$start$Anon2 = new NotificationCallsAndMessagesPresenter$start$Anon2(this.this$Anon0, yb4);
        notificationCallsAndMessagesPresenter$start$Anon2.p$ = (zg4) obj;
        return notificationCallsAndMessagesPresenter$start$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationCallsAndMessagesPresenter$start$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b6  */
    public final Object invokeSuspend(Object obj) {
        List<NotificationSettingsModel> list;
        zg4 zg4;
        Object a2 = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            if (!PortfolioApp.W.c().u().N()) {
                ug4 a3 = this.this$Anon0.b();
                Anon1 anon1 = new Anon1((yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                if (yf4.a(a3, anon1, this) == a2) {
                    return a2;
                }
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            list = (List) obj;
            if (!list.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                NotificationSettingsModel notificationSettingsModel = new NotificationSettingsModel("AllowCallsFrom", 0, true);
                NotificationSettingsModel notificationSettingsModel2 = new NotificationSettingsModel("AllowMessagesFrom", 0, false);
                arrayList.add(notificationSettingsModel);
                arrayList.add(notificationSettingsModel2);
                this.this$Anon0.b((List<NotificationSettingsModel>) arrayList);
                String a4 = this.this$Anon0.a(0);
                this.this$Anon0.t.m(a4);
                this.this$Anon0.t.i(a4);
            } else {
                for (NotificationSettingsModel notificationSettingsModel3 : list) {
                    int component2 = notificationSettingsModel3.component2();
                    if (notificationSettingsModel3.component3()) {
                        String a5 = this.this$Anon0.a(component2);
                        if (this.this$Anon0.k) {
                            this.this$Anon0.c(component2);
                            this.this$Anon0.k = false;
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a6 = NotificationCallsAndMessagesPresenter.C.a();
                        local.d(a6, "start, mAlowCallsFromFirstLoad=" + this.this$Anon0.k());
                        this.this$Anon0.t.m(a5);
                    } else {
                        String a7 = this.this$Anon0.a(component2);
                        if (this.this$Anon0.m) {
                            this.this$Anon0.b(component2);
                            this.this$Anon0.m = false;
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a8 = NotificationCallsAndMessagesPresenter.C.a();
                        local2.d(a8, "start, mAllowMessagesFromFirsLoad=" + this.this$Anon0.j());
                        this.this$Anon0.t.i(a7);
                    }
                }
            }
            this.this$Anon0.v.a(null, new a(this));
            this.this$Anon0.y.a(null, new b(this));
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (this.this$Anon0.r) {
            this.this$Anon0.r = false;
            ug4 b2 = this.this$Anon0.c();
            NotificationCallsAndMessagesPresenter$start$Anon2$settings$Anon1 notificationCallsAndMessagesPresenter$start$Anon2$settings$Anon1 = new NotificationCallsAndMessagesPresenter$start$Anon2$settings$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 2;
            obj = yf4.a(b2, notificationCallsAndMessagesPresenter$start$Anon2$settings$Anon1, this);
            if (obj == a2) {
                return a2;
            }
            list = (List) obj;
            if (!list.isEmpty()) {
            }
        }
        this.this$Anon0.v.a(null, new a(this));
        this.this$Anon0.y.a(null, new b(this));
        return qa4.a;
    }
}
