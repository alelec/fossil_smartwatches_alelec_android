package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$saveWeatherWatchAppSetting$1", mo27670f = "WeatherSettingPresenter.kt", mo27671l = {126}, mo27672m = "invokeSuspend")
public final class WeatherSettingPresenter$saveWeatherWatchAppSetting$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22902p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherSettingPresenter$saveWeatherWatchAppSetting$1(com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter weatherSettingPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = weatherSettingPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$saveWeatherWatchAppSetting$1 weatherSettingPresenter$saveWeatherWatchAppSetting$1 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$saveWeatherWatchAppSetting$1(this.this$0, yb4);
        weatherSettingPresenter$saveWeatherWatchAppSetting$1.f22902p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return weatherSettingPresenter$saveWeatherWatchAppSetting$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$saveWeatherWatchAppSetting$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.data.model.setting.WeatherWatchAppSetting weatherWatchAppSetting;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22902p$;
            com.portfolio.platform.data.model.setting.WeatherWatchAppSetting d = this.this$0.f22892g;
            if (d != null) {
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6501xdb6c7842 weatherSettingPresenter$saveWeatherWatchAppSetting$1$invokeSuspend$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6501xdb6c7842((com.fossil.blesdk.obfuscated.yb4) null, this);
                this.L$0 = zg4;
                this.L$1 = d;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, weatherSettingPresenter$saveWeatherWatchAppSetting$1$invokeSuspend$$inlined$let$lambda$1, this);
                if (obj == a) {
                    return a;
                }
                weatherWatchAppSetting = d;
            }
            this.this$0.f22894i.mo28764a();
            this.this$0.f22894i.mo25624r(true);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            weatherWatchAppSetting = (com.portfolio.platform.data.model.setting.WeatherWatchAppSetting) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        weatherWatchAppSetting.setLocations(com.fossil.blesdk.obfuscated.kb4.m24381d((java.util.List) obj));
        this.this$0.f22894i.mo28764a();
        this.this$0.f22894i.mo25624r(true);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
