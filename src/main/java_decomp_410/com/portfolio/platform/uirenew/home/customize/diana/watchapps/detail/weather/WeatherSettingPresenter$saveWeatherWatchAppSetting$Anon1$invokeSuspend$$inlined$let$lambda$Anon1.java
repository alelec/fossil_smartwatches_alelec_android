package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<? extends WeatherLocationWrapper>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(yb4 yb4, WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1 weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1) {
        super(2, yb4);
        this.this$Anon0 = weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(yb4, this.this$Anon0);
        weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            List b = this.this$Anon0.this$Anon0.h;
            ArrayList arrayList = new ArrayList();
            for (Object next : b) {
                if (dc4.a(((WeatherLocationWrapper) next).isEnableLocation()).booleanValue()) {
                    arrayList.add(next);
                }
            }
            return arrayList;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
