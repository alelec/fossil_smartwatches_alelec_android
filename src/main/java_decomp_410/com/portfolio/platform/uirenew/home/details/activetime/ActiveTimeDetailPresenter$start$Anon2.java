package com.portfolio.platform.uirenew.home.details.activetime;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.enums.Status;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActiveTimeDetailPresenter$start$Anon2<T> implements cc<os3<? extends List<ActivitySummary>>> {
    @DexIgnore
    public /* final */ /* synthetic */ ActiveTimeDetailPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$Anon2$Anon1", f = "ActiveTimeDetailPresenter.kt", l = {126}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeDetailPresenter$start$Anon2 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ActiveTimeDetailPresenter$start$Anon2 activeTimeDetailPresenter$start$Anon2, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = activeTimeDetailPresenter$start$Anon2;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                ug4 a2 = this.this$Anon0.a.b();
                ActiveTimeDetailPresenter$start$Anon2$Anon1$summary$Anon1 activeTimeDetailPresenter$start$Anon2$Anon1$summary$Anon1 = new ActiveTimeDetailPresenter$start$Anon2$Anon1$summary$Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = yf4.a(a2, activeTimeDetailPresenter$start$Anon2$Anon1$summary$Anon1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ActivitySummary activitySummary = (ActivitySummary) obj;
            if (this.this$Anon0.a.m == null || (!kd4.a((Object) this.this$Anon0.a.m, (Object) activitySummary))) {
                this.this$Anon0.a.m = activitySummary;
                this.this$Anon0.a.s.a(this.this$Anon0.a.o, this.this$Anon0.a.m);
                if (this.this$Anon0.a.i && this.this$Anon0.a.j) {
                    fi4 unused = this.this$Anon0.a.l();
                }
            }
            return qa4.a;
        }
    }

    @DexIgnore
    public ActiveTimeDetailPresenter$start$Anon2(ActiveTimeDetailPresenter activeTimeDetailPresenter) {
        this.a = activeTimeDetailPresenter;
    }

    @DexIgnore
    public final void a(os3<? extends List<ActivitySummary>> os3) {
        Status a2 = os3.a();
        List list = (List) os3.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("start - summaryTransformations -- activitySummaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        sb.append(", status=");
        sb.append(a2);
        local.d("ActiveTimeDetailPresenter", sb.toString());
        if (a2 == Status.NETWORK_LOADING || a2 == Status.SUCCESS) {
            this.a.k = list;
            this.a.i = true;
            fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (yb4) null), 3, (Object) null);
        }
    }
}
