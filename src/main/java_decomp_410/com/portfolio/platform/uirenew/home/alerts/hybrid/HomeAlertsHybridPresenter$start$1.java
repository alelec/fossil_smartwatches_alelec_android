package com.portfolio.platform.uirenew.home.alerts.hybrid;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeAlertsHybridPresenter$start$1<T> implements com.fossil.blesdk.obfuscated.C1548cc<java.lang.String> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter f22502a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1", mo27670f = "HomeAlertsHybridPresenter.kt", mo27671l = {58}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1 */
    public static final class C63651 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.lang.String $deviceId;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22503p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1$a")
        /* renamed from: com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1$a */
        public static final class C6366a<T> implements java.util.Comparator<T> {
            @DexIgnore
            public final int compare(T t, T t2) {
                return com.fossil.blesdk.obfuscated.wb4.m29575a(java.lang.Integer.valueOf(((com.portfolio.platform.data.source.local.alarm.Alarm) t).getTotalMinutes()), java.lang.Integer.valueOf(((com.portfolio.platform.data.source.local.alarm.Alarm) t2).getTotalMinutes()));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C63651(com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1 homeAlertsHybridPresenter$start$1, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = homeAlertsHybridPresenter$start$1;
            this.$deviceId = str;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1.C63651 r0 = new com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1.C63651(this.this$0, this.$deviceId, yb4);
            r0.f22503p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1.C63651) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object obj2;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22503p$;
                boolean b = com.portfolio.platform.helper.DeviceHelper.f21188o.mo39565e().mo39548b(this.$deviceId);
                this.this$0.f22502a.f22494i.mo32948q(b);
                if (b) {
                    com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.f22502a.mo31441c();
                    com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1$allAlarms$1 homeAlertsHybridPresenter$start$1$1$allAlarms$1 = new com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1$allAlarms$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg4;
                    this.Z$0 = b;
                    this.label = 1;
                    obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, homeAlertsHybridPresenter$start$1$1$allAlarms$1, this);
                    if (obj2 == a) {
                        return a;
                    }
                }
                com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter homeAlertsHybridPresenter = this.this$0.f22502a;
                homeAlertsHybridPresenter.f22493h = homeAlertsHybridPresenter.f22498m.mo26966D();
                this.this$0.f22502a.f22494i.mo32946i(this.this$0.f22502a.f22493h);
                this.this$0.f22502a.f22494i.mo32947n(this.this$0.f22502a.f22493h);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                obj2 = obj;
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            java.util.List<com.portfolio.platform.data.source.local.alarm.Alarm> list = (java.util.List) obj2;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a3 = com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter.f22490o.mo40880a();
            local.mo33255d(a3, "GetAlarms onSuccess: size = " + list.size());
            this.this$0.f22502a.f22492g.clear();
            for (com.portfolio.platform.data.source.local.alarm.Alarm copy$default : list) {
                this.this$0.f22502a.f22492g.add(com.portfolio.platform.data.source.local.alarm.Alarm.copy$default(copy$default, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, 0, 0, (int[]) null, false, false, (java.lang.String) null, (java.lang.String) null, 0, 2047, (java.lang.Object) null));
            }
            java.util.ArrayList c = this.this$0.f22502a.f22492g;
            if (c.size() > 1) {
                com.fossil.blesdk.obfuscated.gb4.m22644a(c, new com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1.C63651.C6366a());
            }
            this.this$0.f22502a.f22494i.mo32945d(this.this$0.f22502a.f22492g);
            com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter homeAlertsHybridPresenter2 = this.this$0.f22502a;
            homeAlertsHybridPresenter2.f22493h = homeAlertsHybridPresenter2.f22498m.mo26966D();
            this.this$0.f22502a.f22494i.mo32946i(this.this$0.f22502a.f22493h);
            this.this$0.f22502a.f22494i.mo32947n(this.this$0.f22502a.f22493h);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public HomeAlertsHybridPresenter$start$1(com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
        this.f22502a = homeAlertsHybridPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo8689a(java.lang.String str) {
        if (str == null || str.length() == 0) {
            this.f22502a.f22494i.mo32942a(true);
        } else {
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f22502a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1.C63651(this, str, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        }
    }
}
