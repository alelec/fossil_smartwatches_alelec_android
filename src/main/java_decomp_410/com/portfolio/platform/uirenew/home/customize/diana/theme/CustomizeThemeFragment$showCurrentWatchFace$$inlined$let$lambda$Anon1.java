package com.portfolio.platform.uirenew.home.customize.diana.theme;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.ih4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $selectedPos$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ RecyclerView $this_with;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CustomizeThemeFragment this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1(RecyclerView recyclerView, yb4 yb4, CustomizeThemeFragment customizeThemeFragment, int i) {
        super(2, yb4);
        this.$this_with = recyclerView;
        this.this$Anon0 = customizeThemeFragment;
        this.$selectedPos$inlined = i;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1 customizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1 = new CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1(this.$this_with, yb4, this.this$Anon0, this.$selectedPos$inlined);
        customizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return customizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            long a2 = this.this$Anon0.m;
            this.L$Anon0 = zg4;
            this.label = 1;
            if (ih4.a(a2, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.m = 0;
        this.$this_with.j(this.$selectedPos$inlined);
        return qa4.a;
    }
}
