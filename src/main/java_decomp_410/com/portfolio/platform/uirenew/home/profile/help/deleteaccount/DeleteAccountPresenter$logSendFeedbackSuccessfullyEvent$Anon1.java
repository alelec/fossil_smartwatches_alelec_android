package com.portfolio.platform.uirenew.home.profile.help.deleteaccount;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.SKUModel;
import java.util.HashMap;
import java.util.Map;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1", f = "DeleteAccountPresenter.kt", l = {107}, m = "invokeSuspend")
public final class DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeleteAccountPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1(DeleteAccountPresenter deleteAccountPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = deleteAccountPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1 deleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1 = new DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1(this.this$Anon0, yb4);
        deleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1.p$ = (zg4) obj;
        return deleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 b = nh4.b();
            DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1 deleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1 = new DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(b, deleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        SKUModel sKUModel = (SKUModel) obj;
        if (sKUModel != null) {
            HashMap hashMap = new HashMap();
            String sku = sKUModel.getSku();
            if (sku == null) {
                sku = "";
            }
            hashMap.put("Style_Number", sku);
            String deviceName = sKUModel.getDeviceName();
            if (deviceName == null) {
                deviceName = "";
            }
            hashMap.put("Device_Name", deviceName);
            this.this$Anon0.h.a("feedback_submit", (Map<String, ? extends Object>) hashMap);
        }
        return qa4.a;
    }
}
