package com.portfolio.platform.uirenew.home.details.calories;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.ve3;
import com.fossil.blesdk.obfuscated.we3;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.enums.Status;
import com.portfolio.platform.enums.Unit;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$Anon1", f = "CaloriesDetailPresenter.kt", l = {116}, m = "invokeSuspend")
public final class CaloriesDetailPresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CaloriesDetailPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$Anon1$Anon1", f = "CaloriesDetailPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Unit>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesDetailPresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(CaloriesDetailPresenter$start$Anon1 caloriesDetailPresenter$start$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = caloriesDetailPresenter$start$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                MFUser currentUser = this.this$Anon0.this$Anon0.v.getCurrentUser();
                if (currentUser != null) {
                    Unit distanceUnit = currentUser.getDistanceUnit();
                    if (distanceUnit != null) {
                        return distanceUnit;
                    }
                }
                return Unit.METRIC;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2<T> implements cc<os3<? extends List<ActivitySummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesDetailPresenter$start$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @gc4(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$Anon1$Anon2$Anon1", f = "CaloriesDetailPresenter.kt", l = {130}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon2 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon2 anon2, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon2;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 zg4 = this.p$;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("XXX", "find todaySummary of " + this.this$Anon0.a.this$Anon0.k);
                    ug4 a2 = this.this$Anon0.a.this$Anon0.b();
                    CaloriesDetailPresenter$start$Anon1$Anon2$Anon1$summary$Anon1 caloriesDetailPresenter$start$Anon1$Anon2$Anon1$summary$Anon1 = new CaloriesDetailPresenter$start$Anon1$Anon2$Anon1$summary$Anon1(this, (yb4) null);
                    this.L$Anon0 = zg4;
                    this.label = 1;
                    obj = yf4.a(a2, caloriesDetailPresenter$start$Anon1$Anon2$Anon1$summary$Anon1, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ActivitySummary activitySummary = (ActivitySummary) obj;
                if (this.this$Anon0.a.this$Anon0.m == null || (!kd4.a((Object) this.this$Anon0.a.this$Anon0.m, (Object) activitySummary))) {
                    this.this$Anon0.a.this$Anon0.m = activitySummary;
                    this.this$Anon0.a.this$Anon0.s.a(this.this$Anon0.a.this$Anon0.o, this.this$Anon0.a.this$Anon0.m);
                    if (this.this$Anon0.a.this$Anon0.i && this.this$Anon0.a.this$Anon0.j) {
                        fi4 unused = this.this$Anon0.a.this$Anon0.l();
                    }
                }
                return qa4.a;
            }
        }

        @DexIgnore
        public Anon2(CaloriesDetailPresenter$start$Anon1 caloriesDetailPresenter$start$Anon1) {
            this.a = caloriesDetailPresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(os3<? extends List<ActivitySummary>> os3) {
            Status a2 = os3.a();
            List list = (List) os3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - summaryTransformations -- activitySummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("CaloriesDetailPresenter", sb.toString());
            if (a2 == Status.NETWORK_LOADING || a2 == Status.SUCCESS) {
                this.a.this$Anon0.k = list;
                this.a.this$Anon0.i = true;
                fi4 unused = ag4.b(this.a.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (yb4) null), 3, (Object) null);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3<T> implements cc<os3<? extends List<ActivitySample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesDetailPresenter$start$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @gc4(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$Anon1$Anon3$Anon1", f = "CaloriesDetailPresenter.kt", l = {152}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon3 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon3 anon3, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon3;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 zg4 = this.p$;
                    ug4 a2 = this.this$Anon0.a.this$Anon0.b();
                    CaloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1 caloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1 = new CaloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1(this, (yb4) null);
                    this.L$Anon0 = zg4;
                    this.label = 1;
                    obj = yf4.a(a2, caloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) obj;
                if (this.this$Anon0.a.this$Anon0.n == null || (!kd4.a((Object) this.this$Anon0.a.this$Anon0.n, (Object) list))) {
                    this.this$Anon0.a.this$Anon0.n = list;
                    if (this.this$Anon0.a.this$Anon0.i && this.this$Anon0.a.this$Anon0.j) {
                        fi4 unused = this.this$Anon0.a.this$Anon0.l();
                    }
                }
                return qa4.a;
            }
        }

        @DexIgnore
        public Anon3(CaloriesDetailPresenter$start$Anon1 caloriesDetailPresenter$start$Anon1) {
            this.a = caloriesDetailPresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(os3<? extends List<ActivitySample>> os3) {
            Status a2 = os3.a();
            List list = (List) os3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - sampleTransformations -- activitySamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("CaloriesDetailPresenter", sb.toString());
            if (a2 == Status.NETWORK_LOADING || a2 == Status.SUCCESS) {
                this.a.this$Anon0.l = list;
                this.a.this$Anon0.j = true;
                fi4 unused = ag4.b(this.a.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (yb4) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CaloriesDetailPresenter$start$Anon1(CaloriesDetailPresenter caloriesDetailPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = caloriesDetailPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CaloriesDetailPresenter$start$Anon1 caloriesDetailPresenter$start$Anon1 = new CaloriesDetailPresenter$start$Anon1(this.this$Anon0, yb4);
        caloriesDetailPresenter$start$Anon1.p$ = (zg4) obj;
        return caloriesDetailPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CaloriesDetailPresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        CaloriesDetailPresenter caloriesDetailPresenter;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            CaloriesDetailPresenter caloriesDetailPresenter2 = this.this$Anon0;
            ug4 a2 = caloriesDetailPresenter2.b();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = caloriesDetailPresenter2;
            this.label = 1;
            obj = yf4.a(a2, anon1, this);
            if (obj == a) {
                return a;
            }
            caloriesDetailPresenter = caloriesDetailPresenter2;
        } else if (i == 1) {
            caloriesDetailPresenter = (CaloriesDetailPresenter) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        caloriesDetailPresenter.o = (Unit) obj;
        LiveData q = this.this$Anon0.p;
        ve3 o = this.this$Anon0.s;
        if (o != null) {
            q.a((we3) o, new Anon2(this));
            this.this$Anon0.q.a((LifecycleOwner) this.this$Anon0.s, new Anon3(this));
            return qa4.a;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailFragment");
    }
}
