package com.portfolio.platform.uirenew.home.profile.help.deleteaccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeleteAccountPresenter$sendFeedback$1 implements com.portfolio.platform.CoroutineUseCase.C5606e<com.fossil.blesdk.obfuscated.kr2.C4595d, com.fossil.blesdk.obfuscated.kr2.C4593b> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter f23906a;

    @DexIgnore
    public DeleteAccountPresenter$sendFeedback$1(com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter deleteAccountPresenter) {
        this.f23906a = deleteAccountPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(com.fossil.blesdk.obfuscated.kr2.C4595d dVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter.f23895l.mo41528a(), "sendFeedback onSuccess");
        com.zendesk.sdk.network.impl.ZendeskConfig.INSTANCE.setIdentity(new com.zendesk.sdk.model.access.AnonymousIdentity.Builder().withNameIdentifier(dVar.mo28911f()).withEmailIdentifier(dVar.mo28908c()).build());
        com.zendesk.sdk.network.impl.ZendeskConfig.INSTANCE.setCustomFields(dVar.mo28907b());
        this.f23906a.f23896f.mo26511b(new com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$sendFeedback$1$onSuccess$configuration$1(dVar));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo29641a(com.fossil.blesdk.obfuscated.kr2.C4593b bVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bVar, "errorValue");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter.f23895l.mo41528a(), "sendFeedback onError");
    }
}
