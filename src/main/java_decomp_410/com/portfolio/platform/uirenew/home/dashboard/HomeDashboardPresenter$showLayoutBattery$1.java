package com.portfolio.platform.uirenew.home.dashboard;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$1", mo27670f = "HomeDashboardPresenter.kt", mo27671l = {136}, mo27672m = "invokeSuspend")
public final class HomeDashboardPresenter$showLayoutBattery$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $buildMode;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23106p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDashboardPresenter$showLayoutBattery$1(com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter homeDashboardPresenter, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homeDashboardPresenter;
        this.$buildMode = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$1 homeDashboardPresenter$showLayoutBattery$1 = new com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$1(this.this$0, this.$buildMode, yb4);
        homeDashboardPresenter$showLayoutBattery$1.f23106p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeDashboardPresenter$showLayoutBattery$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23106p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$1$activeDevice$1 homeDashboardPresenter$showLayoutBattery$1$activeDevice$1 = new com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$1$activeDevice$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, homeDashboardPresenter$showLayoutBattery$1$activeDevice$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.model.Device device = (com.portfolio.platform.data.model.Device) obj;
        if (device != null) {
            if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.$buildMode, (java.lang.Object) "debug") || com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.$buildMode, (java.lang.Object) "staging")) {
                if (com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId()) || device.getBatteryLevel() >= com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34574u().mo27087w() || device.getBatteryLevel() <= 0) {
                    this.this$0.f23093x.mo30966o(false);
                } else {
                    this.this$0.f23093x.mo30966o(true);
                }
            } else if (com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId()) || device.getBatteryLevel() >= 10 || device.getBatteryLevel() <= 0) {
                this.this$0.f23093x.mo30966o(false);
            } else {
                this.this$0.f23093x.mo30966o(true);
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
