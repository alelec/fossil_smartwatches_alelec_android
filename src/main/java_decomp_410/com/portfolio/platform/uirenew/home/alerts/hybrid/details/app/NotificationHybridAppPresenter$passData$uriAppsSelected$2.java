package com.portfolio.platform.uirenew.home.alerts.hybrid.details.app;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationHybridAppPresenter$passData$uriAppsSelected$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper, java.lang.String> {
    @DexIgnore
    public static /* final */ com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppPresenter$passData$uriAppsSelected$2 INSTANCE; // = new com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppPresenter$passData$uriAppsSelected$2();

    @DexIgnore
    public NotificationHybridAppPresenter$passData$uriAppsSelected$2() {
        super(1);
    }

    @DexIgnore
    public final java.lang.String invoke(com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper appWrapper) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(appWrapper, "it");
        return java.lang.String.valueOf(appWrapper.getUri());
    }
}
