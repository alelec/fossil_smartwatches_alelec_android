package com.portfolio.platform.uirenew.home.details.sleep;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepDetailPresenter$findSleepSessions$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.portfolio.platform.data.model.room.sleep.MFSleepSession, java.lang.Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $date;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepDetailPresenter$findSleepSessions$1(java.util.Date date) {
        super(1);
        this.$date = date;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke((com.portfolio.platform.data.model.room.sleep.MFSleepSession) obj));
    }

    @DexIgnore
    public final boolean invoke(com.portfolio.platform.data.model.room.sleep.MFSleepSession mFSleepSession) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(mFSleepSession, "it");
        return com.fossil.blesdk.obfuscated.rk2.m27396d(mFSleepSession.getDay(), this.$date);
    }
}
