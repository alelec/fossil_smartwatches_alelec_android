package com.portfolio.platform.uirenew.home.details.heartrate;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1$summary$1", mo27670f = "HeartRateDetailPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class HeartRateDetailPresenter$showDayDetail$1$summary$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23728p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$showDayDetail$1$summary$1(com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1 heartRateDetailPresenter$showDayDetail$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = heartRateDetailPresenter$showDayDetail$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1$summary$1 heartRateDetailPresenter$showDayDetail$1$summary$1 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1$summary$1(this.this$0, yb4);
        heartRateDetailPresenter$showDayDetail$1$summary$1.f23728p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return heartRateDetailPresenter$showDayDetail$1$summary$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1$summary$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter heartRateDetailPresenter = this.this$0.this$0;
            return heartRateDetailPresenter.mo41418b(heartRateDetailPresenter.f23703g, (java.util.List<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>) this.this$0.this$0.f23705i);
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
