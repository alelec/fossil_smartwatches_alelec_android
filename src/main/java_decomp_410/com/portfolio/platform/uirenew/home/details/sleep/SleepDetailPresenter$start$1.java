package com.portfolio.platform.uirenew.home.details.sleep;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepDetailPresenter$start$1<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepDay>>> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter f23766a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1$1", mo27670f = "SleepDetailPresenter.kt", mo27671l = {82}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1$1 */
    public static final class C67351 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23767p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C67351(com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1 sleepDetailPresenter$start$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = sleepDetailPresenter$start$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1.C67351 r0 = new com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1.C67351(this.this$0, yb4);
            r0.f23767p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1.C67351) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23767p$;
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.f23766a.mo31440b();
                com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1$1$summary$1 sleepDetailPresenter$start$1$1$summary$1 = new com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1$1$summary$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, sleepDetailPresenter$start$1$1$summary$1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            com.portfolio.platform.data.model.room.sleep.MFSleepDay mFSleepDay = (com.portfolio.platform.data.model.room.sleep.MFSleepDay) obj;
            if (this.this$0.f23766a.f23749n == null || (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23766a.f23749n, (java.lang.Object) mFSleepDay))) {
                this.this$0.f23766a.f23749n = mFSleepDay;
                this.this$0.f23766a.f23753r.mo32015a(mFSleepDay);
                if (this.this$0.f23766a.f23745j && this.this$0.f23766a.f23746k) {
                    com.fossil.blesdk.obfuscated.fi4 unused = this.this$0.f23766a.mo41436k();
                    com.fossil.blesdk.obfuscated.fi4 unused2 = this.this$0.f23766a.mo41437l();
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public SleepDetailPresenter$start$1(com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter sleepDetailPresenter) {
        this.f23766a = sleepDetailPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepDay>> os3) {
        com.portfolio.platform.enums.Status a = os3.mo29972a();
        java.util.List list = (java.util.List) os3.mo29973b();
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("start - summaryTransformations -- sleepSummaries=");
        sb.append(list != null ? java.lang.Integer.valueOf(list.size()) : null);
        sb.append(", status=");
        sb.append(a);
        local.mo33255d("SleepDetailPresenter", sb.toString());
        if (a == com.portfolio.platform.enums.Status.NETWORK_LOADING || a == com.portfolio.platform.enums.Status.SUCCESS) {
            this.f23766a.f23747l = list;
            this.f23766a.f23745j = true;
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23766a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1.C67351(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        }
    }
}
