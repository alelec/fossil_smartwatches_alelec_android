package com.portfolio.platform.uirenew.home.details.calories;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$showDetailChart$Anon1$maxValue$Anon1", f = "CaloriesDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class CaloriesDetailPresenter$showDetailChart$Anon1$maxValue$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Integer>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $data;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CaloriesDetailPresenter$showDetailChart$Anon1$maxValue$Anon1(ArrayList arrayList, yb4 yb4) {
        super(2, yb4);
        this.$data = arrayList;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CaloriesDetailPresenter$showDetailChart$Anon1$maxValue$Anon1 caloriesDetailPresenter$showDetailChart$Anon1$maxValue$Anon1 = new CaloriesDetailPresenter$showDetailChart$Anon1$maxValue$Anon1(this.$data, yb4);
        caloriesDetailPresenter$showDetailChart$Anon1$maxValue$Anon1.p$ = (zg4) obj;
        return caloriesDetailPresenter$showDetailChart$Anon1$maxValue$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CaloriesDetailPresenter$showDetailChart$Anon1$maxValue$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            Iterator it = this.$data.iterator();
            int i = 0;
            if (!it.hasNext()) {
                obj2 = null;
            } else {
                obj2 = it.next();
                if (it.hasNext()) {
                    ArrayList<BarChart.b> arrayList = ((BarChart.a) obj2).c().get(0);
                    kd4.a((Object) arrayList, "it.mListOfBarPoints[0]");
                    int i2 = 0;
                    for (BarChart.b e : arrayList) {
                        i2 += dc4.a(e.e()).intValue();
                    }
                    Integer a = dc4.a(i2);
                    do {
                        Object next = it.next();
                        ArrayList<BarChart.b> arrayList2 = ((BarChart.a) next).c().get(0);
                        kd4.a((Object) arrayList2, "it.mListOfBarPoints[0]");
                        int i3 = 0;
                        for (BarChart.b e2 : arrayList2) {
                            i3 += dc4.a(e2.e()).intValue();
                        }
                        Integer a2 = dc4.a(i3);
                        if (a.compareTo(a2) < 0) {
                            obj2 = next;
                            a = a2;
                        }
                    } while (it.hasNext());
                }
            }
            BarChart.a aVar = (BarChart.a) obj2;
            if (aVar == null) {
                return null;
            }
            ArrayList<ArrayList<BarChart.b>> c = aVar.c();
            if (c == null) {
                return null;
            }
            ArrayList<BarChart.b> arrayList3 = c.get(0);
            if (arrayList3 == null) {
                return null;
            }
            for (BarChart.b e3 : arrayList3) {
                i += dc4.a(e3.e()).intValue();
            }
            return dc4.a(i);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
