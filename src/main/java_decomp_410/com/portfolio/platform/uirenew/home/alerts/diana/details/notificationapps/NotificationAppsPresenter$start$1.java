package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1", mo27670f = "NotificationAppsPresenter.kt", mo27671l = {130, 136, 190, 198, 207, 216}, mo27672m = "invokeSuspend")
public final class NotificationAppsPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22357p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$1", mo27670f = "NotificationAppsPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$1 */
    public static final class C63171 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22358p$;

        @DexIgnore
        public C63171(com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63171 r0 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63171(yb4);
            r0.f22358p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63171) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34463J();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$6")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$6", mo27670f = "NotificationAppsPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$6 */
    public static final class C63186 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $notificationSettings;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22359p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$6$a")
        /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$6$a */
        public static final class C6319a implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63186 f22360e;

            @DexIgnore
            public C6319a(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63186 r1) {
                this.f22360e = r1;
            }

            @DexIgnore
            public final void run() {
                this.f22360e.this$0.this$0.f22354s.getNotificationSettingsDao().insertListNotificationSettings(this.f22360e.$notificationSettings);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C63186(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1 notificationAppsPresenter$start$1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = notificationAppsPresenter$start$1;
            this.$notificationSettings = list;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63186 r0 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63186(this.this$0, this.$notificationSettings, yb4);
            r0.f22359p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63186) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.f22354s.runInTransaction((java.lang.Runnable) new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63186.C6319a(this));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$7")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$7", mo27670f = "NotificationAppsPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$7 */
    public static final class C63207 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $listNotificationSettings;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22361p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C63207(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1 notificationAppsPresenter$start$1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = notificationAppsPresenter$start$1;
            this.$listNotificationSettings = list;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63207 r0 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63207(this.this$0, this.$listNotificationSettings, yb4);
            r0.f22361p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63207) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                java.util.Iterator it = this.$listNotificationSettings.iterator();
                while (it.hasNext()) {
                    com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel = (com.portfolio.platform.data.model.NotificationSettingsModel) it.next();
                    int component2 = notificationSettingsModel.component2();
                    int i = 0;
                    if (notificationSettingsModel.component3()) {
                        java.lang.String a = this.this$0.this$0.mo40808a(component2);
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a2 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.f22340u.mo40818a();
                        local.mo33255d(a2, "CALL settingsTypeName=" + a);
                        if (component2 == 0) {
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String a3 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.f22340u.mo40818a();
                            local2.mo33255d(a3, "mListAppNotificationFilter add - " + com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL);
                            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                            com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName.getAppName(), aApplicationName.getBundleCrc(), aApplicationName.getGroupId(), aApplicationName.getPackageName(), aApplicationName.getIconResourceId(), aApplicationName.getIconFwPath(), aApplicationName.getNotificationType());
                            this.this$0.this$0.f22345j.add(new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification));
                        } else if (component2 == 1) {
                            int size = this.this$0.this$0.f22346k.size();
                            int i2 = 0;
                            while (i2 < size) {
                                com.fossil.wearables.fsl.contact.ContactGroup contactGroup = (com.fossil.wearables.fsl.contact.ContactGroup) this.this$0.this$0.f22346k.get(i2);
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                java.lang.String a4 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.f22340u.mo40818a();
                                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                                sb.append("mListAppNotificationFilter add PHONE item - ");
                                sb.append(i2);
                                sb.append(" name = ");
                                com.fossil.wearables.fsl.contact.Contact contact = contactGroup.getContacts().get(i);
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact, "item.contacts[0]");
                                sb.append(contact.getDisplayName());
                                local3.mo33255d(a4, sb.toString());
                                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName2 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification2 = r9;
                                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification3 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName2.getAppName(), aApplicationName2.getBundleCrc(), aApplicationName2.getGroupId(), aApplicationName2.getPackageName(), aApplicationName2.getIconResourceId(), aApplicationName2.getIconFwPath(), aApplicationName2.getNotificationType());
                                com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter appNotificationFilter = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification2);
                                com.fossil.wearables.fsl.contact.Contact contact2 = contactGroup.getContacts().get(0);
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact2, "item.contacts[0]");
                                appNotificationFilter.setSender(contact2.getDisplayName());
                                this.this$0.this$0.f22345j.add(appNotificationFilter);
                                i2++;
                                i = 0;
                            }
                        }
                    } else {
                        java.lang.String a5 = this.this$0.this$0.mo40808a(component2);
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a6 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.f22340u.mo40818a();
                        local4.mo33255d(a6, "MESSAGE settingsTypeName=" + a5);
                        if (component2 == 0) {
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String a7 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.f22340u.mo40818a();
                            local5.mo33255d(a7, "mListAppNotificationFilter add - " + com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.MESSAGES);
                            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName3 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.MESSAGES;
                            com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification4 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName3.getAppName(), aApplicationName3.getBundleCrc(), aApplicationName3.getGroupId(), aApplicationName3.getPackageName(), aApplicationName3.getIconResourceId(), aApplicationName3.getIconFwPath(), aApplicationName3.getNotificationType());
                            this.this$0.this$0.f22345j.add(new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification4));
                        } else if (component2 == 1) {
                            int size2 = this.this$0.this$0.f22346k.size();
                            int i3 = 0;
                            while (i3 < size2) {
                                com.fossil.wearables.fsl.contact.ContactGroup contactGroup2 = (com.fossil.wearables.fsl.contact.ContactGroup) this.this$0.this$0.f22346k.get(i3);
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                java.lang.String a8 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.f22340u.mo40818a();
                                java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
                                sb2.append("mListAppNotificationFilter add MESSAGE item - ");
                                sb2.append(i3);
                                sb2.append(" name = ");
                                com.fossil.wearables.fsl.contact.Contact contact3 = contactGroup2.getContacts().get(0);
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact3, "item.contacts[0]");
                                sb2.append(contact3.getDisplayName());
                                local6.mo33255d(a8, sb2.toString());
                                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName4 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.MESSAGES;
                                java.util.Iterator it2 = it;
                                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification5 = r8;
                                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification6 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName4.getAppName(), aApplicationName4.getBundleCrc(), aApplicationName4.getGroupId(), aApplicationName4.getPackageName(), aApplicationName4.getIconResourceId(), aApplicationName4.getIconFwPath(), aApplicationName4.getNotificationType());
                                com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter appNotificationFilter2 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification5);
                                com.fossil.wearables.fsl.contact.Contact contact4 = contactGroup2.getContacts().get(0);
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact4, "item.contacts[0]");
                                appNotificationFilter2.setSender(contact4.getDisplayName());
                                this.this$0.this$0.f22345j.add(appNotificationFilter2);
                                i3++;
                                it = it2;
                            }
                        }
                    }
                    it = it;
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationAppsPresenter$start$1(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter notificationAppsPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = notificationAppsPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1 notificationAppsPresenter$start$1 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1(this.this$0, yb4);
        notificationAppsPresenter$start$1.f22357p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationAppsPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0293, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0294, code lost:
        r1 = (com.portfolio.platform.CoroutineUseCase.C5604c) r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0299, code lost:
        if ((r1 instanceof com.fossil.blesdk.obfuscated.px2.C4879d) == false) goto L_0x037f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x029b, code lost:
        r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r6 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.f22340u.mo40818a();
        r7 = new java.lang.StringBuilder();
        r7.append("GetAllContactGroup onSuccess, size = ");
        r8 = (com.fossil.blesdk.obfuscated.px2.C4879d) r1;
        r7.append(r8.mo30277a().size());
        r13.mo33255d(r6, r7.toString());
        r12.this$0.f22346k.clear();
        r12.this$0.f22346k = com.fossil.blesdk.obfuscated.kb4.m24381d(r8.mo30277a());
        r13 = r12.this$0.mo31441c();
        r6 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$listNotificationSettings$1(r12, (com.fossil.blesdk.obfuscated.yb4) null);
        r12.L$0 = r5;
        r12.L$1 = r1;
        r12.label = 4;
        r13 = com.fossil.blesdk.obfuscated.yf4.m30997a(r13, r6, r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x02f2, code lost:
        if (r13 != r0) goto L_0x02f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x02f4, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x02f5, code lost:
        r13 = (java.util.List) r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x02fb, code lost:
        if (r13.isEmpty() == false) goto L_0x0337;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x02fd, code lost:
        r6 = new java.util.ArrayList();
        r7 = new com.portfolio.platform.data.model.NotificationSettingsModel("AllowCallsFrom", 0, true);
        r2 = new com.portfolio.platform.data.model.NotificationSettingsModel("AllowMessagesFrom", 0, false);
        r6.add(r7);
        r6.add(r2);
        r3 = r12.this$0.mo31441c();
        r8 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63186(r12, r6, (com.fossil.blesdk.obfuscated.yb4) null);
        r12.L$0 = r5;
        r12.L$1 = r1;
        r12.L$2 = r13;
        r12.L$3 = r6;
        r12.L$4 = r7;
        r12.L$5 = r2;
        r12.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0334, code lost:
        if (com.fossil.blesdk.obfuscated.yf4.m30997a(r3, r8, r12) != r0) goto L_0x0394;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0336, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0337, code lost:
        r12.this$0.f22345j.clear();
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.f22340u.mo40818a(), "listNotificationSettings.size = " + r13.size());
        r2 = r12.this$0.mo31440b();
        r3 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63207(r12, r13, (com.fossil.blesdk.obfuscated.yb4) null);
        r12.L$0 = r5;
        r12.L$1 = r1;
        r12.L$2 = r13;
        r12.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x037c, code lost:
        if (com.fossil.blesdk.obfuscated.yf4.m30997a(r2, r3, r12) != r0) goto L_0x0394;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x037e, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0381, code lost:
        if ((r1 instanceof com.fossil.blesdk.obfuscated.px2.C4877b) == false) goto L_0x0394;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0383, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.f22340u.mo40818a(), "GetAllContactGroup onError");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0394, code lost:
        r12.this$0.f22348m.mo26373h(r12.this$0.mo40812k());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x03a5, code lost:
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x008d, code lost:
        if (r12.this$0.mo40813l().isEmpty() == false) goto L_0x0274;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x008f, code lost:
        r13 = r12.this$0.mo31440b();
        r5 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$getAppResponse$1(r12, (com.fossil.blesdk.obfuscated.yb4) null);
        r12.L$0 = r1;
        r12.label = 2;
        r13 = com.fossil.blesdk.obfuscated.yf4.m30997a(r13, r5, r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00a3, code lost:
        if (r13 != r0) goto L_0x00a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00a5, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00a6, code lost:
        r13 = (com.portfolio.platform.CoroutineUseCase.C5604c) r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00aa, code lost:
        if ((r13 instanceof com.fossil.blesdk.obfuscated.vy2.C5306a) == false) goto L_0x0255;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00ac, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.f22340u.mo40818a(), "GetApps onSuccess");
        r12.this$0.f22348m.mo26368a();
        r13 = ((com.fossil.blesdk.obfuscated.vy2.C5306a) r13).mo31940a().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00d4, code lost:
        if (r13.hasNext() == false) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00d6, code lost:
        r5 = r13.next();
        r6 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion;
        r7 = r5.getInstalledApp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00e2, code lost:
        if (r7 == null) goto L_0x00fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00e4, code lost:
        r7 = r7.getIdentifier();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r7, "app.installedApp!!.identifier");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00f1, code lost:
        if (r6.isPackageNameSupportedBySDK(r7) == false) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00f3, code lost:
        r12.this$0.mo40813l().add(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00fd, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0100, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0101, code lost:
        r13 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.values();
        r5 = r13.length;
        r6 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0107, code lost:
        if (r6 >= r5) goto L_0x0196;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0109, code lost:
        r7 = r13[r6];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x010d, code lost:
        if (r7 == com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL) goto L_0x0192;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0111, code lost:
        if (r7 == com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_MISSED_CALL) goto L_0x0192;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0115, code lost:
        if (r7 == com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.MESSAGES) goto L_0x0192;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0126, code lost:
        if ((!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) r7.getPackageName(), (java.lang.Object) com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.FOSSIL.getPackageName())) == false) goto L_0x0192;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0128, code lost:
        r8 = r12.this$0.mo40813l().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0136, code lost:
        if (r8.hasNext() == false) goto L_0x015e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0138, code lost:
        r9 = r8.next();
        r10 = ((com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper) r9).getInstalledApp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0143, code lost:
        if (r10 == null) goto L_0x014a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0145, code lost:
        r10 = r10.getIdentifier();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x014a, code lost:
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x015b, code lost:
        if (com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) r10, (java.lang.Object) r7.getPackageName())).booleanValue() == false) goto L_0x0132;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x015e, code lost:
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0161, code lost:
        if (((com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper) r9) != null) goto L_0x0192;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0163, code lost:
        r8 = new com.portfolio.platform.data.model.InstalledApp(r7.getPackageName(), r7.getAppName(), com.fossil.blesdk.obfuscated.dc4.m20839a(false));
        r9 = new com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper();
        r9.setInstalledApp(r8);
        r9.setUri((android.net.Uri) null);
        r9.setIconResourceId(r7.getIconResourceId());
        r9.setCurrentHandGroup(0);
        r12.this$0.mo40813l().add(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0192, code lost:
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0196, code lost:
        r13 = r12.this$0.mo40813l().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x01a4, code lost:
        if (r13.hasNext() == false) goto L_0x01cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0020, code lost:
        r0 = (java.util.List) r12.L$2;
        r0 = (com.portfolio.platform.CoroutineUseCase.C5604c) r12.L$1;
        r0 = (com.fossil.blesdk.obfuscated.zg4) r12.L$0;
        com.fossil.blesdk.obfuscated.na4.m25642a(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01a6, code lost:
        r5 = r13.next().getInstalledApp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01b0, code lost:
        if (r5 == null) goto L_0x01a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01b2, code lost:
        r6 = r5.isSelected();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r6, "it.isSelected");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x01bf, code lost:
        if (r6.booleanValue() == false) goto L_0x01a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x01c1, code lost:
        r12.this$0.f22343h.add(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x01d1, code lost:
        if (r12.this$0.mo40812k() == false) goto L_0x01d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01d3, code lost:
        r12.this$0.mo40810c(true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01de, code lost:
        if (r12.this$0.mo40812k() != false) goto L_0x021f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01e0, code lost:
        r13 = r12.this$0.mo40813l();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01e8, code lost:
        if ((r13 instanceof java.util.Collection) == false) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01ee, code lost:
        if (r13.isEmpty() == false) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01f0, code lost:
        r13 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01f2, code lost:
        r13 = r13.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01fa, code lost:
        if (r13.hasNext() == false) goto L_0x01f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01fc, code lost:
        r5 = ((com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper) r13.next()).getInstalledApp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0206, code lost:
        if (r5 == null) goto L_0x0219;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0208, code lost:
        r5 = r5.isSelected();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r5, "it.installedApp!!.isSelected");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0215, code lost:
        if (r5.booleanValue() == false) goto L_0x01f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0217, code lost:
        r13 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0219, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x021c, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x021d, code lost:
        if (r13 == false) goto L_0x023d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x021f, code lost:
        r13 = r12.this$0.f22348m;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0225, code lost:
        if (r13 == null) goto L_0x024d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0227, code lost:
        r6 = ((com.fossil.blesdk.obfuscated.dw2) r13).getContext();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x022d, code lost:
        if (r6 == null) goto L_0x023d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0045, code lost:
        r5 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x022f, code lost:
        com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.bn2.m20275a(com.fossil.blesdk.obfuscated.bn2.f13679d, r6, "NOTIFICATION_APPS", false, 4, (java.lang.Object) null));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x023d, code lost:
        r12.this$0.f22348m.mo26372g(r12.this$0.mo40813l());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0254, code lost:
        throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0257, code lost:
        if ((r13 instanceof com.portfolio.platform.CoroutineUseCase.C5602a) == false) goto L_0x027d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0259, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.f22340u.mo40818a(), "GetApps onError");
        r12.this$0.f22348m.mo26368a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0274, code lost:
        r12.this$0.f22348m.mo26368a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x027d, code lost:
        r13 = r12.this$0.mo31440b();
        r5 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$getAllContactGroupsResponse$1(r12, (com.fossil.blesdk.obfuscated.yb4) null);
        r12.L$0 = r1;
        r12.label = 3;
        r13 = com.fossil.blesdk.obfuscated.yf4.m30997a(r13, r5, r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0291, code lost:
        if (r13 != r0) goto L_0x0045;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        switch (this.label) {
            case 0:
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                zg4 = this.f22357p$;
                if (!com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34574u().mo26976N()) {
                    com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
                    com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63171 r5 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1.C63171((com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg4;
                    this.label = 1;
                    if (com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r5, this) == a) {
                        return a;
                    }
                }
                break;
            case 1:
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 2:
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 3:
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 4:
                com.portfolio.platform.CoroutineUseCase.C5604c cVar = (com.portfolio.platform.CoroutineUseCase.C5604c) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 5:
                com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel = (com.portfolio.platform.data.model.NotificationSettingsModel) this.L$5;
                com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel2 = (com.portfolio.platform.data.model.NotificationSettingsModel) this.L$4;
                java.util.List list = (java.util.List) this.L$3;
                break;
            case 6:
                break;
            default:
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
