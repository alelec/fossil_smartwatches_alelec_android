package com.portfolio.platform.uirenew.home.customize.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$createNewPreset$1", mo27670f = "HomeDianaCustomizePresenter.kt", mo27671l = {321}, mo27672m = "invokeSuspend")
public final class HomeDianaCustomizePresenter$createNewPreset$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22697p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$createNewPreset$1(com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter homeDianaCustomizePresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homeDianaCustomizePresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$createNewPreset$1 homeDianaCustomizePresenter$createNewPreset$1 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$createNewPreset$1(this.this$0, yb4);
        homeDianaCustomizePresenter$createNewPreset$1.f22697p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeDianaCustomizePresenter$createNewPreset$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$createNewPreset$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object obj2;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22697p$;
            java.util.Iterator it = this.this$0.f22668j.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj2 = null;
                    break;
                }
                obj2 = it.next();
                if (com.fossil.blesdk.obfuscated.dc4.m20839a(((com.portfolio.platform.data.model.diana.preset.DianaPreset) obj2).isActive()).booleanValue()) {
                    break;
                }
            }
            com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset = (com.portfolio.platform.data.model.diana.preset.DianaPreset) obj2;
            if (dianaPreset != null) {
                com.portfolio.platform.data.model.diana.preset.DianaPreset cloneFrom = com.portfolio.platform.data.model.diana.preset.DianaPreset.Companion.cloneFrom(dianaPreset);
                com.fossil.blesdk.obfuscated.ug4 d = this.this$0.mo31441c();
                com.portfolio.platform.uirenew.home.customize.diana.C6418x1ad55037 homeDianaCustomizePresenter$createNewPreset$1$invokeSuspend$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.customize.diana.C6418x1ad55037(cloneFrom, (com.fossil.blesdk.obfuscated.yb4) null, this);
                this.L$0 = zg4;
                this.L$1 = dianaPreset;
                this.L$2 = dianaPreset;
                this.L$3 = cloneFrom;
                this.label = 1;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(d, homeDianaCustomizePresenter$createNewPreset$1$invokeSuspend$$inlined$let$lambda$1, this) == a) {
                    return a;
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset2 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) this.L$3;
            com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset3 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) this.L$2;
            com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset4 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.this$0;
        homeDianaCustomizePresenter.mo40998c(homeDianaCustomizePresenter.mo40999k() + 1);
        this.this$0.f22678t.mo26433v();
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
