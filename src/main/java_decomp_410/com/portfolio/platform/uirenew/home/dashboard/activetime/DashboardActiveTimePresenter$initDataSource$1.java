package com.portfolio.platform.uirenew.home.dashboard.activetime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1", mo27670f = "DashboardActiveTimePresenter.kt", mo27671l = {63}, mo27672m = "invokeSuspend")
public final class DashboardActiveTimePresenter$initDataSource$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23118p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1$a")
    /* renamed from: com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1$a */
    public static final class C6566a<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.C2723qd<com.portfolio.platform.data.model.room.fitness.ActivitySummary>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1 f23119a;

        @DexIgnore
        public C6566a(com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1 dashboardActiveTimePresenter$initDataSource$1) {
            this.f23119a = dashboardActiveTimePresenter$initDataSource$1;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(com.fossil.blesdk.obfuscated.C2723qd<com.portfolio.platform.data.model.room.fitness.ActivitySummary> qdVar) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("getSummariesPaging observer size=");
            sb.append(qdVar != null ? java.lang.Integer.valueOf(qdVar.size()) : null);
            local.mo33255d("DashboardActiveTimePresenter", sb.toString());
            if (qdVar != null) {
                this.f23119a.this$0.f23110h.mo25654a(qdVar);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardActiveTimePresenter$initDataSource$1(com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter dashboardActiveTimePresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = dashboardActiveTimePresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1 dashboardActiveTimePresenter$initDataSource$1 = new com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1(this.this$0, yb4);
        dashboardActiveTimePresenter$initDataSource$1.f23118p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dashboardActiveTimePresenter$initDataSource$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23118p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1$user$1 dashboardActiveTimePresenter$initDataSource$1$user$1 = new com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1$user$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, dashboardActiveTimePresenter$initDataSource$1$user$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) obj;
        if (mFUser != null) {
            java.util.Date d = com.fossil.blesdk.obfuscated.rk2.m27394d(mFUser.getCreatedAt());
            com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter dashboardActiveTimePresenter = this.this$0;
            com.portfolio.platform.data.source.SummariesRepository h = dashboardActiveTimePresenter.f23111i;
            com.portfolio.platform.data.source.SummariesRepository h2 = this.this$0.f23111i;
            com.fossil.blesdk.obfuscated.xk2 g = this.this$0.f23117o;
            com.portfolio.platform.data.source.FitnessDataRepository e = this.this$0.f23112j;
            com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao c = this.this$0.f23113k;
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase f = this.this$0.f23114l;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) d, "createdDate");
            dashboardActiveTimePresenter.f23109g = h.getSummariesPaging(h2, g, e, c, f, d, this.this$0.f23116n, this.this$0);
            com.portfolio.platform.data.Listing b = this.this$0.f23109g;
            if (b != null) {
                androidx.lifecycle.LiveData pagedList = b.getPagedList();
                if (pagedList != null) {
                    com.fossil.blesdk.obfuscated.z73 j = this.this$0.f23110h;
                    if (j != null) {
                        pagedList.mo2277a((com.fossil.blesdk.obfuscated.a83) j, new com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1.C6566a(this));
                    } else {
                        throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment");
                    }
                }
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
