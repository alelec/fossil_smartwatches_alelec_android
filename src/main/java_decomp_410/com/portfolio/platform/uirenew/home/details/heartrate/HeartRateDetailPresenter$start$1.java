package com.portfolio.platform.uirenew.home.details.heartrate;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1", mo27670f = "HeartRateDetailPresenter.kt", mo27671l = {110}, mo27672m = "invokeSuspend")
public final class HeartRateDetailPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23734p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1$a")
    /* renamed from: com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1$a */
    public static final class C6728a<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1 f23735a;

        @DexIgnore
        public C6728a(com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1 heartRateDetailPresenter$start$1) {
            this.f23735a = heartRateDetailPresenter$start$1;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>> os3) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("start - summaryTransformations -- status=");
            com.portfolio.platform.enums.Status status = null;
            sb.append(os3 != null ? os3.mo29978f() : null);
            sb.append(", resource=");
            sb.append(os3 != null ? (java.util.List) os3.mo29975d() : null);
            sb.append(", status=");
            if (os3 != null) {
                status = os3.mo29978f();
            }
            sb.append(status);
            local.mo33255d("HeartRateDetailPresenter", sb.toString());
            if (os3 != null && os3.mo29978f() != com.portfolio.platform.enums.Status.DATABASE_LOADING) {
                this.f23735a.this$0.f23705i = (java.util.List) os3.mo29975d();
                com.fossil.blesdk.obfuscated.fi4 unused = this.f23735a.this$0.mo41421l();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1$b")
    /* renamed from: com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1$b */
    public static final class C6729b<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.diana.heartrate.HeartRateSample>>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1 f23736a;

        @DexIgnore
        public C6729b(com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1 heartRateDetailPresenter$start$1) {
            this.f23736a = heartRateDetailPresenter$start$1;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.diana.heartrate.HeartRateSample>> os3) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("start - sampleTransformations -- status=");
            com.portfolio.platform.enums.Status status = null;
            sb.append(os3 != null ? os3.mo29978f() : null);
            sb.append(", resource=");
            sb.append(os3 != null ? (java.util.List) os3.mo29975d() : null);
            sb.append(", status=");
            if (os3 != null) {
                status = os3.mo29978f();
            }
            sb.append(status);
            local.mo33255d("HeartRateDetailPresenter", sb.toString());
            if (os3 != null && os3.mo29978f() != com.portfolio.platform.enums.Status.DATABASE_LOADING) {
                this.f23736a.this$0.f23706j = (java.util.List) os3.mo29975d();
                com.fossil.blesdk.obfuscated.fi4 unused = this.f23736a.this$0.mo41422m();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$start$1(com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter heartRateDetailPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = heartRateDetailPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1 heartRateDetailPresenter$start$1 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1(this.this$0, yb4);
        heartRateDetailPresenter$start$1.f23734p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return heartRateDetailPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0041, code lost:
        if (r6 != null) goto L_0x0046;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.enums.Unit unit;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23734p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1$currentUser$1 heartRateDetailPresenter$start$1$currentUser$1 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1$currentUser$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, heartRateDetailPresenter$start$1$currentUser$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) obj;
        com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter heartRateDetailPresenter = this.this$0;
        if (mFUser != null) {
            unit = mFUser.getDistanceUnit();
        }
        unit = com.portfolio.platform.enums.Unit.METRIC;
        heartRateDetailPresenter.f23709m = unit;
        androidx.lifecycle.LiveData o = this.this$0.f23710n;
        com.fossil.blesdk.obfuscated.nf3 m = this.this$0.f23713q;
        if (m != null) {
            o.mo2277a((com.fossil.blesdk.obfuscated.of3) m, new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1.C6728a(this));
            this.this$0.f23711o.mo2277a((androidx.lifecycle.LifecycleOwner) this.this$0.f23713q, new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1.C6729b(this));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
    }
}
