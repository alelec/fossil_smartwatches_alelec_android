package com.portfolio.platform.uirenew.home.details.calories;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$showDetailChart$1", mo27670f = "CaloriesDetailPresenter.kt", mo27671l = {240, 242}, mo27672m = "invokeSuspend")
public final class CaloriesDetailPresenter$showDetailChart$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23653p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CaloriesDetailPresenter$showDetailChart$1(com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter caloriesDetailPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = caloriesDetailPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$showDetailChart$1 caloriesDetailPresenter$showDetailChart$1 = new com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$showDetailChart$1(this.this$0, yb4);
        caloriesDetailPresenter$showDetailChart$1.f23653p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return caloriesDetailPresenter$showDetailChart$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$showDetailChart$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00d4  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        kotlin.Pair pair;
        java.util.ArrayList arrayList;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f23653p$;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("XXX", "showDetailChart with summary " + this.this$0.f23632m);
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$showDetailChart$1$pair$1 caloriesDetailPresenter$showDetailChart$1$pair$1 = new com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$showDetailChart$1$pair$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, caloriesDetailPresenter$showDetailChart$1$pair$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            arrayList = (java.util.ArrayList) this.L$2;
            pair = (kotlin.Pair) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.lang.Integer num = (java.lang.Integer) obj;
            int a3 = com.fossil.blesdk.obfuscated.xk2.f20276d.mo32688a(this.this$0.f23632m, com.portfolio.platform.enums.GoalType.CALORIES);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local2.mo33255d("XXX", "goalValue " + a3 + " maxValue " + num);
            this.this$0.f23638s.mo31790a((com.fossil.blesdk.obfuscated.wr2) new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c(java.lang.Math.max(num == null ? num.intValue() : 0, a3 / 16), a3, arrayList), (java.util.ArrayList<java.lang.String>) (java.util.ArrayList) pair.getSecond());
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        kotlin.Pair pair2 = (kotlin.Pair) obj;
        java.util.ArrayList arrayList2 = (java.util.ArrayList) pair2.getFirst();
        com.fossil.blesdk.obfuscated.ug4 a4 = this.this$0.mo31440b();
        com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$showDetailChart$1$maxValue$1 caloriesDetailPresenter$showDetailChart$1$maxValue$1 = new com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$showDetailChart$1$maxValue$1(arrayList2, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.L$1 = pair2;
        this.L$2 = arrayList2;
        this.label = 2;
        java.lang.Object a5 = com.fossil.blesdk.obfuscated.yf4.m30997a(a4, caloriesDetailPresenter$showDetailChart$1$maxValue$1, this);
        if (a5 == a) {
            return a;
        }
        arrayList = arrayList2;
        java.lang.Object obj2 = a5;
        pair = pair2;
        obj = obj2;
        java.lang.Integer num2 = (java.lang.Integer) obj;
        int a32 = com.fossil.blesdk.obfuscated.xk2.f20276d.mo32688a(this.this$0.f23632m, com.portfolio.platform.enums.GoalType.CALORIES);
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local22 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local22.mo33255d("XXX", "goalValue " + a32 + " maxValue " + num2);
        this.this$0.f23638s.mo31790a((com.fossil.blesdk.obfuscated.wr2) new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c(java.lang.Math.max(num2 == null ? num2.intValue() : 0, a32 / 16), a32, arrayList), (java.util.ArrayList<java.lang.String>) (java.util.ArrayList) pair.getSecond());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
