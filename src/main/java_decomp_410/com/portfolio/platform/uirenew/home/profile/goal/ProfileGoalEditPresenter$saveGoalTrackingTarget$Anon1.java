package com.portfolio.platform.uirenew.home.profile.goal;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.vh3;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1", f = "ProfileGoalEditPresenter.kt", l = {58}, m = "invokeSuspend")
public final class ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileGoalEditPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1$Anon1", f = "ProfileGoalEditPresenter.kt", l = {59}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements GoalTrackingRepository.UpdateGoalSettingCallback {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 a;

            @DexIgnore
            public a(Anon1 anon1) {
                this.a = anon1;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:3:0x001d, code lost:
                if (r3 != null) goto L_0x0022;
             */
            @DexIgnore
            public void onFail(po2<GoalSetting> po2) {
                String str;
                kd4.b(po2, "error");
                vh3 m = this.a.this$Anon0.this$Anon0.m();
                int a2 = po2.a();
                ServerError c = po2.c();
                if (c != null) {
                    str = c.getMessage();
                }
                str = "";
                m.d(a2, str);
                this.a.this$Anon0.this$Anon0.o();
            }

            @DexIgnore
            public void onSuccess(ro2<GoalSetting> ro2) {
                kd4.b(ro2, "success");
                this.a.this$Anon0.this$Anon0.q();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1 profileGoalEditPresenter$saveGoalTrackingTarget$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = profileGoalEditPresenter$saveGoalTrackingTarget$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a2 = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                GoalTrackingRepository c = this.this$Anon0.this$Anon0.s;
                GoalSetting goalSetting = new GoalSetting(this.this$Anon0.this$Anon0.l);
                a aVar = new a(this);
                this.L$Anon0 = zg4;
                this.label = 1;
                if (c.updateGoalSetting(goalSetting, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return qa4.a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1(ProfileGoalEditPresenter profileGoalEditPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = profileGoalEditPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1 profileGoalEditPresenter$saveGoalTrackingTarget$Anon1 = new ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1(this.this$Anon0, yb4);
        profileGoalEditPresenter$saveGoalTrackingTarget$Anon1.p$ = (zg4) obj;
        return profileGoalEditPresenter$saveGoalTrackingTarget$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 b = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            if (yf4.a(b, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
