package com.portfolio.platform.uirenew.home.customize.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1 */
public final class C6419xffc74df5 implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase.C6507d, com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase.C6505b> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.diana.preset.DianaPreset f22687a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter f22688b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1$1")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1$1 */
    public static final class C64201 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22689p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.C6419xffc74df5 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1$1$1")
        /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1$1$1 */
        public static final class C64211 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f22690p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.C6419xffc74df5.C64201 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C64211(com.portfolio.platform.uirenew.home.customize.diana.C6419xffc74df5.C64201 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.customize.diana.C6419xffc74df5.C64201.C64211 r0 = new com.portfolio.platform.uirenew.home.customize.diana.C6419xffc74df5.C64201.C64211(this.this$0, yb4);
                r0.f22690p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.customize.diana.C6419xffc74df5.C64201.C64211) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22690p$;
                    com.portfolio.platform.data.source.DianaPresetRepository j = this.this$0.this$0.f22688b.f22681w;
                    com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset = this.this$0.this$0.f22687a;
                    if (dianaPreset != null) {
                        java.lang.String id = dianaPreset.getId();
                        this.L$0 = zg4;
                        this.label = 1;
                        if (j.deletePresetById(id, this) == a) {
                            return a;
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                } else if (i == 1) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64201(com.portfolio.platform.uirenew.home.customize.diana.C6419xffc74df5 homeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = homeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.C6419xffc74df5.C64201 r0 = new com.portfolio.platform.uirenew.home.customize.diana.C6419xffc74df5.C64201(this.this$0, yb4);
            r0.f22689p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.C6419xffc74df5.C64201) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22689p$;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append("set new preset to watch success, delete current active ");
                com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset = this.this$0.f22687a;
                sb.append(dianaPreset != null ? dianaPreset.getName() : null);
                local.mo33255d("HomeDianaCustomizePresenter", sb.toString());
                com.fossil.blesdk.obfuscated.ug4 d = this.this$0.f22688b.mo31441c();
                com.portfolio.platform.uirenew.home.customize.diana.C6419xffc74df5.C64201.C64211 r3 = new com.portfolio.platform.uirenew.home.customize.diana.C6419xffc74df5.C64201.C64211(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(d, r3, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.f22688b.f22678t.mo26432m();
            this.this$0.f22688b.f22678t.mo26424c(this.this$0.f22688b.mo40999k() - 1);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public C6419xffc74df5(com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset, com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter homeDianaCustomizePresenter, java.lang.String str) {
        this.f22687a = dianaPreset;
        this.f22688b = homeDianaCustomizePresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase.C6507d dVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f22688b.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.customize.diana.C6419xffc74df5.C64201(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo29641a(com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase.C6505b bVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bVar, "errorValue");
        this.f22688b.f22678t.mo26432m();
        int b = bVar.mo41118b();
        if (b == 1101 || b == 1112 || b == 1113) {
            java.util.List<com.portfolio.platform.enums.PermissionCodes> convertBLEPermissionErrorCode = com.portfolio.platform.enums.PermissionCodes.convertBLEPermissionErrorCode(bVar.mo41117a());
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            com.fossil.blesdk.obfuscated.d23 r = this.f22688b.f22678t;
            java.lang.Object[] array = convertBLEPermissionErrorCode.toArray(new com.portfolio.platform.enums.PermissionCodes[0]);
            if (array != null) {
                com.portfolio.platform.enums.PermissionCodes[] permissionCodesArr = (com.portfolio.platform.enums.PermissionCodes[]) array;
                r.mo26045a((com.portfolio.platform.enums.PermissionCodes[]) java.util.Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                return;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        this.f22688b.f22678t.mo26430j();
    }
}
