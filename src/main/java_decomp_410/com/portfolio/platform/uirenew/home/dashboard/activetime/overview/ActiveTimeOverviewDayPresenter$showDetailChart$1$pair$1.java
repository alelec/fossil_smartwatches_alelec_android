package com.portfolio.platform.uirenew.home.dashboard.activetime.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$pair$1", mo27670f = "ActiveTimeOverviewDayPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class ActiveTimeOverviewDayPresenter$showDetailChart$1$pair$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super kotlin.Pair<? extends java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a>, ? extends java.util.ArrayList<java.lang.String>>>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23137p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActiveTimeOverviewDayPresenter$showDetailChart$1$pair$1(com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1 activeTimeOverviewDayPresenter$showDetailChart$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = activeTimeOverviewDayPresenter$showDetailChart$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$pair$1 activeTimeOverviewDayPresenter$showDetailChart$1$pair$1 = new com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$pair$1(this.this$0, yb4);
        activeTimeOverviewDayPresenter$showDetailChart$1$pair$1.f23137p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return activeTimeOverviewDayPresenter$showDetailChart$1$pair$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$pair$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.eg3 eg3 = com.fossil.blesdk.obfuscated.eg3.f14494a;
            java.util.Date d = com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter.m34442d(this.this$0.this$0);
            com.fossil.blesdk.obfuscated.os3 os3 = (com.fossil.blesdk.obfuscated.os3) this.this$0.this$0.f23125j.mo2275a();
            return eg3.mo26891a(d, (java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>) os3 != null ? (java.util.List) os3.mo29975d() : null, 1);
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
