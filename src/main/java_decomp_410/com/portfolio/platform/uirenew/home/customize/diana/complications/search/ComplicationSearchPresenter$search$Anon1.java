package com.portfolio.platform.uirenew.home.customize.diana.complications.search;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.diana.Complication;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$Anon1", f = "ComplicationSearchPresenter.kt", l = {75}, m = "invokeSuspend")
public final class ComplicationSearchPresenter$search$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $query;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$ObjectRef $results;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ComplicationSearchPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$Anon1$Anon1", f = "ComplicationSearchPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<Complication>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationSearchPresenter$search$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ComplicationSearchPresenter$search$Anon1 complicationSearchPresenter$search$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = complicationSearchPresenter$search$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return kb4.d(this.this$Anon0.this$Anon0.n.queryComplicationByName(this.this$Anon0.$query));
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ComplicationSearchPresenter$search$Anon1(ComplicationSearchPresenter complicationSearchPresenter, String str, Ref$ObjectRef ref$ObjectRef, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = complicationSearchPresenter;
        this.$query = str;
        this.$results = ref$ObjectRef;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ComplicationSearchPresenter$search$Anon1 complicationSearchPresenter$search$Anon1 = new ComplicationSearchPresenter$search$Anon1(this.this$Anon0, this.$query, this.$results, yb4);
        complicationSearchPresenter$search$Anon1.p$ = (zg4) obj;
        return complicationSearchPresenter$search$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ComplicationSearchPresenter$search$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(T t) {
        Ref$ObjectRef ref$ObjectRef;
        T a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(t);
            zg4 zg4 = this.p$;
            if (this.$query.length() > 0) {
                Ref$ObjectRef ref$ObjectRef2 = this.$results;
                ug4 a2 = this.this$Anon0.c();
                Anon1 anon1 = new Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = ref$ObjectRef2;
                this.label = 1;
                t = yf4.a(a2, anon1, this);
                if (t == a) {
                    return a;
                }
                ref$ObjectRef = ref$ObjectRef2;
            }
            this.this$Anon0.i().b(this.this$Anon0.a((List<Complication>) (List) this.$results.element));
            this.this$Anon0.j = this.$query;
            return qa4.a;
        } else if (i == 1) {
            ref$ObjectRef = (Ref$ObjectRef) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(t);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ref$ObjectRef.element = (List) t;
        this.this$Anon0.i().b(this.this$Anon0.a((List<Complication>) (List) this.$results.element));
        this.this$Anon0.j = this.$query;
        return qa4.a;
    }
}
