package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.t03;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1 implements i62.d<t03.d, t03.b> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter a;

    @DexIgnore
    public NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter) {
        this.a = notificationContactsAndAppsAssignedPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(t03.d dVar) {
        kd4.b(dVar, "responseValue");
        fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1(this, dVar, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(t03.b bVar) {
        kd4.b(bVar, "errorValue");
        FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.x.a(), "mGetAllHybridContactGroups onError");
        this.a.p.d();
    }
}
