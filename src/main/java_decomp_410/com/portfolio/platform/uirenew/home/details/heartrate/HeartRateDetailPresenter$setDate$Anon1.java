package com.portfolio.platform.uirenew.home.details.heartrate;

import android.util.Pair;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nf3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$Anon1", f = "HeartRateDetailPresenter.kt", l = {143}, m = "invokeSuspend")
public final class HeartRateDetailPresenter$setDate$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateDetailPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$Anon1$Anon1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Date>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;

        @DexIgnore
        public Anon1(yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return PortfolioApp.W.c().k();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$setDate$Anon1(HeartRateDetailPresenter heartRateDetailPresenter, Date date, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = heartRateDetailPresenter;
        this.$date = date;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HeartRateDetailPresenter$setDate$Anon1 heartRateDetailPresenter$setDate$Anon1 = new HeartRateDetailPresenter$setDate$Anon1(this.this$Anon0, this.$date, yb4);
        heartRateDetailPresenter$setDate$Anon1.p$ = (zg4) obj;
        return heartRateDetailPresenter$setDate$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HeartRateDetailPresenter$setDate$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Pair<Date, Date> a;
        kotlin.Pair pair;
        HeartRateDetailPresenter heartRateDetailPresenter;
        Object a2 = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            if (this.this$Anon0.f == null) {
                HeartRateDetailPresenter heartRateDetailPresenter2 = this.this$Anon0;
                ug4 a3 = heartRateDetailPresenter2.b();
                Anon1 anon1 = new Anon1((yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = heartRateDetailPresenter2;
                this.label = 1;
                obj = yf4.a(a3, anon1, this);
                if (obj == a2) {
                    return a2;
                }
                heartRateDetailPresenter = heartRateDetailPresenter2;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$Anon0.f);
            this.this$Anon0.g = this.$date;
            Boolean s = rk2.s(this.$date);
            nf3 m = this.this$Anon0.q;
            Date date = this.$date;
            kd4.a((Object) s, "isToday");
            m.a(date, rk2.c(this.this$Anon0.f, this.$date), s.booleanValue(), !rk2.c(new Date(), this.$date));
            a = rk2.a(this.$date, this.this$Anon0.f);
            kd4.a((Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            pair = (kotlin.Pair) this.this$Anon0.h.a();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("HeartRateDetailPresenter", "setDate - rangeDateValue=" + pair + ", newRange=" + new kotlin.Pair(a.first, a.second));
            if (pair != null || !rk2.d((Date) pair.getFirst(), (Date) a.first) || !rk2.d((Date) pair.getSecond(), (Date) a.second)) {
                this.this$Anon0.h.a(new kotlin.Pair(a.first, a.second));
            } else {
                fi4 unused = this.this$Anon0.l();
                fi4 unused2 = this.this$Anon0.m();
                HeartRateDetailPresenter heartRateDetailPresenter3 = this.this$Anon0;
                heartRateDetailPresenter3.c(heartRateDetailPresenter3.g);
            }
            return qa4.a;
        } else if (i == 1) {
            heartRateDetailPresenter = (HeartRateDetailPresenter) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        heartRateDetailPresenter.f = (Date) obj;
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        local3.d("HeartRateDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$Anon0.f);
        this.this$Anon0.g = this.$date;
        Boolean s2 = rk2.s(this.$date);
        nf3 m2 = this.this$Anon0.q;
        Date date2 = this.$date;
        kd4.a((Object) s2, "isToday");
        m2.a(date2, rk2.c(this.this$Anon0.f, this.$date), s2.booleanValue(), !rk2.c(new Date(), this.$date));
        a = rk2.a(this.$date, this.this$Anon0.f);
        kd4.a((Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
        pair = (kotlin.Pair) this.this$Anon0.h.a();
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        local22.d("HeartRateDetailPresenter", "setDate - rangeDateValue=" + pair + ", newRange=" + new kotlin.Pair(a.first, a.second));
        if (pair != null) {
        }
        this.this$Anon0.h.a(new kotlin.Pair(a.first, a.second));
        return qa4.a;
    }
}
