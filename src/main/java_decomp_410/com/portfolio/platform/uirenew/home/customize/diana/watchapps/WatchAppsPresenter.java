package com.portfolio.platform.uirenew.home.customize.diana.watchapps;

import android.content.Context;
import android.os.Parcelable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.jk2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.ns3;
import com.fossil.blesdk.obfuscated.p43;
import com.fossil.blesdk.obfuscated.pl2;
import com.fossil.blesdk.obfuscated.q43;
import com.fossil.blesdk.obfuscated.r43;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppsPresenter extends p43 {
    @DexIgnore
    public DianaCustomizeViewModel f;
    @DexIgnore
    public /* final */ MutableLiveData<WatchApp> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<WatchApp>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Pair<String, Boolean>>> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Triple<String, Boolean, Parcelable>> k; // = new MutableLiveData<>();
    @DexIgnore
    public ArrayList<Category> l; // = new ArrayList<>();
    @DexIgnore
    public /* final */ LiveData<String> m;
    @DexIgnore
    public /* final */ cc<String> n;
    @DexIgnore
    public /* final */ LiveData<List<WatchApp>> o;
    @DexIgnore
    public /* final */ cc<List<WatchApp>> p;
    @DexIgnore
    public /* final */ LiveData<List<Pair<String, Boolean>>> q;
    @DexIgnore
    public /* final */ cc<List<Pair<String, Boolean>>> r;
    @DexIgnore
    public /* final */ cc<Triple<String, Boolean, Parcelable>> s;
    @DexIgnore
    public /* final */ q43 t;
    @DexIgnore
    public /* final */ CategoryRepository u;
    @DexIgnore
    public /* final */ en2 v;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public b(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "onLiveDataChanged category=" + str);
            if (str != null) {
                this.a.t.e(str);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public c(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(WatchApp watchApp) {
            if (watchApp != null) {
                String str = (String) this.a.h.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppsPresenter", "transform from selected WatchApp to category" + " currentCategory=" + str + " watchAppCategories=" + watchApp.getCategories());
                ArrayList<String> categories = watchApp.getCategories();
                if (str == null || !categories.contains(str)) {
                    this.a.h.a(categories.get(0));
                } else {
                    this.a.h.a(str);
                }
            }
            return this.a.h;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements cc<List<? extends Pair<? extends String, ? extends Boolean>>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public d(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        /* JADX WARNING: Can't fix incorrect switch cases order */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0076, code lost:
            if (r5.equals(com.portfolio.platform.data.InAppPermission.ACCESS_FINE_LOCATION) != false) goto L_0x0097;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0095, code lost:
            if (r5.equals(com.portfolio.platform.data.InAppPermission.LOCATION_SERVICE) != false) goto L_0x0097;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0097, code lost:
            r0 = com.fossil.blesdk.obfuscated.pd4.a;
            r0 = com.fossil.blesdk.obfuscated.sm2.a((android.content.Context) com.portfolio.platform.PortfolioApp.W.c(), (int) com.fossil.wearables.fossil.R.string.___Text__TurnOnLocationServicesToAllow);
            com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) r0, "LanguageHelper.getString\u2026nLocationServicesToAllow)");
            r3 = new java.lang.Object[]{com.portfolio.platform.PortfolioApp.W.c().i()};
            r0 = java.lang.String.format(r0, java.util.Arrays.copyOf(r3, r3.length));
            com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) r0, "java.lang.String.format(format, *args)");
         */
        @DexIgnore
        public final void a(List<Pair<String, Boolean>> list) {
            T t;
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (!((Boolean) ((Pair) t).getSecond()).booleanValue()) {
                        break;
                    }
                }
                Pair pair = (Pair) t;
                FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onLiveDataChanged permissionsRequired " + pair + ' ');
                String str = "";
                int i = 0;
                if (pair != null) {
                    String str2 = (String) pair.getFirst();
                    switch (str2.hashCode()) {
                        case 385352715:
                            break;
                        case 564039755:
                            if (str2.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                                str = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.background_location_service_general_explain);
                                break;
                            }
                            break;
                        case 766697727:
                            break;
                        case 2009556792:
                            if (str2.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                                str = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.watch_app_notification_access_required);
                                break;
                            }
                            break;
                    }
                    q43 j = this.a.t;
                    if (!(list instanceof Collection) || !list.isEmpty()) {
                        for (Pair second : list) {
                            if (((Boolean) second.getSecond()).booleanValue()) {
                                i++;
                                if (i < 0) {
                                    cb4.b();
                                    throw null;
                                }
                            }
                        }
                    }
                    int size = list.size();
                    String a2 = ns3.a.a((String) pair.getFirst());
                    kd4.a((Object) str, "content");
                    j.a(i, size, a2, str);
                    return;
                }
                this.a.t.a(0, 0, str, str);
                WatchApp a3 = WatchAppsPresenter.f(this.a).i().a();
                if (a3 != null) {
                    q43 j2 = this.a.t;
                    String a4 = sm2.a(PortfolioApp.W.c(), a3.getDescriptionKey(), a3.getDescription());
                    kd4.a((Object) a4, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    j2.E(a4);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements cc<Triple<? extends String, ? extends Boolean, ? extends Parcelable>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public e(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        public final void a(Triple<String, Boolean, ? extends Parcelable> triple) {
            String str;
            String str2;
            String str3;
            if (triple != null) {
                FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onLiveDataChanged setting of " + triple.getFirst() + " isSettingRequired " + triple.getSecond().booleanValue() + " setting " + ((Parcelable) triple.getThird()) + ' ');
                String str4 = "";
                if (triple.getSecond().booleanValue()) {
                    Parcelable parcelable = (Parcelable) triple.getThird();
                    if (parcelable == null) {
                        str = str4;
                        str4 = pl2.d.a(triple.getFirst());
                    } else {
                        try {
                            String first = triple.getFirst();
                            int hashCode = first.hashCode();
                            if (hashCode == -829740640) {
                                if (first.equals("commute-time")) {
                                    str2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_CommuteTimeDestinationSettings_Title__SavedDestinations);
                                    kd4.a((Object) str2, "LanguageHelper.getString\u2026Title__SavedDestinations)");
                                }
                                str = str4;
                            } else if (hashCode != 1223440372) {
                                str2 = str4;
                            } else {
                                if (first.equals("weather")) {
                                    List<WeatherLocationWrapper> locations = ((WeatherWatchAppSetting) parcelable).getLocations();
                                    str = str4;
                                    for (WeatherLocationWrapper next : locations) {
                                        try {
                                            StringBuilder sb = new StringBuilder();
                                            sb.append(str);
                                            if (locations.indexOf(next) < cb4.a(locations)) {
                                                str3 = next.getName() + "; ";
                                            } else {
                                                str3 = next.getName();
                                            }
                                            sb.append(str3);
                                            str = sb.toString();
                                        } catch (Exception e) {
                                            e = e;
                                            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "exception when parse micro app setting " + e);
                                            this.a.t.a(true, triple.getFirst(), str4, str);
                                            return;
                                        }
                                    }
                                }
                                str = str4;
                            }
                            str = str2;
                        } catch (Exception e2) {
                            e = e2;
                            str = str4;
                            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "exception when parse micro app setting " + e);
                            this.a.t.a(true, triple.getFirst(), str4, str);
                            return;
                        }
                    }
                    this.a.t.a(true, triple.getFirst(), str4, str);
                    return;
                }
                this.a.t.a(false, triple.getFirst(), str4, (String) null);
                WatchApp a2 = WatchAppsPresenter.f(this.a).i().a();
                if (a2 != null) {
                    q43 j = this.a.t;
                    String a3 = sm2.a(PortfolioApp.W.c(), a2.getDescriptionKey(), a2.getDescription());
                    kd4.a((Object) a3, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    j.E(a3);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements cc<List<? extends WatchApp>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public f(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        public final void a(List<WatchApp> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onLiveDataChanged WatchApps by category value=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("WatchAppsPresenter", sb.toString());
            if (list != null) {
                this.a.t.v(list);
                WatchApp a2 = WatchAppsPresenter.f(this.a).i().a();
                if (a2 != null) {
                    this.a.t.b(a2);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public g(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<WatchApp>> apply(String str) {
            T t;
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "transform from category to list watchapps with category=" + str);
            DianaCustomizeViewModel f = WatchAppsPresenter.f(this.a);
            kd4.a((Object) str, "category");
            List<WatchApp> d = f.d(str);
            ArrayList arrayList = new ArrayList();
            DianaPreset a2 = WatchAppsPresenter.f(this.a).c().a();
            WatchApp a3 = WatchAppsPresenter.f(this.a).i().a();
            if (a3 != null) {
                String watchappId = a3.getWatchappId();
                if (a2 != null) {
                    for (WatchApp next : d) {
                        Iterator<T> it = a2.getWatchapps().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it.next();
                            DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t;
                            boolean z = true;
                            if (!kd4.a((Object) dianaPresetWatchAppSetting.getId(), (Object) next.getWatchappId()) || !(!kd4.a((Object) dianaPresetWatchAppSetting.getId(), (Object) watchappId))) {
                                z = false;
                                continue;
                            }
                            if (z) {
                                break;
                            }
                        }
                        if (((DianaPresetWatchAppSetting) t) == null || kd4.a((Object) next.getWatchappId(), (Object) "empty")) {
                            arrayList.add(next);
                        }
                    }
                }
                this.a.i.a(arrayList);
                return this.a.i;
            }
            kd4.a();
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements cc<WatchApp> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public h(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        public final void a(WatchApp watchApp) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "onLiveDataChanged selectedWatchApp value=" + watchApp);
            if (watchApp != null) {
                this.a.g.a(watchApp);
                this.a.t.t(pl2.d.e(watchApp.getWatchappId()));
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public WatchAppsPresenter(q43 q43, CategoryRepository categoryRepository, en2 en2) {
        kd4.b(q43, "mView");
        kd4.b(categoryRepository, "mCategoryRepository");
        kd4.b(en2, "mSharedPreferencesManager");
        this.t = q43;
        this.u = categoryRepository;
        this.v = en2;
        new Gson();
        LiveData<String> b2 = hc.b(this.g, new c(this));
        kd4.a((Object) b2, "Transformations.switchMa\u2026tedWatchAppLiveData\n    }");
        this.m = b2;
        this.n = new b(this);
        LiveData<List<WatchApp>> b3 = hc.b(this.m, new g(this));
        kd4.a((Object) b3, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.o = b3;
        this.p = new f(this);
        LiveData<List<Pair<String, Boolean>>> b4 = hc.b(this.g, new WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1(this));
        kd4.a((Object) b4, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.q = b4;
        this.r = new d(this);
        this.s = new e(this);
    }

    @DexIgnore
    public static final /* synthetic */ DianaCustomizeViewModel f(WatchAppsPresenter watchAppsPresenter) {
        DianaCustomizeViewModel dianaCustomizeViewModel = watchAppsPresenter.f;
        if (dianaCustomizeViewModel != null) {
            return dianaCustomizeViewModel;
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final fi4 b(String str) {
        return ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1(this, str, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onStart");
        if (this.l.isEmpty()) {
            fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new WatchAppsPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
        } else {
            this.t.a(this.l);
        }
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            LiveData<WatchApp> i2 = dianaCustomizeViewModel.i();
            q43 q43 = this.t;
            if (q43 != null) {
                i2.a((r43) q43, new h(this));
                this.m.a(this.n);
                this.o.a(this.p);
                this.q.a(this.r);
                this.k.a(this.s);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsFragment");
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void g() {
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            LiveData<WatchApp> i2 = dianaCustomizeViewModel.i();
            q43 q43 = this.t;
            if (q43 != null) {
                i2.a((LifecycleOwner) (r43) q43);
                this.i.b(this.p);
                this.h.b(this.n);
                this.j.b(this.r);
                this.k.b(this.s);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsFragment");
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003e, code lost:
        if (r3 != null) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006d, code lost:
        if (r4 != null) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x009b, code lost:
        if (r0 != null) goto L_0x009f;
     */
    @DexIgnore
    public void h() {
        T t2;
        String str;
        T t3;
        String str2;
        String str3;
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        T t4 = null;
        if (dianaCustomizeViewModel != null) {
            DianaPreset a2 = dianaCustomizeViewModel.c().a();
            if (a2 != null) {
                Iterator<T> it = a2.getWatchapps().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (kd4.a((Object) ((DianaPresetWatchAppSetting) t2).getPosition(), (Object) ViewHierarchy.DIMENSION_TOP_KEY)) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t2;
                if (dianaPresetWatchAppSetting != null) {
                    str = dianaPresetWatchAppSetting.getId();
                }
                str = "empty";
                Iterator<T> it2 = a2.getWatchapps().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t3 = null;
                        break;
                    }
                    t3 = it2.next();
                    if (kd4.a((Object) ((DianaPresetWatchAppSetting) t3).getPosition(), (Object) "middle")) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) t3;
                if (dianaPresetWatchAppSetting2 != null) {
                    str2 = dianaPresetWatchAppSetting2.getId();
                }
                str2 = "empty";
                Iterator<T> it3 = a2.getWatchapps().iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    }
                    T next = it3.next();
                    if (kd4.a((Object) ((DianaPresetWatchAppSetting) next).getPosition(), (Object) "bottom")) {
                        t4 = next;
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting3 = (DianaPresetWatchAppSetting) t4;
                if (dianaPresetWatchAppSetting3 != null) {
                    str3 = dianaPresetWatchAppSetting3.getId();
                }
                str3 = "empty";
                this.t.a(str, str2, str3);
                return;
            }
            this.t.a("empty", "empty", "empty");
            return;
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0051, code lost:
        if (r1 != null) goto L_0x0056;
     */
    @DexIgnore
    public void i() {
        String str;
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        T t2 = null;
        if (dianaCustomizeViewModel != null) {
            WatchApp a2 = dianaCustomizeViewModel.i().a();
            if (a2 != null) {
                String component1 = a2.component1();
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
                if (dianaCustomizeViewModel2 != null) {
                    DianaPreset a3 = dianaCustomizeViewModel2.c().a();
                    if (a3 != null) {
                        ArrayList<DianaPresetWatchAppSetting> watchapps = a3.getWatchapps();
                        if (watchapps != null) {
                            Iterator<T> it = watchapps.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                T next = it.next();
                                if (kd4.a((Object) component1, (Object) ((DianaPresetWatchAppSetting) next).getId())) {
                                    t2 = next;
                                    break;
                                }
                            }
                            DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t2;
                            if (dianaPresetWatchAppSetting != null) {
                                str = dianaPresetWatchAppSetting.getSettings();
                            }
                        }
                    }
                    str = "";
                    String watchappId = a2.getWatchappId();
                    int hashCode = watchappId.hashCode();
                    if (hashCode != -829740640) {
                        if (hashCode == 1223440372 && watchappId.equals("weather")) {
                            this.t.M(str);
                        }
                    } else if (watchappId.equals("commute-time")) {
                        this.t.b(str);
                    }
                } else {
                    kd4.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void j() {
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            WatchApp a2 = dianaCustomizeViewModel.i().a();
            if (a2 != null) {
                String component1 = a2.component1();
                if (pl2.d.e(component1)) {
                    this.t.c(component1);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0095  */
    public void k() {
        Object obj;
        boolean a2;
        List a3 = this.j.a();
        if (a3 != null) {
            Iterator it = a3.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (!((Boolean) ((Pair) obj).getSecond()).booleanValue()) {
                    break;
                }
            }
            Pair pair = (Pair) obj;
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "processRequiredPermission " + pair);
            if (pair != null) {
                String str = (String) pair.component1();
                boolean z = false;
                int hashCode = str.hashCode();
                if (hashCode != 564039755) {
                    if (hashCode == 2009556792 && str.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                        a2 = ns3.a.e();
                    }
                    if (z) {
                        this.t.A((String) pair.getFirst());
                        return;
                    } else {
                        this.t.f((String) pair.getFirst());
                        return;
                    }
                } else {
                    if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                        a2 = ns3.a.a((Context) PortfolioApp.W.c());
                    }
                    if (z) {
                    }
                }
                z = !a2;
                if (z) {
                }
            }
        }
    }

    @DexIgnore
    public void l() {
        this.t.a(this);
    }

    @DexIgnore
    public void a(DianaCustomizeViewModel dianaCustomizeViewModel) {
        kd4.b(dianaCustomizeViewModel, "viewModel");
        this.f = dianaCustomizeViewModel;
    }

    @DexIgnore
    public void a(Category category) {
        kd4.b(category, "category");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsPresenter", "category change " + category);
        this.h.a(category.getId());
    }

    @DexIgnore
    public void a(String str) {
        T t2;
        kd4.b(str, "watchappId");
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            WatchApp e2 = dianaCustomizeViewModel.e(str);
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onUserChooseWatchApp " + e2);
            if (e2 != null) {
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
                if (dianaCustomizeViewModel2 != null) {
                    DianaPreset a2 = dianaCustomizeViewModel2.c().a();
                    if (a2 != null) {
                        DianaPreset clone = a2.clone();
                        ArrayList arrayList = new ArrayList();
                        DianaCustomizeViewModel dianaCustomizeViewModel3 = this.f;
                        if (dianaCustomizeViewModel3 != null) {
                            String a3 = dianaCustomizeViewModel3.j().a();
                            if (a3 != null) {
                                kd4.a((Object) a3, "mDianaCustomizeViewModel\u2026ctedWatchAppPos().value!!");
                                String str2 = a3;
                                DianaCustomizeViewModel dianaCustomizeViewModel4 = this.f;
                                if (dianaCustomizeViewModel4 != null) {
                                    if (!dianaCustomizeViewModel4.j(str)) {
                                        Iterator<DianaPresetWatchAppSetting> it = clone.getWatchapps().iterator();
                                        while (it.hasNext()) {
                                            DianaPresetWatchAppSetting next = it.next();
                                            if (kd4.a((Object) next.getPosition(), (Object) str2)) {
                                                arrayList.add(new DianaPresetWatchAppSetting(str2, str, next.getLocalUpdateAt()));
                                            } else {
                                                arrayList.add(next);
                                            }
                                        }
                                        clone.getWatchapps().clear();
                                        clone.getWatchapps().addAll(arrayList);
                                        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Update current preset=" + clone);
                                        DianaCustomizeViewModel dianaCustomizeViewModel5 = this.f;
                                        if (dianaCustomizeViewModel5 != null) {
                                            dianaCustomizeViewModel5.a(clone);
                                        } else {
                                            kd4.d("mDianaCustomizeViewModel");
                                            throw null;
                                        }
                                    } else {
                                        Iterator<T> it2 = a2.getWatchapps().iterator();
                                        while (true) {
                                            if (!it2.hasNext()) {
                                                t2 = null;
                                                break;
                                            }
                                            t2 = it2.next();
                                            if (kd4.a((Object) ((DianaPresetWatchAppSetting) t2).getId(), (Object) str)) {
                                                break;
                                            }
                                        }
                                        DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t2;
                                        if (dianaPresetWatchAppSetting != null) {
                                            DianaCustomizeViewModel dianaCustomizeViewModel6 = this.f;
                                            if (dianaCustomizeViewModel6 != null) {
                                                dianaCustomizeViewModel6.l(dianaPresetWatchAppSetting.getPosition());
                                            } else {
                                                kd4.d("mDianaCustomizeViewModel");
                                                throw null;
                                            }
                                        }
                                    }
                                    String watchappId = e2.getWatchappId();
                                    if (watchappId.hashCode() == -829740640 && watchappId.equals("commute-time") && !this.v.h()) {
                                        this.v.o(true);
                                        this.t.c("commute-time");
                                        return;
                                    }
                                    return;
                                }
                                kd4.d("mDianaCustomizeViewModel");
                                throw null;
                            }
                            kd4.a();
                            throw null;
                        }
                        kd4.d("mDianaCustomizeViewModel");
                        throw null;
                    }
                    return;
                }
                kd4.d("mDianaCustomizeViewModel");
                throw null;
            }
            return;
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void a(String str, Parcelable parcelable) {
        T t2;
        kd4.b(str, "watchAppId");
        kd4.b(parcelable, MicroAppSetting.SETTING);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            DianaPreset a2 = dianaCustomizeViewModel.c().a();
            if (a2 != null) {
                DianaPreset clone = a2.clone();
                Iterator<T> it = clone.getWatchapps().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (kd4.a((Object) ((DianaPresetWatchAppSetting) t2).getId(), (Object) str)) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t2;
                rz1 rz1 = new rz1();
                rz1.b(new jk2());
                Gson a3 = rz1.a();
                if (dianaPresetWatchAppSetting != null) {
                    dianaPresetWatchAppSetting.setSettings(a3.a((Object) parcelable));
                }
                FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "update current preset with new setting " + parcelable + " of " + str);
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
                if (dianaCustomizeViewModel2 != null) {
                    dianaCustomizeViewModel2.a(clone);
                } else {
                    kd4.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }
}
