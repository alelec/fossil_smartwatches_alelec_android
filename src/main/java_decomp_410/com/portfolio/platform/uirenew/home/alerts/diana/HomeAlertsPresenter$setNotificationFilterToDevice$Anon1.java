package com.portfolio.platform.uirenew.home.alerts.diana;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$Anon1", f = "HomeAlertsPresenter.kt", l = {432, 434}, m = "invokeSuspend")
public final class HomeAlertsPresenter$setNotificationFilterToDevice$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public long J$Anon0;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeAlertsPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeAlertsPresenter$setNotificationFilterToDevice$Anon1(HomeAlertsPresenter homeAlertsPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = homeAlertsPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeAlertsPresenter$setNotificationFilterToDevice$Anon1 homeAlertsPresenter$setNotificationFilterToDevice$Anon1 = new HomeAlertsPresenter$setNotificationFilterToDevice$Anon1(this.this$Anon0, yb4);
        homeAlertsPresenter$setNotificationFilterToDevice$Anon1.p$ = (zg4) obj;
        return homeAlertsPresenter$setNotificationFilterToDevice$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeAlertsPresenter$setNotificationFilterToDevice$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0106  */
    public final Object invokeSuspend(Object obj) {
        long j;
        List list;
        Object obj2;
        List list2;
        gh4 gh4;
        zg4 zg4;
        gh4 gh42;
        List list3;
        Object obj3;
        List list4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg42 = this.p$;
            list3 = new ArrayList();
            j = System.currentTimeMillis();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsPresenter.x.a();
            local.d(a2, "filter notification, time start=" + j);
            list3.clear();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = HomeAlertsPresenter.x.a();
            local2.d(a3, "mListAppWrapper.size = " + this.this$Anon0.i().size());
            gh4 a4 = ag4.a(zg42, this.this$Anon0.b(), (CoroutineStart) null, new HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$otherAppDeffer$Anon1(this, (yb4) null), 2, (Object) null);
            gh42 = ag4.a(zg42, this.this$Anon0.b(), (CoroutineStart) null, new HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1(this, (yb4) null), 2, (Object) null);
            this.L$Anon0 = zg42;
            this.L$Anon1 = list3;
            this.J$Anon0 = j;
            this.L$Anon2 = a4;
            this.L$Anon3 = gh42;
            this.L$Anon4 = list3;
            this.label = 1;
            obj3 = a4.a(this);
            if (obj3 == a) {
                return a;
            }
            gh4 = a4;
            zg4 = zg42;
            list4 = list3;
        } else if (i == 1) {
            list4 = (List) this.L$Anon4;
            j = this.J$Anon0;
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            gh4 = (gh4) this.L$Anon2;
            list3 = (List) this.L$Anon1;
            gh42 = (gh4) this.L$Anon3;
            obj3 = obj;
        } else if (i == 2) {
            gh4 gh43 = (gh4) this.L$Anon3;
            gh4 gh44 = (gh4) this.L$Anon2;
            long j2 = this.J$Anon0;
            list = (List) this.L$Anon1;
            zg4 zg43 = (zg4) this.L$Anon0;
            na4.a(obj);
            j = j2;
            obj2 = obj;
            list2 = (List) obj2;
            if (list2 != null) {
                list.addAll(list2);
                long b = PortfolioApp.W.c().b(new AppNotificationFilterSettings(list, System.currentTimeMillis()), PortfolioApp.W.c().e());
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a5 = HomeAlertsPresenter.x.a();
                local3.d(a5, "filter notification, time end= " + b);
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String a6 = HomeAlertsPresenter.x.a();
                local4.d(a6, "delayTime =" + (b - j) + " milliseconds");
            }
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        list4.addAll((Collection) obj3);
        this.L$Anon0 = zg4;
        this.L$Anon1 = list3;
        this.J$Anon0 = j;
        this.L$Anon2 = gh4;
        this.L$Anon3 = gh42;
        this.label = 2;
        obj2 = gh42.a(this);
        if (obj2 == a) {
            return a;
        }
        list = list3;
        list2 = (List) obj2;
        if (list2 != null) {
        }
        return qa4.a;
    }
}
