package com.portfolio.platform.uirenew.home.details.goaltracking;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dl4;
import com.fossil.blesdk.obfuscated.ef3;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import java.util.Date;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$Anon1", f = "GoalTrackingDetailPresenter.kt", l = {144, 262, 169}, m = "invokeSuspend")
public final class GoalTrackingDetailPresenter$setDate$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDetailPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$Anon1$Anon1", f = "GoalTrackingDetailPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Date>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;

        @DexIgnore
        public Anon1(yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return PortfolioApp.W.c().k();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDetailPresenter$setDate$Anon1(GoalTrackingDetailPresenter goalTrackingDetailPresenter, Date date, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = goalTrackingDetailPresenter;
        this.$date = date;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        GoalTrackingDetailPresenter$setDate$Anon1 goalTrackingDetailPresenter$setDate$Anon1 = new GoalTrackingDetailPresenter$setDate$Anon1(this.this$Anon0, this.$date, yb4);
        goalTrackingDetailPresenter$setDate$Anon1.p$ = (zg4) obj;
        return goalTrackingDetailPresenter$setDate$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingDetailPresenter$setDate$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v24, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v30, resolved type: com.fossil.blesdk.obfuscated.dl4} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x01a0 A[Catch:{ all -> 0x01eb }, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01b1 A[Catch:{ all -> 0x01eb }] */
    public final Object invokeSuspend(Object obj) {
        dl4 dl4;
        Object obj2;
        GoalTrackingSummary goalTrackingSummary;
        zg4 zg4;
        boolean z;
        Boolean bool;
        Pair pair;
        android.util.Pair<Date, Date> pair2;
        zg4 zg42;
        Pair pair3;
        Object obj3;
        GoalTrackingDetailPresenter goalTrackingDetailPresenter;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg42 = this.p$;
            if (this.this$Anon0.f == null) {
                goalTrackingDetailPresenter = this.this$Anon0;
                ug4 a2 = goalTrackingDetailPresenter.b();
                Anon1 anon1 = new Anon1((yb4) null);
                this.L$Anon0 = zg42;
                this.L$Anon1 = goalTrackingDetailPresenter;
                this.label = 1;
                obj3 = yf4.a(a2, anon1, this);
                if (obj3 == a) {
                    return a;
                }
            }
            zg4 = zg42;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$Anon0.f);
            this.this$Anon0.g = this.$date;
            z = rk2.c(this.this$Anon0.f, this.$date);
            bool = rk2.s(this.$date);
            ef3 m = this.this$Anon0.q;
            Date date = this.$date;
            kd4.a((Object) bool, "isToday");
            m.a(date, z, bool.booleanValue(), !rk2.c(new Date(), this.$date));
            pair2 = rk2.a(this.$date, this.this$Anon0.f);
            kd4.a((Object) pair2, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            pair3 = (Pair) this.this$Anon0.h.a();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("GoalTrackingDetailPresenter", "setDate - rangeDateValue=" + pair3 + ", newRange=" + new Pair(pair2.first, pair2.second));
            if (pair3 != null || !rk2.d((Date) pair3.getFirst(), (Date) pair2.first) || !rk2.d((Date) pair3.getSecond(), (Date) pair2.second)) {
                this.this$Anon0.i = false;
                this.this$Anon0.j = false;
                GoalTrackingDetailPresenter goalTrackingDetailPresenter2 = this.this$Anon0;
                goalTrackingDetailPresenter2.d(goalTrackingDetailPresenter2.g);
                this.this$Anon0.h.a(new Pair(pair2.first, pair2.second));
                return qa4.a;
            }
            dl4 = this.this$Anon0.k;
            this.L$Anon0 = zg4;
            this.Z$Anon0 = z;
            this.L$Anon1 = bool;
            this.L$Anon2 = pair2;
            this.L$Anon3 = pair3;
            this.L$Anon4 = dl4;
            this.label = 2;
            if (dl4.a((Object) null, this) == a) {
                return a;
            }
            pair = pair3;
            ug4 a3 = this.this$Anon0.b();
            GoalTrackingDetailPresenter$setDate$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 goalTrackingDetailPresenter$setDate$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 = new GoalTrackingDetailPresenter$setDate$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1((yb4) null, this);
            this.L$Anon0 = zg4;
            this.Z$Anon0 = z;
            this.L$Anon1 = bool;
            this.L$Anon2 = pair2;
            this.L$Anon3 = pair;
            this.L$Anon4 = dl4;
            this.label = 3;
            obj2 = yf4.a(a3, goalTrackingDetailPresenter$setDate$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1, this);
            if (obj2 == a) {
            }
            goalTrackingSummary = (GoalTrackingSummary) obj2;
            if (!kd4.a((Object) this.this$Anon0.m, (Object) goalTrackingSummary)) {
            }
            this.this$Anon0.q.a(this.this$Anon0.m);
            this.this$Anon0.d(this.this$Anon0.g);
            fi4 unused = this.this$Anon0.m();
            qa4 qa4 = qa4.a;
            dl4.a((Object) null);
            return qa4.a;
        } else if (i == 1) {
            goalTrackingDetailPresenter = (GoalTrackingDetailPresenter) this.L$Anon1;
            zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            obj3 = obj;
        } else if (i == 2) {
            pair = (Pair) this.L$Anon3;
            z = this.Z$Anon0;
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            dl4 = (dl4) this.L$Anon4;
            pair2 = (android.util.Pair) this.L$Anon2;
            bool = (Boolean) this.L$Anon1;
            try {
                ug4 a32 = this.this$Anon0.b();
                GoalTrackingDetailPresenter$setDate$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 goalTrackingDetailPresenter$setDate$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon12 = new GoalTrackingDetailPresenter$setDate$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1((yb4) null, this);
                this.L$Anon0 = zg4;
                this.Z$Anon0 = z;
                this.L$Anon1 = bool;
                this.L$Anon2 = pair2;
                this.L$Anon3 = pair;
                this.L$Anon4 = dl4;
                this.label = 3;
                obj2 = yf4.a(a32, goalTrackingDetailPresenter$setDate$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon12, this);
                if (obj2 == a) {
                    return a;
                }
                goalTrackingSummary = (GoalTrackingSummary) obj2;
                if (!kd4.a((Object) this.this$Anon0.m, (Object) goalTrackingSummary)) {
                }
                this.this$Anon0.q.a(this.this$Anon0.m);
                this.this$Anon0.d(this.this$Anon0.g);
                fi4 unused2 = this.this$Anon0.m();
                qa4 qa42 = qa4.a;
                dl4.a((Object) null);
                return qa4.a;
            } catch (Throwable th) {
                th = th;
                dl4.a((Object) null);
                throw th;
            }
        } else if (i == 3) {
            dl4 dl42 = this.L$Anon4;
            Pair pair4 = (Pair) this.L$Anon3;
            android.util.Pair pair5 = (android.util.Pair) this.L$Anon2;
            Boolean bool2 = (Boolean) this.L$Anon1;
            zg4 zg43 = (zg4) this.L$Anon0;
            try {
                na4.a(obj);
                dl4 = dl42;
                obj2 = obj;
                goalTrackingSummary = (GoalTrackingSummary) obj2;
                if (!kd4.a((Object) this.this$Anon0.m, (Object) goalTrackingSummary)) {
                    this.this$Anon0.m = goalTrackingSummary;
                }
                this.this$Anon0.q.a(this.this$Anon0.m);
                this.this$Anon0.d(this.this$Anon0.g);
                if (this.this$Anon0.i && this.this$Anon0.j) {
                    fi4 unused3 = this.this$Anon0.m();
                }
                qa4 qa422 = qa4.a;
                dl4.a((Object) null);
                return qa4.a;
            } catch (Throwable th2) {
                th = th2;
                dl4 = dl42;
                dl4.a((Object) null);
                throw th;
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        goalTrackingDetailPresenter.f = (Date) obj3;
        zg4 = zg42;
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        local3.d("GoalTrackingDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$Anon0.f);
        this.this$Anon0.g = this.$date;
        z = rk2.c(this.this$Anon0.f, this.$date);
        bool = rk2.s(this.$date);
        ef3 m2 = this.this$Anon0.q;
        Date date2 = this.$date;
        kd4.a((Object) bool, "isToday");
        m2.a(date2, z, bool.booleanValue(), !rk2.c(new Date(), this.$date));
        pair2 = rk2.a(this.$date, this.this$Anon0.f);
        kd4.a((Object) pair2, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
        pair3 = (Pair) this.this$Anon0.h.a();
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        local22.d("GoalTrackingDetailPresenter", "setDate - rangeDateValue=" + pair3 + ", newRange=" + new Pair(pair2.first, pair2.second));
        if (pair3 != null) {
        }
        this.this$Anon0.i = false;
        this.this$Anon0.j = false;
        GoalTrackingDetailPresenter goalTrackingDetailPresenter22 = this.this$Anon0;
        goalTrackingDetailPresenter22.d(goalTrackingDetailPresenter22.g);
        this.this$Anon0.h.a(new Pair(pair2.first, pair2.second));
        return qa4.a;
    }
}
