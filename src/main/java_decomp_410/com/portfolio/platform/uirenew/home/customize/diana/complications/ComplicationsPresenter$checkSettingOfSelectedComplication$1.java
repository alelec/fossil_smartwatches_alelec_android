package com.portfolio.platform.uirenew.home.customize.diana.complications;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1", mo27670f = "ComplicationsPresenter.kt", mo27671l = {260}, mo27672m = "invokeSuspend")
public final class ComplicationsPresenter$checkSettingOfSelectedComplication$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $complicationId;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22731p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1$1", mo27670f = "ComplicationsPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1$1 */
    public static final class C64401 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super android.os.Parcelable>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22732p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64401(com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1 complicationsPresenter$checkSettingOfSelectedComplication$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = complicationsPresenter$checkSettingOfSelectedComplication$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1.C64401 r0 = new com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1.C64401(this.this$0, yb4);
            r0.f22732p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1.C64401) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter.m33908g(this.this$0.this$0).mo40972g(this.this$0.$complicationId);
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ComplicationsPresenter$checkSettingOfSelectedComplication$1(com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter complicationsPresenter, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = complicationsPresenter;
        this.$complicationId = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1 complicationsPresenter$checkSettingOfSelectedComplication$1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1(this.this$0, this.$complicationId, yb4);
        complicationsPresenter$checkSettingOfSelectedComplication$1.f22731p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return complicationsPresenter$checkSettingOfSelectedComplication$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0091  */
    public final java.lang.Object invokeSuspend(T t) {
        boolean z;
        kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef;
        T t2;
        kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2;
        T a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(t);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22731p$;
            ref$ObjectRef = new kotlin.jvm.internal.Ref$ObjectRef();
            ref$ObjectRef.element = null;
            z = com.fossil.blesdk.obfuscated.ok2.f17359c.mo29850d(this.$complicationId);
            if (z) {
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1.C64401 r6 = new com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1.C64401(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = ref$ObjectRef;
                this.Z$0 = z;
                this.L$2 = ref$ObjectRef;
                this.label = 1;
                t = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r6, this);
                if (t == a) {
                    return a;
                }
                ref$ObjectRef2 = ref$ObjectRef;
            }
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter.f22706w, "checkSettingOfSelectedComplication complicationId=" + this.$complicationId + " settings=" + ((android.os.Parcelable) ref$ObjectRef.element));
            if (((android.os.Parcelable) ref$ObjectRef.element) != null) {
                com.portfolio.platform.data.model.diana.preset.DianaPreset a3 = com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter.m33908g(this.this$0).mo40964c().mo2275a();
                if (a3 != null) {
                    com.portfolio.platform.data.model.diana.preset.DianaPreset clone = a3.clone();
                    java.util.Iterator<T> it = clone.getComplications().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t2 = null;
                            break;
                        }
                        t2 = it.next();
                        if (com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) t2).getId(), (java.lang.Object) this.$complicationId)).booleanValue()) {
                            break;
                        }
                    }
                    com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting dianaPresetComplicationSetting = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) t2;
                    if (dianaPresetComplicationSetting != null) {
                        dianaPresetComplicationSetting.setSettings(this.this$0.f22713l.mo23096a((java.lang.Object) (android.os.Parcelable) ref$ObjectRef.element));
                        com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter.m33908g(this.this$0).mo40962a(clone);
                    }
                }
            }
            this.this$0.f22712k.mo2280a(new kotlin.Triple(this.$complicationId, com.fossil.blesdk.obfuscated.dc4.m20839a(z), (android.os.Parcelable) ref$ObjectRef.element));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            ref$ObjectRef2 = (kotlin.jvm.internal.Ref$ObjectRef) this.L$2;
            boolean z2 = this.Z$0;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(t);
            z = z2;
            ref$ObjectRef = (kotlin.jvm.internal.Ref$ObjectRef) this.L$1;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ref$ObjectRef2.element = (android.os.Parcelable) t;
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter.f22706w, "checkSettingOfSelectedComplication complicationId=" + this.$complicationId + " settings=" + ((android.os.Parcelable) ref$ObjectRef.element));
        if (((android.os.Parcelable) ref$ObjectRef.element) != null) {
        }
        this.this$0.f22712k.mo2280a(new kotlin.Triple(this.$complicationId, com.fossil.blesdk.obfuscated.dc4.m20839a(z), (android.os.Parcelable) ref$ObjectRef.element));
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
