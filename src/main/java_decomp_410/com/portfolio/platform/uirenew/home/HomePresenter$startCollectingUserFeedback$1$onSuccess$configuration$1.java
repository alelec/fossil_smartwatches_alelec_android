package com.portfolio.platform.uirenew.home;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomePresenter$startCollectingUserFeedback$1$onSuccess$configuration$1 extends com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.kr2.d $responseValue;

    @DexIgnore
    public HomePresenter$startCollectingUserFeedback$1$onSuccess$configuration$1(com.fossil.blesdk.obfuscated.kr2.d dVar) {
        this.$responseValue = dVar;
    }

    @DexIgnore
    public java.lang.String getAdditionalInfo() {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a = com.portfolio.platform.uirenew.home.HomePresenter.y.a();
        local.d(a, "Inside. getAdditionalInfo: \n" + this.$responseValue.a());
        return this.$responseValue.a();
    }

    @DexIgnore
    public java.lang.String getRequestSubject() {
        return this.$responseValue.d();
    }

    @DexIgnore
    public java.util.List<java.lang.String> getTags() {
        return this.$responseValue.e();
    }
}
