package com.portfolio.platform.uirenew.home.profile.help;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HelpPresenter$sendFeedback$1$onSuccess$configuration$1 extends com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.kr2.C4595d $responseValue;

    @DexIgnore
    public HelpPresenter$sendFeedback$1$onSuccess$configuration$1(com.fossil.blesdk.obfuscated.kr2.C4595d dVar) {
        this.$responseValue = dVar;
    }

    @DexIgnore
    public java.lang.String getAdditionalInfo() {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String k = com.portfolio.platform.uirenew.home.profile.help.HelpPresenter.f23882j;
        local.mo33255d(k, "Inside. getAdditionalInfo: \n" + this.$responseValue.mo28906a());
        return this.$responseValue.mo28906a();
    }

    @DexIgnore
    public java.lang.String getRequestSubject() {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.profile.help.HelpPresenter.f23882j, "getRequestSubject");
        return this.$responseValue.mo28909d();
    }

    @DexIgnore
    public java.util.List<java.lang.String> getTags() {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.profile.help.HelpPresenter.f23882j, "getTags");
        return this.$responseValue.mo28910e();
    }
}
