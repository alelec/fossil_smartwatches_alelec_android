package com.portfolio.platform.uirenew.home.details.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1$$special$$inlined$let$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.qd $dataList;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1$$special$$inlined$let$lambda$1(com.fossil.blesdk.obfuscated.qd qdVar, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1 goalTrackingDetailPresenter$observeGoalTrackingDataPaging$1) {
        super(2, yb4);
        this.$dataList = qdVar;
        this.this$0 = goalTrackingDetailPresenter$observeGoalTrackingDataPaging$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1$$special$$inlined$let$lambda$1 goalTrackingDetailPresenter$observeGoalTrackingDataPaging$1$$special$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1$$special$$inlined$let$lambda$1(this.$dataList, yb4, this.this$0);
        goalTrackingDetailPresenter$observeGoalTrackingDataPaging$1$$special$$inlined$let$lambda$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return goalTrackingDetailPresenter$observeGoalTrackingDataPaging$1$$special$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1$$special$$inlined$let$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.dl4 dl4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.p$;
            com.fossil.blesdk.obfuscated.dl4 h = this.this$0.a.k;
            this.L$0 = zg4;
            this.L$1 = h;
            this.label = 1;
            if (h.a((java.lang.Object) null, this) == a) {
                return a;
            }
            dl4 = h;
        } else if (i == 1) {
            dl4 = (com.fossil.blesdk.obfuscated.dl4) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            if (this.this$0.a.n == null || (!com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) this.this$0.a.n, (java.lang.Object) com.fossil.blesdk.obfuscated.kb4.d(this.$dataList)))) {
                this.this$0.a.n = com.fossil.blesdk.obfuscated.kb4.d(this.$dataList);
            }
            this.this$0.a.q.c(this.$dataList);
            if (this.this$0.a.i && this.this$0.a.j) {
                com.fossil.blesdk.obfuscated.fi4 unused = this.this$0.a.m();
            }
            com.fossil.blesdk.obfuscated.qa4 qa4 = com.fossil.blesdk.obfuscated.qa4.a;
            dl4.a((java.lang.Object) null);
            return com.fossil.blesdk.obfuscated.qa4.a;
        } catch (Throwable th) {
            dl4.a((java.lang.Object) null);
            throw th;
        }
    }
}
