package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherSettingPresenter$addLocation$1<TResult> implements com.fossil.blesdk.obfuscated.tn1<com.google.android.libraries.places.api.net.FetchPlaceResponse> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter f22899a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ /* synthetic */ java.lang.String f22900b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ /* synthetic */ java.lang.String f22901c;

    @DexIgnore
    public WeatherSettingPresenter$addLocation$1(com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter weatherSettingPresenter, java.lang.String str, java.lang.String str2) {
        this.f22899a = weatherSettingPresenter;
        this.f22900b = str;
        this.f22901c = str2;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onSuccess(com.google.android.libraries.places.api.net.FetchPlaceResponse fetchPlaceResponse) {
        this.f22899a.f22894i.mo28764a();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) fetchPlaceResponse, "response");
        com.google.android.libraries.places.api.model.Place place = fetchPlaceResponse.getPlace();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) place, "response.place");
        com.google.android.gms.maps.model.LatLng latLng = place.getLatLng();
        if (latLng != null) {
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25691a()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6499x616cc6a4(latLng, (com.fossil.blesdk.obfuscated.yb4) null, this), 3, (java.lang.Object) null);
        }
    }
}
