package com.portfolio.platform.uirenew.home.dashboard;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$Anon1", f = "HomeDashboardPresenter.kt", l = {136}, m = "invokeSuspend")
public final class HomeDashboardPresenter$showLayoutBattery$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $buildMode;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDashboardPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDashboardPresenter$showLayoutBattery$Anon1(HomeDashboardPresenter homeDashboardPresenter, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = homeDashboardPresenter;
        this.$buildMode = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeDashboardPresenter$showLayoutBattery$Anon1 homeDashboardPresenter$showLayoutBattery$Anon1 = new HomeDashboardPresenter$showLayoutBattery$Anon1(this.this$Anon0, this.$buildMode, yb4);
        homeDashboardPresenter$showLayoutBattery$Anon1.p$ = (zg4) obj;
        return homeDashboardPresenter$showLayoutBattery$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDashboardPresenter$showLayoutBattery$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 a2 = this.this$Anon0.c();
            HomeDashboardPresenter$showLayoutBattery$Anon1$activeDevice$Anon1 homeDashboardPresenter$showLayoutBattery$Anon1$activeDevice$Anon1 = new HomeDashboardPresenter$showLayoutBattery$Anon1$activeDevice$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a2, homeDashboardPresenter$showLayoutBattery$Anon1$activeDevice$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Device device = (Device) obj;
        if (device != null) {
            if (kd4.a((Object) this.$buildMode, (Object) "debug") || kd4.a((Object) this.$buildMode, (Object) "staging")) {
                if (FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId()) || device.getBatteryLevel() >= PortfolioApp.W.c().u().w() || device.getBatteryLevel() <= 0) {
                    this.this$Anon0.x.o(false);
                } else {
                    this.this$Anon0.x.o(true);
                }
            } else if (FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId()) || device.getBatteryLevel() >= 10 || device.getBatteryLevel() <= 0) {
                this.this$Anon0.x.o(false);
            } else {
                this.this$Anon0.x.o(true);
            }
        }
        return qa4.a;
    }
}
