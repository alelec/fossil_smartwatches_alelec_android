package com.portfolio.platform.uirenew.home.customize.diana.watchapps;

import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.pl2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlin.Triple;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1", f = "WatchAppsPresenter.kt", l = {284}, m = "invokeSuspend")
public final class WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $id;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppsPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1$Anon1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Parcelable>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1 watchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = watchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return WatchAppsPresenter.f(this.this$Anon0.this$Anon0).h(this.this$Anon0.$id);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1(WatchAppsPresenter watchAppsPresenter, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = watchAppsPresenter;
        this.$id = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1 watchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1 = new WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1(this.this$Anon0, this.$id, yb4);
        watchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1.p$ = (zg4) obj;
        return watchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        boolean z;
        Object a = cc4.a();
        int i = this.label;
        Parcelable parcelable = null;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            z = pl2.d.g(this.$id);
            if (z) {
                ug4 a2 = this.this$Anon0.b();
                Anon1 anon1 = new Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = null;
                this.Z$Anon0 = z;
                this.label = 1;
                obj = yf4.a(a2, anon1, this);
                if (obj == a) {
                    return a;
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "checkSettingOfSelectedWatchApp id=" + this.$id + " settings=" + parcelable);
            this.this$Anon0.k.a(new Triple(this.$id, dc4.a(z), parcelable));
            return qa4.a;
        } else if (i == 1) {
            boolean z2 = this.Z$Anon0;
            Parcelable parcelable2 = (Parcelable) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            z = z2;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        parcelable = (Parcelable) obj;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("WatchAppsPresenter", "checkSettingOfSelectedWatchApp id=" + this.$id + " settings=" + parcelable);
        this.this$Anon0.k.a(new Triple(this.$id, dc4.a(z), parcelable));
        return qa4.a;
    }
}
