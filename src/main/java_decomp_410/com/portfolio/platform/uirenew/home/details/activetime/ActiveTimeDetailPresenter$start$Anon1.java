package com.portfolio.platform.uirenew.home.details.activetime;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.Unit;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$Anon1", f = "ActiveTimeDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class ActiveTimeDetailPresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActiveTimeDetailPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActiveTimeDetailPresenter$start$Anon1(ActiveTimeDetailPresenter activeTimeDetailPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = activeTimeDetailPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ActiveTimeDetailPresenter$start$Anon1 activeTimeDetailPresenter$start$Anon1 = new ActiveTimeDetailPresenter$start$Anon1(this.this$Anon0, yb4);
        activeTimeDetailPresenter$start$Anon1.p$ = (zg4) obj;
        return activeTimeDetailPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActiveTimeDetailPresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001a, code lost:
        if (r0 != null) goto L_0x001f;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Unit unit;
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            ActiveTimeDetailPresenter activeTimeDetailPresenter = this.this$Anon0;
            MFUser currentUser = activeTimeDetailPresenter.v.getCurrentUser();
            if (currentUser != null) {
                unit = currentUser.getDistanceUnit();
            }
            unit = Unit.METRIC;
            activeTimeDetailPresenter.o = unit;
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
