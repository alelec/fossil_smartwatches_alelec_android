package com.portfolio.platform.uirenew.home.dashboard.calories.overview;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.oa3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.pa3;
import com.fossil.blesdk.obfuscated.qa3;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CaloriesOverviewMonthPresenter extends oa3 {
    @DexIgnore
    public MutableLiveData<Date> f; // = new MutableLiveData<>();
    @DexIgnore
    public Date g;
    @DexIgnore
    public Date h;
    @DexIgnore
    public Date i;
    @DexIgnore
    public Date j;
    @DexIgnore
    public LiveData<os3<List<ActivitySummary>>> k; // = new MutableLiveData();
    @DexIgnore
    public List<ActivitySummary> l; // = new ArrayList();
    @DexIgnore
    public LiveData<os3<List<ActivitySummary>>> m;
    @DexIgnore
    public TreeMap<Long, Float> n;
    @DexIgnore
    public /* final */ pa3 o;
    @DexIgnore
    public /* final */ UserRepository p;
    @DexIgnore
    public /* final */ SummariesRepository q;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewMonthPresenter a;

        @DexIgnore
        public b(CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter) {
            this.a = caloriesOverviewMonthPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<os3<List<ActivitySummary>>> apply(Date date) {
            CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter = this.a;
            kd4.a((Object) date, "it");
            if (caloriesOverviewMonthPresenter.b(date)) {
                CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter2 = this.a;
                caloriesOverviewMonthPresenter2.k = caloriesOverviewMonthPresenter2.q.getSummaries(CaloriesOverviewMonthPresenter.i(this.a), CaloriesOverviewMonthPresenter.h(this.a), true);
            }
            return this.a.k;
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public CaloriesOverviewMonthPresenter(pa3 pa3, UserRepository userRepository, SummariesRepository summariesRepository) {
        kd4.b(pa3, "mView");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(summariesRepository, "mSummariesRepository");
        this.o = pa3;
        this.p = userRepository;
        this.q = summariesRepository;
        LiveData<os3<List<ActivitySummary>>> b2 = hc.b(this.f, new b(this));
        kd4.a((Object) b2, "Transformations.switchMa\u2026 mActivitySummaries\n    }");
        this.m = b2;
    }

    @DexIgnore
    public static final /* synthetic */ Date f(CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter) {
        Date date = caloriesOverviewMonthPresenter.g;
        if (date != null) {
            return date;
        }
        kd4.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date h(CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter) {
        Date date = caloriesOverviewMonthPresenter.i;
        if (date != null) {
            return date;
        }
        kd4.d("mEndDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date i(CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter) {
        Date date = caloriesOverviewMonthPresenter.h;
        if (date != null) {
            return date;
        }
        kd4.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        h();
        LiveData<os3<List<ActivitySummary>>> liveData = this.m;
        pa3 pa3 = this.o;
        if (pa3 != null) {
            liveData.a((qa3) pa3, new CaloriesOverviewMonthPresenter$start$Anon1(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthPresenter", "stop");
        try {
            LiveData<os3<List<ActivitySummary>>> liveData = this.m;
            pa3 pa3 = this.o;
            if (pa3 != null) {
                liveData.a((LifecycleOwner) (qa3) pa3);
                this.k.a((LifecycleOwner) this.o);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthFragment");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CaloriesOverviewMonthPresenter", "stop - e=" + e);
        }
    }

    @DexIgnore
    public void h() {
        Date date = this.g;
        if (date != null) {
            if (date == null) {
                kd4.d("mCurrentDate");
                throw null;
            } else if (rk2.s(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.g;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("CaloriesOverviewMonthPresenter", sb.toString());
                    return;
                }
                kd4.d("mCurrentDate");
                throw null;
            }
        }
        this.g = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.g;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("CaloriesOverviewMonthPresenter", sb2.toString());
            fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new CaloriesOverviewMonthPresenter$loadData$Anon2(this, (yb4) null), 3, (Object) null);
            return;
        }
        kd4.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        this.o.a(this);
    }

    @DexIgnore
    public final boolean b(Date date) {
        Date date2;
        Date date3 = this.j;
        if (date3 == null) {
            date3 = new Date();
        }
        this.h = date3;
        Date date4 = this.h;
        if (date4 != null) {
            if (!rk2.a(date4.getTime(), date.getTime())) {
                Calendar o2 = rk2.o(date);
                kd4.a((Object) o2, "DateHelper.getStartOfMonth(date)");
                Date time = o2.getTime();
                kd4.a((Object) time, "DateHelper.getStartOfMonth(date).time");
                this.h = time;
            }
            Boolean r = rk2.r(date);
            kd4.a((Object) r, "DateHelper.isThisMonth(date)");
            if (r.booleanValue()) {
                date2 = new Date();
            } else {
                Calendar j2 = rk2.j(date);
                kd4.a((Object) j2, "DateHelper.getEndOfMonth(date)");
                date2 = j2.getTime();
                kd4.a((Object) date2, "DateHelper.getEndOfMonth(date).time");
            }
            this.i = date2;
            Date date5 = this.i;
            if (date5 != null) {
                long time2 = date5.getTime();
                Date date6 = this.h;
                if (date6 != null) {
                    return time2 >= date6.getTime();
                }
                kd4.d("mStartDate");
                throw null;
            }
            kd4.d("mEndDate");
            throw null;
        }
        kd4.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void a(Date date) {
        kd4.b(date, "date");
        if (this.f.a() == null || !rk2.d(this.f.a(), date)) {
            this.f.a(date);
        }
    }

    @DexIgnore
    public final TreeMap<Long, Float> a(Date date, List<ActivitySummary> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("CaloriesOverviewMonthPresenter", sb.toString());
        TreeMap<Long, Float> treeMap = new TreeMap<>();
        Calendar instance = Calendar.getInstance();
        if (list != null) {
            for (ActivitySummary next : list) {
                instance.set(next.getYear(), next.getMonth() - 1, next.getDay(), 0, 0, 0);
                instance.set(14, 0);
                if (next.getCaloriesGoal() > 0) {
                    kd4.a((Object) instance, "calendar");
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf(((float) next.getCalories()) / ((float) next.getCaloriesGoal())));
                } else {
                    kd4.a((Object) instance, "calendar");
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                }
            }
        }
        return treeMap;
    }
}
