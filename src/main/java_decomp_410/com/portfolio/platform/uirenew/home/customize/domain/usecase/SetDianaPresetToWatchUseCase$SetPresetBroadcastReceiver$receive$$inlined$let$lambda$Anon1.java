package com.portfolio.platform.uirenew.home.customize.domain.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import java.util.ArrayList;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $errorCode$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ DianaPreset $it;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $permissionErrorCodes$inlined;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SetDianaPresetToWatchUseCase.SetPresetBroadcastReceiver this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1(DianaPreset dianaPreset, yb4 yb4, SetDianaPresetToWatchUseCase.SetPresetBroadcastReceiver setPresetBroadcastReceiver, int i, ArrayList arrayList) {
        super(2, yb4);
        this.$it = dianaPreset;
        this.this$Anon0 = setPresetBroadcastReceiver;
        this.$errorCode$inlined = i;
        this.$permissionErrorCodes$inlined = arrayList;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SetDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1 setDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1 = new SetDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1(this.$it, yb4, this.this$Anon0, this.$errorCode$inlined, this.$permissionErrorCodes$inlined);
        setDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return setDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SetDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase = SetDianaPresetToWatchUseCase.this;
            DianaPreset dianaPreset = this.$it;
            this.L$Anon0 = zg4;
            this.label = 1;
            if (setDianaPresetToWatchUseCase.a(dianaPreset, (yb4<? super qa4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        SetDianaPresetToWatchUseCase.this.a(new SetDianaPresetToWatchUseCase.b(this.$errorCode$inlined, this.$permissionErrorCodes$inlined));
        return qa4.a;
    }
}
