package com.portfolio.platform.uirenew.home.customize.diana.watchapps;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.ns3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
public final class WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super String[]>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    public WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1(yb4 yb4) {
        super(2, yb4);
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1 watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1 = new WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1(yb4);
        watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1.p$ = (zg4) obj;
        return watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return ns3.a.a();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
