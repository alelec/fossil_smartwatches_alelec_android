package com.portfolio.platform.uirenew.home.details.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1", mo27670f = "GoalTrackingDetailPresenter.kt", mo27671l = {213}, mo27672m = "invokeSuspend")
public final class GoalTrackingDetailPresenter$deleteRecord$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.goaltracking.GoalTrackingData $raw;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23689p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1$1", mo27670f = "GoalTrackingDetailPresenter.kt", mo27671l = {213}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1$1 */
    public static final class C67151 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23690p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C67151(com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1 goalTrackingDetailPresenter$deleteRecord$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = goalTrackingDetailPresenter$deleteRecord$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1.C67151 r0 = new com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1.C67151(this.this$0, yb4);
            r0.f23690p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1.C67151) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23690p$;
                com.portfolio.platform.data.source.GoalTrackingRepository d = this.this$0.this$0.f23679r;
                com.portfolio.platform.data.model.goaltracking.GoalTrackingData goalTrackingData = this.this$0.$raw;
                this.L$0 = zg4;
                this.label = 1;
                if (d.deleteGoalTracking(goalTrackingData, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDetailPresenter$deleteRecord$1(com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter goalTrackingDetailPresenter, com.portfolio.platform.data.model.goaltracking.GoalTrackingData goalTrackingData, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = goalTrackingDetailPresenter;
        this.$raw = goalTrackingData;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1 goalTrackingDetailPresenter$deleteRecord$1 = new com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1(this.this$0, this.$raw, yb4);
        goalTrackingDetailPresenter$deleteRecord$1.f23689p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return goalTrackingDetailPresenter$deleteRecord$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23689p$;
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1.C67151 r3 = new com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1.C67151(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
