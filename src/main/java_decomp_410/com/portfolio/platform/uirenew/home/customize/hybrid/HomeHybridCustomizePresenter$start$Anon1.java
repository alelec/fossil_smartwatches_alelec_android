package com.portfolio.platform.uirenew.home.customize.hybrid;

import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.oa4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.z53;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.blesdk.obfuscated.zu2;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$Anon1", f = "HomeHybridCustomizePresenter.kt", l = {60}, m = "invokeSuspend")
public final class HomeHybridCustomizePresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeHybridCustomizePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$Anon1$Anon1", f = "HomeHybridCustomizePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<? extends MicroApp>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HomeHybridCustomizePresenter$start$Anon1 homeHybridCustomizePresenter$start$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = homeHybridCustomizePresenter$start$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return this.this$Anon0.this$Anon0.p.getAllMicroApp(this.this$Anon0.this$Anon0.n.e());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements cc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter$start$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$Anon1$a$a")
        /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$Anon1$a$a  reason: collision with other inner class name */
        public static final class C0144a<T> implements cc<List<? extends HybridPreset>> {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$Anon1$a$a$a")
            /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$Anon1$a$a$a  reason: collision with other inner class name */
            public static final class C0145a<T> implements Comparator<T> {
                @DexIgnore
                public final int compare(T t, T t2) {
                    return wb4.a(Boolean.valueOf(((HybridPreset) t2).isActive()), Boolean.valueOf(((HybridPreset) t).isActive()));
                }
            }

            @DexIgnore
            public C0144a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void a(List<HybridPreset> list) {
                if (list != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("HomeHybridCustomizePresenter", "onObserve on preset list change " + list);
                    List<T> a2 = kb4.a(list, new C0145a());
                    boolean a3 = kd4.a((Object) a2, (Object) this.a.a.this$Anon0.h) ^ true;
                    int itemCount = this.a.a.this$Anon0.o.getItemCount();
                    if (a3 || a2.size() != itemCount - 1) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("HomeHybridCustomizePresenter", "process change - " + a3 + " - itemCount: " + itemCount + " - sortedPresetSize: " + a2.size());
                        if (this.a.a.this$Anon0.l == 1) {
                            this.a.a.this$Anon0.a((List<HybridPreset>) a2);
                        } else {
                            this.a.a.this$Anon0.m = oa4.a(true, a2);
                        }
                    }
                }
            }
        }

        @DexIgnore
        public a(HomeHybridCustomizePresenter$start$Anon1 homeHybridCustomizePresenter$start$Anon1) {
            this.a = homeHybridCustomizePresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizePresenter", "on active serial change " + str + " mCurrentHomeTab " + this.a.this$Anon0.l);
            if (!TextUtils.isEmpty(str)) {
                HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.a.this$Anon0;
                HybridPresetRepository e = homeHybridCustomizePresenter.q;
                if (str != null) {
                    homeHybridCustomizePresenter.f = e.getPresetListAsLiveData(str);
                    this.a.this$Anon0.f.a((LifecycleOwner) this.a.this$Anon0.o, new C0144a(this));
                    this.a.this$Anon0.o.a(false);
                    return;
                }
                kd4.a();
                throw null;
            }
            this.a.this$Anon0.o.a(true);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeHybridCustomizePresenter$start$Anon1(HomeHybridCustomizePresenter homeHybridCustomizePresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = homeHybridCustomizePresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeHybridCustomizePresenter$start$Anon1 homeHybridCustomizePresenter$start$Anon1 = new HomeHybridCustomizePresenter$start$Anon1(this.this$Anon0, yb4);
        homeHybridCustomizePresenter$start$Anon1.p$ = (zg4) obj;
        return homeHybridCustomizePresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeHybridCustomizePresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0085  */
    public final Object invokeSuspend(Object obj) {
        z53 l;
        ArrayList arrayList;
        Object a2 = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            if (this.this$Anon0.g.isEmpty()) {
                ArrayList a3 = this.this$Anon0.g;
                ug4 b = this.this$Anon0.c();
                Anon1 anon1 = new Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = a3;
                this.label = 1;
                obj = yf4.a(b, anon1, this);
                if (obj == a2) {
                    return a2;
                }
                arrayList = a3;
            }
            MutableLiveData i2 = this.this$Anon0.j;
            l = this.this$Anon0.o;
            if (l == null) {
                i2.a((zu2) l, new a(this));
                this.this$Anon0.r.e();
                BleCommandResultManager.d.a(CommunicateMode.SET_LINK_MAPPING);
                return qa4.a;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } else if (i == 1) {
            arrayList = (ArrayList) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        arrayList.addAll((Collection) obj);
        MutableLiveData i22 = this.this$Anon0.j;
        l = this.this$Anon0.o;
        if (l == null) {
        }
    }
}
