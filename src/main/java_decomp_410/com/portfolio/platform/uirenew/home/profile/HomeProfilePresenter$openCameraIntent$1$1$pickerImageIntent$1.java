package com.portfolio.platform.uirenew.home.profile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1$1$pickerImageIntent$1", mo27670f = "HomeProfilePresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class HomeProfilePresenter$openCameraIntent$1$1$pickerImageIntent$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super android.content.Intent>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ androidx.fragment.app.FragmentActivity $it;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23814p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$openCameraIntent$1$1$pickerImageIntent$1(androidx.fragment.app.FragmentActivity fragmentActivity, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$it = fragmentActivity;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1$1$pickerImageIntent$1 homeProfilePresenter$openCameraIntent$1$1$pickerImageIntent$1 = new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1$1$pickerImageIntent$1(this.$it, yb4);
        homeProfilePresenter$openCameraIntent$1$1$pickerImageIntent$1.f23814p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeProfilePresenter$openCameraIntent$1$1$pickerImageIntent$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1$1$pickerImageIntent$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.al2.m19881b(this.$it);
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
