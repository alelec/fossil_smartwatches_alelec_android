package com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.q23;
import com.fossil.blesdk.obfuscated.u33;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SearchSecondTimezoneActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public SearchSecondTimezonePresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            kd4.b(fragment, "fragment");
            kd4.b(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), SearchSecondTimezoneActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 100);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        String str;
        FLogger.INSTANCE.getLocal().d(f(), "onCreate");
        super.onCreate(bundle);
        setContentView(R.layout.activity_base);
        q23 q23 = (q23) getSupportFragmentManager().a((int) R.id.content);
        if (q23 == null) {
            q23 = q23.o.a();
            a((Fragment) q23, q23.o.b(), (int) R.id.content);
        }
        l42 g = PortfolioApp.W.c().g();
        if (q23 != null) {
            g.a(new u33(q23)).a(this);
            Intent intent = getIntent();
            if (intent != null) {
                str = intent.getStringExtra(Constants.USER_SETTING);
                kd4.a((Object) str, "it.getStringExtra(Constants.JSON_KEY_SETTINGS)");
            } else {
                str = "";
            }
            SearchSecondTimezonePresenter searchSecondTimezonePresenter = this.B;
            if (searchSecondTimezonePresenter != null) {
                searchSecondTimezonePresenter.a(str);
            } else {
                kd4.d("mSearchSecondTimezonePresenter");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneContract.View");
        }
    }
}
