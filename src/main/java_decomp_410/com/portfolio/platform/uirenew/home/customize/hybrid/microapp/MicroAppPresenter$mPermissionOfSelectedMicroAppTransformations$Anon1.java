package com.portfolio.platform.uirenew.home.customize.hybrid.microapp;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import java.util.ArrayList;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1", f = "MicroAppPresenter.kt", l = {129, 130}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroApp $it;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1 microAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1, MicroApp microApp, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = microAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1;
            this.$it = microApp;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$it, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:34:0x011a, code lost:
            if (r0 <= 0) goto L_0x011c;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00b2  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00e4  */
        public final Object invokeSuspend(Object obj) {
            List<String> list;
            ArrayList<Pair> arrayList;
            zg4 zg4;
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 = this.p$;
                ug4 a2 = this.this$Anon0.a.b();
                MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1 microAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1 = new MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = yf4.a(a2, microAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else if (i == 2) {
                list = (List) this.L$Anon1;
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
                String[] strArr = (String[]) obj;
                arrayList = new ArrayList<>();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String l = MicroAppPresenter.v;
                local.d(l, "checkPermissionOf id=" + this.$it.getId() + ' ' + "grantedPermission " + strArr.length + " requiredPermission " + list.size());
                if (!list.isEmpty()) {
                    for (String str : list) {
                        arrayList.add(new Pair(str, dc4.a(za4.b((T[]) strArr, str))));
                    }
                }
                this.this$Anon0.a.k.a(arrayList);
                if (!arrayList.isEmpty()) {
                    int i2 = 0;
                    if (!arrayList.isEmpty()) {
                        for (Pair second : arrayList) {
                            if (dc4.a(!((Boolean) second.getSecond()).booleanValue()).booleanValue()) {
                                i2++;
                                if (i2 < 0) {
                                    cb4.b();
                                    throw null;
                                }
                            }
                        }
                    }
                }
                fi4 unused = this.this$Anon0.a.b(this.$it.getId());
                return qa4.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list2 = (List) obj;
            ug4 a3 = this.this$Anon0.a.b();
            MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1 microAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1 = new MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1((yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = list2;
            this.label = 2;
            Object a4 = yf4.a(a3, microAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1, this);
            if (a4 == a) {
                return a;
            }
            list = list2;
            obj = a4;
            String[] strArr2 = (String[]) obj;
            arrayList = new ArrayList<>();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String l2 = MicroAppPresenter.v;
            local2.d(l2, "checkPermissionOf id=" + this.$it.getId() + ' ' + "grantedPermission " + strArr2.length + " requiredPermission " + list.size());
            if (!list.isEmpty()) {
            }
            this.this$Anon0.a.k.a(arrayList);
            if (!arrayList.isEmpty()) {
            }
            fi4 unused2 = this.this$Anon0.a.b(this.$it.getId());
            return qa4.a;
        }
    }

    @DexIgnore
    public MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1(MicroAppPresenter microAppPresenter) {
        this.a = microAppPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final MutableLiveData<List<Pair<String, Boolean>>> apply(MicroApp microApp) {
        fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, microApp, (yb4) null), 3, (Object) null);
        return this.a.k;
    }
}
