package com.portfolio.platform.uirenew.home.dashboard.activity.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$1", mo27670f = "ActivityOverviewDayPresenter.kt", mo27671l = {114, 116, 117}, mo27672m = "invokeSuspend")
public final class ActivityOverviewDayPresenter$showDetailChart$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23208p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityOverviewDayPresenter$showDetailChart$1(com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter activityOverviewDayPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = activityOverviewDayPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$1 activityOverviewDayPresenter$showDetailChart$1 = new com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$1(this.this$0, yb4);
        activityOverviewDayPresenter$showDetailChart$1.f23208p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return activityOverviewDayPresenter$showDetailChart$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a7 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c4  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        kotlin.Pair pair;
        java.util.ArrayList arrayList;
        java.lang.Integer num;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        kotlin.Pair pair2;
        java.lang.Object a;
        java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f23208p$;
            com.fossil.blesdk.obfuscated.ug4 a3 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$1$pair$1 activityOverviewDayPresenter$showDetailChart$1$pair$1 = new com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$1$pair$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg42;
            this.label = 1;
            java.lang.Object a4 = com.fossil.blesdk.obfuscated.yf4.m30997a(a3, activityOverviewDayPresenter$showDetailChart$1$pair$1, this);
            if (a4 == a2) {
                return a2;
            }
            zg4 = zg42;
            obj = a4;
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
        } else if (i == 2) {
            arrayList = (java.util.ArrayList) this.L$2;
            pair2 = (kotlin.Pair) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.lang.Integer num2 = (java.lang.Integer) obj;
            com.fossil.blesdk.obfuscated.ug4 a5 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$1$activitySummary$1 activityOverviewDayPresenter$showDetailChart$1$activitySummary$1 = new com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$1$activitySummary$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = pair2;
            this.L$2 = arrayList;
            this.L$3 = num2;
            this.label = 3;
            a = com.fossil.blesdk.obfuscated.yf4.m30997a(a5, activityOverviewDayPresenter$showDetailChart$1$activitySummary$1, this);
            if (a != a2) {
                return a2;
            }
            num = num2;
            obj = a;
            pair = pair2;
            int a6 = com.fossil.blesdk.obfuscated.xk2.f20276d.mo32688a((com.portfolio.platform.data.model.room.fitness.ActivitySummary) obj, com.portfolio.platform.enums.GoalType.TOTAL_STEPS);
            this.this$0.f23201l.mo27356b(new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c(java.lang.Math.max(num == null ? num.intValue() : 0, a6 / 16), a6, arrayList), (java.util.ArrayList) pair.getSecond());
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 3) {
            num = (java.lang.Integer) this.L$3;
            arrayList = (java.util.ArrayList) this.L$2;
            pair = (kotlin.Pair) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            int a62 = com.fossil.blesdk.obfuscated.xk2.f20276d.mo32688a((com.portfolio.platform.data.model.room.fitness.ActivitySummary) obj, com.portfolio.platform.enums.GoalType.TOTAL_STEPS);
            this.this$0.f23201l.mo27356b(new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c(java.lang.Math.max(num == null ? num.intValue() : 0, a62 / 16), a62, arrayList), (java.util.ArrayList) pair.getSecond());
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        kotlin.Pair pair3 = (kotlin.Pair) obj;
        arrayList = (java.util.ArrayList) pair3.getFirst();
        com.fossil.blesdk.obfuscated.ug4 a7 = this.this$0.mo31440b();
        com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$1$maxValue$1 activityOverviewDayPresenter$showDetailChart$1$maxValue$1 = new com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$1$maxValue$1(arrayList, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.L$1 = pair3;
        this.L$2 = arrayList;
        this.label = 2;
        java.lang.Object a8 = com.fossil.blesdk.obfuscated.yf4.m30997a(a7, activityOverviewDayPresenter$showDetailChart$1$maxValue$1, this);
        if (a8 == a2) {
            return a2;
        }
        java.lang.Object obj2 = a8;
        pair2 = pair3;
        obj = obj2;
        java.lang.Integer num22 = (java.lang.Integer) obj;
        com.fossil.blesdk.obfuscated.ug4 a52 = this.this$0.mo31440b();
        com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$1$activitySummary$1 activityOverviewDayPresenter$showDetailChart$1$activitySummary$12 = new com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$1$activitySummary$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.L$1 = pair2;
        this.L$2 = arrayList;
        this.L$3 = num22;
        this.label = 3;
        a = com.fossil.blesdk.obfuscated.yf4.m30997a(a52, activityOverviewDayPresenter$showDetailChart$1$activitySummary$12, this);
        if (a != a2) {
        }
    }
}
