package com.portfolio.platform.uirenew.home.details.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1$invokeSuspend$$inlined$withLock$lambda$1 */
public final class C6712x243e8dc8 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23685p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6712x243e8dc8(com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1 goalTrackingDetailPresenter$setDate$1) {
        super(2, yb4);
        this.this$0 = goalTrackingDetailPresenter$setDate$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.goaltracking.C6712x243e8dc8 goalTrackingDetailPresenter$setDate$1$invokeSuspend$$inlined$withLock$lambda$1 = new com.portfolio.platform.uirenew.home.details.goaltracking.C6712x243e8dc8(yb4, this.this$0);
        goalTrackingDetailPresenter$setDate$1$invokeSuspend$$inlined$withLock$lambda$1.f23685p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return goalTrackingDetailPresenter$setDate$1$invokeSuspend$$inlined$withLock$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.goaltracking.C6712x243e8dc8) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter goalTrackingDetailPresenter = this.this$0.this$0;
            return goalTrackingDetailPresenter.mo41407a(goalTrackingDetailPresenter.f23668g, (java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>) this.this$0.this$0.f23673l);
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
