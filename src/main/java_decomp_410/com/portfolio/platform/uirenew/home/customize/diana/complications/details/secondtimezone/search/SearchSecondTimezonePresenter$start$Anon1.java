package com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import java.util.ArrayList;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$Anon1", f = "SearchSecondTimezonePresenter.kt", l = {37, 38}, m = "invokeSuspend")
public final class SearchSecondTimezonePresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SearchSecondTimezonePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SearchSecondTimezonePresenter$start$Anon1(SearchSecondTimezonePresenter searchSecondTimezonePresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = searchSecondTimezonePresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SearchSecondTimezonePresenter$start$Anon1 searchSecondTimezonePresenter$start$Anon1 = new SearchSecondTimezonePresenter$start$Anon1(this.this$Anon0, yb4);
        searchSecondTimezonePresenter$start$Anon1.p$ = (zg4) obj;
        return searchSecondTimezonePresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SearchSecondTimezonePresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            ug4 a2 = this.this$Anon0.b();
            SearchSecondTimezonePresenter$start$Anon1$rawDataList$Anon1 searchSecondTimezonePresenter$start$Anon1$rawDataList$Anon1 = new SearchSecondTimezonePresenter$start$Anon1$rawDataList$Anon1((yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a2, searchSecondTimezonePresenter$start$Anon1$rawDataList$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            ArrayList arrayList = (ArrayList) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            this.this$Anon0.g.addAll((ArrayList) obj);
            this.this$Anon0.i.q(this.this$Anon0.g);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ArrayList arrayList2 = (ArrayList) obj;
        ug4 a3 = this.this$Anon0.b();
        SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1 searchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1 = new SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1(arrayList2, (yb4) null);
        this.L$Anon0 = zg4;
        this.L$Anon1 = arrayList2;
        this.label = 2;
        obj = yf4.a(a3, searchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1, this);
        if (obj == a) {
            return a;
        }
        this.this$Anon0.g.addAll((ArrayList) obj);
        this.this$Anon0.i.q(this.this$Anon0.g);
        return qa4.a;
    }
}
