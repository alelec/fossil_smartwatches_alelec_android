package com.portfolio.platform.uirenew.home.customize.diana.complications.search;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.a43;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.b43;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationSearchPresenter extends a43 {
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h; // = "empty";
    @DexIgnore
    public String i; // = "empty";
    @DexIgnore
    public String j;
    @DexIgnore
    public /* final */ ArrayList<Complication> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Complication> l; // = new ArrayList<>();
    @DexIgnore
    public /* final */ b43 m;
    @DexIgnore
    public /* final */ ComplicationRepository n;
    @DexIgnore
    public /* final */ en2 o;

    @DexIgnore
    public ComplicationSearchPresenter(b43 b43, ComplicationRepository complicationRepository, en2 en2) {
        kd4.b(b43, "mView");
        kd4.b(complicationRepository, "mComplicationRepository");
        kd4.b(en2, "sharedPreferencesManager");
        this.m = b43;
        this.n = complicationRepository;
        this.o = en2;
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.j = "";
        this.m.u();
        j();
    }

    @DexIgnore
    public final b43 i() {
        return this.m;
    }

    @DexIgnore
    public final void j() {
        if (this.l.isEmpty()) {
            this.m.b(a((List<Complication>) kb4.d(this.k)));
        } else {
            this.m.e(a((List<Complication>) kb4.d(this.l)));
        }
        if (!TextUtils.isEmpty(this.j)) {
            b43 b43 = this.m;
            String str = this.j;
            if (str != null) {
                b43.a(str);
                String str2 = this.j;
                if (str2 != null) {
                    a(str2);
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void k() {
        this.m.a(this);
    }

    @DexIgnore
    public void f() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ComplicationSearchPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, String str4) {
        kd4.b(str, "topComplication");
        kd4.b(str2, "bottomComplication");
        kd4.b(str3, "rightComplication");
        kd4.b(str4, "leftComplication");
        this.f = str;
        this.g = str2;
        this.i = str3;
        this.h = str4;
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = new ArrayList();
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ComplicationSearchPresenter$search$Anon1(this, str, ref$ObjectRef, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(Complication complication) {
        kd4.b(complication, "selectedComplication");
        List<String> d = this.o.d();
        kd4.a((Object) d, "sharedPreferencesManager\u2026licationSearchedIdsRecent");
        if (!d.contains(complication.getComplicationId())) {
            d.add(0, complication.getComplicationId());
            if (d.size() > 5) {
                d = d.subList(0, 5);
            }
            this.o.b(d);
        }
        this.m.a(complication);
    }

    @DexIgnore
    public final List<Pair<Complication, String>> a(List<Complication> list) {
        ArrayList arrayList = new ArrayList();
        for (Complication next : list) {
            String complicationId = next.getComplicationId();
            if (kd4.a((Object) complicationId, (Object) this.f)) {
                arrayList.add(new Pair(next, ViewHierarchy.DIMENSION_TOP_KEY));
            } else if (kd4.a((Object) complicationId, (Object) this.g)) {
                arrayList.add(new Pair(next, "bottom"));
            } else if (kd4.a((Object) complicationId, (Object) this.i)) {
                arrayList.add(new Pair(next, "right"));
            } else if (kd4.a((Object) complicationId, (Object) this.h)) {
                arrayList.add(new Pair(next, ViewHierarchy.DIMENSION_LEFT_KEY));
            } else {
                arrayList.add(new Pair(next, ""));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        if (bundle != null) {
            bundle.putString(ViewHierarchy.DIMENSION_TOP_KEY, this.f);
            bundle.putString("bottom", this.g);
            bundle.putString(ViewHierarchy.DIMENSION_LEFT_KEY, this.h);
            bundle.putString("right", this.i);
        }
        return bundle;
    }
}
