package com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1", mo27670f = "NotificationHybridContactPresenter.kt", mo27671l = {48}, mo27672m = "invokeSuspend")
public final class NotificationHybridContactPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22567p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$1", mo27670f = "NotificationHybridContactPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$1 */
    public static final class C63831 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22568p$;

        @DexIgnore
        public C63831(com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1.C63831 r0 = new com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1.C63831(yb4);
            r0.f22568p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1.C63831) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34463J();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2 */
    public static final class C63842 implements com.fossil.blesdk.obfuscated.i62.C4439d<com.fossil.blesdk.obfuscated.t03.C5127d, com.fossil.blesdk.obfuscated.t03.C5125b> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1 f22569a;

        @DexIgnore
        public C63842(com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1 notificationHybridContactPresenter$start$1) {
            this.f22569a = notificationHybridContactPresenter$start$1;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(com.fossil.blesdk.obfuscated.t03.C5127d dVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "successResponse");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter.f22559n.mo40926a(), "GetAllContactGroup onSuccess");
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f22569a.this$0.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1(this, dVar, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo28250a(com.fossil.blesdk.obfuscated.t03.C5125b bVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(bVar, "errorResponse");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter.f22559n.mo40926a(), "GetAllContactGroup onError");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationHybridContactPresenter$start$1(com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter notificationHybridContactPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = notificationHybridContactPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1 notificationHybridContactPresenter$start$1 = new com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1(this.this$0, yb4);
        notificationHybridContactPresenter$start$1.f22567p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationHybridContactPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22567p$;
            if (!com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34574u().mo26976N()) {
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1.C63831 r4 = new com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1.C63831((com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r4, this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (this.this$0.mo40924j().isEmpty()) {
            this.this$0.f22565k.mo28479a(this.this$0.f22566l, null, new com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1.C63842(this));
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
