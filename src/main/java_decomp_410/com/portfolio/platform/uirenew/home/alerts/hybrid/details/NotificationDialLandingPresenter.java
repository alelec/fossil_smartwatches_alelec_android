package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.os.Bundle;
import android.util.SparseArray;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.bn2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.pz2;
import com.fossil.blesdk.obfuscated.qc;
import com.fossil.blesdk.obfuscated.qz2;
import com.fossil.blesdk.obfuscated.rz2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationDialLandingPresenter extends pz2 implements LoaderManager.a<SparseArray<List<? extends BaseFeatureModel>>> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public /* final */ qz2 f;
    @DexIgnore
    public /* final */ NotificationsLoader g;
    @DexIgnore
    public /* final */ LoaderManager h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = NotificationDialLandingPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationDialLandingP\u2026er::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public NotificationDialLandingPresenter(qz2 qz2, NotificationsLoader notificationsLoader, LoaderManager loaderManager) {
        kd4.b(qz2, "mView");
        kd4.b(notificationsLoader, "mNotificationLoader");
        kd4.b(loaderManager, "mLoaderManager");
        this.f = qz2;
        this.g = notificationsLoader;
        this.h = loaderManager;
    }

    @DexIgnore
    public void a(qc<SparseArray<List<BaseFeatureModel>>> qcVar) {
        kd4.b(qcVar, "loader");
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(i, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        bn2 bn2 = bn2.d;
        qz2 qz2 = this.f;
        if (qz2 != null) {
            if (bn2.a(bn2, ((rz2) qz2).getContext(), "NOTIFICATION_CONTACTS", false, 4, (Object) null) && bn2.a(bn2.d, ((rz2) this.f).getContext(), "NOTIFICATION_APPS", false, 4, (Object) null) && !PortfolioApp.W.c().u().N()) {
                fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationDialLandingPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
            }
            this.h.a(3, (Bundle) null, this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingFragment");
    }

    @DexIgnore
    public void g() {
        this.h.a(3);
        FLogger.INSTANCE.getLocal().d(i, "stop");
    }

    @DexIgnore
    public void h() {
        this.f.a(this);
    }

    @DexIgnore
    public qc<SparseArray<List<BaseFeatureModel>>> a(int i2, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onCreateLoader id=" + i2);
        return this.g;
    }

    @DexIgnore
    public void a(qc<SparseArray<List<BaseFeatureModel>>> qcVar, SparseArray<List<BaseFeatureModel>> sparseArray) {
        kd4.b(qcVar, "loader");
        if (sparseArray != null) {
            this.f.a(sparseArray);
        } else {
            FLogger.INSTANCE.getLocal().d(i, "onLoadFinished, data=null");
        }
    }
}
