package com.portfolio.platform.uirenew.home.profile.opt;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.MFUser;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$Anon1", f = "ProfileOptInPresenter.kt", l = {31}, m = "invokeSuspend")
public final class ProfileOptInPresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileOptInPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$Anon1$Anon1", f = "ProfileOptInPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super MFUser>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInPresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ProfileOptInPresenter$start$Anon1 profileOptInPresenter$start$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = profileOptInPresenter$start$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return this.this$Anon0.this$Anon0.h().getCurrentUser();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileOptInPresenter$start$Anon1(ProfileOptInPresenter profileOptInPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = profileOptInPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ProfileOptInPresenter$start$Anon1 profileOptInPresenter$start$Anon1 = new ProfileOptInPresenter$start$Anon1(this.this$Anon0, yb4);
        profileOptInPresenter$start$Anon1.p$ = (zg4) obj;
        return profileOptInPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileOptInPresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ProfileOptInPresenter profileOptInPresenter;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ProfileOptInPresenter profileOptInPresenter2 = this.this$Anon0;
            ug4 a2 = profileOptInPresenter2.c();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = profileOptInPresenter2;
            this.label = 1;
            obj = yf4.a(a2, anon1, this);
            if (obj == a) {
                return a;
            }
            profileOptInPresenter = profileOptInPresenter2;
        } else if (i == 1) {
            profileOptInPresenter = (ProfileOptInPresenter) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        profileOptInPresenter.g = (MFUser) obj;
        MFUser b = this.this$Anon0.g;
        if (b != null) {
            this.this$Anon0.i().E(b.isDiagnosticEnabled());
            this.this$Anon0.i().H(b.isEmailOptIn());
        }
        return qa4.a;
    }
}
