package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gb4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ml2;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rj2;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.rt3;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.enums.Status;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$IntRef;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateOverviewDayPresenter$start$Anon1<T> implements cc<os3<? extends List<HeartRateSample>>> {
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateOverviewDayPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$Anon1$Anon1", f = "HeartRateOverviewDayPresenter.kt", l = {50, 58, 65}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $data;
        @DexIgnore
        public int I$Anon0;
        @DexIgnore
        public long J$Anon0;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public Object L$Anon2;
        @DexIgnore
        public Object L$Anon3;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewDayPresenter$start$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$Anon1$Anon1$Anon1")
        @gc4(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$Anon1$Anon1$Anon1", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0154Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$Anon1$Anon1$Anon1$a")
            /* renamed from: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$Anon1$Anon1$Anon1$a */
            public static final class a<T> implements Comparator<T> {
                @DexIgnore
                public final int compare(T t, T t2) {
                    return wb4.a(Long.valueOf(((HeartRateSample) t).getStartTimeId().getMillis()), Long.valueOf(((HeartRateSample) t2).getStartTimeId().getMillis()));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0154Anon1(Anon1 anon1, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0154Anon1 anon1 = new C0154Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0154Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                cc4.a();
                if (this.label == 0) {
                    na4.a(obj);
                    List list = this.this$Anon0.$data;
                    if (list == null) {
                        return null;
                    }
                    if (list.size() > 1) {
                        gb4.a(list, new a());
                    }
                    return qa4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        @gc4(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$Anon1$Anon1$Anon2", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $listTimeZoneChange;
            @DexIgnore
            public /* final */ /* synthetic */ List $listTodayHeartRateModel;
            @DexIgnore
            public /* final */ /* synthetic */ int $maxHR;
            @DexIgnore
            public /* final */ /* synthetic */ Ref$IntRef $previousTimeZoneOffset;
            @DexIgnore
            public /* final */ /* synthetic */ long $startOfDay;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon2(Anon1 anon1, long j, Ref$IntRef ref$IntRef, List list, List list2, int i, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
                this.$startOfDay = j;
                this.$previousTimeZoneOffset = ref$IntRef;
                this.$listTimeZoneChange = list;
                this.$listTodayHeartRateModel = list2;
                this.$maxHR = i;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                Anon2 anon2 = new Anon2(this.this$Anon0, this.$startOfDay, this.$previousTimeZoneOffset, this.$listTimeZoneChange, this.$listTodayHeartRateModel, this.$maxHR, yb4);
                anon2.p$ = (zg4) obj;
                return anon2;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Iterator it;
                char c;
                StringBuilder sb;
                cc4.a();
                if (this.label == 0) {
                    na4.a(obj);
                    List list = this.this$Anon0.$data;
                    if (list == null) {
                        return null;
                    }
                    for (Iterator it2 = list.iterator(); it2.hasNext(); it2 = it) {
                        HeartRateSample heartRateSample = (HeartRateSample) it2.next();
                        long millis = (heartRateSample.getStartTimeId().getMillis() - this.$startOfDay) / 60000;
                        long millis2 = (heartRateSample.getEndTime().getMillis() - this.$startOfDay) / 60000;
                        long j = (millis2 + millis) / ((long) 2);
                        if (heartRateSample.getTimezoneOffsetInSecond() != this.$previousTimeZoneOffset.element) {
                            int hourOfDay = heartRateSample.getStartTimeId().getHourOfDay();
                            String a = ml2.a(hourOfDay);
                            pd4 pd4 = pd4.a;
                            Object[] objArr = {dc4.a(Math.abs(heartRateSample.getTimezoneOffsetInSecond() / 3600)), dc4.a(Math.abs((heartRateSample.getTimezoneOffsetInSecond() / 60) % 60))};
                            String format = String.format("%02d:%02d", Arrays.copyOf(objArr, objArr.length));
                            kd4.a((Object) format, "java.lang.String.format(format, *args)");
                            List list2 = this.$listTimeZoneChange;
                            Integer a2 = dc4.a((int) j);
                            it = it2;
                            Pair pair = new Pair(dc4.a(hourOfDay), dc4.a(((float) heartRateSample.getTimezoneOffsetInSecond()) / 3600.0f));
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(a);
                            if (heartRateSample.getTimezoneOffsetInSecond() >= 0) {
                                sb = new StringBuilder();
                                c = '+';
                            } else {
                                sb = new StringBuilder();
                                c = '-';
                            }
                            sb.append(c);
                            sb.append(format);
                            sb2.append(sb.toString());
                            list2.add(new Triple(a2, pair, sb2.toString()));
                            this.$previousTimeZoneOffset.element = heartRateSample.getTimezoneOffsetInSecond();
                        } else {
                            it = it2;
                        }
                        if (!this.$listTodayHeartRateModel.isEmpty()) {
                            rt3 rt3 = (rt3) kb4.f(this.$listTodayHeartRateModel);
                            if (millis - ((long) rt3.a()) > 1) {
                                this.$listTodayHeartRateModel.add(new rt3(0, 0, 0, rt3.a(), (int) millis, (int) j));
                            }
                        }
                        int average = (int) heartRateSample.getAverage();
                        if (heartRateSample.getMax() == this.$maxHR) {
                            average = heartRateSample.getMax();
                        }
                        this.$listTodayHeartRateModel.add(new rt3(average, heartRateSample.getMin(), heartRateSample.getMax(), (int) millis, (int) millis2, (int) j));
                    }
                    return qa4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HeartRateOverviewDayPresenter$start$Anon1 heartRateOverviewDayPresenter$start$Anon1, List list, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = heartRateOverviewDayPresenter$start$Anon1;
            this.$data = list;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$data, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x012c A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x012d  */
        public final Object invokeSuspend(Object obj) {
            List list;
            List list2;
            int i;
            int i2;
            long j;
            List list3;
            zg4 zg4;
            Object obj2;
            ug4 a;
            Anon2 anon2;
            Object obj3;
            zg4 zg42;
            List list4;
            long j2;
            Object a2 = cc4.a();
            int i3 = this.label;
            if (i3 == 0) {
                na4.a(obj);
                zg4 zg43 = this.p$;
                ArrayList arrayList = new ArrayList();
                ug4 a3 = this.this$Anon0.a.b();
                C0154Anon1 anon1 = new C0154Anon1(this, (yb4) null);
                this.L$Anon0 = zg43;
                this.L$Anon1 = arrayList;
                this.label = 1;
                if (yf4.a(a3, anon1, this) == a2) {
                    return a2;
                }
                ArrayList arrayList2 = arrayList;
                zg42 = zg43;
                list4 = arrayList2;
            } else if (i3 == 1) {
                list4 = (List) this.L$Anon1;
                zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else if (i3 == 2) {
                long j3 = this.J$Anon0;
                na4.a(obj);
                j = j3;
                list3 = (List) this.L$Anon1;
                zg4 = (zg4) this.L$Anon0;
                obj2 = obj;
                int intValue = ((Number) obj2).intValue();
                Ref$IntRef ref$IntRef = new Ref$IntRef();
                ref$IntRef.element = -1;
                ArrayList arrayList3 = new ArrayList();
                a = this.this$Anon0.a.b();
                ArrayList arrayList4 = arrayList3;
                int i4 = intValue;
                anon2 = r0;
                List list5 = list3;
                Anon2 anon22 = new Anon2(this, j, ref$IntRef, arrayList4, list3, i4, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = list5;
                this.J$Anon0 = j;
                this.I$Anon0 = i4;
                this.L$Anon2 = ref$IntRef;
                list2 = arrayList4;
                this.L$Anon3 = list2;
                this.label = 3;
                obj3 = a2;
                if (yf4.a(a, anon2, this) != obj3) {
                    return obj3;
                }
                list = list5;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HeartRateOverviewDayPresenter", "listTimeZoneChange=" + rj2.a(list2));
                if (!list2.isEmpty()) {
                }
                i = 0;
                list2.clear();
                int i5 = i + DateTimeConstants.MINUTES_PER_DAY;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("HeartRateOverviewDayPresenter", "minutesOfDay=" + i5);
                this.this$Anon0.a.i.a(i5, list, list2);
                return qa4.a;
            } else if (i3 == 3) {
                list2 = (List) this.L$Anon3;
                Ref$IntRef ref$IntRef2 = (Ref$IntRef) this.L$Anon2;
                list = (List) this.L$Anon1;
                zg4 zg44 = (zg4) this.L$Anon0;
                na4.a(obj);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("HeartRateOverviewDayPresenter", "listTimeZoneChange=" + rj2.a(list2));
                if ((!list2.isEmpty()) || list2.size() <= 1) {
                    i = 0;
                    list2.clear();
                } else {
                    float floatValue = ((Number) ((Pair) ((Triple) list2.get(0)).getSecond()).getSecond()).floatValue();
                    float floatValue2 = ((Number) ((Pair) ((Triple) list2.get(cb4.a(list2))).getSecond()).getSecond()).floatValue();
                    float f = (float) 0;
                    if ((floatValue >= f || floatValue2 >= f) && (floatValue < f || floatValue2 < f)) {
                        i2 = Math.abs((int) ((floatValue - floatValue2) * ((float) 60)));
                        if (floatValue2 < f) {
                            i2 = -i2;
                        }
                    } else {
                        i2 = (int) ((floatValue - floatValue2) * ((float) 60));
                    }
                    int i6 = i2;
                    ArrayList<Triple> arrayList5 = new ArrayList<>();
                    for (Object next : list2) {
                        if (dc4.a(((Number) ((Pair) ((Triple) next).getSecond()).getSecond()).floatValue() == floatValue).booleanValue()) {
                            arrayList5.add(next);
                        }
                    }
                    ArrayList<Pair> arrayList6 = new ArrayList<>(db4.a(arrayList5, 10));
                    for (Triple second : arrayList5) {
                        arrayList6.add((Pair) second.getSecond());
                    }
                    ArrayList arrayList7 = new ArrayList(db4.a(arrayList6, 10));
                    for (Pair first : arrayList6) {
                        arrayList7.add(dc4.a(((Number) first.getFirst()).intValue()));
                    }
                    if (!arrayList7.contains(dc4.a(0))) {
                        String a4 = this.this$Anon0.a.a(dc4.a(floatValue));
                        Integer a5 = dc4.a(0);
                        Pair pair = new Pair(dc4.a(0), dc4.a(floatValue));
                        list2.add(0, new Triple(a5, pair, "12a" + a4));
                    }
                    ArrayList<Triple> arrayList8 = new ArrayList<>();
                    for (Object next2 : list2) {
                        if (dc4.a(((Number) ((Pair) ((Triple) next2).getSecond()).getSecond()).floatValue() == floatValue2).booleanValue()) {
                            arrayList8.add(next2);
                        }
                    }
                    ArrayList<Pair> arrayList9 = new ArrayList<>(db4.a(arrayList8, 10));
                    for (Triple second2 : arrayList8) {
                        arrayList9.add((Pair) second2.getSecond());
                    }
                    ArrayList arrayList10 = new ArrayList(db4.a(arrayList9, 10));
                    for (Pair first2 : arrayList9) {
                        arrayList10.add(dc4.a(((Number) first2.getFirst()).intValue()));
                    }
                    if (!arrayList10.contains(dc4.a(24))) {
                        String a6 = this.this$Anon0.a.a(dc4.a(floatValue2));
                        Integer a7 = dc4.a(i6 + DateTimeConstants.MINUTES_PER_DAY);
                        Pair pair2 = new Pair(dc4.a(24), dc4.a(floatValue2));
                        list2.add(new Triple(a7, pair2, "12a" + a6));
                    }
                    i = i6;
                }
                int i52 = i + DateTimeConstants.MINUTES_PER_DAY;
                ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                local22.d("HeartRateOverviewDayPresenter", "minutesOfDay=" + i52);
                this.this$Anon0.a.i.a(i52, list, list2);
                return qa4.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list6 = this.$data;
            if (list6 == null || !(!list6.isEmpty())) {
                Date n = rk2.n(HeartRateOverviewDayPresenter.b(this.this$Anon0.a));
                kd4.a((Object) n, "DateHelper.getStartOfDay(mDate)");
                j2 = n.getTime();
            } else {
                DateTime withTimeAtStartOfDay = ((HeartRateSample) this.$data.get(0)).getStartTimeId().withTimeAtStartOfDay();
                kd4.a((Object) withTimeAtStartOfDay, "data[0].getStartTimeId().withTimeAtStartOfDay()");
                j2 = withTimeAtStartOfDay.getMillis();
            }
            ug4 a8 = this.this$Anon0.a.b();
            HeartRateOverviewDayPresenter$start$Anon1$Anon1$maxHR$Anon1 heartRateOverviewDayPresenter$start$Anon1$Anon1$maxHR$Anon1 = new HeartRateOverviewDayPresenter$start$Anon1$Anon1$maxHR$Anon1(this, (yb4) null);
            this.L$Anon0 = zg42;
            this.L$Anon1 = list4;
            this.J$Anon0 = j2;
            this.label = 2;
            obj2 = yf4.a(a8, heartRateOverviewDayPresenter$start$Anon1$Anon1$maxHR$Anon1, this);
            if (obj2 == a2) {
                return a2;
            }
            list3 = list4;
            zg4 = zg42;
            j = j2;
            int intValue2 = ((Number) obj2).intValue();
            Ref$IntRef ref$IntRef3 = new Ref$IntRef();
            ref$IntRef3.element = -1;
            ArrayList arrayList32 = new ArrayList();
            a = this.this$Anon0.a.b();
            ArrayList arrayList42 = arrayList32;
            int i42 = intValue2;
            anon2 = anon22;
            List list52 = list3;
            Anon2 anon222 = new Anon2(this, j, ref$IntRef3, arrayList42, list3, i42, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = list52;
            this.J$Anon0 = j;
            this.I$Anon0 = i42;
            this.L$Anon2 = ref$IntRef3;
            list2 = arrayList42;
            this.L$Anon3 = list2;
            this.label = 3;
            obj3 = a2;
            if (yf4.a(a, anon2, this) != obj3) {
            }
        }
    }

    @DexIgnore
    public HeartRateOverviewDayPresenter$start$Anon1(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
        this.a = heartRateOverviewDayPresenter;
    }

    @DexIgnore
    public final void a(os3<? extends List<HeartRateSample>> os3) {
        Status a2 = os3.a();
        List list = (List) os3.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("start - mHeartRateSamples -- heartRateSamples=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        sb.append(", status=");
        sb.append(a2);
        local.d("HeartRateOverviewDayPresenter", sb.toString());
        if (a2 != Status.DATABASE_LOADING) {
            fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, list, (yb4) null), 3, (Object) null);
        }
    }
}
