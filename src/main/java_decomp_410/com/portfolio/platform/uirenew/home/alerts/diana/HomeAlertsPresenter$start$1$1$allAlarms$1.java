package com.portfolio.platform.uirenew.home.alerts.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$allAlarms$1", mo27670f = "HomeAlertsPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class HomeAlertsPresenter$start$1$1$allAlarms$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<com.portfolio.platform.data.source.local.alarm.Alarm>>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22336p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeAlertsPresenter$start$1$1$allAlarms$1(com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1.C63071 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = r1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$allAlarms$1 homeAlertsPresenter$start$1$1$allAlarms$1 = new com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$allAlarms$1(this.this$0, yb4);
        homeAlertsPresenter$start$1$1$allAlarms$1.f22336p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeAlertsPresenter$start$1$1$allAlarms$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$allAlarms$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return this.this$0.this$0.f22330a.f22315t.getAllAlarmIgnoreDeletePinType();
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
