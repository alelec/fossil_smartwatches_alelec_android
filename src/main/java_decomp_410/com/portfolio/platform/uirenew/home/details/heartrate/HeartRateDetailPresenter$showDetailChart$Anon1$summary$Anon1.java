package com.portfolio.platform.uirenew.home.details.heartrate;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$Anon1$summary$Anon1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class HeartRateDetailPresenter$showDetailChart$Anon1$summary$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<HeartRateSample>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateDetailPresenter$showDetailChart$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$showDetailChart$Anon1$summary$Anon1(HeartRateDetailPresenter$showDetailChart$Anon1 heartRateDetailPresenter$showDetailChart$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = heartRateDetailPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HeartRateDetailPresenter$showDetailChart$Anon1$summary$Anon1 heartRateDetailPresenter$showDetailChart$Anon1$summary$Anon1 = new HeartRateDetailPresenter$showDetailChart$Anon1$summary$Anon1(this.this$Anon0, yb4);
        heartRateDetailPresenter$showDetailChart$Anon1$summary$Anon1.p$ = (zg4) obj;
        return heartRateDetailPresenter$showDetailChart$Anon1$summary$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HeartRateDetailPresenter$showDetailChart$Anon1$summary$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            HeartRateDetailPresenter heartRateDetailPresenter = this.this$Anon0.this$Anon0;
            return heartRateDetailPresenter.a(heartRateDetailPresenter.g, (List<HeartRateSample>) this.this$Anon0.this$Anon0.j);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
