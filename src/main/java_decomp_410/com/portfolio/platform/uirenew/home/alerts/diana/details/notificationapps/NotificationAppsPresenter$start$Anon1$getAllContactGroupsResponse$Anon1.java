package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.w52;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.CoroutineUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1", f = "NotificationAppsPresenter.kt", l = {192}, m = "invokeSuspend")
public final class NotificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super CoroutineUseCase.c>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationAppsPresenter$start$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1(NotificationAppsPresenter$start$Anon1 notificationAppsPresenter$start$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = notificationAppsPresenter$start$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NotificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1 notificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1 = new NotificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1(this.this$Anon0, yb4);
        notificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1.p$ = (zg4) obj;
        return notificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            px2 c = this.this$Anon0.this$Anon0.p;
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = w52.a(c, null, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
