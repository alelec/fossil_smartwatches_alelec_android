package com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.bn2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.k03;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l03;
import com.fossil.blesdk.obfuscated.m03;
import com.fossil.blesdk.obfuscated.pc;
import com.fossil.blesdk.obfuscated.qc;
import com.fossil.blesdk.obfuscated.t03;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationHybridContactPresenter extends k03 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public /* final */ List<ContactWrapper> f; // = new ArrayList();
    @DexIgnore
    public /* final */ l03 g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ ArrayList<ContactWrapper> i;
    @DexIgnore
    public /* final */ LoaderManager j;
    @DexIgnore
    public /* final */ j62 k;
    @DexIgnore
    public /* final */ t03 l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationHybridContactPresenter.m;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = NotificationHybridContactPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationHybridContac\u2026er::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public NotificationHybridContactPresenter(l03 l03, int i2, ArrayList<ContactWrapper> arrayList, LoaderManager loaderManager, j62 j62, t03 t03) {
        kd4.b(l03, "mView");
        kd4.b(arrayList, "mContactWrappersSelected");
        kd4.b(loaderManager, "mLoaderManager");
        kd4.b(j62, "mUseCaseHandler");
        kd4.b(t03, "mGetAllHybridContactGroups");
        this.g = l03;
        this.h = i2;
        this.i = arrayList;
        this.j = loaderManager;
        this.k = j62;
        this.l = t03;
    }

    @DexIgnore
    public void i() {
        ArrayList arrayList = new ArrayList();
        for (ContactWrapper contactWrapper : this.f) {
            if (contactWrapper.isAdded() && contactWrapper.getCurrentHandGroup() == this.h) {
                arrayList.add(contactWrapper);
            }
        }
        this.g.a((ArrayList<ContactWrapper>) arrayList);
    }

    @DexIgnore
    public final List<ContactWrapper> j() {
        return this.f;
    }

    @DexIgnore
    public void k() {
        this.g.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(m, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        bn2 bn2 = bn2.d;
        l03 l03 = this.g;
        if (l03 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactFragment");
        } else if (bn2.a(bn2, ((m03) l03).getContext(), "NOTIFICATION_CONTACTS", false, 4, (Object) null)) {
            fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationHybridContactPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(m, "stop");
    }

    @DexIgnore
    public int h() {
        return this.h;
    }

    @DexIgnore
    public void a(ContactWrapper contactWrapper) {
        T t;
        kd4.b(contactWrapper, "contactWrapper");
        FLogger.INSTANCE.getLocal().d(m, "reassignContact: contactWrapper=" + contactWrapper);
        Iterator<T> it = this.f.iterator();
        while (true) {
            Integer num = null;
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            Contact contact = ((ContactWrapper) t).getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Contact contact2 = contactWrapper.getContact();
            if (contact2 != null) {
                num = Integer.valueOf(contact2.getContactId());
            }
            if (kd4.a((Object) valueOf, (Object) num)) {
                break;
            }
        }
        ContactWrapper contactWrapper2 = (ContactWrapper) t;
        if (contactWrapper2 != null) {
            contactWrapper2.setAdded(true);
            contactWrapper2.setCurrentHandGroup(this.h);
            this.g.x0();
        }
    }

    @DexIgnore
    public qc<Cursor> a(int i2, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, ".Inside onCreateLoader, selection = " + "has_phone_number!=0 AND mimetype=?");
        String[] strArr = {"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"};
        return new pc(PortfolioApp.W.c(), ContactsContract.Data.CONTENT_URI, strArr, "has_phone_number!=0 AND mimetype=?", new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
    }

    @DexIgnore
    public void a(qc<Cursor> qcVar, Cursor cursor) {
        kd4.b(qcVar, "loader");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, ".Inside onLoadFinished cursor=" + cursor);
        this.g.a(cursor);
    }

    @DexIgnore
    public void a(qc<Cursor> qcVar) {
        kd4.b(qcVar, "loader");
        FLogger.INSTANCE.getLocal().d(m, ".Inside onLoaderReset");
        this.g.H();
    }
}
