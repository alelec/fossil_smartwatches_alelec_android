package com.portfolio.platform.uirenew.home.customize.hybrid.microapp;

import android.content.Context;
import android.os.Parcelable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.g63;
import com.fossil.blesdk.obfuscated.gl2;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.ns3;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.t63;
import com.fossil.blesdk.obfuscated.u63;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppPresenter extends t63 {
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public HybridCustomizeViewModel f;
    @DexIgnore
    public ArrayList<Category> g; // = new ArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<MicroApp> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<MicroApp>> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Pair<String, Boolean>>> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Triple<String, Boolean, Parcelable>> l; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<String> m;
    @DexIgnore
    public /* final */ cc<String> n;
    @DexIgnore
    public /* final */ LiveData<List<MicroApp>> o;
    @DexIgnore
    public /* final */ cc<List<MicroApp>> p;
    @DexIgnore
    public /* final */ LiveData<List<Pair<String, Boolean>>> q;
    @DexIgnore
    public /* final */ cc<List<Pair<String, Boolean>>> r;
    @DexIgnore
    public /* final */ cc<Triple<String, Boolean, Parcelable>> s;
    @DexIgnore
    public /* final */ u63 t;
    @DexIgnore
    public /* final */ CategoryRepository u;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public b(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String l = MicroAppPresenter.v;
            local.d(l, "onLiveDataChanged category=" + str);
            if (str != null) {
                this.a.t.e(str);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public c(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(MicroApp microApp) {
            if (microApp != null) {
                String str = (String) this.a.i.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String l = MicroAppPresenter.v;
                local.d(l, "transform from selected microApp to category currentCategory=" + str + " compsCategories=" + microApp.getCategories());
                ArrayList<String> categories = microApp.getCategories();
                if (str == null || !categories.contains(str)) {
                    this.a.i.a(categories.get(0));
                } else {
                    this.a.i.a(str);
                }
            }
            return this.a.i;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements cc<List<? extends MicroApp>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public d(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        public final void a(List<MicroApp> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String l = MicroAppPresenter.v;
            StringBuilder sb = new StringBuilder();
            sb.append("onLiveDataChanged microApps by category value=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d(l, sb.toString());
            if (list != null) {
                this.a.t.w(list);
                MicroApp a2 = MicroAppPresenter.f(this.a).f().a();
                if (a2 != null) {
                    this.a.t.b(a2);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public e(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<MicroApp>> apply(String str) {
            T t;
            FLogger.INSTANCE.getLocal().d(MicroAppPresenter.v, "transform from category to list microApp with category=" + str);
            HybridCustomizeViewModel f = MicroAppPresenter.f(this.a);
            kd4.a((Object) str, "category");
            List<MicroApp> b = f.b(str);
            ArrayList arrayList = new ArrayList();
            HybridPreset a2 = MicroAppPresenter.f(this.a).c().a();
            MicroApp a3 = MicroAppPresenter.f(this.a).f().a();
            if (a3 != null) {
                String id = a3.getId();
                if (a2 != null) {
                    for (MicroApp next : b) {
                        Iterator<T> it = a2.getButtons().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it.next();
                            HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t;
                            boolean z = true;
                            if (!kd4.a((Object) hybridPresetAppSetting.getAppId(), (Object) next.getId()) || !(!kd4.a((Object) hybridPresetAppSetting.getAppId(), (Object) id))) {
                                z = false;
                                continue;
                            }
                            if (z) {
                                break;
                            }
                        }
                        if (((HybridPresetAppSetting) t) == null || kd4.a((Object) next.getId(), (Object) "empty")) {
                            arrayList.add(next);
                        }
                    }
                }
                this.a.j.a(arrayList);
                return this.a.j;
            }
            kd4.a();
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements cc<List<? extends Pair<? extends String, ? extends Boolean>>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public f(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x006e, code lost:
            if (r5.equals(com.portfolio.platform.data.InAppPermission.ACCESS_FINE_LOCATION) == false) goto L_0x00bf;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x008d, code lost:
            if (r5.equals(com.portfolio.platform.data.InAppPermission.LOCATION_SERVICE) != false) goto L_0x008f;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00da  */
        public final void a(List<Pair<String, Boolean>> list) {
            T t;
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (!((Boolean) ((Pair) t).getSecond()).booleanValue()) {
                        break;
                    }
                }
                Pair pair = (Pair) t;
                FLogger.INSTANCE.getLocal().d(MicroAppPresenter.v, "onLiveDataChanged permissionsRequired " + pair + ' ');
                String str = "";
                int i = 0;
                if (pair != null) {
                    String str2 = (String) pair.getFirst();
                    int hashCode = str2.hashCode();
                    if (hashCode != 385352715) {
                        if (hashCode != 564039755) {
                            if (hashCode == 766697727) {
                            }
                        } else if (str2.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                            str = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.background_location_service_general_explain);
                        }
                        u63 k = this.a.t;
                        if (!(list instanceof Collection) || !list.isEmpty()) {
                            for (Pair second : list) {
                                if (((Boolean) second.getSecond()).booleanValue()) {
                                    i++;
                                    if (i < 0) {
                                        cb4.b();
                                        throw null;
                                    }
                                }
                            }
                        }
                        int size = list.size();
                        String a2 = ns3.a.a((String) pair.getFirst());
                        kd4.a((Object) str, "content");
                        k.a(i, size, a2, str);
                        return;
                    }
                    pd4 pd4 = pd4.a;
                    String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.___Text__TurnOnLocationServicesToAllow);
                    kd4.a((Object) a3, "LanguageHelper.getString\u2026nLocationServicesToAllow)");
                    Object[] objArr = {PortfolioApp.W.c().i()};
                    str = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                    kd4.a((Object) str, "java.lang.String.format(format, *args)");
                    u63 k2 = this.a.t;
                    while (r5.hasNext()) {
                    }
                    int size2 = list.size();
                    String a22 = ns3.a.a((String) pair.getFirst());
                    kd4.a((Object) str, "content");
                    k2.a(i, size2, a22, str);
                    return;
                }
                this.a.t.a(0, 0, str, str);
                MicroApp a4 = MicroAppPresenter.f(this.a).f().a();
                if (a4 != null) {
                    u63 k3 = this.a.t;
                    String a5 = sm2.a(PortfolioApp.W.c(), a4.getDescriptionKey(), a4.getDescription());
                    kd4.a((Object) a5, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    k3.z(a5);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements cc<Triple<? extends String, ? extends Boolean, ? extends Parcelable>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public g(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        public final void a(Triple<String, Boolean, ? extends Parcelable> triple) {
            String str;
            if (triple != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String l = MicroAppPresenter.v;
                local.d(l, "onLiveDataChanged setting of " + triple.getFirst() + " isSettingRequired " + triple.getSecond().booleanValue() + " setting " + ((Parcelable) triple.getThird()));
                boolean booleanValue = triple.getSecond().booleanValue();
                MicroApp a2 = MicroAppPresenter.f(this.a).f().a();
                if (kd4.a((Object) a2 != null ? a2.getId() : null, (Object) triple.getFirst())) {
                    String str2 = "";
                    if (booleanValue) {
                        Parcelable parcelable = (Parcelable) triple.getThird();
                        if (parcelable == null) {
                            String str3 = str2;
                            str2 = gl2.c.a(triple.getFirst());
                            str = str3;
                        } else {
                            try {
                                String first = triple.getFirst();
                                if (kd4.a((Object) first, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                                    str = ((CommuteTimeSetting) parcelable).getAddress();
                                } else if (kd4.a((Object) first, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                                    str = ((SecondTimezoneSetting) parcelable).getTimeZoneName();
                                } else {
                                    if (kd4.a((Object) first, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                                        str = ((Ringtone) parcelable).getRingtoneName();
                                    }
                                    str = str2;
                                }
                            } catch (Exception e) {
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                String l2 = MicroAppPresenter.v;
                                local2.d(l2, "exception when parse micro app setting " + e);
                            }
                        }
                        this.a.t.a(true, triple.getFirst(), str2, str);
                        return;
                    }
                    this.a.t.a(false, triple.getFirst(), str2, (String) null);
                    if (a2 != null) {
                        u63 k = this.a.t;
                        String a3 = sm2.a(PortfolioApp.W.c(), a2.getDescriptionKey(), a2.getDescription());
                        kd4.a((Object) a3, "LanguageHelper.getString\u2026ptionKey, it.description)");
                        k.z(a3);
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements cc<MicroApp> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public h(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        public final void a(MicroApp microApp) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String l = MicroAppPresenter.v;
            local.d(l, "onLiveDataChanged selectedMicroApp value=" + microApp);
            if (microApp != null) {
                this.a.h.a(microApp);
            }
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = MicroAppPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "MicroAppPresenter::class.java.simpleName");
        v = simpleName;
    }
    */

    @DexIgnore
    public MicroAppPresenter(u63 u63, CategoryRepository categoryRepository) {
        kd4.b(u63, "mView");
        kd4.b(categoryRepository, "mCategoryRepository");
        this.t = u63;
        this.u = categoryRepository;
        LiveData<String> b2 = hc.b(this.h, new c(this));
        kd4.a((Object) b2, "Transformations.switchMa\u2026tedMicroAppLiveData\n    }");
        this.m = b2;
        this.n = new b(this);
        LiveData<List<MicroApp>> b3 = hc.b(this.m, new e(this));
        kd4.a((Object) b3, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.o = b3;
        this.p = new d(this);
        LiveData<List<Pair<String, Boolean>>> b4 = hc.b(this.h, new MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1(this));
        kd4.a((Object) b4, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.q = b4;
        this.r = new f(this);
        this.s = new g(this);
    }

    @DexIgnore
    public static final /* synthetic */ HybridCustomizeViewModel f(MicroAppPresenter microAppPresenter) {
        HybridCustomizeViewModel hybridCustomizeViewModel = microAppPresenter.f;
        if (hybridCustomizeViewModel != null) {
            return hybridCustomizeViewModel;
        }
        kd4.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final fi4 b(String str) {
        return ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new MicroAppPresenter$checkSettingOfSelectedMicroApp$Anon1(this, str, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(v, "onStart");
        if (this.g.isEmpty()) {
            fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new MicroAppPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
        } else {
            this.t.a(this.g);
        }
        HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
        if (hybridCustomizeViewModel != null) {
            LiveData<MicroApp> f2 = hybridCustomizeViewModel.f();
            u63 u63 = this.t;
            if (u63 != null) {
                f2.a((g63) u63, new h(this));
                this.m.a(this.n);
                this.o.a(this.p);
                this.q.a(this.r);
                this.l.a(this.s);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.MicroAppFragment");
        }
        kd4.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void g() {
        HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
        if (hybridCustomizeViewModel != null) {
            LiveData<MicroApp> f2 = hybridCustomizeViewModel.f();
            u63 u63 = this.t;
            if (u63 != null) {
                f2.a((LifecycleOwner) (g63) u63);
                this.j.b(this.p);
                this.i.b(this.n);
                this.k.b(this.r);
                this.l.b(this.s);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.MicroAppFragment");
        }
        kd4.d("mHybridCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003e, code lost:
        if (r3 != null) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006d, code lost:
        if (r4 != null) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x009b, code lost:
        if (r0 != null) goto L_0x009f;
     */
    @DexIgnore
    public void h() {
        T t2;
        String str;
        T t3;
        String str2;
        String str3;
        HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
        T t4 = null;
        if (hybridCustomizeViewModel != null) {
            HybridPreset a2 = hybridCustomizeViewModel.c().a();
            if (a2 != null) {
                Iterator<T> it = a2.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (kd4.a((Object) ((HybridPresetAppSetting) t2).getPosition(), (Object) ViewHierarchy.DIMENSION_TOP_KEY)) {
                        break;
                    }
                }
                HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t2;
                if (hybridPresetAppSetting != null) {
                    str = hybridPresetAppSetting.getAppId();
                }
                str = "empty";
                Iterator<T> it2 = a2.getButtons().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t3 = null;
                        break;
                    }
                    t3 = it2.next();
                    if (kd4.a((Object) ((HybridPresetAppSetting) t3).getPosition(), (Object) "middle")) {
                        break;
                    }
                }
                HybridPresetAppSetting hybridPresetAppSetting2 = (HybridPresetAppSetting) t3;
                if (hybridPresetAppSetting2 != null) {
                    str2 = hybridPresetAppSetting2.getAppId();
                }
                str2 = "empty";
                Iterator<T> it3 = a2.getButtons().iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    }
                    T next = it3.next();
                    if (kd4.a((Object) ((HybridPresetAppSetting) next).getPosition(), (Object) "bottom")) {
                        t4 = next;
                        break;
                    }
                }
                HybridPresetAppSetting hybridPresetAppSetting3 = (HybridPresetAppSetting) t4;
                if (hybridPresetAppSetting3 != null) {
                    str3 = hybridPresetAppSetting3.getAppId();
                }
                str3 = "empty";
                this.t.a(str, str2, str3);
                return;
            }
            this.t.a("empty", "empty", "empty");
            return;
        }
        kd4.d("mHybridCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0051, code lost:
        if (r1 != null) goto L_0x0056;
     */
    @DexIgnore
    public void i() {
        String str;
        HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
        T t2 = null;
        if (hybridCustomizeViewModel != null) {
            MicroApp a2 = hybridCustomizeViewModel.f().a();
            if (a2 != null) {
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.f;
                if (hybridCustomizeViewModel2 != null) {
                    HybridPreset a3 = hybridCustomizeViewModel2.c().a();
                    if (a3 != null) {
                        ArrayList<HybridPresetAppSetting> buttons = a3.getButtons();
                        if (buttons != null) {
                            Iterator<T> it = buttons.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                T next = it.next();
                                if (kd4.a((Object) a2.getId(), (Object) ((HybridPresetAppSetting) next).getAppId())) {
                                    t2 = next;
                                    break;
                                }
                            }
                            HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t2;
                            if (hybridPresetAppSetting != null) {
                                str = hybridPresetAppSetting.getSettings();
                            }
                        }
                    }
                    str = "";
                    String id = a2.getId();
                    if (kd4.a((Object) id, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                        this.t.b(str);
                    } else if (kd4.a((Object) id, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                        this.t.g(str);
                    } else if (kd4.a((Object) id, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                        this.t.F(str);
                    }
                } else {
                    kd4.d("mHybridCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            kd4.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void j() {
        Object obj;
        List a2 = this.k.a();
        if (a2 != null) {
            Iterator it = a2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (!((Boolean) ((Pair) obj).getSecond()).booleanValue()) {
                    break;
                }
            }
            Pair pair = (Pair) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = v;
            local.d(str, "processRequiredPermission " + pair);
            if (pair != null) {
                this.t.f((String) pair.getFirst());
            }
        }
    }

    @DexIgnore
    public void k() {
        this.t.a(this);
    }

    @DexIgnore
    public void a(HybridCustomizeViewModel hybridCustomizeViewModel) {
        kd4.b(hybridCustomizeViewModel, "viewModel");
        this.f = hybridCustomizeViewModel;
    }

    @DexIgnore
    public void a(Category category) {
        kd4.b(category, "category");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = v;
        local.d(str, "category change " + category);
        this.i.a(category.getId());
    }

    @DexIgnore
    public void a(String str) {
        T t2;
        kd4.b(str, "newMicroAppId");
        HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
        if (hybridCustomizeViewModel != null) {
            MicroApp c2 = hybridCustomizeViewModel.c(str);
            FLogger.INSTANCE.getLocal().d(v, "onUserChooseMicroApp " + c2);
            if (c2 != null) {
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.f;
                if (hybridCustomizeViewModel2 != null) {
                    HybridPreset a2 = hybridCustomizeViewModel2.c().a();
                    if (a2 != null) {
                        HybridPreset clone = a2.clone();
                        ArrayList arrayList = new ArrayList();
                        HybridCustomizeViewModel hybridCustomizeViewModel3 = this.f;
                        if (hybridCustomizeViewModel3 != null) {
                            String a3 = hybridCustomizeViewModel3.g().a();
                            if (a3 != null) {
                                kd4.a((Object) a3, "mHybridCustomizeViewMode\u2026ctedMicroAppPos().value!!");
                                String str2 = a3;
                                HybridCustomizeViewModel hybridCustomizeViewModel4 = this.f;
                                if (hybridCustomizeViewModel4 == null) {
                                    kd4.d("mHybridCustomizeViewModel");
                                    throw null;
                                } else if (!hybridCustomizeViewModel4.e(str)) {
                                    Iterator<HybridPresetAppSetting> it = clone.getButtons().iterator();
                                    while (it.hasNext()) {
                                        HybridPresetAppSetting next = it.next();
                                        if (kd4.a((Object) next.getPosition(), (Object) str2)) {
                                            arrayList.add(new HybridPresetAppSetting(str2, str, next.getLocalUpdateAt()));
                                        } else {
                                            arrayList.add(next);
                                        }
                                    }
                                    clone.getButtons().clear();
                                    clone.getButtons().addAll(arrayList);
                                    FLogger.INSTANCE.getLocal().d(v, "Update current preset=" + clone);
                                    HybridCustomizeViewModel hybridCustomizeViewModel5 = this.f;
                                    if (hybridCustomizeViewModel5 != null) {
                                        hybridCustomizeViewModel5.a(clone);
                                    } else {
                                        kd4.d("mHybridCustomizeViewModel");
                                        throw null;
                                    }
                                } else {
                                    Iterator<T> it2 = a2.getButtons().iterator();
                                    while (true) {
                                        if (!it2.hasNext()) {
                                            t2 = null;
                                            break;
                                        }
                                        t2 = it2.next();
                                        if (kd4.a((Object) ((HybridPresetAppSetting) t2).getAppId(), (Object) str)) {
                                            break;
                                        }
                                    }
                                    HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t2;
                                    if (hybridPresetAppSetting != null) {
                                        HybridCustomizeViewModel hybridCustomizeViewModel6 = this.f;
                                        if (hybridCustomizeViewModel6 != null) {
                                            hybridCustomizeViewModel6.f(hybridPresetAppSetting.getPosition());
                                        } else {
                                            kd4.d("mHybridCustomizeViewModel");
                                            throw null;
                                        }
                                    }
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.d("mHybridCustomizeViewModel");
                            throw null;
                        }
                    }
                } else {
                    kd4.d("mHybridCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            kd4.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        T t2;
        kd4.b(str, "microAppId");
        kd4.b(str2, MicroAppSetting.SETTING);
        HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
        if (hybridCustomizeViewModel != null) {
            HybridPreset a2 = hybridCustomizeViewModel.c().a();
            if (a2 != null) {
                HybridPreset clone = a2.clone();
                Iterator<T> it = clone.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (kd4.a((Object) ((HybridPresetAppSetting) t2).getAppId(), (Object) str)) {
                        break;
                    }
                }
                HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t2;
                if (hybridPresetAppSetting != null) {
                    hybridPresetAppSetting.setSettings(str2);
                }
                FLogger.INSTANCE.getLocal().d(v, "update new setting " + str2 + " of " + str);
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.f;
                if (hybridCustomizeViewModel2 != null) {
                    hybridCustomizeViewModel2.a(clone);
                } else {
                    kd4.d("mHybridCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            kd4.d("mHybridCustomizeViewModel");
            throw null;
        }
    }
}
