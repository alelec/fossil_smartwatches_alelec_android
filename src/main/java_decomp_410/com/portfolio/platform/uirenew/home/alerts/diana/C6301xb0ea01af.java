package com.portfolio.platform.uirenew.home.alerts.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1", mo27670f = "HomeAlertsPresenter.kt", mo27671l = {353, 359, 368, 376}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1 */
public final class C6301xb0ea01af extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<? extends com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter>>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22324p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1 this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$1", mo27670f = "HomeAlertsPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$1 */
    public static final class C63021 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $notificationSettings;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22325p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$1$a")
        /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$1$a */
        public static final class C6303a implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af.C63021 f22326e;

            @DexIgnore
            public C6303a(com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af.C63021 r1) {
                this.f22326e = r1;
            }

            @DexIgnore
            public final void run() {
                this.f22326e.this$0.this$0.this$0.f22313r.getNotificationSettingsDao().insertListNotificationSettings(this.f22326e.$notificationSettings);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C63021(com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af homeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = homeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1;
            this.$notificationSettings = list;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af.C63021 r0 = new com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af.C63021(this.this$0, this.$notificationSettings, yb4);
            r0.f22325p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af.C63021) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.this$0.f22313r.runInTransaction((java.lang.Runnable) new com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af.C63021.C6303a(this));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$2")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$2", mo27670f = "HomeAlertsPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$2 */
    public static final class C63042 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $contactMessageAppFilters;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $listNotificationSettings;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22327p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C63042(com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af homeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1, java.util.List list, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = homeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1;
            this.$listNotificationSettings = list;
            this.$contactMessageAppFilters = ref$ObjectRef;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af.C63042 r0 = new com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af.C63042(this.this$0, this.$listNotificationSettings, this.$contactMessageAppFilters, yb4);
            r0.f22327p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af.C63042) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                for (com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel : this.$listNotificationSettings) {
                    int component2 = notificationSettingsModel.component2();
                    int i = 0;
                    if (notificationSettingsModel.component3()) {
                        java.lang.String a = this.this$0.this$0.this$0.mo40759a(component2);
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a2 = com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a();
                        local.mo33255d(a2, "CALL settingsTypeName=" + a);
                        if (component2 == 0) {
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String a3 = com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a();
                            local2.mo33255d(a3, "mListAppNotificationFilter add - " + com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL);
                            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                            com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName.getAppName(), aApplicationName.getBundleCrc(), aApplicationName.getGroupId(), aApplicationName.getPackageName(), aApplicationName.getIconResourceId(), aApplicationName.getIconFwPath(), aApplicationName.getNotificationType());
                            ((java.util.List) this.$contactMessageAppFilters.element).add(new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification));
                        } else if (component2 == 1) {
                            int size = this.this$0.this$0.this$0.f22306k.size();
                            int i2 = 0;
                            while (i2 < size) {
                                com.fossil.wearables.fsl.contact.ContactGroup contactGroup = (com.fossil.wearables.fsl.contact.ContactGroup) this.this$0.this$0.this$0.f22306k.get(i2);
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                java.lang.String a4 = com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a();
                                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                                sb.append("mListAppNotificationFilter add PHONE item - ");
                                sb.append(i2);
                                sb.append(" name = ");
                                com.fossil.wearables.fsl.contact.Contact contact = contactGroup.getContacts().get(i);
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact, "item.contacts[0]");
                                sb.append(contact.getDisplayName());
                                local3.mo33255d(a4, sb.toString());
                                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName2 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification2 = r11;
                                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification3 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName2.getAppName(), aApplicationName2.getBundleCrc(), aApplicationName2.getGroupId(), aApplicationName2.getPackageName(), aApplicationName2.getIconResourceId(), aApplicationName2.getIconFwPath(), aApplicationName2.getNotificationType());
                                java.util.List<com.fossil.wearables.fsl.contact.Contact> contacts = contactGroup.getContacts();
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contacts, "item.contacts");
                                if (!contacts.isEmpty()) {
                                    com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter appNotificationFilter = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification2);
                                    com.fossil.wearables.fsl.contact.Contact contact2 = contactGroup.getContacts().get(0);
                                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact2, "item.contacts[0]");
                                    appNotificationFilter.setSender(contact2.getDisplayName());
                                    ((java.util.List) this.$contactMessageAppFilters.element).add(appNotificationFilter);
                                }
                                i2++;
                                i = 0;
                            }
                        }
                    } else {
                        java.lang.String a5 = this.this$0.this$0.this$0.mo40759a(component2);
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a6 = com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a();
                        local4.mo33255d(a6, "MESSAGE settingsTypeName=" + a5);
                        if (component2 == 0) {
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String a7 = com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a();
                            local5.mo33255d(a7, "mListAppNotificationFilter add - " + com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.MESSAGES);
                            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName3 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.MESSAGES;
                            com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification4 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName3.getAppName(), aApplicationName3.getBundleCrc(), aApplicationName3.getGroupId(), aApplicationName3.getPackageName(), aApplicationName3.getIconResourceId(), aApplicationName3.getIconFwPath(), aApplicationName3.getNotificationType());
                            ((java.util.List) this.$contactMessageAppFilters.element).add(new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification4));
                        } else if (component2 == 1) {
                            int size2 = this.this$0.this$0.this$0.f22306k.size();
                            for (int i3 = 0; i3 < size2; i3++) {
                                com.fossil.wearables.fsl.contact.ContactGroup contactGroup2 = (com.fossil.wearables.fsl.contact.ContactGroup) this.this$0.this$0.this$0.f22306k.get(i3);
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                java.lang.String a8 = com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a();
                                java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
                                sb2.append("mListAppNotificationFilter add MESSAGE item - ");
                                sb2.append(i3);
                                sb2.append(" name = ");
                                com.fossil.wearables.fsl.contact.Contact contact3 = contactGroup2.getContacts().get(0);
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact3, "item.contacts[0]");
                                sb2.append(contact3.getDisplayName());
                                local6.mo33255d(a8, sb2.toString());
                                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName4 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.MESSAGES;
                                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification5 = r10;
                                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification6 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName4.getAppName(), aApplicationName4.getBundleCrc(), aApplicationName4.getGroupId(), aApplicationName4.getPackageName(), aApplicationName4.getIconResourceId(), aApplicationName4.getIconFwPath(), aApplicationName4.getNotificationType());
                                java.util.List<com.fossil.wearables.fsl.contact.Contact> contacts2 = contactGroup2.getContacts();
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contacts2, "item.contacts");
                                if (!contacts2.isEmpty()) {
                                    com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter appNotificationFilter2 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification5);
                                    com.fossil.wearables.fsl.contact.Contact contact4 = contactGroup2.getContacts().get(0);
                                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact4, "item.contacts[0]");
                                    appNotificationFilter2.setSender(contact4.getDisplayName());
                                    ((java.util.List) this.$contactMessageAppFilters.element).add(appNotificationFilter2);
                                }
                            }
                        }
                    }
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6301xb0ea01af(com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1 homeAlertsPresenter$setNotificationFilterToDevice$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homeAlertsPresenter$setNotificationFilterToDevice$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af homeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1 = new com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af(this.this$0, yb4);
        homeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1.f22324p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x013b  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        com.portfolio.platform.CoroutineUseCase.C5604c cVar;
        java.util.List list;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f22324p$;
            ref$ObjectRef = new kotlin.jvm.internal.Ref$ObjectRef();
            ref$ObjectRef.element = null;
            com.fossil.blesdk.obfuscated.px2 g = this.this$0.this$0.f22311p;
            this.L$0 = zg42;
            this.L$1 = ref$ObjectRef;
            this.label = 1;
            java.lang.Object a2 = com.fossil.blesdk.obfuscated.w52.m29539a(g, null, this);
            if (a2 == a) {
                return a;
            }
            java.lang.Object obj2 = a2;
            zg4 = zg42;
            obj = obj2;
        } else if (i == 1) {
            ref$ObjectRef = (kotlin.jvm.internal.Ref$ObjectRef) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i != 2) {
            if (i == 3) {
                com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel = (com.portfolio.platform.data.model.NotificationSettingsModel) this.L$6;
                com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel2 = (com.portfolio.platform.data.model.NotificationSettingsModel) this.L$5;
                java.util.List list2 = (java.util.List) this.L$4;
            } else if (i != 4) {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            java.util.List list3 = (java.util.List) this.L$3;
            com.portfolio.platform.CoroutineUseCase.C5604c cVar2 = (com.portfolio.platform.CoroutineUseCase.C5604c) this.L$2;
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            ref$ObjectRef = (kotlin.jvm.internal.Ref$ObjectRef) this.L$1;
            return (java.util.List) ref$ObjectRef.element;
        } else {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2 = (kotlin.jvm.internal.Ref$ObjectRef) this.L$1;
            cVar = (com.portfolio.platform.CoroutineUseCase.C5604c) this.L$2;
            ref$ObjectRef = ref$ObjectRef2;
            list = (java.util.List) obj;
            if (!list.isEmpty()) {
                java.util.ArrayList arrayList = new java.util.ArrayList();
                com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel3 = new com.portfolio.platform.data.model.NotificationSettingsModel("AllowCallsFrom", 0, true);
                com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel4 = new com.portfolio.platform.data.model.NotificationSettingsModel("AllowMessagesFrom", 0, false);
                arrayList.add(notificationSettingsModel3);
                arrayList.add(notificationSettingsModel4);
                com.fossil.blesdk.obfuscated.ug4 c = this.this$0.this$0.mo31441c();
                com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af.C63021 r10 = new com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af.C63021(this, arrayList, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = ref$ObjectRef;
                this.L$2 = cVar;
                this.L$3 = list;
                this.L$4 = arrayList;
                this.L$5 = notificationSettingsModel3;
                this.L$6 = notificationSettingsModel4;
                this.label = 3;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r10, this) == a) {
                    return a;
                }
            } else {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a3 = com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a();
                local.mo33255d(a3, "listNotificationSettings.size = " + list.size());
                com.fossil.blesdk.obfuscated.ug4 a4 = this.this$0.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af.C63042 r5 = new com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af.C63042(this, list, ref$ObjectRef, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = ref$ObjectRef;
                this.L$2 = cVar;
                this.L$3 = list;
                this.label = 4;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(a4, r5, this) == a) {
                    return a;
                }
            }
            return (java.util.List) ref$ObjectRef.element;
        }
        com.portfolio.platform.CoroutineUseCase.C5604c cVar3 = (com.portfolio.platform.CoroutineUseCase.C5604c) obj;
        if (cVar3 instanceof com.fossil.blesdk.obfuscated.px2.C4879d) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a5 = com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("GetAllContactGroup onSuccess, size = ");
            com.fossil.blesdk.obfuscated.px2.C4879d dVar = (com.fossil.blesdk.obfuscated.px2.C4879d) cVar3;
            sb.append(dVar.mo30277a().size());
            local2.mo33255d(a5, sb.toString());
            ref$ObjectRef.element = new java.util.ArrayList();
            this.this$0.this$0.f22306k.clear();
            this.this$0.this$0.f22306k = com.fossil.blesdk.obfuscated.kb4.m24381d(dVar.mo30277a());
            com.fossil.blesdk.obfuscated.ug4 c2 = this.this$0.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.alerts.diana.C6305xc492758e homeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$listNotificationSettings$1 = new com.portfolio.platform.uirenew.home.alerts.diana.C6305xc492758e(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = ref$ObjectRef;
            this.L$2 = cVar3;
            this.label = 2;
            java.lang.Object a6 = com.fossil.blesdk.obfuscated.yf4.m30997a(c2, homeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$listNotificationSettings$1, this);
            if (a6 == a) {
                return a;
            }
            java.lang.Object obj3 = a6;
            cVar = cVar3;
            obj = obj3;
            list = (java.util.List) obj;
            if (!list.isEmpty()) {
            }
            return (java.util.List) ref$ObjectRef.element;
        }
        if (cVar3 instanceof com.fossil.blesdk.obfuscated.px2.C4877b) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a(), "GetAllContactGroup onError");
        }
        return (java.util.List) ref$ObjectRef.element;
    }
}
