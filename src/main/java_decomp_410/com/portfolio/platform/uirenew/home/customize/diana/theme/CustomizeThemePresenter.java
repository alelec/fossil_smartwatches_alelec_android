package com.portfolio.platform.uirenew.home.customize.diana.theme;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.i43;
import com.fossil.blesdk.obfuscated.j43;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomizeThemePresenter extends i43 {
    @DexIgnore
    public DianaCustomizeViewModel f;
    @DexIgnore
    public LiveData<List<WatchFace>> g;
    @DexIgnore
    public List<WatchFace> h; // = new ArrayList();
    @DexIgnore
    public /* final */ j43 i;
    @DexIgnore
    public /* final */ WatchFaceRepository j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemePresenter a;

        @DexIgnore
        public b(CustomizeThemePresenter customizeThemePresenter) {
            this.a = customizeThemePresenter;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeThemePresenter", "onLiveDataChanged SelectedCustomizeTheme value=" + str);
            if (str != null) {
                this.a.i.n(CustomizeThemePresenter.b(this.a).k().indexOf(CustomizeThemePresenter.b(this.a).f(str)));
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public CustomizeThemePresenter(j43 j43, WatchFaceRepository watchFaceRepository) {
        kd4.b(j43, "mView");
        kd4.b(watchFaceRepository, "watchFaceRepository");
        this.i = j43;
        this.j = watchFaceRepository;
    }

    @DexIgnore
    public static final /* synthetic */ DianaCustomizeViewModel b(CustomizeThemePresenter customizeThemePresenter) {
        DianaCustomizeViewModel dianaCustomizeViewModel = customizeThemePresenter.f;
        if (dianaCustomizeViewModel != null) {
            return dianaCustomizeViewModel;
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void g() {
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            MutableLiveData<String> h2 = dianaCustomizeViewModel.h();
            j43 j43 = this.i;
            if (j43 != null) {
                h2.a((LifecycleOwner) (CustomizeThemeFragment) j43);
                LiveData<List<WatchFace>> liveData = this.g;
                if (liveData != null) {
                    liveData.a((LifecycleOwner) this.i);
                    return;
                }
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment");
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void h() {
        this.i.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "onStart");
        fi4 unused = ag4.b(e(), nh4.b(), (CoroutineStart) null, new CustomizeThemePresenter$start$Anon1(this, (yb4) null), 2, (Object) null);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            MutableLiveData<String> h2 = dianaCustomizeViewModel.h();
            j43 j43 = this.i;
            if (j43 != null) {
                h2.a((CustomizeThemeFragment) j43, new b(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment");
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void a(WatchFaceWrapper watchFaceWrapper) {
        kd4.b(watchFaceWrapper, "watchFaceWrapper");
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            DianaPreset a2 = dianaCustomizeViewModel.c().a();
            if (a2 != null) {
                a2.setWatchFaceId(watchFaceWrapper.getId());
                kd4.a((Object) a2, "currentPreset");
                a(a2);
                return;
            }
            return;
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final void a(DianaPreset dianaPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "updateCurrentPreset=" + dianaPreset);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            dianaCustomizeViewModel.a(dianaPreset);
        } else {
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(DianaCustomizeViewModel dianaCustomizeViewModel) {
        kd4.b(dianaCustomizeViewModel, "viewModel");
        this.f = dianaCustomizeViewModel;
    }
}
