package com.portfolio.platform.uirenew.home.customize.hybrid.search;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import androidx.fragment.app.Fragment;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.c73;
import com.fossil.blesdk.obfuscated.d73;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SearchMicroAppActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public SearchMicroAppPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, String str2, String str3) {
            kd4.b(fragment, "fragment");
            kd4.b(str, "watchAppTop");
            kd4.b(str2, "watchAppMiddle");
            kd4.b(str3, "watchAppBottom");
            Intent intent = new Intent(fragment.getContext(), SearchMicroAppActivity.class);
            intent.putExtra(ViewHierarchy.DIMENSION_TOP_KEY, str);
            intent.putExtra("middle", str2);
            intent.putExtra("bottom", str3);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 102, ActivityOptions.makeSceneTransitionAnimation(fragment.getActivity(), new Pair[0]).toBundle());
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        c73 c73 = (c73) getSupportFragmentManager().a((int) R.id.content);
        if (c73 == null) {
            c73 = c73.o.b();
            a((Fragment) c73, c73.o.a(), (int) R.id.content);
        }
        l42 g = PortfolioApp.W.c().g();
        if (c73 != null) {
            g.a(new d73(c73)).a(this);
            Intent intent = getIntent();
            kd4.a((Object) intent, "intent");
            Bundle extras = intent.getExtras();
            if (extras != null) {
                SearchMicroAppPresenter searchMicroAppPresenter = this.B;
                if (searchMicroAppPresenter != null) {
                    String string = extras.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string == null) {
                        string = "empty";
                    }
                    String string2 = extras.getString("middle");
                    if (string2 == null) {
                        string2 = "empty";
                    }
                    String string3 = extras.getString("bottom");
                    if (string3 == null) {
                        string3 = "empty";
                    }
                    searchMicroAppPresenter.a(string, string2, string3);
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
            if (bundle != null) {
                SearchMicroAppPresenter searchMicroAppPresenter2 = this.B;
                if (searchMicroAppPresenter2 != null) {
                    String string4 = bundle.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string4 == null) {
                        string4 = "empty";
                    }
                    String string5 = bundle.getString("middle");
                    if (string5 == null) {
                        string5 = "empty";
                    }
                    String string6 = bundle.getString("bottom");
                    if (string6 == null) {
                        string6 = "empty";
                    }
                    searchMicroAppPresenter2.a(string4, string5, string6);
                    return;
                }
                kd4.d("mPresenter");
                throw null;
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppContract.View");
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        SearchMicroAppPresenter searchMicroAppPresenter = this.B;
        if (searchMicroAppPresenter != null) {
            searchMicroAppPresenter.a(bundle);
            super.onSaveInstanceState(bundle);
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }
}
