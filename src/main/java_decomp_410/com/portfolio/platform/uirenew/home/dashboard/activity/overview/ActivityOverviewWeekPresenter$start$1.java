package com.portfolio.platform.uirenew.home.dashboard.activity.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1", mo27670f = "ActivityOverviewWeekPresenter.kt", mo27671l = {50}, mo27672m = "invokeSuspend")
public final class ActivityOverviewWeekPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23248p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1$2")
    /* renamed from: com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1$2 */
    public static final class C65992<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1 f23249a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1$2$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1$2$1", mo27670f = "ActivityOverviewWeekPresenter.kt", mo27671l = {62}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1$2$1 */
        public static final class C66001 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ java.util.List $data;
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public java.lang.Object L$1;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f23250p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1.C65992 this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1$2$1$1")
            @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1$2$1$1", mo27670f = "ActivityOverviewWeekPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1$2$1$1 */
            public static final class C66011 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c>, java.lang.Object> {
                @DexIgnore
                public int label;

                @DexIgnore
                /* renamed from: p$ */
                public com.fossil.blesdk.obfuscated.zg4 f23251p$;
                @DexIgnore
                public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1.C65992.C66001 this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C66011(com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1.C65992.C66001 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                    super(2, yb4);
                    this.this$0 = r1;
                }

                @DexIgnore
                public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                    com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                    com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1.C65992.C66001.C66011 r0 = new com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1.C65992.C66001.C66011(this.this$0, yb4);
                    r0.f23251p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                    return r0;
                }

                @DexIgnore
                public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                    return ((com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1.C65992.C66001.C66011) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
                }

                @DexIgnore
                public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                    com.fossil.blesdk.obfuscated.cc4.m20546a();
                    if (this.label == 0) {
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter activityOverviewWeekPresenter = this.this$0.this$0.f23249a.this$0;
                        return activityOverviewWeekPresenter.mo41254a(com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter.m34611e(activityOverviewWeekPresenter), (java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>) this.this$0.$data);
                    }
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C66001(com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1.C65992 r1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$data = list;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1.C65992.C66001 r0 = new com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1.C65992.C66001(this.this$0, this.$data, yb4);
                r0.f23250p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1.C65992.C66001) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter activityOverviewWeekPresenter;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23250p$;
                    com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter activityOverviewWeekPresenter2 = this.this$0.f23249a.this$0;
                    com.fossil.blesdk.obfuscated.ug4 a2 = activityOverviewWeekPresenter2.mo31440b();
                    com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1.C65992.C66001.C66011 r4 = new com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1.C65992.C66001.C66011(this, (com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg4;
                    this.L$1 = activityOverviewWeekPresenter2;
                    this.label = 1;
                    obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r4, this);
                    if (obj == a) {
                        return a;
                    }
                    activityOverviewWeekPresenter = activityOverviewWeekPresenter2;
                } else if (i == 1) {
                    activityOverviewWeekPresenter = (com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter) this.L$1;
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                activityOverviewWeekPresenter.f23244h = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c) obj;
                com.fossil.blesdk.obfuscated.u93 g = this.this$0.f23249a.this$0.f23245i;
                com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c c = this.this$0.f23249a.this$0.f23244h;
                if (c == null) {
                    c = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c(0, 0, (java.util.ArrayList) null, 7, (com.fossil.blesdk.obfuscated.fd4) null);
                }
                g.mo31463a(c);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        public C65992(com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1 activityOverviewWeekPresenter$start$1) {
            this.f23249a = activityOverviewWeekPresenter$start$1;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>> os3) {
            com.portfolio.platform.enums.Status a = os3.mo29972a();
            java.util.List list = (java.util.List) os3.mo29973b();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("start - mActivitySummaries -- activitySummaries=");
            sb.append(list != null ? java.lang.Integer.valueOf(list.size()) : null);
            local.mo33255d("ActivityOverviewWeekPresenter", sb.toString());
            if (a != com.portfolio.platform.enums.Status.DATABASE_LOADING) {
                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23249a.this$0.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1.C65992.C66001(this, list, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityOverviewWeekPresenter$start$1(com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter activityOverviewWeekPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = activityOverviewWeekPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1 activityOverviewWeekPresenter$start$1 = new com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1(this.this$0, yb4);
        activityOverviewWeekPresenter$start$1.f23248p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return activityOverviewWeekPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00e0  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.u93 g;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23248p$;
            if (this.this$0.f23242f == null || !com.fossil.blesdk.obfuscated.rk2.m27414s(com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter.m34611e(this.this$0)).booleanValue()) {
                this.this$0.f23242f = new java.util.Date();
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1$startAndEnd$1 activityOverviewWeekPresenter$start$1$startAndEnd$1 = new com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1$startAndEnd$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, activityOverviewWeekPresenter$start$1$startAndEnd$1, this);
                if (obj == a) {
                    return a;
                }
            }
            androidx.lifecycle.LiveData b = this.this$0.f23243g;
            g = this.this$0.f23245i;
            if (g == null) {
                b.mo2277a((com.fossil.blesdk.obfuscated.v93) g, new com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1.C65992(this));
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local.mo33255d("ActivityOverviewWeekPresenter", "start - mDate=" + com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter.m34611e(this.this$0));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekFragment");
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        kotlin.Pair pair = (kotlin.Pair) obj;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local2.mo33255d("ActivityOverviewWeekPresenter", "start - startDate=" + ((java.util.Date) pair.getFirst()) + ", endDate=" + ((java.util.Date) pair.getSecond()));
        com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter activityOverviewWeekPresenter = this.this$0;
        activityOverviewWeekPresenter.f23243g = activityOverviewWeekPresenter.f23247k.getSummaries((java.util.Date) pair.getFirst(), (java.util.Date) pair.getSecond(), false);
        androidx.lifecycle.LiveData b2 = this.this$0.f23243g;
        g = this.this$0.f23245i;
        if (g == null) {
        }
    }
}
