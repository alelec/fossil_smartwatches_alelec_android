package com.portfolio.platform.uirenew.home.alerts.diana;

import android.content.Context;
import android.text.SpannableString;
import android.text.format.DateFormat;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.bj2;
import com.fossil.blesdk.obfuscated.bn2;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.iw2;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ll2;
import com.fossil.blesdk.obfuscated.ox3;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.rv2;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.sv2;
import com.fossil.blesdk.obfuscated.tv2;
import com.fossil.blesdk.obfuscated.vy2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeAlertsPresenter extends rv2 {
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ a x; // = new a((fd4) null);
    @DexIgnore
    public LiveData<String> f; // = PortfolioApp.W.c().f();
    @DexIgnore
    public /* final */ LiveData<List<DNDScheduledTimeModel>> g; // = this.v.getDNDScheduledTimeDao().getListDNDScheduledTime();
    @DexIgnore
    public ArrayList<Alarm> h; // = new ArrayList<>();
    @DexIgnore
    public boolean i;
    @DexIgnore
    public List<AppWrapper> j; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> k; // = new ArrayList();
    @DexIgnore
    public /* final */ sv2 l;
    @DexIgnore
    public /* final */ j62 m;
    @DexIgnore
    public /* final */ AlarmHelper n;
    @DexIgnore
    public /* final */ vy2 o;
    @DexIgnore
    public /* final */ px2 p;
    @DexIgnore
    public /* final */ iw2 q;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase r;
    @DexIgnore
    public /* final */ SetAlarms s;
    @DexIgnore
    public /* final */ AlarmsRepository t;
    @DexIgnore
    public /* final */ en2 u;
    @DexIgnore
    public /* final */ DNDSettingsDatabase v;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return HomeAlertsPresenter.w;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<SetAlarms.d, SetAlarms.b> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ Alarm b;

        @DexIgnore
        public b(HomeAlertsPresenter homeAlertsPresenter, Alarm alarm) {
            this.a = homeAlertsPresenter;
            this.b = alarm;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetAlarms.d dVar) {
            kd4.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsPresenter.x.a();
            local.d(a2, "enableAlarm - onSuccess: alarmUri = " + dVar.a().getUri() + ", alarmId = " + dVar.a().getId());
            this.a.l.a();
            this.a.b(this.b, true);
        }

        @DexIgnore
        public void a(SetAlarms.b bVar) {
            kd4.b(bVar, "errorValue");
            this.a.l.a();
            int c = bVar.c();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsPresenter.x.a();
            local.d(a2, "enableAlarm() - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.a.l.c();
                } else if (!(c == 1112 || c == 1113)) {
                    this.a.l.w();
                }
                this.a.b(bVar.a(), false);
                return;
            }
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(bVar.b());
            kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            sv2 m = this.a.l;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                m.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                this.a.b(bVar.a(), false);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements i62.d<iw2.c, i62.a> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;

        @DexIgnore
        public c(HomeAlertsPresenter homeAlertsPresenter) {
            this.a = homeAlertsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(iw2.c cVar) {
            FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), ".Inside mSaveAppsNotification onSuccess");
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_Elements_Label__All);
            sv2 m = this.a.l;
            kd4.a((Object) a2, "notificationAppOverView");
            m.h(a2);
            bn2 bn2 = bn2.d;
            sv2 m2 = this.a.l;
            if (m2 == null) {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
            } else if (bn2.a(bn2, ((tv2) m2).getContext(), "NOTIFICATION_APPS", false, 4, (Object) null)) {
                this.a.k();
            }
        }

        @DexIgnore
        public void a(i62.a aVar) {
            FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), ".Inside mSaveAppsNotification onError");
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Sections_without_Device_Profile_Without_Watch_None);
            sv2 m = this.a.l;
            kd4.a((Object) a2, "notificationAppOverView");
            m.h(a2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements cc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;

        @DexIgnore
        public d(HomeAlertsPresenter homeAlertsPresenter) {
            this.a = homeAlertsPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            sv2 unused = this.a.l;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements cc<List<? extends DNDScheduledTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;

        @DexIgnore
        public e(HomeAlertsPresenter homeAlertsPresenter) {
            this.a = homeAlertsPresenter;
        }

        @DexIgnore
        public final void a(List<DNDScheduledTimeModel> list) {
            sv2 unused = this.a.l;
        }
    }

    /*
    static {
        String simpleName = HomeAlertsPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "HomeAlertsPresenter::class.java.simpleName");
        w = simpleName;
    }
    */

    @DexIgnore
    public HomeAlertsPresenter(sv2 sv2, j62 j62, AlarmHelper alarmHelper, vy2 vy2, px2 px2, iw2 iw2, NotificationSettingsDatabase notificationSettingsDatabase, SetAlarms setAlarms, AlarmsRepository alarmsRepository, en2 en2, DNDSettingsDatabase dNDSettingsDatabase) {
        kd4.b(sv2, "mView");
        kd4.b(j62, "mUseCaseHandler");
        kd4.b(alarmHelper, "mAlarmHelper");
        kd4.b(vy2, "mGetApps");
        kd4.b(px2, "mGetAllContactGroup");
        kd4.b(iw2, "mSaveAppsNotification");
        kd4.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        kd4.b(setAlarms, "mSetAlarms");
        kd4.b(alarmsRepository, "mAlarmRepository");
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(dNDSettingsDatabase, "mDNDSettingsDatabase");
        this.l = sv2;
        this.m = j62;
        this.n = alarmHelper;
        this.o = vy2;
        this.p = px2;
        this.q = iw2;
        this.r = notificationSettingsDatabase;
        this.s = setAlarms;
        this.t = alarmsRepository;
        this.u = en2;
        this.v = dNDSettingsDatabase;
    }

    @DexIgnore
    @ox3
    public final void onSetAlarmEventEndComplete(bj2 bj2) {
        if (bj2 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "onSetAlarmEventEndComplete() - event = " + bj2);
            if (bj2.b()) {
                String a2 = bj2.a();
                Iterator<Alarm> it = this.h.iterator();
                while (it.hasNext()) {
                    Alarm next = it.next();
                    if (kd4.a((Object) next.getUri(), (Object) a2)) {
                        next.setActive(false);
                    }
                }
                this.l.s();
            }
        }
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(w, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.s.f();
        PortfolioApp.W.b((Object) this);
        LiveData<String> liveData = this.f;
        sv2 sv2 = this.l;
        if (sv2 != null) {
            liveData.a((tv2) sv2, new HomeAlertsPresenter$start$Anon1(this));
            BleCommandResultManager.d.a(CommunicateMode.SET_LIST_ALARM);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(w, "stop");
        this.f.b(new d(this));
        this.g.b(new e(this));
        this.s.g();
        PortfolioApp.W.c(this);
    }

    @DexIgnore
    public void h() {
        this.i = !this.i;
        this.u.e(this.i);
        this.l.m(this.i);
    }

    @DexIgnore
    public final List<AppWrapper> i() {
        return this.j;
    }

    @DexIgnore
    public final void j() {
        FLogger.INSTANCE.getLocal().d(w, "onSetAlarmsSuccess");
        this.n.c(PortfolioApp.W.c());
        String a2 = this.f.a();
        if (a2 != null) {
            PortfolioApp c2 = PortfolioApp.W.c();
            kd4.a((Object) a2, "it");
            c2.k(a2);
        }
    }

    @DexIgnore
    public final void k() {
        bn2 bn2 = bn2.d;
        sv2 sv2 = this.l;
        if (sv2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
        } else if (bn2.a(bn2, ((tv2) sv2).getContext(), "SET_NOTIFICATION", false, 4, (Object) null)) {
            fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeAlertsPresenter$setNotificationFilterToDevice$Anon1(this, (yb4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void l() {
        this.l.a(this);
    }

    @DexIgnore
    public final void b(Alarm alarm, boolean z) {
        kd4.b(alarm, "editAlarm");
        Iterator<Alarm> it = this.h.iterator();
        while (it.hasNext()) {
            Alarm next = it.next();
            if (kd4.a((Object) next.getUri(), (Object) alarm.getUri())) {
                ArrayList<Alarm> arrayList = this.h;
                ArrayList<Alarm> arrayList2 = arrayList;
                arrayList2.set(arrayList.indexOf(next), Alarm.copy$default(alarm, (String) null, (String) null, (String) null, 0, 0, (int[]) null, false, false, (String) null, (String) null, 0, 2047, (Object) null));
                if (!z) {
                    break;
                }
                j();
            }
            Alarm alarm2 = alarm;
        }
        this.l.d(this.h);
    }

    @DexIgnore
    public final boolean a(List<AppWrapper> list, List<AppWrapper> list2) {
        Boolean bool;
        T t2;
        kd4.b(list, "listDatabaseAppWrapper");
        kd4.b(list2, "listAppWrapper");
        if (list.size() != list2.size()) {
            return true;
        }
        boolean z = false;
        for (AppWrapper appWrapper : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                bool = null;
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                InstalledApp installedApp = ((AppWrapper) t2).getInstalledApp();
                String identifier = installedApp != null ? installedApp.getIdentifier() : null;
                InstalledApp installedApp2 = appWrapper.getInstalledApp();
                if (kd4.a((Object) identifier, (Object) installedApp2 != null ? installedApp2.getIdentifier() : null)) {
                    break;
                }
            }
            AppWrapper appWrapper2 = (AppWrapper) t2;
            if (appWrapper2 != null) {
                InstalledApp installedApp3 = appWrapper2.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                InstalledApp installedApp4 = appWrapper.getInstalledApp();
                if (installedApp4 != null) {
                    bool = installedApp4.isSelected();
                }
                if (!(!kd4.a((Object) isSelected, (Object) bool))) {
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final SpannableString b(int i2) {
        int i3 = i2 / 60;
        int i4 = i2 % 60;
        if (DateFormat.is24HourFormat(PortfolioApp.W.c())) {
            StringBuilder sb = new StringBuilder();
            pd4 pd4 = pd4.a;
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            Object[] objArr = {Integer.valueOf(i3)};
            String format = String.format(locale, "%02d", Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
            sb.append(format);
            sb.append(':');
            pd4 pd42 = pd4.a;
            Locale locale2 = Locale.US;
            kd4.a((Object) locale2, "Locale.US");
            Object[] objArr2 = {Integer.valueOf(i4)};
            String format2 = String.format(locale2, "%02d", Arrays.copyOf(objArr2, objArr2.length));
            kd4.a((Object) format2, "java.lang.String.format(locale, format, *args)");
            sb.append(format2);
            return new SpannableString(sb.toString());
        }
        int i5 = 12;
        if (i2 < 720) {
            if (i3 == 0) {
                i3 = 12;
            }
            ll2 ll2 = ll2.b;
            StringBuilder sb2 = new StringBuilder();
            pd4 pd43 = pd4.a;
            Locale locale3 = Locale.US;
            kd4.a((Object) locale3, "Locale.US");
            Object[] objArr3 = {Integer.valueOf(i3)};
            String format3 = String.format(locale3, "%02d", Arrays.copyOf(objArr3, objArr3.length));
            kd4.a((Object) format3, "java.lang.String.format(locale, format, *args)");
            sb2.append(format3);
            sb2.append(':');
            pd4 pd44 = pd4.a;
            Locale locale4 = Locale.US;
            kd4.a((Object) locale4, "Locale.US");
            Object[] objArr4 = {Integer.valueOf(i4)};
            String format4 = String.format(locale4, "%02d", Arrays.copyOf(objArr4, objArr4.length));
            kd4.a((Object) format4, "java.lang.String.format(locale, format, *args)");
            sb2.append(format4);
            String sb3 = sb2.toString();
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Am);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
            return ll2.a(sb3, a2, 1.0f);
        }
        if (i3 > 12) {
            i5 = i3 - 12;
        }
        ll2 ll22 = ll2.b;
        StringBuilder sb4 = new StringBuilder();
        pd4 pd45 = pd4.a;
        Locale locale5 = Locale.US;
        kd4.a((Object) locale5, "Locale.US");
        Object[] objArr5 = {Integer.valueOf(i5)};
        String format5 = String.format(locale5, "%02d", Arrays.copyOf(objArr5, objArr5.length));
        kd4.a((Object) format5, "java.lang.String.format(locale, format, *args)");
        sb4.append(format5);
        sb4.append(':');
        pd4 pd46 = pd4.a;
        Locale locale6 = Locale.US;
        kd4.a((Object) locale6, "Locale.US");
        Object[] objArr6 = {Integer.valueOf(i4)};
        String format6 = String.format(locale6, "%02d", Arrays.copyOf(objArr6, objArr6.length));
        kd4.a((Object) format6, "java.lang.String.format(locale, format, *args)");
        sb4.append(format6);
        String sb5 = sb4.toString();
        String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Pm);
        kd4.a((Object) a3, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
        return ll22.a(sb5, a3, 1.0f);
    }

    @DexIgnore
    public void a(Alarm alarm) {
        CharSequence a2 = this.f.a();
        if (a2 == null || a2.length() == 0) {
            FLogger.INSTANCE.getLocal().d("HomeAlertsFragment", "Current Active Device Serial Is Empty");
        } else if (alarm != null || this.h.size() < 32) {
            sv2 sv2 = this.l;
            String a3 = this.f.a();
            if (a3 != null) {
                kd4.a((Object) a3, "mActiveSerial.value!!");
                sv2.a(a3, this.h, alarm);
                return;
            }
            kd4.a();
            throw null;
        } else {
            this.l.r();
        }
    }

    @DexIgnore
    public void a(Alarm alarm, boolean z) {
        kd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        CharSequence a2 = this.f.a();
        if (!(a2 == null || a2.length() == 0)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "enableAlarm - alarmTotalMinue: " + alarm.getTotalMinutes() + " - enable: " + z);
            alarm.setActive(z);
            this.l.b();
            SetAlarms setAlarms = this.s;
            String a3 = this.f.a();
            if (a3 != null) {
                kd4.a((Object) a3, "mActiveSerial.value!!");
                setAlarms.a(new SetAlarms.c(a3, this.h, alarm), new b(this, alarm));
                return;
            }
            kd4.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(w, "enableAlarm - Current Active Device Serial Is Empty");
    }

    @DexIgnore
    public final void a(List<AppWrapper> list) {
        kd4.b(list, "listAppWrapperNotEnabled");
        for (AppWrapper installedApp : list) {
            InstalledApp installedApp2 = installedApp.getInstalledApp();
            if (installedApp2 != null) {
                installedApp2.setSelected(true);
            }
        }
        this.m.a(this.q, new iw2.b(list), new c(this));
    }

    @DexIgnore
    public final String a(int i2) {
        if (i2 == 0) {
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__Everyone);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return a2;
        } else if (i2 != 1) {
            String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__NoOne);
            kd4.a((Object) a3, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return a3;
        } else {
            String a4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__FavoriteContacts);
            kd4.a((Object) a4, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return a4;
        }
    }
}
