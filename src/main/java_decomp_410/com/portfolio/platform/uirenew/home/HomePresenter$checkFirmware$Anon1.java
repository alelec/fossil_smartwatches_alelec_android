package com.portfolio.platform.uirenew.home;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$Anon1", f = "HomePresenter.kt", l = {265}, m = "invokeSuspend")
public final class HomePresenter$checkFirmware$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomePresenter$checkFirmware$Anon1(HomePresenter homePresenter, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = homePresenter;
        this.$activeDeviceSerial = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomePresenter$checkFirmware$Anon1 homePresenter$checkFirmware$Anon1 = new HomePresenter$checkFirmware$Anon1(this.this$Anon0, this.$activeDeviceSerial, yb4);
        homePresenter$checkFirmware$Anon1.p$ = (zg4) obj;
        return homePresenter$checkFirmware$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomePresenter$checkFirmware$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "checkFirmware() enter checkFirmware");
            ug4 a2 = this.this$Anon0.b();
            HomePresenter$checkFirmware$Anon1$isLatestFw$Anon1 homePresenter$checkFirmware$Anon1$isLatestFw$Anon1 = new HomePresenter$checkFirmware$Anon1$isLatestFw$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a2, homePresenter$checkFirmware$Anon1$isLatestFw$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        boolean booleanValue = ((Boolean) obj).booleanValue();
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.OTHER;
        String str = this.$activeDeviceSerial;
        String a3 = HomePresenter.y.a();
        remote.i(component, session, str, a3, "[Sync OK] Is device has latest FW " + booleanValue);
        if (booleanValue) {
            FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "checkFirmware() fw is latest, skip OTA after sync success");
        } else {
            this.this$Anon0.p();
        }
        FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "checkFirmware() exit checkFirmware");
        return qa4.a;
    }
}
