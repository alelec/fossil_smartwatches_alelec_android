package com.portfolio.platform.uirenew.home.customize.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaCustomizeViewModel$watchFaceObserver$1<T> implements com.fossil.blesdk.obfuscated.C1548cc<java.util.List<? extends com.portfolio.platform.data.model.diana.preset.WatchFace>> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel f22659a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1$1", mo27670f = "DianaCustomizeViewModel.kt", mo27671l = {505}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1$1 */
    public static final class C64141 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $it;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public java.lang.Object L$1;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22660p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1$1$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1$1$1", mo27670f = "DianaCustomizeViewModel.kt", mo27671l = {}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1$1$1 */
        public static final class C64151 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Boolean>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ java.util.List $list;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f22661p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1.C64141 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C64151(com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1.C64141 r1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$list = list;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1.C64141.C64151 r0 = new com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1.C64141.C64151(this.this$0, this.$list, yb4);
                r0.f22661p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1.C64141.C64151) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                com.fossil.blesdk.obfuscated.cc4.m20546a();
                if (this.label == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    this.this$0.this$0.f22659a.f22629h.clear();
                    return com.fossil.blesdk.obfuscated.dc4.m20839a(this.this$0.this$0.f22659a.f22629h.addAll(this.$list));
                }
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64141(com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1 dianaCustomizeViewModel$watchFaceObserver$1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = dianaCustomizeViewModel$watchFaceObserver$1;
            this.$it = list;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1.C64141 r0 = new com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1.C64141(this.this$0, this.$it, yb4);
            r0.f22660p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1.C64141) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22660p$;
                if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f22659a.f22638q, (java.lang.Object) this.$it)) {
                    this.this$0.f22659a.f22638q = this.$it;
                    java.util.List list = this.$it;
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) list, "it");
                    com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset = (com.portfolio.platform.data.model.diana.preset.DianaPreset) this.this$0.f22659a.f22625d.mo2275a();
                    java.util.List<com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper> a2 = com.fossil.blesdk.obfuscated.tj2.m28285a((java.util.List<com.portfolio.platform.data.model.diana.preset.WatchFace>) list, (java.util.List<com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting>) dianaPreset != null ? dianaPreset.getComplications() : null);
                    com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
                    com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1.C64141.C64151 r5 = new com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1.C64141.C64151(this, a2, (com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg4;
                    this.L$1 = a2;
                    this.label = 1;
                    if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r5, this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                java.util.List list2 = (java.util.List) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public DianaCustomizeViewModel$watchFaceObserver$1(com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel dianaCustomizeViewModel) {
        this.f22659a = dianaCustomizeViewModel;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo8689a(java.util.List<com.portfolio.platform.data.model.diana.preset.WatchFace> list) {
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25691a()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1.C64141(this, list, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }
}
