package com.portfolio.platform.uirenew.home.profile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1", mo27670f = "HomeProfilePresenter.kt", mo27671l = {355}, mo27672m = "invokeSuspend")
public final class HomeProfilePresenter$openCameraIntent$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23813p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$openCameraIntent$1(com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter homeProfilePresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homeProfilePresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1 homeProfilePresenter$openCameraIntent$1 = new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1(this.this$0, yb4);
        homeProfilePresenter$openCameraIntent$1.f23813p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeProfilePresenter$openCameraIntent$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23813p$;
            com.fossil.blesdk.obfuscated.og3 l = this.this$0.mo41449l();
            if (l != null) {
                androidx.fragment.app.FragmentActivity activity = ((com.fossil.blesdk.obfuscated.ev2) l).getActivity();
                if (activity != null) {
                    com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
                    com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1$1$pickerImageIntent$1 homeProfilePresenter$openCameraIntent$1$1$pickerImageIntent$1 = new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1$1$pickerImageIntent$1(activity, (com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg4;
                    this.L$1 = activity;
                    this.L$2 = activity;
                    this.label = 1;
                    obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, homeProfilePresenter$openCameraIntent$1$1$pickerImageIntent$1, this);
                    if (obj == a) {
                        return a;
                    }
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
        } else if (i == 1) {
            androidx.fragment.app.FragmentActivity fragmentActivity = (androidx.fragment.app.FragmentActivity) this.L$2;
            androidx.fragment.app.FragmentActivity fragmentActivity2 = (androidx.fragment.app.FragmentActivity) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        android.content.Intent intent = (android.content.Intent) obj;
        if (intent != null) {
            ((com.fossil.blesdk.obfuscated.ev2) this.this$0.mo41449l()).startActivityForResult(intent, 1234);
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
