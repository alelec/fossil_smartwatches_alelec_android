package com.portfolio.platform.uirenew.home.customize.diana;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.cq2;
import com.fossil.blesdk.obfuscated.f8;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.j42;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.u13;
import com.fossil.blesdk.obfuscated.v5;
import com.fossil.blesdk.obfuscated.w13;
import com.fossil.blesdk.obfuscated.x5;
import com.fossil.wearables.fossil.R;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.gson.DianaPresetDeserializer;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaCustomizeEditActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a E; // = new a((fd4) null);
    @DexIgnore
    public DianaCustomizeEditPresenter B;
    @DexIgnore
    public j42 C;
    @DexIgnore
    public DianaCustomizeViewModel D;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str, int i, String str2, String str3) {
            kd4.b(context, "context");
            kd4.b(str, "presetId");
            kd4.b(str2, "complicationPos");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditActivity", "start - presetId=" + str + ", complicationPos=" + str2 + ", watchAppPos=" + str3);
            Intent intent = new Intent(context, DianaCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_CUSTOMIZE_TAB", i);
            intent.putExtra("KEY_PRESET_COMPLICATION_POS_SELECTED", str2);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str3);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final void a(FragmentActivity fragmentActivity, String str, ArrayList<f8<View, String>> arrayList, List<? extends f8<CustomizeWidget, String>> list, CopyOnWriteArrayList<CustomizeRealData> copyOnWriteArrayList, int i, String str2, String str3) {
            kd4.b(fragmentActivity, "context");
            kd4.b(str, "presetId");
            kd4.b(arrayList, "views");
            kd4.b(list, "customizeWidgetViews");
            kd4.b(copyOnWriteArrayList, "customizeRealDataList");
            kd4.b(str2, "complicationPos");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditActivity", "startForResultAnimation() - presetId=" + str + ", complicationPos=" + str2 + ", watchAppPos=" + str3);
            Intent intent = new Intent(fragmentActivity, DianaCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_CUSTOMIZE_TAB", i);
            intent.putExtra("KEY_PRESET_COMPLICATION_POS_SELECTED", str2);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str3);
            intent.putExtra("KEY_CUSTOMIZE_REAL_DATA_LIST", copyOnWriteArrayList);
            for (f8 f8Var : list) {
                arrayList.add(new f8(f8Var.a, f8Var.b));
                Bundle bundle = new Bundle();
                cq2.a aVar = cq2.f;
                F f = f8Var.a;
                if (f != null) {
                    kd4.a((Object) f, "wcPair.first!!");
                    aVar.a((CustomizeWidget) f, bundle);
                    F f2 = f8Var.a;
                    if (f2 != null) {
                        kd4.a((Object) f2, "wcPair.first!!");
                        intent.putExtra(((CustomizeWidget) f2).getTransitionName(), bundle);
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
            Object[] array = arrayList.toArray(new f8[0]);
            if (array != null) {
                f8[] f8VarArr = (f8[]) array;
                x5 a = x5.a(fragmentActivity, (f8[]) Arrays.copyOf(f8VarArr, f8VarArr.length));
                kd4.a((Object) a, "ActivityOptionsCompat.ma\u2026context, *viewsTypeArray)");
                v5.a(fragmentActivity, intent, 100, a.a());
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0166  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x016f  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0175  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x017f  */
    public void onCreate(Bundle bundle) {
        DianaPreset dianaPreset;
        DianaPreset dianaPreset2;
        DianaCustomizeViewModel dianaCustomizeViewModel;
        DianaPreset dianaPreset3;
        Bundle bundle2 = bundle;
        Class cls = DianaPreset.class;
        super.onCreate(bundle);
        setContentView(R.layout.activity_base);
        int intExtra = getIntent().getIntExtra("KEY_CUSTOMIZE_TAB", 1);
        String stringExtra = getIntent().getStringExtra("KEY_PRESET_ID");
        String stringExtra2 = getIntent().getStringExtra("KEY_PRESET_COMPLICATION_POS_SELECTED");
        String stringExtra3 = getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
        u13 u13 = (u13) getSupportFragmentManager().a((int) R.id.content);
        if (u13 == null) {
            u13 = new u13();
            a((Fragment) u13, "DianaCustomizeEditFragment", (int) R.id.content);
        }
        l42 g = PortfolioApp.W.c().g();
        kd4.a((Object) stringExtra, "presetId");
        String str = "KEY_PRESET_WATCH_APP_POS_SELECTED";
        w13 w13 = r8;
        String str2 = "KEY_PRESET_COMPLICATION_POS_SELECTED";
        String str3 = stringExtra3;
        w13 w132 = new w13(u13, stringExtra, intExtra, stringExtra2, stringExtra3);
        g.a(w13).a(this);
        if (!(bundle2 == null || intExtra == -1)) {
            DianaCustomizeEditPresenter dianaCustomizeEditPresenter = this.B;
            if (dianaCustomizeEditPresenter != null) {
                dianaCustomizeEditPresenter.b(intExtra);
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("KEY_CUSTOMIZE_REAL_DATA_LIST")) {
            DianaCustomizeEditPresenter dianaCustomizeEditPresenter2 = this.B;
            if (dianaCustomizeEditPresenter2 != null) {
                ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("KEY_CUSTOMIZE_REAL_DATA_LIST");
                kd4.a((Object) parcelableArrayListExtra, "it.getParcelableArrayLis\u2026CUSTOMIZE_REAL_DATA_LIST)");
                dianaCustomizeEditPresenter2.a((ArrayList<CustomizeRealData>) parcelableArrayListExtra);
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        j42 j42 = this.C;
        if (j42 != null) {
            ic a2 = lc.a((FragmentActivity) this, (kc.b) j42).a(DianaCustomizeViewModel.class);
            kd4.a((Object) a2, "ViewModelProviders.of(th\u2026izeViewModel::class.java)");
            this.D = (DianaCustomizeViewModel) a2;
            if (bundle2 == null) {
                FLogger.INSTANCE.getLocal().d(f(), "init from initialize state");
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.D;
                if (dianaCustomizeViewModel2 != null) {
                    dianaCustomizeViewModel2.a(stringExtra, stringExtra2, str3);
                } else {
                    kd4.d("mDianaCustomizeViewModel");
                    throw null;
                }
            } else {
                String str4 = str3;
                FLogger.INSTANCE.getLocal().d(f(), "init from savedInstanceState");
                rz1 rz1 = new rz1();
                rz1.a(cls, new DianaPresetDeserializer());
                Gson a3 = rz1.a();
                try {
                    dianaPreset3 = bundle2.containsKey("KEY_CURRENT_PRESET") ? (DianaPreset) a3.a(bundle2.getString("KEY_CURRENT_PRESET"), cls) : null;
                    try {
                        if (bundle2.containsKey("KEY_ORIGINAL_PRESET")) {
                            dianaPreset2 = (DianaPreset) a3.a(bundle2.getString("KEY_ORIGINAL_PRESET"), cls);
                            dianaPreset = dianaPreset3;
                            String str5 = str2;
                            String string = !bundle2.containsKey(str5) ? bundle2.getString(str5) : stringExtra2;
                            String str6 = str;
                            String string2 = !bundle2.containsKey(str6) ? bundle2.getString(str6) : str4;
                            dianaCustomizeViewModel = this.D;
                            if (dianaCustomizeViewModel == null) {
                                dianaCustomizeViewModel.a(stringExtra, dianaPreset2, dianaPreset, string, string2);
                                return;
                            } else {
                                kd4.d("mDianaCustomizeViewModel");
                                throw null;
                            }
                        }
                    } catch (Exception e) {
                        e = e;
                        FLogger.INSTANCE.getLocal().d(f(), "exception when parse GSON when retrieve from saveInstanceState " + e);
                        dianaPreset = dianaPreset3;
                        dianaPreset2 = null;
                        String str52 = str2;
                        if (!bundle2.containsKey(str52)) {
                        }
                        String str62 = str;
                        if (!bundle2.containsKey(str62)) {
                        }
                        dianaCustomizeViewModel = this.D;
                        if (dianaCustomizeViewModel == null) {
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    dianaPreset3 = null;
                    FLogger.INSTANCE.getLocal().d(f(), "exception when parse GSON when retrieve from saveInstanceState " + e);
                    dianaPreset = dianaPreset3;
                    dianaPreset2 = null;
                    String str522 = str2;
                    if (!bundle2.containsKey(str522)) {
                    }
                    String str622 = str;
                    if (!bundle2.containsKey(str622)) {
                    }
                    dianaCustomizeViewModel = this.D;
                    if (dianaCustomizeViewModel == null) {
                    }
                }
                dianaPreset = dianaPreset3;
                dianaPreset2 = null;
                String str5222 = str2;
                if (!bundle2.containsKey(str5222)) {
                }
                String str6222 = str;
                if (!bundle2.containsKey(str6222)) {
                }
                dianaCustomizeViewModel = this.D;
                if (dianaCustomizeViewModel == null) {
                }
            }
        } else {
            kd4.d("viewModelFactory");
            throw null;
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        DianaCustomizeEditPresenter dianaCustomizeEditPresenter = this.B;
        if (dianaCustomizeEditPresenter != null) {
            dianaCustomizeEditPresenter.a(bundle);
            super.onSaveInstanceState(bundle);
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }
}
