package com.portfolio.platform.uirenew.home.customize.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$setPresetToDb$1", mo27670f = "SetHybridPresetToWatchUseCase.kt", mo27671l = {80}, mo27672m = "invokeSuspend")
public final class SetHybridPresetToWatchUseCase$setPresetToDb$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22942p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetHybridPresetToWatchUseCase$setPresetToDb$1(com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = setHybridPresetToWatchUseCase;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$setPresetToDb$1 setHybridPresetToWatchUseCase$setPresetToDb$1 = new com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$setPresetToDb$1(this.this$0, yb4);
        setHybridPresetToWatchUseCase$setPresetToDb$1.f22942p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return setHybridPresetToWatchUseCase$setPresetToDb$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$setPresetToDb$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.data.model.room.microapp.HybridPreset hybridPreset;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22942p$;
            com.portfolio.platform.data.model.room.microapp.HybridPreset a2 = com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase.m34184c(this.this$0).mo41129a();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("SetHybridPresetToWatchUseCase", "set to watch success, set preset " + a2 + " to db");
            a2.setActive(true);
            com.portfolio.platform.data.source.HybridPresetRepository a3 = this.this$0.f22935h;
            this.L$0 = zg4;
            this.L$1 = a2;
            this.label = 1;
            if (a3.upsertHybridPreset(a2, this) == a) {
                return a;
            }
            hybridPreset = a2;
        } else if (i == 1) {
            hybridPreset = (com.portfolio.platform.data.model.room.microapp.HybridPreset) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        java.util.Calendar instance = java.util.Calendar.getInstance();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "Calendar.getInstance()");
        java.lang.String t = com.fossil.blesdk.obfuscated.rk2.m27415t(instance.getTime());
        java.util.Iterator<com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting> it = hybridPreset.getButtons().iterator();
        while (it.hasNext()) {
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting next = it.next();
            if (!com.fossil.blesdk.obfuscated.oj2.m26116a(next.getSettings())) {
                com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase = this.this$0;
                java.lang.String appId = next.getAppId();
                java.lang.String settings = next.getSettings();
                if (settings != null) {
                    setHybridPresetToWatchUseCase.mo41121a(appId, settings);
                    com.portfolio.platform.data.source.MicroAppLastSettingRepository b = this.this$0.f22937j;
                    java.lang.String appId2 = next.getAppId();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) t, "updatedAt");
                    java.lang.String settings2 = next.getSettings();
                    if (settings2 != null) {
                        b.upsertMicroAppLastSetting(new com.portfolio.platform.data.model.microapp.MicroAppLastSetting(appId2, t, settings2));
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            }
        }
        this.this$0.mo34436a(new com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase.C6512d());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
