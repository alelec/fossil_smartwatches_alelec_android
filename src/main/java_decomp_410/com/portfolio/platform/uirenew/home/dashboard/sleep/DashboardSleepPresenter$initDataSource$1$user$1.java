package com.portfolio.platform.uirenew.home.dashboard.sleep;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$1$user$1", mo27670f = "DashboardSleepPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class DashboardSleepPresenter$initDataSource$1$user$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.MFUser>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23467p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardSleepPresenter$initDataSource$1$user$1(com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$1 dashboardSleepPresenter$initDataSource$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = dashboardSleepPresenter$initDataSource$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$1$user$1 dashboardSleepPresenter$initDataSource$1$user$1 = new com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$1$user$1(this.this$0, yb4);
        dashboardSleepPresenter$initDataSource$1$user$1.f23467p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dashboardSleepPresenter$initDataSource$1$user$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$1$user$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return this.this$0.this$0.f23463n.getCurrentUser();
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
