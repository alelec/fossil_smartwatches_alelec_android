package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import java.util.Iterator;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$createNewPreset$Anon1", f = "HomeDianaCustomizePresenter.kt", l = {321}, m = "invokeSuspend")
public final class HomeDianaCustomizePresenter$createNewPreset$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$createNewPreset$Anon1(HomeDianaCustomizePresenter homeDianaCustomizePresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = homeDianaCustomizePresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeDianaCustomizePresenter$createNewPreset$Anon1 homeDianaCustomizePresenter$createNewPreset$Anon1 = new HomeDianaCustomizePresenter$createNewPreset$Anon1(this.this$Anon0, yb4);
        homeDianaCustomizePresenter$createNewPreset$Anon1.p$ = (zg4) obj;
        return homeDianaCustomizePresenter$createNewPreset$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDianaCustomizePresenter$createNewPreset$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            Iterator it = this.this$Anon0.j.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj2 = null;
                    break;
                }
                obj2 = it.next();
                if (dc4.a(((DianaPreset) obj2).isActive()).booleanValue()) {
                    break;
                }
            }
            DianaPreset dianaPreset = (DianaPreset) obj2;
            if (dianaPreset != null) {
                DianaPreset cloneFrom = DianaPreset.Companion.cloneFrom(dianaPreset);
                ug4 d = this.this$Anon0.c();
                HomeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 homeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new HomeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(cloneFrom, (yb4) null, this);
                this.L$Anon0 = zg4;
                this.L$Anon1 = dianaPreset;
                this.L$Anon2 = dianaPreset;
                this.L$Anon3 = cloneFrom;
                this.label = 1;
                if (yf4.a(d, homeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1, this) == a) {
                    return a;
                }
            }
            return qa4.a;
        } else if (i == 1) {
            DianaPreset dianaPreset2 = (DianaPreset) this.L$Anon3;
            DianaPreset dianaPreset3 = (DianaPreset) this.L$Anon2;
            DianaPreset dianaPreset4 = (DianaPreset) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.this$Anon0;
        homeDianaCustomizePresenter.c(homeDianaCustomizePresenter.k() + 1);
        this.this$Anon0.t.v();
        return qa4.a;
    }
}
