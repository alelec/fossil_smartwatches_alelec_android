package com.portfolio.platform.uirenew.home.customize.hybrid.search;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.a73;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.b73;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SearchMicroAppPresenter extends a73 {
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h; // = "empty";
    @DexIgnore
    public String i;
    @DexIgnore
    public /* final */ ArrayList<MicroApp> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ b73 l;
    @DexIgnore
    public /* final */ MicroAppRepository m;
    @DexIgnore
    public /* final */ en2 n;
    @DexIgnore
    public /* final */ PortfolioApp o;

    @DexIgnore
    public SearchMicroAppPresenter(b73 b73, MicroAppRepository microAppRepository, en2 en2, PortfolioApp portfolioApp) {
        kd4.b(b73, "mView");
        kd4.b(microAppRepository, "mMicroAppRepository");
        kd4.b(en2, "sharedPreferencesManager");
        kd4.b(portfolioApp, "mApp");
        this.l = b73;
        this.m = microAppRepository;
        this.n = en2;
        this.o = portfolioApp;
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public final void i() {
        if (this.k.isEmpty()) {
            this.l.b(a((List<MicroApp>) kb4.d(this.j)));
        } else {
            this.l.e(a((List<MicroApp>) kb4.d(this.k)));
        }
        if (!TextUtils.isEmpty(this.i)) {
            b73 b73 = this.l;
            String str = this.i;
            if (str != null) {
                b73.a(str);
                String str2 = this.i;
                if (str2 != null) {
                    a(str2);
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void j() {
        this.l.a(this);
    }

    @DexIgnore
    public void f() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SearchMicroAppPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void h() {
        this.i = "";
        this.l.u();
        i();
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        kd4.b(str, "watchAppTop");
        kd4.b(str2, "watchAppMiddle");
        kd4.b(str3, "watchAppBottom");
        this.f = str;
        this.h = str3;
        this.g = str2;
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SearchMicroAppPresenter$search$Anon1(this, str, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(MicroApp microApp) {
        kd4.b(microApp, "selectedMicroApp");
        List<String> q = this.n.q();
        kd4.a((Object) q, "sharedPreferencesManager.microAppSearchedIdsRecent");
        if (!q.contains(microApp.getId())) {
            q.add(0, microApp.getId());
            if (q.size() > 5) {
                q = q.subList(0, 5);
            }
            this.n.c(q);
        }
        this.l.a(microApp);
    }

    @DexIgnore
    public final List<Pair<MicroApp, String>> a(List<MicroApp> list) {
        ArrayList arrayList = new ArrayList();
        for (MicroApp next : list) {
            String id = next.getId();
            if (kd4.a((Object) id, (Object) this.f)) {
                arrayList.add(new Pair(next, ViewHierarchy.DIMENSION_TOP_KEY));
            } else if (kd4.a((Object) id, (Object) this.g)) {
                arrayList.add(new Pair(next, "middle"));
            } else if (kd4.a((Object) id, (Object) this.h)) {
                arrayList.add(new Pair(next, "bottom"));
            } else {
                arrayList.add(new Pair(next, ""));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        if (bundle != null) {
            bundle.putString(ViewHierarchy.DIMENSION_TOP_KEY, this.f);
            bundle.putString("middle", this.g);
            bundle.putString("bottom", this.h);
        }
        return bundle;
    }
}
