package com.portfolio.platform.uirenew.home.customize.diana;

import android.content.Intent;
import android.view.View;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.bn2;
import com.fossil.blesdk.obfuscated.c23;
import com.fossil.blesdk.obfuscated.d23;
import com.fossil.blesdk.obfuscated.dl4;
import com.fossil.blesdk.obfuscated.f13;
import com.fossil.blesdk.obfuscated.f8;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.fl4;
import com.fossil.blesdk.obfuscated.h13;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.oa4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.tj2;
import com.fossil.blesdk.obfuscated.vu2;
import com.fossil.blesdk.obfuscated.wm2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeDianaCustomizePresenter extends c23 {
    @DexIgnore
    public /* final */ UserRepository A;
    @DexIgnore
    public /* final */ WatchFaceRepository B;
    @DexIgnore
    public LiveData<List<DianaPreset>> f; // = new MutableLiveData();
    @DexIgnore
    public LiveData<List<CustomizeRealData>> g; // = this.z.getAllRealDataAsLiveData();
    @DexIgnore
    public /* final */ ArrayList<Complication> h; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<WatchApp> i; // = new ArrayList<>();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<DianaPreset> j; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public DianaPreset k;
    @DexIgnore
    public MutableLiveData<String> l;
    @DexIgnore
    public int m; // = -1;
    @DexIgnore
    public CopyOnWriteArrayList<CustomizeRealData> n; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public int o; // = 1;
    @DexIgnore
    public MFUser p;
    @DexIgnore
    public dl4 q; // = fl4.a(false, 1, (Object) null);
    @DexIgnore
    public Pair<Boolean, ? extends List<DianaPreset>> r; // = oa4.a(false, null);
    @DexIgnore
    public boolean s;
    @DexIgnore
    public /* final */ d23 t;
    @DexIgnore
    public /* final */ WatchAppRepository u;
    @DexIgnore
    public /* final */ ComplicationRepository v;
    @DexIgnore
    public /* final */ DianaPresetRepository w;
    @DexIgnore
    public /* final */ SetDianaPresetToWatchUseCase x;
    @DexIgnore
    public /* final */ wm2 y;
    @DexIgnore
    public /* final */ CustomizeRealDataRepository z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<SetDianaPresetToWatchUseCase.d, SetDianaPresetToWatchUseCase.b> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter a;

        @DexIgnore
        public b(HomeDianaCustomizePresenter homeDianaCustomizePresenter) {
            this.a = homeDianaCustomizePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetDianaPresetToWatchUseCase.d dVar) {
            kd4.b(dVar, "responseValue");
            this.a.t.m();
            this.a.t.e(0);
        }

        @DexIgnore
        public void a(SetDianaPresetToWatchUseCase.b bVar) {
            kd4.b(bVar, "errorValue");
            this.a.t.m();
            int b = bVar.b();
            if (b == 1101 || b == 1112 || b == 1113) {
                List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(bVar.a());
                kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                d23 r = this.a.t;
                Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
                if (array != null) {
                    PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                    r.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.a.t.j();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public HomeDianaCustomizePresenter(d23 d23, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, DianaPresetRepository dianaPresetRepository, SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase, wm2 wm2, CustomizeRealDataRepository customizeRealDataRepository, UserRepository userRepository, WatchFaceRepository watchFaceRepository, PortfolioApp portfolioApp) {
        kd4.b(d23, "mView");
        kd4.b(watchAppRepository, "mWatchAppRepository");
        kd4.b(complicationRepository, "mComplicationRepository");
        kd4.b(dianaPresetRepository, "mDianaPresetRepository");
        kd4.b(setDianaPresetToWatchUseCase, "mSetDianaPresetToWatchUseCase");
        kd4.b(wm2, "mCustomizeRealDataManager");
        kd4.b(customizeRealDataRepository, "mCustomizeRealDataRepository");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(watchFaceRepository, "mWatchFaceRepository");
        kd4.b(portfolioApp, "mApp");
        this.t = d23;
        this.u = watchAppRepository;
        this.v = complicationRepository;
        this.w = dianaPresetRepository;
        this.x = setDianaPresetToWatchUseCase;
        this.y = wm2;
        this.z = customizeRealDataRepository;
        this.A = userRepository;
        this.B = watchFaceRepository;
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "init comps, apps first time");
        this.l = portfolioApp.f();
    }

    @DexIgnore
    public final void b(String str) {
        T t2;
        T t3;
        List a2 = this.f.a();
        if (a2 != null) {
            Boolean.valueOf(!a2.isEmpty());
        }
        Iterator<T> it = this.j.iterator();
        while (true) {
            t2 = null;
            if (!it.hasNext()) {
                t3 = null;
                break;
            }
            t3 = it.next();
            if (kd4.a((Object) ((DianaPreset) t3).getId(), (Object) str)) {
                break;
            }
        }
        DianaPreset dianaPreset = (DianaPreset) t3;
        if (dianaPreset != null) {
            Iterator<T> it2 = this.j.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                T next = it2.next();
                if (((DianaPreset) next).isActive()) {
                    t2 = next;
                    break;
                }
            }
            DianaPreset dianaPreset2 = (DianaPreset) t2;
            this.t.l();
            FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "delete activePreset " + dianaPreset2 + " set preset " + dianaPreset + " as active first");
            this.x.a(new SetDianaPresetToWatchUseCase.c(dianaPreset), new HomeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1(dianaPreset2, this, str));
        }
    }

    @DexIgnore
    public final void c(int i2) {
        this.m = i2;
    }

    @DexIgnore
    public void f() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeDianaCustomizePresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        try {
            MutableLiveData<String> mutableLiveData = this.l;
            d23 d23 = this.t;
            if (d23 != null) {
                mutableLiveData.a((LifecycleOwner) (vu2) d23);
                this.f.a((LifecycleOwner) this.t);
                this.g.a((LifecycleOwner) this.t);
                this.x.g();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizePresenter", "stop fail due to " + e);
        }
    }

    @DexIgnore
    public void h() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeDianaCustomizePresenter$createNewPreset$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public CopyOnWriteArrayList<CustomizeRealData> i() {
        return this.n;
    }

    @DexIgnore
    public void j() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizePresenter", "setPresetToWatch mCurrentPreset=" + this.k);
        Set<Integer> a2 = bn2.d.a(this.k);
        bn2 bn2 = bn2.d;
        d23 d23 = this.t;
        if (d23 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        } else if (bn2.a(((vu2) d23).getContext(), a2)) {
            DianaPreset dianaPreset = this.k;
            if (dianaPreset != null) {
                this.t.l();
                this.x.a(new SetDianaPresetToWatchUseCase.c(dianaPreset), new b(this));
            }
        }
    }

    @DexIgnore
    public final int k() {
        return this.m;
    }

    @DexIgnore
    public void l() {
        this.t.a(this);
    }

    @DexIgnore
    public final void m() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "showPresets - size=" + this.j.size());
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeDianaCustomizePresenter$showPresets$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final f13 a(DianaPreset dianaPreset) {
        T t2;
        String str;
        T t3;
        String str2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizePresenter", "convertPresetToDianaPresetConfigWrapper watchAppsSize " + this.i.size() + " compsSize " + this.h.size());
        ArrayList<DianaPresetComplicationSetting> complications = dianaPreset.getComplications();
        ArrayList<DianaPresetWatchAppSetting> watchapps = dianaPreset.getWatchapps();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        WatchFace watchFaceWithId = this.B.getWatchFaceWithId(dianaPreset.getWatchFaceId());
        WatchFaceWrapper b2 = watchFaceWithId != null ? tj2.b(watchFaceWithId, complications) : null;
        Iterator<DianaPresetComplicationSetting> it = complications.iterator();
        while (it.hasNext()) {
            DianaPresetComplicationSetting next = it.next();
            String component1 = next.component1();
            String component2 = next.component2();
            Iterator<T> it2 = this.h.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t3 = null;
                    break;
                }
                t3 = it2.next();
                if (kd4.a((Object) ((Complication) t3).getComplicationId(), (Object) component2)) {
                    break;
                }
            }
            Complication complication = (Complication) t3;
            if (complication != null) {
                String complicationId = complication.getComplicationId();
                String icon = complication.getIcon();
                if (icon != null) {
                    str2 = icon;
                } else {
                    str2 = "";
                }
                arrayList.add(new h13(complicationId, str2, sm2.a(PortfolioApp.W.c(), complication.getNameKey(), complication.getName()), component1, this.y.a(this.p, this.n, complication.getComplicationId(), dianaPreset)));
            } else {
                DianaPreset dianaPreset2 = dianaPreset;
            }
        }
        DianaPreset dianaPreset3 = dianaPreset;
        Iterator<DianaPresetWatchAppSetting> it3 = watchapps.iterator();
        while (it3.hasNext()) {
            DianaPresetWatchAppSetting next2 = it3.next();
            String component12 = next2.component1();
            String component22 = next2.component2();
            Iterator<T> it4 = this.i.iterator();
            while (true) {
                if (!it4.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it4.next();
                if (kd4.a((Object) ((WatchApp) t2).getWatchappId(), (Object) component22)) {
                    break;
                }
            }
            WatchApp watchApp = (WatchApp) t2;
            if (watchApp != null) {
                String watchappId = watchApp.getWatchappId();
                String icon2 = watchApp.getIcon();
                if (icon2 != null) {
                    str = icon2;
                } else {
                    str = "";
                }
                arrayList2.add(new h13(watchappId, str, sm2.a(PortfolioApp.W.c(), watchApp.getNameKey(), watchApp.getName()), component12, (String) null, 16, (fd4) null));
            }
        }
        return new f13(dianaPreset.getId(), dianaPreset.getName(), arrayList, arrayList2, dianaPreset.isActive(), b2);
    }

    @DexIgnore
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizePresenter", "onHomeTabChange - tab: " + i2);
        this.o = i2;
        if (this.o == 1 && this.r.getFirst().booleanValue()) {
            List list = (List) this.r.getSecond();
            if (list != null) {
                a((List<DianaPreset>) list);
            }
            this.r = oa4.a(false, null);
        }
    }

    @DexIgnore
    public void a(int i2) {
        if (this.j.size() > i2) {
            this.m = i2;
            this.k = this.j.get(this.m);
            DianaPreset dianaPreset = this.k;
            if (dianaPreset != null) {
                this.t.d(dianaPreset.isActive());
            }
        } else {
            this.t.d(false);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizePresenter", "viewPos " + i2 + " presenterPos " + this.m + " size " + this.j.size());
    }

    @DexIgnore
    public void a(String str, String str2) {
        kd4.b(str, "name");
        kd4.b(str2, "presetId");
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeDianaCustomizePresenter$renameCurrentPreset$Anon1(this, str2, str, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, "nextActivePresetId");
        DianaPreset dianaPreset = this.k;
        if (dianaPreset == null) {
            return;
        }
        if (dianaPreset.isActive()) {
            b(str);
        } else {
            fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeDianaCustomizePresenter$deletePreset$$inlined$let$lambda$Anon1(dianaPreset, (yb4) null, this, str), 3, (Object) null);
        }
    }

    @DexIgnore
    public void a(int i2, int i3, Intent intent) {
        this.s = false;
        if (i2 == 100 && i3 == -1) {
            this.t.d(0);
        }
    }

    @DexIgnore
    public void a(f13 f13, List<? extends f8<View, String>> list, List<? extends f8<CustomizeWidget, String>> list2) {
        kd4.b(list, "views");
        kd4.b(list2, "customizeWidgetViews");
        if (!this.s) {
            this.s = true;
            this.t.a(f13, list, list2);
        }
    }

    @DexIgnore
    public final void a(List<DianaPreset> list) {
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "doShowingPreset");
        this.j.clear();
        this.j.addAll(list);
        int size = this.j.size();
        int i2 = this.m;
        if (size > i2 && i2 > 0) {
            this.k = this.j.get(i2);
        }
        m();
    }
}
