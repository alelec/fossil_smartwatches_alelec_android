package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.pc3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.MFUser;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$loadData$Anon2", f = "HeartRateOverviewMonthPresenter.kt", l = {94}, m = "invokeSuspend")
public final class HeartRateOverviewMonthPresenter$loadData$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateOverviewMonthPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateOverviewMonthPresenter$loadData$Anon2(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = heartRateOverviewMonthPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HeartRateOverviewMonthPresenter$loadData$Anon2 heartRateOverviewMonthPresenter$loadData$Anon2 = new HeartRateOverviewMonthPresenter$loadData$Anon2(this.this$Anon0, yb4);
        heartRateOverviewMonthPresenter$loadData$Anon2.p$ = (zg4) obj;
        return heartRateOverviewMonthPresenter$loadData$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HeartRateOverviewMonthPresenter$loadData$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 b = this.this$Anon0.c();
            HeartRateOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1 heartRateOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1 = new HeartRateOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(b, heartRateOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser = (MFUser) obj;
        if (mFUser != null) {
            this.this$Anon0.j = rk2.d(mFUser.getCreatedAt());
            pc3 l = this.this$Anon0.n;
            Date d = HeartRateOverviewMonthPresenter.d(this.this$Anon0);
            Date c = this.this$Anon0.j;
            if (c == null) {
                c = new Date();
            }
            l.a(d, c);
            this.this$Anon0.f.a(new Date());
        }
        return qa4.a;
    }
}
