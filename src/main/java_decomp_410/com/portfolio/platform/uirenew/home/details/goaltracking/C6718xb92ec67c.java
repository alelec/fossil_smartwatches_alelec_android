package com.portfolio.platform.uirenew.home.details.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$1$1$summary$1", mo27670f = "GoalTrackingDetailPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$1$1$summary$1 */
public final class C6718xb92ec67c extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23698p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$1.C67171 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6718xb92ec67c(com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$1.C67171 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = r1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.goaltracking.C6718xb92ec67c goalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$1$1$summary$1 = new com.portfolio.platform.uirenew.home.details.goaltracking.C6718xb92ec67c(this.this$0, yb4);
        goalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$1$1$summary$1.f23698p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return goalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$1$1$summary$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.goaltracking.C6718xb92ec67c) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter goalTrackingDetailPresenter = this.this$0.this$0.f23696a;
            return goalTrackingDetailPresenter.mo41407a(goalTrackingDetailPresenter.f23668g, (java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>) this.this$0.this$0.f23696a.f23673l);
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
