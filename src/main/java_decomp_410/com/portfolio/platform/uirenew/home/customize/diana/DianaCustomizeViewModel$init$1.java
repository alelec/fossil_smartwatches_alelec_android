package com.portfolio.platform.uirenew.home.customize.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1", mo27670f = "DianaCustomizeViewModel.kt", mo27671l = {155, 160}, mo27672m = "invokeSuspend")
public final class DianaCustomizeViewModel$init$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $complicationPos;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $presetId;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $watchAppPos;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22655p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1$1", mo27670f = "DianaCustomizeViewModel.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1$1 */
    public static final class C64121 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22656p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64121(com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1 dianaCustomizeViewModel$init$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = dianaCustomizeViewModel$init$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1.C64121 r0 = new com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1.C64121(this.this$0, yb4);
            r0.f22656p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1.C64121) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.f22625d.mo2279a(this.this$0.this$0.f22645x);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaCustomizeViewModel$init$1(com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel dianaCustomizeViewModel, java.lang.String str, java.lang.String str2, java.lang.String str3, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = dianaCustomizeViewModel;
        this.$presetId = str;
        this.$watchAppPos = str2;
        this.$complicationPos = str3;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1 dianaCustomizeViewModel$init$1 = new com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1(this.this$0, this.$presetId, this.$watchAppPos, this.$complicationPos, yb4);
        dianaCustomizeViewModel$init$1.f22655p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dianaCustomizeViewModel$init$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f22655p$;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel.f22618G.mo40985a();
            local.mo33255d(a2, "init presetId=" + this.$presetId + " originalPreset=" + this.this$0.f22624c + " watchAppPos=" + this.$watchAppPos + " complicationPos=" + this.$complicationPos);
            if (this.this$0.f22624c == null) {
                com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel dianaCustomizeViewModel = this.this$0;
                java.lang.String str = this.$presetId;
                this.L$0 = zg4;
                this.label = 1;
                if (dianaCustomizeViewModel.mo40961a(str, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.f22630i.mo2280a(this.$complicationPos);
        this.this$0.f22633l.mo2280a(this.$watchAppPos);
        com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
        com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1.C64121 r3 = new com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1.C64121(this, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.label = 2;
        if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r3, this) == a) {
            return a;
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
