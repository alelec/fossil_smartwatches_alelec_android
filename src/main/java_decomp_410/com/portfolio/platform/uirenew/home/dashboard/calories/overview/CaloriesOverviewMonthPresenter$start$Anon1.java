package com.portfolio.platform.uirenew.home.dashboard.calories.overview;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.pa3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.enums.Status;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CaloriesOverviewMonthPresenter$start$Anon1<T> implements cc<os3<? extends List<ActivitySummary>>> {
    @DexIgnore
    public /* final */ /* synthetic */ CaloriesOverviewMonthPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$start$Anon1$Anon1", f = "CaloriesOverviewMonthPresenter.kt", l = {60}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $data;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewMonthPresenter$start$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$start$Anon1$Anon1$Anon1")
        @gc4(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$start$Anon1$Anon1$Anon1", f = "CaloriesOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$start$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0150Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super TreeMap<Long, Float>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0150Anon1(Anon1 anon1, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0150Anon1 anon1 = new C0150Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0150Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                cc4.a();
                if (this.label == 0) {
                    na4.a(obj);
                    CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter = this.this$Anon0.this$Anon0.a;
                    Object a = caloriesOverviewMonthPresenter.f.a();
                    if (a != null) {
                        kd4.a(a, "mDate.value!!");
                        return caloriesOverviewMonthPresenter.a((Date) a, (List<ActivitySummary>) this.this$Anon0.$data);
                    }
                    kd4.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(CaloriesOverviewMonthPresenter$start$Anon1 caloriesOverviewMonthPresenter$start$Anon1, List list, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = caloriesOverviewMonthPresenter$start$Anon1;
            this.$data = list;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$data, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter;
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                this.this$Anon0.a.l = this.$data;
                CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter2 = this.this$Anon0.a;
                ug4 a2 = caloriesOverviewMonthPresenter2.b();
                C0150Anon1 anon1 = new C0150Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = caloriesOverviewMonthPresenter2;
                this.label = 1;
                obj = yf4.a(a2, anon1, this);
                if (obj == a) {
                    return a;
                }
                caloriesOverviewMonthPresenter = caloriesOverviewMonthPresenter2;
            } else if (i == 1) {
                caloriesOverviewMonthPresenter = (CaloriesOverviewMonthPresenter) this.L$Anon1;
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            caloriesOverviewMonthPresenter.n = (TreeMap) obj;
            pa3 l = this.this$Anon0.a.o;
            TreeMap d = this.this$Anon0.a.n;
            if (d == null) {
                d = new TreeMap();
            }
            l.a(d);
            return qa4.a;
        }
    }

    @DexIgnore
    public CaloriesOverviewMonthPresenter$start$Anon1(CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter) {
        this.a = caloriesOverviewMonthPresenter;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    public final void a(os3<? extends List<ActivitySummary>> os3) {
        Integer num;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("mDateTransformations - status=");
        sb.append(os3 != null ? os3.f() : null);
        sb.append(" -- data.size=");
        if (os3 != null) {
            List list = (List) os3.d();
            if (list != null) {
                num = Integer.valueOf(list.size());
                sb.append(num);
                local.d("CaloriesOverviewMonthPresenter", sb.toString());
                if ((os3 == null ? os3.f() : null) == Status.DATABASE_LOADING) {
                    List list2 = os3 != null ? (List) os3.d() : null;
                    if (list2 != null && (!kd4.a((Object) this.a.l, (Object) list2))) {
                        fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, list2, (yb4) null), 3, (Object) null);
                        return;
                    }
                    return;
                }
                return;
            }
        }
        num = null;
        sb.append(num);
        local.d("CaloriesOverviewMonthPresenter", sb.toString());
        if ((os3 == null ? os3.f() : null) == Status.DATABASE_LOADING) {
        }
    }
}
