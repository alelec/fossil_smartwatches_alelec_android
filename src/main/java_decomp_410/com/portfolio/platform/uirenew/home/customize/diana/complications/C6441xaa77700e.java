package com.portfolio.platform.uirenew.home.customize.diana.complications;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$1 */
public final class C6441xaa77700e<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter f22733a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$1$1", mo27670f = "ComplicationsPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$1$1 */
    public static final class C64421 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $complicationByCategories;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.model.diana.preset.DianaPreset $currentPreset;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.ArrayList $filteredComplicationByCategories;
        @DexIgnore
        public /* final */ /* synthetic */ java.lang.String $selectedComplicationId;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22734p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.C6441xaa77700e this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64421(com.portfolio.platform.uirenew.home.customize.diana.complications.C6441xaa77700e complicationsPresenter$mComplicationsOfSelectedCategoryTransformations$1, com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset, java.util.List list, java.lang.String str, java.util.ArrayList arrayList, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = complicationsPresenter$mComplicationsOfSelectedCategoryTransformations$1;
            this.$currentPreset = dianaPreset;
            this.$complicationByCategories = list;
            this.$selectedComplicationId = str;
            this.$filteredComplicationByCategories = arrayList;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.complications.C6441xaa77700e.C64421 r1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.C6441xaa77700e.C64421(this.this$0, this.$currentPreset, this.$complicationByCategories, this.$selectedComplicationId, this.$filteredComplicationByCategories, yb4);
            r1.f22734p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r1;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.complications.C6441xaa77700e.C64421) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            T t;
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                if (this.$currentPreset != null) {
                    for (com.portfolio.platform.data.model.diana.Complication complication : this.$complicationByCategories) {
                        java.util.Iterator<T> it = this.$currentPreset.getComplications().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it.next();
                            com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting dianaPresetComplicationSetting = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) t;
                            boolean z = true;
                            if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) dianaPresetComplicationSetting.getId(), (java.lang.Object) complication.getComplicationId()) || !(!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) dianaPresetComplicationSetting.getId(), (java.lang.Object) this.$selectedComplicationId))) {
                                z = false;
                            }
                            if (com.fossil.blesdk.obfuscated.dc4.m20839a(z).booleanValue()) {
                                break;
                            }
                        }
                        if (((com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) t) == null || com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) complication.getComplicationId(), (java.lang.Object) "empty")) {
                            this.$filteredComplicationByCategories.add(complication);
                        }
                    }
                }
                this.this$0.f22733a.f22710i.mo2280a(this.$filteredComplicationByCategories);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public C6441xaa77700e(com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter complicationsPresenter) {
        this.f22733a = complicationsPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.portfolio.platform.data.model.diana.Complication>> apply(java.lang.String str) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String l = com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter.f22706w;
        local.mo33255d(l, "transform from category to list complication with category=" + str);
        com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel g = com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter.m33908g(this.f22733a);
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str, "category");
        java.util.List<com.portfolio.platform.data.model.diana.Complication> b = g.mo40963b(str);
        java.util.ArrayList arrayList = new java.util.ArrayList();
        com.portfolio.platform.data.model.diana.preset.DianaPreset a = com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter.m33908g(this.f22733a).mo40964c().mo2275a();
        com.portfolio.platform.data.model.diana.Complication a2 = com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter.m33908g(this.f22733a).mo40970f().mo2275a();
        if (a2 != null) {
            java.lang.String complicationId = a2.getComplicationId();
            com.fossil.blesdk.obfuscated.zg4 m = this.f22733a.mo31443e();
            com.portfolio.platform.uirenew.home.customize.diana.complications.C6441xaa77700e.C64421 r2 = new com.portfolio.platform.uirenew.home.customize.diana.complications.C6441xaa77700e.C64421(this, a, b, complicationId, arrayList, (com.fossil.blesdk.obfuscated.yb4) null);
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(m, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, r2, 3, (java.lang.Object) null);
            return this.f22733a.f22710i;
        }
        com.fossil.blesdk.obfuscated.kd4.m24405a();
        throw null;
    }
}
