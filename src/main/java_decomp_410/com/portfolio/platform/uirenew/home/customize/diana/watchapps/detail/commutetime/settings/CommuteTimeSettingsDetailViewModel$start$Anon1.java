package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.google.android.libraries.places.api.Places;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel$start$Anon1", f = "CommuteTimeSettingsDetailViewModel.kt", l = {}, m = "invokeSuspend")
public final class CommuteTimeSettingsDetailViewModel$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeSettingsDetailViewModel this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsDetailViewModel$start$Anon1(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = commuteTimeSettingsDetailViewModel;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CommuteTimeSettingsDetailViewModel$start$Anon1 commuteTimeSettingsDetailViewModel$start$Anon1 = new CommuteTimeSettingsDetailViewModel$start$Anon1(this.this$Anon0, yb4);
        commuteTimeSettingsDetailViewModel$start$Anon1.p$ = (zg4) obj;
        return commuteTimeSettingsDetailViewModel$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CommuteTimeSettingsDetailViewModel$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            this.this$Anon0.c = Places.createClient(PortfolioApp.W.c());
            AddressWrapper a = this.this$Anon0.d;
            Boolean bool = null;
            boolean z = (a != null ? a.getType() : null) == AddressWrapper.AddressType.OTHER;
            CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.this$Anon0;
            AddressWrapper a2 = commuteTimeSettingsDetailViewModel.d;
            String name = a2 != null ? a2.getName() : null;
            Boolean a3 = dc4.a(z);
            AddressWrapper a4 = this.this$Anon0.d;
            String address = a4 != null ? a4.getAddress() : null;
            AddressWrapper a5 = this.this$Anon0.d;
            if (a5 != null) {
                bool = dc4.a(a5.getAvoidTolls());
            }
            CommuteTimeSettingsDetailViewModel.a(commuteTimeSettingsDetailViewModel, name, address, bool, a3, (Boolean) null, this.this$Anon0.c, (Boolean) null, 80, (Object) null);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
