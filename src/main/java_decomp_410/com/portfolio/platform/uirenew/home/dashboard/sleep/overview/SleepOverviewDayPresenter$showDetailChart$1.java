package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$showDetailChart$1", mo27670f = "SleepOverviewDayPresenter.kt", mo27671l = {159}, mo27672m = "invokeSuspend")
public final class SleepOverviewDayPresenter$showDetailChart$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23479p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepOverviewDayPresenter$showDetailChart$1(com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter sleepOverviewDayPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = sleepOverviewDayPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$showDetailChart$1 sleepOverviewDayPresenter$showDetailChart$1 = new com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$showDetailChart$1(this.this$0, yb4);
        sleepOverviewDayPresenter$showDetailChart$1.f23479p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return sleepOverviewDayPresenter$showDetailChart$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$showDetailChart$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23479p$;
            com.fossil.blesdk.obfuscated.os3 os3 = (com.fossil.blesdk.obfuscated.os3) this.this$0.f23472j.mo2275a();
            if (os3 != null) {
                java.util.List list = (java.util.List) os3.mo29975d();
                if (list != null) {
                    com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
                    com.portfolio.platform.uirenew.home.dashboard.sleep.overview.C6662xbacd57ab sleepOverviewDayPresenter$showDetailChart$1$invokeSuspend$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.dashboard.sleep.overview.C6662xbacd57ab(list, (com.fossil.blesdk.obfuscated.yb4) null, this);
                    this.L$0 = zg4;
                    this.L$1 = list;
                    this.label = 1;
                    obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, sleepOverviewDayPresenter$showDetailChart$1$invokeSuspend$$inlined$let$lambda$1, this);
                    if (obj == a) {
                        return a;
                    }
                }
            }
            this.this$0.f23473k.mo27369s(new java.util.ArrayList());
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            java.util.List list2 = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        java.util.List list3 = (java.util.List) obj;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d("SleepOverviewDayPresenter", "showDetailChart - data=" + list3);
        this.this$0.f23473k.mo27369s(list3);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
