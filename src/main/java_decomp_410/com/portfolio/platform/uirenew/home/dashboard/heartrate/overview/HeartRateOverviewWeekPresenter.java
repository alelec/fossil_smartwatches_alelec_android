package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import android.content.Context;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.tc3;
import com.fossil.blesdk.obfuscated.uc3;
import com.fossil.blesdk.obfuscated.vc3;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateOverviewWeekPresenter extends tc3 {
    @DexIgnore
    public Date f;
    @DexIgnore
    public LiveData<os3<List<DailyHeartRateSummary>>> g;
    @DexIgnore
    public List<String> h; // = new ArrayList();
    @DexIgnore
    public /* final */ uc3 i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public HeartRateOverviewWeekPresenter(uc3 uc3, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        kd4.b(uc3, "mView");
        kd4.b(userRepository, "userRepository");
        kd4.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        this.i = uc3;
        this.j = userRepository;
        this.k = heartRateSummaryRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Date c(HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter) {
        Date date = heartRateOverviewWeekPresenter.f;
        if (date != null) {
            return date;
        }
        kd4.d("mDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewWeekPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HeartRateOverviewWeekPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        MFLogger.d("HeartRateOverviewWeekPresenter", "stop");
        LiveData<os3<List<DailyHeartRateSummary>>> liveData = this.g;
        if (liveData != null) {
            uc3 uc3 = this.i;
            if (uc3 != null) {
                liveData.a((LifecycleOwner) (vc3) uc3);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekFragment");
        }
    }

    @DexIgnore
    public final void h() {
        this.h.clear();
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "calendar");
        Date date = this.f;
        if (date != null) {
            instance.setTime(date);
            instance.add(5, -6);
            for (int i2 = 1; i2 <= 7; i2++) {
                Boolean s = rk2.s(instance.getTime());
                kd4.a((Object) s, "DateHelper.isToday(calendar.time)");
                if (!s.booleanValue()) {
                    switch (instance.get(7)) {
                        case 1:
                            List<String> list = this.h;
                            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__S);
                            kd4.a((Object) a2, "LanguageHelper.getString\u2026Main_Steps7days_Label__S)");
                            list.add(a2);
                            break;
                        case 2:
                            List<String> list2 = this.h;
                            String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__M);
                            kd4.a((Object) a3, "LanguageHelper.getString\u2026Main_Steps7days_Label__M)");
                            list2.add(a3);
                            break;
                        case 3:
                            List<String> list3 = this.h;
                            String a4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__T);
                            kd4.a((Object) a4, "LanguageHelper.getString\u2026Main_Steps7days_Label__T)");
                            list3.add(a4);
                            break;
                        case 4:
                            List<String> list4 = this.h;
                            String a5 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__W);
                            kd4.a((Object) a5, "LanguageHelper.getString\u2026Main_Steps7days_Label__W)");
                            list4.add(a5);
                            break;
                        case 5:
                            List<String> list5 = this.h;
                            String a6 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__T_1);
                            kd4.a((Object) a6, "LanguageHelper.getString\u2026in_Steps7days_Label__T_1)");
                            list5.add(a6);
                            break;
                        case 6:
                            List<String> list6 = this.h;
                            String a7 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__F);
                            kd4.a((Object) a7, "LanguageHelper.getString\u2026Main_Steps7days_Label__F)");
                            list6.add(a7);
                            break;
                        case 7:
                            List<String> list7 = this.h;
                            String a8 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__S_1);
                            kd4.a((Object) a8, "LanguageHelper.getString\u2026in_Steps7days_Label__S_1)");
                            list7.add(a8);
                            break;
                    }
                } else {
                    List<String> list8 = this.h;
                    String a9 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_Sleep7days_Label__Today);
                    kd4.a((Object) a9, "LanguageHelper.getString\u2026_Sleep7days_Label__Today)");
                    list8.add(a9);
                }
                instance.add(5, 1);
            }
            return;
        }
        kd4.d("mDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        this.i.a(this);
    }

    @DexIgnore
    public final Pair<Date, Date> a(Date date) {
        Date b = rk2.b(date, 6);
        MFUser currentUser = this.j.getCurrentUser();
        if (currentUser != null) {
            Date d = rk2.d(currentUser.getCreatedAt());
            if (!rk2.b(b, d)) {
                b = d;
            }
        }
        return new Pair<>(b, date);
    }
}
