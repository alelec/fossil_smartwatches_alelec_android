package com.portfolio.platform.uirenew.home.dashboard.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1", mo27670f = "DashboardGoalTrackingPresenter.kt", mo27671l = {61}, mo27672m = "invokeSuspend")
public final class DashboardGoalTrackingPresenter$initDataSource$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23332p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1$a")
    /* renamed from: com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1$a */
    public static final class C6621a<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.C2723qd<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1 f23333a;

        @DexIgnore
        public C6621a(com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1 dashboardGoalTrackingPresenter$initDataSource$1) {
            this.f23333a = dashboardGoalTrackingPresenter$initDataSource$1;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(com.fossil.blesdk.obfuscated.C2723qd<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary> qdVar) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("getSummariesPaging observer size=");
            sb.append(qdVar != null ? java.lang.Integer.valueOf(qdVar.size()) : null);
            local.mo33255d("DashboardGoalTrackingPresenter", sb.toString());
            if (qdVar != null) {
                this.f23333a.this$0.mo41283k().mo25676a(qdVar);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardGoalTrackingPresenter$initDataSource$1(com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = dashboardGoalTrackingPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1 dashboardGoalTrackingPresenter$initDataSource$1 = new com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1(this.this$0, yb4);
        dashboardGoalTrackingPresenter$initDataSource$1.f23332p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dashboardGoalTrackingPresenter$initDataSource$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23332p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1$user$1 dashboardGoalTrackingPresenter$initDataSource$1$user$1 = new com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1$user$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, dashboardGoalTrackingPresenter$initDataSource$1$user$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) obj;
        if (mFUser != null) {
            java.util.Date d = com.fossil.blesdk.obfuscated.rk2.m27394d(mFUser.getCreatedAt());
            com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter = this.this$0;
            com.portfolio.platform.data.source.GoalTrackingRepository e = dashboardGoalTrackingPresenter.f23327i;
            com.portfolio.platform.data.source.GoalTrackingRepository e2 = this.this$0.f23327i;
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao c = this.this$0.f23329k;
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase d2 = this.this$0.f23330l;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) d, "createdDate");
            dashboardGoalTrackingPresenter.f23325g = e.getSummariesPaging(e2, c, d2, d, this.this$0.f23331m, this.this$0);
            com.portfolio.platform.data.Listing f = this.this$0.f23325g;
            if (f != null) {
                androidx.lifecycle.LiveData pagedList = f.getPagedList();
                if (pagedList != null) {
                    com.fossil.blesdk.obfuscated.za3 k = this.this$0.mo41283k();
                    if (k != null) {
                        pagedList.mo2277a((com.fossil.blesdk.obfuscated.ab3) k, new com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1.C6621a(this));
                    } else {
                        throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment");
                    }
                }
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
