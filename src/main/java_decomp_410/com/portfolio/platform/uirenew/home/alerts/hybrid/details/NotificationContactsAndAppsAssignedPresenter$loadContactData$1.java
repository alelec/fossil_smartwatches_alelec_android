package com.portfolio.platform.uirenew.home.alerts.hybrid.details;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationContactsAndAppsAssignedPresenter$loadContactData$1 implements com.fossil.blesdk.obfuscated.i62.C4439d<com.fossil.blesdk.obfuscated.t03.C5127d, com.fossil.blesdk.obfuscated.t03.C5125b> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter f22534a;

    @DexIgnore
    public NotificationContactsAndAppsAssignedPresenter$loadContactData$1(com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter) {
        this.f22534a = notificationContactsAndAppsAssignedPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(com.fossil.blesdk.obfuscated.t03.C5127d dVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f22534a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.alerts.hybrid.details.C6373x5a73489(this, dVar, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo28250a(com.fossil.blesdk.obfuscated.t03.C5125b bVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bVar, "errorValue");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter.f22508x.mo40900a(), "mGetAllHybridContactGroups onError");
        this.f22534a.f22518p.mo27920d();
    }
}
