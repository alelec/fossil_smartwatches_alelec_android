package com.portfolio.platform.uirenew.home.details.heartrate;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1", mo27670f = "HeartRateDetailPresenter.kt", mo27671l = {205, 209, 218, 225}, mo27672m = "invokeSuspend")
public final class HeartRateDetailPresenter$showDetailChart$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int I$0;
    @DexIgnore
    public long J$0;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23729p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$1", mo27670f = "HeartRateDetailPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$1 */
    public static final class C67251 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23730p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$1$a")
        /* renamed from: com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$1$a */
        public static final class C6726a<T> implements java.util.Comparator<T> {
            @DexIgnore
            public final int compare(T t, T t2) {
                return com.fossil.blesdk.obfuscated.wb4.m29575a(java.lang.Long.valueOf(((com.portfolio.platform.data.model.diana.heartrate.HeartRateSample) t).getStartTimeId().getMillis()), java.lang.Long.valueOf(((com.portfolio.platform.data.model.diana.heartrate.HeartRateSample) t2).getStartTimeId().getMillis()));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C67251(com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1 heartRateDetailPresenter$showDetailChart$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = heartRateDetailPresenter$showDetailChart$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1.C67251 r0 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1.C67251(this.this$0, yb4);
            r0.f23730p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1.C67251) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                java.util.List f = this.this$0.this$0.f23708l;
                if (f == null) {
                    return null;
                }
                if (f.size() > 1) {
                    com.fossil.blesdk.obfuscated.gb4.m22644a(f, new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1.C67251.C6726a());
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$2")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$2", mo27670f = "HeartRateDetailPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$2 */
    public static final class C67272 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $listTimeZoneChange;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $listTodayHeartRateModel;
        @DexIgnore
        public /* final */ /* synthetic */ int $maxHR;
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $previousTimeZoneOffset;
        @DexIgnore
        public /* final */ /* synthetic */ long $startOfDay;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23731p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C67272(com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1 heartRateDetailPresenter$showDetailChart$1, long j, kotlin.jvm.internal.Ref$IntRef ref$IntRef, java.util.List list, java.util.List list2, int i, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = heartRateDetailPresenter$showDetailChart$1;
            this.$startOfDay = j;
            this.$previousTimeZoneOffset = ref$IntRef;
            this.$listTimeZoneChange = list;
            this.$listTodayHeartRateModel = list2;
            this.$maxHR = i;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1.C67272 r1 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1.C67272(this.this$0, this.$startOfDay, this.$previousTimeZoneOffset, this.$listTimeZoneChange, this.$listTodayHeartRateModel, this.$maxHR, yb4);
            r1.f23731p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r1;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1.C67272) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.util.Iterator it;
            char c;
            java.lang.StringBuilder sb;
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                java.util.List f = this.this$0.this$0.f23708l;
                if (f == null) {
                    return null;
                }
                for (java.util.Iterator it2 = f.iterator(); it2.hasNext(); it2 = it) {
                    com.portfolio.platform.data.model.diana.heartrate.HeartRateSample heartRateSample = (com.portfolio.platform.data.model.diana.heartrate.HeartRateSample) it2.next();
                    long j = (long) 60000;
                    long millis = (heartRateSample.getStartTimeId().getMillis() - this.$startOfDay) / j;
                    long millis2 = (heartRateSample.getEndTime().getMillis() - this.$startOfDay) / j;
                    long j2 = (millis2 + millis) / ((long) 2);
                    if (heartRateSample.getTimezoneOffsetInSecond() != this.$previousTimeZoneOffset.element) {
                        int hourOfDay = heartRateSample.getStartTimeId().getHourOfDay();
                        java.lang.String a = com.fossil.blesdk.obfuscated.ml2.m25355a(hourOfDay);
                        com.fossil.blesdk.obfuscated.pd4 pd4 = com.fossil.blesdk.obfuscated.pd4.f17594a;
                        java.lang.Object[] objArr = {com.fossil.blesdk.obfuscated.dc4.m20843a(java.lang.Math.abs(heartRateSample.getTimezoneOffsetInSecond() / 3600)), com.fossil.blesdk.obfuscated.dc4.m20843a(java.lang.Math.abs((heartRateSample.getTimezoneOffsetInSecond() / 60) % 60))};
                        java.lang.String format = java.lang.String.format("%02d:%02d", java.util.Arrays.copyOf(objArr, objArr.length));
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) format, "java.lang.String.format(format, *args)");
                        java.util.List list = this.$listTimeZoneChange;
                        java.lang.Integer a2 = com.fossil.blesdk.obfuscated.dc4.m20843a((int) j2);
                        it = it2;
                        kotlin.Pair pair = new kotlin.Pair(com.fossil.blesdk.obfuscated.dc4.m20843a(hourOfDay), com.fossil.blesdk.obfuscated.dc4.m20842a(((float) heartRateSample.getTimezoneOffsetInSecond()) / 3600.0f));
                        java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
                        sb2.append(a);
                        if (heartRateSample.getTimezoneOffsetInSecond() >= 0) {
                            sb = new java.lang.StringBuilder();
                            c = '+';
                        } else {
                            sb = new java.lang.StringBuilder();
                            c = '-';
                        }
                        sb.append(c);
                        sb.append(format);
                        sb2.append(sb.toString());
                        list.add(new kotlin.Triple(a2, pair, sb2.toString()));
                        this.$previousTimeZoneOffset.element = heartRateSample.getTimezoneOffsetInSecond();
                    } else {
                        it = it2;
                    }
                    if (!this.$listTodayHeartRateModel.isEmpty()) {
                        com.fossil.blesdk.obfuscated.rt3 rt3 = (com.fossil.blesdk.obfuscated.rt3) com.fossil.blesdk.obfuscated.kb4.m24385f(this.$listTodayHeartRateModel);
                        if (millis - ((long) rt3.mo30751a()) > 1) {
                            com.fossil.blesdk.obfuscated.rt3 rt32 = new com.fossil.blesdk.obfuscated.rt3(0, 0, 0, rt3.mo30751a(), (int) millis, (int) j2);
                            this.$listTodayHeartRateModel.add(rt32);
                        }
                    }
                    int average = (int) heartRateSample.getAverage();
                    if (heartRateSample.getMax() == this.$maxHR) {
                        average = heartRateSample.getMax();
                    }
                    com.fossil.blesdk.obfuscated.rt3 rt33 = new com.fossil.blesdk.obfuscated.rt3(average, heartRateSample.getMin(), heartRateSample.getMax(), (int) millis, (int) millis2, (int) j2);
                    this.$listTodayHeartRateModel.add(rt33);
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$showDetailChart$1(com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter heartRateDetailPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = heartRateDetailPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1 heartRateDetailPresenter$showDetailChart$1 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1(this.this$0, yb4);
        heartRateDetailPresenter$showDetailChart$1.f23729p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return heartRateDetailPresenter$showDetailChart$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x013d A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x013e  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x019c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01c3  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.util.ArrayList arrayList;
        java.util.List list;
        int i;
        int i2;
        long j;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.util.List list2;
        java.lang.Object obj2;
        com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1.C67272 r24;
        com.fossil.blesdk.obfuscated.ug4 a;
        com.fossil.blesdk.obfuscated.zg4 zg42;
        java.util.List list3;
        long j2;
        java.lang.Object obj3;
        com.fossil.blesdk.obfuscated.zg4 zg43;
        java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i3 = this.label;
        if (i3 == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg43 = this.f23729p$;
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HeartRateDetailPresenter", "showDetailChart");
            com.fossil.blesdk.obfuscated.ug4 a3 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$summary$1 heartRateDetailPresenter$showDetailChart$1$summary$1 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$summary$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg43;
            this.label = 1;
            obj3 = com.fossil.blesdk.obfuscated.yf4.m30997a(a3, heartRateDetailPresenter$showDetailChart$1$summary$1, this);
            if (obj3 == a2) {
                return a2;
            }
        } else if (i3 == 1) {
            zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            obj3 = obj;
        } else if (i3 == 2) {
            list3 = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            java.util.ArrayList arrayList2 = new java.util.ArrayList();
            if (this.this$0.f23708l != null) {
                java.util.List f = this.this$0.f23708l;
                if (f == null) {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                } else if (!f.isEmpty()) {
                    java.util.List f2 = this.this$0.f23708l;
                    if (f2 != null) {
                        org.joda.time.DateTime withTimeAtStartOfDay = ((com.portfolio.platform.data.model.diana.heartrate.HeartRateSample) f2.get(0)).getStartTimeId().withTimeAtStartOfDay();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) withTimeAtStartOfDay, "mHeartRateSamplesOfDate!\u2026().withTimeAtStartOfDay()");
                        j2 = withTimeAtStartOfDay.getMillis();
                        com.fossil.blesdk.obfuscated.ug4 a4 = this.this$0.mo31440b();
                        com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$maxHR$1 heartRateDetailPresenter$showDetailChart$1$maxHR$1 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$maxHR$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
                        this.L$0 = zg42;
                        this.L$1 = list3;
                        this.L$2 = arrayList2;
                        this.J$0 = j2;
                        this.label = 3;
                        obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(a4, heartRateDetailPresenter$showDetailChart$1$maxHR$1, this);
                        if (obj2 == a2) {
                            return a2;
                        }
                        arrayList = arrayList2;
                        j = j2;
                        list2 = list3;
                        zg4 = zg42;
                        int intValue = ((java.lang.Number) obj2).intValue();
                        kotlin.jvm.internal.Ref$IntRef ref$IntRef = new kotlin.jvm.internal.Ref$IntRef();
                        ref$IntRef.element = -1;
                        java.util.ArrayList arrayList3 = new java.util.ArrayList();
                        r24 = r0;
                        a = this.this$0.mo31440b();
                        kotlin.jvm.internal.Ref$IntRef ref$IntRef2 = ref$IntRef;
                        java.util.ArrayList arrayList4 = arrayList3;
                        int i4 = intValue;
                        com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1.C67272 r0 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1.C67272(this, j, ref$IntRef2, arrayList4, arrayList, i4, (com.fossil.blesdk.obfuscated.yb4) null);
                        this.L$0 = zg4;
                        this.L$1 = list2;
                        this.L$2 = arrayList;
                        this.J$0 = j;
                        this.I$0 = i4;
                        this.L$3 = ref$IntRef2;
                        list = arrayList4;
                        this.L$4 = list;
                        this.label = 4;
                        if (com.fossil.blesdk.obfuscated.yf4.m30997a(a, r24, this) == a2) {
                        }
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        local.mo33255d("HeartRateDetailPresenter", "listTimeZoneChange=" + com.fossil.blesdk.obfuscated.rj2.m27347a(list));
                        if (!list.isEmpty()) {
                        }
                        list.clear();
                        i = 0;
                        int i5 = i + org.joda.time.DateTimeConstants.MINUTES_PER_DAY;
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        local2.mo33255d("HeartRateDetailPresenter", "minutesOfDay=" + i5);
                        this.this$0.f23713q.mo29608a(i5, (java.util.List<com.fossil.blesdk.obfuscated.rt3>) arrayList, (java.util.List<kotlin.Triple<java.lang.Integer, kotlin.Pair<java.lang.Integer, java.lang.Float>, java.lang.String>>) list);
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            }
            java.util.Date n = com.fossil.blesdk.obfuscated.rk2.m27409n(this.this$0.f23703g);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) n, "DateHelper.getStartOfDay(mDate)");
            j2 = n.getTime();
            com.fossil.blesdk.obfuscated.ug4 a42 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$maxHR$1 heartRateDetailPresenter$showDetailChart$1$maxHR$12 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$maxHR$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg42;
            this.L$1 = list3;
            this.L$2 = arrayList2;
            this.J$0 = j2;
            this.label = 3;
            obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(a42, heartRateDetailPresenter$showDetailChart$1$maxHR$12, this);
            if (obj2 == a2) {
            }
        } else if (i3 == 3) {
            long j3 = this.J$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            j = j3;
            arrayList = (java.util.List) this.L$2;
            list2 = (java.util.List) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            obj2 = obj;
            int intValue2 = ((java.lang.Number) obj2).intValue();
            kotlin.jvm.internal.Ref$IntRef ref$IntRef3 = new kotlin.jvm.internal.Ref$IntRef();
            ref$IntRef3.element = -1;
            java.util.ArrayList arrayList32 = new java.util.ArrayList();
            r24 = r0;
            a = this.this$0.mo31440b();
            kotlin.jvm.internal.Ref$IntRef ref$IntRef22 = ref$IntRef3;
            java.util.ArrayList arrayList42 = arrayList32;
            int i42 = intValue2;
            com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1.C67272 r02 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1.C67272(this, j, ref$IntRef22, arrayList42, arrayList, i42, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = list2;
            this.L$2 = arrayList;
            this.J$0 = j;
            this.I$0 = i42;
            this.L$3 = ref$IntRef22;
            list = arrayList42;
            this.L$4 = list;
            this.label = 4;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(a, r24, this) == a2) {
                return a2;
            }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local3.mo33255d("HeartRateDetailPresenter", "listTimeZoneChange=" + com.fossil.blesdk.obfuscated.rj2.m27347a(list));
            if (!list.isEmpty()) {
            }
            list.clear();
            i = 0;
            int i52 = i + org.joda.time.DateTimeConstants.MINUTES_PER_DAY;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local22 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local22.mo33255d("HeartRateDetailPresenter", "minutesOfDay=" + i52);
            this.this$0.f23713q.mo29608a(i52, (java.util.List<com.fossil.blesdk.obfuscated.rt3>) arrayList, (java.util.List<kotlin.Triple<java.lang.Integer, kotlin.Pair<java.lang.Integer, java.lang.Float>, java.lang.String>>) list);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i3 == 4) {
            list = (java.util.List) this.L$4;
            kotlin.jvm.internal.Ref$IntRef ref$IntRef4 = (kotlin.jvm.internal.Ref$IntRef) this.L$3;
            java.util.List list4 = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg44 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            arrayList = (java.util.List) this.L$2;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local32 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local32.mo33255d("HeartRateDetailPresenter", "listTimeZoneChange=" + com.fossil.blesdk.obfuscated.rj2.m27347a(list));
            if ((!list.isEmpty()) || list.size() <= 1) {
                list.clear();
                i = 0;
            } else {
                float floatValue = ((java.lang.Number) ((kotlin.Pair) ((kotlin.Triple) list.get(0)).getSecond()).getSecond()).floatValue();
                float floatValue2 = ((java.lang.Number) ((kotlin.Pair) ((kotlin.Triple) list.get(com.fossil.blesdk.obfuscated.cb4.m20535a(list))).getSecond()).getSecond()).floatValue();
                float f3 = (float) 0;
                if ((floatValue >= f3 || floatValue2 >= f3) && (floatValue < f3 || floatValue2 < f3)) {
                    i2 = java.lang.Math.abs((int) ((floatValue - floatValue2) * ((float) 60)));
                    if (floatValue2 < f3) {
                        i2 = -i2;
                    }
                } else {
                    i2 = (int) ((floatValue - floatValue2) * ((float) 60));
                }
                i = i2;
                java.util.ArrayList<kotlin.Triple> arrayList5 = new java.util.ArrayList<>();
                for (java.lang.Object next : list) {
                    if (com.fossil.blesdk.obfuscated.dc4.m20839a(((java.lang.Number) ((kotlin.Pair) ((kotlin.Triple) next).getSecond()).getSecond()).floatValue() == floatValue).booleanValue()) {
                        arrayList5.add(next);
                    }
                }
                java.util.ArrayList<kotlin.Pair> arrayList6 = new java.util.ArrayList<>(com.fossil.blesdk.obfuscated.db4.m20831a(arrayList5, 10));
                for (kotlin.Triple second : arrayList5) {
                    arrayList6.add((kotlin.Pair) second.getSecond());
                }
                java.util.ArrayList arrayList7 = new java.util.ArrayList(com.fossil.blesdk.obfuscated.db4.m20831a(arrayList6, 10));
                for (kotlin.Pair first : arrayList6) {
                    arrayList7.add(com.fossil.blesdk.obfuscated.dc4.m20843a(((java.lang.Number) first.getFirst()).intValue()));
                }
                if (!arrayList7.contains(com.fossil.blesdk.obfuscated.dc4.m20843a(0))) {
                    java.lang.String a5 = this.this$0.mo41416a(com.fossil.blesdk.obfuscated.dc4.m20842a(floatValue));
                    java.lang.Integer a6 = com.fossil.blesdk.obfuscated.dc4.m20843a(0);
                    kotlin.Pair pair = new kotlin.Pair(com.fossil.blesdk.obfuscated.dc4.m20843a(0), com.fossil.blesdk.obfuscated.dc4.m20842a(floatValue));
                    list.add(0, new kotlin.Triple(a6, pair, "12a" + a5));
                }
                java.util.ArrayList<kotlin.Triple> arrayList8 = new java.util.ArrayList<>();
                for (java.lang.Object next2 : list) {
                    if (com.fossil.blesdk.obfuscated.dc4.m20839a(((java.lang.Number) ((kotlin.Pair) ((kotlin.Triple) next2).getSecond()).getSecond()).floatValue() == floatValue2).booleanValue()) {
                        arrayList8.add(next2);
                    }
                }
                java.util.ArrayList<kotlin.Pair> arrayList9 = new java.util.ArrayList<>(com.fossil.blesdk.obfuscated.db4.m20831a(arrayList8, 10));
                for (kotlin.Triple second2 : arrayList8) {
                    arrayList9.add((kotlin.Pair) second2.getSecond());
                }
                java.util.ArrayList arrayList10 = new java.util.ArrayList(com.fossil.blesdk.obfuscated.db4.m20831a(arrayList9, 10));
                for (kotlin.Pair first2 : arrayList9) {
                    arrayList10.add(com.fossil.blesdk.obfuscated.dc4.m20843a(((java.lang.Number) first2.getFirst()).intValue()));
                }
                if (!arrayList10.contains(com.fossil.blesdk.obfuscated.dc4.m20843a(24))) {
                    java.lang.String a7 = this.this$0.mo41416a(com.fossil.blesdk.obfuscated.dc4.m20842a(floatValue2));
                    java.lang.Integer a8 = com.fossil.blesdk.obfuscated.dc4.m20843a(i + org.joda.time.DateTimeConstants.MINUTES_PER_DAY);
                    kotlin.Pair pair2 = new kotlin.Pair(com.fossil.blesdk.obfuscated.dc4.m20843a(24), com.fossil.blesdk.obfuscated.dc4.m20842a(floatValue2));
                    list.add(new kotlin.Triple(a8, pair2, "12a" + a7));
                }
            }
            int i522 = i + org.joda.time.DateTimeConstants.MINUTES_PER_DAY;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local222.mo33255d("HeartRateDetailPresenter", "minutesOfDay=" + i522);
            this.this$0.f23713q.mo29608a(i522, (java.util.List<com.fossil.blesdk.obfuscated.rt3>) arrayList, (java.util.List<kotlin.Triple<java.lang.Integer, kotlin.Pair<java.lang.Integer, java.lang.Float>, java.lang.String>>) list);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        java.util.List list5 = (java.util.List) obj3;
        if (this.this$0.f23708l == null || (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23708l, (java.lang.Object) list5))) {
            this.this$0.f23708l = list5;
            com.fossil.blesdk.obfuscated.ug4 a9 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1.C67251 r6 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1.C67251(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg43;
            this.L$1 = list5;
            this.label = 2;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(a9, r6, this) == a2) {
                return a2;
            }
            java.util.List list6 = list5;
            zg42 = zg43;
            list3 = list6;
            java.util.ArrayList arrayList22 = new java.util.ArrayList();
            if (this.this$0.f23708l != null) {
            }
            java.util.Date n2 = com.fossil.blesdk.obfuscated.rk2.m27409n(this.this$0.f23703g);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) n2, "DateHelper.getStartOfDay(mDate)");
            j2 = n2.getTime();
            com.fossil.blesdk.obfuscated.ug4 a422 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$maxHR$1 heartRateDetailPresenter$showDetailChart$1$maxHR$122 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$maxHR$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg42;
            this.L$1 = list3;
            this.L$2 = arrayList22;
            this.J$0 = j2;
            this.label = 3;
            obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(a422, heartRateDetailPresenter$showDetailChart$1$maxHR$122, this);
            if (obj2 == a2) {
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
