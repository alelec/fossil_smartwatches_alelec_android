package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.ec3;
import com.fossil.blesdk.obfuscated.fc3;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.gc3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.rk2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.enums.Status;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateOverviewDayPresenter extends ec3 {
    @DexIgnore
    public Date f;
    @DexIgnore
    public LiveData<os3<List<HeartRateSample>>> g; // = new MutableLiveData();
    @DexIgnore
    public LiveData<os3<List<WorkoutSession>>> h; // = new MutableLiveData();
    @DexIgnore
    public /* final */ fc3 i;
    @DexIgnore
    public /* final */ HeartRateSampleRepository j;
    @DexIgnore
    public /* final */ WorkoutSessionRepository k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<os3<? extends List<WorkoutSession>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewDayPresenter a;

        @DexIgnore
        public b(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
            this.a = heartRateOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(os3<? extends List<WorkoutSession>> os3) {
            Status a2 = os3.a();
            List list = (List) os3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mWorkoutSessions -- workoutSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("HeartRateOverviewDayPresenter", sb.toString());
            if (a2 == Status.DATABASE_LOADING) {
                return;
            }
            if (list == null || list.isEmpty()) {
                this.a.i.a(false, new ArrayList());
            } else {
                this.a.i.a(true, list);
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public HeartRateOverviewDayPresenter(fc3 fc3, HeartRateSampleRepository heartRateSampleRepository, WorkoutSessionRepository workoutSessionRepository) {
        kd4.b(fc3, "mView");
        kd4.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        kd4.b(workoutSessionRepository, "mWorkoutSessionRepository");
        this.i = fc3;
        this.j = heartRateSampleRepository;
        this.k = workoutSessionRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Date b(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
        Date date = heartRateOverviewDayPresenter.f;
        if (date != null) {
            return date;
        }
        kd4.d("mDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        h();
        LiveData<os3<List<HeartRateSample>>> liveData = this.g;
        fc3 fc3 = this.i;
        if (fc3 != null) {
            liveData.a((gc3) fc3, new HeartRateOverviewDayPresenter$start$Anon1(this));
            this.h.a((LifecycleOwner) this.i, new b(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewDayPresenter", "stop");
        LiveData<os3<List<HeartRateSample>>> liveData = this.g;
        fc3 fc3 = this.i;
        if (fc3 != null) {
            liveData.a((LifecycleOwner) (gc3) fc3);
            this.h.a((LifecycleOwner) this.i);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayFragment");
    }

    @DexIgnore
    public void h() {
        Date date = this.f;
        if (date != null) {
            if (date == null) {
                kd4.d("mDate");
                throw null;
            } else if (rk2.s(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.f;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("HeartRateOverviewDayPresenter", sb.toString());
                    return;
                }
                kd4.d("mDate");
                throw null;
            }
        }
        this.f = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.f;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("HeartRateOverviewDayPresenter", sb2.toString());
            HeartRateSampleRepository heartRateSampleRepository = this.j;
            Date date4 = this.f;
            if (date4 == null) {
                kd4.d("mDate");
                throw null;
            } else if (date4 != null) {
                this.g = heartRateSampleRepository.getHeartRateSamples(date4, date4, false);
                WorkoutSessionRepository workoutSessionRepository = this.k;
                Date date5 = this.f;
                if (date5 == null) {
                    kd4.d("mDate");
                    throw null;
                } else if (date5 != null) {
                    this.h = workoutSessionRepository.getWorkoutSessions(date5, date5, true);
                } else {
                    kd4.d("mDate");
                    throw null;
                }
            } else {
                kd4.d("mDate");
                throw null;
            }
        } else {
            kd4.d("mDate");
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        this.i.a(this);
    }

    @DexIgnore
    public final String a(Float f2) {
        if (f2 == null) {
            return "";
        }
        pd4 pd4 = pd4.a;
        Object[] objArr = {Integer.valueOf((int) Math.abs(f2.floatValue())), Integer.valueOf(((int) Math.abs(f2.floatValue() - (f2.floatValue() % ((float) 10)))) * 60)};
        String format = String.format("%02d:%02d", Arrays.copyOf(objArr, objArr.length));
        kd4.a((Object) format, "java.lang.String.format(format, *args)");
        if (f2.floatValue() >= ((float) 0)) {
            return '+' + format;
        }
        return '-' + format;
    }
}
