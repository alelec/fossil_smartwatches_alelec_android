package com.portfolio.platform.uirenew.home.customize.hybrid;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.source.HybridPresetRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeHybridCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ HybridPreset $newPreset;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeHybridCustomizePresenter$createNewPreset$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeHybridCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(HybridPreset hybridPreset, yb4 yb4, HomeHybridCustomizePresenter$createNewPreset$Anon1 homeHybridCustomizePresenter$createNewPreset$Anon1) {
        super(2, yb4);
        this.$newPreset = hybridPreset;
        this.this$Anon0 = homeHybridCustomizePresenter$createNewPreset$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeHybridCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 homeHybridCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new HomeHybridCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(this.$newPreset, yb4, this.this$Anon0);
        homeHybridCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return homeHybridCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeHybridCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            HybridPresetRepository e = this.this$Anon0.this$Anon0.q;
            HybridPreset hybridPreset = this.$newPreset;
            this.L$Anon0 = zg4;
            this.label = 1;
            if (e.upsertHybridPreset(hybridPreset, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
