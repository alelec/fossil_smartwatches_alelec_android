package com.portfolio.platform.uirenew.home.dashboard.activetime.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$maxValue$1", mo27670f = "ActiveTimeOverviewDayPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class ActiveTimeOverviewDayPresenter$showDetailChart$1$maxValue$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Integer>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.ArrayList $data;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23136p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActiveTimeOverviewDayPresenter$showDetailChart$1$maxValue$1(java.util.ArrayList arrayList, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$data = arrayList;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$maxValue$1 activeTimeOverviewDayPresenter$showDetailChart$1$maxValue$1 = new com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$maxValue$1(this.$data, yb4);
        activeTimeOverviewDayPresenter$showDetailChart$1$maxValue$1.f23136p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return activeTimeOverviewDayPresenter$showDetailChart$1$maxValue$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$maxValue$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object obj2;
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.util.Iterator it = this.$data.iterator();
            int i = 0;
            if (!it.hasNext()) {
                obj2 = null;
            } else {
                obj2 = it.next();
                if (it.hasNext()) {
                    java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList = ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a) obj2).mo40552c().get(0);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList, "it.mListOfBarPoints[0]");
                    int i2 = 0;
                    for (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b e : arrayList) {
                        i2 += com.fossil.blesdk.obfuscated.dc4.m20843a(e.mo40566e()).intValue();
                    }
                    java.lang.Integer a = com.fossil.blesdk.obfuscated.dc4.m20843a(i2);
                    do {
                        java.lang.Object next = it.next();
                        java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList2 = ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a) next).mo40552c().get(0);
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList2, "it.mListOfBarPoints[0]");
                        int i3 = 0;
                        for (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b e2 : arrayList2) {
                            i3 += com.fossil.blesdk.obfuscated.dc4.m20843a(e2.mo40566e()).intValue();
                        }
                        java.lang.Integer a2 = com.fossil.blesdk.obfuscated.dc4.m20843a(i3);
                        if (a.compareTo(a2) < 0) {
                            obj2 = next;
                            a = a2;
                        }
                    } while (it.hasNext());
                }
            }
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a aVar = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a) obj2;
            if (aVar == null) {
                return null;
            }
            java.util.ArrayList<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> c = aVar.mo40552c();
            if (c == null) {
                return null;
            }
            java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList3 = c.get(0);
            if (arrayList3 == null) {
                return null;
            }
            for (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b e3 : arrayList3) {
                i += com.fossil.blesdk.obfuscated.dc4.m20843a(e3.mo40566e()).intValue();
            }
            return com.fossil.blesdk.obfuscated.dc4.m20843a(i);
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
