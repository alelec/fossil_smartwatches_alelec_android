package com.portfolio.platform.uirenew.home.alerts.hybrid.details;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1$populateContact$1", f = "NotificationContactsAndAppsAssignedPresenter.kt", l = {}, m = "invokeSuspend")
public final class NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1$populateContact$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $contactWrapperList;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1$populateContact$1(com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1 notificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = notificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1;
        this.$contactWrapperList = list;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1$populateContact$1 notificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1$populateContact$1 = new com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1$populateContact$1(this.this$0, this.$contactWrapperList, yb4);
        notificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1$populateContact$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1$populateContact$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1$populateContact$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            for (com.fossil.wearables.fsl.contact.ContactGroup next : this.this$0.$responseValue.a()) {
                for (com.fossil.wearables.fsl.contact.Contact next2 : next.getContacts()) {
                    if (next.getHour() == this.this$0.this$0.a.q) {
                        com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper contactWrapper = new com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper(next2, "");
                        contactWrapper.setAdded(true);
                        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) next2, "contact");
                        com.fossil.wearables.fsl.contact.ContactGroup contactGroup = next2.getContactGroup();
                        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) contactGroup, "contact.contactGroup");
                        contactWrapper.setCurrentHandGroup(contactGroup.getHour());
                        com.fossil.wearables.fsl.contact.Contact contact = contactWrapper.getContact();
                        if (contact != null) {
                            contact.setDbRowId(next2.getDbRowId());
                            contact.setUseSms(next2.isUseSms());
                            contact.setUseCall(next2.isUseCall());
                        }
                        java.util.List<com.fossil.wearables.fsl.contact.PhoneNumber> phoneNumbers = next2.getPhoneNumbers();
                        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) phoneNumbers, "contact.phoneNumbers");
                        if (!phoneNumbers.isEmpty()) {
                            com.fossil.wearables.fsl.contact.PhoneNumber phoneNumber = next2.getPhoneNumbers().get(0);
                            com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) phoneNumber, "contact.phoneNumbers[0]");
                            java.lang.String number = phoneNumber.getNumber();
                            if (!android.text.TextUtils.isEmpty(number)) {
                                contactWrapper.setHasPhoneNumber(true);
                                contactWrapper.setPhoneNumber(number);
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                java.lang.String a = com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter.x.a();
                                local.d(a, "mGetAllHybridContactGroups filter selected contact, phoneNumber=" + number);
                            }
                        }
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a2 = com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter.x.a();
                        java.lang.StringBuilder sb = new java.lang.StringBuilder();
                        sb.append("mGetAllHybridContactGroups filter selected contact, hand=");
                        com.fossil.wearables.fsl.contact.ContactGroup contactGroup2 = next2.getContactGroup();
                        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) contactGroup2, "contact.contactGroup");
                        sb.append(contactGroup2.getHour());
                        sb.append(" ,rowId=");
                        sb.append(next2.getDbRowId());
                        sb.append(" ,isUseText=");
                        sb.append(next2.isUseSms());
                        sb.append(" ,isUseCall=");
                        sb.append(next2.isUseCall());
                        local2.d(a2, sb.toString());
                        this.$contactWrapperList.add(contactWrapper);
                        com.fossil.wearables.fsl.contact.Contact contact2 = contactWrapper.getContact();
                        if (contact2 == null || contact2.getContactId() != -100) {
                            com.fossil.wearables.fsl.contact.Contact contact3 = contactWrapper.getContact();
                            if (contact3 != null) {
                                if (contact3.getContactId() != -200) {
                                }
                            }
                        }
                        this.this$0.this$0.a.r().add(contactWrapper);
                    }
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
