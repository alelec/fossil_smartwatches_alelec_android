package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1", mo27670f = "NotificationCallsAndMessagesPresenter.kt", mo27671l = {310}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1 */
public final class C6329x2c0b6239 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<? extends com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter>>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22399p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6329x2c0b6239(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1 notificationCallsAndMessagesPresenter$setRuleToDevice$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = notificationCallsAndMessagesPresenter$setRuleToDevice$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.C6329x2c0b6239 notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.C6329x2c0b6239(this.this$0, yb4);
        notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.f22399p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.C6329x2c0b6239) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v50, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: java.util.List} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object obj2;
        com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.C6329x2c0b6239 notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1 = this;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.label;
        java.util.List list = null;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.f22399p$;
            com.fossil.blesdk.obfuscated.px2 e = notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.f22386v;
            notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.L$0 = zg4;
            notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.L$1 = null;
            notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.label = 1;
            obj2 = com.fossil.blesdk.obfuscated.w52.m29539a(e, null, notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1);
            if (obj2 == a) {
                return a;
            }
        } else if (i == 1) {
            list = notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            obj2 = obj;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.CoroutineUseCase.C5604c cVar = (com.portfolio.platform.CoroutineUseCase.C5604c) obj2;
        if (cVar instanceof com.fossil.blesdk.obfuscated.px2.C4879d) {
            list = new java.util.ArrayList();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("GetAllContactGroup onSuccess, size = ");
            com.fossil.blesdk.obfuscated.px2.C4879d dVar = (com.fossil.blesdk.obfuscated.px2.C4879d) cVar;
            sb.append(dVar.mo30277a().size());
            local.mo33255d(a2, sb.toString());
            notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.f22372h = com.fossil.blesdk.obfuscated.kb4.m24381d(dVar.mo30277a());
            if (notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.f22384t.mo29518E() == 0) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a(), "call everyone");
                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification = r8;
                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification2 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName.getAppName(), aApplicationName.getBundleCrc(), aApplicationName.getGroupId(), aApplicationName.getPackageName(), aApplicationName.getIconResourceId(), aApplicationName.getIconFwPath(), aApplicationName.getNotificationType());
                list.add(new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification));
            } else if (notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.f22384t.mo29518E() == 1) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a(), "call favorite");
                int size = notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.f22372h.size();
                for (int i2 = 0; i2 < size; i2++) {
                    com.fossil.wearables.fsl.contact.ContactGroup contactGroup = (com.fossil.wearables.fsl.contact.ContactGroup) notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.f22372h.get(i2);
                    com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName2 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                    com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification3 = r10;
                    com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification4 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName2.getAppName(), aApplicationName2.getBundleCrc(), aApplicationName2.getGroupId(), aApplicationName2.getPackageName(), aApplicationName2.getIconResourceId(), aApplicationName2.getIconFwPath(), aApplicationName2.getNotificationType());
                    com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter appNotificationFilter = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification3);
                    java.util.List<com.fossil.wearables.fsl.contact.Contact> contacts = contactGroup.getContacts();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contacts, "item.contacts");
                    if (!contacts.isEmpty()) {
                        com.fossil.wearables.fsl.contact.Contact contact = contactGroup.getContacts().get(0);
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact, "item.contacts[0]");
                        appNotificationFilter.setSender(contact.getDisplayName());
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a3 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a();
                        java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
                        sb2.append("getTypeAllowFromCall - item ");
                        sb2.append(i2);
                        sb2.append(" name = ");
                        com.fossil.wearables.fsl.contact.Contact contact2 = contactGroup.getContacts().get(0);
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact2, "item.contacts[0]");
                        sb2.append(contact2.getDisplayName());
                        local2.mo33255d(a3, sb2.toString());
                        list.add(appNotificationFilter);
                    }
                }
            } else {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a(), "call no one");
            }
            if (notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.f22384t.mo29519J() == 0) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a(), "message everyone");
                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName3 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.MESSAGES;
                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification5 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName3.getAppName(), aApplicationName3.getBundleCrc(), aApplicationName3.getGroupId(), aApplicationName3.getPackageName(), aApplicationName3.getIconResourceId(), aApplicationName3.getIconFwPath(), aApplicationName3.getNotificationType());
                list.add(new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification5));
            } else if (notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.f22384t.mo29519J() == 1) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a(), "message favorite");
                int size2 = notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.f22372h.size();
                int i3 = 0;
                while (i3 < size2) {
                    com.fossil.wearables.fsl.contact.ContactGroup contactGroup2 = (com.fossil.wearables.fsl.contact.ContactGroup) notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.f22372h.get(i3);
                    com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName4 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.MESSAGES;
                    com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification6 = r9;
                    com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification7 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName4.getAppName(), aApplicationName4.getBundleCrc(), aApplicationName4.getGroupId(), aApplicationName4.getPackageName(), aApplicationName4.getIconResourceId(), aApplicationName4.getIconFwPath(), aApplicationName4.getNotificationType());
                    com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter appNotificationFilter2 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification6);
                    java.util.List<com.fossil.wearables.fsl.contact.Contact> contacts2 = contactGroup2.getContacts();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contacts2, "item.contacts");
                    if (!contacts2.isEmpty()) {
                        com.fossil.wearables.fsl.contact.Contact contact3 = contactGroup2.getContacts().get(0);
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact3, "item.contacts[0]");
                        appNotificationFilter2.setSender(contact3.getDisplayName());
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a4 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a();
                        java.lang.StringBuilder sb3 = new java.lang.StringBuilder();
                        sb3.append("getTypeAllowFromMessage - item ");
                        sb3.append(i3);
                        sb3.append(" name = ");
                        com.fossil.wearables.fsl.contact.Contact contact4 = contactGroup2.getContacts().get(0);
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact4, "item.contacts[0]");
                        sb3.append(contact4.getDisplayName());
                        local3.mo33255d(a4, sb3.toString());
                        list.add(appNotificationFilter2);
                    }
                    i3++;
                    notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1 = this;
                }
            } else {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a(), "message no one");
            }
        } else if (cVar instanceof com.fossil.blesdk.obfuscated.px2.C4877b) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a(), "GetAllContactGroup onError");
        }
        return list;
    }
}
