package com.portfolio.platform.uirenew.home.dashboard;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.Device;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$Anon1", f = "HomeDashboardPresenter.kt", l = {102, 103, 104, 105}, m = "invokeSuspend")
public final class HomeDashboardPresenter$checkDeviceStatus$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDashboardPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDashboardPresenter$checkDeviceStatus$Anon1(HomeDashboardPresenter homeDashboardPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = homeDashboardPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeDashboardPresenter$checkDeviceStatus$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1 = new HomeDashboardPresenter$checkDeviceStatus$Anon1(this.this$Anon0, yb4);
        homeDashboardPresenter$checkDeviceStatus$Anon1.p$ = (zg4) obj;
        return homeDashboardPresenter$checkDeviceStatus$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDashboardPresenter$checkDeviceStatus$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b0 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00d1 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0156  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0207  */
    public final Object invokeSuspend(Object obj) {
        SleepStatistic sleepStatistic;
        ActivityStatistic activityStatistic;
        List list;
        Object obj2;
        zg4 zg4;
        Object a;
        Object a2;
        zg4 zg42;
        Object a3 = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg42 = this.p$;
            ug4 a4 = this.this$Anon0.c();
            HomeDashboardPresenter$checkDeviceStatus$Anon1$activityStatistic$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1$activityStatistic$Anon1 = new HomeDashboardPresenter$checkDeviceStatus$Anon1$activityStatistic$Anon1(this, (yb4) null);
            this.L$Anon0 = zg42;
            this.label = 1;
            obj = yf4.a(a4, homeDashboardPresenter$checkDeviceStatus$Anon1$activityStatistic$Anon1, this);
            if (obj == a3) {
                return a3;
            }
        } else if (i == 1) {
            zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            activityStatistic = (ActivityStatistic) this.L$Anon1;
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            SleepStatistic sleepStatistic2 = (SleepStatistic) obj;
            ug4 a5 = this.this$Anon0.c();
            HomeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon1 = new HomeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = activityStatistic;
            this.L$Anon2 = sleepStatistic2;
            this.label = 3;
            a2 = yf4.a(a5, homeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon1, this);
            if (a2 != a3) {
                return a3;
            }
            Object obj3 = a2;
            sleepStatistic = sleepStatistic2;
            obj = obj3;
            List list2 = (List) obj;
            ug4 a6 = this.this$Anon0.c();
            HomeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon1 = new HomeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = activityStatistic;
            this.L$Anon2 = sleepStatistic;
            this.L$Anon3 = list2;
            this.label = 4;
            a = yf4.a(a6, homeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon1, this);
            if (a != a3) {
            }
        } else if (i == 3) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            ActivityStatistic activityStatistic2 = (ActivityStatistic) this.L$Anon1;
            sleepStatistic = (SleepStatistic) this.L$Anon2;
            activityStatistic = activityStatistic2;
            List list22 = (List) obj;
            ug4 a62 = this.this$Anon0.c();
            HomeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon12 = new HomeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = activityStatistic;
            this.L$Anon2 = sleepStatistic;
            this.L$Anon3 = list22;
            this.label = 4;
            a = yf4.a(a62, homeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon12, this);
            if (a != a3) {
                return a3;
            }
            list = list22;
            obj = a;
            String str = (String) obj;
            if (PortfolioApp.W.c().g(this.this$Anon0.h) != 2) {
            }
            this.this$Anon0.p = (activityStatistic != null || activityStatistic.getTotalSteps() == 0) && (sleepStatistic == null || sleepStatistic.getTotalSleeps() == 0);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("checkDeviceStatus activity ");
            sb.append(activityStatistic == null ? dc4.a(activityStatistic.getTotalSteps()) : null);
            sb.append(" sleep ");
            sb.append(sleepStatistic == null ? dc4.a(sleepStatistic.getTotalSleeps()) : null);
            sb.append(" totalDevices ");
            sb.append(list.size());
            local.d("HomeDashboardPresenter", sb.toString());
            if (!(!list.isEmpty())) {
            }
            this.this$Anon0.a("release");
            this.this$Anon0.o();
            return qa4.a;
        } else if (i == 4) {
            list = (List) this.L$Anon3;
            zg4 zg43 = (zg4) this.L$Anon0;
            na4.a(obj);
            sleepStatistic = (SleepStatistic) this.L$Anon2;
            activityStatistic = (ActivityStatistic) this.L$Anon1;
            String str2 = (String) obj;
            boolean z = PortfolioApp.W.c().g(this.this$Anon0.h) != 2;
            this.this$Anon0.p = (activityStatistic != null || activityStatistic.getTotalSteps() == 0) && (sleepStatistic == null || sleepStatistic.getTotalSleeps() == 0);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("checkDeviceStatus activity ");
            sb2.append(activityStatistic == null ? dc4.a(activityStatistic.getTotalSteps()) : null);
            sb2.append(" sleep ");
            sb2.append(sleepStatistic == null ? dc4.a(sleepStatistic.getTotalSleeps()) : null);
            sb2.append(" totalDevices ");
            sb2.append(list.size());
            local2.d("HomeDashboardPresenter", sb2.toString());
            if (!(!list.isEmpty())) {
                Iterator it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    if (dc4.a(kd4.a((Object) ((Device) obj2).getDeviceId(), (Object) this.this$Anon0.h)).booleanValue()) {
                        break;
                    }
                }
                Device device = (Device) obj2;
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                StringBuilder sb3 = new StringBuilder();
                sb3.append("checkDeviceStatus activeDevice ");
                sb3.append(device != null ? device.getDeviceId() : null);
                sb3.append(" currentSerial ");
                sb3.append(this.this$Anon0.h);
                local3.d("HomeDashboardPresenter", sb3.toString());
                if (device != null) {
                    FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "checkDeviceStatus isActiveDeviceConnected " + z);
                    this.this$Anon0.x.a(this.this$Anon0.h, str2);
                    this.this$Anon0.x.a(true, true, this.this$Anon0.p);
                } else {
                    this.this$Anon0.x.a((String) null, "");
                    this.this$Anon0.x.a(true, false, this.this$Anon0.p);
                }
            } else {
                this.this$Anon0.x.a((String) null, "");
                this.this$Anon0.x.a(false, false, this.this$Anon0.p);
            }
            this.this$Anon0.a("release");
            this.this$Anon0.o();
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ActivityStatistic activityStatistic3 = (ActivityStatistic) obj;
        ug4 a7 = this.this$Anon0.c();
        HomeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1 = new HomeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1(this, (yb4) null);
        this.L$Anon0 = zg42;
        this.L$Anon1 = activityStatistic3;
        this.label = 2;
        Object a8 = yf4.a(a7, homeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1, this);
        if (a8 == a3) {
            return a3;
        }
        zg4 zg44 = zg42;
        activityStatistic = activityStatistic3;
        obj = a8;
        zg4 = zg44;
        SleepStatistic sleepStatistic22 = (SleepStatistic) obj;
        ug4 a52 = this.this$Anon0.c();
        HomeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon12 = new HomeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon1(this, (yb4) null);
        this.L$Anon0 = zg4;
        this.L$Anon1 = activityStatistic;
        this.L$Anon2 = sleepStatistic22;
        this.label = 3;
        a2 = yf4.a(a52, homeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon12, this);
        if (a2 != a3) {
        }
    }
}
