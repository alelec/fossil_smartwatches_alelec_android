package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$showDetailChart$1$invokeSuspend$$inlined$let$lambda$1 */
public final class C6662xbacd57ab extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<? extends com.fossil.blesdk.obfuscated.dg3.C4120b>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $it;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23478p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$showDetailChart$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6662xbacd57ab(java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$showDetailChart$1 sleepOverviewDayPresenter$showDetailChart$1) {
        super(2, yb4);
        this.$it = list;
        this.this$0 = sleepOverviewDayPresenter$showDetailChart$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.sleep.overview.C6662xbacd57ab sleepOverviewDayPresenter$showDetailChart$1$invokeSuspend$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.dashboard.sleep.overview.C6662xbacd57ab(this.$it, yb4, this.this$0);
        sleepOverviewDayPresenter$showDetailChart$1$invokeSuspend$$inlined$let$lambda$1.f23478p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return sleepOverviewDayPresenter$showDetailChart$1$invokeSuspend$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.sleep.overview.C6662xbacd57ab) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter sleepOverviewDayPresenter = this.this$0.this$0;
            return sleepOverviewDayPresenter.mo41332a(com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter.m34932b(sleepOverviewDayPresenter), (java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession>) this.$it);
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
