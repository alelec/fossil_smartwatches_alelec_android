package com.portfolio.platform.uirenew.home.details.activity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityDetailPresenter$findActivitySamples$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.portfolio.platform.data.model.room.fitness.ActivitySample, java.lang.Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $date;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityDetailPresenter$findActivitySamples$1(java.util.Date date) {
        super(1);
        this.$date = date;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke((com.portfolio.platform.data.model.room.fitness.ActivitySample) obj));
    }

    @DexIgnore
    public final boolean invoke(com.portfolio.platform.data.model.room.fitness.ActivitySample activitySample) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(activitySample, "it");
        return com.fossil.blesdk.obfuscated.rk2.m27396d(activitySample.getDate(), this.$date);
    }
}
