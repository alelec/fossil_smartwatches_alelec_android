package com.portfolio.platform.uirenew.home.details.heartrate;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$Anon1", f = "HeartRateDetailPresenter.kt", l = {192}, m = "invokeSuspend")
public final class HeartRateDetailPresenter$showDayDetail$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateDetailPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$showDayDetail$Anon1(HeartRateDetailPresenter heartRateDetailPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = heartRateDetailPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HeartRateDetailPresenter$showDayDetail$Anon1 heartRateDetailPresenter$showDayDetail$Anon1 = new HeartRateDetailPresenter$showDayDetail$Anon1(this.this$Anon0, yb4);
        heartRateDetailPresenter$showDayDetail$Anon1.p$ = (zg4) obj;
        return heartRateDetailPresenter$showDayDetail$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HeartRateDetailPresenter$showDayDetail$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x007a  */
    public final Object invokeSuspend(Object obj) {
        int i;
        DailyHeartRateSummary h;
        Object a = cc4.a();
        int i2 = this.label;
        if (i2 == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 a2 = this.this$Anon0.b();
            HeartRateDetailPresenter$showDayDetail$Anon1$summary$Anon1 heartRateDetailPresenter$showDayDetail$Anon1$summary$Anon1 = new HeartRateDetailPresenter$showDayDetail$Anon1$summary$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a2, heartRateDetailPresenter$showDayDetail$Anon1$summary$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i2 == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) obj;
        if (this.this$Anon0.k == null || (!kd4.a((Object) this.this$Anon0.k, (Object) dailyHeartRateSummary))) {
            this.this$Anon0.k = dailyHeartRateSummary;
            DailyHeartRateSummary h2 = this.this$Anon0.k;
            int i3 = 0;
            if (h2 != null) {
                Resting resting = h2.getResting();
                if (resting != null) {
                    Integer a3 = dc4.a(resting.getValue());
                    if (a3 != null) {
                        i = a3.intValue();
                        h = this.this$Anon0.k;
                        if (h != null) {
                            Integer a4 = dc4.a(h.getMax());
                            if (a4 != null) {
                                i3 = a4.intValue();
                            }
                        }
                        this.this$Anon0.q.c(i, i3);
                    }
                }
            }
            i = 0;
            h = this.this$Anon0.k;
            if (h != null) {
            }
            this.this$Anon0.q.c(i, i3);
        }
        return qa4.a;
    }
}
