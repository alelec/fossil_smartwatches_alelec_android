package com.portfolio.platform.uirenew.home.details.activity;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.wr2;
import com.fossil.blesdk.obfuscated.xk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.enums.GoalType;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$showDetailChart$Anon1", f = "ActivityDetailPresenter.kt", l = {240, 242}, m = "invokeSuspend")
public final class ActivityDetailPresenter$showDetailChart$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivityDetailPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityDetailPresenter$showDetailChart$Anon1(ActivityDetailPresenter activityDetailPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = activityDetailPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ActivityDetailPresenter$showDetailChart$Anon1 activityDetailPresenter$showDetailChart$Anon1 = new ActivityDetailPresenter$showDetailChart$Anon1(this.this$Anon0, yb4);
        activityDetailPresenter$showDetailChart$Anon1.p$ = (zg4) obj;
        return activityDetailPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActivityDetailPresenter$showDetailChart$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008f  */
    public final Object invokeSuspend(Object obj) {
        Pair pair;
        ArrayList arrayList;
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            ug4 a2 = this.this$Anon0.b();
            ActivityDetailPresenter$showDetailChart$Anon1$pair$Anon1 activityDetailPresenter$showDetailChart$Anon1$pair$Anon1 = new ActivityDetailPresenter$showDetailChart$Anon1$pair$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a2, activityDetailPresenter$showDetailChart$Anon1$pair$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            arrayList = (ArrayList) this.L$Anon2;
            pair = (Pair) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            Integer num = (Integer) obj;
            int a3 = xk2.d.a(this.this$Anon0.m, GoalType.TOTAL_STEPS);
            this.this$Anon0.s.a((wr2) new BarChart.c(Math.max(num == null ? num.intValue() : 0, a3 / 16), a3, arrayList), (ArrayList<String>) (ArrayList) pair.getSecond());
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair2 = (Pair) obj;
        ArrayList arrayList2 = (ArrayList) pair2.getFirst();
        ug4 a4 = this.this$Anon0.b();
        ActivityDetailPresenter$showDetailChart$Anon1$maxValue$Anon1 activityDetailPresenter$showDetailChart$Anon1$maxValue$Anon1 = new ActivityDetailPresenter$showDetailChart$Anon1$maxValue$Anon1(arrayList2, (yb4) null);
        this.L$Anon0 = zg4;
        this.L$Anon1 = pair2;
        this.L$Anon2 = arrayList2;
        this.label = 2;
        Object a5 = yf4.a(a4, activityDetailPresenter$showDetailChart$Anon1$maxValue$Anon1, this);
        if (a5 == a) {
            return a;
        }
        arrayList = arrayList2;
        Object obj2 = a5;
        pair = pair2;
        obj = obj2;
        Integer num2 = (Integer) obj;
        int a32 = xk2.d.a(this.this$Anon0.m, GoalType.TOTAL_STEPS);
        this.this$Anon0.s.a((wr2) new BarChart.c(Math.max(num2 == null ? num2.intValue() : 0, a32 / 16), a32, arrayList), (ArrayList<String>) (ArrayList) pair.getSecond());
        return qa4.a;
    }
}
