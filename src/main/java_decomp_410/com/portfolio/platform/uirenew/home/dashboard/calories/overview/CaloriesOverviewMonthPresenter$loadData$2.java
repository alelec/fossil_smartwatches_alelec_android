package com.portfolio.platform.uirenew.home.dashboard.calories.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$loadData$2", mo27670f = "CaloriesOverviewMonthPresenter.kt", mo27671l = {88}, mo27672m = "invokeSuspend")
public final class CaloriesOverviewMonthPresenter$loadData$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23308p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CaloriesOverviewMonthPresenter$loadData$2(com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = caloriesOverviewMonthPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$loadData$2 caloriesOverviewMonthPresenter$loadData$2 = new com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$loadData$2(this.this$0, yb4);
        caloriesOverviewMonthPresenter$loadData$2.f23308p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return caloriesOverviewMonthPresenter$loadData$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$loadData$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23308p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$loadData$2$currentUser$1 caloriesOverviewMonthPresenter$loadData$2$currentUser$1 = new com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$loadData$2$currentUser$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, caloriesOverviewMonthPresenter$loadData$2$currentUser$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) obj;
        if (mFUser != null) {
            this.this$0.f23299j = com.fossil.blesdk.obfuscated.rk2.m27394d(mFUser.getCreatedAt());
            com.fossil.blesdk.obfuscated.pa3 l = this.this$0.f23304o;
            java.util.Date f = com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter.m34684f(this.this$0);
            java.util.Date b = this.this$0.f23299j;
            if (b == null) {
                b = new java.util.Date();
            }
            l.mo30100a(f, b);
            this.this$0.f23295f.mo2280a(new java.util.Date());
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
