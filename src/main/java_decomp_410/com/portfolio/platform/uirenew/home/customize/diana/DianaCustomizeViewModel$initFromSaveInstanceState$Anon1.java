package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$initFromSaveInstanceState$Anon1", f = "DianaCustomizeViewModel.kt", l = {171, 174, 182}, m = "invokeSuspend")
public final class DianaCustomizeViewModel$initFromSaveInstanceState$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $complicationPos;
    @DexIgnore
    public /* final */ /* synthetic */ String $presetId;
    @DexIgnore
    public /* final */ /* synthetic */ DianaPreset $savedCurrentPreset;
    @DexIgnore
    public /* final */ /* synthetic */ DianaPreset $savedOriginalPreset;
    @DexIgnore
    public /* final */ /* synthetic */ String $watchAppPos;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaCustomizeViewModel this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$initFromSaveInstanceState$Anon1$Anon1", f = "DianaCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel$initFromSaveInstanceState$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(DianaCustomizeViewModel$initFromSaveInstanceState$Anon1 dianaCustomizeViewModel$initFromSaveInstanceState$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = dianaCustomizeViewModel$initFromSaveInstanceState$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.this$Anon0.d.a(this.this$Anon0.this$Anon0.x);
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaCustomizeViewModel$initFromSaveInstanceState$Anon1(DianaCustomizeViewModel dianaCustomizeViewModel, String str, DianaPreset dianaPreset, DianaPreset dianaPreset2, String str2, String str3, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = dianaCustomizeViewModel;
        this.$presetId = str;
        this.$savedOriginalPreset = dianaPreset;
        this.$savedCurrentPreset = dianaPreset2;
        this.$watchAppPos = str2;
        this.$complicationPos = str3;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DianaCustomizeViewModel$initFromSaveInstanceState$Anon1 dianaCustomizeViewModel$initFromSaveInstanceState$Anon1 = new DianaCustomizeViewModel$initFromSaveInstanceState$Anon1(this.this$Anon0, this.$presetId, this.$savedOriginalPreset, this.$savedCurrentPreset, this.$watchAppPos, this.$complicationPos, yb4);
        dianaCustomizeViewModel$initFromSaveInstanceState$Anon1.p$ = (zg4) obj;
        return dianaCustomizeViewModel$initFromSaveInstanceState$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DianaCustomizeViewModel$initFromSaveInstanceState$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ed A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f2  */
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        pi4 c;
        Anon1 anon1;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = DianaCustomizeViewModel.G.a();
            local.d(a2, "initFromSaveInstanceState presetId " + this.$presetId + " savedOriginalPreset=" + this.$savedOriginalPreset + ',' + " savedCurrentPreset=" + this.$savedCurrentPreset + " watchAppPos=" + this.$watchAppPos + " complicationPos=" + this.$complicationPos + " currentPreset");
            if (this.$savedOriginalPreset == null) {
                DianaCustomizeViewModel dianaCustomizeViewModel = this.this$Anon0;
                String str = this.$presetId;
                this.L$Anon0 = zg4;
                this.label = 1;
                if (dianaCustomizeViewModel.a(str, (yb4<? super qa4>) this) == a) {
                    return a;
                }
                this.this$Anon0.i.a(this.$complicationPos);
                this.this$Anon0.l.a(this.$watchAppPos);
                c = nh4.c();
                anon1 = new Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 3;
                if (yf4.a(c, anon1, this) == a) {
                }
                if (this.$savedCurrentPreset != null) {
                }
                return qa4.a;
            }
            this.this$Anon0.l();
            DianaCustomizeViewModel dianaCustomizeViewModel2 = this.this$Anon0;
            DianaPreset dianaPreset = this.$savedCurrentPreset;
            DianaPreset clone = dianaPreset != null ? dianaPreset.clone() : null;
            this.L$Anon0 = zg4;
            this.label = 2;
            if (dianaCustomizeViewModel2.a(clone, (yb4<? super qa4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            this.this$Anon0.i.a(this.$complicationPos);
            this.this$Anon0.l.a(this.$watchAppPos);
            c = nh4.c();
            anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 3;
            if (yf4.a(c, anon1, this) == a) {
                return a;
            }
            if (this.$savedCurrentPreset != null) {
            }
            return qa4.a;
        } else if (i == 2) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 3) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            if (this.$savedCurrentPreset != null) {
                this.this$Anon0.d.a(this.$savedCurrentPreset);
            }
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.c = this.$savedOriginalPreset;
        this.this$Anon0.i.a(this.$complicationPos);
        this.this$Anon0.l.a(this.$watchAppPos);
        c = nh4.c();
        anon1 = new Anon1(this, (yb4) null);
        this.L$Anon0 = zg4;
        this.label = 3;
        if (yf4.a(c, anon1, this) == a) {
        }
        if (this.$savedCurrentPreset != null) {
        }
        return qa4.a;
    }
}
