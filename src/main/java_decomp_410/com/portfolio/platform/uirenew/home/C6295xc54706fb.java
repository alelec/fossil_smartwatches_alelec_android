package com.portfolio.platform.uirenew.home;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.HomePresenter$startCollectingUserFeedback$1$onSuccess$configuration$1 */
public final class C6295xc54706fb extends com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.kr2.C4595d $responseValue;

    @DexIgnore
    public C6295xc54706fb(com.fossil.blesdk.obfuscated.kr2.C4595d dVar) {
        this.$responseValue = dVar;
    }

    @DexIgnore
    public java.lang.String getAdditionalInfo() {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a = com.portfolio.platform.uirenew.home.HomePresenter.f22267y.mo40748a();
        local.mo33255d(a, "Inside. getAdditionalInfo: \n" + this.$responseValue.mo28906a());
        return this.$responseValue.mo28906a();
    }

    @DexIgnore
    public java.lang.String getRequestSubject() {
        return this.$responseValue.mo28909d();
    }

    @DexIgnore
    public java.util.List<java.lang.String> getTags() {
        return this.$responseValue.mo28910e();
    }
}
