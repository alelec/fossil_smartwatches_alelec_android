package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$start$1", mo27670f = "CommuteTimeSettingsDefaultAddressPresenter.kt", mo27671l = {40}, mo27672m = "invokeSuspend")
public final class CommuteTimeSettingsDefaultAddressPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22756p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsDefaultAddressPresenter$start$1(com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = commuteTimeSettingsDefaultAddressPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$start$1 commuteTimeSettingsDefaultAddressPresenter$start$1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$start$1(this.this$0, yb4);
        commuteTimeSettingsDefaultAddressPresenter$start$1.f22756p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return commuteTimeSettingsDefaultAddressPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22756p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6451x2fffdbf1 commuteTimeSettingsDefaultAddressPresenter$start$1$recentSearchedAddress$1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6451x2fffdbf1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, commuteTimeSettingsDefaultAddressPresenter$start$1$recentSearchedAddress$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.kd4.m24407a(obj, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
        this.this$0.f22747k.clear();
        this.this$0.f22747k.addAll((java.util.List) obj);
        this.this$0.f22742f = com.google.android.libraries.places.api.Places.createClient(com.portfolio.platform.PortfolioApp.f20941W.mo34589c());
        this.this$0.mo41026j().mo29788a(this.this$0.f22742f);
        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f22744h, (java.lang.Object) "Home")) {
            com.fossil.blesdk.obfuscated.y23 j = this.this$0.mo41026j();
            java.lang.String d = this.this$0.f22745i;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) d, "mHomeTitle");
            j.setTitle(d);
        } else {
            com.fossil.blesdk.obfuscated.y23 j2 = this.this$0.mo41026j();
            java.lang.String g = this.this$0.f22746j;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) g, "mWorkTitle");
            j2.setTitle(g);
        }
        this.this$0.mo41026j().mo29791v(this.this$0.f22743g);
        this.this$0.mo41026j().mo29789j(this.this$0.f22747k);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
