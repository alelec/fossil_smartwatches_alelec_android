package com.portfolio.platform.uirenew.home.dashboard.activetime.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1", mo27670f = "ActiveTimeOverviewWeekPresenter.kt", mo27671l = {52}, mo27672m = "invokeSuspend")
public final class ActiveTimeOverviewWeekPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23174p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1$2")
    /* renamed from: com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1$2 */
    public static final class C65812<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1 f23175a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1$2$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1$2$1", mo27670f = "ActiveTimeOverviewWeekPresenter.kt", mo27671l = {65}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1$2$1 */
        public static final class C65821 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ java.util.List $data;
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public java.lang.Object L$1;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f23176p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1.C65812 this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1$2$1$1")
            @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1$2$1$1", mo27670f = "ActiveTimeOverviewWeekPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1$2$1$1 */
            public static final class C65831 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c>, java.lang.Object> {
                @DexIgnore
                public int label;

                @DexIgnore
                /* renamed from: p$ */
                public com.fossil.blesdk.obfuscated.zg4 f23177p$;
                @DexIgnore
                public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1.C65812.C65821 this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C65831(com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1.C65812.C65821 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                    super(2, yb4);
                    this.this$0 = r1;
                }

                @DexIgnore
                public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                    com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                    com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1.C65812.C65821.C65831 r0 = new com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1.C65812.C65821.C65831(this.this$0, yb4);
                    r0.f23177p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                    return r0;
                }

                @DexIgnore
                public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                    return ((com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1.C65812.C65821.C65831) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
                }

                @DexIgnore
                public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                    com.fossil.blesdk.obfuscated.cc4.m20546a();
                    if (this.label == 0) {
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter activeTimeOverviewWeekPresenter = this.this$0.this$0.f23175a.this$0;
                        return activeTimeOverviewWeekPresenter.mo41230a(com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter.m34506e(activeTimeOverviewWeekPresenter), (java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>) this.this$0.$data);
                    }
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C65821(com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1.C65812 r1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$data = list;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1.C65812.C65821 r0 = new com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1.C65812.C65821(this.this$0, this.$data, yb4);
                r0.f23176p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1.C65812.C65821) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter activeTimeOverviewWeekPresenter;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23176p$;
                    com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter activeTimeOverviewWeekPresenter2 = this.this$0.f23175a.this$0;
                    com.fossil.blesdk.obfuscated.ug4 a2 = activeTimeOverviewWeekPresenter2.mo31440b();
                    com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1.C65812.C65821.C65831 r4 = new com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1.C65812.C65821.C65831(this, (com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg4;
                    this.L$1 = activeTimeOverviewWeekPresenter2;
                    this.label = 1;
                    obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r4, this);
                    if (obj == a) {
                        return a;
                    }
                    activeTimeOverviewWeekPresenter = activeTimeOverviewWeekPresenter2;
                } else if (i == 1) {
                    activeTimeOverviewWeekPresenter = (com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter) this.L$1;
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                activeTimeOverviewWeekPresenter.f23170h = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c) obj;
                com.fossil.blesdk.obfuscated.u83 g = this.this$0.f23175a.this$0.f23171i;
                com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c c = this.this$0.f23175a.this$0.f23170h;
                if (c == null) {
                    c = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c(0, 0, (java.util.ArrayList) null, 7, (com.fossil.blesdk.obfuscated.fd4) null);
                }
                g.mo31462a(c);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        public C65812(com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1 activeTimeOverviewWeekPresenter$start$1) {
            this.f23175a = activeTimeOverviewWeekPresenter$start$1;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>> os3) {
            com.portfolio.platform.enums.Status a = os3.mo29972a();
            java.util.List list = (java.util.List) os3.mo29973b();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("start - mActivitySummaries -- activitySummaries=");
            sb.append(list != null ? java.lang.Integer.valueOf(list.size()) : null);
            local.mo33255d("ActiveTimeOverviewWeekPresenter", sb.toString());
            if (a != com.portfolio.platform.enums.Status.DATABASE_LOADING) {
                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23175a.this$0.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1.C65812.C65821(this, list, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActiveTimeOverviewWeekPresenter$start$1(com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter activeTimeOverviewWeekPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = activeTimeOverviewWeekPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1 activeTimeOverviewWeekPresenter$start$1 = new com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1(this.this$0, yb4);
        activeTimeOverviewWeekPresenter$start$1.f23174p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return activeTimeOverviewWeekPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00f0  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.u83 g;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23174p$;
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("ActiveTimeOverviewWeekPresenter", com.facebook.share.internal.VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
            this.this$0.mo41232h();
            if (this.this$0.f23168f == null || !com.fossil.blesdk.obfuscated.rk2.m27414s(com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter.m34506e(this.this$0)).booleanValue()) {
                this.this$0.f23168f = new java.util.Date();
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1$startAndEnd$1 activeTimeOverviewWeekPresenter$start$1$startAndEnd$1 = new com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1$startAndEnd$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, activeTimeOverviewWeekPresenter$start$1$startAndEnd$1, this);
                if (obj == a) {
                    return a;
                }
            }
            androidx.lifecycle.LiveData b = this.this$0.f23169g;
            g = this.this$0.f23171i;
            if (g == null) {
                b.mo2277a((com.fossil.blesdk.obfuscated.v83) g, new com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1.C65812(this));
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local.mo33255d("ActiveTimeOverviewWeekPresenter", "start - mDate=" + com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter.m34506e(this.this$0));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekFragment");
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        kotlin.Pair pair = (kotlin.Pair) obj;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local2.mo33255d("ActiveTimeOverviewWeekPresenter", "start - startDate=" + ((java.util.Date) pair.getFirst()) + ", endDate=" + ((java.util.Date) pair.getSecond()));
        com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter activeTimeOverviewWeekPresenter = this.this$0;
        activeTimeOverviewWeekPresenter.f23169g = activeTimeOverviewWeekPresenter.f23173k.getSummaries((java.util.Date) pair.getFirst(), (java.util.Date) pair.getSecond(), false);
        androidx.lifecycle.LiveData b2 = this.this$0.f23169g;
        g = this.this$0.f23171i;
        if (g == null) {
        }
    }
}
