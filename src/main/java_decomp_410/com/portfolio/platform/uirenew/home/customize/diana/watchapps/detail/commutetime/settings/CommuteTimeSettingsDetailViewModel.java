package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.jc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.sn1;
import com.fossil.blesdk.obfuscated.tn1;
import com.fossil.blesdk.obfuscated.wn1;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeSettingsDetailViewModel extends ic {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public PlacesClient c;
    @DexIgnore
    public AddressWrapper d;
    @DexIgnore
    public AddressWrapper e;
    @DexIgnore
    public List<String> f;
    @DexIgnore
    public MutableLiveData<b> g; // = new MutableLiveData<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public Boolean d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public PlacesClient f;
        @DexIgnore
        public Boolean g;

        @DexIgnore
        public b(String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4) {
            this.a = str;
            this.b = str2;
            this.c = bool;
            this.d = bool2;
            this.e = bool3;
            this.f = placesClient;
            this.g = bool4;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final Boolean b() {
            return this.c;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }

        @DexIgnore
        public final PlacesClient d() {
            return this.f;
        }

        @DexIgnore
        public final Boolean e() {
            return this.g;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return kd4.a((Object) this.a, (Object) bVar.a) && kd4.a((Object) this.b, (Object) bVar.b) && kd4.a((Object) this.c, (Object) bVar.c) && kd4.a((Object) this.d, (Object) bVar.d) && kd4.a((Object) this.e, (Object) bVar.e) && kd4.a((Object) this.f, (Object) bVar.f) && kd4.a((Object) this.g, (Object) bVar.g);
        }

        @DexIgnore
        public final Boolean f() {
            return this.d;
        }

        @DexIgnore
        public final Boolean g() {
            return this.e;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.b;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            Boolean bool = this.c;
            int hashCode3 = (hashCode2 + (bool != null ? bool.hashCode() : 0)) * 31;
            Boolean bool2 = this.d;
            int hashCode4 = (hashCode3 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
            Boolean bool3 = this.e;
            int hashCode5 = (hashCode4 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
            PlacesClient placesClient = this.f;
            int hashCode6 = (hashCode5 + (placesClient != null ? placesClient.hashCode() : 0)) * 31;
            Boolean bool4 = this.g;
            if (bool4 != null) {
                i = bool4.hashCode();
            }
            return hashCode6 + i;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(name=" + this.a + ", address=" + this.b + ", avoidTolls=" + this.c + ", isNameEditable=" + this.d + ", isValid=" + this.e + ", placesClient=" + this.f + ", isLoading=" + this.g + ")";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<TResult> implements tn1<FetchPlaceResponse> {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailViewModel a;

        @DexIgnore
        public c(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel) {
            this.a = commuteTimeSettingsDetailViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
            CommuteTimeSettingsDetailViewModel.a(this.a, (String) null, (String) null, (Boolean) null, (Boolean) null, (Boolean) null, (PlacesClient) null, false, 63, (Object) null);
            AddressWrapper a2 = this.a.d;
            if (a2 != null) {
                kd4.a((Object) fetchPlaceResponse, "response");
                Place place = fetchPlaceResponse.getPlace();
                kd4.a((Object) place, "response.place");
                LatLng latLng = place.getLatLng();
                if (latLng != null) {
                    a2.setLat(latLng.e);
                } else {
                    kd4.a();
                    throw null;
                }
            }
            AddressWrapper a3 = this.a.d;
            if (a3 != null) {
                kd4.a((Object) fetchPlaceResponse, "response");
                Place place2 = fetchPlaceResponse.getPlace();
                kd4.a((Object) place2, "response.place");
                LatLng latLng2 = place2.getLatLng();
                if (latLng2 != null) {
                    a3.setLng(latLng2.f);
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements sn1 {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailViewModel a;

        @DexIgnore
        public d(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel) {
            this.a = commuteTimeSettingsDetailViewModel;
        }

        @DexIgnore
        public final void onFailure(Exception exc) {
            kd4.b(exc, "exception");
            CommuteTimeSettingsDetailViewModel.a(this.a, (String) null, (String) null, (Boolean) null, (Boolean) null, (Boolean) null, (PlacesClient) null, false, 63, (Object) null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String i = CommuteTimeSettingsDetailViewModel.h;
            local.e(i, "FetchPlaceRequest - exception=" + exc);
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = CommuteTimeSettingsDetailViewModel.class.getSimpleName();
        kd4.a((Object) simpleName, "CommuteTimeSettingsDetai\u2026el::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public CommuteTimeSettingsDetailViewModel(en2 en2, UserRepository userRepository) {
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(userRepository, "mUserRepository");
    }

    @DexIgnore
    public final AddressWrapper c() {
        return this.d;
    }

    @DexIgnore
    public final MutableLiveData<b> d() {
        return this.g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0045 A[SYNTHETIC] */
    public final boolean e() {
        String str;
        List<String> list = this.f;
        if (list != null) {
            for (String next : list) {
                AddressWrapper addressWrapper = this.d;
                if (addressWrapper != null) {
                    String name = addressWrapper.getName();
                    if (name != null) {
                        if (name != null) {
                            str = name.toUpperCase();
                            kd4.a((Object) str, "(this as java.lang.String).toUpperCase()");
                            if (next == null) {
                                String upperCase = next.toUpperCase();
                                kd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                                if (kd4.a((Object) str, (Object) upperCase)) {
                                    return true;
                                }
                            } else {
                                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                }
                str = null;
                if (next == null) {
                }
            }
            return false;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x009f  */
    public final boolean f() {
        String str;
        AddressWrapper addressWrapper;
        String str2;
        AddressWrapper addressWrapper2 = this.d;
        String str3 = null;
        if (TextUtils.isEmpty(addressWrapper2 != null ? addressWrapper2.getAddress() : null)) {
            return false;
        }
        AddressWrapper addressWrapper3 = this.d;
        if (TextUtils.isEmpty(addressWrapper3 != null ? addressWrapper3.getName() : null)) {
            return false;
        }
        AddressWrapper addressWrapper4 = this.d;
        Boolean valueOf = addressWrapper4 != null ? Boolean.valueOf(addressWrapper4.getAvoidTolls()) : null;
        AddressWrapper addressWrapper5 = this.e;
        if (!(!kd4.a((Object) valueOf, (Object) addressWrapper5 != null ? Boolean.valueOf(addressWrapper5.getAvoidTolls()) : null))) {
            AddressWrapper addressWrapper6 = this.d;
            AddressWrapper.AddressType type = addressWrapper6 != null ? addressWrapper6.getType() : null;
            AddressWrapper addressWrapper7 = this.e;
            if (type == (addressWrapper7 != null ? addressWrapper7.getType() : null)) {
                AddressWrapper addressWrapper8 = this.d;
                if (addressWrapper8 != null) {
                    String name = addressWrapper8.getName();
                    if (name != null) {
                        if (name != null) {
                            str = name.toUpperCase();
                            kd4.a((Object) str, "(this as java.lang.String).toUpperCase()");
                            addressWrapper = this.e;
                            if (addressWrapper != null) {
                                String name2 = addressWrapper.getName();
                                if (name2 != null) {
                                    if (name2 != null) {
                                        str2 = name2.toUpperCase();
                                        kd4.a((Object) str2, "(this as java.lang.String).toUpperCase()");
                                        if (!(!kd4.a((Object) str, (Object) str2))) {
                                            AddressWrapper addressWrapper9 = this.d;
                                            String address = addressWrapper9 != null ? addressWrapper9.getAddress() : null;
                                            AddressWrapper addressWrapper10 = this.e;
                                            if (addressWrapper10 != null) {
                                                str3 = addressWrapper10.getAddress();
                                            }
                                            if (!kd4.a((Object) address, (Object) str3)) {
                                                return true;
                                            }
                                            return false;
                                        }
                                    } else {
                                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                    }
                                }
                            }
                            str2 = null;
                            if (!(!kd4.a((Object) str, (Object) str2))) {
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                }
                str = null;
                addressWrapper = this.e;
                if (addressWrapper != null) {
                }
                str2 = null;
                if (!(!kd4.a((Object) str, (Object) str2))) {
                }
            }
        }
        return true;
    }

    @DexIgnore
    public final void g() {
        fi4 unused = ag4.b(jc.a(this), (CoroutineContext) null, (CoroutineStart) null, new CommuteTimeSettingsDetailViewModel$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void h() {
        this.c = null;
    }

    @DexIgnore
    public final void b(String str) {
        kd4.b(str, "name");
        AddressWrapper addressWrapper = this.d;
        if (addressWrapper != null) {
            addressWrapper.setName(str);
        }
        a(this, (String) null, (String) null, (Boolean) null, (Boolean) null, Boolean.valueOf(f()), (PlacesClient) null, (Boolean) null, 111, (Object) null);
    }

    @DexIgnore
    public final void a(AddressWrapper addressWrapper, ArrayList<String> arrayList) {
        this.e = addressWrapper;
        this.d = addressWrapper != null ? addressWrapper.clone() : null;
        this.f = arrayList;
    }

    @DexIgnore
    public static /* synthetic */ void a(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel, String str, String str2, AutocompleteSessionToken autocompleteSessionToken, int i, Object obj) {
        if ((i & 1) != 0) {
            str = "";
        }
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            autocompleteSessionToken = null;
        }
        commuteTimeSettingsDetailViewModel.a(str, str2, autocompleteSessionToken);
    }

    @DexIgnore
    public final void a(String str, String str2, AutocompleteSessionToken autocompleteSessionToken) {
        kd4.b(str, "address");
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2) && autocompleteSessionToken != null) {
            if (str2 != null) {
                a(str2, autocompleteSessionToken);
            } else {
                kd4.a();
                throw null;
            }
        }
        AddressWrapper addressWrapper = this.d;
        if (addressWrapper != null) {
            addressWrapper.setAddress(str);
        }
        a(this, (String) null, (String) null, (Boolean) null, (Boolean) null, Boolean.valueOf(f()), (PlacesClient) null, (Boolean) null, 111, (Object) null);
    }

    @DexIgnore
    public final void a(boolean z) {
        AddressWrapper addressWrapper = this.d;
        if (addressWrapper != null) {
            addressWrapper.setAvoidTolls(z);
        }
        a(this, (String) null, (String) null, (Boolean) null, (Boolean) null, Boolean.valueOf(f()), (PlacesClient) null, (Boolean) null, 111, (Object) null);
    }

    @DexIgnore
    public final void a(String str, AutocompleteSessionToken autocompleteSessionToken) {
        if (this.c != null && this.d != null) {
            a(this, (String) null, (String) null, (Boolean) null, (Boolean) null, (Boolean) null, (PlacesClient) null, true, 63, (Object) null);
            ArrayList arrayList = new ArrayList();
            arrayList.add(Place.Field.ADDRESS);
            arrayList.add(Place.Field.LAT_LNG);
            FetchPlaceRequest.Builder builder = FetchPlaceRequest.builder(str, arrayList);
            kd4.a((Object) builder, "FetchPlaceRequest.builder(placeId, placeFields)");
            builder.setSessionToken(autocompleteSessionToken);
            PlacesClient placesClient = this.c;
            if (placesClient != null) {
                wn1<FetchPlaceResponse> fetchPlace = placesClient.fetchPlace(builder.build());
                kd4.a((Object) fetchPlace, "mPlacesClient!!.fetchPlace(request.build())");
                fetchPlace.a((tn1<? super FetchPlaceResponse>) new c(this));
                fetchPlace.a((sn1) new d(this));
                return;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel, String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            bool = null;
        }
        if ((i & 8) != 0) {
            bool2 = null;
        }
        if ((i & 16) != 0) {
            bool3 = null;
        }
        if ((i & 32) != 0) {
            placesClient = null;
        }
        if ((i & 64) != 0) {
            bool4 = null;
        }
        commuteTimeSettingsDetailViewModel.a(str, str2, bool, bool2, bool3, placesClient, bool4);
    }

    @DexIgnore
    public final void a(String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4) {
        this.g.a(new b(str, str2, bool, bool2, bool3, placesClient, bool4));
    }
}
