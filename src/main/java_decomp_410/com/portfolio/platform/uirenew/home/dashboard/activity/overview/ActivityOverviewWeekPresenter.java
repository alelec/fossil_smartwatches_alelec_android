package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import android.graphics.RectF;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.t93;
import com.fossil.blesdk.obfuscated.u93;
import com.fossil.blesdk.obfuscated.v93;
import com.fossil.blesdk.obfuscated.xk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.enums.GoalType;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityOverviewWeekPresenter extends t93 {
    @DexIgnore
    public Date f;
    @DexIgnore
    public LiveData<os3<List<ActivitySummary>>> g; // = new MutableLiveData();
    @DexIgnore
    public BarChart.c h;
    @DexIgnore
    public /* final */ u93 i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ SummariesRepository k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public ActivityOverviewWeekPresenter(u93 u93, UserRepository userRepository, SummariesRepository summariesRepository) {
        kd4.b(u93, "mView");
        kd4.b(userRepository, "userRepository");
        kd4.b(summariesRepository, "mSummariesRepository");
        this.i = u93;
        this.j = userRepository;
        this.k = summariesRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Date e(ActivityOverviewWeekPresenter activityOverviewWeekPresenter) {
        Date date = activityOverviewWeekPresenter.f;
        if (date != null) {
            return date;
        }
        kd4.d("mDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        this.i.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        h();
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ActivityOverviewWeekPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekPresenter", "stop");
        try {
            LiveData<os3<List<ActivitySummary>>> liveData = this.g;
            u93 u93 = this.i;
            if (u93 != null) {
                liveData.a((LifecycleOwner) (v93) u93);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekFragment");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActivityOverviewWeekPresenter", "stop - e=" + e);
        }
    }

    @DexIgnore
    public void h() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekPresenter", "loadData");
    }

    @DexIgnore
    public final BarChart.c a(Date date, List<ActivitySummary> list) {
        int i2;
        T t;
        boolean z;
        Date date2 = date;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date2);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("ActivityOverviewWeekPresenter", sb.toString());
        BarChart.c cVar = new BarChart.c(0, 0, (ArrayList) null, 7, (fd4) null);
        Calendar instance = Calendar.getInstance(Locale.US);
        kd4.a((Object) instance, "calendar");
        instance.setTime(date2);
        instance.add(5, -6);
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 <= 6) {
            Date time = instance.getTime();
            kd4.a((Object) time, "calendar.time");
            long time2 = time.getTime();
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    ActivitySummary activitySummary = (ActivitySummary) t;
                    if (activitySummary.getDay() == instance.get(5) && activitySummary.getMonth() == instance.get(2) + 1 && activitySummary.getYear() == instance.get(1)) {
                        z = true;
                        continue;
                    } else {
                        z = false;
                        continue;
                    }
                    if (z) {
                        break;
                    }
                }
                ActivitySummary activitySummary2 = (ActivitySummary) t;
                if (activitySummary2 != null) {
                    int a2 = xk2.d.a(activitySummary2, GoalType.TOTAL_STEPS);
                    int steps = (int) activitySummary2.getSteps();
                    i4 = Math.max(Math.max(a2, steps), i4);
                    cVar.a().add(new BarChart.a(a2, cb4.a((T[]) new ArrayList[]{cb4.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.State) null, 0, steps, (RectF) null, 23, (fd4) null)})}), time2, i3 == 6));
                    i5 = a2;
                    i2 = 1;
                } else {
                    i2 = 1;
                    cVar.a().add(new BarChart.a(i5, cb4.a((T[]) new ArrayList[]{cb4.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.State) null, 0, 0, (RectF) null, 23, (fd4) null)})}), time2, i3 == 6));
                }
            } else {
                i2 = 1;
                cVar.a().add(new BarChart.a(i5, cb4.a((T[]) new ArrayList[]{cb4.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.State) null, 0, 0, (RectF) null, 23, (fd4) null)})}), time2, i3 == 6));
            }
            instance.add(5, i2);
            i3++;
        }
        if (i4 > 0) {
            i5 = i4;
        }
        cVar.b(i5);
        return cVar;
    }

    @DexIgnore
    public final Pair<Date, Date> a(Date date) {
        Date b = rk2.b(date, 6);
        MFUser currentUser = this.j.getCurrentUser();
        if (currentUser != null) {
            Date d = rk2.d(currentUser.getCreatedAt());
            if (!rk2.b(b, d)) {
                b = d;
            }
        }
        return new Pair<>(b, date);
    }
}
