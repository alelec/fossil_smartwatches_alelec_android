package com.portfolio.platform.uirenew.home.customize.diana;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ yb4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ DianaPreset $currentPreset$inlined;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaCustomizeViewModel this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1(yb4 yb4, DianaCustomizeViewModel dianaCustomizeViewModel, DianaPreset dianaPreset, yb4 yb42) {
        super(2, yb4);
        this.this$Anon0 = dianaCustomizeViewModel;
        this.$currentPreset$inlined = dianaPreset;
        this.$continuation$inlined = yb42;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1 dianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1 = new DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1(yb4, this.this$Anon0, this.$currentPreset$inlined, this.$continuation$inlined);
        dianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return dianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            LiveData m = this.this$Anon0.p;
            if (m == null) {
                return null;
            }
            m.a(this.this$Anon0.y);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
