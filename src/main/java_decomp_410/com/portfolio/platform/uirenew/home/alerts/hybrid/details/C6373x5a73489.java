package com.portfolio.platform.uirenew.home.alerts.hybrid.details;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1", mo27670f = "NotificationContactsAndAppsAssignedPresenter.kt", mo27671l = {129}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1 */
public final class C6373x5a73489 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.t03.C5127d $responseValue;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22535p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6373x5a73489(com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1 notificationContactsAndAppsAssignedPresenter$loadContactData$1, com.fossil.blesdk.obfuscated.t03.C5127d dVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = notificationContactsAndAppsAssignedPresenter$loadContactData$1;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.hybrid.details.C6373x5a73489 notificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1 = new com.portfolio.platform.uirenew.home.alerts.hybrid.details.C6373x5a73489(this.this$0, this.$responseValue, yb4);
        notificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1.f22535p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.hybrid.details.C6373x5a73489) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.util.List list;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22535p$;
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter.f22508x.mo40900a(), "mGetAllHybridContactGroups onSuccess");
            java.util.ArrayList arrayList = new java.util.ArrayList();
            com.fossil.blesdk.obfuscated.gh4 a2 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg4, this.this$0.f22534a.mo31440b(), (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.alerts.hybrid.details.C6374x3e820f88(this, arrayList, (com.fossil.blesdk.obfuscated.yb4) null), 2, (java.lang.Object) null);
            this.L$0 = zg4;
            this.L$1 = arrayList;
            this.L$2 = a2;
            this.label = 1;
            if (a2.mo27693a(this) == a) {
                return a;
            }
            list = arrayList;
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.gh4 gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
            list = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.f22534a.mo40891o().addAll(list);
        this.this$0.f22534a.mo40893q().addAll(list);
        java.util.List<java.lang.Object> p = this.this$0.f22534a.mo40892p();
        java.lang.Object[] array = list.toArray(new com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper[0]);
        if (array != null) {
            java.io.Serializable a3 = com.fossil.blesdk.obfuscated.fp4.m22340a((java.io.Serializable) array);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a3, "SerializationUtils.clone\u2026apperList.toTypedArray())");
            com.fossil.blesdk.obfuscated.hb4.m23080a(p, (T[]) (java.lang.Object[]) a3);
            this.this$0.f22534a.mo40896t();
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
