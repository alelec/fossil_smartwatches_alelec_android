package com.portfolio.platform.uirenew.home.customize.diana;

import android.os.Parcelable;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.oj2;
import com.fossil.blesdk.obfuscated.ok2;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.pl2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.sj2;
import com.fossil.blesdk.obfuscated.tj2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yf4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.ComplicationLastSetting;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaCustomizeViewModel extends ic {
    @DexIgnore
    public static /* final */ String F;
    @DexIgnore
    public static /* final */ a G; // = new a((fd4) null);
    @DexIgnore
    public /* final */ ComplicationRepository A;
    @DexIgnore
    public /* final */ ComplicationLastSettingRepository B;
    @DexIgnore
    public /* final */ WatchAppRepository C;
    @DexIgnore
    public /* final */ WatchAppLastSettingRepository D;
    @DexIgnore
    public /* final */ WatchFaceRepository E;
    @DexIgnore
    public DianaPreset c;
    @DexIgnore
    public MutableLiveData<DianaPreset> d; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<Complication> f; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<WatchApp> g; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<WatchFaceWrapper> h; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Complication> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> l; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> m; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<WatchApp> n; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> o; // = new MutableLiveData<>();
    @DexIgnore
    public LiveData<List<WatchFace>> p;
    @DexIgnore
    public List<WatchFace> q;
    @DexIgnore
    public /* final */ Gson r; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<String> s;
    @DexIgnore
    public /* final */ LiveData<String> t;
    @DexIgnore
    public /* final */ LiveData<Complication> u;
    @DexIgnore
    public /* final */ LiveData<WatchApp> v;
    @DexIgnore
    public /* final */ LiveData<Boolean> w;
    @DexIgnore
    public /* final */ cc<DianaPreset> x;
    @DexIgnore
    public /* final */ cc<List<WatchFace>> y;
    @DexIgnore
    public /* final */ DianaPresetRepository z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DianaCustomizeViewModel.F;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel a;

        @DexIgnore
        public b(DianaCustomizeViewModel dianaCustomizeViewModel) {
            this.a = dianaCustomizeViewModel;
        }

        @DexIgnore
        public final void a(DianaPreset dianaPreset) {
            T t;
            T t2;
            boolean z;
            boolean z2;
            FLogger.INSTANCE.getLocal().d(DianaCustomizeViewModel.G.a(), "current preset change=" + dianaPreset);
            if (dianaPreset != null) {
                String str = (String) this.a.i.a();
                String str2 = (String) this.a.j.a();
                FLogger.INSTANCE.getLocal().d(DianaCustomizeViewModel.G.a(), "selectedComplicationPos=" + str + " selectedComplicationId=" + str2);
                this.a.o.a(dianaPreset.getWatchFaceId());
                Iterator<T> it = dianaPreset.getComplications().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t;
                    if (!kd4.a((Object) dianaPresetComplicationSetting.getPosition(), (Object) str) || qf4.b(dianaPresetComplicationSetting.getId(), str2, true)) {
                        z2 = false;
                        continue;
                    } else {
                        z2 = true;
                        continue;
                    }
                    if (z2) {
                        break;
                    }
                }
                DianaPresetComplicationSetting dianaPresetComplicationSetting2 = (DianaPresetComplicationSetting) t;
                if (dianaPresetComplicationSetting2 != null) {
                    FLogger.INSTANCE.getLocal().d(DianaCustomizeViewModel.G.a(), "Update new complication id=" + dianaPresetComplicationSetting2.getId() + " at position=" + str);
                    this.a.j.a(dianaPresetComplicationSetting2.getId());
                }
                String str3 = (String) this.a.l.a();
                String str4 = (String) this.a.m.a();
                Iterator<T> it2 = dianaPreset.getWatchapps().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it2.next();
                    DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t2;
                    if (!kd4.a((Object) dianaPresetWatchAppSetting.getPosition(), (Object) str3) || qf4.b(dianaPresetWatchAppSetting.getId(), str4, true)) {
                        z = false;
                        continue;
                    } else {
                        z = true;
                        continue;
                    }
                    if (z) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) t2;
                if (dianaPresetWatchAppSetting2 != null) {
                    FLogger.INSTANCE.getLocal().d(DianaCustomizeViewModel.G.a(), "Update new watchapp id=" + dianaPresetWatchAppSetting2.getId() + " at position=" + str3);
                    this.a.m.a(dianaPresetWatchAppSetting2.getId());
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel a;

        @DexIgnore
        public c(DianaCustomizeViewModel dianaCustomizeViewModel) {
            this.a = dianaCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<Complication> apply(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = DianaCustomizeViewModel.G.a();
            local.d(a2, "transformComplicationIdToModel complicationId=" + str);
            DianaCustomizeViewModel dianaCustomizeViewModel = this.a;
            kd4.a((Object) str, "id");
            Complication c = dianaCustomizeViewModel.c(str);
            if (c != null) {
                this.a.k.a(c);
            }
            return this.a.k;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel a;

        @DexIgnore
        public d(DianaCustomizeViewModel dianaCustomizeViewModel) {
            this.a = dianaCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(String str) {
            T t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = DianaCustomizeViewModel.G.a();
            local.d(a2, "transformComplicationPosToId pos=" + str);
            DianaPreset dianaPreset = (DianaPreset) this.a.d.a();
            if (dianaPreset != null) {
                Iterator<T> it = dianaPreset.getComplications().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (kd4.a((Object) ((DianaPresetComplicationSetting) t).getPosition(), (Object) str)) {
                        break;
                    }
                }
                DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t;
                if (dianaPresetComplicationSetting != null) {
                    this.a.j.b(dianaPresetComplicationSetting.getId());
                }
            }
            return this.a.j;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel a;

        @DexIgnore
        public e(DianaCustomizeViewModel dianaCustomizeViewModel) {
            this.a = dianaCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<WatchApp> apply(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = DianaCustomizeViewModel.G.a();
            local.d(a2, "transformWatchAppIdToModel watchAppId=" + str);
            DianaCustomizeViewModel dianaCustomizeViewModel = this.a;
            kd4.a((Object) str, "id");
            WatchApp e = dianaCustomizeViewModel.e(str);
            if (e != null) {
                this.a.n.a(e);
            }
            return this.a.n;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel a;

        @DexIgnore
        public f(DianaCustomizeViewModel dianaCustomizeViewModel) {
            this.a = dianaCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(String str) {
            T t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = DianaCustomizeViewModel.G.a();
            local.d(a2, "transformWatchAppPosToId pos=" + str);
            DianaPreset dianaPreset = (DianaPreset) this.a.d.a();
            if (dianaPreset != null) {
                Iterator<T> it = dianaPreset.getWatchapps().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (kd4.a((Object) ((DianaPresetWatchAppSetting) t).getPosition(), (Object) str)) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t;
                if (dianaPresetWatchAppSetting != null) {
                    this.a.m.b(dianaPresetWatchAppSetting.getId());
                }
            }
            return this.a.m;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel a;

        @DexIgnore
        public g(DianaCustomizeViewModel dianaCustomizeViewModel) {
            this.a = dianaCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<Boolean> apply(DianaPreset dianaPreset) {
            ArrayList<DianaPresetComplicationSetting> complications = dianaPreset.getComplications();
            Gson d = this.a.r;
            DianaPreset e = this.a.c;
            if (e != null) {
                boolean a2 = sj2.a((List<DianaPresetComplicationSetting>) complications, d, (List<DianaPresetComplicationSetting>) e.getComplications());
                ArrayList<DianaPresetWatchAppSetting> watchapps = dianaPreset.getWatchapps();
                Gson d2 = this.a.r;
                DianaPreset e2 = this.a.c;
                if (e2 != null) {
                    boolean c = sj2.c(watchapps, d2, e2.getWatchapps());
                    String watchFaceId = dianaPreset.getWatchFaceId();
                    DianaPreset e3 = this.a.c;
                    if (e3 != null) {
                        boolean a3 = kd4.a((Object) watchFaceId, (Object) e3.getWatchFaceId());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a4 = DianaCustomizeViewModel.G.a();
                        local.d(a4, "isComplicationSettingListChange " + a2 + " isWatchAppChanged " + c + " isThemeChanged " + a3);
                        if (!a2 || !c || !a3) {
                            this.a.e.a(true);
                        } else {
                            this.a.e.a(false);
                        }
                        return this.a.e;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
    }

    /*
    static {
        String simpleName = DianaCustomizeViewModel.class.getSimpleName();
        kd4.a((Object) simpleName, "DianaCustomizeViewModel::class.java.simpleName");
        F = simpleName;
    }
    */

    @DexIgnore
    public DianaCustomizeViewModel(DianaPresetRepository dianaPresetRepository, ComplicationRepository complicationRepository, ComplicationLastSettingRepository complicationLastSettingRepository, WatchAppRepository watchAppRepository, WatchAppLastSettingRepository watchAppLastSettingRepository, WatchFaceRepository watchFaceRepository) {
        kd4.b(dianaPresetRepository, "mDianaPresetRepository");
        kd4.b(complicationRepository, "mComplicationRepository");
        kd4.b(complicationLastSettingRepository, "mLastComplicationSettingRepository");
        kd4.b(watchAppRepository, "mWatchAppRepository");
        kd4.b(watchAppLastSettingRepository, "mWatchAppLastSettingRepository");
        kd4.b(watchFaceRepository, "mWatchFaceRepository");
        this.z = dianaPresetRepository;
        this.A = complicationRepository;
        this.B = complicationLastSettingRepository;
        this.C = watchAppRepository;
        this.D = watchAppLastSettingRepository;
        this.E = watchFaceRepository;
        LiveData<String> b2 = hc.b(this.i, new d(this));
        kd4.a((Object) b2, "Transformations.switchMa\u2026plicationIdLiveData\n    }");
        this.s = b2;
        LiveData<String> b3 = hc.b(this.l, new f(this));
        kd4.a((Object) b3, "Transformations.switchMa\u2026dWatchAppIdLiveData\n    }");
        this.t = b3;
        LiveData<Complication> b4 = hc.b(this.s, new c(this));
        kd4.a((Object) b4, "Transformations.switchMa\u2026omplicationLiveData\n    }");
        this.u = b4;
        LiveData<WatchApp> b5 = hc.b(this.t, new e(this));
        kd4.a((Object) b5, "Transformations.switchMa\u2026tedWatchAppLiveData\n    }");
        this.v = b5;
        LiveData<Boolean> b6 = hc.b(this.d, new g(this));
        kd4.a((Object) b6, "Transformations.switchMa\u2026    isPresetChanged\n    }");
        this.w = b6;
        this.x = new b(this);
        this.y = new DianaCustomizeViewModel$watchFaceObserver$Anon1(this);
    }

    @DexIgnore
    public final List<Complication> b(String str) {
        kd4.b(str, "category");
        CopyOnWriteArrayList<Complication> copyOnWriteArrayList = this.f;
        ArrayList arrayList = new ArrayList();
        for (T next : copyOnWriteArrayList) {
            if (((Complication) next).getCategories().contains(str)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final MutableLiveData<DianaPreset> c() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Boolean> d() {
        LiveData<Boolean> liveData = this.w;
        if (liveData != null) {
            return liveData;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final DianaPreset e() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Complication> f() {
        LiveData<Complication> liveData = this.u;
        if (liveData != null) {
            return liveData;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v3, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeSetting} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v11, resolved type: com.portfolio.platform.data.model.setting.SecondTimezoneSetting} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v12, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeSetting} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v13, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeSetting} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final Parcelable g(String str) {
        CommuteTimeSetting commuteTimeSetting;
        DianaPresetComplicationSetting dianaPresetComplicationSetting;
        DianaPresetComplicationSetting dianaPresetComplicationSetting2;
        kd4.b(str, "complicationId");
        Gson gson = new Gson();
        if (!ok2.c.d(str)) {
            return null;
        }
        DianaPreset a2 = c().a();
        String str2 = "";
        if (a2 != null) {
            Iterator<T> it = a2.getComplications().iterator();
            while (true) {
                if (!it.hasNext()) {
                    dianaPresetComplicationSetting2 = null;
                    break;
                }
                dianaPresetComplicationSetting2 = it.next();
                if (kd4.a((Object) dianaPresetComplicationSetting2.getId(), (Object) str)) {
                    break;
                }
            }
            DianaPresetComplicationSetting dianaPresetComplicationSetting3 = (DianaPresetComplicationSetting) dianaPresetComplicationSetting2;
            if (dianaPresetComplicationSetting3 != null) {
                String settings = dianaPresetComplicationSetting3.getSettings();
                if (settings != null) {
                    str2 = settings;
                }
            }
        }
        if (str2.length() == 0) {
            ComplicationLastSetting complicationLastSetting = this.B.getComplicationLastSetting(str);
            if (complicationLastSetting != null) {
                str2 = complicationLastSetting.getSetting();
            }
            if (!TextUtils.isEmpty(str2) && a2 != null) {
                DianaPreset clone = a2.clone();
                Iterator<T> it2 = clone.getComplications().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        dianaPresetComplicationSetting = null;
                        break;
                    }
                    dianaPresetComplicationSetting = it2.next();
                    if (kd4.a((Object) dianaPresetComplicationSetting.getId(), (Object) str)) {
                        break;
                    }
                }
                DianaPresetComplicationSetting dianaPresetComplicationSetting4 = (DianaPresetComplicationSetting) dianaPresetComplicationSetting;
                if (dianaPresetComplicationSetting4 != null) {
                    dianaPresetComplicationSetting4.setSettings(str2);
                    a(clone);
                }
            }
        }
        if (oj2.a(str2)) {
            return null;
        }
        try {
            int hashCode = str.hashCode();
            if (hashCode != -829740640) {
                if (hashCode != 134170930) {
                    return null;
                }
                if (!str.equals("second-timezone")) {
                    return null;
                }
                SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) gson.a(str2, SecondTimezoneSetting.class);
                if (TextUtils.isEmpty(secondTimezoneSetting.getTimeZoneId())) {
                    return null;
                }
                commuteTimeSetting = secondTimezoneSetting;
            } else if (!str.equals("commute-time")) {
                return null;
            } else {
                CommuteTimeSetting commuteTimeSetting2 = (CommuteTimeSetting) gson.a(str2, CommuteTimeSetting.class);
                boolean isEmpty = TextUtils.isEmpty(commuteTimeSetting2.getAddress());
                commuteTimeSetting = commuteTimeSetting2;
                if (isEmpty) {
                    return null;
                }
            }
            return commuteTimeSetting;
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d(F, "exception when parse setting from json " + e2);
            return null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v7, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v11, resolved type: com.portfolio.platform.data.model.setting.WeatherWatchAppSetting} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v12, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v13, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final Parcelable h(String str) {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting2;
        kd4.b(str, "appId");
        if (!pl2.d.g(str)) {
            return null;
        }
        DianaPreset a2 = c().a();
        String str2 = "";
        if (a2 != null) {
            Iterator<T> it = a2.getWatchapps().iterator();
            while (true) {
                if (!it.hasNext()) {
                    dianaPresetWatchAppSetting2 = null;
                    break;
                }
                dianaPresetWatchAppSetting2 = it.next();
                if (kd4.a((Object) dianaPresetWatchAppSetting2.getId(), (Object) str)) {
                    break;
                }
            }
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting3 = (DianaPresetWatchAppSetting) dianaPresetWatchAppSetting2;
            if (dianaPresetWatchAppSetting3 != null) {
                String settings = dianaPresetWatchAppSetting3.getSettings();
                if (settings != null) {
                    str2 = settings;
                }
            }
        }
        if (str2.length() == 0) {
            WatchAppLastSetting watchAppLastSetting = this.D.getWatchAppLastSetting(str);
            if (watchAppLastSetting != null) {
                str2 = watchAppLastSetting.getSetting();
            }
            if (!TextUtils.isEmpty(str2) && a2 != null) {
                DianaPreset clone = a2.clone();
                Iterator<T> it2 = clone.getWatchapps().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        dianaPresetWatchAppSetting = null;
                        break;
                    }
                    dianaPresetWatchAppSetting = it2.next();
                    if (kd4.a((Object) dianaPresetWatchAppSetting.getId(), (Object) str)) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting4 = (DianaPresetWatchAppSetting) dianaPresetWatchAppSetting;
                if (dianaPresetWatchAppSetting4 != null) {
                    dianaPresetWatchAppSetting4.setSettings(str2);
                    a(clone);
                }
            }
        }
        if (oj2.a(str2)) {
            return null;
        }
        try {
            int hashCode = str.hashCode();
            if (hashCode != -829740640) {
                if (hashCode != 1223440372) {
                    return null;
                }
                if (!str.equals("weather")) {
                    return null;
                }
                WeatherWatchAppSetting weatherWatchAppSetting = (WeatherWatchAppSetting) this.r.a(str2, WeatherWatchAppSetting.class);
                List<WeatherLocationWrapper> locations = weatherWatchAppSetting.getLocations();
                ArrayList arrayList = new ArrayList();
                for (WeatherLocationWrapper next : locations) {
                    if (!TextUtils.isEmpty(next.getId())) {
                        arrayList.add(next);
                    }
                }
                commuteTimeWatchAppSetting = weatherWatchAppSetting;
                if (!(!arrayList.isEmpty())) {
                    return null;
                }
            } else if (!str.equals("commute-time")) {
                return null;
            } else {
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = (CommuteTimeWatchAppSetting) this.r.a(str2, CommuteTimeWatchAppSetting.class);
                boolean z2 = !commuteTimeWatchAppSetting2.getAddresses().isEmpty();
                commuteTimeWatchAppSetting = commuteTimeWatchAppSetting2;
                if (!z2) {
                    return null;
                }
            }
            return commuteTimeWatchAppSetting;
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d(F, "exception when parse setting from json " + e2);
            return null;
        }
    }

    @DexIgnore
    public final LiveData<WatchApp> i() {
        LiveData<WatchApp> liveData = this.v;
        if (liveData != null) {
            return liveData;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final MutableLiveData<String> j() {
        return this.l;
    }

    @DexIgnore
    public final void k(String str) {
        kd4.b(str, "complicationPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = F;
        local.d(str2, "setSelectedComplicationPos complicationPos=" + str);
        this.i.a(str);
    }

    @DexIgnore
    public final void l(String str) {
        kd4.b(str, "watchAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = F;
        local.d(str2, "setSelectedWatchApp watchAppPos=" + str);
        this.l.a(str);
    }

    @DexIgnore
    public final boolean m() {
        Boolean a2 = this.e.a();
        if (a2 == null || a2 == null) {
            return false;
        }
        return a2.booleanValue();
    }

    @DexIgnore
    public final Complication c(String str) {
        T t2;
        kd4.b(str, "complicationId");
        Iterator<T> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            t2 = it.next();
            if (kd4.a((Object) str, (Object) ((Complication) t2).getComplicationId())) {
                break;
            }
        }
        return (Complication) t2;
    }

    @DexIgnore
    public final List<WatchApp> d(String str) {
        kd4.b(str, "category");
        CopyOnWriteArrayList<WatchApp> copyOnWriteArrayList = this.g;
        ArrayList arrayList = new ArrayList();
        for (T next : copyOnWriteArrayList) {
            if (((WatchApp) next).getCategories().contains(str)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final WatchApp e(String str) {
        T t2;
        kd4.b(str, "watchAppId");
        Iterator<T> it = this.g.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            t2 = it.next();
            if (kd4.a((Object) str, (Object) ((WatchApp) t2).getWatchappId())) {
                break;
            }
        }
        return (WatchApp) t2;
    }

    @DexIgnore
    public final WatchFaceWrapper f(String str) {
        T t2;
        kd4.b(str, "watchFaceId");
        Iterator<T> it = this.h.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            t2 = it.next();
            if (kd4.a((Object) str, (Object) ((WatchFaceWrapper) t2).getId())) {
                break;
            }
        }
        return (WatchFaceWrapper) t2;
    }

    @DexIgnore
    public final boolean i(String str) {
        kd4.b(str, "complicationId");
        if (kd4.a((Object) str, (Object) "empty")) {
            return false;
        }
        DianaPreset a2 = this.d.a();
        if (a2 != null) {
            Iterator<DianaPresetComplicationSetting> it = a2.getComplications().iterator();
            while (it.hasNext()) {
                if (kd4.a((Object) it.next().getId(), (Object) str)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean j(String str) {
        kd4.b(str, "watchAppId");
        if (kd4.a((Object) str, (Object) "empty")) {
            return false;
        }
        DianaPreset a2 = this.d.a();
        if (a2 != null) {
            Iterator<DianaPresetWatchAppSetting> it = a2.getWatchapps().iterator();
            while (it.hasNext()) {
                DianaPresetWatchAppSetting next = it.next();
                next.component1();
                if (kd4.a((Object) next.component2(), (Object) str)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final fi4 a(String str, String str2, String str3) {
        kd4.b(str, "presetId");
        return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new DianaCustomizeViewModel$init$Anon1(this, str, str3, str2, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final CopyOnWriteArrayList<WatchFaceWrapper> k() {
        return this.h;
    }

    @DexIgnore
    public final void l() {
        this.f.clear();
        this.f.addAll(this.A.getAllComplicationRaw());
        this.g.clear();
        this.g.addAll(this.C.getAllWatchAppRaw());
    }

    @DexIgnore
    public final fi4 a(String str, DianaPreset dianaPreset, DianaPreset dianaPreset2, String str2, String str3) {
        kd4.b(str, "presetId");
        return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new DianaCustomizeViewModel$initFromSaveInstanceState$Anon1(this, str, dianaPreset, dianaPreset2, str3, str2, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(DianaPreset dianaPreset) {
        kd4.b(dianaPreset, "preset");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = F;
        local.d(str, "savePreset newPreset=" + dianaPreset);
        this.d.a(dianaPreset.clone());
    }

    @DexIgnore
    public void b() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = F;
        local.d(str, " same =" + kd4.a((Object) this.c, (Object) this.d.a()) + "onCleared originalPreset=" + this.c + " currentPreset=" + this.d.a());
        this.d.b(this.x);
        LiveData<List<WatchFace>> liveData = this.p;
        if (liveData != null) {
            liveData.b((cc<? super List<WatchFace>>) this.y);
        }
        super.b();
    }

    @DexIgnore
    public final /* synthetic */ Object a(String str, yb4<? super qa4> yb4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = F;
        local.d(str2, "initializePreset presetId=" + str);
        DianaPreset presetById = this.z.getPresetById(str);
        l();
        if (presetById != null) {
            this.c = presetById.clone();
            this.d.a(presetById.clone());
        }
        return a(presetById != null ? presetById.clone() : null, yb4);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final /* synthetic */ Object a(DianaPreset dianaPreset, yb4<? super qa4> yb4) {
        DianaCustomizeViewModel$initializeCustomizeTheme$Anon1 dianaCustomizeViewModel$initializeCustomizeTheme$Anon1;
        int i2;
        if (yb4 instanceof DianaCustomizeViewModel$initializeCustomizeTheme$Anon1) {
            dianaCustomizeViewModel$initializeCustomizeTheme$Anon1 = (DianaCustomizeViewModel$initializeCustomizeTheme$Anon1) yb4;
            int i3 = dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.result;
                Object a2 = cc4.a();
                i2 = dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.label;
                if (i2 != 0) {
                    na4.a(obj);
                    if (dianaPreset != null) {
                        String serialNumber = dianaPreset.getSerialNumber();
                        this.h.clear();
                        this.q = this.E.getWatchFacesWithSerial(serialNumber);
                        List<WatchFace> list = this.q;
                        if (list != null) {
                            dc4.a(this.h.addAll(tj2.a(list, (List<DianaPresetComplicationSetting>) dianaPreset.getComplications())));
                        }
                        this.p = this.E.getWatchFacesLiveDataWithSerial(serialNumber);
                        pi4 c2 = nh4.c();
                        DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1 dianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1 = new DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1((yb4) null, this, dianaPreset, dianaCustomizeViewModel$initializeCustomizeTheme$Anon1);
                        dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.L$Anon0 = this;
                        dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.L$Anon1 = dianaPreset;
                        dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.L$Anon2 = dianaPreset;
                        dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.L$Anon3 = serialNumber;
                        dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.label = 1;
                        obj = yf4.a(c2, dianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1, dianaCustomizeViewModel$initializeCustomizeTheme$Anon1);
                        if (obj == a2) {
                            return a2;
                        }
                    }
                    return qa4.a;
                } else if (i2 == 1) {
                    String str = (String) dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.L$Anon3;
                    DianaPreset dianaPreset2 = (DianaPreset) dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.L$Anon2;
                    DianaPreset dianaPreset3 = (DianaPreset) dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.L$Anon1;
                    DianaCustomizeViewModel dianaCustomizeViewModel = (DianaCustomizeViewModel) dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qa4 qa4 = (qa4) obj;
                return qa4.a;
            }
        }
        dianaCustomizeViewModel$initializeCustomizeTheme$Anon1 = new DianaCustomizeViewModel$initializeCustomizeTheme$Anon1(this, yb4);
        Object obj2 = dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.result;
        Object a22 = cc4.a();
        i2 = dianaCustomizeViewModel$initializeCustomizeTheme$Anon1.label;
        if (i2 != 0) {
        }
        qa4 qa42 = (qa4) obj2;
        return qa4.a;
    }

    @DexIgnore
    public final MutableLiveData<String> g() {
        return this.i;
    }

    @DexIgnore
    public final MutableLiveData<String> h() {
        return this.o;
    }
}
