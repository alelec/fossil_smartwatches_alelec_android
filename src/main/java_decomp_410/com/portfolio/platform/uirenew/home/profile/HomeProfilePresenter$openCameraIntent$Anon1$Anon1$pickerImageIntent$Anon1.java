package com.portfolio.platform.uirenew.home.profile;

import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.al2;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
public final class HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Intent>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ FragmentActivity $it;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1(FragmentActivity fragmentActivity, yb4 yb4) {
        super(2, yb4);
        this.$it = fragmentActivity;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1 homeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1 = new HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1(this.$it, yb4);
        homeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1.p$ = (zg4) obj;
        return homeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return al2.b(this.$it);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
