package com.portfolio.platform.uirenew.signup;

import android.content.Context;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.ho3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.usecase.CheckAuthenticationEmailExisting;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SignUpPresenter$signupEmail$Anon1 implements CoroutineUseCase.e<CheckAuthenticationEmailExisting.d, CheckAuthenticationEmailExisting.c> {
    @DexIgnore
    public /* final */ /* synthetic */ SignUpPresenter a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;

    @DexIgnore
    public SignUpPresenter$signupEmail$Anon1(SignUpPresenter signUpPresenter, String str, String str2) {
        this.a = signUpPresenter;
        this.b = str;
        this.c = str2;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(CheckAuthenticationEmailExisting.d dVar) {
        kd4.b(dVar, "responseValue");
        boolean a2 = dVar.a();
        if (a2) {
            this.a.N.i();
            ho3 c2 = this.a.N;
            String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_SignUp_EmailInUse_Text__ThisEmailIsAlreadyInUse);
            kd4.a((Object) a3, "LanguageHelper.getString\u2026_ThisEmailIsAlreadyInUse)");
            c2.N(a3);
            this.a.m();
        } else if (!a2) {
            SignUpEmailAuth signUpEmailAuth = new SignUpEmailAuth();
            signUpEmailAuth.setEmail(this.b);
            signUpEmailAuth.setPassword(this.c);
            fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1(this, (yb4) null), 3, (Object) null);
            this.a.a(signUpEmailAuth);
        }
    }

    @DexIgnore
    public void a(CheckAuthenticationEmailExisting.c cVar) {
        kd4.b(cVar, "errorValue");
        this.a.N.i();
        this.a.a(cVar.a(), "");
    }
}
