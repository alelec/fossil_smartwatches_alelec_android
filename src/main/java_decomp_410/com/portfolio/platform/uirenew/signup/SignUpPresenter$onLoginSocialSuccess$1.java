package com.portfolio.platform.uirenew.signup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SignUpPresenter$onLoginSocialSuccess$1 implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase.C6208d, com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase.C6206b> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.signup.SignUpPresenter f24208a;

    @DexIgnore
    public SignUpPresenter$onLoginSocialSuccess$1(com.portfolio.platform.uirenew.signup.SignUpPresenter signUpPresenter) {
        this.f24208a = signUpPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase.C6208d dVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f24208a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1(this, dVar.mo40412a(), (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo29641a(com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase.C6206b bVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bVar, "errorValue");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a = com.portfolio.platform.uirenew.signup.SignUpPresenter.f24156R.mo41710a();
        local.mo33255d(a, "onLoginSuccess download userInfo failed " + bVar.mo40410a());
        this.f24208a.mo41708y().clearAllUser();
        this.f24208a.f24170N.mo28035i();
        this.f24208a.mo41691a(bVar.mo40410a(), bVar.mo40411b());
    }
}
