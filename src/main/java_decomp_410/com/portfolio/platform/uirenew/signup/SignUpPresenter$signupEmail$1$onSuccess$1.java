package com.portfolio.platform.uirenew.signup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1", mo27670f = "SignUpPresenter.kt", mo27671l = {237, 238, 239}, mo27672m = "invokeSuspend")
public final class SignUpPresenter$signupEmail$1$onSuccess$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24220p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1 this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1$1", mo27670f = "SignUpPresenter.kt", mo27671l = {237}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1$1 */
    public static final class C68471 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qo2<com.portfolio.platform.data.model.room.fitness.ActivitySettings>>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24221p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68471(com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1 signUpPresenter$signupEmail$1$onSuccess$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = signUpPresenter$signupEmail$1$onSuccess$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68471 r0 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68471(this.this$0, yb4);
            r0.f24221p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68471) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24221p$;
                com.portfolio.platform.data.source.SummariesRepository x = this.this$0.this$0.f24217a.mo41707x();
                this.L$0 = zg4;
                this.label = 1;
                obj = x.fetchActivitySettings(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1$2")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1$2", mo27670f = "SignUpPresenter.kt", mo27671l = {238}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1$2 */
    public static final class C68482 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qo2<com.portfolio.platform.data.model.room.sleep.MFSleepSettings>>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24222p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68482(com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1 signUpPresenter$signupEmail$1$onSuccess$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = signUpPresenter$signupEmail$1$onSuccess$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68482 r0 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68482(this.this$0, yb4);
            r0.f24222p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68482) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24222p$;
                com.portfolio.platform.data.source.SleepSummariesRepository w = this.this$0.this$0.f24217a.mo41706w();
                this.L$0 = zg4;
                this.label = 1;
                obj = w.fetchLastSleepGoal(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1$3")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1$3", mo27670f = "SignUpPresenter.kt", mo27671l = {239}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1$3 */
    public static final class C68493 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qo2<com.portfolio.platform.data.model.GoalSetting>>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24223p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68493(com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1 signUpPresenter$signupEmail$1$onSuccess$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = signUpPresenter$signupEmail$1$onSuccess$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68493 r0 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68493(this.this$0, yb4);
            r0.f24223p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68493) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24223p$;
                com.portfolio.platform.data.source.GoalTrackingRepository u = this.this$0.this$0.f24217a.mo41704u();
                this.L$0 = zg4;
                this.label = 1;
                obj = u.fetchGoalSetting(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SignUpPresenter$signupEmail$1$onSuccess$1(com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1 signUpPresenter$signupEmail$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = signUpPresenter$signupEmail$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1 signUpPresenter$signupEmail$1$onSuccess$1 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1(this.this$0, yb4);
        signUpPresenter$signupEmail$1$onSuccess$1.f24220p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return signUpPresenter$signupEmail$1$onSuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007f A[RETURN] */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        com.fossil.blesdk.obfuscated.ug4 b;
        com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68493 r3;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f24220p$;
            com.fossil.blesdk.obfuscated.ug4 b2 = this.this$0.f24217a.mo31441c();
            com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68471 r6 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68471(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg42;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(b2, r6, this) == a) {
                return a;
            }
            zg4 = zg42;
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            b = this.this$0.f24217a.mo31441c();
            r3 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68493(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 3;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r3, this) == a) {
                return a;
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 3) {
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.ug4 b3 = this.this$0.f24217a.mo31441c();
        com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68482 r4 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68482(this, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.label = 2;
        if (com.fossil.blesdk.obfuscated.yf4.m30997a(b3, r4, this) == a) {
            return a;
        }
        b = this.this$0.f24217a.mo31441c();
        r3 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1.C68493(this, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.label = 3;
        if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r3, this) == a) {
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
