package com.portfolio.platform.uirenew.signup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.in2;
import com.fossil.blesdk.obfuscated.io3;
import com.fossil.blesdk.obfuscated.jn2;
import com.fossil.blesdk.obfuscated.jo3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kn2;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SignUpActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a G; // = new a((fd4) null);
    @DexIgnore
    public SignUpPresenter B;
    @DexIgnore
    public in2 C;
    @DexIgnore
    public jn2 D;
    @DexIgnore
    public kn2 E;
    @DexIgnore
    public MFLoginWechatManager F;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            kd4.b(context, "context");
            context.startActivity(new Intent(context, SignUpActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null) {
            in2 in2 = this.C;
            if (in2 != null) {
                in2.a(i, i2, intent);
                jn2 jn2 = this.D;
                if (jn2 != null) {
                    jn2.a(i, i2, intent);
                    kn2 kn2 = this.E;
                    if (kn2 != null) {
                        kn2.a(i, i2, intent);
                        Fragment a2 = getSupportFragmentManager().a((int) R.id.content);
                        if (a2 != null) {
                            a2.onActivityResult(i, i2, intent);
                            return;
                        }
                        return;
                    }
                    kd4.d("mLoginWeiboManager");
                    throw null;
                }
                kd4.d("mLoginGoogleManager");
                throw null;
            }
            kd4.d("mLoginFacebookManager");
            throw null;
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        io3 io3 = (io3) getSupportFragmentManager().a((int) R.id.content);
        if (io3 == null) {
            io3 = io3.m.a();
            a((Fragment) io3, f(), (int) R.id.content);
        }
        l42 g = PortfolioApp.W.c().g();
        if (io3 != null) {
            g.a(new jo3(this, io3)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.signup.SignUpContract.View");
    }

    @DexIgnore
    public void onNewIntent(Intent intent) {
        kd4.b(intent, "intent");
        super.onNewIntent(intent);
        MFLoginWechatManager mFLoginWechatManager = this.F;
        if (mFLoginWechatManager != null) {
            Intent intent2 = getIntent();
            kd4.a((Object) intent2, "getIntent()");
            mFLoginWechatManager.a(intent2);
            return;
        }
        kd4.d("mLoginWechatManager");
        throw null;
    }
}
