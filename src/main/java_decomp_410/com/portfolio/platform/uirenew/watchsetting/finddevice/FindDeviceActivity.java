package com.portfolio.platform.uirenew.watchsetting.finddevice;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.mq3;
import com.fossil.blesdk.obfuscated.rp3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FindDeviceActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public FindDevicePresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str) {
            kd4.b(context, "context");
            kd4.b(str, "serial");
            Intent intent = new Intent(context, FindDeviceActivity.class);
            intent.putExtra("SERIAL", str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDeviceActivity", "start with " + str);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        rp3 rp3 = (rp3) getSupportFragmentManager().a((int) R.id.content);
        if (rp3 == null) {
            rp3 = rp3.r.a();
            a((Fragment) rp3, "FindDeviceFragment", (int) R.id.content);
        }
        PortfolioApp.W.c().g().a(new mq3(rp3)).a(this);
        if (bundle != null && bundle.containsKey("SERIAL")) {
            String string = bundle.getString("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String f = f();
            local.d(f, "retrieve serial from savedInstanceState " + string);
            if (string != null) {
                FindDevicePresenter findDevicePresenter = this.B;
                if (findDevicePresenter != null) {
                    findDevicePresenter.b(string);
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        }
        if (getIntent() != null) {
            String stringExtra = getIntent().getStringExtra("SERIAL");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String f2 = f();
            local2.d(f2, "retrieve serial from intent " + stringExtra);
            FindDevicePresenter findDevicePresenter2 = this.B;
            if (findDevicePresenter2 != null) {
                kd4.a((Object) stringExtra, "serial");
                findDevicePresenter2.b(stringExtra);
                return;
            }
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        if (bundle != null) {
            FindDevicePresenter findDevicePresenter = this.B;
            if (findDevicePresenter != null) {
                bundle.putString("SERIAL", findDevicePresenter.k());
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        super.onSaveInstanceState(bundle);
    }
}
