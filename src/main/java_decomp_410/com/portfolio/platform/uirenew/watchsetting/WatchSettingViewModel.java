package com.portfolio.platform.uirenew.watchsetting;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.text.format.DateUtils;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ar2;
import com.fossil.blesdk.obfuscated.as3;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.jc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.tq2;
import com.fossil.blesdk.obfuscated.vj2;
import com.fossil.blesdk.obfuscated.wp3;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yq2;
import com.fossil.blesdk.obfuscated.zq2;
import com.fossil.wearables.fossil.R;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase;
import com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchSettingViewModel extends ic {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ a v; // = new a((fd4) null);
    @DexIgnore
    public MutableLiveData<b> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Handler d; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<Device> f;
    @DexIgnore
    public c g;
    @DexIgnore
    public b h;
    @DexIgnore
    public String i;
    @DexIgnore
    public /* final */ Runnable j;
    @DexIgnore
    public cc<Device> k;
    @DexIgnore
    public /* final */ f l;
    @DexIgnore
    public /* final */ DeviceRepository m;
    @DexIgnore
    public /* final */ SetVibrationStrengthUseCase n;
    @DexIgnore
    public /* final */ UnlinkDeviceUseCase o;
    @DexIgnore
    public /* final */ vj2 p;
    @DexIgnore
    public /* final */ en2 q;
    @DexIgnore
    public /* final */ SwitchActiveDeviceUseCase r;
    @DexIgnore
    public /* final */ tq2 s;
    @DexIgnore
    public /* final */ PortfolioApp t;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return WatchSettingViewModel.u;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public c d;
        @DexIgnore
        public String e;
        @DexIgnore
        public Integer f;
        @DexIgnore
        public Pair<Integer, String> g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public String i;
        @DexIgnore
        public String j;
        @DexIgnore
        public String k;
        @DexIgnore
        public String l;
        @DexIgnore
        public String m;
        @DexIgnore
        public boolean n;
        @DexIgnore
        public boolean o;
        @DexIgnore
        public ArrayList<PermissionCodes> p;

        @DexIgnore
        public b() {
            this(false, false, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65535, (fd4) null);
        }

        @DexIgnore
        public b(boolean z, boolean z2, boolean z3, c cVar, String str, Integer num, Pair<Integer, String> pair, boolean z4, String str2, String str3, String str4, String str5, String str6, boolean z5, boolean z6, ArrayList<PermissionCodes> arrayList) {
            this.a = z;
            this.b = z2;
            this.c = z3;
            this.d = cVar;
            this.e = str;
            this.f = num;
            this.g = pair;
            this.h = z4;
            this.i = str2;
            this.j = str3;
            this.k = str4;
            this.l = str5;
            this.m = str6;
            this.n = z5;
            this.o = z6;
            this.p = arrayList;
        }

        @DexIgnore
        public final boolean a() {
            return this.c;
        }

        @DexIgnore
        public final ArrayList<PermissionCodes> b() {
            return this.p;
        }

        @DexIgnore
        public final String c() {
            return this.l;
        }

        @DexIgnore
        public final String d() {
            return this.i;
        }

        @DexIgnore
        public final boolean e() {
            return this.h;
        }

        @DexIgnore
        public final String f() {
            return this.k;
        }

        @DexIgnore
        public final Pair<Integer, String> g() {
            return this.g;
        }

        @DexIgnore
        public final boolean h() {
            return this.n;
        }

        @DexIgnore
        public final String i() {
            return this.m;
        }

        @DexIgnore
        public final String j() {
            return this.j;
        }

        @DexIgnore
        public final boolean k() {
            return this.a;
        }

        @DexIgnore
        public final boolean l() {
            return this.b;
        }

        @DexIgnore
        public final boolean m() {
            return this.o;
        }

        @DexIgnore
        public final c n() {
            return this.d;
        }

        @DexIgnore
        public final String o() {
            return this.e;
        }

        @DexIgnore
        public final Integer p() {
            return this.f;
        }

        @DexIgnore
        public String toString() {
            String a2 = new Gson().a((Object) this);
            kd4.a((Object) a2, "Gson().toJson(this)");
            return a2;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(boolean z, boolean z2, boolean z3, c cVar, String str, Integer num, Pair pair, boolean z4, String str2, String str3, String str4, String str5, String str6, boolean z5, boolean z6, ArrayList arrayList, int i2, fd4 fd4) {
            this(r1, (r0 & 2) != 0 ? false : z2, (r0 & 4) != 0 ? false : z3, (r0 & 8) != 0 ? null : cVar, (r0 & 16) != 0 ? null : str, (r0 & 32) != 0 ? null : num, (r0 & 64) != 0 ? null : pair, (r0 & 128) != 0 ? false : z4, (r0 & 256) != 0 ? null : str2, (r0 & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? null : str3, (r0 & 1024) != 0 ? null : str4, (r0 & 2048) != 0 ? null : str5, (r0 & 4096) != 0 ? null : str6, (r0 & 8192) != 0 ? false : z5, (r0 & RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE) != 0 ? false : z6, (r0 & 32768) != 0 ? null : arrayList);
            int i3 = i2;
            boolean z7 = (i3 & 1) != 0 ? false : z;
        }

        @DexIgnore
        public final void a(c cVar) {
            this.d = cVar;
        }

        @DexIgnore
        public final void a(String str) {
            this.e = str;
        }

        @DexIgnore
        public static /* synthetic */ void a(b bVar, boolean z, boolean z2, boolean z3, c cVar, String str, Integer num, Pair pair, boolean z4, String str2, String str3, String str4, String str5, String str6, boolean z5, boolean z6, ArrayList arrayList, int i2, Object obj) {
            int i3 = i2;
            bVar.a((i3 & 1) != 0 ? false : z, (i3 & 2) != 0 ? false : z2, (i3 & 4) != 0 ? false : z3, (i3 & 8) != 0 ? null : cVar, (i3 & 16) != 0 ? null : str, (i3 & 32) != 0 ? null : num, (i3 & 64) != 0 ? null : pair, (i3 & 128) != 0 ? false : z4, (i3 & 256) != 0 ? null : str2, (i3 & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? null : str3, (i3 & 1024) != 0 ? null : str4, (i3 & 2048) != 0 ? null : str5, (i3 & 4096) != 0 ? null : str6, (i3 & 8192) != 0 ? false : z5, (i3 & RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE) != 0 ? false : z6, (i3 & 32768) != 0 ? null : arrayList);
        }

        @DexIgnore
        public final synchronized void a(boolean z, boolean z2, boolean z3, c cVar, String str, Integer num, Pair<Integer, String> pair, boolean z4, String str2, String str3, String str4, String str5, String str6, boolean z5, boolean z6, ArrayList<PermissionCodes> arrayList) {
            synchronized (this) {
                this.a = z;
                this.b = z2;
                this.c = z3;
                this.d = cVar;
                this.e = str;
                this.f = num;
                this.g = pair;
                this.h = z4;
                this.i = str2;
                this.j = str3;
                this.k = str4;
                this.l = str5;
                this.m = str6;
                this.n = z5;
                this.o = z6;
                this.p = arrayList;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public Device a;
        @DexIgnore
        public String b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public Boolean e;

        @DexIgnore
        public c(Device device, String str, boolean z, boolean z2, Boolean bool) {
            kd4.b(device, "deviceModel");
            kd4.b(str, "deviceName");
            this.a = device;
            this.b = str;
            this.c = z;
            this.d = z2;
            this.e = bool;
        }

        @DexIgnore
        public final Device a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final Boolean c() {
            return this.e;
        }

        @DexIgnore
        public final boolean d() {
            return this.d;
        }

        @DexIgnore
        public final boolean e() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof c) {
                    c cVar = (c) obj;
                    if (kd4.a((Object) this.a, (Object) cVar.a) && kd4.a((Object) this.b, (Object) cVar.b)) {
                        if (this.c == cVar.c) {
                            if (!(this.d == cVar.d) || !kd4.a((Object) this.e, (Object) cVar.e)) {
                                return false;
                            }
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            Device device = this.a;
            int i = 0;
            int hashCode = (device != null ? device.hashCode() : 0) * 31;
            String str = this.b;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            boolean z = this.c;
            if (z) {
                z = true;
            }
            int i2 = (hashCode2 + (z ? 1 : 0)) * 31;
            boolean z2 = this.d;
            if (z2) {
                z2 = true;
            }
            int i3 = (i2 + (z2 ? 1 : 0)) * 31;
            Boolean bool = this.e;
            if (bool != null) {
                i = bool.hashCode();
            }
            return i3 + i;
        }

        @DexIgnore
        public String toString() {
            return "WatchSettingWrapper(deviceModel=" + this.a + ", deviceName=" + this.b + ", isConnected=" + this.c + ", isActive=" + this.d + ", shouldShowVibrationUI=" + this.e + ")";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(Device device, String str, boolean z, boolean z2, Boolean bool, int i, fd4 fd4) {
            this(device, str, z, z2, (i & 16) != 0 ? null : bool);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.e<UnlinkDeviceUseCase.d, UnlinkDeviceUseCase.c> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public d(WatchSettingViewModel watchSettingViewModel, String str) {
            this.a = watchSettingViewModel;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UnlinkDeviceUseCase.d dVar) {
            kd4.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.v.a();
            local.d(a2, "removeDevice success serial=" + this.a.e + ".value");
            b.a(this.a.h, false, true, true, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65529, (Object) null);
            this.a.d();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:29:0x014c, code lost:
            if (r1.equals("UNLINK_FAIL_DUE_TO_STOP_WORKOUT_FAIL") != false) goto L_0x014e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x014e, code lost:
            com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.b.a(com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.h(r0.a), false, true, false, (com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.c) null, (java.lang.String) null, (java.lang.Integer) null, (kotlin.Pair) null, false, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, r0.b, false, false, (java.util.ArrayList) null, 61437, (java.lang.Object) null);
            com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.a(r0.a);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0176, code lost:
            com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.b.a(com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.h(r0.a), false, true, false, (com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.c) null, (java.lang.String) null, (java.lang.Integer) null, (kotlin.Pair) null, false, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, false, false, (java.util.ArrayList) null, 65533, (java.lang.Object) null);
            com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.a(r0.a);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:6:0x007f, code lost:
            if (r1.equals("UNLINK_FAIL_DUE_TO_SYNC_FAIL") != false) goto L_0x014e;
         */
        @DexIgnore
        public void a(UnlinkDeviceUseCase.c cVar) {
            Integer num;
            kd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.v.a();
            local.d(a2, "remove device " + this.a.e + ".value fail due to " + cVar.b());
            String b2 = cVar.b();
            switch (b2.hashCode()) {
                case -1767173543:
                    break;
                case -1697024179:
                    if (b2.equals("UNLINK_FAIL_DUE_TO_LACK_PERMISSION")) {
                        if (cVar.c() != null) {
                            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(new ArrayList(cVar.c()));
                            kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                            b h = this.a.h;
                            if (convertBLEPermissionErrorCode != null) {
                                b.a(h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) convertBLEPermissionErrorCode, 32765, (Object) null);
                                this.a.d();
                                return;
                            }
                            throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.enums.PermissionCodes> /* = java.util.ArrayList<com.portfolio.platform.enums.PermissionCodes> */");
                        }
                        return;
                    }
                    break;
                case -643272338:
                    if (b2.equals("UNLINK_FAIL_ON_SERVER")) {
                        if (cVar.c() == null || !(!cVar.c().isEmpty())) {
                            num = 600;
                        } else {
                            num = cVar.c().get(0);
                        }
                        kd4.a((Object) num, "if (errorValue.subErrorC\u2026                        }");
                        int intValue = num.intValue();
                        b h2 = this.a.h;
                        Integer valueOf = Integer.valueOf(intValue);
                        String a3 = cVar.a();
                        if (a3 == null) {
                            a3 = "";
                        }
                        b.a(h2, false, true, false, (c) null, (String) null, (Integer) null, new Pair(valueOf, a3), false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65469, (Object) null);
                        this.a.d();
                        return;
                    }
                    break;
                case 1447890910:
                    break;
                case 1768665169:
                    if (b2.equals("UNLINK_FAIL_DUE_TO_PENDING_WORKOUT")) {
                        b.a(this.a.h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, this.b, (String) null, false, false, (ArrayList) null, 63485, (Object) null);
                        this.a.d();
                        return;
                    }
                    break;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CoroutineUseCase.e<SwitchActiveDeviceUseCase.d, SwitchActiveDeviceUseCase.c> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public e(WatchSettingViewModel watchSettingViewModel, String str) {
            this.a = watchSettingViewModel;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SwitchActiveDeviceUseCase.d dVar) {
            kd4.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.v.a();
            local.d(a2, "removeDevice(), switch to newDevice=" + this.b + " success");
            b.a(this.a.h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65533, (Object) null);
            this.a.d();
            this.a.j(this.b);
            this.a.a(dVar.a());
        }

        @DexIgnore
        public void a(SwitchActiveDeviceUseCase.c cVar) {
            Integer num;
            kd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.v.a();
            local.d(a2, "removeDevice switch to " + this.b + " fail due to " + cVar.b());
            int b2 = cVar.b();
            if (b2 != 113) {
                if (b2 == 114) {
                    if (cVar.c() == null || !(!cVar.c().isEmpty())) {
                        num = 600;
                    } else {
                        num = cVar.c().get(0);
                    }
                    kd4.a((Object) num, "if (errorValue.subErrorC\u2026                        }");
                    int intValue = num.intValue();
                    b h = this.a.h;
                    Integer valueOf = Integer.valueOf(intValue);
                    String a3 = cVar.a();
                    if (a3 == null) {
                        a3 = "";
                    }
                    b.a(h, false, true, false, (c) null, (String) null, (Integer) null, new Pair(valueOf, a3), false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65469, (Object) null);
                    this.a.d();
                } else if (b2 != 117) {
                    b.a(this.a.h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65533, (Object) null);
                    this.a.d();
                } else {
                    b.a(this.a.h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, this.b, (String) null, (String) null, false, false, (ArrayList) null, 64509, (Object) null);
                    this.a.d();
                }
            } else if (cVar.c() != null) {
                List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(new ArrayList(cVar.c()));
                kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                b h2 = this.a.h;
                if (convertBLEPermissionErrorCode != null) {
                    b.a(h2, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) convertBLEPermissionErrorCode, 32765, (Object) null);
                    this.a.d();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.enums.PermissionCodes> /* = java.util.ArrayList<com.portfolio.platform.enums.PermissionCodes> */");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;

        @DexIgnore
        public f(WatchSettingViewModel watchSettingViewModel) {
            this.a = watchSettingViewModel;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            kd4.b(context, "context");
            kd4.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.v.a();
            local.d(a2, "onConnectionStateChangeReceiver, serial=" + stringExtra);
            if (kd4.a((Object) stringExtra, (Object) (String) this.a.e.a()) && qf4.b(stringExtra, this.a.t.e(), true)) {
                FLogger.INSTANCE.getLocal().d(WatchSettingViewModel.v.a(), "onConnectionStateChanged");
                LiveData c = this.a.f;
                if (c != null) {
                    Device device = (Device) c.a();
                    if (device != null) {
                        this.a.a(device);
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements m3<Object, LiveData<Object>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;

        @DexIgnore
        public g(WatchSettingViewModel watchSettingViewModel) {
            this.a = watchSettingViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<Device> apply(String str) {
            DeviceRepository d = this.a.m;
            kd4.a((Object) str, "serial");
            return d.getDeviceBySerialAsLiveData(str);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel e;

        @DexIgnore
        public h(WatchSettingViewModel watchSettingViewModel) {
            this.e = watchSettingViewModel;
        }

        @DexIgnore
        public final void run() {
            this.e.n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements cc<Device> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;

        @DexIgnore
        public i(WatchSettingViewModel watchSettingViewModel) {
            this.a = watchSettingViewModel;
        }

        @DexIgnore
        public final void a(Device device) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.v.a();
            local.d(a2, "on device changed " + device);
            this.a.a(device);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements CoroutineUseCase.e<SwitchActiveDeviceUseCase.d, SwitchActiveDeviceUseCase.c> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public j(WatchSettingViewModel watchSettingViewModel, String str) {
            this.a = watchSettingViewModel;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SwitchActiveDeviceUseCase.d dVar) {
            kd4.b(dVar, "responseValue");
            b.a(this.a.h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65533, (Object) null);
            this.a.d();
            this.a.j(this.b);
            this.a.a(dVar.a());
            this.a.j();
        }

        @DexIgnore
        public void a(SwitchActiveDeviceUseCase.c cVar) {
            Integer num;
            kd4.b(cVar, "errorValue");
            if (cVar.c() == null || !(!cVar.c().isEmpty())) {
                num = 600;
            } else {
                num = cVar.c().get(0);
            }
            kd4.a((Object) num, "if (errorValue.subErrorC\u2026                        }");
            int intValue = num.intValue();
            b h = this.a.h;
            Integer valueOf = Integer.valueOf(intValue);
            String a2 = cVar.a();
            if (a2 == null) {
                a2 = "";
            }
            b.a(h, false, true, false, (c) null, (String) null, (Integer) null, new Pair(valueOf, a2), false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65469, (Object) null);
            this.a.d();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements CoroutineUseCase.e<tq2.e, tq2.d> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;

        @DexIgnore
        public k(WatchSettingViewModel watchSettingViewModel) {
            this.a = watchSettingViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(tq2.e eVar) {
            kd4.b(eVar, "responseValue");
            b.a(this.a.h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65533, (Object) null);
            this.a.d();
        }

        @DexIgnore
        public void a(tq2.d dVar) {
            kd4.b(dVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.v.a();
            local.d(a2, "reconnectActiveDevice fail!! errorValue=" + dVar.a());
            int c = dVar.c();
            if (c == 1101 || c == 1112 || c == 1113) {
                List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(dVar.b());
                kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                b h = this.a.h;
                if (convertBLEPermissionErrorCode != null) {
                    b.a(h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) convertBLEPermissionErrorCode, 32765, (Object) null);
                    this.a.d();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.enums.PermissionCodes> /* = java.util.ArrayList<com.portfolio.platform.enums.PermissionCodes> */");
            }
            b.a(this.a.h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, true, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65405, (Object) null);
            this.a.d();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements CoroutineUseCase.e<ar2, yq2> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public l(WatchSettingViewModel watchSettingViewModel, String str, int i) {
            this.a = watchSettingViewModel;
            this.b = str;
            this.c = i;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ar2 ar2) {
            kd4.b(ar2, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.v.a();
            local.d(a2, "syncDevice success - serial=" + this.b);
            int i = this.c;
            if (i == 0) {
                WatchSettingViewModel watchSettingViewModel = this.a;
                Object a3 = watchSettingViewModel.e.a();
                if (a3 != null) {
                    kd4.a(a3, "mSerialLiveData.value!!");
                    watchSettingViewModel.a((String) a3, 1);
                    return;
                }
                kd4.a();
                throw null;
            } else if (i == 1) {
                WatchSettingViewModel watchSettingViewModel2 = this.a;
                Object a4 = watchSettingViewModel2.e.a();
                if (a4 != null) {
                    watchSettingViewModel2.b((String) a4);
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public void a(yq2 yq2) {
            kd4.b(yq2, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.v.a();
            local.d(a2, "syncDevice fail - serial=" + this.b + " - errorCode=" + yq2.a());
            int i = wp3.a[yq2.a().ordinal()];
            if (i != 1) {
                if (i == 2) {
                    int i2 = this.c;
                    if (i2 == 0) {
                        b.a(this.a.h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, this.b, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65277, (Object) null);
                        this.a.d();
                    } else if (i2 == 1) {
                        b.a(this.a.h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, this.b, (String) null, false, false, (ArrayList) null, 63485, (Object) null);
                        this.a.d();
                    }
                } else if (i == 3) {
                    int i3 = this.c;
                    if (i3 == 0) {
                        b h = this.a.h;
                        Object a3 = this.a.e.a();
                        if (a3 != null) {
                            b.a(h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) a3, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65021, (Object) null);
                            this.a.d();
                            return;
                        }
                        kd4.a();
                        throw null;
                    } else if (i3 == 1) {
                        b h2 = this.a.h;
                        Object a4 = this.a.e.a();
                        if (a4 != null) {
                            b.a(h2, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) a4, false, false, (ArrayList) null, 61437, (Object) null);
                            this.a.d();
                            return;
                        }
                        kd4.a();
                        throw null;
                    }
                } else if (i != 4) {
                    b.a(this.a.h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, true, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65405, (Object) null);
                    this.a.d();
                } else {
                    FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "User deny stopping workout");
                }
            } else if (yq2.b() != null) {
                List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(new ArrayList(yq2.b()));
                kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                b h3 = this.a.h;
                if (convertBLEPermissionErrorCode != null) {
                    b.a(h3, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) convertBLEPermissionErrorCode, 32765, (Object) null);
                    this.a.d();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.enums.PermissionCodes> /* = java.util.ArrayList<com.portfolio.platform.enums.PermissionCodes> */");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements CoroutineUseCase.e<SetVibrationStrengthUseCase.d, SetVibrationStrengthUseCase.c> {
        @DexIgnore
        public /* final */ /* synthetic */ Device a;
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public m(Device device, String str, WatchSettingViewModel watchSettingViewModel, int i) {
            this.a = device;
            this.b = watchSettingViewModel;
            this.c = i;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetVibrationStrengthUseCase.d dVar) {
            kd4.b(dVar, "responseValue");
            Integer vibrationStrength = this.a.getVibrationStrength();
            b.a(this.b.h, false, true, false, (c) null, (String) null, Integer.valueOf(this.c), (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65501, (Object) null);
            this.b.d();
            c e = this.b.g;
            if (e != null) {
                Device a2 = e.a();
                if (a2 != null) {
                    a2.setVibrationStrength(vibrationStrength);
                }
            }
            FLogger.INSTANCE.getLocal().d(WatchSettingViewModel.v.a(), "updateVibrationLevel success");
        }

        @DexIgnore
        public void a(SetVibrationStrengthUseCase.c cVar) {
            kd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.v.a();
            local.d(a2, "updateVibrationLevel fail!! errorValue=" + cVar.a());
            int c2 = cVar.c();
            if (c2 != 1101) {
                if (c2 == 8888) {
                    b.a(this.b.h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, true, false, (ArrayList) null, 57341, (Object) null);
                    this.b.d();
                    return;
                } else if (!(c2 == 1112 || c2 == 1113)) {
                    b.a(this.b.h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, true, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65405, (Object) null);
                    this.b.d();
                    return;
                }
            }
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(cVar.b());
            kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            b h = this.b.h;
            if (convertBLEPermissionErrorCode != null) {
                b.a(h, false, true, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) convertBLEPermissionErrorCode, 32765, (Object) null);
                this.b.d();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.enums.PermissionCodes> /* = java.util.ArrayList<com.portfolio.platform.enums.PermissionCodes> */");
        }
    }

    /*
    static {
        String simpleName = WatchSettingViewModel.class.getSimpleName();
        kd4.a((Object) simpleName, "WatchSettingViewModel::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public WatchSettingViewModel(DeviceRepository deviceRepository, SetVibrationStrengthUseCase setVibrationStrengthUseCase, UnlinkDeviceUseCase unlinkDeviceUseCase, vj2 vj2, en2 en2, SwitchActiveDeviceUseCase switchActiveDeviceUseCase, tq2 tq2, PortfolioApp portfolioApp) {
        DeviceRepository deviceRepository2 = deviceRepository;
        SetVibrationStrengthUseCase setVibrationStrengthUseCase2 = setVibrationStrengthUseCase;
        UnlinkDeviceUseCase unlinkDeviceUseCase2 = unlinkDeviceUseCase;
        vj2 vj22 = vj2;
        en2 en22 = en2;
        SwitchActiveDeviceUseCase switchActiveDeviceUseCase2 = switchActiveDeviceUseCase;
        tq2 tq22 = tq2;
        PortfolioApp portfolioApp2 = portfolioApp;
        kd4.b(deviceRepository2, "mDeviceRepository");
        kd4.b(setVibrationStrengthUseCase2, "mSetVibrationStrengthUseCase");
        kd4.b(unlinkDeviceUseCase2, "mUnlinkDeviceUseCase");
        kd4.b(vj22, "mDeviceSettingFactory");
        kd4.b(en22, "mSharedPreferencesManager");
        kd4.b(switchActiveDeviceUseCase2, "mSwitchActiveDeviceUseCase");
        kd4.b(tq22, "mReconnectDeviceUseCase");
        kd4.b(portfolioApp2, "mApp");
        this.m = deviceRepository2;
        this.n = setVibrationStrengthUseCase2;
        this.o = unlinkDeviceUseCase2;
        this.p = vj22;
        this.q = en22;
        this.r = switchActiveDeviceUseCase2;
        this.s = tq22;
        this.t = portfolioApp2;
        LiveData<Device> b2 = hc.b(this.e, new g(this));
        kd4.a((Object) b2, "Transformations.switchMa\u2026lAsLiveData(serial)\n    }");
        this.f = b2;
        this.h = new b(false, false, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65535, (fd4) null);
        this.j = new h(this);
        this.k = new i(this);
        this.l = new f(this);
    }

    @DexIgnore
    public final void j(String str) {
        kd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = u;
        local.d(str2, "setWatchSerial, serial=" + str);
        this.e.b(str);
    }

    @DexIgnore
    public final void k() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "removeDevice serial=" + this.e + ".value");
        b.a(this.h, true, false, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, FragmentActivity.MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS, (Object) null);
        d();
        String e2 = this.t.e();
        if (!kd4.a((Object) this.e.a(), (Object) e2) || qf4.a(e2)) {
            b(this.e.a());
        } else {
            b(e2, 1);
        }
    }

    @DexIgnore
    public final void l() {
        b.a(this.h, false, false, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65535, (Object) null);
        this.n.g();
        this.s.e();
        this.r.h();
        BleCommandResultManager.d.a(CommunicateMode.FORCE_CONNECT, CommunicateMode.SET_VIBRATION_STRENGTH, CommunicateMode.SWITCH_DEVICE);
        PortfolioApp portfolioApp = this.t;
        f fVar = this.l;
        portfolioApp.registerReceiver(fVar, new IntentFilter(this.t.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        LiveData<Device> liveData = this.f;
        if (liveData != null) {
            liveData.a((cc<? super Device>) this.k);
        }
    }

    @DexIgnore
    public final void m() {
        try {
            this.r.i();
            LiveData<Device> liveData = this.f;
            if (liveData != null) {
                liveData.b((cc<? super Device>) this.k);
            }
            this.s.f();
            this.n.h();
            this.t.unregisterReceiver(this.l);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = u;
            local.e(str, "stop with " + e2);
        }
        this.d.removeCallbacksAndMessages((Object) null);
    }

    @DexIgnore
    public final void n() {
        CharSequence charSequence;
        String a2 = this.e.a();
        if (a2 != null) {
            if (TextUtils.equals(PortfolioApp.W.c().e(), a2)) {
                PortfolioApp portfolioApp = this.t;
                kd4.a((Object) a2, "it");
                if (portfolioApp.h(a2)) {
                    charSequence = this.t.getString(R.string.DesignPatterns_AndroidQuickAccessPanel_SyncInProgressExpanded_Text__SyncInProgress);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = u;
                    local.d(str, "updateSyncTime " + charSequence);
                    b.a(this.h, false, false, false, (c) null, charSequence.toString(), (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65519, (Object) null);
                    this.i = charSequence.toString();
                    d();
                    this.d.postDelayed(this.j, 60000);
                }
            }
            long g2 = this.q.g(a2);
            if (((int) g2) == 0) {
                charSequence = "";
            } else if (System.currentTimeMillis() - g2 < 60000) {
                pd4 pd4 = pd4.a;
                String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatch_WatchSettings_Label__NumbermAgo);
                kd4.a((Object) a3, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                Object[] objArr = {1};
                charSequence = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                kd4.a((Object) charSequence, "java.lang.String.format(format, *args)");
            } else {
                charSequence = DateUtils.getRelativeTimeSpanString(g2, System.currentTimeMillis(), TimeUnit.SECONDS.toMillis(1));
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = u;
            local2.d(str2, "updateSyncTime " + charSequence);
            b.a(this.h, false, false, false, (c) null, charSequence.toString(), (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65519, (Object) null);
            this.i = charSequence.toString();
            d();
            this.d.postDelayed(this.j, 60000);
        }
    }

    @DexIgnore
    public final void b(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = u;
        local.d(str2, "removeDevice " + str);
        this.o.a(str != null ? new UnlinkDeviceUseCase.b(str) : null, new d(this, str));
    }

    @DexIgnore
    public final void c(String str) {
        kd4.b(str, "serial");
        b.a(this.h, true, false, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, FragmentActivity.MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS, (Object) null);
        d();
        b(str);
    }

    @DexIgnore
    public final void d(String str) {
        String str2 = str;
        kd4.b(str2, "serial");
        b.a(this.h, true, false, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, FragmentActivity.MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS, (Object) null);
        d();
        this.t.a(str2, true);
    }

    @DexIgnore
    public final String e() {
        c cVar = this.g;
        if (cVar != null) {
            String b2 = cVar.b();
            if (b2 != null) {
                return b2;
            }
        }
        return "";
    }

    @DexIgnore
    public final c f() {
        return this.g;
    }

    @DexIgnore
    public final String g() {
        return this.i;
    }

    @DexIgnore
    public final MutableLiveData<b> h() {
        return this.c;
    }

    @DexIgnore
    public final String i() {
        return this.e.a();
    }

    @DexIgnore
    public final void e(String str) {
        String str2 = str;
        kd4.b(str2, "serial");
        b.a(this.h, true, false, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, FragmentActivity.MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS, (Object) null);
        d();
        this.t.a(str2, true);
    }

    @DexIgnore
    public final void f(String str) {
        String str2 = str;
        kd4.b(str2, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = u;
        local.d(str3, "force switch to device " + str2);
        b.a(this.h, true, false, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, FragmentActivity.MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS, (Object) null);
        d();
        this.r.a(new SwitchActiveDeviceUseCase.b(str2, 4), new j(this, str2));
    }

    @DexIgnore
    public final void g(String str) {
        String str2 = str;
        kd4.b(str2, "serial");
        b.a(this.h, true, false, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, FragmentActivity.MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS, (Object) null);
        d();
        a(str2, 2);
    }

    @DexIgnore
    public final void h(String str) {
        String str2 = str;
        kd4.b(str2, "serial");
        b.a(this.h, true, false, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, FragmentActivity.MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS, (Object) null);
        d();
        this.t.a(str2, false);
    }

    @DexIgnore
    public final void i(String str) {
        String str2 = str;
        kd4.b(str2, "serial");
        b.a(this.h, true, false, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, FragmentActivity.MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS, (Object) null);
        d();
        this.t.a(str2, false);
    }

    @DexIgnore
    public final void j() {
        FLogger.INSTANCE.getLocal().d(u, "reconnectActiveDevice");
        b.a(this.h, true, false, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, FragmentActivity.MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS, (Object) null);
        d();
        tq2 tq2 = this.s;
        String a2 = this.e.a();
        if (a2 != null) {
            kd4.a((Object) a2, "mSerialLiveData.value!!");
            tq2.a(new tq2.c(a2), new k(this));
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void b(String str, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = u;
        local.d(str2, "syncDevice - serial=" + str + ", userAction=" + i2);
        this.p.b(str).a(new zq2(FossilDeviceSerialPatternUtil.getDeviceBySerial(str) != FossilDeviceSerialPatternUtil.DEVICE.DIANA ? 10 : 15, str, false), new l(this, str, i2));
    }

    @DexIgnore
    public final void a(int i2) {
        int i3 = i2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "updateVibrationLevel " + i3 + " of " + this.e.a());
        String a2 = this.e.a();
        if (a2 != null) {
            c cVar = this.g;
            if (cVar != null) {
                Device a3 = cVar.a();
                if (a3 != null) {
                    Integer vibrationStrength = a3.getVibrationStrength();
                    if ((vibrationStrength == null || vibrationStrength.intValue() != i3) && !FossilDeviceSerialPatternUtil.isSamSlimDevice(a2) && !FossilDeviceSerialPatternUtil.isDianaDevice(a2)) {
                        b.a(this.h, true, false, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, FragmentActivity.MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS, (Object) null);
                        d();
                        SetVibrationStrengthUseCase setVibrationStrengthUseCase = this.n;
                        kd4.a((Object) a2, "it");
                        setVibrationStrengthUseCase.a(new SetVibrationStrengthUseCase.b(a2, i3), new m(a3, a2, this, i3));
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void c() {
        FLogger.INSTANCE.getLocal().d(u, "checkToReconnectOrSwitchActiveDevice");
        String e2 = this.t.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "activeSerial=" + e2 + ", mSerialLiveData=" + this.e.a());
        if (kd4.a((Object) this.e.a(), (Object) e2)) {
            j();
        } else if (!as3.b(PortfolioApp.W.c())) {
            b bVar = this.h;
            Pair pair = r1;
            Pair pair2 = new Pair(601, "");
            b.a(bVar, false, false, false, (c) null, (String) null, (Integer) null, pair, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65471, (Object) null);
            d();
        } else {
            b.a(this.h, true, false, false, (c) null, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, FragmentActivity.MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS, (Object) null);
            d();
            if (!qf4.a(e2)) {
                b(e2, 0);
                return;
            }
            String a2 = this.e.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mSerialLiveData.value!!");
                a(a2, 1);
                return;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void d() {
        this.h.a(f());
        this.h.a(g());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, ".emitUIState(), uiModelWrapper=" + this.h);
        this.c.a(this.h);
    }

    @DexIgnore
    public final void a(Device device) {
        if (device != null) {
            fi4 unused = ag4.b(jc.a(this), (CoroutineContext) null, (CoroutineStart) null, new WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$Anon1(device, (yb4) null, this, device), 3, (Object) null);
        }
    }

    @DexIgnore
    public final void a(String str, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = u;
        local.d(str2, "switch to device " + str + " mode " + i2);
        this.r.a(new SwitchActiveDeviceUseCase.b(str, i2), new e(this, str));
    }
}
