package com.portfolio.platform.uirenew.troubleshooting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kp3;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.lp3;
import com.fossil.blesdk.obfuscated.np3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TroubleshootingActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public np3 B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            kd4.b(context, "context");
            context.startActivity(new Intent(context, TroubleshootingActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            kd4.b(context, "context");
            kd4.b(str, "serial");
            Intent intent = new Intent(context, TroubleshootingActivity.class);
            intent.putExtra("DEVICE_TYPE", DeviceHelper.o.f(str));
            context.startActivity(intent);
        }
    }

    @DexIgnore
    public final boolean a(Intent intent) {
        if (intent.getExtras() == null) {
            return false;
        }
        Bundle extras = intent.getExtras();
        if (extras != null) {
            return extras.getBoolean("DEVICE_TYPE", false);
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        kp3 kp3 = (kp3) getSupportFragmentManager().a((int) R.id.content);
        Intent intent = getIntent();
        kd4.a((Object) intent, "intent");
        boolean a2 = a(intent);
        if (kp3 == null) {
            kp3 = kp3.n.a(a2);
            a((Fragment) kp3, kp3.n.a(), (int) R.id.content);
        }
        l42 g = PortfolioApp.W.c().g();
        if (kp3 != null) {
            g.a(new lp3(kp3)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.troubleshooting.TroubleshootingContract.View");
    }
}
