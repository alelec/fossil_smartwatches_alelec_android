package com.portfolio.platform.uirenew.permission;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.bo3;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.sn3;
import com.fossil.blesdk.obfuscated.xn3;
import com.fossil.blesdk.obfuscated.yn3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PermissionActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public bo3 B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, ArrayList<Integer> arrayList) {
            kd4.b(context, "context");
            kd4.b(arrayList, "listPermissions");
            Intent intent = new Intent(context, PermissionActivity.class);
            intent.putIntegerArrayListExtra("EXTRA_LIST_PERMISSIONS", arrayList);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        sn3 sn3;
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        if (Build.VERSION.SDK_INT < 26) {
            setRequestedOrientation(1);
        }
        ArrayList<Integer> integerArrayListExtra = getIntent().getIntegerArrayListExtra("EXTRA_LIST_PERMISSIONS");
        if (integerArrayListExtra.contains(10)) {
            sn3 sn32 = (sn3) getSupportFragmentManager().a((int) R.id.content);
            sn3 sn33 = sn32;
            if (sn32 == null) {
                sn3 a2 = sn3.m.a();
                a((Fragment) a2, "AllowNotificationServiceFragment", (int) R.id.content);
                sn33 = a2;
            }
            kd4.a((Object) integerArrayListExtra, "listPermissions");
            hb4.a(integerArrayListExtra, PermissionActivity$onCreate$currentFragment$Anon1.INSTANCE);
            sn3 = sn33;
        } else {
            xn3 xn3 = (xn3) getSupportFragmentManager().a((int) R.id.content);
            sn3 = xn3;
            if (xn3 == null) {
                xn3 b = xn3.o.b();
                a((Fragment) b, xn3.o.a(), (int) R.id.content);
                sn3 = b;
            }
        }
        l42 g = PortfolioApp.W.c().g();
        if (sn3 != null) {
            g.a(new yn3(sn3, integerArrayListExtra)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.permission.PermissionContract.View");
    }
}
