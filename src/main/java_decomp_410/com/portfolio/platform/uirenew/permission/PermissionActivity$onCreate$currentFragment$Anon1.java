package com.portfolio.platform.uirenew.permission;

import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PermissionActivity$onCreate$currentFragment$Anon1 extends Lambda implements xc4<Integer, Boolean> {
    @DexIgnore
    public static /* final */ PermissionActivity$onCreate$currentFragment$Anon1 INSTANCE; // = new PermissionActivity$onCreate$currentFragment$Anon1();

    @DexIgnore
    public PermissionActivity$onCreate$currentFragment$Anon1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(invoke((Integer) obj));
    }

    @DexIgnore
    public final boolean invoke(Integer num) {
        return num == null || num.intValue() != 10;
    }
}
