package com.portfolio.platform.uirenew.alarm.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1", f = "DeleteAlarm.kt", l = {39}, m = "invokeSuspend")
public final class DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Alarm $requestAlarm;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeleteAlarm.DeleteAlarmsBroadcastReceiver this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1(DeleteAlarm.DeleteAlarmsBroadcastReceiver deleteAlarmsBroadcastReceiver, Alarm alarm, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = deleteAlarmsBroadcastReceiver;
        this.$requestAlarm = alarm;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1 deleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1 = new DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1(this.this$Anon0, this.$requestAlarm, yb4);
        deleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1.p$ = (zg4) obj;
        return deleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            AlarmsRepository a2 = DeleteAlarm.this.g;
            Alarm alarm = this.$requestAlarm;
            this.L$Anon0 = zg4;
            this.label = 1;
            if (a2.deleteAlarm(alarm, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
