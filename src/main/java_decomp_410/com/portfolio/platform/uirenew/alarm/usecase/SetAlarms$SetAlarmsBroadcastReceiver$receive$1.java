package com.portfolio.platform.uirenew.alarm.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.alarm.usecase.SetAlarms$SetAlarmsBroadcastReceiver$receive$1", mo27670f = "SetAlarms.kt", mo27671l = {43, 45, 58}, mo27672m = "invokeSuspend")
public final class SetAlarms$SetAlarmsBroadcastReceiver$receive$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.content.Intent $intent;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22225p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.SetAlarmsBroadcastReceiver this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAlarms$SetAlarmsBroadcastReceiver$receive$1(com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.SetAlarmsBroadcastReceiver setAlarmsBroadcastReceiver, android.content.Intent intent, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = setAlarmsBroadcastReceiver;
        this.$intent = intent;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.alarm.usecase.SetAlarms$SetAlarmsBroadcastReceiver$receive$1 setAlarms$SetAlarmsBroadcastReceiver$receive$1 = new com.portfolio.platform.uirenew.alarm.usecase.SetAlarms$SetAlarmsBroadcastReceiver$receive$1(this.this$0, this.$intent, yb4);
        setAlarms$SetAlarmsBroadcastReceiver$receive$1.f22225p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return setAlarms$SetAlarmsBroadcastReceiver$receive$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.alarm.usecase.SetAlarms$SetAlarmsBroadcastReceiver$receive$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.data.source.local.alarm.Alarm alarm;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22225p$;
            alarm = com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.this.f22216h.getAlarmById(com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.this.mo40706e().mo40713a().getUri());
            if (this.$intent.getIntExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED.ordinal()) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.f22211j.mo40709a(), "onReceive success");
                if (alarm == null) {
                    com.portfolio.platform.data.source.AlarmsRepository a2 = com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.this.f22216h;
                    com.portfolio.platform.data.source.local.alarm.Alarm a3 = com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.this.mo40706e().mo40713a();
                    this.L$0 = zg4;
                    this.L$1 = alarm;
                    this.label = 1;
                    if (a2.insertAlarm(a3, this) == a) {
                        return a;
                    }
                } else {
                    com.portfolio.platform.data.source.AlarmsRepository a4 = com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.this.f22216h;
                    com.portfolio.platform.data.source.local.alarm.Alarm a5 = com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.this.mo40706e().mo40713a();
                    this.L$0 = zg4;
                    this.L$1 = alarm;
                    this.label = 2;
                    if (a4.updateAlarm(a5, this) == a) {
                        return a;
                    }
                }
            } else {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.f22211j.mo40709a(), "onReceive failed");
                com.portfolio.platform.data.source.local.alarm.Alarm copy$default = com.portfolio.platform.data.source.local.alarm.Alarm.copy$default(com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.this.mo40706e().mo40713a(), (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, 0, 0, (int[]) null, false, false, (java.lang.String) null, (java.lang.String) null, 0, 2047, (java.lang.Object) null);
                if (alarm == null) {
                    copy$default.setActive(false);
                    com.portfolio.platform.data.source.AlarmsRepository a6 = com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.this.f22216h;
                    this.L$0 = zg4;
                    this.L$1 = alarm;
                    this.L$2 = copy$default;
                    this.label = 3;
                    if (a6.insertAlarm(copy$default, this) == a) {
                        return a;
                    }
                    alarm = copy$default;
                }
                com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.this.mo40702a(this.$intent, alarm);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        } else if (i == 1 || i == 2) {
            com.portfolio.platform.data.source.local.alarm.Alarm alarm2 = (com.portfolio.platform.data.source.local.alarm.Alarm) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 3) {
            com.portfolio.platform.data.source.local.alarm.Alarm alarm3 = (com.portfolio.platform.data.source.local.alarm.Alarm) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            alarm = (com.portfolio.platform.data.source.local.alarm.Alarm) this.L$2;
            com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.this.mo40702a(this.$intent, alarm);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.ul2 c = com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39516c("set_alarms");
        if (c != null) {
            c.mo31555a("");
        }
        com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39521e("set_alarms");
        com.portfolio.platform.uirenew.alarm.usecase.SetAlarms setAlarms = com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.this;
        setAlarms.mo34436a(new com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.C6271d(setAlarms.mo40706e().mo40713a()));
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
