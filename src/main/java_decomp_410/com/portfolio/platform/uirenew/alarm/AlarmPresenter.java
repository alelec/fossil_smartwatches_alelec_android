package com.portfolio.platform.uirenew.alarm;

import android.text.format.DateFormat;
import android.util.SparseIntArray;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.bn2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.xt2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yt2;
import com.fossil.blesdk.obfuscated.zt2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmPresenter extends xt2 {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ a v; // = new a((fd4) null);
    @DexIgnore
    public String f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public SparseIntArray i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public MFUser k;
    @DexIgnore
    public String l;
    @DexIgnore
    public /* final */ yt2 m;
    @DexIgnore
    public /* final */ String n;
    @DexIgnore
    public /* final */ ArrayList<Alarm> o;
    @DexIgnore
    public /* final */ Alarm p;
    @DexIgnore
    public /* final */ SetAlarms q;
    @DexIgnore
    public /* final */ AlarmHelper r;
    @DexIgnore
    public /* final */ DeleteAlarm s;
    @DexIgnore
    public /* final */ UserRepository t;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return AlarmPresenter.u;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<DeleteAlarm.d, DeleteAlarm.b> {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmPresenter a;

        @DexIgnore
        public b(AlarmPresenter alarmPresenter) {
            this.a = alarmPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(DeleteAlarm.d dVar) {
            kd4.b(dVar, "responseValue");
            this.a.m.a();
            this.a.m.x();
        }

        @DexIgnore
        public void a(DeleteAlarm.b bVar) {
            kd4.b(bVar, "errorValue");
            this.a.m.a();
            int b = bVar.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = AlarmPresenter.v.a();
            local.d(a2, "deleteAlarm() - deleteAlarm - onError - lastErrorCode = " + b);
            if (b != 1101) {
                if (b == 8888) {
                    this.a.m.c();
                    return;
                } else if (!(b == 1112 || b == 1113)) {
                    this.a.m.R();
                    return;
                }
            }
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(bVar.a());
            kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            yt2 l = this.a.m;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                l.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.e<SetAlarms.d, SetAlarms.b> {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmPresenter a;

        @DexIgnore
        public c(AlarmPresenter alarmPresenter) {
            this.a = alarmPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetAlarms.d dVar) {
            kd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(AlarmPresenter.v.a(), "saveAlarm SetAlarms onSuccess");
            this.a.m.a();
            Alarm b = this.a.p;
            if (b == null) {
                b = dVar.a();
            }
            this.a.m.a(b, true);
            this.a.j();
        }

        @DexIgnore
        public void a(SetAlarms.b bVar) {
            kd4.b(bVar, "errorValue");
            this.a.l = bVar.a().getUri();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = AlarmPresenter.v.a();
            local.d(a2, "saveAlarm SetAlarms onError - uri: " + this.a.l);
            this.a.m.a();
            int c = bVar.c();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = AlarmPresenter.v.a();
            local2.d(a3, "saveAlarm - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.a.m.c();
                    return;
                } else if (!(c == 1112 || c == 1113)) {
                    if (this.a.p == null) {
                        this.a.m.a(bVar.a(), false);
                        return;
                    } else {
                        this.a.m.R();
                        return;
                    }
                }
            }
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(bVar.b());
            kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            yt2 l = this.a.m;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                l.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    /*
    static {
        String simpleName = AlarmPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "AlarmPresenter::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public AlarmPresenter(yt2 yt2, String str, ArrayList<Alarm> arrayList, Alarm alarm, SetAlarms setAlarms, AlarmHelper alarmHelper, DeleteAlarm deleteAlarm, UserRepository userRepository) {
        kd4.b(yt2, "mView");
        kd4.b(str, "mDeviceId");
        kd4.b(arrayList, "mAlarms");
        kd4.b(setAlarms, "mSetAlarms");
        kd4.b(alarmHelper, "mAlarmHelper");
        kd4.b(deleteAlarm, "mDeleteAlarm");
        kd4.b(userRepository, "mUserRepository");
        this.m = yt2;
        this.n = str;
        this.o = arrayList;
        this.p = alarm;
        this.q = setAlarms;
        this.r = alarmHelper;
        this.s = deleteAlarm;
        this.t = userRepository;
    }

    @DexIgnore
    public void f() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new AlarmPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(u, "stop");
        this.q.g();
        this.s.g();
        PortfolioApp.W.c(this);
    }

    @DexIgnore
    public void h() {
        this.m.b();
        DeleteAlarm deleteAlarm = this.s;
        String str = this.n;
        ArrayList<Alarm> arrayList = this.o;
        Alarm alarm = this.p;
        if (alarm != null) {
            deleteAlarm.a(new DeleteAlarm.c(str, arrayList, alarm), new b(this));
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        bn2 bn2 = bn2.d;
        yt2 yt2 = this.m;
        if (yt2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.alarm.AlarmFragment");
        } else if (bn2.a(bn2, ((zt2) yt2).getContext(), "SET_ALARMS", false, 4, (Object) null)) {
            this.m.b();
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "Calendar.getInstance()");
            String t2 = rk2.t(instance.getTime());
            String str = this.l;
            if (str == null) {
                StringBuilder sb = new StringBuilder();
                MFUser mFUser = this.k;
                sb.append(mFUser != null ? mFUser.getUserId() : null);
                sb.append(':');
                Calendar instance2 = Calendar.getInstance();
                kd4.a((Object) instance2, "Calendar.getInstance()");
                sb.append(instance2.getTimeInMillis());
                str = sb.toString();
            }
            String str2 = str;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = u;
            local.d(str3, "saveAlarm - uri: " + str2);
            int[] a2 = this.h ? a(this.i) : new int[0];
            String str4 = this.f;
            if (str4 != null) {
                int i2 = this.g;
                int i3 = i2 / 60;
                int i4 = i2 % 60;
                if (a2 != null) {
                    boolean z = this.h;
                    kd4.a((Object) t2, "createdAt");
                    Alarm alarm = new Alarm((String) null, str2, str4, i3, i4, a2, true, z, t2, t2, 0, 1024, (fd4) null);
                    Alarm alarm2 = this.p;
                    if (alarm2 != null) {
                        alarm2.setActive(this.j);
                        Alarm alarm3 = this.p;
                        String str5 = this.f;
                        if (str5 != null) {
                            alarm3.setTitle(str5);
                            this.p.setTotalMinutes(this.g);
                            this.p.setRepeated(this.h);
                            this.p.setDays(a2);
                            Iterator<Alarm> it = this.o.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                Alarm next = it.next();
                                if (kd4.a((Object) next.getUri(), (Object) this.p.getUri())) {
                                    ArrayList<Alarm> arrayList = this.o;
                                    arrayList.set(arrayList.indexOf(next), this.p);
                                    break;
                                }
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        this.o.add(alarm);
                    }
                    FLogger.INSTANCE.getLocal().d(u, "saveAlarm SetAlarms");
                    SetAlarms setAlarms = this.q;
                    String str6 = this.n;
                    ArrayList<Alarm> arrayList2 = this.o;
                    Alarm alarm4 = this.p;
                    if (alarm4 != null) {
                        alarm = alarm4;
                    }
                    setAlarms.a(new SetAlarms.c(str6, arrayList2, alarm), new c(this));
                    return;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void j() {
        FLogger.INSTANCE.getLocal().d(u, "onSetAlarmsSuccess()");
        this.r.c(PortfolioApp.W.c());
        PortfolioApp.W.c().k(this.n);
    }

    @DexIgnore
    public final void k() {
        int i2 = Calendar.getInstance().get(10);
        int i3 = Calendar.getInstance().get(12);
        boolean z = true;
        if (Calendar.getInstance().get(9) != 1) {
            z = false;
        }
        b(String.valueOf(i2), String.valueOf(i3), z);
    }

    @DexIgnore
    public void l() {
        this.m.a(this);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008b  */
    public final void b(String str, String str2, boolean z) {
        int i2;
        int i3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = u;
        local.d(str3, "updateTime: hourValue = " + str + ", minuteValue = " + str2 + ", isPM = " + z);
        int i4 = 0;
        try {
            Integer valueOf = Integer.valueOf(str);
            kd4.a((Object) valueOf, "Integer.valueOf(hourValue)");
            i3 = valueOf.intValue();
            try {
                Integer valueOf2 = Integer.valueOf(str2);
                kd4.a((Object) valueOf2, "Integer.valueOf(minuteValue)");
                i2 = valueOf2.intValue();
            } catch (Exception e) {
                e = e;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str4 = u;
                local2.e(str4, "Exception when parse time e=" + e);
                i2 = 0;
                if (DateFormat.is24HourFormat(PortfolioApp.W.c())) {
                }
            }
        } catch (Exception e2) {
            e = e2;
            i3 = 0;
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            String str42 = u;
            local22.e(str42, "Exception when parse time e=" + e);
            i2 = 0;
            if (DateFormat.is24HourFormat(PortfolioApp.W.c())) {
            }
        }
        if (DateFormat.is24HourFormat(PortfolioApp.W.c())) {
            if (z) {
                i4 = i3 == 12 ? 12 : i3 + 12;
            } else if (i3 != 12) {
                i4 = i3;
            }
            this.g = (i4 * 60) + i2;
            return;
        }
        if (z) {
            i4 = i3 == 12 ? 12 : i3 + 12;
        } else if (i3 != 12) {
            i4 = i3;
        }
        this.g = (i4 * 60) + i2;
    }

    @DexIgnore
    public void a(boolean z, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "updateDaysRepeat: isStateEnabled = " + z + ", day = " + i2);
        if (z) {
            SparseIntArray sparseIntArray = this.i;
            if (sparseIntArray != null) {
                sparseIntArray.put(i2, i2);
            } else {
                kd4.a();
                throw null;
            }
        } else {
            SparseIntArray sparseIntArray2 = this.i;
            if (sparseIntArray2 != null) {
                sparseIntArray2.delete(i2);
            } else {
                kd4.a();
                throw null;
            }
        }
        Alarm alarm = this.p;
        if (alarm != null) {
            boolean z2 = true;
            if (alarm.getDays() != null) {
                SparseIntArray sparseIntArray3 = this.i;
                if (sparseIntArray3 != null) {
                    int size = sparseIntArray3.size();
                    int[] days = this.p.getDays();
                    if (days != null) {
                        if (size == days.length) {
                            int[] days2 = this.p.getDays();
                            if (days2 != null) {
                                int length = days2.length;
                                int i3 = 0;
                                while (true) {
                                    if (i3 >= length) {
                                        z2 = false;
                                        break;
                                    }
                                    int i4 = days2[i3];
                                    SparseIntArray sparseIntArray4 = this.i;
                                    if (sparseIntArray4 == null) {
                                        kd4.a();
                                        throw null;
                                    } else if (sparseIntArray4.indexOfKey(i4) < 0) {
                                        break;
                                    } else {
                                        i3++;
                                    }
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        }
                        this.m.e(z2);
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                SparseIntArray sparseIntArray5 = this.i;
                if (sparseIntArray5 == null) {
                    kd4.a();
                    throw null;
                } else if (sparseIntArray5.size() > 0) {
                    this.m.e(true);
                } else {
                    this.m.e(false);
                }
            }
        }
        SparseIntArray sparseIntArray6 = this.i;
        if (sparseIntArray6 == null) {
            kd4.a();
            throw null;
        } else if (sparseIntArray6.size() == 0) {
            this.h = false;
            this.m.k(false);
        }
    }

    @DexIgnore
    public void a(boolean z) {
        this.h = z;
        Alarm alarm = this.p;
        if (alarm != null) {
            if (this.h != alarm.isRepeated()) {
                this.m.e(true);
            } else {
                this.m.e(false);
            }
        }
        if (this.h) {
            SparseIntArray sparseIntArray = this.i;
            if (sparseIntArray == null) {
                kd4.a();
                throw null;
            } else if (sparseIntArray.size() == 0) {
                this.i = a(new int[]{2, 3, 4, 5, 6, 7, 1});
                yt2 yt2 = this.m;
                SparseIntArray sparseIntArray2 = this.i;
                if (sparseIntArray2 != null) {
                    yt2.a(sparseIntArray2);
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void a(String str, String str2, boolean z) {
        kd4.b(str, "hourValue");
        kd4.b(str2, "minuteValue");
        b(str, str2, z);
        Alarm alarm = this.p;
        if (alarm == null) {
            return;
        }
        if (this.g != alarm.getTotalMinutes()) {
            this.m.e(true);
        } else {
            this.m.e(false);
        }
    }

    @DexIgnore
    public final SparseIntArray a(int[] iArr) {
        int i2 = 0;
        int length = iArr != null ? iArr.length : 0;
        if (length <= 0) {
            return new SparseIntArray();
        }
        SparseIntArray sparseIntArray = new SparseIntArray();
        while (i2 < length) {
            if (iArr != null) {
                int i3 = iArr[i2];
                sparseIntArray.put(i3, i3);
                i2++;
            } else {
                kd4.a();
                throw null;
            }
        }
        return sparseIntArray;
    }

    @DexIgnore
    public final int[] a(SparseIntArray sparseIntArray) {
        int size = sparseIntArray != null ? sparseIntArray.size() : 0;
        if (size <= 0) {
            return null;
        }
        int i2 = size;
        int i3 = 0;
        while (i3 < i2) {
            if (sparseIntArray != null) {
                if (sparseIntArray.keyAt(i3) != sparseIntArray.valueAt(i3)) {
                    sparseIntArray.removeAt(i3);
                    i2--;
                    i3--;
                }
                i3++;
            } else {
                kd4.a();
                throw null;
            }
        }
        if (sparseIntArray != null) {
            int size2 = sparseIntArray.size();
            if (size2 <= 0) {
                return null;
            }
            int[] iArr = new int[size2];
            for (int i4 = 0; i4 < size2; i4++) {
                iArr[i4] = sparseIntArray.valueAt(i4);
            }
            return iArr;
        }
        kd4.a();
        throw null;
    }
}
