package com.portfolio.platform.uirenew.login;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1", mo27670f = "LoginPresenter.kt", mo27671l = {359, 367, 371, 375, 388, 396}, mo27672m = "invokeSuspend")
public final class LoginPresenter$onLoginSuccess$1$onSuccess$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.MFUser $currentUser;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23985p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1 this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$1", mo27670f = "LoginPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$1 */
    public static final class C67921 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23986p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C67921(com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1 loginPresenter$onLoginSuccess$1$onSuccess$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = loginPresenter$onLoginSuccess$1$onSuccess$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1.C67921 r0 = new com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1.C67921(this.this$0, yb4);
            r0.f23986p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1.C67921) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.f23984a.mo41568y().clearAllUser();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$2")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$2", mo27670f = "LoginPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$2 */
    public static final class C67932 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23987p$;

        @DexIgnore
        public C67932(com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1.C67932 r0 = new com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1.C67932(yb4);
            r0.f23987p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1.C67932) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34472S();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4")
    /* renamed from: com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4 */
    public static final class C67944 implements com.portfolio.platform.CoroutineUseCase.C5606e<com.fossil.blesdk.obfuscated.pn3, com.fossil.blesdk.obfuscated.nn3> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1 f23988a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef f23989b;

        @DexIgnore
        public C67944(com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1 loginPresenter$onLoginSuccess$1$onSuccess$1, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef) {
            this.f23988a = loginPresenter$onLoginSuccess$1$onSuccess$1;
            this.f23989b = ref$ObjectRef;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(com.fossil.blesdk.obfuscated.pn3 pn3) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(pn3, "responseValue");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.login.LoginPresenter.f23936O.mo41570a(), "onLoginSuccess download device setting success");
            this.f23988a.this$0.f23984a.mo41556c((java.lang.String) this.f23989b.element);
            this.f23988a.this$0.f23984a.mo41558o();
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().mo33266i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER, (java.lang.String) this.f23989b.element, com.portfolio.platform.uirenew.login.LoginPresenter.f23936O.mo41570a(), "[Sync Start] AUTO SYNC after login");
            com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34485a(this.f23988a.this$0.f23984a.mo41562s(), false, 13);
            this.f23988a.this$0.f23984a.f23948L.mo26932i();
            this.f23988a.this$0.f23984a.mo41557n();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo29641a(com.fossil.blesdk.obfuscated.nn3 nn3) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(nn3, "errorValue");
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23988a.this$0.f23984a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1(this, nn3, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LoginPresenter$onLoginSuccess$1$onSuccess$1(com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1 loginPresenter$onLoginSuccess$1, com.portfolio.platform.data.model.MFUser mFUser, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = loginPresenter$onLoginSuccess$1;
        this.$currentUser = mFUser;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1 loginPresenter$onLoginSuccess$1$onSuccess$1 = new com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1(this.this$0, this.$currentUser, yb4);
        loginPresenter$onLoginSuccess$1$onSuccess$1.f23985p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return loginPresenter$onLoginSuccess$1$onSuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00b5, code lost:
        r13.this$0.f23984a.f23948L.mo26932i();
        r13.this$0.f23984a.mo41552a(600, "");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00cb, code lost:
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x010a, code lost:
        r14 = r13.this$0.f23984a.mo31440b();
        r6 = new com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1.C67932((com.fossil.blesdk.obfuscated.yb4) null);
        r13.L$0 = r1;
        r13.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0120, code lost:
        if (com.fossil.blesdk.obfuscated.yf4.m30997a(r14, r6, r13) != r0) goto L_0x0123;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0122, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0123, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.login.LoginPresenter.f23936O.mo41570a(), "onLoginSuccess download require device and preset");
        r14 = r13.this$0.f23984a.mo31441c();
        r6 = new com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$response$1(r13, (com.fossil.blesdk.obfuscated.yb4) null);
        r13.L$0 = r1;
        r13.label = 4;
        r14 = com.fossil.blesdk.obfuscated.yf4.m30997a(r14, r6, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x014a, code lost:
        if (r14 != r0) goto L_0x014d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x014c, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x014d, code lost:
        r14 = (com.fossil.blesdk.obfuscated.qo2) r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0151, code lost:
        if ((r14 instanceof com.fossil.blesdk.obfuscated.ro2) == false) goto L_0x0296;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0153, code lost:
        r6 = (com.portfolio.platform.data.source.remote.ApiResponse) ((com.fossil.blesdk.obfuscated.ro2) r14).mo30673a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x015c, code lost:
        if (r6 == null) goto L_0x0162;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x015e, code lost:
        r5 = r6.get_items();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0162, code lost:
        r6 = new kotlin.jvm.internal.Ref$ObjectRef();
        r6.element = "";
        r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r7 = com.portfolio.platform.uirenew.login.LoginPresenter.f23936O.mo41570a();
        r4.mo33255d(r7, "onLoginSuccess allDevices " + r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0189, code lost:
        if (r5 == null) goto L_0x01f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x018b, code lost:
        r7 = r14;
        r8 = r1;
        r1 = r5.iterator();
        r4 = r5;
        r14 = r13;
        r5 = r6;
        r6 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x019a, code lost:
        if (r1.hasNext() == false) goto L_0x01ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x019c, code lost:
        r9 = (com.portfolio.platform.data.model.Device) r1.next();
        com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34531d(r9.getDeviceId(), r9.getMacAddress());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x01b7, code lost:
        if (r9.isActive() == false) goto L_0x0196;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x01b9, code lost:
        r5.element = r9.getDeviceId();
        com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34527c((java.lang.String) r5.element, r9.getMacAddress());
        r10 = com.portfolio.platform.util.DeviceUtils.f24406g.mo41910a();
        r14.L$0 = r8;
        r14.L$1 = r7;
        r14.L$2 = r6;
        r14.L$3 = r5;
        r14.L$4 = r4;
        r14.L$5 = r1;
        r14.L$6 = r9;
        r14.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x01eb, code lost:
        if (r10.mo41904a((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) r14) != r0) goto L_0x0196;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x01ed, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x01ee, code lost:
        r4 = r0;
        r0 = r5;
        r5 = r6;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x01f3, code lost:
        r7 = r14;
        r4 = r0;
        r0 = r6;
        r14 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x01ff, code lost:
        if (android.text.TextUtils.isEmpty((java.lang.String) r0.element) != false) goto L_0x027c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x020b, code lost:
        if (com.portfolio.platform.helper.DeviceHelper.f21188o.mo39566e((java.lang.String) r0.element) == false) goto L_0x027c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x020d, code lost:
        r14.this$0.f23984a.mo41565v().mo26999a((java.lang.String) r0.element, 0, false);
        r14.this$0.f23984a.mo41565v().mo27016b((java.lang.String) r0.element, true);
        r2 = r14.this$0.f23984a.mo41563t();
        r9 = r14.$currentUser.getUserId();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r9, "currentUser.userId");
        r6 = new com.portfolio.platform.usecase.GetSecretKeyUseCase.C6902b((java.lang.String) r0.element, r9);
        r14.L$0 = r1;
        r14.L$1 = r7;
        r14.L$2 = r5;
        r14.L$3 = r0;
        r14.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0257, code lost:
        if (com.fossil.blesdk.obfuscated.w52.m29539a(r2, r6, r14) != r4) goto L_0x025a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0259, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x025a, code lost:
        r14.this$0.f23984a.mo41562s().mo31816a((java.lang.String) r0.element).mo34435a(new com.fossil.blesdk.obfuscated.on3((java.lang.String) r0.element), new com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1.C67944(r14, r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x027c, code lost:
        r14.this$0.f23984a.mo41558o();
        r14.this$0.f23984a.f23948L.mo26932i();
        r14.this$0.f23984a.mo41557n();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0298, code lost:
        if ((r14 instanceof com.fossil.blesdk.obfuscated.po2) == false) goto L_0x02ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x029a, code lost:
        r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r1 = com.portfolio.platform.uirenew.login.LoginPresenter.f23936O.mo41570a();
        r2 = new java.lang.StringBuilder();
        r2.append("onLoginSuccess get all device fail ");
        r14 = (com.fossil.blesdk.obfuscated.po2) r14;
        r2.append(r14.mo30176a());
        r0.mo33255d(r1, r2.toString());
        r13.this$0.f23984a.mo41568y().clearAllUser();
        r13.this$0.f23984a.f23948L.mo26932i();
        r0 = r13.this$0.f23984a;
        r1 = r14.mo30176a();
        r14 = r14.mo30178c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x02e2, code lost:
        if (r14 == null) goto L_0x02eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x02e4, code lost:
        r14 = r14.getMessage();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x02e8, code lost:
        if (r14 == null) goto L_0x02eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x02eb, code lost:
        r14 = "";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x02ec, code lost:
        r0.mo41552a(r1, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x02f1, code lost:
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1 loginPresenter$onLoginSuccess$1$onSuccess$1;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        java.util.List list = null;
        switch (this.label) {
            case 0:
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                zg4 = this.f23985p$;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a2 = com.portfolio.platform.uirenew.login.LoginPresenter.f23936O.mo41570a();
                local.mo33255d(a2, "onLoginSuccess download userInfo success user " + this.$currentUser);
                if (this.$currentUser == null) {
                    com.fossil.blesdk.obfuscated.ug4 b = this.this$0.f23984a.mo31441c();
                    com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1.C67921 r3 = new com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1.C67921(this, (com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg4;
                    this.label = 1;
                    if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r3, this) == a) {
                        return a;
                    }
                } else {
                    com.fossil.blesdk.obfuscated.dn2.f14174p.mo26576a().mo26575o();
                    this.this$0.f23984a.mo41560q().mo39501b(this.$currentUser.getUserId());
                    com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
                    java.lang.String userId = this.$currentUser.getUserId();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) userId, "currentUser.userId");
                    c.mo34567p(userId);
                    com.portfolio.platform.PortfolioApp c2 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
                    this.L$0 = zg4;
                    this.label = 2;
                    if (c2.mo34480a((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                        return a;
                    }
                }
                break;
            case 1:
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 2:
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 3:
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 4:
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 5:
                com.portfolio.platform.data.model.Device device = (com.portfolio.platform.data.model.Device) this.L$6;
                java.util.Iterator it = (java.util.Iterator) this.L$5;
                java.util.List list2 = (java.util.List) this.L$4;
                kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef = (kotlin.jvm.internal.Ref$ObjectRef) this.L$3;
                java.util.List list3 = (java.util.List) this.L$2;
                com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                loginPresenter$onLoginSuccess$1$onSuccess$1 = this;
                break;
            case 6:
                kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2 = (kotlin.jvm.internal.Ref$ObjectRef) this.L$3;
                java.util.List list4 = (java.util.List) this.L$2;
                com.fossil.blesdk.obfuscated.qo2 qo22 = (com.fossil.blesdk.obfuscated.qo2) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg44 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                loginPresenter$onLoginSuccess$1$onSuccess$1 = this;
                break;
            default:
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
