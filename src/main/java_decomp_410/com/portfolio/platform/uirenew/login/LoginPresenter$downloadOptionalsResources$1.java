package com.portfolio.platform.uirenew.login;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1", mo27670f = "LoginPresenter.kt", mo27671l = {448, 457}, mo27672m = "invokeSuspend")
public final class LoginPresenter$downloadOptionalsResources$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23981p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.login.LoginPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1$1", mo27670f = "LoginPresenter.kt", mo27671l = {449, 450, 451, 452, 453}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1$1 */
    public static final class C67911 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23982p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C67911(com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1 loginPresenter$downloadOptionalsResources$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = loginPresenter$downloadOptionalsResources$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1.C67911 r0 = new com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1.C67911(this.this$0, yb4);
            r0.f23982p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1.C67911) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0087 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x009a A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00ae A[RETURN] */
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.zg4 zg4;
            com.portfolio.platform.data.source.DeviceRepository r;
            com.portfolio.platform.data.source.GoalTrackingRepository u;
            com.portfolio.platform.data.source.SleepSummariesRepository w;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg42 = this.f23982p$;
                com.portfolio.platform.data.source.WatchLocalizationRepository z = this.this$0.this$0.mo41569z();
                this.L$0 = zg42;
                this.label = 1;
                if (z.getWatchLocalizationFromServer(false, this) == a) {
                    return a;
                }
                zg4 = zg42;
            } else if (i == 1) {
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else if (i == 2) {
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                w = this.this$0.this$0.mo41566w();
                this.L$0 = zg4;
                this.label = 3;
                if (w.fetchLastSleepGoal(this) == a) {
                    return a;
                }
                u = this.this$0.this$0.mo41564u();
                this.L$0 = zg4;
                this.label = 4;
                if (u.fetchGoalSetting(this) == a) {
                }
                r = this.this$0.this$0.mo41561r();
                this.L$0 = zg4;
                this.label = 5;
                if (com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r, 0, this, 1, (java.lang.Object) null) == a) {
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else if (i == 3) {
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                u = this.this$0.this$0.mo41564u();
                this.L$0 = zg4;
                this.label = 4;
                if (u.fetchGoalSetting(this) == a) {
                    return a;
                }
                r = this.this$0.this$0.mo41561r();
                this.L$0 = zg4;
                this.label = 5;
                if (com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r, 0, this, 1, (java.lang.Object) null) == a) {
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else if (i == 4) {
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                r = this.this$0.this$0.mo41561r();
                this.L$0 = zg4;
                this.label = 5;
                if (com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r, 0, this, 1, (java.lang.Object) null) == a) {
                    return a;
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else if (i == 5) {
                com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            com.portfolio.platform.data.source.SummariesRepository x = this.this$0.this$0.mo41567x();
            this.L$0 = zg4;
            this.label = 2;
            if (x.fetchActivitySettings(this) == a) {
                return a;
            }
            w = this.this$0.this$0.mo41566w();
            this.L$0 = zg4;
            this.label = 3;
            if (w.fetchLastSleepGoal(this) == a) {
            }
            u = this.this$0.this$0.mo41564u();
            this.L$0 = zg4;
            this.label = 4;
            if (u.fetchGoalSetting(this) == a) {
            }
            r = this.this$0.this$0.mo41561r();
            this.L$0 = zg4;
            this.label = 5;
            if (com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r, 0, this, 1, (java.lang.Object) null) == a) {
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LoginPresenter$downloadOptionalsResources$1(com.portfolio.platform.uirenew.login.LoginPresenter loginPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = loginPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1 loginPresenter$downloadOptionalsResources$1 = new com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1(this.this$0, yb4);
        loginPresenter$downloadOptionalsResources$1.f23981p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return loginPresenter$downloadOptionalsResources$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005c  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.util.List list;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f23981p$;
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1.C67911 r5 = new com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1.C67911(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r5, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            list = (java.util.List) obj;
            if (list == null) {
                list = new java.util.ArrayList();
            }
            com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34508a((java.util.List<? extends com.misfit.frameworks.buttonservice.model.Alarm>) com.fossil.blesdk.obfuscated.nj2.m25699a(list));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.ug4 b2 = this.this$0.mo31441c();
        com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1$alarms$1 loginPresenter$downloadOptionalsResources$1$alarms$1 = new com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1$alarms$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.label = 2;
        obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b2, loginPresenter$downloadOptionalsResources$1$alarms$1, this);
        if (obj == a) {
            return a;
        }
        list = (java.util.List) obj;
        if (list == null) {
        }
        com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34508a((java.util.List<? extends com.misfit.frameworks.buttonservice.model.Alarm>) com.fossil.blesdk.obfuscated.nj2.m25699a(list));
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
