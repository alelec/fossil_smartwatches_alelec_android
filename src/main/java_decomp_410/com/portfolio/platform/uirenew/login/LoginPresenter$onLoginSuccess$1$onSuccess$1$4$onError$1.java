package com.portfolio.platform.uirenew.login;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1", mo27670f = "LoginPresenter.kt", mo27671l = {413}, mo27672m = "invokeSuspend")
public final class LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.nn3 $errorValue;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23990p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1.C67944 this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1$1", mo27670f = "LoginPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1$1 */
    public static final class C67951 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23991p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C67951(com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1 loginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = loginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1.C67951 r0 = new com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1.C67951(this.this$0, yb4);
            r0.f23991p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1.C67951) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.f23988a.this$0.f23984a.mo41568y().clearAllUser();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1(com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1.C67944 r1, com.fossil.blesdk.obfuscated.nn3 nn3, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = r1;
        this.$errorValue = nn3;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1 loginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1 = new com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1(this.this$0, this.$errorValue, yb4);
        loginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1.f23990p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return loginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23990p$;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.uirenew.login.LoginPresenter.f23936O.mo41570a();
            local.mo33255d(a2, "onLoginSuccess download device setting fail " + this.$errorValue.mo29643a());
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.f23988a.this$0.f23984a.mo31441c();
            com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1.C67951 r3 = new com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1.C67951(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.f23988a.this$0.f23984a.f23948L.mo26932i();
        this.this$0.f23988a.this$0.f23984a.mo41552a(this.$errorValue.mo29643a(), this.$errorValue.mo29644b());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
