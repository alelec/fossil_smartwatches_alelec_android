package com.portfolio.platform.uirenew.inappnotification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class InAppNotificationUtils$Companion$instance$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.portfolio.platform.uirenew.inappnotification.InAppNotificationUtils> {
    @DexIgnore
    public static /* final */ com.portfolio.platform.uirenew.inappnotification.InAppNotificationUtils$Companion$instance$2 INSTANCE; // = new com.portfolio.platform.uirenew.inappnotification.InAppNotificationUtils$Companion$instance$2();

    @DexIgnore
    public InAppNotificationUtils$Companion$instance$2() {
        super(0);
    }

    @DexIgnore
    public final com.portfolio.platform.uirenew.inappnotification.InAppNotificationUtils invoke() {
        return com.portfolio.platform.uirenew.inappnotification.InAppNotificationUtils.C6780b.f23928b.mo41545a();
    }
}
