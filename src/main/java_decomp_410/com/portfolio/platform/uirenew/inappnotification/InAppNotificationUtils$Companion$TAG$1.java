package com.portfolio.platform.uirenew.inappnotification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class InAppNotificationUtils$Companion$TAG$1 extends kotlin.jvm.internal.PropertyReference1 {
    @DexIgnore
    public static /* final */ com.fossil.blesdk.obfuscated.me4 INSTANCE; // = new com.portfolio.platform.uirenew.inappnotification.InAppNotificationUtils$Companion$TAG$1();

    @DexIgnore
    public java.lang.Object get(java.lang.Object obj) {
        return com.fossil.blesdk.obfuscated.vc4.m29206a((com.portfolio.platform.uirenew.inappnotification.InAppNotificationUtils) obj);
    }

    @DexIgnore
    public java.lang.String getName() {
        return "javaClass";
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.je4 getOwner() {
        return com.fossil.blesdk.obfuscated.md4.m25264a(com.fossil.blesdk.obfuscated.vc4.class, "app_fossilRelease");
    }

    @DexIgnore
    public java.lang.String getSignature() {
        return "getJavaClass(Ljava/lang/Object;)Ljava/lang/Class;";
    }
}
