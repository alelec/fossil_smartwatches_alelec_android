package com.portfolio.platform.uirenew.inappnotification;

import com.fossil.blesdk.obfuscated.je4;
import com.fossil.blesdk.obfuscated.md4;
import com.fossil.blesdk.obfuscated.me4;
import com.fossil.blesdk.obfuscated.vc4;
import kotlin.jvm.internal.PropertyReference1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class InAppNotificationUtils$Companion$TAG$Anon1 extends PropertyReference1 {
    @DexIgnore
    public static /* final */ me4 INSTANCE; // = new InAppNotificationUtils$Companion$TAG$Anon1();

    @DexIgnore
    public Object get(Object obj) {
        return vc4.a((InAppNotificationUtils) obj);
    }

    @DexIgnore
    public String getName() {
        return "javaClass";
    }

    @DexIgnore
    public je4 getOwner() {
        return md4.a(vc4.class, "app_fossilRelease");
    }

    @DexIgnore
    public String getSignature() {
        return "getJavaClass(Ljava/lang/Object;)Ljava/lang/Class;";
    }
}
