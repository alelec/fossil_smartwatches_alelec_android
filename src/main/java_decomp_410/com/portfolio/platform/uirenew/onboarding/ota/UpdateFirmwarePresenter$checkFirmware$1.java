package com.portfolio.platform.uirenew.onboarding.ota;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UpdateFirmwarePresenter$checkFirmware$1 implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6139d, com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6138c> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter f24036a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ /* synthetic */ java.lang.String f24037b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.Device f24038c;

    @DexIgnore
    public UpdateFirmwarePresenter$checkFirmware$1(com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter updateFirmwarePresenter, java.lang.String str, com.portfolio.platform.data.model.Device device) {
        this.f24036a = updateFirmwarePresenter;
        this.f24037b = str;
        this.f24038c = device;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6139d dVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f24036a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1(this, dVar, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo29641a(com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6138c cVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(cVar, "errorValue");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter.f24019s.mo41610a(), "checkFirmware - downloadFw FAILED!!!");
        this.f24036a.mo41608o().mo28535f0();
    }
}
