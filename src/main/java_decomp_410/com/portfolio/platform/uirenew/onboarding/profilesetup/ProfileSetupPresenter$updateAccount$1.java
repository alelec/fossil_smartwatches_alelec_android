package com.portfolio.platform.uirenew.onboarding.profilesetup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$1", mo27670f = "ProfileSetupPresenter.kt", mo27671l = {374}, mo27672m = "invokeSuspend")
public final class ProfileSetupPresenter$updateAccount$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.MFUser $user;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24077p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileSetupPresenter$updateAccount$1(com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter profileSetupPresenter, com.portfolio.platform.data.model.MFUser mFUser, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = profileSetupPresenter;
        this.$user = mFUser;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$1 profileSetupPresenter$updateAccount$1 = new com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$1(this.this$0, this.$user, yb4);
        profileSetupPresenter$updateAccount$1.f24077p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return profileSetupPresenter$updateAccount$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00b0, code lost:
        if (r6 != null) goto L_0x00b5;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.String str;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24077p$;
            this.this$0.f24066w.mo28083H0();
            this.this$0.f24066w.mo28099k();
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$1$response$1 profileSetupPresenter$updateAccount$1$response$1 = new com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$1$response$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, profileSetupPresenter$updateAccount$1$response$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
        if (qo2 instanceof com.fossil.blesdk.obfuscated.ro2) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter.f24048z, "updateAccount successfully");
            com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34538g().mo29086a(this.this$0);
            com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter profileSetupPresenter = this.this$0;
            com.portfolio.platform.enums.AuthType authType = this.$user.getAuthType();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) authType, "user.authType");
            java.lang.String value = authType.getValue();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) value, "user.authType.value");
            profileSetupPresenter.mo41623d(value);
        } else if (qo2 instanceof com.fossil.blesdk.obfuscated.po2) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter.f24048z, "updateAccount failed");
            com.fossil.blesdk.obfuscated.vl3 e = this.this$0.f24066w;
            com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
            int a3 = po2.mo30176a();
            com.portfolio.platform.data.model.ServerError c = po2.mo30178c();
            if (c != null) {
                str = c.getMessage();
            }
            str = "";
            e.mo28096g(a3, str);
            this.this$0.f24066w.mo28098i();
            this.this$0.f24066w.mo28100t0();
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
