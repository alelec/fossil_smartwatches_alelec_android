package com.portfolio.platform.uirenew.onboarding.ota;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.ul2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.util.DeviceUtils;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$Anon1", f = "UpdateFirmwarePresenter.kt", l = {247}, m = "invokeSuspend")
public final class UpdateFirmwarePresenter$updateFirmware$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeSerial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UpdateFirmwarePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$Anon1$Anon1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Pair<? extends String, ? extends String>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter$updateFirmware$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(UpdateFirmwarePresenter$updateFirmware$Anon1 updateFirmwarePresenter$updateFirmware$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = updateFirmwarePresenter$updateFirmware$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return DeviceUtils.g.a().a(this.this$Anon0.$activeSerial, (Device) null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.e<UpdateFirmwareUsecase.d, UpdateFirmwareUsecase.c> {
        @DexIgnore
        public void a(UpdateFirmwareUsecase.c cVar) {
            kd4.b(cVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateFirmwareUsecase.d dVar) {
            kd4.b(dVar, "responseValue");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateFirmwarePresenter$updateFirmware$Anon1(UpdateFirmwarePresenter updateFirmwarePresenter, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = updateFirmwarePresenter;
        this.$activeSerial = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        UpdateFirmwarePresenter$updateFirmware$Anon1 updateFirmwarePresenter$updateFirmware$Anon1 = new UpdateFirmwarePresenter$updateFirmware$Anon1(this.this$Anon0, this.$activeSerial, yb4);
        updateFirmwarePresenter$updateFirmware$Anon1.p$ = (zg4) obj;
        return updateFirmwarePresenter$updateFirmware$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((UpdateFirmwarePresenter$updateFirmware$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 b = this.this$Anon0.b();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(b, anon1, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair = (Pair) obj;
        String str = (String) pair.component1();
        String str2 = (String) pair.component2();
        if (!(str == null || str2 == null)) {
            ul2 c = this.this$Anon0.i;
            if (c != null) {
                c.a("old_firmware", str);
                if (c != null) {
                    c.a("new_firmware", str2);
                    if (c != null) {
                        c.d();
                    }
                }
            }
        }
        this.this$Anon0.p.a(new UpdateFirmwareUsecase.b(this.$activeSerial, false, 2, (fd4) null), new a());
        return qa4.a;
    }
}
