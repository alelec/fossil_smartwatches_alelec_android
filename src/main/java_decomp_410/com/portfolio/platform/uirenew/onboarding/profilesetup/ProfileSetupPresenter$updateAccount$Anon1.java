package com.portfolio.platform.uirenew.onboarding.profilesetup;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.vl3;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.enums.AuthType;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$Anon1", f = "ProfileSetupPresenter.kt", l = {374}, m = "invokeSuspend")
public final class ProfileSetupPresenter$updateAccount$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MFUser $user;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileSetupPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileSetupPresenter$updateAccount$Anon1(ProfileSetupPresenter profileSetupPresenter, MFUser mFUser, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = profileSetupPresenter;
        this.$user = mFUser;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ProfileSetupPresenter$updateAccount$Anon1 profileSetupPresenter$updateAccount$Anon1 = new ProfileSetupPresenter$updateAccount$Anon1(this.this$Anon0, this.$user, yb4);
        profileSetupPresenter$updateAccount$Anon1.p$ = (zg4) obj;
        return profileSetupPresenter$updateAccount$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileSetupPresenter$updateAccount$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00b0, code lost:
        if (r6 != null) goto L_0x00b5;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        String str;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            this.this$Anon0.w.H0();
            this.this$Anon0.w.k();
            ug4 a2 = this.this$Anon0.b();
            ProfileSetupPresenter$updateAccount$Anon1$response$Anon1 profileSetupPresenter$updateAccount$Anon1$response$Anon1 = new ProfileSetupPresenter$updateAccount$Anon1$response$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a2, profileSetupPresenter$updateAccount$Anon1$response$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo2 = (qo2) obj;
        if (qo2 instanceof ro2) {
            FLogger.INSTANCE.getLocal().d(ProfileSetupPresenter.z, "updateAccount successfully");
            PortfolioApp.W.c().g().a(this.this$Anon0);
            ProfileSetupPresenter profileSetupPresenter = this.this$Anon0;
            AuthType authType = this.$user.getAuthType();
            kd4.a((Object) authType, "user.authType");
            String value = authType.getValue();
            kd4.a((Object) value, "user.authType.value");
            profileSetupPresenter.d(value);
        } else if (qo2 instanceof po2) {
            FLogger.INSTANCE.getLocal().d(ProfileSetupPresenter.z, "updateAccount failed");
            vl3 e = this.this$Anon0.w;
            po2 po2 = (po2) qo2;
            int a3 = po2.a();
            ServerError c = po2.c();
            if (c != null) {
                str = c.getMessage();
            }
            str = "";
            e.g(a3, str);
            this.this$Anon0.w.i();
            this.this$Anon0.w.t0();
        }
        return qa4.a;
    }
}
