package com.portfolio.platform.uirenew.onboarding.ota;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.util.DeviceUtils;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
public final class UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Boolean>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1(UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1 updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1 updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1 = new UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1(this.this$Anon0, yb4);
        updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1.p$ = (zg4) obj;
        return updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return dc4.a(DeviceUtils.g.a().b(this.this$Anon0.this$Anon0.b));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
