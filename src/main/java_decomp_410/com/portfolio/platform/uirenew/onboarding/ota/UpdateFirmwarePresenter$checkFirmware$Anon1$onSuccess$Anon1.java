package com.portfolio.platform.uirenew.onboarding.ota;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1", f = "UpdateFirmwarePresenter.kt", l = {213, 216}, m = "invokeSuspend")
public final class UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ DownloadFirmwareByDeviceModelUsecase.d $responseValue;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UpdateFirmwarePresenter$checkFirmware$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1(UpdateFirmwarePresenter$checkFirmware$Anon1 updateFirmwarePresenter$checkFirmware$Anon1, DownloadFirmwareByDeviceModelUsecase.d dVar, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = updateFirmwarePresenter$checkFirmware$Anon1;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1 updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1 = new UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1(this.this$Anon0, this.$responseValue, yb4);
        updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1.p$ = (zg4) obj;
        return updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b0  */
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        String str;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg42 = this.p$;
            str = this.$responseValue.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.s.a();
            local.d(a2, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + str);
            ug4 b = this.this$Anon0.a.b();
            UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1 updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1 = new UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1(this, (yb4) null);
            this.L$Anon0 = zg42;
            this.L$Anon1 = str;
            this.label = 1;
            Object a3 = yf4.a(b, updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1, this);
            if (a3 == a) {
                return a;
            }
            Object obj2 = a3;
            zg4 = zg42;
            obj = obj2;
        } else if (i == 1) {
            str = (String) this.L$Anon1;
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            String str2 = (String) this.L$Anon1;
            zg4 zg43 = (zg4) this.L$Anon0;
            na4.a(obj);
            if (!((Boolean) obj).booleanValue()) {
                this.this$Anon0.a.o().v0();
            } else {
                UpdateFirmwarePresenter$checkFirmware$Anon1 updateFirmwarePresenter$checkFirmware$Anon1 = this.this$Anon0;
                updateFirmwarePresenter$checkFirmware$Anon1.a.c(updateFirmwarePresenter$checkFirmware$Anon1.c);
            }
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        boolean booleanValue = ((Boolean) obj).booleanValue();
        if (!booleanValue) {
            ug4 b2 = this.this$Anon0.a.b();
            UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isLatestFw$Anon1 updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isLatestFw$Anon1 = new UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isLatestFw$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = str;
            this.Z$Anon0 = booleanValue;
            this.label = 2;
            obj = yf4.a(b2, updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isLatestFw$Anon1, this);
            if (obj == a) {
                return a;
            }
            if (!((Boolean) obj).booleanValue()) {
            }
            return qa4.a;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String a4 = UpdateFirmwarePresenter.s.a();
        local2.e(a4, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + str + " but device is DianaEV1!!!");
        return qa4.a;
    }
}
