package com.portfolio.platform.uirenew.onboarding.exploreWatch;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.ds2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.pk3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ExploreWatchActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public ExploreWatchPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, boolean z) {
            kd4.b(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ExploreWatchActivity", "start - isOnboarding=" + z);
            Intent intent = new Intent(context, ExploreWatchActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        ds2 ds2 = (ds2) getSupportFragmentManager().a((int) R.id.content);
        boolean z = false;
        if (getIntent() != null) {
            z = getIntent().getBooleanExtra("IS_ONBOARDING_FLOW", false);
        }
        if (ds2 == null) {
            ds2 = ds2.o.a(z);
            a((Fragment) ds2, "ExploreWatchFragment", (int) R.id.content);
        }
        l42 g = PortfolioApp.W.c().g();
        if (ds2 != null) {
            g.a(new pk3(ds2)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchContract.View");
    }
}
