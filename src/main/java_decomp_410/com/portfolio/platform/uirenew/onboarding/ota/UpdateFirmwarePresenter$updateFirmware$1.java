package com.portfolio.platform.uirenew.onboarding.ota;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1", mo27670f = "UpdateFirmwarePresenter.kt", mo27671l = {247}, mo27672m = "invokeSuspend")
public final class UpdateFirmwarePresenter$updateFirmware$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $activeSerial;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24044p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1$1", mo27670f = "UpdateFirmwarePresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1$1 */
    public static final class C68101 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super kotlin.Pair<? extends java.lang.String, ? extends java.lang.String>>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24045p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68101(com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1 updateFirmwarePresenter$updateFirmware$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = updateFirmwarePresenter$updateFirmware$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1.C68101 r0 = new com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1.C68101(this.this$0, yb4);
            r0.f24045p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1.C68101) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return com.portfolio.platform.util.DeviceUtils.f24406g.mo41910a().mo41907a(this.this$0.$activeSerial, (com.portfolio.platform.data.model.Device) null);
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1$a")
    /* renamed from: com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1$a */
    public static final class C6811a implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6173d, com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6172c> {
        @DexIgnore
        /* renamed from: a */
        public void mo29641a(com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6172c cVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(cVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6173d dVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateFirmwarePresenter$updateFirmware$1(com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter updateFirmwarePresenter, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = updateFirmwarePresenter;
        this.$activeSerial = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1 updateFirmwarePresenter$updateFirmware$1 = new com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1(this.this$0, this.$activeSerial, yb4);
        updateFirmwarePresenter$updateFirmware$1.f24044p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return updateFirmwarePresenter$updateFirmware$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24044p$;
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1.C68101 r4 = new com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1.C68101(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b, r4, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        kotlin.Pair pair = (kotlin.Pair) obj;
        java.lang.String str = (java.lang.String) pair.component1();
        java.lang.String str2 = (java.lang.String) pair.component2();
        if (!(str == null || str2 == null)) {
            com.fossil.blesdk.obfuscated.ul2 c = this.this$0.f24023i;
            if (c != null) {
                c.mo31552a("old_firmware", str);
                if (c != null) {
                    c.mo31552a("new_firmware", str2);
                    if (c != null) {
                        c.mo31559d();
                    }
                }
            }
        }
        this.this$0.f24030p.mo34435a(new com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6171b(this.$activeSerial, false, 2, (com.fossil.blesdk.obfuscated.fd4) null), new com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1.C6811a());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
