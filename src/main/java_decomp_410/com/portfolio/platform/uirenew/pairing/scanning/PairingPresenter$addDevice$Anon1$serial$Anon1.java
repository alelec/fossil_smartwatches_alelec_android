package com.portfolio.platform.uirenew.pairing.scanning;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.source.DeviceRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$Anon1$serial$Anon1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
public final class PairingPresenter$addDevice$Anon1$serial$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super String>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PairingPresenter$addDevice$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingPresenter$addDevice$Anon1$serial$Anon1(PairingPresenter$addDevice$Anon1 pairingPresenter$addDevice$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = pairingPresenter$addDevice$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        PairingPresenter$addDevice$Anon1$serial$Anon1 pairingPresenter$addDevice$Anon1$serial$Anon1 = new PairingPresenter$addDevice$Anon1$serial$Anon1(this.this$Anon0, yb4);
        pairingPresenter$addDevice$Anon1$serial$Anon1.p$ = (zg4) obj;
        return pairingPresenter$addDevice$Anon1$serial$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PairingPresenter$addDevice$Anon1$serial$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            DeviceRepository c = this.this$Anon0.this$Anon0.u;
            String serial = this.this$Anon0.$shineDevice.getSerial();
            kd4.a((Object) serial, "shineDevice.serial");
            return c.getDeviceNameBySerial(serial);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
