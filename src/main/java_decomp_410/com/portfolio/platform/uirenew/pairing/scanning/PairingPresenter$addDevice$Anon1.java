package com.portfolio.platform.uirenew.pairing.scanning;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.gn3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import java.util.Iterator;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$Anon1", f = "PairingPresenter.kt", l = {370}, m = "invokeSuspend")
public final class PairingPresenter$addDevice$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ShineDevice $shineDevice;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PairingPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingPresenter$addDevice$Anon1(PairingPresenter pairingPresenter, ShineDevice shineDevice, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = pairingPresenter;
        this.$shineDevice = shineDevice;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        PairingPresenter$addDevice$Anon1 pairingPresenter$addDevice$Anon1 = new PairingPresenter$addDevice$Anon1(this.this$Anon0, this.$shineDevice, yb4);
        pairingPresenter$addDevice$Anon1.p$ = (zg4) obj;
        return pairingPresenter$addDevice$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PairingPresenter$addDevice$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        T t;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 a2 = this.this$Anon0.b();
            PairingPresenter$addDevice$Anon1$serial$Anon1 pairingPresenter$addDevice$Anon1$serial$Anon1 = new PairingPresenter$addDevice$Anon1$serial$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a2, pairingPresenter$addDevice$Anon1$serial$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        String str = (String) obj;
        Iterator<T> it = this.this$Anon0.r().iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (dc4.a(kd4.a((Object) ((ShineDevice) ((Pair) t).getFirst()).getSerial(), (Object) this.$shineDevice.getSerial())).booleanValue()) {
                break;
            }
        }
        Pair pair = (Pair) t;
        if (pair == null) {
            this.this$Anon0.r().add(new Pair(this.$shineDevice, str));
        } else {
            ((ShineDevice) pair.getFirst()).updateRssi(this.$shineDevice.getRssi());
        }
        gn3 g = this.this$Anon0.s;
        PairingPresenter pairingPresenter = this.this$Anon0;
        g.h(pairingPresenter.a(pairingPresenter.r()));
        return qa4.a;
    }
}
