package com.portfolio.platform.uirenew.pairing.scanning;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ar2;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dn3;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gn3;
import com.fossil.blesdk.obfuscated.jn3;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.rc;
import com.fossil.blesdk.obfuscated.ul2;
import com.fossil.blesdk.obfuscated.vj2;
import com.fossil.blesdk.obfuscated.vy2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yq2;
import com.fossil.blesdk.obfuscated.zq2;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PairingPresenter extends dn3 {
    @DexIgnore
    public static /* final */ a y; // = new a((fd4) null);
    @DexIgnore
    public ShineDevice f;
    @DexIgnore
    public /* final */ List<Pair<ShineDevice, String>> g; // = new ArrayList();
    @DexIgnore
    public /* final */ HashMap<String, List<Integer>> h; // = new HashMap<>();
    @DexIgnore
    public Handler i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public /* final */ ArrayList<Device> l; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<SKUModel> m; // = new ArrayList<>();
    @DexIgnore
    public ul2 n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public /* final */ PairingPresenter$pairDeviceCallback$Anon1 q; // = new PairingPresenter$pairDeviceCallback$Anon1(this);
    @DexIgnore
    public /* final */ c r; // = new c(this);
    @DexIgnore
    public /* final */ gn3 s;
    @DexIgnore
    public /* final */ LinkDeviceUseCase t;
    @DexIgnore
    public /* final */ DeviceRepository u;
    @DexIgnore
    public /* final */ vj2 v;
    @DexIgnore
    public /* final */ SetNotificationUseCase w;
    @DexIgnore
    public /* final */ en2 x;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            String simpleName = PairingPresenter.class.getSimpleName();
            kd4.a((Object) simpleName, "PairingPresenter::class.java.simpleName");
            return simpleName;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            PairingPresenter.this.c(true);
            if (PairingPresenter.this.r().isEmpty()) {
                PairingPresenter.this.s.O();
                return;
            }
            gn3 g = PairingPresenter.this.s;
            PairingPresenter pairingPresenter = PairingPresenter.this;
            g.k(pairingPresenter.a(pairingPresenter.r()));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter a;

        @DexIgnore
        public c(PairingPresenter pairingPresenter) {
            this.a = pairingPresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            T t;
            kd4.b(context, "context");
            kd4.b(intent, "intent");
            ShineDevice shineDevice = (ShineDevice) intent.getParcelableExtra("device");
            if (shineDevice == null) {
                FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "scanReceiver - ShineDevice is NULL!!!");
                return;
            }
            String serial = shineDevice.getSerial();
            if (TextUtils.isEmpty(serial)) {
                FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "scanReceiver - ShineDeviceSerial is NULL!!!");
                return;
            }
            FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "scanReceiver - receive device serial=" + serial);
            DeviceHelper e = DeviceHelper.o.e();
            T t2 = null;
            if (serial == null) {
                kd4.a();
                throw null;
            } else if (e.a(serial, this.a.n())) {
                int rssi = shineDevice.getRssi();
                this.a.a(serial, rssi);
                Iterator<T> it = this.a.r().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (kd4.a((Object) ((ShineDevice) ((Pair) t).getFirst()).getSerial(), (Object) serial)) {
                        break;
                    }
                }
                Pair pair = (Pair) t;
                Iterator<T> it2 = this.a.s().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    T next = it2.next();
                    if (kd4.a((Object) ((Device) next).getDeviceId(), (Object) serial)) {
                        t2 = next;
                        break;
                    }
                }
                Device device = (Device) t2;
                if (device == null && pair == null) {
                    FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "Add device " + serial + " to list");
                    this.a.c(shineDevice);
                } else if (device != null || pair == null || this.a.q()) {
                    FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "Device already in list, ignore it " + serial);
                } else {
                    FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "Pre-scan is not complete, update RSSI for scanned devices");
                    int c = this.a.c(serial);
                    if (c == Integer.MIN_VALUE) {
                        c = rssi;
                    }
                    ((ShineDevice) pair.getFirst()).updateRssi(c);
                    gn3 g = this.a.s;
                    PairingPresenter pairingPresenter = this.a;
                    g.h(pairingPresenter.a(pairingPresenter.r()));
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.e<ar2, yq2> {
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public d(PairingPresenter pairingPresenter, String str) {
            this.a = pairingPresenter;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ar2 ar2) {
            kd4.b(ar2, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = PairingPresenter.y.a();
            local.d(a2, "syncDevice success - serial=" + this.b);
            this.a.l();
        }

        @DexIgnore
        public void a(yq2 yq2) {
            kd4.b(yq2, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = PairingPresenter.y.a();
            local.d(a2, "syncDevice fail - serial=" + this.b + " - errorCode=" + yq2.a());
            this.a.s.a();
            int i = jn3.a[yq2.a().ordinal()];
            if (i != 1) {
                if (i == 2) {
                    this.a.s.s(this.b);
                } else if (i == 3) {
                    this.a.s.l(this.b);
                } else if (i != 4) {
                    this.a.s.c(yq2.a().ordinal(), this.b);
                } else {
                    FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "User deny stopping workout");
                }
            } else if (yq2.b() != null) {
                List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(new ArrayList(yq2.b()));
                kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                gn3 g = this.a.s;
                Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
                if (array != null) {
                    PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                    g.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
    }

    @DexIgnore
    public PairingPresenter(gn3 gn3, LinkDeviceUseCase linkDeviceUseCase, DeviceRepository deviceRepository, NotificationsRepository notificationsRepository, vy2 vy2, px2 px2, vj2 vj2, NotificationSettingsDatabase notificationSettingsDatabase, SetNotificationUseCase setNotificationUseCase, en2 en2) {
        kd4.b(gn3, "mPairingView");
        kd4.b(linkDeviceUseCase, "mLinkDeviceUseCase");
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(notificationsRepository, "mNotificationsRepository");
        kd4.b(vy2, "mGetApp");
        kd4.b(px2, "mGetAllContactGroups");
        kd4.b(vj2, "mDeviceSettingFactory");
        kd4.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        kd4.b(setNotificationUseCase, "mSetNotificationUseCase");
        kd4.b(en2, "mSharePrefs");
        this.s = gn3;
        this.t = linkDeviceUseCase;
        this.u = deviceRepository;
        this.v = vj2;
        this.w = setNotificationUseCase;
        this.x = en2;
    }

    @DexIgnore
    public void m() {
        this.t.m();
        gn3 gn3 = this.s;
        ShineDevice shineDevice = this.f;
        if (shineDevice != null) {
            String serial = shineDevice.getSerial();
            kd4.a((Object) serial, "mPairingDevice!!.serial");
            gn3.a(serial, this.k);
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final ArrayList<SKUModel> n() {
        return this.m;
    }

    @DexIgnore
    public final ul2 o() {
        return this.n;
    }

    @DexIgnore
    public final boolean p() {
        return this.k;
    }

    @DexIgnore
    public final boolean q() {
        return this.o;
    }

    @DexIgnore
    public final List<Pair<ShineDevice, String>> r() {
        return this.g;
    }

    @DexIgnore
    public final ArrayList<Device> s() {
        return this.l;
    }

    @DexIgnore
    public boolean t() {
        return this.j;
    }

    @DexIgnore
    public void u() {
        if (!this.j && this.p) {
            if (this.g.isEmpty()) {
                this.s.A();
                this.o = false;
                Handler handler = this.i;
                if (handler != null) {
                    handler.postDelayed(new b(), (long) 15000);
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                this.s.k(a(this.g));
            }
            x();
        }
    }

    @DexIgnore
    public final void v() {
        this.s.a();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.W.c().e());
        if (deviceBySerial != null && jn3.b[deviceBySerial.ordinal()] == 1) {
            this.s.t();
        } else {
            this.s.h();
        }
    }

    @DexIgnore
    public void w() {
        this.s.a(this);
    }

    @DexIgnore
    public void x() {
        try {
            IButtonConnectivity b2 = PortfolioApp.W.b();
            if (b2 != null) {
                b2.deviceStartScan();
            } else {
                kd4.a();
                throw null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    public final void y() {
        try {
            IButtonConnectivity b2 = PortfolioApp.W.b();
            if (b2 != null) {
                b2.deviceStopScan();
            } else {
                kd4.a();
                throw null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    public void b(boolean z) {
        this.k = z;
    }

    @DexIgnore
    public final void c(boolean z) {
        this.o = z;
    }

    @DexIgnore
    public final HashMap<String, String> d(String str) {
        kd4.b(str, "serial");
        HashMap<String, String> hashMap = new HashMap<>();
        SKUModel skuModelBySerialPrefix = this.u.getSkuModelBySerialPrefix(DeviceHelper.o.b(str));
        if (skuModelBySerialPrefix != null) {
            String sku = skuModelBySerialPrefix.getSku();
            if (sku != null) {
                hashMap.put("Style_Number", sku);
                String deviceName = skuModelBySerialPrefix.getDeviceName();
                if (deviceName != null) {
                    hashMap.put("Device_Name", deviceName);
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
        return hashMap;
    }

    @DexIgnore
    public void e(String str) {
        kd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = y.a();
        local.d(a2, "syncDevice - serial=" + str);
        this.v.b(str).a(new zq2(FossilDeviceSerialPatternUtil.getDeviceBySerial(str) != FossilDeviceSerialPatternUtil.DEVICE.DIANA ? 10 : 15, str, false), new d(this, str));
    }

    @DexIgnore
    public void f() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new PairingPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        Handler handler = this.i;
        if (handler != null) {
            if (handler != null) {
                handler.removeCallbacksAndMessages((Object) null);
            } else {
                kd4.a();
                throw null;
            }
        }
        y();
        this.t.n();
        rc.a((Context) PortfolioApp.W.c()).a((BroadcastReceiver) this.r);
    }

    @DexIgnore
    public ShineDevice h() {
        return this.f;
    }

    @DexIgnore
    public boolean i() {
        return this.k;
    }

    @DexIgnore
    public void j() {
        this.s.O();
    }

    @DexIgnore
    public void k() {
        this.s.b();
        this.t.l();
    }

    @DexIgnore
    public void l() {
        this.s.b();
        ShineDevice shineDevice = this.f;
        if (shineDevice != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = y.a();
            local.d(a2, "pairDevice - serial=" + shineDevice.getSerial());
            String serial = shineDevice.getSerial();
            kd4.a((Object) serial, "it.serial");
            String macAddress = shineDevice.getMacAddress();
            kd4.a((Object) macAddress, "it.macAddress");
            LinkDeviceUseCase.g gVar = new LinkDeviceUseCase.g(serial, macAddress);
            ul2 b2 = AnalyticsHelper.f.b("setup_device_session");
            this.n = b2;
            AnalyticsHelper.f.a("setup_device_session", b2);
            PortfolioApp c2 = PortfolioApp.W.c();
            CommunicateMode communicateMode = CommunicateMode.LINK;
            c2.a(communicateMode, "", communicateMode, gVar.a());
            this.t.a(gVar, this.q);
            ul2 ul2 = this.n;
            if (ul2 != null) {
                ul2.d();
            }
        }
    }

    @DexIgnore
    public void a(boolean z) {
        this.p = z;
    }

    @DexIgnore
    public void b(ShineDevice shineDevice) {
        kd4.b(shineDevice, "device");
        this.s.b();
        this.p = false;
        y();
        this.f = shineDevice;
        String e = PortfolioApp.W.c().e();
        if (!qf4.a(e)) {
            e(e);
        } else {
            l();
        }
    }

    @DexIgnore
    public final void c(ShineDevice shineDevice) {
        kd4.b(shineDevice, "shineDevice");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = y.a();
        local.d(a2, "addDevice serial=" + shineDevice.getSerial());
        String serial = shineDevice.getSerial();
        kd4.a((Object) serial, "shineDevice.serial");
        int c2 = c(serial);
        if (c2 == Integer.MIN_VALUE) {
            c2 = shineDevice.getRssi();
        }
        shineDevice.updateRssi(c2);
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new PairingPresenter$addDevice$Anon1(this, shineDevice, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(int i2) {
        this.s.n();
    }

    @DexIgnore
    public void a(ShineDevice shineDevice) {
        kd4.b(shineDevice, "pairingDevice");
        this.f = shineDevice;
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, "serial");
        this.s.b();
        PortfolioApp.W.c().a(str, true);
    }

    @DexIgnore
    public final int c(String str) {
        kd4.b(str, "serial");
        if (!this.h.containsKey(str)) {
            return Integer.MIN_VALUE;
        }
        double d2 = 0.0d;
        List<Number> list = this.h.get(str);
        if (list == null) {
            return Integer.MIN_VALUE;
        }
        for (Number intValue : list) {
            d2 += (double) intValue.intValue();
        }
        int size = list.size();
        if (size <= 0) {
            size = 1;
        }
        return (int) (d2 / ((double) size));
    }

    @DexIgnore
    public void d(boolean z) {
        this.j = z;
    }

    @DexIgnore
    public final void a(String str, int i2) {
        kd4.b(str, "serial");
        int i3 = (i2 == 0 || i2 == -999999) ? 0 : i2;
        if (this.h.containsKey(str)) {
            List list = this.h.get(str);
            if (list != null && !list.contains(0)) {
                if (list.size() < 5) {
                    list.add(Integer.valueOf(i2));
                    return;
                }
                list.remove(0);
                list.add(Integer.valueOf(i2));
                return;
            }
            return;
        }
        this.h.put(str, cb4.d(Integer.valueOf(i3)));
    }

    @DexIgnore
    public void b(String str) {
        kd4.b(str, "serial");
        this.s.b();
        PortfolioApp.W.c().a(str, false);
    }

    @DexIgnore
    public final void a(String str, Map<String, String> map) {
        kd4.b(str, Constants.EVENT);
        kd4.b(map, "values");
        AnalyticsHelper.f.c().a(str, (Map<String, ? extends Object>) map);
    }

    @DexIgnore
    public final void a(Map<String, String> map) {
        kd4.b(map, "properties");
        AnalyticsHelper.f.c().a(map);
    }

    @DexIgnore
    public final List<Pair<ShineDevice, String>> a(List<Pair<ShineDevice, String>> list) {
        kd4.b(list, Constants.DEVICES);
        ArrayList arrayList = new ArrayList();
        for (T next : list) {
            if (((ShineDevice) ((Pair) next).getFirst()).getRssi() > -150) {
                arrayList.add(next);
            }
        }
        return kb4.d(arrayList);
    }
}
