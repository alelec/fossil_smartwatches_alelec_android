package com.portfolio.platform.uirenew.pairing.scanning;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import java.util.HashMap;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
public final class PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super HashMap<String, String>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1(PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1 pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1 pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1 = new PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1(this.this$Anon0, yb4);
        pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1.p$ = (zg4) obj;
        return pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1 pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1 = this.this$Anon0;
            return pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1.this$Anon0.a.d(pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1.$errorValue.a());
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
