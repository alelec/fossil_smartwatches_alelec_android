package com.portfolio.platform.uirenew.pairing.scanning;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PairingPresenter$pairDeviceCallback$1 implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6150i, com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6149h> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter f24113a;

    @DexIgnore
    public PairingPresenter$pairDeviceCallback$1(com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter pairingPresenter) {
        this.f24113a = pairingPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6150i iVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(iVar, "responseValue");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a = com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter.f24087y.mo41670a();
        local.mo33255d(a, "pairDeviceCallback() pairDevice, isSkipOTA=" + this.f24113a.f24106x.mo26982T() + ", response=" + iVar.getClass().getSimpleName());
        if (iVar instanceof com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6142a) {
            this.f24113a.f24101s.mo27729a();
            this.f24113a.f24101s.mo27731a(((com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6142a) iVar).mo40314a(), this.f24113a.mo41660p());
        } else if (iVar instanceof com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6146e) {
            this.f24113a.f24101s.mo27729a();
            this.f24113a.f24101s.mo27734b(((com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6146e) iVar).mo40316a(), this.f24113a.mo41660p());
        } else if (iVar instanceof com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6153l) {
            com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6153l lVar = (com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6153l) iVar;
            this.f24113a.f24101s.mo27732a(lVar.mo40323a(), this.f24113a.mo41660p(), lVar.mo40324b());
        } else if (iVar instanceof com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6152k) {
            java.lang.String deviceId = ((com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6152k) iVar).mo40322a().getDeviceId();
            com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34485a(this.f24113a.f24104v, false, 13);
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().mo33266i(com.misfit.frameworks.buttonservice.log.FLogger.Component.API, com.misfit.frameworks.buttonservice.log.FLogger.Session.PAIR, deviceId, com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter.f24087y.mo41670a(), "Pair Success");
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f24113a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1(this, deviceId, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo29641a(com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6149h hVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(hVar, "errorValue");
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f24113a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1(this, hVar, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }
}
