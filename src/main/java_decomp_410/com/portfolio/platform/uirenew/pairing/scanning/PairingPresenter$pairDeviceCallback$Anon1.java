package com.portfolio.platform.uirenew.pairing.scanning;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PairingPresenter$pairDeviceCallback$Anon1 implements CoroutineUseCase.e<LinkDeviceUseCase.i, LinkDeviceUseCase.h> {
    @DexIgnore
    public /* final */ /* synthetic */ PairingPresenter a;

    @DexIgnore
    public PairingPresenter$pairDeviceCallback$Anon1(PairingPresenter pairingPresenter) {
        this.a = pairingPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(LinkDeviceUseCase.i iVar) {
        kd4.b(iVar, "responseValue");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = PairingPresenter.y.a();
        local.d(a2, "pairDeviceCallback() pairDevice, isSkipOTA=" + this.a.x.T() + ", response=" + iVar.getClass().getSimpleName());
        if (iVar instanceof LinkDeviceUseCase.a) {
            this.a.s.a();
            this.a.s.a(((LinkDeviceUseCase.a) iVar).a(), this.a.p());
        } else if (iVar instanceof LinkDeviceUseCase.e) {
            this.a.s.a();
            this.a.s.b(((LinkDeviceUseCase.e) iVar).a(), this.a.p());
        } else if (iVar instanceof LinkDeviceUseCase.l) {
            LinkDeviceUseCase.l lVar = (LinkDeviceUseCase.l) iVar;
            this.a.s.a(lVar.a(), this.a.p(), lVar.b());
        } else if (iVar instanceof LinkDeviceUseCase.k) {
            String deviceId = ((LinkDeviceUseCase.k) iVar).a().getDeviceId();
            PortfolioApp.W.c().a(this.a.v, false, 13);
            FLogger.INSTANCE.getRemote().i(FLogger.Component.API, FLogger.Session.PAIR, deviceId, PairingPresenter.y.a(), "Pair Success");
            fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new PairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1(this, deviceId, (yb4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void a(LinkDeviceUseCase.h hVar) {
        kd4.b(hVar, "errorValue");
        fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1(this, hVar, (yb4) null), 3, (Object) null);
    }
}
