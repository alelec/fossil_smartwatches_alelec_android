package com.portfolio.platform.uirenew.pairing.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase$start$1", mo27670f = "GetDianaDeviceSettingUseCase.kt", mo27671l = {62, 63, 64, 65, 66, 67, 68, 69, 76, 92, 102}, mo27672m = "invokeSuspend")
public final class GetDianaDeviceSettingUseCase$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24135p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetDianaDeviceSettingUseCase$start$1(com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase getDianaDeviceSettingUseCase, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = getDianaDeviceSettingUseCase;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase$start$1 getDianaDeviceSettingUseCase$start$1 = new com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase$start$1(this.this$0, yb4);
        getDianaDeviceSettingUseCase$start$1.f24135p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return getDianaDeviceSettingUseCase$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x031a, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x031b, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x031e, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x031f, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0322, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0323, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0326, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0327, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x032a, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x032b, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x032e, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00ae, code lost:
        r15 = r14.this$0.f24127h;
        r14.L$0 = r1;
        r14.label = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00bd, code lost:
        if (r15.downloadCategories(r14) != r0) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00bf, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00c0, code lost:
        r15 = r14.this$0.f24125f;
        r5 = r14.this$0.f24123d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00cc, code lost:
        if (r5 == null) goto L_0x032b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00ce, code lost:
        r14.L$0 = r1;
        r14.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00d7, code lost:
        if (r15.downloadAllComplication(r5, r14) != r0) goto L_0x00da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00d9, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00da, code lost:
        r15 = r14.this$0.f24124e;
        r5 = r14.this$0.f24123d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00e6, code lost:
        if (r5 == null) goto L_0x0327;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00e8, code lost:
        r14.L$0 = r1;
        r14.label = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00f1, code lost:
        if (r15.downloadWatchApp(r5, r14) != r0) goto L_0x00f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00f3, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00f4, code lost:
        r15 = r14.this$0.f24126g;
        r5 = r14.this$0.f24123d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0100, code lost:
        if (r5 == null) goto L_0x0323;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0102, code lost:
        r14.L$0 = r1;
        r14.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x010b, code lost:
        if (r15.downloadPresetList(r5, r14) != r0) goto L_0x010e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x010d, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x010e, code lost:
        r15 = r14.this$0.f24126g;
        r5 = r14.this$0.f24123d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x011a, code lost:
        if (r5 == null) goto L_0x031f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x011c, code lost:
        r14.L$0 = r1;
        r14.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0125, code lost:
        if (r15.downloadRecommendPresetList(r5, r14) != r0) goto L_0x0128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0127, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0128, code lost:
        r15 = r14.this$0.f24128i;
        r5 = r14.this$0.f24123d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0134, code lost:
        if (r5 == null) goto L_0x031b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0136, code lost:
        r14.L$0 = r1;
        r14.label = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x013f, code lost:
        if (r15.getWatchFacesFromServer(r5, r14) != r0) goto L_0x0142;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0141, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0142, code lost:
        r15 = r14.this$0.f24134o;
        r14.L$0 = r1;
        r14.label = 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0152, code lost:
        if (r15.downloadAlarms(r14) != r0) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0154, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0155, code lost:
        r15 = r14.this$0.f24126g;
        r1 = r14.this$0.f24123d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0161, code lost:
        if (r1 == null) goto L_0x0317;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0163, code lost:
        r1 = r15.getPresetList(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x016b, code lost:
        if (r1.isEmpty() == false) goto L_0x01b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x016d, code lost:
        r15 = r14.this$0.f24126g;
        r6 = r14.this$0.f24123d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0179, code lost:
        if (r6 == null) goto L_0x01b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x017b, code lost:
        r15 = r15.getRecommendPresetList(r6);
        r6 = r15.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0187, code lost:
        if (r6.hasNext() == false) goto L_0x0199;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0189, code lost:
        r1.add(com.portfolio.platform.data.model.diana.preset.DianaPreset.Companion.cloneFromDefaultPreset((com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset) r6.next()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0199, code lost:
        r6 = r14.this$0.f24126g;
        r14.L$0 = r5;
        r14.L$1 = r1;
        r14.L$2 = r15;
        r14.label = 9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01ad, code lost:
        if (r6.upsertPresetList(r1, r14) != r0) goto L_0x01b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01af, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x01b0, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x01b3, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x01b4, code lost:
        r15 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01bc, code lost:
        if (r15.hasNext() == false) goto L_0x01d4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01be, code lost:
        r6 = r15.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01d1, code lost:
        if (com.fossil.blesdk.obfuscated.dc4.m20839a(((com.portfolio.platform.data.model.diana.preset.DianaPreset) r6).isActive()).booleanValue() == false) goto L_0x01b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01d4, code lost:
        r6 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01d5, code lost:
        r6 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x01d7, code lost:
        if (r6 != null) goto L_0x01ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01dd, code lost:
        if (r1.isEmpty() == false) goto L_0x01ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01df, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.f24122p, "activePreset is null, preset list is empty?????");
        r14.this$0.mo34434a(new com.fossil.blesdk.obfuscated.nn3(600, ""));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01fe, code lost:
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01ff, code lost:
        if (r6 != null) goto L_0x0240;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0201, code lost:
        r15 = r1.get(0);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.f24122p, "Active preset is null ,pick " + r15);
        r15.setActive(true);
        r2 = r14.this$0.f24126g;
        r14.L$0 = r5;
        r14.L$1 = r1;
        r14.L$2 = r15;
        r14.label = 10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x023c, code lost:
        if (r2.upsertPreset(r15, r14) != r0) goto L_0x023f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x023e, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x023f, code lost:
        r6 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0240, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.f24122p, "activePreset=" + r6);
        r15 = com.fossil.blesdk.obfuscated.sj2.m27752a(r6.getComplications(), new com.google.gson.Gson());
        r2 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
        r3 = r14.this$0.f24123d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0277, code lost:
        if (r3 == null) goto L_0x0313;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0279, code lost:
        r2.mo34493a(r15, r3);
        r2 = com.fossil.blesdk.obfuscated.sj2.m27762b(r6.getWatchapps(), new com.google.gson.Gson());
        r3 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
        r7 = r14.this$0.f24123d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0295, code lost:
        if (r7 == null) goto L_0x030f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0297, code lost:
        r3.mo34495a(r2, r7);
        r8 = com.portfolio.platform.util.NotificationAppHelper.f24430b;
        r9 = r14.this$0.f24129j;
        r10 = r14.this$0.f24130k;
        r11 = r14.this$0.f24131l;
        r12 = r14.this$0.f24132m;
        r14.L$0 = r5;
        r14.L$1 = r1;
        r14.L$2 = r6;
        r14.L$3 = r15;
        r14.L$4 = r2;
        r14.label = 11;
        r15 = r8.mo41918a(r9, r10, r11, r12, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x02c7, code lost:
        if (r15 != r0) goto L_0x02ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x02c9, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x02ca, code lost:
        r0 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings((java.util.List) r15, java.lang.System.currentTimeMillis());
        r15 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
        r1 = r14.this$0.f24123d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0059, code lost:
        r5 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x02e1, code lost:
        if (r1 == null) goto L_0x030b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x02e3, code lost:
        r15.mo34494a(r0, r1);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.f24122p, "start set localization");
        com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34468O();
        r14.this$0.mo34436a(new com.fossil.blesdk.obfuscated.pn3());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x030a, code lost:
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x030b, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x030e, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x030f, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0312, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0313, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0316, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0317, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.DianaPreset> arrayList;
        com.fossil.blesdk.obfuscated.zg4 zg42;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        switch (this.label) {
            case 0:
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                zg42 = this.f24135p$;
                com.portfolio.platform.data.source.WatchLocalizationRepository l = this.this$0.f24133n;
                this.L$0 = zg42;
                this.label = 1;
                if (l.getWatchLocalizationFromServer(false, this) == a) {
                    return a;
                }
                break;
            case 1:
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 2:
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 3:
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 4:
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 5:
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 6:
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 7:
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 8:
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 9:
                java.util.List list = (java.util.List) this.L$2;
                arrayList = (java.util.ArrayList) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 10:
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset = (com.portfolio.platform.data.model.diana.preset.DianaPreset) this.L$2;
                arrayList = (java.util.ArrayList) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                break;
            case 11:
                com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings watchAppMappingSettings = (com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings) this.L$4;
                com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings complicationAppMappingSettings = (com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings) this.L$3;
                com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset2 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) this.L$2;
                java.util.ArrayList arrayList2 = (java.util.ArrayList) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            default:
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
