package com.portfolio.platform.uirenew.pairing.usecase;

import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.as3;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.nn3;
import com.fossil.blesdk.obfuscated.on3;
import com.fossil.blesdk.obfuscated.pn3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetHybridDeviceSettingUseCase extends CoroutineUseCase<on3, pn3, nn3> {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ HybridPresetRepository e;
    @DexIgnore
    public /* final */ MicroAppRepository f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ NotificationsRepository h;
    @DexIgnore
    public /* final */ CategoryRepository i;
    @DexIgnore
    public /* final */ AlarmsRepository j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = GetHybridDeviceSettingUseCase.class.getSimpleName();
        kd4.a((Object) simpleName, "GetHybridDeviceSettingUs\u2026se::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public GetHybridDeviceSettingUseCase(HybridPresetRepository hybridPresetRepository, MicroAppRepository microAppRepository, DeviceRepository deviceRepository, NotificationsRepository notificationsRepository, CategoryRepository categoryRepository, AlarmsRepository alarmsRepository) {
        kd4.b(hybridPresetRepository, "mPresetRepository");
        kd4.b(microAppRepository, "mMicroAppRepository");
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(notificationsRepository, "mNotificationsRepository");
        kd4.b(categoryRepository, "mCategoryRepository");
        kd4.b(alarmsRepository, "mAlarmsRepository");
        this.e = hybridPresetRepository;
        this.f = microAppRepository;
        this.g = deviceRepository;
        this.h = notificationsRepository;
        this.i = categoryRepository;
        this.j = alarmsRepository;
    }

    @DexIgnore
    public String c() {
        return k;
    }

    @DexIgnore
    public final fi4 d() {
        return ag4.b(b(), (CoroutineContext) null, (CoroutineStart) null, new GetHybridDeviceSettingUseCase$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public Object a(on3 on3, yb4<? super qa4> yb4) {
        if (on3 == null) {
            a(new nn3(600, ""));
            return qa4.a;
        }
        this.d = on3.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        StringBuilder sb = new StringBuilder();
        sb.append("Inside .GetHybridDeviceSettingUseCase executing!!! with serial=");
        String str2 = this.d;
        if (str2 != null) {
            sb.append(str2);
            local.d(str, sb.toString());
            if (!as3.b(PortfolioApp.W.c())) {
                a(new nn3(601, ""));
                return qa4.a;
            }
            if (!TextUtils.isEmpty(this.d)) {
                DeviceHelper.a aVar = DeviceHelper.o;
                String str3 = this.d;
                if (str3 == null) {
                    kd4.a();
                    throw null;
                } else if (aVar.e(str3)) {
                    d();
                    return qa4.a;
                }
            }
            a(new nn3(600, ""));
            return qa4.a;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final fi4 a(SparseArray<List<BaseFeatureModel>> sparseArray, String str, boolean z) {
        return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$Anon1(sparseArray, z, str, (yb4) null), 3, (Object) null);
    }
}
