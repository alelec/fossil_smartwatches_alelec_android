package com.portfolio.platform.uirenew.splash;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$Anon1", f = "SplashPresenter.kt", l = {63}, m = "invokeSuspend")
public final class SplashPresenter$checkToGoToNextStep$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SplashPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SplashPresenter$checkToGoToNextStep$Anon1(SplashPresenter splashPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = splashPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SplashPresenter$checkToGoToNextStep$Anon1 splashPresenter$checkToGoToNextStep$Anon1 = new SplashPresenter$checkToGoToNextStep$Anon1(this.this$Anon0, yb4);
        splashPresenter$checkToGoToNextStep$Anon1.p$ = (zg4) obj;
        return splashPresenter$checkToGoToNextStep$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SplashPresenter$checkToGoToNextStep$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            String h = PortfolioApp.W.c().h();
            boolean a2 = this.this$Anon0.j.a(h);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = SplashPresenter.l.a();
            local.d(a3, "checkToGoToNextStep isMigrationComplete " + a2);
            if (!a2) {
                this.this$Anon0.f.postDelayed(this.this$Anon0.g, 500);
                return qa4.a;
            }
            ug4 b = this.this$Anon0.b();
            SplashPresenter$checkToGoToNextStep$Anon1$mCurrentUser$Anon1 splashPresenter$checkToGoToNextStep$Anon1$mCurrentUser$Anon1 = new SplashPresenter$checkToGoToNextStep$Anon1$mCurrentUser$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = h;
            this.Z$Anon0 = a2;
            this.label = 1;
            obj = yf4.a(b, splashPresenter$checkToGoToNextStep$Anon1$mCurrentUser$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            String str = (String) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser = (MFUser) obj;
        if (mFUser == null) {
            this.this$Anon0.h.p0();
        } else {
            String email = mFUser.getEmail();
            boolean z = false;
            if (!(email == null || qf4.a(email))) {
                String birthday = mFUser.getBirthday();
                if (birthday == null || qf4.a(birthday)) {
                    z = true;
                }
                if (!z) {
                    String firstName = mFUser.getFirstName();
                    kd4.a((Object) firstName, "mCurrentUser.firstName");
                    if (!qf4.a(firstName)) {
                        String lastName = mFUser.getLastName();
                        kd4.a((Object) lastName, "mCurrentUser.lastName");
                        if (!qf4.a(lastName)) {
                            this.this$Anon0.h.h();
                        }
                    }
                }
            }
            this.this$Anon0.h.d0();
        }
        return qa4.a;
    }
}
