package com.portfolio.platform.uirenew.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RecyclerViewEmptySupport extends RecyclerView {
    @DexIgnore
    public View N0;
    @DexIgnore
    public /* final */ a O0; // = new a(this);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.AdapterDataObserver {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewEmptySupport a;

        @DexIgnore
        public a(RecyclerViewEmptySupport recyclerViewEmptySupport) {
            this.a = recyclerViewEmptySupport;
        }

        @DexIgnore
        public void a() {
            if (this.a.getAdapter() != null && this.a.getEmptyView$app_fossilRelease() != null) {
                RecyclerView.g adapter = this.a.getAdapter();
                if (adapter != null) {
                    kd4.a((Object) adapter, "adapter!!");
                    if (adapter.getItemCount() == 0) {
                        View emptyView$app_fossilRelease = this.a.getEmptyView$app_fossilRelease();
                        if (emptyView$app_fossilRelease != null) {
                            emptyView$app_fossilRelease.setVisibility(0);
                            this.a.setVisibility(8);
                            return;
                        }
                        kd4.a();
                        throw null;
                    }
                    View emptyView$app_fossilRelease2 = this.a.getEmptyView$app_fossilRelease();
                    if (emptyView$app_fossilRelease2 != null) {
                        emptyView$app_fossilRelease2.setVisibility(8);
                        this.a.setVisibility(0);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewEmptySupport(Context context) {
        super(context);
        kd4.b(context, "context");
    }

    @DexIgnore
    public final View getEmptyView$app_fossilRelease() {
        return this.N0;
    }

    @DexIgnore
    public void setAdapter(RecyclerView.g<?> gVar) {
        super.setAdapter(gVar);
        if (gVar != null) {
            gVar.registerAdapterDataObserver(this.O0);
        }
        this.O0.a();
    }

    @DexIgnore
    public final void setEmptyView(View view) {
        kd4.b(view, "emptyView");
        this.N0 = view;
    }

    @DexIgnore
    public final void setEmptyView$app_fossilRelease(View view) {
        this.N0 = view;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewEmptySupport(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        kd4.b(context, "context");
        kd4.b(attributeSet, "attrs");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewEmptySupport(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        kd4.b(context, "context");
        kd4.b(attributeSet, "attrs");
    }
}
