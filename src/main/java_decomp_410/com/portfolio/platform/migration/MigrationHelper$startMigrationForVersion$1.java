package com.portfolio.platform.migration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.migration.MigrationHelper$startMigrationForVersion$1", mo27670f = "MigrationHelper.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class MigrationHelper$startMigrationForVersion$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $version;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21300p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.migration.MigrationHelper this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MigrationHelper$startMigrationForVersion$1(com.portfolio.platform.migration.MigrationHelper migrationHelper, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = migrationHelper;
        this.$version = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.migration.MigrationHelper$startMigrationForVersion$1 migrationHelper$startMigrationForVersion$1 = new com.portfolio.platform.migration.MigrationHelper$startMigrationForVersion$1(this.this$0, this.$version, yb4);
        migrationHelper$startMigrationForVersion$1.f21300p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return migrationHelper$startMigrationForVersion$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.migration.MigrationHelper$startMigrationForVersion$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.lang.String m = this.this$0.f21298a.mo27057m();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("MigrationHelper", "start migration for " + this.$version + " lastVersion " + m);
            this.this$0.f21299b.mo34451d();
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
