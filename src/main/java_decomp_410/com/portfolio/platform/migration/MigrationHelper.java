package com.portfolio.platform.migration;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.b62;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.MigrationManager;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MigrationHelper {
    @DexIgnore
    public /* final */ en2 a;
    @DexIgnore
    public /* final */ MigrationManager b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public MigrationHelper(en2 en2, MigrationManager migrationManager, b62 b62) {
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(migrationManager, "mMigrationManager");
        kd4.b(b62, "mLegacyMigrationManager");
        this.a = en2;
        this.b = migrationManager;
    }

    @DexIgnore
    public final boolean a(String str) {
        kd4.b(str, "version");
        return this.a.m(str);
    }

    @DexIgnore
    public final fi4 b(String str) {
        throw null;
        // kd4.b(str, "version");
        // return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new MigrationHelper$startMigrationForVersion$Anon1(this, str, (yb4) null), 3, (Object) null);
    }
}
