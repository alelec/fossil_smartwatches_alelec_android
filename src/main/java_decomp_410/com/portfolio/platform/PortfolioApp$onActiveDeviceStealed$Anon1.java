package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.helper.DeviceHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.PortfolioApp$onActiveDeviceStealed$Anon1", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
public final class PortfolioApp$onActiveDeviceStealed$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PortfolioApp this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PortfolioApp$onActiveDeviceStealed$Anon1(PortfolioApp portfolioApp, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = portfolioApp;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        PortfolioApp$onActiveDeviceStealed$Anon1 portfolioApp$onActiveDeviceStealed$Anon1 = new PortfolioApp$onActiveDeviceStealed$Anon1(this.this$Anon0, yb4);
        portfolioApp$onActiveDeviceStealed$Anon1.p$ = (zg4) obj;
        return portfolioApp$onActiveDeviceStealed$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PortfolioApp$onActiveDeviceStealed$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d = PortfolioApp.W.d();
            local.d(d, "activeDevice=" + this.this$Anon0.e() + ", was stealed remove it!!!");
            if (DeviceHelper.o.f(this.this$Anon0.e())) {
                this.this$Anon0.v().deleteWatchFacesWithSerial(this.this$Anon0.e());
            }
            PortfolioApp portfolioApp = this.this$Anon0;
            portfolioApp.r(portfolioApp.e());
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
