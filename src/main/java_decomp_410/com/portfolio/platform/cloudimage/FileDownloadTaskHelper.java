package com.portfolio.platform.cloudimage;

import android.os.AsyncTask;
import android.text.TextUtils;
import com.facebook.internal.NativeProtocol;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FileDownloadTaskHelper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = (Constants.MAIN_TAG + FileDownloadTaskHelper.class.getSimpleName());
    @DexIgnore
    public String destinationUnzipPath;
    @DexIgnore
    public OnDownloadFinishListener listener;
    @DexIgnore
    public AssetsDeviceResponse response;
    @DexIgnore
    public String zipFilePath;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class FileDownloadTask extends AsyncTask<Void, Void, Boolean> {
            @DexIgnore
            public /* final */ String destinationUnzipPath;
            @DexIgnore
            public OnDownloadFinishListener listener;
            @DexIgnore
            public /* final */ AssetsDeviceResponse objectResponse;
            @DexIgnore
            public /* final */ String zipFilePath;

            @DexIgnore
            public FileDownloadTask(String str, String str2, AssetsDeviceResponse assetsDeviceResponse, OnDownloadFinishListener onDownloadFinishListener) {
                kd4.b(str, "zipFilePath");
                kd4.b(str2, "destinationUnzipPath");
                kd4.b(assetsDeviceResponse, "objectResponse");
                this.zipFilePath = str;
                this.destinationUnzipPath = str2;
                this.objectResponse = assetsDeviceResponse;
                this.listener = onDownloadFinishListener;
            }

            @DexIgnore
            /* JADX INFO: this call moved to the top of the method (can break code semantics) */
            public /* synthetic */ FileDownloadTask(String str, String str2, AssetsDeviceResponse assetsDeviceResponse, OnDownloadFinishListener onDownloadFinishListener, int i, fd4 fd4) {
                this(str, str2, assetsDeviceResponse, (i & 8) != 0 ? null : onDownloadFinishListener);
            }

            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: java.lang.String} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: java.lang.String} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: java.lang.String} */
            /* JADX WARNING: type inference failed for: r0v1 */
            /* JADX WARNING: type inference failed for: r0v6, types: [java.io.BufferedInputStream] */
            /* JADX WARNING: type inference failed for: r0v15 */
            /* JADX WARNING: type inference failed for: r0v16 */
            /* JADX WARNING: type inference failed for: r0v17 */
            /* JADX WARNING: type inference failed for: r0v18 */
            /* JADX WARNING: Code restructure failed: missing block: B:35:0x007e, code lost:
                if (r4 != null) goto L_0x006a;
             */
            @DexIgnore
            /* JADX WARNING: Multi-variable type inference failed */
            /* JADX WARNING: Removed duplicated region for block: B:62:0x0111  */
            public Boolean doInBackground(Void... voidArr) {
                BufferedInputStream bufferedInputStream;
                kd4.b(voidArr, NativeProtocol.WEB_DIALOG_PARAMS);
                Data data = this.objectResponse.getData();
                Object r0 = 0;
                boolean z = false;
                if (!TextUtils.isEmpty(data != null ? data.getUrl() : null)) {
                    FileOutputStream fileOutputStream = new FileOutputStream(this.zipFilePath);
                    try {
                        Data data2 = this.objectResponse.getData();
                        URLConnection openConnection = new URL(data2 != null ? data2.getUrl() : null).openConnection();
                        openConnection.connect();
                        bufferedInputStream = new BufferedInputStream(openConnection.getInputStream());
                        try {
                            byte[] bArr = new byte[1024];
                            while (true) {
                                int read = bufferedInputStream.read(bArr);
                                if (!(read != -1)) {
                                    z = true;
                                    break;
                                } else if (isCancelled()) {
                                    break;
                                } else {
                                    fileOutputStream.write(bArr, 0, read);
                                }
                            }
                            fileOutputStream.flush();
                            fileOutputStream.close();
                        } catch (Exception e) {
                            e = e;
                            try {
                                e.printStackTrace();
                                fileOutputStream.flush();
                                fileOutputStream.close();
                            } catch (Throwable th) {
                                th = th;
                                r0 = bufferedInputStream;
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                if (r0 != 0) {
                                    r0.close();
                                }
                                throw th;
                            }
                        }
                    } catch (Exception e2) {
                        e = e2;
                        bufferedInputStream = null;
                        e.printStackTrace();
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    } catch (Throwable th2) {
                        th = th2;
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        if (r0 != 0) {
                        }
                        throw th;
                    }
                    bufferedInputStream.close();
                    Metadata metadata = this.objectResponse.getMetadata();
                    if (!TextUtils.isEmpty(metadata != null ? metadata.getChecksum() : null)) {
                        ChecksumUtil checksumUtil = ChecksumUtil.INSTANCE;
                        String str = this.zipFilePath;
                        Metadata metadata2 = this.objectResponse.getMetadata();
                        if (metadata2 != null) {
                            r0 = metadata2.getChecksum();
                        }
                        if (!checksumUtil.verifyDownloadFile(str, r0)) {
                            FLogger.INSTANCE.getLocal().e(FileDownloadTaskHelper.Companion.getTAG$app_fossilRelease(), "Inconsistent checksum, retry download?");
                        }
                    } else {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String tAG$app_fossilRelease = FileDownloadTaskHelper.Companion.getTAG$app_fossilRelease();
                        StringBuilder sb = new StringBuilder();
                        sb.append("Download assets completed for serialNumber = [");
                        Metadata metadata3 = this.objectResponse.getMetadata();
                        sb.append(metadata3 != null ? metadata3.getSerialNumber() : null);
                        sb.append("], feature = [");
                        Metadata metadata4 = this.objectResponse.getMetadata();
                        if (metadata4 != null) {
                            r0 = metadata4.getFeature();
                        }
                        sb.append(r0);
                        sb.append("] with risk cause by empty checksum.");
                        local.e(tAG$app_fossilRelease, sb.toString());
                    }
                    return Boolean.valueOf(z);
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease2 = FileDownloadTaskHelper.Companion.getTAG$app_fossilRelease();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Download file failed for serialNumber = [");
                Metadata metadata5 = this.objectResponse.getMetadata();
                sb2.append(metadata5 != null ? metadata5.getSerialNumber() : null);
                sb2.append("], feature = [");
                Metadata metadata6 = this.objectResponse.getMetadata();
                sb2.append(metadata6 != null ? metadata6.getFeature() : null);
                sb2.append("], downloadUrl = [");
                Data data3 = this.objectResponse.getData();
                if (data3 != null) {
                    r0 = data3.getUrl();
                }
                sb2.append(r0);
                sb2.append("]");
                local2.e(tAG$app_fossilRelease2, sb2.toString());
                return false;
            }

            @DexIgnore
            public void onPostExecute(Boolean bool) {
                super.onPostExecute(bool);
                if (bool == null) {
                    kd4.a();
                    throw null;
                } else if (bool.booleanValue()) {
                    OnDownloadFinishListener onDownloadFinishListener = this.listener;
                    if (onDownloadFinishListener != null) {
                        onDownloadFinishListener.onDownloadSuccess(this.zipFilePath, this.destinationUnzipPath);
                    }
                } else {
                    OnDownloadFinishListener onDownloadFinishListener2 = this.listener;
                    if (onDownloadFinishListener2 != null) {
                        onDownloadFinishListener2.onDownloadFail(this.zipFilePath, this.destinationUnzipPath, this.objectResponse);
                    }
                }
            }
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return FileDownloadTaskHelper.TAG;
        }

        @DexIgnore
        public final FileDownloadTaskHelper newInstance() {
            return new FileDownloadTaskHelper();
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface OnDownloadFinishListener {
        @DexIgnore
        void onDownloadFail(String str, String str2, AssetsDeviceResponse assetsDeviceResponse);

        @DexIgnore
        void onDownloadSuccess(String str, String str2);
    }

    @DexIgnore
    public static final FileDownloadTaskHelper newInstance() {
        return Companion.newInstance();
    }

    @DexIgnore
    public final void execute() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("execute() called with serialNumber = [");
        AssetsDeviceResponse assetsDeviceResponse = this.response;
        if (assetsDeviceResponse != null) {
            Metadata metadata = assetsDeviceResponse.getMetadata();
            sb.append(metadata != null ? metadata.getSerialNumber() : null);
            sb.append("], feature = [");
            AssetsDeviceResponse assetsDeviceResponse2 = this.response;
            if (assetsDeviceResponse2 != null) {
                Metadata metadata2 = assetsDeviceResponse2.getMetadata();
                sb.append(metadata2 != null ? metadata2.getFeature() : null);
                sb.append("]");
                local.d(str, sb.toString());
                String str2 = this.zipFilePath;
                if (str2 != null) {
                    String str3 = this.destinationUnzipPath;
                    if (str3 != null) {
                        AssetsDeviceResponse assetsDeviceResponse3 = this.response;
                        if (assetsDeviceResponse3 != null) {
                            new Companion.FileDownloadTask(str2, str3, assetsDeviceResponse3, this.listener).execute(new Void[0]);
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void init(String str, String str2, AssetsDeviceResponse assetsDeviceResponse) {
        kd4.b(str, "zipFilePath");
        kd4.b(str2, "destinationUnzipPath");
        kd4.b(assetsDeviceResponse, "response");
        this.zipFilePath = str;
        this.destinationUnzipPath = str2;
        this.response = assetsDeviceResponse;
    }

    @DexIgnore
    public final void setOnDownloadFinishListener(OnDownloadFinishListener onDownloadFinishListener) {
        kd4.b(onDownloadFinishListener, "listener");
        this.listener = onDownloadFinishListener;
    }
}
