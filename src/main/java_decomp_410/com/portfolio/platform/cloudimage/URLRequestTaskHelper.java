package com.portfolio.platform.cloudimage;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yf4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class URLRequestTaskHelper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = (Constants.MAIN_TAG + URLRequestTaskHelper.class.getSimpleName());
    @DexIgnore
    public String destinationUnzipPath;
    @DexIgnore
    public String feature;
    @DexIgnore
    public OnNextTaskListener listener;
    @DexIgnore
    public ApiServiceV2 mApiService;
    @DexIgnore
    public String resolution;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public String zipFilePath;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return URLRequestTaskHelper.TAG;
        }

        @DexIgnore
        public final URLRequestTaskHelper newInstance() {
            return new URLRequestTaskHelper();
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface OnNextTaskListener {
        @DexIgnore
        void downloadFile(String str, String str2, AssetsDeviceResponse assetsDeviceResponse);

        @DexIgnore
        void onGetDeviceAssetFailed();
    }

    @DexIgnore
    public URLRequestTaskHelper() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public static final URLRequestTaskHelper newInstance() {
        return Companion.newInstance();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x014a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object execute(yb4<? super qa4> yb4) {
        URLRequestTaskHelper$execute$Anon1 uRLRequestTaskHelper$execute$Anon1;
        int i;
        URLRequestTaskHelper uRLRequestTaskHelper;
        qo2 qo2;
        if (yb4 instanceof URLRequestTaskHelper$execute$Anon1) {
            uRLRequestTaskHelper$execute$Anon1 = (URLRequestTaskHelper$execute$Anon1) yb4;
            int i2 = uRLRequestTaskHelper$execute$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                uRLRequestTaskHelper$execute$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = uRLRequestTaskHelper$execute$Anon1.result;
                Object a = cc4.a();
                i = uRLRequestTaskHelper$execute$Anon1.label;
                String str = null;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local.d(str2, "execute() called with serialNumber = [" + this.serialNumber + "], feature = [" + this.feature + ']');
                    URLRequestTaskHelper$execute$response$Anon1 uRLRequestTaskHelper$execute$response$Anon1 = new URLRequestTaskHelper$execute$response$Anon1(this, (yb4) null);
                    uRLRequestTaskHelper$execute$Anon1.L$Anon0 = this;
                    uRLRequestTaskHelper$execute$Anon1.label = 1;
                    obj = ResponseKt.a(uRLRequestTaskHelper$execute$response$Anon1, uRLRequestTaskHelper$execute$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    uRLRequestTaskHelper = this;
                } else if (i == 1) {
                    uRLRequestTaskHelper = (URLRequestTaskHelper) uRLRequestTaskHelper$execute$Anon1.L$Anon0;
                    na4.a(obj);
                } else if (i == 2) {
                    AssetsDeviceResponse assetsDeviceResponse = (AssetsDeviceResponse) uRLRequestTaskHelper$execute$Anon1.L$Anon2;
                    qo2 qo22 = (qo2) uRLRequestTaskHelper$execute$Anon1.L$Anon1;
                    URLRequestTaskHelper uRLRequestTaskHelper2 = (URLRequestTaskHelper) uRLRequestTaskHelper$execute$Anon1.L$Anon0;
                    na4.a(obj);
                    return qa4.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    Object a2 = ro2.a();
                    if (a2 == null) {
                        kd4.a();
                        throw null;
                    } else if (true ^ ((ApiResponse) a2).get_items().isEmpty()) {
                        AssetsDeviceResponse assetsDeviceResponse2 = (AssetsDeviceResponse) new Gson().a((JsonElement) ((ApiResponse) ro2.a()).get_items().get(0), AssetsDeviceResponse.class);
                        AssetUtil assetUtil = AssetUtil.INSTANCE;
                        String str3 = uRLRequestTaskHelper.zipFilePath;
                        if (str3 != null) {
                            if (assetUtil.checkFileExist(str3)) {
                                ChecksumUtil checksumUtil = ChecksumUtil.INSTANCE;
                                String str4 = uRLRequestTaskHelper.zipFilePath;
                                if (str4 != null) {
                                    Metadata metadata = assetsDeviceResponse2.getMetadata();
                                    if (checksumUtil.verifyDownloadFile(str4, metadata != null ? metadata.getChecksum() : null)) {
                                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                        String str5 = TAG;
                                        local2.d(str5, "onSuccess: The assets with serialNumber = [" + uRLRequestTaskHelper.serialNumber + "] for feature =[" + uRLRequestTaskHelper.feature + "] is the latest, no need to download!");
                                    }
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            }
                            FLogger.INSTANCE.getLocal().d(TAG, "onSuccess: need to download new asset for this");
                            pi4 c = nh4.c();
                            URLRequestTaskHelper$execute$Anon2 uRLRequestTaskHelper$execute$Anon2 = new URLRequestTaskHelper$execute$Anon2(uRLRequestTaskHelper, assetsDeviceResponse2, (yb4) null);
                            uRLRequestTaskHelper$execute$Anon1.L$Anon0 = uRLRequestTaskHelper;
                            uRLRequestTaskHelper$execute$Anon1.L$Anon1 = qo2;
                            uRLRequestTaskHelper$execute$Anon1.L$Anon2 = assetsDeviceResponse2;
                            uRLRequestTaskHelper$execute$Anon1.label = 2;
                            if (yf4.a(c, uRLRequestTaskHelper$execute$Anon2, uRLRequestTaskHelper$execute$Anon1) == a) {
                                return a;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str6 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onFail: serialNumber = [");
                    sb.append(uRLRequestTaskHelper.serialNumber);
                    sb.append("], feature = [");
                    sb.append(uRLRequestTaskHelper.feature);
                    sb.append("], error = [");
                    Throwable d = ((po2) qo2).d();
                    if (d != null) {
                        str = d.getMessage();
                    }
                    sb.append(str);
                    sb.append("]");
                    local3.e(str6, sb.toString());
                    OnNextTaskListener onNextTaskListener = uRLRequestTaskHelper.listener;
                    if (onNextTaskListener != null) {
                        onNextTaskListener.onGetDeviceAssetFailed();
                    }
                }
                return qa4.a;
            }
        }
        uRLRequestTaskHelper$execute$Anon1 = new URLRequestTaskHelper$execute$Anon1(this, yb4);
        Object obj2 = uRLRequestTaskHelper$execute$Anon1.result;
        Object a3 = cc4.a();
        i = uRLRequestTaskHelper$execute$Anon1.label;
        String str7 = null;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
        return qa4.a;
    }

    @DexIgnore
    public final String getDestinationUnzipPath$app_fossilRelease() {
        return this.destinationUnzipPath;
    }

    @DexIgnore
    public final String getFeature$app_fossilRelease() {
        return this.feature;
    }

    @DexIgnore
    public final OnNextTaskListener getListener$app_fossilRelease() {
        return this.listener;
    }

    @DexIgnore
    public final ApiServiceV2 getMApiService() {
        ApiServiceV2 apiServiceV2 = this.mApiService;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        kd4.d("mApiService");
        throw null;
    }

    @DexIgnore
    public final String getSerialNumber$app_fossilRelease() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getZipFilePath$app_fossilRelease() {
        return this.zipFilePath;
    }

    @DexIgnore
    public final void init(String str, String str2, String str3, String str4, String str5) {
        kd4.b(str, "zipFilePath");
        kd4.b(str2, "destinationUnzipPath");
        kd4.b(str3, "serialNumber");
        kd4.b(str4, "feature");
        kd4.b(str5, "resolution");
        this.zipFilePath = str;
        this.destinationUnzipPath = str2;
        this.serialNumber = str3;
        this.feature = str4;
        this.resolution = str5;
    }

    @DexIgnore
    public final void setDestinationUnzipPath$app_fossilRelease(String str) {
        this.destinationUnzipPath = str;
    }

    @DexIgnore
    public final void setFeature$app_fossilRelease(String str) {
        this.feature = str;
    }

    @DexIgnore
    public final void setListener$app_fossilRelease(OnNextTaskListener onNextTaskListener) {
        this.listener = onNextTaskListener;
    }

    @DexIgnore
    public final void setMApiService(ApiServiceV2 apiServiceV2) {
        kd4.b(apiServiceV2, "<set-?>");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public final void setOnNextTaskListener(OnNextTaskListener onNextTaskListener) {
        kd4.b(onNextTaskListener, "listener");
        this.listener = onNextTaskListener;
    }

    @DexIgnore
    public final void setSerialNumber$app_fossilRelease(String str) {
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setZipFilePath$app_fossilRelease(String str) {
        this.zipFilePath = str;
    }
}
