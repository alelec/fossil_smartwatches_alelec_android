package com.portfolio.platform.cloudimage;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1", mo27670f = "CloudImageHelper.kt", mo27671l = {112, 116}, mo27672m = "invokeSuspend")
public final class CloudImageHelper$ItemImage$downloadForCalibration$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20996p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1$1", mo27670f = "CloudImageHelper.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1$1 */
    public static final class C56151 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f20997p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C56151(com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1 cloudImageHelper$ItemImage$downloadForCalibration$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = cloudImageHelper$ItemImage$downloadForCalibration$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1.C56151 r0 = new com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1.C56151(this.this$0, yb4);
            r0.f20997p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1.C56151) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                if (this.this$0.this$0.mWeakReferenceImageView != null) {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String tag = com.portfolio.platform.cloudimage.CloudImageHelper.Companion.getTAG();
                    local.mo33255d(tag, "download setDefaultImage first, resourceId=" + this.this$0.this$0.mResourceId);
                    java.lang.ref.WeakReference access$getMWeakReferenceImageView$p = this.this$0.this$0.mWeakReferenceImageView;
                    if (access$getMWeakReferenceImageView$p != null) {
                        android.widget.ImageView imageView = (android.widget.ImageView) access$getMWeakReferenceImageView$p.get();
                        if (imageView != null) {
                            java.lang.Integer access$getMResourceId$p = this.this$0.this$0.mResourceId;
                            if (access$getMResourceId$p != null) {
                                imageView.setImageResource(access$getMResourceId$p.intValue());
                            } else {
                                com.fossil.blesdk.obfuscated.kd4.m24405a();
                                throw null;
                            }
                        }
                    } else {
                        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.ref.WeakReference<android.widget.ImageView>");
                    }
                }
                java.io.File access$getMFile$p = this.this$0.this$0.mFile;
                if (access$getMFile$p != null) {
                    java.lang.String access$getMSerialNumber$p = this.this$0.this$0.mSerialNumber;
                    if (access$getMSerialNumber$p != null) {
                        java.lang.String access$getMSerialPrefix$p = this.this$0.this$0.mSerialPrefix;
                        if (access$getMSerialPrefix$p != null) {
                            com.portfolio.platform.cloudimage.CloudImageRunnable cloudImageRunnable = new com.portfolio.platform.cloudimage.CloudImageRunnable(access$getMFile$p, access$getMSerialNumber$p, access$getMSerialPrefix$p, com.portfolio.platform.cloudimage.ResolutionHelper.INSTANCE.getResolutionFromDevice().getResolution(), com.portfolio.platform.cloudimage.Constants.DownloadAssetType.CALIBRATION, this.this$0.this$0.mCalibrationType.getType(), this.this$0.this$0.mListener);
                            com.portfolio.platform.cloudimage.CloudImageHelper.this.getMAppExecutors().mo27947b().execute(cloudImageRunnable);
                            return com.fossil.blesdk.obfuscated.qa4.f17909a;
                        }
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageHelper$ItemImage$downloadForCalibration$1(com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage itemImage, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = itemImage;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1 cloudImageHelper$ItemImage$downloadForCalibration$1 = new com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1(this.this$0, yb4);
        cloudImageHelper$ItemImage$downloadForCalibration$1.f20996p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return cloudImageHelper$ItemImage$downloadForCalibration$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object obj2;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f20996p$;
            if (this.this$0.mFile == null) {
                com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage itemImage = this.this$0;
                itemImage.mFile = com.portfolio.platform.cloudimage.CloudImageHelper.this.getMApp().getFilesDir();
            }
            com.portfolio.platform.cloudimage.AssetUtil assetUtil = com.portfolio.platform.cloudimage.AssetUtil.INSTANCE;
            java.io.File access$getMFile$p = this.this$0.mFile;
            if (access$getMFile$p != null) {
                java.lang.String access$getMSerialNumber$p = this.this$0.mSerialNumber;
                if (access$getMSerialNumber$p != null) {
                    java.lang.String access$getMSerialPrefix$p = this.this$0.mSerialPrefix;
                    if (access$getMSerialPrefix$p != null) {
                        java.lang.String resolution = com.portfolio.platform.cloudimage.ResolutionHelper.INSTANCE.getResolutionFromDevice().getResolution();
                        java.lang.String feature = com.portfolio.platform.cloudimage.Constants.Feature.CALIBRATION.getFeature();
                        java.lang.String type = this.this$0.mCalibrationType.getType();
                        com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$0.mListener;
                        this.L$0 = zg4;
                        this.label = 1;
                        obj2 = assetUtil.checkAssetExist(access$getMFile$p, access$getMSerialNumber$p, access$getMSerialPrefix$p, resolution, feature, type, access$getMListener$p, this);
                        if (obj2 == a) {
                            return a;
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            obj2 = obj;
        } else if (i == 2) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (((java.lang.Boolean) obj2).booleanValue()) {
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
        com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1.C56151 r1 = new com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1.C56151(this, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.label = 2;
        if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r1, this) == a) {
            return a;
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
