package com.portfolio.platform.cloudimage;

import com.fossil.blesdk.obfuscated.wc4;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CloudImageHelper$Companion$instance$Anon2 extends Lambda implements wc4<CloudImageHelper> {
    @DexIgnore
    public static /* final */ CloudImageHelper$Companion$instance$Anon2 INSTANCE; // = new CloudImageHelper$Companion$instance$Anon2();

    @DexIgnore
    public CloudImageHelper$Companion$instance$Anon2() {
        super(0);
    }

    @DexIgnore
    public final CloudImageHelper invoke() {
        return CloudImageHelper.Holder.INSTANCE.getINSTANCE();
    }
}
