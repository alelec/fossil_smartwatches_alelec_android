package com.portfolio.platform.cloudimage;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$isFilePath2Exist$Anon1", f = "AssetUtil.kt", l = {}, m = "invokeSuspend")
public final class AssetUtil$checkAssetExist$isFilePath2Exist$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Boolean>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $filePath2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AssetUtil$checkAssetExist$isFilePath2Exist$Anon1(String str, yb4 yb4) {
        super(2, yb4);
        this.$filePath2 = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        AssetUtil$checkAssetExist$isFilePath2Exist$Anon1 assetUtil$checkAssetExist$isFilePath2Exist$Anon1 = new AssetUtil$checkAssetExist$isFilePath2Exist$Anon1(this.$filePath2, yb4);
        assetUtil$checkAssetExist$isFilePath2Exist$Anon1.p$ = (zg4) obj;
        return assetUtil$checkAssetExist$isFilePath2Exist$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AssetUtil$checkAssetExist$isFilePath2Exist$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return dc4.a(AssetUtil.INSTANCE.checkFileExist(this.$filePath2));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
