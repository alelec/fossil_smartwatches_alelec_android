package com.portfolio.platform.cloudimage;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yf4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AssetUtil {
    @DexIgnore
    public static /* final */ AssetUtil INSTANCE; // = new AssetUtil();
    @DexIgnore
    public static /* final */ String TAG; // = "AssetUtil";

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x01b7  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0210  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x026d  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x02cd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0036  */
    public final Object checkAssetExist(File file, String str, String str2, String str3, String str4, String str5, CloudImageHelper.OnImageCallbackListener onImageCallbackListener, yb4<? super Boolean> yb4) {
        AssetUtil$checkAssetExist$Anon1 assetUtil$checkAssetExist$Anon1;
        int i;
        String str6;
        String str7;
        String str8;
        String str9;
        String str10;
        boolean z;
        File file2;
        String str11;
        String str12;
        String str13;
        CloudImageHelper.OnImageCallbackListener onImageCallbackListener2;
        String str14;
        String str15;
        AssetUtil assetUtil;
        boolean booleanValue;
        String str16;
        String str17;
        String str18;
        CloudImageHelper.OnImageCallbackListener onImageCallbackListener3;
        String str19;
        String str20;
        AssetUtil assetUtil2;
        Object obj;
        boolean booleanValue2;
        String str21 = str2;
        String str22 = str3;
        String str23 = str4;
        String str24 = str5;
        yb4<? super Boolean> yb42 = yb4;
        if (yb42 instanceof AssetUtil$checkAssetExist$Anon1) {
            assetUtil$checkAssetExist$Anon1 = (AssetUtil$checkAssetExist$Anon1) yb42;
            int i2 = assetUtil$checkAssetExist$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                assetUtil$checkAssetExist$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj2 = assetUtil$checkAssetExist$Anon1.result;
                Object a = cc4.a();
                i = assetUtil$checkAssetExist$Anon1.label;
                if (i != 0) {
                    na4.a(obj2);
                    String str25 = file.getAbsolutePath() + '/' + str21 + '-' + str22 + '-' + str23;
                    String str26 = str25 + '/' + str24 + ".webp";
                    ug4 b = nh4.b();
                    AssetUtil$checkAssetExist$isFilePath1Exist$Anon1 assetUtil$checkAssetExist$isFilePath1Exist$Anon1 = new AssetUtil$checkAssetExist$isFilePath1Exist$Anon1(str26, (yb4) null);
                    assetUtil$checkAssetExist$Anon1.L$Anon0 = this;
                    file2 = file;
                    assetUtil$checkAssetExist$Anon1.L$Anon1 = file2;
                    String str27 = str;
                    assetUtil$checkAssetExist$Anon1.L$Anon2 = str27;
                    assetUtil$checkAssetExist$Anon1.L$Anon3 = str21;
                    assetUtil$checkAssetExist$Anon1.L$Anon4 = str22;
                    assetUtil$checkAssetExist$Anon1.L$Anon5 = str23;
                    assetUtil$checkAssetExist$Anon1.L$Anon6 = str24;
                    onImageCallbackListener3 = onImageCallbackListener;
                    assetUtil$checkAssetExist$Anon1.L$Anon7 = onImageCallbackListener3;
                    assetUtil$checkAssetExist$Anon1.L$Anon8 = str25;
                    assetUtil$checkAssetExist$Anon1.L$Anon9 = str26;
                    assetUtil$checkAssetExist$Anon1.label = 1;
                    obj = yf4.a(b, assetUtil$checkAssetExist$isFilePath1Exist$Anon1, assetUtil$checkAssetExist$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    str12 = str21;
                    str11 = str24;
                    str14 = str25;
                    assetUtil2 = this;
                    String str28 = str27;
                    str10 = str22;
                    str8 = str28;
                    String str29 = str26;
                    str19 = str23;
                    str20 = str29;
                } else if (i == 1) {
                    str19 = (String) assetUtil$checkAssetExist$Anon1.L$Anon5;
                    file2 = (File) assetUtil$checkAssetExist$Anon1.L$Anon1;
                    na4.a(obj2);
                    obj = obj2;
                    CloudImageHelper.OnImageCallbackListener onImageCallbackListener4 = (CloudImageHelper.OnImageCallbackListener) assetUtil$checkAssetExist$Anon1.L$Anon7;
                    str20 = (String) assetUtil$checkAssetExist$Anon1.L$Anon9;
                    assetUtil2 = (AssetUtil) assetUtil$checkAssetExist$Anon1.L$Anon0;
                    str10 = (String) assetUtil$checkAssetExist$Anon1.L$Anon4;
                    str12 = (String) assetUtil$checkAssetExist$Anon1.L$Anon3;
                    onImageCallbackListener3 = onImageCallbackListener4;
                    String str30 = (String) assetUtil$checkAssetExist$Anon1.L$Anon6;
                    str14 = (String) assetUtil$checkAssetExist$Anon1.L$Anon8;
                    str8 = (String) assetUtil$checkAssetExist$Anon1.L$Anon2;
                    str11 = str30;
                } else if (i == 2) {
                    boolean z2 = assetUtil$checkAssetExist$Anon1.Z$Anon0;
                    str18 = (String) assetUtil$checkAssetExist$Anon1.L$Anon9;
                    String str31 = (String) assetUtil$checkAssetExist$Anon1.L$Anon8;
                    CloudImageHelper.OnImageCallbackListener onImageCallbackListener5 = (CloudImageHelper.OnImageCallbackListener) assetUtil$checkAssetExist$Anon1.L$Anon7;
                    String str32 = (String) assetUtil$checkAssetExist$Anon1.L$Anon6;
                    String str33 = (String) assetUtil$checkAssetExist$Anon1.L$Anon5;
                    String str34 = (String) assetUtil$checkAssetExist$Anon1.L$Anon4;
                    String str35 = (String) assetUtil$checkAssetExist$Anon1.L$Anon3;
                    str8 = (String) assetUtil$checkAssetExist$Anon1.L$Anon2;
                    File file3 = (File) assetUtil$checkAssetExist$Anon1.L$Anon1;
                    AssetUtil assetUtil3 = (AssetUtil) assetUtil$checkAssetExist$Anon1.L$Anon0;
                    na4.a(obj2);
                    str16 = " serialNumber=";
                    str17 = TAG;
                    FLogger.INSTANCE.getLocal().d(str17, "filePath1=" + str18 + str16 + str8);
                    return dc4.a(true);
                } else if (i == 3) {
                    boolean z3 = assetUtil$checkAssetExist$Anon1.Z$Anon0;
                    str14 = (String) assetUtil$checkAssetExist$Anon1.L$Anon8;
                    onImageCallbackListener2 = (CloudImageHelper.OnImageCallbackListener) assetUtil$checkAssetExist$Anon1.L$Anon7;
                    str11 = (String) assetUtil$checkAssetExist$Anon1.L$Anon6;
                    str10 = (String) assetUtil$checkAssetExist$Anon1.L$Anon4;
                    str12 = (String) assetUtil$checkAssetExist$Anon1.L$Anon3;
                    String str36 = (String) assetUtil$checkAssetExist$Anon1.L$Anon10;
                    File file4 = (File) assetUtil$checkAssetExist$Anon1.L$Anon1;
                    na4.a(obj2);
                    assetUtil = (AssetUtil) assetUtil$checkAssetExist$Anon1.L$Anon0;
                    str6 = " serialNumber=";
                    str7 = TAG;
                    str9 = str36;
                    z = z3;
                    str13 = (String) assetUtil$checkAssetExist$Anon1.L$Anon9;
                    str15 = (String) assetUtil$checkAssetExist$Anon1.L$Anon5;
                    str8 = (String) assetUtil$checkAssetExist$Anon1.L$Anon2;
                    file2 = file4;
                    booleanValue = ((Boolean) obj2).booleanValue();
                    if (!booleanValue) {
                        Object obj3 = a;
                        AssetUtil$checkAssetExist$Anon3 assetUtil$checkAssetExist$Anon3 = new AssetUtil$checkAssetExist$Anon3(onImageCallbackListener2, str8, str9, (yb4) null);
                        assetUtil$checkAssetExist$Anon1.L$Anon0 = assetUtil;
                        assetUtil$checkAssetExist$Anon1.L$Anon1 = file2;
                        assetUtil$checkAssetExist$Anon1.L$Anon2 = str8;
                        assetUtil$checkAssetExist$Anon1.L$Anon3 = str12;
                        assetUtil$checkAssetExist$Anon1.L$Anon4 = str10;
                        assetUtil$checkAssetExist$Anon1.L$Anon5 = str15;
                        assetUtil$checkAssetExist$Anon1.L$Anon6 = str11;
                        assetUtil$checkAssetExist$Anon1.L$Anon7 = onImageCallbackListener2;
                        assetUtil$checkAssetExist$Anon1.L$Anon8 = str14;
                        assetUtil$checkAssetExist$Anon1.L$Anon9 = str13;
                        assetUtil$checkAssetExist$Anon1.Z$Anon0 = z;
                        assetUtil$checkAssetExist$Anon1.L$Anon10 = str9;
                        assetUtil$checkAssetExist$Anon1.Z$Anon1 = booleanValue;
                        assetUtil$checkAssetExist$Anon1.label = 4;
                        Object obj4 = obj3;
                        if (yf4.a(nh4.c(), assetUtil$checkAssetExist$Anon3, assetUtil$checkAssetExist$Anon1) == obj4) {
                            return obj4;
                        }
                        FLogger.INSTANCE.getLocal().d(str7, "filePath2=" + str9 + str6 + str8);
                        return dc4.a(true);
                    }
                    FLogger.INSTANCE.getLocal().d(str7, "file is not exist serialNumber = " + str8);
                    return dc4.a(false);
                } else if (i == 4) {
                    boolean z4 = assetUtil$checkAssetExist$Anon1.Z$Anon1;
                    str9 = (String) assetUtil$checkAssetExist$Anon1.L$Anon10;
                    boolean z5 = assetUtil$checkAssetExist$Anon1.Z$Anon0;
                    String str37 = (String) assetUtil$checkAssetExist$Anon1.L$Anon9;
                    String str38 = (String) assetUtil$checkAssetExist$Anon1.L$Anon8;
                    CloudImageHelper.OnImageCallbackListener onImageCallbackListener6 = (CloudImageHelper.OnImageCallbackListener) assetUtil$checkAssetExist$Anon1.L$Anon7;
                    String str39 = (String) assetUtil$checkAssetExist$Anon1.L$Anon6;
                    String str40 = (String) assetUtil$checkAssetExist$Anon1.L$Anon5;
                    String str41 = (String) assetUtil$checkAssetExist$Anon1.L$Anon4;
                    String str42 = (String) assetUtil$checkAssetExist$Anon1.L$Anon3;
                    str8 = (String) assetUtil$checkAssetExist$Anon1.L$Anon2;
                    File file5 = (File) assetUtil$checkAssetExist$Anon1.L$Anon1;
                    AssetUtil assetUtil4 = (AssetUtil) assetUtil$checkAssetExist$Anon1.L$Anon0;
                    na4.a(obj2);
                    str6 = " serialNumber=";
                    str7 = TAG;
                    FLogger.INSTANCE.getLocal().d(str7, "filePath2=" + str9 + str6 + str8);
                    return dc4.a(true);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                booleanValue2 = ((Boolean) obj).booleanValue();
                if (!booleanValue2) {
                    pi4 c = nh4.c();
                    str17 = TAG;
                    str16 = " serialNumber=";
                    AssetUtil$checkAssetExist$Anon2 assetUtil$checkAssetExist$Anon2 = new AssetUtil$checkAssetExist$Anon2(onImageCallbackListener3, str8, str20, (yb4) null);
                    assetUtil$checkAssetExist$Anon1.L$Anon0 = assetUtil2;
                    assetUtil$checkAssetExist$Anon1.L$Anon1 = file2;
                    assetUtil$checkAssetExist$Anon1.L$Anon2 = str8;
                    assetUtil$checkAssetExist$Anon1.L$Anon3 = str12;
                    assetUtil$checkAssetExist$Anon1.L$Anon4 = str10;
                    assetUtil$checkAssetExist$Anon1.L$Anon5 = str19;
                    assetUtil$checkAssetExist$Anon1.L$Anon6 = str11;
                    assetUtil$checkAssetExist$Anon1.L$Anon7 = onImageCallbackListener3;
                    assetUtil$checkAssetExist$Anon1.L$Anon8 = str14;
                    assetUtil$checkAssetExist$Anon1.L$Anon9 = str20;
                    assetUtil$checkAssetExist$Anon1.Z$Anon0 = booleanValue2;
                    assetUtil$checkAssetExist$Anon1.label = 2;
                    if (yf4.a(c, assetUtil$checkAssetExist$Anon2, assetUtil$checkAssetExist$Anon1) == a) {
                        return a;
                    }
                    str18 = str20;
                    FLogger.INSTANCE.getLocal().d(str17, "filePath1=" + str18 + str16 + str8);
                    return dc4.a(true);
                }
                String str43 = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append(str14);
                str7 = str43;
                sb.append('/');
                sb.append(str11);
                sb.append(".png");
                String sb2 = sb.toString();
                ug4 b2 = nh4.b();
                str6 = " serialNumber=";
                AssetUtil$checkAssetExist$isFilePath2Exist$Anon1 assetUtil$checkAssetExist$isFilePath2Exist$Anon1 = new AssetUtil$checkAssetExist$isFilePath2Exist$Anon1(sb2, (yb4) null);
                assetUtil$checkAssetExist$Anon1.L$Anon0 = assetUtil2;
                assetUtil$checkAssetExist$Anon1.L$Anon1 = file2;
                assetUtil$checkAssetExist$Anon1.L$Anon2 = str8;
                assetUtil$checkAssetExist$Anon1.L$Anon3 = str12;
                assetUtil$checkAssetExist$Anon1.L$Anon4 = str10;
                assetUtil$checkAssetExist$Anon1.L$Anon5 = str19;
                assetUtil$checkAssetExist$Anon1.L$Anon6 = str11;
                assetUtil$checkAssetExist$Anon1.L$Anon7 = onImageCallbackListener3;
                assetUtil$checkAssetExist$Anon1.L$Anon8 = str14;
                assetUtil$checkAssetExist$Anon1.L$Anon9 = str20;
                assetUtil$checkAssetExist$Anon1.Z$Anon0 = booleanValue2;
                assetUtil$checkAssetExist$Anon1.L$Anon10 = sb2;
                assetUtil$checkAssetExist$Anon1.label = 3;
                obj2 = yf4.a(b2, assetUtil$checkAssetExist$isFilePath2Exist$Anon1, assetUtil$checkAssetExist$Anon1);
                a = a;
                if (obj2 == a) {
                    return a;
                }
                z = booleanValue2;
                assetUtil = assetUtil2;
                str9 = sb2;
                str13 = str20;
                str15 = str19;
                onImageCallbackListener2 = onImageCallbackListener3;
                booleanValue = ((Boolean) obj2).booleanValue();
                if (!booleanValue) {
                }
            }
        }
        assetUtil$checkAssetExist$Anon1 = new AssetUtil$checkAssetExist$Anon1(this, yb42);
        Object obj22 = assetUtil$checkAssetExist$Anon1.result;
        Object a2 = cc4.a();
        i = assetUtil$checkAssetExist$Anon1.label;
        if (i != 0) {
        }
        booleanValue2 = ((Boolean) obj).booleanValue();
        if (!booleanValue2) {
        }
    }

    @DexIgnore
    public final boolean checkFileExist(String str) {
        kd4.b(str, "filePath");
        return new File(str).exists();
    }
}
