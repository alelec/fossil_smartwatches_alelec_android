package com.portfolio.platform.cloudimage;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$isFilePath1Exist$1", mo27670f = "AssetUtil.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class AssetUtil$checkAssetExist$isFilePath1Exist$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Boolean>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $filePath1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20991p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AssetUtil$checkAssetExist$isFilePath1Exist$1(java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$filePath1 = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$isFilePath1Exist$1 assetUtil$checkAssetExist$isFilePath1Exist$1 = new com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$isFilePath1Exist$1(this.$filePath1, yb4);
        assetUtil$checkAssetExist$isFilePath1Exist$1.f20991p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return assetUtil$checkAssetExist$isFilePath1Exist$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$isFilePath1Exist$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.dc4.m20839a(com.portfolio.platform.cloudimage.AssetUtil.INSTANCE.checkFileExist(this.$filePath1));
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
