package com.portfolio.platform.cloudimage;

import com.fossil.blesdk.obfuscated.h42;
import com.portfolio.platform.PortfolioApp;
import dagger.MembersInjector;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CloudImageHelper_MembersInjector implements MembersInjector<CloudImageHelper> {
    @DexIgnore
    public /* final */ Provider<h42> mAppExecutorsProvider;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> mAppProvider;

    @DexIgnore
    public CloudImageHelper_MembersInjector(Provider<h42> provider, Provider<PortfolioApp> provider2) {
        this.mAppExecutorsProvider = provider;
        this.mAppProvider = provider2;
    }

    @DexIgnore
    public static MembersInjector<CloudImageHelper> create(Provider<h42> provider, Provider<PortfolioApp> provider2) {
        return new CloudImageHelper_MembersInjector(provider, provider2);
    }

    @DexIgnore
    public static void injectMApp(CloudImageHelper cloudImageHelper, PortfolioApp portfolioApp) {
        cloudImageHelper.mApp = portfolioApp;
    }

    @DexIgnore
    public static void injectMAppExecutors(CloudImageHelper cloudImageHelper, h42 h42) {
        cloudImageHelper.mAppExecutors = h42;
    }

    @DexIgnore
    public void injectMembers(CloudImageHelper cloudImageHelper) {
        injectMAppExecutors(cloudImageHelper, this.mAppExecutorsProvider.get());
        injectMApp(cloudImageHelper, this.mAppProvider.get());
    }
}
