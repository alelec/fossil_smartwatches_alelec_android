package com.portfolio.platform.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import java.util.Arrays;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.receiver.AlarmReceiver$onReceive$Anon1", f = "AlarmReceiver.kt", l = {}, m = "invokeSuspend")
public final class AlarmReceiver$onReceive$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $action;
    @DexIgnore
    public /* final */ /* synthetic */ Context $context;
    @DexIgnore
    public /* final */ /* synthetic */ Intent $intent;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmReceiver this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmReceiver$onReceive$Anon1(AlarmReceiver alarmReceiver, int i, Intent intent, Context context, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = alarmReceiver;
        this.$action = i;
        this.$intent = intent;
        this.$context = context;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // AlarmReceiver$onReceive$Anon1 alarmReceiver$onReceive$Anon1 = new AlarmReceiver$onReceive$Anon1(this.this$Anon0, this.$action, this.$intent, this.$context, yb4);
        // alarmReceiver$onReceive$Anon1.p$ = (zg4) obj;
        // return alarmReceiver$onReceive$Anon1;
    }

    // @DexIgnore
    // public final Object invoke(Object obj, Object obj2) {
    //     return ((AlarmReceiver$onReceive$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    // }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        throw null;
        // NotificationBaseObj.ANotificationFlag aNotificationFlag;
        // cc4.a();
        // if (this.label == 0) {
        //     na4.a(obj);
        //     MFUser currentUser = this.this$Anon0.e().getCurrentUser();
        //     int i = this.$action;
        //     if (i == 0) {
        //         Alarm findNextActiveAlarm = this.this$Anon0.b().findNextActiveAlarm();
        //         String stringExtra = this.$intent.getStringExtra("DEF_ALARM_RECEIVER_USER_ID");
        //         if (currentUser == null || TextUtils.isEmpty(stringExtra) || (true ^ kd4.a((Object) currentUser.getUserId(), (Object) stringExtra)) || findNextActiveAlarm == null) {
        //             FLogger.INSTANCE.getLocal().e("AlarmReceiver", "onReceive - user=null||empty||equals=false");
        //             this.this$Anon0.a().a(this.$context);
        //         } else {
        //             Bundle bundle = new Bundle();
        //             bundle.putInt("DEF_ALARM_RECEIVER_ACTION", 3);
        //             bundle.putString("DEF_ALARM_RECEIVER_USER_ID", currentUser.getUserId());
        //             Intent intent = new Intent(this.$context, AlarmReceiver.class);
        //             intent.setAction("com.portfolio.platform.ALARM_RECEIVER");
        //             intent.putExtras(bundle);
        //             AlarmManager alarmManager = (AlarmManager) this.$context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        //             if (alarmManager != null) {
        //                 alarmManager.setExact(0, AlarmHelper.f.a(findNextActiveAlarm.getMillisecond()), PendingIntent.getBroadcast(this.$context, 101, intent, 134217728));
        //             }
        //         }
        //     } else if (i == 2) {
        //         FLogger.INSTANCE.getLocal().d("AlarmReceiver", "onReceive - ACTION_ALARM_REPEAT");
        //         try {
        //             String e = PortfolioApp.W.c().e();
        //             if (e.length() == 0) {
        //                 return qa4.a;
        //             }
        //             long g = this.this$Anon0.d().g(e);
        //             long e2 = this.this$Anon0.d().e(e);
        //             long currentTimeMillis = System.currentTimeMillis();
        //             long j = (long) 432000000;
        //             if (currentTimeMillis - e2 > j && currentTimeMillis - g > j) {
        //                 FLogger.INSTANCE.getLocal().d("AlarmReceiver", "Remind to sync success");
        //                 this.this$Anon0.d().a(currentTimeMillis, e);
        //                 MFUser currentUser2 = PortfolioApp.W.c().s().getCurrentUser();
        //                 String firstName = currentUser2 != null ? currentUser2.getFirstName() : null;
        //                 if (firstName != null) {
        //                     pd4 pd4 = pd4.a;
        //                     String a = sm2.a(this.$context, (int) R.string.hello_user);
        //                     kd4.a((Object) a, "LanguageHelper.getString\u2026ext, R.string.hello_user)");
        //                     Object[] objArr = {firstName};
        //                     String format = String.format(a, Arrays.copyOf(objArr, objArr.length));
        //                     kd4.a((Object) format, "java.lang.String.format(format, *args)");
        //                     String a2 = sm2.a(this.$context, (int) R.string.General_Sync_Reminder_Text__ItsBeenAFewDaysSince);
        //                     FossilNotificationBar.Companion companion = FossilNotificationBar.c;
        //                     Context applicationContext = PortfolioApp.W.c().getApplicationContext();
        //                     kd4.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
        //                     kd4.a((Object) a2, "message");
        //                     companion.a(applicationContext, format, a2);
        //                     if (FossilDeviceSerialPatternUtil.isDianaDevice(e)) {
        //                         FLogger.INSTANCE.getLocal().d("AlarmReceiver", "Remind to sync send notificaiton to watch");
        //                         PortfolioApp c = PortfolioApp.W.c();
        //                         NotificationBaseObj.ANotificationType notificationType = DianaNotificationObj.AApplicationName.FOSSIL.getNotificationType();
        //                         DianaNotificationObj.AApplicationName aApplicationName = DianaNotificationObj.AApplicationName.FOSSIL;
        //                         NotificationBaseObj.ANotificationFlag[] aNotificationFlagArr = new NotificationBaseObj.ANotificationFlag[1];
        //                         if (DeviceHelper.o.l()) {
        //                             aNotificationFlag = NotificationBaseObj.ANotificationFlag.ALLOW_USER_ACTION;
        //                         } else {
        //                             aNotificationFlag = NotificationBaseObj.ANotificationFlag.IMPORTANT;
        //                         }
        //                         aNotificationFlagArr[0] = aNotificationFlag;
        //                         c.a(e, (NotificationBaseObj) new DianaNotificationObj(99, notificationType, aApplicationName, "Smart Watch", "Fossil", a2, cb4.d(aNotificationFlagArr)));
        //                     }
        //                 }
        //             }
        //         } catch (Exception e3) {
        //             FLogger.INSTANCE.getLocal().e("AlarmReceiver", e3.getMessage());
        //             e3.printStackTrace();
        //         }
        //     } else if (i == 3) {
        //         String stringExtra2 = this.$intent.getStringExtra("DEF_ALARM_RECEIVER_USER_ID");
        //         if (currentUser == null || TextUtils.isEmpty(stringExtra2) || (!kd4.a((Object) currentUser.getUserId(), (Object) stringExtra2))) {
        //             FLogger.INSTANCE.getLocal().e("AlarmReceiver", "onReceive - user=null||empty||equals=false");
        //             this.this$Anon0.a().a(this.$context);
        //         } else {
        //             this.this$Anon0.a().b(this.$context);
        //         }
        //     }
        //     if (TextUtils.isEmpty(PortfolioApp.W.c().e())) {
        //         FLogger.INSTANCE.getLocal().d("AlarmReceiver", "onReceive - active device is EMPTY!!!");
        //         return qa4.a;
        //     }
        //     Device deviceBySerial = this.this$Anon0.c().getDeviceBySerial(PortfolioApp.W.c().e());
        //     if (deviceBySerial != null) {
        //         int w = this.this$Anon0.d().w();
        //         int intExtra = this.$intent.getIntExtra("REQUEST_CODE", -1);
        //         ILocalFLogger local = FLogger.INSTANCE.getLocal();
        //         local.d("AlarmReceiver", "onReceive - extraCode=" + intExtra + ", lowBatteryLevel=" + w + ", deviceBattery=" + deviceBySerial.getBatteryLevel());
        //         if (intExtra == 0) {
        //             if (deviceBySerial.getBatteryLevel() >= w) {
        //                 this.this$Anon0.a().f();
        //             } else {
        //                 this.this$Anon0.a().c();
        //             }
        //         }
        //     }
        //     return qa4.a;
        // }
        // throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }

    @Override
    @DexIgnore
    public Object invoke(zg4 zg4, yb4<? super qa4> yb4) {
        throw null;
        // return null;
    }
}
