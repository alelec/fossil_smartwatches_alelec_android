package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.am2;
import com.fossil.blesdk.obfuscated.bm2;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.im2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.ts3;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.model.NotificationInfo;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import java.util.Calendar;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SmsMmsReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public DianaNotificationComponent a;
    @DexIgnore
    public en2 b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = SmsMmsReceiver.class.getSimpleName();
        kd4.a((Object) simpleName, "SmsMmsReceiver::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        throw null;
        // kd4.b(context, "context");
        // kd4.b(intent, "intent");
        // Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        // ref$ObjectRef.element = null;
        // ILocalFLogger local = FLogger.INSTANCE.getLocal();
        // String str = c;
        // StringBuilder sb = new StringBuilder();
        // sb.append("SmsMmsReceiver : ");
        // String action = intent.getAction();
        // if (action != null) {
        //     sb.append(action);
        //     local.d(str, sb.toString());
        //     en2 en2 = this.b;
        //     if (en2 == null) {
        //         kd4.d("mSharePref");
        //         throw null;
        //     } else if (!en2.M()) {
        //         String str2 = "";
        //         if (TextUtils.isEmpty(intent.getAction()) || !kd4.a((Object) intent.getAction(), (Object) "android.provider.Telephony.WAP_PUSH_RECEIVED")) {
        //             try {
        //                 SmsMessage[] messagesFromIntent = Telephony.Sms.Intents.getMessagesFromIntent(intent);
        //                 if (messagesFromIntent != null) {
        //                     if (!(messagesFromIntent.length == 0)) {
        //                         for (SmsMessage smsMessage : messagesFromIntent) {
        //                             kd4.a((Object) smsMessage, "message");
        //                             String messageBody = smsMessage.getMessageBody();
        //                             String originatingAddress = smsMessage.getOriginatingAddress();
        //                             ref$ObjectRef.element = new NotificationInfo(NotificationSource.TEXT, originatingAddress, messageBody, str2);
        //                             if (!TextUtils.isEmpty(messageBody) && !TextUtils.isEmpty(originatingAddress)) {
        //                                 break;
        //                             }
        //                         }
        //                     }
        //                 }
        //             } catch (Exception e) {
        //                 FLogger.INSTANCE.getLocal().e(c, "onReceive() - SMS - error " + e);
        //             }
        //         } else {
        //             try {
        //                 if (intent.hasExtra("data")) {
        //                     bm2 a2 = new im2(intent.getByteArrayExtra("data")).a();
        //                     if (a2 != null && a2.a() != null) {
        //                         am2 a3 = a2.a();
        //                         kd4.a((Object) a3, "pdu.from");
        //                         String a4 = a3.a();
        //                         FLogger.INSTANCE.getLocal().d(c, "onReceive() - MMS - originatingAddress = " + a4);
        //                         ref$ObjectRef.element = new NotificationInfo(NotificationSource.TEXT, a4, str2, str2);
        //                     }
        //                 }
        //             } catch (Exception e2) {
        //                 FLogger.INSTANCE.getLocal().e(c, "onReceive() - MMS - error " + e2);
        //             }
        //         }
        //         if (((NotificationInfo) ref$ObjectRef.element) != null) {
        //             ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        //             String str3 = c;
        //             StringBuilder sb2 = new StringBuilder();
        //             sb2.append("onReceive() - SMSMMS - sender info ");
        //             String senderInfo = ((NotificationInfo) ref$ObjectRef.element).getSenderInfo();
        //             if (senderInfo != null) {
        //                 sb2.append(senderInfo);
        //                 local2.d(str3, sb2.toString());
        //                 if (!FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.W.c().e())) {
        //                     ts3.b(((NotificationInfo) ref$ObjectRef.element).getSenderInfo());
        //                     en2 en22 = this.b;
        //                     if (en22 == null) {
        //                         kd4.d("mSharePref");
        //                         throw null;
        //                     } else if (en22.D()) {
        //                         FLogger.INSTANCE.getLocal().d(c, "onReceive() - Blocked by DND mode");
        //                     } else {
        //                         ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        //                         String str4 = c;
        //                         StringBuilder sb3 = new StringBuilder();
        //                         sb3.append("onReceive() - SMS-MMS - sender info ");
        //                         String senderInfo2 = ((NotificationInfo) ref$ObjectRef.element).getSenderInfo();
        //                         if (senderInfo2 != null) {
        //                             sb3.append(senderInfo2);
        //                             local3.d(str4, sb3.toString());
        //                             fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new SmsMmsReceiver$onReceive$Anon1(ref$ObjectRef, (yb4) null), 3, (Object) null);
        //                             return;
        //                         }
        //                         kd4.a();
        //                         throw null;
        //                     }
        //                 } else {
        //                     if (((NotificationInfo) ref$ObjectRef.element).getSenderInfo() != null) {
        //                         str2 = ((NotificationInfo) ref$ObjectRef.element).getSenderInfo();
        //                     }
        //                     DianaNotificationComponent dianaNotificationComponent = this.a;
        //                     if (dianaNotificationComponent == null) {
        //                         kd4.d("mDianaNotificationComponent");
        //                         throw null;
        //                     } else if (str2 != null) {
        //                         String body = ((NotificationInfo) ref$ObjectRef.element).getBody();
        //                         kd4.a((Object) body, "notificationInfo.body");
        //                         Calendar instance = Calendar.getInstance();
        //                         kd4.a((Object) instance, "Calendar.getInstance()");
        //                         dianaNotificationComponent.a(str2, body, instance.getTimeInMillis());
        //                     } else {
        //                         kd4.a();
        //                         throw null;
        //                     }
        //                 }
        //             } else {
        //                 kd4.a();
        //                 throw null;
        //             }
        //         }
        //     }
        // } else {
        //     kd4.a();
        //     throw null;
        // }
    }
}
