package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ps3;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.service.MFDeviceService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RestartServiceReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = RestartServiceReceiver.class.getSimpleName();
        kd4.a((Object) simpleName, "RestartServiceReceiver::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        throw null;
        // kd4.b(context, "context");
        // kd4.b(intent, "intent");
        // FLogger.INSTANCE.getLocal().e(a, "Inside ---onReceive RestartServiceReceiver");
        // Context context2 = context;
        // ps3.a.a(ps3.a, context2, MFDeviceService.class, (String) null, 4, (Object) null);
        // ps3.a.a(ps3.a, context2, ButtonService.class, (String) null, 4, (Object) null);
    }
}
