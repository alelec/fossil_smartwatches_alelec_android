package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AlarmHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class BootReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String b; // = BootReceiver.class.getSimpleName();
    @DexIgnore
    public AlarmHelper a;

    @DexIgnore
    public BootReceiver() {
        PortfolioApp.R.g().a(this);
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "Inside " + b + ".BootReceiver, start hwlog sync scheduler");
        if (!TextUtils.isEmpty(intent.getAction()) && intent.getAction().equalsIgnoreCase("android.intent.action.BOOT_COMPLETED")) {
            AlarmHelper alarmHelper = this.a;
            if (alarmHelper != null) {
                alarmHelper.d();
                this.a.e();
            }
        }
    }
}
