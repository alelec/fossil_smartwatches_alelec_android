package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.ts3;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ImageButton extends RelativeLayout {
    @DexIgnore
    public /* final */ ImageView e; // = ((ImageView) findViewById(R.id.fossil_button_icon));
    @DexIgnore
    public /* final */ TextView f; // = ((TextView) findViewById(R.id.fossil_button_title));
    @DexIgnore
    public /* final */ ViewGroup g; // = ((ViewGroup) findViewById(R.id.fossil_button_container));

    @DexIgnore
    public ImageButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        RelativeLayout.inflate(context, R.layout.view_fossil_button, this);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, h62.ImageButton);
        setIcon(obtainStyledAttributes.getResourceId(1, -1));
        setTitle(obtainStyledAttributes.getResourceId(2, -1));
        setTitleColor(obtainStyledAttributes.getResourceId(3, -1));
        setBackground(obtainStyledAttributes.getResourceId(0, -1));
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    private void setBackground(int i) {
        Drawable c = k6.c(getContext(), R.drawable.bg_graydark_to_scarlett);
        if (i != -1) {
            c = k6.c(getContext(), i);
        }
        this.g.setBackground(c);
    }

    @DexIgnore
    private void setIcon(int i) {
        if (i != -1) {
            this.e.setImageDrawable(k6.c(getContext(), i));
            this.e.setVisibility(0);
            return;
        }
        this.e.setVisibility(8);
    }

    @DexIgnore
    private void setTitle(int i) {
        if (i != -1) {
            sm2.a(this.f, i);
            this.f.setVisibility(0);
            return;
        }
        this.f.setVisibility(8);
    }

    @DexIgnore
    private void setTitleColor(int i) {
        if (i != -1) {
            this.f.setTextColor(getResources().getColorStateList(i));
        }
    }

    @DexIgnore
    public void a(int i, int i2) {
        ViewGroup.LayoutParams layoutParams = this.e.getLayoutParams();
        layoutParams.width = i;
        layoutParams.height = i2;
        this.e.setLayoutParams(layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.e.getLayoutParams();
        layoutParams2.setMarginStart((int) (ts3.a(70.0f) - (((float) i) - ts3.a(30.0f))));
        this.e.setLayoutParams(layoutParams2);
    }

    @DexIgnore
    public TextView getTextView() {
        return this.f;
    }

    @DexIgnore
    public String getTitle() {
        return this.f.getText().toString();
    }

    @DexIgnore
    public void setTitle(String str) {
        this.f.setText(str);
        this.f.setVisibility(0);
    }
}
