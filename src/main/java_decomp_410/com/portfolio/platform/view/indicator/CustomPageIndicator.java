package com.portfolio.platform.view.indicator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kl2;
import com.fossil.blesdk.obfuscated.wi;
import com.fossil.blesdk.obfuscated.yt3;
import com.fossil.wearables.fossil.R;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomPageIndicator extends View implements yt3 {
    @DexIgnore
    public float e;
    @DexIgnore
    public /* final */ Paint f;
    @DexIgnore
    public /* final */ Paint g;
    @DexIgnore
    public /* final */ Paint h;
    @DexIgnore
    public RecyclerView i;
    @DexIgnore
    public ViewPager j;
    @DexIgnore
    public ViewPager.i k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;
    @DexIgnore
    public int o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public float r;
    @DexIgnore
    public int s;
    @DexIgnore
    public float t;
    @DexIgnore
    public int u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public List<a> w;
    @DexIgnore
    public Rect x;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.indicator.CustomPageIndicator$a$a")
        /* renamed from: com.portfolio.platform.view.indicator.CustomPageIndicator$a$a  reason: collision with other inner class name */
        public static final class C0160a {
            @DexIgnore
            public C0160a() {
            }

            @DexIgnore
            public /* synthetic */ C0160a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            new C0160a((fd4) null);
        }
        */

        @DexIgnore
        public a(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(int i, int i2, int i3, fd4 fd4) {
            this(i, (i3 & 2) != 0 ? 0 : i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends View.BaseSavedState {
        @DexIgnore
        public static /* final */ a CREATOR; // = new a((fd4) null);
        @DexIgnore
        public int e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Parcelable.Creator<b> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }

            @DexIgnore
            public b createFromParcel(Parcel parcel) {
                kd4.b(parcel, "parcel");
                return new b(parcel);
            }

            @DexIgnore
            public b[] newArray(int i) {
                return new b[i];
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Parcelable parcelable) {
            super(parcelable);
            kd4.b(parcelable, "superState");
        }

        @DexIgnore
        public final int a() {
            return this.e;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            kd4.b(parcel, "dest");
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.e);
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Parcel parcel) {
            super(parcel);
            kd4.b(parcel, "in");
            this.e = parcel.readInt();
        }

        @DexIgnore
        public final void a(int i) {
            this.e = i;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends RecyclerView.q {
        @DexIgnore
        public /* final */ /* synthetic */ CustomPageIndicator a;

        @DexIgnore
        public c(CustomPageIndicator customPageIndicator) {
            this.a = customPageIndicator;
        }

        @DexIgnore
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            kd4.b(recyclerView, "recyclerView");
            super.onScrolled(recyclerView, i, i2);
            RecyclerView mRecyclerView$app_fossilRelease = this.a.getMRecyclerView$app_fossilRelease();
            if (mRecyclerView$app_fossilRelease != null) {
                RecyclerView.m layoutManager = mRecyclerView$app_fossilRelease.getLayoutManager();
                if (layoutManager != null) {
                    int G = ((LinearLayoutManager) layoutManager).G();
                    CustomPageIndicator customPageIndicator = this.a;
                    if (G == -1) {
                        G = customPageIndicator.getMCurrentPage$app_fossilRelease();
                    } else if (kl2.a(customPageIndicator.getContext())) {
                        RecyclerView mRecyclerView$app_fossilRelease2 = this.a.getMRecyclerView$app_fossilRelease();
                        if (mRecyclerView$app_fossilRelease2 != null) {
                            RecyclerView.g adapter = mRecyclerView$app_fossilRelease2.getAdapter();
                            if (adapter != null) {
                                kd4.a((Object) adapter, "mRecyclerView!!.adapter!!");
                                G = (adapter.getItemCount() - G) - 1;
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    customPageIndicator.setMCurrentPage$app_fossilRelease(G);
                    ViewPager.i mListener$app_fossilRelease = this.a.getMListener$app_fossilRelease();
                    if (mListener$app_fossilRelease != null) {
                        mListener$app_fossilRelease.a(this.a.getMCurrentPage$app_fossilRelease());
                    }
                    this.a.invalidate();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomPageIndicator(Context context) {
        super(context);
        kd4.b(context, "context");
        this.f = new Paint(1);
        this.g = new Paint(1);
        this.h = new Paint(1);
        this.t = -1.0f;
        this.u = -1;
        this.w = new ArrayList();
        this.x = new Rect();
    }

    @DexIgnore
    public void a(ViewPager viewPager, int i2) {
        setViewPager(viewPager);
        setCurrentItem(i2);
    }

    @DexIgnore
    public void b(int i2) {
        this.n = i2;
        ViewPager.i iVar = this.k;
        if (iVar != null) {
            iVar.b(i2);
        }
    }

    @DexIgnore
    public final int c(int i2) {
        for (a next : this.w) {
            if (next.b() == i2) {
                return next.a();
            }
        }
        return 0;
    }

    @DexIgnore
    public final int d(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            return size;
        }
        ViewPager viewPager = this.j;
        if (viewPager == null) {
            return size;
        }
        if (viewPager != null) {
            wi adapter = viewPager.getAdapter();
            if (adapter != null) {
                kd4.a((Object) adapter, "mViewPager!!.adapter!!");
                int a2 = adapter.a();
                float f2 = this.e;
                int paddingLeft = (int) (((float) getPaddingLeft()) + ((float) getPaddingRight()) + (((float) a2) * 2.0f * f2) + (((float) (a2 - 1)) * f2) + 1.0f);
                return mode == Integer.MIN_VALUE ? Math.min(paddingLeft, size) : paddingLeft;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final int e(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            return size;
        }
        int paddingTop = (int) ((((float) 2) * this.e) + ((float) getPaddingTop()) + ((float) getPaddingBottom()) + 1.0f);
        return mode == Integer.MIN_VALUE ? Math.min(paddingTop, size) : paddingTop;
    }

    @DexIgnore
    public final int getMCurrentPage$app_fossilRelease() {
        return this.l;
    }

    @DexIgnore
    public final ViewPager.i getMListener$app_fossilRelease() {
        return this.k;
    }

    @DexIgnore
    public final RecyclerView getMRecyclerView$app_fossilRelease() {
        return this.i;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0069 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x006a  */
    public void onDraw(Canvas canvas) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        float f2;
        int i7;
        int i8;
        Canvas canvas2 = canvas;
        kd4.b(canvas2, "canvas");
        super.onDraw(canvas);
        ViewPager viewPager = this.j;
        Throwable th = null;
        if (viewPager != null) {
            if (viewPager == null) {
                kd4.a();
                throw null;
            } else if (viewPager.getAdapter() != null) {
                ViewPager viewPager2 = this.j;
                if (viewPager2 != null) {
                    wi adapter = viewPager2.getAdapter();
                    if (adapter != null) {
                        kd4.a((Object) adapter, "mViewPager!!.adapter!!");
                        i2 = adapter.a();
                        if (i2 != 0) {
                            if (this.l >= i2) {
                                setCurrentItem(i2 - 1);
                                return;
                            }
                            if (this.o == 0) {
                                i6 = getWidth();
                                i5 = getPaddingLeft();
                                i4 = getPaddingRight();
                                i3 = getPaddingTop();
                            } else {
                                i6 = getHeight();
                                i5 = getPaddingTop();
                                i4 = getPaddingBottom();
                                i3 = getPaddingLeft();
                            }
                            float f3 = this.e;
                            float f4 = (((float) 2) * f3) + this.r;
                            float f5 = ((float) i3) + f3;
                            float f6 = ((float) i5) + f3;
                            if (this.p) {
                                f6 += (((float) ((i6 - i5) - i4)) / 2.0f) - ((((float) i2) * f4) / 2.0f);
                            }
                            float f7 = this.e;
                            if (this.g.getStrokeWidth() > ((float) 0)) {
                                f7 -= this.g.getStrokeWidth() / 2.0f;
                            }
                            int i9 = 0;
                            while (i9 < i2) {
                                float f8 = (((float) i9) * f4) + f6;
                                if (this.o == 0) {
                                    f2 = f5;
                                } else {
                                    f2 = f8;
                                    f8 = f5;
                                }
                                int i10 = this.q ? this.m : this.l;
                                RecyclerView recyclerView = this.i;
                                if (recyclerView == null) {
                                    i7 = 0;
                                } else if (recyclerView != null) {
                                    RecyclerView.g adapter2 = recyclerView.getAdapter();
                                    if (adapter2 != null) {
                                        i7 = c(adapter2.getItemViewType(i9));
                                    } else {
                                        kd4.a();
                                        throw th;
                                    }
                                } else {
                                    kd4.a();
                                    throw th;
                                }
                                if (i7 != 0) {
                                    Drawable c2 = k6.c(getContext(), i7);
                                    Rect rect = this.x;
                                    float f9 = this.e;
                                    i8 = i2;
                                    rect.set((int) (f8 - f9), (int) (f2 - f9), ((int) (f8 - f9)) + (((int) f9) * 2), ((int) (f2 - f9)) + (((int) f9) * 2));
                                    if (c2 != null) {
                                        c2.setBounds(this.x);
                                    }
                                    if (i9 != i10) {
                                        if (c2 != null) {
                                            c2.setColorFilter(this.f.getColor(), PorterDuff.Mode.SRC_IN);
                                        }
                                    } else if (c2 != null) {
                                        c2.clearColorFilter();
                                    }
                                    if (c2 != null) {
                                        c2.draw(canvas2);
                                    }
                                } else {
                                    i8 = i2;
                                    if (i9 == i10) {
                                        canvas2.drawCircle(f8, f2, this.e, this.h);
                                    } else if (this.f.getAlpha() > 0) {
                                        canvas2.drawCircle(f8, f2, f7, this.f);
                                    }
                                    float f10 = this.e;
                                    if (f7 != f10) {
                                        canvas2.drawCircle(f8, f2, f10, this.g);
                                    }
                                }
                                i9++;
                                i2 = i8;
                                th = null;
                            }
                            return;
                        }
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
        }
        RecyclerView recyclerView2 = this.i;
        if (recyclerView2 != null) {
            if (recyclerView2 == null) {
                kd4.a();
                throw null;
            } else if (recyclerView2.getAdapter() != null) {
                RecyclerView recyclerView3 = this.i;
                if (recyclerView3 != null) {
                    RecyclerView.g adapter3 = recyclerView3.getAdapter();
                    if (adapter3 != null) {
                        kd4.a((Object) adapter3, "mRecyclerView!!.adapter!!");
                        i2 = adapter3.getItemCount();
                        if (i2 != 0) {
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }
        i2 = 0;
        if (i2 != 0) {
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (this.o == 0) {
            setMeasuredDimension(d(i2), e(i3));
        } else {
            setMeasuredDimension(e(i2), d(i3));
        }
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        kd4.b(parcelable, "state");
        b bVar = (b) parcelable;
        super.onRestoreInstanceState(bVar.getSuperState());
        this.l = bVar.a();
        this.m = bVar.a();
        requestLayout();
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (onSaveInstanceState == null) {
            return null;
        }
        b bVar = new b(onSaveInstanceState);
        bVar.a(this.l);
        return bVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0082, code lost:
        if (r11.a() != false) goto L_0x0089;
     */
    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        kd4.b(motionEvent, "ev");
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        ViewPager viewPager = this.j;
        int i2 = 0;
        if (viewPager != null) {
            if (viewPager == null) {
                kd4.a();
                throw null;
            } else if (viewPager.getAdapter() != null) {
                ViewPager viewPager2 = this.j;
                if (viewPager2 != null) {
                    wi adapter = viewPager2.getAdapter();
                    if (adapter != null) {
                        kd4.a((Object) adapter, "mViewPager!!.adapter!!");
                        if (adapter.a() != 0) {
                            int action = motionEvent.getAction() & 255;
                            if (action == 0) {
                                this.u = motionEvent.getPointerId(0);
                                this.t = motionEvent.getX();
                            } else if (action == 2) {
                                float x2 = motionEvent.getX(motionEvent.findPointerIndex(this.u));
                                float f2 = x2 - this.t;
                                if (!this.v && Math.abs(f2) > ((float) this.s)) {
                                    this.v = true;
                                }
                                if (this.v) {
                                    this.t = x2;
                                    ViewPager viewPager3 = this.j;
                                    if (viewPager3 != null) {
                                        if (!viewPager3.g()) {
                                            ViewPager viewPager4 = this.j;
                                            if (viewPager4 == null) {
                                                kd4.a();
                                                throw null;
                                            }
                                        }
                                        ViewPager viewPager5 = this.j;
                                        if (viewPager5 != null) {
                                            viewPager5.b(f2);
                                        } else {
                                            kd4.a();
                                            throw null;
                                        }
                                    } else {
                                        kd4.a();
                                        throw null;
                                    }
                                }
                            } else if (action == 3 || action == 1) {
                                if (!this.v) {
                                    ViewPager viewPager6 = this.j;
                                    if (viewPager6 != null) {
                                        wi adapter2 = viewPager6.getAdapter();
                                        if (adapter2 != null) {
                                            kd4.a((Object) adapter2, "mViewPager!!.adapter!!");
                                            int a2 = adapter2.a();
                                            float width = (float) getWidth();
                                            float f3 = width / 2.0f;
                                            float f4 = width / 6.0f;
                                            if (this.l > 0 && motionEvent.getX() < f3 - f4) {
                                                if (action != 3) {
                                                    ViewPager viewPager7 = this.j;
                                                    if (viewPager7 != null) {
                                                        viewPager7.setCurrentItem(this.l - 1);
                                                    } else {
                                                        kd4.a();
                                                        throw null;
                                                    }
                                                }
                                                return true;
                                            } else if (this.l < a2 - 1 && motionEvent.getX() > f3 + f4) {
                                                if (action != 3) {
                                                    ViewPager viewPager8 = this.j;
                                                    if (viewPager8 != null) {
                                                        viewPager8.setCurrentItem(this.l + 1);
                                                    } else {
                                                        kd4.a();
                                                        throw null;
                                                    }
                                                }
                                                return true;
                                            }
                                        } else {
                                            kd4.a();
                                            throw null;
                                        }
                                    } else {
                                        kd4.a();
                                        throw null;
                                    }
                                }
                                this.v = false;
                                this.u = -1;
                                ViewPager viewPager9 = this.j;
                                if (viewPager9 == null) {
                                    kd4.a();
                                    throw null;
                                } else if (viewPager9.g()) {
                                    ViewPager viewPager10 = this.j;
                                    if (viewPager10 != null) {
                                        viewPager10.d();
                                    } else {
                                        kd4.a();
                                        throw null;
                                    }
                                }
                            } else if (action == 5) {
                                int actionIndex = motionEvent.getActionIndex();
                                this.t = motionEvent.getX(actionIndex);
                                this.u = motionEvent.getPointerId(actionIndex);
                            } else if (action == 6) {
                                int actionIndex2 = motionEvent.getActionIndex();
                                if (motionEvent.getPointerId(actionIndex2) == this.u) {
                                    if (actionIndex2 == 0) {
                                        i2 = 1;
                                    }
                                    this.u = motionEvent.getPointerId(i2);
                                }
                                this.t = motionEvent.getX(motionEvent.findPointerIndex(this.u));
                            }
                            return true;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public void setCurrentItem(int i2) {
        if (this.j == null && this.i == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        ViewPager viewPager = this.j;
        if (viewPager == null) {
            if (kl2.a(getContext())) {
                RecyclerView recyclerView = this.i;
                if (recyclerView != null) {
                    RecyclerView.g adapter = recyclerView.getAdapter();
                    if (adapter != null) {
                        kd4.a((Object) adapter, "mRecyclerView!!.adapter!!");
                        i2 = (adapter.getItemCount() - i2) - 1;
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
            this.l = i2;
        } else if (viewPager != null) {
            viewPager.setCurrentItem(i2);
            if (kl2.a(getContext())) {
                ViewPager viewPager2 = this.j;
                if (viewPager2 != null) {
                    wi adapter2 = viewPager2.getAdapter();
                    if (adapter2 != null) {
                        kd4.a((Object) adapter2, "mViewPager!!.adapter!!");
                        i2 = (adapter2.a() - i2) - 1;
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
            this.l = i2;
        } else {
            kd4.a();
            throw null;
        }
        invalidate();
    }

    @DexIgnore
    public final void setMCurrentPage$app_fossilRelease(int i2) {
        this.l = i2;
    }

    @DexIgnore
    public final void setMListener$app_fossilRelease(ViewPager.i iVar) {
        this.k = iVar;
    }

    @DexIgnore
    public final void setMRecyclerView$app_fossilRelease(RecyclerView recyclerView) {
        this.i = recyclerView;
    }

    @DexIgnore
    public void setOnPageChangeListener(ViewPager.i iVar) {
    }

    @DexIgnore
    public void setViewPager(ViewPager viewPager) {
        if (!kd4.a((Object) this.j, (Object) viewPager)) {
            ViewPager viewPager2 = this.j;
            if (viewPager2 != null) {
                viewPager2.b((ViewPager.i) this);
            }
            if ((viewPager != null ? viewPager.getAdapter() : null) != null) {
                this.j = viewPager;
                ViewPager viewPager3 = this.j;
                if (viewPager3 != null) {
                    viewPager3.a((ViewPager.i) this);
                }
                invalidate();
                return;
            }
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
    }

    @DexIgnore
    public final void a(RecyclerView recyclerView, int i2, List<a> list) {
        kd4.b(list, "indicatorType");
        this.w.clear();
        this.w.addAll(list);
        a(recyclerView, i2);
    }

    @DexIgnore
    public void a(RecyclerView recyclerView, int i2) {
        if (!kd4.a((Object) this.i, (Object) recyclerView)) {
            if ((recyclerView != null ? recyclerView.getAdapter() : null) != null) {
                this.i = recyclerView;
                RecyclerView recyclerView2 = this.i;
                if (recyclerView2 != null) {
                    recyclerView2.a((RecyclerView.q) new c(this));
                }
                if (this.i != null) {
                    if (kl2.a(getContext())) {
                        RecyclerView recyclerView3 = this.i;
                        if (recyclerView3 != null) {
                            RecyclerView.g adapter = recyclerView3.getAdapter();
                            if (adapter != null) {
                                kd4.a((Object) adapter, "mRecyclerView!!.adapter!!");
                                i2 = (adapter.getItemCount() - i2) - 1;
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    this.l = i2;
                    invalidate();
                    return;
                }
                return;
            }
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CustomPageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.vpiCirclePageIndicatorStyle);
        kd4.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomPageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        kd4.b(context, "context");
        this.f = new Paint(1);
        this.g = new Paint(1);
        this.h = new Paint(1);
        this.t = -1.0f;
        this.u = -1;
        this.w = new ArrayList();
        this.x = new Rect();
        if (!isInEditMode()) {
            Resources resources = getResources();
            int a2 = k6.a(context, (int) R.color.hex80A3A19E);
            int a3 = k6.a(context, (int) R.color.fossilOrange);
            int a4 = k6.a(context, (int) R.color.transparent);
            float dimension = resources.getDimension(R.dimen.dp6);
            float dimension2 = resources.getDimension(R.dimen.dp9);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.CirclePageIndicator, i2, 0);
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, h62.LinePageIndicator, i2, 0);
            this.p = obtainStyledAttributes.getBoolean(3, true);
            this.o = obtainStyledAttributes.getInt(0, 0);
            this.f.setStyle(Paint.Style.FILL);
            this.f.setColor(obtainStyledAttributes.getColor(4, a2));
            this.g.setStyle(Paint.Style.STROKE);
            this.g.setColor(obtainStyledAttributes.getColor(7, a4));
            this.g.setStrokeWidth(obtainStyledAttributes.getDimension(8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            this.h.setStyle(Paint.Style.FILL);
            this.h.setColor(obtainStyledAttributes.getColor(2, a3));
            this.e = obtainStyledAttributes.getDimension(5, dimension);
            this.q = obtainStyledAttributes.getBoolean(6, false);
            this.r = obtainStyledAttributes2.getDimension(1, dimension2);
            Drawable drawable = obtainStyledAttributes.getDrawable(1);
            if (drawable != null) {
                setBackground(drawable);
            }
            obtainStyledAttributes.recycle();
            obtainStyledAttributes2.recycle();
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            kd4.a((Object) viewConfiguration, "configuration");
            this.s = viewConfiguration.getScaledPagingTouchSlop();
        }
    }

    @DexIgnore
    public void a(int i2, float f2, int i3) {
        this.l = i2;
        invalidate();
        ViewPager.i iVar = this.k;
        if (iVar != null) {
            iVar.a(i2, f2, i3);
        }
    }

    @DexIgnore
    public void a(int i2) {
        int i3;
        if (this.q || this.n == 0) {
            if (kl2.a(getContext())) {
                ViewPager viewPager = this.j;
                wi adapter = viewPager != null ? viewPager.getAdapter() : null;
                if (adapter != null) {
                    kd4.a((Object) adapter, "mViewPager?.adapter!!");
                    i3 = (adapter.a() - i2) - 1;
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                i3 = i2;
            }
            this.l = i3;
            this.m = i2;
            invalidate();
        }
        ViewPager.i iVar = this.k;
        if (iVar != null) {
            iVar.a(i2);
        }
    }
}
