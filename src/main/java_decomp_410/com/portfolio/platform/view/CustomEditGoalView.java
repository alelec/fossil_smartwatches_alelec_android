package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ll2;
import com.fossil.blesdk.obfuscated.ys3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.enums.GoalType;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomEditGoalView extends ConstraintLayout {
    @DexIgnore
    public AppCompatImageView u;
    @DexIgnore
    public AutoResizeTextView v;
    @DexIgnore
    public FlexibleTextView w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public GoalType z;

    @DexIgnore
    public CustomEditGoalView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        View view = null;
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        view = layoutInflater != null ? layoutInflater.inflate(R.layout.view_custom_edit_goal, this, true) : view;
        if (view != null) {
            View findViewById = view.findViewById(R.id.aciv_icon);
            kd4.a((Object) findViewById, "view.findViewById(R.id.aciv_icon)");
            this.u = (AppCompatImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.tv_value);
            kd4.a((Object) findViewById2, "view.findViewById(R.id.tv_value)");
            this.v = (AutoResizeTextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.ftv_goal_type);
            kd4.a((Object) findViewById3, "view.findViewById(R.id.ftv_goal_type)");
            this.w = (FlexibleTextView) findViewById3;
            this.x = k6.a(view.getContext(), (int) R.color.disabledCalendarDay);
            int i = this.x;
            a(-1, i, i);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    public final void a(int i, int i2, int i3) {
        this.u.setBackground(c(i));
        this.v.setTextColor(i2);
        this.w.setTextColor(i3);
    }

    @DexIgnore
    public final Drawable c(int i) {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(1);
        gradientDrawable.setColor(i);
        return gradientDrawable;
    }

    @DexIgnore
    public final GoalType getMGoalType() {
        return this.z;
    }

    @DexIgnore
    public final int getMValue() {
        return this.y;
    }

    @DexIgnore
    public final int getValue() {
        return this.y;
    }

    @DexIgnore
    public final void setMGoalType(GoalType goalType) {
        this.z = goalType;
    }

    @DexIgnore
    public final void setMValue(int i) {
        this.y = i;
    }

    @DexIgnore
    public void setSelected(boolean z2) {
        if (z2) {
            int i = this.x;
            a(i, i, k6.a(getContext(), (int) R.color.black));
        } else {
            int a = k6.a(getContext(), (int) R.color.gray);
            a(-1, a, a);
        }
        super.setSelected(z2);
    }

    @DexIgnore
    public final void setType(GoalType goalType) {
        kd4.b(goalType, "type");
        this.z = goalType;
        int i = ys3.a[goalType.ordinal()];
        if (i == 1) {
            this.u.setImageDrawable(getContext().getDrawable(R.drawable.ic_customize_calories));
            this.x = k6.a(getContext(), (int) R.color.dianaActiveCaloriesTab);
            this.w.setText(getContext().getString(R.string.DashboardDiana_Main_ActiveCaloriesToday_Title__ActiveCalories));
        } else if (i == 2) {
            this.u.setImageDrawable(getContext().getDrawable(R.drawable.ic_customize_stopwatch));
            this.x = k6.a(getContext(), (int) R.color.dianaActiveMinutesTab);
            this.w.setText(getContext().getString(R.string.DashboardDiana_Main_ActiveMinutesToday_Title__ActiveMinutes));
        } else if (i == 3) {
            this.u.setImageDrawable(getContext().getDrawable(R.drawable.ic_customize_steps));
            this.x = k6.a(getContext(), (int) R.color.dianaStepsTab);
            this.w.setText(getContext().getString(R.string.Profile_MyProfileDiana_SetGoalsSteps_Label__Steps));
        } else if (i == 4) {
            this.u.setImageDrawable(getContext().getDrawable(R.drawable.ic_tab_sleep));
            this.x = k6.a(getContext(), (int) R.color.dianaSleepTab);
            this.w.setText(getContext().getString(R.string.DashboardHybrid_Main_SleepToday_Title__Sleep));
        } else if (i == 5) {
            this.u.setImageDrawable(getContext().getDrawable(R.drawable.ic_goal));
            this.x = k6.a(getContext(), (int) R.color.hybridGoalTrackingTab);
            this.w.setText(getContext().getString(R.string.Customization_Buttons_HybridWatchAppsList_List__GoalTracking));
        }
    }

    @DexIgnore
    public final void setValue(int i) {
        this.y = i;
        GoalType goalType = this.z;
        if (goalType != null) {
            int i2 = ys3.b[goalType.ordinal()];
            if (i2 == 1 || i2 == 2 || i2 == 3 || i2 == 4) {
                this.v.setText(ll2.b.c(this.y));
            } else if (i2 == 5) {
                this.v.setText(ll2.b.d(this.y));
            }
        }
    }
}
