package com.portfolio.platform.view.chart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeekHeartRateChart$findMidValue$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<java.lang.Integer, java.lang.Boolean> {
    @DexIgnore
    public static /* final */ com.portfolio.platform.view.chart.WeekHeartRateChart$findMidValue$1 INSTANCE; // = new com.portfolio.platform.view.chart.WeekHeartRateChart$findMidValue$1();

    @DexIgnore
    public WeekHeartRateChart$findMidValue$1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke(((java.lang.Number) obj).intValue()));
    }

    @DexIgnore
    public final boolean invoke(int i) {
        return i > 0;
    }
}
