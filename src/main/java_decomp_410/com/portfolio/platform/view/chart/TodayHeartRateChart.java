package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.r6;
import com.fossil.blesdk.obfuscated.rt3;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.ts3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BaseChart;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TodayHeartRateChart extends BaseChart {
    @DexIgnore
    public Integer A;
    @DexIgnore
    public float B;
    @DexIgnore
    public float C;
    @DexIgnore
    public float D;
    @DexIgnore
    public float E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public short M;
    @DexIgnore
    public short N;
    @DexIgnore
    public Paint O;
    @DexIgnore
    public Paint P;
    @DexIgnore
    public Paint Q;
    @DexIgnore
    public Paint R;
    @DexIgnore
    public List<rt3> S;
    @DexIgnore
    public float T;
    @DexIgnore
    public List<Triple<Integer, Pair<Integer, Float>, String>> U; // = new ArrayList();
    @DexIgnore
    public int V;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public Integer y;
    @DexIgnore
    public float z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TodayHeartRateChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        kd4.b(context, "context");
        kd4.b(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.TodayHeartRateChart);
        this.u = obtainStyledAttributes.getColor(0, -3355444);
        this.v = obtainStyledAttributes.getColor(4, k6.a(context, (int) R.color.steps));
        this.w = obtainStyledAttributes.getColor(3, k6.a(context, (int) R.color.heartRate));
        this.x = obtainStyledAttributes.getColor(5, -3355444);
        this.y = Integer.valueOf(obtainStyledAttributes.getResourceId(2, -1));
        this.z = obtainStyledAttributes.getDimension(1, ts3.c(13.0f));
        this.A = Integer.valueOf(obtainStyledAttributes.getResourceId(7, -1));
        this.B = obtainStyledAttributes.getDimension(6, ts3.c(13.0f));
        obtainStyledAttributes.recycle();
        this.O = new Paint();
        this.O.setColor(this.u);
        this.O.setStrokeWidth(2.0f);
        this.O.setStyle(Paint.Style.STROKE);
        this.P = new Paint(1);
        this.P.setColor(this.x);
        this.P.setStyle(Paint.Style.FILL);
        this.P.setTextSize(this.z);
        Integer num = this.y;
        if (num == null || num.intValue() != -1) {
            Paint paint = this.P;
            Integer num2 = this.y;
            if (num2 != null) {
                paint.setTypeface(r6.a(context, num2.intValue()));
            } else {
                kd4.a();
                throw null;
            }
        }
        this.Q = new Paint(1);
        this.Q.setColor(this.x);
        this.Q.setStyle(Paint.Style.FILL);
        this.Q.setTextSize(this.B);
        Integer num3 = this.A;
        if (num3 == null || num3.intValue() != -1) {
            Paint paint2 = this.Q;
            Integer num4 = this.A;
            if (num4 != null) {
                paint2.setTypeface(r6.a(context, num4.intValue()));
            } else {
                kd4.a();
                throw null;
            }
        }
        this.R = new Paint(1);
        this.R.setStrokeWidth(ts3.a(2.0f));
        this.R.setStyle(Paint.Style.STROKE);
        Rect rect = new Rect();
        this.P.getTextBounds("222", 0, 3, rect);
        this.J = (float) rect.width();
        this.K = (float) rect.height();
        Rect rect2 = new Rect();
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string._12am);
        this.Q.getTextBounds(a2, 0, a2.length(), rect2);
        this.L = (float) rect2.height();
        this.S = new ArrayList();
        d();
        a();
    }

    @DexIgnore
    public final void a(List<rt3> list) {
        kd4.b(list, "heartRatePointList");
        this.S.clear();
        this.S.addAll(list);
        d();
        getMGraph().invalidate();
    }

    @DexIgnore
    public void b(Canvas canvas) {
        kd4.b(canvas, "canvas");
        super.b(canvas);
        this.C = ((float) canvas.getHeight()) - (this.L * ((float) 2));
        this.D = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.E = (float) canvas.getWidth();
        this.F = this.B;
        this.G = this.D + ts3.a(5.0f);
        this.I = this.C - (this.B * ((float) 6));
        this.H = this.E - (this.J * 2.0f);
        float f = this.H - this.G;
        int i = this.V;
        if (i == 0) {
            i = DateTimeConstants.MINUTES_PER_DAY;
        }
        this.T = f / ((float) i);
        f(canvas);
    }

    @DexIgnore
    public final void d() {
        Object obj;
        List<rt3> list = this.S;
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (true) {
            boolean z2 = true;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (((rt3) next).d() <= 0) {
                z2 = false;
            }
            if (z2) {
                arrayList.add(next);
            }
        }
        Iterator it2 = arrayList.iterator();
        Object obj2 = null;
        if (!it2.hasNext()) {
            obj = null;
        } else {
            obj = it2.next();
            if (it2.hasNext()) {
                int d = ((rt3) obj).d();
                do {
                    Object next2 = it2.next();
                    int d2 = ((rt3) next2).d();
                    if (d > d2) {
                        obj = next2;
                        d = d2;
                    }
                } while (it2.hasNext());
            }
        }
        rt3 rt3 = (rt3) obj;
        this.M = rt3 != null ? (short) rt3.d() : 0;
        List<rt3> list2 = this.S;
        ArrayList arrayList2 = new ArrayList();
        for (T next3 : list2) {
            if (((rt3) next3).b() > 0) {
                arrayList2.add(next3);
            }
        }
        Iterator it3 = arrayList2.iterator();
        if (it3.hasNext()) {
            obj2 = it3.next();
            if (it3.hasNext()) {
                int b = ((rt3) obj2).b();
                do {
                    Object next4 = it3.next();
                    int b2 = ((rt3) next4).b();
                    if (b < b2) {
                        obj2 = next4;
                        b = b2;
                    }
                } while (it3.hasNext());
            }
        }
        rt3 rt32 = (rt3) obj2;
        this.N = rt32 != null ? (short) rt32.b() : 0;
        if (this.N == ((short) 0)) {
            this.N = 100;
        }
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        canvas.drawLine(this.D, this.I, (float) canvas.getWidth(), this.I, this.O);
        canvas.drawLine(this.D, this.F, (float) canvas.getWidth(), this.F, this.O);
        float f = (float) 2;
        canvas.drawText(String.valueOf(this.M), this.H + (((((float) canvas.getWidth()) - this.H) - this.P.measureText(String.valueOf(this.M))) / f) + ts3.a(2.0f), this.I + (this.K * 1.5f), this.P);
        canvas.drawText(String.valueOf(this.N), this.H + (((((float) canvas.getWidth()) - this.H) - this.P.measureText(String.valueOf(this.N))) / f) + ts3.a(2.0f), this.F + (this.K * 1.5f), this.P);
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        e(canvas);
        g(canvas);
    }

    @DexIgnore
    public final void g(Canvas canvas) {
        int i;
        ArrayList<Path> arrayList = new ArrayList<>();
        while (true) {
            Path path = null;
            do {
                boolean z2 = true;
                i = 0;
                for (rt3 rt3 : this.S) {
                    float c = this.G + (((float) rt3.c()) * this.T);
                    float f = this.F;
                    short s = this.N;
                    float d = f + (((this.I - f) / ((float) (s - this.M))) * ((float) (s - rt3.d())));
                    if (d <= this.I) {
                        if (z2) {
                            path = new Path();
                            path.moveTo(c, d);
                            z2 = false;
                        } else if (path != null) {
                            path.lineTo(c, d);
                        } else {
                            kd4.a();
                            throw null;
                        }
                        i++;
                    }
                }
                if (path != null) {
                    arrayList.add(path);
                }
                Paint paint = this.R;
                float f2 = this.G;
                paint.setShader(new LinearGradient(f2, this.F, f2, this.I, this.w, this.v, Shader.TileMode.MIRROR));
                for (Path drawPath : arrayList) {
                    canvas.drawPath(drawPath, this.R);
                }
                return;
            } while (path == null);
            if (i > 1) {
                arrayList.add(path);
            }
        }
    }

    @DexIgnore
    public final int getDayInMinuteWithTimeZone() {
        return this.V;
    }

    @DexIgnore
    public final List<Triple<Integer, Pair<Integer, Float>, String>> getListTimeZoneChange() {
        return this.U;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        getMLegend().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), (i2 - getMLegendHeight()) - getMBottomPadding());
    }

    @DexIgnore
    public final void setDayInMinuteWithTimeZone(int i) {
        this.V = i;
    }

    @DexIgnore
    public final void setListTimeZoneChange(List<Triple<Integer, Pair<Integer, Float>, String>> list) {
        kd4.b(list, "value");
        this.U = list;
        getMLegend().invalidate();
    }

    @DexIgnore
    public final void a(Canvas canvas, List<String> list, List<Float> list2, float f) {
        int i = 0;
        for (T next : list) {
            int i2 = i + 1;
            if (i >= 0) {
                canvas.drawText((String) next, list2.get(i).floatValue(), f, this.Q);
                i = i2;
            } else {
                cb4.c();
                throw null;
            }
        }
    }

    @DexIgnore
    public void d(Canvas canvas) {
        kd4.b(canvas, "canvas");
        super.d(canvas);
        if (this.U.isEmpty()) {
            List d = cb4.d(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Steps_DetailPageNoActivity_Label__12a), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Steps_DetailPageNoActivity_Label__6a), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Steps_DetailPageNoActivity_Label__12p), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Steps_DetailPageNoActivity_Label__6p), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Steps_DetailPageNoActivity_Label__12a));
            ArrayList arrayList = new ArrayList();
            int size = d.size();
            float f = (this.H - this.G) / ((float) (size - 1));
            for (int i = 0; i < size; i++) {
                arrayList.add(Float.valueOf(this.G + (((float) i) * f)));
            }
            a(canvas, d, arrayList, this.C + (((((float) canvas.getHeight()) - this.C) + this.L) / ((float) 2)));
            return;
        }
        List<Triple<Integer, Pair<Integer, Float>, String>> list = this.U;
        ArrayList arrayList2 = new ArrayList(db4.a(list, 10));
        for (Triple third : list) {
            arrayList2.add((String) third.getThird());
        }
        List d2 = kb4.d(arrayList2);
        List<Triple<Integer, Pair<Integer, Float>, String>> list2 = this.U;
        ArrayList arrayList3 = new ArrayList(db4.a(list2, 10));
        for (Triple first : list2) {
            arrayList3.add(Integer.valueOf(((Number) first.getFirst()).intValue()));
        }
        List<Number> d3 = kb4.d(arrayList3);
        ArrayList arrayList4 = new ArrayList();
        for (Number intValue : d3) {
            arrayList4.add(Float.valueOf(this.G + (((float) intValue.intValue()) * this.T)));
        }
        a(canvas, d2, arrayList4, this.C + (((((float) canvas.getHeight()) - this.C) + this.L) / ((float) 2)));
    }
}
