package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.r6;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.ts3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BaseChart;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.sequences.SequencesKt___SequencesKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeekHeartRateChart extends BaseChart {
    @DexIgnore
    public Integer A;
    @DexIgnore
    public float B;
    @DexIgnore
    public int C;
    @DexIgnore
    public float D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public float O;
    @DexIgnore
    public float P;
    @DexIgnore
    public float Q;
    @DexIgnore
    public short R;
    @DexIgnore
    public short S;
    @DexIgnore
    public Paint T;
    @DexIgnore
    public Paint U;
    @DexIgnore
    public Paint V;
    @DexIgnore
    public Paint W;
    @DexIgnore
    public Paint a0;
    @DexIgnore
    public Paint b0;
    @DexIgnore
    public int c0;
    @DexIgnore
    public List<Integer> d0;
    @DexIgnore
    public List<PointF> e0;
    @DexIgnore
    public int f0; // = 1;
    @DexIgnore
    public List<String> g0; // = new ArrayList();
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public Integer w;
    @DexIgnore
    public float x;
    @DexIgnore
    public int y;
    @DexIgnore
    public Integer z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeekHeartRateChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        kd4.b(context, "context");
        kd4.b(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.WeekHeartRateChart);
        this.u = obtainStyledAttributes.getColor(0, k6.a(context, (int) R.color.heartRate));
        this.v = obtainStyledAttributes.getColor(2, k6.a(context, (int) R.color.heartRate));
        this.w = Integer.valueOf(obtainStyledAttributes.getResourceId(3, -1));
        this.x = obtainStyledAttributes.getDimension(1, ts3.c(13.0f));
        this.y = obtainStyledAttributes.getColor(8, -1);
        this.z = Integer.valueOf(obtainStyledAttributes.getResourceId(9, -1));
        this.A = Integer.valueOf(obtainStyledAttributes.getResourceId(12, -1));
        this.B = obtainStyledAttributes.getDimension(10, ts3.c(13.0f));
        this.C = obtainStyledAttributes.getColor(11, -3355444);
        this.D = obtainStyledAttributes.getDimension(6, ts3.a(13.0f));
        this.E = obtainStyledAttributes.getColor(7, k6.a(context, (int) R.color.heartRate));
        this.F = obtainStyledAttributes.getColor(4, k6.a(context, (int) R.color.aboveHeartRate));
        this.G = obtainStyledAttributes.getColor(5, k6.a(context, (int) R.color.outlineCircleHeartRate));
        obtainStyledAttributes.recycle();
        this.T = new Paint();
        this.T.setColor(this.u);
        this.T.setStrokeWidth(2.0f);
        this.T.setStyle(Paint.Style.STROKE);
        this.U = new Paint(1);
        this.U.setColor(this.v);
        this.U.setStyle(Paint.Style.FILL);
        this.U.setTextSize(this.x);
        Integer num = this.w;
        if (num == null || num.intValue() != -1) {
            Paint paint = this.U;
            Integer num2 = this.w;
            if (num2 != null) {
                paint.setTypeface(r6.a(context, num2.intValue()));
            } else {
                kd4.a();
                throw null;
            }
        }
        this.V = new Paint(1);
        this.V.setColor(this.y);
        this.V.setTextSize(this.D);
        this.V.setStyle(Paint.Style.FILL);
        Integer num3 = this.z;
        if (num3 == null || num3.intValue() != -1) {
            Paint paint2 = this.V;
            Integer num4 = this.z;
            if (num4 != null) {
                paint2.setTypeface(r6.a(context, num4.intValue()));
            } else {
                kd4.a();
                throw null;
            }
        }
        this.W = new Paint(1);
        this.W.setColor(this.C);
        this.W.setStyle(Paint.Style.FILL);
        this.W.setTextSize(this.B);
        Integer num5 = this.A;
        if (num5 == null || num5.intValue() != -1) {
            Paint paint3 = this.W;
            Integer num6 = this.A;
            if (num6 != null) {
                paint3.setTypeface(r6.a(context, num6.intValue()));
            } else {
                kd4.a();
                throw null;
            }
        }
        this.a0 = new Paint(1);
        this.a0.setColor(this.E);
        this.a0.setStyle(Paint.Style.FILL);
        this.b0 = new Paint(1);
        this.b0.setColor(this.G);
        this.b0.setStyle(Paint.Style.STROKE);
        this.b0.setStrokeWidth(ts3.a(2.0f));
        Rect rect = new Rect();
        this.U.getTextBounds("222", 0, 3, rect);
        this.O = (float) rect.width();
        this.P = (float) rect.height();
        Rect rect2 = new Rect();
        String a = sm2.a((Context) PortfolioApp.W.c(), (int) R.string._12am);
        this.W.getTextBounds(a, 0, a.length(), rect2);
        this.Q = (float) rect2.height();
        ArrayList arrayList = new ArrayList(7);
        for (int i = 0; i < 7; i++) {
            arrayList.add(0);
        }
        this.d0 = arrayList;
        this.e0 = new ArrayList();
        d();
        a();
    }

    @DexIgnore
    public final void a(List<Integer> list) {
        kd4.b(list, "heartRatePointList");
        this.d0.clear();
        this.e0.clear();
        List<Integer> list2 = this.d0;
        ArrayList arrayList = new ArrayList(db4.a(list, 10));
        Iterator<T> it = list.iterator();
        while (true) {
            int i = 0;
            if (!it.hasNext()) {
                break;
            }
            Integer num = (Integer) it.next();
            if (num != null) {
                i = num.intValue();
            }
            arrayList.add(Integer.valueOf(i));
        }
        list2.addAll(arrayList);
        d();
        int size = this.d0.size();
        if (size < 7) {
            int i2 = 7 - size;
            ArrayList arrayList2 = new ArrayList(i2);
            for (int i3 = 0; i3 < i2; i3++) {
                arrayList2.add(0);
            }
            this.d0.addAll(arrayList2);
        }
        getMGraph().invalidate();
    }

    @DexIgnore
    public void b(Canvas canvas) {
        kd4.b(canvas, "canvas");
        super.b(canvas);
        this.H = ((float) canvas.getHeight()) - (this.Q * ((float) 2));
        this.I = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.J = ((float) canvas.getWidth()) - (this.O / ((float) 3));
        float f = (float) 6;
        this.K = this.B + this.D + (this.Q * f);
        float a = this.I + ts3.a(10.0f);
        float f2 = this.D;
        this.L = a + f2;
        this.N = (this.H - f2) - (this.B * f);
        this.M = (this.J - (this.O * 1.5f)) - f2;
        f(canvas);
    }

    @DexIgnore
    public final void d() {
        int i;
        short s;
        Integer num = (Integer) SequencesKt___SequencesKt.e(SequencesKt___SequencesKt.a(kb4.b(this.d0), WeekHeartRateChart$findMidValue$Anon1.INSTANCE));
        int i2 = 0;
        this.R = num != null ? (short) num.intValue() : 0;
        Integer num2 = (Integer) SequencesKt___SequencesKt.d(SequencesKt___SequencesKt.a(kb4.b(this.d0), WeekHeartRateChart$findMidValue$Anon2.INSTANCE));
        this.S = num2 != null ? (short) num2.intValue() : 0;
        if (this.S == ((short) 0)) {
            this.S = 100;
        }
        int i3 = 0;
        for (Number intValue : this.d0) {
            int intValue2 = intValue.intValue();
            if (intValue2 > 0) {
                i3 += intValue2;
                i2++;
            }
        }
        if (i2 > 0) {
            i = i3 / i2;
        } else {
            i = (this.S - this.R) / 2;
        }
        this.c0 = i;
        if (Math.abs(this.S - this.c0) >= Math.abs(this.R - this.c0)) {
            s = this.S;
        } else {
            s = this.R;
        }
        int abs = Math.abs(s);
        int i4 = this.c0;
        this.f0 = abs != i4 ? Math.abs(s - i4) * 2 : 1;
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        float f;
        short s = this.S;
        int i = this.c0;
        if (s != ((short) i)) {
            float f2 = this.K;
            f = f2 + (((this.N - f2) / ((float) this.f0)) * ((float) (s - i)));
        } else {
            float f3 = this.K;
            f = f3 + ((this.N - f3) / 2.0f);
        }
        canvas.drawLine(this.I, f, (float) canvas.getWidth(), f, this.T);
        float measureText = this.J - this.U.measureText(String.valueOf(this.c0));
        float f4 = f + (this.P * 1.5f);
        canvas.drawText(String.valueOf(this.c0), measureText, f4, this.U);
        String a = sm2.a(getContext(), (int) R.string.Dashboard_Heart_Rate__Main_page_HeartRate_Today_avg);
        canvas.drawText(a, this.J - this.U.measureText(a.toString()), f4 + this.P, this.U);
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        e(canvas);
        a(canvas, (this.M - this.L) / ((float) 6));
    }

    @DexIgnore
    public final List<String> getListWeekDays() {
        return this.g0;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        setMWidth(i);
        setMHeight(i2);
        setMLeftPadding(getPaddingLeft());
        setMTopPadding(getPaddingTop());
        setMRightPadding(getPaddingRight());
        setMBottomPadding(getPaddingBottom());
        getMGraph().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
        getMLegend().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
        getMBackground().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
        getMGraphOverlay().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
    }

    @DexIgnore
    public final void setListWeekDays(List<String> list) {
        kd4.b(list, "value");
        this.g0.clear();
        this.g0.addAll(list);
        getMLegend().invalidate();
    }

    @DexIgnore
    public final void a(Canvas canvas, float f) {
        Rect rect = new Rect();
        int i = 0;
        for (T next : this.d0) {
            int i2 = i + 1;
            if (i >= 0) {
                int intValue = ((Number) next).intValue();
                float f2 = this.L + (((float) i) * f);
                float f3 = this.K;
                float f4 = this.N;
                float f5 = (f4 - f3) / ((float) this.f0);
                short s = this.S;
                float f6 = f3 + (f5 * ((float) (s - intValue)));
                if (this.R == s) {
                    if (intValue > 0) {
                        f6 = f3 + ((f4 - f3) / 2.0f);
                        a(canvas, f2, f6, intValue, this.E, rect);
                    }
                } else if (intValue > 0) {
                    a(canvas, f2, f6, intValue, intValue > this.c0 ? this.F : this.E, rect);
                }
                this.e0.add(new PointF(f2, f6));
                i = i2;
            } else {
                cb4.c();
                throw null;
            }
        }
    }

    @DexIgnore
    public void d(Canvas canvas) {
        kd4.b(canvas, "canvas");
        super.d(canvas);
        if (this.g0.isEmpty()) {
            this.g0.addAll(cb4.d(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__S), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__M), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__T), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__W), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__T_1), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__F), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__S_1)));
        }
        a(canvas, this.g0, (this.M - this.L) / ((float) 6), this.H + (((((float) canvas.getHeight()) - this.H) + this.Q) / ((float) 2)));
    }

    @DexIgnore
    public final void a(Canvas canvas, float f, float f2, int i, int i2, Rect rect) {
        this.a0.setColor(i2);
        canvas.drawCircle(f, f2, this.D, this.a0);
        canvas.drawCircle(f, f2, this.D + ts3.a(3.0f), this.b0);
        String valueOf = String.valueOf(i);
        this.W.getTextBounds(valueOf, 0, valueOf.length(), rect);
        canvas.drawText(valueOf, f - (this.V.measureText(valueOf) / ((float) 2)), f2 + ((float) (rect.height() / 2)), this.V);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<String> list, float f, float f2) {
        int i = 0;
        for (T next : list) {
            int i2 = i + 1;
            if (i >= 0) {
                String str = (String) next;
                canvas.drawText(str, (this.L + (((float) i) * f)) - (this.U.measureText(str) / ((float) 2)), f2, this.W);
                i = i2;
            } else {
                cb4.c();
                throw null;
            }
        }
    }
}
