package com.portfolio.platform.view.recyclerview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RecyclerViewHeartRateCalendar$init$1 extends androidx.recyclerview.widget.GridLayoutManager {

    @DexIgnore
    /* renamed from: P */
    public /* final */ /* synthetic */ android.content.Context f25602P;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewHeartRateCalendar$init$1(android.content.Context context, android.content.Context context2, int i, int i2, boolean z) {
        super(context2, i, i2, z);
        this.f25602P = context;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo2426a() {
        return false;
    }

    @DexIgnore
    /* renamed from: k */
    public int mo2458k(androidx.recyclerview.widget.RecyclerView.State state) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(state, "state");
        return com.fossil.blesdk.obfuscated.as3.m19960a(this.f25602P);
    }
}
