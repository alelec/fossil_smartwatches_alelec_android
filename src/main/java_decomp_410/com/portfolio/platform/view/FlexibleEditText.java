package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatEditText;
import com.fossil.blesdk.obfuscated.ft3;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rs3;
import com.fossil.blesdk.obfuscated.sm2;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class FlexibleEditText extends AppCompatEditText {
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i; // = FlexibleTextView.l.a();
    @DexIgnore
    public boolean j; // = true;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleEditText(Context context) {
        super(context);
        kd4.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        this.g = 0;
        CharSequence text = getText();
        if (text != null) {
            CharSequence hint = getHint();
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, h62.FlexibleEditText);
                this.g = obtainStyledAttributes.getInt(2, 0);
                this.h = obtainStyledAttributes.getInt(1, 0);
                this.i = obtainStyledAttributes.getColor(3, FlexibleTextView.l.a());
                this.j = obtainStyledAttributes.getBoolean(0, true);
                obtainStyledAttributes.recycle();
                TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(attributeSet, new int[]{16843087, 16843088}, 0, 0);
                int resourceId = obtainStyledAttributes2.getResourceId(0, -1);
                if (resourceId != -1) {
                    text = a(resourceId);
                }
                int resourceId2 = obtainStyledAttributes2.getResourceId(1, -1);
                if (resourceId2 != -1) {
                    hint = a(resourceId2);
                }
                obtainStyledAttributes2.recycle();
            }
            if (!TextUtils.isEmpty(text)) {
                setText(text);
            }
            if (!TextUtils.isEmpty(hint)) {
                kd4.a((Object) hint, "hint");
                setHint(a(hint, this.h));
            }
            if (this.i != FlexibleTextView.l.a()) {
                ft3.a((TextView) this, this.i);
                return;
            }
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public boolean onKeyPreIme(int i2, KeyEvent keyEvent) {
        kd4.b(keyEvent, Constants.EVENT);
        if (keyEvent.getKeyCode() != 4) {
            return false;
        }
        clearFocus();
        return false;
    }

    @DexIgnore
    public void onSelectionChanged(int i2, int i3) {
        if (!this.j) {
            Editable text = getText();
            if (!(text == null || (i2 == text.length() && i3 == text.length()))) {
                setSelection(text.length(), text.length());
                return;
            }
        }
        super.onSelectionChanged(i2, i3);
    }

    @DexIgnore
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        kd4.b(bufferType, "type");
        if (charSequence == null) {
            charSequence = "";
        }
        super.setText(a(charSequence), bufferType);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        kd4.b(context, "context");
        kd4.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleEditText(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        kd4.b(context, "context");
        kd4.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    public final CharSequence a(CharSequence charSequence) {
        return a(charSequence, this.g);
    }

    @DexIgnore
    public final CharSequence a(CharSequence charSequence, int i2) {
        if (i2 == 1) {
            return rs3.a(charSequence);
        }
        if (i2 == 2) {
            return rs3.b(charSequence);
        }
        if (i2 == 3) {
            return rs3.d(charSequence);
        }
        if (i2 == 4) {
            return rs3.e(charSequence);
        }
        if (i2 != 5) {
            return charSequence;
        }
        return rs3.c(charSequence);
    }

    @DexIgnore
    public final String a(int i2) {
        String a = sm2.a((Context) PortfolioApp.W.c(), i2);
        kd4.a((Object) a, "LanguageHelper.getString\u2026ioApp.instance, stringId)");
        return a;
    }
}
