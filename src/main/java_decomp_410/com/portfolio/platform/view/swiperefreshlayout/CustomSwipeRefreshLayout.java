package com.portfolio.platform.view.swiperefreshlayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import com.facebook.internal.NativeProtocol;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hu3;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomSwipeRefreshLayout extends ViewGroup {
    @DexIgnore
    public static /* final */ String E;
    @DexIgnore
    public /* final */ k A;
    @DexIgnore
    public /* final */ Runnable B;
    @DexIgnore
    public /* final */ Runnable C;
    @DexIgnore
    public /* final */ g D;
    @DexIgnore
    public DecelerateInterpolator e;
    @DexIgnore
    public /* final */ f f;
    @DexIgnore
    public e g;
    @DexIgnore
    public e h;
    @DexIgnore
    public View i;
    @DexIgnore
    public View j;
    @DexIgnore
    public MotionEvent k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;
    @DexIgnore
    public float o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public d w;
    @DexIgnore
    public /* final */ j x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public /* final */ Runnable z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Animation.AnimationListener {
        @DexIgnore
        public a(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
            kd4.b(animation, "animation");
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            kd4.b(animation, "animation");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(e eVar, e eVar2);
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();

        @DexIgnore
        void a(boolean z);

        @DexIgnore
        void b();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public int a;
        @DexIgnore
        public float b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            new a((fd4) null);
        }
        */

        @DexIgnore
        public e(int i) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public String toString() {
            return "[refreshState = " + this.a + ", percent = " + this.b + ", top = " + this.c + ", trigger = " + this.d + "]";
        }

        @DexIgnore
        public final void a(int i, int i2, int i3) {
            this.a = i;
            this.c = i2;
            this.d = i3;
            this.b = ((float) i2) / ((float) i3);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends Animation {
        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            kd4.b(transformation, "t");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends Animation {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout e;

        @DexIgnore
        public g(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            this.e = customSwipeRefreshLayout;
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            kd4.b(transformation, "t");
            int mTargetOriginalTop$app_fossilRelease = this.e.getMTargetOriginalTop$app_fossilRelease();
            if (this.e.getMFrom$app_fossilRelease() != this.e.getMTargetOriginalTop$app_fossilRelease()) {
                mTargetOriginalTop$app_fossilRelease = this.e.getMFrom$app_fossilRelease() + ((int) (((float) (this.e.getMTargetOriginalTop$app_fossilRelease() - this.e.getMFrom$app_fossilRelease())) * f));
            }
            View mTarget$app_fossilRelease = this.e.getMTarget$app_fossilRelease();
            if (mTarget$app_fossilRelease != null) {
                int top = mTargetOriginalTop$app_fossilRelease - mTarget$app_fossilRelease.getTop();
                View mTarget$app_fossilRelease2 = this.e.getMTarget$app_fossilRelease();
                if (mTarget$app_fossilRelease2 != null) {
                    int top2 = mTarget$app_fossilRelease2.getTop();
                    if (top + top2 < 0) {
                        top = 0 - top2;
                    }
                    this.e.a(top, true);
                    return;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout e;

        @DexIgnore
        public h(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            this.e = customSwipeRefreshLayout;
        }

        @DexIgnore
        public final void run() {
            this.e.setMInReturningAnimation$app_fossilRelease(true);
            CustomSwipeRefreshLayout customSwipeRefreshLayout = this.e;
            View mTarget$app_fossilRelease = customSwipeRefreshLayout.getMTarget$app_fossilRelease();
            if (mTarget$app_fossilRelease != null) {
                customSwipeRefreshLayout.a(mTarget$app_fossilRelease.getTop(), (Animation.AnimationListener) this.e.x);
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout e;

        @DexIgnore
        public i(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            this.e = customSwipeRefreshLayout;
        }

        @DexIgnore
        public final void run() {
            this.e.setMInReturningAnimation$app_fossilRelease(true);
            CustomSwipeRefreshLayout customSwipeRefreshLayout = this.e;
            View mTarget$app_fossilRelease = customSwipeRefreshLayout.getMTarget$app_fossilRelease();
            if (mTarget$app_fossilRelease != null) {
                customSwipeRefreshLayout.a(mTarget$app_fossilRelease.getTop(), (Animation.AnimationListener) this.e.x);
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends a {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            super(customSwipeRefreshLayout);
            this.a = customSwipeRefreshLayout;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            kd4.b(animation, "animation");
            this.a.setMInReturningAnimation$app_fossilRelease(false);
            this.a.setStartSwipe$app_fossilRelease(true);
            if (this.a.getLastState$app_fossilRelease().a() == 2) {
                d mListener$app_fossilRelease = this.a.getMListener$app_fossilRelease();
                if (mListener$app_fossilRelease != null) {
                    mListener$app_fossilRelease.a(true);
                    return;
                }
                return;
            }
            d mListener$app_fossilRelease2 = this.a.getMListener$app_fossilRelease();
            if (mListener$app_fossilRelease2 != null) {
                mListener$app_fossilRelease2.a(false);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends a {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            super(customSwipeRefreshLayout);
            this.a = customSwipeRefreshLayout;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            kd4.b(animation, "animation");
            this.a.getMReturnToStartPosition$app_fossilRelease().run();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout e;

        @DexIgnore
        public l(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            this.e = customSwipeRefreshLayout;
        }

        @DexIgnore
        public final void run() {
            CustomSwipeRefreshLayout customSwipeRefreshLayout = this.e;
            customSwipeRefreshLayout.a((Animation.AnimationListener) customSwipeRefreshLayout.A);
        }
    }

    /*
    static {
        new b((fd4) null);
        String simpleName = CustomSwipeRefreshLayout.class.getSimpleName();
        kd4.a((Object) simpleName, "CustomSwipeRefreshLayout::class.java.simpleName");
        E = simpleName;
    }
    */

    @DexIgnore
    public CustomSwipeRefreshLayout(Context context) {
        this(context, (AttributeSet) null, 0, 6, (fd4) null);
    }

    @DexIgnore
    public CustomSwipeRefreshLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (fd4) null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomSwipeRefreshLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        kd4.b(context, "context");
        this.f = new f();
        this.g = new e(0);
        this.h = new e(-1);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        kd4.a((Object) viewConfiguration, "ViewConfiguration.get(context)");
        this.l = viewConfiguration.getScaledTouchSlop();
        this.n = -1;
        this.r = true;
        this.s = true;
        this.x = new j(this);
        this.z = new i(this);
        this.A = new k(this);
        this.B = new l(this);
        this.C = new h(this);
        this.D = new g(this);
        setWillNotDraw(false);
        this.e = new DecelerateInterpolator(2.0f);
        new AccelerateInterpolator(1.5f);
        d();
    }

    @DexIgnore
    private final View getContentView() {
        String str;
        View view;
        if (getChildAt(0) == this.i) {
            view = getChildAt(1);
            str = "getChildAt(1)";
        } else {
            view = getChildAt(0);
            str = "getChildAt(0)";
        }
        kd4.a((Object) view, str);
        return view;
    }

    @DexIgnore
    private final void setRefreshState(int i2) {
        this.g.a(i2, this.q, this.n);
        View view = this.i;
        if (view != null) {
            ((c) view).a(this.g, this.h);
            this.h.a(i2, this.q, this.n);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.CustomSwipeRefreshHeadLayout");
    }

    @DexIgnore
    private final void setRefreshing(boolean z2) {
        if (this.t != z2) {
            a();
            this.t = z2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = E;
            local.d(str, "isRefreshing - mRefreshing: " + this.t);
            if (this.t) {
                this.B.run();
                return;
            }
            setRefreshState(3);
            removeCallbacks(this.z);
            removeCallbacks(this.C);
            this.B.run();
        }
    }

    @DexIgnore
    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        kd4.b(view, "child");
        kd4.b(layoutParams, NativeProtocol.WEB_DIALOG_PARAMS);
        if (getChildCount() <= 1 || isInEditMode()) {
            super.addView(view, i2, layoutParams);
            return;
        }
        throw new IllegalStateException("CustomSwipeRefreshLayout can host ONLY one child content view");
    }

    @DexIgnore
    public final void c() {
        setRefreshing(false);
    }

    @DexIgnore
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        kd4.b(layoutParams, "p");
        return layoutParams instanceof ViewGroup.MarginLayoutParams;
    }

    @DexIgnore
    public final void d() {
        Context context = getContext();
        kd4.a((Object) context, "context");
        this.i = new hu3(context);
        addView(this.i, new ViewGroup.MarginLayoutParams(-1, -2));
    }

    @DexIgnore
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        kd4.b(motionEvent, Constants.EVENT);
        boolean dispatchTouchEvent = super.dispatchTouchEvent(motionEvent);
        if (motionEvent.getAction() == 0) {
            return true;
        }
        return dispatchTouchEvent;
    }

    @DexIgnore
    public final void e() {
        FLogger.INSTANCE.getLocal().d(E, "startRefresh");
        setRefreshing(true);
        setRefreshState(2);
        d dVar = this.w;
        if (dVar == null) {
            return;
        }
        if (dVar != null) {
            dVar.a();
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void f() {
        removeCallbacks(this.C);
        postDelayed(this.C, 100);
    }

    @DexIgnore
    public final void g() {
        FLogger.INSTANCE.getLocal().d(E, "updateRefreshingUI");
        setRefreshState(2);
        setRefreshing(true);
    }

    @DexIgnore
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-1, -1);
    }

    @DexIgnore
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        kd4.b(layoutParams, "p");
        return new ViewGroup.MarginLayoutParams(layoutParams);
    }

    @DexIgnore
    public final boolean getByPass() {
        return this.u;
    }

    @DexIgnore
    public final boolean getDisableSwipe() {
        return this.v;
    }

    @DexIgnore
    public final View getHeadView() {
        return this.i;
    }

    @DexIgnore
    public final e getLastState$app_fossilRelease() {
        return this.h;
    }

    @DexIgnore
    public final int getMFrom$app_fossilRelease() {
        return this.p;
    }

    @DexIgnore
    public final boolean getMInReturningAnimation$app_fossilRelease() {
        return this.y;
    }

    @DexIgnore
    public final d getMListener$app_fossilRelease() {
        return this.w;
    }

    @DexIgnore
    public final Runnable getMReturnToStartPosition$app_fossilRelease() {
        return this.z;
    }

    @DexIgnore
    public final View getMTarget$app_fossilRelease() {
        return this.j;
    }

    @DexIgnore
    public final int getMTargetOriginalTop$app_fossilRelease() {
        return this.m;
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        removeCallbacks(this.C);
        removeCallbacks(this.z);
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.z);
        removeCallbacks(this.C);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:41:? A[RETURN, SYNTHETIC] */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        kd4.b(motionEvent, "ev");
        boolean z2 = false;
        if (this.v) {
            return false;
        }
        a();
        float y2 = motionEvent.getY();
        if (!isEnabled()) {
            return false;
        }
        if (this.q == 0) {
            this.y = false;
        }
        int action = motionEvent.getAction();
        if (action == 0) {
            this.k = MotionEvent.obtain(motionEvent);
            MotionEvent motionEvent2 = this.k;
            if (motionEvent2 != null) {
                this.o = motionEvent2.getY();
                this.s = true;
            } else {
                kd4.a();
                throw null;
            }
        } else if (action == 1 || action == 2) {
            MotionEvent motionEvent3 = this.k;
            if (motionEvent3 != null) {
                if (motionEvent3 == null) {
                    kd4.a();
                    throw null;
                } else if (Math.abs(y2 - motionEvent3.getY()) < ((float) this.l)) {
                    this.o = y2;
                    return false;
                }
            }
        }
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        if (!this.y) {
            View view = this.j;
            if (view != null) {
                kd4.a((Object) obtain, Constants.EVENT);
                if (!b(view, obtain)) {
                    z2 = onTouchEvent(motionEvent);
                    return z2 ? super.onInterceptTouchEvent(motionEvent) : z2;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
        this.o = motionEvent.getY();
        if (z2) {
        }
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        if (getChildCount() != 0) {
            View view = this.i;
            if (view != null) {
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                if (layoutParams != null) {
                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                    int paddingLeft = getPaddingLeft() + marginLayoutParams.leftMargin;
                    int paddingTop = getPaddingTop() + marginLayoutParams.topMargin;
                    View view2 = this.i;
                    if (view2 != null) {
                        int measuredWidth = view2.getMeasuredWidth() + paddingLeft;
                        View view3 = this.i;
                        if (view3 != null) {
                            int measuredHeight = view3.getMeasuredHeight() + paddingTop;
                            View view4 = this.i;
                            if (view4 != null) {
                                view4.layout(paddingLeft, paddingTop, measuredWidth, measuredHeight);
                            }
                            View contentView = getContentView();
                            ViewGroup.LayoutParams layoutParams2 = contentView.getLayoutParams();
                            if (layoutParams2 != null) {
                                ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) layoutParams2;
                                int paddingLeft2 = getPaddingLeft() + marginLayoutParams2.leftMargin;
                                int paddingTop2 = this.q + getPaddingTop() + marginLayoutParams2.topMargin;
                                contentView.layout(paddingLeft2, paddingTop2, contentView.getMeasuredWidth() + paddingLeft2, contentView.getMeasuredHeight() + paddingTop2);
                                return;
                            }
                            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (getChildCount() <= 2 || isInEditMode()) {
            measureChildWithMargins(this.i, i2, 0, i3, 0);
            View contentView = getContentView();
            if (getChildCount() > 0) {
                ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
                if (layoutParams != null) {
                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                    contentView.measure(View.MeasureSpec.makeMeasureSpec((((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight()) - marginLayoutParams.leftMargin) - marginLayoutParams.rightMargin, 1073741824), View.MeasureSpec.makeMeasureSpec((((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom()) - marginLayoutParams.topMargin) - marginLayoutParams.bottomMargin, 1073741824));
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            return;
        }
        throw new IllegalStateException("CustomSwipeRefreshLayout can host one child content view.");
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        kd4.b(motionEvent, Constants.EVENT);
        boolean z2 = false;
        if (!isEnabled() || this.v) {
            return false;
        }
        int action = motionEvent.getAction();
        View view = this.j;
        if (view != null) {
            int top = view.getTop();
            this.q = top - this.m;
            if (action != 1) {
                if (action != 2) {
                    if (action == 3) {
                        MotionEvent motionEvent2 = this.k;
                        if (motionEvent2 != null) {
                            if (motionEvent2 != null) {
                                motionEvent2.recycle();
                                this.k = null;
                            } else {
                                kd4.a();
                                throw null;
                            }
                        }
                    }
                } else if (this.k != null && !this.y) {
                    float y2 = motionEvent.getY();
                    MotionEvent motionEvent3 = this.k;
                    if (motionEvent3 != null) {
                        float y3 = y2 - motionEvent3.getY();
                        boolean z3 = y2 - this.o > ((float) 0);
                        if (this.s) {
                            int i2 = this.l;
                            if (y3 > ((float) i2) || y3 < ((float) (-i2))) {
                                this.s = false;
                            }
                        }
                        if (z3 || top >= this.n || top >= this.m + 1) {
                            if (this.r) {
                                this.r = false;
                                d dVar = this.w;
                                if (dVar != null) {
                                    dVar.b();
                                }
                            }
                            int i3 = (int) ((y2 - this.o) * 0.3f);
                            if (top < this.m || b()) {
                                z2 = true;
                            }
                            a(i3, z2);
                            this.o = motionEvent.getY();
                            return true;
                        }
                        this.o = motionEvent.getY();
                        return false;
                    }
                    kd4.a();
                    throw null;
                }
                return false;
            } else if (this.t && this.h.a() == 2) {
                removeCallbacks(this.C);
                this.B.run();
                return false;
            } else if (this.q < this.n || this.u) {
                f();
                return true;
            } else {
                e();
                return true;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void requestDisallowInterceptTouchEvent(boolean z2) {
    }

    @DexIgnore
    public final void setByPass(boolean z2) {
        this.u = z2;
    }

    @DexIgnore
    public final void setDisableSwipe(boolean z2) {
        this.v = z2;
    }

    @DexIgnore
    public final void setLastState$app_fossilRelease(e eVar) {
        kd4.b(eVar, "<set-?>");
        this.h = eVar;
    }

    @DexIgnore
    public final void setMFrom$app_fossilRelease(int i2) {
        this.p = i2;
    }

    @DexIgnore
    public final void setMInReturningAnimation$app_fossilRelease(boolean z2) {
        this.y = z2;
    }

    @DexIgnore
    public final void setMListener$app_fossilRelease(d dVar) {
        this.w = dVar;
    }

    @DexIgnore
    public final void setMTarget$app_fossilRelease(View view) {
        this.j = view;
    }

    @DexIgnore
    public final void setMTargetOriginalTop$app_fossilRelease(int i2) {
        this.m = i2;
    }

    @DexIgnore
    public final void setOnRefreshListener(d dVar) {
        kd4.b(dVar, "listener");
        this.w = dVar;
    }

    @DexIgnore
    public final void setStartSwipe$app_fossilRelease(boolean z2) {
        this.r = z2;
    }

    @DexIgnore
    public final boolean b() {
        return this.t;
    }

    @DexIgnore
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        kd4.b(attributeSet, "attrs");
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    @DexIgnore
    @SuppressLint({"ObsoleteSdkInt"})
    public final boolean b(View view, MotionEvent motionEvent) {
        motionEvent.offsetLocation((float) (view.getScrollX() - view.getLeft()), (float) (view.getScrollY() - view.getTop()));
        return view.canScrollVertically(-1) || a(view, motionEvent);
    }

    @DexIgnore
    public final void a(Animation.AnimationListener animationListener) {
        this.f.reset();
        this.f.setDuration(50);
        this.f.setAnimationListener(animationListener);
        View view = this.j;
        if (view != null) {
            view.startAnimation(this.f);
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void b(int i2, boolean z2) {
        int i3;
        View view = this.j;
        if (view == null) {
            i3 = 0;
        } else if (view != null) {
            i3 = view.getTop();
        } else {
            kd4.a();
            throw null;
        }
        int i4 = this.m;
        if (i2 < i4) {
            i2 = i4;
        }
        a(i2 - i3, z2);
    }

    @DexIgnore
    public final void a(int i2, Animation.AnimationListener animationListener) {
        this.p = i2;
        this.D.reset();
        this.D.setDuration(500);
        this.D.setAnimationListener(animationListener);
        this.D.setInterpolator(this.e);
        View view = this.j;
        if (view != null) {
            view.startAnimation(this.D);
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final boolean a(View view, MotionEvent motionEvent) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = viewGroup.getChildAt(i2);
                Rect rect = new Rect();
                childAt.getHitRect(rect);
                if (rect.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                    kd4.a((Object) childAt, "child");
                    return b(childAt, motionEvent);
                }
            }
        }
        return false;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CustomSwipeRefreshLayout(Context context, AttributeSet attributeSet, int i2, int i3, fd4 fd4) {
        this(context, (i3 & 2) != 0 ? null : attributeSet, (i3 & 4) != 0 ? 0 : i2);
    }

    @DexIgnore
    public final void a(boolean z2) {
        if (z2) {
            setRefreshState(this.g.a());
            return;
        }
        View view = this.j;
        if (view == null) {
            kd4.a();
            throw null;
        } else if (view.getTop() > this.n) {
            setRefreshState(1);
        } else {
            setRefreshState(0);
        }
    }

    @DexIgnore
    public final void a() {
        if (this.j == null) {
            if (getChildCount() <= 2 || isInEditMode()) {
                this.j = getContentView();
                View view = this.j;
                if (view != null) {
                    this.m = view.getTop();
                    View view2 = this.j;
                    if (view2 != null) {
                        view2.getHeight();
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                throw new IllegalStateException("CustomSwipeRefreshLayout can host ONLY one direct child");
            }
        }
        if (this.n <= 0) {
            View view3 = this.i;
            this.n = view3 != null ? view3.getHeight() : 300;
        }
    }

    @DexIgnore
    public final void a(int i2, boolean z2) {
        if (i2 != 0) {
            View headView = getHeadView();
            if (headView != null) {
                int i3 = this.q;
                if (i3 + i2 < 0) {
                    b(this.m, z2);
                } else if (i3 <= headView.getHeight() || i2 < 0) {
                    View view = this.j;
                    if (view != null) {
                        view.offsetTopAndBottom(i2);
                        this.q += i2;
                        invalidate();
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            }
            a(z2);
        }
    }
}
