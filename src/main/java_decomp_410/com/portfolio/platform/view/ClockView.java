package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.fl2;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.o8;
import com.fossil.blesdk.obfuscated.ts3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ClockView extends View implements GestureDetector.OnGestureListener {
    @DexIgnore
    public static /* final */ String W; // = ClockView.class.getSimpleName();
    @DexIgnore
    public static /* final */ int[] a0; // = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    @DexIgnore
    public float A;
    @DexIgnore
    public float B;
    @DexIgnore
    public boolean C;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public /* final */ Rect E; // = new Rect();
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public int H;
    @DexIgnore
    public int I;
    @DexIgnore
    public int J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public boolean N; // = true;
    @DexIgnore
    public boolean O; // = false;
    @DexIgnore
    public boolean P; // = false;
    @DexIgnore
    public boolean Q; // = false;
    @DexIgnore
    public boolean R; // = true;
    @DexIgnore
    public boolean S; // = false;
    @DexIgnore
    public float T;
    @DexIgnore
    public b U;
    @DexIgnore
    public a V;
    @DexIgnore
    public String e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public float m;
    @DexIgnore
    public float n;
    @DexIgnore
    public Paint o;
    @DexIgnore
    public Paint p;
    @DexIgnore
    public Paint q;
    @DexIgnore
    public Bitmap r;
    @DexIgnore
    public o8 s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public float y;
    @DexIgnore
    public float z;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(float f);

        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore
    public ClockView(Context context) {
        super(context);
        c();
    }

    @DexIgnore
    private void setScale(boolean z2) {
        FLogger.INSTANCE.getLocal().d(W, "setScale() called with: scale = [" + z2 + "]");
        if (this.D != z2) {
            if (z2) {
                float f2 = this.i;
                float f3 = this.A;
                this.i = f2 / f3;
                this.t /= f3;
                this.z /= f3;
                this.w = this.t / 2.0f;
                this.B = 1.0f;
            } else {
                float f4 = this.i;
                float f5 = this.A;
                this.i = f4 * f5;
                this.t *= f5;
                this.z *= f5;
                this.w *= f5;
                this.B = f5;
            }
            this.D = z2;
            invalidate();
            a aVar = this.V;
            if (aVar != null) {
                aVar.a(this.t);
                this.V.a(this.I == this.J);
            }
        }
    }

    @DexIgnore
    public final void a() {
        this.o.setStyle(Paint.Style.FILL);
    }

    @DexIgnore
    public final boolean a(float f2, float f3, float f4) {
        return f3 <= f2 && f2 < f4;
    }

    @DexIgnore
    public final void b() {
        int i2 = this.H;
        this.t = (float) i2;
        this.u = (float) i2;
        this.v = this.u / 2.0f;
        this.w = this.t / 2.0f;
        this.x = (float) ((int) ts3.a(15.0f));
        this.y = (Math.min(this.t, this.u) / 2.0f) - this.x;
        this.z = (float) ((int) (this.y - ts3.a(15.0f)));
        float f2 = this.z - (this.i * 3.0f);
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), R.drawable.notification_hands);
        float height = f2 / ((float) decodeResource.getHeight());
        int width = (int) (((float) decodeResource.getWidth()) * height);
        int height2 = (int) (((float) decodeResource.getHeight()) * height);
        if (width <= 0) {
            width = decodeResource.getWidth();
        }
        if (height2 <= 0) {
            height2 = decodeResource.getHeight();
        }
        this.r = Bitmap.createScaledBitmap(decodeResource, width, height2, false);
        this.T = (this.A - 1.0f) / 20.0f;
    }

    @DexIgnore
    public final void c() {
        this.g = this.f;
        if (getContext().getResources().getConfiguration().getLayoutDirection() == 1) {
            this.f = ((float) fl2.b().a()) - this.f;
            this.S = true;
        }
        this.A = this.n / this.m;
        this.D = true;
        setScale(false);
        this.o = new Paint();
        if (!TextUtils.isEmpty(this.e)) {
            this.o.setTypeface(Typeface.createFromAsset(getResources().getAssets(), this.e));
            this.o.setAntiAlias(true);
        }
        this.o.setTextSize(this.i);
        this.p = new Paint();
        this.p.setAntiAlias(true);
        this.p.setColor(this.l);
        this.p.setStyle(Paint.Style.FILL);
        this.q = new Paint();
        this.q.setAntiAlias(true);
        this.q.setFilterBitmap(true);
        this.q.setDither(true);
        this.s = new o8(getContext(), this);
    }

    @DexIgnore
    public boolean onDown(MotionEvent motionEvent) {
        return true;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (!this.C) {
            b();
            this.C = true;
        }
        if (this.Q) {
            b(canvas);
            return;
        }
        a();
        b(canvas);
        a(canvas);
    }

    @DexIgnore
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    @DexIgnore
    public void onLongPress(MotionEvent motionEvent) {
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int a2 = a(i2);
        int a3 = a(i3);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = W;
        local.d(str, "onMeasure() called with: measureWidth = [" + a2 + "], measureHeight = [" + a3 + "]");
        int min = Math.min(a2, a3);
        setMeasuredDimension(a2, min);
        if (this.N && a2 > a3) {
            this.H = min;
            this.G = (a2 - a3) / 2;
            this.N = false;
        }
    }

    @DexIgnore
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    @DexIgnore
    public void onShowPress(MotionEvent motionEvent) {
    }

    @DexIgnore
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        this.F = a(a(motionEvent.getX(), motionEvent.getY()));
        b bVar = this.U;
        if (bVar != null) {
            bVar.a(this.F);
        }
        invalidate();
        return true;
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        return isEnabled() && this.s.a(motionEvent);
    }

    @DexIgnore
    public void setClockOnTouchListener(b bVar) {
        this.U = bVar;
    }

    @DexIgnore
    public void setCurrentHour(int i2) {
        this.F = i2;
        invalidate();
    }

    @DexIgnore
    public void setOnAnimationListener(a aVar) {
        this.V = aVar;
    }

    @DexIgnore
    public void setShowAnimation(boolean z2) {
        this.O = false;
    }

    @DexIgnore
    public void setShowHand(boolean z2) {
        this.R = z2;
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        int i2 = this.F;
        if (i2 != -1 && this.R) {
            Matrix matrix = new Matrix();
            float f2 = this.B;
            matrix.postScale(f2, f2);
            matrix.postTranslate((((float) (-this.r.getWidth())) * this.B) / 2.0f, (((float) (-this.r.getHeight())) * this.B) / 2.0f);
            matrix.postRotate((float) Math.toDegrees(((double) i2) * 0.5235987755982988d), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (((float) this.r.getHeight()) * this.B) / 2.0f);
            matrix.postTranslate(this.v + ((float) this.G), this.w - ((((float) this.r.getHeight()) * this.B) / 2.0f));
            canvas.drawBitmap(this.r, matrix, this.q);
            matrix.reset();
        }
    }

    @DexIgnore
    public ClockView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.ClockView);
        this.e = obtainStyledAttributes.getString(3);
        this.m = obtainStyledAttributes.getDimension(7, ts3.a(202.0f));
        this.n = obtainStyledAttributes.getDimension(6, ts3.a(314.0f));
        this.f = obtainStyledAttributes.getDimension(1, ts3.a(50.0f));
        this.h = obtainStyledAttributes.getDimension(0, ts3.a(50.0f));
        this.i = obtainStyledAttributes.getDimension(5, (float) ts3.b(13.0f));
        this.j = obtainStyledAttributes.getColor(2, k6.a(context, (int) R.color.black));
        this.k = obtainStyledAttributes.getColor(4, k6.a(context, (int) R.color.white));
        this.l = obtainStyledAttributes.getColor(8, k6.a(context, (int) R.color.fossilOrange));
        obtainStyledAttributes.recycle();
        c();
    }

    @DexIgnore
    public void a(int i2, int i3) {
        this.I = Math.abs(i3);
        this.J = i2;
        this.Q = this.I > 0;
        if (!this.O) {
            float f2 = this.h;
            float f3 = this.i;
            float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (f2 - f3 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                f4 = f2 - f3;
            }
            this.K = f4;
            this.O = true;
        }
        invalidate();
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        float f2;
        float f3;
        Canvas canvas2 = canvas;
        double d = 0.5235987755982988d;
        int i2 = 0;
        float f4 = 2.0f;
        if (this.Q) {
            int i3 = this.I;
            float f5 = ((float) i3) / ((float) this.J);
            float f6 = this.K * f5;
            float f7 = i3 == 0 ? this.i : this.i + f6;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = W;
            local.d(str, "drawNumeral() called with: deltaOffset = [" + f5 + "], deltaFontScale = [" + this.K + "], fontScale = [" + f6 + "], finalFontSize = [" + f7 + "], deltaXScale = [" + this.L + "], deltaYScale = [" + this.M + "]");
            float f8 = this.i;
            if (f7 <= f8) {
                f7 = f8;
            }
            this.o.setTextSize(f7);
            this.o.getTextBounds("22", 0, "22".length(), this.E);
            double d2 = ((double) (this.F - 3)) * 0.5235987755982988d;
            if (!this.S) {
                f3 = ((float) (((int) ((((double) (this.u / 2.0f)) + (Math.cos(d2) * ((double) this.z))) - ((double) (this.E.width() / 2)))) + this.G)) - (this.L * f5);
                f2 = ((float) ((int) ((((double) (this.t / 2.0f)) + (Math.sin(d2) * ((double) this.z))) + ((double) (this.E.height() / 2))))) - (this.M * f5);
                if (f3 - f7 <= this.f - ts3.a(10.0f)) {
                    f3 = (this.f - ts3.a(10.0f)) + f7;
                }
                float f9 = this.g;
                if (f2 - f7 <= f9) {
                    f2 = f7 + f9;
                }
            } else {
                float cos = ((float) (((int) ((((double) (this.u / 2.0f)) + (Math.cos(d2) * ((double) this.z))) - ((double) (this.E.width() / 2)))) + this.G)) + (this.L * f5);
                f2 = ((float) ((int) ((((double) (this.t / 2.0f)) + (Math.sin(d2) * ((double) this.z))) + ((double) (this.E.height() / 2))))) - (this.M * f5);
                if (cos + f7 >= this.f - ts3.a(10.0f)) {
                    cos = (this.f - ts3.a(10.0f)) - f7;
                }
                float f10 = this.g;
                if (f2 - f7 <= f10) {
                    f2 = f10 + f7;
                }
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = W;
            local2.d(str2, "drawNumeral() called with: x = [" + f3 + "], y = [" + f2 + "]");
            canvas2.drawCircle(((float) (this.E.width() / 2)) + f3, f2 - ((float) (this.E.height() / 2)), f7, this.p);
            this.o.setColor(this.k);
            String valueOf = String.valueOf(this.F);
            canvas2.drawText(valueOf, (float) ((int) (f3 + ((((float) this.E.width()) - this.o.measureText(valueOf)) / 2.0f))), f2, this.o);
            return;
        }
        int[] iArr = a0;
        int length = iArr.length;
        int i4 = 0;
        while (i4 < length) {
            int i5 = iArr[i4];
            this.o.setTextSize(this.i);
            this.o.getTextBounds("22", i2, "22".length(), this.E);
            double d3 = ((double) (i5 - 3)) * d;
            int cos2 = ((int) ((((double) (this.u / f4)) + (Math.cos(d3) * ((double) this.z))) - ((double) (this.E.width() / 2)))) + this.G;
            int sin = (int) (((double) (this.t / f4)) + (Math.sin(d3) * ((double) this.z)) + ((double) (this.E.height() / 2)));
            if (i5 == this.F) {
                canvas2.drawCircle((float) ((this.E.width() / 2) + cos2), (float) (sin - (this.E.height() / 2)), this.i, this.p);
                this.o.setColor(this.k);
                if (!this.P) {
                    if (!this.S) {
                        this.L = (float) cos2;
                    } else {
                        this.L = ((float) fl2.b().a()) - this.L;
                    }
                    this.M = (float) sin;
                    this.P = true;
                }
            } else {
                this.o.setColor(this.j);
            }
            canvas2.drawText(String.valueOf(i5), (float) ((int) (((float) cos2) + ((((float) this.E.width()) - this.o.measureText(String.valueOf(i5))) / 2.0f))), (float) sin, this.o);
            i4++;
            d = 0.5235987755982988d;
            i2 = 0;
            f4 = 2.0f;
        }
    }

    @DexIgnore
    public final int a(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 0) {
            return 200;
        }
        return size;
    }

    @DexIgnore
    public final float a(float f2, float f3) {
        return ((float) Math.toDegrees(Math.atan2((double) (f3 - ((float) (getHeight() / 2))), (double) (f2 - ((float) (getWidth() / 2)))))) + 90.0f;
    }

    @DexIgnore
    public final int a(float f2) {
        if (a(f2, -15.0f, 15.0f)) {
            return 12;
        }
        if (a(f2, 15.0f, 45.0f)) {
            return 1;
        }
        if (a(f2, 45.0f, 75.0f)) {
            return 2;
        }
        if (a(f2, 75.0f, 105.0f)) {
            return 3;
        }
        if (a(f2, 105.0f, 135.0f)) {
            return 4;
        }
        if (a(f2, 135.0f, 165.0f)) {
            return 5;
        }
        if (a(f2, 165.0f, 195.0f)) {
            return 6;
        }
        if (a(f2, 195.0f, 225.0f)) {
            return 7;
        }
        if (a(f2, 225.0f, 255.0f)) {
            return 8;
        }
        if (a(f2, 255.0f, 270.0f) || a(f2, -90.0f, -70.0f)) {
            return 9;
        }
        if (a(f2, -70.0f, -40.0f)) {
            return 10;
        }
        return a(f2, -40.0f, -15.0f) ? 11 : 0;
    }
}
