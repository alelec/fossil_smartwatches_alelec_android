package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WaveView extends View {
    @DexIgnore
    public Paint e;
    @DexIgnore
    public int f;
    @DexIgnore
    public float g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float k;
    @DexIgnore
    public float l;
    @DexIgnore
    public float m;
    @DexIgnore
    public int n;
    @DexIgnore
    public long o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public WaveView(Context context) {
        this(context, (AttributeSet) null, 2, (fd4) null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WaveView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        kd4.b(context, "context");
        this.e = new Paint(1);
        this.f = 3;
        this.g = 374.0f;
        this.h = 1670;
        this.i = 330;
        this.j = 244.0f;
        this.k = 618.0f;
        this.l = 318.0f;
        this.m = 6.0f;
        this.n = 50;
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        this.e.setStyle(Paint.Style.STROKE);
        if (attributeSet != null) {
            Context context = getContext();
            if (context != null) {
                TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.WaveView);
                if (obtainStyledAttributes != null) {
                    try {
                        this.e.setColor(obtainStyledAttributes.getColor(0, -16777216));
                        this.f = obtainStyledAttributes.getInteger(1, 3);
                        this.h = obtainStyledAttributes.getInteger(2, 1670);
                        this.i = obtainStyledAttributes.getInteger(3, 330);
                        obtainStyledAttributes.getInteger(4, 417);
                        this.j = (float) obtainStyledAttributes.getDimensionPixelSize(6, 244);
                        this.k = (float) obtainStyledAttributes.getDimensionPixelSize(5, 618);
                        this.g = this.k - this.j;
                        this.l = this.j + ((this.g * ((float) this.i)) / ((float) this.h));
                        this.m = (float) obtainStyledAttributes.getDimensionPixelSize(8, 6);
                        this.n = obtainStyledAttributes.getDimensionPixelSize(7, 50);
                    } catch (Exception e2) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.e("WaveView", "init - e=" + e2);
                    } catch (Throwable th) {
                        obtainStyledAttributes.recycle();
                        throw th;
                    }
                    obtainStyledAttributes.recycle();
                }
            }
        }
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        long j2;
        float f2;
        float f3;
        int i2;
        kd4.b(canvas, "canvas");
        super.dispatchDraw(canvas);
        long currentTimeMillis = System.currentTimeMillis();
        int width = getWidth() / 2;
        int height = getHeight() / 2;
        long j3 = this.o;
        if (j3 == 0) {
            this.o = currentTimeMillis;
            j2 = 0;
        } else {
            j2 = currentTimeMillis - j3;
        }
        if (j2 == 0) {
            f2 = this.j;
        } else {
            f2 = ((((float) j2) * this.g) / ((float) this.h)) + this.j;
        }
        if (f2 - ((float) ((this.f - 1) * this.n)) > this.k) {
            f2 = this.j;
            this.o = currentTimeMillis;
        }
        int i3 = 0;
        while (i3 < this.f) {
            float f4 = this.j;
            if (f2 < f4) {
                break;
            }
            float f5 = this.l;
            if (f2 < f5) {
                float f6 = (f2 - f4) / (f5 - f4);
                f3 = f6 * this.m;
                i2 = (int) (((float) 255) * f6);
            } else {
                float f7 = (f2 - f5) / (this.k - f5);
                i2 = 255 - ((int) (((float) 255) * f7));
                if (i2 < 0) {
                    i2 = 0;
                }
                float f8 = this.m;
                f3 = f8 - (f7 * f8);
                if (f3 < ((float) 0)) {
                    f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
            }
            this.e.setAlpha(i2);
            this.e.setStrokeWidth(f3);
            canvas.drawCircle((float) width, (float) height, f2, this.e);
            i3++;
            f2 -= (float) this.n;
        }
        postInvalidateDelayed(0);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WaveView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        kd4.b(context, "context");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WaveView(Context context, AttributeSet attributeSet, int i2, fd4 fd4) {
        this(context, (i2 & 2) != 0 ? null : attributeSet);
    }
}
