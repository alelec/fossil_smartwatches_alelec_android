package com.portfolio.platform.view.cardstackview;

import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum SwipeDirection {
    Left,
    Right,
    Top,
    Bottom;
    
    @DexIgnore
    public static /* final */ List<SwipeDirection> FREEDOM; // = null;
    @DexIgnore
    public static /* final */ List<SwipeDirection> FREEDOM_NO_BOTTOM; // = null;
    @DexIgnore
    public static /* final */ List<SwipeDirection> HORIZONTAL; // = null;
    @DexIgnore
    public static /* final */ List<SwipeDirection> VERTICAL; // = null;

    /*
    static {
        FREEDOM = Arrays.asList(values());
        FREEDOM_NO_BOTTOM = Arrays.asList(new SwipeDirection[]{Top, Left, Right});
        HORIZONTAL = Arrays.asList(new SwipeDirection[]{Left, Right});
        VERTICAL = Arrays.asList(new SwipeDirection[]{Top, Bottom});
    }
    */

    @DexIgnore
    public static List<SwipeDirection> from(int i) {
        if (i == 0) {
            return FREEDOM;
        }
        if (i == 1) {
            return FREEDOM_NO_BOTTOM;
        }
        if (i == 2) {
            return HORIZONTAL;
        }
        if (i != 3) {
            return FREEDOM;
        }
        return VERTICAL;
    }
}
