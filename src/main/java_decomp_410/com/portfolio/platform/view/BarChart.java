package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.rn;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class BarChart extends View {
    @DexIgnore
    public Paint A;
    @DexIgnore
    public /* final */ Path B; // = new Path();
    @DexIgnore
    public Bitmap C;
    @DexIgnore
    public Canvas D;
    @DexIgnore
    public /* final */ List<b> e; // = new ArrayList();
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public /* final */ Rect j; // = new Rect();
    @DexIgnore
    public float k;
    @DexIgnore
    public int l;
    @DexIgnore
    public float m;
    @DexIgnore
    public String n;
    @DexIgnore
    public float o;
    @DexIgnore
    public int p;
    @DexIgnore
    public float q;
    @DexIgnore
    public float r;
    @DexIgnore
    public float s;
    @DexIgnore
    public Drawable t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public GestureDetector w;
    @DexIgnore
    public View.OnClickListener x;
    @DexIgnore
    public Paint y;
    @DexIgnore
    public Paint z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onDown(MotionEvent motionEvent) {
            return BarChart.this.x != null;
        }

        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            if (BarChart.this.x != null) {
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                float x2 = BarChart.this.getX();
                float y2 = BarChart.this.getY();
                float measuredWidth = ((float) (BarChart.this.getMeasuredWidth() / 2)) - x2;
                float f = (x - x2) - measuredWidth;
                float f2 = (y - y2) - measuredWidth;
                float f3 = (f * f) + (f2 * f2);
                BarChart barChart = BarChart.this;
                float f4 = barChart.g;
                if (f3 < (measuredWidth + f4) * (measuredWidth + f4)) {
                    barChart.x.onClick(barChart);
                }
            }
            return super.onSingleTapUp(motionEvent);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b {
        @DexIgnore
        public String a;
        @DexIgnore
        public float b;
        @DexIgnore
        public List<Pair<Float, Integer>> c;

        @DexIgnore
        public b(BarChart barChart) {
        }
    }

    @DexIgnore
    public BarChart(Context context) {
        super(context);
        a();
    }

    @DexIgnore
    public void a(String str, float f2, List<Pair<Float, Integer>> list) {
        b bVar = new b(this);
        bVar.a = str;
        bVar.b = f2;
        bVar.c = new ArrayList(list);
        this.e.add(bVar);
    }

    @DexIgnore
    public void b() {
        for (b next : this.e) {
            float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            for (Pair<Float, Integer> pair : next.c) {
                f2 = Math.max(f2, ((Float) pair.first).floatValue());
            }
            this.f = Math.max(this.f, f2);
            this.f = Math.max(this.f, next.b);
        }
        invalidate();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        float f2;
        super.onDraw(canvas);
        Bitmap bitmap = this.C;
        if (bitmap != null) {
            bitmap.eraseColor(0);
            this.B.reset();
            float f3 = this.k;
            float measuredHeight = ((float) getMeasuredHeight()) - f3;
            float ascent = (((f3 - this.z.ascent()) - this.z.descent()) / 2.0f) + measuredHeight;
            float measuredWidth = ((((float) getMeasuredWidth()) - this.h) - this.i) / ((float) this.e.size());
            float f4 = measuredHeight - this.o;
            float f5 = f4 - (this.u / 2.0f);
            float f6 = this.h + (measuredWidth / 2.0f);
            Iterator<b> it = this.e.iterator();
            float f7 = f6;
            float f8 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f9 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            while (it.hasNext()) {
                b next = it.next();
                float f10 = f4;
                for (Pair next2 : next.c) {
                    this.y.setColor(((Integer) next2.second).intValue());
                    float floatValue = f10 - ((((Float) next2.first).floatValue() * f5) / this.f);
                    this.D.drawLine(f7, f10, f7, floatValue, this.y);
                    next = next;
                    f10 = floatValue;
                }
                b bVar = next;
                Paint paint = this.z;
                String str = bVar.a;
                paint.getTextBounds(str, 0, str.length(), this.j);
                this.D.drawText(bVar.a, f7 - ((float) (this.j.width() / 2)), ascent, this.z);
                float f11 = bVar.b;
                float f12 = f4 - ((f5 * f11) / this.f);
                if (f12 != f8 && f11 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    this.B.moveTo(f9, f12);
                    f8 = f12;
                }
                float f13 = (this.e.indexOf(bVar) == 0 ? this.h : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) + measuredWidth;
                if (this.e.indexOf(bVar) == this.e.size() - 1) {
                    f2 = this.i - (this.t != null ? this.u + (this.v * 2.0f) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                } else {
                    f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                f9 += f13 + f2;
                if (f8 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && bVar.b > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    this.B.lineTo(f9, f8);
                }
                f7 += measuredWidth;
            }
            this.D.drawPath(this.B, this.A);
            Drawable drawable = this.t;
            if (drawable != null && f8 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                float f14 = f9 + this.v;
                float f15 = this.u;
                drawable.setBounds((int) f14, (int) (f8 - (f15 / 2.0f)), (int) (f14 + f15), (int) (f8 + (f15 / 2.0f)));
                this.t.draw(this.D);
            }
            canvas.drawBitmap(this.C, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
        }
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        Bitmap bitmap = this.C;
        if (bitmap == null || i2 > bitmap.getWidth() || i3 > this.C.getHeight()) {
            this.C = rn.a(getContext()).c().a(i2, i3, Bitmap.Config.ARGB_8888);
        } else {
            this.C.setWidth(i2);
            this.C.setHeight(i3);
        }
        Canvas canvas = this.D;
        if (canvas == null) {
            this.D = new Canvas(this.C);
        } else {
            canvas.setBitmap(this.C);
        }
        b();
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.w.onTouchEvent(motionEvent);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public BarChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, h62.BarChart, 0, 0);
        try {
            this.g = obtainStyledAttributes.getDimension(14, getResources().getDimension(R.dimen.dp4));
            this.h = obtainStyledAttributes.getDimension(8, getResources().getDimension(R.dimen.dp8));
            this.i = obtainStyledAttributes.getDimension(0, getResources().getDimension(R.dimen.dp18));
            this.k = obtainStyledAttributes.getDimension(11, getResources().getDimension(R.dimen.dp24));
            this.l = obtainStyledAttributes.getColor(9, k6.a(context, (int) R.color.fossilCoolGray));
            this.m = obtainStyledAttributes.getDimension(13, getResources().getDimension(R.dimen.sp18));
            this.n = obtainStyledAttributes.getString(10);
            this.o = obtainStyledAttributes.getDimension(12, getResources().getDimension(R.dimen.dp8));
            this.p = obtainStyledAttributes.getColor(1, k6.a(context, (int) R.color.fossilCoolGray));
            this.q = obtainStyledAttributes.getDimension(4, getResources().getDimension(R.dimen.dp1));
            this.r = obtainStyledAttributes.getDimension(3, getResources().getDimension(R.dimen.dp3));
            this.s = obtainStyledAttributes.getDimension(2, getResources().getDimension(R.dimen.dp4));
            this.t = obtainStyledAttributes.getDrawable(5);
            this.u = obtainStyledAttributes.getDimension(7, getResources().getDimension(R.dimen.dp22));
            this.v = obtainStyledAttributes.getDimension(6, getResources().getDimension(R.dimen.dp6));
            obtainStyledAttributes.recycle();
            a();
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    @DexIgnore
    public final void a() {
        this.y = new Paint(1);
        this.y.setStrokeWidth(this.g);
        this.z = new Paint(1);
        if (this.n != null) {
            this.z.setTypeface(Typeface.createFromAsset(getContext().getAssets(), this.n));
        }
        this.z.setTextSize(this.m);
        this.z.setColor(this.l);
        this.A = new Paint(1);
        this.A.setColor(this.p);
        this.A.setStrokeWidth(this.q);
        this.A.setStyle(Paint.Style.STROKE);
        this.A.setPathEffect(new DashPathEffect(new float[]{this.r, this.s}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.w = new GestureDetector(getContext(), new a());
        if (isInEditMode()) {
            Context context = getContext();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new Pair(Float.valueOf(123.0f), Integer.valueOf(k6.a(context, (int) R.color.hexFF9F84))));
            arrayList.add(new Pair(Float.valueOf(234.0f), Integer.valueOf(k6.a(context, (int) R.color.fossilOrange))));
            arrayList.add(new Pair(Float.valueOf(345.0f), Integer.valueOf(k6.a(context, (int) R.color.hexCD2D00))));
            a(DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX, 500.0f, arrayList);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(new Pair(Float.valueOf(234.0f), Integer.valueOf(k6.a(context, (int) R.color.hexFF9F84))));
            arrayList2.add(new Pair(Float.valueOf(345.0f), Integer.valueOf(k6.a(context, (int) R.color.fossilOrange))));
            arrayList2.add(new Pair(Float.valueOf(456.0f), Integer.valueOf(k6.a(context, (int) R.color.hexCD2D00))));
            a("M", 1000.0f, arrayList2);
            ArrayList arrayList3 = new ArrayList();
            arrayList3.add(new Pair(Float.valueOf(345.0f), Integer.valueOf(k6.a(context, (int) R.color.hexFF9F84))));
            arrayList3.add(new Pair(Float.valueOf(456.0f), Integer.valueOf(k6.a(context, (int) R.color.fossilOrange))));
            arrayList3.add(new Pair(Float.valueOf(678.0f), Integer.valueOf(k6.a(context, (int) R.color.hexCD2D00))));
            a("T", 1500.0f, arrayList3);
            this.f = 1479.0f;
            this.t = k6.c(context, R.drawable.circle_checkmark);
        }
    }
}
