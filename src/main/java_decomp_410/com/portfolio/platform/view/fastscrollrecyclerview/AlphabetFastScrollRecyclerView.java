package com.portfolio.platform.view.fastscrollrecyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.tt3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class AlphabetFastScrollRecyclerView extends RecyclerView {
    @DexIgnore
    public tt3 N0; // = null;
    @DexIgnore
    public GestureDetector O0; // = null;
    @DexIgnore
    public boolean P0; // = true;
    @DexIgnore
    public int Q0; // = 12;
    @DexIgnore
    public float R0; // = 20.0f;
    @DexIgnore
    public float S0; // = 5.0f;
    @DexIgnore
    public int T0; // = 5;
    @DexIgnore
    public int U0; // = 5;
    @DexIgnore
    public float V0; // = 0.6f;
    @DexIgnore
    public int W0; // = -16777216;
    @DexIgnore
    public int X0; // = -1;
    @DexIgnore
    public int Y0; // = -16777216;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public a(AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView) {
        }

        @DexIgnore
        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            return super.onFling(motionEvent, motionEvent2, f, f2);
        }
    }

    @DexIgnore
    public AlphabetFastScrollRecyclerView(Context context) {
        super(context);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.AlphabetFastScrollRecyclerView, 0, 0);
            if (obtainStyledAttributes != null) {
                try {
                    this.Q0 = obtainStyledAttributes.getInt(8, this.Q0);
                    this.R0 = obtainStyledAttributes.getFloat(10, this.R0);
                    this.S0 = obtainStyledAttributes.getFloat(9, this.S0);
                    this.T0 = obtainStyledAttributes.getInt(11, this.T0);
                    this.U0 = obtainStyledAttributes.getInt(2, this.U0);
                    this.V0 = obtainStyledAttributes.getFloat(7, this.V0);
                    if (obtainStyledAttributes.hasValue(0)) {
                        this.W0 = Color.parseColor(obtainStyledAttributes.getString(0));
                    }
                    if (obtainStyledAttributes.hasValue(5)) {
                        this.X0 = Color.parseColor(obtainStyledAttributes.getString(5));
                    }
                    if (obtainStyledAttributes.hasValue(3)) {
                        this.Y0 = Color.parseColor(obtainStyledAttributes.getString(3));
                    }
                    if (obtainStyledAttributes.hasValue(1)) {
                        this.W0 = obtainStyledAttributes.getColor(1, this.W0);
                    }
                    if (obtainStyledAttributes.hasValue(6)) {
                        this.X0 = obtainStyledAttributes.getColor(6, this.X0);
                    }
                    if (obtainStyledAttributes.hasValue(4)) {
                        this.Y0 = obtainStyledAttributes.getColor(3, this.Y0);
                    }
                } finally {
                    obtainStyledAttributes.recycle();
                }
            }
        }
        this.N0 = new tt3(context, this);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        tt3 tt3 = this.N0;
        if (tt3 != null) {
            tt3.a(canvas);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0014, code lost:
        if (r0.a(r4.getX(), r4.getY()) != false) goto L_0x001c;
     */
    @DexIgnore
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.P0) {
            tt3 tt3 = this.N0;
            if (tt3 != null) {
            }
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    @DexIgnore
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        tt3 tt3 = this.N0;
        if (tt3 != null) {
            tt3.a(i, i2, i3, i4);
        }
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.P0) {
            tt3 tt3 = this.N0;
            if (tt3 != null && tt3.a(motionEvent)) {
                return true;
            }
            if (this.O0 == null) {
                this.O0 = new GestureDetector(getContext(), new a(this));
            }
            this.O0.onTouchEvent(motionEvent);
        }
        return super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    public void setAdapter(RecyclerView.g gVar) {
        super.setAdapter(gVar);
        tt3 tt3 = this.N0;
        if (tt3 != null) {
            tt3.a(gVar);
        }
    }

    @DexIgnore
    public void setIndexBarColor(String str) {
        this.N0.a(Color.parseColor(str));
    }

    @DexIgnore
    public void setIndexBarCornerRadius(int i) {
        this.N0.b(i);
    }

    @DexIgnore
    public void setIndexBarHighLateTextVisibility(boolean z) {
        this.N0.a(z);
    }

    @DexIgnore
    public void setIndexBarTextColor(String str) {
        this.N0.d(Color.parseColor(str));
    }

    @DexIgnore
    public void setIndexBarTransparentValue(float f) {
        this.N0.c(f);
    }

    @DexIgnore
    public void setIndexBarVisibility(boolean z) {
        this.N0.b(z);
        this.P0 = z;
    }

    @DexIgnore
    public void setIndexTextSize(int i) {
        this.N0.e(i);
    }

    @DexIgnore
    public void setIndexbarHighLateTextColor(String str) {
        this.N0.c(Color.parseColor(str));
    }

    @DexIgnore
    public void setIndexbarMargin(float f) {
        this.N0.d(f);
    }

    @DexIgnore
    public void setIndexbarWidth(float f) {
        this.N0.e(f);
    }

    @DexIgnore
    public void setPreviewPadding(int i) {
        this.N0.f(i);
    }

    @DexIgnore
    public void setPreviewVisibility(boolean z) {
        this.N0.c(z);
    }

    @DexIgnore
    public void setTypeface(Typeface typeface) {
        this.N0.a(typeface);
    }

    @DexIgnore
    public void setIndexBarColor(int i) {
        this.N0.a(getContext().getResources().getColor(i));
    }

    @DexIgnore
    public void setIndexBarTextColor(int i) {
        this.N0.d(getContext().getResources().getColor(i));
    }

    @DexIgnore
    public void setIndexbarHighLateTextColor(int i) {
        this.N0.c(getContext().getResources().getColor(i));
    }

    @DexIgnore
    public AlphabetFastScrollRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    @DexIgnore
    public AlphabetFastScrollRecyclerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, attributeSet);
    }
}
