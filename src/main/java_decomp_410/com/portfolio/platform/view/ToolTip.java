package com.portfolio.platform.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ToolTip extends ProgressBar {
    @DexIgnore
    public ToolTip(Context context) {
        super(context);
    }

    @DexIgnore
    public ToolTip(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @DexIgnore
    public ToolTip(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
