package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.fossil.blesdk.obfuscated.h62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class UnderlinedTextView extends FlexibleTextView {
    @DexIgnore
    public Rect m;
    @DexIgnore
    public Paint n;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public /* final */ Rect q;

    @DexIgnore
    public UnderlinedTextView(Context context) {
        this(context, (AttributeSet) null, 0);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet, int i) {
        float f = context.getResources().getDisplayMetrics().density;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.UnderlinedTextView, i, 0);
        int color = obtainStyledAttributes.getColor(0, -65536);
        this.o = obtainStyledAttributes.getDimension(3, -1.0f);
        float dimension = obtainStyledAttributes.getDimension(2, f * 1.0f);
        this.p = obtainStyledAttributes.getDimension(1, 4.0f * dimension);
        obtainStyledAttributes.recycle();
        this.m = new Rect();
        this.n = new Paint();
        this.n.setStyle(Paint.Style.STROKE);
        this.n.setColor(color);
        this.n.setStrokeWidth(dimension);
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        int lineCount = getLineCount();
        CharSequence text = getText();
        getPaint().getTextBounds(text.toString(), 0, text.length(), this.q);
        if (this.o == -1.0f) {
            for (int i = 0; i < lineCount; i++) {
                int lineBounds = getLineBounds(i, this.m);
                Rect rect = this.m;
                float f = (float) rect.left;
                float f2 = (float) lineBounds;
                float f3 = this.p;
                canvas.drawLine(f, f2 + f3, (float) rect.right, f2 + f3, this.n);
            }
        } else {
            int lineBounds2 = getLineBounds(getLineCount() - 1, this.m);
            float width = (((float) this.m.width()) - this.o) / 2.0f;
            Rect rect2 = this.m;
            float f4 = ((float) rect2.left) + width;
            float f5 = (float) lineBounds2;
            float f6 = this.p;
            canvas.drawLine(f4, f5 + f6, ((float) rect2.right) - width, f5 + f6, this.n);
        }
        super.onDraw(canvas);
    }

    @DexIgnore
    public UnderlinedTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public UnderlinedTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.q = new Rect();
        a(context, attributeSet, i);
    }
}
