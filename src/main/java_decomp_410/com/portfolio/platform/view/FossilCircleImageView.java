package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.fossil.blesdk.obfuscated.bk2;
import com.fossil.blesdk.obfuscated.bw;
import com.fossil.blesdk.obfuscated.ck2;
import com.fossil.blesdk.obfuscated.fk2;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.lv;
import com.fossil.blesdk.obfuscated.nk2;
import com.fossil.blesdk.obfuscated.oo;
import com.fossil.blesdk.obfuscated.qv;
import com.fossil.blesdk.obfuscated.rv;
import com.fossil.blesdk.obfuscated.vr3;
import com.fossil.blesdk.obfuscated.xn;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class FossilCircleImageView extends AppCompatImageView {
    @DexIgnore
    public static /* final */ String v; // = FossilCircleImageView.class.getSimpleName();
    @DexIgnore
    public static /* final */ ImageView.ScaleType w; // = ImageView.ScaleType.CENTER_CROP;
    @DexIgnore
    public /* final */ RectF g;
    @DexIgnore
    public /* final */ RectF h;
    @DexIgnore
    public /* final */ Paint i;
    @DexIgnore
    public /* final */ Paint j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public String n;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public ColorFilter q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements qv<Drawable> {
        @DexIgnore
        public /* final */ /* synthetic */ fk2 e;
        @DexIgnore
        public /* final */ /* synthetic */ String f;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.FossilCircleImageView$a$a")
        /* renamed from: com.portfolio.platform.view.FossilCircleImageView$a$a  reason: collision with other inner class name */
        public class C0159a implements Runnable {
            @DexIgnore
            public C0159a() {
            }

            @DexIgnore
            public void run() {
                a aVar = a.this;
                aVar.e.a((Object) new bk2("", aVar.f)).a((lv<?>) new rv().a((oo<Bitmap>) new nk2())).a((ImageView) FossilCircleImageView.this);
            }
        }

        @DexIgnore
        public a(fk2 fk2, String str) {
            this.e = fk2;
            this.f = str;
        }

        @DexIgnore
        public boolean a(Drawable drawable, Object obj, bw<Drawable> bwVar, DataSource dataSource, boolean z) {
            return false;
        }

        @DexIgnore
        public boolean a(GlideException glideException, Object obj, bw<Drawable> bwVar, boolean z) {
            new Handler(Looper.getMainLooper()).post(new C0159a());
            return true;
        }
    }

    @DexIgnore
    public FossilCircleImageView(Context context) {
        super(context);
        this.g = new RectF();
        this.h = new RectF();
        this.i = new Paint();
        this.j = new Paint();
        this.k = -16777216;
        this.l = 0;
        this.m = 0;
        e();
    }

    @DexIgnore
    public final void a() {
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2) {
        new b(new WeakReference(this)).execute(new Bitmap[]{bitmap, bitmap2});
    }

    @DexIgnore
    public final RectF d() {
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
        int min = Math.min(width, height);
        float paddingLeft = ((float) getPaddingLeft()) + (((float) (width - min)) / 2.0f);
        float paddingTop = ((float) getPaddingTop()) + (((float) (height - min)) / 2.0f);
        float f = (float) min;
        return new RectF(paddingLeft, paddingTop, paddingLeft + f, f + paddingTop);
    }

    @DexIgnore
    public final void e() {
        super.setScaleType(w);
        this.r = true;
        if (this.s) {
            f();
            this.s = false;
        }
        FLogger.INSTANCE.getLocal().d(v, "init()");
    }

    @DexIgnore
    public final void f() {
        if (!this.r) {
            this.s = true;
        } else if (getWidth() != 0 || getHeight() != 0) {
            this.i.setStyle(Paint.Style.STROKE);
            this.i.setAntiAlias(true);
            this.i.setColor(this.k);
            this.i.setStrokeWidth((float) this.l);
            this.j.setStyle(Paint.Style.FILL);
            this.j.setAntiAlias(true);
            this.j.setColor(this.m);
            this.j.setFilterBitmap(true);
            this.j.setDither(true);
            this.h.set(d());
            this.p = Math.min((this.h.height() - ((float) this.l)) / 2.0f, (this.h.width() - ((float) this.l)) / 2.0f);
            this.g.set(this.h);
            if (!this.t) {
                int i2 = this.l;
                if (i2 > 0) {
                    this.g.inset(((float) i2) - 1.0f, ((float) i2) - 1.0f);
                }
            }
            this.o = Math.min(this.g.height() / 2.0f, this.g.width() / 2.0f);
            a();
            g();
            invalidate();
        }
    }

    @DexIgnore
    public final void g() {
    }

    @DexIgnore
    public int getBorderColor() {
        return this.k;
    }

    @DexIgnore
    public int getBorderWidth() {
        return this.l;
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        return this.q;
    }

    @DexIgnore
    @Deprecated
    public int getFillColor() {
        return this.m;
    }

    @DexIgnore
    public String getHandNumber() {
        return this.n;
    }

    @DexIgnore
    public ImageView.ScaleType getScaleType() {
        return w;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.m != 0) {
            canvas.drawCircle(this.g.centerX(), this.g.centerY(), this.o, this.j);
        }
        if (this.l > 0) {
            canvas.drawCircle(this.h.centerX(), this.h.centerY(), this.p, this.i);
        }
        if (this.u) {
            this.i.setAntiAlias(true);
            this.i.setFilterBitmap(true);
            this.i.setDither(true);
            this.i.setColor(Color.parseColor("#EEEEEE"));
            canvas.drawCircle(this.h.centerX(), this.h.centerY(), this.p, this.i);
        }
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        f();
    }

    @DexIgnore
    public void setAdjustViewBounds(boolean z) {
        if (z) {
            throw new IllegalArgumentException("adjustViewBounds not supported.");
        }
    }

    @DexIgnore
    public void setBorderColor(int i2) {
        if (i2 != this.k) {
            this.k = i2;
            this.i.setColor(this.k);
            invalidate();
        }
    }

    @DexIgnore
    @Deprecated
    public void setBorderColorResource(int i2) {
        setBorderColor(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    public void setBorderWidth(int i2) {
        if (i2 != this.l) {
            this.l = i2;
            f();
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        if (!Objects.equals(colorFilter, this.q)) {
            this.q = colorFilter;
            a();
            invalidate();
        }
    }

    @DexIgnore
    public void setDefault(boolean z) {
        this.u = z;
    }

    @DexIgnore
    @Deprecated
    public void setFillColor(int i2) {
        if (i2 != this.m) {
            this.m = i2;
            this.j.setColor(i2);
            invalidate();
        }
    }

    @DexIgnore
    @Deprecated
    public void setFillColorResource(int i2) {
        setColorFilter(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    public void setHandNumber(String str) {
        this.n = str;
    }

    @DexIgnore
    public void setPadding(int i2, int i3, int i4, int i5) {
        super.setPadding(i2, i3, i4, i5);
        f();
    }

    @DexIgnore
    public void setPaddingRelative(int i2, int i3, int i4, int i5) {
        super.setPaddingRelative(i2, i3, i4, i5);
        f();
    }

    @DexIgnore
    public void setScaleType(ImageView.ScaleType scaleType) {
        if (scaleType != w) {
            throw new IllegalArgumentException(String.format("ScaleType %s not supported.", new Object[]{scaleType}));
        }
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3) {
        new b(new WeakReference(this)).execute(new Bitmap[]{bitmap, bitmap2, bitmap3});
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, Bitmap bitmap4) {
        new b(new WeakReference(this)).execute(new Bitmap[]{bitmap, bitmap2, bitmap3, bitmap4});
    }

    @DexIgnore
    public void a(fk2 fk2, String str, String str2) {
        fk2.a(str).a((lv<?>) new rv().a((oo<Bitmap>) new nk2())).b(new a(fk2, str2)).a((ImageView) this);
    }

    @DexIgnore
    public void a(String str, xn xnVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = v;
        local.d(str2, "setImageUrl without defaultName url=" + str);
        xnVar.a(str).a(new rv().a((oo<Bitmap>) new nk2())).a((ImageView) this);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends AsyncTask<Bitmap, Void, Bitmap> {
        @DexIgnore
        public WeakReference<FossilCircleImageView> a;

        @DexIgnore
        public b(WeakReference<FossilCircleImageView> weakReference) {
            this.a = weakReference;
        }

        @DexIgnore
        /* renamed from: a */
        public Bitmap doInBackground(Bitmap... bitmapArr) {
            int length = bitmapArr.length;
            if (length == 1) {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.v, "doInBackground bitmap list = 0");
                return bitmapArr[0];
            } else if (length == 2) {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.v, "doInBackground bitmap list = 2");
                return vr3.a(bitmapArr[0], bitmapArr[1], ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            } else if (length == 3) {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.v, "doInBackground bitmap list = 3");
                return vr3.a(bitmapArr[0], bitmapArr[1], bitmapArr[2], ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            } else if (length != 4) {
                return null;
            } else {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.v, "doInBackground bitmap list = 4");
                return vr3.a(bitmapArr[0], bitmapArr[1], bitmapArr[2], bitmapArr[3], ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            }
        }

        @DexIgnore
        public void onPreExecute() {
            super.onPreExecute();
        }

        @DexIgnore
        /* renamed from: a */
        public void onPostExecute(Bitmap bitmap) {
            FLogger.INSTANCE.getLocal().d(FossilCircleImageView.v, "onPostExecute");
            if (this.a.get() != null) {
                ((FossilCircleImageView) this.a.get()).setImageBitmap(bitmap);
            }
        }
    }

    @DexIgnore
    public FossilCircleImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public FossilCircleImageView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.g = new RectF();
        this.h = new RectF();
        this.i = new Paint();
        this.j = new Paint();
        this.k = -16777216;
        this.l = 0;
        this.m = 0;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.FossilCircleImageView, i2, 0);
        this.l = obtainStyledAttributes.getDimensionPixelSize(2, 0);
        this.k = obtainStyledAttributes.getColor(0, -16777216);
        this.t = obtainStyledAttributes.getBoolean(1, false);
        this.m = obtainStyledAttributes.getColor(3, 0);
        this.n = obtainStyledAttributes.getString(4);
        if (this.n == null) {
            this.n = "";
        }
        obtainStyledAttributes.recycle();
        FLogger.INSTANCE.getLocal().d(v, "FossilCircleImageView constructor");
        e();
    }

    @DexIgnore
    public void a(String str, String str2, xn xnVar) {
        ck2.a((View) this).a((Object) new bk2(str, str2)).a((lv<?>) new rv().a((oo<Bitmap>) new nk2())).a((ImageView) this);
    }

    @DexIgnore
    public void a(Bitmap bitmap, xn xnVar) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream);
        xnVar.a(byteArrayOutputStream.toByteArray()).a(((rv) new rv().a((oo<Bitmap>) new nk2())).c()).a((ImageView) this);
        try {
            byteArrayOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.u = false;
    }

    @DexIgnore
    public void a(int i2, xn xnVar) {
        xnVar.a(Integer.valueOf(i2)).a(new rv().a((oo<Bitmap>) new nk2())).a((ImageView) this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = v;
        local.d(str, "setImageResource resId = " + i2);
    }
}
