package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.h62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class AutoResizeTextView extends FlexibleTextView {
    @DexIgnore
    public a m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public float q;
    @DexIgnore
    public float r;
    @DexIgnore
    public float s;
    @DexIgnore
    public boolean t;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(TextView textView, float f, float f2);
    }

    @DexIgnore
    public AutoResizeTextView(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        CharSequence text = getText();
        if (text != null && text.length() != 0 && i4 > 0 && i3 > 0 && this.o != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            if (getTransformationMethod() != null) {
                text = getTransformationMethod().getTransformation(text, this);
            }
            CharSequence charSequence = text;
            TextPaint paint = getPaint();
            float textSize = paint.getTextSize();
            float f = this.p;
            float min = f > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? Math.min(this.o, f) : this.o;
            int a2 = a(charSequence, paint, i3, min);
            float f2 = min;
            while (a2 > i4) {
                float f3 = this.q;
                if (f2 <= f3) {
                    break;
                }
                f2 = Math.max(f2 - 2.0f, f3);
                a2 = a(charSequence, paint, i3, f2);
            }
            if (this.t && f2 == this.q && a2 > i4) {
                StaticLayout staticLayout = r1;
                StaticLayout staticLayout2 = new StaticLayout(charSequence, new TextPaint(paint), i, Layout.Alignment.ALIGN_NORMAL, this.r, this.s, false);
                if (staticLayout.getLineCount() > 0) {
                    StaticLayout staticLayout3 = staticLayout;
                    int lineForVertical = staticLayout3.getLineForVertical(i4) - 1;
                    if (lineForVertical < 0) {
                        setText("");
                    } else {
                        int lineStart = staticLayout3.getLineStart(lineForVertical);
                        int lineEnd = staticLayout3.getLineEnd(lineForVertical);
                        float lineWidth = staticLayout3.getLineWidth(lineForVertical);
                        float measureText = paint.measureText("...");
                        while (((float) i3) < lineWidth + measureText) {
                            lineEnd--;
                            lineWidth = paint.measureText(charSequence.subSequence(lineStart, lineEnd + 1).toString());
                        }
                        setText(charSequence.subSequence(0, lineEnd) + "...");
                    }
                }
            }
            setTextSize(0, f2);
            setLineSpacing(this.s, this.r);
            a aVar = this.m;
            if (aVar != null) {
                aVar.a(this, textSize, f2);
            }
            this.n = false;
        }
    }

    @DexIgnore
    public final void f() {
        float f = this.o;
        if (f > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            super.setTextSize(0, f);
            this.p = this.o;
        }
    }

    @DexIgnore
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (z || this.n) {
            a(((i3 - i) - getCompoundPaddingLeft()) - getCompoundPaddingRight(), ((i4 - i2) - getCompoundPaddingBottom()) - getCompoundPaddingTop());
        }
        super.onLayout(z, i, i2, i3, i4);
    }

    @DexIgnore
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        if (i != i3 || i2 != i4) {
            this.n = true;
        }
    }

    @DexIgnore
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.n = true;
        f();
    }

    @DexIgnore
    public void setLineSpacing(float f, float f2) {
        super.setLineSpacing(f, f2);
        this.r = f2;
        this.s = f;
    }

    @DexIgnore
    public void setTextSize(float f) {
        super.setTextSize(f);
        this.o = getTextSize();
    }

    @DexIgnore
    public AutoResizeTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public AutoResizeTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.n = false;
        this.p = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.r = 1.0f;
        this.s = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.t = true;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.AutoResizeTextView);
        this.q = obtainStyledAttributes.getDimension(0, 20.0f);
        obtainStyledAttributes.recycle();
        this.o = getTextSize();
    }

    @DexIgnore
    public void setTextSize(int i, float f) {
        super.setTextSize(i, f);
        this.o = getTextSize();
    }

    @DexIgnore
    public final int a(CharSequence charSequence, TextPaint textPaint, int i, float f) {
        TextPaint textPaint2 = new TextPaint(textPaint);
        textPaint2.setTextSize(f);
        return new StaticLayout(charSequence, textPaint2, i, Layout.Alignment.ALIGN_NORMAL, this.r, this.s, true).getHeight();
    }
}
