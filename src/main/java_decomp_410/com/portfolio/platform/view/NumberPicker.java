package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Scroller;
import androidx.appcompat.widget.AppCompatEditText;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.wearables.fossil.R;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class NumberPicker extends LinearLayout {
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public /* final */ Scroller C;
    @DexIgnore
    public /* final */ Scroller D;
    @DexIgnore
    public int E;
    @DexIgnore
    public e F;
    @DexIgnore
    public d G;
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public VelocityTracker J;
    @DexIgnore
    public int K;
    @DexIgnore
    public int L;
    @DexIgnore
    public int M;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public /* final */ int O;
    @DexIgnore
    public /* final */ int P;
    @DexIgnore
    public /* final */ boolean Q;
    @DexIgnore
    public /* final */ Drawable R;
    @DexIgnore
    public /* final */ int S;
    @DexIgnore
    public int T;
    @DexIgnore
    public boolean U;
    @DexIgnore
    public boolean V;
    @DexIgnore
    public int W;
    @DexIgnore
    public int a0;
    @DexIgnore
    public int b0;
    @DexIgnore
    public boolean c0;
    @DexIgnore
    public boolean d0;
    @DexIgnore
    public /* final */ ImageButton e;
    @DexIgnore
    public i e0;
    @DexIgnore
    public /* final */ ImageButton f;
    @DexIgnore
    public /* final */ h f0;
    @DexIgnore
    public /* final */ AppCompatEditText g;
    @DexIgnore
    public int g0;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public /* final */ boolean m;
    @DexIgnore
    public /* final */ int n;
    @DexIgnore
    public int o;
    @DexIgnore
    public String[] p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public g t;
    @DexIgnore
    public f u;
    @DexIgnore
    public /* final */ SparseArray<String> v;
    @DexIgnore
    public /* final */ int[] w;
    @DexIgnore
    public /* final */ Paint x;
    @DexIgnore
    public /* final */ Drawable y;
    @DexIgnore
    public int z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class CustomEditText extends AppCompatEditText {
        @DexIgnore
        public CustomEditText(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        @DexIgnore
        public void onEditorAction(int i) {
            super.onEditorAction(i);
            if (i == 6) {
                clearFocus();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            NumberPicker.this.b();
            NumberPicker.this.g.clearFocus();
            if (view.getId() == R.id.np__increment) {
                NumberPicker.this.a(true);
            } else {
                NumberPicker.this.a(false);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnLongClickListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            NumberPicker.this.b();
            NumberPicker.this.g.clearFocus();
            if (view.getId() == R.id.np__increment) {
                NumberPicker.this.a(true, 0);
            } else {
                NumberPicker.this.a(false, 0);
            }
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            NumberPicker.this.j();
            NumberPicker.this.U = true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public boolean e;

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a(boolean z) {
            this.e = z;
        }

        @DexIgnore
        public void run() {
            NumberPicker.this.a(this.e);
            NumberPicker.this.postDelayed(this, 300);
        }
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        String format(int i);
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(NumberPicker numberPicker, int i, int i2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class i {
        @DexIgnore
        public c a;

        @DexIgnore
        public i(NumberPicker numberPicker) {
            this.a = new c();
        }

        @DexIgnore
        public boolean a(int i, int i2, Bundle bundle) {
            c cVar = this.a;
            return cVar != null && cVar.performAction(i, i2, bundle);
        }

        @DexIgnore
        public void a(int i, int i2) {
            c cVar = this.a;
            if (cVar != null) {
                cVar.a(i, i2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class j implements f {
        @DexIgnore
        public /* final */ StringBuilder a; // = new StringBuilder();
        @DexIgnore
        public char b;
        @DexIgnore
        public transient Formatter c;
        @DexIgnore
        public /* final */ Object[] d; // = new Object[1];

        @DexIgnore
        public j() {
            b(Locale.getDefault());
        }

        @DexIgnore
        public static char c(Locale locale) {
            return new DecimalFormatSymbols(locale).getZeroDigit();
        }

        @DexIgnore
        public final Formatter a(Locale locale) {
            return new Formatter(this.a, locale);
        }

        @DexIgnore
        public final void b(Locale locale) {
            this.c = a(locale);
            this.b = c(locale);
        }

        @DexIgnore
        public String format(int i) {
            Locale locale = Locale.getDefault();
            if (this.b != c(locale)) {
                b(locale);
            }
            this.d[0] = Integer.valueOf(i);
            StringBuilder sb = this.a;
            sb.delete(0, sb.length());
            this.c.format("%02d", this.d);
            return this.c.toString();
        }
    }

    /*
    static {
        new j();
    }
    */

    @DexIgnore
    public NumberPicker(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    private i getSupportAccessibilityNodeProvider() {
        return new i(this);
    }

    @DexIgnore
    public static int resolveSizeAndState(int i2, int i3, int i4) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode != Integer.MIN_VALUE) {
            if (mode != 0 && mode == 1073741824) {
                i2 = size;
            }
        } else if (size < i2) {
            i2 = 16777216 | size;
        }
        return i2 | (-16777216 & i4);
    }

    @DexIgnore
    public final boolean a(Scroller scroller) {
        scroller.forceFinished(true);
        int finalY = scroller.getFinalY() - scroller.getCurrY();
        int i2 = this.A - ((this.B + finalY) % this.z);
        if (i2 == 0) {
            return false;
        }
        int abs = Math.abs(i2);
        int i3 = this.z;
        if (abs > i3 / 2) {
            i2 = i2 > 0 ? i2 - i3 : i2 + i3;
        }
        scrollBy(0, finalY + i2);
        return true;
    }

    @DexIgnore
    public void b() {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null && inputMethodManager.isActive(this.g)) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            if (this.Q) {
                this.g.setVisibility(4);
            }
        }
    }

    @DexIgnore
    public final void c() {
        setVerticalFadingEdgeEnabled(true);
        setFadingEdgeLength(((getBottom() - getTop()) - this.n) / 2);
    }

    @DexIgnore
    public void computeScroll() {
        Scroller scroller = this.C;
        if (scroller.isFinished()) {
            scroller = this.D;
            if (scroller.isFinished()) {
                return;
            }
        }
        scroller.computeScrollOffset();
        int currY = scroller.getCurrY();
        if (this.E == 0) {
            this.E = scroller.getStartY();
        }
        scrollBy(0, currY - this.E);
        this.E = currY;
        if (scroller.isFinished()) {
            b(scroller);
        } else {
            invalidate();
        }
    }

    @DexIgnore
    public final void d() {
        e();
        int[] iArr = this.w;
        this.o = (int) ((((float) ((getBottom() - getTop()) - (iArr.length * this.n))) / ((float) iArr.length)) + 0.5f);
        this.z = this.n + this.o;
        this.A = (this.g.getBaseline() + this.g.getTop()) - (this.z * 1);
        this.B = this.A;
        l();
    }

    @DexIgnore
    public boolean dispatchHoverEvent(MotionEvent motionEvent) {
        int i2;
        if (!this.Q) {
            return super.dispatchHoverEvent(motionEvent);
        }
        if (!((AccessibilityManager) getContext().getSystemService("accessibility")).isEnabled()) {
            return false;
        }
        int y2 = (int) motionEvent.getY();
        if (y2 < this.W) {
            i2 = 3;
        } else {
            i2 = y2 > this.a0 ? 1 : 2;
        }
        int action = motionEvent.getAction() & 255;
        i supportAccessibilityNodeProvider = getSupportAccessibilityNodeProvider();
        if (action == 7) {
            int i3 = this.b0;
            if (i3 == i2 || i3 == -1) {
                return false;
            }
            supportAccessibilityNodeProvider.a(i3, 256);
            supportAccessibilityNodeProvider.a(i2, 128);
            this.b0 = i2;
            supportAccessibilityNodeProvider.a(i2, 64, (Bundle) null);
            return false;
        } else if (action == 9) {
            supportAccessibilityNodeProvider.a(i2, 128);
            this.b0 = i2;
            supportAccessibilityNodeProvider.a(i2, 64, (Bundle) null);
            return false;
        } else if (action != 10) {
            return false;
        } else {
            supportAccessibilityNodeProvider.a(i2, 256);
            this.b0 = -1;
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
        requestFocus();
        r5.g0 = r0;
        g();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
        if (r5.C.isFinished() == false) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0054, code lost:
        if (r0 != 20) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0056, code lost:
        r6 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0058, code lost:
        r6 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0059, code lost:
        a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005c, code lost:
        return true;
     */
    @DexIgnore
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        if (keyCode == 19 || keyCode == 20) {
            if (this.Q) {
                int action = keyEvent.getAction();
                if (action != 0) {
                    if (action == 1 && this.g0 == keyCode) {
                        this.g0 = -1;
                        return true;
                    }
                } else if (!this.N) {
                }
            }
        } else if (keyCode == 23 || keyCode == 66) {
            g();
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    @DexIgnore
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 3) {
            g();
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    @DexIgnore
    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 3) {
            g();
        }
        return super.dispatchTrackballEvent(motionEvent);
    }

    @DexIgnore
    public final void e() {
        this.v.clear();
        int[] iArr = this.w;
        int i2 = this.s;
        for (int i3 = 0; i3 < this.w.length; i3++) {
            int i4 = (i3 - 1) + i2;
            if (this.N) {
                i4 = d(i4);
            }
            iArr[i3] = i4;
            a(iArr[i3]);
        }
    }

    @DexIgnore
    public final void f(int i2) {
        if (this.T != i2) {
            this.T = i2;
        }
    }

    @DexIgnore
    public final void g() {
        e eVar = this.F;
        if (eVar != null) {
            removeCallbacks(eVar);
        }
        d dVar = this.G;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
        this.f0.a();
    }

    @DexIgnore
    public AccessibilityNodeProvider getAccessibilityNodeProvider() {
        if (!this.Q) {
            return super.getAccessibilityNodeProvider();
        }
        if (this.e0 == null) {
            this.e0 = new i(this);
        }
        return this.e0.a;
    }

    @DexIgnore
    public float getBottomFadingEdgeStrength() {
        return 0.9f;
    }

    @DexIgnore
    public int getMaxValue() {
        return this.r;
    }

    @DexIgnore
    public int getMinValue() {
        return this.q;
    }

    @DexIgnore
    public int getSolidColor() {
        return this.O;
    }

    @DexIgnore
    public float getTopFadingEdgeStrength() {
        return 0.9f;
    }

    @DexIgnore
    public int getValue() {
        return this.s;
    }

    @DexIgnore
    public boolean getWrapSelectorWheel() {
        return this.N;
    }

    @DexIgnore
    public final void h() {
        d dVar = this.G;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
    }

    @DexIgnore
    public final void i() {
        e eVar = this.F;
        if (eVar != null) {
            removeCallbacks(eVar);
        }
    }

    @DexIgnore
    public void j() {
    }

    @DexIgnore
    public final void k() {
        int i2;
        if (this.m) {
            String[] strArr = this.p;
            int i3 = 0;
            if (strArr == null) {
                float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                for (int i4 = 0; i4 <= 9; i4++) {
                    float measureText = this.x.measureText(g(i4));
                    if (measureText > f2) {
                        f2 = measureText;
                    }
                }
                for (int i5 = this.r; i5 > 0; i5 /= 10) {
                    i3++;
                }
                i2 = (int) (((float) i3) * f2);
            } else {
                int length = strArr.length;
                int i6 = 0;
                while (i3 < length) {
                    float measureText2 = this.x.measureText(strArr[i3]);
                    if (measureText2 > ((float) i6)) {
                        i6 = (int) measureText2;
                    }
                    i3++;
                }
                i2 = i6;
            }
            int paddingLeft = i2 + this.g.getPaddingLeft() + this.g.getPaddingRight();
            if (this.l != paddingLeft) {
                int i7 = this.k;
                if (paddingLeft > i7) {
                    this.l = paddingLeft;
                } else {
                    this.l = i7;
                }
                invalidate();
            }
        }
    }

    @DexIgnore
    public final boolean l() {
        String[] strArr = this.p;
        String c2 = strArr == null ? c(this.s) : strArr[this.s - this.q];
        if (TextUtils.isEmpty(c2) || c2.equals(this.g.getText().toString())) {
            return false;
        }
        this.g.setText(c2);
        return true;
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        g();
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (!this.Q) {
            super.onDraw(canvas);
            return;
        }
        float right = (float) ((getRight() - getLeft()) / 2);
        float f2 = (float) this.B;
        Drawable drawable = this.y;
        if (drawable != null && this.T == 0) {
            if (this.d0) {
                drawable.setState(LinearLayout.PRESSED_ENABLED_STATE_SET);
                this.y.setBounds(0, 0, getRight(), this.W);
                this.y.draw(canvas);
            }
            if (this.c0) {
                this.y.setState(LinearLayout.PRESSED_ENABLED_STATE_SET);
                this.y.setBounds(0, this.a0, getRight(), getBottom());
                this.y.draw(canvas);
            }
        }
        int[] iArr = this.w;
        float f3 = f2;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            String str = this.v.get(iArr[i2]);
            if (i2 != 1 || this.g.getVisibility() != 0) {
                a(this.x, (float) getWidth(), str);
                canvas.drawText(str, right, f3, this.x);
            }
            f3 += (float) this.z;
        }
        Drawable drawable2 = this.R;
        if (drawable2 != null) {
            int i3 = this.W;
            drawable2.setBounds(0, i3, getRight(), this.S + i3);
            this.R.draw(canvas);
            int i4 = this.a0;
            this.R.setBounds(0, i4 - this.S, getRight(), i4);
            this.R.draw(canvas);
        }
    }

    @DexIgnore
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(NumberPicker.class.getName());
        accessibilityEvent.setScrollable(true);
        accessibilityEvent.setScrollY((this.q + this.s) * this.z);
        accessibilityEvent.setMaxScrollY((this.r - this.q) * this.z);
    }

    @DexIgnore
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.Q || !isEnabled() || (motionEvent.getAction() & 255) != 0) {
            return false;
        }
        g();
        this.g.setVisibility(4);
        float y2 = motionEvent.getY();
        this.H = y2;
        this.I = y2;
        this.U = false;
        this.V = false;
        float f2 = this.H;
        if (f2 < ((float) this.W)) {
            if (this.T == 0) {
                this.f0.a(2);
            }
        } else if (f2 > ((float) this.a0) && this.T == 0) {
            this.f0.a(1);
        }
        getParent().requestDisallowInterceptTouchEvent(true);
        if (!this.C.isFinished()) {
            this.C.forceFinished(true);
            this.D.forceFinished(true);
            f(0);
        } else if (!this.D.isFinished()) {
            this.C.forceFinished(true);
            this.D.forceFinished(true);
        } else {
            float f3 = this.H;
            if (f3 < ((float) this.W)) {
                b();
                a(false, (long) ViewConfiguration.getLongPressTimeout());
            } else if (f3 > ((float) this.a0)) {
                b();
                a(true, (long) ViewConfiguration.getLongPressTimeout());
            } else {
                this.V = true;
                f();
            }
        }
        return true;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        if (!this.Q) {
            super.onLayout(z2, i2, i3, i4, i5);
            return;
        }
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        int measuredWidth2 = this.g.getMeasuredWidth();
        int measuredHeight2 = this.g.getMeasuredHeight();
        int i6 = (measuredWidth - measuredWidth2) / 2;
        int i7 = (measuredHeight - measuredHeight2) / 2;
        this.g.layout(i6, i7, measuredWidth2 + i6, measuredHeight2 + i7);
        if (z2) {
            d();
            c();
            int height = getHeight();
            int i8 = this.h;
            int i9 = this.S;
            this.W = ((height - i8) / 2) - i9;
            this.a0 = this.W + (i9 * 2) + i8;
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (!this.Q) {
            super.onMeasure(i2, i3);
            return;
        }
        super.onMeasure(a(i2, this.l), a(i3, this.j));
        setMeasuredDimension(a(this.k, getMeasuredWidth(), i2), a(this.i, getMeasuredHeight(), i3));
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!isEnabled() || !this.Q) {
            return false;
        }
        if (this.J == null) {
            this.J = VelocityTracker.obtain();
        }
        this.J.addMovement(motionEvent);
        int action = motionEvent.getAction() & 255;
        if (action == 1) {
            h();
            i();
            this.f0.a();
            VelocityTracker velocityTracker = this.J;
            velocityTracker.computeCurrentVelocity(1000, (float) this.M);
            int yVelocity = (int) velocityTracker.getYVelocity();
            if (Math.abs(yVelocity) > this.L) {
                b(yVelocity);
                f(2);
            } else {
                int y2 = (int) motionEvent.getY();
                if (((int) Math.abs(((float) y2) - this.H)) > this.K) {
                    a();
                } else if (this.V) {
                    this.V = false;
                    j();
                } else {
                    int i2 = (y2 / this.z) - 1;
                    if (i2 > 0) {
                        a(true);
                        this.f0.b(1);
                    } else if (i2 < 0) {
                        a(false);
                        this.f0.b(2);
                    }
                }
                f(0);
            }
            this.J.recycle();
            this.J = null;
        } else if (action == 2 && !this.U) {
            float y3 = motionEvent.getY();
            if (this.T == 1) {
                scrollBy(0, (int) (y3 - this.I));
                invalidate();
            } else if (((int) Math.abs(y3 - this.H)) > this.K) {
                g();
                f(1);
            }
            this.I = y3;
        }
        return true;
    }

    @DexIgnore
    public void scrollBy(int i2, int i3) {
        int[] iArr = this.w;
        if (!this.N && i3 > 0 && iArr[1] <= this.q) {
            this.B = this.A;
        } else if (this.N || i3 >= 0 || iArr[1] < this.r) {
            this.B += i3;
            while (true) {
                int i4 = this.B;
                if (i4 - this.A <= this.o) {
                    break;
                }
                this.B = i4 - this.z;
                a(iArr);
                a(iArr[1], true);
                if (!this.N && iArr[1] <= this.q) {
                    this.B = this.A;
                }
            }
            while (true) {
                int i5 = this.B;
                if (i5 - this.A < (-this.o)) {
                    this.B = i5 + this.z;
                    b(iArr);
                    a(iArr[1], true);
                    if (!this.N && iArr[1] >= this.r) {
                        this.B = this.A;
                    }
                } else {
                    return;
                }
            }
        } else {
            this.B = this.A;
        }
    }

    @DexIgnore
    public void setDisplayedValues(String[] strArr) {
        if (!Arrays.equals(this.p, strArr)) {
            this.p = strArr;
            if (this.p != null) {
                this.g.setRawInputType(524289);
            } else {
                this.g.setRawInputType(2);
            }
            l();
            e();
            k();
        }
    }

    @DexIgnore
    public void setEnabled(boolean z2) {
        super.setEnabled(z2);
        if (!this.Q) {
            ImageButton imageButton = this.e;
            if (imageButton != null) {
                imageButton.setEnabled(z2);
            }
        }
        if (!this.Q) {
            ImageButton imageButton2 = this.f;
            if (imageButton2 != null) {
                imageButton2.setEnabled(z2);
            }
        }
        this.g.setEnabled(z2);
    }

    @DexIgnore
    public void setMaxValue(int i2) {
        if (this.r != i2) {
            if (i2 >= 0) {
                this.r = i2;
                int i3 = this.r;
                if (i3 < this.s) {
                    this.s = i3;
                }
                setWrapSelectorWheel(this.r - this.q > this.w.length);
                e();
                l();
                k();
                invalidate();
                return;
            }
            throw new IllegalArgumentException("maxValue must be >= 0");
        }
    }

    @DexIgnore
    public void setMinValue(int i2) {
        if (this.q != i2) {
            if (i2 >= 0) {
                this.q = i2;
                int i3 = this.q;
                if (i3 > this.s) {
                    this.s = i3;
                }
                setWrapSelectorWheel(this.r - this.q > this.w.length);
                e();
                l();
                k();
                invalidate();
                return;
            }
            throw new IllegalArgumentException("minValue must be >= 0");
        }
    }

    @DexIgnore
    public void setOnValueChangedListener(g gVar) {
        this.t = gVar;
    }

    @DexIgnore
    public void setValue(int i2) {
        a(i2, false);
    }

    @DexIgnore
    public void setWrapSelectorWheel(boolean z2) {
        boolean z3 = this.r - this.q >= this.w.length;
        if ((!z2 || z3) && z2 != this.N) {
            this.N = z2;
        }
    }

    @DexIgnore
    public NumberPicker(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.numberPickerStyle);
    }

    @DexIgnore
    public NumberPicker(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        this.v = new SparseArray<>();
        this.w = new int[3];
        this.A = Integer.MIN_VALUE;
        this.T = 0;
        this.g0 = -1;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.NumberPicker, i2, 0);
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        this.Q = resourceId != 0;
        this.P = obtainStyledAttributes.getColor(8, 0);
        this.O = obtainStyledAttributes.getColor(12, 0);
        this.R = obtainStyledAttributes.getDrawable(9);
        this.S = obtainStyledAttributes.getDimensionPixelSize(10, (int) TypedValue.applyDimension(1, 2.0f, getResources().getDisplayMetrics()));
        this.h = obtainStyledAttributes.getDimensionPixelSize(11, (int) TypedValue.applyDimension(1, 48.0f, getResources().getDisplayMetrics()));
        this.i = obtainStyledAttributes.getDimensionPixelSize(3, -1);
        this.j = obtainStyledAttributes.getDimensionPixelSize(1, -1);
        int i3 = this.i;
        if (i3 != -1) {
            int i4 = this.j;
            if (i4 != -1 && i3 > i4) {
                throw new IllegalArgumentException("minHeight > maxHeight");
            }
        }
        this.k = obtainStyledAttributes.getDimensionPixelSize(4, -1);
        this.l = obtainStyledAttributes.getDimensionPixelSize(2, -1);
        int i5 = this.k;
        if (i5 != -1) {
            int i6 = this.l;
            if (i6 != -1 && i5 > i6) {
                throw new IllegalArgumentException("minWidth > maxWidth");
            }
        }
        this.m = this.l == -1;
        this.y = obtainStyledAttributes.getDrawable(14);
        obtainStyledAttributes.recycle();
        this.f0 = new h();
        setWillNotDraw(!this.Q);
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
        if (layoutInflater != null) {
            layoutInflater.inflate(resourceId, this, true);
        }
        a aVar = new a();
        b bVar = new b();
        if (!this.Q) {
            this.e = (ImageButton) findViewById(R.id.np__increment);
            this.e.setOnClickListener(aVar);
            this.e.setOnLongClickListener(bVar);
        } else {
            this.e = null;
        }
        if (!this.Q) {
            this.f = (ImageButton) findViewById(R.id.np__decrement);
            this.f.setOnClickListener(aVar);
            this.f.setOnLongClickListener(bVar);
        } else {
            this.f = null;
        }
        this.g = (AppCompatEditText) findViewById(R.id.np__numberpicker_input);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.K = viewConfiguration.getScaledTouchSlop();
        this.L = viewConfiguration.getScaledMinimumFlingVelocity();
        this.M = viewConfiguration.getScaledMaximumFlingVelocity() / 8;
        this.n = (int) this.g.getTextSize();
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize((float) this.n);
        paint.setTypeface(this.g.getTypeface());
        this.g.setTextColor(new ColorStateList(new int[][]{new int[]{16842910}, new int[]{-16842910}, new int[]{-16842912}, new int[]{16842919}}, new int[]{getResources().getColor(R.color.gray_dark), getResources().getColor(R.color.gray_dark), getResources().getColor(R.color.gray_dark), getResources().getColor(R.color.gray_dark)}));
        int i7 = this.P;
        if (i7 != 0) {
            this.g.setTextColor(i7);
        }
        paint.setColor(this.g.getTextColors().getColorForState(LinearLayout.ENABLED_STATE_SET, -1));
        this.x = paint;
        this.C = new Scroller(getContext(), (Interpolator) null, true);
        this.D = new Scroller(getContext(), new DecelerateInterpolator(2.5f));
        l();
        if (getImportantForAccessibility() == 0) {
            setImportantForAccessibility(1);
        }
    }

    @DexIgnore
    public String c(int i2) {
        f fVar = this.u;
        return fVar != null ? fVar.format(i2) : g(i2);
    }

    @DexIgnore
    public final void f() {
        d dVar = this.G;
        if (dVar == null) {
            this.G = new d();
        } else {
            removeCallbacks(dVar);
        }
        postDelayed(this.G, (long) ViewConfiguration.getLongPressTimeout());
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements Runnable {
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;

        @DexIgnore
        public h() {
        }

        @DexIgnore
        public void a() {
            this.f = 0;
            this.e = 0;
            NumberPicker.this.removeCallbacks(this);
            NumberPicker numberPicker = NumberPicker.this;
            if (numberPicker.c0) {
                numberPicker.c0 = false;
                numberPicker.invalidate(0, numberPicker.a0, numberPicker.getRight(), NumberPicker.this.getBottom());
            }
        }

        @DexIgnore
        public void b(int i) {
            a();
            this.f = 2;
            this.e = i;
            NumberPicker.this.post(this);
        }

        @DexIgnore
        public void run() {
            int i = this.f;
            if (i == 1) {
                int i2 = this.e;
                if (i2 == 1) {
                    NumberPicker numberPicker = NumberPicker.this;
                    numberPicker.c0 = true;
                    numberPicker.invalidate(0, numberPicker.a0, numberPicker.getRight(), NumberPicker.this.getBottom());
                } else if (i2 == 2) {
                    NumberPicker numberPicker2 = NumberPicker.this;
                    numberPicker2.d0 = true;
                    numberPicker2.invalidate(0, 0, numberPicker2.getRight(), NumberPicker.this.W);
                }
            } else if (i == 2) {
                int i3 = this.e;
                if (i3 == 1) {
                    NumberPicker numberPicker3 = NumberPicker.this;
                    if (!numberPicker3.c0) {
                        numberPicker3.postDelayed(this, (long) ViewConfiguration.getPressedStateDuration());
                    }
                    NumberPicker numberPicker4 = NumberPicker.this;
                    numberPicker4.c0 = !numberPicker4.c0;
                    numberPicker4.invalidate(0, numberPicker4.a0, numberPicker4.getRight(), NumberPicker.this.getBottom());
                } else if (i3 == 2) {
                    NumberPicker numberPicker5 = NumberPicker.this;
                    if (!numberPicker5.d0) {
                        numberPicker5.postDelayed(this, (long) ViewConfiguration.getPressedStateDuration());
                    }
                    NumberPicker numberPicker6 = NumberPicker.this;
                    numberPicker6.d0 = !numberPicker6.d0;
                    numberPicker6.invalidate(0, 0, numberPicker6.getRight(), NumberPicker.this.W);
                }
            }
        }

        @DexIgnore
        public void a(int i) {
            a();
            this.f = 1;
            this.e = i;
            NumberPicker.this.postDelayed(this, (long) ViewConfiguration.getTapTimeout());
        }
    }

    @DexIgnore
    public static String g(int i2) {
        return String.format(Locale.getDefault(), "%d", new Object[]{Integer.valueOf(i2)});
    }

    @DexIgnore
    public final void b(Scroller scroller) {
        if (Objects.equals(scroller, this.C)) {
            if (!a()) {
                l();
            }
            f(0);
        } else if (this.T != 1) {
            l();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends AccessibilityNodeProvider {
        @DexIgnore
        public /* final */ Rect a; // = new Rect();
        @DexIgnore
        public /* final */ int[] b; // = new int[2];
        @DexIgnore
        public int c; // = Integer.MIN_VALUE;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(int i, int i2) {
            if (i != 1) {
                if (i == 2) {
                    a(i2);
                } else if (i == 3 && d()) {
                    a(i, i2, b());
                }
            } else if (e()) {
                a(i, i2, c());
            }
        }

        @DexIgnore
        public final String b() {
            NumberPicker numberPicker = NumberPicker.this;
            int i = numberPicker.s - 1;
            if (numberPicker.N) {
                i = numberPicker.d(i);
            }
            NumberPicker numberPicker2 = NumberPicker.this;
            int i2 = numberPicker2.q;
            if (i < i2) {
                return null;
            }
            String[] strArr = numberPicker2.p;
            return strArr == null ? numberPicker2.c(i) : strArr[i - i2];
        }

        @DexIgnore
        public final String c() {
            NumberPicker numberPicker = NumberPicker.this;
            int i = numberPicker.s + 1;
            if (numberPicker.N) {
                i = numberPicker.d(i);
            }
            NumberPicker numberPicker2 = NumberPicker.this;
            if (i > numberPicker2.r) {
                return null;
            }
            String[] strArr = numberPicker2.p;
            return strArr == null ? numberPicker2.c(i) : strArr[i - numberPicker2.q];
        }

        @DexIgnore
        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            if (i == -1) {
                return a(NumberPicker.this.getScrollX(), NumberPicker.this.getScrollY(), NumberPicker.this.getScrollX() + (NumberPicker.this.getRight() - NumberPicker.this.getLeft()), NumberPicker.this.getScrollY() + (NumberPicker.this.getBottom() - NumberPicker.this.getTop()));
            }
            if (i == 1) {
                String c2 = c();
                int scrollX = NumberPicker.this.getScrollX();
                NumberPicker numberPicker = NumberPicker.this;
                return a(1, c2, scrollX, numberPicker.a0 - numberPicker.S, numberPicker.getScrollX() + (NumberPicker.this.getRight() - NumberPicker.this.getLeft()), NumberPicker.this.getScrollY() + (NumberPicker.this.getBottom() - NumberPicker.this.getTop()));
            } else if (i == 2) {
                return a();
            } else {
                if (i != 3) {
                    return super.createAccessibilityNodeInfo(i);
                }
                String b2 = b();
                int scrollX2 = NumberPicker.this.getScrollX();
                int scrollY = NumberPicker.this.getScrollY();
                int scrollX3 = NumberPicker.this.getScrollX() + (NumberPicker.this.getRight() - NumberPicker.this.getLeft());
                NumberPicker numberPicker2 = NumberPicker.this;
                return a(3, b2, scrollX2, scrollY, scrollX3, numberPicker2.W + numberPicker2.S);
            }
        }

        @DexIgnore
        public final boolean d() {
            return NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() > NumberPicker.this.getMinValue();
        }

        @DexIgnore
        public final boolean e() {
            return NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() < NumberPicker.this.getMaxValue();
        }

        @DexIgnore
        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            if (TextUtils.isEmpty(str)) {
                return Collections.emptyList();
            }
            String lowerCase = str.toLowerCase();
            ArrayList arrayList = new ArrayList();
            if (i == -1) {
                a(lowerCase, 3, (List<AccessibilityNodeInfo>) arrayList);
                a(lowerCase, 2, (List<AccessibilityNodeInfo>) arrayList);
                a(lowerCase, 1, (List<AccessibilityNodeInfo>) arrayList);
                return arrayList;
            } else if (i != 1 && i != 2 && i != 3) {
                return super.findAccessibilityNodeInfosByText(str, i);
            } else {
                a(lowerCase, i, (List<AccessibilityNodeInfo>) arrayList);
                return arrayList;
            }
        }

        @DexIgnore
        public boolean performAction(int i, int i2, Bundle bundle) {
            if (i != -1) {
                if (i != 1) {
                    if (i != 2) {
                        if (i == 3) {
                            if (i2 != 16) {
                                if (i2 != 64) {
                                    if (i2 != 128 || this.c != i) {
                                        return false;
                                    }
                                    this.c = Integer.MIN_VALUE;
                                    a(i, 65536);
                                    NumberPicker numberPicker = NumberPicker.this;
                                    numberPicker.invalidate(0, 0, numberPicker.getRight(), NumberPicker.this.W);
                                    return true;
                                } else if (this.c == i) {
                                    return false;
                                } else {
                                    this.c = i;
                                    a(i, 32768);
                                    NumberPicker numberPicker2 = NumberPicker.this;
                                    numberPicker2.invalidate(0, 0, numberPicker2.getRight(), NumberPicker.this.W);
                                    return true;
                                }
                            } else if (!NumberPicker.this.isEnabled()) {
                                return false;
                            } else {
                                NumberPicker.this.a(false);
                                a(i, 1);
                                return true;
                            }
                        }
                    } else if (i2 != 1) {
                        if (i2 != 2) {
                            if (i2 != 16) {
                                if (i2 != 64) {
                                    if (i2 != 128) {
                                        return NumberPicker.this.g.performAccessibilityAction(i2, bundle);
                                    }
                                    if (this.c != i) {
                                        return false;
                                    }
                                    this.c = Integer.MIN_VALUE;
                                    a(i, 65536);
                                    NumberPicker.this.g.invalidate();
                                    return true;
                                } else if (this.c == i) {
                                    return false;
                                } else {
                                    this.c = i;
                                    a(i, 32768);
                                    NumberPicker.this.g.invalidate();
                                    return true;
                                }
                            } else if (!NumberPicker.this.isEnabled()) {
                                return false;
                            } else {
                                NumberPicker.this.j();
                                return true;
                            }
                        } else if (!NumberPicker.this.isEnabled() || !NumberPicker.this.g.isFocused()) {
                            return false;
                        } else {
                            NumberPicker.this.g.clearFocus();
                            return true;
                        }
                    } else if (!NumberPicker.this.isEnabled() || NumberPicker.this.g.isFocused()) {
                        return false;
                    } else {
                        return NumberPicker.this.g.requestFocus();
                    }
                } else if (i2 != 16) {
                    if (i2 != 64) {
                        if (i2 != 128 || this.c != i) {
                            return false;
                        }
                        this.c = Integer.MIN_VALUE;
                        a(i, 65536);
                        NumberPicker numberPicker3 = NumberPicker.this;
                        numberPicker3.invalidate(0, numberPicker3.a0, numberPicker3.getRight(), NumberPicker.this.getBottom());
                        return true;
                    } else if (this.c == i) {
                        return false;
                    } else {
                        this.c = i;
                        a(i, 32768);
                        NumberPicker numberPicker4 = NumberPicker.this;
                        numberPicker4.invalidate(0, numberPicker4.a0, numberPicker4.getRight(), NumberPicker.this.getBottom());
                        return true;
                    }
                } else if (!NumberPicker.this.isEnabled()) {
                    return false;
                } else {
                    NumberPicker.this.a(true);
                    a(i, 1);
                    return true;
                }
            } else if (i2 != 64) {
                if (i2 != 128) {
                    if (i2 != 4096) {
                        if (i2 == 8192) {
                            if (!NumberPicker.this.isEnabled() || (!NumberPicker.this.getWrapSelectorWheel() && NumberPicker.this.getValue() <= NumberPicker.this.getMinValue())) {
                                return false;
                            }
                            NumberPicker.this.a(false);
                            return true;
                        }
                    } else if (!NumberPicker.this.isEnabled() || (!NumberPicker.this.getWrapSelectorWheel() && NumberPicker.this.getValue() >= NumberPicker.this.getMaxValue())) {
                        return false;
                    } else {
                        NumberPicker.this.a(true);
                        return true;
                    }
                } else if (this.c != i) {
                    return false;
                } else {
                    this.c = Integer.MIN_VALUE;
                    NumberPicker.this.performAccessibilityAction(128, (Bundle) null);
                    return true;
                }
            } else if (this.c == i) {
                return false;
            } else {
                this.c = i;
                NumberPicker.this.performAccessibilityAction(64, (Bundle) null);
                return true;
            }
            return super.performAction(i, i2, bundle);
        }

        @DexIgnore
        public final void a(int i) {
            if (((AccessibilityManager) NumberPicker.this.getContext().getSystemService("accessibility")).isEnabled()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i);
                NumberPicker.this.g.onInitializeAccessibilityEvent(obtain);
                NumberPicker.this.g.onPopulateAccessibilityEvent(obtain);
                obtain.setSource(NumberPicker.this, 2);
                NumberPicker numberPicker = NumberPicker.this;
                numberPicker.requestSendAccessibilityEvent(numberPicker, obtain);
            }
        }

        @DexIgnore
        public final void a(int i, int i2, String str) {
            if (((AccessibilityManager) NumberPicker.this.getContext().getSystemService("accessibility")).isEnabled()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
                obtain.setClassName(Button.class.getName());
                obtain.setPackageName(NumberPicker.this.getContext().getPackageName());
                obtain.getText().add(str);
                obtain.setEnabled(NumberPicker.this.isEnabled());
                obtain.setSource(NumberPicker.this, i);
                NumberPicker numberPicker = NumberPicker.this;
                numberPicker.requestSendAccessibilityEvent(numberPicker, obtain);
            }
        }

        @DexIgnore
        public final void a(String str, int i, List<AccessibilityNodeInfo> list) {
            if (i == 1) {
                String c2 = c();
                if (!TextUtils.isEmpty(c2) && c2.toLowerCase().contains(str)) {
                    list.add(createAccessibilityNodeInfo(1));
                }
            } else if (i == 2) {
                Editable text = NumberPicker.this.g.getText();
                if (TextUtils.isEmpty(text) || !text.toString().toLowerCase().contains(str)) {
                    Editable text2 = NumberPicker.this.g.getText();
                    if (!TextUtils.isEmpty(text2) && text2.toString().toLowerCase().contains(str)) {
                        list.add(createAccessibilityNodeInfo(2));
                        return;
                    }
                    return;
                }
                list.add(createAccessibilityNodeInfo(2));
            } else if (i == 3) {
                String b2 = b();
                if (!TextUtils.isEmpty(b2) && b2.toLowerCase().contains(str)) {
                    list.add(createAccessibilityNodeInfo(3));
                }
            }
        }

        @DexIgnore
        public final AccessibilityNodeInfo a() {
            AccessibilityNodeInfo createAccessibilityNodeInfo = NumberPicker.this.g.createAccessibilityNodeInfo();
            createAccessibilityNodeInfo.setSource(NumberPicker.this, 2);
            if (this.c != 2) {
                createAccessibilityNodeInfo.addAction(64);
            }
            if (this.c == 2) {
                createAccessibilityNodeInfo.addAction(128);
            }
            return createAccessibilityNodeInfo;
        }

        @DexIgnore
        public final AccessibilityNodeInfo a(int i, String str, int i2, int i3, int i4, int i5) {
            AccessibilityNodeInfo obtain = AccessibilityNodeInfo.obtain();
            obtain.setClassName(Button.class.getName());
            obtain.setPackageName(NumberPicker.this.getContext().getPackageName());
            obtain.setSource(NumberPicker.this, i);
            obtain.setParent(NumberPicker.this);
            obtain.setText(str);
            obtain.setClickable(true);
            obtain.setLongClickable(true);
            obtain.setEnabled(NumberPicker.this.isEnabled());
            Rect rect = this.a;
            rect.set(i2, i3, i4, i5);
            obtain.setBoundsInParent(rect);
            int[] iArr = this.b;
            NumberPicker.this.getLocationOnScreen(iArr);
            rect.offset(iArr[0], iArr[1]);
            obtain.setBoundsInScreen(rect);
            if (this.c != i) {
                obtain.addAction(64);
            }
            if (this.c == i) {
                obtain.addAction(128);
            }
            if (NumberPicker.this.isEnabled()) {
                obtain.addAction(16);
            }
            return obtain;
        }

        @DexIgnore
        public final AccessibilityNodeInfo a(int i, int i2, int i3, int i4) {
            AccessibilityNodeInfo obtain = AccessibilityNodeInfo.obtain();
            obtain.setClassName(NumberPicker.class.getName());
            obtain.setPackageName(NumberPicker.this.getContext().getPackageName());
            obtain.setSource(NumberPicker.this);
            if (d()) {
                obtain.addChild(NumberPicker.this, 3);
            }
            obtain.addChild(NumberPicker.this, 2);
            if (e()) {
                obtain.addChild(NumberPicker.this, 1);
            }
            obtain.setParent((View) NumberPicker.this.getParentForAccessibility());
            obtain.setEnabled(NumberPicker.this.isEnabled());
            obtain.setScrollable(true);
            if (this.c != -1) {
                obtain.addAction(64);
            }
            if (this.c == -1) {
                obtain.addAction(128);
            }
            if (NumberPicker.this.isEnabled()) {
                if (NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() < NumberPicker.this.getMaxValue()) {
                    obtain.addAction(4096);
                }
                if (NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() > NumberPicker.this.getMinValue()) {
                    obtain.addAction(8192);
                }
            }
            return obtain;
        }
    }

    @DexIgnore
    public final int a(int i2, int i3) {
        if (i3 == -1) {
            return i2;
        }
        int size = View.MeasureSpec.getSize(i2);
        int mode = View.MeasureSpec.getMode(i2);
        if (mode == Integer.MIN_VALUE) {
            return View.MeasureSpec.makeMeasureSpec(Math.min(size, i3), 1073741824);
        }
        if (mode == 0) {
            return View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
        }
        if (mode == 1073741824) {
            return i2;
        }
        throw new IllegalArgumentException("Unknown measure mode: " + mode);
    }

    @DexIgnore
    public final void e(int i2) {
        g gVar = this.t;
        if (gVar != null) {
            gVar.a(this, i2, this.s);
        }
    }

    @DexIgnore
    public final int a(int i2, int i3, int i4) {
        return i2 != -1 ? resolveSizeAndState(Math.max(i2, i3), i4, 0) : i3;
    }

    @DexIgnore
    public final void b(int i2) {
        this.E = 0;
        if (i2 > 0) {
            this.C.fling(0, 0, 0, i2, 0, 0, 0, Integer.MAX_VALUE);
        } else {
            this.C.fling(0, Integer.MAX_VALUE, 0, i2, 0, 0, 0, Integer.MAX_VALUE);
        }
        invalidate();
    }

    @DexIgnore
    public int d(int i2) {
        int i3 = this.r;
        if (i2 > i3) {
            int i4 = this.q;
            return (i4 + ((i2 - i3) % (i3 - i4))) - 1;
        }
        int i5 = this.q;
        return i2 < i5 ? (i3 - ((i5 - i2) % (i3 - i5))) + 1 : i2;
    }

    @DexIgnore
    public final void a(int i2, boolean z2) {
        int i3;
        if (this.s != i2) {
            if (this.N) {
                i3 = d(i2);
            } else {
                i3 = Math.min(Math.max(i2, this.q), this.r);
            }
            int i4 = this.s;
            this.s = i3;
            l();
            if (z2) {
                e(i4);
            }
            e();
            invalidate();
        }
    }

    @DexIgnore
    public final void b(int[] iArr) {
        System.arraycopy(iArr, 1, iArr, 0, iArr.length - 1);
        int i2 = iArr[iArr.length - 2] + 1;
        if (this.N && i2 > this.r) {
            i2 = this.q;
        }
        iArr[iArr.length - 1] = i2;
        a(i2);
    }

    @DexIgnore
    public void a(boolean z2) {
        if (this.Q) {
            this.g.setVisibility(4);
            if (!a(this.C)) {
                a(this.D);
            }
            this.E = 0;
            if (z2) {
                this.C.startScroll(0, 0, 0, -this.z, 300);
            } else {
                this.C.startScroll(0, 0, 0, this.z, 300);
            }
            invalidate();
        } else if (z2) {
            a(this.s + 1, true);
        } else {
            a(this.s - 1, true);
        }
    }

    @DexIgnore
    public final void a(int[] iArr) {
        System.arraycopy(iArr, 0, iArr, 1, iArr.length - 1);
        int i2 = iArr[1] - 1;
        if (this.N && i2 < this.q) {
            i2 = this.r;
        }
        iArr[0] = i2;
        a(i2);
    }

    @DexIgnore
    public final void a(int i2) {
        String str;
        SparseArray<String> sparseArray = this.v;
        if (sparseArray.get(i2) == null) {
            int i3 = this.q;
            if (i2 < i3 || i2 > this.r) {
                str = "";
            } else {
                String[] strArr = this.p;
                str = strArr != null ? strArr[i2 - i3] : c(i2);
            }
            sparseArray.put(i2, str);
        }
    }

    @DexIgnore
    public void a(boolean z2, long j2) {
        e eVar = this.F;
        if (eVar == null) {
            this.F = new e();
        } else {
            removeCallbacks(eVar);
        }
        this.F.a(z2);
        postDelayed(this.F, j2);
    }

    @DexIgnore
    public final boolean a() {
        int i2 = this.A - this.B;
        if (i2 == 0) {
            return false;
        }
        this.E = 0;
        int abs = Math.abs(i2);
        int i3 = this.z;
        if (abs > i3 / 2) {
            if (i2 > 0) {
                i3 = -i3;
            }
            i2 += i3;
        }
        this.D.startScroll(0, 0, 0, i2, 800);
        invalidate();
        return true;
    }

    @DexIgnore
    public final void a(Paint paint, float f2, String str) {
        paint.setTextSize((float) this.n);
        Rect rect = new Rect();
        paint.getTextBounds(str, 0, str.length(), rect);
        if (((float) rect.width()) >= f2) {
            paint.setTextSize((paint.getTextSize() * f2) / ((float) rect.width()));
        }
    }
}
