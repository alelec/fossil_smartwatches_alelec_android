package com.portfolio.platform.view.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepWeekDetailsChart$draw$1$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.yc4<java.lang.Integer, java.util.List<? extends java.lang.Integer>, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $bottomTextHeight;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $charactersCenterXList;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepWeekDetailsChart$draw$1$1(kotlin.jvm.internal.Ref$IntRef ref$IntRef, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef) {
        super(2);
        this.$bottomTextHeight = ref$IntRef;
        this.$charactersCenterXList = ref$ObjectRef;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        invoke(((java.lang.Number) obj).intValue(), (java.util.List<java.lang.Integer>) (java.util.List) obj2);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(int i, java.util.List<java.lang.Integer> list) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "list");
        this.$bottomTextHeight.element = i;
        this.$charactersCenterXList.element = list;
    }
}
