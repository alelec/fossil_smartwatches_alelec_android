package com.portfolio.platform.view.fitness;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yc4;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import kotlin.jvm.internal.Ref$IntRef;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityWeekDetailsChart$draw$Anon1$Anon1 extends Lambda implements yc4<Integer, List<? extends Integer>, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ Ref$IntRef $bottomTextHeight;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$ObjectRef $charactersCenterXList;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityWeekDetailsChart$draw$Anon1$Anon1(Ref$IntRef ref$IntRef, Ref$ObjectRef ref$ObjectRef) {
        super(2);
        this.$bottomTextHeight = ref$IntRef;
        this.$charactersCenterXList = ref$ObjectRef;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke(((Number) obj).intValue(), (List<Integer>) (List) obj2);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(int i, List<Integer> list) {
        kd4.b(list, "list");
        this.$bottomTextHeight.element = i;
        this.$charactersCenterXList.element = list;
    }
}
