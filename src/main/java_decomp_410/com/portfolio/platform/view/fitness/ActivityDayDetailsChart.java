package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.il2;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.r6;
import com.fossil.blesdk.obfuscated.ut3;
import com.fossil.blesdk.obfuscated.wr3;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.enums.GoalType;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import kotlin.Triple;
import kotlin.jvm.internal.Ref$FloatRef;
import kotlin.jvm.internal.Ref$IntRef;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityDayDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String j0;
    @DexIgnore
    public /* final */ Paint A;
    @DexIgnore
    public /* final */ Paint B;
    @DexIgnore
    public /* final */ Paint C;
    @DexIgnore
    public /* final */ Paint D;
    @DexIgnore
    public /* final */ Paint E;
    @DexIgnore
    public /* final */ Paint F;
    @DexIgnore
    public /* final */ Paint G;
    @DexIgnore
    public /* final */ Paint H;
    @DexIgnore
    public /* final */ int I;
    @DexIgnore
    public /* final */ String J;
    @DexIgnore
    public /* final */ String K;
    @DexIgnore
    public /* final */ int L;
    @DexIgnore
    public int M;
    @DexIgnore
    public /* final */ int N;
    @DexIgnore
    public /* final */ int O;
    @DexIgnore
    public /* final */ int P;
    @DexIgnore
    public /* final */ Typeface Q;
    @DexIgnore
    public float R;
    @DexIgnore
    public float S;
    @DexIgnore
    public float T;
    @DexIgnore
    public float U;
    @DexIgnore
    public float V;
    @DexIgnore
    public int W;
    @DexIgnore
    public float a0;
    @DexIgnore
    public float b0;
    @DexIgnore
    public float c0;
    @DexIgnore
    public float d0;
    @DexIgnore
    public float e0;
    @DexIgnore
    public float f0;
    @DexIgnore
    public float g0;
    @DexIgnore
    public float h;
    @DexIgnore
    public int h0;
    @DexIgnore
    public float i;
    @DexIgnore
    public int i0;
    @DexIgnore
    public float j;
    @DexIgnore
    public float k;
    @DexIgnore
    public float l;
    @DexIgnore
    public List<Triple<Integer, Float, Triple<Float, Float, Float>>> m;
    @DexIgnore
    public /* final */ int n;
    @DexIgnore
    public /* final */ int o;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public GoalType x;
    @DexIgnore
    public /* final */ Paint y;
    @DexIgnore
    public /* final */ Paint z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = ActivityDayDetailsChart.class.getSimpleName();
        kd4.a((Object) simpleName, "ActivityDayDetailsChart::class.java.simpleName");
        j0 = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityDayDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        kd4.b(context, "context");
        this.i = 0.5f;
        this.x = GoalType.ACTIVE_TIME;
        this.y = new Paint(1);
        this.z = new Paint(1);
        this.A = new Paint(1);
        this.B = new Paint(1);
        this.C = new Paint(1);
        this.D = new Paint(1);
        this.E = new Paint(1);
        this.F = new Paint(1);
        this.G = new Paint(1);
        this.H = new Paint(1);
        this.I = context.getResources().getDimensionPixelSize(R.dimen.dp5);
        pd4 pd4 = pd4.a;
        String string = context.getString(R.string.am_hour);
        kd4.a((Object) string, "context.getString(R.string.am_hour)");
        Object[] objArr = {12};
        String format = String.format(string, Arrays.copyOf(objArr, objArr.length));
        kd4.a((Object) format, "java.lang.String.format(format, *args)");
        this.J = format;
        pd4 pd42 = pd4.a;
        String string2 = context.getString(R.string.pm_hour);
        kd4.a((Object) string2, "context.getString(R.string.pm_hour)");
        Object[] objArr2 = {12};
        String format2 = String.format(string2, Arrays.copyOf(objArr2, objArr2.length));
        kd4.a((Object) format2, "java.lang.String.format(format, *args)");
        this.K = format2;
        this.L = k6.a(context, (int) R.color.hexE19180);
        this.M = 4;
        this.N = context.getResources().getDimensionPixelSize(R.dimen.dp5);
        this.O = context.getResources().getDimensionPixelSize(R.dimen.dp6);
        this.P = context.getResources().getDimensionPixelSize(R.dimen.dp15);
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, h62.ActivityDayDetailsChart, 0, 0));
        }
        this.Q = r6.a(context, R.font.font_regular);
        int a2 = k6.a(context, (int) R.color.gray_30);
        int a3 = k6.a(context, (int) R.color.hexE19180);
        int a4 = k6.a(context, (int) R.color.steps);
        int a5 = k6.a(context, (int) R.color.hexBF3214);
        TypedArray mTypedArray = getMTypedArray();
        this.q = mTypedArray != null ? mTypedArray.getColor(4, a3) : a3;
        TypedArray mTypedArray2 = getMTypedArray();
        this.r = mTypedArray2 != null ? mTypedArray2.getColor(5, a4) : a4;
        TypedArray mTypedArray3 = getMTypedArray();
        this.s = mTypedArray3 != null ? mTypedArray3.getColor(3, a5) : a5;
        TypedArray mTypedArray4 = getMTypedArray();
        this.t = mTypedArray4 != null ? mTypedArray4.getColor(1, a4) : a4;
        TypedArray mTypedArray5 = getMTypedArray();
        this.u = mTypedArray5 != null ? mTypedArray5.getColor(0, a4) : a4;
        TypedArray mTypedArray6 = getMTypedArray();
        this.n = mTypedArray6 != null ? mTypedArray6.getColor(7, a2) : a2;
        TypedArray mTypedArray7 = getMTypedArray();
        this.o = mTypedArray7 != null ? mTypedArray7.getColor(6, a3) : a3;
        TypedArray mTypedArray8 = getMTypedArray();
        this.p = mTypedArray8 != null ? mTypedArray8.getColor(2, a2) : a2;
        TypedArray mTypedArray9 = getMTypedArray();
        this.v = mTypedArray9 != null ? mTypedArray9.getColor(8, a2) : a2;
        TypedArray mTypedArray10 = getMTypedArray();
        this.w = mTypedArray10 != null ? mTypedArray10.getDimensionPixelSize(9, 40) : 40;
        TypedArray mTypedArray11 = getMTypedArray();
        if (mTypedArray11 != null) {
            mTypedArray11.recycle();
        }
    }

    @DexIgnore
    private final float getGoalForEachBar() {
        return this.l / ((float) 16);
    }

    @DexIgnore
    private final String getMHighLevelText() {
        int i2 = ut3.a[this.x.ordinal()];
        int i3 = i2 != 1 ? i2 != 2 ? R.string.minutes : R.string.kilo_calories : R.string.miles;
        pd4 pd4 = pd4.a;
        String string = getContext().getString(i3);
        kd4.a((Object) string, "context.getString(rawText)");
        Object[] objArr = {il2.a(this.k, 3)};
        String format = String.format(string, Arrays.copyOf(objArr, objArr.length));
        kd4.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    private final String getMLowLevelText() {
        int i2 = ut3.b[this.x.ordinal()];
        int i3 = i2 != 1 ? i2 != 2 ? R.string.minutes : R.string.kilo_calories : R.string.miles;
        pd4 pd4 = pd4.a;
        String string = getContext().getString(i3);
        kd4.a((Object) string, "context.getString(rawText)");
        Object[] objArr = {il2.a(this.j, 3)};
        String format = String.format(string, Arrays.copyOf(objArr, objArr.length));
        kd4.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    private final float getMMax() {
        float f;
        T t2;
        List<Triple<Integer, Float, Triple<Float, Float, Float>>> list = this.m;
        if (list != null) {
            Iterator<T> it = list.iterator();
            if (!it.hasNext()) {
                t2 = null;
            } else {
                t2 = it.next();
                if (it.hasNext()) {
                    float floatValue = ((Number) ((Triple) t2).getSecond()).floatValue();
                    do {
                        T next = it.next();
                        float floatValue2 = ((Number) ((Triple) next).getSecond()).floatValue();
                        if (Float.compare(floatValue, floatValue2) < 0) {
                            t2 = next;
                            floatValue = floatValue2;
                        }
                    } while (it.hasNext());
                }
            }
            Triple triple = (Triple) t2;
            if (triple != null) {
                Float f2 = (Float) triple.getSecond();
                if (f2 != null) {
                    f = f2.floatValue();
                    return Math.max(Math.max(getGoalForEachBar(), f), this.k);
                }
            }
        }
        f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        return Math.max(Math.max(getGoalForEachBar(), f), this.k);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, float f, float f2, float f3, float f4, Triple<Integer, Float, Triple<Float, Float, Float>> triple, GoalType goalType, boolean z2, xc4<? super RectF, qa4> xc4) {
        Paint paint;
        Canvas canvas2 = canvas;
        int i3 = i2;
        float f5 = f4;
        xc4<? super RectF, qa4> xc42 = xc4;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j0;
        local.d(str, "Maximum: " + f5 + ", value: " + triple);
        float f6 = f / ((float) 2);
        float f7 = (float) 0;
        if (triple.getSecond().floatValue() <= f7) {
            xc42.invoke(new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            return;
        }
        float floatValue = triple.getSecond().floatValue() / f5;
        if (floatValue > 1.0f) {
            floatValue = 1.0f;
        }
        float f8 = (float) this.M;
        float f9 = (float) i3;
        float max = Math.max(f9 * floatValue, f6);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = j0;
        local2.d(str2, "Percentage: " + floatValue + ", chart height: " + i3 + ", real bar height: " + max);
        float max2 = Math.max((f9 - max) + f8, f8 + ((float) (z2 ? getStartBitmap().getHeight() * 2 : 0)));
        float round = (float) Math.round(f2);
        float round2 = (float) Math.round(f3);
        RectF rectF = new RectF(round, max2, round2, f9);
        Triple third = triple.getThird();
        if (third == null) {
            if (ut3.e[goalType.ordinal()] != 1) {
                paint = this.G;
            } else {
                paint = this.H;
            }
            wr3.c(canvas2, rectF, paint, f6);
        } else {
            wr3.c(canvas2, rectF, this.D, f6);
            float floatValue2 = (((Number) third.getThird()).floatValue() / triple.getSecond().floatValue()) * max;
            float floatValue3 = ((((Number) third.getThird()).floatValue() + ((Number) third.getSecond()).floatValue()) / triple.getSecond().floatValue()) * max;
            if (floatValue3 > f7) {
                wr3.c(canvas2, new RectF(round, f9 - floatValue3, round2, f9), this.E, f6);
            }
            if (floatValue2 > f7) {
                wr3.c(canvas2, new RectF(round, f9 - floatValue2, round2, f9), this.F, f6);
            }
        }
        xc42.invoke(rectF);
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        Paint paint = this.A;
        String str = this.J;
        this.R = paint.measureText(str, 0, StringsKt__StringsKt.c(str));
        this.S = ((float) getHeight()) - ((float) this.I);
        this.U = ((float) (getWidth() / 2)) - (this.R / ((float) 2));
        this.T = (float) this.I;
        this.V = (((float) getWidth()) - this.R) - ((float) (this.I * 3));
        Rect rect = new Rect();
        Paint paint2 = this.A;
        String str2 = this.J;
        paint2.getTextBounds(str2, 0, StringsKt__StringsKt.c(str2), rect);
        this.M = rect.height();
        this.W = (getHeight() - rect.height()) - (this.I * 2);
        Rect rect2 = new Rect();
        Rect rect3 = new Rect();
        int i2 = 1;
        this.A.getTextBounds(getMHighLevelText(), 0, StringsKt__StringsKt.c(getMHighLevelText()) > 0 ? StringsKt__StringsKt.c(getMHighLevelText()) : 1, rect2);
        Paint paint3 = this.A;
        String mLowLevelText = getMLowLevelText();
        if (StringsKt__StringsKt.c(getMLowLevelText()) > 0) {
            i2 = StringsKt__StringsKt.c(getMLowLevelText());
        }
        paint3.getTextBounds(mLowLevelText, 0, i2, rect3);
        this.a0 = (((float) getWidth()) - Math.max(this.A.measureText(getMHighLevelText()), this.A.measureText(getMLowLevelText()))) - ((float) (this.I * 3));
        this.i0 = 0;
        int i3 = this.W;
        this.h0 = i3 - 4;
        this.e0 = Math.max(((float) i3) * this.h, (float) this.M);
        this.b0 = Math.max(((float) this.W) * this.i, (float) this.M);
        if (this.b0 < this.e0 + ((float) rect2.height())) {
            this.e0 = this.b0 - ((float) rect2.height());
        }
        float f = this.a0;
        int i4 = this.I;
        this.c0 = ((float) i4) + f;
        this.f0 = f + ((float) i4);
        this.d0 = this.b0 + ((float) (rect3.height() / 2));
        this.g0 = this.e0 + ((float) (rect2.height() / 2));
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Canvas canvas2;
        Canvas canvas3 = canvas;
        super.draw(canvas);
        if (canvas3 != null) {
            canvas3.drawText(this.J, this.T, this.S, this.A);
            canvas3.drawText(this.K, this.U, this.S, this.A);
            canvas3.drawText(this.J, this.V, this.S, this.A);
            List<Triple<Integer, Float, Triple<Float, Float, Float>>> list = this.m;
            if (list != null) {
                float f = this.a0 / ((float) 2);
                int i2 = this.N;
                float f2 = (float) (i2 / 2);
                float f3 = f - f2;
                float f4 = (f + ((float) i2)) - f2;
                Ref$IntRef ref$IntRef = new Ref$IntRef();
                ref$IntRef.element = -1;
                int size = list.size();
                int i3 = 0;
                float f5 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                while (true) {
                    if (i3 >= size) {
                        break;
                    }
                    f5 += ((Number) list.get(i3).getSecond()).floatValue();
                    if (f5 >= this.l) {
                        FLogger.INSTANCE.getLocal().d(j0, "Goal reach at " + i3);
                        ref$IntRef.element = i3;
                        break;
                    }
                    i3++;
                }
                Ref$FloatRef ref$FloatRef = new Ref$FloatRef();
                ref$FloatRef.element = -1.0f;
                Ref$FloatRef ref$FloatRef2 = new Ref$FloatRef();
                ref$FloatRef2.element = -1.0f;
                float mMax = getMMax();
                FLogger.INSTANCE.getLocal().d(j0, "Data max: " + getMMax() + ", goal for each bar: " + getGoalForEachBar() + ", chart max: " + mMax);
                int i4 = this.O;
                float f6 = f3;
                int i5 = 11;
                while (true) {
                    boolean z2 = true;
                    if (i5 < 0) {
                        break;
                    }
                    Triple triple = list.get(i5);
                    int i6 = this.W;
                    float f7 = (float) i4;
                    float f8 = f6 - f7;
                    GoalType goalType = this.x;
                    if (i5 != ref$IntRef.element) {
                        z2 = false;
                    }
                    int i7 = i4;
                    a(canvas, i6, f7, f8, f6, mMax, triple, goalType, z2, new ActivityDayDetailsChart$draw$Anon1$Anon1$Anon1(i5, ref$IntRef, ref$FloatRef2, ref$FloatRef));
                    f6 -= (float) (i7 + this.N);
                    i5--;
                    ref$IntRef = ref$IntRef;
                    ref$FloatRef2 = ref$FloatRef2;
                    i4 = i7;
                    ref$FloatRef = ref$FloatRef;
                    mMax = mMax;
                    Canvas canvas4 = canvas;
                }
                int i8 = i4;
                float f9 = mMax;
                Ref$FloatRef ref$FloatRef3 = ref$FloatRef2;
                Ref$FloatRef ref$FloatRef4 = ref$FloatRef;
                Ref$IntRef ref$IntRef2 = ref$IntRef;
                int i9 = 12;
                for (int i10 = 24; i9 < i10; i10 = 24) {
                    Triple triple2 = list.get(i9);
                    float f10 = (float) i8;
                    Ref$FloatRef ref$FloatRef5 = ref$FloatRef4;
                    a(canvas, this.W, f10, f4, f4 + f10, f9, triple2, this.x, i9 == ref$IntRef2.element, new ActivityDayDetailsChart$draw$Anon1$Anon1$Anon2(i9, ref$IntRef2, ref$FloatRef3, ref$FloatRef5));
                    f4 += (float) (i8 + this.N);
                    i9++;
                    ref$FloatRef4 = ref$FloatRef5;
                    ref$IntRef2 = ref$IntRef2;
                }
                Ref$FloatRef ref$FloatRef6 = ref$FloatRef4;
                float f11 = (float) 0;
                if (ref$FloatRef3.element < f11 || ref$FloatRef6.element < f11) {
                    canvas2 = canvas;
                } else {
                    int height = getStartBitmap().getHeight();
                    int width = getStartBitmap().getWidth();
                    float f12 = (float) (height + 10);
                    if (ref$FloatRef3.element >= f12) {
                        canvas2 = canvas;
                        Ref$FloatRef ref$FloatRef7 = ref$FloatRef6;
                        canvas2.drawBitmap(getStartBitmap(), ref$FloatRef6.element - ((float) (width / 2)), (float) 10, this.B);
                        Path path = new Path();
                        path.moveTo(ref$FloatRef7.element, f12);
                        path.lineTo(ref$FloatRef7.element, ref$FloatRef3.element);
                        canvas2.drawPath(path, this.z);
                    } else {
                        canvas2 = canvas;
                        canvas2.drawBitmap(getStartBitmap(), ref$FloatRef6.element - ((float) (width / 2)), (float) 10, this.B);
                    }
                }
            } else {
                canvas2 = canvas3;
            }
            canvas2.drawRect(new Rect(0, this.h0, getWidth(), this.W), this.y);
            canvas2.drawRect(new Rect(0, this.i0, getWidth(), 4), this.y);
            FLogger.INSTANCE.getLocal().d(j0, "low level x: " + this.c0 + " y: " + this.d0 + ", high level x: " + this.f0 + " y: " + this.g0);
            float f13 = this.d0;
            if (f13 > ((float) this.i0) && f13 < ((float) this.h0)) {
                canvas2.drawText(getMLowLevelText(), this.c0, this.d0, this.A);
            }
            float f14 = this.g0;
            if (f14 > ((float) this.i0) && f14 < ((float) this.h0)) {
                canvas2.drawText(getMHighLevelText(), this.f0, this.g0, this.A);
            }
        }
    }

    @DexIgnore
    public int getStarIconResId() {
        return R.drawable.ic_goal_not_meet;
    }

    @DexIgnore
    public int getStarSizeInPx() {
        return this.P;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.y.setColor(this.n);
        float f = (float) 4;
        this.y.setStrokeWidth(f);
        this.C.setColor(this.p);
        this.C.setStrokeWidth((float) this.O);
        a(this.D, this.q, (float) this.O, Paint.Style.FILL);
        a(this.E, this.r, (float) this.O, Paint.Style.FILL);
        a(this.F, this.s, (float) this.O, Paint.Style.FILL);
        a(this.G, this.t, (float) this.O, Paint.Style.FILL);
        a(this.H, this.u, (float) this.O, Paint.Style.FILL);
        this.z.setColor(this.o);
        this.z.setStyle(Paint.Style.STROKE);
        this.z.setStrokeWidth(f);
        this.z.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.A.setColor(this.v);
        this.A.setStyle(Paint.Style.FILL);
        this.A.setTextSize((float) this.w);
        this.A.setTypeface(this.Q);
        this.B.setStyle(Paint.Style.FILL);
        this.B.setColorFilter(new PorterDuffColorFilter(this.L, PorterDuff.Mode.SRC_IN));
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Paint paint, int i2, float f, Paint.Style style) {
        paint.setColor(i2);
        paint.setStrokeWidth(f);
        paint.setStyle(style);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityDayDetailsChart(Context context) {
        this(context, (AttributeSet) null, 0);
        kd4.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityDayDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        kd4.b(context, "context");
    }
}
