package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.wr3;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import kotlin.Pair;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityMonthDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String C;
    @DexIgnore
    public float A;
    @DexIgnore
    public float B;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ String m;
    @DexIgnore
    public /* final */ int n;
    @DexIgnore
    public /* final */ Paint o;
    @DexIgnore
    public /* final */ Paint p;
    @DexIgnore
    public /* final */ Paint q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public /* final */ ArrayList<Pair<Float, Float>> z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = ActivityMonthDetailsChart.class.getSimpleName();
        kd4.a((Object) simpleName, "ActivityMonthDetailsChart::class.java.simpleName");
        C = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityMonthDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        kd4.b(context, "context");
        this.o = new Paint(1);
        this.p = new Paint(1);
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = context.getResources().getDimensionPixelSize(R.dimen.dp15);
        this.u = context.getResources().getDimensionPixelSize(R.dimen.dp10);
        this.v = context.getResources().getDimensionPixelSize(R.dimen.dp5);
        this.w = context.getResources().getDimensionPixelSize(R.dimen.dp3);
        this.x = context.getResources().getDimensionPixelSize(R.dimen.dp10);
        this.y = 4;
        this.z = new ArrayList<>();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, h62.ActivityMonthDetailsChart, 0, 0));
        }
        int a2 = k6.a(context, (int) R.color.gray_30);
        String string = context.getString(R.string.font_path_regular);
        kd4.a((Object) string, "context.getString(R.string.font_path_regular)");
        TypedArray mTypedArray = getMTypedArray();
        this.h = mTypedArray != null ? mTypedArray.getColor(3, a2) : a2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.i = mTypedArray2 != null ? mTypedArray2.getColor(2, a2) : a2;
        TypedArray mTypedArray3 = getMTypedArray();
        this.j = mTypedArray3 != null ? mTypedArray3.getColor(1, a2) : a2;
        TypedArray mTypedArray4 = getMTypedArray();
        this.k = mTypedArray4 != null ? mTypedArray4.getColor(0, a2) : a2;
        TypedArray mTypedArray5 = getMTypedArray();
        this.l = mTypedArray5 != null ? mTypedArray5.getColor(4, a2) : a2;
        TypedArray mTypedArray6 = getMTypedArray();
        this.n = mTypedArray6 != null ? mTypedArray6.getDimensionPixelSize(6, 40) : 40;
        TypedArray mTypedArray7 = getMTypedArray();
        if (mTypedArray7 != null) {
            String string2 = mTypedArray7.getString(5);
            if (string2 != null) {
                string = string2;
            }
        }
        this.m = string;
        TypedArray mTypedArray8 = getMTypedArray();
        if (mTypedArray8 != null) {
            mTypedArray8.recycle();
        }
    }

    @DexIgnore
    private final float getMChartMax() {
        return Math.max(getMMaxGoal(), getMMaxValues());
    }

    @DexIgnore
    private final float getMMaxGoal() {
        T t2;
        Iterator<T> it = this.z.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                float floatValue = ((Number) ((Pair) t2).getSecond()).floatValue();
                do {
                    T next = it.next();
                    float floatValue2 = ((Number) ((Pair) next).getSecond()).floatValue();
                    if (Float.compare(floatValue, floatValue2) < 0) {
                        t2 = next;
                        floatValue = floatValue2;
                    }
                } while (it.hasNext());
            }
        }
        Pair pair = (Pair) t2;
        if (pair != null) {
            Float f = (Float) pair.getSecond();
            if (f != null) {
                return f.floatValue();
            }
        }
        return this.A;
    }

    @DexIgnore
    private final float getMMaxValues() {
        T t2;
        Iterator<T> it = this.z.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                float floatValue = ((Number) ((Pair) t2).getFirst()).floatValue();
                do {
                    T next = it.next();
                    float floatValue2 = ((Number) ((Pair) next).getFirst()).floatValue();
                    if (Float.compare(floatValue, floatValue2) < 0) {
                        t2 = next;
                        floatValue = floatValue2;
                    }
                } while (it.hasNext());
            }
        }
        Pair pair = (Pair) t2;
        if (pair != null) {
            Float f = (Float) pair.getFirst();
            if (f != null) {
                return f.floatValue();
            }
        }
        return this.B;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            Rect rect = new Rect();
            this.q.getTextBounds(AppEventsConstants.EVENT_PARAM_VALUE_YES, 0, StringsKt__StringsKt.c(AppEventsConstants.EVENT_PARAM_VALUE_YES) > 0 ? StringsKt__StringsKt.c(AppEventsConstants.EVENT_PARAM_VALUE_YES) : 1, rect);
            int height = (int) (((((float) getHeight()) - ((float) rect.height())) - ((float) this.v)) - ((float) this.w));
            int i2 = this.x;
            int width = ((getWidth() - getStartBitmap().getWidth()) - (this.v * 2)) - i2;
            a(canvas, width, height, i2, (xc4<? super LinkedList<Integer>, qa4>) new ActivityMonthDetailsChart$draw$$inlined$let$lambda$Anon1(width, height, this, canvas));
            a(canvas, 0, height);
        }
    }

    @DexIgnore
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    public int getStarSizeInPx() {
        return this.t;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.o.setColor(this.h);
        float f = (float) 4;
        this.o.setStrokeWidth(f);
        this.q.setColor(this.l);
        this.q.setStyle(Paint.Style.FILL);
        this.q.setTextSize((float) this.n);
        Paint paint = this.q;
        Context context = getContext();
        kd4.a((Object) context, "context");
        paint.setTypeface(Typeface.createFromAsset(context.getAssets(), this.m));
        this.p.setColor(this.i);
        this.p.setStyle(Paint.Style.STROKE);
        this.p.setStrokeWidth(f);
        this.p.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.r.setColor(this.j);
        this.r.setStrokeWidth((float) this.u);
        this.r.setStyle(Paint.Style.FILL);
        this.s.setColor(this.k);
        this.s.setStrokeWidth((float) this.u);
        this.s.setStyle(Paint.Style.FILL);
        this.y = getStartBitmap().getHeight() / 2;
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3, int i4, xc4<? super LinkedList<Integer>, qa4> xc4) {
        if (!this.z.isEmpty()) {
            float mChartMax = getMChartMax();
            int size = i2 / this.z.size();
            int i5 = this.u;
            if (size < i5) {
                i5 = i2 / this.z.size();
            }
            int i6 = i5 / 2;
            int i7 = i6 / 3;
            LinkedList linkedList = new LinkedList();
            Iterator<Pair<Float, Float>> it = this.z.iterator();
            while (it.hasNext()) {
                float f = (float) i3;
                RectF rectF = new RectF((float) i4, Math.max(f - ((((Number) it.next().component1()).floatValue() / mChartMax) * f), (float) this.y), (float) (i4 + i6), f);
                linkedList.add(Integer.valueOf((int) (rectF.left + (rectF.width() / ((float) 2)))));
                i4 += i5;
                wr3.c(canvas, rectF, this.r, (float) i7);
            }
            xc4.invoke(linkedList);
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.o);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.o);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3, int i4) {
        Canvas canvas2 = canvas;
        List<Integer> list2 = list;
        if ((!this.z.isEmpty()) && this.z.size() > 1) {
            float mChartMax = getMChartMax();
            if (mChartMax > ((float) 0)) {
                Path path = new Path();
                int size = list.size();
                float height = (float) getStartBitmap().getHeight();
                int i5 = 1;
                while (i5 < size) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = C;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Previous goal: ");
                    int i6 = i5 - 1;
                    sb.append(((Number) this.z.get(i6).getSecond()).floatValue());
                    sb.append(", current goal: ");
                    sb.append(((Number) this.z.get(i5).getSecond()).floatValue());
                    sb.append(", chart max: ");
                    sb.append(mChartMax);
                    local.d(str, sb.toString());
                    float intValue = (float) list2.get(i5).intValue();
                    float f = (float) i3;
                    float f2 = (float) i4;
                    float max = Math.max(Math.min((1.0f - (((Number) this.z.get(i5).getSecond()).floatValue() / mChartMax)) * f, f2), (float) this.y);
                    float intValue2 = (float) list2.get(i6).intValue();
                    float max2 = Math.max(Math.min((1.0f - (((Number) this.z.get(i6).getSecond()).floatValue() / mChartMax)) * f, f2), (float) this.y);
                    if (max == max2) {
                        path.moveTo(intValue2, max2);
                        if (i5 == list.size() - 1) {
                            path.lineTo((float) (i2 + this.v), max);
                        } else {
                            path.lineTo(intValue, max);
                        }
                        canvas2.drawPath(path, this.p);
                    } else {
                        path.moveTo(intValue2, max2);
                        path.lineTo(intValue, max2);
                        canvas2.drawPath(path, this.p);
                        path.moveTo(intValue, max2);
                        path.lineTo(intValue, max);
                        canvas2.drawPath(path, this.p);
                        if (i5 == list.size() - 1) {
                            path.moveTo(intValue, max);
                            path.lineTo((float) (i2 + this.v), max);
                            canvas2.drawPath(path, this.p);
                        }
                    }
                    i5++;
                    height = max;
                }
                canvas2.drawBitmap(getStartBitmap(), (float) (i2 + this.v), height - ((float) (getStartBitmap().getHeight() / 2)), this.q);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityMonthDetailsChart(Context context) {
        this(context, (AttributeSet) null, 0);
        kd4.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityMonthDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        kd4.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list) {
        Integer[] numArr;
        int i2;
        if (!list.isEmpty()) {
            int size = list.size();
            int i3 = 0;
            switch (size) {
                case 28:
                    numArr = new Integer[]{0, 7, 14, 21, 27};
                    break;
                case 29:
                    numArr = new Integer[]{0, 7, 14, 21, 28};
                    break;
                case 30:
                    numArr = new Integer[]{0, 7, 14, 21, 29};
                    break;
                default:
                    numArr = new Integer[]{0, 7, 14, 21, 30};
                    break;
            }
            try {
                int length = numArr.length;
                i2 = 0;
                while (i3 < length) {
                    try {
                        i2 = numArr[i3].intValue();
                        a(canvas, String.valueOf(i2 + 1), list.get(i2).intValue());
                        i3++;
                    } catch (Exception e) {
                        e = e;
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = C;
                        local.d(str, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e);
                    }
                }
            } catch (Exception e2) {
                e = e2;
                i2 = 0;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = C;
                local2.d(str2, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e);
            }
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, String str, int i2) {
        canvas.drawText(str, ((float) i2) - (this.q.measureText(str, 0, str.length()) / ((float) 2)), (float) ((getHeight() - (new Rect().height() / 2)) - this.w), this.q);
    }
}
