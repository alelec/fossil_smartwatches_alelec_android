package com.portfolio.platform.view.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityDayDetailsChart$draw$1$1$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<android.graphics.RectF, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$FloatRef $goalDashLineBottom;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$FloatRef $goalDashLineX;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $goalPosition;
    @DexIgnore
    public /* final */ /* synthetic */ int $leftIndex;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityDayDetailsChart$draw$1$1$1(int i, kotlin.jvm.internal.Ref$IntRef ref$IntRef, kotlin.jvm.internal.Ref$FloatRef ref$FloatRef, kotlin.jvm.internal.Ref$FloatRef ref$FloatRef2) {
        super(1);
        this.$leftIndex = i;
        this.$goalPosition = ref$IntRef;
        this.$goalDashLineBottom = ref$FloatRef;
        this.$goalDashLineX = ref$FloatRef2;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((android.graphics.RectF) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(android.graphics.RectF rectF) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(rectF, "drawnRect");
        if (rectF.height() > ((float) 0) && this.$leftIndex == this.$goalPosition.element) {
            this.$goalDashLineBottom.element = rectF.top;
            this.$goalDashLineX.element = (rectF.right + rectF.left) / ((float) 2);
        }
    }
}
