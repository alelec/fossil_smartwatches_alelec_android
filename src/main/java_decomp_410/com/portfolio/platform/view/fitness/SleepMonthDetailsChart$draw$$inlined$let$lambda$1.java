package com.portfolio.platform.view.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepMonthDetailsChart$draw$$inlined$let$lambda$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<java.util.LinkedList<java.lang.Integer>, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ android.graphics.Canvas $canvas$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $chartHeight;
    @DexIgnore
    public /* final */ /* synthetic */ int $chartWidth;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.view.fitness.SleepMonthDetailsChart this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepMonthDetailsChart$draw$$inlined$let$lambda$1(int i, int i2, com.portfolio.platform.view.fitness.SleepMonthDetailsChart sleepMonthDetailsChart, android.graphics.Canvas canvas) {
        super(1);
        this.$chartWidth = i;
        this.$chartHeight = i2;
        this.this$0 = sleepMonthDetailsChart;
        this.$canvas$inlined = canvas;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((java.util.LinkedList<java.lang.Integer>) (java.util.LinkedList) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(java.util.LinkedList<java.lang.Integer> linkedList) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(linkedList, "centerXList");
        this.this$0.mo42659a(this.$canvas$inlined, linkedList);
        com.portfolio.platform.view.fitness.SleepMonthDetailsChart sleepMonthDetailsChart = this.this$0;
        sleepMonthDetailsChart.mo42660a(this.$canvas$inlined, (java.util.List<java.lang.Integer>) linkedList, this.$chartWidth, this.$chartHeight, sleepMonthDetailsChart.f25459x * 2, this.$chartHeight - 4);
    }
}
