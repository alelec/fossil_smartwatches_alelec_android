package com.portfolio.platform.gson;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rj2;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.vz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaRecommendPresetDeserializer implements vz1<DianaRecommendPreset> {
    @DexIgnore
    public DianaRecommendPreset deserialize(JsonElement jsonElement, Type type, uz1 uz1) {
        String str;
        boolean z;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        String str9;
        String str10;
        Iterator<JsonElement> it;
        String str11;
        String str12;
        String str13;
        String str14;
        String str15;
        String str16;
        String str17;
        Iterator<JsonElement> it2;
        String str18;
        String str19;
        String str20;
        if (jsonElement != null) {
            xz1 d = jsonElement.d();
            JsonElement a = d.a("name");
            kd4.a((Object) a, "jsonObject.get(Constants.JSON_KEY_NAME)");
            String f = a.f();
            JsonElement a2 = d.a("id");
            kd4.a((Object) a2, "jsonObject.get(\"id\")");
            String f2 = a2.f();
            JsonElement a3 = d.a("isDefault");
            kd4.a((Object) a3, "jsonObject.get(Constants.JSON_KEY_IS_DEFAULT)");
            boolean a4 = a3.a();
            String str21 = "updatedAt";
            JsonElement a5 = d.a(str21);
            kd4.a((Object) a5, "jsonObject.get(Constants.JSON_KEY_UPDATED_AT)");
            String f3 = a5.f();
            String str22 = "createdAt";
            JsonElement a6 = d.a(str22);
            kd4.a((Object) a6, "jsonObject.get(Constants.JSON_KEY_CREATED_AT)");
            String f4 = a6.f();
            String str23 = "watchFaceId";
            JsonElement a7 = d.a(str23);
            kd4.a((Object) a7, "jsonObject.get(Constants.JSON_KEY_WATCH_FACE_ID)");
            String f5 = a7.f();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (uz1 != null) {
                Iterator<JsonElement> it3 = d.b("buttons").iterator();
                while (true) {
                    boolean hasNext = it3.hasNext();
                    z = a4;
                    str = str21;
                    str7 = "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)";
                    str6 = f3;
                    str5 = str22;
                    str8 = "item";
                    str4 = f4;
                    str9 = Constants.USER_SETTING;
                    str3 = str23;
                    str2 = f5;
                    str10 = "";
                    if (!hasNext) {
                        break;
                    }
                    JsonElement next = it3.next();
                    kd4.a((Object) next, str8);
                    xz1 d2 = next.d();
                    if (d2.d("buttonPosition")) {
                        JsonElement a8 = d2.a("buttonPosition");
                        it2 = it3;
                        kd4.a((Object) a8, "itemJsonObject.get(Constants.JSON_KEY_BUTTON_POS)");
                        str18 = a8.f();
                    } else {
                        it2 = it3;
                        str18 = str10;
                    }
                    if (d2.d("appId")) {
                        JsonElement a9 = d2.a("appId");
                        kd4.a((Object) a9, "itemJsonObject.get(Constants.JSON_KEY_APP_ID)");
                        str19 = a9.f();
                    } else {
                        str19 = str10;
                    }
                    if (d2.d("localUpdatedAt")) {
                        JsonElement a10 = d2.a("localUpdatedAt");
                        kd4.a((Object) a10, str7);
                        str20 = a10.f();
                    } else {
                        str20 = str10;
                    }
                    if (d2.d(str9)) {
                        try {
                            JsonElement a11 = d2.a(str9);
                            kd4.a((Object) a11, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str10 = rj2.a(a11.d());
                        } catch (Exception unused) {
                            FLogger.INSTANCE.getLocal().d("DianaPresetDeserializer", "Exception when parse json string");
                        }
                    }
                    kd4.a((Object) str18, "position");
                    kd4.a((Object) str19, "appId");
                    kd4.a((Object) str20, "localUpdatedAt");
                    arrayList.add(new DianaPresetWatchAppSetting(str18, str19, str20, str10));
                    a4 = z;
                    str21 = str;
                    it3 = it2;
                    f3 = str6;
                    str22 = str5;
                    f4 = str4;
                    str23 = str3;
                    f5 = str2;
                }
                Iterator<JsonElement> it4 = d.b("complications").iterator();
                while (it4.hasNext()) {
                    JsonElement next2 = it4.next();
                    kd4.a((Object) next2, str8);
                    xz1 d3 = next2.d();
                    if (d3.d("complicationPosition")) {
                        JsonElement a12 = d3.a("complicationPosition");
                        it = it4;
                        kd4.a((Object) a12, "itemJsonObject.get(Const\u2026SON_KEY_COMPLICATION_POS)");
                        str11 = a12.f();
                    } else {
                        it = it4;
                        str11 = str10;
                    }
                    if (d3.d("appId")) {
                        JsonElement a13 = d3.a("appId");
                        kd4.a((Object) a13, "itemJsonObject.get(Constants.JSON_KEY_APP_ID)");
                        str12 = a13.f();
                    } else {
                        str12 = str10;
                    }
                    if (d3.d("localUpdatedAt")) {
                        str14 = str8;
                        JsonElement a14 = d3.a("localUpdatedAt");
                        kd4.a((Object) a14, str7);
                        str15 = a14.f();
                        str13 = str7;
                    } else {
                        str14 = str8;
                        Calendar instance = Calendar.getInstance();
                        str13 = str7;
                        kd4.a((Object) instance, "Calendar.getInstance()");
                        str15 = rk2.t(instance.getTime());
                    }
                    if (d3.d(str9)) {
                        try {
                            JsonElement a15 = d3.a(str9);
                            kd4.a((Object) a15, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str17 = rj2.a(a15.d());
                            str16 = str9;
                        } catch (Exception unused2) {
                            str16 = str9;
                            FLogger.INSTANCE.getLocal().d("DianaPresetDeserializer", "Exception when parse json string");
                        }
                    } else {
                        str16 = str9;
                        str17 = str10;
                    }
                    kd4.a((Object) str11, "position");
                    kd4.a((Object) str12, "appId");
                    kd4.a((Object) str15, "localUpdatedAt");
                    arrayList2.add(new DianaPresetComplicationSetting(str11, str12, str15, str17));
                    it4 = it;
                    str8 = str14;
                    str7 = str13;
                    str9 = str16;
                }
            } else {
                str = str21;
                str5 = str22;
                str3 = str23;
                z = a4;
                str2 = f5;
                str4 = f4;
                str6 = f3;
            }
            kd4.a((Object) f2, "id");
            kd4.a((Object) f, "name");
            String str24 = str2;
            kd4.a((Object) str24, str3);
            String str25 = str4;
            kd4.a((Object) str25, str5);
            String str26 = str6;
            kd4.a((Object) str26, str);
            return new DianaRecommendPreset("", f2, f, z, arrayList2, arrayList, str24, str25, str26);
        }
        kd4.a();
        throw null;
    }
}
