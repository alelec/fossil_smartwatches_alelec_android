package com.portfolio.platform.helper;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.google.firebase.analytics.FirebaseAnalytics;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.helper.AnalyticsHelper$doLogEvent$Anon1", f = "AnalyticsHelper.kt", l = {}, m = "invokeSuspend")
public final class AnalyticsHelper$doLogEvent$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle $data;
    @DexIgnore
    public /* final */ /* synthetic */ String $event;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnalyticsHelper$doLogEvent$Anon1(String str, Bundle bundle, yb4 yb4) {
        super(2, yb4);
        this.$event = str;
        this.$data = bundle;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        AnalyticsHelper$doLogEvent$Anon1 analyticsHelper$doLogEvent$Anon1 = new AnalyticsHelper$doLogEvent$Anon1(this.$event, this.$data, yb4);
        analyticsHelper$doLogEvent$Anon1.p$ = (zg4) obj;
        return analyticsHelper$doLogEvent$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AnalyticsHelper$doLogEvent$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            FirebaseAnalytics d = AnalyticsHelper.c;
            if (d != null) {
                d.a(this.$event, this.$data);
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
