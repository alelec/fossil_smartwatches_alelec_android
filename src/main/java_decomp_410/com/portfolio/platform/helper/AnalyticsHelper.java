package com.portfolio.platform.helper;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.sl2;
import com.fossil.blesdk.obfuscated.tl2;
import com.fossil.blesdk.obfuscated.ul2;
import com.fossil.blesdk.obfuscated.vl2;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.SKUModel;
import com.zendesk.sdk.network.impl.DeviceInfo;
import java.util.HashMap;
import java.util.Map;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.text.Regex;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AnalyticsHelper {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static FirebaseAnalytics c;
    @DexIgnore
    public static AnalyticsHelper d;
    @DexIgnore
    public static /* final */ HashMap<String, ul2> e; // = new HashMap<>();
    @DexIgnore
    public static /* final */ a f; // = new a((fd4) null);
    @DexIgnore
    public volatile String a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(AnalyticsHelper analyticsHelper) {
            AnalyticsHelper.d = analyticsHelper;
        }

        @DexIgnore
        public final ul2 b(String str) {
            kd4.b(str, "traceName");
            AnalyticsHelper c = c();
            if (kd4.a((Object) "view_appearance", (Object) str)) {
                return new vl2(c, str, c.a());
            }
            return new ul2(c, str, c.a());
        }

        @DexIgnore
        public final ul2 c(String str) {
            kd4.b(str, "tracingKey");
            return d().get(str);
        }

        @DexIgnore
        public final HashMap<String, ul2> d() {
            return AnalyticsHelper.e;
        }

        @DexIgnore
        public final AnalyticsHelper e() {
            return AnalyticsHelper.d;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final void a(String str, ul2 ul2) {
            kd4.b(str, "tracingKey");
            kd4.b(ul2, "trace");
            d().put(str, ul2);
        }

        @DexIgnore
        public final synchronized AnalyticsHelper c() {
            AnalyticsHelper e;
            if (e() == null) {
                a(new AnalyticsHelper((fd4) null));
            }
            e = e();
            if (e == null) {
                kd4.a();
                throw null;
            }
            return e;
        }

        @DexIgnore
        public final String d(String str) {
            kd4.b(str, "input");
            return new Regex("[^a-zA-Z0-9]+").replace((CharSequence) str, "_");
        }

        @DexIgnore
        public final void e(String str) {
            kd4.b(str, "tracingKey");
            d().remove(str);
        }

        @DexIgnore
        public final sl2 a(String str) {
            kd4.b(str, "eventName");
            return new sl2(c(), str);
        }

        @DexIgnore
        public final tl2 a() {
            return new tl2(c());
        }

        @DexIgnore
        public final vl2 b() {
            AnalyticsHelper c = c();
            return new vl2(c, "view_appearance", c.a());
        }
    }

    /*
    static {
        String simpleName = AnalyticsHelper.class.getSimpleName();
        kd4.a((Object) simpleName, "AnalyticsHelper::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public AnalyticsHelper() {
        c = FirebaseAnalytics.getInstance(PortfolioApp.W.c());
    }

    @DexIgnore
    public final void b(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "User id: " + str);
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.a(str);
        }
        this.a = str;
    }

    @DexIgnore
    public final void c(String str, String str2, int i) {
        kd4.b(str, "styleNumber");
        kd4.b(str2, "deviceName");
        a("sync_success", (Map<String, ? extends Object>) a(str, str2, i));
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final void c() {
        b("Auth", "None");
    }

    @DexIgnore
    public /* synthetic */ AnalyticsHelper(fd4 fd4) {
        this();
    }

    @DexIgnore
    public final void a(boolean z) {
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.a(z);
        }
    }

    @DexIgnore
    public final void c(boolean z) {
        b("Optin_Usage", z ? "Yes" : "No");
    }

    @DexIgnore
    public final void a(String str, String str2) {
        kd4.b(str, Constants.EVENT);
        kd4.b(str2, "value");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b;
        local.d(str3, "Inside .analyticLogEvent event=" + str + ", value=" + str2);
        Bundle bundle = new Bundle();
        bundle.putString(str, str2);
        a(str, bundle);
    }

    @DexIgnore
    public final boolean b() {
        MFUser b2 = dn2.p.a().n().b();
        return b2 != null && b2.isDiagnosticEnabled();
    }

    @DexIgnore
    public final void b(String str, String str2, String str3) {
        kd4.b(str, Constants.EVENT);
        kd4.b(str2, "paramName");
        kd4.b(str3, "paramValue");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = b;
        local.d(str4, "Inside .analyticLogEvent event=" + str + ", name=" + str2 + ", value=" + str3);
        Bundle bundle = new Bundle();
        bundle.putString(str2, str3);
        a(str, bundle);
    }

    @DexIgnore
    public final void a(String str, Map<String, ? extends Object> map) {
        kd4.b(str, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "Inside .analyticLogEvent event=" + str + ", value=" + map);
        if (map != null && (!map.isEmpty())) {
            Bundle bundle = new Bundle();
            for (Map.Entry next : map.entrySet()) {
                String str3 = (String) next.getKey();
                Object value = next.getValue();
                if (value instanceof Integer) {
                    bundle.putInt(str3, ((Number) value).intValue());
                } else if (value != null) {
                    bundle.putString(str3, (String) value);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
                }
            }
            a(str, bundle);
        }
    }

    @DexIgnore
    public final void b(String str, String str2) {
        kd4.b(str, "propertyName");
        kd4.b(str2, "value");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b;
        local.d(str3, "User property " + str + " with value=" + str2);
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.a(str, str2);
        }
    }

    @DexIgnore
    public final void b(String str, String str2, int i) {
        kd4.b(str, "styleNumber");
        kd4.b(str2, "deviceName");
        a("sync_error", (Map<String, ? extends Object>) a(str, str2, i));
    }

    @DexIgnore
    public final void b(boolean z) {
        b("Optin_Emails", z ? "Yes" : "No");
    }

    @DexIgnore
    public final void a(String str) {
        kd4.b(str, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "Inside .analyticLogEvent event=" + str);
        a(str, (Bundle) null);
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        throw null;
        // fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new AnalyticsHelper$doLogEvent$Anon1(str, bundle, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(Map<String, String> map) {
        if (map != null && (!map.isEmpty())) {
            for (Map.Entry next : map.entrySet()) {
                String str = (String) next.getKey();
                String str2 = (String) next.getValue();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = b;
                local.d(str3, "User property " + str + " with value=" + str2);
                FirebaseAnalytics firebaseAnalytics = c;
                if (firebaseAnalytics != null) {
                    firebaseAnalytics.a(str, str2);
                }
            }
        }
    }

    @DexIgnore
    public final Map<String, String> a(String str, String str2, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("Style_Number", str);
        hashMap.put("Device_Name", str2);
        hashMap.put("Type", String.valueOf(i));
        return hashMap;
    }

    @DexIgnore
    public final void a(int i, SKUModel sKUModel) {
        if (sKUModel != null) {
            String sku = sKUModel.getSku();
            if (sku != null) {
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                a("sync_start", (Map<String, ? extends Object>) a(sku, deviceName, i));
                return;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        kd4.b(str, "serialPrefix");
        kd4.b(str2, "activeDeviceName");
        kd4.b(str3, "firmwareVersion");
        tl2 a2 = f.a();
        a2.a("serial_number_prefix", str);
        a2.a(DeviceInfo.DEVICE_INFO_DEVICE_NAME, str2);
        a2.a(Constants.FIRMWARE_VERSION, str3);
        a2.a();
    }
}
