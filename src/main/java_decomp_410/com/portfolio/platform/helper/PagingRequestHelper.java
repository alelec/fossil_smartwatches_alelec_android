package com.portfolio.platform.helper;

import android.util.Log;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class PagingRequestHelper {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ c[] c; // = {new c(this, RequestType.INITIAL), new c(this, RequestType.BEFORE), new c(this, RequestType.AFTER)};
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<a> d; // = new CopyOnWriteArrayList<>();

    @DexIgnore
    public enum RequestType {
        INITIAL,
        BEFORE,
        AFTER
    }

    @DexIgnore
    public enum Status {
        RUNNING,
        SUCCESS,
        FAILED
    }

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(e eVar);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c {
        @DexIgnore
        public d a;
        @DexIgnore
        public b b;
        @DexIgnore
        public Throwable c;
        @DexIgnore
        public Status d; // = Status.SUCCESS;

        @DexIgnore
        public c(PagingRequestHelper pagingRequestHelper, RequestType requestType) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ b e;
        @DexIgnore
        public /* final */ PagingRequestHelper f;
        @DexIgnore
        public /* final */ RequestType g;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void run() {
                d dVar = d.this;
                dVar.f.a(dVar.g, dVar.e);
            }
        }

        @DexIgnore
        public d(b bVar, PagingRequestHelper pagingRequestHelper, RequestType requestType) {
            this.e = bVar;
            this.f = pagingRequestHelper;
            this.g = requestType;
        }

        @DexIgnore
        public void a(Executor executor) {
            executor.execute(new a());
        }

        @DexIgnore
        public void run() {
            this.e.run(new b.a(this, this.f));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public /* final */ Status a;
        @DexIgnore
        public /* final */ Status b;
        @DexIgnore
        public /* final */ Status c;
        @DexIgnore
        public /* final */ Throwable[] d;

        @DexIgnore
        public e(Status status, Status status2, Status status3, Throwable[] thArr) {
            this.a = status;
            this.b = status2;
            this.c = status3;
            this.d = thArr;
        }

        @DexIgnore
        public boolean a() {
            Status status = this.a;
            Status status2 = Status.FAILED;
            return status == status2 || this.b == status2 || this.c == status2;
        }

        @DexIgnore
        public boolean b() {
            Status status = this.a;
            Status status2 = Status.RUNNING;
            return status == status2 || this.b == status2 || this.c == status2;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || e.class != obj.getClass()) {
                return false;
            }
            e eVar = (e) obj;
            if (this.a == eVar.a && this.b == eVar.b && this.c == eVar.c) {
                return Arrays.equals(this.d, eVar.d);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + Arrays.hashCode(this.d);
        }

        @DexIgnore
        public String toString() {
            return "StatusReport{initial=" + this.a + ", before=" + this.b + ", after=" + this.c + ", mErrors=" + Arrays.toString(this.d) + '}';
        }

        @DexIgnore
        public Throwable a(RequestType requestType) {
            return this.d[requestType.ordinal()];
        }
    }

    @DexIgnore
    public PagingRequestHelper(Executor executor) {
        this.b = executor;
    }

    @DexIgnore
    public boolean a(a aVar) {
        return this.d.add(aVar);
    }

    @DexIgnore
    public boolean b(a aVar) {
        return this.d.remove(aVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        if (r4 == null) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002e, code lost:
        a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        new com.portfolio.platform.helper.PagingRequestHelper.d(r7, r5, r6).run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0039, code lost:
        return true;
     */
    @DexIgnore
    public boolean a(RequestType requestType, b bVar) {
        boolean z = !this.d.isEmpty();
        synchronized (this.a) {
            c cVar = this.c[requestType.ordinal()];
            if (cVar.b != null) {
                return false;
            }
            cVar.b = bVar;
            cVar.d = Status.RUNNING;
            e eVar = null;
            cVar.a = null;
            cVar.c = null;
            if (z) {
                eVar = a();
            }
        }
    }

    @DexIgnore
    public boolean b() {
        int i;
        FLogger.INSTANCE.getLocal().d("PagingRequestHelper", "retryAllFailed");
        d[] dVarArr = new d[RequestType.values().length];
        synchronized (this.a) {
            for (int i2 = 0; i2 < RequestType.values().length; i2++) {
                dVarArr[i2] = this.c[i2].a;
                this.c[i2].a = null;
            }
        }
        boolean z = false;
        for (d dVar : dVarArr) {
            if (dVar != null) {
                dVar.a(this.b);
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    @FunctionalInterface
    public interface b {
        @DexIgnore
        void run(a aVar);

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a {
            @DexIgnore
            public /* final */ AtomicBoolean a; // = new AtomicBoolean();
            @DexIgnore
            public /* final */ d b;
            @DexIgnore
            public /* final */ PagingRequestHelper c;

            @DexIgnore
            public a(d dVar, PagingRequestHelper pagingRequestHelper) {
                this.b = dVar;
                this.c = pagingRequestHelper;
            }

            @DexIgnore
            public final void a() {
                Log.d("PagingRequestHelper", "recordSuccess");
                if (this.a.compareAndSet(false, true)) {
                    this.c.a(this.b, (Throwable) null);
                    return;
                }
                throw new IllegalStateException("already called recordSuccess or recordFailure");
            }

            @DexIgnore
            public final void a(Throwable th) {
                Log.d("PagingRequestHelper", "recordFailure");
                if (th == null) {
                    throw new IllegalArgumentException("You must provide a throwable describing the error to record the failure");
                } else if (this.a.compareAndSet(false, true)) {
                    this.c.a(this.b, th);
                } else {
                    throw new IllegalStateException("already called recordSuccess or recordFailure");
                }
            }
        }
    }

    @DexIgnore
    public final e a() {
        c[] cVarArr = this.c;
        return new e(a(RequestType.INITIAL), a(RequestType.BEFORE), a(RequestType.AFTER), new Throwable[]{cVarArr[0].c, cVarArr[1].c, cVarArr[2].c});
    }

    @DexIgnore
    public final Status a(RequestType requestType) {
        return this.c[requestType.ordinal()].d;
    }

    @DexIgnore
    public void a(d dVar, Throwable th) {
        e eVar;
        boolean z = th == null;
        boolean isEmpty = true ^ this.d.isEmpty();
        synchronized (this.a) {
            c cVar = this.c[dVar.g.ordinal()];
            eVar = null;
            cVar.b = null;
            cVar.c = th;
            if (z) {
                cVar.a = null;
                cVar.d = Status.SUCCESS;
            } else {
                cVar.a = dVar;
                cVar.d = Status.FAILED;
            }
            if (isEmpty) {
                eVar = a();
            }
        }
        if (eVar != null) {
            a(eVar);
        }
    }

    @DexIgnore
    public final void a(e eVar) {
        Iterator<a> it = this.d.iterator();
        while (it.hasNext()) {
            it.next().a(eVar);
        }
    }
}
