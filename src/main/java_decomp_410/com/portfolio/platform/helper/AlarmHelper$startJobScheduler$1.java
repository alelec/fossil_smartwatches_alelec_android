package com.portfolio.platform.helper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.helper.AlarmHelper$startJobScheduler$1", mo27670f = "AlarmHelper.kt", mo27671l = {109}, mo27672m = "invokeSuspend")
public final class AlarmHelper$startJobScheduler$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.content.Context $context;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21160p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.helper.AlarmHelper this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmHelper$startJobScheduler$1(com.portfolio.platform.helper.AlarmHelper alarmHelper, android.content.Context context, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = alarmHelper;
        this.$context = context;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.helper.AlarmHelper$startJobScheduler$1 alarmHelper$startJobScheduler$1 = new com.portfolio.platform.helper.AlarmHelper$startJobScheduler$1(this.this$0, this.$context, yb4);
        alarmHelper$startJobScheduler$1.f21160p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return alarmHelper$startJobScheduler$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.helper.AlarmHelper$startJobScheduler$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004e  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object obj2;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.util.List<com.portfolio.platform.data.source.local.alarm.Alarm> list;
        java.util.Iterator<com.portfolio.platform.data.source.local.alarm.Alarm> it;
        com.portfolio.platform.helper.AlarmHelper$startJobScheduler$1 alarmHelper$startJobScheduler$1;
        int i;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i2 = this.label;
        if (i2 == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f21160p$;
            list = this.this$0.mo39479a().getActiveAlarms();
            if (list != null) {
                obj2 = a;
                alarmHelper$startJobScheduler$1 = this;
                zg4 = zg42;
                it = list.iterator();
                while (it.hasNext()) {
                }
            } else {
                alarmHelper$startJobScheduler$1 = this;
            }
        } else if (i2 == 1) {
            it = (java.util.Iterator) this.L$3;
            com.portfolio.platform.data.source.local.alarm.Alarm alarm = (com.portfolio.platform.data.source.local.alarm.Alarm) this.L$2;
            list = (java.util.List) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            obj2 = a;
            alarmHelper$startJobScheduler$1 = this;
            while (it.hasNext()) {
                com.portfolio.platform.data.source.local.alarm.Alarm next = it.next();
                if (com.portfolio.platform.helper.AlarmHelper.f21153f.mo39490a(next)) {
                    next.setActive(false);
                    com.portfolio.platform.data.source.AlarmsRepository a2 = alarmHelper$startJobScheduler$1.this$0.mo39479a();
                    alarmHelper$startJobScheduler$1.L$0 = zg4;
                    alarmHelper$startJobScheduler$1.L$1 = list;
                    alarmHelper$startJobScheduler$1.L$2 = next;
                    alarmHelper$startJobScheduler$1.L$3 = it;
                    alarmHelper$startJobScheduler$1.label = 1;
                    if (a2.updateAlarm(next, alarmHelper$startJobScheduler$1) == obj2) {
                        return obj2;
                    }
                }
            }
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.source.local.alarm.Alarm findNextActiveAlarm = alarmHelper$startJobScheduler$1.this$0.mo39479a().findNextActiveAlarm();
        if (findNextActiveAlarm == null) {
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        long millisecond = findNextActiveAlarm.getMillisecond();
        java.util.Calendar instance = java.util.Calendar.getInstance();
        if (instance.get(9) == 1) {
            int i3 = instance.get(10);
            i = i3 == 12 ? 12 : i3 + 12;
        } else {
            i = instance.get(10);
        }
        long j = (((long) ((((i * 60) + instance.get(12)) * 60) + instance.get(13))) * 1000) + ((long) instance.get(14));
        long j2 = j <= millisecond ? millisecond - j : com.sina.weibo.sdk.statistic.LogBuilder.MAX_INTERVAL - (j - millisecond);
        long currentTimeMillis = java.lang.System.currentTimeMillis() + j2;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d("AlarmHelper", "startJobScheduler - duration=" + j2);
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local2.mo33255d("AlarmHelper", "startJobScheduler - alarmEnd=" + new java.util.Date(currentTimeMillis));
        com.portfolio.platform.data.model.MFUser currentUser = alarmHelper$startJobScheduler$1.this$0.mo39481b().getCurrentUser();
        if (currentUser == null || android.text.TextUtils.isEmpty(currentUser.getUserId())) {
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putString("DEF_ALARM_RECEIVER_USER_ID", currentUser.getUserId());
        bundle.putInt("DEF_ALARM_RECEIVER_ACTION", 0);
        android.content.Intent intent = new android.content.Intent(alarmHelper$startJobScheduler$1.$context, com.portfolio.platform.receiver.AlarmReceiver.class);
        intent.setAction("com.portfolio.platform.ALARM_RECEIVER");
        intent.putExtras(bundle);
        android.app.PendingIntent broadcast = android.app.PendingIntent.getBroadcast(alarmHelper$startJobScheduler$1.$context, 102, intent, 134217728);
        android.app.AlarmManager alarmManager = (android.app.AlarmManager) alarmHelper$startJobScheduler$1.$context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local3.mo33255d("AlarmHelper", "startJobScheduler - inexactAlarm=" + alarmManager);
        if (j2 <= 3600000 || alarmManager == null) {
            android.os.Bundle bundle2 = new android.os.Bundle();
            bundle2.putString("DEF_ALARM_RECEIVER_USER_ID", currentUser.getUserId());
            bundle2.putInt("DEF_ALARM_RECEIVER_ACTION", 3);
            android.content.Intent intent2 = new android.content.Intent(alarmHelper$startJobScheduler$1.$context, com.portfolio.platform.receiver.AlarmReceiver.class);
            intent2.setAction("com.portfolio.platform.ALARM_RECEIVER");
            intent2.putExtras(bundle2);
            android.app.AlarmManager alarmManager2 = (android.app.AlarmManager) alarmHelper$startJobScheduler$1.$context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            if (alarmManager2 != null) {
                alarmManager2.setExact(0, currentTimeMillis, android.app.PendingIntent.getBroadcast(alarmHelper$startJobScheduler$1.$context, 101, intent2, 134217728));
            }
        } else {
            alarmManager.set(0, currentTimeMillis - 3600000, broadcast);
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
