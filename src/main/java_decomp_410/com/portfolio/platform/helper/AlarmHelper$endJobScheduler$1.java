package com.portfolio.platform.helper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.helper.AlarmHelper$endJobScheduler$1", mo27670f = "AlarmHelper.kt", mo27671l = {182}, mo27672m = "invokeSuspend")
public final class AlarmHelper$endJobScheduler$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.content.Context $context;
    @DexIgnore
    public /* final */ /* synthetic */ int $currentMinute;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21159p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.helper.AlarmHelper this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmHelper$endJobScheduler$1(com.portfolio.platform.helper.AlarmHelper alarmHelper, int i, android.content.Context context, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = alarmHelper;
        this.$currentMinute = i;
        this.$context = context;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.helper.AlarmHelper$endJobScheduler$1 alarmHelper$endJobScheduler$1 = new com.portfolio.platform.helper.AlarmHelper$endJobScheduler$1(this.this$0, this.$currentMinute, this.$context, yb4);
        alarmHelper$endJobScheduler$1.f21159p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return alarmHelper$endJobScheduler$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.helper.AlarmHelper$endJobScheduler$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0085 A[SYNTHETIC] */
    public final java.lang.Object invokeSuspend(java.lang.Object r9) {
        /*
            r8 = this;
            java.lang.Object r0 = com.fossil.blesdk.obfuscated.cc4.m20546a()
            int r1 = r8.label
            r2 = 1
            if (r1 == 0) goto L_0x0028
            if (r1 != r2) goto L_0x0020
            java.lang.Object r1 = r8.L$3
            java.util.Iterator r1 = (java.util.Iterator) r1
            java.lang.Object r3 = r8.L$2
            com.portfolio.platform.data.source.local.alarm.Alarm r3 = (com.portfolio.platform.data.source.local.alarm.Alarm) r3
            java.lang.Object r4 = r8.L$1
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r5 = r8.L$0
            com.fossil.blesdk.obfuscated.zg4 r5 = (com.fossil.blesdk.obfuscated.zg4) r5
            com.fossil.blesdk.obfuscated.na4.m25642a((java.lang.Object) r9)
            r9 = r8
            goto L_0x0076
        L_0x0020:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r0)
            throw r9
        L_0x0028:
            com.fossil.blesdk.obfuscated.na4.m25642a((java.lang.Object) r9)
            com.fossil.blesdk.obfuscated.zg4 r9 = r8.f21159p$
            com.portfolio.platform.helper.AlarmHelper r1 = r8.this$0
            com.portfolio.platform.data.source.AlarmsRepository r1 = r1.mo39479a()
            java.util.List r1 = r1.getActiveAlarms()
            if (r1 == 0) goto L_0x00d4
            java.util.Iterator r3 = r1.iterator()
            r5 = r9
            r4 = r1
            r1 = r3
            r9 = r8
        L_0x0041:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto L_0x0085
            java.lang.Object r3 = r1.next()
            com.portfolio.platform.data.source.local.alarm.Alarm r3 = (com.portfolio.platform.data.source.local.alarm.Alarm) r3
            int r6 = r3.getTotalMinutes()
            int r7 = r9.$currentMinute
            if (r6 != r7) goto L_0x0041
            boolean r6 = r3.isRepeated()
            if (r6 != 0) goto L_0x0041
            r6 = 0
            r3.setActive(r6)
            com.portfolio.platform.helper.AlarmHelper r6 = r9.this$0
            com.portfolio.platform.data.source.AlarmsRepository r6 = r6.mo39479a()
            r9.L$0 = r5
            r9.L$1 = r4
            r9.L$2 = r3
            r9.L$3 = r1
            r9.label = r2
            java.lang.Object r6 = r6.updateAlarm(r3, r9)
            if (r6 != r0) goto L_0x0076
            return r0
        L_0x0076:
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.f20941W
            com.fossil.blesdk.obfuscated.bj2 r7 = new com.fossil.blesdk.obfuscated.bj2
            java.lang.String r3 = r3.getUri()
            r7.<init>(r2, r3)
            r6.mo34583a((java.lang.Object) r7)
            goto L_0x0041
        L_0x0085:
            com.portfolio.platform.helper.AlarmHelper r0 = r9.this$0
            com.portfolio.platform.data.source.AlarmsRepository r0 = r0.mo39479a()
            java.util.List r0 = r0.getActiveAlarms()
            java.lang.String r1 = "AlarmHelper"
            if (r0 == 0) goto L_0x00b3
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = "endJobScheduler - getActiveAlarms again onAlarmsLoaded"
            r0.mo33255d(r1, r2)
            com.portfolio.platform.helper.AlarmHelper r0 = r9.this$0
            android.content.Context r9 = r9.$context
            r0.mo39484c(r9)
            com.portfolio.platform.PortfolioApp$a r9 = com.portfolio.platform.PortfolioApp.f20941W
            com.portfolio.platform.PortfolioApp r9 = r9.mo34589c()
            java.util.List r0 = com.fossil.blesdk.obfuscated.nj2.m25699a(r4)
            r9.mo34508a((java.util.List<? extends com.misfit.frameworks.buttonservice.model.Alarm>) r0)
            goto L_0x00db
        L_0x00b3:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = "endJobScheduler - getActiveAlarms again onDataNotAvailable"
            r0.mo33255d(r1, r2)
            com.portfolio.platform.helper.AlarmHelper r0 = r9.this$0
            android.content.Context r9 = r9.$context
            r0.mo39480a(r9)
            com.portfolio.platform.PortfolioApp$a r9 = com.portfolio.platform.PortfolioApp.f20941W
            com.portfolio.platform.PortfolioApp r9 = r9.mo34589c()
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r9.mo34508a((java.util.List<? extends com.misfit.frameworks.buttonservice.model.Alarm>) r0)
            goto L_0x00db
        L_0x00d4:
            com.portfolio.platform.helper.AlarmHelper r9 = r8.this$0
            android.content.Context r0 = r8.$context
            r9.mo39480a(r0)
        L_0x00db:
            com.fossil.blesdk.obfuscated.qa4 r9 = com.fossil.blesdk.obfuscated.qa4.f17909a
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.helper.AlarmHelper$endJobScheduler$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
