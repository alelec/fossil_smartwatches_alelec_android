package com.portfolio.platform.localization;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.localization.LocalizationManager$activityCallbacks$1$onActivityCreated$$inlined$let$lambda$1 */
public final class C5964x80fd64c8 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21233p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.localization.LocalizationManager$activityCallbacks$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C5964x80fd64c8(com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.localization.LocalizationManager$activityCallbacks$1 localizationManager$activityCallbacks$1) {
        super(2, yb4);
        this.this$0 = localizationManager$activityCallbacks$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.localization.C5964x80fd64c8 localizationManager$activityCallbacks$1$onActivityCreated$$inlined$let$lambda$1 = new com.portfolio.platform.localization.C5964x80fd64c8(yb4, this.this$0);
        localizationManager$activityCallbacks$1$onActivityCreated$$inlined$let$lambda$1.f21233p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return localizationManager$activityCallbacks$1$onActivityCreated$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.localization.C5964x80fd64c8) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21233p$;
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            java.io.File filesDir = this.this$0.f21234e.mo39617g().getFilesDir();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) filesDir, "mContext.filesDir");
            sb.append(filesDir.getAbsolutePath());
            sb.append(com.zendesk.sdk.network.impl.ZendeskConfig.SLASH);
            sb.append("language.zip");
            java.lang.String sb2 = sb.toString();
            java.io.File file = new java.io.File(sb2);
            if (!file.exists()) {
                com.portfolio.platform.localization.LocalizationManager localizationManager = this.this$0.f21234e;
                this.L$0 = zg4;
                this.L$1 = sb2;
                this.L$2 = file;
                this.label = 1;
                if (localizationManager.mo39604a((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            java.io.File file2 = (java.io.File) this.L$2;
            java.lang.String str = (java.lang.String) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
