package com.portfolio.platform.localization;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.common.constants.Constants;
import java.lang.ref.WeakReference;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LocalizationManager$activityCallbacks$Anon1 implements Application.ActivityLifecycleCallbacks {
    @DexIgnore
    public /* final */ /* synthetic */ LocalizationManager e;

    @DexIgnore
    public LocalizationManager$activityCallbacks$Anon1(LocalizationManager localizationManager) {
        this.e = localizationManager;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    public void onActivityCreated(Activity activity, Bundle bundle) {
        String str;
        if (activity != null) {
            this.e.a((WeakReference<Activity>) new WeakReference(activity));
            WeakReference<Activity> h = this.e.h();
            if (h != null) {
                Activity activity2 = (Activity) h.get();
                if (activity2 != null) {
                    Class<?> cls = activity2.getClass();
                    if (cls != null) {
                        str = cls.getSimpleName();
                        if (!kd4.a((Object) str, (Object) this.e.f())) {
                            fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new LocalizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1((yb4) null, this), 3, (Object) null);
                            return;
                        }
                        return;
                    }
                }
                str = null;
                if (!kd4.a((Object) str, (Object) this.e.f())) {
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void onActivityDestroyed(Activity activity) {
        kd4.b(activity, Constants.ACTIVITY);
        if (this.e.h() != null) {
            WeakReference<Activity> h = this.e.h();
            if (h == null) {
                kd4.a();
                throw null;
            } else if (kd4.a((Object) (Activity) h.get(), (Object) activity)) {
                this.e.a((WeakReference<Activity>) null);
            }
        }
    }

    @DexIgnore
    public void onActivityPaused(Activity activity) {
        kd4.b(activity, Constants.ACTIVITY);
    }

    @DexIgnore
    public void onActivityResumed(Activity activity) {
        kd4.b(activity, Constants.ACTIVITY);
    }

    @DexIgnore
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        kd4.b(activity, Constants.ACTIVITY);
        kd4.b(bundle, "bundle");
    }

    @DexIgnore
    public void onActivityStarted(Activity activity) {
        kd4.b(activity, Constants.ACTIVITY);
    }

    @DexIgnore
    public void onActivityStopped(Activity activity) {
        kd4.b(activity, Constants.ACTIVITY);
    }
}
