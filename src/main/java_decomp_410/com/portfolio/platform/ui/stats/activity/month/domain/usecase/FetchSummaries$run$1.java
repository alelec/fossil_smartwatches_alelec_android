package com.portfolio.platform.ui.stats.activity.month.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries", f = "FetchSummaries.kt", l = {61}, m = "run")
public final class FetchSummaries$run$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public java.lang.Object L$7;
    @DexIgnore
    public java.lang.Object L$8;
    @DexIgnore
    public java.lang.Object L$9;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FetchSummaries$run$1(com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries fetchSummaries, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = fetchSummaries;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.a((com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries.b) null, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this);
    }
}
