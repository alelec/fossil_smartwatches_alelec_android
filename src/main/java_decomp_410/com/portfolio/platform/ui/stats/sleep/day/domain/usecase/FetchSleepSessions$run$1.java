package com.portfolio.platform.ui.stats.sleep.day.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions", f = "FetchSleepSessions.kt", l = {46}, m = "run")
public final class FetchSleepSessions$run$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FetchSleepSessions$run$1(com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions fetchSleepSessions, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = fetchSleepSessions;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.a((com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions.b) null, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this);
    }
}
