package com.portfolio.platform.ui.debug;

import android.content.Context;
import android.widget.Toast;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.ih4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.service.ShakeFeedbackService;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$Anon1", f = "DebugActivity.kt", l = {294, 295, 296}, m = "invokeSuspend")
public final class DebugActivity$onItemClicked$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DebugActivity this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$Anon1$Anon1", f = "DebugActivity.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity$onItemClicked$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(DebugActivity$onItemClicked$Anon1 debugActivity$onItemClicked$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = debugActivity$onItemClicked$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                Toast.makeText(this.this$Anon0.this$Anon0, "Log will be sent after 1 second", 1).show();
                this.this$Anon0.this$Anon0.finish();
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DebugActivity$onItemClicked$Anon1(DebugActivity debugActivity, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = debugActivity;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DebugActivity$onItemClicked$Anon1 debugActivity$onItemClicked$Anon1 = new DebugActivity$onItemClicked$Anon1(this.this$Anon0, yb4);
        debugActivity$onItemClicked$Anon1.p$ = (zg4) obj;
        return debugActivity$onItemClicked$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DebugActivity$onItemClicked$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006b A[RETURN] */
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        pi4 c;
        Anon1 anon1;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg42 = this.p$;
            ShakeFeedbackService B = this.this$Anon0.B();
            DebugActivity debugActivity = this.this$Anon0;
            this.L$Anon0 = zg42;
            this.label = 1;
            if (B.a((Context) debugActivity, (yb4<? super qa4>) this) == a) {
                return a;
            }
            zg4 = zg42;
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            c = nh4.c();
            anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 3;
            if (yf4.a(c, anon1, this) == a) {
                return a;
            }
            return qa4.a;
        } else if (i == 3) {
            zg4 zg43 = (zg4) this.L$Anon0;
            na4.a(obj);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.L$Anon0 = zg4;
        this.label = 2;
        if (ih4.a(1000, this) == a) {
            return a;
        }
        c = nh4.c();
        anon1 = new Anon1(this, (yb4) null);
        this.L$Anon0 = zg4;
        this.label = 3;
        if (yf4.a(c, anon1, this) == a) {
        }
        return qa4.a;
    }
}
