package com.portfolio.platform.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepQualityChart extends View {
    @DexIgnore
    public Paint e; // = new Paint(1);
    @DexIgnore
    public float f; // = 10.0f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public Drawable i;
    @DexIgnore
    public Bitmap j;
    @DexIgnore
    public int k; // = 50;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public SleepQualityChart(Context context) {
        super(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a(float f2) {
        this.e.setDither(true);
        this.e.setStyle(Paint.Style.STROKE);
        this.e.setStrokeWidth(f2);
        this.e.setAntiAlias(true);
    }

    @DexIgnore
    public final void b(float f2) {
        this.e.setShader(new LinearGradient(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.g, this.h, Shader.TileMode.CLAMP));
        this.e.setPathEffect(new DashPathEffect(new float[]{f2 / ((float) 5), 6.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        b((float) getWidth());
        if (canvas != null) {
            Bitmap bitmap = this.j;
            float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (bitmap != null) {
                float width = (float) bitmap.getWidth();
                float height = (float) bitmap.getHeight();
                float width2 = ((float) (canvas.getWidth() * this.k)) / 100.0f;
                if (width2 + width > ((float) canvas.getWidth())) {
                    width2 = ((float) canvas.getWidth()) - width;
                }
                canvas.drawBitmap(bitmap, width2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.e);
                f2 = height;
            }
            float f3 = f2 + (this.f / ((float) 2)) + 2.0f;
            canvas.drawLine(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3, (float) canvas.getWidth(), f3, this.e);
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        Bitmap bitmap = this.j;
        if (bitmap != null) {
            bitmap.getHeight();
        }
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        int i4 = 0;
        if (!(mode == Integer.MIN_VALUE || mode == 0 || mode == 1073741824)) {
            size = 0;
        }
        if (mode2 != Integer.MIN_VALUE) {
            if (mode2 == 0) {
                Bitmap bitmap2 = this.j;
                if (bitmap2 != null) {
                    i4 = bitmap2.getHeight();
                }
                size2 = (((int) this.f) * 2) + i4;
            } else if (mode2 != 1073741824) {
                size2 = 0;
            }
        }
        setMeasuredDimension(size, size2);
    }

    @DexIgnore
    public final void setPercent(int i2) {
        this.k = i2;
        invalidate();
    }

    @DexIgnore
    public SleepQualityChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Bitmap bitmap = null;
        TypedArray obtainStyledAttributes = context != null ? context.obtainStyledAttributes(attributeSet, h62.SleepQualityChart) : null;
        if (obtainStyledAttributes != null) {
            this.f = obtainStyledAttributes.getDimension(4, 10.0f);
            this.g = obtainStyledAttributes.getColor(1, 0);
            this.h = obtainStyledAttributes.getColor(0, 0);
            this.i = obtainStyledAttributes.getDrawable(3);
            this.k = obtainStyledAttributes.getInt(2, 0);
        }
        a(this.f);
        BitmapDrawable bitmapDrawable = (BitmapDrawable) this.i;
        this.j = bitmapDrawable != null ? bitmapDrawable.getBitmap() : bitmap;
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.recycle();
        }
    }
}
