package com.portfolio.platform.ui.view.chart.overview;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ll2;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.wr3;
import com.fossil.blesdk.obfuscated.xr2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.enums.GoalType;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class OverviewSleepDayChart extends BarChart {
    @DexIgnore
    public float u0;
    @DexIgnore
    public float v0;
    @DexIgnore
    public float w0;
    @DexIgnore
    public int x0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public OverviewSleepDayChart(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public void a(float f, float f2) {
        if (getMChartModel().a().size() != 0) {
            ArrayList<ArrayList<BarChart.b>> c = getMChartModel().a().get(0).c();
            Iterator<ArrayList<BarChart.b>> it = c.iterator();
            while (it.hasNext()) {
                ArrayList next = it.next();
                Object obj = next.get(0);
                kd4.a(obj, "session[0]");
                BarChart.b bVar = (BarChart.b) obj;
                Object obj2 = next.get(next.size() - 1);
                kd4.a(obj2, "session[session.size - 1]");
                BarChart.b bVar2 = (BarChart.b) obj2;
                long j = (long) 1000;
                long e = ((long) bVar.e()) * j;
                long d = ((((long) bVar2.d()) - 1) * ((long) 60) * j) + e;
                Rect rect = new Rect();
                int i = this.x0;
                String a2 = i != -1 ? rk2.a(e, i) : rk2.b(e);
                int i2 = this.x0;
                String a3 = i2 != -1 ? rk2.a(d, i2) : rk2.b(d);
                ll2 ll2 = ll2.b;
                kd4.a((Object) a2, "startTimeString");
                String b = ll2.b(a2);
                ll2 ll22 = ll2.b;
                kd4.a((Object) a3, "endTimeString");
                String b2 = ll22.b(a3);
                getMLegendPaint().getTextBounds(b, 0, StringsKt__StringsKt.c(b), rect);
                float mTextMargin = ((float) getMTextMargin()) + ((float) rect.height());
                if (bVar2.a().right - bVar.a().left > ((float) (rect.right * 2))) {
                    getMTextPoint().add(new Pair(b, new PointF(bVar.a().left, mTextMargin)));
                    getMTextPoint().add(new Pair(b2, new PointF((bVar2.a().right - f) - ((float) rect.right), mTextMargin)));
                } else if (c.indexOf(next) == c.size() - 1) {
                    getMTextPoint().add(new Pair(b2, new PointF((bVar2.a().right - f) - ((float) rect.right), mTextMargin)));
                } else {
                    getMTextPoint().add(new Pair(b, new PointF(bVar.a().left, mTextMargin)));
                }
            }
        }
    }

    @DexIgnore
    public void d() {
        float f;
        float f2;
        float f3;
        float f4;
        float f5;
        float f6;
        float f7;
        float f8;
        float f9;
        float f10;
        ArrayList<BarChart.a> a2 = getMChartModel().a();
        if (a2.size() != 0) {
            setMGoalLinePath(new Path());
            setMGoalIconPoint(new PointF());
            setMGoalIconShow(false);
            RectF rectF = new RectF(getMBarMargin(), getMSafeAreaHeight(), ((float) getMGraphWidth()) - getMBarMargin(), (float) getMGraphHeight());
            getMGraphHeight();
            float f11 = rectF.left;
            float f12 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            ArrayList<ArrayList<BarChart.b>> c = a2.get(0).c();
            Iterator<ArrayList<BarChart.b>> it = c.iterator();
            int i = 0;
            while (it.hasNext()) {
                ArrayList next = it.next();
                i += ((BarChart.b) next.get(0)).d();
                if (c.indexOf(next) != 0) {
                    i += 10;
                }
            }
            float f13 = (rectF.right - rectF.left) / ((float) i);
            Iterator<ArrayList<BarChart.b>> it2 = c.iterator();
            while (it2.hasNext()) {
                ArrayList next2 = it2.next();
                float mGraphHeight = ((float) getMGraphHeight()) * this.u0;
                int size = next2.size() - 1;
                if (size >= 0) {
                    float f14 = f12;
                    float f15 = f11;
                    int i2 = 0;
                    while (true) {
                        if (i2 < next2.size() - 1) {
                            f14 = (((float) (((BarChart.b) next2.get(i2 + 1)).b() - ((BarChart.b) next2.get(i2)).b())) * f13) + f15;
                            int i3 = xr2.b[((BarChart.b) next2.get(i2)).c().ordinal()];
                            if (i3 != 1) {
                                if (i3 == 2) {
                                    f10 = (float) getMGraphHeight();
                                    f9 = (float) getMGraphHeight();
                                    f8 = this.v0;
                                } else if (i3 == 3) {
                                    f10 = (float) getMGraphHeight();
                                    f9 = (float) getMGraphHeight();
                                    f8 = this.w0;
                                } else {
                                    throw new NoWhenBranchMatchedException();
                                }
                                f6 = (f10 - (f9 * f8)) - mGraphHeight;
                            } else {
                                f6 = ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.u0);
                            }
                            int i4 = xr2.c[((BarChart.b) next2.get(i2)).c().ordinal()];
                            if (i4 == 1) {
                                f7 = (float) getMGraphHeight();
                            } else if (i4 == 2 || i4 == 3) {
                                f7 = ((float) getMGraphHeight()) - mGraphHeight;
                            } else {
                                throw new NoWhenBranchMatchedException();
                            }
                            ((BarChart.b) next2.get(i2)).a(new RectF(f15, f6, f14, f7));
                            f15 = f14;
                        } else if (i2 == next2.size() - 1) {
                            f14 = (((float) (((BarChart.b) next2.get(i2)).d() - ((BarChart.b) next2.get(i2)).b())) * f13) + f15;
                            int i5 = xr2.d[((BarChart.b) next2.get(i2)).c().ordinal()];
                            if (i5 != 1) {
                                if (i5 == 2) {
                                    f5 = (float) getMGraphHeight();
                                    f4 = (float) getMGraphHeight();
                                    f3 = this.v0;
                                } else if (i5 == 3) {
                                    f5 = (float) getMGraphHeight();
                                    f4 = (float) getMGraphHeight();
                                    f3 = this.w0;
                                } else {
                                    throw new NoWhenBranchMatchedException();
                                }
                                f = (f5 - (f4 * f3)) - mGraphHeight;
                            } else {
                                f = ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.u0);
                            }
                            int i6 = xr2.e[((BarChart.b) next2.get(i2)).c().ordinal()];
                            if (i6 == 1) {
                                f2 = (float) getMGraphHeight();
                            } else if (i6 == 2 || i6 == 3) {
                                f2 = ((float) getMGraphHeight()) - mGraphHeight;
                            } else {
                                throw new NoWhenBranchMatchedException();
                            }
                            ((BarChart.b) next2.get(next2.size() - 1)).a(new RectF(f15, f, f14, f2));
                        }
                        if (i2 == size) {
                            f12 = f14;
                            break;
                        }
                        i2++;
                    }
                }
                f11 = f12 + (((float) 10) * f13);
                f12 = f11;
            }
        }
    }

    @DexIgnore
    public void e(Canvas canvas) {
        Canvas canvas2 = canvas;
        kd4.b(canvas2, "canvas");
        canvas2.drawRect(new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.u0), (float) getMGraphWidth(), (((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.u0)) + ((float) 2)), getMLegendLinePaint());
        Iterator<BarChart.a> it = getMChartModel().a().iterator();
        while (it.hasNext()) {
            Iterator<ArrayList<BarChart.b>> it2 = it.next().c().iterator();
            while (it2.hasNext()) {
                Iterator it3 = it2.next().iterator();
                while (it3.hasNext()) {
                    BarChart.b bVar = (BarChart.b) it3.next();
                    int i = xr2.a[bVar.c().ordinal()];
                    if (i == 1) {
                        getMGraphPaint().setColor(getMLowestColor());
                        wr3.a(canvas, bVar.a().left, bVar.a().top, bVar.a().right, bVar.a().bottom, getMBarRadius(), getMBarRadius(), false, false, true, true, getMGraphPaint());
                    } else if (i == 2) {
                        getMGraphPaint().setColor(getMDefaultColor());
                        wr3.a(canvas, bVar.a().left, bVar.a().top, bVar.a().right, bVar.a().bottom, getMBarRadius(), getMBarRadius(), true, true, false, false, getMGraphPaint());
                    } else if (i == 3) {
                        getMGraphPaint().setColor(getMHighestColor());
                        wr3.a(canvas, bVar.a().left, bVar.a().top, bVar.a().right, bVar.a().bottom, getMBarRadius(), getMBarRadius(), true, true, false, false, getMGraphPaint());
                    }
                }
            }
        }
    }

    @DexIgnore
    public void i(Canvas canvas) {
        kd4.b(canvas, "canvas");
    }

    @DexIgnore
    public final void setTimeZoneOffsetInSecond(int i) {
        this.x0 = i;
    }

    @DexIgnore
    public OverviewSleepDayChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewSleepDayChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewSleepDayChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.x0 = -1;
        if (attributeSet != null && context != null) {
            Resources.Theme theme = context.getTheme();
            if (theme != null) {
                TypedArray obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, h62.OverviewSleepDayChart, 0, 0);
                if (obtainStyledAttributes != null) {
                    try {
                        this.u0 = obtainStyledAttributes.getFloat(0, 0.1f);
                        this.v0 = obtainStyledAttributes.getFloat(2, 0.33f);
                        this.w0 = obtainStyledAttributes.getFloat(1, 0.6f);
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String tag = getTAG();
                        local.d(tag, "constructor - e=" + e);
                    } catch (Throwable th) {
                        obtainStyledAttributes.recycle();
                        throw th;
                    }
                    obtainStyledAttributes.recycle();
                }
            }
        }
    }

    @DexIgnore
    public void e() {
        if (getMGoalType() == GoalType.TOTAL_SLEEP && getMChartModel().a().size() != 0) {
            RectF rectF = new RectF(getMBarMargin(), getMSafeAreaHeight(), ((float) getMGraphWidth()) - getMBarMargin(), (float) getMGraphHeight());
            ArrayList<BarChart.a> a2 = getMChartModel().a();
            int b = getMChartModel().b();
            ArrayList<ArrayList<BarChart.b>> c = a2.get(0).c();
            Iterator<ArrayList<BarChart.b>> it = c.iterator();
            int i = 0;
            while (it.hasNext()) {
                ArrayList next = it.next();
                i += ((BarChart.b) next.get(0)).d();
                if (c.indexOf(next) != 0) {
                    i += 10;
                    b += 10;
                }
            }
            float f = (((float) b) * (rectF.right - rectF.left)) / ((float) i);
            Iterator<ArrayList<BarChart.b>> it2 = c.iterator();
            while (it2.hasNext()) {
                ArrayList next2 = it2.next();
                if (f <= ((BarChart.b) next2.get(next2.size() - 1)).a().right) {
                    Iterator it3 = next2.iterator();
                    while (it3.hasNext()) {
                        BarChart.b bVar = (BarChart.b) it3.next();
                        if (f >= bVar.a().left && f <= bVar.a().right) {
                            getMGoalLinePath().moveTo(f, getMSafeAreaHeight() - 10.0f);
                            getMGoalLinePath().lineTo(f, bVar.a().top);
                            getMGoalIconPoint().set(f - (((float) getMGoalIconSize()) * 0.5f), (getMSafeAreaHeight() * 0.5f) - 20.0f);
                            setMGoalIconShow(false);
                            return;
                        }
                    }
                    continue;
                }
            }
        }
    }
}
