package com.portfolio.platform.ui;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.BaseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1", f = "BaseActivity.kt", l = {}, m = "invokeSuspend")
public final class BaseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BaseActivity$mButtonServiceConnection$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1(BaseActivity$mButtonServiceConnection$Anon1 baseActivity$mButtonServiceConnection$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = baseActivity$mButtonServiceConnection$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        BaseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1 baseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1 = new BaseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1(this.this$Anon0, yb4);
        baseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1.p$ = (zg4) obj;
        return baseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BaseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            PortfolioApp.a aVar = PortfolioApp.W;
            IButtonConnectivity a = BaseActivity.A.a();
            if (a != null) {
                aVar.b(a);
                try {
                    MFUser currentUser = this.this$Anon0.a.d().getCurrentUser();
                    if (!(currentUser == null || BaseActivity.A.a() == null)) {
                        IButtonConnectivity a2 = BaseActivity.A.a();
                        if (a2 != null) {
                            a2.updateUserId(currentUser.getUserId());
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    this.this$Anon0.a.n();
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String f = this.this$Anon0.a.f();
                    local.e(f, ".onServiceConnected(), ex=" + e);
                    e.printStackTrace();
                }
                return qa4.a;
            }
            kd4.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
