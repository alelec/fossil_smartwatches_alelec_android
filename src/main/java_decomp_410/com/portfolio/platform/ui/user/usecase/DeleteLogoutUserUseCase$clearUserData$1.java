package com.portfolio.platform.ui.user.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$1", f = "DeleteLogoutUserUseCase.kt", l = {148}, m = "invokeSuspend")
public final class DeleteLogoutUserUseCase$clearUserData$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeleteLogoutUserUseCase$clearUserData$1(com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase deleteLogoutUserUseCase, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = deleteLogoutUserUseCase;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$1 deleteLogoutUserUseCase$clearUserData$1 = new com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$1(this.this$0, yb4);
        deleteLogoutUserUseCase$clearUserData$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return deleteLogoutUserUseCase$clearUserData$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.p$;
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().flush();
            try {
                com.fossil.blesdk.obfuscated.is3.b.f(com.misfit.frameworks.buttonservice.ButtonService.DEVICE_SECRET_KEY);
            } catch (java.lang.Exception e) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a2 = com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase.I.a();
                local.e(a2, "exception when remove alias in keystore " + e);
            }
            this.this$0.k.clearAllNotificationSetting();
            this.this$0.m.cleanUp();
            this.this$0.q.cleanUp();
            this.this$0.A.cleanUp();
            this.this$0.f.V();
            this.this$0.d.clearAllUser();
            com.portfolio.platform.PortfolioApp.W.c().S();
            com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase deleteLogoutUserUseCase = this.this$0;
            this.L$0 = zg4;
            this.label = 1;
            if (deleteLogoutUserUseCase.a((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.x.cleanUp();
        this.this$0.y.cleanUp();
        this.this$0.z.cleanUp();
        this.this$0.g.cleanUp();
        this.this$0.w.cleanUp();
        this.this$0.h.cleanUp();
        this.this$0.i.cleanUp();
        this.this$0.n.cleanUp();
        this.this$0.r.cleanUp();
        this.this$0.s.cleanUp();
        this.this$0.t.cleanUp();
        this.this$0.j.clearData();
        this.this$0.e.cleanUp();
        com.fossil.blesdk.obfuscated.dn2.p.a().o();
        com.fossil.blesdk.obfuscated.ap2.g.a();
        com.portfolio.platform.PortfolioApp.W.c().c();
        com.portfolio.platform.PortfolioApp.W.c().F();
        this.this$0.u.getNotificationSettingsDao().delete();
        this.this$0.v.getDNDScheduledTimeDao().delete();
        this.this$0.E.getInactivityNudgeTimeDao().delete();
        this.this$0.E.getRemindTimeDao().delete();
        this.this$0.B.cleanUp();
        this.this$0.C.cleanUp();
        this.this$0.D.cleanUp();
        com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase w = this.this$0.F;
        w.getGFitSampleDao().clearAll();
        w.getGFitActiveTimeDao().clearAll();
        w.getGFitWorkoutSessionDao().clearAll();
        w.getUASampleDao().clearAll();
        this.this$0.a(new com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase.d());
        this.this$0.l.cleanUp();
        return com.fossil.blesdk.obfuscated.qa4.a;
    }
}
