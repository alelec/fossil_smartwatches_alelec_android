package com.portfolio.platform.ui.user.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase", f = "DeleteLogoutUserUseCase.kt", l = {187}, m = "removeAllConnectedApps")
public final class DeleteLogoutUserUseCase$removeAllConnectedApps$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeleteLogoutUserUseCase$removeAllConnectedApps$1(com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase deleteLogoutUserUseCase, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = deleteLogoutUserUseCase;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.a((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this);
    }
}
