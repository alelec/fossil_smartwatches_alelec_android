package com.portfolio.platform.ui.user.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DownloadUserInfoUseCase extends CoroutineUseCase<DownloadUserInfoUseCase.c, DownloadUserInfoUseCase.d, DownloadUserInfoUseCase.b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(int i, String str) {
            kd4.b(str, "errorMessage");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ MFUser a;

        @DexIgnore
        public d(MFUser mFUser) {
            this.a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.a;
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = DownloadUserInfoUseCase.class.getSimpleName();
        kd4.a((Object) simpleName, "DownloadUserInfoUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public DownloadUserInfoUseCase(UserRepository userRepository) {
        kd4.b(userRepository, "userRepository");
        this.d = userRepository;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0082, code lost:
        if (r7 != null) goto L_0x0087;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(c cVar, yb4<Object> yb4) {
        DownloadUserInfoUseCase$run$Anon1 downloadUserInfoUseCase$run$Anon1;
        int i;
        qo2 qo2;
        String str;
        if (yb4 instanceof DownloadUserInfoUseCase$run$Anon1) {
            downloadUserInfoUseCase$run$Anon1 = (DownloadUserInfoUseCase$run$Anon1) yb4;
            int i2 = downloadUserInfoUseCase$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                downloadUserInfoUseCase$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = downloadUserInfoUseCase$run$Anon1.result;
                Object a2 = cc4.a();
                i = downloadUserInfoUseCase$run$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d(e, "running UseCase");
                    UserRepository userRepository = this.d;
                    downloadUserInfoUseCase$run$Anon1.L$Anon0 = this;
                    downloadUserInfoUseCase$run$Anon1.L$Anon1 = cVar;
                    downloadUserInfoUseCase$run$Anon1.label = 1;
                    obj = userRepository.loadUserInfo(downloadUserInfoUseCase$run$Anon1);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    c cVar2 = (c) downloadUserInfoUseCase$run$Anon1.L$Anon1;
                    DownloadUserInfoUseCase downloadUserInfoUseCase = (DownloadUserInfoUseCase) downloadUserInfoUseCase$run$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    return new d((MFUser) ((ro2) qo2).a());
                }
                if (!(qo2 instanceof po2)) {
                    return qa4.a;
                }
                po2 po2 = (po2) qo2;
                int a3 = po2.a();
                ServerError c2 = po2.c();
                if (c2 != null) {
                    str = c2.getMessage();
                }
                str = "";
                return new b(a3, str);
            }
        }
        downloadUserInfoUseCase$run$Anon1 = new DownloadUserInfoUseCase$run$Anon1(this, yb4);
        Object obj2 = downloadUserInfoUseCase$run$Anon1.result;
        Object a22 = cc4.a();
        i = downloadUserInfoUseCase$run$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
        throw null;
    }
}
