package com.portfolio.platform.ui.user.usecase;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase", f = "SignUpSocialUseCase.kt", l = {26}, m = "run")
public final class SignUpSocialUseCase$run$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ SignUpSocialUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SignUpSocialUseCase$run$Anon1(SignUpSocialUseCase signUpSocialUseCase, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = signUpSocialUseCase;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((SignUpSocialUseCase.a) null, (yb4<Object>) this);
    }
}
