package com.portfolio.platform.ui;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.BaseActivity$mButtonServiceConnection$1$onServiceConnected$1", f = "BaseActivity.kt", l = {}, m = "invokeSuspend")
public final class BaseActivity$mButtonServiceConnection$1$onServiceConnected$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.BaseActivity$mButtonServiceConnection$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseActivity$mButtonServiceConnection$1$onServiceConnected$1(com.portfolio.platform.ui.BaseActivity$mButtonServiceConnection$1 baseActivity$mButtonServiceConnection$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = baseActivity$mButtonServiceConnection$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.ui.BaseActivity$mButtonServiceConnection$1$onServiceConnected$1 baseActivity$mButtonServiceConnection$1$onServiceConnected$1 = new com.portfolio.platform.ui.BaseActivity$mButtonServiceConnection$1$onServiceConnected$1(this.this$0, yb4);
        baseActivity$mButtonServiceConnection$1$onServiceConnected$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return baseActivity$mButtonServiceConnection$1$onServiceConnected$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.ui.BaseActivity$mButtonServiceConnection$1$onServiceConnected$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.portfolio.platform.PortfolioApp.a aVar = com.portfolio.platform.PortfolioApp.W;
            com.misfit.frameworks.buttonservice.IButtonConnectivity a = com.portfolio.platform.ui.BaseActivity.A.a();
            if (a != null) {
                aVar.b(a);
                try {
                    com.portfolio.platform.data.model.MFUser currentUser = this.this$0.a.d().getCurrentUser();
                    if (!(currentUser == null || com.portfolio.platform.ui.BaseActivity.A.a() == null)) {
                        com.misfit.frameworks.buttonservice.IButtonConnectivity a2 = com.portfolio.platform.ui.BaseActivity.A.a();
                        if (a2 != null) {
                            a2.updateUserId(currentUser.getUserId());
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.a();
                            throw null;
                        }
                    }
                    this.this$0.a.n();
                } catch (java.lang.Exception e) {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String f = this.this$0.a.f();
                    local.e(f, ".onServiceConnected(), ex=" + e);
                    e.printStackTrace();
                }
                return com.fossil.blesdk.obfuscated.qa4.a;
            }
            com.fossil.blesdk.obfuscated.kd4.a();
            throw null;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
