package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.PortfolioApp;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1", f = "SwitchActiveDeviceUseCase.kt", l = {74}, m = "invokeSuspend")
public final class SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1(SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1 switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1, String str, MisfitDeviceProfile misfitDeviceProfile, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1;
        this.$serial = str;
        this.$currentDeviceProfile = misfitDeviceProfile;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1 switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1 = new SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1(this.this$Anon0, this.$serial, this.$currentDeviceProfile, yb4);
        switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1.p$ = (zg4) obj;
        return switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.this$Anon0.a;
            String str = this.$serial;
            kd4.a((Object) str, "serial");
            MisfitDeviceProfile misfitDeviceProfile = this.$currentDeviceProfile;
            if (misfitDeviceProfile != null) {
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = switchActiveDeviceUseCase.a(str, misfitDeviceProfile, this);
                if (obj == a) {
                    return a;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair = (Pair) obj;
        boolean booleanValue = ((Boolean) pair.component1()).booleanValue();
        int intValue = ((Number) pair.component2()).intValue();
        PortfolioApp c = PortfolioApp.W.c();
        String str2 = this.$serial;
        kd4.a((Object) str2, "serial");
        c.a(str2, booleanValue, intValue);
        return qa4.a;
    }
}
