package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import android.os.Bundle;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.ra4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;
import java.util.ArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1 implements BleCommandResultManager.b {
    @DexIgnore
    public /* final */ /* synthetic */ SwitchActiveDeviceUseCase a;

    @DexIgnore
    public SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1(SwitchActiveDeviceUseCase switchActiveDeviceUseCase) {
        this.a = switchActiveDeviceUseCase;
    }

    @DexIgnore
    public void a(CommunicateMode communicateMode, Intent intent) {
        kd4.b(communicateMode, "communicateMode");
        kd4.b(intent, "intent");
        int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
        String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
        if (communicateMode == CommunicateMode.SWITCH_DEVICE) {
            SwitchActiveDeviceUseCase.b f = this.a.f();
            if (kd4.a((Object) stringExtra, (Object) f != null ? f.b() : null)) {
                boolean z = true;
                if (intExtra == ServiceActionResult.ASK_FOR_LINK_SERVER.ordinal()) {
                    if (intent.getExtras() == null) {
                        z = false;
                    }
                    if (!ra4.a || z) {
                        Bundle extras = intent.getExtras();
                        if (extras != null) {
                            fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1(this, stringExtra, (MisfitDeviceProfile) extras.getParcelable("device"), (yb4) null), 3, (Object) null);
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        throw new AssertionError("Assertion failed");
                    }
                } else if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.a.i();
                    FLogger.INSTANCE.getLocal().d(SwitchActiveDeviceUseCase.n.a(), "Switch device  success");
                    if (intent.getExtras() == null) {
                        z = false;
                    }
                    if (!ra4.a || z) {
                        Bundle extras2 = intent.getExtras();
                        if (extras2 != null) {
                            MisfitDeviceProfile misfitDeviceProfile = (MisfitDeviceProfile) extras2.getParcelable("device");
                            if (misfitDeviceProfile != null) {
                                if (misfitDeviceProfile.getHeartRateMode() != HeartRateMode.NONE) {
                                    this.a.l.a(misfitDeviceProfile.getHeartRateMode());
                                }
                                SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.a;
                                kd4.a((Object) stringExtra, "serial");
                                switchActiveDeviceUseCase.b(stringExtra);
                                fi4 unused2 = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2(this, misfitDeviceProfile, stringExtra, (yb4) null), 3, (Object) null);
                                PortfolioApp.W.c().j(stringExtra);
                                SwitchActiveDeviceUseCase switchActiveDeviceUseCase2 = this.a;
                                switchActiveDeviceUseCase2.a(new SwitchActiveDeviceUseCase.d(switchActiveDeviceUseCase2.e()));
                                return;
                            }
                            kd4.a();
                            throw null;
                        }
                        kd4.a();
                        throw null;
                    }
                    throw new AssertionError("Assertion failed");
                } else if (intExtra == ServiceActionResult.FAILED.ordinal()) {
                    this.a.i();
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>();
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = SwitchActiveDeviceUseCase.n.a();
                    local.d(a2, "stop current workout fail due to " + intExtra2);
                    if (intExtra2 != 1101) {
                        if (intExtra2 == 1928) {
                            SwitchActiveDeviceUseCase.c g = this.a.g();
                            if (g == null || this.a.a(g) == null) {
                                this.a.a(new SwitchActiveDeviceUseCase.c(116, (ArrayList<Integer>) null, ""));
                                return;
                            }
                            return;
                        } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                            this.a.a(new SwitchActiveDeviceUseCase.c(117, (ArrayList<Integer>) null, ""));
                            return;
                        }
                    }
                    this.a.a(new SwitchActiveDeviceUseCase.c(113, integerArrayListExtra, ""));
                }
            }
        }
    }
}
