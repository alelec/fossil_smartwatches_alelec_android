package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.nn3;
import com.fossil.blesdk.obfuscated.on3;
import com.fossil.blesdk.obfuscated.pn3;
import com.fossil.blesdk.obfuscated.vj2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yf4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SwitchActiveDeviceUseCase extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public b d;
    @DexIgnore
    public Device e;
    @DexIgnore
    public c f;
    @DexIgnore
    public /* final */ BleCommandResultManager.b g; // = new SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1(this);
    @DexIgnore
    public /* final */ UserRepository h;
    @DexIgnore
    public /* final */ DeviceRepository i;
    @DexIgnore
    public /* final */ vj2 j;
    @DexIgnore
    public /* final */ PortfolioApp k;
    @DexIgnore
    public /* final */ en2 l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return SwitchActiveDeviceUseCase.m;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(String str, int i) {
            kd4.b(str, "newActiveSerial");
            this.a = str;
            this.b = i;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public c(int i, ArrayList<Integer> arrayList, String str) {
            this.a = i;
            this.b = arrayList;
            this.c = str;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> c() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ Device a;

        @DexIgnore
        public d(Device device) {
            this.a = device;
        }

        @DexIgnore
        public final Device a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CoroutineUseCase.e<pn3, nn3> {
        @DexIgnore
        /* renamed from: a */
        public void onSuccess(pn3 pn3) {
            kd4.b(pn3, "responseValue");
            FLogger.INSTANCE.getLocal().d(SwitchActiveDeviceUseCase.n.a(), "getDeviceSetting success");
        }

        @DexIgnore
        public void a(nn3 nn3) {
            kd4.b(nn3, "errorValue");
            FLogger.INSTANCE.getLocal().d(SwitchActiveDeviceUseCase.n.a(), "getDeviceSetting fail");
        }
    }

    /*
    static {
        String simpleName = SwitchActiveDeviceUseCase.class.getSimpleName();
        kd4.a((Object) simpleName, "SwitchActiveDeviceUseCase::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public SwitchActiveDeviceUseCase(UserRepository userRepository, DeviceRepository deviceRepository, vj2 vj2, PortfolioApp portfolioApp, en2 en2) {
        kd4.b(userRepository, "mUserRepository");
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(vj2, "mDeviceSettingFactory");
        kd4.b(portfolioApp, "mApp");
        kd4.b(en2, "mSharePrefs");
        this.h = userRepository;
        this.i = deviceRepository;
        this.j = vj2;
        this.k = portfolioApp;
        this.l = en2;
    }

    @DexIgnore
    public final Device e() {
        return this.e;
    }

    @DexIgnore
    public final b f() {
        return this.d;
    }

    @DexIgnore
    public final c g() {
        return this.f;
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d(m, "registerReceiver ");
        BleCommandResultManager.d.b(this.g, CommunicateMode.SWITCH_DEVICE);
        BleCommandResultManager.d.a(this.g, CommunicateMode.SWITCH_DEVICE);
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d(m, "unregisterReceiver ");
        BleCommandResultManager.d.b(this.g, CommunicateMode.SWITCH_DEVICE);
    }

    @DexIgnore
    public final void b(String str) {
        kd4.b(str, "serial");
        FLogger.INSTANCE.getLocal().d(m, "getDeviceSetting start");
        this.j.a(str).a(new on3(str), new e());
    }

    @DexIgnore
    public String c() {
        return m;
    }

    @DexIgnore
    public final fi4 d() {
        return ag4.b(b(), (CoroutineContext) null, (CoroutineStart) null, new SwitchActiveDeviceUseCase$doSwitchDevice$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(Device device) {
        this.e = device;
    }

    @DexIgnore
    public final void a(c cVar) {
        this.f = cVar;
    }

    @DexIgnore
    public Object a(b bVar, yb4<Object> yb4) {
        if (bVar == null) {
            return new c(600, (ArrayList<Integer>) null, "");
        }
        this.d = bVar;
        String e2 = this.k.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "run with mode " + bVar.a() + " currentActive " + e2);
        if (bVar.a() != 4) {
            d();
        } else {
            a(bVar.b());
        }
        return new Object();
    }

    @DexIgnore
    public final fi4 a(String str) {
        return ag4.b(b(), (CoroutineContext) null, (CoroutineStart) null, new SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1(this, str, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final /* synthetic */ Object a(String str, MisfitDeviceProfile misfitDeviceProfile, yb4<? super Pair<Boolean, Integer>> yb4) {
        return yf4.a(nh4.b(), new SwitchActiveDeviceUseCase$linkServer$Anon2(this, misfitDeviceProfile, str, (yb4) null), yb4);
    }
}
