package com.portfolio.platform.ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$linkServer$2", f = "SwitchActiveDeviceUseCase.kt", l = {232, 239}, m = "invokeSuspend")
public final class SwitchActiveDeviceUseCase$linkServer$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super kotlin.Pair<? extends java.lang.Boolean, ? extends java.lang.Integer>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile $currentDeviceProfile;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serial;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$linkServer$2(com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase, com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile misfitDeviceProfile, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = switchActiveDeviceUseCase;
        this.$currentDeviceProfile = misfitDeviceProfile;
        this.$serial = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$linkServer$2 switchActiveDeviceUseCase$linkServer$2 = new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$linkServer$2(this.this$0, this.$currentDeviceProfile, this.$serial, yb4);
        switchActiveDeviceUseCase$linkServer$2.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return switchActiveDeviceUseCase$linkServer$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$linkServer$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00bd, code lost:
        if (r22 == null) goto L_0x00c0;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object obj2;
        java.lang.Object obj3;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        com.portfolio.platform.data.model.Device device;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        java.lang.String str = null;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            zg4 = this.p$;
            com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.this$0;
            com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile misfitDeviceProfile = this.$currentDeviceProfile;
            if (misfitDeviceProfile != null) {
                int batteryLevel = misfitDeviceProfile.getBatteryLevel();
                if (batteryLevel < 0 || 100 < batteryLevel) {
                    batteryLevel = batteryLevel < 0 ? 0 : 100;
                }
                int b = com.fossil.blesdk.obfuscated.yk2.b(misfitDeviceProfile.getVibrationStrength().getVibrationStrengthLevel());
                com.portfolio.platform.data.model.Device deviceBySerial = this.this$0.i.getDeviceBySerial(this.$serial);
                if (deviceBySerial != null) {
                    deviceBySerial.setBatteryLevel(batteryLevel);
                    deviceBySerial.setFirmwareRevision(misfitDeviceProfile.getFirmwareVersion());
                    deviceBySerial.setMacAddress(misfitDeviceProfile.getAddress());
                    deviceBySerial.setMajor(misfitDeviceProfile.getMicroAppMajorVersion());
                    deviceBySerial.setMinor(misfitDeviceProfile.getMicroAppMinorVersion());
                    if (deviceBySerial != null) {
                        device = deviceBySerial;
                    }
                }
                device = new com.portfolio.platform.data.model.Device(misfitDeviceProfile.getDeviceSerial(), misfitDeviceProfile.getAddress(), misfitDeviceProfile.getDeviceModel(), misfitDeviceProfile.getFirmwareVersion(), batteryLevel, com.fossil.blesdk.obfuscated.dc4.a(b), false, 64, (com.fossil.blesdk.obfuscated.fd4) null);
                com.fossil.blesdk.obfuscated.qa4 qa4 = com.fossil.blesdk.obfuscated.qa4.a;
            }
            device = this.this$0.i.getDeviceBySerial(this.$serial);
            switchActiveDeviceUseCase.a(device);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.n.a();
            local.d(a2, "doSwitchDevice device " + this.this$0.e());
            if (this.this$0.e() == null) {
                this.this$0.a(new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.c(116, com.fossil.blesdk.obfuscated.cb4.a((T[]) new java.lang.Integer[]{com.fossil.blesdk.obfuscated.dc4.a(-1)}), "No device data"));
                return new kotlin.Pair(com.fossil.blesdk.obfuscated.dc4.a(false), com.fossil.blesdk.obfuscated.dc4.a(-1));
            }
            com.portfolio.platform.data.source.DeviceRepository b2 = this.this$0.i;
            com.portfolio.platform.data.model.Device e = this.this$0.e();
            if (e != null) {
                this.L$0 = zg4;
                this.label = 1;
                obj3 = b2.forceLinkDevice(e, this);
                if (obj3 == a) {
                    return a;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.a();
                throw null;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
            obj3 = obj;
        } else if (i == 2) {
            com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) this.L$3;
            com.portfolio.platform.data.model.MFUser mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
            com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
            obj2 = obj;
            com.fossil.blesdk.obfuscated.qo2 qo22 = (com.fossil.blesdk.obfuscated.qo2) obj2;
            return new kotlin.Pair(com.fossil.blesdk.obfuscated.dc4.a(true), com.fossil.blesdk.obfuscated.dc4.a(-1));
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo23 = (com.fossil.blesdk.obfuscated.qo2) obj3;
        if (qo23 instanceof com.fossil.blesdk.obfuscated.ro2) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.n.a(), "doSwitchDevice success");
            com.portfolio.platform.data.model.MFUser currentUser = this.this$0.h.getCurrentUser();
            if (currentUser != null) {
                com.portfolio.platform.data.model.Device e2 = this.this$0.e();
                if (e2 != null) {
                    currentUser.setActiveDeviceId(e2.getDeviceId());
                    com.portfolio.platform.data.source.UserRepository d = this.this$0.h;
                    this.L$0 = zg4;
                    this.L$1 = qo23;
                    this.L$2 = currentUser;
                    this.L$3 = currentUser;
                    this.label = 2;
                    obj2 = d.updateUser(currentUser, false, this);
                    if (obj2 == a) {
                        return a;
                    }
                    com.fossil.blesdk.obfuscated.qo2 qo222 = (com.fossil.blesdk.obfuscated.qo2) obj2;
                } else {
                    com.fossil.blesdk.obfuscated.kd4.a();
                    throw null;
                }
            }
            return new kotlin.Pair(com.fossil.blesdk.obfuscated.dc4.a(true), com.fossil.blesdk.obfuscated.dc4.a(-1));
        } else if (!(qo23 instanceof com.fossil.blesdk.obfuscated.po2)) {
            return new kotlin.Pair(com.fossil.blesdk.obfuscated.dc4.a(false), com.fossil.blesdk.obfuscated.dc4.a(-1));
        } else {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a3 = com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.n.a();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("doSwitchDevice fail ");
            com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo23;
            sb.append(po2.a());
            local2.d(a3, sb.toString());
            com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase2 = this.this$0;
            java.util.ArrayList a4 = com.fossil.blesdk.obfuscated.cb4.a((T[]) new java.lang.Integer[]{com.fossil.blesdk.obfuscated.dc4.a(po2.a())});
            com.portfolio.platform.data.model.ServerError c = po2.c();
            if (c != null) {
                str = c.getMessage();
            }
            switchActiveDeviceUseCase2.a(new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.c(114, a4, str));
            com.portfolio.platform.PortfolioApp.W.c().a(this.$serial, false, po2.a());
            return new kotlin.Pair(com.fossil.blesdk.obfuscated.dc4.a(false), com.fossil.blesdk.obfuscated.dc4.a(po2.a()));
        }
    }
}
