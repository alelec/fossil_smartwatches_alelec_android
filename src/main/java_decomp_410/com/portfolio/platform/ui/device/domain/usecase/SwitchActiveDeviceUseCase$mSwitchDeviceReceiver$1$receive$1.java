package com.portfolio.platform.ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1", f = "SwitchActiveDeviceUseCase.kt", l = {74}, m = "invokeSuspend")
public final class SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile $currentDeviceProfile;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serial;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1(com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1 switchActiveDeviceUseCase$mSwitchDeviceReceiver$1, java.lang.String str, com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile misfitDeviceProfile, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = switchActiveDeviceUseCase$mSwitchDeviceReceiver$1;
        this.$serial = str;
        this.$currentDeviceProfile = misfitDeviceProfile;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1 switchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1 = new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1(this.this$0, this.$serial, this.$currentDeviceProfile, yb4);
        switchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return switchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.p$;
            com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.this$0.a;
            java.lang.String str = this.$serial;
            com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) str, "serial");
            com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile misfitDeviceProfile = this.$currentDeviceProfile;
            if (misfitDeviceProfile != null) {
                this.L$0 = zg4;
                this.label = 1;
                obj = switchActiveDeviceUseCase.a(str, misfitDeviceProfile, this);
                if (obj == a) {
                    return a;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.a();
                throw null;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        kotlin.Pair pair = (kotlin.Pair) obj;
        boolean booleanValue = ((java.lang.Boolean) pair.component1()).booleanValue();
        int intValue = ((java.lang.Number) pair.component2()).intValue();
        com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.W.c();
        java.lang.String str2 = this.$serial;
        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) str2, "serial");
        c.a(str2, booleanValue, intValue);
        return com.fossil.blesdk.obfuscated.qa4.a;
    }
}
