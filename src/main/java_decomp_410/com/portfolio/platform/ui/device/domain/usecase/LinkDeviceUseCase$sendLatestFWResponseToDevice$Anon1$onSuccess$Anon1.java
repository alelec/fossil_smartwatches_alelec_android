package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.EmptyFirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1", f = "LinkDeviceUseCase.kt", l = {}, m = "invokeSuspend")
public final class LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ DownloadFirmwareByDeviceModelUsecase.d $responseValue;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1(LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1 linkDeviceUseCase$sendLatestFWResponseToDevice$Anon1, DownloadFirmwareByDeviceModelUsecase.d dVar, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = linkDeviceUseCase$sendLatestFWResponseToDevice$Anon1;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1 linkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1 = new LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1(this.this$Anon0, this.$responseValue, yb4);
        linkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1.p$ = (zg4) obj;
        return linkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            String a = this.$responseValue.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LinkDeviceUseCase.q.a();
            local.d(a2, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + a);
            FirmwareData a3 = UpdateFirmwareUsecase.g.a(this.this$Anon0.a.n, this.this$Anon0.a.f());
            if (a3 == null) {
                a3 = new EmptyFirmwareData();
            }
            PortfolioApp c = PortfolioApp.W.c();
            String h = this.this$Anon0.a.h();
            if (h != null) {
                c.a(h, (PairingResponse) PairingResponse.CREATOR.buildPairingUpdateFWResponse(a3));
                return qa4.a;
            }
            kd4.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
