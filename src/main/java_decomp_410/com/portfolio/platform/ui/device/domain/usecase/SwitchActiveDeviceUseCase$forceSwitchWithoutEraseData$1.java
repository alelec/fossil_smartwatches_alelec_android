package com.portfolio.platform.ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1", f = "SwitchActiveDeviceUseCase.kt", l = {180}, m = "invokeSuspend")
public final class SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $newActiveDeviceSerial;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1(com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = switchActiveDeviceUseCase;
        this.$newActiveDeviceSerial = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1 switchActiveDeviceUseCase$forceSwitchWithoutEraseData$1 = new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1(this.this$0, this.$newActiveDeviceSerial, yb4);
        switchActiveDeviceUseCase$forceSwitchWithoutEraseData$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return switchActiveDeviceUseCase$forceSwitchWithoutEraseData$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.String str;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.p$;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.n.a();
            local.d(a2, "could not erase new data file " + this.$newActiveDeviceSerial + ", force switch to it");
            java.lang.String e = this.this$0.k.e();
            com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.this$0;
            switchActiveDeviceUseCase.a(switchActiveDeviceUseCase.i.getDeviceBySerial(this.$newActiveDeviceSerial));
            boolean e2 = com.portfolio.platform.PortfolioApp.W.c().e(this.$newActiveDeviceSerial);
            if (e2) {
                com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase2 = this.this$0;
                java.lang.String str2 = this.$newActiveDeviceSerial;
                this.L$0 = zg4;
                this.L$1 = e;
                this.Z$0 = e2;
                this.label = 1;
                obj = switchActiveDeviceUseCase2.a(str2, (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) null, this);
                if (obj == a) {
                    return a;
                }
                str = e;
            } else {
                this.this$0.a(new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.c(116, (java.util.ArrayList<java.lang.Integer>) null, ""));
                return com.fossil.blesdk.obfuscated.qa4.a;
            }
        } else if (i == 1) {
            str = (java.lang.String) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (((java.lang.Boolean) ((kotlin.Pair) obj).component1()).booleanValue()) {
            this.this$0.b(this.$newActiveDeviceSerial);
            com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase3 = this.this$0;
            switchActiveDeviceUseCase3.a(new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.d(switchActiveDeviceUseCase3.e()));
        } else {
            com.portfolio.platform.PortfolioApp.W.c().e(str);
            com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.c g = this.this$0.g();
            if (g == null || this.this$0.a(g) == null) {
                this.this$0.a(new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.c(116, (java.util.ArrayList<java.lang.Integer>) null, ""));
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.a;
    }
}
