package com.portfolio.platform.ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$1", f = "SetVibrationStrengthUseCase.kt", l = {49}, m = "invokeSuspend")
public final class SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase.SetVibrationStrengthBroadcastReceiver this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$1(com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase.SetVibrationStrengthBroadcastReceiver setVibrationStrengthBroadcastReceiver, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = setVibrationStrengthBroadcastReceiver;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$1 setVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$1 = new com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$1(this.this$0, yb4);
        setVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return setVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.p$;
            com.portfolio.platform.data.model.Device deviceBySerial = com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase.this.h.getDeviceBySerial(com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase.this.e());
            if (deviceBySerial != null) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a2 = com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase.j.a();
                local.d(a2, "Update vibration stregnth " + com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase.this.d() + " to db");
                deviceBySerial.setVibrationStrength(com.fossil.blesdk.obfuscated.dc4.a(com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase.this.d()));
                com.portfolio.platform.data.source.DeviceRepository a3 = com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase.this.h;
                this.L$0 = zg4;
                this.L$1 = deviceBySerial;
                this.L$2 = deviceBySerial;
                this.label = 1;
                obj = a3.updateDevice(deviceBySerial, false, this);
                if (obj == a) {
                    return a;
                }
            }
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase.j.a(), "onReceive #getDeviceBySerial success");
            com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase.this.a(new com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase.d());
            return com.fossil.blesdk.obfuscated.qa4.a;
        } else if (i == 1) {
            com.portfolio.platform.data.model.Device device = (com.portfolio.platform.data.model.Device) this.L$2;
            com.portfolio.platform.data.model.Device device2 = (com.portfolio.platform.data.model.Device) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase.j.a(), "onReceive #getDeviceBySerial success");
        com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase.this.a(new com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase.d());
        return com.fossil.blesdk.obfuscated.qa4.a;
    }
}
