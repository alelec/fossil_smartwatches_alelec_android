package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;
import java.util.ArrayList;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$Anon1", f = "SwitchActiveDeviceUseCase.kt", l = {}, m = "invokeSuspend")
public final class SwitchActiveDeviceUseCase$doSwitchDevice$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SwitchActiveDeviceUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$doSwitchDevice$Anon1(SwitchActiveDeviceUseCase switchActiveDeviceUseCase, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = switchActiveDeviceUseCase;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SwitchActiveDeviceUseCase$doSwitchDevice$Anon1 switchActiveDeviceUseCase$doSwitchDevice$Anon1 = new SwitchActiveDeviceUseCase$doSwitchDevice$Anon1(this.this$Anon0, yb4);
        switchActiveDeviceUseCase$doSwitchDevice$Anon1.p$ = (zg4) obj;
        return switchActiveDeviceUseCase$doSwitchDevice$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SwitchActiveDeviceUseCase$doSwitchDevice$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = SwitchActiveDeviceUseCase.n.a();
            StringBuilder sb = new StringBuilder();
            sb.append("doSwitchDevice serial ");
            SwitchActiveDeviceUseCase.b f = this.this$Anon0.f();
            if (f != null) {
                sb.append(f.b());
                local.d(a, sb.toString());
                PortfolioApp c = PortfolioApp.W.c();
                SwitchActiveDeviceUseCase.b f2 = this.this$Anon0.f();
                if (f2 != null) {
                    if (!c.q(f2.b())) {
                        this.this$Anon0.i();
                        this.this$Anon0.a(new SwitchActiveDeviceUseCase.c(116, (ArrayList<Integer>) null, ""));
                    }
                    return qa4.a;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
