package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase", f = "DownloadFirmwareByDeviceModelUsecase.kt", l = {37, 52}, m = "run")
public final class DownloadFirmwareByDeviceModelUsecase$run$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ DownloadFirmwareByDeviceModelUsecase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DownloadFirmwareByDeviceModelUsecase$run$Anon1(DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = downloadFirmwareByDeviceModelUsecase;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((DownloadFirmwareByDeviceModelUsecase.b) null, (yb4<Object>) this);
    }
}
