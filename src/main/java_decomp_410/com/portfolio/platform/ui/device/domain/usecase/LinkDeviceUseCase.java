package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import android.os.Bundle;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nn3;
import com.fossil.blesdk.obfuscated.on3;
import com.fossil.blesdk.obfuscated.pn3;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.ra4;
import com.fossil.blesdk.obfuscated.sq2;
import com.fossil.blesdk.obfuscated.vj2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.misfit.frameworks.buttonservice.model.SkipFirmwareData;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LinkDeviceUseCase extends CoroutineUseCase<g, i, h> {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ b q; // = new b((fd4) null);
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public Device f;
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public j j;
    @DexIgnore
    public /* final */ f k; // = new f();
    @DexIgnore
    public /* final */ DeviceRepository l;
    @DexIgnore
    public /* final */ DownloadFirmwareByDeviceModelUsecase m;
    @DexIgnore
    public /* final */ en2 n;
    @DexIgnore
    public /* final */ vj2 o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends i {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public a(String str) {
            kd4.b(str, "serial");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final String a() {
            return LinkDeviceUseCase.p;
        }

        @DexIgnore
        public /* synthetic */ b(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends h {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(int i, String str, String str2) {
            super(i, str, str2);
            kd4.b(str, "deviceId");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends h {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(String str, String str2) {
            super(-1, str, str2);
            kd4.b(str, "deviceId");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends i {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public e(String str) {
            kd4.b(str, "serial");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f implements BleCommandResultManager.b {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            kd4.b(communicateMode, "communicateMode");
            kd4.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), FailureCode.UNKNOWN_ERROR);
            ServiceActionResult serviceActionResult = ServiceActionResult.values()[intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), ServiceActionResult.UNALLOWED_ACTION.ordinal())];
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LinkDeviceUseCase.q.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + LinkDeviceUseCase.this.j() + ", isSuccess=" + intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1));
            boolean z = true;
            if (qf4.b(stringExtra, LinkDeviceUseCase.this.h(), true) && communicateMode == CommunicateMode.LINK && LinkDeviceUseCase.this.j()) {
                boolean z2 = false;
                switch (sq2.a[serviceActionResult.ordinal()]) {
                    case 1:
                        if (intent.getExtras() == null) {
                            z = false;
                        }
                        if (!ra4.a || z) {
                            LinkDeviceUseCase linkDeviceUseCase = LinkDeviceUseCase.this;
                            Bundle extras = intent.getExtras();
                            if (extras != null) {
                                String string = extras.getString("device_model", "");
                                kd4.a((Object) string, "intent.extras!!.getStrin\u2026nstants.DEVICE_MODEL, \"\")");
                                linkDeviceUseCase.a(string);
                                LinkDeviceUseCase linkDeviceUseCase2 = LinkDeviceUseCase.this;
                                kd4.a((Object) stringExtra, "serial");
                                linkDeviceUseCase2.a(new a(stringExtra));
                                LinkDeviceUseCase.this.m();
                                return;
                            }
                            kd4.a();
                            throw null;
                        }
                        throw new AssertionError("Assertion failed");
                    case 2:
                        LinkDeviceUseCase linkDeviceUseCase3 = LinkDeviceUseCase.this;
                        kd4.a((Object) stringExtra, "serial");
                        linkDeviceUseCase3.a(new e(stringExtra));
                        return;
                    case 3:
                        LinkDeviceUseCase linkDeviceUseCase4 = LinkDeviceUseCase.this;
                        kd4.a((Object) stringExtra, "serial");
                        linkDeviceUseCase4.a(new l(stringExtra, true));
                        return;
                    case 4:
                        LinkDeviceUseCase linkDeviceUseCase5 = LinkDeviceUseCase.this;
                        kd4.a((Object) stringExtra, "serial");
                        linkDeviceUseCase5.a(new l(stringExtra, false));
                        return;
                    case 5:
                        boolean z3 = intent.getExtras() != null;
                        if (!ra4.a || z3) {
                            Bundle extras2 = intent.getExtras();
                            if (extras2 != null) {
                                z2 = extras2.getBoolean(com.misfit.frameworks.buttonservice.utils.Constants.IS_JUST_OTA, false);
                            }
                            Bundle extras3 = intent.getExtras();
                            if (extras3 != null) {
                                MisfitDeviceProfile misfitDeviceProfile = (MisfitDeviceProfile) extras3.getParcelable("device");
                                if (z2) {
                                    LinkDeviceUseCase linkDeviceUseCase6 = LinkDeviceUseCase.this;
                                    kd4.a((Object) stringExtra, "serial");
                                    linkDeviceUseCase6.a(new l(stringExtra, true));
                                } else {
                                    LinkDeviceUseCase linkDeviceUseCase7 = LinkDeviceUseCase.this;
                                    kd4.a((Object) stringExtra, "serial");
                                    linkDeviceUseCase7.a(new e(stringExtra));
                                }
                                if (misfitDeviceProfile != null) {
                                    LinkDeviceUseCase.this.c(misfitDeviceProfile.getDeviceSerial());
                                    LinkDeviceUseCase.this.b(misfitDeviceProfile.getAddress());
                                    LinkDeviceUseCase.this.a(new Device(misfitDeviceProfile.getDeviceSerial(), misfitDeviceProfile.getAddress(), misfitDeviceProfile.getDeviceModel(), misfitDeviceProfile.getFirmwareVersion(), misfitDeviceProfile.getBatteryLevel(), 50, false, 64, (fd4) null));
                                    Device e = LinkDeviceUseCase.this.e();
                                    if (e != null) {
                                        e.appendAdditionalInfo(misfitDeviceProfile);
                                    }
                                    LinkDeviceUseCase.this.d();
                                    return;
                                }
                                LinkDeviceUseCase linkDeviceUseCase8 = LinkDeviceUseCase.this;
                                String h = linkDeviceUseCase8.h();
                                if (h != null) {
                                    linkDeviceUseCase8.a(new c(FailureCode.UNKNOWN_ERROR, h, "No device profile"));
                                    PortfolioApp c = PortfolioApp.W.c();
                                    String h2 = LinkDeviceUseCase.this.h();
                                    if (h2 != null) {
                                        c.a(h2);
                                        return;
                                    } else {
                                        kd4.a();
                                        throw null;
                                    }
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            throw new AssertionError("Assertion failed");
                        }
                    case 6:
                        LinkDeviceUseCase.this.a(false);
                        PortfolioApp c2 = PortfolioApp.W.c();
                        kd4.a((Object) stringExtra, "serial");
                        c2.c(stringExtra, LinkDeviceUseCase.this.g());
                        LinkDeviceUseCase linkDeviceUseCase9 = LinkDeviceUseCase.this;
                        Device e2 = linkDeviceUseCase9.e();
                        if (e2 != null) {
                            linkDeviceUseCase9.a(new k(e2));
                            return;
                        } else {
                            kd4.a();
                            throw null;
                        }
                    case 7:
                        LinkDeviceUseCase.this.a(false);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a3 = LinkDeviceUseCase.q.a();
                        local2.d(a3, "Pair device failed due to " + intExtra + ", remove this device on button service");
                        try {
                            PortfolioApp c3 = PortfolioApp.W.c();
                            String h3 = LinkDeviceUseCase.this.h();
                            if (h3 != null) {
                                c3.m(h3);
                                PortfolioApp c4 = PortfolioApp.W.c();
                                String h4 = LinkDeviceUseCase.this.h();
                                if (h4 != null) {
                                    c4.l(h4);
                                    if (intExtra > 1000) {
                                        LinkDeviceUseCase linkDeviceUseCase10 = LinkDeviceUseCase.this;
                                        String h5 = linkDeviceUseCase10.h();
                                        if (h5 != null) {
                                            linkDeviceUseCase10.a(new c(intExtra, h5, ""));
                                            return;
                                        } else {
                                            kd4.a();
                                            throw null;
                                        }
                                    } else {
                                        LinkDeviceUseCase linkDeviceUseCase11 = LinkDeviceUseCase.this;
                                        String h6 = linkDeviceUseCase11.h();
                                        if (h6 != null) {
                                            linkDeviceUseCase11.a(new j(intExtra, h6, ""));
                                            LinkDeviceUseCase linkDeviceUseCase12 = LinkDeviceUseCase.this;
                                            j i = linkDeviceUseCase12.i();
                                            if (i != null) {
                                                linkDeviceUseCase12.a(i);
                                                LinkDeviceUseCase.this.a((j) null);
                                                return;
                                            }
                                            kd4.a();
                                            throw null;
                                        }
                                        kd4.a();
                                        throw null;
                                    }
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } catch (Exception e3) {
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String a4 = LinkDeviceUseCase.q.a();
                            local3.d(a4, "Pair device failed, remove this device on button service exception=" + e3.getMessage());
                        }
                    default:
                        return;
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public g(String str, String str2) {
            kd4.b(str, "device");
            kd4.b(str2, "macAddress");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public h(int i, String str, String str2) {
            kd4.b(str, "deviceId");
            this.a = i;
            this.b = str;
            this.c = str2;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class i implements CoroutineUseCase.d {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends h {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(int i, String str, String str2) {
            super(i, str, str2);
            kd4.b(str, "deviceId");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends i {
        @DexIgnore
        public /* final */ Device a;

        @DexIgnore
        public k(Device device) {
            kd4.b(device, "device");
            this.a = device;
        }

        @DexIgnore
        public final Device a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l extends i {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public l(String str, boolean z) {
            kd4.b(str, "serial");
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements CoroutineUseCase.e<pn3, nn3> {
        @DexIgnore
        public /* final */ /* synthetic */ LinkDeviceUseCase a;

        @DexIgnore
        public m(LinkDeviceUseCase linkDeviceUseCase) {
            this.a = linkDeviceUseCase;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(pn3 pn3) {
            kd4.b(pn3, "responseValue");
            FLogger.INSTANCE.getLocal().d(LinkDeviceUseCase.q.a(), " get device setting success");
            this.a.l();
        }

        @DexIgnore
        public void a(nn3 nn3) {
            kd4.b(nn3, "errorValue");
            FLogger.INSTANCE.getLocal().d(LinkDeviceUseCase.q.a(), " get device setting fail!!");
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.API;
            FLogger.Session session = FLogger.Session.PAIR;
            String h = this.a.h();
            if (h != null) {
                String a2 = LinkDeviceUseCase.q.a();
                StringBuilder sb = new StringBuilder();
                sb.append("Get device setting of ");
                String h2 = this.a.h();
                if (h2 != null) {
                    sb.append(h2);
                    sb.append(", server error=");
                    sb.append(nn3.a());
                    sb.append(", error = ");
                    sb.append(ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.LINK_DEVICE, ErrorCodeBuilder.Component.APP, ErrorCodeBuilder.AppError.NETWORK_ERROR));
                    remote.e(component, session, h, a2, sb.toString());
                    LinkDeviceUseCase linkDeviceUseCase = this.a;
                    int a3 = nn3.a();
                    String h3 = this.a.h();
                    if (h3 != null) {
                        linkDeviceUseCase.a(new j(a3, h3, ""));
                        PortfolioApp c = PortfolioApp.W.c();
                        String h4 = this.a.h();
                        if (h4 != null) {
                            c.a(h4, (PairingResponse) PairingResponse.CREATOR.buildPairingLinkServerResponse(false, nn3.a()));
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    /*
    static {
        String simpleName = LinkDeviceUseCase.class.getSimpleName();
        kd4.a((Object) simpleName, "LinkDeviceUseCase::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public LinkDeviceUseCase(DeviceRepository deviceRepository, DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase, en2 en2, vj2 vj2) {
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(downloadFirmwareByDeviceModelUsecase, "mDownloadFwByDeviceModel");
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(vj2, "mDeviceSettingFactory");
        this.l = deviceRepository;
        this.m = downloadFirmwareByDeviceModelUsecase;
        this.n = en2;
        this.o = vj2;
    }

    @DexIgnore
    public final synchronized void d() {
        vj2 vj2 = this.o;
        String str = this.d;
        if (str != null) {
            CoroutineUseCase<on3, pn3, nn3> a2 = vj2.a(str);
            String str2 = this.d;
            if (str2 != null) {
                a2.a(new on3(str2), (CoroutineUseCase.e<? super pn3, ? super nn3>) new m(this));
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final Device e() {
        return this.f;
    }

    @DexIgnore
    public final String f() {
        return this.g;
    }

    @DexIgnore
    public final String g() {
        return this.e;
    }

    @DexIgnore
    public final String h() {
        return this.d;
    }

    @DexIgnore
    public final j i() {
        return this.j;
    }

    @DexIgnore
    public final boolean j() {
        return this.h;
    }

    @DexIgnore
    public final void k() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.k, CommunicateMode.LINK);
    }

    @DexIgnore
    public final synchronized void l() {
        if (!this.i) {
            this.i = true;
        } else {
            Device device = this.f;
            if (device != null) {
                fi4 unused = ag4.b(b(), (CoroutineContext) null, (CoroutineStart) null, new LinkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$Anon1(device, (yb4) null, this), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    public final void m() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "sendLatestFWResponseToDevice(), deviceModel=" + this.g + ", isSkipOTA=" + this.n.T());
        if (PortfolioApp.W.c().D() || !this.n.T()) {
            this.m.a(new DownloadFirmwareByDeviceModelUsecase.b(this.g), new LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1(this));
            return;
        }
        PortfolioApp c2 = PortfolioApp.W.c();
        String str2 = this.d;
        if (str2 != null) {
            c2.a(str2, (PairingResponse) PairingResponse.CREATOR.buildPairingUpdateFWResponse(new SkipFirmwareData()));
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void n() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.k, CommunicateMode.LINK);
    }

    @DexIgnore
    public final void b(String str) {
        this.e = str;
    }

    @DexIgnore
    public final void c(String str) {
        this.d = str;
    }

    @DexIgnore
    public final void a(Device device) {
        this.f = device;
    }

    @DexIgnore
    public String c() {
        return p;
    }

    @DexIgnore
    public final void a(String str) {
        kd4.b(str, "<set-?>");
        this.g = str;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.h = z;
    }

    @DexIgnore
    public final void a(j jVar) {
        this.j = jVar;
    }

    @DexIgnore
    public final void a(ShineDevice shineDevice, CoroutineUseCase.e<? super i, ? super h> eVar) {
        kd4.b(shineDevice, "closestDevice");
        kd4.b(eVar, "caseCallback");
        FLogger.INSTANCE.getLocal().d(p, "onRetrieveLinkAction");
        this.h = true;
        this.d = shineDevice.getSerial();
        this.e = shineDevice.getMacAddress();
        a(eVar);
    }

    @DexIgnore
    public Object a(g gVar, yb4<Object> yb4) {
        FLogger.INSTANCE.getLocal().d(p, "running UseCase");
        if (gVar == null) {
            return new h(600, "", "");
        }
        this.h = true;
        this.d = gVar.a();
        this.e = "";
        gVar.b();
        this.e = gVar.b();
        try {
            PortfolioApp c2 = PortfolioApp.W.c();
            String str = this.d;
            if (str != null) {
                String str2 = this.e;
                if (str2 != null) {
                    c2.a(str, str2);
                    return new Object();
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = p;
            local.e(str3, "Error inside " + p + ".connectDevice - e=" + e2);
        }
    }
}
