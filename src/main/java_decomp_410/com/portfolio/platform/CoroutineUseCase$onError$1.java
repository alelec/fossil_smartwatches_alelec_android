package com.portfolio.platform;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.CoroutineUseCase$onError$1", mo27670f = "CoroutineUseCase.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class CoroutineUseCase$onError$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.CoroutineUseCase.C5602a $errorValue;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20911p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.CoroutineUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CoroutineUseCase$onError$1(com.portfolio.platform.CoroutineUseCase coroutineUseCase, com.portfolio.platform.CoroutineUseCase.C5602a aVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = coroutineUseCase;
        this.$errorValue = aVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.CoroutineUseCase$onError$1 coroutineUseCase$onError$1 = new com.portfolio.platform.CoroutineUseCase$onError$1(this.this$0, this.$errorValue, yb4);
        coroutineUseCase$onError$1.f20911p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return coroutineUseCase$onError$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.CoroutineUseCase$onError$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.this$0.f20908b, "Trigger UseCase callback failed");
            com.portfolio.platform.CoroutineUseCase.C5606e a = this.this$0.f20907a;
            if (a != null) {
                a.mo29641a(this.$errorValue);
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
