package com.portfolio.platform;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MigrationManager$migrateFor4Dot0$$inlined$let$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.LegacyGoalTrackingSettings $it;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20930p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.MigrationManager this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MigrationManager$migrateFor4Dot0$$inlined$let$lambda$1(com.portfolio.platform.data.legacy.threedotzero.LegacyGoalTrackingSettings legacyGoalTrackingSettings, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.MigrationManager migrationManager) {
        super(2, yb4);
        this.$it = legacyGoalTrackingSettings;
        this.this$0 = migrationManager;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.MigrationManager$migrateFor4Dot0$$inlined$let$lambda$1 migrationManager$migrateFor4Dot0$$inlined$let$lambda$1 = new com.portfolio.platform.MigrationManager$migrateFor4Dot0$$inlined$let$lambda$1(this.$it, yb4, this.this$0);
        migrationManager$migrateFor4Dot0$$inlined$let$lambda$1.f20930p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return migrationManager$migrateFor4Dot0$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.MigrationManager$migrateFor4Dot0$$inlined$let$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f20930p$;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.MigrationManager.f20914n.mo34452a();
            local.mo33255d(a2, "Migrate goal tracking with target " + this.$it.getTarget() + " value " + this.$it.getValue());
            this.this$0.f20921g.getGoalTrackingDao().upsertGoalSettings(new com.portfolio.platform.data.model.GoalSetting(this.$it.getTarget()));
            java.util.ArrayList arrayList = new java.util.ArrayList();
            int value = this.$it.getValue();
            for (int i2 = 0; i2 < value; i2++) {
                java.lang.String uuid = java.util.UUID.randomUUID().toString();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) uuid, "UUID.randomUUID().toString()");
                java.util.Date date = new java.util.Date();
                java.util.TimeZone timeZone = java.util.TimeZone.getDefault();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) timeZone, "TimeZone.getDefault()");
                org.joda.time.DateTime a3 = com.fossil.blesdk.obfuscated.rk2.m27368a(date, timeZone.getRawOffset() / 1000);
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a3, "DateHelper.createDateTim\u2026fault().rawOffset / 1000)");
                java.util.TimeZone timeZone2 = java.util.TimeZone.getDefault();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) timeZone2, "TimeZone.getDefault()");
                com.portfolio.platform.data.model.goaltracking.GoalTrackingData goalTrackingData = r7;
                com.portfolio.platform.data.model.goaltracking.GoalTrackingData goalTrackingData2 = new com.portfolio.platform.data.model.goaltracking.GoalTrackingData(uuid, a3, timeZone2.getRawOffset() / 1000, new java.util.Date(), new java.util.Date().getTime(), new java.util.Date().getTime());
                arrayList.add(goalTrackingData);
            }
            com.portfolio.platform.data.source.GoalTrackingRepository d = this.this$0.f20920f;
            java.util.List d2 = com.fossil.blesdk.obfuscated.kb4.m24381d(arrayList);
            this.L$0 = zg4;
            this.L$1 = arrayList;
            this.label = 1;
            if (d.insertFromDevice(d2, this) == a) {
                return a;
            }
        } else if (i == 1) {
            java.util.ArrayList arrayList2 = (java.util.ArrayList) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
