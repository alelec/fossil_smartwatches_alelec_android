package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase.a;
import com.portfolio.platform.CoroutineUseCase.b;
import com.portfolio.platform.CoroutineUseCase.d;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class CoroutineUseCase<Q extends b, R extends d, E extends a> {
    @DexIgnore
    public e<? super R, ? super E> a;
    @DexIgnore
    public String b; // = "CoroutineUseCase";
    @DexIgnore
    public /* final */ zg4 c; // = ah4.a(nh4.a());

    @DexIgnore
    public interface a extends c {
    }

    @DexIgnore
    public interface b {
    }

    @DexIgnore
    public interface c {
    }

    @DexIgnore
    public interface d extends c {
    }

    @DexIgnore
    public interface e<R, E> {
        @DexIgnore
        void a(E e);

        @DexIgnore
        void onSuccess(R r);
    }

    @DexIgnore
    public abstract Object a(Q q, yb4<Object> yb4);

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public final zg4 b() {
        return this.c;
    }

    @DexIgnore
    public final fi4 a(Q q, e<? super R, ? super E> eVar) {
        throw null;
        // return ag4.b(this.c, (CoroutineContext) null, (CoroutineStart) null, new CoroutineUseCase$executeUseCase$Anon1(this, eVar, q, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final e<R, E> a() {
        throw null;
        // return this.a;
    }

    @DexIgnore
    public final void a(e<? super R, ? super E> eVar) {
        kd4.b(eVar, Constants.CALLBACK);
        this.a = eVar;
    }

    @DexIgnore
    public final fi4 a(R r) {
        throw null;
        // kd4.b(r, "response");
        // return ag4.b(this.c, nh4.c(), (CoroutineStart) null, new CoroutineUseCase$onSuccess$Anon1(this, r, (yb4) null), 2, (Object) null);
    }

    @DexIgnore
    public final fi4 a(E e2) {
        throw null;
        // kd4.b(e2, "errorValue");
        // return ag4.b(this.c, nh4.c(), (CoroutineStart) null, new CoroutineUseCase$onError$Anon1(this, e2, (yb4) null), 2, (Object) null);
    }
}
