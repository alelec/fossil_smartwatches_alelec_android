package com.portfolio.platform.data;

import android.content.Context;
import com.fossil.wearables.fossil.R;
import com.sina.weibo.sdk.WeiboAppManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum AppType {
    BADOO(-1, "com.badoo.mobile"),
    CLOCK(R.string.app_clock, ""),
    ESPN(-1, "com.espn.score_center"),
    FACEBOOK(-1, "com.facebook.katana"),
    FACEBOOK_MESSENGER(-1, "com.facebook.orca"),
    FLICKR(-1, "com.yahoo.mobile.client.android.flickr"),
    FOURSQUARE(-1, "com.joelapenna.foursquared"),
    GMAIL(-1, "com.google.android.gm"),
    GOOGLE_PLUS(-1, "com.google.android.apps.plus"),
    GOOGLE_INBOX(-1, "com.google.android.apps.inbox"),
    HANGOUTS(-1, "com.google.android.talk"),
    HIPCHAT(-1, "com.hipchat"),
    INSTAGRAM(-1, "com.instagram.android"),
    KAKAO(-1, "com.kakao.talk"),
    LINE(-1, "jp.naver.line.android"),
    LYNC(-1, "com.microsoft.office.lync15"),
    MESSAGING(-1, "com.android.mms"),
    PANDORA(-1, "com.pandora.android"),
    PINTEREST(-1, "com.pinterest"),
    SHAZAM(-1, "com.shazam.android"),
    SINA_WEIBO(-1, WeiboAppManager.WEIBO_PACKAGENAME),
    SKYPE(-1, "com.skype.raider"),
    SLACK(-1, "com.Slack"),
    SNAPCHAT(-1, "com.snapchat.android"),
    SPOTIFY(-1, "com.spotify.music"),
    SWARM(-1, "com.foursquare.robin"),
    TUMBLR(-1, "com.tumblr"),
    TWITTER(-1, "com.twitter.android"),
    UBER(-1, "com.ubercab"),
    VIADEO(-1, "com.viadeo.android"),
    WECHAT(-1, "com.tencent.mm"),
    WHATSAPP(-1, "com.whatsapp"),
    ALL_SMS(-1, "All Texts"),
    ALL_EMAIL(R.string.email_from_everyone, ""),
    ALL_CALLS(-1, "All Calls");
    
    @DexIgnore
    public /* final */ String packageName;
    @DexIgnore
    public /* final */ int titleResId;

    @DexIgnore
    AppType(int i, String str) {
        this.titleResId = i;
        this.packageName = str;
    }

    @DexIgnore
    public static boolean isInstalled(Context context, String str) {
        return context.getPackageManager().getLaunchIntentForPackage(str) != null;
    }

    @DexIgnore
    public int getAppResId() {
        return this.titleResId;
    }

    @DexIgnore
    public String getPackageName() {
        return this.packageName;
    }
}
