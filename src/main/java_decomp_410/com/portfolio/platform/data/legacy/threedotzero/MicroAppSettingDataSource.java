package com.portfolio.platform.data.legacy.threedotzero;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface MicroAppSettingDataSource {

    @DexIgnore
    public interface MicroAppSettingCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(MicroAppSetting microAppSetting);
    }

    @DexIgnore
    public interface MicroAppSettingListCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(List<MicroAppSetting> list);
    }

    @DexIgnore
    public interface PushMicroAppSettingToServerCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess();
    }

    @DexIgnore
    public interface PushPendingMicroAppSettingsCallback {
        @DexIgnore
        void onDone();
    }

    @DexIgnore
    void addOrUpdateMicroAppSetting(MicroAppSetting microAppSetting, MicroAppSettingCallback microAppSettingCallback);

    @DexIgnore
    void clearData();

    @DexIgnore
    void getMicroAppSetting(String str, MicroAppSettingCallback microAppSettingCallback);

    @DexIgnore
    void getMicroAppSettingList(MicroAppSettingListCallback microAppSettingListCallback);

    @DexIgnore
    void mergeMicroAppSetting(MicroAppSetting microAppSetting, MicroAppSettingCallback microAppSettingCallback);
}
