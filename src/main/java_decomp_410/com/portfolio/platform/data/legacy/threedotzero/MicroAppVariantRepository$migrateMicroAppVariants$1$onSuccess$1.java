package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRepository$migrateMicroAppVariants$1$onSuccess$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.ArrayList $variantParserList;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository$migrateMicroAppVariants$1 this$0;

    @DexIgnore
    public MicroAppVariantRepository$migrateMicroAppVariants$1$onSuccess$1(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository$migrateMicroAppVariants$1 microAppVariantRepository$migrateMicroAppVariants$1, java.util.ArrayList arrayList) {
        this.this$0 = microAppVariantRepository$migrateMicroAppVariants$1;
        this.$variantParserList = arrayList;
    }

    @DexIgnore
    public final void run() {
        com.misfit.frameworks.common.log.MFLogger.m31689d(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG(), "diskIO enter onSuccess downloadAllVariants");
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource access$getMMicroAppVariantLocalDataSource$p = this.this$0.this$0.mMicroAppVariantLocalDataSource;
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository$migrateMicroAppVariants$1 microAppVariantRepository$migrateMicroAppVariants$1 = this.this$0;
        access$getMMicroAppVariantLocalDataSource$p.removeMicroAppVariants(microAppVariantRepository$migrateMicroAppVariants$1.$serialNumber, microAppVariantRepository$migrateMicroAppVariants$1.$major, microAppVariantRepository$migrateMicroAppVariants$1.$minor);
        java.util.ArrayList<com.fossil.blesdk.obfuscated.wo2> filterVariantList$app_fossilRelease = this.this$0.this$0.filterVariantList$app_fossilRelease(this.$variantParserList);
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository$migrateMicroAppVariants$1 microAppVariantRepository$migrateMicroAppVariants$12 = this.this$0;
        microAppVariantRepository$migrateMicroAppVariants$12.this$0.saveMicroAppVariant$app_fossilRelease(microAppVariantRepository$migrateMicroAppVariants$12.$serialNumber, filterVariantList$app_fossilRelease);
        com.portfolio.platform.data.legacy.threedotzero.UAppSystemVersionModel uAppSystemVersionModel = this.this$0.this$0.mUAppSystemVersionRepository.getUAppSystemVersionModel(this.this$0.$serialNumber);
        if (uAppSystemVersionModel != null && uAppSystemVersionModel.getMajorVersion() == this.this$0.$major && uAppSystemVersionModel.getMinorVersion() == this.this$0.$minor) {
            uAppSystemVersionModel.setPinType(0);
            this.this$0.this$0.mUAppSystemVersionRepository.addOrUpdateUAppSystemVersionModel(uAppSystemVersionModel);
        }
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository$migrateMicroAppVariants$1 microAppVariantRepository$migrateMicroAppVariants$13 = this.this$0;
        microAppVariantRepository$migrateMicroAppVariants$13.this$0.notifyStatusChanged("DECLARATION_FILES_DOWNLOADED", microAppVariantRepository$migrateMicroAppVariants$13.$serialNumber);
        com.misfit.frameworks.common.log.MFLogger.m31689d(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG(), "diskIO exit onSuccess downloadAllVariants");
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.MigrateVariantCallback migrateVariantCallback = this.this$0.$callback;
        if (migrateVariantCallback != null) {
            migrateVariantCallback.onDone();
        }
    }
}
