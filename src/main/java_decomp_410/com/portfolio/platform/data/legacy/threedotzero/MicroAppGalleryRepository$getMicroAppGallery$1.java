package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepository$getMicroAppGallery$1 implements com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $deviceSerial;

    @DexIgnore
    public MicroAppGalleryRepository$getMicroAppGallery$1(java.lang.String str, com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        this.$deviceSerial = str;
        this.$callback = getMicroAppGalleryCallback;
    }

    @DexIgnore
    public void onFail() {
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "getMicroAppGallery deviceSerial=" + this.$deviceSerial + " local onFail");
        com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onFail();
        }
    }

    @DexIgnore
    public void onSuccess(java.util.List<? extends com.portfolio.platform.data.legacy.threedotzero.MicroApp> list) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "microAppList");
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "getMicroAppGallery deviceSerial=" + this.$deviceSerial + " local onSuccess");
        com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onSuccess(list);
        }
    }
}
