package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class PresetDataSource {

    @DexIgnore
    public interface AddOrUpdateActivePresetCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(ActivePreset activePreset);
    }

    @DexIgnore
    public interface AddOrUpdateSavedPresetCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(SavedPreset savedPreset);
    }

    @DexIgnore
    public interface AddOrUpdateSavedPresetListCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(List<SavedPreset> list);
    }

    @DexIgnore
    public interface DeleteMappingSetCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess();
    }

    @DexIgnore
    public interface DeleteSavedPresetInServerCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess();
    }

    @DexIgnore
    public interface GetActivePresetCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(ActivePreset activePreset);
    }

    @DexIgnore
    public interface GetActivePresetListCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(List<ActivePreset> list);
    }

    @DexIgnore
    public interface GetRecommendedPresetCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(RecommendedPreset recommendedPreset);
    }

    @DexIgnore
    public interface GetRecommendedPresetListCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(List<RecommendedPreset> list);
    }

    @DexIgnore
    public interface GetSavedPresetCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(SavedPreset savedPreset);
    }

    @DexIgnore
    public interface GetSavedPresetListCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(List<SavedPreset> list);
    }

    @DexIgnore
    public interface PushActivePresetToServerCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess();
    }

    @DexIgnore
    public interface PushPendingActivePresetsCallback {
        @DexIgnore
        void onDone();
    }

    @DexIgnore
    public interface PushPendingSavedPresetsCallback {
        @DexIgnore
        void onDone();
    }

    @DexIgnore
    public interface PushSavedPresetToServerCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess();
    }

    @DexIgnore
    public abstract void addOrUpdateActivePreset(ActivePreset activePreset, AddOrUpdateActivePresetCallback addOrUpdateActivePresetCallback);

    @DexIgnore
    public abstract void addOrUpdateSavedPreset(SavedPreset savedPreset, AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback);

    @DexIgnore
    public abstract void addOrUpdateSavedPresetList(List<SavedPreset> list, AddOrUpdateSavedPresetListCallback addOrUpdateSavedPresetListCallback);

    @DexIgnore
    public abstract void clearData();

    @DexIgnore
    public void deleteSavedPreset(SavedPreset savedPreset, DeleteMappingSetCallback deleteMappingSetCallback) {
        kd4.b(savedPreset, "savedPreset");
        kd4.b(deleteMappingSetCallback, Constants.CALLBACK);
    }

    @DexIgnore
    public void deleteSavedPresetList(List<SavedPreset> list, DeleteMappingSetCallback deleteMappingSetCallback) {
        kd4.b(list, "savedPresetList");
    }

    @DexIgnore
    public abstract void downloadActivePresetList(GetActivePresetListCallback getActivePresetListCallback);

    @DexIgnore
    public void getActivePreset(String str, GetActivePresetCallback getActivePresetCallback) {
        kd4.b(str, "serial");
    }

    @DexIgnore
    public abstract void getDefaultPreset(String str, GetRecommendedPresetCallback getRecommendedPresetCallback);

    @DexIgnore
    public abstract void getRecommendedPresets(String str, GetRecommendedPresetListCallback getRecommendedPresetListCallback);

    @DexIgnore
    public void getSavedPreset(String str, GetSavedPresetCallback getSavedPresetCallback) {
        kd4.b(str, "savedPresetId");
        kd4.b(getSavedPresetCallback, Constants.CALLBACK);
    }

    @DexIgnore
    public abstract void getSavedPresetList(GetSavedPresetListCallback getSavedPresetListCallback);
}
