package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.n44;
import com.fossil.blesdk.obfuscated.x72;
import com.fossil.blesdk.obfuscated.y72;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MicroAppSettingRepository implements MicroAppSettingDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "MicroAppSettingRepository";
    @DexIgnore
    public /* final */ h42 mAppExecutors;
    @DexIgnore
    public MicroAppSetting mCachedSetting;
    @DexIgnore
    public List<MicroAppSetting> mCachedSettingList;
    @DexIgnore
    public boolean mIsCachedDirty;
    @DexIgnore
    public boolean mIsCachedListDirty;
    @DexIgnore
    public /* final */ MicroAppSettingDataSource mMicroAppSettingLocalDataSource;
    @DexIgnore
    public /* final */ MicroAppSettingDataSource mMicroAppSettingRemoteDataSource;
    @DexIgnore
    public List<MicroAppSettingRepositoryObserver> microAppSettingRepositoryObservers; // = new CopyOnWriteArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 implements MicroAppSettingDataSource.MicroAppSettingCallback {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppSettingDataSource.MicroAppSettingCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppSetting val$microAppSetting;

        @DexIgnore
        public Anon1(MicroAppSetting microAppSetting, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
            this.val$microAppSetting = microAppSetting;
            this.val$callback = microAppSettingCallback;
        }

        @DexIgnore
        public void onFail() {
            String str = MicroAppSettingRepository.TAG;
            MFLogger.d(str, "addOrUpdateMicroAppSetting local onSuccess microAppSetting=" + this.val$microAppSetting.getSetting());
            this.val$callback.onFail();
        }

        @DexIgnore
        public void onSuccess(MicroAppSetting microAppSetting) {
            String str = MicroAppSettingRepository.TAG;
            MFLogger.d(str, "addOrUpdateMicroAppSetting local onSuccess microAppSetting=" + this.val$microAppSetting.getSetting());
            MicroAppSettingRepository microAppSettingRepository = MicroAppSettingRepository.this;
            microAppSettingRepository.mIsCachedDirty = true;
            microAppSettingRepository.mIsCachedListDirty = true;
            microAppSettingRepository.pushMicroAppSettingToServer(microAppSetting, (MicroAppSettingDataSource.PushMicroAppSettingToServerCallback) null);
            MicroAppSettingRepository.this.notifyMicroAppSettingObservers();
            this.val$callback.onSuccess(microAppSetting);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 implements MicroAppSettingDataSource.MicroAppSettingListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppSettingDataSource.MicroAppSettingListCallback val$callback;

        @DexIgnore
        public Anon2(MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback) {
            this.val$callback = microAppSettingListCallback;
        }

        @DexIgnore
        public void onFail() {
            MFLogger.d(MicroAppSettingRepository.TAG, "getMicroAppSettingList local onFail");
            this.val$callback.onFail();
        }

        @DexIgnore
        public void onSuccess(List<MicroAppSetting> list) {
            String str = MicroAppSettingRepository.TAG;
            MFLogger.d(str, "getMicroAppSettingList local onSuccess getMicroAppSettingListSize=" + list.size());
            this.val$callback.onSuccess(list);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements MicroAppSettingDataSource.MicroAppSettingCallback {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppSettingDataSource.MicroAppSettingCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ String val$microAppId;

        @DexIgnore
        public Anon3(String str, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
            this.val$microAppId = str;
            this.val$callback = microAppSettingCallback;
        }

        @DexIgnore
        public void onFail() {
            String str = MicroAppSettingRepository.TAG;
            MFLogger.d(str, "getMicroAppSetting onFail microAppId=" + this.val$microAppId);
            this.val$callback.onFail();
        }

        @DexIgnore
        public void onSuccess(MicroAppSetting microAppSetting) {
            String str = MicroAppSettingRepository.TAG;
            MFLogger.d(str, "getMicroAppSetting local onSuccess microAppId=" + this.val$microAppId);
            this.val$callback.onSuccess(microAppSetting);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements MicroAppSettingDataSource.MicroAppSettingListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppSettingDataSource.MicroAppSettingListCallback val$callback;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1 implements MicroAppSettingDataSource.MicroAppSettingCallback {
            @DexIgnore
            public /* final */ /* synthetic */ MicroAppSetting val$setting;

            @DexIgnore
            public Anon1(MicroAppSetting microAppSetting) {
                this.val$setting = microAppSetting;
            }

            @DexIgnore
            public void onFail() {
                String str = MicroAppSettingRepository.TAG;
                MFLogger.d(str, "getMicroAppSettingList migrateMicroAppSetting onFail setting=" + this.val$setting.getSetting());
            }

            @DexIgnore
            public void onSuccess(MicroAppSetting microAppSetting) {
                String str = MicroAppSettingRepository.TAG;
                MFLogger.d(str, "getMicroAppSettingList migrateMicroAppSetting onSuccess setting=" + this.val$setting.getSetting());
            }
        }

        @DexIgnore
        public Anon4(MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback) {
            this.val$callback = microAppSettingListCallback;
        }

        @DexIgnore
        public /* synthetic */ void a(List list) {
            MFLogger.d(MicroAppSettingRepository.TAG, "diskIO enter downloadMicroAppSettingList");
            Iterator it = list.iterator();
            while (it.hasNext()) {
                MicroAppSetting microAppSetting = (MicroAppSetting) it.next();
                MicroAppSettingRepository.this.mMicroAppSettingLocalDataSource.mergeMicroAppSetting(microAppSetting, new Anon1(microAppSetting));
            }
            MicroAppSettingRepository.this.notifyMicroAppSettingObservers();
            MFLogger.d(MicroAppSettingRepository.TAG, "diskIO exit downloadMicroAppSettingList");
        }

        @DexIgnore
        public void onFail() {
            MFLogger.d(MicroAppSettingRepository.TAG, "getMicroAppSettingList remote onFail");
            MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback = this.val$callback;
            if (microAppSettingListCallback != null) {
                microAppSettingListCallback.onFail();
            }
        }

        @DexIgnore
        public void onSuccess(List<MicroAppSetting> list) {
            String str = MicroAppSettingRepository.TAG;
            MFLogger.d(str, "downloadMicroAppSettingList settingListSize=" + list.size());
            MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback = this.val$callback;
            if (microAppSettingListCallback != null) {
                microAppSettingListCallback.onSuccess(list);
            }
            MicroAppSettingRepository.this.mAppExecutors.a().execute(new x72(this, list));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 implements MicroAppSettingDataSource.MicroAppSettingCallback {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppSettingDataSource.MicroAppSettingCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ String val$microAppId;

        @DexIgnore
        public Anon5(String str, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
            this.val$microAppId = str;
            this.val$callback = microAppSettingCallback;
        }

        @DexIgnore
        public void onFail() {
            String str = MicroAppSettingRepository.TAG;
            MFLogger.d(str, "getMicroAppSettingInDB onFail microAppId=" + this.val$microAppId);
            MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback = this.val$callback;
            if (microAppSettingCallback != null) {
                microAppSettingCallback.onFail();
            }
        }

        @DexIgnore
        public void onSuccess(MicroAppSetting microAppSetting) {
            String str = MicroAppSettingRepository.TAG;
            MFLogger.d(str, "getMicroAppSettingInDB onSuccess microAppId=" + this.val$microAppId);
            MicroAppSettingRepository.this.processLoadedSetting(microAppSetting);
            MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback = this.val$callback;
            if (microAppSettingCallback != null) {
                microAppSettingCallback.onSuccess(microAppSetting);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 implements MicroAppSettingDataSource.MicroAppSettingListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppSettingDataSource.MicroAppSettingListCallback val$callback;

        @DexIgnore
        public Anon6(MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback) {
            this.val$callback = microAppSettingListCallback;
        }

        @DexIgnore
        public void onFail() {
            MFLogger.d(MicroAppSettingRepository.TAG, "getMicroAppSettingListInDB onFail");
            MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback = this.val$callback;
            if (microAppSettingListCallback != null) {
                microAppSettingListCallback.onFail();
            }
        }

        @DexIgnore
        public void onSuccess(List<MicroAppSetting> list) {
            MFLogger.d(MicroAppSettingRepository.TAG, "getMicroAppSettingListInDB onSuccess");
            MicroAppSettingRepository.this.processLoadedSettingList(list);
            MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback = this.val$callback;
            if (microAppSettingListCallback != null) {
                microAppSettingListCallback.onSuccess(list);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements MicroAppSettingDataSource.PushMicroAppSettingToServerCallback {
        @DexIgnore
        public /* final */ /* synthetic */ CountDownLatch val$countDownLatch;

        @DexIgnore
        public Anon7(CountDownLatch countDownLatch) {
            this.val$countDownLatch = countDownLatch;
        }

        @DexIgnore
        public void onFail() {
            MFLogger.d(MicroAppSettingRepository.TAG, "pushMicroAppSettingToServer failed");
            CountDownLatch countDownLatch = this.val$countDownLatch;
            if (countDownLatch != null) {
                countDownLatch.countDown();
            }
        }

        @DexIgnore
        public void onSuccess() {
            MFLogger.d(MicroAppSettingRepository.TAG, "pushMicroAppSettingToServer success, bravo!!!");
            CountDownLatch countDownLatch = this.val$countDownLatch;
            if (countDownLatch != null) {
                countDownLatch.countDown();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements MicroAppSettingDataSource.MicroAppSettingCallback {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppSettingDataSource.PushMicroAppSettingToServerCallback val$callback;

        @DexIgnore
        public Anon8(MicroAppSettingDataSource.PushMicroAppSettingToServerCallback pushMicroAppSettingToServerCallback) {
            this.val$callback = pushMicroAppSettingToServerCallback;
        }

        @DexIgnore
        public /* synthetic */ void a(MicroAppSetting microAppSetting, MicroAppSettingDataSource.PushMicroAppSettingToServerCallback pushMicroAppSettingToServerCallback) {
            MFLogger.d(MicroAppSettingRepository.TAG, "diskIO enter pushMicroAppSettingToServer");
            ((MicroAppSettingLocalDataSource) MicroAppSettingRepository.this.mMicroAppSettingLocalDataSource).updateMicroAppSettingPinType(microAppSetting.getMicroAppId(), 0);
            if (pushMicroAppSettingToServerCallback != null) {
                pushMicroAppSettingToServerCallback.onSuccess();
            }
            MFLogger.d(MicroAppSettingRepository.TAG, "diskIO exit pushMicroAppSettingToServer");
        }

        @DexIgnore
        public void onFail() {
            MicroAppSettingDataSource.PushMicroAppSettingToServerCallback pushMicroAppSettingToServerCallback = this.val$callback;
            if (pushMicroAppSettingToServerCallback != null) {
                pushMicroAppSettingToServerCallback.onFail();
            }
        }

        @DexIgnore
        public void onSuccess(MicroAppSetting microAppSetting) {
            MicroAppSettingRepository.this.mAppExecutors.a().execute(new y72(this, microAppSetting, this.val$callback));
        }
    }

    @DexIgnore
    public interface MicroAppSettingRepositoryObserver {
        @DexIgnore
        void onMicroAppChanged();
    }

    @DexIgnore
    public MicroAppSettingRepository(@Remote MicroAppSettingDataSource microAppSettingDataSource, @Local MicroAppSettingDataSource microAppSettingDataSource2, h42 h42) {
        n44.a(microAppSettingDataSource, "mappingRemoteSetDataSource cannot be null!");
        this.mMicroAppSettingRemoteDataSource = microAppSettingDataSource;
        n44.a(microAppSettingDataSource2, "mappingLocalSetDataSource cannot be null!");
        this.mMicroAppSettingLocalDataSource = microAppSettingDataSource2;
        n44.a(h42, "appExecutors an not be null!");
        this.mAppExecutors = h42;
    }

    @DexIgnore
    public void addMicroAppSettingRepositoryObserver(MicroAppSettingRepositoryObserver microAppSettingRepositoryObserver) {
        if (!this.microAppSettingRepositoryObservers.contains(microAppSettingRepositoryObserver)) {
            this.microAppSettingRepositoryObservers.add(microAppSettingRepositoryObserver);
        }
    }

    @DexIgnore
    public void addOrUpdateMicroAppSetting(MicroAppSetting microAppSetting, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateMicroAppSetting microAppSetting=" + microAppSetting.getSetting());
        microAppSetting.setPinType(2);
        this.mMicroAppSettingLocalDataSource.addOrUpdateMicroAppSetting(microAppSetting, new Anon1(microAppSetting, microAppSettingCallback));
    }

    @DexIgnore
    public void clearData() {
        this.mMicroAppSettingLocalDataSource.clearData();
        this.mCachedSetting = null;
        this.mIsCachedDirty = true;
        this.mCachedSettingList = null;
        this.mIsCachedListDirty = true;
    }

    @DexIgnore
    public void downloadMicroAppSettingList(MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback) {
        this.mMicroAppSettingRemoteDataSource.getMicroAppSettingList(new Anon4(microAppSettingListCallback));
    }

    @DexIgnore
    public MicroAppSetting getCachedSetting() {
        return this.mCachedSetting;
    }

    @DexIgnore
    public List<MicroAppSetting> getCachedSettingList() {
        List<MicroAppSetting> list = this.mCachedSettingList;
        return list == null ? new ArrayList() : list;
    }

    @DexIgnore
    public void getMicroAppSetting(String str, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getMicroAppSetting microAppId=" + str);
        this.mMicroAppSettingLocalDataSource.getMicroAppSetting(str, new Anon3(str, microAppSettingCallback));
    }

    @DexIgnore
    public void getMicroAppSettingInDB(String str, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "Inside .getMicroAppSettingInDB microAppId=" + str + ", isCacheDirty=" + this.mIsCachedDirty);
        if (!this.mIsCachedDirty) {
            MicroAppSetting microAppSetting = this.mCachedSetting;
            if (!(microAppSetting == null || !microAppSetting.getMicroAppId().equals(str) || microAppSettingCallback == null)) {
                String str3 = TAG;
                MFLogger.d(str3, "Inside .getMicroAppSettingInDB get from cache cachedSetting=" + this.mCachedSetting.getSetting());
                microAppSettingCallback.onSuccess(this.mCachedSetting);
                return;
            }
        }
        String str4 = TAG;
        MFLogger.d(str4, "getMicroAppSettingInDB microAppId=" + str);
        this.mMicroAppSettingLocalDataSource.getMicroAppSetting(str, new Anon5(str, microAppSettingCallback));
    }

    @DexIgnore
    public void getMicroAppSettingList(MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback) {
        this.mMicroAppSettingLocalDataSource.getMicroAppSettingList(new Anon2(microAppSettingListCallback));
    }

    @DexIgnore
    public void getMicroAppSettingListInDB(MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback) {
        String str = TAG;
        MFLogger.d(str, "Inside .getMicroAppSettingListInDB isCacheDirty=" + this.mIsCachedDirty);
        if (this.mIsCachedListDirty || this.mCachedSettingList == null || microAppSettingListCallback == null) {
            MFLogger.d(TAG, "getMicroAppSettingListInDB");
            this.mMicroAppSettingLocalDataSource.getMicroAppSettingList(new Anon6(microAppSettingListCallback));
            return;
        }
        String str2 = TAG;
        MFLogger.d(str2, "Inside .getMicroAppSettingListInDB get from cache cachedSettingListSize=" + this.mCachedSettingList.size());
        microAppSettingListCallback.onSuccess(getCachedSettingList());
    }

    @DexIgnore
    public boolean isCachedSettingAvailable(String str) {
        MicroAppSetting microAppSetting = this.mCachedSetting;
        return microAppSetting != null && !this.mIsCachedDirty && microAppSetting.getMicroAppId().equals(str);
    }

    @DexIgnore
    public boolean isCachedSettingListAvailable() {
        return this.mCachedSettingList != null && !this.mIsCachedListDirty;
    }

    @DexIgnore
    public void mergeMicroAppSetting(MicroAppSetting microAppSetting, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
        MFLogger.d(TAG, "mergeMicroAppSetting");
        this.mMicroAppSettingLocalDataSource.mergeMicroAppSetting(microAppSetting, microAppSettingCallback);
    }

    @DexIgnore
    public void notifyMicroAppSettingObservers() {
        String str = TAG;
        MFLogger.d(str, "notifyMicroAppSettingObservers, observerSize=" + this.microAppSettingRepositoryObservers.size());
        for (MicroAppSettingRepositoryObserver onMicroAppChanged : this.microAppSettingRepositoryObservers) {
            onMicroAppChanged.onMicroAppChanged();
        }
    }

    @DexIgnore
    public void processLoadedSetting(MicroAppSetting microAppSetting) {
        String str = TAG;
        MFLogger.d(str, "processLoadedSetting microAppSetting=" + microAppSetting);
        this.mIsCachedDirty = false;
        if (microAppSetting == null) {
            this.mCachedSetting = null;
            return;
        }
        String str2 = TAG;
        MFLogger.d(str2, "processLoadedSetting microAppSetting=" + microAppSetting.getSetting());
        this.mCachedSetting = microAppSetting;
    }

    @DexIgnore
    public void processLoadedSettingList(List<MicroAppSetting> list) {
        MFLogger.d(TAG, "processLoadedSettingList");
        this.mIsCachedListDirty = false;
        this.mCachedSettingList = list;
    }

    @DexIgnore
    public void pushMicroAppSettingToServer(MicroAppSetting microAppSetting, MicroAppSettingDataSource.PushMicroAppSettingToServerCallback pushMicroAppSettingToServerCallback) {
        this.mMicroAppSettingRemoteDataSource.addOrUpdateMicroAppSetting(microAppSetting, new Anon8(pushMicroAppSettingToServerCallback));
    }

    @DexIgnore
    public void pushPendingMicroAppSettings(MicroAppSettingDataSource.PushPendingMicroAppSettingsCallback pushPendingMicroAppSettingsCallback) {
        MFLogger.d(TAG, "pushPendingMicroAppSettings");
        List<MicroAppSetting> pendingMicroAppSettings = ((MicroAppSettingLocalDataSource) this.mMicroAppSettingLocalDataSource).getPendingMicroAppSettings();
        CountDownLatch countDownLatch = (pushPendingMicroAppSettingsCallback == null || pendingMicroAppSettings.isEmpty()) ? null : new CountDownLatch(pendingMicroAppSettings.size());
        for (MicroAppSetting pushMicroAppSettingToServer : pendingMicroAppSettings) {
            pushMicroAppSettingToServer(pushMicroAppSettingToServer, new Anon7(countDownLatch));
        }
        if (countDownLatch != null) {
            try {
                String str = TAG;
                MFLogger.d(str, "Await on thread=" + Thread.currentThread().getName());
                countDownLatch.await();
                String str2 = TAG;
                MFLogger.d(str2, "Await done on thread=" + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }
        if (pushPendingMicroAppSettingsCallback != null) {
            pushPendingMicroAppSettingsCallback.onDone();
        }
    }

    @DexIgnore
    public void removeMicroAppSettingRepositoryObserver(MicroAppSettingRepositoryObserver microAppSettingRepositoryObserver) {
        if (this.microAppSettingRepositoryObservers.contains(microAppSettingRepositoryObserver)) {
            this.microAppSettingRepositoryObservers.remove(microAppSettingRepositoryObserver);
        }
    }
}
