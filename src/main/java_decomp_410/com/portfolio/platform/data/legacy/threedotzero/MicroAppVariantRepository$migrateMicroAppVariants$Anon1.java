package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wo2;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRepository$migrateMicroAppVariants$Anon1 implements MicroAppVariantDataSource.GetVariantListRemoteCallback {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantDataSource.MigrateVariantCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ int $major;
    @DexIgnore
    public /* final */ /* synthetic */ int $minor;
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantRepository this$Anon0;

    @DexIgnore
    public MicroAppVariantRepository$migrateMicroAppVariants$Anon1(MicroAppVariantRepository microAppVariantRepository, String str, int i, int i2, MicroAppVariantDataSource.MigrateVariantCallback migrateVariantCallback) {
        this.this$Anon0 = microAppVariantRepository;
        this.$serialNumber = str;
        this.$major = i;
        this.$minor = i2;
        this.$callback = migrateVariantCallback;
    }

    @DexIgnore
    public void onFail(int i) {
        String tag = MicroAppVariantRepository.Companion.getTAG();
        MFLogger.d(tag, "getAllMicroAppVariants remote serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onFail");
        MicroAppVariantDataSource.MigrateVariantCallback migrateVariantCallback = this.$callback;
        if (migrateVariantCallback != null) {
            migrateVariantCallback.onDone();
        }
    }

    @DexIgnore
    public void onSuccess(ArrayList<wo2> arrayList) {
        kd4.b(arrayList, "variantParserList");
        String tag = MicroAppVariantRepository.Companion.getTAG();
        MFLogger.d(tag, "getAllMicroAppVariants remote serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onSuccess");
        this.this$Anon0.mAppExecutors.a().execute(new MicroAppVariantRepository$migrateMicroAppVariants$Anon1$onSuccess$Anon1(this, arrayList));
    }

    @DexIgnore
    public void onSuccess(List<MicroAppVariant> list) {
        kd4.b(list, "variantList");
    }
}
