package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.n44;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepositoryModule_ProvideGalleryRemoteDataSource$app_fossilReleaseFactory implements Factory<MicroAppGalleryDataSource> {
    @DexIgnore
    public /* final */ Provider<h42> appExecutorsProvider;
    @DexIgnore
    public /* final */ MicroAppGalleryRepositoryModule module;
    @DexIgnore
    public /* final */ Provider<ShortcutApiService> shortcutApiServiceProvider;

    @DexIgnore
    public MicroAppGalleryRepositoryModule_ProvideGalleryRemoteDataSource$app_fossilReleaseFactory(MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule, Provider<ShortcutApiService> provider, Provider<h42> provider2) {
        this.module = microAppGalleryRepositoryModule;
        this.shortcutApiServiceProvider = provider;
        this.appExecutorsProvider = provider2;
    }

    @DexIgnore
    public static MicroAppGalleryRepositoryModule_ProvideGalleryRemoteDataSource$app_fossilReleaseFactory create(MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule, Provider<ShortcutApiService> provider, Provider<h42> provider2) {
        return new MicroAppGalleryRepositoryModule_ProvideGalleryRemoteDataSource$app_fossilReleaseFactory(microAppGalleryRepositoryModule, provider, provider2);
    }

    @DexIgnore
    public static MicroAppGalleryDataSource provideInstance(MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule, Provider<ShortcutApiService> provider, Provider<h42> provider2) {
        return proxyProvideGalleryRemoteDataSource$app_fossilRelease(microAppGalleryRepositoryModule, provider.get(), provider2.get());
    }

    @DexIgnore
    public static MicroAppGalleryDataSource proxyProvideGalleryRemoteDataSource$app_fossilRelease(MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule, ShortcutApiService shortcutApiService, h42 h42) {
        MicroAppGalleryDataSource provideGalleryRemoteDataSource$app_fossilRelease = microAppGalleryRepositoryModule.provideGalleryRemoteDataSource$app_fossilRelease(shortcutApiService, h42);
        n44.a(provideGalleryRemoteDataSource$app_fossilRelease, "Cannot return null from a non-@Nullable @Provides method");
        return provideGalleryRemoteDataSource$app_fossilRelease;
    }

    @DexIgnore
    public MicroAppGalleryDataSource get() {
        return provideInstance(this.module, this.shortcutApiServiceProvider, this.appExecutorsProvider);
    }
}
