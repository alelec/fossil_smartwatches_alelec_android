package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.er4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xz1;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRemoteDataSource extends MicroAppVariantDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ h42 mAppExecutors;
    @DexIgnore
    public /* final */ ShortcutApiService mShortcutApiService;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppVariantRemoteDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            kd4.b(str, "<set-?>");
            MicroAppVariantRemoteDataSource.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppVariantRemoteDataSource.class.getSimpleName();
        kd4.a((Object) simpleName, "MicroAppVariantRemoteDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppVariantRemoteDataSource(ShortcutApiService shortcutApiService, h42 h42) {
        kd4.b(shortcutApiService, "mShortcutApiService");
        kd4.b(h42, "mAppExecutors");
        this.mShortcutApiService = shortcutApiService;
        this.mAppExecutors = h42;
    }

    @DexIgnore
    public void getAllMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        kd4.b(str, "serialNumber");
        String str2 = TAG;
        MFLogger.d(str2, "getAllMicroAppVariants serialNumber=" + str);
        getAllMicroAppVariants$app_fossilRelease(str, i, i2, 0, 100, new MicroAppVariantRemoteDataSource$getAllMicroAppVariants$resultCallback$Anon1(this, str, new ArrayList(), i, i2, getVariantListCallback));
    }

    @DexIgnore
    public final void getAllMicroAppVariants$app_fossilRelease(String str, int i, int i2, int i3, int i4, er4<xz1> er4) {
        kd4.b(str, "serialNumber");
        kd4.b(er4, Constants.CALLBACK);
    }
}
