package com.portfolio.platform.data.legacy.threedotzero;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.gson.stream.JsonWriter;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;
import com.portfolio.platform.data.model.room.microapp.ButtonMapping;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@DatabaseTable(tableName = "recommendedPreset")
public class RecommendedPreset implements Parcelable {
    @DexIgnore
    public static /* final */ String COLUMN_BUTTONS; // = "buttons";
    @DexIgnore
    public static /* final */ String COLUMN_CREATE_AT; // = "createAt";
    @DexIgnore
    public static /* final */ String COLUMN_DESCRIPTION; // = "description";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_NAME; // = "name";
    @DexIgnore
    public static /* final */ String COLUMN_OBJECT_ID; // = "objectId";
    @DexIgnore
    public static /* final */ String COLUMN_TYPE; // = "type";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ Parcelable.Creator<RecommendedPreset> CREATOR; // = new Anon1();
    @DexIgnore
    public static /* final */ String SERIAL_NUMBER; // = "serialNumber";
    @DexIgnore
    public static /* final */ String TAG; // = RecommendedPreset.class.getSimpleName();
    @DexIgnore
    public List<ButtonMapping> buttonMappingList; // = new ArrayList();
    @DexIgnore
    @DatabaseField(columnName = "buttons")
    public String buttons;
    @DexIgnore
    @DatabaseField(columnName = "createAt")
    public long createAt;
    @DexIgnore
    @DatabaseField(columnName = "description")
    public String description;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String id;
    @DexIgnore
    @DatabaseField(columnName = "name")
    public String name;
    @DexIgnore
    @DatabaseField(columnName = "serialNumber")
    public String serialNumber;
    @DexIgnore
    @DatabaseField(columnName = "type")
    public String type;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    public long updateAt;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<RecommendedPreset> {
        @DexIgnore
        public RecommendedPreset createFromParcel(Parcel parcel) {
            return new RecommendedPreset(parcel);
        }

        @DexIgnore
        public RecommendedPreset[] newArray(int i) {
            return new RecommendedPreset[i];
        }
    }

    @DexIgnore
    public RecommendedPreset() {
    }

    @DexIgnore
    private List<ButtonMapping> getListMappingFromJson() {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = new JSONArray(this.buttons);
            if (jSONArray.length() > 0) {
                for (int i = 0; i < jSONArray.length(); i++) {
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    @DexIgnore
    private void write(JsonWriter jsonWriter, List<ButtonMapping> list) throws IOException {
        jsonWriter.A();
        for (ButtonMapping next : list) {
            jsonWriter.B();
            jsonWriter.D();
        }
        jsonWriter.C();
    }

    @DexIgnore
    public ActivePreset convertToActivePreset() {
        ActivePreset activePreset = new ActivePreset();
        activePreset.setCreateAt(System.currentTimeMillis());
        activePreset.setButtons(this.buttons);
        activePreset.setOriginalId(this.id);
        activePreset.setSerialNumber(this.serialNumber);
        return activePreset;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public List<ButtonMapping> getButtonMappingList() {
        if (this.buttonMappingList.isEmpty() && !TextUtils.isEmpty(this.buttons)) {
            this.buttonMappingList = new ArrayList(getListMappingFromJson());
        }
        return this.buttonMappingList;
    }

    @DexIgnore
    public String getButtons() {
        return this.buttons;
    }

    @DexIgnore
    public long getCreateAt() {
        return this.createAt;
    }

    @DexIgnore
    public String getDescription() {
        return this.description;
    }

    @DexIgnore
    public String getId() {
        return this.id;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public SavedPreset.MappingSetType getType() {
        return SavedPreset.MappingSetType.fromString(this.type);
    }

    @DexIgnore
    public long getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public void setButtonMappingList(List<ButtonMapping> list) throws IOException {
        StringWriter stringWriter = new StringWriter();
        JsonWriter jsonWriter = new JsonWriter(stringWriter);
        write(jsonWriter, list);
        this.buttons = stringWriter.toString();
        this.buttonMappingList.clear();
        this.buttonMappingList.addAll(list);
        try {
            stringWriter.close();
            jsonWriter.close();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "Exception when close writer e=" + e);
        }
    }

    @DexIgnore
    public void setButtons(String str) {
        this.buttons = str;
        getButtonMappingList();
    }

    @DexIgnore
    public void setCreateAt(long j) {
        this.createAt = j;
    }

    @DexIgnore
    public void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public void setSerialNumber(String str) {
        this.serialNumber = str;
    }

    @DexIgnore
    public void setType(SavedPreset.MappingSetType mappingSetType) {
        this.type = mappingSetType.getValue();
    }

    @DexIgnore
    public void setUpdateAt(long j) {
        this.updateAt = j;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.buttonMappingList);
        parcel.writeString(this.name);
        parcel.writeString(this.description);
        parcel.writeString(this.buttons);
        parcel.writeString(this.serialNumber);
        parcel.writeLong(this.createAt);
        parcel.writeLong(this.updateAt);
        parcel.writeString(this.id);
        parcel.writeString(this.type);
    }

    @DexIgnore
    public RecommendedPreset(RecommendedPreset recommendedPreset) {
        this.type = recommendedPreset.type;
        this.name = recommendedPreset.name;
        this.description = recommendedPreset.description;
        this.createAt = recommendedPreset.createAt;
        this.updateAt = recommendedPreset.updateAt;
        this.serialNumber = recommendedPreset.serialNumber;
    }

    @DexIgnore
    public RecommendedPreset(Parcel parcel) {
        this.name = parcel.readString();
        this.description = parcel.readString();
        this.buttons = parcel.readString();
        this.serialNumber = parcel.readString();
        this.createAt = parcel.readLong();
        this.updateAt = parcel.readLong();
        this.id = parcel.readString();
        this.type = parcel.readString();
    }
}
