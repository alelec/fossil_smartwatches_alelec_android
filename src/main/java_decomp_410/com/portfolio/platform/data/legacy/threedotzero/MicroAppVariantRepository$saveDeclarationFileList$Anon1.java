package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRepository$saveDeclarationFileList$Anon1 implements MicroAppVariantDataSource.AddOrUpdateDeclarationFileCallback {
    @DexIgnore
    public void onFail() {
        MFLogger.d(MicroAppVariantRepository.Companion.getTAG(), "save microAppDeclarationFile onFail");
    }

    @DexIgnore
    public void onSuccess(DeclarationFile declarationFile) {
        kd4.b(declarationFile, "declarationFile");
        String tag = MicroAppVariantRepository.Companion.getTAG();
        StringBuilder sb = new StringBuilder();
        sb.append("save microAppDeclarationFile onSuccess variantId=");
        MicroAppVariant microAppVariant = declarationFile.getMicroAppVariant();
        kd4.a((Object) microAppVariant, "declarationFile.microAppVariant");
        sb.append(microAppVariant.getId());
        MFLogger.d(tag, sb.toString());
    }
}
