package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.f02;
import com.google.gson.Gson;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@DatabaseTable(tableName = "uappsystemversion")
public class UAppSystemVersionModel {
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_ID; // = "deviceId";
    @DexIgnore
    public static /* final */ String COLUMN_MAJOR_VERSION; // = "majorVersion";
    @DexIgnore
    public static /* final */ String COLUMN_MINOR_VERSION; // = "minorVersion";
    @DexIgnore
    public static /* final */ String COLUMN_PIN_TYPE; // = "pinType";
    @DexIgnore
    public static /* final */ int UNSUPPORTED_MAJOR_MINOR_NUMBER; // = 255;
    @DexIgnore
    @f02("deviceId")
    @DatabaseField(columnName = "deviceId", id = true)
    public String deviceId;
    @DexIgnore
    @DatabaseField(columnName = "majorVersion")
    public int majorVersion;
    @DexIgnore
    @DatabaseField(columnName = "minorVersion")
    public int minorVersion;
    @DexIgnore
    @DatabaseField(columnName = "pinType")
    public int pinType;

    @DexIgnore
    public UAppSystemVersionModel() {
    }

    @DexIgnore
    private String getHash() {
        return this.deviceId + ':' + this.majorVersion + ':' + this.minorVersion + ':' + this.pinType;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof UAppSystemVersionModel) {
            return getHash().equals(((UAppSystemVersionModel) obj).getHash());
        }
        return false;
    }

    @DexIgnore
    public String getDeviceId() {
        return this.deviceId;
    }

    @DexIgnore
    public int getMajorVersion() {
        return this.majorVersion;
    }

    @DexIgnore
    public int getMinorVersion() {
        return this.minorVersion;
    }

    @DexIgnore
    public int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public void setDeviceId(String str) {
        this.deviceId = str;
    }

    @DexIgnore
    public void setMajorVersion(int i) {
        this.majorVersion = i;
    }

    @DexIgnore
    public void setMinorVersion(int i) {
        this.minorVersion = i;
    }

    @DexIgnore
    public void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public String toString() {
        return new Gson().a((Object) this);
    }

    @DexIgnore
    public UAppSystemVersionModel(String str, int i, int i2) {
        this.deviceId = str;
        this.majorVersion = i;
        this.minorVersion = i2;
        this.pinType = 0;
    }

    @DexIgnore
    public UAppSystemVersionModel(String str, int i, int i2, int i3) {
        this(str, i, i2);
        this.pinType = i3;
    }
}
