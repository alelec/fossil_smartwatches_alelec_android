package com.portfolio.platform.data.legacy.threedotzero;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.f02;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@DatabaseTable(tableName = "microAppVariant")
public class MicroAppVariant implements Parcelable {
    @DexIgnore
    public static /* final */ String COLUMN_APP_ID; // = "appId";
    @DexIgnore
    public static /* final */ String COLUMN_CREATE_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String COLUMN_DECLARATION_FILES; // = "declarationFiles";
    @DexIgnore
    public static /* final */ String COLUMN_DESCRIPTION; // = "description";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_MAJOR_NUMBER; // = "majorNumber";
    @DexIgnore
    public static /* final */ String COLUMN_MINOR_NUMBER; // = "minorNumber";
    @DexIgnore
    public static /* final */ String COLUMN_NAME; // = "name";
    @DexIgnore
    public static /* final */ String COLUMN_SERIAL_NUMBER; // = "serialNumber";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ Parcelable.Creator<MicroAppVariant> CREATOR; // = new Anon1();
    @DexIgnore
    @f02("appId")
    @DatabaseField(columnName = "appId")
    public String appId;
    @DexIgnore
    @f02("createdAt")
    @DatabaseField(columnName = "createdAt")
    public long createAt;
    @DexIgnore
    @ForeignCollectionField(columnName = "declarationFiles", eager = true)
    public ForeignCollection<DeclarationFile> declarationFiles;
    @DexIgnore
    @f02("description")
    @DatabaseField(columnName = "description")
    public String description;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String id;
    @DexIgnore
    @f02("majorNumber")
    @DatabaseField(columnName = "majorNumber")
    public int majorNumber;
    @DexIgnore
    @f02("minorNumber")
    @DatabaseField(columnName = "minorNumber")
    public int minorNumber;
    @DexIgnore
    @f02("name")
    @DatabaseField(columnName = "name")
    public String name;
    @DexIgnore
    @f02("serialNumber")
    @DatabaseField(columnName = "serialNumber")
    public String serialNumbers;
    @DexIgnore
    @f02("updatedAt")
    @DatabaseField(columnName = "updatedAt")
    public long updateAt;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<MicroAppVariant> {
        @DexIgnore
        public MicroAppVariant createFromParcel(Parcel parcel) {
            return new MicroAppVariant(parcel);
        }

        @DexIgnore
        public MicroAppVariant[] newArray(int i) {
            return new MicroAppVariant[i];
        }
    }

    @DexIgnore
    public MicroAppVariant() {
        this.id = UUID.randomUUID().toString();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String getAppId() {
        return this.appId;
    }

    @DexIgnore
    public long getCreateAt() {
        return this.createAt;
    }

    @DexIgnore
    public List<DeclarationFile> getDeclarationFiles() {
        ArrayList arrayList = new ArrayList();
        ForeignCollection<DeclarationFile> foreignCollection = this.declarationFiles;
        return (foreignCollection == null || foreignCollection.isEmpty()) ? arrayList : new ArrayList(this.declarationFiles);
    }

    @DexIgnore
    public String getDescription() {
        return this.description;
    }

    @DexIgnore
    public String getId() {
        return this.id;
    }

    @DexIgnore
    public int getMajorNumber() {
        return this.majorNumber;
    }

    @DexIgnore
    public int getMinorNumber() {
        return this.minorNumber;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public String getSerialNumbers() {
        return this.serialNumbers;
    }

    @DexIgnore
    public long getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public void setAppId(String str) {
        this.appId = str;
    }

    @DexIgnore
    public void setCreateAt(long j) {
        this.createAt = j;
    }

    @DexIgnore
    public void setDeclarationFiles(List<DeclarationFile> list) {
        this.declarationFiles.addAll(list);
    }

    @DexIgnore
    public void setDescription(String str) {
        this.description = str;
    }

    @DexIgnore
    public void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public void setMajorNumber(int i) {
        this.majorNumber = i;
    }

    @DexIgnore
    public void setMinorNumber(int i) {
        this.minorNumber = i;
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public void setSerialNumbers(String str) {
        this.serialNumbers = str;
    }

    @DexIgnore
    public void setUpdateAt(long j) {
        this.updateAt = j;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.id);
        parcel.writeString(this.appId);
        parcel.writeString(this.name);
        parcel.writeString(this.description);
        parcel.writeList((List) this.declarationFiles);
        parcel.writeLong(this.createAt);
        parcel.writeLong(this.updateAt);
    }

    @DexIgnore
    public MicroAppVariant(Parcel parcel) {
        this.id = parcel.readString();
        this.appId = parcel.readString();
        this.name = parcel.readString();
        this.description = parcel.readString();
        ArrayList arrayList = new ArrayList();
        parcel.readList(arrayList, String.class.getClassLoader());
        this.declarationFiles.addAll(arrayList);
        this.createAt = parcel.readLong();
        this.updateAt = parcel.readLong();
    }
}
