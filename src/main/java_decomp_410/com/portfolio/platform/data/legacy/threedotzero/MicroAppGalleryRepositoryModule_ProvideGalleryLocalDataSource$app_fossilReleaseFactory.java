package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.n44;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepositoryModule_ProvideGalleryLocalDataSource$app_fossilReleaseFactory implements Factory<MicroAppGalleryDataSource> {
    @DexIgnore
    public /* final */ MicroAppGalleryRepositoryModule module;

    @DexIgnore
    public MicroAppGalleryRepositoryModule_ProvideGalleryLocalDataSource$app_fossilReleaseFactory(MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule) {
        this.module = microAppGalleryRepositoryModule;
    }

    @DexIgnore
    public static MicroAppGalleryRepositoryModule_ProvideGalleryLocalDataSource$app_fossilReleaseFactory create(MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule) {
        return new MicroAppGalleryRepositoryModule_ProvideGalleryLocalDataSource$app_fossilReleaseFactory(microAppGalleryRepositoryModule);
    }

    @DexIgnore
    public static MicroAppGalleryDataSource provideInstance(MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule) {
        return proxyProvideGalleryLocalDataSource$app_fossilRelease(microAppGalleryRepositoryModule);
    }

    @DexIgnore
    public static MicroAppGalleryDataSource proxyProvideGalleryLocalDataSource$app_fossilRelease(MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule) {
        MicroAppGalleryDataSource provideGalleryLocalDataSource$app_fossilRelease = microAppGalleryRepositoryModule.provideGalleryLocalDataSource$app_fossilRelease();
        n44.a(provideGalleryLocalDataSource$app_fossilRelease, "Cannot return null from a non-@Nullable @Provides method");
        return provideGalleryLocalDataSource$app_fossilRelease;
    }

    @DexIgnore
    public MicroAppGalleryDataSource get() {
        return provideInstance(this.module);
    }
}
