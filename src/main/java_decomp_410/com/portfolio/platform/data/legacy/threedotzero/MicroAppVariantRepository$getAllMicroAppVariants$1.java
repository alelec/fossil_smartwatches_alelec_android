package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRepository$getAllMicroAppVariants$1 implements com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ int $major;
    @DexIgnore
    public /* final */ /* synthetic */ int $minor;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository this$0;

    @DexIgnore
    public MicroAppVariantRepository$getAllMicroAppVariants$1(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository microAppVariantRepository, java.lang.String str, int i, int i2, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        this.this$0 = microAppVariantRepository;
        this.$serialNumber = str;
        this.$major = i;
        this.$minor = i2;
        this.$callback = getVariantListCallback;
    }

    @DexIgnore
    public void onFail(int i) {
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "getAllMicroAppVariants local serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onFail");
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback = this.$callback;
        if (getVariantListCallback != null) {
            getVariantListCallback.onFail(i);
        }
    }

    @DexIgnore
    public void onSuccess(java.util.List<com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant> list) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "variantList");
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "getAllMicroAppVariants local serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onSuccess");
        this.this$0.notifyStatusChanged("DECLARATION_FILES_DOWNLOADED", this.$serialNumber);
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback = this.$callback;
        if (getVariantListCallback != null) {
            getVariantListCallback.onSuccess(list);
        }
    }
}
