package com.portfolio.platform.data.legacy.onedotfive;

import com.fossil.wearables.fsl.BaseProvider;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface LegacyDeviceProvider extends BaseProvider {
    @DexIgnore
    List<LegacyDeviceModel> getAllDevice();

    @DexIgnore
    List<FavoriteMappingSet> getAllFavoriteMappingSet();

    @DexIgnore
    LegacyDeviceModel getDeviceById(String str);
}
