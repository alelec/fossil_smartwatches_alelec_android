package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRepository$getMicroAppVariant$Anon1$onFail$Anon1 implements MicroAppVariantDataSource.GetVariantListCallback {
    @DexIgnore
    public /* final */ /* synthetic */ int $errorCode;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantRepository$getMicroAppVariant$Anon1 this$Anon0;

    @DexIgnore
    public MicroAppVariantRepository$getMicroAppVariant$Anon1$onFail$Anon1(MicroAppVariantRepository$getMicroAppVariant$Anon1 microAppVariantRepository$getMicroAppVariant$Anon1, int i) {
        this.this$Anon0 = microAppVariantRepository$getMicroAppVariant$Anon1;
        this.$errorCode = i;
    }

    @DexIgnore
    public void onFail(int i) {
        String tag = MicroAppVariantRepository.Companion.getTAG();
        MFLogger.d(tag, "getMicroAppVariant remote serialNumber=" + this.this$Anon0.$serialNumber + " major=" + this.this$Anon0.$major + " minor=" + this.this$Anon0.$minor + " onFail");
        MicroAppVariantDataSource.GetVariantCallback getVariantCallback = this.this$Anon0.$callback;
        if (getVariantCallback != null) {
            getVariantCallback.onFail(i);
        }
    }

    @DexIgnore
    public void onSuccess(List<MicroAppVariant> list) {
        MicroAppVariant microAppVariant;
        T t;
        T t2;
        kd4.b(list, "variantList");
        MFLogger.d(MicroAppVariantRepository.Companion.getTAG(), "getMicroAppVariant remote serialNumber=" + this.this$Anon0.$serialNumber + " major=" + this.this$Anon0.$major + " minor=" + this.this$Anon0.$minor + " onSuccess");
        if (MicroAppVariantRepository.WhenMappings.$EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.this$Anon0.$microAppId).ordinal()] != 1) {
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                if (kd4.a((Object) ((MicroAppVariant) t2).getAppId(), (Object) this.this$Anon0.$microAppId)) {
                    break;
                }
            }
            microAppVariant = (MicroAppVariant) t2;
        } else {
            Iterator<T> it2 = list.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t = null;
                    break;
                }
                t = it2.next();
                if (kd4.a((Object) ((MicroAppVariant) t).getName(), (Object) this.this$Anon0.$variantName)) {
                    break;
                }
            }
            microAppVariant = (MicroAppVariant) t;
        }
        if (microAppVariant != null) {
            MicroAppVariantDataSource.GetVariantCallback getVariantCallback = this.this$Anon0.$callback;
            if (getVariantCallback != null) {
                getVariantCallback.onSuccess(microAppVariant);
                return;
            }
            return;
        }
        MicroAppVariantDataSource.GetVariantCallback getVariantCallback2 = this.this$Anon0.$callback;
        if (getVariantCallback2 != null) {
            getVariantCallback2.onFail(this.$errorCode);
        }
    }
}
