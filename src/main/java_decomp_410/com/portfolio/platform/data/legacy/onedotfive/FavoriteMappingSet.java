package com.portfolio.platform.data.legacy.onedotfive;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.tz1;
import com.fossil.blesdk.obfuscated.yz1;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.model.Mapping;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@DatabaseTable(tableName = "favoriteMappingSet")
public class FavoriteMappingSet implements Parcelable {
    @DexIgnore
    public static /* final */ String COLUMN_CHECKSUM; // = "checksum";
    @DexIgnore
    public static /* final */ String COLUMN_CREATE_AT; // = "createAt";
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_FAMILY; // = "deviceFamily";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_IS_ACTIVE; // = "isActive";
    @DexIgnore
    public static /* final */ String COLUMN_LIST_MAPPING_JSON; // = "listMappingJson";
    @DexIgnore
    public static /* final */ String COLUMN_LOCALIZED_NAME; // = "localizedName";
    @DexIgnore
    public static /* final */ String COLUMN_LOCALIZED_SUB_NAME; // = "localizedSubName";
    @DexIgnore
    public static /* final */ String COLUMN_NAME; // = "name";
    @DexIgnore
    public static /* final */ String COLUMN_OBJECT_ID; // = "objectId";
    @DexIgnore
    public static /* final */ String COLUMN_SUB_NAME; // = "subName";
    @DexIgnore
    public static /* final */ String COLUMN_THUMBNAIL; // = "thumbnail";
    @DexIgnore
    public static /* final */ String COLUMN_TYPE; // = "type";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ Parcelable.Creator<FavoriteMappingSet> CREATOR; // = new Anon1();
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "favoriteMappingSet";
    @DexIgnore
    public static /* final */ String TAG; // = FavoriteMappingSet.class.getSimpleName();
    @DexIgnore
    @DatabaseField(columnName = "checksum")
    public String checksum;
    @DexIgnore
    @DatabaseField(columnName = "createAt")
    public long createAt;
    @DexIgnore
    @DatabaseField(columnName = "deviceFamily")
    public int deviceFamily;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String id;
    @DexIgnore
    @DatabaseField(columnName = "isActive")
    public boolean isActive;
    @DexIgnore
    @DatabaseField(columnName = "localizedName")
    public String localizedName;
    @DexIgnore
    @DatabaseField(columnName = "localizedSubName")
    public String localizedSubName;
    @DexIgnore
    public List<Mapping> mappingList; // = new ArrayList();
    @DexIgnore
    @DatabaseField(columnName = "listMappingJson")
    public String mappingListJsonString;
    @DexIgnore
    @DatabaseField(columnName = "name")
    public String name;
    @DexIgnore
    @DatabaseField(columnName = "objectId")
    public String objectId;
    @DexIgnore
    @DatabaseField(columnName = "subName")
    public String subName;
    @DexIgnore
    @DatabaseField(columnName = "thumbnail")
    public String thumbnail;
    @DexIgnore
    @DatabaseField(columnName = "type")
    public int type;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    public long updateAt;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<FavoriteMappingSet> {
        @DexIgnore
        public FavoriteMappingSet createFromParcel(Parcel parcel) {
            return new FavoriteMappingSet(parcel);
        }

        @DexIgnore
        public FavoriteMappingSet[] newArray(int i) {
            return new FavoriteMappingSet[i];
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class MappingGestureComparator implements Comparator<Mapping> {
        @DexIgnore
        public MappingGestureComparator() {
        }

        @DexIgnore
        public int compare(Mapping mapping, Mapping mapping2) {
            if (mapping == null || mapping.getGesture() == null) {
                return 1;
            }
            if (mapping2 == null || mapping2.getGesture() == null) {
                return -1;
            }
            return mapping.getGesture().compareTo(mapping2.getGesture());
        }
    }

    @DexIgnore
    public enum MappingSetType {
        FEATURE(0),
        DEFAULT(1),
        USER_SAVED(2),
        USER_NOT_SAVED(3);
        
        @DexIgnore
        public int value;

        @DexIgnore
        MappingSetType(int i) {
            this.value = i;
        }

        @DexIgnore
        public static MappingSetType fromInt(int i) {
            for (MappingSetType mappingSetType : values()) {
                if (mappingSetType.value == i) {
                    return mappingSetType;
                }
            }
            return FEATURE;
        }

        @DexIgnore
        public int getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public FavoriteMappingSet() {
    }

    @DexIgnore
    private List<Mapping> getListMappingFromJson() {
        Gson gson = new Gson();
        tz1 c = new yz1().a(this.mappingListJsonString).c();
        ArrayList arrayList = new ArrayList();
        Iterator<JsonElement> it = c.iterator();
        while (it.hasNext()) {
            arrayList.add((Mapping) gson.a(it.next(), Mapping.class));
        }
        return arrayList;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String getChecksum() {
        return this.checksum;
    }

    @DexIgnore
    public long getCreateAt() {
        return this.createAt;
    }

    @DexIgnore
    public MFDeviceFamily getDeviceFamily() {
        return MFDeviceFamily.fromInt(this.deviceFamily);
    }

    @DexIgnore
    public String getId() {
        return this.id;
    }

    @DexIgnore
    public String getLocalizedName() {
        return this.localizedName;
    }

    @DexIgnore
    public String getLocalizedSubName() {
        return this.localizedSubName;
    }

    @DexIgnore
    public List<Mapping> getMappingList() {
        List<Mapping> list;
        synchronized (this.mappingList) {
            if ((this.mappingList == null || this.mappingList.size() == 0) && !TextUtils.isEmpty(this.mappingListJsonString)) {
                this.mappingList = getListMappingFromJson();
            }
            list = this.mappingList;
        }
        return list;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public String getObjectId() {
        return this.objectId;
    }

    @DexIgnore
    public String getSubName() {
        return this.subName;
    }

    @DexIgnore
    public String getThumbnail() {
        return this.thumbnail;
    }

    @DexIgnore
    public MappingSetType getType() {
        return MappingSetType.fromInt(this.type);
    }

    @DexIgnore
    public long getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public boolean is(FavoriteMappingSet favoriteMappingSet) {
        return favoriteMappingSet != null && this.id.equals(favoriteMappingSet.id);
    }

    @DexIgnore
    public boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public boolean isSameId(FavoriteMappingSet favoriteMappingSet) {
        return this.id.equals(favoriteMappingSet.id);
    }

    @DexIgnore
    public boolean isSameName(FavoriteMappingSet favoriteMappingSet) {
        return this.name.equals(favoriteMappingSet.name);
    }

    @DexIgnore
    public void notifyMappingListChanged() {
        synchronized (this.mappingList) {
            this.mappingListJsonString = new Gson().a((Object) this.mappingList);
        }
    }

    @DexIgnore
    public void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public void setChecksum(String str) {
        this.checksum = str;
    }

    @DexIgnore
    public void setCreateAt(long j) {
        this.createAt = j;
    }

    @DexIgnore
    public void setDeviceFamily(MFDeviceFamily mFDeviceFamily) {
        this.deviceFamily = mFDeviceFamily.getValue();
    }

    @DexIgnore
    public void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public void setLocalizedName(String str) {
        this.localizedName = str;
    }

    @DexIgnore
    public void setLocalizedSubName(String str) {
        this.localizedSubName = str;
    }

    @DexIgnore
    public void setMappingList(List<Mapping> list) {
        this.mappingList = list;
        notifyMappingListChanged();
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public void setObjectId(String str) {
        this.objectId = str;
    }

    @DexIgnore
    public void setSubName(String str) {
        this.subName = str;
    }

    @DexIgnore
    public void setThumbnail(String str) {
        this.thumbnail = str;
    }

    @DexIgnore
    public void setType(MappingSetType mappingSetType) {
        this.type = mappingSetType.getValue();
    }

    @DexIgnore
    public void setUpdateAt(long j) {
        this.updateAt = j;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.mappingList);
        parcel.writeString(this.name);
        parcel.writeString(this.localizedName);
        parcel.writeString(this.subName);
        parcel.writeString(this.localizedSubName);
        parcel.writeString(this.mappingListJsonString);
        parcel.writeInt(this.deviceFamily);
        parcel.writeLong(this.createAt);
        parcel.writeLong(this.updateAt);
        parcel.writeString(this.id);
        parcel.writeString(this.objectId);
        parcel.writeByte(this.isActive ? (byte) 1 : 0);
        parcel.writeInt(this.type);
        parcel.writeString(this.checksum);
        parcel.writeString(this.thumbnail);
    }

    @DexIgnore
    public FavoriteMappingSet clone() {
        FavoriteMappingSet favoriteMappingSet = new FavoriteMappingSet();
        favoriteMappingSet.setMappingList(this.mappingList);
        favoriteMappingSet.name = this.name;
        favoriteMappingSet.localizedName = this.localizedName;
        favoriteMappingSet.subName = this.subName;
        favoriteMappingSet.localizedSubName = this.localizedSubName;
        favoriteMappingSet.createAt = this.createAt;
        favoriteMappingSet.updateAt = this.updateAt;
        favoriteMappingSet.id = this.id;
        favoriteMappingSet.objectId = this.objectId;
        favoriteMappingSet.setDeviceFamily(getDeviceFamily());
        favoriteMappingSet.isActive = this.isActive;
        favoriteMappingSet.setType(getType());
        favoriteMappingSet.checksum = this.checksum;
        favoriteMappingSet.thumbnail = this.thumbnail;
        return favoriteMappingSet;
    }

    @DexIgnore
    public FavoriteMappingSet(Parcel parcel) {
        parcel.readTypedList(this.mappingList, Mapping.CREATOR);
        this.name = parcel.readString();
        this.localizedName = parcel.readString();
        this.subName = parcel.readString();
        this.localizedSubName = parcel.readString();
        this.mappingListJsonString = parcel.readString();
        this.deviceFamily = parcel.readInt();
        this.createAt = parcel.readLong();
        this.updateAt = parcel.readLong();
        this.id = parcel.readString();
        this.objectId = parcel.readString();
        this.isActive = parcel.readByte() != 0;
        this.type = parcel.readInt();
        this.checksum = parcel.readString();
        this.thumbnail = parcel.readString();
    }
}
