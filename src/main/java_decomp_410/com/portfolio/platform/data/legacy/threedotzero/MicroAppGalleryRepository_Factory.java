package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.h42;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepository_Factory implements Factory<MicroAppGalleryRepository> {
    @DexIgnore
    public /* final */ Provider<h42> mAppExecutorsProvider;
    @DexIgnore
    public /* final */ Provider<MicroAppGalleryDataSource> mMicroAppSettingLocalDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<MicroAppGalleryDataSource> mMicroAppSettingRemoteDataSourceProvider;

    @DexIgnore
    public MicroAppGalleryRepository_Factory(Provider<MicroAppGalleryDataSource> provider, Provider<MicroAppGalleryDataSource> provider2, Provider<h42> provider3) {
        this.mMicroAppSettingRemoteDataSourceProvider = provider;
        this.mMicroAppSettingLocalDataSourceProvider = provider2;
        this.mAppExecutorsProvider = provider3;
    }

    @DexIgnore
    public static MicroAppGalleryRepository_Factory create(Provider<MicroAppGalleryDataSource> provider, Provider<MicroAppGalleryDataSource> provider2, Provider<h42> provider3) {
        return new MicroAppGalleryRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static MicroAppGalleryRepository newMicroAppGalleryRepository(MicroAppGalleryDataSource microAppGalleryDataSource, MicroAppGalleryDataSource microAppGalleryDataSource2, h42 h42) {
        return new MicroAppGalleryRepository(microAppGalleryDataSource, microAppGalleryDataSource2, h42);
    }

    @DexIgnore
    public static MicroAppGalleryRepository provideInstance(Provider<MicroAppGalleryDataSource> provider, Provider<MicroAppGalleryDataSource> provider2, Provider<h42> provider3) {
        return new MicroAppGalleryRepository(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public MicroAppGalleryRepository get() {
        return provideInstance(this.mMicroAppSettingRemoteDataSourceProvider, this.mMicroAppSettingLocalDataSourceProvider, this.mAppExecutorsProvider);
    }
}
