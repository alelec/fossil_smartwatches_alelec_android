package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepositoryModule_ProvideMicroAppVariantLocalDataSource$app_fossilReleaseFactory */
public final class C5648x2527f7a9 implements dagger.internal.Factory<com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource> {
    @DexIgnore
    public /* final */ com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepositoryModule module;

    @DexIgnore
    public C5648x2527f7a9(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepositoryModule microAppVariantRepositoryModule) {
        this.module = microAppVariantRepositoryModule;
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.C5648x2527f7a9 create(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepositoryModule microAppVariantRepositoryModule) {
        return new com.portfolio.platform.data.legacy.threedotzero.C5648x2527f7a9(microAppVariantRepositoryModule);
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource provideInstance(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepositoryModule microAppVariantRepositoryModule) {
        return proxyProvideMicroAppVariantLocalDataSource$app_fossilRelease(microAppVariantRepositoryModule);
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource proxyProvideMicroAppVariantLocalDataSource$app_fossilRelease(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepositoryModule microAppVariantRepositoryModule) {
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource provideMicroAppVariantLocalDataSource$app_fossilRelease = microAppVariantRepositoryModule.provideMicroAppVariantLocalDataSource$app_fossilRelease();
        com.fossil.blesdk.obfuscated.n44.m25602a(provideMicroAppVariantLocalDataSource$app_fossilRelease, "Cannot return null from a non-@Nullable @Provides method");
        return provideMicroAppVariantLocalDataSource$app_fossilRelease;
    }

    @DexIgnore
    public com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource get() {
        return provideInstance(this.module);
    }
}
