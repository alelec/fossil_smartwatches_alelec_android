package com.portfolio.platform.data.legacy.threedotzero;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepository extends MicroAppGalleryDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ h42 mAppExecutors;
    @DexIgnore
    public /* final */ MicroAppGalleryDataSource mMicroAppSettingLocalDataSource;
    @DexIgnore
    public /* final */ MicroAppGalleryDataSource mMicroAppSettingRemoteDataSource;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppGalleryRepository.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            kd4.b(str, "<set-?>");
            MicroAppGalleryRepository.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppGalleryRepository.class.getSimpleName();
        kd4.a((Object) simpleName, "MicroAppGalleryRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppGalleryRepository(@Remote MicroAppGalleryDataSource microAppGalleryDataSource, @Local MicroAppGalleryDataSource microAppGalleryDataSource2, h42 h42) {
        kd4.b(microAppGalleryDataSource, "mMicroAppSettingRemoteDataSource");
        kd4.b(microAppGalleryDataSource2, "mMicroAppSettingLocalDataSource");
        kd4.b(h42, "mAppExecutors");
        this.mMicroAppSettingRemoteDataSource = microAppGalleryDataSource;
        this.mMicroAppSettingLocalDataSource = microAppGalleryDataSource2;
        this.mAppExecutors = h42;
    }

    @DexIgnore
    public final void downloadMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        kd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        String str2 = TAG;
        MFLogger.d(str2, "downloadMicroAppGallery deviceSerial=" + str);
        this.mMicroAppSettingRemoteDataSource.getMicroAppGallery(str, new MicroAppGalleryRepository$downloadMicroAppGallery$Anon1(this, str, getMicroAppGalleryCallback));
    }

    @DexIgnore
    public void getMicroApp(String str, String str2, MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback) {
        kd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        kd4.b(str2, "microAppId");
        if (TextUtils.isEmpty(str)) {
            MFLogger.d(TAG, "getMicroApp deviceSerial=empty");
            if (getMicroAppCallback != null) {
                getMicroAppCallback.onFail();
                return;
            }
            return;
        }
        this.mMicroAppSettingLocalDataSource.getMicroApp(str, str2, new MicroAppGalleryRepository$getMicroApp$Anon1(this, str2, getMicroAppCallback, str));
    }

    @DexIgnore
    public void getMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        kd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        String str2 = TAG;
        MFLogger.d(str2, "getMicroAppGallery deviceSerial=" + str);
        if (TextUtils.isEmpty(str)) {
            MFLogger.d(TAG, "getMicroAppGallery deviceSerial=empty");
            if (getMicroAppGalleryCallback != null) {
                getMicroAppGalleryCallback.onFail();
                return;
            }
            return;
        }
        this.mMicroAppSettingLocalDataSource.getMicroAppGallery(str, new MicroAppGalleryRepository$getMicroAppGallery$Anon1(str, getMicroAppGalleryCallback));
    }

    @DexIgnore
    public void updateMicroApp(MicroApp microApp, MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback) {
        kd4.b(microApp, "microApp");
        String str = TAG;
        MFLogger.d(str, "updateMicroApp microApp=" + microApp.getAppId());
        this.mMicroAppSettingLocalDataSource.updateMicroApp(microApp, getMicroAppCallback);
    }
}
