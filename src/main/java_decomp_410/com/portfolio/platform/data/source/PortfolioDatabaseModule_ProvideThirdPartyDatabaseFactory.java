package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideThirdPartyDatabaseFactory implements Factory<ThirdPartyDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideThirdPartyDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideThirdPartyDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideThirdPartyDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static ThirdPartyDatabase provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return proxyProvideThirdPartyDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static ThirdPartyDatabase proxyProvideThirdPartyDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        ThirdPartyDatabase provideThirdPartyDatabase = portfolioDatabaseModule.provideThirdPartyDatabase(portfolioApp);
        n44.a(provideThirdPartyDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideThirdPartyDatabase;
    }

    @DexIgnore
    public ThirdPartyDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
