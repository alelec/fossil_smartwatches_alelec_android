package com.portfolio.platform.data.source.local.diana.heartrate;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource$loadData$1", mo27670f = "HeartRateSummaryLocalDataSource.kt", mo27671l = {160}, mo27672m = "invokeSuspend")
public final class HeartRateSummaryLocalDataSource$loadData$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $startDate;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21117p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSummaryLocalDataSource$loadData$1(com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource, java.util.Date date, java.util.Date date2, com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a aVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = heartRateSummaryLocalDataSource;
        this.$startDate = date;
        this.$endDate = date2;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource$loadData$1 heartRateSummaryLocalDataSource$loadData$1 = new com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource$loadData$1(this.this$0, this.$startDate, this.$endDate, this.$helperCallback, yb4);
        heartRateSummaryLocalDataSource$loadData$1.f21117p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return heartRateSummaryLocalDataSource$loadData$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource$loadData$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21117p$;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource.Companion.getTAG$app_fossilRelease();
            local.mo33255d(tAG$app_fossilRelease, "loadData start=" + this.$startDate + ", end=" + this.$endDate);
            java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> fitnessData = this.this$0.mFitnessDataRepository.getFitnessData(this.$startDate, this.$endDate);
            kotlin.Pair<java.util.Date, java.util.Date> calculateRangeDownload = com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt.calculateRangeDownload(fitnessData, this.$startDate, this.$endDate);
            if (calculateRangeDownload != null) {
                this.L$0 = zg4;
                this.L$1 = fitnessData;
                this.L$2 = calculateRangeDownload;
                this.label = 1;
                obj = this.this$0.mSummariesRepository.loadSummaries(calculateRangeDownload.getFirst(), calculateRangeDownload.getSecond(), this);
                if (obj == a) {
                    return a;
                }
            } else {
                this.$helperCallback.mo39588a();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        } else if (i == 1) {
            kotlin.Pair pair = (kotlin.Pair) this.L$2;
            java.util.List list = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
        if (qo2 instanceof com.fossil.blesdk.obfuscated.ro2) {
            if (!this.this$0.mRequestAfterQueue.isEmpty()) {
                this.this$0.mRequestAfterQueue.remove(0);
            }
            this.$helperCallback.mo39588a();
        } else if (qo2 instanceof com.fossil.blesdk.obfuscated.po2) {
            com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
            if (po2.mo30179d() != null) {
                this.$helperCallback.mo39589a(po2.mo30179d());
            } else if (po2.mo30178c() != null) {
                com.portfolio.platform.data.model.ServerError c = po2.mo30178c();
                com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a aVar = this.$helperCallback;
                java.lang.String userMessage = c.getUserMessage();
                if (userMessage == null) {
                    userMessage = c.getMessage();
                }
                if (userMessage == null) {
                    userMessage = "";
                }
                aVar.mo39589a(new java.lang.Throwable(userMessage));
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
