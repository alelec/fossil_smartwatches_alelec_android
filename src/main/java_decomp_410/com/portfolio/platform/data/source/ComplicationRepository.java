package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.source.local.diana.ComplicationDao;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "ComplicationRepository";
    @DexIgnore
    public /* final */ ComplicationDao mComplicationDao;
    @DexIgnore
    public /* final */ ComplicationRemoteDataSource mComplicationRemoteDataSource;
    @DexIgnore
    public /* final */ PortfolioApp mPortfolioApp;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public ComplicationRepository(ComplicationDao complicationDao, ComplicationRemoteDataSource complicationRemoteDataSource, PortfolioApp portfolioApp) {
        kd4.b(complicationDao, "mComplicationDao");
        kd4.b(complicationRemoteDataSource, "mComplicationRemoteDataSource");
        kd4.b(portfolioApp, "mPortfolioApp");
        this.mComplicationDao = complicationDao;
        this.mComplicationRemoteDataSource = complicationRemoteDataSource;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mComplicationDao.clearAll();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object downloadAllComplication(String str, yb4<? super qa4> yb4) {
        ComplicationRepository$downloadAllComplication$Anon1 complicationRepository$downloadAllComplication$Anon1;
        int i;
        ComplicationRepository complicationRepository;
        qo2 qo2;
        if (yb4 instanceof ComplicationRepository$downloadAllComplication$Anon1) {
            complicationRepository$downloadAllComplication$Anon1 = (ComplicationRepository$downloadAllComplication$Anon1) yb4;
            int i2 = complicationRepository$downloadAllComplication$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                complicationRepository$downloadAllComplication$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = complicationRepository$downloadAllComplication$Anon1.result;
                Object a = cc4.a();
                i = complicationRepository$downloadAllComplication$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "downloadAllComplication of " + str);
                    ComplicationRemoteDataSource complicationRemoteDataSource = this.mComplicationRemoteDataSource;
                    complicationRepository$downloadAllComplication$Anon1.L$Anon0 = this;
                    complicationRepository$downloadAllComplication$Anon1.L$Anon1 = str;
                    complicationRepository$downloadAllComplication$Anon1.label = 1;
                    obj = complicationRemoteDataSource.getAllComplication(str, complicationRepository$downloadAllComplication$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    complicationRepository = this;
                } else if (i == 1) {
                    str = (String) complicationRepository$downloadAllComplication$Anon1.L$Anon1;
                    complicationRepository = (ComplicationRepository) complicationRepository$downloadAllComplication$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                Integer num = null;
                if (!(qo2 instanceof ro2)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadAllComplication of ");
                    sb.append(str);
                    sb.append(" success isFromCache ");
                    ro2 ro2 = (ro2) qo2;
                    sb.append(ro2.b());
                    sb.append(" response ");
                    sb.append((List) ro2.a());
                    local2.d(TAG, sb.toString());
                    if (!ro2.b()) {
                        Object a2 = ro2.a();
                        if (a2 == null) {
                            kd4.a();
                            throw null;
                        } else if (!((Collection) a2).isEmpty()) {
                            complicationRepository.mComplicationDao.upsertComplicationList((List) ro2.a());
                        }
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadAllComplication of ");
                    sb2.append(str);
                    sb2.append(" fail!!! error=");
                    po2 po2 = (po2) qo2;
                    sb2.append(po2.a());
                    sb2.append(" serverErrorCode=");
                    ServerError c = po2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local3.d(TAG, sb2.toString());
                }
                return qa4.a;
            }
        }
        complicationRepository$downloadAllComplication$Anon1 = new ComplicationRepository$downloadAllComplication$Anon1(this, yb4);
        Object obj2 = complicationRepository$downloadAllComplication$Anon1.result;
        Object a3 = cc4.a();
        i = complicationRepository$downloadAllComplication$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        Integer num2 = null;
        if (!(qo2 instanceof ro2)) {
        }
        return qa4.a;
    }

    @DexIgnore
    public final List<Complication> getAllComplicationRaw() {
        return this.mComplicationDao.getAllComplications();
    }

    @DexIgnore
    public final List<Complication> getComplicationByIds(List<String> list) {
        kd4.b(list, "ids");
        if (!list.isEmpty()) {
            return kb4.a(this.mComplicationDao.getComplicationByIds(list), new ComplicationRepository$getComplicationByIds$$inlined$sortedBy$Anon1(list));
        }
        return new ArrayList();
    }

    @DexIgnore
    public final List<Complication> queryComplicationByName(String str) {
        kd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        ArrayList arrayList = new ArrayList();
        for (Complication next : this.mComplicationDao.getAllComplications()) {
            String normalize = Normalizer.normalize(sm2.a(this.mPortfolioApp, next.getNameKey(), next.getName()), Normalizer.Form.NFC);
            String normalize2 = Normalizer.normalize(str, Normalizer.Form.NFC);
            kd4.a((Object) normalize, "name");
            kd4.a((Object) normalize2, "searchQuery");
            if (StringsKt__StringsKt.a((CharSequence) normalize, (CharSequence) normalize2, true)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }
}
