package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ yb4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $it;
    @DexIgnore
    public /* final */ /* synthetic */ List $serverAlarm$inlined;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1(List list, yb4 yb4, AlarmsRepository alarmsRepository, yb4 yb42, List list2) {
        super(2, yb4);
        this.$it = list;
        this.this$Anon0 = alarmsRepository;
        this.$continuation$inlined = yb42;
        this.$serverAlarm$inlined = list2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 = new AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1(this.$it, yb4, this.this$Anon0, this.$continuation$inlined, this.$serverAlarm$inlined);
        alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            this.this$Anon0.mAlarmsLocalDataSource.insertAlarms(this.$it);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
