package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.jk2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nd;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.xk2;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.FitnessDayData;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDate;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.PagingRequestHelper;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import kotlin.NoWhenBranchMatchedException;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "SummariesRepository";
    @DexIgnore
    public /* final */ ActivitySummaryDao mActivitySummaryDao;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public /* final */ xk2 mFitnessHelper;
    @DexIgnore
    public List<ActivitySummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public SummariesRepository(ApiServiceV2 apiServiceV2, ActivitySummaryDao activitySummaryDao, FitnessDataDao fitnessDataDao, xk2 xk2) {
        kd4.b(apiServiceV2, "mApiServiceV2");
        kd4.b(activitySummaryDao, "mActivitySummaryDao");
        kd4.b(fitnessDataDao, "mFitnessDataDao");
        kd4.b(xk2, "mFitnessHelper");
        this.mApiServiceV2 = apiServiceV2;
        this.mActivitySummaryDao = activitySummaryDao;
        this.mFitnessDataDao = fitnessDataDao;
        this.mFitnessHelper = xk2;
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        removePagingListener();
        this.mActivitySummaryDao.deleteAllActivitySummaries();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object downloadRecommendedGoals(int i, int i2, int i3, String str, yb4<? super qo2<ActivityRecommendedGoals>> yb4) {
        SummariesRepository$downloadRecommendedGoals$Anon1 summariesRepository$downloadRecommendedGoals$Anon1;
        int i4;
        qo2 qo2;
        yb4<? super qo2<ActivityRecommendedGoals>> yb42 = yb4;
        if (yb42 instanceof SummariesRepository$downloadRecommendedGoals$Anon1) {
            summariesRepository$downloadRecommendedGoals$Anon1 = (SummariesRepository$downloadRecommendedGoals$Anon1) yb42;
            int i5 = summariesRepository$downloadRecommendedGoals$Anon1.label;
            if ((i5 & Integer.MIN_VALUE) != 0) {
                summariesRepository$downloadRecommendedGoals$Anon1.label = i5 - Integer.MIN_VALUE;
                SummariesRepository$downloadRecommendedGoals$Anon1 summariesRepository$downloadRecommendedGoals$Anon12 = summariesRepository$downloadRecommendedGoals$Anon1;
                Object obj = summariesRepository$downloadRecommendedGoals$Anon12.result;
                Object a = cc4.a();
                i4 = summariesRepository$downloadRecommendedGoals$Anon12.label;
                if (i4 != 0) {
                    na4.a(obj);
                    SummariesRepository$downloadRecommendedGoals$response$Anon1 summariesRepository$downloadRecommendedGoals$response$Anon1 = new SummariesRepository$downloadRecommendedGoals$response$Anon1(this, i, i2, i3, str, (yb4) null);
                    summariesRepository$downloadRecommendedGoals$Anon12.L$Anon0 = this;
                    summariesRepository$downloadRecommendedGoals$Anon12.I$Anon0 = i;
                    summariesRepository$downloadRecommendedGoals$Anon12.I$Anon1 = i2;
                    summariesRepository$downloadRecommendedGoals$Anon12.I$Anon2 = i3;
                    summariesRepository$downloadRecommendedGoals$Anon12.L$Anon1 = str;
                    summariesRepository$downloadRecommendedGoals$Anon12.label = 1;
                    obj = ResponseKt.a(summariesRepository$downloadRecommendedGoals$response$Anon1, summariesRepository$downloadRecommendedGoals$Anon12);
                    if (obj == a) {
                        return a;
                    }
                } else if (i4 == 1) {
                    String str2 = (String) summariesRepository$downloadRecommendedGoals$Anon12.L$Anon1;
                    int i6 = summariesRepository$downloadRecommendedGoals$Anon12.I$Anon2;
                    int i7 = summariesRepository$downloadRecommendedGoals$Anon12.I$Anon1;
                    int i8 = summariesRepository$downloadRecommendedGoals$Anon12.I$Anon0;
                    SummariesRepository summariesRepository = (SummariesRepository) summariesRepository$downloadRecommendedGoals$Anon12.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    Object a2 = ((ro2) qo2).a();
                    if (a2 != null) {
                        return new ro2((ActivityRecommendedGoals) a2, false, 2, (fd4) null);
                    }
                    kd4.a();
                    throw null;
                } else if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), (Throwable) null, (String) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        summariesRepository$downloadRecommendedGoals$Anon1 = new SummariesRepository$downloadRecommendedGoals$Anon1(this, yb42);
        SummariesRepository$downloadRecommendedGoals$Anon1 summariesRepository$downloadRecommendedGoals$Anon122 = summariesRepository$downloadRecommendedGoals$Anon1;
        Object obj2 = summariesRepository$downloadRecommendedGoals$Anon122.result;
        Object a3 = cc4.a();
        i4 = summariesRepository$downloadRecommendedGoals$Anon122.label;
        if (i4 != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object fetchActivitySettings(yb4<? super qo2<ActivitySettings>> yb4) {
        SummariesRepository$fetchActivitySettings$Anon1 summariesRepository$fetchActivitySettings$Anon1;
        int i;
        SummariesRepository summariesRepository;
        qo2 qo2;
        if (yb4 instanceof SummariesRepository$fetchActivitySettings$Anon1) {
            summariesRepository$fetchActivitySettings$Anon1 = (SummariesRepository$fetchActivitySettings$Anon1) yb4;
            int i2 = summariesRepository$fetchActivitySettings$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                summariesRepository$fetchActivitySettings$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = summariesRepository$fetchActivitySettings$Anon1.result;
                Object a = cc4.a();
                i = summariesRepository$fetchActivitySettings$Anon1.label;
                String str = null;
                if (i != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "fetchActivitySettings");
                    SummariesRepository$fetchActivitySettings$response$Anon1 summariesRepository$fetchActivitySettings$response$Anon1 = new SummariesRepository$fetchActivitySettings$response$Anon1(this, (yb4) null);
                    summariesRepository$fetchActivitySettings$Anon1.L$Anon0 = this;
                    summariesRepository$fetchActivitySettings$Anon1.label = 1;
                    obj = ResponseKt.a(summariesRepository$fetchActivitySettings$response$Anon1, summariesRepository$fetchActivitySettings$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    summariesRepository = this;
                } else if (i == 1) {
                    summariesRepository = (SummariesRepository) summariesRepository$fetchActivitySettings$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    if (ro2.a() != null) {
                        summariesRepository.saveActivitySettingsToDB$app_fossilRelease(new Date(), (ActivitySettings) ro2.a());
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("fetchActivitySettings - Failure -- code=");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(", message=");
                    ServerError c = po2.c();
                    if (c != null) {
                        String message = c.getMessage();
                        if (message != null) {
                            str = message;
                            if (str == null) {
                                str = "";
                            }
                            sb.append(str);
                            local.e(TAG, sb.toString());
                        }
                    }
                    ServerError c2 = po2.c();
                    if (c2 != null) {
                        str = c2.getUserMessage();
                    }
                    if (str == null) {
                    }
                    sb.append(str);
                    local.e(TAG, sb.toString());
                }
                return qo2;
            }
        }
        summariesRepository$fetchActivitySettings$Anon1 = new SummariesRepository$fetchActivitySettings$Anon1(this, yb4);
        Object obj2 = summariesRepository$fetchActivitySettings$Anon1.result;
        Object a2 = cc4.a();
        i = summariesRepository$fetchActivitySettings$Anon1.label;
        String str2 = null;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
        return qo2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object fetchActivityStatistic(yb4<? super ActivityStatistic> yb4) {
        SummariesRepository$fetchActivityStatistic$Anon1 summariesRepository$fetchActivityStatistic$Anon1;
        int i;
        SummariesRepository summariesRepository;
        qo2 qo2;
        if (yb4 instanceof SummariesRepository$fetchActivityStatistic$Anon1) {
            summariesRepository$fetchActivityStatistic$Anon1 = (SummariesRepository$fetchActivityStatistic$Anon1) yb4;
            int i2 = summariesRepository$fetchActivityStatistic$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                summariesRepository$fetchActivityStatistic$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = summariesRepository$fetchActivityStatistic$Anon1.result;
                Object a = cc4.a();
                i = summariesRepository$fetchActivityStatistic$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    SummariesRepository$fetchActivityStatistic$response$Anon1 summariesRepository$fetchActivityStatistic$response$Anon1 = new SummariesRepository$fetchActivityStatistic$response$Anon1(this, (yb4) null);
                    summariesRepository$fetchActivityStatistic$Anon1.L$Anon0 = this;
                    summariesRepository$fetchActivityStatistic$Anon1.label = 1;
                    obj = ResponseKt.a(summariesRepository$fetchActivityStatistic$response$Anon1, summariesRepository$fetchActivityStatistic$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    summariesRepository = this;
                } else if (i == 1) {
                    summariesRepository = (SummariesRepository) summariesRepository$fetchActivityStatistic$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    if (ro2.a() != null) {
                        summariesRepository.mActivitySummaryDao.upsertActivityStatistic((ActivityStatistic) ro2.a());
                        return ro2.a();
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("getActivityStatisticAwait - Failure -- code=");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(", message=");
                    ServerError c = po2.c();
                    sb.append(c != null ? c.getMessage() : null);
                    local.e(TAG, sb.toString());
                }
                return null;
            }
        }
        summariesRepository$fetchActivityStatistic$Anon1 = new SummariesRepository$fetchActivityStatistic$Anon1(this, yb4);
        Object obj2 = summariesRepository$fetchActivityStatistic$Anon1.result;
        Object a2 = cc4.a();
        i = summariesRepository$fetchActivityStatistic$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
        return null;
    }

    @DexIgnore
    public final LiveData<os3<ActivitySettings>> getActivitySettings() {
        return new SummariesRepository$getActivitySettings$Anon1(this).asLiveData();
    }

    @DexIgnore
    public final LiveData<os3<ActivityStatistic>> getActivityStatistic(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getActivityStatistic - shouldFetch=" + z);
        return new SummariesRepository$getActivityStatistic$Anon1(this, z).asLiveData();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object getActivityStatisticAwait(yb4<? super ActivityStatistic> yb4) {
        SummariesRepository$getActivityStatisticAwait$Anon1 summariesRepository$getActivityStatisticAwait$Anon1;
        int i;
        if (yb4 instanceof SummariesRepository$getActivityStatisticAwait$Anon1) {
            summariesRepository$getActivityStatisticAwait$Anon1 = (SummariesRepository$getActivityStatisticAwait$Anon1) yb4;
            int i2 = summariesRepository$getActivityStatisticAwait$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                summariesRepository$getActivityStatisticAwait$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = summariesRepository$getActivityStatisticAwait$Anon1.result;
                Object a = cc4.a();
                i = summariesRepository$getActivityStatisticAwait$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "getActivityStatisticAwait");
                    ActivityStatistic activityStatistic = this.mActivitySummaryDao.getActivityStatistic();
                    if (activityStatistic != null) {
                        return activityStatistic;
                    }
                    summariesRepository$getActivityStatisticAwait$Anon1.L$Anon0 = this;
                    summariesRepository$getActivityStatisticAwait$Anon1.label = 1;
                    obj = fetchActivityStatistic(summariesRepository$getActivityStatisticAwait$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    SummariesRepository summariesRepository = (SummariesRepository) summariesRepository$getActivityStatisticAwait$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return (ActivityStatistic) obj;
            }
        }
        summariesRepository$getActivityStatisticAwait$Anon1 = new SummariesRepository$getActivityStatisticAwait$Anon1(this, yb4);
        Object obj2 = summariesRepository$getActivityStatisticAwait$Anon1.result;
        Object a2 = cc4.a();
        i = summariesRepository$getActivityStatisticAwait$Anon1.label;
        if (i != 0) {
        }
        return (ActivityStatistic) obj2;
    }

    @DexIgnore
    public final ActivitySettings getCurrentActivitySettings() {
        ActivitySettings activitySetting = this.mActivitySummaryDao.getActivitySetting();
        if (activitySetting == null) {
            return new ActivitySettings(VideoUploader.RETRY_DELAY_UNIT_MS, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 30);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getCurrentActivitySettings - " + "stepGoal=" + activitySetting.getCurrentStepGoal() + ", caloriesGoal=" + activitySetting.getCurrentCaloriesGoal() + ", " + "activeTimeGoal=" + activitySetting.getCurrentActiveTimeGoal());
        return new ActivitySettings(activitySetting.getCurrentStepGoal(), activitySetting.getCurrentCaloriesGoal(), activitySetting.getCurrentActiveTimeGoal());
    }

    @DexIgnore
    public final LiveData<os3<List<ActivitySummary>>> getSummaries(Date date, Date date2, boolean z) {
        kd4.b(date, GoalPhase.COLUMN_START_DATE);
        kd4.b(date2, GoalPhase.COLUMN_END_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getSummaries - startDate=" + date + ", endDate=" + date2);
        LiveData<os3<List<ActivitySummary>>> b = hc.b(this.mFitnessDataDao.getFitnessDataLiveData(date, date2), new SummariesRepository$getSummaries$Anon1(this, date, date2, z));
        kd4.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final Listing<ActivitySummary> getSummariesPaging(SummariesRepository summariesRepository, xk2 xk2, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, Date date, h42 h42, PagingRequestHelper.a aVar) {
        Date date2 = date;
        kd4.b(summariesRepository, "summariesRepository");
        kd4.b(xk2, "fitnessHelper");
        kd4.b(fitnessDataRepository, "fitnessDataRepository");
        kd4.b(activitySummaryDao, "activitySummaryDao");
        FitnessDatabase fitnessDatabase2 = fitnessDatabase;
        kd4.b(fitnessDatabase2, "fitnessDatabase");
        kd4.b(date2, "createdDate");
        h42 h422 = h42;
        kd4.b(h422, "appExecutors");
        PagingRequestHelper.a aVar2 = aVar;
        kd4.b(aVar2, "listener");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getSummariesPaging - createdDate=" + date2);
        ActivitySummaryLocalDataSource.Companion companion = ActivitySummaryLocalDataSource.Companion;
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        Date time = instance.getTime();
        kd4.a((Object) time, "Calendar.getInstance().time");
        Date calculateNextKey = companion.calculateNextKey(time, date2);
        Calendar instance2 = Calendar.getInstance();
        kd4.a((Object) instance2, "calendar");
        instance2.setTime(calculateNextKey);
        ActivitySummaryDataSourceFactory activitySummaryDataSourceFactory = new ActivitySummaryDataSourceFactory(summariesRepository, xk2, fitnessDataRepository, activitySummaryDao, fitnessDatabase2, date2, h422, aVar2, instance2);
        this.mSourceFactoryList.add(activitySummaryDataSourceFactory);
        qd.f.a aVar3 = new qd.f.a();
        aVar3.a(30);
        aVar3.a(false);
        aVar3.b(30);
        aVar3.c(5);
        qd.f a = aVar3.a();
        kd4.a((Object) a, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a2 = new nd(activitySummaryDataSourceFactory, a).a();
        kd4.a((Object) a2, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData<Y> b = hc.b(activitySummaryDataSourceFactory.getSourceLiveData(), SummariesRepository$getSummariesPaging$Anon1.INSTANCE);
        kd4.a((Object) b, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing<>(a2, b, new SummariesRepository$getSummariesPaging$Anon2(activitySummaryDataSourceFactory), new SummariesRepository$getSummariesPaging$Anon3(activitySummaryDataSourceFactory));
    }

    @DexIgnore
    public final LiveData<os3<ActivitySummary>> getSummary(Date date) {
        kd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getSummary - date=" + date);
        LiveData<os3<ActivitySummary>> b = hc.b(this.mFitnessDataDao.getFitnessDataLiveData(date, date), new SummariesRepository$getSummary$Anon1(this, date));
        kd4.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final void insertFromDevice(List<ActivitySummary> list) {
        kd4.b(list, "summaries");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "insertFromDevice: summaries = " + list.size());
        this.mActivitySummaryDao.insertActivitySummaries(list);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v15, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v9, resolved type: java.util.Date} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x01ac  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object loadSummaries(Date date, Date date2, yb4<? super qo2<xz1>> yb4) {
        SummariesRepository$loadSummaries$Anon1 summariesRepository$loadSummaries$Anon1;
        int i;
        SummariesRepository summariesRepository;
        qo2 qo2;
        if (yb4 instanceof SummariesRepository$loadSummaries$Anon1) {
            summariesRepository$loadSummaries$Anon1 = (SummariesRepository$loadSummaries$Anon1) yb4;
            int i2 = summariesRepository$loadSummaries$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                summariesRepository$loadSummaries$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = summariesRepository$loadSummaries$Anon1.result;
                Object a = cc4.a();
                i = summariesRepository$loadSummaries$Anon1.label;
                String str = null;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "loadSummaries - startDate=" + date + ", endDate=" + date2);
                    SummariesRepository$loadSummaries$response$Anon1 summariesRepository$loadSummaries$response$Anon1 = new SummariesRepository$loadSummaries$response$Anon1(this, date, date2, (yb4) null);
                    summariesRepository$loadSummaries$Anon1.L$Anon0 = this;
                    summariesRepository$loadSummaries$Anon1.L$Anon1 = date;
                    summariesRepository$loadSummaries$Anon1.L$Anon2 = date2;
                    summariesRepository$loadSummaries$Anon1.label = 1;
                    obj = ResponseKt.a(summariesRepository$loadSummaries$response$Anon1, summariesRepository$loadSummaries$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    summariesRepository = this;
                } else if (i == 1) {
                    date2 = summariesRepository$loadSummaries$Anon1.L$Anon2;
                    date = (Date) summariesRepository$loadSummaries$Anon1.L$Anon1;
                    summariesRepository = (SummariesRepository) summariesRepository$loadSummaries$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    if (ro2.a() != null && !ro2.b()) {
                        try {
                            ArrayList arrayList = new ArrayList();
                            rz1 rz1 = new rz1();
                            rz1.a(Date.class, new GsonConvertDate());
                            rz1.a(DateTime.class, new GsonConvertDateTime());
                            ApiResponse apiResponse = (ApiResponse) rz1.a().a(((xz1) ((ro2) qo2).a()).toString(), new SummariesRepository$loadSummaries$Anon2().getType());
                            if (apiResponse != null) {
                                List<FitnessDayData> list = apiResponse.get_items();
                                if (list != null) {
                                    for (FitnessDayData activitySummary : list) {
                                        ActivitySummary activitySummary2 = activitySummary.toActivitySummary();
                                        kd4.a((Object) activitySummary2, "it.toActivitySummary()");
                                        arrayList.add(activitySummary2);
                                    }
                                }
                            }
                            List<FitnessDataWrapper> fitnessData = summariesRepository.mFitnessDataDao.getFitnessData(date, date2);
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            local2.d(TAG, "fitnessDataSize " + fitnessData.size() + " from " + date + " to " + date2);
                            if (fitnessData.isEmpty()) {
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                local3.d(TAG, "upsert 2 list " + arrayList);
                                summariesRepository.mActivitySummaryDao.upsertActivitySummaries(arrayList);
                            }
                        } catch (Exception e) {
                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                            StringBuilder sb = new StringBuilder();
                            sb.append("loadSummaries - e=");
                            e.printStackTrace();
                            sb.append(qa4.a);
                            local4.d(TAG, sb.toString());
                        }
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("loadSummaries - Failure -- code=");
                    po2 po2 = (po2) qo2;
                    sb2.append(po2.a());
                    sb2.append(", message=");
                    ServerError c = po2.c();
                    if (c != null) {
                        String message = c.getMessage();
                        if (message != null) {
                            str = message;
                            if (str == null) {
                                str = "";
                            }
                            sb2.append(str);
                            local5.d(TAG, sb2.toString());
                        }
                    }
                    ServerError c2 = po2.c();
                    if (c2 != null) {
                        str = c2.getUserMessage();
                    }
                    if (str == null) {
                    }
                    sb2.append(str);
                    local5.d(TAG, sb2.toString());
                }
                return qo2;
            }
        }
        summariesRepository$loadSummaries$Anon1 = new SummariesRepository$loadSummaries$Anon1(this, yb4);
        Object obj2 = summariesRepository$loadSummaries$Anon1.result;
        Object a2 = cc4.a();
        i = summariesRepository$loadSummaries$Anon1.label;
        String str2 = null;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
        return qo2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object pushActivitySettingsToServer(ActivitySettings activitySettings, yb4<? super qa4> yb4) {
        SummariesRepository$pushActivitySettingsToServer$Anon1 summariesRepository$pushActivitySettingsToServer$Anon1;
        int i;
        if (yb4 instanceof SummariesRepository$pushActivitySettingsToServer$Anon1) {
            summariesRepository$pushActivitySettingsToServer$Anon1 = (SummariesRepository$pushActivitySettingsToServer$Anon1) yb4;
            int i2 = summariesRepository$pushActivitySettingsToServer$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                summariesRepository$pushActivitySettingsToServer$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = summariesRepository$pushActivitySettingsToServer$Anon1.result;
                Object a = cc4.a();
                i = summariesRepository$pushActivitySettingsToServer$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "pushActivitySettingsToServer - settings=" + activitySettings);
                    rz1 rz1 = new rz1();
                    rz1.b(new jk2());
                    JsonElement b = rz1.a().b((Object) activitySettings);
                    kd4.a((Object) b, "GsonBuilder()\n          \u2026sonTree(activitySettings)");
                    xz1 d = b.d();
                    ApiServiceV2 apiServiceV2 = this.mApiServiceV2;
                    kd4.a((Object) d, "jsonObject");
                    summariesRepository$pushActivitySettingsToServer$Anon1.L$Anon0 = this;
                    summariesRepository$pushActivitySettingsToServer$Anon1.L$Anon1 = activitySettings;
                    summariesRepository$pushActivitySettingsToServer$Anon1.L$Anon2 = d;
                    summariesRepository$pushActivitySettingsToServer$Anon1.label = 1;
                    if (apiServiceV2.updateActivitySetting(d, summariesRepository$pushActivitySettingsToServer$Anon1) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    xz1 xz1 = (xz1) summariesRepository$pushActivitySettingsToServer$Anon1.L$Anon2;
                    ActivitySettings activitySettings2 = (ActivitySettings) summariesRepository$pushActivitySettingsToServer$Anon1.L$Anon1;
                    SummariesRepository summariesRepository = (SummariesRepository) summariesRepository$pushActivitySettingsToServer$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return qa4.a;
            }
        }
        summariesRepository$pushActivitySettingsToServer$Anon1 = new SummariesRepository$pushActivitySettingsToServer$Anon1(this, yb4);
        Object obj2 = summariesRepository$pushActivitySettingsToServer$Anon1.result;
        Object a2 = cc4.a();
        i = summariesRepository$pushActivitySettingsToServer$Anon1.label;
        if (i != 0) {
        }
        return qa4.a;
    }

    @DexIgnore
    public final void removePagingListener() {
        for (ActivitySummaryDataSourceFactory localDataSource : this.mSourceFactoryList) {
            ActivitySummaryLocalDataSource localDataSource2 = localDataSource.getLocalDataSource();
            if (localDataSource2 != null) {
                localDataSource2.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }

    @DexIgnore
    public final void saveActivitySettingsToDB$app_fossilRelease(Date date, ActivitySettings activitySettings) {
        Date date2 = date;
        ActivitySettings activitySettings2 = activitySettings;
        kd4.b(date2, "date");
        kd4.b(activitySettings2, com.fossil.wearables.fsl.fitness.ActivitySettings.TABLE_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "saveActivitySettingsToDB - date=" + date2 + ", stepGoal=" + activitySettings.getCurrentStepGoal() + ", " + "caloriesGoal=" + activitySettings.getCurrentCaloriesGoal() + ", activeTimeGoal=" + activitySettings.getCurrentActiveTimeGoal());
        ActivitySummary activitySummary = this.mActivitySummaryDao.getActivitySummary(date2);
        if (activitySummary == null) {
            DateTime dateTime = new DateTime();
            TimeZone timeZone = dateTime.getZone().toTimeZone();
            int year = dateTime.getYear();
            int monthOfYear = dateTime.getMonthOfYear();
            int dayOfMonth = dateTime.getDayOfMonth();
            kd4.a((Object) timeZone, "timeZone");
            activitySummary = new ActivitySummary(year, monthOfYear, dayOfMonth, timeZone.getID(), Integer.valueOf(timeZone.inDaylightTime(dateTime.toDate()) ? timeZone.getDSTSavings() : 0), 0.0d, 0.0d, 0.0d, cb4.d(0, 0, 0), 0, 0, 0, 0, 7680, (fd4) null);
        }
        activitySummary.setStepGoal(activitySettings.getCurrentStepGoal());
        activitySummary.setCaloriesGoal(activitySettings.getCurrentCaloriesGoal());
        activitySummary.setActiveTimeGoal(activitySettings.getCurrentActiveTimeGoal());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "updateDb activitySetting " + activitySettings2);
        this.mActivitySummaryDao.upsertActivitySettings(activitySettings2);
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        local3.d(TAG, "updateDb activitySummary " + activitySummary);
        this.mActivitySummaryDao.upsertActivitySummary(activitySummary);
        ActivitySummary activitySummary2 = this.mActivitySummaryDao.getActivitySummary(date2);
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        local4.d(TAG, "after upsertDb summary " + activitySummary2);
    }

    @DexIgnore
    public final LiveData<os3<ActivitySettings>> updateActivitySettings(ActivitySettings activitySettings) {
        kd4.b(activitySettings, com.fossil.wearables.fsl.fitness.ActivitySettings.TABLE_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "updateActivitySettings - settings=" + activitySettings);
        return new SummariesRepository$updateActivitySettings$Anon1(this, activitySettings).asLiveData();
    }

    @DexIgnore
    public final void upsertRecommendGoals(ActivityRecommendedGoals activityRecommendedGoals) {
        kd4.b(activityRecommendedGoals, "recommendedGoals");
        this.mActivitySummaryDao.upsertActivityRecommendedGoals(activityRecommendedGoals);
    }

    @DexIgnore
    public final ActivitySummary getSummary(Calendar calendar) {
        kd4.b(calendar, "date");
        ActivitySummaryDao activitySummaryDao = this.mActivitySummaryDao;
        Date time = calendar.getTime();
        kd4.a((Object) time, "date.time");
        return activitySummaryDao.getActivitySummary(time);
    }
}
