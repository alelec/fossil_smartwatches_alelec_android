package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationRemoteDataSource_Factory implements Factory<ComplicationRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;

    @DexIgnore
    public ComplicationRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceV2Provider = provider;
    }

    @DexIgnore
    public static ComplicationRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new ComplicationRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static ComplicationRemoteDataSource newComplicationRemoteDataSource(ApiServiceV2 apiServiceV2) {
        return new ComplicationRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public static ComplicationRemoteDataSource provideInstance(Provider<ApiServiceV2> provider) {
        return new ComplicationRemoteDataSource(provider.get());
    }

    @DexIgnore
    public ComplicationRemoteDataSource get() {
        return provideInstance(this.mApiServiceV2Provider);
    }
}
