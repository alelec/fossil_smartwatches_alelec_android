package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.f62;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ MicroAppDao mMicroAppDao;
    @DexIgnore
    public /* final */ MicroAppRemoteDataSource mMicroAppRemoteDataSource;
    @DexIgnore
    public /* final */ PortfolioApp mPortfolioApp;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppRepository.class.getSimpleName();
        kd4.a((Object) simpleName, "MicroAppRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppRepository(MicroAppDao microAppDao, MicroAppRemoteDataSource microAppRemoteDataSource, PortfolioApp portfolioApp) {
        kd4.b(microAppDao, "mMicroAppDao");
        kd4.b(microAppRemoteDataSource, "mMicroAppRemoteDataSource");
        kd4.b(portfolioApp, "mPortfolioApp");
        this.mMicroAppDao = microAppDao;
        this.mMicroAppRemoteDataSource = microAppRemoteDataSource;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mMicroAppDao.clearAllDeclarationFileTable();
        this.mMicroAppDao.clearAllMicroAppGalleryTable();
        this.mMicroAppDao.clearAllMicroAppSettingTable();
        this.mMicroAppDao.clearAllMicroAppVariantTable();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object downloadAllMicroApp(String str, yb4<? super qo2<List<MicroApp>>> yb4) {
        MicroAppRepository$downloadAllMicroApp$Anon1 microAppRepository$downloadAllMicroApp$Anon1;
        int i;
        MicroAppRepository microAppRepository;
        qo2 qo2;
        if (yb4 instanceof MicroAppRepository$downloadAllMicroApp$Anon1) {
            microAppRepository$downloadAllMicroApp$Anon1 = (MicroAppRepository$downloadAllMicroApp$Anon1) yb4;
            int i2 = microAppRepository$downloadAllMicroApp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                microAppRepository$downloadAllMicroApp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = microAppRepository$downloadAllMicroApp$Anon1.result;
                Object a = cc4.a();
                i = microAppRepository$downloadAllMicroApp$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local.d(str2, "downloadAllMicroApp - serial=" + str);
                    MicroAppRemoteDataSource microAppRemoteDataSource = this.mMicroAppRemoteDataSource;
                    microAppRepository$downloadAllMicroApp$Anon1.L$Anon0 = this;
                    microAppRepository$downloadAllMicroApp$Anon1.L$Anon1 = str;
                    microAppRepository$downloadAllMicroApp$Anon1.label = 1;
                    obj = microAppRemoteDataSource.getAllMicroApp(str, microAppRepository$downloadAllMicroApp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    microAppRepository = this;
                } else if (i == 1) {
                    str = (String) microAppRepository$downloadAllMicroApp$Anon1.L$Anon1;
                    microAppRepository = (MicroAppRepository) microAppRepository$downloadAllMicroApp$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                Integer num = null;
                if (!(qo2 instanceof ro2)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadAllMicroApp - serial=");
                    sb.append(str);
                    sb.append(" success isFromCache ");
                    ro2 ro2 = (ro2) qo2;
                    sb.append(ro2.b());
                    local2.d(str3, sb.toString());
                    Object a2 = ro2.a();
                    if (a2 != null) {
                        List list = (List) a2;
                        if (!ro2.b() && (!list.isEmpty())) {
                            ArrayList arrayList = new ArrayList();
                            for (Object next : list) {
                                if (dc4.a(!StringsKt__StringsKt.a((CharSequence) f62.x.s(), (CharSequence) ((MicroApp) next).getId(), false, 2, (Object) null)).booleanValue()) {
                                    arrayList.add(next);
                                }
                            }
                            microAppRepository.mMicroAppDao.upsertListMicroApp(arrayList);
                        }
                        return new ro2(list, false, 2, (fd4) null);
                    }
                    kd4.a();
                    throw null;
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadAllMicroApp - serial=");
                    sb2.append(str);
                    sb2.append(" failed!!! ");
                    po2 po2 = (po2) qo2;
                    sb2.append(po2.a());
                    sb2.append(" serverError=");
                    ServerError c = po2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local3.d(str4, sb2.toString());
                    return new po2(po2.a(), po2.c(), po2.d(), (String) null, 8, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        microAppRepository$downloadAllMicroApp$Anon1 = new MicroAppRepository$downloadAllMicroApp$Anon1(this, yb4);
        Object obj2 = microAppRepository$downloadAllMicroApp$Anon1.result;
        Object a3 = cc4.a();
        i = microAppRepository$downloadAllMicroApp$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        Integer num2 = null;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object downloadMicroAppVariant(String str, String str2, String str3, yb4<? super qo2<List<MicroAppVariant>>> yb4) {
        MicroAppRepository$downloadMicroAppVariant$Anon1 microAppRepository$downloadMicroAppVariant$Anon1;
        int i;
        MicroAppRepository microAppRepository;
        qo2 qo2;
        if (yb4 instanceof MicroAppRepository$downloadMicroAppVariant$Anon1) {
            microAppRepository$downloadMicroAppVariant$Anon1 = (MicroAppRepository$downloadMicroAppVariant$Anon1) yb4;
            int i2 = microAppRepository$downloadMicroAppVariant$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                microAppRepository$downloadMicroAppVariant$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = microAppRepository$downloadMicroAppVariant$Anon1.result;
                Object a = cc4.a();
                i = microAppRepository$downloadMicroAppVariant$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local.d(str4, "downloadMicroAppVariant - serial=" + str + " major " + str2 + " minor " + str3);
                    MicroAppRemoteDataSource microAppRemoteDataSource = this.mMicroAppRemoteDataSource;
                    microAppRepository$downloadMicroAppVariant$Anon1.L$Anon0 = this;
                    microAppRepository$downloadMicroAppVariant$Anon1.L$Anon1 = str;
                    microAppRepository$downloadMicroAppVariant$Anon1.L$Anon2 = str2;
                    microAppRepository$downloadMicroAppVariant$Anon1.L$Anon3 = str3;
                    microAppRepository$downloadMicroAppVariant$Anon1.label = 1;
                    obj = microAppRemoteDataSource.getAllMicroAppVariant(str, str2, str3, microAppRepository$downloadMicroAppVariant$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    microAppRepository = this;
                } else if (i == 1) {
                    String str5 = (String) microAppRepository$downloadMicroAppVariant$Anon1.L$Anon3;
                    String str6 = (String) microAppRepository$downloadMicroAppVariant$Anon1.L$Anon2;
                    str = (String) microAppRepository$downloadMicroAppVariant$Anon1.L$Anon1;
                    microAppRepository = (MicroAppRepository) microAppRepository$downloadMicroAppVariant$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                Integer num = null;
                if (!(qo2 instanceof ro2)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str7 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadMicroAppVariant - serial=");
                    sb.append(str);
                    sb.append(" success isFromCache ");
                    ro2 ro2 = (ro2) qo2;
                    sb.append(ro2.b());
                    local2.d(str7, sb.toString());
                    Object a2 = ro2.a();
                    if (a2 != null) {
                        List<MicroAppVariant> list = (List) a2;
                        if (!ro2.b()) {
                            ArrayList arrayList = new ArrayList();
                            for (MicroAppVariant declarationFileList : list) {
                                dc4.a(arrayList.addAll(declarationFileList.getDeclarationFileList()));
                            }
                            microAppRepository.mMicroAppDao.upsertMicroAppVariantList(list);
                            microAppRepository.mMicroAppDao.upsertDeclarationFileList(arrayList);
                        }
                        return new ro2(list, false, 2, (fd4) null);
                    }
                    kd4.a();
                    throw null;
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str8 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadMicroAppVariant - serial=");
                    sb2.append(str);
                    sb2.append(" failed!!! ");
                    po2 po2 = (po2) qo2;
                    sb2.append(po2.a());
                    sb2.append(" serverError=");
                    ServerError c = po2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local3.d(str8, sb2.toString());
                    return new po2(po2.a(), po2.c(), po2.d(), (String) null, 8, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        microAppRepository$downloadMicroAppVariant$Anon1 = new MicroAppRepository$downloadMicroAppVariant$Anon1(this, yb4);
        Object obj2 = microAppRepository$downloadMicroAppVariant$Anon1.result;
        Object a3 = cc4.a();
        i = microAppRepository$downloadMicroAppVariant$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        Integer num2 = null;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    public final List<MicroApp> getAllMicroApp(String str) {
        kd4.b(str, "serialNumber");
        return this.mMicroAppDao.getListMicroApp(str);
    }

    @DexIgnore
    public final List<MicroApp> getMicroAppByIds(List<String> list, String str) {
        kd4.b(list, "ids");
        kd4.b(str, "serialNumber");
        return this.mMicroAppDao.getMicroAppByIds(list, str);
    }

    @DexIgnore
    public final MicroAppVariant getMicroAppVariant(String str, String str2, String str3, int i) {
        kd4.b(str, "serialNumber");
        kd4.b(str2, "microAppId");
        kd4.b(str3, "variantName");
        MicroAppVariant microAppVariant = this.mMicroAppDao.getMicroAppVariant(str2, str, i, str3);
        if (microAppVariant != null) {
            microAppVariant.getDeclarationFileList().addAll(this.mMicroAppDao.getDeclarationFiles(str2, str, microAppVariant.getName()));
        }
        return microAppVariant;
    }

    @DexIgnore
    public final List<MicroApp> queryMicroAppByName(String str, String str2) {
        kd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        kd4.b(str2, "serialNumber");
        ArrayList arrayList = new ArrayList();
        for (MicroApp next : this.mMicroAppDao.getListMicroApp(str2)) {
            String normalize = Normalizer.normalize(sm2.a(this.mPortfolioApp, next.getNameKey(), next.getName()), Normalizer.Form.NFC);
            String normalize2 = Normalizer.normalize(str, Normalizer.Form.NFC);
            kd4.a((Object) normalize, "name");
            kd4.a((Object) normalize2, "searchQuery");
            if (StringsKt__StringsKt.a((CharSequence) normalize, (CharSequence) normalize2, true)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final MicroAppVariant getMicroAppVariant(String str, String str2, int i) {
        kd4.b(str, "serialNumber");
        kd4.b(str2, "microAppId");
        MicroAppVariant microAppVariant = this.mMicroAppDao.getMicroAppVariant(str2, str, i);
        if (microAppVariant != null) {
            microAppVariant.getDeclarationFileList().addAll(this.mMicroAppDao.getDeclarationFiles(str2, str, microAppVariant.getName()));
        }
        return microAppVariant;
    }
}
