package com.portfolio.platform.data.source.local.hybrid.microapp;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface MicroAppLastSettingDao {
    @DexIgnore
    void cleanUp();

    @DexIgnore
    void deleteMicroAppLastSettingById(String str);

    @DexIgnore
    List<MicroAppLastSetting> getAllMicroAppLastSetting();

    @DexIgnore
    LiveData<List<MicroAppLastSetting>> getAllMicroAppLastSettingAsLiveData();

    @DexIgnore
    MicroAppLastSetting getMicroAppLastSetting(String str);

    @DexIgnore
    void upsertMicroAppLastSetting(MicroAppLastSetting microAppLastSetting);

    @DexIgnore
    void upsertMicroAppLastSettingList(List<MicroAppLastSetting> list);
}
