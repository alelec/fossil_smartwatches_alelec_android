package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationRepository$getComplicationByIds$$inlined$sortedBy$1<T> implements java.util.Comparator<T> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $ids$inlined;

    @DexIgnore
    public ComplicationRepository$getComplicationByIds$$inlined$sortedBy$1(java.util.List list) {
        this.$ids$inlined = list;
    }

    @DexIgnore
    public final int compare(T t, T t2) {
        return com.fossil.blesdk.obfuscated.wb4.m29575a(java.lang.Integer.valueOf(this.$ids$inlined.indexOf(((com.portfolio.platform.data.model.diana.Complication) t).getComplicationId())), java.lang.Integer.valueOf(this.$ids$inlined.indexOf(((com.portfolio.platform.data.model.diana.Complication) t2).getComplicationId())));
    }
}
