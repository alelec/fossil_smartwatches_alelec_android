package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getSummaries$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SummariesRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.SummariesRepository$getSummaries$1$1")
    /* renamed from: com.portfolio.platform.data.source.SummariesRepository$getSummaries$1$1 */
    public static final class C57171 extends com.portfolio.platform.util.NetworkBoundResource<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>, com.fossil.blesdk.obfuscated.xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.Pair $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.SummariesRepository$getSummaries$1 this$0;

        @DexIgnore
        public C57171(com.portfolio.platform.data.source.SummariesRepository$getSummaries$1 summariesRepository$getSummaries$1, kotlin.Pair pair) {
            this.this$0 = summariesRepository$getSummaries$1;
            this.$downloadingDate = pair;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
            if (r0 != null) goto L_0x0019;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
            if (r0 != null) goto L_0x0033;
         */
        @DexIgnore
        public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.fossil.blesdk.obfuscated.xz1>> yb4) {
            java.util.Date date;
            java.util.Date date2;
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiServiceV2$p = this.this$0.this$0.mApiServiceV2;
            kotlin.Pair pair = this.$downloadingDate;
            if (pair != null) {
                date = (java.util.Date) pair.getFirst();
            }
            date = this.this$0.$startDate;
            java.lang.String e = com.fossil.blesdk.obfuscated.rk2.m27397e(date);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            kotlin.Pair pair2 = this.$downloadingDate;
            if (pair2 != null) {
                date2 = (java.util.Date) pair2.getSecond();
            }
            date2 = this.this$0.$endDate;
            java.lang.String e2 = com.fossil.blesdk.obfuscated.rk2.m27397e(date2);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiServiceV2$p.getSummaries(e, e2, 0, 100, yb4);
        }

        @DexIgnore
        public androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>> loadFromDb() {
            if (!com.fossil.blesdk.obfuscated.rk2.m27414s(this.this$0.$endDate).booleanValue()) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "getSummaries - loadFromDb -- isNotToday - startDate=" + this.this$0.$startDate + ", endDate=" + this.this$0.$endDate);
                com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao access$getMActivitySummaryDao$p = this.this$0.this$0.mActivitySummaryDao;
                com.portfolio.platform.data.source.SummariesRepository$getSummaries$1 summariesRepository$getSummaries$1 = this.this$0;
                return access$getMActivitySummaryDao$p.getActivitySummariesLiveData(summariesRepository$getSummaries$1.$startDate, summariesRepository$getSummaries$1.$endDate);
            }
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "getSummaries - loadFromDb -- isToday");
            com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao access$getMActivitySummaryDao$p2 = this.this$0.this$0.mActivitySummaryDao;
            com.portfolio.platform.data.source.SummariesRepository$getSummaries$1 summariesRepository$getSummaries$12 = this.this$0;
            androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>> activitySummariesLiveData = access$getMActivitySummaryDao$p2.getActivitySummariesLiveData(summariesRepository$getSummaries$12.$startDate, summariesRepository$getSummaries$12.$endDate);
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.SummariesRepository$getSummaries$1$1$loadFromDb$1(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>> a = com.fossil.blesdk.obfuscated.C1935hc.m7843a(activitySummariesLiveData, new com.portfolio.platform.data.source.SummariesRepository$getSummaries$1$1$loadFromDb$2(this, activitySummariesLiveData));
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "Transformations.map(resu\u2026                        }");
            return a;
        }

        @DexIgnore
        public void onFetchFailed(java.lang.Throwable th) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "getSummaries - onFetchFailed");
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0098 A[Catch:{ Exception -> 0x00f3 }] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00cd A[Catch:{ Exception -> 0x00f3 }] */
        /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
        public void saveCallResult(com.fossil.blesdk.obfuscated.xz1 xz1) {
            java.util.Date date;
            kotlin.Pair pair;
            java.util.Date date2;
            java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> fitnessData;
            com.fossil.blesdk.obfuscated.kd4.m24411b(xz1, "item");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "getSummaries - saveCallResult -- item=" + xz1);
            try {
                java.util.ArrayList arrayList = new java.util.ArrayList();
                com.fossil.blesdk.obfuscated.rz1 rz1 = new com.fossil.blesdk.obfuscated.rz1();
                rz1.mo15797a(java.util.Date.class, new com.portfolio.platform.helper.GsonConvertDate());
                rz1.mo15797a(org.joda.time.DateTime.class, new com.portfolio.platform.helper.GsonConvertDateTime());
                com.portfolio.platform.data.source.remote.ApiResponse apiResponse = (com.portfolio.platform.data.source.remote.ApiResponse) rz1.mo15799a().mo23094a(xz1.toString(), new com.portfolio.platform.data.source.SummariesRepository$getSummaries$1$1$saveCallResult$1().getType());
                if (apiResponse != null) {
                    java.util.List<com.portfolio.platform.data.model.FitnessDayData> list = apiResponse.get_items();
                    if (list != null) {
                        for (com.portfolio.platform.data.model.FitnessDayData activitySummary : list) {
                            com.portfolio.platform.data.model.room.fitness.ActivitySummary activitySummary2 = activitySummary.toActivitySummary();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) activitySummary2, "it.toActivitySummary()");
                            arrayList.add(activitySummary2);
                        }
                    }
                }
                com.portfolio.platform.data.source.local.FitnessDataDao access$getMFitnessDataDao$p = this.this$0.this$0.mFitnessDataDao;
                kotlin.Pair pair2 = this.$downloadingDate;
                if (pair2 != null) {
                    date = (java.util.Date) pair2.getFirst();
                    if (date != null) {
                        pair = this.$downloadingDate;
                        if (pair != null) {
                            date2 = (java.util.Date) pair.getSecond();
                            if (date2 != null) {
                                fitnessData = access$getMFitnessDataDao$p.getFitnessData(date, date2);
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                local2.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "fitnessDatasSize " + fitnessData.size());
                                if (fitnessData.isEmpty()) {
                                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                    local3.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "upsert 1 list " + arrayList);
                                    this.this$0.this$0.mActivitySummaryDao.upsertActivitySummaries(arrayList);
                                    return;
                                }
                                return;
                            }
                        }
                        date2 = this.this$0.$endDate;
                        fitnessData = access$getMFitnessDataDao$p.getFitnessData(date, date2);
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local22 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        local22.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "fitnessDatasSize " + fitnessData.size());
                        if (fitnessData.isEmpty()) {
                        }
                    }
                }
                date = this.this$0.$startDate;
                pair = this.$downloadingDate;
                if (pair != null) {
                }
                date2 = this.this$0.$endDate;
                fitnessData = access$getMFitnessDataDao$p.getFitnessData(date, date2);
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local222.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "fitnessDatasSize " + fitnessData.size());
                if (fitnessData.isEmpty()) {
                }
            } catch (java.lang.Exception e) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append("getSummaries - saveCallResult -- e=");
                e.printStackTrace();
                sb.append(com.fossil.blesdk.obfuscated.qa4.f17909a);
                local4.mo33256e(com.portfolio.platform.data.source.SummariesRepository.TAG, sb.toString());
            }
        }

        @DexIgnore
        public boolean shouldFetch(java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary> list) {
            return this.this$0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public SummariesRepository$getSummaries$1(com.portfolio.platform.data.source.SummariesRepository summariesRepository, java.util.Date date, java.util.Date date2, boolean z) {
        this.this$0 = summariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public final androidx.lifecycle.LiveData<com.fossil.blesdk.obfuscated.os3<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>>> apply(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> list) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "getSummaries - startDate=" + this.$startDate + ", endDate=" + this.$endDate + " fitnessDataList=" + list.size());
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) list, "fitnessDataList");
        return new com.portfolio.platform.data.source.SummariesRepository$getSummaries$1.C57171(this, com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt.calculateRangeDownload(list, this.$startDate, this.$endDate)).asLiveData();
    }
}
