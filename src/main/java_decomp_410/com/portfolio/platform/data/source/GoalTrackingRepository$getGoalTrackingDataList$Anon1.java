package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingDataKt;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.jvm.internal.Ref$IntRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository$getGoalTrackingDataList$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends NetworkBoundResource<List<GoalTrackingData>, ApiResponse<GoalEvent>> {
        @DexIgnore
        public /* final */ /* synthetic */ Pair $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$IntRef $offset;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingRepository$getGoalTrackingDataList$Anon1 this$Anon0;

        @DexIgnore
        public Anon1(GoalTrackingRepository$getGoalTrackingDataList$Anon1 goalTrackingRepository$getGoalTrackingDataList$Anon1, Ref$IntRef ref$IntRef, int i, Pair pair) {
            this.this$Anon0 = goalTrackingRepository$getGoalTrackingDataList$Anon1;
            this.$offset = ref$IntRef;
            this.$limit = i;
            this.$downloadingDate = pair;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
            if (r0 != null) goto L_0x0019;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
            if (r0 != null) goto L_0x0033;
         */
        @DexIgnore
        public Object createCall(yb4<? super qr4<ApiResponse<GoalEvent>>> yb4) {
            Date date;
            Date date2;
            ApiServiceV2 access$getMApiServiceV2$p = this.this$Anon0.this$Anon0.mApiServiceV2;
            Pair pair = this.$downloadingDate;
            if (pair != null) {
                date = (Date) pair.getFirst();
            }
            date = this.this$Anon0.$startDate;
            String e = rk2.e(date);
            kd4.a((Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            Pair pair2 = this.$downloadingDate;
            if (pair2 != null) {
                date2 = (Date) pair2.getSecond();
            }
            date2 = this.this$Anon0.$endDate;
            String e2 = rk2.e(date2);
            kd4.a((Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiServiceV2$p.getGoalTrackingDataList(e, e2, this.$offset.element, this.$limit, yb4);
        }

        @DexIgnore
        public LiveData<List<GoalTrackingData>> loadFromDb() {
            GoalTrackingDao access$getMGoalTrackingDao$p = this.this$Anon0.this$Anon0.mGoalTrackingDao;
            GoalTrackingRepository$getGoalTrackingDataList$Anon1 goalTrackingRepository$getGoalTrackingDataList$Anon1 = this.this$Anon0;
            return access$getMGoalTrackingDao$p.getGoalTrackingDataListLiveData(goalTrackingRepository$getGoalTrackingDataList$Anon1.$startDate, goalTrackingRepository$getGoalTrackingDataList$Anon1.$endDate);
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().e(GoalTrackingRepository.Companion.getTAG(), "getGoalTrackingDataList onFetchFailed");
        }

        @DexIgnore
        public boolean processContinueFetching(ApiResponse<GoalEvent> apiResponse) {
            kd4.b(apiResponse, "item");
            Range range = apiResponse.get_range();
            if (range == null || !range.isHasNext()) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "getGoalTrackingDataList processContinueFetching hasNext");
            this.$offset.element += this.$limit;
            return true;
        }

        @DexIgnore
        public void saveCallResult(ApiResponse<GoalEvent> apiResponse) {
            kd4.b(apiResponse, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = GoalTrackingRepository.Companion.getTAG();
            local.d(tag, "getGoalTrackingDataList startDate=" + this.this$Anon0.$startDate + ", endDate=" + this.this$Anon0.$endDate + " saveCallResult onResponse: response = " + apiResponse);
            try {
                List<GoalEvent> list = apiResponse.get_items();
                ArrayList arrayList = new ArrayList(db4.a(list, 10));
                for (GoalEvent goalTrackingData : list) {
                    GoalTrackingData goalTrackingData2 = goalTrackingData.toGoalTrackingData();
                    if (goalTrackingData2 != null) {
                        arrayList.add(goalTrackingData2);
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                this.this$Anon0.this$Anon0.mGoalTrackingDao.upsertGoalTrackingDataList(kb4.d(arrayList));
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String tag2 = GoalTrackingRepository.Companion.getTAG();
                local2.e(tag2, "getGoalTrackingDataList startDate=" + this.this$Anon0.$startDate + ", endDate=" + this.this$Anon0.$endDate + " exception=" + e);
                e.printStackTrace();
            }
        }

        @DexIgnore
        public boolean shouldFetch(List<GoalTrackingData> list) {
            return this.this$Anon0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public GoalTrackingRepository$getGoalTrackingDataList$Anon1(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, boolean z) {
        this.this$Anon0 = goalTrackingRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public final LiveData<os3<List<GoalTrackingData>>> apply(List<GoalTrackingData> list) {
        kd4.a((Object) list, "pendingList");
        Pair<Date, Date> calculateRangeDownload = GoalTrackingDataKt.calculateRangeDownload(list, this.$startDate, this.$endDate);
        Ref$IntRef ref$IntRef = new Ref$IntRef();
        ref$IntRef.element = 0;
        return new Anon1(this, ref$IntRef, 300, calculateRangeDownload).asLiveData();
    }
}
