package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.yb4;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {59}, m = "downloadRecommendPresetList")
public final class DianaPresetRepository$downloadRecommendPresetList$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ DianaPresetRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaPresetRepository$downloadRecommendPresetList$Anon1(DianaPresetRepository dianaPresetRepository, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = dianaPresetRepository;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.downloadRecommendPresetList((String) null, this);
    }
}
