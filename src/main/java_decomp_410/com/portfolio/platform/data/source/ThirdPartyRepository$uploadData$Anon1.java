package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1", f = "ThirdPartyRepository.kt", l = {157, 158, 159, 160, 161, 162, 163}, m = "invokeSuspend")
public final class ThirdPartyRepository$uploadData$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository.PushPendingThirdPartyDataCallback $pushPendingThirdPartyDataCallback;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public Object L$Anon8;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1$Anon1", f = "ThirdPartyRepository.kt", l = {111}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitSampleList;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitSampleList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$gFitSampleList, this.$activeDeviceSerial, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$Anon0.this$Anon0;
                List list = this.$gFitSampleList;
                String str = this.$activeDeviceSerial;
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitSampleToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1$Anon2", f = "ThirdPartyRepository.kt", l = {118}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitActiveTimeList;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitActiveTimeList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, this.$gFitActiveTimeList, this.$activeDeviceSerial, yb4);
            anon2.p$ = (zg4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$Anon0.this$Anon0;
                List list = this.$gFitActiveTimeList;
                String str = this.$activeDeviceSerial;
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitActiveTimeToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1$Anon3", f = "ThirdPartyRepository.kt", l = {125}, m = "invokeSuspend")
    public static final class Anon3 extends SuspendLambda implements yc4<zg4, yb4<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitHeartRateList;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitHeartRateList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon3 anon3 = new Anon3(this.this$Anon0, this.$gFitHeartRateList, this.$activeDeviceSerial, yb4);
            anon3.p$ = (zg4) obj;
            return anon3;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon3) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$Anon0.this$Anon0;
                List list = this.$gFitHeartRateList;
                String str = this.$activeDeviceSerial;
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitHeartRateToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1$Anon4", f = "ThirdPartyRepository.kt", l = {132}, m = "invokeSuspend")
    public static final class Anon4 extends SuspendLambda implements yc4<zg4, yb4<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitSleepList;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon4(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitSleepList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon4 anon4 = new Anon4(this.this$Anon0, this.$gFitSleepList, this.$activeDeviceSerial, yb4);
            anon4.p$ = (zg4) obj;
            return anon4;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon4) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$Anon0.this$Anon0;
                List list = this.$gFitSleepList;
                String str = this.$activeDeviceSerial;
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitSleepDataToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1$Anon5", f = "ThirdPartyRepository.kt", l = {141}, m = "invokeSuspend")
    public static final class Anon5 extends SuspendLambda implements yc4<zg4, yb4<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitWorkoutSessionList;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon5(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitWorkoutSessionList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon5 anon5 = new Anon5(this.this$Anon0, this.$gFitWorkoutSessionList, this.$activeDeviceSerial, yb4);
            anon5.p$ = (zg4) obj;
            return anon5;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon5) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$Anon0.this$Anon0;
                List list = this.$gFitWorkoutSessionList;
                String str = this.$activeDeviceSerial;
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitWorkoutSessionToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1$Anon6", f = "ThirdPartyRepository.kt", l = {150}, m = "invokeSuspend")
    public static final class Anon6 extends SuspendLambda implements yc4<zg4, yb4<? super Object>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon6(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = thirdPartyRepository$uploadData$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon6 anon6 = new Anon6(this.this$Anon0, yb4);
            anon6.p$ = (zg4) obj;
            return anon6;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon6) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$Anon0.this$Anon0;
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = thirdPartyRepository.saveToUA(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1$Anon7", f = "ThirdPartyRepository.kt", l = {151}, m = "invokeSuspend")
    public static final class Anon7 extends SuspendLambda implements yc4<zg4, yb4<? super Object>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon7(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = thirdPartyRepository$uploadData$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon7 anon7 = new Anon7(this.this$Anon0, yb4);
            anon7.p$ = (zg4) obj;
            return anon7;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon7) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$Anon0.this$Anon0;
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = thirdPartyRepository.saveToUAOldFlow(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThirdPartyRepository$uploadData$Anon1(ThirdPartyRepository thirdPartyRepository, ThirdPartyRepository.PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = thirdPartyRepository;
        this.$pushPendingThirdPartyDataCallback = pushPendingThirdPartyDataCallback;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1 = new ThirdPartyRepository$uploadData$Anon1(this.this$Anon0, this.$pushPendingThirdPartyDataCallback, yb4);
        thirdPartyRepository$uploadData$Anon1.p$ = (zg4) obj;
        return thirdPartyRepository$uploadData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ThirdPartyRepository$uploadData$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x02d9, code lost:
        r11 = r2;
        r2 = r12;
        r19 = r6;
        r6 = r4;
        r4 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x02e3, code lost:
        if (r8 == null) goto L_0x0301;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x02e5, code lost:
        r0.L$Anon0 = r11;
        r0.L$Anon1 = r10;
        r0.L$Anon2 = r9;
        r0.L$Anon3 = r8;
        r0.L$Anon4 = r7;
        r0.L$Anon5 = r6;
        r0.L$Anon6 = r5;
        r0.L$Anon7 = r4;
        r0.L$Anon8 = r2;
        r0.label = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x02fe, code lost:
        if (r8.a(r0) != r1) goto L_0x0301;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0300, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0301, code lost:
        if (r7 == null) goto L_0x031f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0303, code lost:
        r0.L$Anon0 = r11;
        r0.L$Anon1 = r10;
        r0.L$Anon2 = r9;
        r0.L$Anon3 = r8;
        r0.L$Anon4 = r7;
        r0.L$Anon5 = r6;
        r0.L$Anon6 = r5;
        r0.L$Anon7 = r4;
        r0.L$Anon8 = r2;
        r0.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x031c, code lost:
        if (r7.a(r0) != r1) goto L_0x031f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x031e, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x031f, code lost:
        if (r6 == null) goto L_0x033d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0321, code lost:
        r0.L$Anon0 = r11;
        r0.L$Anon1 = r10;
        r0.L$Anon2 = r9;
        r0.L$Anon3 = r8;
        r0.L$Anon4 = r7;
        r0.L$Anon5 = r6;
        r0.L$Anon6 = r5;
        r0.L$Anon7 = r4;
        r0.L$Anon8 = r2;
        r0.label = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x033a, code lost:
        if (r6.a(r0) != r1) goto L_0x033d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x033c, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x033d, code lost:
        if (r5 == null) goto L_0x035b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x033f, code lost:
        r0.L$Anon0 = r11;
        r0.L$Anon1 = r10;
        r0.L$Anon2 = r9;
        r0.L$Anon3 = r8;
        r0.L$Anon4 = r7;
        r0.L$Anon5 = r6;
        r0.L$Anon6 = r5;
        r0.L$Anon7 = r4;
        r0.L$Anon8 = r2;
        r0.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0358, code lost:
        if (r5.a(r0) != r1) goto L_0x035b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x035a, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x035b, code lost:
        if (r4 == null) goto L_0x0379;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x035d, code lost:
        r0.L$Anon0 = r11;
        r0.L$Anon1 = r10;
        r0.L$Anon2 = r9;
        r0.L$Anon3 = r8;
        r0.L$Anon4 = r7;
        r0.L$Anon5 = r6;
        r0.L$Anon6 = r5;
        r0.L$Anon7 = r4;
        r0.L$Anon8 = r2;
        r0.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0376, code lost:
        if (r4.a(r0) != r1) goto L_0x0379;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0378, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0379, code lost:
        if (r2 == null) goto L_0x0397;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x037b, code lost:
        r0.L$Anon0 = r11;
        r0.L$Anon1 = r10;
        r0.L$Anon2 = r9;
        r0.L$Anon3 = r8;
        r0.L$Anon4 = r7;
        r0.L$Anon5 = r6;
        r0.L$Anon6 = r5;
        r0.L$Anon7 = r4;
        r0.L$Anon8 = r2;
        r0.label = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0394, code lost:
        if (r2.a(r0) != r1) goto L_0x0397;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0396, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0397, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "End uploadData");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x03a2, code lost:
        r1 = r0.$pushPendingThirdPartyDataCallback;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x03a4, code lost:
        if (r1 == null) goto L_0x03a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x03a6, code lost:
        r1.onComplete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x03ab, code lost:
        return com.fossil.blesdk.obfuscated.qa4.a;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        String str;
        gh4 gh4;
        gh4 gh42;
        gh4 gh43;
        gh4 gh44;
        gh4 gh45;
        gh4 gh46;
        gh4 gh47;
        gh4 gh48;
        gh4 gh49;
        gh4 gh410;
        zg4 zg42;
        gh4 gh411;
        gh4 gh412;
        gh4 gh413;
        gh4 gh414;
        gh4 gh415;
        Object a = cc4.a();
        switch (this.label) {
            case 0:
                na4.a(obj);
                zg42 = this.p$;
                FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "Start uploadData");
                str = this.this$Anon0.getMPortfolioApp().e();
                if (str.length() > 0) {
                    gh49 = null;
                    if (this.this$Anon0.mGoogleFitHelper.e()) {
                        List<GFitSample> allGFitSample = this.this$Anon0.getMThirdPartyDatabase().getGFitSampleDao().getAllGFitSample();
                        if (!allGFitSample.isEmpty()) {
                            gh412 = ag4.a(zg42, (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, allGFitSample, str, (yb4) null), 3, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitSample is empty");
                            gh412 = null;
                        }
                        List<GFitActiveTime> allGFitActiveTime = this.this$Anon0.getMThirdPartyDatabase().getGFitActiveTimeDao().getAllGFitActiveTime();
                        if (!allGFitActiveTime.isEmpty()) {
                            gh413 = ag4.a(zg42, (CoroutineContext) null, (CoroutineStart) null, new Anon2(this, allGFitActiveTime, str, (yb4) null), 3, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitActiveTime is empty");
                            gh413 = null;
                        }
                        List<GFitHeartRate> allGFitHeartRate = this.this$Anon0.getMThirdPartyDatabase().getGFitHeartRateDao().getAllGFitHeartRate();
                        if (!allGFitHeartRate.isEmpty()) {
                            gh414 = ag4.a(zg42, (CoroutineContext) null, (CoroutineStart) null, new Anon3(this, allGFitHeartRate, str, (yb4) null), 3, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitHeartRate is empty");
                            gh414 = null;
                        }
                        List<GFitSleep> allGFitSleep = this.this$Anon0.getMThirdPartyDatabase().getGFitSleepDao().getAllGFitSleep();
                        if (!allGFitSleep.isEmpty()) {
                            gh415 = ag4.a(zg42, (CoroutineContext) null, (CoroutineStart) null, new Anon4(this, allGFitSleep, str, (yb4) null), 3, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitSleep is empty");
                            gh415 = null;
                        }
                        List<GFitWorkoutSession> allGFitWorkoutSession = this.this$Anon0.getMThirdPartyDatabase().getGFitWorkoutSessionDao().getAllGFitWorkoutSession();
                        if (!allGFitWorkoutSession.isEmpty()) {
                            gh411 = ag4.a(zg42, (CoroutineContext) null, (CoroutineStart) null, new Anon5(this, allGFitWorkoutSession, str, (yb4) null), 3, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitWorkoutSession is empty");
                            gh411 = null;
                        }
                        gh4 gh416 = gh415;
                        gh4 = gh412;
                        gh410 = gh416;
                        gh4 gh417 = gh414;
                        gh42 = gh413;
                        gh43 = gh417;
                    } else {
                        FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "Google Fit is not connected");
                        gh410 = null;
                        gh43 = null;
                        gh42 = null;
                        gh4 = null;
                        gh411 = null;
                    }
                    if (this.this$Anon0.getMPortfolioApp().r().b()) {
                        zg4 zg43 = zg42;
                        gh4 a2 = ag4.a(zg43, (CoroutineContext) null, (CoroutineStart) null, new Anon6(this, (yb4) null), 3, (Object) null);
                        gh48 = ag4.a(zg43, (CoroutineContext) null, (CoroutineStart) null, new Anon7(this, (yb4) null), 3, (Object) null);
                        gh49 = a2;
                    } else {
                        FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "Not logged in to UA");
                        gh48 = null;
                    }
                    if (gh4 != null) {
                        this.L$Anon0 = zg42;
                        this.L$Anon1 = str;
                        this.L$Anon2 = gh4;
                        this.L$Anon3 = gh42;
                        this.L$Anon4 = gh43;
                        this.L$Anon5 = gh410;
                        this.L$Anon6 = gh411;
                        this.L$Anon7 = gh49;
                        this.L$Anon8 = gh48;
                        this.label = 1;
                        if (gh4.a(this) == a) {
                            return a;
                        }
                    }
                    gh45 = gh411;
                    break;
                }
                break;
            case 1:
                gh45 = (gh4) this.L$Anon6;
                gh43 = (gh4) this.L$Anon4;
                gh42 = (gh4) this.L$Anon3;
                gh4 = (gh4) this.L$Anon2;
                str = (String) this.L$Anon1;
                na4.a(obj);
                gh48 = (gh4) this.L$Anon8;
                zg42 = (zg4) this.L$Anon0;
                gh4 gh418 = (gh4) this.L$Anon5;
                gh49 = (gh4) this.L$Anon7;
                gh410 = gh418;
                break;
            case 2:
                gh47 = (gh4) this.L$Anon8;
                gh46 = (gh4) this.L$Anon7;
                gh45 = (gh4) this.L$Anon6;
                gh44 = (gh4) this.L$Anon5;
                gh43 = (gh4) this.L$Anon4;
                gh42 = (gh4) this.L$Anon3;
                gh4 = (gh4) this.L$Anon2;
                str = (String) this.L$Anon1;
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
                break;
            case 3:
                gh47 = (gh4) this.L$Anon8;
                gh46 = (gh4) this.L$Anon7;
                gh45 = (gh4) this.L$Anon6;
                gh44 = (gh4) this.L$Anon5;
                gh43 = (gh4) this.L$Anon4;
                gh42 = (gh4) this.L$Anon3;
                gh4 = (gh4) this.L$Anon2;
                str = (String) this.L$Anon1;
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
                break;
            case 4:
                gh47 = (gh4) this.L$Anon8;
                gh46 = (gh4) this.L$Anon7;
                gh45 = (gh4) this.L$Anon6;
                gh44 = (gh4) this.L$Anon5;
                gh43 = (gh4) this.L$Anon4;
                gh42 = (gh4) this.L$Anon3;
                gh4 = (gh4) this.L$Anon2;
                str = (String) this.L$Anon1;
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
                break;
            case 5:
                gh47 = (gh4) this.L$Anon8;
                gh46 = (gh4) this.L$Anon7;
                gh45 = (gh4) this.L$Anon6;
                gh44 = (gh4) this.L$Anon5;
                gh43 = (gh4) this.L$Anon4;
                gh42 = (gh4) this.L$Anon3;
                gh4 = (gh4) this.L$Anon2;
                str = (String) this.L$Anon1;
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
                break;
            case 6:
                gh47 = (gh4) this.L$Anon8;
                gh46 = (gh4) this.L$Anon7;
                gh45 = (gh4) this.L$Anon6;
                gh44 = (gh4) this.L$Anon5;
                gh43 = (gh4) this.L$Anon4;
                gh42 = (gh4) this.L$Anon3;
                gh4 = (gh4) this.L$Anon2;
                str = (String) this.L$Anon1;
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
                break;
            case 7:
                gh4 gh419 = (gh4) this.L$Anon8;
                gh4 gh420 = (gh4) this.L$Anon7;
                gh4 gh421 = (gh4) this.L$Anon6;
                gh4 gh422 = (gh4) this.L$Anon5;
                gh4 gh423 = (gh4) this.L$Anon4;
                gh4 gh424 = (gh4) this.L$Anon3;
                gh4 gh425 = (gh4) this.L$Anon2;
                String str2 = (String) this.L$Anon1;
                zg4 zg44 = (zg4) this.L$Anon0;
                na4.a(obj);
                break;
            default:
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
