package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getSummariesPaging$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {
    @DexIgnore
    public static /* final */ com.portfolio.platform.data.source.SummariesRepository$getSummariesPaging$1 INSTANCE; // = new com.portfolio.platform.data.source.SummariesRepository$getSummariesPaging$1();

    @DexIgnore
    public final androidx.lifecycle.LiveData<com.portfolio.platform.data.NetworkState> apply(com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        return activitySummaryLocalDataSource.getMNetworkState();
    }
}
