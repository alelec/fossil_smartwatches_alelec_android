package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.m3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon1;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getSummary$Anon1$Anon1$loadFromDb$Anon1<I, O> implements m3<X, Y> {
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummary$Anon1.Anon1 this$Anon0;

    @DexIgnore
    public SummariesRepository$getSummary$Anon1$Anon1$loadFromDb$Anon1(SummariesRepository$getSummary$Anon1.Anon1 anon1) {
        this.this$Anon0 = anon1;
    }

    @DexIgnore
    public final ActivitySummary apply(ActivitySummary activitySummary) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getSummary - loadFromDb -- date=" + this.this$Anon0.this$Anon0.$date + ", summary=" + activitySummary);
        if (activitySummary != null) {
            activitySummary.setSteps(Math.max((double) this.this$Anon0.this$Anon0.this$Anon0.mFitnessHelper.a(new Date()), activitySummary.getSteps()));
        }
        return activitySummary;
    }
}
