package com.portfolio.platform.data.source.local.reminders;

import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class RemindersSettingsDatabase extends RoomDatabase {
    @DexIgnore
    public abstract InactivityNudgeTimeDao getInactivityNudgeTimeDao();

    @DexIgnore
    public abstract RemindTimeDao getRemindTimeDao();
}
