package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1 */
public final class C5719x78611b51<R extends com.fossil.blesdk.obfuscated.me0> implements com.fossil.blesdk.obfuscated.ne0<com.google.android.gms.common.api.Status> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.dg4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $countSizeOfList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime $gFitActiveTime;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $gFitActiveTimeList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.ge0 $googleApiClient$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfGFitActiveTimeList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1$1")
    /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1$1 */
    public static final class C57201 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21096p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.C5719x78611b51 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1$1$1")
        /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1$1$1 */
        public static final class C57211 implements java.lang.Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.data.source.C5719x78611b51.C57201 this$0;

            @DexIgnore
            public C57211(com.portfolio.platform.data.source.C5719x78611b51.C57201 r1) {
                this.this$0 = r1;
            }

            @DexIgnore
            public final void run() {
                this.this$0.this$0.this$0.getMThirdPartyDatabase().getGFitActiveTimeDao().deleteGFitActiveTime(this.this$0.this$0.$gFitActiveTime);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C57201(com.portfolio.platform.data.source.C5719x78611b51 thirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = thirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.data.source.C5719x78611b51.C57201 r0 = new com.portfolio.platform.data.source.C5719x78611b51.C57201(this.this$0, yb4);
            r0.f21096p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.data.source.C5719x78611b51.C57201) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.getMThirdPartyDatabase().runInTransaction((java.lang.Runnable) new com.portfolio.platform.data.source.C5719x78611b51.C57201.C57211(this));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public C5719x78611b51(com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime gFitActiveTime, com.fossil.blesdk.obfuscated.ge0 ge0, kotlin.jvm.internal.Ref$IntRef ref$IntRef, int i, com.fossil.blesdk.obfuscated.dg4 dg4, com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository, java.util.List list, java.lang.String str) {
        this.$gFitActiveTime = gFitActiveTime;
        this.$googleApiClient$inlined = ge0;
        this.$countSizeOfList$inlined = ref$IntRef;
        this.$sizeOfGFitActiveTimeList$inlined = i;
        this.$continuation$inlined = dg4;
        this.this$0 = thirdPartyRepository;
        this.$gFitActiveTimeList$inlined = list;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    public final void onResult(com.google.android.gms.common.api.Status status) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(status, "status");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("Sending GFitActiveTime: Status = ");
        status.mo10091G();
        sb.append(status);
        sb.append(" - Status Message = ");
        sb.append(status.mo19651J());
        local.mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, sb.toString());
        if (status.mo19653L()) {
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.C5719x78611b51.C57201(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        }
        kotlin.jvm.internal.Ref$IntRef ref$IntRef = this.$countSizeOfList$inlined;
        ref$IntRef.element++;
        if (ref$IntRef.element >= this.$sizeOfGFitActiveTimeList$inlined && this.$continuation$inlined.isActive()) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "End saveGFitActiveTimeToGoogleFit");
            com.fossil.blesdk.obfuscated.dg4 dg4 = this.$continuation$inlined;
            kotlin.Result.C7350a aVar = kotlin.Result.Companion;
            dg4.resumeWith(kotlin.Result.m37419constructorimpl((java.lang.Object) null));
        }
    }
}
