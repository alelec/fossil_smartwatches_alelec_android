package com.portfolio.platform.data.source;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.Device;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceDao_Impl implements DeviceDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfDevice;
    @DexIgnore
    public /* final */ wf __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ wf __preparedStmtOfRemoveDeviceByDeviceId;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<Device> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `device`(`major`,`minor`,`createdAt`,`updatedAt`,`owner`,`productDisplayName`,`manufacturer`,`softwareRevision`,`hardwareRevision`,`deviceId`,`macAddress`,`sku`,`firmwareRevision`,`batteryLevel`,`vibrationStrength`,`isActive`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, Device device) {
            kgVar.b(1, (long) device.getMajor());
            kgVar.b(2, (long) device.getMinor());
            if (device.getCreatedAt() == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, device.getCreatedAt());
            }
            if (device.getUpdatedAt() == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, device.getUpdatedAt());
            }
            if (device.getOwner() == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, device.getOwner());
            }
            if (device.getProductDisplayName() == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, device.getProductDisplayName());
            }
            if (device.getManufacturer() == null) {
                kgVar.a(7);
            } else {
                kgVar.a(7, device.getManufacturer());
            }
            if (device.getSoftwareRevision() == null) {
                kgVar.a(8);
            } else {
                kgVar.a(8, device.getSoftwareRevision());
            }
            if (device.getHardwareRevision() == null) {
                kgVar.a(9);
            } else {
                kgVar.a(9, device.getHardwareRevision());
            }
            if (device.getDeviceId() == null) {
                kgVar.a(10);
            } else {
                kgVar.a(10, device.getDeviceId());
            }
            if (device.getMacAddress() == null) {
                kgVar.a(11);
            } else {
                kgVar.a(11, device.getMacAddress());
            }
            if (device.getSku() == null) {
                kgVar.a(12);
            } else {
                kgVar.a(12, device.getSku());
            }
            if (device.getFirmwareRevision() == null) {
                kgVar.a(13);
            } else {
                kgVar.a(13, device.getFirmwareRevision());
            }
            kgVar.b(14, (long) device.getBatteryLevel());
            if (device.getVibrationStrength() == null) {
                kgVar.a(15);
            } else {
                kgVar.b(15, (long) device.getVibrationStrength().intValue());
            }
            kgVar.b(16, device.isActive() ? 1 : 0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends wf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM device WHERE deviceId=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends wf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM device";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<Device>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon4(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<Device> call() throws Exception {
            Integer valueOf;
            Cursor a = bg.a(DeviceDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "major");
                int b2 = ag.b(a, "minor");
                int b3 = ag.b(a, "createdAt");
                int b4 = ag.b(a, "updatedAt");
                int b5 = ag.b(a, "owner");
                int b6 = ag.b(a, "productDisplayName");
                int b7 = ag.b(a, "manufacturer");
                int b8 = ag.b(a, "softwareRevision");
                int b9 = ag.b(a, "hardwareRevision");
                int b10 = ag.b(a, "deviceId");
                int b11 = ag.b(a, "macAddress");
                int b12 = ag.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
                int b13 = ag.b(a, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
                int b14 = ag.b(a, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
                int b15 = ag.b(a, "vibrationStrength");
                int i = b9;
                int b16 = ag.b(a, "isActive");
                int i2 = b8;
                int i3 = b7;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    String string = a.getString(b10);
                    String string2 = a.getString(b11);
                    String string3 = a.getString(b12);
                    String string4 = a.getString(b13);
                    int i4 = a.getInt(b14);
                    if (a.isNull(b15)) {
                        valueOf = null;
                    } else {
                        valueOf = Integer.valueOf(a.getInt(b15));
                    }
                    Device device = new Device(string, string2, string3, string4, i4, valueOf, a.getInt(b16) != 0);
                    int i5 = b15;
                    device.setMajor(a.getInt(b));
                    device.setMinor(a.getInt(b2));
                    device.setCreatedAt(a.getString(b3));
                    device.setUpdatedAt(a.getString(b4));
                    device.setOwner(a.getString(b5));
                    device.setProductDisplayName(a.getString(b6));
                    int i6 = i3;
                    int i7 = b;
                    device.setManufacturer(a.getString(i6));
                    int i8 = i2;
                    int i9 = i6;
                    device.setSoftwareRevision(a.getString(i8));
                    int i10 = i;
                    int i11 = i8;
                    device.setHardwareRevision(a.getString(i10));
                    arrayList.add(device);
                    b = i7;
                    i3 = i9;
                    i2 = i11;
                    i = i10;
                    b15 = i5;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 implements Callable<Device> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon5(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public Device call() throws Exception {
            Device device;
            Cursor a = bg.a(DeviceDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "major");
                int b2 = ag.b(a, "minor");
                int b3 = ag.b(a, "createdAt");
                int b4 = ag.b(a, "updatedAt");
                int b5 = ag.b(a, "owner");
                int b6 = ag.b(a, "productDisplayName");
                int b7 = ag.b(a, "manufacturer");
                int b8 = ag.b(a, "softwareRevision");
                int b9 = ag.b(a, "hardwareRevision");
                int b10 = ag.b(a, "deviceId");
                int b11 = ag.b(a, "macAddress");
                int b12 = ag.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
                int b13 = ag.b(a, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
                int b14 = ag.b(a, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
                int b15 = ag.b(a, "vibrationStrength");
                int i = b9;
                int b16 = ag.b(a, "isActive");
                Integer num = null;
                if (a.moveToFirst()) {
                    String string = a.getString(b10);
                    String string2 = a.getString(b11);
                    String string3 = a.getString(b12);
                    String string4 = a.getString(b13);
                    int i2 = a.getInt(b14);
                    if (!a.isNull(b15)) {
                        num = Integer.valueOf(a.getInt(b15));
                    }
                    device = new Device(string, string2, string3, string4, i2, num, a.getInt(b16) != 0);
                    device.setMajor(a.getInt(b));
                    device.setMinor(a.getInt(b2));
                    device.setCreatedAt(a.getString(b3));
                    device.setUpdatedAt(a.getString(b4));
                    device.setOwner(a.getString(b5));
                    device.setProductDisplayName(a.getString(b6));
                    device.setManufacturer(a.getString(b7));
                    device.setSoftwareRevision(a.getString(b8));
                    device.setHardwareRevision(a.getString(i));
                } else {
                    device = null;
                }
                return device;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public DeviceDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfDevice = new Anon1(roomDatabase);
        this.__preparedStmtOfRemoveDeviceByDeviceId = new Anon2(roomDatabase);
        this.__preparedStmtOfCleanUp = new Anon3(roomDatabase);
    }

    @DexIgnore
    public void addAllDevice(List<Device> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDevice.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void addOrUpdateDevice(Device device) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDevice.insert(device);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    public List<Device> getAllDevice() throws Throwable {
        uf ufVar;
        Integer valueOf;
        uf b = uf.b("SELECT * FROM device", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "major");
            int b3 = ag.b(a, "minor");
            int b4 = ag.b(a, "createdAt");
            int b5 = ag.b(a, "updatedAt");
            int b6 = ag.b(a, "owner");
            int b7 = ag.b(a, "productDisplayName");
            int b8 = ag.b(a, "manufacturer");
            int b9 = ag.b(a, "softwareRevision");
            int b10 = ag.b(a, "hardwareRevision");
            int b11 = ag.b(a, "deviceId");
            int b12 = ag.b(a, "macAddress");
            int b13 = ag.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int b14 = ag.b(a, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
            int b15 = ag.b(a, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
            ufVar = b;
            try {
                int b16 = ag.b(a, "vibrationStrength");
                int i = b10;
                int b17 = ag.b(a, "isActive");
                int i2 = b9;
                int i3 = b8;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    String string = a.getString(b11);
                    String string2 = a.getString(b12);
                    String string3 = a.getString(b13);
                    String string4 = a.getString(b14);
                    int i4 = a.getInt(b15);
                    if (a.isNull(b16)) {
                        valueOf = null;
                    } else {
                        valueOf = Integer.valueOf(a.getInt(b16));
                    }
                    Device device = new Device(string, string2, string3, string4, i4, valueOf, a.getInt(b17) != 0);
                    int i5 = b14;
                    device.setMajor(a.getInt(b2));
                    device.setMinor(a.getInt(b3));
                    device.setCreatedAt(a.getString(b4));
                    device.setUpdatedAt(a.getString(b5));
                    device.setOwner(a.getString(b6));
                    device.setProductDisplayName(a.getString(b7));
                    int i6 = i3;
                    int i7 = b15;
                    device.setManufacturer(a.getString(i6));
                    int i8 = i2;
                    int i9 = i6;
                    device.setSoftwareRevision(a.getString(i8));
                    int i10 = i;
                    int i11 = i8;
                    device.setHardwareRevision(a.getString(i10));
                    arrayList.add(device);
                    b15 = i7;
                    i3 = i9;
                    i2 = i11;
                    i = i10;
                    b14 = i5;
                }
                a.close();
                ufVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            ufVar = b;
            a.close();
            ufVar.c();
            throw th2;
        }
    }

    @DexIgnore
    public LiveData<List<Device>> getAllDeviceAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"device"}, false, new Anon4(uf.b("SELECT * FROM device", 0)));
    }

    @DexIgnore
    public Device getDeviceByDeviceId(String str) throws Throwable {
        uf ufVar;
        Device device;
        String str2 = str;
        uf b = uf.b("SELECT * FROM device WHERE deviceId=?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "major");
            int b3 = ag.b(a, "minor");
            int b4 = ag.b(a, "createdAt");
            int b5 = ag.b(a, "updatedAt");
            int b6 = ag.b(a, "owner");
            int b7 = ag.b(a, "productDisplayName");
            int b8 = ag.b(a, "manufacturer");
            int b9 = ag.b(a, "softwareRevision");
            int b10 = ag.b(a, "hardwareRevision");
            int b11 = ag.b(a, "deviceId");
            int b12 = ag.b(a, "macAddress");
            int b13 = ag.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int b14 = ag.b(a, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
            int b15 = ag.b(a, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
            ufVar = b;
            try {
                int b16 = ag.b(a, "vibrationStrength");
                int i = b10;
                int b17 = ag.b(a, "isActive");
                Integer num = null;
                if (a.moveToFirst()) {
                    String string = a.getString(b11);
                    String string2 = a.getString(b12);
                    String string3 = a.getString(b13);
                    String string4 = a.getString(b14);
                    int i2 = a.getInt(b15);
                    if (!a.isNull(b16)) {
                        num = Integer.valueOf(a.getInt(b16));
                    }
                    device = new Device(string, string2, string3, string4, i2, num, a.getInt(b17) != 0);
                    device.setMajor(a.getInt(b2));
                    device.setMinor(a.getInt(b3));
                    device.setCreatedAt(a.getString(b4));
                    device.setUpdatedAt(a.getString(b5));
                    device.setOwner(a.getString(b6));
                    device.setProductDisplayName(a.getString(b7));
                    device.setManufacturer(a.getString(b8));
                    device.setSoftwareRevision(a.getString(b9));
                    device.setHardwareRevision(a.getString(i));
                } else {
                    device = null;
                }
                a.close();
                ufVar.c();
                return device;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            ufVar = b;
            a.close();
            ufVar.c();
            throw th2;
        }
    }

    @DexIgnore
    public LiveData<Device> getDeviceBySerialAsLiveData(String str) {
        uf b = uf.b("SELECT * FROM device WHERE deviceId=?", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"device"}, false, new Anon5(b));
    }

    @DexIgnore
    public void removeDeviceByDeviceId(String str) {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfRemoveDeviceByDeviceId.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveDeviceByDeviceId.release(acquire);
        }
    }
}
