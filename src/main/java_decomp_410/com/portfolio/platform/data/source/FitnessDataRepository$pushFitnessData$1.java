package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.FitnessDataRepository", mo27670f = "FitnessDataRepository.kt", mo27671l = {44}, mo27672m = "pushFitnessData$app_fossilRelease")
public final class FitnessDataRepository$pushFitnessData$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public int I$0;
    @DexIgnore
    public int I$1;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.FitnessDataRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FitnessDataRepository$pushFitnessData$1(com.portfolio.platform.data.source.FitnessDataRepository fitnessDataRepository, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = fitnessDataRepository;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.pushFitnessData$app_fossilRelease((java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper>) null, this);
    }
}
