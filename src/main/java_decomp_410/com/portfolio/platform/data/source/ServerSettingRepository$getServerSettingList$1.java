package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ServerSettingRepository$getServerSettingList$1 implements com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList $callback;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ServerSettingRepository this$0;

    @DexIgnore
    public ServerSettingRepository$getServerSettingList$1(com.portfolio.platform.data.source.ServerSettingRepository serverSettingRepository, com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        this.this$0 = serverSettingRepository;
        this.$callback = onGetServerSettingList;
    }

    @DexIgnore
    public void onFailed(int i) {
        this.$callback.onFailed(i);
    }

    @DexIgnore
    public void onSuccess(com.portfolio.platform.data.model.ServerSettingList serverSettingList) {
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.ServerSettingRepository$getServerSettingList$1$onSuccess$1(this, serverSettingList, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        this.$callback.onSuccess(serverSettingList);
    }
}
