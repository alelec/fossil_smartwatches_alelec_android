package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.en2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UserRepository_Factory implements Factory<UserRepository> {
    @DexIgnore
    public /* final */ Provider<en2> mSharedPreferencesManagerProvider;
    @DexIgnore
    public /* final */ Provider<UserDataSource> mUserLocalDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<UserDataSource> mUserRemoteDataSourceProvider;

    @DexIgnore
    public UserRepository_Factory(Provider<UserDataSource> provider, Provider<UserDataSource> provider2, Provider<en2> provider3) {
        this.mUserRemoteDataSourceProvider = provider;
        this.mUserLocalDataSourceProvider = provider2;
        this.mSharedPreferencesManagerProvider = provider3;
    }

    @DexIgnore
    public static UserRepository_Factory create(Provider<UserDataSource> provider, Provider<UserDataSource> provider2, Provider<en2> provider3) {
        return new UserRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static UserRepository newUserRepository(UserDataSource userDataSource, UserDataSource userDataSource2, en2 en2) {
        return new UserRepository(userDataSource, userDataSource2, en2);
    }

    @DexIgnore
    public static UserRepository provideInstance(Provider<UserDataSource> provider, Provider<UserDataSource> provider2, Provider<en2> provider3) {
        return new UserRepository(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public UserRepository get() {
        return provideInstance(this.mUserRemoteDataSourceProvider, this.mUserLocalDataSourceProvider, this.mSharedPreferencesManagerProvider);
    }
}
