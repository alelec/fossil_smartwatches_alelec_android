package com.portfolio.platform.data.source.local;

import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class CustomizeRealDataDatabase extends RoomDatabase {
    @DexIgnore
    public abstract CustomizeRealDataDao realDataDao();
}
