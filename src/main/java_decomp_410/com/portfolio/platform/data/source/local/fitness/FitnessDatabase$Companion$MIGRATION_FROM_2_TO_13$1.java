package com.portfolio.platform.data.source.local.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FitnessDatabase$Companion$MIGRATION_FROM_2_TO_13$1 extends com.fossil.blesdk.obfuscated.C3361yf {
    @DexIgnore
    public FitnessDatabase$Companion$MIGRATION_FROM_2_TO_13$1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    public void migrate(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        java.lang.String str;
        java.lang.String str2;
        java.lang.String str3;
        com.fossil.blesdk.obfuscated.C1874gg ggVar2 = ggVar;
        com.fossil.blesdk.obfuscated.kd4.m24411b(ggVar2, "database");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migration 2 to 13 start");
        ggVar.mo11234s();
        org.joda.time.DateTime dateTime = new org.joda.time.DateTime();
        java.util.TimeZone timeZone = dateTime.getZone().toTimeZone();
        int year = dateTime.getYear();
        int monthOfYear = dateTime.getMonthOfYear();
        int dayOfMonth = dateTime.getDayOfMonth();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) timeZone, "timeZone");
        java.lang.String id = timeZone.getID();
        int dSTSavings = timeZone.inDaylightTime(dateTime.toDate()) ? timeZone.getDSTSavings() : 0;
        try {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate sample raw table");
            ggVar2.mo11230b("CREATE TABLE sampleraw_new (id TEXT PRIMARY KEY NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, sourceId TEXT NOT NULL DEFAULT '', movementTypeValue TEXT, " + "steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, sourceTypeValue TEXT NOT NULL DEFAULT '" + com.portfolio.platform.enums.FitnessSourceType.Device.getValue() + "'," + " pinType INTEGER NOT NULL DEFAULT 0, uaPinType INTEGER NOT NULL DEFAULT 0, timeZoneID TEXT, activeTime INTEGER NOT NULL DEFAULT 0, " + "intensityDistInSteps TEXT NOT NULL DEFAULT '')");
            ggVar2.mo11230b("INSERT INTO sampleraw_new (id, startTime, endTime, sourceId, movementTypeValue, steps, calories, distance, sourceTypeValue, timeZoneID) SELECT id, substr(startTime, 1, 23), substr(endTime, 1, 23), sourceId, movementTypeValue, steps, calories, distance, sourceTypeValue, timeZoneID  FROM sampleraw");
            ggVar2.mo11230b("DROP TABLE sampleraw");
            ggVar2.mo11230b("ALTER TABLE sampleraw_new RENAME TO sampleraw");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate sample raw table success");
            str2 = ", 0, 1)";
            str = ", 0.0, 0.0, 0.0, '[0,0,0]', 5000, 140, 30, ";
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            str2 = ", 0, 1)";
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            str = ", 0.0, 0.0, 0.0, '[0,0,0]', 5000, 140, 30, ";
            sb.append("Migrate sample raw table fail ");
            sb.append(e);
            local.mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, sb.toString());
            ggVar2.mo11230b("DROP TABLE IF EXISTS sampleraw_new");
            ggVar2.mo11230b("DROP TABLE IF EXISTS sampleraw");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS sampleraw (id TEXT PRIMARY KEY NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, sourceId TEXT NOT NULL DEFAULT '', movementTypeValue TEXT NOT NULL DEFAULT '" + com.portfolio.platform.enums.FitnessMovementType.WALKING.getValue() + "', steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, sourceTypeValue TEXT NOT NULL DEFAULT '" + com.portfolio.platform.enums.FitnessSourceType.Device.getValue() + "', pinType INTEGER NOT NULL DEFAULT 1, uaPinType INTEGER NOT NULL DEFAULT 1, timeZoneID TEXT, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '')");
        }
        try {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate intensity data for sampleraw");
            ggVar2.mo11230b("CREATE TABLE intensities (min INTEGER NOT NULL, max INTEGER NOT NULL, intensity TEXT NOT NULL DEFAULT '')");
            ggVar2.mo11230b("INSERT INTO intensities (min, max, intensity) VALUES (0, 70, 'light')");
            ggVar2.mo11230b("INSERT INTO intensities (min, max, intensity) VALUES (70, 140, 'moderate')");
            ggVar2.mo11230b("INSERT INTO intensities (min, max, intensity) VALUES (140, 2147483647, 'intense')");
            ggVar2.mo11230b("UPDATE sampleraw SET intensityDistInSteps=( SELECT '{\"' || intensity || '\":' || sampleraw.steps || '}' FROM intensities WHERE sampleraw.steps >= intensities.min AND sampleraw.steps < intensities.max)");
            ggVar2.mo11230b("DROP TABLE intensities");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate intensity data for sampleraw success");
        } catch (java.lang.Exception e2) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local2.mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate intensity data for sampleraw " + e2);
            ggVar2.mo11230b("DROP TABLE IF EXISTS intensities");
        }
        try {
            ggVar2.mo11230b("CREATE TABLE sampleday_new (year INTEGER NOT NULL, month INTEGER NOT NULL, day INTEGER NOT NULL, timezoneName TEXT, dstOffset INTEGER, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, intensities TEXT NOT NULL DEFAULT '', stepGoal INTEGER NOT NULL DEFAULT 0, caloriesGoal INTEGER NOT NULL DEFAULT 0, activeTimeGoal INTEGER NOT NULL DEFAULT 0, createdAt INTEGER, updatedAt INTEGER, activeTime INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 0, PRIMARY KEY (year, month, day))");
            ggVar2.mo11230b("INSERT INTO sampleday_new (year, month, day, timezoneName, dstOffset, steps, calories, distance, createdAt, updatedAt) SELECT year, month, day, timezoneName, dstOffset, steps, calories, distance, createdAt, updatedAt FROM sampleday");
            ggVar2.mo11230b("DROP TABLE sampleday");
            ggVar2.mo11230b("ALTER TABLE sampleday_new RENAME TO sampleday");
            java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
            sb2.append("INSERT OR IGNORE INTO sampleday (year, month, day, timezoneName, dstOffset, steps, calories, distance, intensities, stepGoal, caloriesGoal, activeTimeGoal, createdAt, updatedAt, activeTime, pinType)");
            sb2.append(" VALUES(");
            sb2.append(year);
            sb2.append(", ");
            sb2.append(monthOfYear);
            sb2.append(", ");
            sb2.append(dayOfMonth);
            sb2.append(", '");
            sb2.append(id);
            sb2.append("', ");
            sb2.append(dSTSavings);
            java.lang.String str4 = str;
            try {
                sb2.append(str4);
                str = str4;
                sb2.append(java.lang.System.currentTimeMillis());
                sb2.append(", ");
                sb2.append(java.lang.System.currentTimeMillis());
                str3 = str2;
                try {
                    sb2.append(str3);
                    ggVar2.mo11230b(sb2.toString());
                    ggVar2.mo11230b("UPDATE sampleday SET stepGoal=(SELECT steps FROM dailygoal) WHERE sampleday.year = year AND sampleday.month = month AND sampleday.day = day");
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate sample day success");
                } catch (java.lang.Exception e3) {
                    e = e3;
                }
            } catch (java.lang.Exception e4) {
                e = e4;
                str = str4;
                str3 = str2;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local3.mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate sample day fail " + e);
                ggVar2.mo11230b("DROP TABLE IF EXISTS sampleday_new");
                ggVar2.mo11230b("DROP TABLE IF EXISTS sampleday");
                ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS sampleday (year INTEGER NOT NULL, month INTEGER NOT NULL, day INTEGER NOT NULL, timezoneName TEXT, dstOffset INTEGER, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, intensities TEXT NOT NULL DEFAULT '', stepGoal INTEGER NOT NULL DEFAULT 0, caloriesGoal INTEGER NOT NULL DEFAULT 0, activeTimeGoal INTEGER NOT NULL DEFAULT 0, createdAt INTEGER, updatedAt INTEGER, activeTime INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 1, PRIMARY KEY (year, month, day))");
                ggVar2.mo11230b("INSERT OR IGNORE INTO sampleday (year, month, day, timezoneName, dstOffset, steps, calories, distance, intensities, stepGoal, caloriesGoal, activeTimeGoal, createdAt, updatedAt, activeTime, pinType)" + " VALUES(" + year + ", " + monthOfYear + ", " + dayOfMonth + ", '" + id + "', " + dSTSavings + str + java.lang.System.currentTimeMillis() + ", " + java.lang.System.currentTimeMillis() + str3);
                ggVar2.mo11230b("CREATE TABLE activitySettings (currentStepGoal INTEGER NOT NULL DEFAULT 5000, currentCaloriesGoal INTEGER NOT NULL DEFAULT 140, currentActiveTimeGoal INTEGER NOT NULL DEFAULT 30, id INTEGER PRIMARY KEY ASC NOT NULL)");
                ggVar2.mo11230b("INSERT INTO activitySettings (currentStepGoal) SELECT steps FROM dailygoal WHERE dailygoal.year=" + year + " AND dailygoal.month=" + monthOfYear + " AND dailygoal.day=" + dayOfMonth);
                ggVar2.mo11230b("DROP TABLE dailygoal");
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migration activitySetting success");
                ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS workout_session (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, deviceSerialNumber TEXT, step TEXT, calorie TEXT, distance TEXT, heartRate TEXT, speed TEXT, location TEXT, states TEXT NOT NULL DEFAULT '', sourceType TEXT, workoutType TEXT, timezoneOffset INTEGER NOT NULL, duration INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
                ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS fitness_data (startTime TEXT PRIMARY KEY NOT NULL, endTime TEXT NOT NULL, syncTime TEXT NOT NULL, timezoneOffsetInSecond INTEGER NOT NULL, serialNumber TEXT NOT NULL DEFAULT '', step TEXT NOT NULL, activeMinute TEXT NOT NULL, calorie TEXT NOT NULL, distance TEXT NOT NULL, stress TEXT, resting TEXT, heartRate TEXT, sleeps TEXT NOT NULL DEFAULT '', workouts TEXT NOT NULL DEFAULT '')");
                ggVar2.mo11230b("CREATE TABLE activityRecommendedGoals (recommendedStepsGoal INTEGER NOT NULL, recommendedCaloriesGoal INTEGER NOT NULL, recommendedActiveTimeGoal INTEGER NOT NULL, id INTEGER PRIMARY KEY ASC NOT NULL)");
                ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS heart_rate_sample (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, average REAL NOT NULL, timezoneOffset INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
                ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS daily_heart_rate_summary (average REAL NOT NULL, date TEXT PRIMARY KEY NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT)");
                ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS activity_statistic (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, activeTimeBestDay TEXT, activeTimeBestStreak TEXT, caloriesBestDay TEXT, caloriesBestStreak TEXT, stepsBestDay TEXT, stepsBestStreak TEXT, totalActiveTime INTEGER NOT NULL, totalCalories REAL NOT NULL, totalDays INTEGER NOT NULL, totalDistance REAL NOT NULL, totalSteps INTEGER NOT NULL, totalIntensityDistInStep TEXT NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
                ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS activity_sample (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '', timeZoneOffsetInSecond INTEGER NOT NULL, sourceId TEXT NOT NULL DEFAULT '', syncTime INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
                ggVar.mo11236u();
                ggVar.mo11237v();
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "MIGRATION_FROM_4_TO_13 done, start schedule push pending data from 3.0");
                com.portfolio.platform.workers.PushPendingDataWorker.f25690y.mo42934a();
            }
        } catch (java.lang.Exception e5) {
            e = e5;
            str3 = str2;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local32 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local32.mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate sample day fail " + e);
            ggVar2.mo11230b("DROP TABLE IF EXISTS sampleday_new");
            ggVar2.mo11230b("DROP TABLE IF EXISTS sampleday");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS sampleday (year INTEGER NOT NULL, month INTEGER NOT NULL, day INTEGER NOT NULL, timezoneName TEXT, dstOffset INTEGER, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, intensities TEXT NOT NULL DEFAULT '', stepGoal INTEGER NOT NULL DEFAULT 0, caloriesGoal INTEGER NOT NULL DEFAULT 0, activeTimeGoal INTEGER NOT NULL DEFAULT 0, createdAt INTEGER, updatedAt INTEGER, activeTime INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 1, PRIMARY KEY (year, month, day))");
            ggVar2.mo11230b("INSERT OR IGNORE INTO sampleday (year, month, day, timezoneName, dstOffset, steps, calories, distance, intensities, stepGoal, caloriesGoal, activeTimeGoal, createdAt, updatedAt, activeTime, pinType)" + " VALUES(" + year + ", " + monthOfYear + ", " + dayOfMonth + ", '" + id + "', " + dSTSavings + str + java.lang.System.currentTimeMillis() + ", " + java.lang.System.currentTimeMillis() + str3);
            ggVar2.mo11230b("CREATE TABLE activitySettings (currentStepGoal INTEGER NOT NULL DEFAULT 5000, currentCaloriesGoal INTEGER NOT NULL DEFAULT 140, currentActiveTimeGoal INTEGER NOT NULL DEFAULT 30, id INTEGER PRIMARY KEY ASC NOT NULL)");
            ggVar2.mo11230b("INSERT INTO activitySettings (currentStepGoal) SELECT steps FROM dailygoal WHERE dailygoal.year=" + year + " AND dailygoal.month=" + monthOfYear + " AND dailygoal.day=" + dayOfMonth);
            ggVar2.mo11230b("DROP TABLE dailygoal");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migration activitySetting success");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS workout_session (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, deviceSerialNumber TEXT, step TEXT, calorie TEXT, distance TEXT, heartRate TEXT, speed TEXT, location TEXT, states TEXT NOT NULL DEFAULT '', sourceType TEXT, workoutType TEXT, timezoneOffset INTEGER NOT NULL, duration INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS fitness_data (startTime TEXT PRIMARY KEY NOT NULL, endTime TEXT NOT NULL, syncTime TEXT NOT NULL, timezoneOffsetInSecond INTEGER NOT NULL, serialNumber TEXT NOT NULL DEFAULT '', step TEXT NOT NULL, activeMinute TEXT NOT NULL, calorie TEXT NOT NULL, distance TEXT NOT NULL, stress TEXT, resting TEXT, heartRate TEXT, sleeps TEXT NOT NULL DEFAULT '', workouts TEXT NOT NULL DEFAULT '')");
            ggVar2.mo11230b("CREATE TABLE activityRecommendedGoals (recommendedStepsGoal INTEGER NOT NULL, recommendedCaloriesGoal INTEGER NOT NULL, recommendedActiveTimeGoal INTEGER NOT NULL, id INTEGER PRIMARY KEY ASC NOT NULL)");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS heart_rate_sample (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, average REAL NOT NULL, timezoneOffset INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS daily_heart_rate_summary (average REAL NOT NULL, date TEXT PRIMARY KEY NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT)");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS activity_statistic (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, activeTimeBestDay TEXT, activeTimeBestStreak TEXT, caloriesBestDay TEXT, caloriesBestStreak TEXT, stepsBestDay TEXT, stepsBestStreak TEXT, totalActiveTime INTEGER NOT NULL, totalCalories REAL NOT NULL, totalDays INTEGER NOT NULL, totalDistance REAL NOT NULL, totalSteps INTEGER NOT NULL, totalIntensityDistInStep TEXT NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS activity_sample (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '', timeZoneOffsetInSecond INTEGER NOT NULL, sourceId TEXT NOT NULL DEFAULT '', syncTime INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
            ggVar.mo11236u();
            ggVar.mo11237v();
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "MIGRATION_FROM_4_TO_13 done, start schedule push pending data from 3.0");
            com.portfolio.platform.workers.PushPendingDataWorker.f25690y.mo42934a();
        }
        try {
            ggVar2.mo11230b("CREATE TABLE activitySettings (currentStepGoal INTEGER NOT NULL DEFAULT 5000, currentCaloriesGoal INTEGER NOT NULL DEFAULT 140, currentActiveTimeGoal INTEGER NOT NULL DEFAULT 30, id INTEGER PRIMARY KEY ASC NOT NULL)");
            ggVar2.mo11230b("INSERT INTO activitySettings (currentStepGoal) SELECT steps FROM dailygoal WHERE dailygoal.year=" + year + " AND dailygoal.month=" + monthOfYear + " AND dailygoal.day=" + dayOfMonth);
            ggVar2.mo11230b("DROP TABLE dailygoal");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migration activitySetting success");
        } catch (java.lang.Exception e6) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local4.mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migration activitySetting fail " + e6);
            ggVar2.mo11230b("DROP TABLE IF EXISTS dailygoal");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS activitySettings (currentStepGoal INTEGER NOT NULL DEFAULT 5000, currentCaloriesGoal INTEGER NOT NULL DEFAULT 140, currentActiveTimeGoal INTEGER NOT NULL DEFAULT 30, id INTEGER PRIMARY KEY ASC NOT NULL)");
        }
        ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS workout_session (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, deviceSerialNumber TEXT, step TEXT, calorie TEXT, distance TEXT, heartRate TEXT, speed TEXT, location TEXT, states TEXT NOT NULL DEFAULT '', sourceType TEXT, workoutType TEXT, timezoneOffset INTEGER NOT NULL, duration INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
        ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS fitness_data (startTime TEXT PRIMARY KEY NOT NULL, endTime TEXT NOT NULL, syncTime TEXT NOT NULL, timezoneOffsetInSecond INTEGER NOT NULL, serialNumber TEXT NOT NULL DEFAULT '', step TEXT NOT NULL, activeMinute TEXT NOT NULL, calorie TEXT NOT NULL, distance TEXT NOT NULL, stress TEXT, resting TEXT, heartRate TEXT, sleeps TEXT NOT NULL DEFAULT '', workouts TEXT NOT NULL DEFAULT '')");
        ggVar2.mo11230b("CREATE TABLE activityRecommendedGoals (recommendedStepsGoal INTEGER NOT NULL, recommendedCaloriesGoal INTEGER NOT NULL, recommendedActiveTimeGoal INTEGER NOT NULL, id INTEGER PRIMARY KEY ASC NOT NULL)");
        ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS heart_rate_sample (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, average REAL NOT NULL, timezoneOffset INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
        ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS daily_heart_rate_summary (average REAL NOT NULL, date TEXT PRIMARY KEY NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT)");
        try {
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS activity_statistic (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, activeTimeBestDay TEXT, activeTimeBestStreak TEXT, caloriesBestDay TEXT, caloriesBestStreak TEXT, stepsBestDay TEXT, stepsBestStreak TEXT, totalActiveTime INTEGER NOT NULL, totalCalories REAL NOT NULL, totalDays INTEGER NOT NULL, totalDistance REAL NOT NULL, totalSteps INTEGER NOT NULL, totalIntensityDistInStep TEXT NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS activity_sample (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '', timeZoneOffsetInSecond INTEGER NOT NULL, sourceId TEXT NOT NULL DEFAULT '', syncTime INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
        } catch (java.lang.Exception e7) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local5.mo33256e(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "MIGRATION_FROM_4_TO_13 - ActivityStatistic -- e=" + e7);
            e7.printStackTrace();
        }
        ggVar.mo11236u();
        ggVar.mo11237v();
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "MIGRATION_FROM_4_TO_13 done, start schedule push pending data from 3.0");
        com.portfolio.platform.workers.PushPendingDataWorker.f25690y.mo42934a();
    }
}
