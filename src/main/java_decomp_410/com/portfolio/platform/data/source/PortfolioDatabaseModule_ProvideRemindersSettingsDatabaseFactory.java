package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory implements Factory<RemindersSettingsDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static RemindersSettingsDatabase provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return proxyProvideRemindersSettingsDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static RemindersSettingsDatabase proxyProvideRemindersSettingsDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        RemindersSettingsDatabase provideRemindersSettingsDatabase = portfolioDatabaseModule.provideRemindersSettingsDatabase(portfolioApp);
        n44.a(provideRemindersSettingsDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideRemindersSettingsDatabase;
    }

    @DexIgnore
    public RemindersSettingsDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
