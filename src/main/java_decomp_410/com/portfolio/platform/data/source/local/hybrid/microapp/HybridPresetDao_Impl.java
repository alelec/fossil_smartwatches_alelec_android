package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.k72;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridPresetDao_Impl implements HybridPresetDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ k72 __hybridAppSettingTypeConverter; // = new k72();
    @DexIgnore
    public /* final */ lf __insertionAdapterOfHybridPreset;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfHybridRecommendPreset;
    @DexIgnore
    public /* final */ wf __preparedStmtOfClearAllPresetTable;
    @DexIgnore
    public /* final */ wf __preparedStmtOfClearAllRecommendPresetTable;
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeletePreset;
    @DexIgnore
    public /* final */ wf __preparedStmtOfRemoveAllDeletePinTypePreset;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<HybridRecommendPreset> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `hybridRecommendPreset`(`id`,`name`,`serialNumber`,`buttons`,`isDefault`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, HybridRecommendPreset hybridRecommendPreset) {
            if (hybridRecommendPreset.getId() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, hybridRecommendPreset.getId());
            }
            if (hybridRecommendPreset.getName() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, hybridRecommendPreset.getName());
            }
            if (hybridRecommendPreset.getSerialNumber() == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, hybridRecommendPreset.getSerialNumber());
            }
            String a = HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.a(hybridRecommendPreset.getButtons());
            if (a == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, a);
            }
            kgVar.b(5, hybridRecommendPreset.isDefault() ? 1 : 0);
            if (hybridRecommendPreset.getCreatedAt() == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, hybridRecommendPreset.getCreatedAt());
            }
            if (hybridRecommendPreset.getUpdatedAt() == null) {
                kgVar.a(7);
            } else {
                kgVar.a(7, hybridRecommendPreset.getUpdatedAt());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends lf<HybridPreset> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `hybridPreset`(`pinType`,`createdAt`,`updatedAt`,`id`,`name`,`serialNumber`,`buttons`,`isActive`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, HybridPreset hybridPreset) {
            kgVar.b(1, (long) hybridPreset.getPinType());
            if (hybridPreset.getCreatedAt() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, hybridPreset.getCreatedAt());
            }
            if (hybridPreset.getUpdatedAt() == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, hybridPreset.getUpdatedAt());
            }
            if (hybridPreset.getId() == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, hybridPreset.getId());
            }
            if (hybridPreset.getName() == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, hybridPreset.getName());
            }
            if (hybridPreset.getSerialNumber() == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, hybridPreset.getSerialNumber());
            }
            String a = HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.a(hybridPreset.getButtons());
            if (a == null) {
                kgVar.a(7);
            } else {
                kgVar.a(7, a);
            }
            kgVar.b(8, hybridPreset.isActive() ? 1 : 0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends wf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM hybridPreset";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends wf {
        @DexIgnore
        public Anon4(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM hybridRecommendPreset";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends wf {
        @DexIgnore
        public Anon5(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM hybridPreset WHERE id = ?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends wf {
        @DexIgnore
        public Anon6(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM hybridPreset WHERE pinType = 3";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements Callable<List<HybridPreset>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon7(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<HybridPreset> call() throws Exception {
            Cursor a = bg.a(HybridPresetDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "pinType");
                int b2 = ag.b(a, "createdAt");
                int b3 = ag.b(a, "updatedAt");
                int b4 = ag.b(a, "id");
                int b5 = ag.b(a, "name");
                int b6 = ag.b(a, "serialNumber");
                int b7 = ag.b(a, "buttons");
                int b8 = ag.b(a, "isActive");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    HybridPreset hybridPreset = new HybridPreset(a.getString(b4), a.getString(b5), a.getString(b6), HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.a(a.getString(b7)), a.getInt(b8) != 0);
                    hybridPreset.setPinType(a.getInt(b));
                    hybridPreset.setCreatedAt(a.getString(b2));
                    hybridPreset.setUpdatedAt(a.getString(b3));
                    arrayList.add(hybridPreset);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public HybridPresetDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfHybridRecommendPreset = new Anon1(roomDatabase);
        this.__insertionAdapterOfHybridPreset = new Anon2(roomDatabase);
        this.__preparedStmtOfClearAllPresetTable = new Anon3(roomDatabase);
        this.__preparedStmtOfClearAllRecommendPresetTable = new Anon4(roomDatabase);
        this.__preparedStmtOfDeletePreset = new Anon5(roomDatabase);
        this.__preparedStmtOfRemoveAllDeletePinTypePreset = new Anon6(roomDatabase);
    }

    @DexIgnore
    public void clearAllPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfClearAllPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllPresetTable.release(acquire);
        }
    }

    @DexIgnore
    public void clearAllRecommendPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfClearAllRecommendPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllRecommendPresetTable.release(acquire);
        }
    }

    @DexIgnore
    public void deletePreset(String str) {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeletePreset.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeletePreset.release(acquire);
        }
    }

    @DexIgnore
    public HybridPreset getActivePresetBySerial(String str) {
        HybridPreset hybridPreset;
        String str2 = str;
        uf b = uf.b("SELECT * FROM hybridPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "createdAt");
            int b4 = ag.b(a, "updatedAt");
            int b5 = ag.b(a, "id");
            int b6 = ag.b(a, "name");
            int b7 = ag.b(a, "serialNumber");
            int b8 = ag.b(a, "buttons");
            int b9 = ag.b(a, "isActive");
            if (a.moveToFirst()) {
                hybridPreset = new HybridPreset(a.getString(b5), a.getString(b6), a.getString(b7), this.__hybridAppSettingTypeConverter.a(a.getString(b8)), a.getInt(b9) != 0);
                hybridPreset.setPinType(a.getInt(b2));
                hybridPreset.setCreatedAt(a.getString(b3));
                hybridPreset.setUpdatedAt(a.getString(b4));
            } else {
                hybridPreset = null;
            }
            return hybridPreset;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<HybridPreset> getAllPendingPreset(String str) {
        String str2 = str;
        uf b = uf.b("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 0", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "createdAt");
            int b4 = ag.b(a, "updatedAt");
            int b5 = ag.b(a, "id");
            int b6 = ag.b(a, "name");
            int b7 = ag.b(a, "serialNumber");
            int b8 = ag.b(a, "buttons");
            int b9 = ag.b(a, "isActive");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                HybridPreset hybridPreset = new HybridPreset(a.getString(b5), a.getString(b6), a.getString(b7), this.__hybridAppSettingTypeConverter.a(a.getString(b8)), a.getInt(b9) != 0);
                hybridPreset.setPinType(a.getInt(b2));
                hybridPreset.setCreatedAt(a.getString(b3));
                hybridPreset.setUpdatedAt(a.getString(b4));
                arrayList.add(hybridPreset);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<HybridPreset> getAllPreset(String str) {
        String str2 = str;
        uf b = uf.b("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC ", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "createdAt");
            int b4 = ag.b(a, "updatedAt");
            int b5 = ag.b(a, "id");
            int b6 = ag.b(a, "name");
            int b7 = ag.b(a, "serialNumber");
            int b8 = ag.b(a, "buttons");
            int b9 = ag.b(a, "isActive");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                HybridPreset hybridPreset = new HybridPreset(a.getString(b5), a.getString(b6), a.getString(b7), this.__hybridAppSettingTypeConverter.a(a.getString(b8)), a.getInt(b9) != 0);
                hybridPreset.setPinType(a.getInt(b2));
                hybridPreset.setCreatedAt(a.getString(b3));
                hybridPreset.setUpdatedAt(a.getString(b4));
                arrayList.add(hybridPreset);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<HybridPreset>> getAllPresetAsLiveData(String str) {
        uf b = uf.b("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"hybridPreset"}, false, new Anon7(b));
    }

    @DexIgnore
    public HybridPreset getPresetById(String str) {
        HybridPreset hybridPreset;
        String str2 = str;
        uf b = uf.b("SELECT * FROM hybridPreset WHERE id=? AND pinType != 3", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "createdAt");
            int b4 = ag.b(a, "updatedAt");
            int b5 = ag.b(a, "id");
            int b6 = ag.b(a, "name");
            int b7 = ag.b(a, "serialNumber");
            int b8 = ag.b(a, "buttons");
            int b9 = ag.b(a, "isActive");
            if (a.moveToFirst()) {
                hybridPreset = new HybridPreset(a.getString(b5), a.getString(b6), a.getString(b7), this.__hybridAppSettingTypeConverter.a(a.getString(b8)), a.getInt(b9) != 0);
                hybridPreset.setPinType(a.getInt(b2));
                hybridPreset.setCreatedAt(a.getString(b3));
                hybridPreset.setUpdatedAt(a.getString(b4));
            } else {
                hybridPreset = null;
            }
            return hybridPreset;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<HybridRecommendPreset> getRecommendPresetList(String str) {
        String str2 = str;
        uf b = uf.b("SELECT * FROM hybridRecommendPreset WHERE serialNumber=?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "name");
            int b4 = ag.b(a, "serialNumber");
            int b5 = ag.b(a, "buttons");
            int b6 = ag.b(a, "isDefault");
            int b7 = ag.b(a, "createdAt");
            int b8 = ag.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new HybridRecommendPreset(a.getString(b2), a.getString(b3), a.getString(b4), this.__hybridAppSettingTypeConverter.a(a.getString(b5)), a.getInt(b6) != 0, a.getString(b7), a.getString(b8)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void removeAllDeletePinTypePreset() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfRemoveAllDeletePinTypePreset.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAllDeletePinTypePreset.release(acquire);
        }
    }

    @DexIgnore
    public void upsertPreset(HybridPreset hybridPreset) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridPreset.insert(hybridPreset);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertPresetList(List<HybridPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertRecommendPresetList(List<HybridRecommendPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridRecommendPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
