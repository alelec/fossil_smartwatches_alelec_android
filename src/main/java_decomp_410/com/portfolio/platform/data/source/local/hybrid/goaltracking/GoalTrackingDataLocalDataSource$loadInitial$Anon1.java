package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.helper.PagingRequestHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDataLocalDataSource$loadInitial$Anon1 implements PagingRequestHelper.b {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource this$Anon0;

    @DexIgnore
    public GoalTrackingDataLocalDataSource$loadInitial$Anon1(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
        this.this$Anon0 = goalTrackingDataLocalDataSource;
    }

    @DexIgnore
    public final void run(PagingRequestHelper.b.a aVar) {
        GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = this.this$Anon0;
        PagingRequestHelper.RequestType requestType = PagingRequestHelper.RequestType.INITIAL;
        kd4.a((Object) aVar, "helperCallback");
        fi4 unused = goalTrackingDataLocalDataSource.loadData(requestType, aVar, this.this$Anon0.mOffset);
    }
}
