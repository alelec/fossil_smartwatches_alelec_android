package com.portfolio.platform.data.source.local.fitness;

import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitySummaryLocalDataSource$loadAfter$Anon1 implements PagingRequestHelper.b {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$Anon0;

    @DexIgnore
    public ActivitySummaryLocalDataSource$loadAfter$Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.this$Anon0 = activitySummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(PagingRequestHelper.b.a aVar) {
        Pair pair = (Pair) kb4.d(this.this$Anon0.mRequestAfterQueue);
        kd4.a((Object) aVar, "helperCallback");
        this.this$Anon0.loadData(PagingRequestHelper.RequestType.AFTER, (Date) pair.getFirst(), (Date) pair.getSecond(), aVar);
    }
}
