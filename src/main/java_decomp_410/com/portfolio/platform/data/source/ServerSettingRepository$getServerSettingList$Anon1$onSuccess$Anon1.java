package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.ServerSettingList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1", f = "ServerSettingRepository.kt", l = {}, m = "invokeSuspend")
public final class ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingList $serverSettingList;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRepository$getServerSettingList$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1(ServerSettingRepository$getServerSettingList$Anon1 serverSettingRepository$getServerSettingList$Anon1, ServerSettingList serverSettingList, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = serverSettingRepository$getServerSettingList$Anon1;
        this.$serverSettingList = serverSettingList;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1 serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1 = new ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1(this.this$Anon0, this.$serverSettingList, yb4);
        // serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1.p$ = (zg4) obj;
        // return serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ServerSettingRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getServerSettingList - onSuccess - serverSettingList = [ " + this.$serverSettingList + " ]");
            ServerSettingList serverSettingList = this.$serverSettingList;
            if (serverSettingList != null) {
                List<ServerSetting> serverSettings = serverSettingList.getServerSettings();
                Boolean a = serverSettings != null ? dc4.a(!serverSettings.isEmpty()) : null;
                if (a == null) {
                    kd4.a();
                    throw null;
                } else if (a.booleanValue()) {
                    this.this$Anon0.this$Anon0.mServerSettingLocalDataSource.addOrUpdateServerSettingList(this.$serverSettingList.getServerSettings());
                }
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
