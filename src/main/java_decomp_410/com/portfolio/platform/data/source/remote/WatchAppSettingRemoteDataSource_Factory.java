package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppSettingRemoteDataSource_Factory implements Factory<WatchAppSettingRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;

    @DexIgnore
    public WatchAppSettingRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceV2Provider = provider;
    }

    @DexIgnore
    public static WatchAppSettingRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new WatchAppSettingRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static WatchAppSettingRemoteDataSource newWatchAppSettingRemoteDataSource(ApiServiceV2 apiServiceV2) {
        return new WatchAppSettingRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public static WatchAppSettingRemoteDataSource provideInstance(Provider<ApiServiceV2> provider) {
        return new WatchAppSettingRemoteDataSource(provider.get());
    }

    @DexIgnore
    public WatchAppSettingRemoteDataSource get() {
        return provideInstance(this.mApiServiceV2Provider);
    }
}
