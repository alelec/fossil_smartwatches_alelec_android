package com.portfolio.platform.data.source.local.diana.workout;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.le;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSessionDifference extends le.d<WorkoutSession> {
    @DexIgnore
    public boolean areContentsTheSame(WorkoutSession workoutSession, WorkoutSession workoutSession2) {
        kd4.b(workoutSession, "oldItem");
        kd4.b(workoutSession2, "newItem");
        return kd4.a((Object) workoutSession, (Object) workoutSession2);
    }

    @DexIgnore
    public boolean areItemsTheSame(WorkoutSession workoutSession, WorkoutSession workoutSession2) {
        kd4.b(workoutSession, "oldItem");
        kd4.b(workoutSession2, "newItem");
        return kd4.a((Object) workoutSession.getId(), (Object) workoutSession2.getId());
    }
}
