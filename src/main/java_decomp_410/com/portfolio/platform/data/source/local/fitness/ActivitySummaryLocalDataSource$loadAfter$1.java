package com.portfolio.platform.data.source.local.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitySummaryLocalDataSource$loadAfter$1 implements com.portfolio.platform.helper.PagingRequestHelper.C5952b {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource this$0;

    @DexIgnore
    public ActivitySummaryLocalDataSource$loadAfter$1(com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.this$0 = activitySummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a aVar) {
        kotlin.Pair pair = (kotlin.Pair) com.fossil.blesdk.obfuscated.kb4.m24380d(this.this$0.mRequestAfterQueue);
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) aVar, "helperCallback");
        this.this$0.loadData(com.portfolio.platform.helper.PagingRequestHelper.RequestType.AFTER, (java.util.Date) pair.getFirst(), (java.util.Date) pair.getSecond(), aVar);
    }
}
