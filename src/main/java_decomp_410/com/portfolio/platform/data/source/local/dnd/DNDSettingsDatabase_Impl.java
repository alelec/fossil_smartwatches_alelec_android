package com.portfolio.platform.data.source.local.dnd;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DNDSettingsDatabase_Impl extends DNDSettingsDatabase {
    @DexIgnore
    public volatile DNDScheduledTimeDao _dNDScheduledTimeDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `dndScheduledTimeModel` (`scheduledTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, `scheduledTimeType` INTEGER NOT NULL, PRIMARY KEY(`scheduledTimeName`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'edbea9a651a6427903efe74c61fd6b87')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `dndScheduledTimeModel`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (DNDSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = DNDSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) DNDSettingsDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = DNDSettingsDatabase_Impl.this.mDatabase = ggVar;
            DNDSettingsDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (DNDSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = DNDSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) DNDSettingsDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("scheduledTimeName", new eg.a("scheduledTimeName", "TEXT", true, 1));
            hashMap.put("minutes", new eg.a("minutes", "INTEGER", true, 0));
            hashMap.put("scheduledTimeType", new eg.a("scheduledTimeType", "INTEGER", true, 0));
            eg egVar = new eg("dndScheduledTimeModel", hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar, "dndScheduledTimeModel");
            if (!egVar.equals(a)) {
                throw new IllegalStateException("Migration didn't properly handle dndScheduledTimeModel(com.portfolio.platform.data.model.DNDScheduledTimeModel).\n Expected:\n" + egVar + "\n Found:\n" + a);
            }
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `dndScheduledTimeModel`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), "dndScheduledTimeModel");
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(1), "edbea9a651a6427903efe74c61fd6b87", "1a7527e5c35fa2c8a6aef70719b78d76");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }

    @DexIgnore
    public DNDScheduledTimeDao getDNDScheduledTimeDao() {
        DNDScheduledTimeDao dNDScheduledTimeDao;
        if (this._dNDScheduledTimeDao != null) {
            return this._dNDScheduledTimeDao;
        }
        synchronized (this) {
            if (this._dNDScheduledTimeDao == null) {
                this._dNDScheduledTimeDao = new DNDScheduledTimeDao_Impl(this);
            }
            dNDScheduledTimeDao = this._dNDScheduledTimeDao;
        }
        return dNDScheduledTimeDao;
    }
}
