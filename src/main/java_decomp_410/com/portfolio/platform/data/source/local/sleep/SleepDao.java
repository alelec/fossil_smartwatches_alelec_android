package com.portfolio.platform.data.source.local.sleep;

import android.database.sqlite.SQLiteConstraintException;
import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.wearables.fsl.utils.TimeUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class SleepDao {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = SleepDao.class.getSimpleName();
        kd4.a((Object) simpleName, "SleepDao::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    private final MFSleepDay calculateSleepDay(MFSleepDay mFSleepDay, MFSleepSession mFSleepSession) {
        SleepDistribution sleepStateDistInMinute = mFSleepDay.getSleepStateDistInMinute();
        SleepDistribution sleepState = mFSleepSession.getSleepState();
        if (sleepStateDistInMinute != null) {
            sleepStateDistInMinute.setAwake(sleepStateDistInMinute.getAwake() + sleepState.getAwake());
            sleepStateDistInMinute.setLight(sleepStateDistInMinute.getLight() + sleepState.getLight());
            sleepStateDistInMinute.setDeep(sleepStateDistInMinute.getDeep() + sleepState.getDeep());
        }
        mFSleepDay.setSleepMinutes(mFSleepDay.getSleepMinutes() + mFSleepSession.getSleepMinutes());
        mFSleepDay.setSleepStateDistInMinute(sleepStateDistInMinute);
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        mFSleepDay.setUpdatedAt(new DateTime(instance.getTimeInMillis()));
        return mFSleepDay;
    }

    @DexIgnore
    private final MFSleepDay createSleepDayFromSleepSession(MFSleepSession mFSleepSession) {
        MFSleepDay nearestSleepDayFromDate = getNearestSleepDayFromDate(getStringDateDBFromDate(new Date()));
        return new MFSleepDay(new Date(mFSleepSession.getDate()), nearestSleepDayFromDate != null ? nearestSleepDayFromDate.getGoalMinutes() : 480, mFSleepSession.getSleepMinutes(), mFSleepSession.getSleepState(), new DateTime(), mFSleepSession.getUpdatedAt());
    }

    @DexIgnore
    private final String getStringDateDBFromDate(Date date) {
        String format = new SimpleDateFormat(TimeUtils.SIMPLE_FORMAT_YYYY_MM_DD, Locale.US).format(date);
        kd4.a((Object) format, "simpleDateFormat.format(date)");
        return format;
    }

    @DexIgnore
    public final void addSleepSession(MFSleepSession mFSleepSession) {
        kd4.b(mFSleepSession, "sleepSession");
        upsertSleepSession(mFSleepSession);
        MFSleepDay sleepDay = getSleepDay(getStringDateDBFromDate(mFSleepSession.getDay()));
        if (sleepDay == null) {
            upsertSleepDay(createSleepDayFromSleepSession(mFSleepSession));
        } else {
            upsertSleepDay(calculateSleepDay(sleepDay, mFSleepSession));
        }
    }

    @DexIgnore
    public abstract void deleteAllSleepDays();

    @DexIgnore
    public abstract void deleteAllSleepSessions();

    @DexIgnore
    public abstract MFSleepDay getLastSleepDay();

    @DexIgnore
    public abstract LiveData<Integer> getLastSleepGoal();

    @DexIgnore
    public abstract MFSleepDay getNearestSleepDayFromDate(String str);

    @DexIgnore
    public final int getNearestSleepGoalFromDate(Date date) {
        kd4.b(date, "date");
        MFSleepDay nearestSleepDayFromDate = getNearestSleepDayFromDate(getStringDateDBFromDate(date));
        if (nearestSleepDayFromDate != null) {
            return nearestSleepDayFromDate.getGoalMinutes();
        }
        return 480;
    }

    @DexIgnore
    public abstract List<MFSleepSession> getPendingSleepSessions();

    @DexIgnore
    public abstract List<MFSleepSession> getPendingSleepSessions(long j, long j2);

    @DexIgnore
    public final List<MFSleepSession> getPendingSleepSessions(Date date, Date date2) {
        kd4.b(date, GoalPhase.COLUMN_START_DATE);
        kd4.b(date2, GoalPhase.COLUMN_END_DATE);
        Date startOfDay = TimeUtils.getStartOfDay(date);
        kd4.a((Object) startOfDay, "TimeUtils.getStartOfDay(startDate)");
        long time = startOfDay.getTime();
        Date endOfDay = TimeUtils.getEndOfDay(date2);
        kd4.a((Object) endOfDay, "TimeUtils.getEndOfDay(endDate)");
        return getPendingSleepSessions(time, endOfDay.getTime());
    }

    @DexIgnore
    public abstract MFSleepDay getSleepDay(String str);

    @DexIgnore
    public final MFSleepDay getSleepDay(Date date) {
        kd4.b(date, "date");
        return getSleepDay(getStringDateDBFromDate(date));
    }

    @DexIgnore
    public abstract LiveData<MFSleepDay> getSleepDayLiveData(String str);

    @DexIgnore
    public abstract List<MFSleepDay> getSleepDays(String str, String str2);

    @DexIgnore
    public abstract LiveData<List<MFSleepDay>> getSleepDaysLiveData(String str, String str2);

    @DexIgnore
    public abstract LiveData<SleepRecommendedGoal> getSleepRecommendedGoalLiveData();

    @DexIgnore
    public abstract MFSleepSession getSleepSession(long j);

    @DexIgnore
    public final List<MFSleepSession> getSleepSessions(long j) {
        Date date = new Date(j);
        Date startOfDay = TimeUtils.getStartOfDay(date);
        kd4.a((Object) startOfDay, "TimeUtils.getStartOfDay(date)");
        long time = startOfDay.getTime();
        Date endOfDay = TimeUtils.getEndOfDay(date);
        kd4.a((Object) endOfDay, "TimeUtils.getEndOfDay(date)");
        return getSleepSessions(time, endOfDay.getTime());
    }

    @DexIgnore
    public abstract List<MFSleepSession> getSleepSessions(long j, long j2);

    @DexIgnore
    public abstract List<MFSleepSession> getSleepSessions(String str);

    @DexIgnore
    public abstract LiveData<List<MFSleepSession>> getSleepSessionsLiveData(long j, long j2);

    @DexIgnore
    public abstract MFSleepSettings getSleepSettings();

    @DexIgnore
    public abstract SleepStatistic getSleepStatistic();

    @DexIgnore
    public abstract LiveData<SleepStatistic> getSleepStatisticLiveData();

    @DexIgnore
    public abstract List<SleepSummary> getSleepSummariesDesc(String str, String str2);

    @DexIgnore
    public final List<SleepSummary> getSleepSummariesDesc(Date date, Date date2) {
        kd4.b(date, GoalPhase.COLUMN_START_DATE);
        kd4.b(date2, GoalPhase.COLUMN_END_DATE);
        return getSleepSummariesDesc(getStringDateDBFromDate(date), getStringDateDBFromDate(date2));
    }

    @DexIgnore
    public abstract SleepSummary getSleepSummary(String str);

    @DexIgnore
    public final SleepSummary getSleepSummary(Date date) {
        kd4.b(date, "date");
        return getSleepSummary(getStringDateDBFromDate(date));
    }

    @DexIgnore
    public abstract int getTotalSleep(String str, String str2);

    @DexIgnore
    public final int getTotalSleep(Date date, Date date2) {
        kd4.b(date, GoalPhase.COLUMN_START_DATE);
        kd4.b(date2, GoalPhase.COLUMN_END_DATE);
        return getTotalSleep(getStringDateDBFromDate(date), getStringDateDBFromDate(date2));
    }

    @DexIgnore
    public abstract void insertSleepSettings(MFSleepSettings mFSleepSettings);

    @DexIgnore
    public abstract void updateSleepSettings(int i);

    @DexIgnore
    public abstract void upsertSleepDay(MFSleepDay mFSleepDay);

    @DexIgnore
    public abstract void upsertSleepDays(List<MFSleepDay> list);

    @DexIgnore
    public abstract void upsertSleepRecommendedGoal(SleepRecommendedGoal sleepRecommendedGoal);

    @DexIgnore
    public abstract void upsertSleepSession(MFSleepSession mFSleepSession);

    @DexIgnore
    public abstract void upsertSleepSessionList(List<MFSleepSession> list);

    @DexIgnore
    public final void upsertSleepSettings(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "upsertSleepSettings sleepGoal=" + i);
        try {
            insertSleepSettings(new MFSleepSettings(i));
        } catch (SQLiteConstraintException unused) {
            updateSleepSettings(i);
        }
    }

    @DexIgnore
    public abstract long upsertSleepStatistic(SleepStatistic sleepStatistic);
}
