package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.wc4;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource;
import com.portfolio.platform.helper.PagingRequestHelper;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository$getSummariesPaging$Anon3 extends Lambda implements wc4<qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSummariesPaging$Anon3(SleepSummaryDataSourceFactory sleepSummaryDataSourceFactory) {
        super(0);
        this.$sourceFactory = sleepSummaryDataSourceFactory;
    }

    @DexIgnore
    public final void invoke() {
        SleepSummaryLocalDataSource a = this.$sourceFactory.getSourceLiveData().a();
        if (a != null) {
            PagingRequestHelper mHelper = a.getMHelper();
            if (mHelper != null) {
                mHelper.b();
            }
        }
    }
}
