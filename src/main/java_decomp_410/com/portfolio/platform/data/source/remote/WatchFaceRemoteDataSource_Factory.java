package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchFaceRemoteDataSource_Factory implements Factory<WatchFaceRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiProvider;

    @DexIgnore
    public WatchFaceRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.apiProvider = provider;
    }

    @DexIgnore
    public static WatchFaceRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new WatchFaceRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static WatchFaceRemoteDataSource newWatchFaceRemoteDataSource(ApiServiceV2 apiServiceV2) {
        return new WatchFaceRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public static WatchFaceRemoteDataSource provideInstance(Provider<ApiServiceV2> provider) {
        return new WatchFaceRemoteDataSource(provider.get());
    }

    @DexIgnore
    public WatchFaceRemoteDataSource get() {
        return provideInstance(this.apiProvider);
    }
}
