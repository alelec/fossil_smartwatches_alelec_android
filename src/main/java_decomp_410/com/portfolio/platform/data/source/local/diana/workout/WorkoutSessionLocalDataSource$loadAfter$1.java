package com.portfolio.platform.data.source.local.diana.workout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSessionLocalDataSource$loadAfter$1 implements com.portfolio.platform.helper.PagingRequestHelper.C5952b {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource this$0;

    @DexIgnore
    public WorkoutSessionLocalDataSource$loadAfter$1(com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource workoutSessionLocalDataSource) {
        this.this$0 = workoutSessionLocalDataSource;
    }

    @DexIgnore
    public final void run(com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a aVar) {
        com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource workoutSessionLocalDataSource = this.this$0;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) aVar, "helperCallback");
        com.fossil.blesdk.obfuscated.fi4 unused = workoutSessionLocalDataSource.loadData(aVar, this.this$0.mOffset);
    }
}
