package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.portfolio.platform.data.model.Category;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CategoryDao_Impl implements CategoryDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfCategory;
    @DexIgnore
    public /* final */ wf __preparedStmtOfClearData;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<Category> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `category`(`id`,`englishName`,`name`,`updatedAt`,`createdAt`,`priority`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, Category category) {
            if (category.getId() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, category.getId());
            }
            if (category.getEnglishName() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, category.getEnglishName());
            }
            if (category.getName() == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, category.getName());
            }
            if (category.getUpdatedAt() == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, category.getUpdatedAt());
            }
            if (category.getCreatedAt() == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, category.getCreatedAt());
            }
            kgVar.b(6, (long) category.getPriority());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends wf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM category";
        }
    }

    @DexIgnore
    public CategoryDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfCategory = new Anon1(roomDatabase);
        this.__preparedStmtOfClearData = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void clearData() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfClearData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearData.release(acquire);
        }
    }

    @DexIgnore
    public List<Category> getAllCategory() {
        uf b = uf.b("SELECT * FROM category ORDER BY priority DESC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "englishName");
            int b4 = ag.b(a, "name");
            int b5 = ag.b(a, "updatedAt");
            int b6 = ag.b(a, "createdAt");
            int b7 = ag.b(a, "priority");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new Category(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public Category getCategoryById(String str) {
        String str2 = str;
        uf b = uf.b("SELECT * FROM category WHERE id=?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            return a.moveToFirst() ? new Category(a.getString(ag.b(a, "id")), a.getString(ag.b(a, "englishName")), a.getString(ag.b(a, "name")), a.getString(ag.b(a, "updatedAt")), a.getString(ag.b(a, "createdAt")), a.getInt(ag.b(a, "priority"))) : null;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertCategoryList(List<Category> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfCategory.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
