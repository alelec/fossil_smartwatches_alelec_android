package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideHeartRateDailySummaryDaoFactory implements Factory<HeartRateDailySummaryDao> {
    @DexIgnore
    public /* final */ Provider<FitnessDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideHeartRateDailySummaryDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideHeartRateDailySummaryDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideHeartRateDailySummaryDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static HeartRateDailySummaryDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        return proxyProvideHeartRateDailySummaryDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static HeartRateDailySummaryDao proxyProvideHeartRateDailySummaryDao(PortfolioDatabaseModule portfolioDatabaseModule, FitnessDatabase fitnessDatabase) {
        HeartRateDailySummaryDao provideHeartRateDailySummaryDao = portfolioDatabaseModule.provideHeartRateDailySummaryDao(fitnessDatabase);
        n44.a(provideHeartRateDailySummaryDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideHeartRateDailySummaryDao;
    }

    @DexIgnore
    public HeartRateDailySummaryDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
