package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.yb4;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource", f = "MicroAppRemoteDataSource.kt", l = {18}, m = "getAllMicroApp")
public final class MicroAppRemoteDataSource$getAllMicroApp$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppRemoteDataSource$getAllMicroApp$Anon1(MicroAppRemoteDataSource microAppRemoteDataSource, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = microAppRemoteDataSource;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.getAllMicroApp((String) null, this);
    }
}
