package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideAlarmsRemoteDataSourceFactory implements Factory<AlarmsRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiServiceProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideAlarmsRemoteDataSourceFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        this.module = portfolioDatabaseModule;
        this.apiServiceProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideAlarmsRemoteDataSourceFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        return new PortfolioDatabaseModule_ProvideAlarmsRemoteDataSourceFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static AlarmsRemoteDataSource provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        return proxyProvideAlarmsRemoteDataSource(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static AlarmsRemoteDataSource proxyProvideAlarmsRemoteDataSource(PortfolioDatabaseModule portfolioDatabaseModule, ApiServiceV2 apiServiceV2) {
        AlarmsRemoteDataSource provideAlarmsRemoteDataSource = portfolioDatabaseModule.provideAlarmsRemoteDataSource(apiServiceV2);
        n44.a(provideAlarmsRemoteDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideAlarmsRemoteDataSource;
    }

    @DexIgnore
    public AlarmsRemoteDataSource get() {
        return provideInstance(this.module, this.apiServiceProvider);
    }
}
