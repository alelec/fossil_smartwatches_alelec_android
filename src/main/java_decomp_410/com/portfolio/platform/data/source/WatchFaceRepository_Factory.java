package com.portfolio.platform.data.source;

import android.content.Context;
import com.portfolio.platform.data.source.local.diana.WatchFaceDao;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchFaceRepository_Factory implements Factory<WatchFaceRepository> {
    @DexIgnore
    public /* final */ Provider<Context> contextProvider;
    @DexIgnore
    public /* final */ Provider<WatchFaceDao> watchFaceDaoProvider;
    @DexIgnore
    public /* final */ Provider<WatchFaceRemoteDataSource> watchFaceRemoteDataSourceProvider;

    @DexIgnore
    public WatchFaceRepository_Factory(Provider<Context> provider, Provider<WatchFaceDao> provider2, Provider<WatchFaceRemoteDataSource> provider3) {
        this.contextProvider = provider;
        this.watchFaceDaoProvider = provider2;
        this.watchFaceRemoteDataSourceProvider = provider3;
    }

    @DexIgnore
    public static WatchFaceRepository_Factory create(Provider<Context> provider, Provider<WatchFaceDao> provider2, Provider<WatchFaceRemoteDataSource> provider3) {
        return new WatchFaceRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static WatchFaceRepository newWatchFaceRepository(Context context, WatchFaceDao watchFaceDao, WatchFaceRemoteDataSource watchFaceRemoteDataSource) {
        return new WatchFaceRepository(context, watchFaceDao, watchFaceRemoteDataSource);
    }

    @DexIgnore
    public static WatchFaceRepository provideInstance(Provider<Context> provider, Provider<WatchFaceDao> provider2, Provider<WatchFaceRemoteDataSource> provider3) {
        return new WatchFaceRepository(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public WatchFaceRepository get() {
        return provideInstance(this.contextProvider, this.watchFaceDaoProvider, this.watchFaceRemoteDataSourceProvider);
    }
}
