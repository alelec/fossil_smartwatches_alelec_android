package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getSummary$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $date;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SummariesRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.SummariesRepository$getSummary$1$1")
    /* renamed from: com.portfolio.platform.data.source.SummariesRepository$getSummary$1$1 */
    public static final class C57181 extends com.portfolio.platform.util.NetworkBoundResource<com.portfolio.platform.data.model.room.fitness.ActivitySummary, com.fossil.blesdk.obfuscated.xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.SummariesRepository$getSummary$1 this$0;

        @DexIgnore
        public C57181(com.portfolio.platform.data.source.SummariesRepository$getSummary$1 summariesRepository$getSummary$1, java.util.List list) {
            this.this$0 = summariesRepository$getSummary$1;
            this.$fitnessDataList = list;
        }

        @DexIgnore
        public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.fossil.blesdk.obfuscated.xz1>> yb4) {
            java.util.Date n = com.fossil.blesdk.obfuscated.rk2.m27409n(this.this$0.$date);
            java.util.Date i = com.fossil.blesdk.obfuscated.rk2.m27404i(this.this$0.$date);
            java.util.Calendar instance = java.util.Calendar.getInstance();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "calendar");
            instance.setTimeInMillis(0);
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiServiceV2$p = this.this$0.this$0.mApiServiceV2;
            java.lang.String e = com.fossil.blesdk.obfuscated.rk2.m27397e(n);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.formatShortDate(startDate)");
            java.lang.String e2 = com.fossil.blesdk.obfuscated.rk2.m27397e(i);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e2, "DateHelper.formatShortDate(endDate)");
            return access$getMApiServiceV2$p.getSummaries(e, e2, 0, 100, yb4);
        }

        @DexIgnore
        public androidx.lifecycle.LiveData<com.portfolio.platform.data.model.room.fitness.ActivitySummary> loadFromDb() {
            java.util.Calendar instance = java.util.Calendar.getInstance();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "calendar");
            instance.setTime(this.this$0.$date);
            androidx.lifecycle.LiveData<com.portfolio.platform.data.model.room.fitness.ActivitySummary> activitySummaryLiveData = this.this$0.this$0.mActivitySummaryDao.getActivitySummaryLiveData(instance.get(1), instance.get(2) + 1, instance.get(5));
            if (!com.fossil.blesdk.obfuscated.rk2.m27414s(this.this$0.$date).booleanValue()) {
                return activitySummaryLiveData;
            }
            androidx.lifecycle.LiveData<com.portfolio.platform.data.model.room.fitness.ActivitySummary> a = com.fossil.blesdk.obfuscated.C1935hc.m7843a(activitySummaryLiveData, new com.portfolio.platform.data.source.SummariesRepository$getSummary$1$1$loadFromDb$1(this));
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "Transformations.map(acti\u2026ary\n                    }");
            return a;
        }

        @DexIgnore
        public void onFetchFailed(java.lang.Throwable th) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(com.portfolio.platform.data.source.SummariesRepository.TAG, "getSummary - onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(com.fossil.blesdk.obfuscated.xz1 xz1) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(xz1, "item");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "getSummary - saveCallResult -- date=" + this.this$0.$date + ", item=" + xz1);
            try {
                java.util.ArrayList arrayList = new java.util.ArrayList();
                com.fossil.blesdk.obfuscated.rz1 rz1 = new com.fossil.blesdk.obfuscated.rz1();
                rz1.mo15797a(java.util.Date.class, new com.portfolio.platform.helper.GsonConvertDate());
                rz1.mo15797a(org.joda.time.DateTime.class, new com.portfolio.platform.helper.GsonConvertDateTime());
                com.portfolio.platform.data.source.remote.ApiResponse apiResponse = (com.portfolio.platform.data.source.remote.ApiResponse) rz1.mo15799a().mo23094a(xz1.toString(), new com.portfolio.platform.data.source.SummariesRepository$getSummary$1$1$saveCallResult$1().getType());
                if (apiResponse != null) {
                    java.util.List<com.portfolio.platform.data.model.FitnessDayData> list = apiResponse.get_items();
                    if (list != null) {
                        for (com.portfolio.platform.data.model.FitnessDayData activitySummary : list) {
                            com.portfolio.platform.data.model.room.fitness.ActivitySummary activitySummary2 = activitySummary.toActivitySummary();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) activitySummary2, "it.toActivitySummary()");
                            arrayList.add(activitySummary2);
                        }
                    }
                }
                java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> fitnessData = this.this$0.this$0.mFitnessDataDao.getFitnessData(this.this$0.$date, this.this$0.$date);
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local2.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "fitnessDatasSize " + fitnessData.size());
                if ((!arrayList.isEmpty()) && fitnessData.isEmpty()) {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    local3.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "upsert " + ((com.portfolio.platform.data.model.room.fitness.ActivitySummary) arrayList.get(0)));
                    com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao access$getMActivitySummaryDao$p = this.this$0.this$0.mActivitySummaryDao;
                    java.lang.Object obj = arrayList.get(0);
                    com.fossil.blesdk.obfuscated.kd4.m24407a(obj, "summaryList[0]");
                    access$getMActivitySummaryDao$p.upsertActivitySummary((com.portfolio.platform.data.model.room.fitness.ActivitySummary) obj);
                }
            } catch (java.lang.Exception e) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append("getSummary - saveCallResult -- e=");
                e.printStackTrace();
                sb.append(com.fossil.blesdk.obfuscated.qa4.f17909a);
                local4.mo33256e(com.portfolio.platform.data.source.SummariesRepository.TAG, sb.toString());
            }
        }

        @DexIgnore
        public boolean shouldFetch(com.portfolio.platform.data.model.room.fitness.ActivitySummary activitySummary) {
            return this.$fitnessDataList.isEmpty();
        }
    }

    @DexIgnore
    public SummariesRepository$getSummary$1(com.portfolio.platform.data.source.SummariesRepository summariesRepository, java.util.Date date) {
        this.this$0 = summariesRepository;
        this.$date = date;
    }

    @DexIgnore
    public final androidx.lifecycle.LiveData<com.fossil.blesdk.obfuscated.os3<com.portfolio.platform.data.model.room.fitness.ActivitySummary>> apply(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> list) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "getSummary - date=" + this.$date + " fitnessDataList=" + list.size());
        return new com.portfolio.platform.data.source.SummariesRepository$getSummary$1.C57181(this, list).asLiveData();
    }
}
