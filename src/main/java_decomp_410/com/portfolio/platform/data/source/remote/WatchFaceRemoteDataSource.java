package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.em4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchFaceRemoteDataSource {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 api;

    @DexIgnore
    public WatchFaceRemoteDataSource(ApiServiceV2 apiServiceV2) {
        kd4.b(apiServiceV2, "api");
        this.api = apiServiceV2;
        String simpleName = WatchFaceRemoteDataSource.class.getSimpleName();
        kd4.a((Object) simpleName, "WatchFaceRemoteDataSource::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    public final Object downloadWatchFaceFile(String str, String str2, yb4<? super qo2<em4>> yb4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = this.TAG;
        local.d(str3, "download: " + str + " path: " + str2);
        return ResponseKt.a(new WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2(this, str, (yb4) null), yb4);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getWatchFacesFromServer(String str, yb4<? super qo2<ArrayList<WatchFace>>> yb4) {
        WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1 watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1;
        int i;
        WatchFaceRemoteDataSource watchFaceRemoteDataSource;
        qo2 qo2;
        if (yb4 instanceof WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1) {
            watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1 = (WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1) yb4;
            int i2 = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.result;
                Object a = cc4.a();
                i = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1 watchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1 = new WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1(this, str, (yb4) null);
                    watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.L$Anon0 = this;
                    watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.L$Anon1 = str;
                    watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label = 1;
                    obj = ResponseKt.a(watchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1, watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    watchFaceRemoteDataSource = this;
                } else if (i == 1) {
                    String str2 = (String) watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.L$Anon1;
                    watchFaceRemoteDataSource = (WatchFaceRemoteDataSource) watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = watchFaceRemoteDataSource.TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("data: ");
                    ro2 ro2 = (ro2) qo2;
                    Object a2 = ro2.a();
                    if (a2 != null) {
                        sb.append(((ApiResponse) a2).get_items());
                        sb.append(" - isFromCache: ");
                        sb.append(ro2.b());
                        local.d(str3, sb.toString());
                        List list = ((ApiResponse) ro2.a()).get_items();
                        if (!(list instanceof ArrayList)) {
                            list = null;
                        }
                        return new ro2((ArrayList) list, ro2.b());
                    }
                    kd4.a();
                    throw null;
                } else if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), (Throwable) null, (String) null, 12, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1 = new WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1(this, yb4);
        Object obj2 = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.result;
        Object a3 = cc4.a();
        i = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }
}
