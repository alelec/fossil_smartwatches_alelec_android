package com.portfolio.platform.data.source;

import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.vk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.setting.WatchLocalization;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;
import java.io.File;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.text.StringsKt__StringsKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchLocalizationRepository {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 api;
    @DexIgnore
    public /* final */ en2 sharedPreferencesManager;

    @DexIgnore
    public WatchLocalizationRepository(ApiServiceV2 apiServiceV2, en2 en2) {
        kd4.b(apiServiceV2, "api");
        kd4.b(en2, "sharedPreferencesManager");
        this.api = apiServiceV2;
        this.sharedPreferencesManager = en2;
        String simpleName = WatchLocalizationRepository.class.getSimpleName();
        kd4.a((Object) simpleName, "WatchLocalizationRepository::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    private final gh4<String> processDownloadAndStore(String str, String str2) {
        throw null;
        // return ag4.a(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new WatchLocalizationRepository$processDownloadAndStore$Anon1(this, str, str2, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x01dd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    public final Object getWatchLocalizationFromServer(boolean z, yb4<? super String> yb4) {
        throw null;
        // WatchLocalizationRepository$getWatchLocalizationFromServer$Anon1 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1;
        // int i;
        // WatchLocalizationRepository watchLocalizationRepository;
        // boolean z2;
        // String str;
        // qo2 qo2;
        // WatchLocalization watchLocalization;
        // Object obj;
        // yb4<? super String> yb42 = yb4;
        // if (yb42 instanceof WatchLocalizationRepository$getWatchLocalizationFromServer$Anon1) {
        //     watchLocalizationRepository$getWatchLocalizationFromServer$Anon1 = (WatchLocalizationRepository$getWatchLocalizationFromServer$Anon1) yb42;
        //     int i2 = watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.label;
        //     if ((i2 & Integer.MIN_VALUE) != 0) {
        //         watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.label = i2 - Integer.MIN_VALUE;
        //         Object obj2 = watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.result;
        //         Object a = cc4.a();
        //         i = watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.label;
        //         if (i != 0) {
        //             na4.a(obj2);
        //             str = PortfolioApp.W.c().n();
        //             WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1 watchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1 = new WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1(this, str, (yb4) null);
        //             watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon0 = this;
        //             boolean z3 = z;
        //             watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.Z$Anon0 = z3;
        //             watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon1 = str;
        //             watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.label = 1;
        //             obj2 = ResponseKt.a(watchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1, watchLocalizationRepository$getWatchLocalizationFromServer$Anon1);
        //             if (obj2 == a) {
        //                 return a;
        //             }
        //             z2 = z3;
        //             watchLocalizationRepository = this;
        //         } else if (i == 1) {
        //             str = (String) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon1;
        //             z2 = watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.Z$Anon0;
        //             watchLocalizationRepository = (WatchLocalizationRepository) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon0;
        //             na4.a(obj2);
        //         } else if (i == 2) {
        //             String str2 = (String) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon10;
        //             gh4 gh4 = (gh4) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon9;
        //             File file = (File) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon8;
        //             String str3 = (String) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon7;
        //             String str4 = (String) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon6;
        //             WatchLocalization watchLocalization2 = (WatchLocalization) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon5;
        //             List list = (List) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon4;
        //             List list2 = (List) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon3;
        //             qo2 qo22 = (qo2) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon2;
        //             String str5 = (String) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon1;
        //             boolean z4 = watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.Z$Anon0;
        //             WatchLocalizationRepository watchLocalizationRepository2 = (WatchLocalizationRepository) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon0;
        //             na4.a(obj2);
        //             return (String) obj2;
        //         } else {
        //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        //         }
        //         qo2 = (qo2) obj2;
        //         if (!(qo2 instanceof ro2)) {
        //             ro2 ro2 = (ro2) qo2;
        //             ApiResponse apiResponse = (ApiResponse) ro2.a();
        //             List list3 = apiResponse != null ? apiResponse.get_items() : null;
        //             if (list3 == null) {
        //                 return null;
        //             }
        //             WatchLocalization watchLocalization3 = (WatchLocalization) list3.get(0);
        //             watchLocalizationRepository.sharedPreferencesManager.d(watchLocalization3.getName(), str);
        //             watchLocalizationRepository.sharedPreferencesManager.t(watchLocalization3.getMetaData().getVersion().toString());
        //             String url = watchLocalization3.getData().getUrl();
        //             StringBuilder sb = new StringBuilder();
        //             sb.append("localization_");
        //             sb.append(str);
        //             StringBuilder sb2 = sb;
        //             int b = StringsKt__StringsKt.b((CharSequence) url, CodelessMatcher.CURRENT_CLASS_NAME, 0, false, 6, (Object) null);
        //             if (url != null) {
        //                 String substring = url.substring(b);
        //                 kd4.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
        //                 sb2.append(substring);
        //                 String sb3 = sb2.toString();
        //                 String str6 = PortfolioApp.W.c().getFilesDir() + "/localization";
        //                 File file2 = new File(str6);
        //                 if (!file2.exists()) {
        //                     boolean mkdirs = file2.mkdirs();
        //                     ILocalFLogger local = FLogger.INSTANCE.getLocal();
        //                     String str7 = watchLocalizationRepository.TAG;
        //                     obj = a;
        //                     StringBuilder sb4 = new StringBuilder();
        //                     watchLocalization = watchLocalization3;
        //                     sb4.append("create ");
        //                     sb4.append(file2);
        //                     sb4.append(" -  ");
        //                     sb4.append(mkdirs);
        //                     local.d(str7, sb4.toString());
        //                 } else {
        //                     obj = a;
        //                     watchLocalization = watchLocalization3;
        //                 }
        //                 if (ro2.b()) {
        //                     if (vk2.a.a(str6 + File.separator + sb3)) {
        //                         return null;
        //                     }
        //                 }
        //                 gh4<String> processDownloadAndStore = watchLocalizationRepository.processDownloadAndStore(url, str6 + File.separator + sb3);
        //                 if (!z2) {
        //                     return null;
        //                 }
        //                 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon0 = watchLocalizationRepository;
        //                 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.Z$Anon0 = z2;
        //                 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon1 = str;
        //                 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon2 = qo2;
        //                 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon3 = list3;
        //                 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon4 = list3;
        //                 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon5 = watchLocalization;
        //                 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon6 = url;
        //                 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon7 = str6;
        //                 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon8 = file2;
        //                 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon9 = processDownloadAndStore;
        //                 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$Anon10 = sb3;
        //                 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.label = 2;
        //                 obj2 = processDownloadAndStore.a(watchLocalizationRepository$getWatchLocalizationFromServer$Anon1);
        //                 Object obj3 = obj;
        //                 if (obj2 == obj3) {
        //                     return obj3;
        //                 }
        //                 return (String) obj2;
        //             }
        //             throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        //         }
        //         FLogger.INSTANCE.getLocal().d(watchLocalizationRepository.TAG, "getWatchLocalizationFromServer - FAILED");
        //         return null;
        //     }
        // }
        // watchLocalizationRepository$getWatchLocalizationFromServer$Anon1 = new WatchLocalizationRepository$getWatchLocalizationFromServer$Anon1(this, yb42);
        // Object obj22 = watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.result;
        // Object a2 = cc4.a();
        // i = watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.label;
        // if (i != 0) {
        // }
        // qo2 = (qo2) obj22;
        // if (!(qo2 instanceof ro2)) {
        // }
    }
}
