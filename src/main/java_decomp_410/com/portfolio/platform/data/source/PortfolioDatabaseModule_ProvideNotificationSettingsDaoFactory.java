package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory implements Factory<NotificationSettingsDao> {
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<NotificationSettingsDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<NotificationSettingsDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static NotificationSettingsDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<NotificationSettingsDatabase> provider) {
        return proxyProvideNotificationSettingsDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static NotificationSettingsDao proxyProvideNotificationSettingsDao(PortfolioDatabaseModule portfolioDatabaseModule, NotificationSettingsDatabase notificationSettingsDatabase) {
        NotificationSettingsDao provideNotificationSettingsDao = portfolioDatabaseModule.provideNotificationSettingsDao(notificationSettingsDatabase);
        n44.a(provideNotificationSettingsDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideNotificationSettingsDao;
    }

    @DexIgnore
    public NotificationSettingsDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
