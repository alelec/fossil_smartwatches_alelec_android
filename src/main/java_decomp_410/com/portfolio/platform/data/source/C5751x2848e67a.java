package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory */
public final class C5751x2848e67a implements dagger.internal.Factory<com.portfolio.platform.data.source.UAppSystemVersionDataSource> {
    @DexIgnore
    public /* final */ com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule module;

    @DexIgnore
    public C5751x2848e67a(com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        this.module = uAppSystemVersionRepositoryModule;
    }

    @DexIgnore
    public static com.portfolio.platform.data.source.C5751x2848e67a create(com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        return new com.portfolio.platform.data.source.C5751x2848e67a(uAppSystemVersionRepositoryModule);
    }

    @DexIgnore
    public static com.portfolio.platform.data.source.UAppSystemVersionDataSource provideInstance(com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        return proxyProvideUserLocalDataSource(uAppSystemVersionRepositoryModule);
    }

    @DexIgnore
    public static com.portfolio.platform.data.source.UAppSystemVersionDataSource proxyProvideUserLocalDataSource(com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        com.portfolio.platform.data.source.UAppSystemVersionDataSource provideUserLocalDataSource = uAppSystemVersionRepositoryModule.provideUserLocalDataSource();
        com.fossil.blesdk.obfuscated.n44.m25602a(provideUserLocalDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideUserLocalDataSource;
    }

    @DexIgnore
    public com.portfolio.platform.data.source.UAppSystemVersionDataSource get() {
        return provideInstance(this.module);
    }
}
