package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository$pushPendingGoalTrackingDataList$3 implements com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.GoalTrackingRepository this$0;

    @DexIgnore
    public GoalTrackingRepository$pushPendingGoalTrackingDataList$3(com.portfolio.platform.data.source.GoalTrackingRepository goalTrackingRepository) {
        this.this$0 = goalTrackingRepository;
    }

    @DexIgnore
    public void onFail(int i) {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.GoalTrackingRepository.Companion.getTAG(), "pushPendingGoalTrackingDataList onFetchFailed");
    }

    @DexIgnore
    public void onSuccess(java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> list) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "goalTrackingList");
        kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef = new kotlin.jvm.internal.Ref$ObjectRef();
        ref$ObjectRef.element = list.get(0).getDate();
        kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2 = new kotlin.jvm.internal.Ref$ObjectRef();
        ref$ObjectRef2.element = list.get(0).getDate();
        for (com.portfolio.platform.data.model.goaltracking.GoalTrackingData next : list) {
            if (next.getDate().getTime() < ((java.util.Date) ref$ObjectRef.element).getTime()) {
                ref$ObjectRef.element = next.getDate();
            }
            if (next.getDate().getTime() > ((java.util.Date) ref$ObjectRef2.element).getTime()) {
                ref$ObjectRef2.element = next.getDate();
            }
        }
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.C5700x9e2a485b(this, ref$ObjectRef, ref$ObjectRef2, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }
}
