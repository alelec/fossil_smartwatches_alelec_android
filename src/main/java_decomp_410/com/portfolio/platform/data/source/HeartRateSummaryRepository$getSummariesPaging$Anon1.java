package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.m3;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSummaryRepository$getSummariesPaging$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public static /* final */ HeartRateSummaryRepository$getSummariesPaging$Anon1 INSTANCE; // = new HeartRateSummaryRepository$getSummariesPaging$Anon1();

    @DexIgnore
    public final LiveData<NetworkState> apply(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        return heartRateSummaryLocalDataSource.getMNetworkState();
    }
}
