package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AlarmHelper;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.AlarmsRepository$downloadAlarms$Anon2", f = "AlarmsRepository.kt", l = {}, m = "invokeSuspend")
public final class AlarmsRepository$downloadAlarms$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ qo2 $alarmsResponse;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$downloadAlarms$Anon2(AlarmsRepository alarmsRepository, qo2 qo2, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = alarmsRepository;
        this.$alarmsResponse = qo2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        AlarmsRepository$downloadAlarms$Anon2 alarmsRepository$downloadAlarms$Anon2 = new AlarmsRepository$downloadAlarms$Anon2(this.this$Anon0, this.$alarmsResponse, yb4);
        alarmsRepository$downloadAlarms$Anon2.p$ = (zg4) obj;
        return alarmsRepository$downloadAlarms$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AlarmsRepository$downloadAlarms$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            if (!((ro2) this.$alarmsResponse).b()) {
                List<Alarm> list = (List) ((ro2) this.$alarmsResponse).a();
                if (list != null) {
                    for (Alarm alarm : list) {
                        if (AlarmHelper.f.a(alarm)) {
                            alarm.setActive(false);
                            alarm.setPinType(2);
                        }
                    }
                    this.this$Anon0.mAlarmsLocalDataSource.insertAlarms(list);
                }
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
