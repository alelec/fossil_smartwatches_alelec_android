package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.jvm.internal.Ref$IntRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSampleRepository$getHeartRateSamples$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSampleRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends NetworkBoundResource<List<HeartRateSample>, ApiResponse<HeartRate>> {
        @DexIgnore
        public /* final */ /* synthetic */ Pair $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$IntRef $offset;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSampleRepository$getHeartRateSamples$Anon1 this$Anon0;

        @DexIgnore
        public Anon1(HeartRateSampleRepository$getHeartRateSamples$Anon1 heartRateSampleRepository$getHeartRateSamples$Anon1, Ref$IntRef ref$IntRef, int i, Pair pair) {
            this.this$Anon0 = heartRateSampleRepository$getHeartRateSamples$Anon1;
            this.$offset = ref$IntRef;
            this.$limit = i;
            this.$downloadingDate = pair;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
            if (r0 != null) goto L_0x0019;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
            if (r0 != null) goto L_0x0033;
         */
        @DexIgnore
        public Object createCall(yb4<? super qr4<ApiResponse<HeartRate>>> yb4) {
            Date date;
            Date date2;
            ApiServiceV2 access$getMApiService$p = this.this$Anon0.this$Anon0.mApiService;
            Pair pair = this.$downloadingDate;
            if (pair != null) {
                date = (Date) pair.getFirst();
            }
            date = this.this$Anon0.$startDate;
            String e = rk2.e(date);
            kd4.a((Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            Pair pair2 = this.$downloadingDate;
            if (pair2 != null) {
                date2 = (Date) pair2.getSecond();
            }
            date2 = this.this$Anon0.$endDate;
            String e2 = rk2.e(date2);
            kd4.a((Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiService$p.getHeartRateSamples(e, e2, this.$offset.element, this.$limit, yb4);
        }

        @DexIgnore
        public LiveData<List<HeartRateSample>> loadFromDb() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = HeartRateSampleRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getHeartRateSamples loadFromDb isNotToday day = " + this.this$Anon0.$end);
            HeartRateSampleDao access$getMHeartRateSampleDao$p = this.this$Anon0.this$Anon0.mHeartRateSampleDao;
            Date date = this.this$Anon0.$startDate;
            kd4.a((Object) date, GoalPhase.COLUMN_START_DATE);
            Date date2 = this.this$Anon0.$endDate;
            kd4.a((Object) date2, GoalPhase.COLUMN_END_DATE);
            return access$getMHeartRateSampleDao$p.getHeartRateSamples(date, date2);
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().d(HeartRateSampleRepository.Companion.getTAG$app_fossilRelease(), "getHeartRateSamples onFetchFailed");
        }

        @DexIgnore
        public boolean processContinueFetching(ApiResponse<HeartRate> apiResponse) {
            kd4.b(apiResponse, "item");
            Range range = apiResponse.get_range();
            if (range == null || !range.isHasNext()) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d(HeartRateSampleRepository.Companion.getTAG$app_fossilRelease(), "getHeartRateSamples processContinueFetching hasNext");
            this.$offset.element += this.$limit;
            return true;
        }

        @DexIgnore
        public void saveCallResult(ApiResponse<HeartRate> apiResponse) {
            kd4.b(apiResponse, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = HeartRateSampleRepository.Companion.getTAG$app_fossilRelease();
            StringBuilder sb = new StringBuilder();
            sb.append("getHeartRateSamples saveCallResult onResponse: response = ");
            sb.append(apiResponse.get_items().size());
            sb.append(" hasNext=");
            Range range = apiResponse.get_range();
            sb.append(range != null ? Boolean.valueOf(range.isHasNext()) : null);
            local.d(tAG$app_fossilRelease, sb.toString());
            ArrayList arrayList = new ArrayList();
            for (HeartRate heartRateSample : apiResponse.get_items()) {
                HeartRateSample heartRateSample2 = heartRateSample.toHeartRateSample();
                if (heartRateSample2 != null) {
                    arrayList.add(heartRateSample2);
                }
            }
            this.this$Anon0.this$Anon0.mHeartRateSampleDao.upsertHeartRateSampleList(arrayList);
        }

        @DexIgnore
        public boolean shouldFetch(List<HeartRateSample> list) {
            return this.this$Anon0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public HeartRateSampleRepository$getHeartRateSamples$Anon1(HeartRateSampleRepository heartRateSampleRepository, Date date, Date date2, boolean z, Date date3) {
        this.this$Anon0 = heartRateSampleRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
        this.$end = date3;
    }

    @DexIgnore
    public final LiveData<os3<List<HeartRateSample>>> apply(List<FitnessDataWrapper> list) {
        Ref$IntRef ref$IntRef = new Ref$IntRef();
        ref$IntRef.element = 0;
        kd4.a((Object) list, "fitnessDataList");
        Date date = this.$startDate;
        kd4.a((Object) date, GoalPhase.COLUMN_START_DATE);
        Date date2 = this.$endDate;
        kd4.a((Object) date2, GoalPhase.COLUMN_END_DATE);
        return new Anon1(this, ref$IntRef, 300, FitnessDataWrapperKt.calculateRangeDownload(list, date, date2)).asLiveData();
    }
}
