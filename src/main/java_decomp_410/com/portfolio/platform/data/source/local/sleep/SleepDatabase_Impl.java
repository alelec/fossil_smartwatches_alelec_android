package com.portfolio.platform.data.source.local.sleep;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import com.fossil.wearables.fsl.sleep.MFSleepDay;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.portfolio.platform.data.SleepStatistic;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepDatabase_Impl extends SleepDatabase {
    @DexIgnore
    public volatile SleepDao _sleepDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `sleep_session` (`pinType` INTEGER NOT NULL, `date` INTEGER NOT NULL, `day` TEXT NOT NULL, `deviceSerialNumber` TEXT, `syncTime` INTEGER, `bookmarkTime` INTEGER, `normalizedSleepQuality` REAL NOT NULL, `source` INTEGER NOT NULL, `realStartTime` INTEGER NOT NULL, `realEndTime` INTEGER NOT NULL, `realSleepMinutes` INTEGER NOT NULL, `realSleepStateDistInMinute` TEXT NOT NULL, `editedStartTime` INTEGER, `editedEndTime` INTEGER, `editedSleepMinutes` INTEGER, `editedSleepStateDistInMinute` TEXT, `sleepStates` TEXT NOT NULL, `heartRate` TEXT, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `timezoneOffset` INTEGER NOT NULL, PRIMARY KEY(`realEndTime`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `sleep_date` (`pinType` INTEGER NOT NULL, `timezoneOffset` INTEGER NOT NULL, `date` TEXT NOT NULL, `goalMinutes` INTEGER NOT NULL, `sleepMinutes` INTEGER NOT NULL, `sleepStateDistInMinute` TEXT, `createdAt` INTEGER, `updatedAt` INTEGER, PRIMARY KEY(`date`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `sleep_settings` (`id` INTEGER NOT NULL, `sleepGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `sleepRecommendedGoals` (`id` INTEGER NOT NULL, `recommendedSleepGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `sleep_statistic` (`id` TEXT NOT NULL, `uid` TEXT NOT NULL, `sleepTimeBestDay` TEXT, `sleepTimeBestStreak` TEXT, `totalDays` INTEGER NOT NULL, `totalSleeps` INTEGER NOT NULL, `totalSleepMinutes` INTEGER NOT NULL, `totalSleepStateDistInMinute` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '6563ab2be16ee1f30194e9dfab2b7513')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `sleep_session`");
            ggVar.b("DROP TABLE IF EXISTS `sleep_date`");
            ggVar.b("DROP TABLE IF EXISTS `sleep_settings`");
            ggVar.b("DROP TABLE IF EXISTS `sleepRecommendedGoals`");
            ggVar.b("DROP TABLE IF EXISTS `sleep_statistic`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (SleepDatabase_Impl.this.mCallbacks != null) {
                int size = SleepDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) SleepDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = SleepDatabase_Impl.this.mDatabase = ggVar;
            SleepDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (SleepDatabase_Impl.this.mCallbacks != null) {
                int size = SleepDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) SleepDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            HashMap hashMap = new HashMap(21);
            hashMap.put("pinType", new eg.a("pinType", "INTEGER", true, 0));
            hashMap.put("date", new eg.a("date", "INTEGER", true, 0));
            hashMap.put("day", new eg.a("day", "TEXT", true, 0));
            hashMap.put(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, new eg.a(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, "TEXT", false, 0));
            hashMap.put("syncTime", new eg.a("syncTime", "INTEGER", false, 0));
            hashMap.put(MFSleepSession.COLUMN_BOOKMARK_TIME, new eg.a(MFSleepSession.COLUMN_BOOKMARK_TIME, "INTEGER", false, 0));
            hashMap.put(MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY, new eg.a(MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY, "REAL", true, 0));
            hashMap.put("source", new eg.a("source", "INTEGER", true, 0));
            hashMap.put(MFSleepSession.COLUMN_REAL_START_TIME, new eg.a(MFSleepSession.COLUMN_REAL_START_TIME, "INTEGER", true, 0));
            hashMap.put(MFSleepSession.COLUMN_REAL_END_TIME, new eg.a(MFSleepSession.COLUMN_REAL_END_TIME, "INTEGER", true, 1));
            hashMap.put(MFSleepSession.COLUMN_REAL_SLEEP_MINUTES, new eg.a(MFSleepSession.COLUMN_REAL_SLEEP_MINUTES, "INTEGER", true, 0));
            hashMap.put(MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE, new eg.a(MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE, "TEXT", true, 0));
            hashMap.put(MFSleepSession.COLUMN_EDITED_START_TIME, new eg.a(MFSleepSession.COLUMN_EDITED_START_TIME, "INTEGER", false, 0));
            hashMap.put(MFSleepSession.COLUMN_EDITED_END_TIME, new eg.a(MFSleepSession.COLUMN_EDITED_END_TIME, "INTEGER", false, 0));
            hashMap.put(MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES, new eg.a(MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES, "INTEGER", false, 0));
            hashMap.put(MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE, new eg.a(MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE, "TEXT", false, 0));
            hashMap.put(MFSleepSession.COLUMN_SLEEP_STATES, new eg.a(MFSleepSession.COLUMN_SLEEP_STATES, "TEXT", true, 0));
            hashMap.put("heartRate", new eg.a("heartRate", "TEXT", false, 0));
            hashMap.put("createdAt", new eg.a("createdAt", "INTEGER", true, 0));
            hashMap.put("updatedAt", new eg.a("updatedAt", "INTEGER", true, 0));
            hashMap.put("timezoneOffset", new eg.a("timezoneOffset", "INTEGER", true, 0));
            eg egVar = new eg(MFSleepSession.TABLE_NAME, hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar, MFSleepSession.TABLE_NAME);
            if (egVar.equals(a)) {
                HashMap hashMap2 = new HashMap(8);
                hashMap2.put("pinType", new eg.a("pinType", "INTEGER", true, 0));
                hashMap2.put("timezoneOffset", new eg.a("timezoneOffset", "INTEGER", true, 0));
                hashMap2.put("date", new eg.a("date", "TEXT", true, 1));
                hashMap2.put(MFSleepDay.COLUMN_GOAL_MINUTES, new eg.a(MFSleepDay.COLUMN_GOAL_MINUTES, "INTEGER", true, 0));
                hashMap2.put(MFSleepDay.COLUMN_SLEEP_MINUTES, new eg.a(MFSleepDay.COLUMN_SLEEP_MINUTES, "INTEGER", true, 0));
                hashMap2.put(MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE, new eg.a(MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE, "TEXT", false, 0));
                hashMap2.put("createdAt", new eg.a("createdAt", "INTEGER", false, 0));
                hashMap2.put("updatedAt", new eg.a("updatedAt", "INTEGER", false, 0));
                eg egVar2 = new eg(MFSleepDay.TABLE_NAME, hashMap2, new HashSet(0), new HashSet(0));
                eg a2 = eg.a(ggVar, MFSleepDay.TABLE_NAME);
                if (egVar2.equals(a2)) {
                    HashMap hashMap3 = new HashMap(2);
                    hashMap3.put("id", new eg.a("id", "INTEGER", true, 1));
                    hashMap3.put("sleepGoal", new eg.a("sleepGoal", "INTEGER", true, 0));
                    eg egVar3 = new eg("sleep_settings", hashMap3, new HashSet(0), new HashSet(0));
                    eg a3 = eg.a(ggVar, "sleep_settings");
                    if (egVar3.equals(a3)) {
                        HashMap hashMap4 = new HashMap(2);
                        hashMap4.put("id", new eg.a("id", "INTEGER", true, 1));
                        hashMap4.put("recommendedSleepGoal", new eg.a("recommendedSleepGoal", "INTEGER", true, 0));
                        eg egVar4 = new eg("sleepRecommendedGoals", hashMap4, new HashSet(0), new HashSet(0));
                        eg a4 = eg.a(ggVar, "sleepRecommendedGoals");
                        if (egVar4.equals(a4)) {
                            HashMap hashMap5 = new HashMap(10);
                            hashMap5.put("id", new eg.a("id", "TEXT", true, 1));
                            hashMap5.put("uid", new eg.a("uid", "TEXT", true, 0));
                            hashMap5.put("sleepTimeBestDay", new eg.a("sleepTimeBestDay", "TEXT", false, 0));
                            hashMap5.put("sleepTimeBestStreak", new eg.a("sleepTimeBestStreak", "TEXT", false, 0));
                            hashMap5.put("totalDays", new eg.a("totalDays", "INTEGER", true, 0));
                            hashMap5.put("totalSleeps", new eg.a("totalSleeps", "INTEGER", true, 0));
                            hashMap5.put("totalSleepMinutes", new eg.a("totalSleepMinutes", "INTEGER", true, 0));
                            hashMap5.put("totalSleepStateDistInMinute", new eg.a("totalSleepStateDistInMinute", "TEXT", true, 0));
                            hashMap5.put("createdAt", new eg.a("createdAt", "INTEGER", true, 0));
                            hashMap5.put("updatedAt", new eg.a("updatedAt", "INTEGER", true, 0));
                            eg egVar5 = new eg(SleepStatistic.TABLE_NAME, hashMap5, new HashSet(0), new HashSet(0));
                            eg a5 = eg.a(ggVar, SleepStatistic.TABLE_NAME);
                            if (!egVar5.equals(a5)) {
                                throw new IllegalStateException("Migration didn't properly handle sleep_statistic(com.portfolio.platform.data.SleepStatistic).\n Expected:\n" + egVar5 + "\n Found:\n" + a5);
                            }
                            return;
                        }
                        throw new IllegalStateException("Migration didn't properly handle sleepRecommendedGoals(com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal).\n Expected:\n" + egVar4 + "\n Found:\n" + a4);
                    }
                    throw new IllegalStateException("Migration didn't properly handle sleep_settings(com.portfolio.platform.data.model.room.sleep.MFSleepSettings).\n Expected:\n" + egVar3 + "\n Found:\n" + a3);
                }
                throw new IllegalStateException("Migration didn't properly handle sleep_date(com.portfolio.platform.data.model.room.sleep.MFSleepDay).\n Expected:\n" + egVar2 + "\n Found:\n" + a2);
            }
            throw new IllegalStateException("Migration didn't properly handle sleep_session(com.portfolio.platform.data.model.room.sleep.MFSleepSession).\n Expected:\n" + egVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `sleep_session`");
            a.b("DELETE FROM `sleep_date`");
            a.b("DELETE FROM `sleep_settings`");
            a.b("DELETE FROM `sleepRecommendedGoals`");
            a.b("DELETE FROM `sleep_statistic`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), MFSleepSession.TABLE_NAME, MFSleepDay.TABLE_NAME, "sleep_settings", "sleepRecommendedGoals", SleepStatistic.TABLE_NAME);
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(9), "6563ab2be16ee1f30194e9dfab2b7513", "318ff9dd57f69c3e478563bc453dc851");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }

    @DexIgnore
    public SleepDao sleepDao() {
        SleepDao sleepDao;
        if (this._sleepDao != null) {
            return this._sleepDao;
        }
        synchronized (this) {
            if (this._sleepDao == null) {
                this._sleepDao = new SleepDao_Impl(this);
            }
            sleepDao = this._sleepDao;
        }
        return sleepDao;
    }
}
