package com.portfolio.platform.data.source.remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource$getServerSettingList$1", mo27670f = "ServerSettingRemoteDataSource.kt", mo27671l = {38}, mo27672m = "invokeSuspend")
public final class ServerSettingRemoteDataSource$getServerSettingList$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList $callback;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21131p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ServerSettingRemoteDataSource$getServerSettingList$1(com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource serverSettingRemoteDataSource, com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = serverSettingRemoteDataSource;
        this.$callback = onGetServerSettingList;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource$getServerSettingList$1 serverSettingRemoteDataSource$getServerSettingList$1 = new com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource$getServerSettingList$1(this.this$0, this.$callback, yb4);
        serverSettingRemoteDataSource$getServerSettingList$1.f21131p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return serverSettingRemoteDataSource$getServerSettingList$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource$getServerSettingList$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21131p$;
            com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource$getServerSettingList$1$response$1 serverSettingRemoteDataSource$getServerSettingList$1$response$1 = new com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource$getServerSettingList$1$response$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.portfolio.platform.response.ResponseKt.m32232a(serverSettingRemoteDataSource$getServerSettingList$1$response$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
        if (qo2 instanceof com.fossil.blesdk.obfuscated.ro2) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource.Companion.getTAG$app_fossilRelease(), "getServerSettingList- is successful");
            this.$callback.onSuccess((com.portfolio.platform.data.model.ServerSettingList) ((com.fossil.blesdk.obfuscated.ro2) qo2).mo30673a());
        } else if (qo2 instanceof com.fossil.blesdk.obfuscated.po2) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource.Companion.getTAG$app_fossilRelease();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("getServerSettingList - Failure, code=");
            com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
            sb.append(po2.mo30176a());
            local.mo33256e(tAG$app_fossilRelease, sb.toString());
            if (po2.mo30179d() instanceof java.net.SocketTimeoutException) {
                this.$callback.onFailed(com.misfit.frameworks.common.constants.MFNetworkReturnCode.CLIENT_TIMEOUT);
            } else {
                this.$callback.onFailed(601);
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
