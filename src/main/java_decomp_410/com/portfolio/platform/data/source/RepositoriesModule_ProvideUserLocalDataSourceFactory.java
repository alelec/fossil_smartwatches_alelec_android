package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RepositoriesModule_ProvideUserLocalDataSourceFactory implements Factory<UserDataSource> {
    @DexIgnore
    public /* final */ RepositoriesModule module;

    @DexIgnore
    public RepositoriesModule_ProvideUserLocalDataSourceFactory(RepositoriesModule repositoriesModule) {
        this.module = repositoriesModule;
    }

    @DexIgnore
    public static RepositoriesModule_ProvideUserLocalDataSourceFactory create(RepositoriesModule repositoriesModule) {
        return new RepositoriesModule_ProvideUserLocalDataSourceFactory(repositoriesModule);
    }

    @DexIgnore
    public static UserDataSource provideInstance(RepositoriesModule repositoriesModule) {
        return proxyProvideUserLocalDataSource(repositoriesModule);
    }

    @DexIgnore
    public static UserDataSource proxyProvideUserLocalDataSource(RepositoriesModule repositoriesModule) {
        UserDataSource provideUserLocalDataSource = repositoriesModule.provideUserLocalDataSource();
        n44.a(provideUserLocalDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideUserLocalDataSource;
    }

    @DexIgnore
    public UserDataSource get() {
        return provideInstance(this.module);
    }
}
