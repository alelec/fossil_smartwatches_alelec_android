package com.portfolio.platform.data.source;

import com.portfolio.platform.data.legacy.threedotzero.UAppSystemVersionLocalDataSource;
import com.portfolio.platform.data.source.scope.Local;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class UAppSystemVersionRepositoryModule {
    @DexIgnore
    @Local
    public UAppSystemVersionDataSource provideUserLocalDataSource() {
        return new UAppSystemVersionLocalDataSource();
    }
}
