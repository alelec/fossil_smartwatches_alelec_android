package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.b72;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.l72;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.fossil.blesdk.obfuscated.x62;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitySummaryDao_Impl extends ActivitySummaryDao {
    @DexIgnore
    public /* final */ x62 __activityStatisticConverter; // = new x62();
    @DexIgnore
    public /* final */ b72 __dateTimeConverter; // = new b72();
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfActivityRecommendedGoals;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfActivitySettings;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfActivityStatistic;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfActivitySummary;
    @DexIgnore
    public /* final */ l72 __integerArrayConverter; // = new l72();
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeleteAllActivitySummaries;
    @DexIgnore
    public /* final */ wf __preparedStmtOfUpdateActivitySettings;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<ActivitySummary> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sampleday`(`createdAt`,`updatedAt`,`pinType`,`year`,`month`,`day`,`timezoneName`,`dstOffset`,`steps`,`calories`,`distance`,`intensities`,`activeTime`,`stepGoal`,`caloriesGoal`,`activeTimeGoal`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, ActivitySummary activitySummary) {
            kgVar.b(1, ActivitySummaryDao_Impl.this.__dateTimeConverter.a(activitySummary.getCreatedAt()));
            kgVar.b(2, ActivitySummaryDao_Impl.this.__dateTimeConverter.a(activitySummary.getUpdatedAt()));
            kgVar.b(3, (long) activitySummary.getPinType());
            kgVar.b(4, (long) activitySummary.getYear());
            kgVar.b(5, (long) activitySummary.getMonth());
            kgVar.b(6, (long) activitySummary.getDay());
            if (activitySummary.getTimezoneName() == null) {
                kgVar.a(7);
            } else {
                kgVar.a(7, activitySummary.getTimezoneName());
            }
            if (activitySummary.getDstOffset() == null) {
                kgVar.a(8);
            } else {
                kgVar.b(8, (long) activitySummary.getDstOffset().intValue());
            }
            kgVar.a(9, activitySummary.getSteps());
            kgVar.a(10, activitySummary.getCalories());
            kgVar.a(11, activitySummary.getDistance());
            String a = ActivitySummaryDao_Impl.this.__integerArrayConverter.a(activitySummary.getIntensities());
            if (a == null) {
                kgVar.a(12);
            } else {
                kgVar.a(12, a);
            }
            kgVar.b(13, (long) activitySummary.getActiveTime());
            kgVar.b(14, (long) activitySummary.getStepGoal());
            kgVar.b(15, (long) activitySummary.getCaloriesGoal());
            kgVar.b(16, (long) activitySummary.getActiveTimeGoal());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<ActivityStatistic> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon10(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public ActivityStatistic call() throws Exception {
            ActivityStatistic activityStatistic;
            Cursor a = bg.a(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "id");
                int b2 = ag.b(a, "uid");
                int b3 = ag.b(a, "activeTimeBestDay");
                int b4 = ag.b(a, "activeTimeBestStreak");
                int b5 = ag.b(a, "caloriesBestDay");
                int b6 = ag.b(a, "caloriesBestStreak");
                int b7 = ag.b(a, "stepsBestDay");
                int b8 = ag.b(a, "stepsBestStreak");
                int b9 = ag.b(a, "totalActiveTime");
                int b10 = ag.b(a, "totalCalories");
                int b11 = ag.b(a, "totalDays");
                int b12 = ag.b(a, "totalDistance");
                int b13 = ag.b(a, "totalSteps");
                int b14 = ag.b(a, "totalIntensityDistInStep");
                int b15 = ag.b(a, "createdAt");
                int b16 = ag.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    activityStatistic = new ActivityStatistic(a.getString(b), a.getString(b2), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b3)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b4)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.b(a.getString(b5)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b6)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b7)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b8)), a.getInt(b9), a.getDouble(b10), a.getInt(b11), a.getDouble(b12), a.getInt(b13), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(a.getString(b14)), ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(b15)), ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(b16)));
                } else {
                    activityStatistic = null;
                }
                return activityStatistic;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends lf<ActivitySettings> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activitySettings`(`id`,`currentStepGoal`,`currentCaloriesGoal`,`currentActiveTimeGoal`) VALUES (?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, ActivitySettings activitySettings) {
            kgVar.b(1, (long) activitySettings.getId());
            kgVar.b(2, (long) activitySettings.getCurrentStepGoal());
            kgVar.b(3, (long) activitySettings.getCurrentCaloriesGoal());
            kgVar.b(4, (long) activitySettings.getCurrentActiveTimeGoal());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends lf<ActivityRecommendedGoals> {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activityRecommendedGoals`(`id`,`recommendedStepsGoal`,`recommendedCaloriesGoal`,`recommendedActiveTimeGoal`) VALUES (?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, ActivityRecommendedGoals activityRecommendedGoals) {
            kgVar.b(1, (long) activityRecommendedGoals.getId());
            kgVar.b(2, (long) activityRecommendedGoals.getRecommendedStepsGoal());
            kgVar.b(3, (long) activityRecommendedGoals.getRecommendedCaloriesGoal());
            kgVar.b(4, (long) activityRecommendedGoals.getRecommendedActiveTimeGoal());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends lf<ActivityStatistic> {
        @DexIgnore
        public Anon4(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activity_statistic`(`id`,`uid`,`activeTimeBestDay`,`activeTimeBestStreak`,`caloriesBestDay`,`caloriesBestStreak`,`stepsBestDay`,`stepsBestStreak`,`totalActiveTime`,`totalCalories`,`totalDays`,`totalDistance`,`totalSteps`,`totalIntensityDistInStep`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, ActivityStatistic activityStatistic) {
            if (activityStatistic.getId() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, activityStatistic.getId());
            }
            if (activityStatistic.getUid() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, activityStatistic.getUid());
            }
            String a = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getActiveTimeBestDay());
            if (a == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, a);
            }
            String a2 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getActiveTimeBestStreak());
            if (a2 == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, a2);
            }
            String a3 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getCaloriesBestDay());
            if (a3 == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, a3);
            }
            String a4 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getCaloriesBestStreak());
            if (a4 == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, a4);
            }
            String a5 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getStepsBestDay());
            if (a5 == null) {
                kgVar.a(7);
            } else {
                kgVar.a(7, a5);
            }
            String a6 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getStepsBestStreak());
            if (a6 == null) {
                kgVar.a(8);
            } else {
                kgVar.a(8, a6);
            }
            kgVar.b(9, (long) activityStatistic.getTotalActiveTime());
            kgVar.a(10, activityStatistic.getTotalCalories());
            kgVar.b(11, (long) activityStatistic.getTotalDays());
            kgVar.a(12, activityStatistic.getTotalDistance());
            kgVar.b(13, (long) activityStatistic.getTotalSteps());
            String a7 = ActivitySummaryDao_Impl.this.__integerArrayConverter.a(activityStatistic.getTotalIntensityDistInStep());
            if (a7 == null) {
                kgVar.a(14);
            } else {
                kgVar.a(14, a7);
            }
            kgVar.b(15, ActivitySummaryDao_Impl.this.__dateTimeConverter.a(activityStatistic.getCreatedAt()));
            kgVar.b(16, ActivitySummaryDao_Impl.this.__dateTimeConverter.a(activityStatistic.getUpdatedAt()));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends wf {
        @DexIgnore
        public Anon5(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM sampleday";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends wf {
        @DexIgnore
        public Anon6(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE activitySettings SET currentStepGoal = ?, currentCaloriesGoal = ?, currentActiveTimeGoal = ?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements Callable<ActivitySummary> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon7(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r19v0, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r19v1, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r19v3, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r25v0, resolved type: java.lang.Integer} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r19v4, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r19v5, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
        /* JADX WARNING: type inference failed for: r19v2, types: [java.lang.Integer] */
        /* JADX WARNING: Multi-variable type inference failed */
        public ActivitySummary call() throws Exception {
            Cursor a = bg.a(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "createdAt");
                int b2 = ag.b(a, "updatedAt");
                int b3 = ag.b(a, "pinType");
                int b4 = ag.b(a, "year");
                int b5 = ag.b(a, "month");
                int b6 = ag.b(a, "day");
                int b7 = ag.b(a, SampleDay.COLUMN_TIMEZONE_NAME);
                int b8 = ag.b(a, SampleDay.COLUMN_DST_OFFSET);
                int b9 = ag.b(a, "steps");
                int b10 = ag.b(a, "calories");
                int b11 = ag.b(a, "distance");
                int b12 = ag.b(a, SampleDay.COLUMN_INTENSITIES);
                int b13 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME);
                int b14 = ag.b(a, SampleDay.COLUMN_STEP_GOAL);
                int i = b3;
                int b15 = ag.b(a, SampleDay.COLUMN_CALORIES_GOAL);
                int i2 = b2;
                int b16 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                ActivitySummary activitySummary = null;
                if (a.moveToFirst()) {
                    int i3 = a.getInt(b4);
                    int i4 = a.getInt(b5);
                    int i5 = a.getInt(b6);
                    String string = a.getString(b7);
                    if (!a.isNull(b8)) {
                        activitySummary = Integer.valueOf(a.getInt(b8));
                    }
                    Integer num = activitySummary;
                    ActivitySummary activitySummary2 = new ActivitySummary(i3, i4, i5, string, num, a.getDouble(b9), a.getDouble(b10), a.getDouble(b11), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(a.getString(b12)), a.getInt(b13), a.getInt(b14), a.getInt(b15), a.getInt(b16));
                    activitySummary2.setCreatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(b)));
                    activitySummary2.setUpdatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(i2)));
                    activitySummary2.setPinType(a.getInt(i));
                    activitySummary = activitySummary2;
                }
                return activitySummary;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<List<ActivitySummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon8(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<ActivitySummary> call() throws Exception {
            Integer valueOf;
            Cursor a = bg.a(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "createdAt");
                int b2 = ag.b(a, "updatedAt");
                int b3 = ag.b(a, "pinType");
                int b4 = ag.b(a, "year");
                int b5 = ag.b(a, "month");
                int b6 = ag.b(a, "day");
                int b7 = ag.b(a, SampleDay.COLUMN_TIMEZONE_NAME);
                int b8 = ag.b(a, SampleDay.COLUMN_DST_OFFSET);
                int b9 = ag.b(a, "steps");
                int b10 = ag.b(a, "calories");
                int b11 = ag.b(a, "distance");
                int b12 = ag.b(a, SampleDay.COLUMN_INTENSITIES);
                int b13 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME);
                int b14 = ag.b(a, SampleDay.COLUMN_STEP_GOAL);
                int i = b3;
                int b15 = ag.b(a, SampleDay.COLUMN_CALORIES_GOAL);
                int i2 = b2;
                int i3 = b;
                int b16 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i4 = a.getInt(b4);
                    int i5 = a.getInt(b5);
                    int i6 = a.getInt(b6);
                    String string = a.getString(b7);
                    if (a.isNull(b8)) {
                        valueOf = null;
                    } else {
                        valueOf = Integer.valueOf(a.getInt(b8));
                    }
                    Integer num = valueOf;
                    int i7 = b4;
                    int i8 = b16;
                    ActivitySummary activitySummary = new ActivitySummary(i4, i5, i6, string, num, a.getDouble(b9), a.getDouble(b10), a.getDouble(b11), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(a.getString(b12)), a.getInt(b13), a.getInt(b14), a.getInt(b15), a.getInt(i8));
                    b16 = i8;
                    int i9 = b6;
                    int i10 = i3;
                    int i11 = b5;
                    int i12 = i10;
                    activitySummary.setCreatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(i10)));
                    int i13 = i2;
                    i2 = i13;
                    activitySummary.setUpdatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(i13)));
                    int i14 = i;
                    activitySummary.setPinType(a.getInt(i14));
                    arrayList.add(activitySummary);
                    i = i14;
                    b5 = i11;
                    b6 = i9;
                    i3 = i12;
                    b4 = i7;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 implements Callable<ActivitySettings> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon9(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public ActivitySettings call() throws Exception {
            ActivitySettings activitySettings;
            Cursor a = bg.a(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "id");
                int b2 = ag.b(a, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_STEP_GOAL);
                int b3 = ag.b(a, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_CALORIES_GOAL);
                int b4 = ag.b(a, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_ACTIVE_TIME_GOAL);
                if (a.moveToFirst()) {
                    activitySettings = new ActivitySettings(a.getInt(b2), a.getInt(b3), a.getInt(b4));
                    activitySettings.setId(a.getInt(b));
                } else {
                    activitySettings = null;
                }
                return activitySettings;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public ActivitySummaryDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfActivitySummary = new Anon1(roomDatabase);
        this.__insertionAdapterOfActivitySettings = new Anon2(roomDatabase);
        this.__insertionAdapterOfActivityRecommendedGoals = new Anon3(roomDatabase);
        this.__insertionAdapterOfActivityStatistic = new Anon4(roomDatabase);
        this.__preparedStmtOfDeleteAllActivitySummaries = new Anon5(roomDatabase);
        this.__preparedStmtOfUpdateActivitySettings = new Anon6(roomDatabase);
    }

    @DexIgnore
    public void deleteAllActivitySummaries() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeleteAllActivitySummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySummaries.release(acquire);
        }
    }

    @DexIgnore
    public ActivitySettings getActivitySetting() {
        ActivitySettings activitySettings;
        uf b = uf.b("SELECT * FROM activitySettings LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_STEP_GOAL);
            int b4 = ag.b(a, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_CALORIES_GOAL);
            int b5 = ag.b(a, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_ACTIVE_TIME_GOAL);
            if (a.moveToFirst()) {
                activitySettings = new ActivitySettings(a.getInt(b3), a.getInt(b4), a.getInt(b5));
                activitySettings.setId(a.getInt(b2));
            } else {
                activitySettings = null;
            }
            return activitySettings;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<ActivitySettings> getActivitySettingLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{com.fossil.wearables.fsl.fitness.ActivitySettings.TABLE_NAME}, false, new Anon9(uf.b("SELECT * FROM activitySettings LIMIT 1", 0)));
    }

    @DexIgnore
    public ActivityStatistic getActivityStatistic() {
        uf ufVar;
        ActivityStatistic activityStatistic;
        uf b = uf.b("SELECT * FROM activity_statistic LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "uid");
            int b4 = ag.b(a, "activeTimeBestDay");
            int b5 = ag.b(a, "activeTimeBestStreak");
            int b6 = ag.b(a, "caloriesBestDay");
            int b7 = ag.b(a, "caloriesBestStreak");
            int b8 = ag.b(a, "stepsBestDay");
            int b9 = ag.b(a, "stepsBestStreak");
            int b10 = ag.b(a, "totalActiveTime");
            int b11 = ag.b(a, "totalCalories");
            int b12 = ag.b(a, "totalDays");
            int b13 = ag.b(a, "totalDistance");
            int b14 = ag.b(a, "totalSteps");
            ufVar = b;
            try {
                int b15 = ag.b(a, "totalIntensityDistInStep");
                int b16 = ag.b(a, "createdAt");
                int b17 = ag.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    activityStatistic = new ActivityStatistic(a.getString(b2), a.getString(b3), this.__activityStatisticConverter.a(a.getString(b4)), this.__activityStatisticConverter.a(a.getString(b5)), this.__activityStatisticConverter.b(a.getString(b6)), this.__activityStatisticConverter.a(a.getString(b7)), this.__activityStatisticConverter.a(a.getString(b8)), this.__activityStatisticConverter.a(a.getString(b9)), a.getInt(b10), a.getDouble(b11), a.getInt(b12), a.getDouble(b13), a.getInt(b14), this.__integerArrayConverter.a(a.getString(b15)), this.__dateTimeConverter.a(a.getLong(b16)), this.__dateTimeConverter.a(a.getLong(b17)));
                } else {
                    activityStatistic = null;
                }
                a.close();
                ufVar.c();
                return activityStatistic;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public LiveData<ActivityStatistic> getActivityStatisticLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{ActivityStatistic.TABLE_NAME}, false, new Anon10(uf.b("SELECT * FROM activity_statistic LIMIT 1", 0)));
    }

    @DexIgnore
    public List<ActivitySummary> getActivitySummariesDesc(int i, int i2, int i3, int i4, int i5, int i6) {
        uf ufVar;
        Integer valueOf;
        uf b = uf.b("SELECT * FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ORDER BY year DESC, month DESC, day DESC\n    ", 12);
        long j = (long) i3;
        b.b(1, j);
        b.b(2, j);
        long j2 = (long) i2;
        b.b(3, j2);
        b.b(4, j);
        b.b(5, j2);
        b.b(6, (long) i);
        long j3 = (long) i6;
        b.b(7, j3);
        b.b(8, j3);
        long j4 = (long) i5;
        b.b(9, j4);
        b.b(10, j3);
        b.b(11, j4);
        b.b(12, (long) i4);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "createdAt");
            int b3 = ag.b(a, "updatedAt");
            int b4 = ag.b(a, "pinType");
            int b5 = ag.b(a, "year");
            int b6 = ag.b(a, "month");
            int b7 = ag.b(a, "day");
            int b8 = ag.b(a, SampleDay.COLUMN_TIMEZONE_NAME);
            int b9 = ag.b(a, SampleDay.COLUMN_DST_OFFSET);
            int b10 = ag.b(a, "steps");
            int b11 = ag.b(a, "calories");
            int b12 = ag.b(a, "distance");
            int b13 = ag.b(a, SampleDay.COLUMN_INTENSITIES);
            int b14 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            ufVar = b;
            try {
                int b15 = ag.b(a, SampleDay.COLUMN_STEP_GOAL);
                int i7 = b4;
                int b16 = ag.b(a, SampleDay.COLUMN_CALORIES_GOAL);
                int i8 = b3;
                int i9 = b2;
                int b17 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i10 = a.getInt(b5);
                    int i11 = a.getInt(b6);
                    int i12 = a.getInt(b7);
                    String string = a.getString(b8);
                    if (a.isNull(b9)) {
                        valueOf = null;
                    } else {
                        valueOf = Integer.valueOf(a.getInt(b9));
                    }
                    Integer num = valueOf;
                    int i13 = b5;
                    int i14 = b17;
                    ActivitySummary activitySummary = new ActivitySummary(i10, i11, i12, string, num, a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), this.__integerArrayConverter.a(a.getString(b13)), a.getInt(b14), a.getInt(b15), a.getInt(b16), a.getInt(i14));
                    int i15 = b15;
                    int i16 = i14;
                    int i17 = i9;
                    int i18 = b16;
                    int i19 = i17;
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(a.getLong(i17)));
                    int i20 = i8;
                    i8 = i20;
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(a.getLong(i20)));
                    int i21 = i7;
                    activitySummary.setPinType(a.getInt(i21));
                    arrayList.add(activitySummary);
                    b16 = i18;
                    b5 = i13;
                    i7 = i21;
                    i9 = i19;
                    b15 = i15;
                    b17 = i16;
                }
                a.close();
                ufVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public LiveData<List<ActivitySummary>> getActivitySummariesLiveData(int i, int i2, int i3, int i4, int i5, int i6) {
        uf b = uf.b("SELECT * FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ORDER BY year ASC, month ASC, day ASC\n    ", 12);
        long j = (long) i3;
        b.b(1, j);
        b.b(2, j);
        long j2 = (long) i2;
        b.b(3, j2);
        b.b(4, j);
        b.b(5, j2);
        b.b(6, (long) i);
        long j3 = (long) i6;
        b.b(7, j3);
        b.b(8, j3);
        long j4 = (long) i5;
        b.b(9, j4);
        b.b(10, j3);
        b.b(11, j4);
        b.b(12, (long) i4);
        return this.__db.getInvalidationTracker().a(new String[]{SampleDay.TABLE_NAME}, false, new Anon8(b));
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r18v0, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r18v1, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r18v3, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v0, resolved type: java.lang.Integer} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r18v4, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r18v5, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX WARNING: type inference failed for: r18v2, types: [java.lang.Integer] */
    /* JADX WARNING: Multi-variable type inference failed */
    public ActivitySummary getActivitySummary(int i, int i2, int i3) {
        uf ufVar;
        uf b = uf.b("SELECT * FROM sampleday WHERE year = ? AND month = ? AND day = ? LIMIT 1", 3);
        b.b(1, (long) i);
        b.b(2, (long) i2);
        b.b(3, (long) i3);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "createdAt");
            int b3 = ag.b(a, "updatedAt");
            int b4 = ag.b(a, "pinType");
            int b5 = ag.b(a, "year");
            int b6 = ag.b(a, "month");
            int b7 = ag.b(a, "day");
            int b8 = ag.b(a, SampleDay.COLUMN_TIMEZONE_NAME);
            int b9 = ag.b(a, SampleDay.COLUMN_DST_OFFSET);
            int b10 = ag.b(a, "steps");
            int b11 = ag.b(a, "calories");
            int b12 = ag.b(a, "distance");
            int b13 = ag.b(a, SampleDay.COLUMN_INTENSITIES);
            int b14 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            ufVar = b;
            try {
                int b15 = ag.b(a, SampleDay.COLUMN_STEP_GOAL);
                int i4 = b4;
                int b16 = ag.b(a, SampleDay.COLUMN_CALORIES_GOAL);
                int i5 = b3;
                int b17 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                ActivitySummary activitySummary = null;
                if (a.moveToFirst()) {
                    int i6 = a.getInt(b5);
                    int i7 = a.getInt(b6);
                    int i8 = a.getInt(b7);
                    String string = a.getString(b8);
                    if (!a.isNull(b9)) {
                        activitySummary = Integer.valueOf(a.getInt(b9));
                    }
                    Integer num = activitySummary;
                    ActivitySummary activitySummary2 = new ActivitySummary(i6, i7, i8, string, num, a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), this.__integerArrayConverter.a(a.getString(b13)), a.getInt(b14), a.getInt(b15), a.getInt(b16), a.getInt(b17));
                    activitySummary2.setCreatedAt(this.__dateTimeConverter.a(a.getLong(b2)));
                    activitySummary2.setUpdatedAt(this.__dateTimeConverter.a(a.getLong(i5)));
                    activitySummary2.setPinType(a.getInt(i4));
                    activitySummary = activitySummary2;
                }
                a.close();
                ufVar.c();
                return activitySummary;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public LiveData<ActivitySummary> getActivitySummaryLiveData(int i, int i2, int i3) {
        uf b = uf.b("SELECT * FROM sampleday WHERE year = ? AND month = ? AND day = ?", 3);
        b.b(1, (long) i);
        b.b(2, (long) i2);
        b.b(3, (long) i3);
        return this.__db.getInvalidationTracker().a(new String[]{SampleDay.TABLE_NAME}, false, new Anon7(b));
    }

    @DexIgnore
    public ActivitySummary getLastSummary() {
        uf ufVar;
        ActivitySummary activitySummary;
        uf b = uf.b("SELECT * FROM sampleday ORDER BY year ASC, month ASC, day ASC LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "createdAt");
            int b3 = ag.b(a, "updatedAt");
            int b4 = ag.b(a, "pinType");
            int b5 = ag.b(a, "year");
            int b6 = ag.b(a, "month");
            int b7 = ag.b(a, "day");
            int b8 = ag.b(a, SampleDay.COLUMN_TIMEZONE_NAME);
            int b9 = ag.b(a, SampleDay.COLUMN_DST_OFFSET);
            int b10 = ag.b(a, "steps");
            int b11 = ag.b(a, "calories");
            int b12 = ag.b(a, "distance");
            int b13 = ag.b(a, SampleDay.COLUMN_INTENSITIES);
            int b14 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            ufVar = b;
            try {
                int b15 = ag.b(a, SampleDay.COLUMN_STEP_GOAL);
                int i = b4;
                int b16 = ag.b(a, SampleDay.COLUMN_CALORIES_GOAL);
                int i2 = b3;
                int b17 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                Integer num = null;
                if (a.moveToFirst()) {
                    int i3 = a.getInt(b5);
                    int i4 = a.getInt(b6);
                    int i5 = a.getInt(b7);
                    String string = a.getString(b8);
                    if (!a.isNull(b9)) {
                        num = Integer.valueOf(a.getInt(b9));
                    }
                    activitySummary = new ActivitySummary(i3, i4, i5, string, num, a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), this.__integerArrayConverter.a(a.getString(b13)), a.getInt(b14), a.getInt(b15), a.getInt(b16), a.getInt(b17));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(a.getLong(b2)));
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(a.getLong(i2)));
                    activitySummary.setPinType(a.getInt(i));
                } else {
                    activitySummary = null;
                }
                a.close();
                ufVar.c();
                return activitySummary;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r18v0, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r18v1, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r18v3, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v0, resolved type: java.lang.Integer} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r18v4, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r18v5, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX WARNING: type inference failed for: r18v2, types: [java.lang.Integer] */
    /* JADX WARNING: Multi-variable type inference failed */
    public ActivitySummary getNearestSampleDayFromDate(int i, int i2, int i3) {
        uf ufVar;
        uf b = uf.b("SELECT * FROM sampleday\n        WHERE year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?) ORDER BY year DESC, month DESC, day DESC LIMIT 1", 6);
        long j = (long) i;
        b.b(1, j);
        b.b(2, j);
        long j2 = (long) i2;
        b.b(3, j2);
        b.b(4, j);
        b.b(5, j2);
        b.b(6, (long) i3);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "createdAt");
            int b3 = ag.b(a, "updatedAt");
            int b4 = ag.b(a, "pinType");
            int b5 = ag.b(a, "year");
            int b6 = ag.b(a, "month");
            int b7 = ag.b(a, "day");
            int b8 = ag.b(a, SampleDay.COLUMN_TIMEZONE_NAME);
            int b9 = ag.b(a, SampleDay.COLUMN_DST_OFFSET);
            int b10 = ag.b(a, "steps");
            int b11 = ag.b(a, "calories");
            int b12 = ag.b(a, "distance");
            int b13 = ag.b(a, SampleDay.COLUMN_INTENSITIES);
            int b14 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            ufVar = b;
            try {
                int b15 = ag.b(a, SampleDay.COLUMN_STEP_GOAL);
                int i4 = b4;
                int b16 = ag.b(a, SampleDay.COLUMN_CALORIES_GOAL);
                int i5 = b3;
                int b17 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                ActivitySummary activitySummary = null;
                if (a.moveToFirst()) {
                    int i6 = a.getInt(b5);
                    int i7 = a.getInt(b6);
                    int i8 = a.getInt(b7);
                    String string = a.getString(b8);
                    if (!a.isNull(b9)) {
                        activitySummary = Integer.valueOf(a.getInt(b9));
                    }
                    Integer num = activitySummary;
                    ActivitySummary activitySummary2 = new ActivitySummary(i6, i7, i8, string, num, a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), this.__integerArrayConverter.a(a.getString(b13)), a.getInt(b14), a.getInt(b15), a.getInt(b16), a.getInt(b17));
                    activitySummary2.setCreatedAt(this.__dateTimeConverter.a(a.getLong(b2)));
                    activitySummary2.setUpdatedAt(this.__dateTimeConverter.a(a.getLong(i5)));
                    activitySummary2.setPinType(a.getInt(i4));
                    activitySummary = activitySummary2;
                }
                a.close();
                ufVar.c();
                return activitySummary;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public ActivitySummary.TotalValuesOfWeek getTotalValuesOfWeek(int i, int i2, int i3, int i4, int i5, int i6) {
        uf b = uf.b("SELECT SUM(steps) as totalStepsOfWeek,  SUM(calories) as totalCaloriesOfWeek,  SUM(activeTime) as totalActiveTimeOfWeek FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ", 12);
        long j = (long) i3;
        b.b(1, j);
        b.b(2, j);
        long j2 = (long) i2;
        b.b(3, j2);
        b.b(4, j);
        b.b(5, j2);
        b.b(6, (long) i);
        long j3 = (long) i6;
        b.b(7, j3);
        b.b(8, j3);
        long j4 = (long) i5;
        b.b(9, j4);
        b.b(10, j3);
        b.b(11, j4);
        b.b(12, (long) i4);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            return a.moveToFirst() ? new ActivitySummary.TotalValuesOfWeek(a.getDouble(ag.b(a, "totalStepsOfWeek")), a.getDouble(ag.b(a, "totalCaloriesOfWeek")), a.getInt(ag.b(a, "totalActiveTimeOfWeek"))) : null;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public long insertActivitySettings(ActivitySettings activitySettings) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfActivitySettings.insertAndReturnId(activitySettings);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void updateActivitySettings(int i, int i2, int i3) {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfUpdateActivitySettings.acquire();
        acquire.b(1, (long) i);
        acquire.b(2, (long) i2);
        acquire.b(3, (long) i3);
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfUpdateActivitySettings.release(acquire);
        }
    }

    @DexIgnore
    public void upsertActivityRecommendedGoals(ActivityRecommendedGoals activityRecommendedGoals) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivityRecommendedGoals.insert(activityRecommendedGoals);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public long upsertActivityStatistic(ActivityStatistic activityStatistic) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfActivityStatistic.insertAndReturnId(activityStatistic);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertActivitySummaries(List<ActivitySummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivitySummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertActivitySummary(ActivitySummary activitySummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivitySummary.insert(activitySummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
