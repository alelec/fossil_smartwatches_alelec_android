package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository$getSleepStatistic$1 extends com.portfolio.platform.util.NetworkBoundResource<com.portfolio.platform.data.SleepStatistic, com.portfolio.platform.data.SleepStatistic> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSummariesRepository this$0;

    @DexIgnore
    public SleepSummariesRepository$getSleepStatistic$1(com.portfolio.platform.data.source.SleepSummariesRepository sleepSummariesRepository, boolean z) {
        this.this$0 = sleepSummariesRepository;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.SleepStatistic>> yb4) {
        return this.this$0.mApiService.getSleepStatistic(yb4);
    }

    @DexIgnore
    public androidx.lifecycle.LiveData<com.portfolio.platform.data.SleepStatistic> loadFromDb() {
        return this.this$0.mSleepDao.getSleepStatisticLiveData();
    }

    @DexIgnore
    public void onFetchFailed(java.lang.Throwable th) {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "getSleepStatistic - onFetchFailed");
    }

    @DexIgnore
    public void saveCallResult(com.portfolio.platform.data.SleepStatistic sleepStatistic) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(sleepStatistic, "item");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
        local.mo33255d(tAG$app_fossilRelease, "getSleepStatistic - saveCallResult -- item=" + sleepStatistic);
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$1$saveCallResult$1(this, sleepStatistic, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    public boolean shouldFetch(com.portfolio.platform.data.SleepStatistic sleepStatistic) {
        return this.$shouldFetch;
    }
}
