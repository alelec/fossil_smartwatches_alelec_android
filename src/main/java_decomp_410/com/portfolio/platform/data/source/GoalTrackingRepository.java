package com.portfolio.platform.data.source;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nd;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.tz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.UpsertApiResponse;
import com.portfolio.platform.helper.PagingRequestHelper;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import kotlin.NoWhenBranchMatchedException;
import kotlin.sequences.SequencesKt___SequencesKt;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;
    @DexIgnore
    public /* final */ GoalTrackingDao mGoalTrackingDao;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ en2 mSharedPreferencesManager;
    @DexIgnore
    public List<GoalTrackingDataSourceFactory> mSourceDataFactoryList; // = new ArrayList();
    @DexIgnore
    public List<GoalTrackingSummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();
    @DexIgnore
    public /* final */ UserRepository mUserRepository;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return GoalTrackingRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingGoalTrackingDataListCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(List<GoalTrackingData> list);
    }

    @DexIgnore
    public interface UpdateGoalSettingCallback {
        @DexIgnore
        void onFail(po2<GoalSetting> po2);

        @DexIgnore
        void onSuccess(ro2<GoalSetting> ro2);
    }

    /*
    static {
        String simpleName = GoalTrackingRepository.class.getSimpleName();
        kd4.a((Object) simpleName, "GoalTrackingRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public GoalTrackingRepository(GoalTrackingDatabase goalTrackingDatabase, GoalTrackingDao goalTrackingDao, UserRepository userRepository, en2 en2, ApiServiceV2 apiServiceV2) {
        kd4.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        kd4.b(goalTrackingDao, "mGoalTrackingDao");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(apiServiceV2, "mApiServiceV2");
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mGoalTrackingDao = goalTrackingDao;
        this.mUserRepository = userRepository;
        this.mSharedPreferencesManager = en2;
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    private final LiveData<List<GoalTrackingData>> getPendingGoalTrackingDataListLiveData(Date date, Date date2) {
        return this.mGoalTrackingDao.getPendingGoalTrackingDataListLiveData(date, date2);
    }

    @DexIgnore
    public static /* synthetic */ Object loadGoalTrackingDataList$default(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, int i, int i2, yb4 yb4, int i3, Object obj) {
        return goalTrackingRepository.loadGoalTrackingDataList(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, yb4);
    }

    @DexIgnore
    private final void removeDeletedGoalTrackingList(List<GoalTrackingData> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "removeDeletedGoalTrackingList goalTrackingDataList size= " + list.size());
        for (GoalTrackingData component1 : list) {
            this.mGoalTrackingDao.removeDeletedGoalTrackingData(component1.component1());
        }
    }

    @DexIgnore
    private final void updateGoalTrackingPinType(List<GoalTrackingData> list, int i) {
        for (GoalTrackingData pinType : list) {
            pinType.setPinType(i);
        }
        this.mGoalTrackingDao.upsertListGoalTrackingData(list);
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        removePagingListener();
        this.mGoalTrackingDao.deleteAllGoalTrackingSummaries();
        this.mGoalTrackingDao.deleteAllGoalTrackingData();
        this.mGoalTrackingDao.deleteGoalSetting();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object delete(List<GoalTrackingData> list, yb4<? super qo2<List<GoalTrackingData>>> yb4) {
        GoalTrackingRepository$delete$Anon1 goalTrackingRepository$delete$Anon1;
        int i;
        GoalTrackingRepository goalTrackingRepository;
        qo2 qo2;
        if (yb4 instanceof GoalTrackingRepository$delete$Anon1) {
            goalTrackingRepository$delete$Anon1 = (GoalTrackingRepository$delete$Anon1) yb4;
            int i2 = goalTrackingRepository$delete$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                goalTrackingRepository$delete$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = goalTrackingRepository$delete$Anon1.result;
                Object a = cc4.a();
                i = goalTrackingRepository$delete$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "deleteGoalTrackingDataList: sampleRawList =" + list.size());
                    tz1 tz1 = new tz1();
                    for (GoalTrackingData component1 : list) {
                        try {
                            tz1.a(component1.component1());
                        } catch (Exception e) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str2 = TAG;
                            local2.e(str2, "updateGoalSetting exception=" + e);
                            e.printStackTrace();
                        }
                    }
                    xz1 xz1 = new xz1();
                    xz1.a("_ids", (JsonElement) tz1);
                    GoalTrackingRepository$delete$repoResponse$Anon1 goalTrackingRepository$delete$repoResponse$Anon1 = new GoalTrackingRepository$delete$repoResponse$Anon1(this, xz1, (yb4) null);
                    goalTrackingRepository$delete$Anon1.L$Anon0 = this;
                    goalTrackingRepository$delete$Anon1.L$Anon1 = list;
                    goalTrackingRepository$delete$Anon1.L$Anon2 = tz1;
                    goalTrackingRepository$delete$Anon1.L$Anon3 = xz1;
                    goalTrackingRepository$delete$Anon1.label = 1;
                    obj = ResponseKt.a(goalTrackingRepository$delete$repoResponse$Anon1, goalTrackingRepository$delete$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    goalTrackingRepository = this;
                } else if (i == 1) {
                    xz1 xz12 = (xz1) goalTrackingRepository$delete$Anon1.L$Anon3;
                    tz1 tz12 = (tz1) goalTrackingRepository$delete$Anon1.L$Anon2;
                    list = (List) goalTrackingRepository$delete$Anon1.L$Anon1;
                    goalTrackingRepository = (GoalTrackingRepository) goalTrackingRepository$delete$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local3.d(str3, "deleteGoalTrackingDataList onResponse: response = " + qo2);
                    goalTrackingRepository.removeDeletedGoalTrackingList(list);
                    return new ro2(list, false, 2, (fd4) null);
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("deleteGoalTrackingDataList Failure code=");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(" message=");
                    ServerError c = po2.c();
                    sb.append(c != null ? c.getMessage() : null);
                    local4.d(str4, sb.toString());
                    if (po2.a() == 422) {
                        ArrayList arrayList = new ArrayList();
                        if (!TextUtils.isEmpty(po2.b())) {
                            try {
                                Object a2 = new Gson().a(((po2) qo2).b(), new GoalTrackingRepository$delete$type$Anon1().getType());
                                kd4.a(a2, "Gson().fromJson(repoResponse.errorItems, type)");
                                List list2 = ((UpsertApiResponse) a2).get_items();
                                if (true ^ list2.isEmpty()) {
                                    int size = list2.size();
                                    for (int i3 = 0; i3 < size; i3++) {
                                        Integer code = ((ServerError) list2.get(i3)).getCode();
                                        if (code != null) {
                                            if (code.intValue() == 404001) {
                                                GoalTrackingData goalTrackingData = list.get(i3);
                                                goalTrackingData.setPinType(0);
                                                arrayList.add(goalTrackingData);
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e2) {
                                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                                String str5 = TAG;
                                local5.e(str5, "insertGoalTrackingDataList ex=" + e2);
                                e2.printStackTrace();
                            }
                            goalTrackingRepository.removeDeletedGoalTrackingList(arrayList);
                        }
                        new ro2(list, false, 2, (fd4) null);
                    }
                    return new po2(po2.a(), po2.c(), po2.d(), po2.b());
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        goalTrackingRepository$delete$Anon1 = new GoalTrackingRepository$delete$Anon1(this, yb4);
        Object obj2 = goalTrackingRepository$delete$Anon1.result;
        Object a3 = cc4.a();
        i = goalTrackingRepository$delete$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    public final Object deleteGoalTracking(GoalTrackingData goalTrackingData, yb4<? super qa4> yb4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "deleteGoalTracking " + goalTrackingData);
        this.mGoalTrackingDao.deleteGoalTrackingRawData(goalTrackingData);
        return pushPendingGoalTrackingDataList(yb4);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object fetchGoalSetting(yb4<? super qo2<GoalSetting>> yb4) {
        GoalTrackingRepository$fetchGoalSetting$Anon1 goalTrackingRepository$fetchGoalSetting$Anon1;
        int i;
        GoalTrackingRepository goalTrackingRepository;
        qo2 qo2;
        if (yb4 instanceof GoalTrackingRepository$fetchGoalSetting$Anon1) {
            goalTrackingRepository$fetchGoalSetting$Anon1 = (GoalTrackingRepository$fetchGoalSetting$Anon1) yb4;
            int i2 = goalTrackingRepository$fetchGoalSetting$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                goalTrackingRepository$fetchGoalSetting$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = goalTrackingRepository$fetchGoalSetting$Anon1.result;
                Object a = cc4.a();
                i = goalTrackingRepository$fetchGoalSetting$Anon1.label;
                String str = null;
                if (i != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "fetchGoalSetting");
                    GoalTrackingRepository$fetchGoalSetting$response$Anon1 goalTrackingRepository$fetchGoalSetting$response$Anon1 = new GoalTrackingRepository$fetchGoalSetting$response$Anon1(this, (yb4) null);
                    goalTrackingRepository$fetchGoalSetting$Anon1.L$Anon0 = this;
                    goalTrackingRepository$fetchGoalSetting$Anon1.label = 1;
                    obj = ResponseKt.a(goalTrackingRepository$fetchGoalSetting$response$Anon1, goalTrackingRepository$fetchGoalSetting$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    goalTrackingRepository = this;
                } else if (i == 1) {
                    goalTrackingRepository = (GoalTrackingRepository) goalTrackingRepository$fetchGoalSetting$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    if (ro2.a() != null) {
                        goalTrackingRepository.saveSettingToDB((GoalSetting) ro2.a());
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("fetchGoalSettings Failure code=");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(" message=");
                    ServerError c = po2.c();
                    if (c != null) {
                        String message = c.getMessage();
                        if (message != null) {
                            str = message;
                            if (str == null) {
                                str = "";
                            }
                            sb.append(str);
                            local.e(str2, sb.toString());
                        }
                    }
                    ServerError c2 = po2.c();
                    if (c2 != null) {
                        str = c2.getUserMessage();
                    }
                    if (str == null) {
                    }
                    sb.append(str);
                    local.e(str2, sb.toString());
                }
                return qo2;
            }
        }
        goalTrackingRepository$fetchGoalSetting$Anon1 = new GoalTrackingRepository$fetchGoalSetting$Anon1(this, yb4);
        Object obj2 = goalTrackingRepository$fetchGoalSetting$Anon1.result;
        Object a2 = cc4.a();
        i = goalTrackingRepository$fetchGoalSetting$Anon1.label;
        String str3 = null;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
        return qo2;
    }

    @DexIgnore
    public final LiveData<os3<List<GoalTrackingData>>> getGoalTrackingDataList(Date date, Date date2, boolean z) {
        kd4.b(date, GoalPhase.COLUMN_START_DATE);
        kd4.b(date2, GoalPhase.COLUMN_END_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getGoalTrackingDataList startDate=" + date + ", endDate=" + date2);
        LiveData<os3<List<GoalTrackingData>>> b = hc.b(getPendingGoalTrackingDataListLiveData(date, date2), new GoalTrackingRepository$getGoalTrackingDataList$Anon1(this, date, date2, z));
        kd4.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final Listing<GoalTrackingData> getGoalTrackingDataPaging(Date date, GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, h42 h42, PagingRequestHelper.a aVar) {
        kd4.b(date, "currentDate");
        kd4.b(goalTrackingRepository, "goalTrackingRepository");
        kd4.b(goalTrackingDao, "goalTrackingDao");
        kd4.b(goalTrackingDatabase, "goalTrackingDatabase");
        kd4.b(h42, "appExecutors");
        kd4.b(aVar, "listener");
        FLogger.INSTANCE.getLocal().d(TAG, "getGoalTrackingDataPaging");
        GoalTrackingDataSourceFactory goalTrackingDataSourceFactory = new GoalTrackingDataSourceFactory(goalTrackingRepository, goalTrackingDao, goalTrackingDatabase, date, h42, aVar);
        this.mSourceDataFactoryList.add(goalTrackingDataSourceFactory);
        qd.f.a aVar2 = new qd.f.a();
        aVar2.a(100);
        aVar2.a(false);
        aVar2.b(100);
        aVar2.c(5);
        qd.f a = aVar2.a();
        kd4.a((Object) a, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a2 = new nd(goalTrackingDataSourceFactory, a).a();
        kd4.a((Object) a2, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData<Y> b = hc.b(goalTrackingDataSourceFactory.getSourceLiveData(), GoalTrackingRepository$getGoalTrackingDataPaging$Anon1.INSTANCE);
        kd4.a((Object) b, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing<>(a2, b, new GoalTrackingRepository$getGoalTrackingDataPaging$Anon2(goalTrackingDataSourceFactory), new GoalTrackingRepository$getGoalTrackingDataPaging$Anon3(goalTrackingDataSourceFactory));
    }

    @DexIgnore
    public final List<GoalTrackingSummary> getGoalTrackingSummaries(Date date, Date date2) {
        kd4.b(date, GoalPhase.COLUMN_START_DATE);
        kd4.b(date2, GoalPhase.COLUMN_END_DATE);
        return this.mGoalTrackingDao.getGoalTrackingSummaries(date, date2);
    }

    @DexIgnore
    public final LiveData<os3<Integer>> getLastGoalSetting() {
        return new GoalTrackingRepository$getLastGoalSetting$Anon1(this).asLiveData();
    }

    @DexIgnore
    public final List<GoalTrackingData> getPendingGoalTrackingDataList(Date date, Date date2) {
        kd4.b(date, GoalPhase.COLUMN_START_DATE);
        kd4.b(date2, GoalPhase.COLUMN_END_DATE);
        return this.mGoalTrackingDao.getPendingGoalTrackingDataList(date, date2);
    }

    @DexIgnore
    public final LiveData<os3<List<GoalTrackingSummary>>> getSummaries(Date date, Date date2, boolean z) {
        kd4.b(date, GoalPhase.COLUMN_START_DATE);
        kd4.b(date2, GoalPhase.COLUMN_END_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSummaries startDate=" + date + ", endDate=" + date2);
        LiveData<os3<List<GoalTrackingSummary>>> b = hc.b(getPendingGoalTrackingDataListLiveData(date, date2), new GoalTrackingRepository$getSummaries$Anon1(this, date, date2, z));
        kd4.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final Listing<GoalTrackingSummary> getSummariesPaging(GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, Date date, h42 h42, PagingRequestHelper.a aVar) {
        kd4.b(goalTrackingRepository, "goalTrackingRepository");
        kd4.b(goalTrackingDao, "goalTrackingDao");
        kd4.b(goalTrackingDatabase, "goalTrackingDatabase");
        kd4.b(date, "createdDate");
        kd4.b(h42, "appExecutors");
        kd4.b(aVar, "listener");
        FLogger.INSTANCE.getLocal().d(TAG, "getSummariesPaging");
        GoalTrackingSummaryLocalDataSource.Companion companion = GoalTrackingSummaryLocalDataSource.Companion;
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        Date time = instance.getTime();
        kd4.a((Object) time, "Calendar.getInstance().time");
        Date calculateNextKey = companion.calculateNextKey(time, date);
        Calendar instance2 = Calendar.getInstance();
        kd4.a((Object) instance2, "calendar");
        instance2.setTime(calculateNextKey);
        GoalTrackingSummaryDataSourceFactory goalTrackingSummaryDataSourceFactory = new GoalTrackingSummaryDataSourceFactory(goalTrackingRepository, goalTrackingDao, goalTrackingDatabase, date, h42, aVar, instance2);
        this.mSourceFactoryList.add(goalTrackingSummaryDataSourceFactory);
        qd.f.a aVar2 = new qd.f.a();
        aVar2.a(30);
        aVar2.a(false);
        aVar2.b(30);
        aVar2.c(5);
        qd.f a = aVar2.a();
        kd4.a((Object) a, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a2 = new nd(goalTrackingSummaryDataSourceFactory, a).a();
        kd4.a((Object) a2, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData<Y> b = hc.b(goalTrackingSummaryDataSourceFactory.getSourceLiveData(), GoalTrackingRepository$getSummariesPaging$Anon1.INSTANCE);
        kd4.a((Object) b, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing<>(a2, b, new GoalTrackingRepository$getSummariesPaging$Anon2(goalTrackingSummaryDataSourceFactory), new GoalTrackingRepository$getSummariesPaging$Anon3(goalTrackingSummaryDataSourceFactory));
    }

    @DexIgnore
    public final LiveData<os3<GoalTrackingSummary>> getSummary(Date date) {
        kd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSummary date=" + date);
        LiveData<os3<GoalTrackingSummary>> b = hc.b(getPendingGoalTrackingDataListLiveData(date, date), new GoalTrackingRepository$getSummary$Anon1(this, date));
        kd4.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01d5, code lost:
        if (r7.intValue() != 409000) goto L_0x01d7;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x013e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object insert(List<GoalTrackingData> list, yb4<? super qo2<List<GoalTrackingData>>> yb4) {
        GoalTrackingRepository$insert$Anon1 goalTrackingRepository$insert$Anon1;
        int i;
        GoalTrackingRepository goalTrackingRepository;
        qo2 qo2;
        if (yb4 instanceof GoalTrackingRepository$insert$Anon1) {
            goalTrackingRepository$insert$Anon1 = (GoalTrackingRepository$insert$Anon1) yb4;
            int i2 = goalTrackingRepository$insert$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                goalTrackingRepository$insert$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = goalTrackingRepository$insert$Anon1.result;
                Object a = cc4.a();
                i = goalTrackingRepository$insert$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "insertGoalTrackingDataList: sampleRawList =" + list.size());
                    MFUser currentUser = this.mUserRepository.getCurrentUser();
                    String userId = currentUser != null ? currentUser.getUserId() : null;
                    if (!TextUtils.isEmpty(userId)) {
                        tz1 tz1 = new tz1();
                        for (GoalTrackingData next : list) {
                            String component1 = next.component1();
                            DateTime component2 = next.component2();
                            int component3 = next.component3();
                            Date component4 = next.component4();
                            try {
                                xz1 xz1 = new xz1();
                                xz1.a("id", component1);
                                xz1.a("date", rk2.f(component4));
                                xz1.a(GoalTrackingEvent.COLUMN_TRACKED_AT, rk2.b(component2));
                                xz1.a("timezoneOffset", (Number) dc4.a(component3));
                                tz1.a((JsonElement) xz1);
                            } catch (Exception e) {
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                String str2 = TAG;
                                local2.e(str2, "updateGoalSetting exception=" + e);
                                e.printStackTrace();
                            }
                        }
                        xz1 xz12 = new xz1();
                        xz12.a(CloudLogWriter.ITEMS_PARAM, (JsonElement) tz1);
                        GoalTrackingRepository$insert$repoResponse$Anon1 goalTrackingRepository$insert$repoResponse$Anon1 = new GoalTrackingRepository$insert$repoResponse$Anon1(this, xz12, (yb4) null);
                        goalTrackingRepository$insert$Anon1.L$Anon0 = this;
                        goalTrackingRepository$insert$Anon1.L$Anon1 = list;
                        goalTrackingRepository$insert$Anon1.L$Anon2 = userId;
                        goalTrackingRepository$insert$Anon1.L$Anon3 = tz1;
                        goalTrackingRepository$insert$Anon1.L$Anon4 = xz12;
                        goalTrackingRepository$insert$Anon1.label = 1;
                        obj = ResponseKt.a(goalTrackingRepository$insert$repoResponse$Anon1, goalTrackingRepository$insert$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        goalTrackingRepository = this;
                    } else {
                        FLogger.INSTANCE.getLocal().d(TAG, "insertGoalTrackingDataList userId is null");
                        return new po2(600, new ServerError(600, ""), (Throwable) null, (String) null, 8, (fd4) null);
                    }
                } else if (i == 1) {
                    xz1 xz13 = (xz1) goalTrackingRepository$insert$Anon1.L$Anon4;
                    tz1 tz12 = (tz1) goalTrackingRepository$insert$Anon1.L$Anon3;
                    String str3 = (String) goalTrackingRepository$insert$Anon1.L$Anon2;
                    list = (List) goalTrackingRepository$insert$Anon1.L$Anon1;
                    goalTrackingRepository = (GoalTrackingRepository) goalTrackingRepository$insert$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local3.d(str4, "insertGoalTrackingDataList onResponse: response = " + qo2);
                    goalTrackingRepository.updateGoalTrackingPinType(list, 0);
                    return new ro2(list, false, 2, (fd4) null);
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str5 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("insertGoalTrackingDataList Failure code=");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(" message=");
                    ServerError c = po2.c();
                    sb.append(c != null ? c.getMessage() : null);
                    local4.d(str5, sb.toString());
                    if (po2.a() == 422) {
                        ArrayList arrayList = new ArrayList();
                        if (!TextUtils.isEmpty(po2.b())) {
                            try {
                                Object a2 = new Gson().a(((po2) qo2).b(), new GoalTrackingRepository$insert$type$Anon1().getType());
                                kd4.a(a2, "Gson().fromJson(repoResponse.errorItems, type)");
                                List list2 = ((UpsertApiResponse) a2).get_items();
                                if (true ^ list2.isEmpty()) {
                                    int size = list2.size();
                                    for (int i3 = 0; i3 < size; i3++) {
                                        Integer code = ((ServerError) list2.get(i3)).getCode();
                                        if (code == null) {
                                        }
                                        Integer code2 = ((ServerError) list2.get(i3)).getCode();
                                        if (code2 != null) {
                                            if (code2.intValue() != 409001) {
                                            }
                                            GoalTrackingData goalTrackingData = list.get(i3);
                                            goalTrackingData.setPinType(0);
                                            arrayList.add(goalTrackingData);
                                        }
                                    }
                                }
                            } catch (Exception e2) {
                                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                                String str6 = TAG;
                                local5.e(str6, "insertGoalTrackingDataList ex=" + e2);
                                e2.printStackTrace();
                            }
                            goalTrackingRepository.mGoalTrackingDao.upsertListGoalTrackingData(arrayList);
                        }
                        new ro2(list, false, 2, (fd4) null);
                    }
                    return new po2(po2.a(), po2.c(), po2.d(), po2.b());
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        goalTrackingRepository$insert$Anon1 = new GoalTrackingRepository$insert$Anon1(this, yb4);
        Object obj2 = goalTrackingRepository$insert$Anon1.result;
        Object a3 = cc4.a();
        i = goalTrackingRepository$insert$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    public final Object insertFromDevice(List<GoalTrackingData> list, yb4<? super qa4> yb4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insertFromDevice: goalTrackingDataList = " + list.size());
        this.mGoalTrackingDao.addGoalTrackingRawDataList(list);
        if (!list.isEmpty()) {
            this.mSharedPreferencesManager.j(true);
        }
        return pushPendingGoalTrackingDataList(yb4);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01da  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    public final Object loadGoalTrackingDataList(Date date, Date date2, int i, int i2, yb4<? super qo2<ApiResponse<GoalEvent>>> yb4) {
        GoalTrackingRepository$loadGoalTrackingDataList$Anon1 goalTrackingRepository$loadGoalTrackingDataList$Anon1;
        int i3;
        qo2 qo2;
        qo2 qo22;
        Object obj;
        int i4;
        GoalTrackingRepository goalTrackingRepository;
        Date date3;
        int i5;
        Date date4 = date;
        Date date5 = date2;
        yb4<? super qo2<ApiResponse<GoalEvent>>> yb42 = yb4;
        if (yb42 instanceof GoalTrackingRepository$loadGoalTrackingDataList$Anon1) {
            goalTrackingRepository$loadGoalTrackingDataList$Anon1 = (GoalTrackingRepository$loadGoalTrackingDataList$Anon1) yb42;
            int i6 = goalTrackingRepository$loadGoalTrackingDataList$Anon1.label;
            if ((i6 & Integer.MIN_VALUE) != 0) {
                goalTrackingRepository$loadGoalTrackingDataList$Anon1.label = i6 - Integer.MIN_VALUE;
                GoalTrackingRepository$loadGoalTrackingDataList$Anon1 goalTrackingRepository$loadGoalTrackingDataList$Anon12 = goalTrackingRepository$loadGoalTrackingDataList$Anon1;
                Object obj2 = goalTrackingRepository$loadGoalTrackingDataList$Anon12.result;
                Object a = cc4.a();
                i3 = goalTrackingRepository$loadGoalTrackingDataList$Anon12.label;
                if (i3 != 0) {
                    na4.a(obj2);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "loadGoalTrackingDataList startDate=" + date4 + ", endDate=" + date5);
                    GoalTrackingRepository$loadGoalTrackingDataList$response$Anon1 goalTrackingRepository$loadGoalTrackingDataList$response$Anon1 = new GoalTrackingRepository$loadGoalTrackingDataList$response$Anon1(this, date, date2, i, (yb4) null);
                    goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon0 = this;
                    goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon1 = date4;
                    goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon2 = date5;
                    i5 = i;
                    goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$Anon0 = i5;
                    int i7 = i2;
                    goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$Anon1 = i7;
                    goalTrackingRepository$loadGoalTrackingDataList$Anon12.label = 1;
                    Object a2 = ResponseKt.a(goalTrackingRepository$loadGoalTrackingDataList$response$Anon1, goalTrackingRepository$loadGoalTrackingDataList$Anon12);
                    if (a2 == a) {
                        return a;
                    }
                    i4 = i7;
                    obj2 = a2;
                    date3 = date5;
                    goalTrackingRepository = this;
                } else if (i3 == 1) {
                    int i8 = goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$Anon1;
                    i5 = goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$Anon0;
                    date3 = (Date) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon2;
                    na4.a(obj2);
                    i4 = i8;
                    date4 = (Date) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon1;
                    goalTrackingRepository = (GoalTrackingRepository) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon0;
                } else if (i3 == 2) {
                    List list = (List) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon4;
                    qo22 = (qo2) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon3;
                    int i9 = goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$Anon1;
                    int i10 = goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$Anon0;
                    Date date6 = (Date) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon2;
                    Date date7 = (Date) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon1;
                    GoalTrackingRepository goalTrackingRepository2 = (GoalTrackingRepository) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon0;
                    try {
                        na4.a(obj2);
                        obj = obj2;
                        return (qo2) obj;
                    } catch (Exception e) {
                        e = e;
                        qo2 = qo22;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj2;
                String str2 = null;
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    if (ro2.a() != null && !ro2.b()) {
                        try {
                            List<GoalEvent> list2 = ((ApiResponse) ((ro2) qo2).a()).get_items();
                            ArrayList arrayList = new ArrayList(db4.a(list2, 10));
                            for (GoalEvent goalTrackingData : list2) {
                                GoalTrackingData goalTrackingData2 = goalTrackingData.toGoalTrackingData();
                                if (goalTrackingData2 != null) {
                                    arrayList.add(goalTrackingData2);
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            }
                            goalTrackingRepository.mGoalTrackingDao.upsertGoalTrackingDataList(kb4.d(arrayList));
                            if (!((ApiResponse) ((ro2) qo2).a()).get_items().isEmpty()) {
                                goalTrackingRepository.mSharedPreferencesManager.j(true);
                            }
                            if (((ApiResponse) ((ro2) qo2).a()).get_range() == null) {
                                return qo2;
                            }
                            Range range = ((ApiResponse) ((ro2) qo2).a()).get_range();
                            if (range == null) {
                                kd4.a();
                                throw null;
                            } else if (!range.isHasNext()) {
                                return qo2;
                            } else {
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon0 = goalTrackingRepository;
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon1 = date4;
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon2 = date3;
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$Anon0 = i5;
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$Anon1 = i4;
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon3 = qo2;
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$Anon4 = arrayList;
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.label = 2;
                                obj = goalTrackingRepository.loadGoalTrackingDataList(date4, date3, i5 + i4, i4, goalTrackingRepository$loadGoalTrackingDataList$Anon12);
                                if (obj == a) {
                                    return a;
                                }
                                qo22 = qo2;
                                return (qo2) obj;
                            }
                        } catch (Exception e2) {
                            e = e2;
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str3 = TAG;
                            local2.e(str3, "loadGoalTrackingDataList exception=" + e);
                            e.printStackTrace();
                            return qo2;
                        }
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("loadGoalTrackingDataList Failure code=");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(" message=");
                    ServerError c = po2.c();
                    if (c != null) {
                        String message = c.getMessage();
                        if (message != null) {
                            str2 = message;
                            if (str2 == null) {
                                str2 = "";
                            }
                            sb.append(str2);
                            local3.d(str4, sb.toString());
                        }
                    }
                    ServerError c2 = po2.c();
                    if (c2 != null) {
                        str2 = c2.getUserMessage();
                    }
                    if (str2 == null) {
                    }
                    sb.append(str2);
                    local3.d(str4, sb.toString());
                }
                return qo2;
            }
        }
        goalTrackingRepository$loadGoalTrackingDataList$Anon1 = new GoalTrackingRepository$loadGoalTrackingDataList$Anon1(this, yb42);
        GoalTrackingRepository$loadGoalTrackingDataList$Anon1 goalTrackingRepository$loadGoalTrackingDataList$Anon122 = goalTrackingRepository$loadGoalTrackingDataList$Anon1;
        Object obj22 = goalTrackingRepository$loadGoalTrackingDataList$Anon122.result;
        Object a3 = cc4.a();
        i3 = goalTrackingRepository$loadGoalTrackingDataList$Anon122.label;
        if (i3 != 0) {
        }
        qo2 = (qo2) obj22;
        String str22 = null;
        if (!(qo2 instanceof ro2)) {
        }
        return qo2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0176  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object loadSummaries(Date date, Date date2, yb4<? super qo2<ApiResponse<GoalDailySummary>>> yb4) {
        GoalTrackingRepository$loadSummaries$Anon1 goalTrackingRepository$loadSummaries$Anon1;
        int i;
        GoalTrackingRepository goalTrackingRepository;
        qo2 qo2;
        if (yb4 instanceof GoalTrackingRepository$loadSummaries$Anon1) {
            goalTrackingRepository$loadSummaries$Anon1 = (GoalTrackingRepository$loadSummaries$Anon1) yb4;
            int i2 = goalTrackingRepository$loadSummaries$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                goalTrackingRepository$loadSummaries$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = goalTrackingRepository$loadSummaries$Anon1.result;
                Object a = cc4.a();
                i = goalTrackingRepository$loadSummaries$Anon1.label;
                String str = null;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local.d(str2, "loadSummaries startDate=" + date + ", endDate=" + date2);
                    GoalTrackingRepository$loadSummaries$response$Anon1 goalTrackingRepository$loadSummaries$response$Anon1 = new GoalTrackingRepository$loadSummaries$response$Anon1(this, date, date2, (yb4) null);
                    goalTrackingRepository$loadSummaries$Anon1.L$Anon0 = this;
                    goalTrackingRepository$loadSummaries$Anon1.L$Anon1 = date;
                    goalTrackingRepository$loadSummaries$Anon1.L$Anon2 = date2;
                    goalTrackingRepository$loadSummaries$Anon1.label = 1;
                    obj = ResponseKt.a(goalTrackingRepository$loadSummaries$response$Anon1, goalTrackingRepository$loadSummaries$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    goalTrackingRepository = this;
                } else if (i == 1) {
                    Date date3 = (Date) goalTrackingRepository$loadSummaries$Anon1.L$Anon2;
                    Date date4 = (Date) goalTrackingRepository$loadSummaries$Anon1.L$Anon1;
                    goalTrackingRepository = (GoalTrackingRepository) goalTrackingRepository$loadSummaries$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    if (ro2.a() != null && !ro2.b()) {
                        try {
                            List<GoalDailySummary> list = ((ApiResponse) ((ro2) qo2).a()).get_items();
                            ArrayList arrayList = new ArrayList(db4.a(list, 10));
                            for (GoalDailySummary goalTrackingSummary : list) {
                                GoalTrackingSummary goalTrackingSummary2 = goalTrackingSummary.toGoalTrackingSummary();
                                if (goalTrackingSummary2 != null) {
                                    arrayList.add(goalTrackingSummary2);
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            }
                            goalTrackingRepository.mGoalTrackingDao.upsertGoalTrackingSummaries(kb4.d(arrayList));
                            List list2 = ((ApiResponse) ((ro2) qo2).a()).get_items();
                            ArrayList arrayList2 = new ArrayList();
                            for (Object next : list2) {
                                if (dc4.a(((GoalDailySummary) next).getMTotalTracked() > 0).booleanValue()) {
                                    arrayList2.add(next);
                                }
                            }
                            if (!arrayList2.isEmpty()) {
                                goalTrackingRepository.mSharedPreferencesManager.j(true);
                            }
                        } catch (Exception e) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str3 = TAG;
                            local2.e(str3, "loadSummaries exception=" + e);
                            e.printStackTrace();
                        }
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("loadSummaries Failure code=");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(" message=");
                    ServerError c = po2.c();
                    if (c != null) {
                        String message = c.getMessage();
                        if (message != null) {
                            str = message;
                            if (str == null) {
                                str = "";
                            }
                            sb.append(str);
                            local3.d(str4, sb.toString());
                        }
                    }
                    ServerError c2 = po2.c();
                    if (c2 != null) {
                        str = c2.getUserMessage();
                    }
                    if (str == null) {
                    }
                    sb.append(str);
                    local3.d(str4, sb.toString());
                }
                return qo2;
            }
        }
        goalTrackingRepository$loadSummaries$Anon1 = new GoalTrackingRepository$loadSummaries$Anon1(this, yb4);
        Object obj2 = goalTrackingRepository$loadSummaries$Anon1.result;
        Object a2 = cc4.a();
        i = goalTrackingRepository$loadSummaries$Anon1.label;
        String str5 = null;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
        return qo2;
    }

    @DexIgnore
    public final Object pushPendingGoalTrackingDataList(PushPendingGoalTrackingDataListCallback pushPendingGoalTrackingDataListCallback, yb4<? super qa4> yb4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "pushPendingGoalTrackingDataList mGoalTrackingDatabase=" + this.mGoalTrackingDatabase);
        List<GoalTrackingData> pendingGoalTrackingDataList = this.mGoalTrackingDao.getPendingGoalTrackingDataList();
        if (pendingGoalTrackingDataList.size() > 0) {
            return saveGoalTrackingDataListToServer(pendingGoalTrackingDataList, pushPendingGoalTrackingDataListCallback, yb4);
        }
        if (pushPendingGoalTrackingDataListCallback != null) {
            pushPendingGoalTrackingDataListCallback.onFail(MFNetworkReturnCode.NOT_FOUND);
        }
        return qa4.a;
    }

    @DexIgnore
    public final void removePagingListener() {
        for (GoalTrackingDataSourceFactory localDataSource : this.mSourceDataFactoryList) {
            GoalTrackingDataLocalDataSource localDataSource2 = localDataSource.getLocalDataSource();
            if (localDataSource2 != null) {
                localDataSource2.removePagingObserver();
            }
        }
        this.mSourceDataFactoryList.clear();
        for (GoalTrackingSummaryDataSourceFactory localDataSource3 : this.mSourceFactoryList) {
            GoalTrackingSummaryLocalDataSource localDataSource4 = localDataSource3.getLocalDataSource();
            if (localDataSource4 != null) {
                localDataSource4.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0156  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x01b2  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01d4  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0207  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x024c  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0254  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x025a  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0262  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0032  */
    public final /* synthetic */ Object saveGoalTrackingDataListToServer(List<GoalTrackingData> list, PushPendingGoalTrackingDataListCallback pushPendingGoalTrackingDataListCallback, yb4<? super qa4> yb4) {
        GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1;
        int i;
        PushPendingGoalTrackingDataListCallback pushPendingGoalTrackingDataListCallback2;
        int i2;
        List list2;
        List<GoalTrackingData> list3;
        GoalTrackingRepository goalTrackingRepository;
        String str;
        GoalTrackingRepository goalTrackingRepository2;
        List<GoalTrackingData> list4;
        PushPendingGoalTrackingDataListCallback pushPendingGoalTrackingDataListCallback3;
        int i3;
        List list5;
        Object obj;
        int i4;
        qo2 qo2;
        List<GoalTrackingData> list6;
        List<T> list7;
        List<T> list8;
        String str2;
        int i5;
        List<T> list9;
        qo2 qo22;
        GoalTrackingRepository goalTrackingRepository3;
        GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 goalTrackingRepository$saveGoalTrackingDataListToServer$Anon12;
        yb4<? super qa4> yb42 = yb4;
        if (yb42 instanceof GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1) {
            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 = (GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1) yb42;
            int i6 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.label;
            if ((i6 & Integer.MIN_VALUE) != 0) {
                goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.label = i6 - Integer.MIN_VALUE;
                Object obj2 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.result;
                Object a = cc4.a();
                i = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.label;
                String str3 = "startIndex=";
                if (i != 0) {
                    na4.a(obj2);
                    list4 = list;
                    pushPendingGoalTrackingDataListCallback3 = pushPendingGoalTrackingDataListCallback;
                    list5 = new ArrayList();
                    i3 = 0;
                    goalTrackingRepository2 = this;
                } else if (i == 1) {
                    list8 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon6;
                    list9 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon5;
                    list6 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon4;
                    i5 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$Anon1;
                    list5 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon3;
                    i3 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$Anon0;
                    pushPendingGoalTrackingDataListCallback3 = (PushPendingGoalTrackingDataListCallback) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon2;
                    list4 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon1;
                    goalTrackingRepository2 = (GoalTrackingRepository) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon0;
                    na4.a(obj2);
                    str2 = str3;
                    qo22 = (qo2) obj2;
                    if (!(qo22 instanceof ro2)) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon12 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1;
                        String str4 = TAG;
                        goalTrackingRepository3 = goalTrackingRepository2;
                        local.d(str4, "saveGoalTrackingDataListToServer success, bravo!!! startIndex=" + i3 + " endIndex=" + i5);
                        Object a2 = ((ro2) qo22).a();
                        if (a2 != null) {
                            list5.addAll((List) a2);
                        }
                        kd4.a();
                        throw null;
                    }
                    goalTrackingRepository$saveGoalTrackingDataListToServer$Anon12 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1;
                    goalTrackingRepository3 = goalTrackingRepository2;
                    if (qo22 instanceof po2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str5 = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("saveGoalTrackingDataListToServer failed, errorCode=");
                        sb.append(((po2) qo22).a());
                        sb.append(' ');
                        str = str2;
                        sb.append(str);
                        sb.append(i3);
                        sb.append(" endIndex=");
                        sb.append(i5);
                        local2.d(str5, sb.toString());
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon12;
                        list7 = list9;
                        i4 = i5;
                        list2 = list5;
                        i2 = i3;
                        goalTrackingRepository = goalTrackingRepository3;
                        pushPendingGoalTrackingDataListCallback2 = pushPendingGoalTrackingDataListCallback3;
                        List<GoalTrackingData> list10 = list6;
                        list3 = list4;
                        if (!list8.isEmpty()) {
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon0 = goalTrackingRepository;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon1 = list3;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon2 = pushPendingGoalTrackingDataListCallback2;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$Anon0 = i2;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon3 = list2;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$Anon1 = i4;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon4 = list10;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon5 = list7;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon6 = list8;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.label = 2;
                            obj = goalTrackingRepository.delete(list8, goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1);
                            if (obj == a) {
                                return a;
                            }
                            qo2 = (qo2) obj;
                            if (qo2 instanceof ro2) {
                            }
                        }
                        goalTrackingRepository2 = goalTrackingRepository;
                        list4 = list3;
                        pushPendingGoalTrackingDataListCallback3 = pushPendingGoalTrackingDataListCallback2;
                        i3 = i2 + 100;
                        if (i3 >= list4.size()) {
                        }
                        if (!(!list2.isEmpty())) {
                        }
                        return qa4.a;
                    }
                    str = str2;
                    goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon12;
                    list7 = list9;
                    i4 = i5;
                    list2 = list5;
                    i2 = i3;
                    goalTrackingRepository = goalTrackingRepository3;
                    pushPendingGoalTrackingDataListCallback2 = pushPendingGoalTrackingDataListCallback3;
                    List<GoalTrackingData> list102 = list6;
                    list3 = list4;
                    if (!list8.isEmpty()) {
                    }
                    goalTrackingRepository2 = goalTrackingRepository;
                    list4 = list3;
                    pushPendingGoalTrackingDataListCallback3 = pushPendingGoalTrackingDataListCallback2;
                    i3 = i2 + 100;
                    if (i3 >= list4.size()) {
                    }
                    if (!(!list2.isEmpty())) {
                    }
                    return qa4.a;
                } else if (i == 2) {
                    List list11 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon6;
                    List list12 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon5;
                    List list13 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon4;
                    int i7 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$Anon1;
                    list2 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon3;
                    i2 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$Anon0;
                    pushPendingGoalTrackingDataListCallback2 = (PushPendingGoalTrackingDataListCallback) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon2;
                    list3 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon1;
                    goalTrackingRepository = (GoalTrackingRepository) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon0;
                    na4.a(obj2);
                    i4 = i7;
                    obj = obj2;
                    str = str3;
                    qo2 = (qo2) obj;
                    if (qo2 instanceof ro2) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str6 = TAG;
                        local3.d(str6, "saveGoalTrackingDataListToServer success, bravo!!! startIndex=" + i2 + " endIndex=" + i4);
                        Object a3 = ((ro2) qo2).a();
                        if (a3 != null) {
                            list2.addAll((List) a3);
                        }
                        kd4.a();
                        throw null;
                    } else if (qo2 instanceof po2) {
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String str7 = TAG;
                        local4.d(str7, "saveGoalTrackingDataListToServer failed, errorCode=" + ((po2) qo2).a() + ' ' + str + i2 + " endIndex=" + i4);
                        goalTrackingRepository2 = goalTrackingRepository;
                        list4 = list3;
                        pushPendingGoalTrackingDataListCallback3 = pushPendingGoalTrackingDataListCallback2;
                        i3 = i2 + 100;
                        if (i3 >= list4.size()) {
                            str3 = str;
                            list5 = list2;
                        }
                        if (!(!list2.isEmpty())) {
                            if (pushPendingGoalTrackingDataListCallback3 != null) {
                                pushPendingGoalTrackingDataListCallback3.onSuccess(list2);
                            }
                        } else if (pushPendingGoalTrackingDataListCallback3 != null) {
                            pushPendingGoalTrackingDataListCallback3.onFail(600);
                        }
                        return qa4.a;
                    }
                    goalTrackingRepository2 = goalTrackingRepository;
                    list4 = list3;
                    pushPendingGoalTrackingDataListCallback3 = pushPendingGoalTrackingDataListCallback2;
                    i3 = i2 + 100;
                    if (i3 >= list4.size()) {
                    }
                    if (!(!list2.isEmpty())) {
                    }
                    return qa4.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (i3 < list4.size()) {
                    int i8 = i3 + 100;
                    if (i8 > list4.size()) {
                        i8 = list4.size();
                    }
                    i5 = i8;
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    String str8 = TAG;
                    local5.d(str8, "saveGoalTrackingDataListToServer startIndex=" + i3 + " endIndex=" + i5);
                    list6 = list4.subList(i3, i5);
                    list9 = SequencesKt___SequencesKt.g(SequencesKt___SequencesKt.a(kb4.b(list6), GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$Anon1.INSTANCE));
                    list8 = SequencesKt___SequencesKt.g(SequencesKt___SequencesKt.a(kb4.b(list6), GoalTrackingRepository$saveGoalTrackingDataListToServer$subDeletedGoalTrackingDataListList$Anon1.INSTANCE));
                    str2 = str3;
                    if (!list9.isEmpty()) {
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon0 = goalTrackingRepository2;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon1 = list4;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon2 = pushPendingGoalTrackingDataListCallback3;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$Anon0 = i3;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon3 = list5;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$Anon1 = i5;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon4 = list6;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon5 = list9;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$Anon6 = list8;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.label = 1;
                        obj2 = goalTrackingRepository2.insert(list9, goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1);
                        if (obj2 == a) {
                            return a;
                        }
                        qo22 = (qo2) obj2;
                        if (!(qo22 instanceof ro2)) {
                        }
                        str = str2;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon12;
                        list7 = list9;
                        i4 = i5;
                        list2 = list5;
                        i2 = i3;
                        goalTrackingRepository = goalTrackingRepository3;
                        pushPendingGoalTrackingDataListCallback2 = pushPendingGoalTrackingDataListCallback3;
                        List<GoalTrackingData> list1022 = list6;
                        list3 = list4;
                        if (!list8.isEmpty()) {
                        }
                        goalTrackingRepository2 = goalTrackingRepository;
                        list4 = list3;
                        pushPendingGoalTrackingDataListCallback3 = pushPendingGoalTrackingDataListCallback2;
                        i3 = i2 + 100;
                        if (i3 >= list4.size()) {
                        }
                        if (!(!list2.isEmpty())) {
                        }
                    }
                    str = str2;
                    list7 = list9;
                    goalTrackingRepository = goalTrackingRepository2;
                    i4 = i5;
                    list2 = list5;
                    i2 = i3;
                    pushPendingGoalTrackingDataListCallback2 = pushPendingGoalTrackingDataListCallback3;
                    List<GoalTrackingData> list10222 = list6;
                    list3 = list4;
                    if (!list8.isEmpty()) {
                    }
                    goalTrackingRepository2 = goalTrackingRepository;
                    list4 = list3;
                    pushPendingGoalTrackingDataListCallback3 = pushPendingGoalTrackingDataListCallback2;
                    i3 = i2 + 100;
                    if (i3 >= list4.size()) {
                    }
                    if (!(!list2.isEmpty())) {
                    }
                }
                return qa4.a;
            }
        }
        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 = new GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1(this, yb42);
        Object obj22 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.result;
        Object a4 = cc4.a();
        i = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.label;
        String str32 = "startIndex=";
        if (i != 0) {
        }
        if (i3 < list4.size()) {
        }
        return qa4.a;
    }

    @DexIgnore
    public final void saveSettingToDB(GoalSetting goalSetting) {
        kd4.b(goalSetting, "goalSetting");
        this.mGoalTrackingDao.upsertGoalSettings(goalSetting);
        GoalTrackingSummary goalTrackingSummary = this.mGoalTrackingDao.getGoalTrackingSummary(new Date());
        if (goalTrackingSummary == null) {
            goalTrackingSummary = new GoalTrackingSummary(new Date(), 0, goalSetting.getCurrentTarget(), new Date().getTime(), new Date().getTime());
        } else {
            goalTrackingSummary.setGoalTarget(goalSetting.getCurrentTarget());
        }
        this.mGoalTrackingDao.upsertGoalTrackingSummary(goalTrackingSummary);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object updateGoalSetting(GoalSetting goalSetting, UpdateGoalSettingCallback updateGoalSettingCallback, yb4<? super qa4> yb4) {
        GoalTrackingRepository$updateGoalSetting$Anon1 goalTrackingRepository$updateGoalSetting$Anon1;
        int i;
        GoalTrackingRepository goalTrackingRepository;
        xz1 xz1;
        qo2 qo2;
        if (yb4 instanceof GoalTrackingRepository$updateGoalSetting$Anon1) {
            goalTrackingRepository$updateGoalSetting$Anon1 = (GoalTrackingRepository$updateGoalSetting$Anon1) yb4;
            int i2 = goalTrackingRepository$updateGoalSetting$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                goalTrackingRepository$updateGoalSetting$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = goalTrackingRepository$updateGoalSetting$Anon1.result;
                Object a = cc4.a();
                i = goalTrackingRepository$updateGoalSetting$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    xz1 xz12 = new xz1();
                    try {
                        xz12.a("currentTarget", (Number) dc4.a(goalSetting.getCurrentTarget()));
                        TimeZone timeZone = TimeZone.getDefault();
                        kd4.a((Object) timeZone, "TimeZone.getDefault()");
                        xz12.a("timezoneOffset", (Number) dc4.a(timeZone.getRawOffset() / 1000));
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = TAG;
                        local.e(str, "updateGoalSetting exception=" + e);
                        e.printStackTrace();
                    }
                    GoalTrackingRepository$updateGoalSetting$repoResponse$Anon1 goalTrackingRepository$updateGoalSetting$repoResponse$Anon1 = new GoalTrackingRepository$updateGoalSetting$repoResponse$Anon1(this, xz12, (yb4) null);
                    goalTrackingRepository$updateGoalSetting$Anon1.L$Anon0 = this;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$Anon1 = goalSetting;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$Anon2 = updateGoalSettingCallback;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$Anon3 = xz12;
                    goalTrackingRepository$updateGoalSetting$Anon1.label = 1;
                    Object a2 = ResponseKt.a(goalTrackingRepository$updateGoalSetting$repoResponse$Anon1, goalTrackingRepository$updateGoalSetting$Anon1);
                    if (a2 == a) {
                        return a;
                    }
                    goalTrackingRepository = this;
                    Object obj2 = a2;
                    xz1 = xz12;
                    obj = obj2;
                } else if (i == 1) {
                    updateGoalSettingCallback = (UpdateGoalSettingCallback) goalTrackingRepository$updateGoalSetting$Anon1.L$Anon2;
                    goalTrackingRepository = (GoalTrackingRepository) goalTrackingRepository$updateGoalSetting$Anon1.L$Anon0;
                    na4.a(obj);
                    GoalSetting goalSetting2 = (GoalSetting) goalTrackingRepository$updateGoalSetting$Anon1.L$Anon1;
                    xz1 = (xz1) goalTrackingRepository$updateGoalSetting$Anon1.L$Anon3;
                    goalSetting = goalSetting2;
                } else if (i == 2 || i == 3) {
                    qo2 qo22 = (qo2) goalTrackingRepository$updateGoalSetting$Anon1.L$Anon4;
                    xz1 xz13 = (xz1) goalTrackingRepository$updateGoalSetting$Anon1.L$Anon3;
                    UpdateGoalSettingCallback updateGoalSettingCallback2 = (UpdateGoalSettingCallback) goalTrackingRepository$updateGoalSetting$Anon1.L$Anon2;
                    GoalSetting goalSetting3 = (GoalSetting) goalTrackingRepository$updateGoalSetting$Anon1.L$Anon1;
                    GoalTrackingRepository goalTrackingRepository2 = (GoalTrackingRepository) goalTrackingRepository$updateGoalSetting$Anon1.L$Anon0;
                    na4.a(obj);
                    return qa4.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local2.d(str2, "updateGoalSetting onResponse: response = " + qo2);
                    goalTrackingRepository.saveSettingToDB(goalSetting);
                    pi4 c = nh4.c();
                    GoalTrackingRepository$updateGoalSetting$Anon2 goalTrackingRepository$updateGoalSetting$Anon2 = new GoalTrackingRepository$updateGoalSetting$Anon2(updateGoalSettingCallback, goalSetting, (yb4) null);
                    goalTrackingRepository$updateGoalSetting$Anon1.L$Anon0 = goalTrackingRepository;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$Anon1 = goalSetting;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$Anon2 = updateGoalSettingCallback;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$Anon3 = xz1;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$Anon4 = qo2;
                    goalTrackingRepository$updateGoalSetting$Anon1.label = 2;
                    if (yf4.a(c, goalTrackingRepository$updateGoalSetting$Anon2, goalTrackingRepository$updateGoalSetting$Anon1) == a) {
                        return a;
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("updateGoalSetting Failure code=");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(" message=");
                    ServerError c2 = po2.c();
                    sb.append(c2 != null ? c2.getMessage() : null);
                    local3.d(str3, sb.toString());
                    pi4 c3 = nh4.c();
                    GoalTrackingRepository$updateGoalSetting$Anon3 goalTrackingRepository$updateGoalSetting$Anon3 = new GoalTrackingRepository$updateGoalSetting$Anon3(updateGoalSettingCallback, qo2, (yb4) null);
                    goalTrackingRepository$updateGoalSetting$Anon1.L$Anon0 = goalTrackingRepository;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$Anon1 = goalSetting;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$Anon2 = updateGoalSettingCallback;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$Anon3 = xz1;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$Anon4 = qo2;
                    goalTrackingRepository$updateGoalSetting$Anon1.label = 3;
                    if (yf4.a(c3, goalTrackingRepository$updateGoalSetting$Anon3, goalTrackingRepository$updateGoalSetting$Anon1) == a) {
                        return a;
                    }
                }
                return qa4.a;
            }
        }
        goalTrackingRepository$updateGoalSetting$Anon1 = new GoalTrackingRepository$updateGoalSetting$Anon1(this, yb4);
        Object obj3 = goalTrackingRepository$updateGoalSetting$Anon1.result;
        Object a3 = cc4.a();
        i = goalTrackingRepository$updateGoalSetting$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj3;
        if (!(qo2 instanceof ro2)) {
        }
        return qa4.a;
    }

    @DexIgnore
    public final /* synthetic */ Object pushPendingGoalTrackingDataList(yb4<? super qa4> yb4) {
        FLogger.INSTANCE.getLocal().d(TAG, "pushPendingGoalTrackingDataList");
        return pushPendingGoalTrackingDataList(new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3(this), yb4);
    }
}
