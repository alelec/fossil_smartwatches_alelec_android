package com.portfolio.platform.data.source.remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$response$1", mo27670f = "DianaPresetRemoteDataSource.kt", mo27671l = {25}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$response$1 */
public final class C5933xcc19f195 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset>>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serial;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C5933xcc19f195(com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource dianaPresetRemoteDataSource, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(1, yb4);
        this.this$0 = dianaPresetRemoteDataSource;
        this.$serial = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        return new com.portfolio.platform.data.source.remote.C5933xcc19f195(this.this$0, this.$serial, yb4);
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj) {
        return ((com.portfolio.platform.data.source.remote.C5933xcc19f195) create((com.fossil.blesdk.obfuscated.yb4) obj)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiServiceV2Dot1$p = this.this$0.mApiServiceV2Dot1;
            java.lang.String str = this.$serial;
            this.label = 1;
            obj = access$getMApiServiceV2Dot1$p.getDianaRecommendPresetList(str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
