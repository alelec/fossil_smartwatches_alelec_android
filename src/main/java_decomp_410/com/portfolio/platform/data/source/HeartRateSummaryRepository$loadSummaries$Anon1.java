package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.yb4;
import java.util.Date;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.HeartRateSummaryRepository", f = "HeartRateSummaryRepository.kt", l = {107}, m = "loadSummaries")
public final class HeartRateSummaryRepository$loadSummaries$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSummaryRepository$loadSummaries$Anon1(HeartRateSummaryRepository heartRateSummaryRepository, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = heartRateSummaryRepository;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.loadSummaries((Date) null, (Date) null, this);
    }
}
