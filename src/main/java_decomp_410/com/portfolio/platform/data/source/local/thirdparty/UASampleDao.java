package com.portfolio.platform.data.source.local.thirdparty;

import com.portfolio.platform.data.model.thirdparty.ua.UASample;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface UASampleDao {
    @DexIgnore
    void clearAll();

    @DexIgnore
    void deleteListUASample(List<UASample> list);

    @DexIgnore
    List<UASample> getAllUASample();

    @DexIgnore
    void insertListUASample(List<UASample> list);

    @DexIgnore
    void insertUASample(UASample uASample);
}
