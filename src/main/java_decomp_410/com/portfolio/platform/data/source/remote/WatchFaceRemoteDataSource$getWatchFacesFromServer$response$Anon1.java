package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1", f = "WatchFaceRemoteDataSource.kt", l = {16}, m = "invokeSuspend")
public final class WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<ApiResponse<WatchFace>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $serialPrefix;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WatchFaceRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1(WatchFaceRemoteDataSource watchFaceRemoteDataSource, String str, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = watchFaceRemoteDataSource;
        this.$serialPrefix = str;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1(this.this$Anon0, this.$serialPrefix, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            ApiServiceV2 access$getApi$p = this.this$Anon0.api;
            String str = this.$serialPrefix;
            this.label = 1;
            obj = access$getApi$p.getWatchFaces(str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
