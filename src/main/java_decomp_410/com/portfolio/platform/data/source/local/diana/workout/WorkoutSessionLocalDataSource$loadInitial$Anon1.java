package com.portfolio.platform.data.source.local.diana.workout;

import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.helper.PagingRequestHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSessionLocalDataSource$loadInitial$Anon1 implements PagingRequestHelper.b {
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionLocalDataSource this$Anon0;

    @DexIgnore
    public WorkoutSessionLocalDataSource$loadInitial$Anon1(WorkoutSessionLocalDataSource workoutSessionLocalDataSource) {
        this.this$Anon0 = workoutSessionLocalDataSource;
    }

    @DexIgnore
    public final void run(PagingRequestHelper.b.a aVar) {
        WorkoutSessionLocalDataSource workoutSessionLocalDataSource = this.this$Anon0;
        kd4.a((Object) aVar, "helperCallback");
        fi4 unused = workoutSessionLocalDataSource.loadData(aVar, this.this$Anon0.mOffset);
    }
}
