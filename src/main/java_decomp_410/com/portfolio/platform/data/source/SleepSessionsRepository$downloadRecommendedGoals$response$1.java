package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$response$1", mo27670f = "SleepSessionsRepository.kt", mo27671l = {380}, mo27672m = "invokeSuspend")
public final class SleepSessionsRepository$downloadRecommendedGoals$response$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $age;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $gender;
    @DexIgnore
    public /* final */ /* synthetic */ int $heightInCentimeters;
    @DexIgnore
    public /* final */ /* synthetic */ int $weightInGrams;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSessionsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$downloadRecommendedGoals$response$1(com.portfolio.platform.data.source.SleepSessionsRepository sleepSessionsRepository, int i, int i2, int i3, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(1, yb4);
        this.this$0 = sleepSessionsRepository;
        this.$age = i;
        this.$weightInGrams = i2;
        this.$heightInCentimeters = i3;
        this.$gender = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$response$1 sleepSessionsRepository$downloadRecommendedGoals$response$1 = new com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$response$1(this.this$0, this.$age, this.$weightInGrams, this.$heightInCentimeters, this.$gender, yb4);
        return sleepSessionsRepository$downloadRecommendedGoals$response$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj) {
        return ((com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$response$1) create((com.fossil.blesdk.obfuscated.yb4) obj)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiService$p = this.this$0.mApiService;
            int i2 = this.$age;
            int i3 = this.$weightInGrams;
            int i4 = this.$heightInCentimeters;
            java.lang.String str = this.$gender;
            this.label = 1;
            obj = access$getMApiService$p.getRecommendedSleepGoalRaw(i2, i3, i4, str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
