package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ld;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingSummaryDataSourceFactory extends ld.b<Date, GoalTrackingSummary> {
    @DexIgnore
    public GoalTrackingSummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ h42 mAppExecutors;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public /* final */ GoalTrackingDao mGoalTrackingDao;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public /* final */ PagingRequestHelper.a mListener;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<GoalTrackingSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public GoalTrackingSummaryDataSourceFactory(GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, Date date, h42 h42, PagingRequestHelper.a aVar, Calendar calendar) {
        kd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        kd4.b(goalTrackingDao, "mGoalTrackingDao");
        kd4.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        kd4.b(date, "mCreatedDate");
        kd4.b(h42, "mAppExecutors");
        kd4.b(aVar, "mListener");
        kd4.b(calendar, "mStartCalendar");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDao = goalTrackingDao;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mCreatedDate = date;
        this.mAppExecutors = h42;
        this.mListener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    public ld<Date, GoalTrackingSummary> create() {
        this.localDataSource = new GoalTrackingSummaryLocalDataSource(this.mGoalTrackingRepository, this.mGoalTrackingDao, this.mGoalTrackingDatabase, this.mCreatedDate, this.mAppExecutors, this.mListener, this.mStartCalendar);
        this.sourceLiveData.a(this.localDataSource);
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource = this.localDataSource;
        if (goalTrackingSummaryLocalDataSource != null) {
            return goalTrackingSummaryLocalDataSource;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final GoalTrackingSummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<GoalTrackingSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        this.localDataSource = goalTrackingSummaryLocalDataSource;
    }
}
