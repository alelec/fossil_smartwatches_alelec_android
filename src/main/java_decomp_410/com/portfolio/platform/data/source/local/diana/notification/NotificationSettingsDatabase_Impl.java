package com.portfolio.platform.data.source.local.diana.notification;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationSettingsDatabase_Impl extends NotificationSettingsDatabase {
    @DexIgnore
    public volatile NotificationSettingsDao _notificationSettingsDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `notificationSettings` (`settingsName` TEXT NOT NULL, `settingsType` INTEGER NOT NULL, `isCall` INTEGER NOT NULL, PRIMARY KEY(`settingsName`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '563d2dcf0170d812960699bb88bdb35d')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `notificationSettings`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (NotificationSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = NotificationSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) NotificationSettingsDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = NotificationSettingsDatabase_Impl.this.mDatabase = ggVar;
            NotificationSettingsDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (NotificationSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = NotificationSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) NotificationSettingsDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("settingsName", new eg.a("settingsName", "TEXT", true, 1));
            hashMap.put("settingsType", new eg.a("settingsType", "INTEGER", true, 0));
            hashMap.put("isCall", new eg.a("isCall", "INTEGER", true, 0));
            eg egVar = new eg("notificationSettings", hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar, "notificationSettings");
            if (!egVar.equals(a)) {
                throw new IllegalStateException("Migration didn't properly handle notificationSettings(com.portfolio.platform.data.model.NotificationSettingsModel).\n Expected:\n" + egVar + "\n Found:\n" + a);
            }
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `notificationSettings`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), "notificationSettings");
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(1), "563d2dcf0170d812960699bb88bdb35d", "d29e13e7c48fbcf2227b41f4f512e273");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }

    @DexIgnore
    public NotificationSettingsDao getNotificationSettingsDao() {
        NotificationSettingsDao notificationSettingsDao;
        if (this._notificationSettingsDao != null) {
            return this._notificationSettingsDao;
        }
        synchronized (this) {
            if (this._notificationSettingsDao == null) {
                this._notificationSettingsDao = new NotificationSettingsDao_Impl(this);
            }
            notificationSettingsDao = this._notificationSettingsDao;
        }
        return notificationSettingsDao;
    }
}
