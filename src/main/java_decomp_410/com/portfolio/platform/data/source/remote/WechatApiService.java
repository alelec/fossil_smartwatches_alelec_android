package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.gt4;
import com.fossil.blesdk.obfuscated.us4;
import com.portfolio.platform.data.WechatToken;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface WechatApiService {
    @DexIgnore
    @us4("oauth2/access_token")
    Call<WechatToken> getWechatToken(@gt4("appid") String str, @gt4("secret") String str2, @gt4("code") String str3, @gt4("grant_type") String str4);
}
