package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.GoalTrackingRepository$loadGoalTrackingDataList$response$1", mo27670f = "GoalTrackingRepository.kt", mo27671l = {390}, mo27672m = "invokeSuspend")
public final class GoalTrackingRepository$loadGoalTrackingDataList$response$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.goaltracking.response.GoalEvent>>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $startDate;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.GoalTrackingRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$loadGoalTrackingDataList$response$1(com.portfolio.platform.data.source.GoalTrackingRepository goalTrackingRepository, java.util.Date date, java.util.Date date2, int i, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(1, yb4);
        this.this$0 = goalTrackingRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$offset = i;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.GoalTrackingRepository$loadGoalTrackingDataList$response$1 goalTrackingRepository$loadGoalTrackingDataList$response$1 = new com.portfolio.platform.data.source.GoalTrackingRepository$loadGoalTrackingDataList$response$1(this.this$0, this.$startDate, this.$endDate, this.$offset, yb4);
        return goalTrackingRepository$loadGoalTrackingDataList$response$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj) {
        return ((com.portfolio.platform.data.source.GoalTrackingRepository$loadGoalTrackingDataList$response$1) create((com.fossil.blesdk.obfuscated.yb4) obj)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiServiceV2$p = this.this$0.mApiServiceV2;
            java.lang.String e = com.fossil.blesdk.obfuscated.rk2.m27397e(this.$startDate);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.formatShortDate(startDate)");
            java.lang.String e2 = com.fossil.blesdk.obfuscated.rk2.m27397e(this.$endDate);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e2, "DateHelper.formatShortDate(endDate)");
            int i2 = this.$offset;
            this.label = 1;
            obj = access$getMApiServiceV2$p.getGoalTrackingDataList(e, e2, i2, 100, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
