package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$response$Anon1", f = "HybridPresetRemoteDataSource.kt", l = {135}, m = "invokeSuspend")
public final class HybridPresetRemoteDataSource$deletePreset$response$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<Void>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ xz1 $jsonObject;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ HybridPresetRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridPresetRemoteDataSource$deletePreset$response$Anon1(HybridPresetRemoteDataSource hybridPresetRemoteDataSource, xz1 xz1, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = hybridPresetRemoteDataSource;
        this.$jsonObject = xz1;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new HybridPresetRemoteDataSource$deletePreset$response$Anon1(this.this$Anon0, this.$jsonObject, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((HybridPresetRemoteDataSource$deletePreset$response$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            ApiServiceV2 access$getMApiServiceV2$p = this.this$Anon0.mApiServiceV2;
            xz1 xz1 = this.$jsonObject;
            this.label = 1;
            obj = access$getMApiServiceV2$p.batchDeleteHybridPresetList(xz1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
