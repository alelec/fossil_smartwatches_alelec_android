package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingSummaryLocalDataSource$loadInitial$Anon1 implements PagingRequestHelper.b {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource this$Anon0;

    @DexIgnore
    public GoalTrackingSummaryLocalDataSource$loadInitial$Anon1(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        this.this$Anon0 = goalTrackingSummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(PagingRequestHelper.b.a aVar) {
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource = this.this$Anon0;
        goalTrackingSummaryLocalDataSource.calculateStartDate(goalTrackingSummaryLocalDataSource.mCreatedDate);
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource2 = this.this$Anon0;
        PagingRequestHelper.RequestType requestType = PagingRequestHelper.RequestType.INITIAL;
        Date mStartDate = goalTrackingSummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$Anon0.getMEndDate();
        kd4.a((Object) aVar, "helperCallback");
        fi4 unused = goalTrackingSummaryLocalDataSource2.loadData(requestType, mStartDate, mEndDate, aVar);
    }
}
