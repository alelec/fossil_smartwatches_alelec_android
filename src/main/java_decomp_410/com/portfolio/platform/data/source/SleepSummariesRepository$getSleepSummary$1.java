package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository$getSleepSummary$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $date;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSummariesRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$1$1")
    /* renamed from: com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$1$1 */
    public static final class C57161 extends com.portfolio.platform.util.NetworkBoundResource<com.portfolio.platform.data.model.room.sleep.MFSleepDay, com.fossil.blesdk.obfuscated.xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$1 this$0;

        @DexIgnore
        public C57161(com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$1 sleepSummariesRepository$getSleepSummary$1, java.util.List list) {
            this.this$0 = sleepSummariesRepository$getSleepSummary$1;
            this.$fitnessDataList = list;
        }

        @DexIgnore
        public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.fossil.blesdk.obfuscated.xz1>> yb4) {
            java.util.Date n = com.fossil.blesdk.obfuscated.rk2.m27409n(this.this$0.$date);
            java.util.Date i = com.fossil.blesdk.obfuscated.rk2.m27404i(this.this$0.$date);
            java.util.Calendar instance = java.util.Calendar.getInstance();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "calendar");
            instance.setTimeInMillis(0);
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
            java.lang.String e = com.fossil.blesdk.obfuscated.rk2.m27397e(n);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.formatShortDate(startDate)");
            java.lang.String e2 = com.fossil.blesdk.obfuscated.rk2.m27397e(i);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e2, "DateHelper.formatShortDate(endDate)");
            return access$getMApiService$p.getSleepSummaries(e, e2, 0, 100, yb4);
        }

        @DexIgnore
        public androidx.lifecycle.LiveData<com.portfolio.platform.data.model.room.sleep.MFSleepDay> loadFromDb() {
            java.util.Calendar instance = java.util.Calendar.getInstance();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "calendar");
            instance.setTime(this.this$0.$date);
            com.portfolio.platform.data.source.local.sleep.SleepDao access$getMSleepDao$p = this.this$0.this$0.mSleepDao;
            java.lang.String e = com.fossil.blesdk.obfuscated.rk2.m27397e(this.this$0.$date);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.formatShortDate(date)");
            return access$getMSleepDao$p.getSleepDayLiveData(e);
        }

        @DexIgnore
        public void onFetchFailed(java.lang.Throwable th) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "getActivityList onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(com.fossil.blesdk.obfuscated.xz1 xz1) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(xz1, "item");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
            local.mo33255d(tAG$app_fossilRelease, "getSleepSummary saveCallResult onResponse: response = " + xz1);
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$1 sleepSummariesRepository$getSleepSummary$1 = this.this$0;
            sleepSummariesRepository$getSleepSummary$1.this$0.saveSleepSummary$app_fossilRelease(xz1, sleepSummariesRepository$getSleepSummary$1.$date);
        }

        @DexIgnore
        public boolean shouldFetch(com.portfolio.platform.data.model.room.sleep.MFSleepDay mFSleepDay) {
            return this.$fitnessDataList.isEmpty();
        }
    }

    @DexIgnore
    public SleepSummariesRepository$getSleepSummary$1(com.portfolio.platform.data.source.SleepSummariesRepository sleepSummariesRepository, java.util.Date date) {
        this.this$0 = sleepSummariesRepository;
        this.$date = date;
    }

    @DexIgnore
    public final androidx.lifecycle.LiveData<com.fossil.blesdk.obfuscated.os3<com.portfolio.platform.data.model.room.sleep.MFSleepDay>> apply(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> list) {
        return new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$1.C57161(this, list).asLiveData();
    }
}
