package com.portfolio.platform.data.source.local.reminders;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RemindersSettingsDatabase_Impl extends RemindersSettingsDatabase {
    @DexIgnore
    public volatile InactivityNudgeTimeDao _inactivityNudgeTimeDao;
    @DexIgnore
    public volatile RemindTimeDao _remindTimeDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `inactivityNudgeTimeModel` (`nudgeTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, `nudgeTimeType` INTEGER NOT NULL, PRIMARY KEY(`nudgeTimeName`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `remindTimeModel` (`remindTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, PRIMARY KEY(`remindTimeName`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '0ee434ba350bbb753b81bda456b5c107')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `inactivityNudgeTimeModel`");
            ggVar.b("DROP TABLE IF EXISTS `remindTimeModel`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (RemindersSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = RemindersSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) RemindersSettingsDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = RemindersSettingsDatabase_Impl.this.mDatabase = ggVar;
            RemindersSettingsDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (RemindersSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = RemindersSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) RemindersSettingsDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("nudgeTimeName", new eg.a("nudgeTimeName", "TEXT", true, 1));
            hashMap.put("minutes", new eg.a("minutes", "INTEGER", true, 0));
            hashMap.put("nudgeTimeType", new eg.a("nudgeTimeType", "INTEGER", true, 0));
            eg egVar = new eg("inactivityNudgeTimeModel", hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar, "inactivityNudgeTimeModel");
            if (egVar.equals(a)) {
                HashMap hashMap2 = new HashMap(2);
                hashMap2.put("remindTimeName", new eg.a("remindTimeName", "TEXT", true, 1));
                hashMap2.put("minutes", new eg.a("minutes", "INTEGER", true, 0));
                eg egVar2 = new eg("remindTimeModel", hashMap2, new HashSet(0), new HashSet(0));
                eg a2 = eg.a(ggVar, "remindTimeModel");
                if (!egVar2.equals(a2)) {
                    throw new IllegalStateException("Migration didn't properly handle remindTimeModel(com.portfolio.platform.data.RemindTimeModel).\n Expected:\n" + egVar2 + "\n Found:\n" + a2);
                }
                return;
            }
            throw new IllegalStateException("Migration didn't properly handle inactivityNudgeTimeModel(com.portfolio.platform.data.InactivityNudgeTimeModel).\n Expected:\n" + egVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `inactivityNudgeTimeModel`");
            a.b("DELETE FROM `remindTimeModel`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), "inactivityNudgeTimeModel", "remindTimeModel");
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(1), "0ee434ba350bbb753b81bda456b5c107", "7e66671f5f316f81e6558b840c854b68");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }

    @DexIgnore
    public InactivityNudgeTimeDao getInactivityNudgeTimeDao() {
        InactivityNudgeTimeDao inactivityNudgeTimeDao;
        if (this._inactivityNudgeTimeDao != null) {
            return this._inactivityNudgeTimeDao;
        }
        synchronized (this) {
            if (this._inactivityNudgeTimeDao == null) {
                this._inactivityNudgeTimeDao = new InactivityNudgeTimeDao_Impl(this);
            }
            inactivityNudgeTimeDao = this._inactivityNudgeTimeDao;
        }
        return inactivityNudgeTimeDao;
    }

    @DexIgnore
    public RemindTimeDao getRemindTimeDao() {
        RemindTimeDao remindTimeDao;
        if (this._remindTimeDao != null) {
            return this._remindTimeDao;
        }
        synchronized (this) {
            if (this._remindTimeDao == null) {
                this._remindTimeDao = new RemindTimeDao_Impl(this);
            }
            remindTimeDao = this._remindTimeDao;
        }
        return remindTimeDao;
    }
}
