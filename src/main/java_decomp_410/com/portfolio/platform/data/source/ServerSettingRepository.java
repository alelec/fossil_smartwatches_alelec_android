package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ServerSettingRepository implements ServerSettingDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ServerSettingDataSource mServerSettingLocalDataSource;
    @DexIgnore
    public /* final */ ServerSettingDataSource mServerSettingRemoteDataSource;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ServerSettingRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = ServerSettingRepository.class.getSimpleName();
        kd4.a((Object) simpleName, "ServerSettingRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ServerSettingRepository(@Local ServerSettingDataSource serverSettingDataSource, @Remote ServerSettingDataSource serverSettingDataSource2) {
        kd4.b(serverSettingDataSource, "mServerSettingLocalDataSource");
        kd4.b(serverSettingDataSource2, "mServerSettingRemoteDataSource");
        this.mServerSettingLocalDataSource = serverSettingDataSource;
        this.mServerSettingRemoteDataSource = serverSettingDataSource2;
    }

    @DexIgnore
    public void addOrUpdateServerSetting(ServerSetting serverSetting) {
        this.mServerSettingLocalDataSource.addOrUpdateServerSetting(serverSetting);
    }

    @DexIgnore
    public void addOrUpdateServerSettingList(List<ServerSetting> list) {
        this.mServerSettingLocalDataSource.addOrUpdateServerSettingList(list);
    }

    @DexIgnore
    public void clearData() {
        this.mServerSettingLocalDataSource.clearData();
    }

    @DexIgnore
    public final ServerSetting generateSetting(String str, String str2) {
        kd4.b(str, "key");
        kd4.b(str2, "value");
        ServerSetting serverSetting = new ServerSetting();
        serverSetting.setKey(str);
        serverSetting.setValue(str2);
        return serverSetting;
    }

    @DexIgnore
    public ServerSetting getServerSettingByKey(String str) {
        kd4.b(str, "key");
        return this.mServerSettingLocalDataSource.getServerSettingByKey(str);
    }

    @DexIgnore
    public void getServerSettingList(ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        kd4.b(onGetServerSettingList, Constants.CALLBACK);
        this.mServerSettingRemoteDataSource.getServerSettingList(new ServerSettingRepository$getServerSettingList$Anon1(this, onGetServerSettingList));
    }
}
