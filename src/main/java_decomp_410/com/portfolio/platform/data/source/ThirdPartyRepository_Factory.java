package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.zk2;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ThirdPartyRepository_Factory implements Factory<ThirdPartyRepository> {
    @DexIgnore
    public /* final */ Provider<ActivitiesRepository> mActivitiesRepositoryProvider;
    @DexIgnore
    public /* final */ Provider<zk2> mGoogleFitHelperProvider;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> mPortfolioAppProvider;
    @DexIgnore
    public /* final */ Provider<ThirdPartyDatabase> mThirdPartyDatabaseProvider;

    @DexIgnore
    public ThirdPartyRepository_Factory(Provider<zk2> provider, Provider<ThirdPartyDatabase> provider2, Provider<ActivitiesRepository> provider3, Provider<PortfolioApp> provider4) {
        this.mGoogleFitHelperProvider = provider;
        this.mThirdPartyDatabaseProvider = provider2;
        this.mActivitiesRepositoryProvider = provider3;
        this.mPortfolioAppProvider = provider4;
    }

    @DexIgnore
    public static ThirdPartyRepository_Factory create(Provider<zk2> provider, Provider<ThirdPartyDatabase> provider2, Provider<ActivitiesRepository> provider3, Provider<PortfolioApp> provider4) {
        return new ThirdPartyRepository_Factory(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static ThirdPartyRepository newThirdPartyRepository(zk2 zk2, ThirdPartyDatabase thirdPartyDatabase, ActivitiesRepository activitiesRepository, PortfolioApp portfolioApp) {
        return new ThirdPartyRepository(zk2, thirdPartyDatabase, activitiesRepository, portfolioApp);
    }

    @DexIgnore
    public static ThirdPartyRepository provideInstance(Provider<zk2> provider, Provider<ThirdPartyDatabase> provider2, Provider<ActivitiesRepository> provider3, Provider<PortfolioApp> provider4) {
        return new ThirdPartyRepository(provider.get(), provider2.get(), provider3.get(), provider4.get());
    }

    @DexIgnore
    public ThirdPartyRepository get() {
        return provideInstance(this.mGoogleFitHelperProvider, this.mThirdPartyDatabaseProvider, this.mActivitiesRepositoryProvider, this.mPortfolioAppProvider);
    }
}
