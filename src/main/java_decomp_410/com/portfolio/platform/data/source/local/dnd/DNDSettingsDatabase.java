package com.portfolio.platform.data.source.local.dnd;

import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class DNDSettingsDatabase extends RoomDatabase {
    @DexIgnore
    public abstract DNDScheduledTimeDao getDNDScheduledTimeDao();
}
