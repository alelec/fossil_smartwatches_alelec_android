package com.portfolio.platform.data.source.local.sleep;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import java.util.Date;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1", f = "SleepSummaryLocalDataSource.kt", l = {176}, m = "invokeSuspend")
public final class SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qo2<xz1>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Pair $downloadingRange;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource$loadData$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1(SleepSummaryLocalDataSource$loadData$Anon1 sleepSummaryLocalDataSource$loadData$Anon1, Pair pair, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = sleepSummaryLocalDataSource$loadData$Anon1;
        this.$downloadingRange = pair;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1 sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1 = new SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1(this.this$Anon0, this.$downloadingRange, yb4);
        sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1.p$ = (zg4) obj;
        return sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            this.L$Anon0 = this.p$;
            this.label = 1;
            obj = SleepSessionsRepository.fetchSleepSessions$default(this.this$Anon0.this$Anon0.mSleepSessionsRepository, (Date) this.$downloadingRange.getFirst(), (Date) this.$downloadingRange.getSecond(), 0, 0, this, 12, (Object) null);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
