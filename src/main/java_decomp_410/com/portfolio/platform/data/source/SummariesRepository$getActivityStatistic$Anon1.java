package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.util.NetworkBoundResource;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getActivityStatistic$Anon1 extends NetworkBoundResource<ActivityStatistic, ActivityStatistic> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$Anon0;

    @DexIgnore
    public SummariesRepository$getActivityStatistic$Anon1(SummariesRepository summariesRepository, boolean z) {
        this.this$Anon0 = summariesRepository;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public Object createCall(yb4<? super qr4<ActivityStatistic>> yb4) {
        return this.this$Anon0.mApiServiceV2.getActivityStatistic(yb4);
    }

    @DexIgnore
    public LiveData<ActivityStatistic> loadFromDb() {
        return this.this$Anon0.mActivitySummaryDao.getActivityStatisticLiveData();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getActivityStatistic - onFetchFailed");
    }

    @DexIgnore
    public void saveCallResult(ActivityStatistic activityStatistic) {
        kd4.b(activityStatistic, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getActivityStatistic - saveCallResult -- item=" + activityStatistic);
        fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new SummariesRepository$getActivityStatistic$Anon1$saveCallResult$Anon1(this, activityStatistic, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public boolean shouldFetch(ActivityStatistic activityStatistic) {
        return this.$shouldFetch;
    }
}
