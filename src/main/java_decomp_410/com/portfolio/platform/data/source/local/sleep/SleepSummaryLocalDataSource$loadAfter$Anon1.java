package com.portfolio.platform.data.source.local.sleep;

import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummaryLocalDataSource$loadAfter$Anon1 implements PagingRequestHelper.b {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$Anon0;

    @DexIgnore
    public SleepSummaryLocalDataSource$loadAfter$Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        this.this$Anon0 = sleepSummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(PagingRequestHelper.b.a aVar) {
        Pair pair = (Pair) kb4.d(this.this$Anon0.mRequestAfterQueue);
        kd4.a((Object) aVar, "helperCallback");
        fi4 unused = this.this$Anon0.loadData(PagingRequestHelper.RequestType.AFTER, (Date) pair.getFirst(), (Date) pair.getSecond(), aVar);
    }
}
