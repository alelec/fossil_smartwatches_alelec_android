package com.portfolio.platform.data.source.local.sleep;

import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummaryLocalDataSource$loadInitial$Anon1 implements PagingRequestHelper.b {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$Anon0;

    @DexIgnore
    public SleepSummaryLocalDataSource$loadInitial$Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        this.this$Anon0 = sleepSummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(PagingRequestHelper.b.a aVar) {
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource = this.this$Anon0;
        sleepSummaryLocalDataSource.calculateStartDate(sleepSummaryLocalDataSource.mCreatedDate);
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource2 = this.this$Anon0;
        PagingRequestHelper.RequestType requestType = PagingRequestHelper.RequestType.INITIAL;
        Date mStartDate = sleepSummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$Anon0.getMEndDate();
        kd4.a((Object) aVar, "helperCallback");
        fi4 unused = sleepSummaryLocalDataSource2.loadData(requestType, mStartDate, mEndDate, aVar);
    }
}
