package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.jvm.internal.Ref$IntRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSessionRepository$getWorkoutSessions$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends NetworkBoundResource<List<WorkoutSession>, ApiResponse<ServerWorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$IntRef $offset;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionRepository$getWorkoutSessions$Anon1 this$Anon0;

        @DexIgnore
        public Anon1(WorkoutSessionRepository$getWorkoutSessions$Anon1 workoutSessionRepository$getWorkoutSessions$Anon1, Ref$IntRef ref$IntRef, int i, List list) {
            this.this$Anon0 = workoutSessionRepository$getWorkoutSessions$Anon1;
            this.$offset = ref$IntRef;
            this.$limit = i;
            this.$fitnessDataList = list;
        }

        @DexIgnore
        public Object createCall(yb4<? super qr4<ApiResponse<ServerWorkoutSession>>> yb4) {
            ApiServiceV2 access$getMApiService$p = this.this$Anon0.this$Anon0.mApiService;
            String e = rk2.e(this.this$Anon0.$startDate);
            kd4.a((Object) e, "DateHelper.formatShortDate(startDate)");
            String e2 = rk2.e(this.this$Anon0.$endDate);
            kd4.a((Object) e2, "DateHelper.formatShortDate(endDate)");
            return access$getMApiService$p.getWorkoutSessions(e, e2, this.$offset.element, this.$limit, yb4);
        }

        @DexIgnore
        public LiveData<List<WorkoutSession>> loadFromDb() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getWorkoutSessions - loadFromDb -- end=" + this.this$Anon0.$end + ", startDate=" + this.this$Anon0.$startDate + ", endDate=" + this.this$Anon0.$endDate);
            WorkoutDao access$getMWorkoutDao$p = this.this$Anon0.this$Anon0.mWorkoutDao;
            Date date = this.this$Anon0.$startDate;
            kd4.a((Object) date, GoalPhase.COLUMN_START_DATE);
            Date date2 = this.this$Anon0.$endDate;
            kd4.a((Object) date2, GoalPhase.COLUMN_END_DATE);
            return access$getMWorkoutDao$p.getWorkoutSessionsDesc(date, date2);
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - onFetchFailed");
        }

        @DexIgnore
        public boolean processContinueFetching(ApiResponse<ServerWorkoutSession> apiResponse) {
            kd4.b(apiResponse, "item");
            Range range = apiResponse.get_range();
            if (range == null || !range.isHasNext()) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - processContinueFetching -- hasNext=TRUE");
            this.$offset.element += this.$limit;
            return true;
        }

        @DexIgnore
        public void saveCallResult(ApiResponse<ServerWorkoutSession> apiResponse) {
            kd4.b(apiResponse, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
            StringBuilder sb = new StringBuilder();
            sb.append("getWorkoutSessions - saveCallResult -- item.size=");
            sb.append(apiResponse.get_items().size());
            sb.append(", hasNext=");
            Range range = apiResponse.get_range();
            sb.append(range != null ? Boolean.valueOf(range.isHasNext()) : null);
            local.d(tAG$app_fossilRelease, sb.toString());
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease2 = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
            local2.d(tAG$app_fossilRelease2, "getWorkoutSessions - saveCallResult -- items=" + apiResponse.get_items());
            ArrayList arrayList = new ArrayList();
            for (ServerWorkoutSession workoutSession : apiResponse.get_items()) {
                WorkoutSession workoutSession2 = workoutSession.toWorkoutSession();
                if (workoutSession2 != null) {
                    arrayList.add(workoutSession2);
                }
            }
            if (!apiResponse.get_items().isEmpty()) {
                this.this$Anon0.this$Anon0.mWorkoutDao.upsertListWorkoutSession(arrayList);
            }
            FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - saveCallResult -- DONE!!!");
        }

        @DexIgnore
        public boolean shouldFetch(List<WorkoutSession> list) {
            return this.this$Anon0.$shouldFetch && this.$fitnessDataList.isEmpty();
        }
    }

    @DexIgnore
    public WorkoutSessionRepository$getWorkoutSessions$Anon1(WorkoutSessionRepository workoutSessionRepository, boolean z, Date date, Date date2, Date date3) {
        this.this$Anon0 = workoutSessionRepository;
        this.$shouldFetch = z;
        this.$end = date;
        this.$startDate = date2;
        this.$endDate = date3;
    }

    @DexIgnore
    public final LiveData<os3<List<WorkoutSession>>> apply(List<FitnessDataWrapper> list) {
        Ref$IntRef ref$IntRef = new Ref$IntRef();
        ref$IntRef.element = 0;
        return new Anon1(this, ref$IntRef, 100, list).asLiveData();
    }
}
