package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.SleepSessionsRepository$fetchSleepSessions$repoResponse$1", mo27670f = "SleepSessionsRepository.kt", mo27671l = {172}, mo27672m = "invokeSuspend")
public final class SleepSessionsRepository$fetchSleepSessions$repoResponse$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.fossil.blesdk.obfuscated.xz1>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $start;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSessionsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$fetchSleepSessions$repoResponse$1(com.portfolio.platform.data.source.SleepSessionsRepository sleepSessionsRepository, java.util.Date date, java.util.Date date2, int i, int i2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(1, yb4);
        this.this$0 = sleepSessionsRepository;
        this.$start = date;
        this.$end = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.SleepSessionsRepository$fetchSleepSessions$repoResponse$1 sleepSessionsRepository$fetchSleepSessions$repoResponse$1 = new com.portfolio.platform.data.source.SleepSessionsRepository$fetchSleepSessions$repoResponse$1(this.this$0, this.$start, this.$end, this.$offset, this.$limit, yb4);
        return sleepSessionsRepository$fetchSleepSessions$repoResponse$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj) {
        return ((com.portfolio.platform.data.source.SleepSessionsRepository$fetchSleepSessions$repoResponse$1) create((com.fossil.blesdk.obfuscated.yb4) obj)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiService$p = this.this$0.mApiService;
            java.lang.String e = com.fossil.blesdk.obfuscated.rk2.m27397e(this.$start);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.formatShortDate(start)");
            java.lang.String e2 = com.fossil.blesdk.obfuscated.rk2.m27397e(this.$end);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e2, "DateHelper.formatShortDate(end)");
            int i2 = this.$offset;
            int i3 = this.$limit;
            this.label = 1;
            obj = access$getMApiService$p.getSleepSessions(e, e2, i2, i3, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
