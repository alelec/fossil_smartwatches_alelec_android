package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$3", mo27670f = "AlarmsRepository.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class AlarmsRepository$executePendingRequest$3 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.qo2 $upsertResponse;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21082p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.AlarmsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$executePendingRequest$3(com.portfolio.platform.data.source.AlarmsRepository alarmsRepository, com.fossil.blesdk.obfuscated.qo2 qo2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = alarmsRepository;
        this.$upsertResponse = qo2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$3 alarmsRepository$executePendingRequest$3 = new com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$3(this.this$0, this.$upsertResponse, yb4);
        alarmsRepository$executePendingRequest$3.f21082p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return alarmsRepository$executePendingRequest$3;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$3) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.util.List list = (java.util.List) ((com.fossil.blesdk.obfuscated.ro2) this.$upsertResponse).mo30673a();
            if (list == null) {
                return null;
            }
            this.this$0.mAlarmsLocalDataSource.insertAlarms(list);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
