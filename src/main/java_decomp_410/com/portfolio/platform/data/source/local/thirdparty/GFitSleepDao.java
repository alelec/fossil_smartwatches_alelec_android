package com.portfolio.platform.data.source.local.thirdparty;

import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface GFitSleepDao {
    @DexIgnore
    void clearAll();

    @DexIgnore
    void deleteListGFitSleep(List<GFitSleep> list);

    @DexIgnore
    List<GFitSleep> getAllGFitSleep();

    @DexIgnore
    void insertGFitSleep(GFitSleep gFitSleep);

    @DexIgnore
    void insertListGFitSleep(List<GFitSleep> list);
}
