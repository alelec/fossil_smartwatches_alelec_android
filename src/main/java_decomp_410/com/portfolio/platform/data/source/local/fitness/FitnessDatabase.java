package com.portfolio.platform.data.source.local.fitness;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.yf;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class FitnessDatabase extends RoomDatabase {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ yf MIGRATION_FROM_2_TO_13; // = new FitnessDatabase$Companion$MIGRATION_FROM_2_TO_13$Anon1(2, 13);
    @DexIgnore
    public static /* final */ yf MIGRATION_FROM_4_TO_21; // = new FitnessDatabase$Companion$MIGRATION_FROM_4_TO_21$Anon1(4, 21);
    @DexIgnore
    public static /* final */ String TAG; // = "FitnessDatabase";

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_2_TO_13$annotations() {
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_4_TO_21$annotations() {
        }

        @DexIgnore
        public final yf getMIGRATION_FROM_2_TO_13() {
            return FitnessDatabase.MIGRATION_FROM_2_TO_13;
        }

        @DexIgnore
        public final yf getMIGRATION_FROM_4_TO_21() {
            return FitnessDatabase.MIGRATION_FROM_4_TO_21;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public abstract ActivitySampleDao activitySampleDao();

    @DexIgnore
    public abstract ActivitySummaryDao activitySummaryDao();

    @DexIgnore
    public abstract FitnessDataDao getFitnessDataDao();

    @DexIgnore
    public abstract HeartRateDailySummaryDao getHeartRateDailySummaryDao();

    @DexIgnore
    public abstract HeartRateSampleDao getHeartRateDao();

    @DexIgnore
    public abstract WorkoutDao getWorkoutDao();

    @DexIgnore
    public abstract SampleRawDao sampleRawDao();
}
