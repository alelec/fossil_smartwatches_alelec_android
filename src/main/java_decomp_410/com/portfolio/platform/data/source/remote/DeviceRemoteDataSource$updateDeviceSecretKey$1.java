package com.portfolio.platform.data.source.remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.remote.DeviceRemoteDataSource", mo27670f = "DeviceRemoteDataSource.kt", mo27671l = {109}, mo27672m = "updateDeviceSecretKey")
public final class DeviceRemoteDataSource$updateDeviceSecretKey$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.remote.DeviceRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRemoteDataSource$updateDeviceSecretKey$1(com.portfolio.platform.data.source.remote.DeviceRemoteDataSource deviceRemoteDataSource, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = deviceRemoteDataSource;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.updateDeviceSecretKey((java.lang.String) null, (java.lang.String) null, this);
    }
}
