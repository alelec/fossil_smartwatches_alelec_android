package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatisticAwait$response$Anon1", f = "SleepSummariesRepository.kt", l = {394}, m = "invokeSuspend")
public final class SleepSummariesRepository$getSleepStatisticAwait$response$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<SleepStatistic>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepStatisticAwait$response$Anon1(SleepSummariesRepository sleepSummariesRepository, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = sleepSummariesRepository;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new SleepSummariesRepository$getSleepStatisticAwait$response$Anon1(this.this$Anon0, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((SleepSummariesRepository$getSleepStatisticAwait$response$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            ApiServiceV2 access$getMApiService$p = this.this$Anon0.mApiService;
            this.label = 1;
            obj = access$getMApiService$p.getSleepStatistic(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
