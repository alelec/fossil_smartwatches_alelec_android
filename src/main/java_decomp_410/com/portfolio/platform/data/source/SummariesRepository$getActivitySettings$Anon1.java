package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.util.NetworkBoundResource;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getActivitySettings$Anon1 extends NetworkBoundResource<ActivitySettings, ActivitySettings> {
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$Anon0;

    @DexIgnore
    public SummariesRepository$getActivitySettings$Anon1(SummariesRepository summariesRepository) {
        this.this$Anon0 = summariesRepository;
    }

    @DexIgnore
    public Object createCall(yb4<? super qr4<ActivitySettings>> yb4) {
        return this.this$Anon0.mApiServiceV2.getActivitySetting(yb4);
    }

    @DexIgnore
    public LiveData<ActivitySettings> loadFromDb() {
        return this.this$Anon0.mActivitySummaryDao.getActivitySettingLiveData();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getActivitySettings - onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(ActivitySettings activitySettings) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(ActivitySettings activitySettings) {
        kd4.b(activitySettings, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getActivitySettings - saveCallResult -- item=" + activitySettings);
        fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new SummariesRepository$getActivitySettings$Anon1$saveCallResult$Anon1(this, activitySettings, (yb4) null), 3, (Object) null);
    }
}
