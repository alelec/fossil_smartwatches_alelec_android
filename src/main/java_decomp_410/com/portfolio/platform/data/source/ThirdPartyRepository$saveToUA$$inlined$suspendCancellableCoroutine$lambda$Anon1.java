package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dg4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gr3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.ua.UAActivityTimeSeries;
import java.util.List;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$IntRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ UAActivityTimeSeries $activityTimeSeries;
    @DexIgnore
    public /* final */ /* synthetic */ dg4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$IntRef $countSizeOfLists$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $sampleList;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfLists$inlined;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements gr3.d {
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0119Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0119Anon1(Anon1 anon1, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0119Anon1 anon1 = new C0119Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0119Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                cc4.a();
                if (this.label == 0) {
                    na4.a(obj);
                    this.this$Anon0.this$Anon0.this$Anon0.getMThirdPartyDatabase().getUASampleDao().deleteListUASample(this.this$Anon0.this$Anon0.$sampleList);
                    return qa4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public Anon1(ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1) {
            this.this$Anon0 = thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        public void onSuccess() {
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "Sending UASample to UnderAmour successfully!");
            fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new C0119Anon1(this, (yb4) null), 3, (Object) null);
            ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1 = this.this$Anon0;
            Ref$IntRef ref$IntRef = thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1.$countSizeOfLists$inlined;
            ref$IntRef.element++;
            if (ref$IntRef.element >= thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1.$sizeOfLists$inlined && thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1.$continuation$inlined.isActive()) {
                FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveToUA");
                dg4 dg4 = this.this$Anon0.$continuation$inlined;
                Result.a aVar = Result.Companion;
                dg4.resumeWith(Result.m3constructorimpl((Object) null));
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1(UAActivityTimeSeries uAActivityTimeSeries, List list, yb4 yb4, Ref$IntRef ref$IntRef, int i, dg4 dg4, ThirdPartyRepository thirdPartyRepository) {
        super(2, yb4);
        this.$activityTimeSeries = uAActivityTimeSeries;
        this.$sampleList = list;
        this.$countSizeOfLists$inlined = ref$IntRef;
        this.$sizeOfLists$inlined = i;
        this.$continuation$inlined = dg4;
        this.this$Anon0 = thirdPartyRepository;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1 = new ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1(this.$activityTimeSeries, this.$sampleList, yb4, this.$countSizeOfLists$inlined, this.$sizeOfLists$inlined, this.$continuation$inlined, this.this$Anon0);
        thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1.p$ = (zg4) obj;
        return thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            gr3 r = this.this$Anon0.getMPortfolioApp().r();
            UAActivityTimeSeries uAActivityTimeSeries = this.$activityTimeSeries;
            Anon1 anon1 = new Anon1(this);
            this.L$Anon0 = zg4;
            this.label = 1;
            if (r.a(uAActivityTimeSeries, (gr3.d) anon1, (yb4<? super qa4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
