package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository$updateLastSleepGoal$1 extends com.portfolio.platform.util.NetworkBoundResource<java.lang.Integer, com.portfolio.platform.data.model.room.sleep.MFSleepSettings> {
    @DexIgnore
    public /* final */ /* synthetic */ int $sleepGoal;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSummariesRepository this$0;

    @DexIgnore
    public SleepSummariesRepository$updateLastSleepGoal$1(com.portfolio.platform.data.source.SleepSummariesRepository sleepSummariesRepository, int i) {
        this.this$0 = sleepSummariesRepository;
        this.$sleepGoal = i;
    }

    @DexIgnore
    public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.model.room.sleep.MFSleepSettings>> yb4) {
        com.fossil.blesdk.obfuscated.xz1 xz1 = new com.fossil.blesdk.obfuscated.xz1();
        try {
            xz1.mo17939a("currentGoalMinutes", (java.lang.Number) com.fossil.blesdk.obfuscated.dc4.m20843a(this.$sleepGoal));
            java.util.TimeZone timeZone = java.util.TimeZone.getDefault();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) timeZone, "TimeZone.getDefault()");
            xz1.mo17939a("timezoneOffset", (java.lang.Number) com.fossil.blesdk.obfuscated.dc4.m20843a(timeZone.getRawOffset() / 1000));
        } catch (java.lang.Exception unused) {
        }
        return this.this$0.mApiService.setSleepSetting(xz1, yb4);
    }

    @DexIgnore
    public androidx.lifecycle.LiveData<java.lang.Integer> loadFromDb() {
        return this.this$0.mSleepDao.getLastSleepGoal();
    }

    @DexIgnore
    public void onFetchFailed(java.lang.Throwable th) {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "fetchActivitySettings onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(java.lang.Integer num) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(com.portfolio.platform.data.model.room.sleep.MFSleepSettings mFSleepSettings) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(mFSleepSettings, "item");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
        local.mo33255d(tAG$app_fossilRelease, "updateLastSleepGoal saveCallResult goal: " + mFSleepSettings);
        this.this$0.saveSleepSettingToDB$app_fossilRelease(mFSleepSettings.getSleepGoal());
    }
}
