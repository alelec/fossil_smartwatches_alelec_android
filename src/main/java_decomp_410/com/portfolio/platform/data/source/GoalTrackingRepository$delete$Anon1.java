package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import java.util.List;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.GoalTrackingRepository", f = "GoalTrackingRepository.kt", l = {566}, m = "delete")
public final class GoalTrackingRepository$delete$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$delete$Anon1(GoalTrackingRepository goalTrackingRepository, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = goalTrackingRepository;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.delete((List<GoalTrackingData>) null, this);
    }
}
