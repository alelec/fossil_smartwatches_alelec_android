package com.portfolio.platform.data.source.local.sleep;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummaryLocalDataSource$loadInitial$1 implements com.portfolio.platform.helper.PagingRequestHelper.C5952b {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource this$0;

    @DexIgnore
    public SleepSummaryLocalDataSource$loadInitial$1(com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        this.this$0 = sleepSummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a aVar) {
        com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource sleepSummaryLocalDataSource = this.this$0;
        sleepSummaryLocalDataSource.calculateStartDate(sleepSummaryLocalDataSource.mCreatedDate);
        com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource sleepSummaryLocalDataSource2 = this.this$0;
        com.portfolio.platform.helper.PagingRequestHelper.RequestType requestType = com.portfolio.platform.helper.PagingRequestHelper.RequestType.INITIAL;
        java.util.Date mStartDate = sleepSummaryLocalDataSource2.getMStartDate();
        java.util.Date mEndDate = this.this$0.getMEndDate();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) aVar, "helperCallback");
        com.fossil.blesdk.obfuscated.fi4 unused = sleepSummaryLocalDataSource2.loadData(requestType, mStartDate, mEndDate, aVar);
    }
}
