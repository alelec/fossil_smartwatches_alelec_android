package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConverterShortDate;
import com.portfolio.platform.response.sleep.SleepSessionParse;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.jvm.internal.Ref$IntRef;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSessionsRepository$getSleepSessionList$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends NetworkBoundResource<List<MFSleepSession>, xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ Pair $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$IntRef $offset;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSessionsRepository$getSleepSessionList$Anon1 this$Anon0;

        @DexIgnore
        public Anon1(SleepSessionsRepository$getSleepSessionList$Anon1 sleepSessionsRepository$getSleepSessionList$Anon1, Ref$IntRef ref$IntRef, int i, Pair pair) {
            this.this$Anon0 = sleepSessionsRepository$getSleepSessionList$Anon1;
            this.$offset = ref$IntRef;
            this.$limit = i;
            this.$downloadingDate = pair;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
            if (r0 != null) goto L_0x0019;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
            if (r0 != null) goto L_0x0033;
         */
        @DexIgnore
        public Object createCall(yb4<? super qr4<xz1>> yb4) {
            Date date;
            Date date2;
            ApiServiceV2 access$getMApiService$p = this.this$Anon0.this$Anon0.mApiService;
            Pair pair = this.$downloadingDate;
            if (pair != null) {
                date = (Date) pair.getFirst();
            }
            date = this.this$Anon0.$startDate;
            String e = rk2.e(date);
            kd4.a((Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            Pair pair2 = this.$downloadingDate;
            if (pair2 != null) {
                date2 = (Date) pair2.getSecond();
            }
            date2 = this.this$Anon0.$endDate;
            String e2 = rk2.e(date2);
            kd4.a((Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiService$p.getSleepSessions(e, e2, this.$offset.element, this.$limit, yb4);
        }

        @DexIgnore
        public LiveData<List<MFSleepSession>> loadFromDb() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
            StringBuilder sb = new StringBuilder();
            sb.append("getSleepSessionList: startMilli = ");
            Date date = this.this$Anon0.$startDate;
            kd4.a((Object) date, GoalPhase.COLUMN_START_DATE);
            sb.append(date.getTime());
            sb.append(", endMilli = ");
            Date date2 = this.this$Anon0.$endDate;
            kd4.a((Object) date2, GoalPhase.COLUMN_END_DATE);
            sb.append(date2.getTime());
            local.d(tAG$app_fossilRelease, sb.toString());
            SleepDao access$getMSleepDao$p = this.this$Anon0.this$Anon0.mSleepDao;
            Date date3 = this.this$Anon0.$startDate;
            kd4.a((Object) date3, GoalPhase.COLUMN_START_DATE);
            long time = date3.getTime();
            Date date4 = this.this$Anon0.$endDate;
            kd4.a((Object) date4, GoalPhase.COLUMN_END_DATE);
            return access$getMSleepDao$p.getSleepSessionsLiveData(time, date4.getTime());
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().d(SleepSessionsRepository.Companion.getTAG$app_fossilRelease(), "getSleepSessionList onFetchFailed");
        }

        @DexIgnore
        public boolean processContinueFetching(xz1 xz1) {
            kd4.b(xz1, "item");
            rz1 rz1 = new rz1();
            rz1.a(DateTime.class, new GsonConvertDateTime());
            rz1.a(Date.class, new GsonConverterShortDate());
            Range range = ((ApiResponse) rz1.a().a(xz1.toString(), new SleepSessionsRepository$getSleepSessionList$Anon1$Anon1$processContinueFetching$sleepSessionList$Anon1().getType())).get_range();
            if (range == null || !range.isHasNext()) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d(SleepSessionsRepository.Companion.getTAG$app_fossilRelease(), "getSleepSessionList getActivityList processContinueFetching hasNext");
            this.$offset.element += this.$limit;
            return true;
        }

        @DexIgnore
        public void saveCallResult(xz1 xz1) {
            kd4.b(xz1, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getSleepSessionList saveCallResult onResponse: response = " + xz1);
            ArrayList arrayList = new ArrayList();
            try {
                rz1 rz1 = new rz1();
                rz1.a(DateTime.class, new GsonConvertDateTime());
                rz1.a(Date.class, new GsonConverterShortDate());
                ApiResponse apiResponse = (ApiResponse) rz1.a().a(xz1.toString(), new SleepSessionsRepository$getSleepSessionList$Anon1$Anon1$saveCallResult$Anon1().getType());
                if (apiResponse != null) {
                    List<SleepSessionParse> list = apiResponse.get_items();
                    if (list != null) {
                        for (SleepSessionParse mfSleepSessionBySleepSessionParse : list) {
                            arrayList.add(mfSleepSessionBySleepSessionParse.getMfSleepSessionBySleepSessionParse());
                        }
                    }
                }
                this.this$Anon0.this$Anon0.insert$app_fossilRelease(arrayList);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease2 = SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
                StringBuilder sb = new StringBuilder();
                sb.append("getSleepSessionList saveCallResult exception=");
                e.printStackTrace();
                sb.append(qa4.a);
                local2.e(tAG$app_fossilRelease2, sb.toString());
            }
        }

        @DexIgnore
        public boolean shouldFetch(List<MFSleepSession> list) {
            return this.this$Anon0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public SleepSessionsRepository$getSleepSessionList$Anon1(SleepSessionsRepository sleepSessionsRepository, Date date, Date date2, boolean z) {
        this.this$Anon0 = sleepSessionsRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public final LiveData<os3<List<MFSleepSession>>> apply(List<FitnessDataWrapper> list) {
        Ref$IntRef ref$IntRef = new Ref$IntRef();
        ref$IntRef.element = 0;
        kd4.a((Object) list, "fitnessDataList");
        Date date = this.$startDate;
        kd4.a((Object) date, GoalPhase.COLUMN_START_DATE);
        Date date2 = this.$endDate;
        kd4.a((Object) date2, GoalPhase.COLUMN_END_DATE);
        return new Anon1(this, ref$IntRef, 300, FitnessDataWrapperKt.calculateRangeDownload(list, date, date2)).asLiveData();
    }
}
