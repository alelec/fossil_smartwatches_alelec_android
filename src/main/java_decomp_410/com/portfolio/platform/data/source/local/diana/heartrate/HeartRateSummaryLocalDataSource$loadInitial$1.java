package com.portfolio.platform.data.source.local.diana.heartrate;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSummaryLocalDataSource$loadInitial$1 implements com.portfolio.platform.helper.PagingRequestHelper.C5952b {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource this$0;

    @DexIgnore
    public HeartRateSummaryLocalDataSource$loadInitial$1(com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        this.this$0 = heartRateSummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a aVar) {
        com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource = this.this$0;
        heartRateSummaryLocalDataSource.calculateStartDate(heartRateSummaryLocalDataSource.mCreatedDate);
        com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource2 = this.this$0;
        java.util.Date mStartDate = heartRateSummaryLocalDataSource2.getMStartDate();
        java.util.Date mEndDate = this.this$0.getMEndDate();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) aVar, "helperCallback");
        com.fossil.blesdk.obfuscated.fi4 unused = heartRateSummaryLocalDataSource2.loadData(mStartDate, mEndDate, aVar);
    }
}
