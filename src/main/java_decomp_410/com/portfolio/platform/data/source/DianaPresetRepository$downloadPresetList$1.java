package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.DianaPresetRepository", mo27670f = "DianaPresetRepository.kt", mo27671l = {157, 160}, mo27672m = "downloadPresetList")
public final class DianaPresetRepository$downloadPresetList$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.DianaPresetRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaPresetRepository$downloadPresetList$1(com.portfolio.platform.data.source.DianaPresetRepository dianaPresetRepository, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = dianaPresetRepository;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.downloadPresetList((java.lang.String) null, this);
    }
}
