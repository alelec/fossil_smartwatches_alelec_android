package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository_Factory implements Factory<SleepSummariesRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;
    @DexIgnore
    public /* final */ Provider<FitnessDataDao> mFitnessDataDaoProvider;
    @DexIgnore
    public /* final */ Provider<SleepDao> mSleepDaoProvider;

    @DexIgnore
    public SleepSummariesRepository_Factory(Provider<SleepDao> provider, Provider<ApiServiceV2> provider2, Provider<FitnessDataDao> provider3) {
        this.mSleepDaoProvider = provider;
        this.mApiServiceProvider = provider2;
        this.mFitnessDataDaoProvider = provider3;
    }

    @DexIgnore
    public static SleepSummariesRepository_Factory create(Provider<SleepDao> provider, Provider<ApiServiceV2> provider2, Provider<FitnessDataDao> provider3) {
        return new SleepSummariesRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static SleepSummariesRepository newSleepSummariesRepository(SleepDao sleepDao, ApiServiceV2 apiServiceV2, FitnessDataDao fitnessDataDao) {
        return new SleepSummariesRepository(sleepDao, apiServiceV2, fitnessDataDao);
    }

    @DexIgnore
    public static SleepSummariesRepository provideInstance(Provider<SleepDao> provider, Provider<ApiServiceV2> provider2, Provider<FitnessDataDao> provider3) {
        return new SleepSummariesRepository(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public SleepSummariesRepository get() {
        return provideInstance(this.mSleepDaoProvider, this.mApiServiceProvider, this.mFitnessDataDaoProvider);
    }
}
