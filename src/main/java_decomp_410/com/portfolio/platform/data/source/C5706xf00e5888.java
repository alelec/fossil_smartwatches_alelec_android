package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.source.NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory */
public final class C5706xf00e5888 implements dagger.internal.Factory<com.portfolio.platform.data.source.NotificationsDataSource> {
    @DexIgnore
    public /* final */ com.portfolio.platform.data.source.NotificationsRepositoryModule module;

    @DexIgnore
    public C5706xf00e5888(com.portfolio.platform.data.source.NotificationsRepositoryModule notificationsRepositoryModule) {
        this.module = notificationsRepositoryModule;
    }

    @DexIgnore
    public static com.portfolio.platform.data.source.C5706xf00e5888 create(com.portfolio.platform.data.source.NotificationsRepositoryModule notificationsRepositoryModule) {
        return new com.portfolio.platform.data.source.C5706xf00e5888(notificationsRepositoryModule);
    }

    @DexIgnore
    public static com.portfolio.platform.data.source.NotificationsDataSource provideInstance(com.portfolio.platform.data.source.NotificationsRepositoryModule notificationsRepositoryModule) {
        return proxyProvideLocalNotificationsDataSource(notificationsRepositoryModule);
    }

    @DexIgnore
    public static com.portfolio.platform.data.source.NotificationsDataSource proxyProvideLocalNotificationsDataSource(com.portfolio.platform.data.source.NotificationsRepositoryModule notificationsRepositoryModule) {
        com.portfolio.platform.data.source.NotificationsDataSource provideLocalNotificationsDataSource = notificationsRepositoryModule.provideLocalNotificationsDataSource();
        com.fossil.blesdk.obfuscated.n44.m25602a(provideLocalNotificationsDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideLocalNotificationsDataSource;
    }

    @DexIgnore
    public com.portfolio.platform.data.source.NotificationsDataSource get() {
        return provideInstance(this.module);
    }
}
