package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.response.ResponseKt;
import java.net.SocketTimeoutException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource$getServerSettingList$Anon1", f = "ServerSettingRemoteDataSource.kt", l = {38}, m = "invokeSuspend")
public final class ServerSettingRemoteDataSource$getServerSettingList$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingDataSource.OnGetServerSettingList $callback;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ServerSettingRemoteDataSource$getServerSettingList$Anon1(ServerSettingRemoteDataSource serverSettingRemoteDataSource, ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = serverSettingRemoteDataSource;
        this.$callback = onGetServerSettingList;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ServerSettingRemoteDataSource$getServerSettingList$Anon1 serverSettingRemoteDataSource$getServerSettingList$Anon1 = new ServerSettingRemoteDataSource$getServerSettingList$Anon1(this.this$Anon0, this.$callback, yb4);
        serverSettingRemoteDataSource$getServerSettingList$Anon1.p$ = (zg4) obj;
        return serverSettingRemoteDataSource$getServerSettingList$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ServerSettingRemoteDataSource$getServerSettingList$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ServerSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1 serverSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1 = new ServerSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = ResponseKt.a(serverSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo2 = (qo2) obj;
        if (qo2 instanceof ro2) {
            FLogger.INSTANCE.getLocal().e(ServerSettingRemoteDataSource.Companion.getTAG$app_fossilRelease(), "getServerSettingList- is successful");
            this.$callback.onSuccess((ServerSettingList) ((ro2) qo2).a());
        } else if (qo2 instanceof po2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ServerSettingRemoteDataSource.Companion.getTAG$app_fossilRelease();
            StringBuilder sb = new StringBuilder();
            sb.append("getServerSettingList - Failure, code=");
            po2 po2 = (po2) qo2;
            sb.append(po2.a());
            local.e(tAG$app_fossilRelease, sb.toString());
            if (po2.d() instanceof SocketTimeoutException) {
                this.$callback.onFailed(MFNetworkReturnCode.CLIENT_TIMEOUT);
            } else {
                this.$callback.onFailed(601);
            }
        }
        return qa4.a;
    }
}
