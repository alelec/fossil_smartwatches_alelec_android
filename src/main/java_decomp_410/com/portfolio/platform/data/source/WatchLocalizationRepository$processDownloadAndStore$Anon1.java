package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.em4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.vk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.response.ResponseKt;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.WatchLocalizationRepository$processDownloadAndStore$Anon1", f = "WatchLocalizationRepository.kt", l = {66}, m = "invokeSuspend")
public final class WatchLocalizationRepository$processDownloadAndStore$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super String>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $path;
    @DexIgnore
    public /* final */ /* synthetic */ String $url;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchLocalizationRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchLocalizationRepository$processDownloadAndStore$Anon1(WatchLocalizationRepository watchLocalizationRepository, String str, String str2, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = watchLocalizationRepository;
        this.$url = str;
        this.$path = str2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WatchLocalizationRepository$processDownloadAndStore$Anon1 watchLocalizationRepository$processDownloadAndStore$Anon1 = new WatchLocalizationRepository$processDownloadAndStore$Anon1(this.this$Anon0, this.$url, this.$path, yb4);
        watchLocalizationRepository$processDownloadAndStore$Anon1.p$ = (zg4) obj;
        return watchLocalizationRepository$processDownloadAndStore$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WatchLocalizationRepository$processDownloadAndStore$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1 watchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1 = new WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = ResponseKt.a(watchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo2 = (qo2) obj;
        if (qo2 instanceof ro2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$p = this.this$Anon0.TAG;
            local.d(access$getTAG$p, "start storing for url: " + this.$url + " to path: " + this.$path);
            vk2 vk2 = vk2.a;
            Object a2 = ((ro2) qo2).a();
            if (a2 == null) {
                kd4.a();
                throw null;
            } else if (vk2.a((em4) a2, this.$path)) {
                return this.$path;
            } else {
                return null;
            }
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String access$getTAG$p2 = this.this$Anon0.TAG;
            local2.d(access$getTAG$p2, "download: " + this.$url + " | FAILED");
            return null;
        }
    }
}
