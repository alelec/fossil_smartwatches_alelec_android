package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.SleepStatistic;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1", f = "SleepSummariesRepository.kt", l = {}, m = "invokeSuspend")
public final class SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepStatistic $item;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository$getSleepStatistic$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1(SleepSummariesRepository$getSleepStatistic$Anon1 sleepSummariesRepository$getSleepStatistic$Anon1, SleepStatistic sleepStatistic, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = sleepSummariesRepository$getSleepStatistic$Anon1;
        this.$item = sleepStatistic;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1 sleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1 = new SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1(this.this$Anon0, this.$item, yb4);
        sleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1.p$ = (zg4) obj;
        return sleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            this.this$Anon0.this$Anon0.mSleepDao.upsertSleepStatistic(this.$item);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
