package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$updateActivitySettings$1 extends com.portfolio.platform.util.NetworkBoundResource<com.portfolio.platform.data.model.room.fitness.ActivitySettings, com.portfolio.platform.data.model.room.fitness.ActivitySettings> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.room.fitness.ActivitySettings $activitySettings;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SummariesRepository this$0;

    @DexIgnore
    public SummariesRepository$updateActivitySettings$1(com.portfolio.platform.data.source.SummariesRepository summariesRepository, com.portfolio.platform.data.model.room.fitness.ActivitySettings activitySettings) {
        this.this$0 = summariesRepository;
        this.$activitySettings = activitySettings;
    }

    @DexIgnore
    public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.model.room.fitness.ActivitySettings>> yb4) {
        com.fossil.blesdk.obfuscated.rz1 rz1 = new com.fossil.blesdk.obfuscated.rz1();
        rz1.mo15801b(new com.fossil.blesdk.obfuscated.jk2());
        com.google.gson.JsonElement b = rz1.mo15799a().mo23102b((java.lang.Object) this.$activitySettings);
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) b, "GsonBuilder()\n          \u2026sonTree(activitySettings)");
        com.fossil.blesdk.obfuscated.xz1 d = b.mo23113d();
        com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiServiceV2$p = this.this$0.mApiServiceV2;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) d, "jsonObject");
        return access$getMApiServiceV2$p.updateActivitySetting(d, yb4);
    }

    @DexIgnore
    public androidx.lifecycle.LiveData<com.portfolio.platform.data.model.room.fitness.ActivitySettings> loadFromDb() {
        return this.this$0.mActivitySummaryDao.getActivitySettingLiveData();
    }

    @DexIgnore
    public void onFetchFailed(java.lang.Throwable th) {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(com.portfolio.platform.data.source.SummariesRepository.TAG, "updateActivitySettings - onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(com.portfolio.platform.data.model.room.fitness.ActivitySettings activitySettings) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(com.portfolio.platform.data.model.room.fitness.ActivitySettings activitySettings) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(activitySettings, "item");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "updateActivitySettings - saveCallResult -- item=" + activitySettings);
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.SummariesRepository$updateActivitySettings$1$saveCallResult$1(this, activitySettings, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }
}
