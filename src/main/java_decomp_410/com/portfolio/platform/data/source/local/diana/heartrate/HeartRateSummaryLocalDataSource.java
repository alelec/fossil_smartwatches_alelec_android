package com.portfolio.platform.data.source.local.diana.heartrate;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.jl2;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.od;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSummaryLocalDataSource extends od<Date, DailyHeartRateSummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ PagingRequestHelper.a listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public /* final */ HeartRateDailySummaryDao mHeartRateSummaryDao;
    @DexIgnore
    public PagingRequestHelper mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState; // = jl2.a(this.mHelper);
    @DexIgnore
    public /* final */ pf.c mObserver;
    @DexIgnore
    public List<Pair<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public Date mStartDate; // = new Date();
    @DexIgnore
    public /* final */ HeartRateSummaryRepository mSummariesRepository;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends pf.c {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSummaryLocalDataSource this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$Anon0 = heartRateSummaryLocalDataSource;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            kd4.b(set, "tables");
            this.this$Anon0.invalidate();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            kd4.b(date, "date");
            kd4.b(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(getTAG$app_fossilRelease(), "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar c = rk2.c(instance);
            if (rk2.b(date2, c.getTime())) {
                c.setTime(date2);
            }
            Date time = c.getTime();
            kd4.a((Object) time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return HeartRateSummaryLocalDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = HeartRateSummaryLocalDataSource.class.getSimpleName();
        kd4.a((Object) simpleName, "HeartRateSummaryLocalDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public HeartRateSummaryLocalDataSource(HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, HeartRateDailySummaryDao heartRateDailySummaryDao, FitnessDatabase fitnessDatabase, Date date, h42 h42, PagingRequestHelper.a aVar, Calendar calendar) {
        kd4.b(heartRateSummaryRepository, "mSummariesRepository");
        kd4.b(fitnessDataRepository, "mFitnessDataRepository");
        kd4.b(heartRateDailySummaryDao, "mHeartRateSummaryDao");
        kd4.b(fitnessDatabase, "mFitnessDatabase");
        kd4.b(date, "mCreatedDate");
        kd4.b(h42, "appExecutors");
        kd4.b(aVar, "listener");
        kd4.b(calendar, "key");
        this.mSummariesRepository = heartRateSummaryRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mHeartRateSummaryDao = heartRateDailySummaryDao;
        this.mFitnessDatabase = fitnessDatabase;
        this.mCreatedDate = date;
        this.listener = aVar;
        this.key = calendar;
        this.mHelper = new PagingRequestHelper(h42.a());
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "daily_heart_rate_summary", new String[0]);
        this.mFitnessDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar c = rk2.c(instance);
        c.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + c.getTime());
        Date time = c.getTime();
        kd4.a((Object) time, "calendar.time");
        this.mStartDate = time;
        if (rk2.b(date, this.mStartDate)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00d1  */
    private final List<DailyHeartRateSummary> calculateSummaries(List<DailyHeartRateSummary> list) {
        int i;
        int i2;
        List<DailyHeartRateSummary> list2 = list;
        int i3 = 1;
        if (!list.isEmpty()) {
            DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) kb4.f(list);
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "endCalendar");
            instance.setTime(dailyHeartRateSummary.getDate());
            if (instance.get(7) != 1) {
                Resting resting = dailyHeartRateSummary.getResting();
                if (resting != null) {
                    i = resting.getValue();
                    Calendar instance2 = Calendar.getInstance();
                    kd4.a((Object) instance2, "calendar");
                    instance2.setTime(((DailyHeartRateSummary) kb4.d(list)).getDate());
                    Calendar p = rk2.p(instance2.getTime());
                    kd4.a((Object) p, "DateHelper.getStartOfWeek(calendar.time)");
                    p.add(5, -1);
                    Calendar instance3 = Calendar.getInstance();
                    int i4 = 0;
                    int i5 = 0;
                    i2 = 0;
                    int i6 = 0;
                    for (T next : list) {
                        int i7 = i6 + 1;
                        if (i6 >= 0) {
                            DailyHeartRateSummary dailyHeartRateSummary2 = (DailyHeartRateSummary) next;
                            kd4.a((Object) instance3, "mSummaryCalendar");
                            instance3.setTime(dailyHeartRateSummary2.getDate());
                            if (instance3.get(5) == p.get(5)) {
                                DailyHeartRateSummary dailyHeartRateSummary3 = list2.get(i4);
                                if (i2 <= 0) {
                                    i2 = 1;
                                }
                                dailyHeartRateSummary3.setAvgRestingHeartRateOfWeek(Integer.valueOf(i5 / i2));
                                p.add(5, -7);
                                i4 = i6;
                                i5 = 0;
                                i2 = 0;
                            }
                            Resting resting2 = dailyHeartRateSummary2.getResting();
                            if (resting2 != null) {
                                int value = resting2.getValue();
                                if (value > 0) {
                                    i5 += value;
                                    i2++;
                                }
                            }
                            if (i6 == list.size() - 1 && i > 0) {
                                i5 += i;
                                i2++;
                            }
                            i6 = i7;
                        } else {
                            cb4.c();
                            throw null;
                        }
                    }
                    DailyHeartRateSummary dailyHeartRateSummary4 = list2.get(i4);
                    if (i2 > 0) {
                        i3 = i2;
                    }
                    dailyHeartRateSummary4.setAvgRestingHeartRateOfWeek(Integer.valueOf(i5 / i3));
                }
            }
            i = 0;
            Calendar instance22 = Calendar.getInstance();
            kd4.a((Object) instance22, "calendar");
            instance22.setTime(((DailyHeartRateSummary) kb4.d(list)).getDate());
            Calendar p2 = rk2.p(instance22.getTime());
            kd4.a((Object) p2, "DateHelper.getStartOfWeek(calendar.time)");
            p2.add(5, -1);
            Calendar instance32 = Calendar.getInstance();
            int i42 = 0;
            int i52 = 0;
            i2 = 0;
            int i62 = 0;
            while (r7.hasNext()) {
            }
            DailyHeartRateSummary dailyHeartRateSummary42 = list2.get(i42);
            if (i2 > 0) {
            }
            dailyHeartRateSummary42.setAvgRestingHeartRateOfWeek(Integer.valueOf(i52 / i3));
        }
        return list2;
    }

    @DexIgnore
    private final DailyHeartRateSummary dummySummary(Date date) {
        DailyHeartRateSummary dailyHeartRateSummary = new DailyHeartRateSummary(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, date, System.currentTimeMillis(), System.currentTimeMillis(), 0, 0, 0, (Resting) null);
        dailyHeartRateSummary.setCreatedAt(System.currentTimeMillis());
        dailyHeartRateSummary.setUpdatedAt(System.currentTimeMillis());
        return dailyHeartRateSummary;
    }

    @DexIgnore
    private final List<DailyHeartRateSummary> getDataInDatabase(Date date, Date date2) {
        Object obj;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getDataInDatabase - startDate=" + date + ", endDate=" + date2);
        List<DailyHeartRateSummary> calculateSummaries = calculateSummaries(this.mHeartRateSummaryDao.getDailyHeartRateSummariesDesc(rk2.b(date, date2) ? date2 : date, date2));
        ArrayList arrayList = new ArrayList();
        Date lastDate = this.mHeartRateSummaryDao.getLastDate();
        if (lastDate == null) {
            lastDate = date;
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(calculateSummaries);
        if (!rk2.b(date, lastDate)) {
            date = lastDate;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "getDataInDatabase - summaries.size=" + calculateSummaries.size() + ", " + "lastDate=" + lastDate + ", startDateToFill=" + date);
        while (rk2.c(date2, date)) {
            Iterator it = arrayList2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (rk2.d(((DailyHeartRateSummary) obj).getDate(), date2)) {
                    break;
                }
            }
            DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) obj;
            if (dailyHeartRateSummary == null) {
                arrayList.add(dummySummary(date2));
            } else {
                arrayList.add(dailyHeartRateSummary);
                arrayList2.remove(dailyHeartRateSummary);
            }
            date2 = rk2.m(date2);
            kd4.a((Object) date2, "DateHelper.getPrevDate(endDateToFill)");
        }
        if (!arrayList.isEmpty()) {
            DailyHeartRateSummary dailyHeartRateSummary2 = (DailyHeartRateSummary) kb4.d(arrayList);
            Boolean s = rk2.s(dailyHeartRateSummary2.getDate());
            kd4.a((Object) s, "DateHelper.isToday(todaySummary.date)");
            if (s.booleanValue()) {
                arrayList.add(0, new DailyHeartRateSummary(dailyHeartRateSummary2));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final fi4 loadData(Date date, Date date2, PagingRequestHelper.b.a aVar) {
        return ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new HeartRateSummaryLocalDataSource$loadData$Anon1(this, date, date2, aVar, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final PagingRequestHelper getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    public boolean isInvalid() {
        this.mFitnessDatabase.getInvalidationTracker().b();
        return super.isInvalid();
    }

    @DexIgnore
    public void loadAfter(od.f<Date> fVar, od.a<Date, DailyHeartRateSummary> aVar) {
        kd4.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        kd4.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Date) fVar.a));
        if (rk2.b((Date) fVar.a, this.mCreatedDate)) {
            Key key2 = fVar.a;
            kd4.a((Object) key2, "params.key");
            Date date = (Date) key2;
            Companion companion = Companion;
            Key key3 = fVar.a;
            kd4.a((Object) key3, "params.key");
            Date calculateNextKey = companion.calculateNextKey((Date) key3, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date l = rk2.d(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : rk2.l(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + l + ", endQueryDate=" + date);
            kd4.a((Object) l, "startQueryDate");
            aVar.a(getDataInDatabase(l, date), calculateNextKey);
            if (rk2.b(this.mStartDate, date)) {
                this.mEndDate = date;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.d(str3, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new Pair(this.mStartDate, this.mEndDate));
                this.mHelper.a(PagingRequestHelper.RequestType.AFTER, (PagingRequestHelper.b) new HeartRateSummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    public void loadBefore(od.f<Date> fVar, od.a<Date, DailyHeartRateSummary> aVar) {
        kd4.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        kd4.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    public void loadInitial(od.e<Date> eVar, od.c<Date, DailyHeartRateSummary> cVar) {
        kd4.b(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        kd4.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date l = rk2.d(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : rk2.l(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + l + ", endQueryDate=" + date);
        kd4.a((Object) l, "startQueryDate");
        cVar.a(getDataInDatabase(l, date), null, this.key.getTime());
        this.mHelper.a(PagingRequestHelper.RequestType.INITIAL, (PagingRequestHelper.b) new HeartRateSummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        kd4.b(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(PagingRequestHelper pagingRequestHelper) {
        kd4.b(pagingRequestHelper, "<set-?>");
        this.mHelper = pagingRequestHelper;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        kd4.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        kd4.b(date, "<set-?>");
        this.mStartDate = date;
    }
}
