package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.ActivitiesRepository", mo27670f = "ActivitiesRepository.kt", mo27671l = {245}, mo27672m = "saveActivitiesToServer")
public final class ActivitiesRepository$saveActivitiesToServer$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public int I$0;
    @DexIgnore
    public int I$1;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ActivitiesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$saveActivitiesToServer$1(com.portfolio.platform.data.source.ActivitiesRepository activitiesRepository, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = activitiesRepository;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.saveActivitiesToServer((java.util.List<com.portfolio.platform.data.model.room.fitness.SampleRaw>) null, (com.portfolio.platform.data.source.ActivitiesRepository.PushPendingActivitiesCallback) null, this);
    }
}
