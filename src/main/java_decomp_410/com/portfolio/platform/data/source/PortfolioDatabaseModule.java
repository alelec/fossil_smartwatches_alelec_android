package com.portfolio.platform.data.source;

import androidx.room.RoomDatabase;
import com.facebook.appevents.UserDataStore;
import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rf;
import com.fossil.blesdk.obfuscated.xk2;
import com.fossil.wearables.fsl.fitness.FitnessProviderFactory;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProviderImp;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.local.AlarmsLocalDataSource;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.local.CategoryDatabase;
import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.alarm.AlarmDao;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import com.portfolio.platform.data.source.local.diana.ComplicationDao;
import com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.DianaPresetDao;
import com.portfolio.platform.data.source.local.diana.WatchAppDao;
import com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao;
import com.portfolio.platform.data.source.local.diana.WatchFaceDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.fitness.ActivitySampleDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.fitness.SampleRawDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDatabase;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule {
    @DexIgnore
    public final synchronized ActivitySampleDao provideActivitySampleDao(FitnessDatabase fitnessDatabase) {
        kd4.b(fitnessDatabase, UserDataStore.DATE_OF_BIRTH);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("provideActivitySampleDao: DBNAME=");
        hg openHelper = fitnessDatabase.getOpenHelper();
        kd4.a((Object) openHelper, "db.openHelper");
        sb.append(openHelper.getDatabaseName());
        local.d("PortfolioDatabaseModule", sb.toString());
        return fitnessDatabase.activitySampleDao();
    }

    @DexIgnore
    public final synchronized ActivitySummaryDao provideActivitySummaryDao(FitnessDatabase fitnessDatabase) {
        kd4.b(fitnessDatabase, UserDataStore.DATE_OF_BIRTH);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("provideActivitySummaryDao: DBNAME=");
        hg openHelper = fitnessDatabase.getOpenHelper();
        kd4.a((Object) openHelper, "db.openHelper");
        sb.append(openHelper.getDatabaseName());
        local.d("PortfolioDatabaseModule", sb.toString());
        return fitnessDatabase.activitySummaryDao();
    }

    @DexIgnore
    public final AlarmsLocalDataSource provideAlarmsLocalDataSource(AlarmDao alarmDao) {
        kd4.b(alarmDao, "alarmDao");
        return new AlarmsLocalDataSource(alarmDao);
    }

    @DexIgnore
    public final AlarmsRemoteDataSource provideAlarmsRemoteDataSource(ApiServiceV2 apiServiceV2) {
        kd4.b(apiServiceV2, "apiService");
        return new AlarmsRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public final CategoryDao provideCategoryDao(CategoryDatabase categoryDatabase) {
        kd4.b(categoryDatabase, UserDataStore.DATE_OF_BIRTH);
        return categoryDatabase.categoryDao();
    }

    @DexIgnore
    public final synchronized CategoryDatabase provideCategoryDatabase(PortfolioApp portfolioApp) {
        CategoryDatabase b;
        kd4.b(portfolioApp, "app");
        RoomDatabase.a<CategoryDatabase> a = rf.a(portfolioApp, CategoryDatabase.class, "category.db");
        a.d();
        b = a.b();
        kd4.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final ComplicationDao provideComplicationDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        kd4.b(dianaCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return dianaCustomizeDatabase.getComplicationDao();
    }

    @DexIgnore
    public final ComplicationLastSettingDao provideComplicationSettingDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        kd4.b(dianaCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return dianaCustomizeDatabase.getComplicationSettingDao();
    }

    @DexIgnore
    public final CustomizeRealDataDao provideCustomizeRealDataDao(CustomizeRealDataDatabase customizeRealDataDatabase) {
        kd4.b(customizeRealDataDatabase, UserDataStore.DATE_OF_BIRTH);
        return customizeRealDataDatabase.realDataDao();
    }

    @DexIgnore
    public final CustomizeRealDataDatabase provideCustomizeRealDataDatabase(PortfolioApp portfolioApp) {
        kd4.b(portfolioApp, "app");
        RoomDatabase.a<CustomizeRealDataDatabase> a = rf.a(portfolioApp, CustomizeRealDataDatabase.class, "customizeRealData.db");
        a.d();
        CustomizeRealDataDatabase b = a.b();
        kd4.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final DNDSettingsDatabase provideDNDSettingsDatabase(PortfolioApp portfolioApp) {
        kd4.b(portfolioApp, "app");
        RoomDatabase.a<DNDSettingsDatabase> a = rf.a(portfolioApp, DNDSettingsDatabase.class, "dndSettings.db");
        a.d();
        DNDSettingsDatabase b = a.b();
        kd4.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final DeviceDao provideDeviceDao(DeviceDatabase deviceDatabase) {
        kd4.b(deviceDatabase, UserDataStore.DATE_OF_BIRTH);
        return deviceDatabase.deviceDao();
    }

    @DexIgnore
    public final DeviceDatabase provideDeviceDatabase(PortfolioApp portfolioApp) {
        kd4.b(portfolioApp, "app");
        RoomDatabase.a<DeviceDatabase> a = rf.a(portfolioApp, DeviceDatabase.class, "devices.db");
        a.a(DeviceDatabase.Companion.getMIGRATION_FROM_1_TO_2());
        a.d();
        DeviceDatabase b = a.b();
        kd4.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final DianaCustomizeDatabase provideDianaCustomizeDatabase(PortfolioApp portfolioApp) {
        kd4.b(portfolioApp, "app");
        RoomDatabase.a<DianaCustomizeDatabase> a = rf.a(portfolioApp, DianaCustomizeDatabase.class, "dianaCustomize.db");
        a.d();
        DianaCustomizeDatabase b = a.b();
        kd4.a((Object) b, "Room\n                .da\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final DianaPresetDao provideDianaPresetDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        kd4.b(dianaCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return dianaCustomizeDatabase.getPresetDao();
    }

    @DexIgnore
    public final synchronized FitnessDataDao provideFitnessDataDao(FitnessDatabase fitnessDatabase) {
        kd4.b(fitnessDatabase, UserDataStore.DATE_OF_BIRTH);
        return fitnessDatabase.getFitnessDataDao();
    }

    @DexIgnore
    public final synchronized FitnessDatabase provideFitnessDatabase(PortfolioApp portfolioApp) {
        String str;
        FitnessDatabase b;
        kd4.b(portfolioApp, "app");
        FLogger.INSTANCE.getLocal().d("PortfolioDatabaseModule", "provideFitnessDatabase");
        MFUser b2 = dn2.p.a().n().b();
        if (b2 != null) {
            str = b2.getUserId();
            if (str != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("PortfolioDatabaseModule", "provideFitnessDatabase userId " + str);
                RoomDatabase.a<FitnessDatabase> a = rf.a(portfolioApp, FitnessDatabase.class, str + "_" + FitnessProviderFactory.DB_NAME);
                a.a(FitnessDatabase.Companion.getMIGRATION_FROM_4_TO_21());
                a.d();
                a.c();
                b = a.b();
                kd4.a((Object) b, "Room.databaseBuilder<Fit\u2026\n                .build()");
            }
        }
        str = "Anonymous";
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("PortfolioDatabaseModule", "provideFitnessDatabase userId " + str);
        RoomDatabase.a<FitnessDatabase> a2 = rf.a(portfolioApp, FitnessDatabase.class, str + "_" + FitnessProviderFactory.DB_NAME);
        a2.a(FitnessDatabase.Companion.getMIGRATION_FROM_4_TO_21());
        a2.d();
        a2.c();
        b = a2.b();
        kd4.a((Object) b, "Room.databaseBuilder<Fit\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final xk2 provideFitnessHelper(en2 en2, ActivitySummaryDao activitySummaryDao) {
        kd4.b(en2, "sharedPreferencesManager");
        kd4.b(activitySummaryDao, "activitySummaryDao");
        return new xk2(en2, activitySummaryDao);
    }

    @DexIgnore
    public final GoalTrackingDao provideGoalTrackingDao(GoalTrackingDatabase goalTrackingDatabase) {
        kd4.b(goalTrackingDatabase, UserDataStore.DATE_OF_BIRTH);
        return goalTrackingDatabase.getGoalTrackingDao();
    }

    @DexIgnore
    public final GoalTrackingDatabase provideGoalTrackingDatabase(PortfolioApp portfolioApp) {
        kd4.b(portfolioApp, "app");
        RoomDatabase.a<GoalTrackingDatabase> a = rf.a(portfolioApp, GoalTrackingDatabase.class, "goalTracking.db");
        a.d();
        GoalTrackingDatabase b = a.b();
        kd4.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final synchronized HeartRateDailySummaryDao provideHeartRateDailySummaryDao(FitnessDatabase fitnessDatabase) {
        kd4.b(fitnessDatabase, UserDataStore.DATE_OF_BIRTH);
        return fitnessDatabase.getHeartRateDailySummaryDao();
    }

    @DexIgnore
    public final synchronized HeartRateSampleDao provideHeartRateDao(FitnessDatabase fitnessDatabase) {
        kd4.b(fitnessDatabase, UserDataStore.DATE_OF_BIRTH);
        return fitnessDatabase.getHeartRateDao();
    }

    @DexIgnore
    public final HybridCustomizeDatabase provideHybridCustomizeDatabase(PortfolioApp portfolioApp) {
        kd4.b(portfolioApp, "app");
        RoomDatabase.a<HybridCustomizeDatabase> a = rf.a(portfolioApp, HybridCustomizeDatabase.class, "hybridCustomize.db");
        a.d();
        HybridCustomizeDatabase b = a.b();
        kd4.a((Object) b, "Room\n                .da\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final InAppNotificationDao provideInAppNotificationDao(InAppNotificationDatabase inAppNotificationDatabase) {
        kd4.b(inAppNotificationDatabase, UserDataStore.DATE_OF_BIRTH);
        return inAppNotificationDatabase.inAppNotificationDao();
    }

    @DexIgnore
    public final InAppNotificationDatabase provideInAppNotificationDatabase(PortfolioApp portfolioApp) {
        kd4.b(portfolioApp, "app");
        RoomDatabase.a<InAppNotificationDatabase> a = rf.a(portfolioApp, InAppNotificationDatabase.class, "inAppNotification.db");
        a.d();
        InAppNotificationDatabase b = a.b();
        kd4.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final MicroAppDao provideMicroAppDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        kd4.b(hybridCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return hybridCustomizeDatabase.microAppDao();
    }

    @DexIgnore
    public final MicroAppLastSettingDao provideMicroAppLastSettingDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        kd4.b(hybridCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return hybridCustomizeDatabase.microAppLastSettingDao();
    }

    @DexIgnore
    public final NotificationSettingsDao provideNotificationSettingsDao(NotificationSettingsDatabase notificationSettingsDatabase) {
        kd4.b(notificationSettingsDatabase, UserDataStore.DATE_OF_BIRTH);
        return notificationSettingsDatabase.getNotificationSettingsDao();
    }

    @DexIgnore
    public final NotificationSettingsDatabase provideNotificationSettingsDatabase(PortfolioApp portfolioApp) {
        kd4.b(portfolioApp, "app");
        RoomDatabase.a<NotificationSettingsDatabase> a = rf.a(portfolioApp, NotificationSettingsDatabase.class, "notificationSettings.db");
        a.d();
        NotificationSettingsDatabase b = a.b();
        kd4.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final HybridPresetDao providePresetDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        kd4.b(hybridCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return hybridCustomizeDatabase.presetDao();
    }

    @DexIgnore
    public final RemindersSettingsDatabase provideRemindersSettingsDatabase(PortfolioApp portfolioApp) {
        kd4.b(portfolioApp, "app");
        RoomDatabase.a<RemindersSettingsDatabase> a = rf.a(portfolioApp, RemindersSettingsDatabase.class, "remindersSettings.db");
        a.d();
        RemindersSettingsDatabase b = a.b();
        kd4.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final synchronized SampleRawDao provideSampleRawDao(FitnessDatabase fitnessDatabase) {
        kd4.b(fitnessDatabase, UserDataStore.DATE_OF_BIRTH);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("provideSampleRawDao: DBNAME=");
        hg openHelper = fitnessDatabase.getOpenHelper();
        kd4.a((Object) openHelper, "db.openHelper");
        sb.append(openHelper.getDatabaseName());
        local.d("PortfolioDatabaseModule", sb.toString());
        return fitnessDatabase.sampleRawDao();
    }

    @DexIgnore
    public final SkuDao provideSkuDao(DeviceDatabase deviceDatabase) {
        kd4.b(deviceDatabase, UserDataStore.DATE_OF_BIRTH);
        return deviceDatabase.skuDao();
    }

    @DexIgnore
    public final synchronized SleepDao provideSleepDao(SleepDatabase sleepDatabase) {
        kd4.b(sleepDatabase, UserDataStore.DATE_OF_BIRTH);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("provideFitnessDao: DBNAME=");
        hg openHelper = sleepDatabase.getOpenHelper();
        kd4.a((Object) openHelper, "db.openHelper");
        sb.append(openHelper.getDatabaseName());
        local.d("PortfolioDatabaseModule", sb.toString());
        return sleepDatabase.sleepDao();
    }

    @DexIgnore
    public final synchronized SleepDatabase provideSleepDatabase(PortfolioApp portfolioApp) {
        String str;
        SleepDatabase b;
        kd4.b(portfolioApp, "app");
        FLogger.INSTANCE.getLocal().d("PortfolioDatabaseModule", "provideSleepDatabase");
        MFUser b2 = dn2.p.a().n().b();
        if (b2 != null) {
            str = b2.getUserId();
            if (str != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("PortfolioDatabaseModule", "provideSleepDatabase: userID " + str);
                RoomDatabase.a<SleepDatabase> a = rf.a(portfolioApp, SleepDatabase.class, str + "_" + MFSleepSessionProviderImp.DB_NAME);
                a.a(SleepDatabase.Companion.getMIGRATION_FROM_3_TO_9());
                a.d();
                b = a.b();
                kd4.a((Object) b, "Room.databaseBuilder<Sle\u2026\n                .build()");
            }
        }
        str = "Anonymous";
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("PortfolioDatabaseModule", "provideSleepDatabase: userID " + str);
        RoomDatabase.a<SleepDatabase> a2 = rf.a(portfolioApp, SleepDatabase.class, str + "_" + MFSleepSessionProviderImp.DB_NAME);
        a2.a(SleepDatabase.Companion.getMIGRATION_FROM_3_TO_9());
        a2.d();
        b = a2.b();
        kd4.a((Object) b, "Room.databaseBuilder<Sle\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final ThirdPartyDatabase provideThirdPartyDatabase(PortfolioApp portfolioApp) {
        kd4.b(portfolioApp, "app");
        RoomDatabase.a<ThirdPartyDatabase> a = rf.a(portfolioApp, ThirdPartyDatabase.class, "thirdParty.db");
        a.d();
        ThirdPartyDatabase b = a.b();
        kd4.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final WatchAppDao provideWatchAppDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        kd4.b(dianaCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return dianaCustomizeDatabase.getWatchAppDao();
    }

    @DexIgnore
    public final WatchAppLastSettingDao provideWatchAppSettingDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        kd4.b(dianaCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return dianaCustomizeDatabase.getWatchAppSettingDao();
    }

    @DexIgnore
    public final synchronized WorkoutDao provideWorkoutDao(FitnessDatabase fitnessDatabase) {
        kd4.b(fitnessDatabase, UserDataStore.DATE_OF_BIRTH);
        return fitnessDatabase.getWorkoutDao();
    }

    @DexIgnore
    public final AlarmDao providesAlarmDao(AlarmDatabase alarmDatabase) {
        kd4.b(alarmDatabase, UserDataStore.DATE_OF_BIRTH);
        return alarmDatabase.alarmDao();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0019, code lost:
        if (r0 != null) goto L_0x001e;
     */
    @DexIgnore
    public final AlarmDatabase providesAlarmDatabase(PortfolioApp portfolioApp) {
        String str;
        kd4.b(portfolioApp, "app");
        MFUser b = dn2.p.a().n().b();
        if (b != null) {
            str = b.getUserId();
        }
        str = "Anonymous";
        RoomDatabase.a<AlarmDatabase> a = rf.a(portfolioApp, AlarmDatabase.class, str + "_" + "alarm.db");
        a.a(AlarmDatabase.Companion.migrating3Or4To5(str, 3), AlarmDatabase.Companion.migrating3Or4To5(str, 4));
        a.d();
        AlarmDatabase b2 = a.b();
        kd4.a((Object) b2, "Room.databaseBuilder(app\u2026\n                .build()");
        return b2;
    }

    @DexIgnore
    public final WatchFaceDao providesWatchFaceDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        kd4.b(dianaCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return dianaCustomizeDatabase.getWatchFaceDao();
    }
}
