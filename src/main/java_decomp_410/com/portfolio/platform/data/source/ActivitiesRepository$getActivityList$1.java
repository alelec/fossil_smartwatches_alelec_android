package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitiesRepository$getActivityList$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ActivitiesRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$1$1")
    /* renamed from: com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$1$1 */
    public static final class C56891 extends com.portfolio.platform.util.NetworkBoundResource<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>, com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.Activity>> {
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.Pair $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $offset;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$1 this$0;

        @DexIgnore
        public C56891(com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$1 activitiesRepository$getActivityList$1, kotlin.jvm.internal.Ref$IntRef ref$IntRef, int i, kotlin.Pair pair) {
            this.this$0 = activitiesRepository$getActivityList$1;
            this.$offset = ref$IntRef;
            this.$limit = i;
            this.$downloadingDate = pair;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
            if (r0 != null) goto L_0x0019;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
            if (r0 != null) goto L_0x0033;
         */
        @DexIgnore
        public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.Activity>>> yb4) {
            java.util.Date date;
            java.util.Date date2;
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
            kotlin.Pair pair = this.$downloadingDate;
            if (pair != null) {
                date = (java.util.Date) pair.getFirst();
            }
            date = this.this$0.$startDate;
            java.lang.String e = com.fossil.blesdk.obfuscated.rk2.m27397e(date);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            kotlin.Pair pair2 = this.$downloadingDate;
            if (pair2 != null) {
                date2 = (java.util.Date) pair2.getSecond();
            }
            date2 = this.this$0.$endDate;
            java.lang.String e2 = com.fossil.blesdk.obfuscated.rk2.m27397e(date2);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiService$p.getActivities(e, e2, this.$offset.element, this.$limit, yb4);
        }

        @DexIgnore
        public androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>> loadFromDb() {
            if (!com.fossil.blesdk.obfuscated.rk2.m27414s(this.this$0.$endDate).booleanValue()) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.ActivitiesRepository.Companion.getTAG$app_fossilRelease();
                local.mo33255d(tAG$app_fossilRelease, "getActivityList loadFromDb isNotToday day = " + this.this$0.$end);
                com.portfolio.platform.data.source.local.fitness.ActivitySampleDao access$getMActivitySampleDao$p = this.this$0.this$0.mActivitySampleDao;
                java.util.Date date = this.this$0.$startDate;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_START_DATE);
                java.util.Date date2 = this.this$0.$endDate;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date2, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_END_DATE);
                return access$getMActivitySampleDao$p.getActivitySamplesLiveData(date, date2);
            }
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ActivitiesRepository.Companion.getTAG$app_fossilRelease(), "getActivityList loadFromDb: isToday");
            com.portfolio.platform.data.source.local.fitness.ActivitySampleDao access$getMActivitySampleDao$p2 = this.this$0.this$0.mActivitySampleDao;
            java.util.Date date3 = this.this$0.$startDate;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date3, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_START_DATE);
            java.util.Date m = com.fossil.blesdk.obfuscated.rk2.m27408m(this.this$0.$endDate);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) m, "DateHelper.getPrevDate(endDate)");
            androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>> b = com.fossil.blesdk.obfuscated.C1935hc.m7844b(access$getMActivitySampleDao$p2.getActivitySamplesLiveData(date3, m), new com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$1$1$loadFromDb$1(this));
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) b, "Transformations.switchMa\u2026                        }");
            return b;
        }

        @DexIgnore
        public void onFetchFailed(java.lang.Throwable th) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ActivitiesRepository.Companion.getTAG$app_fossilRelease(), "getActivityList onFetchFailed");
        }

        @DexIgnore
        public boolean processContinueFetching(com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.Activity> apiResponse) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(apiResponse, "item");
            com.portfolio.platform.data.model.Range range = apiResponse.get_range();
            if (range == null || !range.isHasNext()) {
                return false;
            }
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ActivitiesRepository.Companion.getTAG$app_fossilRelease(), "getActivityList processContinueFetching hasNext");
            this.$offset.element += this.$limit;
            return true;
        }

        @DexIgnore
        public void saveCallResult(com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.Activity> apiResponse) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(apiResponse, "item");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.ActivitiesRepository.Companion.getTAG$app_fossilRelease();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("getActivityList saveCallResult onResponse: response = ");
            sb.append(apiResponse.get_items().size());
            sb.append(" hasNext=");
            com.portfolio.platform.data.model.Range range = apiResponse.get_range();
            sb.append(range != null ? java.lang.Boolean.valueOf(range.isHasNext()) : null);
            local.mo33255d(tAG$app_fossilRelease, sb.toString());
            java.util.ArrayList arrayList = new java.util.ArrayList();
            for (com.portfolio.platform.data.Activity activitySample : apiResponse.get_items()) {
                arrayList.add(activitySample.toActivitySample());
            }
            this.this$0.this$0.mActivitySampleDao.upsertListActivitySample(arrayList);
        }

        @DexIgnore
        public boolean shouldFetch(java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample> list) {
            return this.this$0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public ActivitiesRepository$getActivityList$1(com.portfolio.platform.data.source.ActivitiesRepository activitiesRepository, java.util.Date date, java.util.Date date2, boolean z, java.util.Date date3) {
        this.this$0 = activitiesRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
        this.$end = date3;
    }

    @DexIgnore
    public final androidx.lifecycle.LiveData<com.fossil.blesdk.obfuscated.os3<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>>> apply(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> list) {
        kotlin.jvm.internal.Ref$IntRef ref$IntRef = new kotlin.jvm.internal.Ref$IntRef();
        ref$IntRef.element = 0;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) list, "fitnessDataList");
        java.util.Date date = this.$startDate;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_START_DATE);
        java.util.Date date2 = this.$endDate;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date2, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_END_DATE);
        return new com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$1.C56891(this, ref$IntRef, 300, com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt.calculateRangeDownload(list, date, date2)).asLiveData();
    }
}
