package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$1 */
public final class C5738xbf05d628 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.p006ua.UAActivityTimeSeries $activityTimeSeries;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.dg4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $countSizeOfLists$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $sampleList;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfLists$inlined;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21102p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$1$1")
    /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$1$1 */
    public static final class C57391 implements com.fossil.blesdk.obfuscated.gr3.C4353d {
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.C5738xbf05d628 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$1$1$1")
        /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$1$1$1 */
        public static final class C57401 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f21103p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.data.source.C5738xbf05d628.C57391 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C57401(com.portfolio.platform.data.source.C5738xbf05d628.C57391 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.data.source.C5738xbf05d628.C57391.C57401 r0 = new com.portfolio.platform.data.source.C5738xbf05d628.C57391.C57401(this.this$0, yb4);
                r0.f21103p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.data.source.C5738xbf05d628.C57391.C57401) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                com.fossil.blesdk.obfuscated.cc4.m20546a();
                if (this.label == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    this.this$0.this$0.this$0.getMThirdPartyDatabase().getUASampleDao().deleteListUASample(this.this$0.this$0.$sampleList);
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public C57391(com.portfolio.platform.data.source.C5738xbf05d628 thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$1) {
            this.this$0 = thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$1;
        }

        @DexIgnore
        public void onSuccess() {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "Sending UASample to UnderAmour successfully!");
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.C5738xbf05d628.C57391.C57401(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            com.portfolio.platform.data.source.C5738xbf05d628 thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$1 = this.this$0;
            kotlin.jvm.internal.Ref$IntRef ref$IntRef = thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$1.$countSizeOfLists$inlined;
            ref$IntRef.element++;
            if (ref$IntRef.element >= thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$1.$sizeOfLists$inlined && thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$1.$continuation$inlined.isActive()) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "End saveToUA");
                com.fossil.blesdk.obfuscated.dg4 dg4 = this.this$0.$continuation$inlined;
                kotlin.Result.C7350a aVar = kotlin.Result.Companion;
                dg4.resumeWith(kotlin.Result.m37419constructorimpl((java.lang.Object) null));
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C5738xbf05d628(com.portfolio.platform.data.model.p006ua.UAActivityTimeSeries uAActivityTimeSeries, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4, kotlin.jvm.internal.Ref$IntRef ref$IntRef, int i, com.fossil.blesdk.obfuscated.dg4 dg4, com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository) {
        super(2, yb4);
        this.$activityTimeSeries = uAActivityTimeSeries;
        this.$sampleList = list;
        this.$countSizeOfLists$inlined = ref$IntRef;
        this.$sizeOfLists$inlined = i;
        this.$continuation$inlined = dg4;
        this.this$0 = thirdPartyRepository;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.C5738xbf05d628 thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$1 = new com.portfolio.platform.data.source.C5738xbf05d628(this.$activityTimeSeries, this.$sampleList, yb4, this.$countSizeOfLists$inlined, this.$sizeOfLists$inlined, this.$continuation$inlined, this.this$0);
        thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$1.f21102p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.C5738xbf05d628) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21102p$;
            com.fossil.blesdk.obfuscated.gr3 r = this.this$0.getMPortfolioApp().mo34570r();
            com.portfolio.platform.data.model.p006ua.UAActivityTimeSeries uAActivityTimeSeries = this.$activityTimeSeries;
            com.portfolio.platform.data.source.C5738xbf05d628.C57391 r4 = new com.portfolio.platform.data.source.C5738xbf05d628.C57391(this);
            this.L$0 = zg4;
            this.label = 1;
            if (r.mo27792a(uAActivityTimeSeries, (com.fossil.blesdk.obfuscated.gr3.C4353d) r4, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
