package com.portfolio.platform.data.source.local.diana;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.diana.ComplicationLastSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ComplicationLastSettingDao {
    @DexIgnore
    void cleanUp();

    @DexIgnore
    void deleteComplicationLastSettingByComplicationId(String str);

    @DexIgnore
    List<ComplicationLastSetting> getAllComplicationLastSetting();

    @DexIgnore
    LiveData<List<ComplicationLastSetting>> getAllComplicationLastSettingAsLiveData();

    @DexIgnore
    ComplicationLastSetting getComplicationLastSetting(String str);

    @DexIgnore
    void upsertComplicationLastSetting(ComplicationLastSetting complicationLastSetting);
}
