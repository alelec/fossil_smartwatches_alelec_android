package com.portfolio.platform.data.source;

import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.WatchParam;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface SkuDao {
    @DexIgnore
    void addOrUpdateSkuList(List<SKUModel> list);

    @DexIgnore
    void addOrUpdateWatchParam(WatchParam watchParam);

    @DexIgnore
    void cleanUpSku();

    @DexIgnore
    void cleanUpWatchParam();

    @DexIgnore
    List<SKUModel> getAllSkus();

    @DexIgnore
    SKUModel getSkuByDeviceIdPrefix(String str);

    @DexIgnore
    WatchParam getWatchParamById(String str);
}
