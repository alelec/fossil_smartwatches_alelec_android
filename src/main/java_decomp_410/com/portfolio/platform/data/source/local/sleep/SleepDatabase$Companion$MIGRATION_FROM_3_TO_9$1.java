package com.portfolio.platform.data.source.local.sleep;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepDatabase$Companion$MIGRATION_FROM_3_TO_9$1 extends com.fossil.blesdk.obfuscated.C3361yf {
    @DexIgnore
    public SleepDatabase$Companion$MIGRATION_FROM_3_TO_9$1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    public void migrate(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(ggVar, "database");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.sleep.SleepDatabase.TAG, "Migration 3 to 5 start");
        ggVar.mo11234s();
        try {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.sleep.SleepDatabase.TAG, "Migrate sleep session table");
            ggVar.mo11230b("CREATE TABLE sleep_session_new (date INTEGER NOT NULL, day TEXT NOT NULL, deviceSerialNumber TEXT, syncTime INTEGER, bookmarkTime INTEGER, normalizedSleepQuality REAL NOT NULL DEFAULT 0, source INTEGER NOT NULL DEFAULT 0, realStartTime INTEGER NOT NULL DEFAULT 0, realEndTime INTEGER PRIMARY KEY NOT NULL DEFAULT 0, realSleepMinutes INTEGER NOT NULL DEFAULT 0, realSleepStateDistInMinute TEXT NOT NULL DEFAULT '{\"awake\":0,\"deep\":0,\"light\":0}', editedStartTime INTEGER, editedEndTime INTEGER, editedSleepMinutes INTEGER, editedSleepStateDistInMinute TEXT, sleepStates TEXT NOT NULL DEFAULT '', createdAt INTEGER NOT NULL DEFAULT 0, updatedAt INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 1, timezoneOffset INTEGER NOT NULL DEFAULT 0, heartRate TEXT)");
            ggVar.mo11230b("UPDATE sleep_session SET createdAt = 0 WHERE createdAt IS NULL");
            ggVar.mo11230b("UPDATE sleep_session SET updatedAt = 0 WHERE updatedAt IS NULL");
            ggVar.mo11230b("INSERT INTO sleep_session_new (date, day, deviceSerialNumber, syncTime, bookmarkTime, normalizedSleepQuality, source, realStartTime, realEndTime, realSleepMinutes, realSleepMinutes, realSleepStateDistInMinute, editedStartTime, editedEndTime, editedSleepMinutes, editedSleepStateDistInMinute, sleepStates, createdAt, updatedAt, pinType, timezoneOffset) SELECT date, day, deviceSerialNumber, syncTime, bookmarkTime, normalizedSleepQuality, source, realStartTime, realEndTime, realSleepMinutes, realSleepMinutes, realSleepStateDistInMinute, editedStartTime, editedEndTime, editedSleepMinutes, editedSleepStateDistInMinute, sleepStates, createdAt, updatedAt, pinType, timezoneOffset  FROM sleep_session");
            ggVar.mo11230b("DROP TABLE sleep_session");
            ggVar.mo11230b("ALTER TABLE sleep_session_new RENAME TO sleep_session");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.sleep.SleepDatabase.TAG, "Migrate sleep session table success");
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d(com.portfolio.platform.data.source.local.sleep.SleepDatabase.TAG, "Migrate sleep session table fail " + e);
            ggVar.mo11230b("DROP TABLE IF EXISTS sleep_session_new");
            ggVar.mo11230b("DROP TABLE IF EXISTS sleep_session");
            ggVar.mo11230b("CREATE TABLE IF NOT EXISTS sleep_session (date INTEGER NOT NULL, day TEXT NOT NULL, deviceSerialNumber TEXT, syncTime INTEGER, bookmarkTime INTEGER, normalizedSleepQuality REAL NOT NULL DEFAULT 0, source INTEGER NOT NULL DEFAULT 0, realStartTime INTEGER NOT NULL DEFAULT 0, realEndTime INTEGER PRIMARY KEY NOT NULL DEFAULT 0, realSleepMinutes INTEGER NOT NULL DEFAULT 0, realSleepStateDistInMinute TEXT NOT NULL DEFAULT '{\"awake\":0,\"deep\":0,\"light\":0}', editedStartTime INTEGER, editedEndTime INTEGER, editedSleepMinutes INTEGER, editedSleepStateDistInMinute TEXT, sleepStates TEXT NOT NULL DEFAULT '', createdAt INTEGER NOT NULL DEFAULT 0, updatedAt INTEGER, pinType INTEGER NOT NULL DEFAULT 1, timezoneOffset INTEGER NOT NULL DEFAULT 0, heartRate TEXT)");
        }
        try {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.sleep.SleepDatabase.TAG, "Migrate sleep date table");
            ggVar.mo11230b("CREATE TABLE sleep_date_new (date TEXT PRIMARY KEY NOT NULL, goalMinutes INTEGER NOT NULL DEFAULT 0, sleepMinutes INTEGER NOT NULL DEFAULT 0, sleepStateDistInMinute TEXT, createdAt INTEGER, updatedAt INTEGER, pinType INTEGER NOT NULL DEFAULT 1, timezoneOffset INTEGER NOT NULL DEFAULT 0)");
            ggVar.mo11230b("INSERT INTO sleep_date_new (date, goalMinutes, sleepMinutes, sleepStateDistInMinute, createdAt, updatedAt, pinType, timezoneOffset) SELECT date, goalMinutes, sleepMinutes, sleepStateDistInMinute, createdAt, updatedAt, pinType, timezoneOffset  FROM sleep_date");
            ggVar.mo11230b("DROP TABLE sleep_date");
            ggVar.mo11230b("ALTER TABLE sleep_date_new RENAME TO sleep_date");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.sleep.SleepDatabase.TAG, "Migrate sleep date table success");
        } catch (java.lang.Exception e2) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local2.mo33255d(com.portfolio.platform.data.source.local.sleep.SleepDatabase.TAG, "Migrate sleep date table fail " + e2);
            ggVar.mo11230b("DROP TABLE IF EXISTS sleep_date_new");
            ggVar.mo11230b("DROP TABLE IF EXISTS sleep_date");
            ggVar.mo11230b("CREATE TABLE IF NOT EXISTS sleep_date (date TEXT PRIMARY KEY NOT NULL, goalMinutes INTEGER NOT NULL DEFAULT 0, sleepMinutes INTEGER NOT NULL DEFAULT 0, sleepStateDistInMinute TEXT, createdAt INTEGER, updatedAt INTEGER, pinType INTEGER NOT NULL DEFAULT 1, timezoneOffset INTEGER NOT NULL DEFAULT 0)");
        }
        try {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.sleep.SleepDatabase.TAG, "Migrate sleep setting");
            ggVar.mo11230b("CREATE TABLE sleep_settings (sleepGoal INTEGER NOT NULL DEFAULT 480, id INTEGER PRIMARY KEY ASC NOT NULL)");
            ggVar.mo11230b("INSERT INTO sleep_settings (sleepGoal) SELECT minute FROM sleep_goal ORDER BY date DESC LIMIT 1");
            ggVar.mo11230b("DROP TABLE sleep_goal");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.sleep.SleepDatabase.TAG, "Migrate sleep setting success");
        } catch (java.lang.Exception e3) {
            ggVar.mo11230b("CREATE TABLE IF NOT EXISTS sleep_settings (sleepGoal INTEGER NOT NULL DEFAULT 480, id INTEGER PRIMARY KEY ASC NOT NULL)");
            ggVar.mo11230b("DROP TABLE IF EXISTS sleep_goal");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local3.mo33255d(com.portfolio.platform.data.source.local.sleep.SleepDatabase.TAG, "Migrate sleep setting fail " + e3);
        }
        ggVar.mo11230b("CREATE TABLE sleepRecommendedGoals (recommendedSleepGoal INTEGER NOT NULL, id INTEGER PRIMARY KEY ASC NOT NULL)");
        try {
            ggVar.mo11230b("CREATE TABLE sleep_statistic (id TEXT NOT NULL, uid TEXT NOT NULL, sleepTimeBestDay TEXT, sleepTimeBestStreak TEXT, totalDays INTEGER NOT NULL, totalSleeps INTEGER NOT NULL, totalSleepMinutes INTEGER NOT NULL, totalSleepStateDistInMinute TEXT NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL, PRIMARY KEY(id));");
        } catch (java.lang.Exception e4) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local4.mo33256e(com.portfolio.platform.data.source.local.sleep.SleepDatabase.TAG, "MIGRATION_FROM_3_TO_5 - ActivityStatistic -- e=" + e4);
            e4.printStackTrace();
        }
        ggVar.mo11236u();
        ggVar.mo11237v();
    }
}
