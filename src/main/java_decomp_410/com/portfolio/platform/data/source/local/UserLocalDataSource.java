package com.portfolio.platform.data.source.local;

import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UserLocalDataSource extends UserDataSource {
    @DexIgnore
    public void clearAllUser() {
        dn2.p.a().n().f();
    }

    @DexIgnore
    public Object deleteUser(MFUser mFUser, yb4<? super Integer> yb4) {
        dn2.p.a().n().c(mFUser);
        return dc4.a(200);
    }

    @DexIgnore
    public MFUser getCurrentUser() {
        return dn2.p.a().n().b();
    }

    @DexIgnore
    public void insertUser(MFUser mFUser) {
        kd4.b(mFUser, "user");
        dn2.p.a().n().b(mFUser);
    }

    @DexIgnore
    public Object updateUser(MFUser mFUser, boolean z, yb4<? super qo2<MFUser>> yb4) {
        dn2.p.a().n().a(mFUser);
        return new ro2(getCurrentUser(), false, 2, (fd4) null);
    }
}
