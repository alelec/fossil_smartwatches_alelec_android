package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1", mo27670f = "ThirdPartyRepository.kt", mo27671l = {157, 158, 159, 160, 161, 162, 163}, mo27672m = "invokeSuspend")
public final class ThirdPartyRepository$uploadData$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository.PushPendingThirdPartyDataCallback $pushPendingThirdPartyDataCallback;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public java.lang.Object L$7;
    @DexIgnore
    public java.lang.Object L$8;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21106p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$1", mo27670f = "ThirdPartyRepository.kt", mo27671l = {111}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$1 */
    public static final class C57441 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Object>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.lang.String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $gFitSampleList;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21107p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C57441(com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 thirdPartyRepository$uploadData$1, java.util.List list, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = thirdPartyRepository$uploadData$1;
            this.$gFitSampleList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57441 r0 = new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57441(this.this$0, this.$gFitSampleList, this.$activeDeviceSerial, yb4);
            r0.f21107p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57441) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21107p$;
                com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                java.util.List list = this.$gFitSampleList;
                java.lang.String str = this.$activeDeviceSerial;
                this.L$0 = zg4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitSampleToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$2")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$2", mo27670f = "ThirdPartyRepository.kt", mo27671l = {118}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$2 */
    public static final class C57452 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Object>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.lang.String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $gFitActiveTimeList;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21108p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C57452(com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 thirdPartyRepository$uploadData$1, java.util.List list, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = thirdPartyRepository$uploadData$1;
            this.$gFitActiveTimeList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57452 r0 = new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57452(this.this$0, this.$gFitActiveTimeList, this.$activeDeviceSerial, yb4);
            r0.f21108p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57452) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21108p$;
                com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                java.util.List list = this.$gFitActiveTimeList;
                java.lang.String str = this.$activeDeviceSerial;
                this.L$0 = zg4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitActiveTimeToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$3")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$3", mo27670f = "ThirdPartyRepository.kt", mo27671l = {125}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$3 */
    public static final class C57463 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Object>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.lang.String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $gFitHeartRateList;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21109p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C57463(com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 thirdPartyRepository$uploadData$1, java.util.List list, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = thirdPartyRepository$uploadData$1;
            this.$gFitHeartRateList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57463 r0 = new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57463(this.this$0, this.$gFitHeartRateList, this.$activeDeviceSerial, yb4);
            r0.f21109p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57463) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21109p$;
                com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                java.util.List list = this.$gFitHeartRateList;
                java.lang.String str = this.$activeDeviceSerial;
                this.L$0 = zg4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitHeartRateToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$4")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$4", mo27670f = "ThirdPartyRepository.kt", mo27671l = {132}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$4 */
    public static final class C57474 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Object>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.lang.String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $gFitSleepList;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21110p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C57474(com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 thirdPartyRepository$uploadData$1, java.util.List list, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = thirdPartyRepository$uploadData$1;
            this.$gFitSleepList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57474 r0 = new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57474(this.this$0, this.$gFitSleepList, this.$activeDeviceSerial, yb4);
            r0.f21110p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57474) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21110p$;
                com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                java.util.List list = this.$gFitSleepList;
                java.lang.String str = this.$activeDeviceSerial;
                this.L$0 = zg4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitSleepDataToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$5")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$5", mo27670f = "ThirdPartyRepository.kt", mo27671l = {141}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$5 */
    public static final class C57485 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Object>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.lang.String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $gFitWorkoutSessionList;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21111p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C57485(com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 thirdPartyRepository$uploadData$1, java.util.List list, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = thirdPartyRepository$uploadData$1;
            this.$gFitWorkoutSessionList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57485 r0 = new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57485(this.this$0, this.$gFitWorkoutSessionList, this.$activeDeviceSerial, yb4);
            r0.f21111p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57485) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21111p$;
                com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                java.util.List list = this.$gFitWorkoutSessionList;
                java.lang.String str = this.$activeDeviceSerial;
                this.L$0 = zg4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitWorkoutSessionToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$6")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$6", mo27670f = "ThirdPartyRepository.kt", mo27671l = {150}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$6 */
    public static final class C57496 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Object>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21112p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C57496(com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 thirdPartyRepository$uploadData$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = thirdPartyRepository$uploadData$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57496 r0 = new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57496(this.this$0, yb4);
            r0.f21112p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57496) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21112p$;
                com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                this.L$0 = zg4;
                this.label = 1;
                obj = thirdPartyRepository.saveToUA(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$7")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$7", mo27670f = "ThirdPartyRepository.kt", mo27671l = {151}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$7 */
    public static final class C57507 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Object>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21113p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C57507(com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 thirdPartyRepository$uploadData$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = thirdPartyRepository$uploadData$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57507 r0 = new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57507(this.this$0, yb4);
            r0.f21113p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57507) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21113p$;
                com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                this.L$0 = zg4;
                this.label = 1;
                obj = thirdPartyRepository.saveToUAOldFlow(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThirdPartyRepository$uploadData$1(com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository, com.portfolio.platform.data.source.ThirdPartyRepository.PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = thirdPartyRepository;
        this.$pushPendingThirdPartyDataCallback = pushPendingThirdPartyDataCallback;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1 thirdPartyRepository$uploadData$1 = new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1(this.this$0, this.$pushPendingThirdPartyDataCallback, yb4);
        thirdPartyRepository$uploadData$1.f21106p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return thirdPartyRepository$uploadData$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x02d9, code lost:
        r11 = r2;
        r2 = r12;
        r19 = r6;
        r6 = r4;
        r4 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x02e3, code lost:
        if (r8 == null) goto L_0x0301;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x02e5, code lost:
        r0.L$0 = r11;
        r0.L$1 = r10;
        r0.L$2 = r9;
        r0.L$3 = r8;
        r0.L$4 = r7;
        r0.L$5 = r6;
        r0.L$6 = r5;
        r0.L$7 = r4;
        r0.L$8 = r2;
        r0.label = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x02fe, code lost:
        if (r8.mo27693a(r0) != r1) goto L_0x0301;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0300, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0301, code lost:
        if (r7 == null) goto L_0x031f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0303, code lost:
        r0.L$0 = r11;
        r0.L$1 = r10;
        r0.L$2 = r9;
        r0.L$3 = r8;
        r0.L$4 = r7;
        r0.L$5 = r6;
        r0.L$6 = r5;
        r0.L$7 = r4;
        r0.L$8 = r2;
        r0.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x031c, code lost:
        if (r7.mo27693a(r0) != r1) goto L_0x031f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x031e, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x031f, code lost:
        if (r6 == null) goto L_0x033d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0321, code lost:
        r0.L$0 = r11;
        r0.L$1 = r10;
        r0.L$2 = r9;
        r0.L$3 = r8;
        r0.L$4 = r7;
        r0.L$5 = r6;
        r0.L$6 = r5;
        r0.L$7 = r4;
        r0.L$8 = r2;
        r0.label = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x033a, code lost:
        if (r6.mo27693a(r0) != r1) goto L_0x033d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x033c, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x033d, code lost:
        if (r5 == null) goto L_0x035b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x033f, code lost:
        r0.L$0 = r11;
        r0.L$1 = r10;
        r0.L$2 = r9;
        r0.L$3 = r8;
        r0.L$4 = r7;
        r0.L$5 = r6;
        r0.L$6 = r5;
        r0.L$7 = r4;
        r0.L$8 = r2;
        r0.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0358, code lost:
        if (r5.mo27693a(r0) != r1) goto L_0x035b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x035a, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x035b, code lost:
        if (r4 == null) goto L_0x0379;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x035d, code lost:
        r0.L$0 = r11;
        r0.L$1 = r10;
        r0.L$2 = r9;
        r0.L$3 = r8;
        r0.L$4 = r7;
        r0.L$5 = r6;
        r0.L$6 = r5;
        r0.L$7 = r4;
        r0.L$8 = r2;
        r0.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0376, code lost:
        if (r4.mo27693a(r0) != r1) goto L_0x0379;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0378, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0379, code lost:
        if (r2 == null) goto L_0x0397;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x037b, code lost:
        r0.L$0 = r11;
        r0.L$1 = r10;
        r0.L$2 = r9;
        r0.L$3 = r8;
        r0.L$4 = r7;
        r0.L$5 = r6;
        r0.L$6 = r5;
        r0.L$7 = r4;
        r0.L$8 = r2;
        r0.label = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0394, code lost:
        if (r2.mo27693a(r0) != r1) goto L_0x0397;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0396, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0397, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "End uploadData");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x03a2, code lost:
        r1 = r0.$pushPendingThirdPartyDataCallback;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x03a4, code lost:
        if (r1 == null) goto L_0x03a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x03a6, code lost:
        r1.onComplete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x03ab, code lost:
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.String str;
        com.fossil.blesdk.obfuscated.gh4 gh4;
        com.fossil.blesdk.obfuscated.gh4 gh42;
        com.fossil.blesdk.obfuscated.gh4 gh43;
        com.fossil.blesdk.obfuscated.gh4 gh44;
        com.fossil.blesdk.obfuscated.gh4 gh45;
        com.fossil.blesdk.obfuscated.gh4 gh46;
        com.fossil.blesdk.obfuscated.gh4 gh47;
        com.fossil.blesdk.obfuscated.gh4 gh48;
        com.fossil.blesdk.obfuscated.gh4 gh49;
        com.fossil.blesdk.obfuscated.gh4 gh410;
        com.fossil.blesdk.obfuscated.zg4 zg42;
        com.fossil.blesdk.obfuscated.gh4 gh411;
        com.fossil.blesdk.obfuscated.gh4 gh412;
        com.fossil.blesdk.obfuscated.gh4 gh413;
        com.fossil.blesdk.obfuscated.gh4 gh414;
        com.fossil.blesdk.obfuscated.gh4 gh415;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        switch (this.label) {
            case 0:
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                zg42 = this.f21106p$;
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "Start uploadData");
                str = this.this$0.getMPortfolioApp().mo34532e();
                if (str.length() > 0) {
                    gh49 = null;
                    if (this.this$0.mGoogleFitHelper.mo33047e()) {
                        if (!this.this$0.getMThirdPartyDatabase().getGFitSampleDao().getAllGFitSample().isEmpty()) {
                            gh412 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57441(this, this.this$0.getMThirdPartyDatabase().getGFitSampleDao().getAllGFitSample(), str, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                        } else {
                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "GFitSample is empty");
                            gh412 = null;
                        }
                        if (!this.this$0.getMThirdPartyDatabase().getGFitActiveTimeDao().getAllGFitActiveTime().isEmpty()) {
                            gh413 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57452(this, this.this$0.getMThirdPartyDatabase().getGFitActiveTimeDao().getAllGFitActiveTime(), str, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                        } else {
                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "GFitActiveTime is empty");
                            gh413 = null;
                        }
                        if (!this.this$0.getMThirdPartyDatabase().getGFitHeartRateDao().getAllGFitHeartRate().isEmpty()) {
                            gh414 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57463(this, this.this$0.getMThirdPartyDatabase().getGFitHeartRateDao().getAllGFitHeartRate(), str, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                        } else {
                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "GFitHeartRate is empty");
                            gh414 = null;
                        }
                        if (!this.this$0.getMThirdPartyDatabase().getGFitSleepDao().getAllGFitSleep().isEmpty()) {
                            gh415 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57474(this, this.this$0.getMThirdPartyDatabase().getGFitSleepDao().getAllGFitSleep(), str, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                        } else {
                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "GFitSleep is empty");
                            gh415 = null;
                        }
                        if (!this.this$0.getMThirdPartyDatabase().getGFitWorkoutSessionDao().getAllGFitWorkoutSession().isEmpty()) {
                            gh411 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57485(this, this.this$0.getMThirdPartyDatabase().getGFitWorkoutSessionDao().getAllGFitWorkoutSession(), str, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                        } else {
                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "GFitWorkoutSession is empty");
                            gh411 = null;
                        }
                        com.fossil.blesdk.obfuscated.gh4 gh416 = gh415;
                        gh4 = gh412;
                        gh410 = gh416;
                        com.fossil.blesdk.obfuscated.gh4 gh417 = gh414;
                        gh42 = gh413;
                        gh43 = gh417;
                    } else {
                        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "Google Fit is not connected");
                        gh410 = null;
                        gh43 = null;
                        gh42 = null;
                        gh4 = null;
                        gh411 = null;
                    }
                    if (this.this$0.getMPortfolioApp().mo34570r().mo27797b()) {
                        com.fossil.blesdk.obfuscated.zg4 zg43 = zg42;
                        com.fossil.blesdk.obfuscated.gh4 a2 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg43, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57496(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                        gh48 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg43, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1.C57507(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                        gh49 = a2;
                    } else {
                        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "Not logged in to UA");
                        gh48 = null;
                    }
                    if (gh4 != null) {
                        this.L$0 = zg42;
                        this.L$1 = str;
                        this.L$2 = gh4;
                        this.L$3 = gh42;
                        this.L$4 = gh43;
                        this.L$5 = gh410;
                        this.L$6 = gh411;
                        this.L$7 = gh49;
                        this.L$8 = gh48;
                        this.label = 1;
                        if (gh4.mo27693a(this) == a) {
                            return a;
                        }
                    }
                    gh45 = gh411;
                    break;
                }
                break;
            case 1:
                gh45 = (com.fossil.blesdk.obfuscated.gh4) this.L$6;
                gh43 = (com.fossil.blesdk.obfuscated.gh4) this.L$4;
                gh42 = (com.fossil.blesdk.obfuscated.gh4) this.L$3;
                gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
                str = (java.lang.String) this.L$1;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                gh48 = (com.fossil.blesdk.obfuscated.gh4) this.L$8;
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.gh4 gh418 = (com.fossil.blesdk.obfuscated.gh4) this.L$5;
                gh49 = (com.fossil.blesdk.obfuscated.gh4) this.L$7;
                gh410 = gh418;
                break;
            case 2:
                gh47 = (com.fossil.blesdk.obfuscated.gh4) this.L$8;
                gh46 = (com.fossil.blesdk.obfuscated.gh4) this.L$7;
                gh45 = (com.fossil.blesdk.obfuscated.gh4) this.L$6;
                gh44 = (com.fossil.blesdk.obfuscated.gh4) this.L$5;
                gh43 = (com.fossil.blesdk.obfuscated.gh4) this.L$4;
                gh42 = (com.fossil.blesdk.obfuscated.gh4) this.L$3;
                gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
                str = (java.lang.String) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 3:
                gh47 = (com.fossil.blesdk.obfuscated.gh4) this.L$8;
                gh46 = (com.fossil.blesdk.obfuscated.gh4) this.L$7;
                gh45 = (com.fossil.blesdk.obfuscated.gh4) this.L$6;
                gh44 = (com.fossil.blesdk.obfuscated.gh4) this.L$5;
                gh43 = (com.fossil.blesdk.obfuscated.gh4) this.L$4;
                gh42 = (com.fossil.blesdk.obfuscated.gh4) this.L$3;
                gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
                str = (java.lang.String) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 4:
                gh47 = (com.fossil.blesdk.obfuscated.gh4) this.L$8;
                gh46 = (com.fossil.blesdk.obfuscated.gh4) this.L$7;
                gh45 = (com.fossil.blesdk.obfuscated.gh4) this.L$6;
                gh44 = (com.fossil.blesdk.obfuscated.gh4) this.L$5;
                gh43 = (com.fossil.blesdk.obfuscated.gh4) this.L$4;
                gh42 = (com.fossil.blesdk.obfuscated.gh4) this.L$3;
                gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
                str = (java.lang.String) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 5:
                gh47 = (com.fossil.blesdk.obfuscated.gh4) this.L$8;
                gh46 = (com.fossil.blesdk.obfuscated.gh4) this.L$7;
                gh45 = (com.fossil.blesdk.obfuscated.gh4) this.L$6;
                gh44 = (com.fossil.blesdk.obfuscated.gh4) this.L$5;
                gh43 = (com.fossil.blesdk.obfuscated.gh4) this.L$4;
                gh42 = (com.fossil.blesdk.obfuscated.gh4) this.L$3;
                gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
                str = (java.lang.String) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 6:
                gh47 = (com.fossil.blesdk.obfuscated.gh4) this.L$8;
                gh46 = (com.fossil.blesdk.obfuscated.gh4) this.L$7;
                gh45 = (com.fossil.blesdk.obfuscated.gh4) this.L$6;
                gh44 = (com.fossil.blesdk.obfuscated.gh4) this.L$5;
                gh43 = (com.fossil.blesdk.obfuscated.gh4) this.L$4;
                gh42 = (com.fossil.blesdk.obfuscated.gh4) this.L$3;
                gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
                str = (java.lang.String) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 7:
                com.fossil.blesdk.obfuscated.gh4 gh419 = (com.fossil.blesdk.obfuscated.gh4) this.L$8;
                com.fossil.blesdk.obfuscated.gh4 gh420 = (com.fossil.blesdk.obfuscated.gh4) this.L$7;
                com.fossil.blesdk.obfuscated.gh4 gh421 = (com.fossil.blesdk.obfuscated.gh4) this.L$6;
                com.fossil.blesdk.obfuscated.gh4 gh422 = (com.fossil.blesdk.obfuscated.gh4) this.L$5;
                com.fossil.blesdk.obfuscated.gh4 gh423 = (com.fossil.blesdk.obfuscated.gh4) this.L$4;
                com.fossil.blesdk.obfuscated.gh4 gh424 = (com.fossil.blesdk.obfuscated.gh4) this.L$3;
                com.fossil.blesdk.obfuscated.gh4 gh425 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
                java.lang.String str2 = (java.lang.String) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg44 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            default:
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
