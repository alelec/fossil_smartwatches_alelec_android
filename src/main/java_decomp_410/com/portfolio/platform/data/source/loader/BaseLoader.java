package com.portfolio.platform.data.source.loader;

import android.content.Context;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.oc;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class BaseLoader<T> extends oc<T> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return BaseLoader.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = BaseLoader.class.getSimpleName();
        kd4.a((Object) simpleName, "BaseLoader::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseLoader(Context context) {
        super(context);
        if (context != null) {
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void deliverResult(T t) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside .deliverResult data=" + t + ", isReset=" + isReset() + ". isStarted=" + isStarted());
        if (!isReset() && isStarted()) {
            super.deliverResult(t);
        }
    }

    @DexIgnore
    public void onReset() {
        FLogger.INSTANCE.getLocal().d(TAG, "Inside .onReset");
        onStopLoading();
        super.onReset();
    }

    @DexIgnore
    public void onStopLoading() {
        FLogger.INSTANCE.getLocal().d(TAG, "Inside .onStopLoading");
        cancelLoad();
        super.onStopLoading();
    }
}
