package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideGoalTrackingDatabaseFactory implements Factory<GoalTrackingDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideGoalTrackingDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideGoalTrackingDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideGoalTrackingDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static GoalTrackingDatabase provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return proxyProvideGoalTrackingDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static GoalTrackingDatabase proxyProvideGoalTrackingDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        GoalTrackingDatabase provideGoalTrackingDatabase = portfolioDatabaseModule.provideGoalTrackingDatabase(portfolioApp);
        n44.a(provideGoalTrackingDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideGoalTrackingDatabase;
    }

    @DexIgnore
    public GoalTrackingDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
