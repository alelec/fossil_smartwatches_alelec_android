package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitySampleDao$getActivitySamplesLiveData$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySampleDao this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1<I, O> implements m3<X, Y> {
        @DexIgnore
        public /* final */ /* synthetic */ List $activitySamples;

        @DexIgnore
        public Anon1(List list) {
            this.$activitySamples = list;
        }

        @DexIgnore
        public final List<ActivitySample> apply(List<SampleRaw> list) {
            List list2 = this.$activitySamples;
            kd4.a((Object) list2, "activitySamples");
            if ((!list2.isEmpty()) || list.isEmpty()) {
                return this.$activitySamples;
            }
            this.$activitySamples.clear();
            List list3 = this.$activitySamples;
            kd4.a((Object) list, "samplesRaw");
            ArrayList arrayList = new ArrayList(db4.a(list, 10));
            for (SampleRaw activitySample : list) {
                arrayList.add(activitySample.toActivitySample());
            }
            list3.addAll(arrayList);
            return this.$activitySamples;
        }
    }

    @DexIgnore
    public ActivitySampleDao$getActivitySamplesLiveData$Anon1(ActivitySampleDao activitySampleDao, Date date, Date date2) {
        this.this$Anon0 = activitySampleDao;
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    public final LiveData<List<ActivitySample>> apply(List<ActivitySample> list) {
        return hc.a(this.this$Anon0.getActivitySamplesLiveDataV1(this.$startDate, this.$endDate), new Anon1(list));
    }
}
