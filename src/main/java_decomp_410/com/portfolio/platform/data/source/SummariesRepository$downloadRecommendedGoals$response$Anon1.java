package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.SummariesRepository$downloadRecommendedGoals$response$Anon1", f = "SummariesRepository.kt", l = {416}, m = "invokeSuspend")
public final class SummariesRepository$downloadRecommendedGoals$response$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<ActivityRecommendedGoals>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $age;
    @DexIgnore
    public /* final */ /* synthetic */ String $gender;
    @DexIgnore
    public /* final */ /* synthetic */ int $heightInCentimeters;
    @DexIgnore
    public /* final */ /* synthetic */ int $weightInGrams;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$downloadRecommendedGoals$response$Anon1(SummariesRepository summariesRepository, int i, int i2, int i3, String str, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = summariesRepository;
        this.$age = i;
        this.$weightInGrams = i2;
        this.$heightInCentimeters = i3;
        this.$gender = str;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new SummariesRepository$downloadRecommendedGoals$response$Anon1(this.this$Anon0, this.$age, this.$weightInGrams, this.$heightInCentimeters, this.$gender, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((SummariesRepository$downloadRecommendedGoals$response$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            ApiServiceV2 access$getMApiServiceV2$p = this.this$Anon0.mApiServiceV2;
            int i2 = this.$age;
            int i3 = this.$weightInGrams;
            int i4 = this.$heightInCentimeters;
            String str = this.$gender;
            this.label = 1;
            obj = access$getMApiServiceV2$p.getRecommendedGoalsRaw(i2, i3, i4, str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
