package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getSummaries$1$1$loadFromDb$2<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, Y> {
    @DexIgnore
    public /* final */ /* synthetic */ androidx.lifecycle.LiveData $resultList;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SummariesRepository$getSummaries$1.C57171 this$0;

    @DexIgnore
    public SummariesRepository$getSummaries$1$1$loadFromDb$2(com.portfolio.platform.data.source.SummariesRepository$getSummaries$1.C57171 r1, androidx.lifecycle.LiveData liveData) {
        this.this$0 = r1;
        this.$resultList = liveData;
    }

    @DexIgnore
    public final java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary> apply(java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary> list) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "getSummaries - loadFromDb -- isToday - resultList=" + ((java.util.List) this.$resultList.mo2275a()));
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) list, "activitySummaries");
        if (!list.isEmpty()) {
            ((com.portfolio.platform.data.model.room.fitness.ActivitySummary) com.fossil.blesdk.obfuscated.kb4.m24385f(list)).setSteps(java.lang.Math.max((double) this.this$0.this$0.this$0.mFitnessHelper.mo32683a(new java.util.Date()), ((com.portfolio.platform.data.model.room.fitness.ActivitySummary) com.fossil.blesdk.obfuscated.kb4.m24385f(list)).getSteps()));
        }
        return list;
    }
}
