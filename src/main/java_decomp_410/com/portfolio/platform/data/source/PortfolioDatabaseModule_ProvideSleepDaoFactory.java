package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideSleepDaoFactory implements Factory<SleepDao> {
    @DexIgnore
    public /* final */ Provider<SleepDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideSleepDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<SleepDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideSleepDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<SleepDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideSleepDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static SleepDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<SleepDatabase> provider) {
        return proxyProvideSleepDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static SleepDao proxyProvideSleepDao(PortfolioDatabaseModule portfolioDatabaseModule, SleepDatabase sleepDatabase) {
        SleepDao provideSleepDao = portfolioDatabaseModule.provideSleepDao(sleepDatabase);
        n44.a(provideSleepDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideSleepDao;
    }

    @DexIgnore
    public SleepDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
