package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.wz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.response.ResponseKt;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = DeviceRemoteDataSource.class.getSimpleName();
        kd4.a((Object) simpleName, "DeviceRemoteDataSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public DeviceRemoteDataSource(ApiServiceV2 apiServiceV2) {
        kd4.b(apiServiceV2, "mApiService");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public final Object forceLinkDevice(Device device, yb4<? super qo2<Void>> yb4) {
        return ResponseKt.a(new DeviceRemoteDataSource$forceLinkDevice$Anon2(this, device, (yb4) null), yb4);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object generatePairingKey(String str, yb4<? super qo2<String>> yb4) {
        DeviceRemoteDataSource$generatePairingKey$Anon1 deviceRemoteDataSource$generatePairingKey$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof DeviceRemoteDataSource$generatePairingKey$Anon1) {
            deviceRemoteDataSource$generatePairingKey$Anon1 = (DeviceRemoteDataSource$generatePairingKey$Anon1) yb4;
            int i2 = deviceRemoteDataSource$generatePairingKey$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$generatePairingKey$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$generatePairingKey$Anon1.result;
                Object a = cc4.a();
                i = deviceRemoteDataSource$generatePairingKey$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    xz1 xz1 = new xz1();
                    xz1.a("serialNumber", str);
                    DeviceRemoteDataSource$generatePairingKey$response$Anon1 deviceRemoteDataSource$generatePairingKey$response$Anon1 = new DeviceRemoteDataSource$generatePairingKey$response$Anon1(this, xz1, (yb4) null);
                    deviceRemoteDataSource$generatePairingKey$Anon1.L$Anon0 = this;
                    deviceRemoteDataSource$generatePairingKey$Anon1.L$Anon1 = str;
                    deviceRemoteDataSource$generatePairingKey$Anon1.L$Anon2 = xz1;
                    deviceRemoteDataSource$generatePairingKey$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$generatePairingKey$response$Anon1, deviceRemoteDataSource$generatePairingKey$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    xz1 xz12 = (xz1) deviceRemoteDataSource$generatePairingKey$Anon1.L$Anon2;
                    str = (String) deviceRemoteDataSource$generatePairingKey$Anon1.L$Anon1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$generatePairingKey$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local.d(str2, "generatePairingKey of " + str + " response " + qo2);
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    Object a2 = ro2.a();
                    if (a2 == null) {
                        kd4.a();
                        throw null;
                    } else if (!((xz1) a2).d("randomKey")) {
                        return new po2(600, (ServerError) null, (Throwable) null, (String) null);
                    } else {
                        JsonElement a3 = ((xz1) ro2.a()).a("randomKey");
                        kd4.a((Object) a3, "response.response.get(Co\u2026ants.JSON_KEY_RANDOM_KEY)");
                        return new ro2(a3.f(), false, 2, (fd4) null);
                    }
                } else if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), po2.d(), po2.b());
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        deviceRemoteDataSource$generatePairingKey$Anon1 = new DeviceRemoteDataSource$generatePairingKey$Anon1(this, yb4);
        Object obj2 = deviceRemoteDataSource$generatePairingKey$Anon1.result;
        Object a4 = cc4.a();
        i = deviceRemoteDataSource$generatePairingKey$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str22 = TAG;
        local2.d(str22, "generatePairingKey of " + str + " response " + qo2);
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    public final Object getAllDevice(yb4<? super qo2<ApiResponse<Device>>> yb4) {
        FLogger.INSTANCE.getLocal().d(TAG, "getAllDevice");
        return ResponseKt.a(new DeviceRemoteDataSource$getAllDevice$Anon2(this, (yb4) null), yb4);
    }

    @DexIgnore
    public final Object getDeviceBySerial(String str, yb4<? super qo2<Device>> yb4) {
        FLogger.INSTANCE.getLocal().d(TAG, "getDeviceBySerial");
        return ResponseKt.a(new DeviceRemoteDataSource$getDeviceBySerial$Anon2(this, str, (yb4) null), yb4);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getDeviceSecretKey(String str, yb4<? super qo2<String>> yb4) {
        DeviceRemoteDataSource$getDeviceSecretKey$Anon1 deviceRemoteDataSource$getDeviceSecretKey$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof DeviceRemoteDataSource$getDeviceSecretKey$Anon1) {
            deviceRemoteDataSource$getDeviceSecretKey$Anon1 = (DeviceRemoteDataSource$getDeviceSecretKey$Anon1) yb4;
            int i2 = deviceRemoteDataSource$getDeviceSecretKey$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$getDeviceSecretKey$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$getDeviceSecretKey$Anon1.result;
                Object a = cc4.a();
                i = deviceRemoteDataSource$getDeviceSecretKey$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    DeviceRemoteDataSource$getDeviceSecretKey$response$Anon1 deviceRemoteDataSource$getDeviceSecretKey$response$Anon1 = new DeviceRemoteDataSource$getDeviceSecretKey$response$Anon1(this, str, (yb4) null);
                    deviceRemoteDataSource$getDeviceSecretKey$Anon1.L$Anon0 = this;
                    deviceRemoteDataSource$getDeviceSecretKey$Anon1.L$Anon1 = str;
                    deviceRemoteDataSource$getDeviceSecretKey$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$getDeviceSecretKey$response$Anon1, deviceRemoteDataSource$getDeviceSecretKey$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) deviceRemoteDataSource$getDeviceSecretKey$Anon1.L$Anon1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$getDeviceSecretKey$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local.d(str2, "getDeviceSecretKey of " + str + " response " + qo2);
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    Object a2 = ro2.a();
                    if (a2 == null) {
                        kd4.a();
                        throw null;
                    } else if (!((xz1) a2).d("secretKey")) {
                        return new po2(600, (ServerError) null, (Throwable) null, (String) null);
                    } else {
                        JsonElement a3 = ((xz1) ro2.a()).a("secretKey");
                        if (a3 instanceof wz1) {
                            return new ro2("", false, 2, (fd4) null);
                        }
                        kd4.a((Object) a3, "secretKey");
                        return new ro2(a3.f(), false, 2, (fd4) null);
                    }
                } else if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), po2.d(), po2.b());
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        deviceRemoteDataSource$getDeviceSecretKey$Anon1 = new DeviceRemoteDataSource$getDeviceSecretKey$Anon1(this, yb4);
        Object obj2 = deviceRemoteDataSource$getDeviceSecretKey$Anon1.result;
        Object a4 = cc4.a();
        i = deviceRemoteDataSource$getDeviceSecretKey$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str22 = TAG;
        local2.d(str22, "getDeviceSecretKey of " + str + " response " + qo2);
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getLatestWatchParamFromServer(String str, int i, yb4<? super WatchParameterResponse> yb4) {
        DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1 deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1;
        int i2;
        qo2 qo2;
        if (yb4 instanceof DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1) {
            deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1 = (DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1) yb4;
            int i3 = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.result;
                Object a = cc4.a();
                i2 = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label;
                if (i2 != 0) {
                    na4.a(obj);
                    DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1 deviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1 = new DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1(this, str, i, (yb4) null);
                    deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.L$Anon0 = this;
                    deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.L$Anon1 = str;
                    deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.I$Anon0 = i;
                    deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1, deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i2 == 1) {
                    i = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.I$Anon0;
                    str = (String) deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.L$Anon1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local.d(str2, "getLatestWatchParamFromServer, serial =" + str + ", majorVersion= " + i);
                if (!(qo2 instanceof ro2)) {
                    ApiResponse apiResponse = (ApiResponse) ((ro2) qo2).a();
                    List list = apiResponse != null ? apiResponse.get_items() : null;
                    if (list == null || !(!list.isEmpty())) {
                        return null;
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local2.d(str3, "getLatestWatchParamFromServer success, response = " + ((WatchParameterResponse) list.get(0)));
                    return (WatchParameterResponse) list.get(0);
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local3.d(str4, "getLatestWatchParamFromServer failed, errorCode = " + ((po2) qo2).a());
                    return null;
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1 = new DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1(this, yb4);
        Object obj2 = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.result;
        Object a2 = cc4.a();
        i2 = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label;
        if (i2 != 0) {
        }
        qo2 = (qo2) obj2;
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str22 = TAG;
        local4.d(str22, "getLatestWatchParamFromServer, serial =" + str + ", majorVersion= " + i);
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getSupportedSku(int i, yb4<? super qo2<ApiResponse<SKUModel>>> yb4) {
        DeviceRemoteDataSource$getSupportedSku$Anon1 deviceRemoteDataSource$getSupportedSku$Anon1;
        int i2;
        qo2 qo2;
        if (yb4 instanceof DeviceRemoteDataSource$getSupportedSku$Anon1) {
            deviceRemoteDataSource$getSupportedSku$Anon1 = (DeviceRemoteDataSource$getSupportedSku$Anon1) yb4;
            int i3 = deviceRemoteDataSource$getSupportedSku$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$getSupportedSku$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$getSupportedSku$Anon1.result;
                Object a = cc4.a();
                i2 = deviceRemoteDataSource$getSupportedSku$Anon1.label;
                if (i2 != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "fetchSupportedSkus");
                    DeviceRemoteDataSource$getSupportedSku$response$Anon1 deviceRemoteDataSource$getSupportedSku$response$Anon1 = new DeviceRemoteDataSource$getSupportedSku$response$Anon1(this, i, (yb4) null);
                    deviceRemoteDataSource$getSupportedSku$Anon1.L$Anon0 = this;
                    deviceRemoteDataSource$getSupportedSku$Anon1.I$Anon0 = i;
                    deviceRemoteDataSource$getSupportedSku$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$getSupportedSku$response$Anon1, deviceRemoteDataSource$getSupportedSku$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i2 == 1) {
                    int i4 = deviceRemoteDataSource$getSupportedSku$Anon1.I$Anon0;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$getSupportedSku$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    Object a2 = ((ro2) qo2).a();
                    if (a2 != null) {
                        return new ro2(a2, false, 2, (fd4) null);
                    }
                    kd4.a();
                    throw null;
                } else if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), (Throwable) null, (String) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        deviceRemoteDataSource$getSupportedSku$Anon1 = new DeviceRemoteDataSource$getSupportedSku$Anon1(this, yb4);
        Object obj2 = deviceRemoteDataSource$getSupportedSku$Anon1.result;
        Object a3 = cc4.a();
        i2 = deviceRemoteDataSource$getSupportedSku$Anon1.label;
        if (i2 != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    public final Object removeDevice(Device device, yb4<? super qo2<Void>> yb4) {
        FLogger.INSTANCE.getLocal().d(TAG, "removeDevice");
        return ResponseKt.a(new DeviceRemoteDataSource$removeDevice$Anon2(this, device, (yb4) null), yb4);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v22, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v6, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object swapPairingKey(String str, String str2, yb4<? super qo2<String>> yb4) {
        DeviceRemoteDataSource$swapPairingKey$Anon1 deviceRemoteDataSource$swapPairingKey$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof DeviceRemoteDataSource$swapPairingKey$Anon1) {
            deviceRemoteDataSource$swapPairingKey$Anon1 = (DeviceRemoteDataSource$swapPairingKey$Anon1) yb4;
            int i2 = deviceRemoteDataSource$swapPairingKey$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$swapPairingKey$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$swapPairingKey$Anon1.result;
                Object a = cc4.a();
                i = deviceRemoteDataSource$swapPairingKey$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    xz1 xz1 = new xz1();
                    xz1.a("serialNumber", str2);
                    xz1.a("encryptedData", str);
                    DeviceRemoteDataSource$swapPairingKey$response$Anon1 deviceRemoteDataSource$swapPairingKey$response$Anon1 = new DeviceRemoteDataSource$swapPairingKey$response$Anon1(this, xz1, (yb4) null);
                    deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon0 = this;
                    deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon1 = str;
                    deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon2 = str2;
                    deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon3 = xz1;
                    deviceRemoteDataSource$swapPairingKey$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$swapPairingKey$response$Anon1, deviceRemoteDataSource$swapPairingKey$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    xz1 xz12 = (xz1) deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon3;
                    str2 = deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon2;
                    str = (String) deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local.d(str3, "swapPairingKey " + str + " of " + str2 + " response " + qo2);
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    Object a2 = ro2.a();
                    if (a2 == null) {
                        kd4.a();
                        throw null;
                    } else if (!((xz1) a2).d("encryptedData")) {
                        return new po2(600, (ServerError) null, (Throwable) null, (String) null);
                    } else {
                        JsonElement a3 = ((xz1) ro2.a()).a("encryptedData");
                        if (a3 instanceof wz1) {
                            return new po2(600, (ServerError) null, (Throwable) null, (String) null);
                        }
                        kd4.a((Object) a3, "encryptedData");
                        return new ro2(a3.f(), false, 2, (fd4) null);
                    }
                } else if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), po2.d(), po2.b());
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        deviceRemoteDataSource$swapPairingKey$Anon1 = new DeviceRemoteDataSource$swapPairingKey$Anon1(this, yb4);
        Object obj2 = deviceRemoteDataSource$swapPairingKey$Anon1.result;
        Object a4 = cc4.a();
        i = deviceRemoteDataSource$swapPairingKey$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str32 = TAG;
        local2.d(str32, "swapPairingKey " + str + " of " + str2 + " response " + qo2);
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    public final Object updateDevice(Device device, yb4<? super qo2<Void>> yb4) {
        FLogger.INSTANCE.getLocal().d(TAG, "updateDevice");
        return ResponseKt.a(new DeviceRemoteDataSource$updateDevice$Anon2(this, device, (yb4) null), yb4);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v8, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v4, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object updateDeviceSecretKey(String str, String str2, yb4<? super qa4> yb4) {
        DeviceRemoteDataSource$updateDeviceSecretKey$Anon1 deviceRemoteDataSource$updateDeviceSecretKey$Anon1;
        int i;
        if (yb4 instanceof DeviceRemoteDataSource$updateDeviceSecretKey$Anon1) {
            deviceRemoteDataSource$updateDeviceSecretKey$Anon1 = (DeviceRemoteDataSource$updateDeviceSecretKey$Anon1) yb4;
            int i2 = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.result;
                Object a = cc4.a();
                i = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    xz1 xz1 = new xz1();
                    xz1.a("secretKey", str2);
                    DeviceRemoteDataSource$updateDeviceSecretKey$response$Anon1 deviceRemoteDataSource$updateDeviceSecretKey$response$Anon1 = new DeviceRemoteDataSource$updateDeviceSecretKey$response$Anon1(this, str, xz1, (yb4) null);
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon0 = this;
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon1 = str;
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon2 = str2;
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon3 = xz1;
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$updateDeviceSecretKey$response$Anon1, deviceRemoteDataSource$updateDeviceSecretKey$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    xz1 xz12 = (xz1) deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon3;
                    str2 = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon2;
                    str = (String) deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local.d(str3, "updateDeviceSecretKey " + str2 + " of " + str + " response " + ((qo2) obj));
                return qa4.a;
            }
        }
        deviceRemoteDataSource$updateDeviceSecretKey$Anon1 = new DeviceRemoteDataSource$updateDeviceSecretKey$Anon1(this, yb4);
        Object obj2 = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.result;
        Object a2 = cc4.a();
        i = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label;
        if (i != 0) {
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str32 = TAG;
        local2.d(str32, "updateDeviceSecretKey " + str2 + " of " + str + " response " + ((qo2) obj2));
        return qa4.a;
    }
}
