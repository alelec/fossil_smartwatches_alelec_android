package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroApp$response$Anon1", f = "MicroAppRemoteDataSource.kt", l = {18}, m = "invokeSuspend")
public final class MicroAppRemoteDataSource$getAllMicroApp$response$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<ApiResponse<MicroApp>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppRemoteDataSource$getAllMicroApp$response$Anon1(MicroAppRemoteDataSource microAppRemoteDataSource, String str, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = microAppRemoteDataSource;
        this.$serial = str;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new MicroAppRemoteDataSource$getAllMicroApp$response$Anon1(this.this$Anon0, this.$serial, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((MicroAppRemoteDataSource$getAllMicroApp$response$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            ApiServiceV2 access$getMApiServiceV2$p = this.this$Anon0.mApiServiceV2;
            String str = this.$serial;
            this.label = 1;
            obj = access$getMApiServiceV2$p.getAllMicroApp(str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
