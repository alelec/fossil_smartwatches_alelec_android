package com.portfolio.platform.data.source.local.hybrid.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDataLocalDataSource$loadInitial$1 implements com.portfolio.platform.helper.PagingRequestHelper.C5952b {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource this$0;

    @DexIgnore
    public GoalTrackingDataLocalDataSource$loadInitial$1(com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
        this.this$0 = goalTrackingDataLocalDataSource;
    }

    @DexIgnore
    public final void run(com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a aVar) {
        com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = this.this$0;
        com.portfolio.platform.helper.PagingRequestHelper.RequestType requestType = com.portfolio.platform.helper.PagingRequestHelper.RequestType.INITIAL;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) aVar, "helperCallback");
        com.fossil.blesdk.obfuscated.fi4 unused = goalTrackingDataLocalDataSource.loadData(requestType, aVar, this.this$0.mOffset);
    }
}
