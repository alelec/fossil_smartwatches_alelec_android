package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.WatchLocalizationRepository$processDownloadAndStore$1", mo27670f = "WatchLocalizationRepository.kt", mo27671l = {66}, mo27672m = "invokeSuspend")
public final class WatchLocalizationRepository$processDownloadAndStore$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.String>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $path;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $url;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21115p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.WatchLocalizationRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchLocalizationRepository$processDownloadAndStore$1(com.portfolio.platform.data.source.WatchLocalizationRepository watchLocalizationRepository, java.lang.String str, java.lang.String str2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = watchLocalizationRepository;
        this.$url = str;
        this.$path = str2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.WatchLocalizationRepository$processDownloadAndStore$1 watchLocalizationRepository$processDownloadAndStore$1 = new com.portfolio.platform.data.source.WatchLocalizationRepository$processDownloadAndStore$1(this.this$0, this.$url, this.$path, yb4);
        watchLocalizationRepository$processDownloadAndStore$1.f21115p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return watchLocalizationRepository$processDownloadAndStore$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.WatchLocalizationRepository$processDownloadAndStore$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21115p$;
            com.portfolio.platform.data.source.WatchLocalizationRepository$processDownloadAndStore$1$repo$1 watchLocalizationRepository$processDownloadAndStore$1$repo$1 = new com.portfolio.platform.data.source.WatchLocalizationRepository$processDownloadAndStore$1$repo$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.portfolio.platform.response.ResponseKt.m32232a(watchLocalizationRepository$processDownloadAndStore$1$repo$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
        if (qo2 instanceof com.fossil.blesdk.obfuscated.ro2) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String access$getTAG$p = this.this$0.TAG;
            local.mo33255d(access$getTAG$p, "start storing for url: " + this.$url + " to path: " + this.$path);
            com.fossil.blesdk.obfuscated.vk2 vk2 = com.fossil.blesdk.obfuscated.vk2.f19449a;
            java.lang.Object a2 = ((com.fossil.blesdk.obfuscated.ro2) qo2).mo30673a();
            if (a2 == null) {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            } else if (vk2.mo31820a((com.fossil.blesdk.obfuscated.em4) a2, this.$path)) {
                return this.$path;
            } else {
                return null;
            }
        } else {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String access$getTAG$p2 = this.this$0.TAG;
            local2.mo33255d(access$getTAG$p2, "download: " + this.$url + " | FAILED");
            return null;
        }
    }
}
