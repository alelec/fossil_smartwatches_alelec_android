package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.bt4;
import com.fossil.blesdk.obfuscated.ps4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface AuthApiUserService {
    @DexIgnore
    @bt4("rpc/auth/password/change")
    Call<xz1> changePassword(@ps4 xz1 xz1);

    @DexIgnore
    @bt4("rpc/auth/logout")
    Object logout(@ps4 xz1 xz1, yb4<? super qr4<Void>> yb4);
}
