package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$1$saveCallResult$1", mo27670f = "SleepSummariesRepository.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class SleepSummariesRepository$getSleepStatistic$1$saveCallResult$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.SleepStatistic $item;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21091p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepStatistic$1$saveCallResult$1(com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$1 sleepSummariesRepository$getSleepStatistic$1, com.portfolio.platform.data.SleepStatistic sleepStatistic, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = sleepSummariesRepository$getSleepStatistic$1;
        this.$item = sleepStatistic;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$1$saveCallResult$1 sleepSummariesRepository$getSleepStatistic$1$saveCallResult$1 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$1$saveCallResult$1(this.this$0, this.$item, yb4);
        sleepSummariesRepository$getSleepStatistic$1$saveCallResult$1.f21091p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return sleepSummariesRepository$getSleepStatistic$1$saveCallResult$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$1$saveCallResult$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            this.this$0.this$0.mSleepDao.upsertSleepStatistic(this.$item);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
