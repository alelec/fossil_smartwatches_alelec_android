package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideFitnessDatabaseFactory implements Factory<FitnessDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideFitnessDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideFitnessDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideFitnessDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static FitnessDatabase provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return proxyProvideFitnessDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static FitnessDatabase proxyProvideFitnessDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        FitnessDatabase provideFitnessDatabase = portfolioDatabaseModule.provideFitnessDatabase(portfolioApp);
        n44.a(provideFitnessDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideFitnessDatabase;
    }

    @DexIgnore
    public FitnessDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
