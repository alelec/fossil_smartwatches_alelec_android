package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.a72;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.c72;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.w62;
import com.fossil.blesdk.obfuscated.wf;
import com.fossil.blesdk.obfuscated.z62;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitySampleDao_Impl extends ActivitySampleDao {
    @DexIgnore
    public /* final */ w62 __activityIntensitiesConverter; // = new w62();
    @DexIgnore
    public /* final */ z62 __dateLongStringConverter; // = new z62();
    @DexIgnore
    public /* final */ a72 __dateShortStringConverter; // = new a72();
    @DexIgnore
    public /* final */ c72 __dateTimeISOStringConverter; // = new c72();
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfActivitySample;
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeleteAllActivitySamples;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<ActivitySample> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activity_sample`(`id`,`uid`,`date`,`startTime`,`endTime`,`steps`,`calories`,`distance`,`activeTime`,`intensityDistInSteps`,`timeZoneOffsetInSecond`,`sourceId`,`syncTime`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, ActivitySample activitySample) {
            if (activitySample.getId() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, activitySample.getId());
            }
            if (activitySample.getUid() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, activitySample.getUid());
            }
            String a = ActivitySampleDao_Impl.this.__dateShortStringConverter.a(activitySample.getDate());
            if (a == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, a);
            }
            String a2 = ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.a(activitySample.getStartTime());
            if (a2 == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, a2);
            }
            String a3 = ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.a(activitySample.getEndTime());
            if (a3 == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, a3);
            }
            kgVar.a(6, activitySample.getSteps());
            kgVar.a(7, activitySample.getCalories());
            kgVar.a(8, activitySample.getDistance());
            kgVar.b(9, (long) activitySample.getActiveTime());
            String a4 = ActivitySampleDao_Impl.this.__activityIntensitiesConverter.a(activitySample.getIntensityDistInSteps());
            if (a4 == null) {
                kgVar.a(10);
            } else {
                kgVar.a(10, a4);
            }
            kgVar.b(11, (long) activitySample.getTimeZoneOffsetInSecond());
            if (activitySample.getSourceId() == null) {
                kgVar.a(12);
            } else {
                kgVar.a(12, activitySample.getSourceId());
            }
            kgVar.b(13, activitySample.getSyncTime());
            kgVar.b(14, activitySample.getCreatedAt());
            kgVar.b(15, activitySample.getUpdatedAt());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends wf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM activity_sample";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<ActivitySample>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon3(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<ActivitySample> call() throws Exception {
            Anon3 anon3 = this;
            Cursor a = bg.a(ActivitySampleDao_Impl.this.__db, anon3.val$_statement, false);
            try {
                int b = ag.b(a, "id");
                int b2 = ag.b(a, "uid");
                int b3 = ag.b(a, "date");
                int b4 = ag.b(a, SampleRaw.COLUMN_START_TIME);
                int b5 = ag.b(a, SampleRaw.COLUMN_END_TIME);
                int b6 = ag.b(a, "steps");
                int b7 = ag.b(a, "calories");
                int b8 = ag.b(a, "distance");
                int b9 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME);
                int b10 = ag.b(a, "intensityDistInSteps");
                int b11 = ag.b(a, "timeZoneOffsetInSecond");
                int b12 = ag.b(a, SampleRaw.COLUMN_SOURCE_ID);
                int b13 = ag.b(a, "syncTime");
                int b14 = ag.b(a, "createdAt");
                int i = b;
                int b15 = ag.b(a, "updatedAt");
                int i2 = b14;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i3 = b2;
                    int i4 = i2;
                    int i5 = b15;
                    ActivitySample activitySample = new ActivitySample(a.getString(b2), ActivitySampleDao_Impl.this.__dateShortStringConverter.a(a.getString(b3)), ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b4)), ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b5)), a.getDouble(b6), a.getDouble(b7), a.getDouble(b8), a.getInt(b9), ActivitySampleDao_Impl.this.__activityIntensitiesConverter.a(a.getString(b10)), a.getInt(b11), a.getString(b12), a.getLong(b13), a.getLong(i4), a.getLong(i5));
                    i2 = i4;
                    int i6 = i;
                    int i7 = b3;
                    activitySample.setId(a.getString(i6));
                    arrayList.add(activitySample);
                    anon3 = this;
                    b15 = i5;
                    b3 = i7;
                    i = i6;
                    b2 = i3;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<com.portfolio.platform.data.model.room.fitness.SampleRaw>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon4(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<com.portfolio.platform.data.model.room.fitness.SampleRaw> call() throws Exception {
            Anon4 anon4 = this;
            Cursor a = bg.a(ActivitySampleDao_Impl.this.__db, anon4.val$_statement, false);
            try {
                int b = ag.b(a, "id");
                int b2 = ag.b(a, "pinType");
                int b3 = ag.b(a, SampleRaw.COLUMN_UA_PIN_TYPE);
                int b4 = ag.b(a, SampleRaw.COLUMN_START_TIME);
                int b5 = ag.b(a, SampleRaw.COLUMN_END_TIME);
                int b6 = ag.b(a, SampleRaw.COLUMN_SOURCE_ID);
                int b7 = ag.b(a, SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
                int b8 = ag.b(a, SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
                int b9 = ag.b(a, "steps");
                int b10 = ag.b(a, "calories");
                int b11 = ag.b(a, "distance");
                int b12 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME);
                int b13 = ag.b(a, "intensityDistInSteps");
                int b14 = ag.b(a, SampleRaw.COLUMN_TIMEZONE_ID);
                int i = b3;
                int i2 = b2;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i3 = b4;
                    com.portfolio.platform.data.model.room.fitness.SampleRaw sampleRaw = new com.portfolio.platform.data.model.room.fitness.SampleRaw(ActivitySampleDao_Impl.this.__dateLongStringConverter.a(a.getString(b4)), ActivitySampleDao_Impl.this.__dateLongStringConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getString(b8), a.getDouble(b9), a.getDouble(b10), a.getDouble(b11), a.getInt(b12), ActivitySampleDao_Impl.this.__activityIntensitiesConverter.a(a.getString(b13)), a.getString(b14));
                    sampleRaw.setId(a.getString(b));
                    int i4 = i2;
                    int i5 = b;
                    sampleRaw.setPinType(a.getInt(i4));
                    int i6 = i;
                    sampleRaw.setUaPinType(a.getInt(i6));
                    arrayList.add(sampleRaw);
                    anon4 = this;
                    i = i6;
                    b = i5;
                    i2 = i4;
                    b4 = i3;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public ActivitySampleDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfActivitySample = new Anon1(roomDatabase);
        this.__preparedStmtOfDeleteAllActivitySamples = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void deleteAllActivitySamples() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeleteAllActivitySamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySamples.release(acquire);
        }
    }

    @DexIgnore
    public ActivitySample getActivitySample(String str) {
        uf ufVar;
        ActivitySample activitySample;
        String str2 = str;
        uf b = uf.b("SELECT * FROM activity_sample WHERE id = ? LIMIT 1", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "uid");
            int b4 = ag.b(a, "date");
            int b5 = ag.b(a, SampleRaw.COLUMN_START_TIME);
            int b6 = ag.b(a, SampleRaw.COLUMN_END_TIME);
            int b7 = ag.b(a, "steps");
            int b8 = ag.b(a, "calories");
            int b9 = ag.b(a, "distance");
            int b10 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            int b11 = ag.b(a, "intensityDistInSteps");
            int b12 = ag.b(a, "timeZoneOffsetInSecond");
            int b13 = ag.b(a, SampleRaw.COLUMN_SOURCE_ID);
            int b14 = ag.b(a, "syncTime");
            ufVar = b;
            try {
                int b15 = ag.b(a, "createdAt");
                int i = b2;
                int b16 = ag.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    activitySample = new ActivitySample(a.getString(b3), this.__dateShortStringConverter.a(a.getString(b4)), this.__dateTimeISOStringConverter.a(a.getString(b5)), this.__dateTimeISOStringConverter.a(a.getString(b6)), a.getDouble(b7), a.getDouble(b8), a.getDouble(b9), a.getInt(b10), this.__activityIntensitiesConverter.a(a.getString(b11)), a.getInt(b12), a.getString(b13), a.getLong(b14), a.getLong(b15), a.getLong(b16));
                    activitySample.setId(a.getString(i));
                } else {
                    activitySample = null;
                }
                a.close();
                ufVar.c();
                return activitySample;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public LiveData<List<com.portfolio.platform.data.model.room.fitness.SampleRaw>> getActivitySamplesLiveDataV1(Date date, Date date2) {
        uf b = uf.b("SELECT * FROM sampleraw WHERE startTime >= ? AND startTime < ? ORDER BY startTime ASC", 2);
        String a = this.__dateLongStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateLongStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{SampleRaw.TABLE_NAME}, false, new Anon4(b));
    }

    @DexIgnore
    public LiveData<List<ActivitySample>> getActivitySamplesLiveDataV2(Date date, Date date2) {
        uf b = uf.b("SELECT * FROM activity_sample WHERE date >= ? AND date <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{ActivitySample.TABLE_NAME}, false, new Anon3(b));
    }

    @DexIgnore
    public void upsertListActivitySample(List<ActivitySample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivitySample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
