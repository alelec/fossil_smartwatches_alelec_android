package com.portfolio.platform.data;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Activity extends ServerError {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "Activity";
    @DexIgnore
    @f02("activeTime")
    public /* final */ int activeTime;
    @DexIgnore
    @f02("calories")
    public /* final */ double calories;
    @DexIgnore
    @f02("createdAt")
    public /* final */ DateTime createdAt;
    @DexIgnore
    @f02("date")
    public /* final */ Date date;
    @DexIgnore
    @f02("distance")
    public /* final */ double distance;
    @DexIgnore
    @f02("endTime")
    public /* final */ DateTime endTime;
    @DexIgnore
    @f02("id")
    public /* final */ String id;
    @DexIgnore
    @f02("intensityDistInSteps")
    public /* final */ ActivityIntensities intensityDistInSteps;
    @DexIgnore
    @f02("sourceId")
    public /* final */ String sourceId;
    @DexIgnore
    @f02("startTime")
    public /* final */ DateTime startTime;
    @DexIgnore
    @f02("steps")
    public /* final */ int steps;
    @DexIgnore
    @f02("syncTime")
    public /* final */ DateTime syncTime;
    @DexIgnore
    @f02("timezoneOffset")
    public /* final */ int timezoneOffset;
    @DexIgnore
    @f02("uid")
    public /* final */ String uid;
    @DexIgnore
    @f02("updatedAt")
    public /* final */ DateTime updatedAt;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Gson gsonConverter() {
            rz1 rz1 = new rz1();
            rz1.a(Date.class, new GsonConverterShortDate());
            rz1.a(DateTime.class, new GsonConvertDateTime());
            Gson a = rz1.a();
            if (a != null) {
                return a;
            }
            kd4.a();
            throw null;
        }

        @DexIgnore
        public final Activity toActivity(String str, ActivitySample activitySample) {
            String str2 = str;
            ActivitySample activitySample2 = activitySample;
            kd4.b(str2, "uid");
            kd4.b(activitySample2, "sample");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(Activity.TAG, "toActivity - sample=" + activitySample2);
            DateTime dateTime = r0;
            DateTime dateTime2 = new DateTime(activitySample.getSyncTime(), DateTimeZone.UTC);
            DateTime dateTime3 = r0;
            DateTime dateTime4 = new DateTime(activitySample.getCreatedAt(), DateTimeZone.UTC);
            DateTime dateTime5 = r0;
            DateTime dateTime6 = new DateTime(activitySample.getUpdatedAt(), DateTimeZone.UTC);
            return new Activity(str2 + activitySample.getId(), str, activitySample.getDate(), activitySample.getStartTime(), activitySample.getEndTime(), (int) activitySample.getSteps(), activitySample.getCalories(), activitySample.getDistance(), activitySample.getActiveTime(), activitySample.getIntensityDistInSteps(), activitySample.getTimeZoneOffsetInSecond(), activitySample.getSourceId(), dateTime, dateTime3, dateTime5);
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public Activity(String str, String str2, Date date2, DateTime dateTime, DateTime dateTime2, int i, double d, double d2, int i2, ActivityIntensities activityIntensities, int i3, String str3, DateTime dateTime3, DateTime dateTime4, DateTime dateTime5) {
        DateTime dateTime6 = dateTime2;
        ActivityIntensities activityIntensities2 = activityIntensities;
        String str4 = str3;
        DateTime dateTime7 = dateTime4;
        DateTime dateTime8 = dateTime5;
        kd4.b(str, "id");
        kd4.b(str2, "uid");
        kd4.b(date2, "date");
        kd4.b(dateTime, SampleRaw.COLUMN_START_TIME);
        kd4.b(dateTime6, SampleRaw.COLUMN_END_TIME);
        kd4.b(activityIntensities2, "intensityDistInSteps");
        kd4.b(str4, SampleRaw.COLUMN_SOURCE_ID);
        kd4.b(dateTime7, "createdAt");
        kd4.b(dateTime8, "updatedAt");
        this.id = str;
        this.uid = str2;
        this.date = date2;
        this.startTime = dateTime;
        this.endTime = dateTime6;
        this.steps = i;
        this.calories = d;
        this.distance = d2;
        this.activeTime = i2;
        this.intensityDistInSteps = activityIntensities2;
        this.timezoneOffset = i3;
        this.sourceId = str4;
        this.syncTime = dateTime3;
        this.createdAt = dateTime7;
        this.updatedAt = dateTime8;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final ActivitySample toActivitySample() {
        long j;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "toActivitySample - id=" + this.id + ", date=" + this.date + ", startTime=" + this.startTime + ", syncTime=" + this.syncTime);
        DateTimeZone forOffsetMillis = DateTimeZone.forOffsetMillis(this.timezoneOffset * 1000);
        String str = this.uid;
        Date date2 = this.date;
        DateTime withZone = this.startTime.withZone(forOffsetMillis);
        kd4.a((Object) withZone, "startTime.withZone(timeZone)");
        DateTime withZone2 = this.endTime.withZone(forOffsetMillis);
        kd4.a((Object) withZone2, "endTime.withZone(timeZone)");
        double d = (double) this.steps;
        double d2 = this.calories;
        double d3 = this.distance;
        int i = this.activeTime;
        ActivityIntensities activityIntensities = this.intensityDistInSteps;
        int i2 = this.timezoneOffset;
        String str2 = this.sourceId;
        DateTime dateTime = this.syncTime;
        if (dateTime != null) {
            j = dateTime.getMillis();
        } else {
            j = this.createdAt.getMillis();
        }
        ActivitySample activitySample = r2;
        ActivitySample activitySample2 = new ActivitySample(str, date2, withZone, withZone2, d, d2, d3, i, activityIntensities, i2, str2, j, this.createdAt.getMillis(), this.updatedAt.getMillis());
        ActivitySample activitySample3 = activitySample;
        activitySample3.setId(this.id);
        return activitySample3;
    }

    @DexIgnore
    public final String toJsonString() {
        FLogger.INSTANCE.getLocal().d(TAG, "toJsonString");
        return toJsonString(Companion.gsonConverter());
    }

    @DexIgnore
    public String toString() {
        return "[Activity: id='" + this.id + "', uid='" + this.uid + "', date=" + this.date + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", " + "steps=" + this.steps + ", calories=" + this.calories + ", distance=" + this.distance + ", activeTime=" + this.activeTime + ", " + "intensityDistInSteps=" + this.intensityDistInSteps + ", timezoneOffset=" + this.timezoneOffset + ", " + "sourceId='" + this.sourceId + "', syncTime=" + this.syncTime + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")]";
    }

    @DexIgnore
    public final synchronized String toJsonString(Gson gson) {
        String str;
        kd4.b(gson, "gson");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "toJsonString - gson=" + gson);
        try {
            str = gson.a((Object) this);
            kd4.a((Object) str, "gson.toJson(this)");
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.e(TAG, "toJsonString - e=" + e);
            str = "";
        }
        return str;
    }
}
