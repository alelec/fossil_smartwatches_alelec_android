package com.portfolio.platform.data;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WechatAccessToken {
    @DexIgnore
    @f02("access_token")
    public /* final */ String mAccessToken;
    @DexIgnore
    @f02("expires_in")
    public /* final */ int mExpiresIn;
    @DexIgnore
    @f02("openid")
    public /* final */ String mOpenId;
    @DexIgnore
    @f02("refresh_token")
    public /* final */ String mRefreshToken;
    @DexIgnore
    @f02("scope")
    public /* final */ String mScope;

    @DexIgnore
    public WechatAccessToken(String str, int i, String str2, String str3, String str4) {
        kd4.b(str, "mAccessToken");
        kd4.b(str2, "mRefreshToken");
        kd4.b(str3, "mOpenId");
        kd4.b(str4, "mScope");
        this.mAccessToken = str;
        this.mExpiresIn = i;
        this.mRefreshToken = str2;
        this.mOpenId = str3;
        this.mScope = str4;
    }

    @DexIgnore
    public final String getAccessToken() {
        return this.mAccessToken;
    }

    @DexIgnore
    public final int getExpiresIn() {
        return this.mExpiresIn;
    }

    @DexIgnore
    public final String getOpenId() {
        return this.mOpenId;
    }

    @DexIgnore
    public final String getRefreshToken() {
        return this.mRefreshToken;
    }

    @DexIgnore
    public final String getScope() {
        return this.mScope;
    }
}
