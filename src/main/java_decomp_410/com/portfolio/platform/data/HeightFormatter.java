package com.portfolio.platform.data;

import android.content.Context;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.NumberPickerLarge;
import java.io.Serializable;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class HeightFormatter implements NumberPickerLarge.Formatter, Serializable {
    @DexIgnore
    public static /* final */ int UNIT_FEET; // = 0;
    @DexIgnore
    public static /* final */ int UNIT_INCHES; // = 1;
    @DexIgnore
    public /* final */ int unit;

    @DexIgnore
    public HeightFormatter(int i) {
        this.unit = i;
    }

    @DexIgnore
    public String format(int i) {
        int i2 = this.unit;
        if (i2 == 0) {
            return String.format(Locale.US, sm2.a((Context) PortfolioApp.R, (int) R.string.feet_format), new Object[]{Integer.valueOf(i)});
        } else if (i2 == 1) {
            return String.format(Locale.US, sm2.a((Context) PortfolioApp.R, (int) R.string.inches_format), new Object[]{Integer.valueOf(i)});
        } else {
            return String.format(Locale.US, sm2.a((Context) PortfolioApp.R, (int) R.string.normal_format), new Object[]{Integer.valueOf(i)});
        }
    }
}
