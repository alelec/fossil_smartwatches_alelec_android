package com.portfolio.platform.data.model.p006ua;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.model.ua.UADataSource */
public final class UADataSource {
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("active")
    public java.lang.Boolean active;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("advertised_name")
    public java.lang.String advertisedName;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("bluetooth_device_address")
    public java.lang.String bluetoothDeviceAddress;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("_embedded")
    public com.portfolio.platform.data.model.p006ua.UAEmbedded embedded;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("firmware_version")
    public java.lang.String firmwareVersion;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("hardware_version")
    public java.lang.String hardwareVersion;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("_links")
    public com.portfolio.platform.data.model.p006ua.UALinks link;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("name")
    public java.lang.String name;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("serial_number")
    public java.lang.String serialNumber;

    @DexIgnore
    public final java.lang.Boolean getActive() {
        return this.active;
    }

    @DexIgnore
    public final java.lang.String getAdvertisedName() {
        return this.advertisedName;
    }

    @DexIgnore
    public final java.lang.String getBluetoothDeviceAddress() {
        return this.bluetoothDeviceAddress;
    }

    @DexIgnore
    public final com.portfolio.platform.data.model.p006ua.UAEmbedded getEmbedded() {
        return this.embedded;
    }

    @DexIgnore
    public final java.lang.String getFirmwareVersion() {
        return this.firmwareVersion;
    }

    @DexIgnore
    public final java.lang.String getHardwareVersion() {
        return this.hardwareVersion;
    }

    @DexIgnore
    public final com.portfolio.platform.data.model.p006ua.UALinks getLink() {
        return this.link;
    }

    @DexIgnore
    public final java.lang.String getName() {
        return this.name;
    }

    @DexIgnore
    public final java.lang.String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final void setActive(java.lang.Boolean bool) {
        this.active = bool;
    }

    @DexIgnore
    public final void setAdvertisedName(java.lang.String str) {
        this.advertisedName = str;
    }

    @DexIgnore
    public final void setBluetoothDeviceAddress(java.lang.String str) {
        this.bluetoothDeviceAddress = str;
    }

    @DexIgnore
    public final void setEmbedded(com.portfolio.platform.data.model.p006ua.UAEmbedded uAEmbedded) {
        this.embedded = uAEmbedded;
    }

    @DexIgnore
    public final void setFirmwareVersion(java.lang.String str) {
        this.firmwareVersion = str;
    }

    @DexIgnore
    public final void setHardwareVersion(java.lang.String str) {
        this.hardwareVersion = str;
    }

    @DexIgnore
    public final void setLink(com.portfolio.platform.data.model.p006ua.UALinks uALinks) {
        this.link = uALinks;
    }

    @DexIgnore
    public final void setName(java.lang.String str) {
        this.name = str;
    }

    @DexIgnore
    public final void setSerialNumber(java.lang.String str) {
        this.serialNumber = str;
    }
}
