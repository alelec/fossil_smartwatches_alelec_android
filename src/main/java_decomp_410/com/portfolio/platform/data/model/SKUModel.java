package com.portfolio.platform.data.model;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SKUModel {
    @DexIgnore
    public String createdAt;
    @DexIgnore
    public String deviceName;
    @DexIgnore
    public String deviceType;
    @DexIgnore
    public String gender;
    @DexIgnore
    public String groupName;
    @DexIgnore
    public String serialNumberPrefix;
    @DexIgnore
    public String sku;
    @DexIgnore
    public String updatedAt;

    @DexIgnore
    public SKUModel(String str, String str2, String str3, String str4, String str5, String str6) {
        kd4.b(str, "serialNumberPrefix");
        this.serialNumberPrefix = str;
        this.sku = str2;
        this.deviceName = str3;
        this.groupName = str4;
        this.gender = str5;
        this.deviceType = str6;
    }

    @DexIgnore
    public static /* synthetic */ SKUModel copy$default(SKUModel sKUModel, String str, String str2, String str3, String str4, String str5, String str6, int i, Object obj) {
        if ((i & 1) != 0) {
            str = sKUModel.serialNumberPrefix;
        }
        if ((i & 2) != 0) {
            str2 = sKUModel.sku;
        }
        String str7 = str2;
        if ((i & 4) != 0) {
            str3 = sKUModel.deviceName;
        }
        String str8 = str3;
        if ((i & 8) != 0) {
            str4 = sKUModel.groupName;
        }
        String str9 = str4;
        if ((i & 16) != 0) {
            str5 = sKUModel.gender;
        }
        String str10 = str5;
        if ((i & 32) != 0) {
            str6 = sKUModel.deviceType;
        }
        return sKUModel.copy(str, str7, str8, str9, str10, str6);
    }

    @DexIgnore
    public final String component1() {
        return this.serialNumberPrefix;
    }

    @DexIgnore
    public final String component2() {
        return this.sku;
    }

    @DexIgnore
    public final String component3() {
        return this.deviceName;
    }

    @DexIgnore
    public final String component4() {
        return this.groupName;
    }

    @DexIgnore
    public final String component5() {
        return this.gender;
    }

    @DexIgnore
    public final String component6() {
        return this.deviceType;
    }

    @DexIgnore
    public final SKUModel copy(String str, String str2, String str3, String str4, String str5, String str6) {
        kd4.b(str, "serialNumberPrefix");
        return new SKUModel(str, str2, str3, str4, str5, str6);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SKUModel)) {
            return false;
        }
        SKUModel sKUModel = (SKUModel) obj;
        return kd4.a((Object) this.serialNumberPrefix, (Object) sKUModel.serialNumberPrefix) && kd4.a((Object) this.sku, (Object) sKUModel.sku) && kd4.a((Object) this.deviceName, (Object) sKUModel.deviceName) && kd4.a((Object) this.groupName, (Object) sKUModel.groupName) && kd4.a((Object) this.gender, (Object) sKUModel.gender) && kd4.a((Object) this.deviceType, (Object) sKUModel.deviceType);
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getDeviceName() {
        return this.deviceName;
    }

    @DexIgnore
    public final String getDeviceType() {
        return this.deviceType;
    }

    @DexIgnore
    public final String getGender() {
        return this.gender;
    }

    @DexIgnore
    public final String getGroupName() {
        return this.groupName;
    }

    @DexIgnore
    public final String getSerialNumberPrefix() {
        return this.serialNumberPrefix;
    }

    @DexIgnore
    public final String getSku() {
        return this.sku;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.serialNumberPrefix;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.sku;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.deviceName;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.groupName;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.gender;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.deviceType;
        if (str6 != null) {
            i = str6.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDeviceName(String str) {
        this.deviceName = str;
    }

    @DexIgnore
    public final void setDeviceType(String str) {
        this.deviceType = str;
    }

    @DexIgnore
    public final void setGender(String str) {
        this.gender = str;
    }

    @DexIgnore
    public final void setGroupName(String str) {
        this.groupName = str;
    }

    @DexIgnore
    public final void setSerialNumberPrefix(String str) {
        kd4.b(str, "<set-?>");
        this.serialNumberPrefix = str;
    }

    @DexIgnore
    public final void setSku(String str) {
        this.sku = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "SKUModel(serialNumberPrefix=" + this.serialNumberPrefix + ", sku=" + this.sku + ", deviceName=" + this.deviceName + ", groupName=" + this.groupName + ", gender=" + this.gender + ", deviceType=" + this.deviceType + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ SKUModel(String str, String str2, String str3, String str4, String str5, String str6, int i, fd4 fd4) {
        this(str, r0, (i & 4) != 0 ? null : str3, (i & 8) != 0 ? null : str4, (i & 16) != 0 ? null : str5, (i & 32) == 0 ? str6 : r1);
        String str7 = null;
        String str8 = (i & 2) != 0 ? null : str2;
    }
}
