package com.portfolio.platform.data.model.ua;

import com.fossil.blesdk.obfuscated.f02;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UAEmbedded {
    @DexIgnore
    @f02("device")
    public List<UADevice> device;

    @DexIgnore
    public final List<UADevice> getDevice() {
        return this.device;
    }

    @DexIgnore
    public final void setDevice(List<UADevice> list) {
        this.device = list;
    }
}
