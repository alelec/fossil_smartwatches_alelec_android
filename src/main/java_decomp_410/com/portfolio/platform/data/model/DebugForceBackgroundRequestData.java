package com.portfolio.platform.data.model;

import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.model.customrequest.ForceBackgroundRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DebugForceBackgroundRequestData {
    @DexIgnore
    public ForceBackgroundRequest.BackgroundRequestType backgroundRequestType;
    @DexIgnore
    public /* final */ String visibleName;

    @DexIgnore
    public DebugForceBackgroundRequestData(String str, ForceBackgroundRequest.BackgroundRequestType backgroundRequestType2) {
        kd4.b(str, "visibleName");
        kd4.b(backgroundRequestType2, "backgroundRequestType");
        this.visibleName = str;
        this.backgroundRequestType = backgroundRequestType2;
    }

    @DexIgnore
    public final ForceBackgroundRequest.BackgroundRequestType getBackgroundRequestType() {
        return this.backgroundRequestType;
    }

    @DexIgnore
    public final String getVisibleName() {
        return this.visibleName;
    }

    @DexIgnore
    public final void setBackgroundRequestType(ForceBackgroundRequest.BackgroundRequestType backgroundRequestType2) {
        kd4.b(backgroundRequestType2, "<set-?>");
        this.backgroundRequestType = backgroundRequestType2;
    }
}
