package com.portfolio.platform.data.model;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DebugFirmwareData {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ int DOWNLOADED; // = 2;
    @DexIgnore
    public static /* final */ int DOWNLOADING; // = 1;
    @DexIgnore
    public static /* final */ int NONE; // = 0;
    @DexIgnore
    public /* final */ Firmware firmware;
    @DexIgnore
    public int state;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public DebugFirmwareData(Firmware firmware2, int i) {
        kd4.b(firmware2, "firmware");
        this.firmware = firmware2;
        this.state = i;
    }

    @DexIgnore
    public final Firmware getFirmware() {
        return this.firmware;
    }

    @DexIgnore
    public final int getState() {
        return this.state;
    }

    @DexIgnore
    public final void setState(int i) {
        this.state = i;
    }
}
