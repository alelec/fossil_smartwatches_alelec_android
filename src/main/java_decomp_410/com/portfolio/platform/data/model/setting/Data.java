package com.portfolio.platform.data.model.setting;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Data {
    @DexIgnore
    @f02("url")
    public String url;

    @DexIgnore
    public Data(String str) {
        kd4.b(str, "url");
        this.url = str;
    }

    @DexIgnore
    public static /* synthetic */ Data copy$default(Data data, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = data.url;
        }
        return data.copy(str);
    }

    @DexIgnore
    public final String component1() {
        return this.url;
    }

    @DexIgnore
    public final Data copy(String str) {
        kd4.b(str, "url");
        return new Data(str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof Data) && kd4.a((Object) this.url, (Object) ((Data) obj).url);
        }
        return true;
    }

    @DexIgnore
    public final String getUrl() {
        return this.url;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.url;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public final void setUrl(String str) {
        kd4.b(str, "<set-?>");
        this.url = str;
    }

    @DexIgnore
    public String toString() {
        return "Data(url=" + this.url + ")";
    }
}
