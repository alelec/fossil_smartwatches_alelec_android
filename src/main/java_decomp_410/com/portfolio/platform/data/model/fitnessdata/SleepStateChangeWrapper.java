package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.fitness.SleepStateChange;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepStateChangeWrapper {
    @DexIgnore
    public int indexInMinute;
    @DexIgnore
    public int state;

    @DexIgnore
    public SleepStateChangeWrapper(int i, int i2) {
        this.state = i;
        this.indexInMinute = i2;
    }

    @DexIgnore
    public static /* synthetic */ SleepStateChangeWrapper copy$default(SleepStateChangeWrapper sleepStateChangeWrapper, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = sleepStateChangeWrapper.state;
        }
        if ((i3 & 2) != 0) {
            i2 = sleepStateChangeWrapper.indexInMinute;
        }
        return sleepStateChangeWrapper.copy(i, i2);
    }

    @DexIgnore
    public final int component1() {
        return this.state;
    }

    @DexIgnore
    public final int component2() {
        return this.indexInMinute;
    }

    @DexIgnore
    public final SleepStateChangeWrapper copy(int i, int i2) {
        return new SleepStateChangeWrapper(i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SleepStateChangeWrapper) {
                SleepStateChangeWrapper sleepStateChangeWrapper = (SleepStateChangeWrapper) obj;
                if (this.state == sleepStateChangeWrapper.state) {
                    if (this.indexInMinute == sleepStateChangeWrapper.indexInMinute) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getIndexInMinute() {
        return this.indexInMinute;
    }

    @DexIgnore
    public final int getState() {
        return this.state;
    }

    @DexIgnore
    public int hashCode() {
        return (this.state * 31) + this.indexInMinute;
    }

    @DexIgnore
    public final void setIndexInMinute(int i) {
        this.indexInMinute = i;
    }

    @DexIgnore
    public final void setState(int i) {
        this.state = i;
    }

    @DexIgnore
    public String toString() {
        return "SleepStateChangeWrapper(state=" + this.state + ", indexInMinute=" + this.indexInMinute + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepStateChangeWrapper(SleepStateChange sleepStateChange) {
        this(sleepStateChange.getState().ordinal(), sleepStateChange.getIndexInMinute());
        kd4.b(sleepStateChange, "stateChange");
    }
}
