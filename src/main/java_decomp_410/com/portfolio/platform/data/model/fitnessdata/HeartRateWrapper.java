package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.fitness.HeartRate;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateWrapper {
    @DexIgnore
    public short average;
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public List<Short> values;

    @DexIgnore
    public HeartRateWrapper(int i, short s, List<Short> list) {
        kd4.b(list, "values");
        this.resolutionInSecond = i;
        this.average = s;
        this.values = list;
    }

    @DexIgnore
    public static /* synthetic */ HeartRateWrapper copy$default(HeartRateWrapper heartRateWrapper, int i, short s, List<Short> list, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = heartRateWrapper.resolutionInSecond;
        }
        if ((i2 & 2) != 0) {
            s = heartRateWrapper.average;
        }
        if ((i2 & 4) != 0) {
            list = heartRateWrapper.values;
        }
        return heartRateWrapper.copy(i, s, list);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final short component2() {
        return this.average;
    }

    @DexIgnore
    public final List<Short> component3() {
        return this.values;
    }

    @DexIgnore
    public final HeartRateWrapper copy(int i, short s, List<Short> list) {
        kd4.b(list, "values");
        return new HeartRateWrapper(i, s, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof HeartRateWrapper) {
                HeartRateWrapper heartRateWrapper = (HeartRateWrapper) obj;
                if (this.resolutionInSecond == heartRateWrapper.resolutionInSecond) {
                    if (!(this.average == heartRateWrapper.average) || !kd4.a((Object) this.values, (Object) heartRateWrapper.values)) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final short getAverage() {
        return this.average;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Short> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = ((this.resolutionInSecond * 31) + this.average) * 31;
        List<Short> list = this.values;
        return i + (list != null ? list.hashCode() : 0);
    }

    @DexIgnore
    public final void setAverage(short s) {
        this.average = s;
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setValues(List<Short> list) {
        kd4.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "HeartRateWrapper(resolutionInSecond=" + this.resolutionInSecond + ", average=" + this.average + ", values=" + this.values + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public HeartRateWrapper(HeartRate heartRate) {
        throw null;
/*        this(r0, r1, r4);
        kd4.b(heartRate, "heartRate");
        int resolutionInSecond2 = heartRate.getResolutionInSecond();
        short average2 = heartRate.getAverage();
        ArrayList<Short> values2 = heartRate.getValues();
        kd4.a((Object) values2, "heartRate.values");
*/    }
}
