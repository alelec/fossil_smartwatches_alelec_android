package com.portfolio.platform.data.model;

import com.fossil.blesdk.obfuscated.f02;
import com.google.gson.Gson;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@DatabaseTable(tableName = "PhoneFavoritesContact")
public class PhoneFavoritesContact {
    @DexIgnore
    public static /* final */ String COLUMN_PHONE_NUMBER; // = "phoneNumber";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "PhoneFavoritesContact";
    @DexIgnore
    @f02("phoneNumber")
    @DatabaseField(columnName = "phoneNumber", id = true)
    public String phoneNumber;

    @DexIgnore
    public PhoneFavoritesContact() {
    }

    @DexIgnore
    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    @DexIgnore
    public void setPhoneNumber(String str) {
        this.phoneNumber = str;
    }

    @DexIgnore
    public String toString() {
        return new Gson().a((Object) this);
    }

    @DexIgnore
    public PhoneFavoritesContact(String str) {
        this.phoneNumber = str;
    }
}
