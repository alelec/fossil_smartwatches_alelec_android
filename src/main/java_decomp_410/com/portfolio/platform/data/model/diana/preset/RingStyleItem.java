package com.portfolio.platform.data.model.diana.preset;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RingStyleItem {
    @DexIgnore
    @f02("complicationPosition")
    public String position;
    @DexIgnore
    @f02("ringStyle")
    public RingStyle ringStyle;

    @DexIgnore
    public RingStyleItem(String str, RingStyle ringStyle2) {
        kd4.b(str, "position");
        kd4.b(ringStyle2, "ringStyle");
        this.position = str;
        this.ringStyle = ringStyle2;
    }

    @DexIgnore
    public static /* synthetic */ RingStyleItem copy$default(RingStyleItem ringStyleItem, String str, RingStyle ringStyle2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = ringStyleItem.position;
        }
        if ((i & 2) != 0) {
            ringStyle2 = ringStyleItem.ringStyle;
        }
        return ringStyleItem.copy(str, ringStyle2);
    }

    @DexIgnore
    public final String component1() {
        return this.position;
    }

    @DexIgnore
    public final RingStyle component2() {
        return this.ringStyle;
    }

    @DexIgnore
    public final RingStyleItem copy(String str, RingStyle ringStyle2) {
        kd4.b(str, "position");
        kd4.b(ringStyle2, "ringStyle");
        return new RingStyleItem(str, ringStyle2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof RingStyleItem)) {
            return false;
        }
        RingStyleItem ringStyleItem = (RingStyleItem) obj;
        return kd4.a((Object) this.position, (Object) ringStyleItem.position) && kd4.a((Object) this.ringStyle, (Object) ringStyleItem.ringStyle);
    }

    @DexIgnore
    public final String getPosition() {
        return this.position;
    }

    @DexIgnore
    public final RingStyle getRingStyle() {
        return this.ringStyle;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.position;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        RingStyle ringStyle2 = this.ringStyle;
        if (ringStyle2 != null) {
            i = ringStyle2.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setPosition(String str) {
        kd4.b(str, "<set-?>");
        this.position = str;
    }

    @DexIgnore
    public final void setRingStyle(RingStyle ringStyle2) {
        kd4.b(ringStyle2, "<set-?>");
        this.ringStyle = ringStyle2;
    }

    @DexIgnore
    public String toString() {
        return "RingStyleItem(position=" + this.position + ", ringStyle=" + this.ringStyle + ")";
    }
}
