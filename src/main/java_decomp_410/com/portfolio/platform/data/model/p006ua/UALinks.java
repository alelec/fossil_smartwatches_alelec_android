package com.portfolio.platform.data.model.p006ua;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.model.ua.UALinks */
public final class UALinks {
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("device")
    public java.util.List<com.portfolio.platform.data.model.p006ua.UALink> device;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("self")
    public java.util.List<com.portfolio.platform.data.model.p006ua.UALink> self;

    @DexIgnore
    public final java.util.List<com.portfolio.platform.data.model.p006ua.UALink> getDevice() {
        return this.device;
    }

    @DexIgnore
    public final java.util.List<com.portfolio.platform.data.model.p006ua.UALink> getSelf() {
        return this.self;
    }

    @DexIgnore
    public final void setDevice(java.util.List<com.portfolio.platform.data.model.p006ua.UALink> list) {
        this.device = list;
    }

    @DexIgnore
    public final void setSelf(java.util.List<com.portfolio.platform.data.model.p006ua.UALink> list) {
        this.self = list;
    }
}
