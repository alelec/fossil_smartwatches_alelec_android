package com.portfolio.platform.data.model.goaltracking;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import java.io.Serializable;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingData implements Serializable {
    @DexIgnore
    @f02("createdAt")
    public long createdAt;
    @DexIgnore
    @f02("date")
    public Date date;
    @DexIgnore
    @f02("id")
    public String id;
    @DexIgnore
    public int pinType;
    @DexIgnore
    @f02("timezoneOffset")
    public /* final */ int timezoneOffsetInSecond;
    @DexIgnore
    @f02("trackedAt")
    public /* final */ DateTime trackedAt;
    @DexIgnore
    @f02("updatedAt")
    public long updatedAt;

    @DexIgnore
    public GoalTrackingData(String str, DateTime dateTime, int i, Date date2, long j, long j2) {
        kd4.b(str, "id");
        kd4.b(dateTime, GoalTrackingEvent.COLUMN_TRACKED_AT);
        kd4.b(date2, "date");
        this.id = str;
        this.trackedAt = dateTime;
        this.timezoneOffsetInSecond = i;
        this.date = date2;
        this.createdAt = j;
        this.updatedAt = j2;
    }

    @DexIgnore
    public static /* synthetic */ GoalTrackingData copy$default(GoalTrackingData goalTrackingData, String str, DateTime dateTime, int i, Date date2, long j, long j2, int i2, Object obj) {
        GoalTrackingData goalTrackingData2 = goalTrackingData;
        return goalTrackingData.copy((i2 & 1) != 0 ? goalTrackingData2.id : str, (i2 & 2) != 0 ? goalTrackingData2.trackedAt : dateTime, (i2 & 4) != 0 ? goalTrackingData2.timezoneOffsetInSecond : i, (i2 & 8) != 0 ? goalTrackingData2.date : date2, (i2 & 16) != 0 ? goalTrackingData2.createdAt : j, (i2 & 32) != 0 ? goalTrackingData2.updatedAt : j2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.trackedAt;
    }

    @DexIgnore
    public final int component3() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final Date component4() {
        return this.date;
    }

    @DexIgnore
    public final long component5() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component6() {
        return this.updatedAt;
    }

    @DexIgnore
    public final GoalTrackingData copy(String str, DateTime dateTime, int i, Date date2, long j, long j2) {
        kd4.b(str, "id");
        kd4.b(dateTime, GoalTrackingEvent.COLUMN_TRACKED_AT);
        kd4.b(date2, "date");
        return new GoalTrackingData(str, dateTime, i, date2, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof GoalTrackingData) {
                GoalTrackingData goalTrackingData = (GoalTrackingData) obj;
                if (kd4.a((Object) this.id, (Object) goalTrackingData.id) && kd4.a((Object) this.trackedAt, (Object) goalTrackingData.trackedAt)) {
                    if ((this.timezoneOffsetInSecond == goalTrackingData.timezoneOffsetInSecond) && kd4.a((Object) this.date, (Object) goalTrackingData.date)) {
                        if (this.createdAt == goalTrackingData.createdAt) {
                            if (this.updatedAt == goalTrackingData.updatedAt) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final DateTime getTrackedAt() {
        return this.trackedAt;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        DateTime dateTime = this.trackedAt;
        int hashCode2 = (((hashCode + (dateTime != null ? dateTime.hashCode() : 0)) * 31) + this.timezoneOffsetInSecond) * 31;
        Date date2 = this.date;
        if (date2 != null) {
            i = date2.hashCode();
        }
        long j = this.createdAt;
        long j2 = this.updatedAt;
        return ((((hashCode2 + i) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)));
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        kd4.b(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setId(String str) {
        kd4.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "GoalTrackingData(id=" + this.id + ", trackedAt=" + this.trackedAt + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", date=" + this.date + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
