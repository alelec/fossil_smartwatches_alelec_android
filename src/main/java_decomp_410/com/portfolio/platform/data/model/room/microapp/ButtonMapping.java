package com.portfolio.platform.data.model.room.microapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ButtonMapping implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    @f02("button")
    public String button;
    @DexIgnore
    @f02("appId")
    public String microAppId;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ButtonMapping> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public ButtonMapping createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new ButtonMapping(parcel);
        }

        @DexIgnore
        public ButtonMapping[] newArray(int i) {
            return new ButtonMapping[i];
        }
    }

    @DexIgnore
    public ButtonMapping(String str, String str2) {
        kd4.b(str, "button");
        kd4.b(str2, "microAppId");
        this.button = str;
        this.microAppId = str2;
    }

    @DexIgnore
    public static /* synthetic */ ButtonMapping copy$default(ButtonMapping buttonMapping, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = buttonMapping.button;
        }
        if ((i & 2) != 0) {
            str2 = buttonMapping.microAppId;
        }
        return buttonMapping.copy(str, str2);
    }

    @DexIgnore
    public final String component1() {
        return this.button;
    }

    @DexIgnore
    public final String component2() {
        return this.microAppId;
    }

    @DexIgnore
    public final ButtonMapping copy(String str, String str2) {
        kd4.b(str, "button");
        kd4.b(str2, "microAppId");
        return new ButtonMapping(str, str2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ButtonMapping)) {
            return false;
        }
        ButtonMapping buttonMapping = (ButtonMapping) obj;
        return kd4.a((Object) this.button, (Object) buttonMapping.button) && kd4.a((Object) this.microAppId, (Object) buttonMapping.microAppId);
    }

    @DexIgnore
    public final String getButton() {
        return this.button;
    }

    @DexIgnore
    public final String getMicroAppId() {
        return this.microAppId;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.button;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.microAppId;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setButton(String str) {
        kd4.b(str, "<set-?>");
        this.button = str;
    }

    @DexIgnore
    public final void setMicroAppId(String str) {
        kd4.b(str, "<set-?>");
        this.microAppId = str;
    }

    @DexIgnore
    public String toString() {
        return "ButtonMapping(button=" + this.button + ", microAppId=" + this.microAppId + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(this.button);
        parcel.writeString(this.microAppId);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ButtonMapping(Parcel parcel) {
        throw null;
/*        this(r0, r3 == null ? "" : r3);
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        readString = readString == null ? "" : readString;
        String readString2 = parcel.readString();
*/    }
}
