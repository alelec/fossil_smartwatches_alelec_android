package com.portfolio.platform.data.model;

import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class FitnessDayData implements Serializable {
    @DexIgnore
    public DateTime createdAt;
    @DexIgnore
    public Date date;
    @DexIgnore
    public int goalActiveTime;
    @DexIgnore
    public int goalCalories;
    @DexIgnore
    public int goalSteps;
    @DexIgnore
    public String owner;
    @DexIgnore
    public int totalActiveTime;
    @DexIgnore
    public int totalActivities;
    @DexIgnore
    public double totalCalories;
    @DexIgnore
    public double totalDistance;
    @DexIgnore
    public List<Integer> totalIntensityDistInStep;
    @DexIgnore
    public long totalSteps;
    @DexIgnore
    public DateTime updatedAt;

    @DexIgnore
    public int getActiveTime() {
        return this.totalActiveTime;
    }

    @DexIgnore
    public DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public Date getDate() {
        return this.date;
    }

    @DexIgnore
    public int getGoalActiveTime() {
        return this.goalActiveTime;
    }

    @DexIgnore
    public int getGoalCalories() {
        return this.goalCalories;
    }

    @DexIgnore
    public int getGoalSteps() {
        return this.goalSteps;
    }

    @DexIgnore
    public String getOwner() {
        return this.owner;
    }

    @DexIgnore
    public int getTotalActivities() {
        return this.totalActivities;
    }

    @DexIgnore
    public double getTotalCalories() {
        return this.totalCalories;
    }

    @DexIgnore
    public double getTotalDistance() {
        return this.totalDistance;
    }

    @DexIgnore
    public List<Integer> getTotalIntensityDistInStep() {
        return this.totalIntensityDistInStep;
    }

    @DexIgnore
    public double getTotalSteps() {
        return (double) this.totalSteps;
    }

    @DexIgnore
    public DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public ActivitySummary toActivitySummary() {
        Date date2 = this.date;
        Calendar instance = Calendar.getInstance();
        instance.setTime(date2);
        int i = instance.get(1);
        int i2 = instance.get(2) + 1;
        int i3 = instance.get(5);
        TimeZone timeZone = TimeZone.getDefault();
        ActivitySummary activitySummary = new ActivitySummary(i, i2, i3, timeZone.getID(), Integer.valueOf(timeZone.inDaylightTime(date2) ? timeZone.getDSTSavings() : 0), getTotalSteps(), this.totalCalories, this.totalDistance, this.totalIntensityDistInStep, this.totalActiveTime, this.goalSteps, this.goalCalories, this.goalActiveTime);
        DateTime dateTime = this.createdAt;
        if (dateTime == null) {
            activitySummary.setCreatedAt(DateTime.now());
        } else {
            activitySummary.setCreatedAt(dateTime);
        }
        DateTime dateTime2 = this.updatedAt;
        if (dateTime2 == null) {
            activitySummary.setUpdatedAt(DateTime.now());
        } else {
            activitySummary.setUpdatedAt(dateTime2);
        }
        return activitySummary;
    }
}
