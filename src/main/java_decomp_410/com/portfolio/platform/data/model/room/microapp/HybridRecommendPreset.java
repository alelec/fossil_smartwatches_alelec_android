package com.portfolio.platform.data.model.room.microapp;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.sj2;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridRecommendPreset {
    @DexIgnore
    @f02("buttons")
    public ArrayList<HybridPresetAppSetting> buttons;
    @DexIgnore
    @f02("createdAt")
    public String createdAt;
    @DexIgnore
    @f02("id")
    public String id;
    @DexIgnore
    @f02("isDefault")
    public boolean isDefault;
    @DexIgnore
    @f02("name")
    public String name;
    @DexIgnore
    @f02("serialNumber")
    public String serialNumber;
    @DexIgnore
    @f02("updatedAt")
    public String updatedAt;

    @DexIgnore
    public HybridRecommendPreset(String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z, String str4, String str5) {
        kd4.b(str, "id");
        kd4.b(str3, "serialNumber");
        kd4.b(arrayList, "buttons");
        kd4.b(str4, "createdAt");
        kd4.b(str5, "updatedAt");
        this.id = str;
        this.name = str2;
        this.serialNumber = str3;
        this.buttons = arrayList;
        this.isDefault = z;
        this.createdAt = str4;
        this.updatedAt = str5;
    }

    @DexIgnore
    public static /* synthetic */ HybridRecommendPreset copy$default(HybridRecommendPreset hybridRecommendPreset, String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z, String str4, String str5, int i, Object obj) {
        if ((i & 1) != 0) {
            str = hybridRecommendPreset.id;
        }
        if ((i & 2) != 0) {
            str2 = hybridRecommendPreset.name;
        }
        String str6 = str2;
        if ((i & 4) != 0) {
            str3 = hybridRecommendPreset.serialNumber;
        }
        String str7 = str3;
        if ((i & 8) != 0) {
            arrayList = hybridRecommendPreset.buttons;
        }
        ArrayList<HybridPresetAppSetting> arrayList2 = arrayList;
        if ((i & 16) != 0) {
            z = hybridRecommendPreset.isDefault;
        }
        boolean z2 = z;
        if ((i & 32) != 0) {
            str4 = hybridRecommendPreset.createdAt;
        }
        String str8 = str4;
        if ((i & 64) != 0) {
            str5 = hybridRecommendPreset.updatedAt;
        }
        return hybridRecommendPreset.copy(str, str6, str7, arrayList2, z2, str8, str5);
    }

    @DexIgnore
    public final HybridPreset clone() {
        HybridPreset hybridPreset = new HybridPreset(this.id, this.name, this.serialNumber, sj2.a((List<HybridPresetAppSetting>) this.buttons), this.isDefault);
        hybridPreset.setCreatedAt(this.createdAt);
        hybridPreset.setUpdatedAt(this.updatedAt);
        return hybridPreset;
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final String component3() {
        return this.serialNumber;
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> component4() {
        return this.buttons;
    }

    @DexIgnore
    public final boolean component5() {
        return this.isDefault;
    }

    @DexIgnore
    public final String component6() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component7() {
        return this.updatedAt;
    }

    @DexIgnore
    public final HybridRecommendPreset copy(String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z, String str4, String str5) {
        kd4.b(str, "id");
        kd4.b(str3, "serialNumber");
        kd4.b(arrayList, "buttons");
        kd4.b(str4, "createdAt");
        String str6 = str5;
        kd4.b(str6, "updatedAt");
        return new HybridRecommendPreset(str, str2, str3, arrayList, z, str4, str6);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof HybridRecommendPreset) {
                HybridRecommendPreset hybridRecommendPreset = (HybridRecommendPreset) obj;
                if (kd4.a((Object) this.id, (Object) hybridRecommendPreset.id) && kd4.a((Object) this.name, (Object) hybridRecommendPreset.name) && kd4.a((Object) this.serialNumber, (Object) hybridRecommendPreset.serialNumber) && kd4.a((Object) this.buttons, (Object) hybridRecommendPreset.buttons)) {
                    if (!(this.isDefault == hybridRecommendPreset.isDefault) || !kd4.a((Object) this.createdAt, (Object) hybridRecommendPreset.createdAt) || !kd4.a((Object) this.updatedAt, (Object) hybridRecommendPreset.updatedAt)) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> getButtons() {
        return this.buttons;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.serialNumber;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        ArrayList<HybridPresetAppSetting> arrayList = this.buttons;
        int hashCode4 = (hashCode3 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        boolean z = this.isDefault;
        if (z) {
            z = true;
        }
        int i2 = (hashCode4 + (z ? 1 : 0)) * 31;
        String str4 = this.createdAt;
        int hashCode5 = (i2 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.updatedAt;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final boolean isDefault() {
        return this.isDefault;
    }

    @DexIgnore
    public final void setButtons(ArrayList<HybridPresetAppSetting> arrayList) {
        kd4.b(arrayList, "<set-?>");
        this.buttons = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        kd4.b(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDefault(boolean z) {
        this.isDefault = z;
    }

    @DexIgnore
    public final void setId(String str) {
        kd4.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        kd4.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        kd4.b(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "HybridRecommendPreset(id=" + this.id + ", name=" + this.name + ", serialNumber=" + this.serialNumber + ", buttons=" + this.buttons + ", isDefault=" + this.isDefault + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HybridRecommendPreset(String str, String str2, String str3, ArrayList arrayList, boolean z, String str4, String str5, int i, fd4 fd4) {
        this(str, (i & 2) != 0 ? "" : str2, str3, arrayList, z, str4, str5);
    }
}
