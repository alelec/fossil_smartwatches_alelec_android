package com.portfolio.platform.data.model;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.ik2;
import java.util.Date;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalSetting {
    @DexIgnore
    @f02("currentTarget")
    public int currentTarget;
    @DexIgnore
    @ik2
    public int id;
    @DexIgnore
    @f02("timezoneOffset")
    public Integer timezone; // = Integer.valueOf(TimeZone.getDefault().getOffset(new Date().getTime()) / 1000);

    @DexIgnore
    public GoalSetting(int i) {
        this.currentTarget = i;
    }

    @DexIgnore
    public static /* synthetic */ GoalSetting copy$default(GoalSetting goalSetting, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = goalSetting.currentTarget;
        }
        return goalSetting.copy(i);
    }

    @DexIgnore
    public final int component1() {
        return this.currentTarget;
    }

    @DexIgnore
    public final GoalSetting copy(int i) {
        return new GoalSetting(i);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof GoalSetting) {
                if (this.currentTarget == ((GoalSetting) obj).currentTarget) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getCurrentTarget() {
        return this.currentTarget;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final Integer getTimezone() {
        return this.timezone;
    }

    @DexIgnore
    public int hashCode() {
        return this.currentTarget;
    }

    @DexIgnore
    public final void setCurrentTarget(int i) {
        this.currentTarget = i;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setTimezone(Integer num) {
        this.timezone = num;
    }

    @DexIgnore
    public String toString() {
        return "GoalSetting(currentTarget=" + this.currentTarget + ")";
    }
}
