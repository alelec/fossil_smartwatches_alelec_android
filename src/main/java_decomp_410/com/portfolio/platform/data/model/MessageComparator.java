package com.portfolio.platform.data.model;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MessageComparator implements Comparator<LightAndHaptics> {
    @DexIgnore
    public int compare(LightAndHaptics lightAndHaptics, LightAndHaptics lightAndHaptics2) {
        if (lightAndHaptics == null && lightAndHaptics2 == null) {
            return 0;
        }
        if (lightAndHaptics != null && lightAndHaptics2 == null) {
            return 1;
        }
        if (lightAndHaptics != null || lightAndHaptics2 == null) {
            return lightAndHaptics.getPriority().compareTo(lightAndHaptics2.getPriority());
        }
        return -1;
    }
}
