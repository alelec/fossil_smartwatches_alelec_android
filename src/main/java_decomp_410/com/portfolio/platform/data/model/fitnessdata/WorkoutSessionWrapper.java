package com.portfolio.platform.data.model.fitnessdata;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.fitness.Calorie;
import com.fossil.fitness.Distance;
import com.fossil.fitness.HeartRate;
import com.fossil.fitness.Step;
import com.fossil.fitness.WorkoutSession;
import com.fossil.fitness.WorkoutStateChange;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.enums.WorkoutState;
import com.portfolio.platform.enums.WorkoutType;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSessionWrapper {
    @DexIgnore
    public CalorieWrapper calorie;
    @DexIgnore
    public DistanceWrapper distance;
    @DexIgnore
    public int duration;
    @DexIgnore
    public DateTime endTime;
    @DexIgnore
    public HeartRateWrapper heartRate;
    @DexIgnore
    public int id;
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public List<WorkoutStateChangeWrapper> stateChanges;
    @DexIgnore
    public StepWrapper step;
    @DexIgnore
    public int timezoneOffsetInSecond;
    @DexIgnore
    public int type;

    @DexIgnore
    public WorkoutSessionWrapper(int i, DateTime dateTime, DateTime dateTime2, int i2, int i3, int i4, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper) {
        kd4.b(dateTime, SampleRaw.COLUMN_START_TIME);
        kd4.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        kd4.b(stepWrapper, "step");
        kd4.b(calorieWrapper, "calorie");
        kd4.b(distanceWrapper, "distance");
        this.id = i;
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.timezoneOffsetInSecond = i2;
        this.duration = i3;
        this.type = i4;
        this.step = stepWrapper;
        this.calorie = calorieWrapper;
        this.distance = distanceWrapper;
        this.heartRate = heartRateWrapper;
        this.stateChanges = new ArrayList();
    }

    @DexIgnore
    public static /* synthetic */ WorkoutSessionWrapper copy$default(WorkoutSessionWrapper workoutSessionWrapper, int i, DateTime dateTime, DateTime dateTime2, int i2, int i3, int i4, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper, int i5, Object obj) {
        WorkoutSessionWrapper workoutSessionWrapper2 = workoutSessionWrapper;
        int i6 = i5;
        return workoutSessionWrapper.copy((i6 & 1) != 0 ? workoutSessionWrapper2.id : i, (i6 & 2) != 0 ? workoutSessionWrapper2.startTime : dateTime, (i6 & 4) != 0 ? workoutSessionWrapper2.endTime : dateTime2, (i6 & 8) != 0 ? workoutSessionWrapper2.timezoneOffsetInSecond : i2, (i6 & 16) != 0 ? workoutSessionWrapper2.duration : i3, (i6 & 32) != 0 ? workoutSessionWrapper2.type : i4, (i6 & 64) != 0 ? workoutSessionWrapper2.step : stepWrapper, (i6 & 128) != 0 ? workoutSessionWrapper2.calorie : calorieWrapper, (i6 & 256) != 0 ? workoutSessionWrapper2.distance : distanceWrapper, (i6 & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? workoutSessionWrapper2.heartRate : heartRateWrapper);
    }

    @DexIgnore
    public final int component1() {
        return this.id;
    }

    @DexIgnore
    public final HeartRateWrapper component10() {
        return this.heartRate;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.endTime;
    }

    @DexIgnore
    public final int component4() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int component5() {
        return this.duration;
    }

    @DexIgnore
    public final int component6() {
        return this.type;
    }

    @DexIgnore
    public final StepWrapper component7() {
        return this.step;
    }

    @DexIgnore
    public final CalorieWrapper component8() {
        return this.calorie;
    }

    @DexIgnore
    public final DistanceWrapper component9() {
        return this.distance;
    }

    @DexIgnore
    public final WorkoutSessionWrapper copy(int i, DateTime dateTime, DateTime dateTime2, int i2, int i3, int i4, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper) {
        kd4.b(dateTime, SampleRaw.COLUMN_START_TIME);
        kd4.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        StepWrapper stepWrapper2 = stepWrapper;
        kd4.b(stepWrapper2, "step");
        CalorieWrapper calorieWrapper2 = calorieWrapper;
        kd4.b(calorieWrapper2, "calorie");
        DistanceWrapper distanceWrapper2 = distanceWrapper;
        kd4.b(distanceWrapper2, "distance");
        return new WorkoutSessionWrapper(i, dateTime, dateTime2, i2, i3, i4, stepWrapper2, calorieWrapper2, distanceWrapper2, heartRateWrapper);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutSessionWrapper) {
                WorkoutSessionWrapper workoutSessionWrapper = (WorkoutSessionWrapper) obj;
                if ((this.id == workoutSessionWrapper.id) && kd4.a((Object) this.startTime, (Object) workoutSessionWrapper.startTime) && kd4.a((Object) this.endTime, (Object) workoutSessionWrapper.endTime)) {
                    if (this.timezoneOffsetInSecond == workoutSessionWrapper.timezoneOffsetInSecond) {
                        if (this.duration == workoutSessionWrapper.duration) {
                            if (!(this.type == workoutSessionWrapper.type) || !kd4.a((Object) this.step, (Object) workoutSessionWrapper.step) || !kd4.a((Object) this.calorie, (Object) workoutSessionWrapper.calorie) || !kd4.a((Object) this.distance, (Object) workoutSessionWrapper.distance) || !kd4.a((Object) this.heartRate, (Object) workoutSessionWrapper.heartRate)) {
                                return false;
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final CalorieWrapper getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public final DistanceWrapper getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final int getDuration() {
        return this.duration;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final HeartRateWrapper getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final List<WorkoutStateChangeWrapper> getStateChanges() {
        return this.stateChanges;
    }

    @DexIgnore
    public final StepWrapper getStep() {
        return this.step;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.id * 31;
        DateTime dateTime = this.startTime;
        int i2 = 0;
        int hashCode = (i + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.endTime;
        int hashCode2 = (((((((hashCode + (dateTime2 != null ? dateTime2.hashCode() : 0)) * 31) + this.timezoneOffsetInSecond) * 31) + this.duration) * 31) + this.type) * 31;
        StepWrapper stepWrapper = this.step;
        int hashCode3 = (hashCode2 + (stepWrapper != null ? stepWrapper.hashCode() : 0)) * 31;
        CalorieWrapper calorieWrapper = this.calorie;
        int hashCode4 = (hashCode3 + (calorieWrapper != null ? calorieWrapper.hashCode() : 0)) * 31;
        DistanceWrapper distanceWrapper = this.distance;
        int hashCode5 = (hashCode4 + (distanceWrapper != null ? distanceWrapper.hashCode() : 0)) * 31;
        HeartRateWrapper heartRateWrapper = this.heartRate;
        if (heartRateWrapper != null) {
            i2 = heartRateWrapper.hashCode();
        }
        return hashCode5 + i2;
    }

    @DexIgnore
    public final void setCalorie(CalorieWrapper calorieWrapper) {
        kd4.b(calorieWrapper, "<set-?>");
        this.calorie = calorieWrapper;
    }

    @DexIgnore
    public final void setDistance(DistanceWrapper distanceWrapper) {
        kd4.b(distanceWrapper, "<set-?>");
        this.distance = distanceWrapper;
    }

    @DexIgnore
    public final void setDuration(int i) {
        this.duration = i;
    }

    @DexIgnore
    public final void setEndTime(DateTime dateTime) {
        kd4.b(dateTime, "<set-?>");
        this.endTime = dateTime;
    }

    @DexIgnore
    public final void setHeartRate(HeartRateWrapper heartRateWrapper) {
        this.heartRate = heartRateWrapper;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        kd4.b(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setStateChanges(List<WorkoutStateChangeWrapper> list) {
        kd4.b(list, "<set-?>");
        this.stateChanges = list;
    }

    @DexIgnore
    public final void setStep(StepWrapper stepWrapper) {
        kd4.b(stepWrapper, "<set-?>");
        this.step = stepWrapper;
    }

    @DexIgnore
    public final void setTimezoneOffsetInSecond(int i) {
        this.timezoneOffsetInSecond = i;
    }

    @DexIgnore
    public final void setType(int i) {
        this.type = i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSessionWrapper(id=" + this.id + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", duration=" + this.duration + ", type=" + this.type + ", step=" + this.step + ", calorie=" + this.calorie + ", distance=" + this.distance + ", heartRate=" + this.heartRate + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public WorkoutSessionWrapper(WorkoutSession workoutSession) {
        throw null;
/*        this(r2, r3, r0, r5, r6, r7, r8, r9, r10, r11);
        HeartRateWrapper heartRateWrapper;
        kd4.b(workoutSession, "workoutSession");
        int id2 = workoutSession.getId();
        DateTime dateTime = new DateTime(((long) workoutSession.getStarttime()) * 1000, DateTimeZone.forOffsetMillis(workoutSession.getTimezoneOffsetInSecond() * 1000));
        DateTime dateTime2 = new DateTime(((long) workoutSession.getEndtime()) * 1000, DateTimeZone.forOffsetMillis(workoutSession.getTimezoneOffsetInSecond() * 1000));
        int timezoneOffsetInSecond2 = workoutSession.getTimezoneOffsetInSecond();
        int duration2 = workoutSession.getDuration();
        int ordinal = WorkoutType.Companion.a(workoutSession.getType().ordinal()).ordinal();
        Step step2 = workoutSession.getStep();
        kd4.a((Object) step2, "workoutSession.step");
        StepWrapper stepWrapper = new StepWrapper(step2);
        Calorie calorie2 = workoutSession.getCalorie();
        kd4.a((Object) calorie2, "workoutSession.calorie");
        CalorieWrapper calorieWrapper = new CalorieWrapper(calorie2);
        Distance distance2 = workoutSession.getDistance();
        kd4.a((Object) distance2, "workoutSession.distance");
        DistanceWrapper distanceWrapper = new DistanceWrapper(distance2);
        if (workoutSession.getHeartrate() != null) {
            HeartRate heartrate = workoutSession.getHeartrate();
            kd4.a((Object) heartrate, "workoutSession.heartrate");
            heartRateWrapper = new HeartRateWrapper(heartrate);
        } else {
            heartRateWrapper = null;
        }
        HeartRateWrapper heartRateWrapper2 = heartRateWrapper;
        ArrayList<WorkoutStateChange> stateChanges2 = workoutSession.getStateChanges();
        kd4.a((Object) stateChanges2, "workoutSession.stateChanges");
        for (WorkoutStateChange workoutStateChange : stateChanges2) {
            List<WorkoutStateChangeWrapper> list = this.stateChanges;
            WorkoutState.a aVar = WorkoutState.Companion;
            kd4.a((Object) workoutStateChange, "it");
            list.add(new WorkoutStateChangeWrapper(aVar.a(workoutStateChange.getState().ordinal()).ordinal(), workoutStateChange.getIndexInSecond()));
        }
*/    }
}
