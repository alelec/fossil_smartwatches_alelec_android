package com.portfolio.platform.data.model.thirdparty.p005ua;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.model.thirdparty.ua.UASample */
public final class UASample {
    @DexIgnore
    public /* final */ double calorie;
    @DexIgnore
    public /* final */ double distance;

    @DexIgnore
    /* renamed from: id */
    public int f21077id;
    @DexIgnore
    public /* final */ int step;
    @DexIgnore
    public /* final */ long time;

    @DexIgnore
    public UASample(int i, double d, double d2, long j) {
        this.step = i;
        this.distance = d;
        this.calorie = d2;
        this.time = j;
    }

    @DexIgnore
    public static /* synthetic */ com.portfolio.platform.data.model.thirdparty.p005ua.UASample copy$default(com.portfolio.platform.data.model.thirdparty.p005ua.UASample uASample, int i, double d, double d2, long j, int i2, java.lang.Object obj) {
        if ((i2 & 1) != 0) {
            i = uASample.step;
        }
        if ((i2 & 2) != 0) {
            d = uASample.distance;
        }
        double d3 = d;
        if ((i2 & 4) != 0) {
            d2 = uASample.calorie;
        }
        double d4 = d2;
        if ((i2 & 8) != 0) {
            j = uASample.time;
        }
        return uASample.copy(i, d3, d4, j);
    }

    @DexIgnore
    public final int component1() {
        return this.step;
    }

    @DexIgnore
    public final double component2() {
        return this.distance;
    }

    @DexIgnore
    public final double component3() {
        return this.calorie;
    }

    @DexIgnore
    public final long component4() {
        return this.time;
    }

    @DexIgnore
    public final com.portfolio.platform.data.model.thirdparty.p005ua.UASample copy(int i, double d, double d2, long j) {
        com.portfolio.platform.data.model.thirdparty.p005ua.UASample uASample = new com.portfolio.platform.data.model.thirdparty.p005ua.UASample(i, d, d2, j);
        return uASample;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this != obj) {
            if (obj instanceof com.portfolio.platform.data.model.thirdparty.p005ua.UASample) {
                com.portfolio.platform.data.model.thirdparty.p005ua.UASample uASample = (com.portfolio.platform.data.model.thirdparty.p005ua.UASample) obj;
                if ((this.step == uASample.step) && java.lang.Double.compare(this.distance, uASample.distance) == 0 && java.lang.Double.compare(this.calorie, uASample.calorie) == 0) {
                    if (this.time == uASample.time) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final double getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public final double getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final int getId() {
        return this.f21077id;
    }

    @DexIgnore
    public final int getStep() {
        return this.step;
    }

    @DexIgnore
    public final long getTime() {
        return this.time;
    }

    @DexIgnore
    public int hashCode() {
        long doubleToLongBits = java.lang.Double.doubleToLongBits(this.distance);
        long doubleToLongBits2 = java.lang.Double.doubleToLongBits(this.calorie);
        long j = this.time;
        return (((((this.step * 31) + ((int) (doubleToLongBits ^ (doubleToLongBits >>> 32)))) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)))) * 31) + ((int) (j ^ (j >>> 32)));
    }

    @DexIgnore
    public final void setId(int i) {
        this.f21077id = i;
    }

    @DexIgnore
    public java.lang.String toString() {
        return "UASample(step=" + this.step + ", distance=" + this.distance + ", calorie=" + this.calorie + ", time=" + this.time + ")";
    }
}
