package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationActivitySetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    @f02("currentValue")
    public String currentValue;
    @DexIgnore
    @f02("isRingEnabled")
    public boolean isRingEnabled;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ComplicationActivitySetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public ComplicationActivitySetting createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new ComplicationActivitySetting(parcel);
        }

        @DexIgnore
        public ComplicationActivitySetting[] newArray(int i) {
            return new ComplicationActivitySetting[i];
        }
    }

    @DexIgnore
    public ComplicationActivitySetting() {
        this(false, 1, (fd4) null);
    }

    @DexIgnore
    public ComplicationActivitySetting(boolean z) {
        this.isRingEnabled = z;
        this.currentValue = "";
    }

    @DexIgnore
    public static /* synthetic */ ComplicationActivitySetting copy$default(ComplicationActivitySetting complicationActivitySetting, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = complicationActivitySetting.isRingEnabled;
        }
        return complicationActivitySetting.copy(z);
    }

    @DexIgnore
    public final boolean component1() {
        return this.isRingEnabled;
    }

    @DexIgnore
    public final ComplicationActivitySetting copy(boolean z) {
        return new ComplicationActivitySetting(z);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ComplicationActivitySetting) {
                if (this.isRingEnabled == ((ComplicationActivitySetting) obj).isRingEnabled) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getCurrentValue() {
        return this.currentValue;
    }

    @DexIgnore
    public int hashCode() {
        boolean z = this.isRingEnabled;
        if (z) {
            return 1;
        }
        return z ? 1 : 0;
    }

    @DexIgnore
    public final boolean isRingEnabled() {
        return this.isRingEnabled;
    }

    @DexIgnore
    public final void setCurrentValue(String str) {
        kd4.b(str, "<set-?>");
        this.currentValue = str;
    }

    @DexIgnore
    public final void setRingEnabled(boolean z) {
        this.isRingEnabled = z;
    }

    @DexIgnore
    public String toString() {
        return "ComplicationActivitySetting(isRingEnabled=" + this.isRingEnabled + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeByte(this.isRingEnabled ? (byte) 1 : 0);
        parcel.writeString(this.currentValue);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ComplicationActivitySetting(boolean z, int i, fd4 fd4) {
        this((i & 1) != 0 ? false : z);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ComplicationActivitySetting(Parcel parcel) {
        this(parcel.readByte() != ((byte) 0));
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        this.currentValue = readString == null ? "" : readString;
    }
}
