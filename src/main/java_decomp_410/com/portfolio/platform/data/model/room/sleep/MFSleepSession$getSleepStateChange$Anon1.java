package com.portfolio.platform.data.model.room.sleep;

import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFSleepSession$getSleepStateChange$Anon1 extends TypeToken<List<? extends WrapperSleepStateChange>> {
}
