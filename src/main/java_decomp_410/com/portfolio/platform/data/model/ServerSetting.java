package com.portfolio.platform.data.model;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.fd4;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@DatabaseTable(tableName = "SERVER_SETTING")
public final class ServerSetting {
    @DexIgnore
    public static /* final */ String CREATE_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String KEY; // = "key";
    @DexIgnore
    public static /* final */ String OBJECT_ID; // = "objectId";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "SERVER_SETTING";
    @DexIgnore
    public static /* final */ String UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ String VALUE; // = "value";
    @DexIgnore
    @f02("createdAt")
    @DatabaseField(columnName = "createdAt")
    public String createdAt;
    @DexIgnore
    @f02("key")
    @DatabaseField(columnName = "key", id = true)
    public String key;
    @DexIgnore
    @f02("id")
    @DatabaseField(columnName = "objectId")
    public String objectId;
    @DexIgnore
    @f02("updatedAt")
    @DatabaseField(columnName = "updatedAt")
    public String updateAt;
    @DexIgnore
    @f02("value")
    @DatabaseField(columnName = "value")
    public String value;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getKey() {
        return this.key;
    }

    @DexIgnore
    public final String getObjectId() {
        return this.objectId;
    }

    @DexIgnore
    public final String getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public final String getValue() {
        return this.value;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setKey(String str) {
        this.key = str;
    }

    @DexIgnore
    public final void setObjectId(String str) {
        this.objectId = str;
    }

    @DexIgnore
    public final void setUpdateAt(String str) {
        this.updateAt = str;
    }

    @DexIgnore
    public final void setValue(String str) {
        this.value = str;
    }

    @DexIgnore
    public String toString() {
        return "key: " + this.key + " - value: " + this.value;
    }
}
