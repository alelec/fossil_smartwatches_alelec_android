package com.portfolio.platform.data.model.diana.workout;

import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.enums.WorkoutState;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutStateChange {
    @DexIgnore
    public /* final */ WorkoutState state;
    @DexIgnore
    public /* final */ Date time;

    @DexIgnore
    public WorkoutStateChange(WorkoutState workoutState, Date date) {
        kd4.b(workoutState, "state");
        kd4.b(date, LogBuilder.KEY_TIME);
        this.state = workoutState;
        this.time = date;
    }

    @DexIgnore
    private final WorkoutState component1() {
        return this.state;
    }

    @DexIgnore
    private final Date component2() {
        return this.time;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutStateChange copy$default(WorkoutStateChange workoutStateChange, WorkoutState workoutState, Date date, int i, Object obj) {
        if ((i & 1) != 0) {
            workoutState = workoutStateChange.state;
        }
        if ((i & 2) != 0) {
            date = workoutStateChange.time;
        }
        return workoutStateChange.copy(workoutState, date);
    }

    @DexIgnore
    public final WorkoutStateChange copy(WorkoutState workoutState, Date date) {
        kd4.b(workoutState, "state");
        kd4.b(date, LogBuilder.KEY_TIME);
        return new WorkoutStateChange(workoutState, date);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutStateChange)) {
            return false;
        }
        WorkoutStateChange workoutStateChange = (WorkoutStateChange) obj;
        return kd4.a((Object) this.state, (Object) workoutStateChange.state) && kd4.a((Object) this.time, (Object) workoutStateChange.time);
    }

    @DexIgnore
    public int hashCode() {
        WorkoutState workoutState = this.state;
        int i = 0;
        int hashCode = (workoutState != null ? workoutState.hashCode() : 0) * 31;
        Date date = this.time;
        if (date != null) {
            i = date.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutStateChange(state=" + this.state + ", time=" + this.time + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutStateChange(int i, com.fossil.fitness.WorkoutStateChange workoutStateChange) {
        this(WorkoutState.Companion.a(workoutStateChange.getState().name()), new Date(((long) (i + workoutStateChange.getIndexInSecond())) * 1000));
        kd4.b(workoutStateChange, "workoutStageChange");
    }
}
