package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    @f02("commuteAddress")
    public String address;
    @DexIgnore
    @f02("commuteAvoidTolls")
    public boolean avoidTolls;
    @DexIgnore
    @f02("commuteFormat")
    public String format;
    @DexIgnore
    @f02("commuteMovement")
    public String movement;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<CommuteTimeSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public CommuteTimeSetting createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new CommuteTimeSetting(parcel);
        }

        @DexIgnore
        public CommuteTimeSetting[] newArray(int i) {
            return new CommuteTimeSetting[i];
        }
    }

    @DexIgnore
    public CommuteTimeSetting() {
        this((String) null, (String) null, false, (String) null, 15, (fd4) null);
    }

    @DexIgnore
    public CommuteTimeSetting(String str, String str2, boolean z, String str3) {
        kd4.b(str, "address");
        kd4.b(str2, "format");
        kd4.b(str3, "movement");
        this.address = str;
        this.format = str2;
        this.avoidTolls = z;
        this.movement = str3;
    }

    @DexIgnore
    public static /* synthetic */ CommuteTimeSetting copy$default(CommuteTimeSetting commuteTimeSetting, String str, String str2, boolean z, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = commuteTimeSetting.address;
        }
        if ((i & 2) != 0) {
            str2 = commuteTimeSetting.format;
        }
        if ((i & 4) != 0) {
            z = commuteTimeSetting.avoidTolls;
        }
        if ((i & 8) != 0) {
            str3 = commuteTimeSetting.movement;
        }
        return commuteTimeSetting.copy(str, str2, z, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.address;
    }

    @DexIgnore
    public final String component2() {
        return this.format;
    }

    @DexIgnore
    public final boolean component3() {
        return this.avoidTolls;
    }

    @DexIgnore
    public final String component4() {
        return this.movement;
    }

    @DexIgnore
    public final CommuteTimeSetting copy(String str, String str2, boolean z, String str3) {
        kd4.b(str, "address");
        kd4.b(str2, "format");
        kd4.b(str3, "movement");
        return new CommuteTimeSetting(str, str2, z, str3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof CommuteTimeSetting) {
                CommuteTimeSetting commuteTimeSetting = (CommuteTimeSetting) obj;
                if (kd4.a((Object) this.address, (Object) commuteTimeSetting.address) && kd4.a((Object) this.format, (Object) commuteTimeSetting.format)) {
                    if (!(this.avoidTolls == commuteTimeSetting.avoidTolls) || !kd4.a((Object) this.movement, (Object) commuteTimeSetting.movement)) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getAddress() {
        return this.address;
    }

    @DexIgnore
    public final boolean getAvoidTolls() {
        return this.avoidTolls;
    }

    @DexIgnore
    public final String getFormat() {
        return this.format;
    }

    @DexIgnore
    public final String getMovement() {
        return this.movement;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.address;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.format;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        boolean z = this.avoidTolls;
        if (z) {
            z = true;
        }
        int i2 = (hashCode2 + (z ? 1 : 0)) * 31;
        String str3 = this.movement;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return i2 + i;
    }

    @DexIgnore
    public final void setAddress(String str) {
        kd4.b(str, "<set-?>");
        this.address = str;
    }

    @DexIgnore
    public final void setAvoidTolls(boolean z) {
        this.avoidTolls = z;
    }

    @DexIgnore
    public final void setFormat(String str) {
        kd4.b(str, "<set-?>");
        this.format = str;
    }

    @DexIgnore
    public final void setMovement(String str) {
        kd4.b(str, "<set-?>");
        this.movement = str;
    }

    @DexIgnore
    public String toString() {
        return "CommuteTimeSetting(address=" + this.address + ", format=" + this.format + ", avoidTolls=" + this.avoidTolls + ", movement=" + this.movement + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(this.address);
        parcel.writeString(this.format);
        parcel.writeByte(this.avoidTolls ? (byte) 1 : 0);
        parcel.writeString(this.movement);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CommuteTimeSetting(String str, String str2, boolean z, String str3, int i, fd4 fd4) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? "travel" : str2, (i & 4) != 0 ? true : z, (i & 8) != 0 ? "car" : str3);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public CommuteTimeSetting(Parcel parcel) {
        throw null;
/*        this(r0, r2, r4, r7 == null ? "" : r7);
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        readString = readString == null ? "" : readString;
        String readString2 = parcel.readString();
        readString2 = readString2 == null ? "" : readString2;
        boolean z = parcel.readByte() != ((byte) 0);
        String readString3 = parcel.readString();
*/    }
}
