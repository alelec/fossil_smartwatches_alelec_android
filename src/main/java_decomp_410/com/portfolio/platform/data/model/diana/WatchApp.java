package com.portfolio.platform.data.model.diana;

import com.fossil.blesdk.obfuscated.d02;
import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchApp {
    @DexIgnore
    @f02("categoryIds")
    @d02
    public ArrayList<String> categories;
    @DexIgnore
    @f02("createdAt")
    @d02
    public String createdAt;
    @DexIgnore
    @f02("englishDescription")
    @d02
    public String description;
    @DexIgnore
    @f02("description")
    @d02
    public String descriptionKey;
    @DexIgnore
    @d02
    public String icon;
    @DexIgnore
    @f02("englishName")
    @d02
    public String name;
    @DexIgnore
    @f02("name")
    @d02
    public String nameKey;
    @DexIgnore
    @f02("updatedAt")
    @d02
    public String updatedAt;
    @DexIgnore
    @f02("id")
    @d02
    public String watchappId;

    @DexIgnore
    public WatchApp(String str, String str2, String str3, String str4, String str5, ArrayList<String> arrayList, String str6, String str7, String str8) {
        kd4.b(str, "watchappId");
        kd4.b(str2, "name");
        kd4.b(str3, "nameKey");
        kd4.b(str4, "description");
        kd4.b(str5, "descriptionKey");
        kd4.b(arrayList, "categories");
        kd4.b(str7, "updatedAt");
        kd4.b(str8, "createdAt");
        this.watchappId = str;
        this.name = str2;
        this.nameKey = str3;
        this.description = str4;
        this.descriptionKey = str5;
        this.categories = arrayList;
        this.icon = str6;
        this.updatedAt = str7;
        this.createdAt = str8;
    }

    @DexIgnore
    public static /* synthetic */ WatchApp copy$default(WatchApp watchApp, String str, String str2, String str3, String str4, String str5, ArrayList arrayList, String str6, String str7, String str8, int i, Object obj) {
        WatchApp watchApp2 = watchApp;
        int i2 = i;
        return watchApp.copy((i2 & 1) != 0 ? watchApp2.watchappId : str, (i2 & 2) != 0 ? watchApp2.name : str2, (i2 & 4) != 0 ? watchApp2.nameKey : str3, (i2 & 8) != 0 ? watchApp2.description : str4, (i2 & 16) != 0 ? watchApp2.descriptionKey : str5, (i2 & 32) != 0 ? watchApp2.categories : arrayList, (i2 & 64) != 0 ? watchApp2.icon : str6, (i2 & 128) != 0 ? watchApp2.updatedAt : str7, (i2 & 256) != 0 ? watchApp2.createdAt : str8);
    }

    @DexIgnore
    public final String component1() {
        return this.watchappId;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final String component3() {
        return this.nameKey;
    }

    @DexIgnore
    public final String component4() {
        return this.description;
    }

    @DexIgnore
    public final String component5() {
        return this.descriptionKey;
    }

    @DexIgnore
    public final ArrayList<String> component6() {
        return this.categories;
    }

    @DexIgnore
    public final String component7() {
        return this.icon;
    }

    @DexIgnore
    public final String component8() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String component9() {
        return this.createdAt;
    }

    @DexIgnore
    public final WatchApp copy(String str, String str2, String str3, String str4, String str5, ArrayList<String> arrayList, String str6, String str7, String str8) {
        kd4.b(str, "watchappId");
        kd4.b(str2, "name");
        kd4.b(str3, "nameKey");
        kd4.b(str4, "description");
        String str9 = str5;
        kd4.b(str9, "descriptionKey");
        ArrayList<String> arrayList2 = arrayList;
        kd4.b(arrayList2, "categories");
        String str10 = str7;
        kd4.b(str10, "updatedAt");
        String str11 = str8;
        kd4.b(str11, "createdAt");
        return new WatchApp(str, str2, str3, str4, str9, arrayList2, str6, str10, str11);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WatchApp)) {
            return false;
        }
        WatchApp watchApp = (WatchApp) obj;
        return kd4.a((Object) this.watchappId, (Object) watchApp.watchappId) && kd4.a((Object) this.name, (Object) watchApp.name) && kd4.a((Object) this.nameKey, (Object) watchApp.nameKey) && kd4.a((Object) this.description, (Object) watchApp.description) && kd4.a((Object) this.descriptionKey, (Object) watchApp.descriptionKey) && kd4.a((Object) this.categories, (Object) watchApp.categories) && kd4.a((Object) this.icon, (Object) watchApp.icon) && kd4.a((Object) this.updatedAt, (Object) watchApp.updatedAt) && kd4.a((Object) this.createdAt, (Object) watchApp.createdAt);
    }

    @DexIgnore
    public final ArrayList<String> getCategories() {
        return this.categories;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getDescription() {
        return this.description;
    }

    @DexIgnore
    public final String getDescriptionKey() {
        return this.descriptionKey;
    }

    @DexIgnore
    public final String getIcon() {
        return this.icon;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getNameKey() {
        return this.nameKey;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getWatchappId() {
        return this.watchappId;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.watchappId;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.nameKey;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.description;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.descriptionKey;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        ArrayList<String> arrayList = this.categories;
        int hashCode6 = (hashCode5 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        String str6 = this.icon;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.updatedAt;
        int hashCode8 = (hashCode7 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.createdAt;
        if (str8 != null) {
            i = str8.hashCode();
        }
        return hashCode8 + i;
    }

    @DexIgnore
    public final void setCategories(ArrayList<String> arrayList) {
        kd4.b(arrayList, "<set-?>");
        this.categories = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        kd4.b(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDescription(String str) {
        kd4.b(str, "<set-?>");
        this.description = str;
    }

    @DexIgnore
    public final void setDescriptionKey(String str) {
        kd4.b(str, "<set-?>");
        this.descriptionKey = str;
    }

    @DexIgnore
    public final void setIcon(String str) {
        this.icon = str;
    }

    @DexIgnore
    public final void setName(String str) {
        kd4.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setNameKey(String str) {
        kd4.b(str, "<set-?>");
        this.nameKey = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        kd4.b(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setWatchappId(String str) {
        kd4.b(str, "<set-?>");
        this.watchappId = str;
    }

    @DexIgnore
    public String toString() {
        return "WatchApp(watchappId=" + this.watchappId + ", name=" + this.name + ", nameKey=" + this.nameKey + ", description=" + this.description + ", descriptionKey=" + this.descriptionKey + ", categories=" + this.categories + ", icon=" + this.icon + ", updatedAt=" + this.updatedAt + ", createdAt=" + this.createdAt + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WatchApp(String str, String str2, String str3, String str4, String str5, ArrayList arrayList, String str6, String str7, String str8, int i, fd4 fd4) {
        this(str, str2, str3, str4, str5, arrayList, (i & 64) != 0 ? "" : str6, str7, str8);
    }
}
