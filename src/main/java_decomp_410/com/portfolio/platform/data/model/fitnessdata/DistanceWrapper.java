package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.fitness.Distance;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DistanceWrapper {
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public double total;
    @DexIgnore
    public List<Double> values;

    @DexIgnore
    public DistanceWrapper(int i, List<Double> list, double d) {
        kd4.b(list, "values");
        this.resolutionInSecond = i;
        this.values = list;
        this.total = d;
    }

    @DexIgnore
    public static /* synthetic */ DistanceWrapper copy$default(DistanceWrapper distanceWrapper, int i, List<Double> list, double d, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = distanceWrapper.resolutionInSecond;
        }
        if ((i2 & 2) != 0) {
            list = distanceWrapper.values;
        }
        if ((i2 & 4) != 0) {
            d = distanceWrapper.total;
        }
        return distanceWrapper.copy(i, list, d);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Double> component2() {
        return this.values;
    }

    @DexIgnore
    public final double component3() {
        return this.total;
    }

    @DexIgnore
    public final DistanceWrapper copy(int i, List<Double> list, double d) {
        kd4.b(list, "values");
        return new DistanceWrapper(i, list, d);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DistanceWrapper) {
                DistanceWrapper distanceWrapper = (DistanceWrapper) obj;
                if (!(this.resolutionInSecond == distanceWrapper.resolutionInSecond) || !kd4.a((Object) this.values, (Object) distanceWrapper.values) || Double.compare(this.total, distanceWrapper.total) != 0) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final double getTotal() {
        return this.total;
    }

    @DexIgnore
    public final List<Double> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.resolutionInSecond * 31;
        List<Double> list = this.values;
        int hashCode = list != null ? list.hashCode() : 0;
        long doubleToLongBits = Double.doubleToLongBits(this.total);
        return ((i + hashCode) * 31) + ((int) (doubleToLongBits ^ (doubleToLongBits >>> 32)));
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setTotal(double d) {
        this.total = d;
    }

    @DexIgnore
    public final void setValues(List<Double> list) {
        kd4.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "DistanceWrapper(resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ", total=" + this.total + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public DistanceWrapper(Distance distance) {
        throw null;
/*        this(r0, r1, distance.getTotal());
        kd4.b(distance, "distance");
        int resolutionInSecond2 = distance.getResolutionInSecond();
        ArrayList<Double> values2 = distance.getValues();
        kd4.a((Object) values2, "distance.values");
*/    }
}
