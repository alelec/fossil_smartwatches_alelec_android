package com.portfolio.platform.data.model.microapp.weather;

import com.fossil.blesdk.obfuscated.f02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class Temperature {
    @DexIgnore
    @f02("currently")
    public float currently;
    @DexIgnore
    @f02("max")
    public float max;
    @DexIgnore
    @f02("min")
    public float min;
    @DexIgnore
    @f02("unit")
    public String unit;

    @DexIgnore
    public float getCurrently() {
        return this.currently;
    }

    @DexIgnore
    public float getMax() {
        return this.max;
    }

    @DexIgnore
    public float getMin() {
        return this.min;
    }

    @DexIgnore
    public String getUnit() {
        return this.unit;
    }
}
