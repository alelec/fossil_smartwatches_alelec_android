package com.portfolio.platform.data.model.diana.workout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Location {
    @DexIgnore
    public /* final */ long latitude;
    @DexIgnore
    public /* final */ long longitude;

    @DexIgnore
    public Location(long j, long j2) {
        this.latitude = j;
        this.longitude = j2;
    }

    @DexIgnore
    private final long component1() {
        return this.latitude;
    }

    @DexIgnore
    private final long component2() {
        return this.longitude;
    }

    @DexIgnore
    public static /* synthetic */ Location copy$default(Location location, long j, long j2, int i, Object obj) {
        if ((i & 1) != 0) {
            j = location.latitude;
        }
        if ((i & 2) != 0) {
            j2 = location.longitude;
        }
        return location.copy(j, j2);
    }

    @DexIgnore
    public final Location copy(long j, long j2) {
        return new Location(j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Location) {
                Location location = (Location) obj;
                if (this.latitude == location.latitude) {
                    if (this.longitude == location.longitude) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.latitude;
        long j2 = this.longitude;
        return (((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "Location(latitude=" + this.latitude + ", longitude=" + this.longitude + ")";
    }
}
