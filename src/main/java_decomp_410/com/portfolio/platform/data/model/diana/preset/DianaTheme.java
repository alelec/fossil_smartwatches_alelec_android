package com.portfolio.platform.data.model.diana.preset;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaTheme {
    @DexIgnore
    @f02("watchFaceId")
    public String backgroundId;
    @DexIgnore
    @f02("ringStyleId")
    public String ringStyleId;
    @DexIgnore
    @f02("themeId")
    public String themeId;
    @DexIgnore
    @f02("wallpaperId")
    public String wallpaperId;

    @DexIgnore
    public DianaTheme(String str, String str2, String str3, String str4) {
        kd4.b(str, "backgroundId");
        kd4.b(str2, "ringStyleId");
        kd4.b(str3, "themeId");
        kd4.b(str4, "wallpaperId");
        this.backgroundId = str;
        this.ringStyleId = str2;
        this.themeId = str3;
        this.wallpaperId = str4;
    }

    @DexIgnore
    public static /* synthetic */ DianaTheme copy$default(DianaTheme dianaTheme, String str, String str2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = dianaTheme.backgroundId;
        }
        if ((i & 2) != 0) {
            str2 = dianaTheme.ringStyleId;
        }
        if ((i & 4) != 0) {
            str3 = dianaTheme.themeId;
        }
        if ((i & 8) != 0) {
            str4 = dianaTheme.wallpaperId;
        }
        return dianaTheme.copy(str, str2, str3, str4);
    }

    @DexIgnore
    public final String component1() {
        return this.backgroundId;
    }

    @DexIgnore
    public final String component2() {
        return this.ringStyleId;
    }

    @DexIgnore
    public final String component3() {
        return this.themeId;
    }

    @DexIgnore
    public final String component4() {
        return this.wallpaperId;
    }

    @DexIgnore
    public final DianaTheme copy(String str, String str2, String str3, String str4) {
        kd4.b(str, "backgroundId");
        kd4.b(str2, "ringStyleId");
        kd4.b(str3, "themeId");
        kd4.b(str4, "wallpaperId");
        return new DianaTheme(str, str2, str3, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DianaTheme)) {
            return false;
        }
        DianaTheme dianaTheme = (DianaTheme) obj;
        return kd4.a((Object) this.backgroundId, (Object) dianaTheme.backgroundId) && kd4.a((Object) this.ringStyleId, (Object) dianaTheme.ringStyleId) && kd4.a((Object) this.themeId, (Object) dianaTheme.themeId) && kd4.a((Object) this.wallpaperId, (Object) dianaTheme.wallpaperId);
    }

    @DexIgnore
    public final String getBackgroundId() {
        return this.backgroundId;
    }

    @DexIgnore
    public final String getRingStyleId() {
        return this.ringStyleId;
    }

    @DexIgnore
    public final String getThemeId() {
        return this.themeId;
    }

    @DexIgnore
    public final String getWallpaperId() {
        return this.wallpaperId;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.backgroundId;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.ringStyleId;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.themeId;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.wallpaperId;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public final void setBackgroundId(String str) {
        kd4.b(str, "<set-?>");
        this.backgroundId = str;
    }

    @DexIgnore
    public final void setRingStyleId(String str) {
        kd4.b(str, "<set-?>");
        this.ringStyleId = str;
    }

    @DexIgnore
    public final void setThemeId(String str) {
        kd4.b(str, "<set-?>");
        this.themeId = str;
    }

    @DexIgnore
    public final void setWallpaperId(String str) {
        kd4.b(str, "<set-?>");
        this.wallpaperId = str;
    }

    @DexIgnore
    public String toString() {
        return "DianaTheme(backgroundId=" + this.backgroundId + ", ringStyleId=" + this.ringStyleId + ", themeId=" + this.themeId + ", wallpaperId=" + this.wallpaperId + ")";
    }

    @DexIgnore
    public DianaTheme() {
        this("watch_face_1", "ringStyle1", "dark", "wallpaper1");
    }
}
