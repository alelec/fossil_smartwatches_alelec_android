package com.portfolio.platform.data.model.room.fitness;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.ik2;
import java.util.Date;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitySettings {
    @DexIgnore
    @f02("currentGoalActiveTime")
    public int currentActiveTimeGoal;
    @DexIgnore
    @f02("currentGoalCalories")
    public int currentCaloriesGoal;
    @DexIgnore
    @f02("currentGoalSteps")
    public int currentStepGoal;
    @DexIgnore
    @ik2
    public int id;
    @DexIgnore
    @f02("timezoneOffset")
    public Integer timezone; // = Integer.valueOf(TimeZone.getDefault().getOffset(new Date().getTime()) / 1000);

    @DexIgnore
    public ActivitySettings(int i, int i2, int i3) {
        this.currentStepGoal = i;
        this.currentCaloriesGoal = i2;
        this.currentActiveTimeGoal = i3;
    }

    @DexIgnore
    public static /* synthetic */ ActivitySettings copy$default(ActivitySettings activitySettings, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 1) != 0) {
            i = activitySettings.currentStepGoal;
        }
        if ((i4 & 2) != 0) {
            i2 = activitySettings.currentCaloriesGoal;
        }
        if ((i4 & 4) != 0) {
            i3 = activitySettings.currentActiveTimeGoal;
        }
        return activitySettings.copy(i, i2, i3);
    }

    @DexIgnore
    public final int component1() {
        return this.currentStepGoal;
    }

    @DexIgnore
    public final int component2() {
        return this.currentCaloriesGoal;
    }

    @DexIgnore
    public final int component3() {
        return this.currentActiveTimeGoal;
    }

    @DexIgnore
    public final ActivitySettings copy(int i, int i2, int i3) {
        return new ActivitySettings(i, i2, i3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ActivitySettings) {
                ActivitySettings activitySettings = (ActivitySettings) obj;
                if (this.currentStepGoal == activitySettings.currentStepGoal) {
                    if (this.currentCaloriesGoal == activitySettings.currentCaloriesGoal) {
                        if (this.currentActiveTimeGoal == activitySettings.currentActiveTimeGoal) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getCurrentActiveTimeGoal() {
        return this.currentActiveTimeGoal;
    }

    @DexIgnore
    public final int getCurrentCaloriesGoal() {
        return this.currentCaloriesGoal;
    }

    @DexIgnore
    public final int getCurrentStepGoal() {
        return this.currentStepGoal;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final Integer getTimezone() {
        return this.timezone;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.currentStepGoal * 31) + this.currentCaloriesGoal) * 31) + this.currentActiveTimeGoal;
    }

    @DexIgnore
    public final void setCurrentActiveTimeGoal(int i) {
        this.currentActiveTimeGoal = i;
    }

    @DexIgnore
    public final void setCurrentCaloriesGoal(int i) {
        this.currentCaloriesGoal = i;
    }

    @DexIgnore
    public final void setCurrentStepGoal(int i) {
        this.currentStepGoal = i;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setTimezone(Integer num) {
        this.timezone = num;
    }

    @DexIgnore
    public String toString() {
        return "ActivitySettings(currentStepGoal=" + this.currentStepGoal + ", currentCaloriesGoal=" + this.currentCaloriesGoal + ", currentActiveTimeGoal=" + this.currentActiveTimeGoal + ")";
    }
}
