package com.portfolio.platform.data.model.thirdparty.googlefit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GFitWOHeartRate {
    @DexIgnore
    public long endTime;
    @DexIgnore
    public float heartRate;
    @DexIgnore
    public long startTime;

    @DexIgnore
    public GFitWOHeartRate(float f, long j, long j2) {
        this.heartRate = f;
        this.startTime = j;
        this.endTime = j2;
    }

    @DexIgnore
    public final long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final float getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public final void setHeartRate(float f) {
        this.heartRate = f;
    }

    @DexIgnore
    public final void setStartTime(long j) {
        this.startTime = j;
    }
}
