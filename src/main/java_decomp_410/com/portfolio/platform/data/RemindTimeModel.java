package com.portfolio.platform.data;

import com.fossil.blesdk.obfuscated.d02;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RemindTimeModel {
    @DexIgnore
    @d02
    public int minutes;
    @DexIgnore
    @d02
    public String remindTimeName;

    @DexIgnore
    public RemindTimeModel(String str, int i) {
        kd4.b(str, "remindTimeName");
        this.remindTimeName = str;
        this.minutes = i;
    }

    @DexIgnore
    public final int getMinutes() {
        return this.minutes;
    }

    @DexIgnore
    public final String getRemindTimeName() {
        return this.remindTimeName;
    }

    @DexIgnore
    public final void setMinutes(int i) {
        this.minutes = i;
    }

    @DexIgnore
    public final void setRemindTimeName(String str) {
        kd4.b(str, "<set-?>");
        this.remindTimeName = str;
    }
}
