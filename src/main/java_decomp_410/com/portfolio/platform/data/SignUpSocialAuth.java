package com.portfolio.platform.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SignUpSocialAuth implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    @f02("birthday")
    public String birthday;
    @DexIgnore
    @f02("clientId")
    public String clientId;
    @DexIgnore
    @f02("diagnosticEnabled")
    public boolean diagnosticEnabled;
    @DexIgnore
    @f02("email")
    public String email;
    @DexIgnore
    @f02("firstName")
    public String firstName;
    @DexIgnore
    @f02("gender")
    public String gender;
    @DexIgnore
    @f02("lastName")
    public String lastName;
    @DexIgnore
    @f02("service")
    public String service;
    @DexIgnore
    @f02("token")
    public String token;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SignUpSocialAuth> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public SignUpSocialAuth createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new SignUpSocialAuth(parcel);
        }

        @DexIgnore
        public SignUpSocialAuth[] newArray(int i) {
            return new SignUpSocialAuth[i];
        }
    }

    @DexIgnore
    public SignUpSocialAuth(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z) {
        kd4.b(str, "email");
        kd4.b(str2, Constants.SERVICE);
        kd4.b(str3, "token");
        kd4.b(str4, "clientId");
        kd4.b(str5, "firstName");
        kd4.b(str6, "lastName");
        kd4.b(str7, "birthday");
        kd4.b(str8, "gender");
        this.email = str;
        this.service = str2;
        this.token = str3;
        this.clientId = str4;
        this.firstName = str5;
        this.lastName = str6;
        this.birthday = str7;
        this.gender = str8;
        this.diagnosticEnabled = z;
    }

    @DexIgnore
    public static /* synthetic */ SignUpSocialAuth copy$default(SignUpSocialAuth signUpSocialAuth, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z, int i, Object obj) {
        SignUpSocialAuth signUpSocialAuth2 = signUpSocialAuth;
        int i2 = i;
        return signUpSocialAuth.copy((i2 & 1) != 0 ? signUpSocialAuth2.email : str, (i2 & 2) != 0 ? signUpSocialAuth2.service : str2, (i2 & 4) != 0 ? signUpSocialAuth2.token : str3, (i2 & 8) != 0 ? signUpSocialAuth2.clientId : str4, (i2 & 16) != 0 ? signUpSocialAuth2.firstName : str5, (i2 & 32) != 0 ? signUpSocialAuth2.lastName : str6, (i2 & 64) != 0 ? signUpSocialAuth2.birthday : str7, (i2 & 128) != 0 ? signUpSocialAuth2.gender : str8, (i2 & 256) != 0 ? signUpSocialAuth2.diagnosticEnabled : z);
    }

    @DexIgnore
    public final String component1() {
        return this.email;
    }

    @DexIgnore
    public final String component2() {
        return this.service;
    }

    @DexIgnore
    public final String component3() {
        return this.token;
    }

    @DexIgnore
    public final String component4() {
        return this.clientId;
    }

    @DexIgnore
    public final String component5() {
        return this.firstName;
    }

    @DexIgnore
    public final String component6() {
        return this.lastName;
    }

    @DexIgnore
    public final String component7() {
        return this.birthday;
    }

    @DexIgnore
    public final String component8() {
        return this.gender;
    }

    @DexIgnore
    public final boolean component9() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public final SignUpSocialAuth copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z) {
        kd4.b(str, "email");
        kd4.b(str2, Constants.SERVICE);
        kd4.b(str3, "token");
        kd4.b(str4, "clientId");
        String str9 = str5;
        kd4.b(str9, "firstName");
        String str10 = str6;
        kd4.b(str10, "lastName");
        String str11 = str7;
        kd4.b(str11, "birthday");
        String str12 = str8;
        kd4.b(str12, "gender");
        return new SignUpSocialAuth(str, str2, str3, str4, str9, str10, str11, str12, z);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SignUpSocialAuth) {
                SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) obj;
                if (kd4.a((Object) this.email, (Object) signUpSocialAuth.email) && kd4.a((Object) this.service, (Object) signUpSocialAuth.service) && kd4.a((Object) this.token, (Object) signUpSocialAuth.token) && kd4.a((Object) this.clientId, (Object) signUpSocialAuth.clientId) && kd4.a((Object) this.firstName, (Object) signUpSocialAuth.firstName) && kd4.a((Object) this.lastName, (Object) signUpSocialAuth.lastName) && kd4.a((Object) this.birthday, (Object) signUpSocialAuth.birthday) && kd4.a((Object) this.gender, (Object) signUpSocialAuth.gender)) {
                    if (this.diagnosticEnabled == signUpSocialAuth.diagnosticEnabled) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getBirthday() {
        return this.birthday;
    }

    @DexIgnore
    public final String getClientId() {
        return this.clientId;
    }

    @DexIgnore
    public final boolean getDiagnosticEnabled() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public final String getEmail() {
        return this.email;
    }

    @DexIgnore
    public final String getFirstName() {
        return this.firstName;
    }

    @DexIgnore
    public final String getGender() {
        return this.gender;
    }

    @DexIgnore
    public final String getLastName() {
        return this.lastName;
    }

    @DexIgnore
    public final String getService() {
        return this.service;
    }

    @DexIgnore
    public final String getToken() {
        return this.token;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.email;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.service;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.token;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.clientId;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.firstName;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.lastName;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.birthday;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.gender;
        if (str8 != null) {
            i = str8.hashCode();
        }
        int i2 = (hashCode7 + i) * 31;
        boolean z = this.diagnosticEnabled;
        if (z) {
            z = true;
        }
        return i2 + (z ? 1 : 0);
    }

    @DexIgnore
    public final void setBirthday(String str) {
        kd4.b(str, "<set-?>");
        this.birthday = str;
    }

    @DexIgnore
    public final void setClientId(String str) {
        kd4.b(str, "<set-?>");
        this.clientId = str;
    }

    @DexIgnore
    public final void setDiagnosticEnabled(boolean z) {
        this.diagnosticEnabled = z;
    }

    @DexIgnore
    public final void setEmail(String str) {
        kd4.b(str, "<set-?>");
        this.email = str;
    }

    @DexIgnore
    public final void setFirstName(String str) {
        kd4.b(str, "<set-?>");
        this.firstName = str;
    }

    @DexIgnore
    public final void setGender(String str) {
        kd4.b(str, "<set-?>");
        this.gender = str;
    }

    @DexIgnore
    public final void setLastName(String str) {
        kd4.b(str, "<set-?>");
        this.lastName = str;
    }

    @DexIgnore
    public final void setService(String str) {
        kd4.b(str, "<set-?>");
        this.service = str;
    }

    @DexIgnore
    public final void setToken(String str) {
        kd4.b(str, "<set-?>");
        this.token = str;
    }

    @DexIgnore
    public String toString() {
        return "SignUpSocialAuth(email=" + this.email + ", service=" + this.service + ", token=" + this.token + ", clientId=" + this.clientId + ", firstName=" + this.firstName + ", lastName=" + this.lastName + ", birthday=" + this.birthday + ", gender=" + this.gender + ", diagnosticEnabled=" + this.diagnosticEnabled + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(this.email);
        parcel.writeString(this.service);
        parcel.writeString(this.token);
        parcel.writeString(this.clientId);
        parcel.writeString(this.firstName);
        parcel.writeString(this.lastName);
        parcel.writeString(this.birthday);
        parcel.writeString(this.gender);
        parcel.writeByte(this.diagnosticEnabled ? (byte) 1 : 0);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public SignUpSocialAuth(Parcel parcel) {
        throw null;
/*        this(r3, r4, r5, r6, r7, r8, r9, r10, parcel.readByte() != ((byte) 0));
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        String str8 = readString != null ? readString : "";
        String readString2 = parcel.readString();
        if (readString2 != null) {
            str = readString2;
        } else {
            str = "";
        }
        String readString3 = parcel.readString();
        if (readString3 != null) {
            str2 = readString3;
        } else {
            str2 = "";
        }
        String readString4 = parcel.readString();
        if (readString4 != null) {
            str3 = readString4;
        } else {
            str3 = "";
        }
        String readString5 = parcel.readString();
        if (readString5 != null) {
            str4 = readString5;
        } else {
            str4 = "";
        }
        String readString6 = parcel.readString();
        if (readString6 != null) {
            str5 = readString6;
        } else {
            str5 = "";
        }
        String readString7 = parcel.readString();
        if (readString7 != null) {
            str6 = readString7;
        } else {
            str6 = "";
        }
        String readString8 = parcel.readString();
        if (readString8 != null) {
            str7 = readString8;
        } else {
            str7 = "";
        }
    }

    @DexIgnore
    public SignUpSocialAuth() {
        this("", "", "", "", "", "", "", "", false);
*/    }
}
