package com.portfolio.platform.viewmodel;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.DebugFirmwareData;
import com.portfolio.platform.data.model.Firmware;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FirmwareDebugViewModel extends ic {
    @DexIgnore
    public /* final */ MutableLiveData<List<DebugFirmwareData>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Firmware> d; // = new MutableLiveData<>();

    @DexIgnore
    public final fi4 a(xc4<? super yb4<? super List<DebugFirmwareData>>, ? extends Object> xc4) {
        kd4.b(xc4, "block");
        return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new FirmwareDebugViewModel$loadFirmware$Anon1(this, xc4, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final MutableLiveData<List<DebugFirmwareData>> c() {
        return this.c;
    }

    @DexIgnore
    public final MutableLiveData<Firmware> d() {
        return this.d;
    }

    @DexIgnore
    public final boolean e() {
        if (this.c.a() != null) {
            List<DebugFirmwareData> a = this.c.a();
            if (a != null) {
                kd4.a((Object) a, "firmwares.value!!");
                if (!a.isEmpty()) {
                    return true;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
        return false;
    }

    @DexIgnore
    public final void f() {
        MutableLiveData<List<DebugFirmwareData>> mutableLiveData = this.c;
        mutableLiveData.a(mutableLiveData.a());
        MutableLiveData<Firmware> mutableLiveData2 = this.d;
        mutableLiveData2.a(mutableLiveData2.a());
    }

    @DexIgnore
    public final void a(Firmware firmware) {
        kd4.b(firmware, "firmware");
        this.d.a(firmware);
    }
}
