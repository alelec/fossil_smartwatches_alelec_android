package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.vy2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.util.NotificationAppHelper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetNotificationUseCase extends CoroutineUseCase<SetNotificationUseCase.b, SetNotificationUseCase.a, SetNotificationUseCase.c> {
    @DexIgnore
    public /* final */ vy2 d;
    @DexIgnore
    public /* final */ px2 e;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase f;
    @DexIgnore
    public /* final */ NotificationsRepository g;
    @DexIgnore
    public /* final */ DeviceRepository h;
    @DexIgnore
    public /* final */ en2 i;

    @DexIgnore
    public static final class a implements CoroutineUseCase.d {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            kd4.b(str, "deviceId");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public c(String str) {
            kd4.b(str, "message");
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public SetNotificationUseCase(vy2 vy2, px2 px2, NotificationSettingsDatabase notificationSettingsDatabase, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, en2 en2) {
        kd4.b(vy2, "mGetApp");
        kd4.b(px2, "mGetAllContactGroups");
        kd4.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        kd4.b(notificationsRepository, "mNotificationsRepository");
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(en2, "mSharePref");
        this.d = vy2;
        this.e = px2;
        this.f = notificationSettingsDatabase;
        this.g = notificationsRepository;
        this.h = deviceRepository;
        this.i = en2;
    }

    @DexIgnore
    public String c() {
        return "SetNotificationUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public Object a(b bVar, yb4<Object> yb4) {
        SetNotificationUseCase$run$Anon1 setNotificationUseCase$run$Anon1;
        int i2;
        String str = null;
        AppNotificationFilterSettings appNotificationFilterSettings;
        Object obj = null;
        if (yb4 instanceof SetNotificationUseCase$run$Anon1) {
            setNotificationUseCase$run$Anon1 = (SetNotificationUseCase$run$Anon1) yb4;
            int i3 = setNotificationUseCase$run$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                setNotificationUseCase$run$Anon1.label = i3 - Integer.MIN_VALUE;
                SetNotificationUseCase$run$Anon1 setNotificationUseCase$run$Anon12 = setNotificationUseCase$run$Anon1;
                Object obj2 = setNotificationUseCase$run$Anon12.result;
                Object a2 = cc4.a();
                i2 = setNotificationUseCase$run$Anon12.label;
                if (i2 != 0) {
                    na4.a(obj2);
                    if (bVar == null || (qf4.a(bVar.a())) == 0) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("SetNotificationUseCase", "Invalid request: Request = " + bVar);
                        return new c("Invalid request");
                    }
                    str = bVar.a();
                    if (FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
                        NotificationAppHelper notificationAppHelper = NotificationAppHelper.b;
                        vy2 vy2 = this.d;
                        px2 px2 = this.e;
                        NotificationSettingsDatabase notificationSettingsDatabase = this.f;
                        en2 en2 = this.i;
                        setNotificationUseCase$run$Anon12.L$Anon0 = this;
                        setNotificationUseCase$run$Anon12.L$Anon1 = bVar;
                        setNotificationUseCase$run$Anon12.L$Anon2 = str;
                        setNotificationUseCase$run$Anon12.label = 1;
                        obj = notificationAppHelper.a(vy2, px2, notificationSettingsDatabase, en2, setNotificationUseCase$run$Anon12);
                        if (obj == a2) {
                            return a2;
                        }
                    } else {
                        appNotificationFilterSettings = NotificationAppHelper.b.a(this.g.getAllNotificationsByHour(str, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()), NotificationAppHelper.b.a(this.h.getSkuModelBySerialPrefix(DeviceHelper.o.b(str)), str));
                        PortfolioApp.W.c().a(appNotificationFilterSettings, str);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("SetNotificationUseCase", "saveNotificationSettingToDevice, total: " + appNotificationFilterSettings.getNotificationFilters().size() + " items");
                        return new Object();
                    }
                } else if (i2 == 1) {
                    b bVar2 = (b) setNotificationUseCase$run$Anon12.L$Anon1;
                    SetNotificationUseCase setNotificationUseCase = (SetNotificationUseCase) setNotificationUseCase$run$Anon12.L$Anon0;
                    na4.a(obj2);
                    Object obj3 = obj2;
                    str = (String) setNotificationUseCase$run$Anon12.L$Anon2;
                    obj = obj3;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                appNotificationFilterSettings = new AppNotificationFilterSettings((List) obj, System.currentTimeMillis());
                PortfolioApp.W.c().a(appNotificationFilterSettings, str);
                ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                local22.d("SetNotificationUseCase", "saveNotificationSettingToDevice, total: " + appNotificationFilterSettings.getNotificationFilters().size() + " items");
                return new Object();
            }
        }
        setNotificationUseCase$run$Anon1 = new SetNotificationUseCase$run$Anon1(this, yb4);
        SetNotificationUseCase$run$Anon1 setNotificationUseCase$run$Anon122 = setNotificationUseCase$run$Anon1;
        Object obj22 = setNotificationUseCase$run$Anon122.result;
        Object a22 = cc4.a();
        i2 = setNotificationUseCase$run$Anon122.label;
        if (i2 != 0) {
        }
        appNotificationFilterSettings = new AppNotificationFilterSettings((List) obj, System.currentTimeMillis());
        PortfolioApp.W.c().a(appNotificationFilterSettings, str);
        ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
        local222.d("SetNotificationUseCase", "saveNotificationSettingToDevice, total: " + appNotificationFilterSettings.getNotificationFilters().size() + " items");
        return new Object();
    }
}
