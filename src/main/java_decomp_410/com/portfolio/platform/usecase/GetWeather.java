package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.i02;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetWeather extends i62<GetWeather.c, GetWeather.d, GetWeather.b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((fd4) null);
    @DexIgnore
    public /* final */ ApiServiceV2 d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return GetWeather.e;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements i62.a {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            kd4.b(str, "errorMessage");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements i62.b {
        @DexIgnore
        public /* final */ WeatherSettings.TEMP_UNIT a;
        @DexIgnore
        public /* final */ LatLng b;

        @DexIgnore
        public c(LatLng latLng, WeatherSettings.TEMP_UNIT temp_unit) {
            kd4.b(latLng, "latLng");
            kd4.b(temp_unit, "tempUnit");
            i02.a(temp_unit);
            kd4.a((Object) temp_unit, "checkNotNull(tempUnit)");
            this.a = temp_unit;
            i02.a(latLng);
            kd4.a((Object) latLng, "checkNotNull(latLng)");
            this.b = latLng;
        }

        @DexIgnore
        public final LatLng a() {
            return this.b;
        }

        @DexIgnore
        public final WeatherSettings.TEMP_UNIT b() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements i62.c {
        @DexIgnore
        public /* final */ Weather a;

        @DexIgnore
        public d(Weather weather) {
            kd4.b(weather, "weather");
            this.a = weather;
        }

        @DexIgnore
        public final Weather a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = GetWeather.class.getSimpleName();
        kd4.a((Object) simpleName, "GetWeather::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public GetWeather(ApiServiceV2 apiServiceV2) {
        kd4.b(apiServiceV2, "mApiServiceV2");
        this.d = apiServiceV2;
    }

    @DexIgnore
    public void a(c cVar) {
        throw null;
        // kd4.b(cVar, "requestValues");
        // FLogger.INSTANCE.getLocal().d(e, "executeUseCase");
        // fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new GetWeather$executeUseCase$Anon1(this, cVar, (yb4) null), 3, (Object) null);
    }
}
