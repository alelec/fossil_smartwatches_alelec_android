package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.vo2;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.enums.SyncErrorCode;
import com.portfolio.platform.response.ResponseKt;
import com.portfolio.platform.usecase.GetWeather;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.usecase.GetWeather$executeUseCase$Anon1", f = "GetWeather.kt", l = {31}, m = "invokeSuspend")
public final class GetWeather$executeUseCase$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GetWeather.c $requestValues;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GetWeather this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetWeather$executeUseCase$Anon1(GetWeather getWeather, GetWeather.c cVar, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = getWeather;
        this.$requestValues = cVar;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        GetWeather$executeUseCase$Anon1 getWeather$executeUseCase$Anon1 = new GetWeather$executeUseCase$Anon1(this.this$Anon0, this.$requestValues, yb4);
        getWeather$executeUseCase$Anon1.p$ = (zg4) obj;
        return getWeather$executeUseCase$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GetWeather$executeUseCase$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            GetWeather$executeUseCase$Anon1$response$Anon1 getWeather$executeUseCase$Anon1$response$Anon1 = new GetWeather$executeUseCase$Anon1$response$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = ResponseKt.a(getWeather$executeUseCase$Anon1$response$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo2 = (qo2) obj;
        if (qo2 instanceof ro2) {
            vo2 vo2 = new vo2();
            vo2.a((xz1) ((ro2) qo2).a());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = GetWeather.f.a();
            local.d(a2, "onSuccess" + vo2.a());
            i62.d b = this.this$Anon0.a();
            Weather a3 = vo2.a();
            kd4.a((Object) a3, "mfWeatherResponse.weather");
            b.onSuccess(new GetWeather.d(a3));
        } else if (qo2 instanceof po2) {
            this.this$Anon0.a().a(new GetWeather.b(SyncErrorCode.NETWORK_ERROR.name()));
        }
        return qa4.a;
    }
}
