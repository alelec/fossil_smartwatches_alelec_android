package com.portfolio.platform;

import androidx.lifecycle.Lifecycle;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.t52;
import com.fossil.blesdk.obfuscated.wb;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AutoClearedValue$Anon1 implements wb {
    @DexIgnore
    public /* final */ /* synthetic */ t52 a;

    @DexIgnore
    @dc(Lifecycle.Event.ON_DESTROY)
    public final void onDestroy() {
        this.a.a(null);
        throw null;
    }
}
