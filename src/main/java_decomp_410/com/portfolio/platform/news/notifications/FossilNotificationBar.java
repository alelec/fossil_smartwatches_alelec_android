package com.portfolio.platform.news.notifications;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import androidx.core.app.TaskStackBuilder;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.NotificationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.HomeActivity;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FossilNotificationBar {
    @DexIgnore
    public static /* final */ Companion c; // = new Companion((fd4) null);
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public static /* synthetic */ void b(Companion companion, Context context, FossilNotificationBar fossilNotificationBar, boolean z, int i, Object obj) {
            if ((i & 4) != 0) {
                z = false;
            }
            companion.b(context, fossilNotificationBar, z);
        }

        @DexIgnore
        public final void a(Context context) {
            throw null;
            // kd4.b(context, "context");
            // ILocalFLogger local = FLogger.INSTANCE.getLocal();
            // local.d("FossilNotificationBar", "updateData() - context=" + context);
            // try {
            //     fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new FossilNotificationBar$Companion$updateData$Anon1(context, (yb4) null), 3, (Object) null);
            // } catch (Exception e) {
            //     ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            //     local2.e("FossilNotificationBar", "updateData - ex=" + e);
            // }
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final void b(Context context, FossilNotificationBar fossilNotificationBar, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateNotificationWithRichContent " + fossilNotificationBar.a);
            if (!TextUtils.isEmpty(PortfolioApp.W.c().e())) {
                NotificationUtils.Companion.getInstance().updateRichTextNotification(context, 1, fossilNotificationBar.b, fossilNotificationBar.a, a(context, (int) Action.DisplayMode.ACTIVITY, 0), a(context, ".news.notifications.NotificationReceiver", 1), z);
                return;
            }
            NotificationUtils.Companion.getInstance().updateRichTextNotification(context, 1, fossilNotificationBar.b, fossilNotificationBar.a, (PendingIntent) null, (PendingIntent) null, z);
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            throw null;
            // kd4.b(context, "context");
            // kd4.b(str, "title");
            // kd4.b(str2, "content");
            // ILocalFLogger local = FLogger.INSTANCE.getLocal();
            // local.d("FossilNotificationBar", "updateData() - context=" + context + " - content=" + str2);
            // try {
            //     fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new FossilNotificationBar$Companion$updateData$Anon2(str2, str, context, (yb4) null), 3, (Object) null);
            // } catch (Exception e) {
            //     ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            //     local2.e("FossilNotificationBar", "updateData - ex=" + e);
            //     e.printStackTrace();
            // }
        }

        @DexIgnore
        public final void a(Context context, Service service, boolean z) {
            kd4.b(context, "context");
            kd4.b(service, Constants.SERVICE);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "startForegroundNotification() - context=" + context + ", service=" + service + ", isStopForeground = " + z);
            try {
                NotificationUtils.Companion.getInstance().startForegroundNotification(context, service, "", "", z);
                a(context);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("FossilNotificationBar", "startForegroundNotification() - ex=" + e);
            }
        }

        @DexIgnore
        public static /* synthetic */ void a(Companion companion, Context context, FossilNotificationBar fossilNotificationBar, boolean z, int i, Object obj) {
            if ((i & 4) != 0) {
                z = false;
            }
            companion.a(context, fossilNotificationBar, z);
        }

        @DexIgnore
        public final void a(Context context, FossilNotificationBar fossilNotificationBar, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateNotification content " + fossilNotificationBar.a);
            if (!TextUtils.isEmpty(PortfolioApp.W.c().e())) {
                NotificationUtils.Companion.getInstance().updateNotification(context, 1, fossilNotificationBar.a, a(context, (int) Action.DisplayMode.ACTIVITY, 0), a(context, ".news.notifications.NotificationReceiver", 1), z);
                return;
            }
            NotificationUtils.Companion.getInstance().updateNotification(context, 1, fossilNotificationBar.a, (PendingIntent) null, (PendingIntent) null, z);
        }

        @DexIgnore
        public final PendingIntent a(Context context, int i, int i2) {
            Intent intent = new Intent(context, HomeActivity.class);
            intent.putExtra("OUT_STATE_DASHBOARD_CURRENT_TAB", i2);
            TaskStackBuilder b = TaskStackBuilder.a(context).b(intent);
            kd4.a((Object) b, "TaskStackBuilder.create(\u2026ntWithParentStack(intent)");
            return b.a(i, 134217728);
        }

        @DexIgnore
        public final PendingIntent a(Context context, String str, int i) {
            throw null;
            // Intent intent = new Intent(context, NotificationReceiver.class);
            // intent.setAction(str);
            // intent.putExtra("ACTION_EVENT", i);
            // return PendingIntent.getBroadcast(context, Action.DisplayMode.DATE, intent, 134217728);
        }
    }

    @DexIgnore
    public FossilNotificationBar() {
        this((String) null, (String) null, 3, (fd4) null);
    }

    @DexIgnore
    public FossilNotificationBar(String str, String str2) {
        kd4.b(str, "mContent");
        kd4.b(str2, "mTitle");
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ FossilNotificationBar(String str, String str2, int i, fd4 fd4) {
        this(str, (i & 2) != 0 ? "" : str2);
        if ((i & 1) != 0) {
            str = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_WithoutDevice_Dashboard_CTA__PairWatch);
            kd4.a((Object) str, "LanguageHelper.getString\u2026Dashboard_CTA__PairWatch)");
        }
    }
}
