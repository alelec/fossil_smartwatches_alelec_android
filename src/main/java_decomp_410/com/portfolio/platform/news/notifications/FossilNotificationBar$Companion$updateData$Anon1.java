package com.portfolio.platform.news.notifications;

import android.content.Context;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$Anon1", f = "FossilNotificationBar.kt", l = {38}, m = "invokeSuspend")
public final class FossilNotificationBar$Companion$updateData$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Context $context;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$Anon1$Anon1", f = "FossilNotificationBar.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationBar $fossilNotificationBar;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationBar$Companion$updateData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(FossilNotificationBar$Companion$updateData$Anon1 fossilNotificationBar$Companion$updateData$Anon1, FossilNotificationBar fossilNotificationBar, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = fossilNotificationBar$Companion$updateData$Anon1;
            this.$fossilNotificationBar = fossilNotificationBar;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$fossilNotificationBar, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                FossilNotificationBar.Companion.a(FossilNotificationBar.c, this.this$Anon0.$context, this.$fossilNotificationBar, false, 4, (Object) null);
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FossilNotificationBar$Companion$updateData$Anon1(Context context, yb4 yb4) {
        super(2, yb4);
        this.$context = context;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        FossilNotificationBar$Companion$updateData$Anon1 fossilNotificationBar$Companion$updateData$Anon1 = new FossilNotificationBar$Companion$updateData$Anon1(this.$context, yb4);
        fossilNotificationBar$Companion$updateData$Anon1.p$ = (zg4) obj;
        return fossilNotificationBar$Companion$updateData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((FossilNotificationBar$Companion$updateData$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            String d = PortfolioApp.W.c().d();
            FossilNotificationBar fossilNotificationBar = new FossilNotificationBar(d, (String) null, 2, (fd4) null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "content " + d);
            pi4 c = nh4.c();
            Anon1 anon1 = new Anon1(this, fossilNotificationBar, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = d;
            this.L$Anon2 = fossilNotificationBar;
            this.label = 1;
            if (yf4.a(c, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            FossilNotificationBar fossilNotificationBar2 = (FossilNotificationBar) this.L$Anon2;
            String str = (String) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
