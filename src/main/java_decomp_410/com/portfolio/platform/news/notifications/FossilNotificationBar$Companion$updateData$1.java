package com.portfolio.platform.news.notifications;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1", mo27670f = "FossilNotificationBar.kt", mo27671l = {38}, mo27672m = "invokeSuspend")
public final class FossilNotificationBar$Companion$updateData$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.content.Context $context;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21304p$;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1$1", mo27670f = "FossilNotificationBar.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1$1 */
    public static final class C59781 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.news.notifications.FossilNotificationBar $fossilNotificationBar;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21305p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C59781(com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1 fossilNotificationBar$Companion$updateData$1, com.portfolio.platform.news.notifications.FossilNotificationBar fossilNotificationBar, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = fossilNotificationBar$Companion$updateData$1;
            this.$fossilNotificationBar = fossilNotificationBar;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1.C59781 r0 = new com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1.C59781(this.this$0, this.$fossilNotificationBar, yb4);
            r0.f21305p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1.C59781) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.portfolio.platform.news.notifications.FossilNotificationBar.Companion.m32191a(com.portfolio.platform.news.notifications.FossilNotificationBar.f21301c, this.this$0.$context, this.$fossilNotificationBar, false, 4, (java.lang.Object) null);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FossilNotificationBar$Companion$updateData$1(android.content.Context context, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$context = context;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1 fossilNotificationBar$Companion$updateData$1 = new com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1(this.$context, yb4);
        fossilNotificationBar$Companion$updateData$1.f21304p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return fossilNotificationBar$Companion$updateData$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21304p$;
            java.lang.String d = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34529d();
            com.portfolio.platform.news.notifications.FossilNotificationBar fossilNotificationBar = new com.portfolio.platform.news.notifications.FossilNotificationBar(d, (java.lang.String) null, 2, (com.fossil.blesdk.obfuscated.fd4) null);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("FossilNotificationBar", "content " + d);
            com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
            com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1.C59781 r6 = new com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1.C59781(this, fossilNotificationBar, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = d;
            this.L$2 = fossilNotificationBar;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r6, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.portfolio.platform.news.notifications.FossilNotificationBar fossilNotificationBar2 = (com.portfolio.platform.news.notifications.FossilNotificationBar) this.L$2;
            java.lang.String str = (java.lang.String) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
