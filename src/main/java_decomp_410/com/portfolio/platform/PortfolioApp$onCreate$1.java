package com.portfolio.platform;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.PortfolioApp$onCreate$1", mo27670f = "PortfolioApp.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class PortfolioApp$onCreate$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20986p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.PortfolioApp this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PortfolioApp$onCreate$1(com.portfolio.platform.PortfolioApp portfolioApp, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = portfolioApp;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.PortfolioApp$onCreate$1 portfolioApp$onCreate$1 = new com.portfolio.platform.PortfolioApp$onCreate$1(this.this$0, yb4);
        portfolioApp$onCreate$1.f20986p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return portfolioApp$onCreate$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.PortfolioApp$onCreate$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004f, code lost:
        if (r2 != null) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0059, code lost:
        if (r4 != null) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0091, code lost:
        if (r1 != null) goto L_0x0095;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00df, code lost:
        if (r4 != null) goto L_0x00e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00e9, code lost:
        if (r15 != null) goto L_0x00ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0027, code lost:
        if (r4 != null) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0031, code lost:
        if (r5 != null) goto L_0x0035;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.String str;
        java.lang.String str2;
        java.lang.String str3;
        java.lang.String str4;
        java.lang.String str5;
        java.lang.String str6;
        java.lang.String str7;
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.Access a = new com.portfolio.platform.manager.SoLibraryLoader().mo39677a((android.content.Context) this.this$0);
            com.fossil.blesdk.obfuscated.xa0 xa0 = com.fossil.blesdk.obfuscated.xa0.f10879c;
            java.lang.String q = com.fossil.blesdk.obfuscated.f62.f14731x.mo27342q();
            if (a != null) {
                str = a.getL();
            }
            str = "";
            if (a != null) {
                str2 = a.getM();
            }
            str2 = "";
            xa0.mo17641a(new com.fossil.blesdk.obfuscated.na0(q, str, str2));
            java.lang.String f = com.fossil.blesdk.obfuscated.f62.f14731x.mo27331f();
            java.lang.String c = com.fossil.blesdk.obfuscated.f62.f14731x.mo27328c();
            if (a != null) {
                str3 = a.getI();
            }
            str3 = "";
            if (a != null) {
                str4 = a.getK();
            }
            str4 = "";
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.init("App", com.portfolio.platform.helper.AppHelper.f21170f.mo39525a().mo39524b(), com.portfolio.platform.helper.AppHelper.f21170f.mo39525a().mo39522a(), new com.misfit.frameworks.buttonservice.log.model.CloudLogConfig(f, c, str3, str4), this.this$0, true, "App");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().flush();
            java.lang.String str8 = "0000000000000";
            if (a != null) {
                str5 = a.getO();
            }
            str5 = str8;
            com.facebook.FacebookSdk.setApplicationId(str5);
            if (!com.facebook.FacebookSdk.isInitialized()) {
                com.facebook.FacebookSdk.sdkInitialize(this.this$0);
            }
            if (!com.google.android.libraries.places.api.Places.isInitialized()) {
                com.portfolio.platform.PortfolioApp portfolioApp = this.this$0;
                if (a != null) {
                    java.lang.String n = a.getN();
                    if (n != null) {
                        str8 = n;
                    }
                }
                com.google.android.libraries.places.api.Places.initialize(portfolioApp, str8);
            }
            com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39517c().mo39500a(com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39517c().mo39506b());
            com.portfolio.platform.helper.AppHelper.f21170f.mo39533c();
            com.zendesk.sdk.network.impl.ZendeskConfig zendeskConfig = com.zendesk.sdk.network.impl.ZendeskConfig.INSTANCE;
            com.portfolio.platform.PortfolioApp portfolioApp2 = this.this$0;
            java.lang.String v = com.fossil.blesdk.obfuscated.f62.f14731x.mo27347v();
            if (a != null) {
                str6 = a.getG();
            }
            str6 = "";
            if (a != null) {
                str7 = a.getH();
            }
            str7 = "";
            zendeskConfig.init(portfolioApp2, v, str6, str7);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
