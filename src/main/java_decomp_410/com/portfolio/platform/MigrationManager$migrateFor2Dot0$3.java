package com.portfolio.platform;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.MigrationManager$migrateFor2Dot0$3", mo27670f = "MigrationManager.kt", mo27671l = {460}, mo27672m = "invokeSuspend")
public final class MigrationManager$migrateFor2Dot0$3 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20931p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.MigrationManager this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MigrationManager$migrateFor2Dot0$3(com.portfolio.platform.MigrationManager migrationManager, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = migrationManager;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.MigrationManager$migrateFor2Dot0$3 migrationManager$migrateFor2Dot0$3 = new com.portfolio.platform.MigrationManager$migrateFor2Dot0$3(this.this$0, yb4);
        migrationManager$migrateFor2Dot0$3.f20931p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return migrationManager$migrateFor2Dot0$3;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.MigrationManager$migrateFor2Dot0$3) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f20931p$;
            com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase b = this.this$0.f20918d;
            com.fossil.blesdk.obfuscated.on3 on3 = new com.fossil.blesdk.obfuscated.on3(this.this$0.f20919e.mo34532e());
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.w52.m29539a(b, on3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
