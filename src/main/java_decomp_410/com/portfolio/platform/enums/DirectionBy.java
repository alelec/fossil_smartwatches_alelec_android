package com.portfolio.platform.enums;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.google.maps.model.TravelMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum DirectionBy {
    CAR(r2, r3),
    BUS(r2, r3),
    WALK(r2, r3),
    BIKE(r2, r3);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ TravelMode travelMode;
    @DexIgnore
    public /* final */ String type;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final TravelMode a(String str) {
            DirectionBy directionBy;
            kd4.b(str, "type");
            DirectionBy[] values = DirectionBy.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    directionBy = null;
                    break;
                }
                directionBy = values[i];
                if (kd4.a((Object) directionBy.getType(), (Object) str)) {
                    break;
                }
                i++;
            }
            if (directionBy != null) {
                TravelMode travelMode = directionBy.getTravelMode();
                if (travelMode != null) {
                    return travelMode;
                }
            }
            return DirectionBy.CAR.getTravelMode();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    DirectionBy(TravelMode travelMode2, String str) {
        this.travelMode = travelMode2;
        this.type = str;
    }

    @DexIgnore
    public static final TravelMode getTravelModeFromType(String str) {
        return Companion.a(str);
    }

    @DexIgnore
    public final TravelMode getTravelMode() {
        return this.travelMode;
    }

    @DexIgnore
    public final String getType() {
        return this.type;
    }
}
