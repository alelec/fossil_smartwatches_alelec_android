package com.portfolio.platform.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum Unit {
    IMPERIAL("imperial"),
    METRIC("metric");
    
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    Unit(String str) {
        this.value = str;
    }

    @DexIgnore
    public static Unit fromString(String str) {
        if (str.equalsIgnoreCase("imperial")) {
            return IMPERIAL;
        }
        return METRIC;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }
}
