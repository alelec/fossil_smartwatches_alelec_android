package com.portfolio.platform.enums;

import android.text.TextUtils;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum AuthType {
    EMAIL("email", false),
    FACEBOOK(Constants.FACEBOOK, true),
    GOOGLE("google", true),
    APPLE("apple", true),
    WECHAT("wechat", true),
    WEIBO("weibo", true);
    
    @DexIgnore
    public /* final */ boolean isSSO;
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    AuthType(String str, boolean z) {
        this.value = str;
        this.isSSO = z;
    }

    @DexIgnore
    public static AuthType fromString(String str) {
        if (!TextUtils.isEmpty(str)) {
            for (AuthType authType : values()) {
                if (str.equalsIgnoreCase(authType.value)) {
                    return authType;
                }
            }
        }
        return EMAIL;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }

    @DexIgnore
    public boolean isSSO() {
        return this.isSSO;
    }
}
