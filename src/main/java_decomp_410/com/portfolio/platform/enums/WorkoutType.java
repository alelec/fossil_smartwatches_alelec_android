package com.portfolio.platform.enums;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.zi2;
import com.fossil.wearables.fossil.R;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum WorkoutType {
    UNKNOWN("unknown"),
    RUNNING("running"),
    CYCLING("cycling"),
    TREADMILL("treadmill"),
    ELLIPTICAL("elliptical"),
    WEIGHTS("weights"),
    WORKOUT("workout");
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public String mValue;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final WorkoutType a(String str) {
            WorkoutType workoutType;
            kd4.b(str, "value");
            WorkoutType[] values = WorkoutType.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    workoutType = null;
                    break;
                }
                workoutType = values[i];
                String mValue = workoutType.getMValue();
                String lowerCase = str.toLowerCase();
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                if (kd4.a((Object) mValue, (Object) lowerCase)) {
                    break;
                }
                i++;
            }
            return workoutType != null ? workoutType : WorkoutType.UNKNOWN;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final WorkoutType a(int i) {
            if (i == com.fossil.fitness.WorkoutType.UNKNOWN.ordinal()) {
                return WorkoutType.UNKNOWN;
            }
            if (i == com.fossil.fitness.WorkoutType.RUNNING.ordinal()) {
                return WorkoutType.RUNNING;
            }
            if (i == com.fossil.fitness.WorkoutType.CYCLING.ordinal()) {
                return WorkoutType.CYCLING;
            }
            if (i == com.fossil.fitness.WorkoutType.TREADMILL.ordinal()) {
                return WorkoutType.TREADMILL;
            }
            if (i == com.fossil.fitness.WorkoutType.ELLIPTICAL.ordinal()) {
                return WorkoutType.ELLIPTICAL;
            }
            if (i == com.fossil.fitness.WorkoutType.WEIGHTS.ordinal()) {
                return WorkoutType.WEIGHTS;
            }
            if (i == com.fossil.fitness.WorkoutType.WORKOUT.ordinal()) {
                return WorkoutType.WORKOUT;
            }
            return WorkoutType.UNKNOWN;
        }

        @DexIgnore
        public final Pair<Integer, Integer> a(WorkoutType workoutType) {
            throw null;
            // if (workoutType == null) {
            //     workoutType = WorkoutType.UNKNOWN;
            // }
            // if (workoutType != null) {
            //     switch (zi2.a[workoutType.ordinal()]) {
            //         case 1:
            //             return new Pair<>(Integer.valueOf(R.drawable.ic_workout_run), Integer.valueOf(R.string.DashboardDiana_ActiveMinutes_DetailPage_Title__Run));
            //         case 2:
            //             return new Pair<>(Integer.valueOf(R.drawable.ic_workout_cycle), Integer.valueOf(R.string.DashboardDiana_ActiveMinutes_DetailPage_Title__Cycling));
            //         case 3:
            //             return new Pair<>(Integer.valueOf(R.drawable.ic_workout_treadmill), Integer.valueOf(R.string.DashboardDiana_ActiveMinutes_DetailPage_Title__Treadmill));
            //         case 4:
            //             return new Pair<>(Integer.valueOf(R.drawable.ic_workout_elliptical), Integer.valueOf(R.string.DashboardDiana_ActiveMinutes_DetailPage_Title__Elliptical));
            //         case 5:
            //             return new Pair<>(Integer.valueOf(R.drawable.ic_workout_weights), Integer.valueOf(R.string.DashboardDiana_ActiveMinutes_DetailPage_Title__Weights));
            //         case 6:
            //             return new Pair<>(Integer.valueOf(R.drawable.ic_workout_generic), Integer.valueOf(R.string.DashboardDiana_ActiveMinutes_DetailPage_Title__Workout));
            //         case 7:
            //             return new Pair<>(Integer.valueOf(R.drawable.ic_workout_generic), Integer.valueOf(R.string.DashboardDiana_ActiveMinutes_DetailPage_Title__Workout));
            //     }
            // }
            // throw new NoWhenBranchMatchedException();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    WorkoutType(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        kd4.b(str, "<set-?>");
        this.mValue = str;
    }
}
