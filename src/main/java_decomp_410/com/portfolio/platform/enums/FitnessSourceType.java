package com.portfolio.platform.enums;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum FitnessSourceType {
    Device("Device"),
    User("User"),
    Mock("Mock");
    
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    FitnessSourceType(String str) {
        this.value = str;
    }

    @DexIgnore
    public static FitnessSourceType fromString(String str) {
        if (!TextUtils.isEmpty(str)) {
            for (FitnessSourceType fitnessSourceType : values()) {
                if (str.equalsIgnoreCase(fitnessSourceType.value)) {
                    return fitnessSourceType;
                }
            }
        }
        return Device;
    }

    @DexIgnore
    public String getName() {
        return this.value;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }
}
