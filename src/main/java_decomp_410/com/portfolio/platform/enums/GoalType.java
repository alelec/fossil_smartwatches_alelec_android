package com.portfolio.platform.enums;

import com.fossil.blesdk.obfuscated.fd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum GoalType {
    ACTIVE_TIME(1),
    TOTAL_STEPS(2),
    CALORIES(3),
    GOAL_TRACKING(3),
    TOTAL_SLEEP(11),
    RESTFUL(12),
    LIGHT(13),
    AWAKE(14);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public int mValue;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    GoalType(int i) {
        this.mValue = i;
    }

    @DexIgnore
    public final int getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(int i) {
        this.mValue = i;
    }
}
