package com.portfolio.platform.underamour;

import com.fossil.blesdk.obfuscated.fd4;
import com.zendesk.sdk.model.SdkConfiguration;
import com.zendesk.sdk.network.Constants;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UAValues {
    @DexIgnore
    public static /* final */ long a; // = TimeUnit.DAYS.toMillis(2);
    @DexIgnore
    public static /* final */ a b; // = new a((fd4) null);

    @DexIgnore
    public enum Authorization {
        BEARER(SdkConfiguration.BEARER_FORMAT);
        
        @DexIgnore
        public /* final */ String value;

        @DexIgnore
        Authorization(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public enum ContentType {
        X_WWW_FORM_URLENCODED("application/x-www-form-urlencoded"),
        JSON(Constants.APPLICATION_JSON);
        
        @DexIgnore
        public /* final */ String value;

        @DexIgnore
        ContentType(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public enum GrantType {
        AUTHORIZATION_CODE("authorization_code"),
        REFRESH_TOKEN("refresh_token");
        
        @DexIgnore
        public /* final */ String value;

        @DexIgnore
        GrantType(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final long a() {
            return UAValues.a;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }
}
