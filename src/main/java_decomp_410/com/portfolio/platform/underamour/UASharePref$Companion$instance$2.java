package com.portfolio.platform.underamour;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UASharePref$Companion$instance$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.portfolio.platform.underamour.UASharePref> {
    @DexIgnore
    public static /* final */ com.portfolio.platform.underamour.UASharePref$Companion$instance$2 INSTANCE; // = new com.portfolio.platform.underamour.UASharePref$Companion$instance$2();

    @DexIgnore
    public UASharePref$Companion$instance$2() {
        super(0);
    }

    @DexIgnore
    public final com.portfolio.platform.underamour.UASharePref invoke() {
        return new com.portfolio.platform.underamour.UASharePref();
    }
}
