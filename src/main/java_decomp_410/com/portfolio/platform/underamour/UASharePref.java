package com.portfolio.platform.underamour;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.la4;
import com.fossil.blesdk.obfuscated.le4;
import com.fossil.blesdk.obfuscated.ma4;
import com.fossil.blesdk.obfuscated.md4;
import com.google.gson.Gson;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ua.UAAccessToken;
import com.portfolio.platform.data.model.ua.UADataSource;
import com.portfolio.platform.data.model.ua.UADevice;
import kotlin.TypeCastException;
import kotlin.jvm.internal.PropertyReference1;
import kotlin.jvm.internal.PropertyReference1Impl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UASharePref {
    @DexIgnore
    public static /* final */ la4 b; // = ma4.a(UASharePref$Companion$instance$Anon2.INSTANCE);
    @DexIgnore
    public static /* final */ a c; // = new a((fd4) null);
    @DexIgnore
    public SharedPreferences a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ /* synthetic */ le4[] a;

        /*
        static {
            PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(md4.a(a.class), "instance", "getInstance()Lcom/portfolio/platform/underamour/UASharePref;");
            md4.a((PropertyReference1) propertyReference1Impl);
            a = new le4[]{propertyReference1Impl};
        }
        */

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final UASharePref a() {
            la4 e = UASharePref.b;
            a aVar = UASharePref.c;
            le4 le4 = a[0];
            return (UASharePref) e.getValue();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public UASharePref() {
        SharedPreferences sharedPreferences = PortfolioApp.W.c().getSharedPreferences("PREFS_UA", 0);
        kd4.a((Object) sharedPreferences, "PortfolioApp.instance.ge\u2026ME, Context.MODE_PRIVATE)");
        this.a = sharedPreferences;
    }

    @DexIgnore
    public static /* synthetic */ void a(UASharePref uASharePref, UAAccessToken uAAccessToken, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        uASharePref.a(uAAccessToken, z);
    }

    @DexIgnore
    public final UAAccessToken b() {
        String string = this.a.getString("KEY_UA_ACCESS_TOKEN_OBJECT_RESPONSE", "");
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        return (UAAccessToken) new Gson().a(string, UAAccessToken.class);
    }

    @DexIgnore
    public final UADataSource c() {
        String string = this.a.getString("KEY_UA_DATA_SOURCE", "");
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        return (UADataSource) new Gson().a(string, UADataSource.class);
    }

    @DexIgnore
    public final UADevice d() {
        String string = this.a.getString("KEY_UA_DEVICE", "");
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        return (UADevice) new Gson().a(string, UADevice.class);
    }

    @DexIgnore
    public final void a(UAAccessToken uAAccessToken, boolean z) {
        if (uAAccessToken != null) {
            if (!z) {
                long currentTimeMillis = System.currentTimeMillis();
                Long expiresIn = uAAccessToken.getExpiresIn();
                Long valueOf = expiresIn != null ? Long.valueOf(expiresIn.longValue() * 1000) : null;
                if (valueOf != null) {
                    uAAccessToken.setExpiresAt(Long.valueOf(currentTimeMillis + valueOf.longValue()));
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Long");
                }
            }
            this.a.edit().putString("KEY_UA_ACCESS_TOKEN_OBJECT_RESPONSE", new Gson().a((Object) uAAccessToken)).apply();
            String accessToken = uAAccessToken.getAccessToken();
            if (accessToken != null) {
                a(accessToken);
            } else {
                kd4.a();
                throw null;
            }
        } else {
            a("");
        }
    }

    @DexIgnore
    public final void a(String str) {
        this.a.edit().putString("KEY_ACCESS_TOKEN", str).apply();
    }

    @DexIgnore
    public final String a() {
        return this.a.getString("KEY_ACCESS_TOKEN", "");
    }

    @DexIgnore
    public final void a(UADevice uADevice) {
        kd4.b(uADevice, "uaDevice");
        this.a.edit().putString("KEY_UA_DEVICE", new Gson().a((Object) uADevice)).apply();
    }

    @DexIgnore
    public final void a(UADataSource uADataSource) {
        kd4.b(uADataSource, "uaDataSource");
        this.a.edit().putString("KEY_UA_DATA_SOURCE", new Gson().a((Object) uADataSource)).apply();
    }
}
