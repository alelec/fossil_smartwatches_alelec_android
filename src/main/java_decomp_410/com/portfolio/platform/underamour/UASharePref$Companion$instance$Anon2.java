package com.portfolio.platform.underamour;

import com.fossil.blesdk.obfuscated.wc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UASharePref$Companion$instance$Anon2 extends Lambda implements wc4<UASharePref> {
    @DexIgnore
    public static /* final */ UASharePref$Companion$instance$Anon2 INSTANCE; // = new UASharePref$Companion$instance$Anon2();

    @DexIgnore
    public UASharePref$Companion$instance$Anon2() {
        super(0);
    }

    @DexIgnore
    public final UASharePref invoke() {
        return new UASharePref();
    }
}
