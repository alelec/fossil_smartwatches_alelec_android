package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.on3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.w52;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.MigrationManager$migrateFor2Dot0$Anon3", f = "MigrationManager.kt", l = {460}, m = "invokeSuspend")
public final class MigrationManager$migrateFor2Dot0$Anon3 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MigrationManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MigrationManager$migrateFor2Dot0$Anon3(MigrationManager migrationManager, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = migrationManager;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MigrationManager$migrateFor2Dot0$Anon3 migrationManager$migrateFor2Dot0$Anon3 = new MigrationManager$migrateFor2Dot0$Anon3(this.this$Anon0, yb4);
        migrationManager$migrateFor2Dot0$Anon3.p$ = (zg4) obj;
        return migrationManager$migrateFor2Dot0$Anon3;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MigrationManager$migrateFor2Dot0$Anon3) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            GetHybridDeviceSettingUseCase b = this.this$Anon0.d;
            on3 on3 = new on3(this.this$Anon0.e.e());
            this.L$Anon0 = zg4;
            this.label = 1;
            if (w52.a(b, on3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
