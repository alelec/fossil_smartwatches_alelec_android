package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.portfolio.platform.helper.AppHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.PortfolioApp$updateActiveDeviceInfoLog$Anon1", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
public final class PortfolioApp$updateActiveDeviceInfoLog$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    public PortfolioApp$updateActiveDeviceInfoLog$Anon1(yb4 yb4) {
        super(2, yb4);
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        PortfolioApp$updateActiveDeviceInfoLog$Anon1 portfolioApp$updateActiveDeviceInfoLog$Anon1 = new PortfolioApp$updateActiveDeviceInfoLog$Anon1(yb4);
        portfolioApp$updateActiveDeviceInfoLog$Anon1.p$ = (zg4) obj;
        return portfolioApp$updateActiveDeviceInfoLog$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PortfolioApp$updateActiveDeviceInfoLog$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            ActiveDeviceInfo a = AppHelper.f.a().a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d = PortfolioApp.W.d();
            local.d(d, ".updateActiveDeviceInfoLog(), " + new Gson().a((Object) a));
            FLogger.INSTANCE.getRemote().updateActiveDeviceInfo(a);
            try {
                IButtonConnectivity b = PortfolioApp.W.b();
                if (b != null) {
                    b.updateActiveDeviceInfoLog(a);
                }
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String d2 = PortfolioApp.W.d();
                local2.e(d2, ".updateActiveDeviceInfoToButtonService(), error=" + e);
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
