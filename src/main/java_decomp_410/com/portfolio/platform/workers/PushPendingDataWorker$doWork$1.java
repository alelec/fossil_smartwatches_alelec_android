package com.portfolio.platform.workers;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.workers.PushPendingDataWorker", mo27670f = "PushPendingDataWorker.kt", mo27671l = {73}, mo27672m = "doWork")
public final class PushPendingDataWorker$doWork$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.workers.PushPendingDataWorker this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PushPendingDataWorker$doWork$1(com.portfolio.platform.workers.PushPendingDataWorker pushPendingDataWorker, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = pushPendingDataWorker;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.mo3736a((com.fossil.blesdk.obfuscated.yb4<? super androidx.work.ListenableWorker.C0354a>) this);
    }
}
