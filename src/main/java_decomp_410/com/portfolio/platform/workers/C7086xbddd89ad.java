package com.portfolio.platform.workers;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$2 */
public final class C7086xbddd89ad extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.dg4 $cancellableContinuation;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f25723p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.workers.PushPendingDataWorker this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$2$1")
    /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$2$1 */
    public static final class C70871 implements com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.workers.C7086xbddd89ad f25724a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$2$1$1")
        /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$2$1$1 */
        public static final class C70881 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ java.util.List $sleepSessionList;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public java.lang.Object L$1;
            @DexIgnore
            public java.lang.Object L$2;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f25725p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.workers.C7086xbddd89ad.C70871 this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$2$1$1$1")
            /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$2$1$1$1 */
            public static final class C70891 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qo2<com.fossil.blesdk.obfuscated.xz1>>, java.lang.Object> {
                @DexIgnore
                public /* final */ /* synthetic */ java.util.Calendar $end;
                @DexIgnore
                public /* final */ /* synthetic */ java.util.Calendar $start;
                @DexIgnore
                public java.lang.Object L$0;
                @DexIgnore
                public int label;

                @DexIgnore
                /* renamed from: p$ */
                public com.fossil.blesdk.obfuscated.zg4 f25726p$;
                @DexIgnore
                public /* final */ /* synthetic */ com.portfolio.platform.workers.C7086xbddd89ad.C70871.C70881 this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C70891(com.portfolio.platform.workers.C7086xbddd89ad.C70871.C70881 r1, java.util.Calendar calendar, java.util.Calendar calendar2, com.fossil.blesdk.obfuscated.yb4 yb4) {
                    super(2, yb4);
                    this.this$0 = r1;
                    this.$start = calendar;
                    this.$end = calendar2;
                }

                @DexIgnore
                public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                    com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                    com.portfolio.platform.workers.C7086xbddd89ad.C70871.C70881.C70891 r0 = new com.portfolio.platform.workers.C7086xbddd89ad.C70871.C70881.C70891(this.this$0, this.$start, this.$end, yb4);
                    r0.f25726p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                    return r0;
                }

                @DexIgnore
                public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                    return ((com.portfolio.platform.workers.C7086xbddd89ad.C70871.C70881.C70891) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
                }

                @DexIgnore
                public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                    java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                    int i = this.label;
                    if (i == 0) {
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        com.fossil.blesdk.obfuscated.zg4 zg4 = this.f25726p$;
                        com.portfolio.platform.data.source.SleepSummariesRepository f = this.this$0.this$0.f25724a.this$0.f25694n;
                        java.util.Calendar calendar = this.$start;
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) calendar, com.facebook.share.internal.VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                        java.util.Date time = calendar.getTime();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) time, "start.time");
                        java.util.Calendar calendar2 = this.$end;
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) calendar2, "end");
                        java.util.Date time2 = calendar2.getTime();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) time2, "end.time");
                        this.L$0 = zg4;
                        this.label = 1;
                        obj = f.fetchSleepSummaries(time, time2, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    } else {
                        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C70881(com.portfolio.platform.workers.C7086xbddd89ad.C70871 r1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$sleepSessionList = list;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.workers.C7086xbddd89ad.C70871.C70881 r0 = new com.portfolio.platform.workers.C7086xbddd89ad.C70871.C70881(this.this$0, this.$sleepSessionList, yb4);
                r0.f25725p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.workers.C7086xbddd89ad.C70871.C70881) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f25725p$;
                    if (!this.$sleepSessionList.isEmpty()) {
                        int realStartTime = ((com.portfolio.platform.data.model.room.sleep.MFSleepSession) this.$sleepSessionList.get(0)).getRealStartTime();
                        int realEndTime = ((com.portfolio.platform.data.model.room.sleep.MFSleepSession) this.$sleepSessionList.get(0)).getRealEndTime();
                        for (com.portfolio.platform.data.model.room.sleep.MFSleepSession mFSleepSession : this.$sleepSessionList) {
                            if (mFSleepSession.getRealStartTime() < realStartTime) {
                                realStartTime = mFSleepSession.getRealStartTime();
                            }
                            if (mFSleepSession.getRealEndTime() > realEndTime) {
                                realEndTime = mFSleepSession.getRealEndTime();
                            }
                        }
                        java.util.Calendar instance = java.util.Calendar.getInstance();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, com.facebook.share.internal.VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                        instance.setTimeInMillis((long) realStartTime);
                        java.util.Calendar instance2 = java.util.Calendar.getInstance();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance2, "end");
                        instance2.setTimeInMillis((long) realEndTime);
                        com.fossil.blesdk.obfuscated.ug4 b = com.fossil.blesdk.obfuscated.nh4.m25692b();
                        com.portfolio.platform.workers.C7086xbddd89ad.C70871.C70881.C70891 r7 = new com.portfolio.platform.workers.C7086xbddd89ad.C70871.C70881.C70891(this, instance, instance2, (com.fossil.blesdk.obfuscated.yb4) null);
                        this.L$0 = zg4;
                        this.I$0 = realStartTime;
                        this.I$1 = realEndTime;
                        this.L$1 = instance;
                        this.L$2 = instance2;
                        this.label = 1;
                        if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r7, this) == a) {
                            return a;
                        }
                    }
                } else if (i == 1) {
                    java.util.Calendar calendar = (java.util.Calendar) this.L$2;
                    java.util.Calendar calendar2 = (java.util.Calendar) this.L$1;
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (this.this$0.f25724a.$cancellableContinuation.isActive()) {
                    com.fossil.blesdk.obfuscated.dg4 dg4 = this.this$0.f25724a.$cancellableContinuation;
                    java.lang.Boolean a2 = com.fossil.blesdk.obfuscated.dc4.m20839a(true);
                    kotlin.Result.C7350a aVar = kotlin.Result.Companion;
                    dg4.resumeWith(kotlin.Result.m37419constructorimpl(a2));
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        public C70871(com.portfolio.platform.workers.C7086xbddd89ad pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$2) {
            this.f25724a = pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$2;
        }

        @DexIgnore
        public void onFail(int i) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("PushPendingDataWorker", "SleepSessionRepository.pushPendingSleepSessions onFail, go to next, errorCode = " + i);
            if (this.f25724a.$cancellableContinuation.isActive()) {
                com.fossil.blesdk.obfuscated.dg4 dg4 = this.f25724a.$cancellableContinuation;
                kotlin.Result.C7350a aVar = kotlin.Result.Companion;
                dg4.resumeWith(kotlin.Result.m37419constructorimpl(true));
            }
        }

        @DexIgnore
        public void onSuccess(java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession> list) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(list, "sleepSessionList");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("PushPendingDataWorker", "SleepSessionRepository.pushPendingSleepSessions onSuccess, go to next");
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25691a()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.workers.C7086xbddd89ad.C70871.C70881(this, list, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C7086xbddd89ad(com.fossil.blesdk.obfuscated.dg4 dg4, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.workers.PushPendingDataWorker pushPendingDataWorker) {
        super(2, yb4);
        this.$cancellableContinuation = dg4;
        this.this$0 = pushPendingDataWorker;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.workers.C7086xbddd89ad pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$2 = new com.portfolio.platform.workers.C7086xbddd89ad(this.$cancellableContinuation, yb4, this.this$0);
        pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$2.f25723p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.workers.C7086xbddd89ad) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            this.this$0.f25693m.pushPendingSleepSessions(new com.portfolio.platform.workers.C7086xbddd89ad.C70871(this));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
