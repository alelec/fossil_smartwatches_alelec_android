package com.portfolio.platform.workers;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.workers.PushPendingDataWorker$start$5", mo27670f = "PushPendingDataWorker.kt", mo27671l = {207, 208, 210, 211, 213, 214, 215}, mo27672m = "invokeSuspend")
public final class PushPendingDataWorker$start$5 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $startDate;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f25731p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.workers.PushPendingDataWorker this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PushPendingDataWorker$start$5(com.portfolio.platform.workers.PushPendingDataWorker pushPendingDataWorker, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = pushPendingDataWorker;
        this.$startDate = ref$ObjectRef;
        this.$endDate = ref$ObjectRef2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.workers.PushPendingDataWorker$start$5 pushPendingDataWorker$start$5 = new com.portfolio.platform.workers.PushPendingDataWorker$start$5(this.this$0, this.$startDate, this.$endDate, yb4);
        pushPendingDataWorker$start$5.f25731p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return pushPendingDataWorker$start$5;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.workers.PushPendingDataWorker$start$5) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x008a, code lost:
        r13 = r12.this$0.f25692l;
        r4 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r4, "startDate.toDate()");
        r5 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r5, "endDate.toDate()");
        r12.L$0 = r1;
        r12.label = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00b3, code lost:
        if (r13.loadSummaries(r4, r5, r12) != r0) goto L_0x00b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00b5, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00b6, code lost:
        r4 = r12.this$0.f25693m;
        r5 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r5, "startDate.toDate()");
        r6 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r6, "endDate.toDate()");
        r12.L$0 = r1;
        r12.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00e5, code lost:
        if (com.portfolio.platform.data.source.SleepSessionsRepository.fetchSleepSessions$default(r4, r5, r6, 0, 0, r12, 12, (java.lang.Object) null) != r0) goto L_0x00e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00e7, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00e8, code lost:
        r13 = r12.this$0.f25694n;
        r4 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r4, "startDate.toDate()");
        r5 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r5, "endDate.toDate()");
        r12.L$0 = r1;
        r12.label = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0111, code lost:
        if (r13.fetchSleepSummaries(r4, r5, r12) != r0) goto L_0x0114;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0113, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0114, code lost:
        r4 = r12.this$0.f25696p;
        r5 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r5, "startDate.toDate()");
        r6 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r6, "endDate.toDate()");
        r12.L$0 = r1;
        r12.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0143, code lost:
        if (com.portfolio.platform.data.source.HeartRateSampleRepository.fetchHeartRateSamples$default(r4, r5, r6, 0, 0, r12, 12, (java.lang.Object) null) != r0) goto L_0x0146;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0145, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0146, code lost:
        r13 = r12.this$0.f25697q;
        r4 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r4, "startDate.toDate()");
        r3 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r3, "endDate.toDate()");
        r12.L$0 = r1;
        r12.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x016f, code lost:
        if (r13.loadSummaries(r4, r3, r12) != r0) goto L_0x0172;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0171, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0172, code lost:
        r13 = r12.this$0.f25692l;
        r12.L$0 = r1;
        r12.label = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0181, code lost:
        if (r13.fetchActivityStatistic(r12) != r0) goto L_0x0184;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0183, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0186, code lost:
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        switch (this.label) {
            case 0:
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg42 = this.f25731p$;
                com.portfolio.platform.data.source.ActivitiesRepository a2 = this.this$0.f25691k;
                java.util.Date date = ((org.joda.time.DateTime) this.$startDate.element).toDate();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date, "startDate.toDate()");
                java.util.Date date2 = ((org.joda.time.DateTime) this.$endDate.element).toDate();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date2, "endDate.toDate()");
                this.L$0 = zg42;
                this.label = 1;
                if (com.portfolio.platform.data.source.ActivitiesRepository.fetchActivitySamples$default(a2, date, date2, 0, 0, this, 12, (java.lang.Object) null) != a) {
                    zg4 = zg42;
                    break;
                } else {
                    return a;
                }
            case 1:
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 2:
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 3:
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 4:
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 5:
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 6:
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 7:
                com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            default:
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
