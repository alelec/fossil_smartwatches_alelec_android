package com.portfolio.platform.workers;

import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.dg4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ dg4 $cancellableContinuation;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PushPendingDataWorker this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements SleepSessionsRepository.PushPendingSleepSessionsCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0165Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $sleepSessionList;
            @DexIgnore
            public int I$Anon0;
            @DexIgnore
            public int I$Anon1;
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public Object L$Anon1;
            @DexIgnore
            public Object L$Anon2;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2$Anon1$Anon1$Anon1")
            /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2$Anon1$Anon1$Anon1  reason: collision with other inner class name */
            public static final class C0166Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qo2<xz1>>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Calendar $end;
                @DexIgnore
                public /* final */ /* synthetic */ Calendar $start;
                @DexIgnore
                public Object L$Anon0;
                @DexIgnore
                public int label;
                @DexIgnore
                public zg4 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0165Anon1 this$Anon0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0166Anon1(C0165Anon1 anon1, Calendar calendar, Calendar calendar2, yb4 yb4) {
                    super(2, yb4);
                    this.this$Anon0 = anon1;
                    this.$start = calendar;
                    this.$end = calendar2;
                }

                @DexIgnore
                public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                    kd4.b(yb4, "completion");
                    C0166Anon1 anon1 = new C0166Anon1(this.this$Anon0, this.$start, this.$end, yb4);
                    anon1.p$ = (zg4) obj;
                    return anon1;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0166Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    Object a = cc4.a();
                    int i = this.label;
                    if (i == 0) {
                        na4.a(obj);
                        zg4 zg4 = this.p$;
                        SleepSummariesRepository f = this.this$Anon0.this$Anon0.a.this$Anon0.n;
                        Calendar calendar = this.$start;
                        kd4.a((Object) calendar, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                        Date time = calendar.getTime();
                        kd4.a((Object) time, "start.time");
                        Calendar calendar2 = this.$end;
                        kd4.a((Object) calendar2, "end");
                        Date time2 = calendar2.getTime();
                        kd4.a((Object) time2, "end.time");
                        this.L$Anon0 = zg4;
                        this.label = 1;
                        obj = f.fetchSleepSummaries(time, time2, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        zg4 zg42 = (zg4) this.L$Anon0;
                        na4.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0165Anon1(Anon1 anon1, List list, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
                this.$sleepSessionList = list;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0165Anon1 anon1 = new C0165Anon1(this.this$Anon0, this.$sleepSessionList, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0165Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 zg4 = this.p$;
                    if (!this.$sleepSessionList.isEmpty()) {
                        int realStartTime = ((MFSleepSession) this.$sleepSessionList.get(0)).getRealStartTime();
                        int realEndTime = ((MFSleepSession) this.$sleepSessionList.get(0)).getRealEndTime();
                        for (MFSleepSession mFSleepSession : this.$sleepSessionList) {
                            if (mFSleepSession.getRealStartTime() < realStartTime) {
                                realStartTime = mFSleepSession.getRealStartTime();
                            }
                            if (mFSleepSession.getRealEndTime() > realEndTime) {
                                realEndTime = mFSleepSession.getRealEndTime();
                            }
                        }
                        Calendar instance = Calendar.getInstance();
                        kd4.a((Object) instance, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                        instance.setTimeInMillis((long) realStartTime);
                        Calendar instance2 = Calendar.getInstance();
                        kd4.a((Object) instance2, "end");
                        instance2.setTimeInMillis((long) realEndTime);
                        ug4 b = nh4.b();
                        C0166Anon1 anon1 = new C0166Anon1(this, instance, instance2, (yb4) null);
                        this.L$Anon0 = zg4;
                        this.I$Anon0 = realStartTime;
                        this.I$Anon1 = realEndTime;
                        this.L$Anon1 = instance;
                        this.L$Anon2 = instance2;
                        this.label = 1;
                        if (yf4.a(b, anon1, this) == a) {
                            return a;
                        }
                    }
                } else if (i == 1) {
                    Calendar calendar = (Calendar) this.L$Anon2;
                    Calendar calendar2 = (Calendar) this.L$Anon1;
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (this.this$Anon0.a.$cancellableContinuation.isActive()) {
                    dg4 dg4 = this.this$Anon0.a.$cancellableContinuation;
                    Boolean a2 = dc4.a(true);
                    Result.a aVar = Result.Companion;
                    dg4.resumeWith(Result.m3constructorimpl(a2));
                }
                return qa4.a;
            }
        }

        @DexIgnore
        public Anon1(PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2 pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2) {
            this.a = pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2;
        }

        @DexIgnore
        public void onFail(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PushPendingDataWorker", "SleepSessionRepository.pushPendingSleepSessions onFail, go to next, errorCode = " + i);
            if (this.a.$cancellableContinuation.isActive()) {
                dg4 dg4 = this.a.$cancellableContinuation;
                Result.a aVar = Result.Companion;
                dg4.resumeWith(Result.m3constructorimpl(true));
            }
        }

        @DexIgnore
        public void onSuccess(List<MFSleepSession> list) {
            kd4.b(list, "sleepSessionList");
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "SleepSessionRepository.pushPendingSleepSessions onSuccess, go to next");
            fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new C0165Anon1(this, list, (yb4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2(dg4 dg4, yb4 yb4, PushPendingDataWorker pushPendingDataWorker) {
        super(2, yb4);
        this.$cancellableContinuation = dg4;
        this.this$Anon0 = pushPendingDataWorker;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2 pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2 = new PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2(this.$cancellableContinuation, yb4, this.this$Anon0);
        pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2.p$ = (zg4) obj;
        return pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            this.this$Anon0.m.pushPendingSleepSessions(new Anon1(this));
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
