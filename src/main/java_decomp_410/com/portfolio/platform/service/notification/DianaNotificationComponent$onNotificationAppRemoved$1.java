package com.portfolio.platform.service.notification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.notification.DianaNotificationComponent$onNotificationAppRemoved$1", mo27670f = "DianaNotificationComponent.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class DianaNotificationComponent$onNotificationAppRemoved$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.service.notification.StatusBarNotification $sbn;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21621p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.notification.DianaNotificationComponent this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaNotificationComponent$onNotificationAppRemoved$1(com.portfolio.platform.service.notification.DianaNotificationComponent dianaNotificationComponent, android.service.notification.StatusBarNotification statusBarNotification, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = dianaNotificationComponent;
        this.$sbn = statusBarNotification;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.notification.DianaNotificationComponent$onNotificationAppRemoved$1 dianaNotificationComponent$onNotificationAppRemoved$1 = new com.portfolio.platform.service.notification.DianaNotificationComponent$onNotificationAppRemoved$1(this.this$0, this.$sbn, yb4);
        dianaNotificationComponent$onNotificationAppRemoved$1.f21621p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dianaNotificationComponent$onNotificationAppRemoved$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.notification.DianaNotificationComponent$onNotificationAppRemoved$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.service.notification.DianaNotificationComponent.C6078d dVar = new com.portfolio.platform.service.notification.DianaNotificationComponent.C6078d(this.$sbn);
            com.portfolio.platform.service.notification.DianaNotificationComponent.C6076b bVar = com.portfolio.platform.service.notification.DianaNotificationComponent.f21585l.mo40063a().get(dVar.mo40077b());
            if (bVar != null) {
                dVar.mo40088e(bVar.mo40064a().getAppName());
            }
            this.this$0.mo40055d(new com.portfolio.platform.service.notification.DianaNotificationComponent.C6078d(this.$sbn));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
