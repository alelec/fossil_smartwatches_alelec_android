package com.portfolio.platform.service.notification;

import android.content.Context;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qv1;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.notification.HybridMessageNotificationComponent;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.notification.HybridMessageNotificationComponent$handleNotificationPosted$Anon1", f = "HybridMessageNotificationComponent.kt", l = {}, m = "invokeSuspend")
public final class HybridMessageNotificationComponent$handleNotificationPosted$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ StatusBarNotification $sbn;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HybridMessageNotificationComponent this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridMessageNotificationComponent$handleNotificationPosted$Anon1(HybridMessageNotificationComponent hybridMessageNotificationComponent, StatusBarNotification statusBarNotification, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = hybridMessageNotificationComponent;
        this.$sbn = statusBarNotification;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // HybridMessageNotificationComponent$handleNotificationPosted$Anon1 hybridMessageNotificationComponent$handleNotificationPosted$Anon1 = new HybridMessageNotificationComponent$handleNotificationPosted$Anon1(this.this$Anon0, this.$sbn, yb4);
        // hybridMessageNotificationComponent$handleNotificationPosted$Anon1.p$ = (zg4) obj;
        // return hybridMessageNotificationComponent$handleNotificationPosted$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((HybridMessageNotificationComponent$handleNotificationPosted$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ea  */
    public final Object invokeSuspend(Object obj) {
        throw null;
        // boolean z;
        // cc4.a();
        // if (this.label == 0) {
        //     na4.a(obj);
        //     if (this.this$Anon0.b(this.$sbn)) {
        //         HybridMessageNotificationComponent hybridMessageNotificationComponent = this.this$Anon0;
        //         Context applicationContext = PortfolioApp.W.c().getApplicationContext();
        //         kd4.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
        //         HybridMessageNotificationComponent.b a = hybridMessageNotificationComponent.b(applicationContext, this.this$Anon0.d);
        //         if (a != null) {
        //             String a2 = a.a();
        //             long b = a.b();
        //             if (!TextUtils.isEmpty(a2) && this.this$Anon0.c.c() < b) {
        //                 z = false;
        //                 for (String str : StringsKt__StringsKt.a((CharSequence) a2, new String[]{" "}, false, 0, 6, (Object) null)) {
        //                     if (str != null) {
        //                         Long a3 = qv1.a(StringsKt__StringsKt.d(str).toString());
        //                         if (a3 != null) {
        //                             long longValue = a3.longValue();
        //                             HybridMessageNotificationComponent hybridMessageNotificationComponent2 = this.this$Anon0;
        //                             Context applicationContext2 = PortfolioApp.W.c().getApplicationContext();
        //                             kd4.a((Object) applicationContext2, "PortfolioApp.instance.applicationContext");
        //                             List a4 = hybridMessageNotificationComponent2.a(applicationContext2, longValue);
        //                             int i = 0;
        //                             for (Object next : a4) {
        //                                 int i2 = i + 1;
        //                                 if (i >= 0) {
        //                                     String str2 = (String) next;
        //                                     int intValue = dc4.a(i).intValue();
        //                                     if (!TextUtils.isEmpty(str2)) {
        //                                         this.this$Anon0.c = a;
        //                                         HybridMessageNotificationComponent hybridMessageNotificationComponent3 = this.this$Anon0;
        //                                         String packageName = this.$sbn.getPackageName();
        //                                         kd4.a((Object) packageName, "sbn.packageName");
        //                                         hybridMessageNotificationComponent3.a(str2, packageName, intValue == a4.size() - 1);
        //                                         z = true;
        //                                     }
        //                                     i = i2;
        //                                 } else {
        //                                     cb4.c();
        //                                     throw null;
        //                                 }
        //                             }
        //                             continue;
        //                         }
        //                     } else {
        //                         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        //                     }
        //                 }
        //                 if (!z) {
        //                     String tag = this.$sbn.getTag();
        //                     if (tag != null && StringsKt__StringsKt.a((CharSequence) tag, (CharSequence) "one_to_one", true)) {
        //                         String packageName2 = this.$sbn.getPackageName();
        //                         CharSequence charSequence = this.$sbn.getNotification().tickerText;
        //                         if (charSequence != null) {
        //                             try {
        //                                 int length = charSequence.length();
        //                                 int i3 = 0;
        //                                 while (true) {
        //                                     if (i3 >= length) {
        //                                         i3 = -1;
        //                                         break;
        //                                     } else if (dc4.a(kd4.a((Object) String.valueOf(dc4.a(charSequence.charAt(i3)).charValue()), (Object) ":")).booleanValue()) {
        //                                         break;
        //                                     } else {
        //                                         i3++;
        //                                     }
        //                                 }
        //                                 if (i3 != -1) {
        //                                     String obj2 = charSequence.subSequence(0, i3).toString();
        //                                     if (obj2 != null) {
        //                                         packageName2 = StringsKt__StringsKt.d(obj2).toString();
        //                                     } else {
        //                                         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        //                                     }
        //                                 }
        //                             } catch (Exception e) {
        //                                 FLogger.INSTANCE.getLocal().d("HybridMessageNotificationComponent", e.getMessage());
        //                             }
        //                         }
        //                         HybridMessageNotificationComponent hybridMessageNotificationComponent4 = this.this$Anon0;
        //                         kd4.a((Object) packageName2, "contact");
        //                         String packageName3 = this.$sbn.getPackageName();
        //                         kd4.a((Object) packageName3, "sbn.packageName");
        //                         hybridMessageNotificationComponent4.a(packageName2, packageName3, true);
        //                     }
        //                 }
        //             }
        //         }
        //         z = false;
        //         if (!z) {
        //         }
        //     }
        //     return qa4.a;
        // }
        // throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
