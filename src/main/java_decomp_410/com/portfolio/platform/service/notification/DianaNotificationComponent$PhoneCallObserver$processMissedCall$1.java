package com.portfolio.platform.service.notification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$processMissedCall$1", mo27670f = "DianaNotificationComponent.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class DianaNotificationComponent$PhoneCallObserver$processMissedCall$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21620p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.notification.DianaNotificationComponent.PhoneCallObserver this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaNotificationComponent$PhoneCallObserver$processMissedCall$1(com.portfolio.platform.service.notification.DianaNotificationComponent.PhoneCallObserver phoneCallObserver, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = phoneCallObserver;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$processMissedCall$1 dianaNotificationComponent$PhoneCallObserver$processMissedCall$1 = new com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$processMissedCall$1(this.this$0, yb4);
        dianaNotificationComponent$PhoneCallObserver$processMissedCall$1.f21620p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dianaNotificationComponent$PhoneCallObserver$processMissedCall$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$processMissedCall$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.service.notification.DianaNotificationComponent.PhoneCallObserver.C6074a b = this.this$0.mo40056a();
            if (b != null) {
                com.portfolio.platform.service.notification.DianaNotificationComponent.PhoneCallObserver.C6074a a = this.this$0.f21595a;
                if (a != null) {
                    if (a.mo40060a() < b.mo40060a()) {
                        if (b.mo40062c() == 3) {
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String b2 = com.portfolio.platform.service.notification.DianaNotificationComponent.this.mo40045b();
                            local.mo33255d(b2, "processMissedCall - number = " + b.mo40061b());
                            com.portfolio.platform.service.notification.DianaNotificationComponent.this.mo40035a(b.mo40061b(), new java.util.Date(), com.portfolio.platform.service.notification.DianaNotificationComponent.PhoneStateEnum.MISSED);
                        } else if (b.mo40062c() == 5 || b.mo40062c() == 6 || b.mo40062c() == 1) {
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String b3 = com.portfolio.platform.service.notification.DianaNotificationComponent.this.mo40045b();
                            local2.mo33255d(b3, "processHangUpCall - number = " + b.mo40061b());
                            com.portfolio.platform.service.notification.DianaNotificationComponent.this.mo40035a(b.mo40061b(), new java.util.Date(), com.portfolio.platform.service.notification.DianaNotificationComponent.PhoneStateEnum.PICKED);
                        }
                    }
                    this.this$0.f21595a = b;
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
