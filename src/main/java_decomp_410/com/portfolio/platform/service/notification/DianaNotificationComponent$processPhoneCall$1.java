package com.portfolio.platform.service.notification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processPhoneCall$1", mo27670f = "DianaNotificationComponent.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class DianaNotificationComponent$processPhoneCall$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $phoneNumber;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $startTime;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.notification.DianaNotificationComponent.PhoneStateEnum $state;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21623p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.notification.DianaNotificationComponent this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaNotificationComponent$processPhoneCall$1(com.portfolio.platform.service.notification.DianaNotificationComponent dianaNotificationComponent, com.portfolio.platform.service.notification.DianaNotificationComponent.PhoneStateEnum phoneStateEnum, java.lang.String str, java.util.Date date, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = dianaNotificationComponent;
        this.$state = phoneStateEnum;
        this.$phoneNumber = str;
        this.$startTime = date;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.notification.DianaNotificationComponent$processPhoneCall$1 dianaNotificationComponent$processPhoneCall$1 = new com.portfolio.platform.service.notification.DianaNotificationComponent$processPhoneCall$1(this.this$0, this.$state, this.$phoneNumber, this.$startTime, yb4);
        dianaNotificationComponent$processPhoneCall$1.f21623p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dianaNotificationComponent$processPhoneCall$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.notification.DianaNotificationComponent$processPhoneCall$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            int i = com.fossil.blesdk.obfuscated.up2.f19210a[this.$state.ordinal()];
            if (i != 1) {
                if (i == 2) {
                    com.portfolio.platform.service.notification.DianaNotificationComponent.C6080e a = this.this$0.f21590e;
                    if (a != null && com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) a.mo40100l(), (java.lang.Object) this.$phoneNumber)) {
                        com.portfolio.platform.service.notification.DianaNotificationComponent.C6080e eVar = new com.portfolio.platform.service.notification.DianaNotificationComponent.C6080e(com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL.getPackageName(), "", this.$phoneNumber, "", this.$startTime);
                        eVar.mo40086d(a.mo40085d());
                        eVar.mo40073a(a.mo40070a());
                        this.this$0.mo40055d((com.portfolio.platform.service.notification.DianaNotificationComponent.C6078d) eVar);
                        this.this$0.f21590e = null;
                    }
                    if (android.os.Build.VERSION.SDK_INT >= 28) {
                        com.portfolio.platform.service.notification.DianaNotificationComponent dianaNotificationComponent = this.this$0;
                        android.content.Context applicationContext = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().getApplicationContext();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) applicationContext, "PortfolioApp.instance.applicationContext");
                        dianaNotificationComponent.mo40046b(applicationContext);
                    }
                } else if (i == 3) {
                    if (!this.this$0.mo40043a(this.$phoneNumber, true)) {
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    com.portfolio.platform.service.notification.DianaNotificationComponent.C6080e a2 = this.this$0.f21590e;
                    if (a2 != null && com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) a2.mo40100l(), (java.lang.Object) this.$phoneNumber)) {
                        com.portfolio.platform.service.notification.DianaNotificationComponent.C6080e eVar2 = new com.portfolio.platform.service.notification.DianaNotificationComponent.C6080e(com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL.getPackageName(), "", this.$phoneNumber, "", this.$startTime);
                        eVar2.mo40073a(a2.mo40070a());
                        eVar2.mo40086d(a2.mo40085d());
                        this.this$0.mo40055d((com.portfolio.platform.service.notification.DianaNotificationComponent.C6078d) eVar2);
                        this.this$0.f21590e = null;
                    }
                    java.lang.String a3 = this.this$0.mo40054d(this.$phoneNumber);
                    com.portfolio.platform.service.notification.DianaNotificationComponent dianaNotificationComponent2 = this.this$0;
                    java.lang.String packageName = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_MISSED_CALL.getPackageName();
                    java.lang.String str = this.$phoneNumber;
                    java.util.Calendar instance = java.util.Calendar.getInstance();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "Calendar.getInstance()");
                    java.util.Date time = instance.getTime();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) time, "Calendar.getInstance().time");
                    com.portfolio.platform.service.notification.DianaNotificationComponent.C6080e eVar3 = new com.portfolio.platform.service.notification.DianaNotificationComponent.C6080e(packageName, a3, str, "Missed Call", time);
                    dianaNotificationComponent2.mo40047b((com.portfolio.platform.service.notification.DianaNotificationComponent.C6078d) eVar3);
                    if (android.os.Build.VERSION.SDK_INT >= 28) {
                        com.portfolio.platform.service.notification.DianaNotificationComponent dianaNotificationComponent3 = this.this$0;
                        android.content.Context applicationContext2 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().getApplicationContext();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) applicationContext2, "PortfolioApp.instance.applicationContext");
                        dianaNotificationComponent3.mo40046b(applicationContext2);
                    }
                }
            } else if (!this.this$0.mo40043a(this.$phoneNumber, true)) {
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else {
                if (android.os.Build.VERSION.SDK_INT >= 28) {
                    com.portfolio.platform.service.notification.DianaNotificationComponent dianaNotificationComponent4 = this.this$0;
                    android.content.Context applicationContext3 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().getApplicationContext();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) applicationContext3, "PortfolioApp.instance.applicationContext");
                    dianaNotificationComponent4.mo40046b(applicationContext3);
                    com.portfolio.platform.service.notification.DianaNotificationComponent dianaNotificationComponent5 = this.this$0;
                    android.content.Context applicationContext4 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().getApplicationContext();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) applicationContext4, "PortfolioApp.instance.applicationContext");
                    dianaNotificationComponent5.mo40038a(applicationContext4);
                }
                java.lang.String a4 = this.this$0.mo40054d(this.$phoneNumber);
                com.portfolio.platform.service.notification.DianaNotificationComponent.C6080e a5 = this.this$0.f21590e;
                if (a5 != null) {
                    this.this$0.mo40055d((com.portfolio.platform.service.notification.DianaNotificationComponent.C6078d) a5);
                }
                com.portfolio.platform.service.notification.DianaNotificationComponent.C6080e eVar4 = new com.portfolio.platform.service.notification.DianaNotificationComponent.C6080e(com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL.getPackageName(), a4, this.$phoneNumber, "Incoming Call", this.$startTime);
                this.this$0.f21590e = eVar4.clone();
                this.this$0.mo40047b((com.portfolio.platform.service.notification.DianaNotificationComponent.C6078d) eVar4);
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
