package com.portfolio.platform.service.notification;

import android.service.notification.StatusBarNotification;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processAppNotification$Anon1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
public final class DianaNotificationComponent$processAppNotification$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ StatusBarNotification $sbn;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaNotificationComponent this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaNotificationComponent$processAppNotification$Anon1(DianaNotificationComponent dianaNotificationComponent, StatusBarNotification statusBarNotification, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = dianaNotificationComponent;
        this.$sbn = statusBarNotification;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // DianaNotificationComponent$processAppNotification$Anon1 dianaNotificationComponent$processAppNotification$Anon1 = new DianaNotificationComponent$processAppNotification$Anon1(this.this$Anon0, this.$sbn, yb4);
        // dianaNotificationComponent$processAppNotification$Anon1.p$ = (zg4) obj;
        // return dianaNotificationComponent$processAppNotification$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((DianaNotificationComponent$processAppNotification$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            this.this$Anon0.c(new DianaNotificationComponent.d(this.$sbn));
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
