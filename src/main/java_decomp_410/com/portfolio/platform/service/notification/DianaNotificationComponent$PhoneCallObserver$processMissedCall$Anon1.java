package com.portfolio.platform.service.notification;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
public final class DianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaNotificationComponent.PhoneCallObserver this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1(DianaNotificationComponent.PhoneCallObserver phoneCallObserver, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = phoneCallObserver;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // DianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1 dianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1 = new DianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1(this.this$Anon0, yb4);
        // dianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1.p$ = (zg4) obj;
        // return dianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((DianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        throw null;
        // cc4.a();
        // if (this.label == 0) {
        //     na4.a(obj);
        //     DianaNotificationComponent.PhoneCallObserver.a b = this.this$Anon0.a();
        //     if (b != null) {
        //         DianaNotificationComponent.PhoneCallObserver.a a = this.this$Anon0.a;
        //         if (a != null) {
        //             if (a.a() < b.a()) {
        //                 if (b.c() == 3) {
        //                     ILocalFLogger local = FLogger.INSTANCE.getLocal();
        //                     String b2 = DianaNotificationComponent.this.b();
        //                     local.d(b2, "processMissedCall - number = " + b.b());
        //                     DianaNotificationComponent.this.a(b.b(), new Date(), DianaNotificationComponent.PhoneStateEnum.MISSED);
        //                 } else if (b.c() == 5 || b.c() == 6 || b.c() == 1) {
        //                     ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        //                     String b3 = DianaNotificationComponent.this.b();
        //                     local2.d(b3, "processHangUpCall - number = " + b.b());
        //                     DianaNotificationComponent.this.a(b.b(), new Date(), DianaNotificationComponent.PhoneStateEnum.PICKED);
        //                 }
        //             }
        //             this.this$Anon0.a = b;
        //         } else {
        //             kd4.a();
        //             throw null;
        //         }
        //     }
        //     return qa4.a;
        // }
        // throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
