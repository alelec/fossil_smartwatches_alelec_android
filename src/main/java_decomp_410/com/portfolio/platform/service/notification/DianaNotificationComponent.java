package com.portfolio.platform.service.notification;

import android.app.Notification;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.ns3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qc4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.j256.ormlite.field.FieldType;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.text.Regex;
import kotlin.text.StringsKt__StringsKt;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaNotificationComponent {
    @DexIgnore
    public static /* final */ HashMap<String, b> j;
    @DexIgnore
    public static /* final */ HashMap<String, g> k;
    @DexIgnore
    public static /* final */ a l; // = new a((fd4) null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ c b; // = new c(this.g);
    @DexIgnore
    public int c; // = 1380;
    @DexIgnore
    public int d; // = 1140;
    @DexIgnore
    public e e;
    @DexIgnore
    public PhoneCallObserver f;
    @DexIgnore
    public /* final */ en2 g;
    @DexIgnore
    public /* final */ DNDSettingsDatabase h;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class PhoneCallObserver extends ContentObserver {
        @DexIgnore
        public a a; // = new a(this, 0, (String) null, 0, 0, 15, (fd4) null);

        @DexEdit(defaultAction = DexAction.IGNORE)
        @gc4(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$Anon1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
        public final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ PhoneCallObserver this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(PhoneCallObserver phoneCallObserver, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = phoneCallObserver;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                throw null;
                // kd4.b(yb4, "completion");
                // Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
                // anon1.p$ = (zg4) obj;
                // return anon1;
            }

            @DexIgnore
            public final Object invoke(zg4 obj, yb4<? super qa4> obj2) {
                throw null;
                // return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                throw null;
                // cc4.a();
                // if (this.label == 0) {
                //     na4.a(obj);
                //     PhoneCallObserver phoneCallObserver = this.this$Anon0;
                //     a b = phoneCallObserver.a();
                //     if (b == null) {
                //         b = new a(this.this$Anon0, 0, (String) null, 0, 0, 15, (fd4) null);
                //     }
                //     phoneCallObserver.a = b;
                //     return qa4.a;
                // }
                // throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }

            @Override
            public Object invoke(Object o, Object o2) {
                return null;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public final class a {
            @DexIgnore
            public /* final */ long a;
            @DexIgnore
            public /* final */ String b;
            @DexIgnore
            public /* final */ int c;

            @DexIgnore
            public a(PhoneCallObserver phoneCallObserver, long j, String str, int i, long j2) {
                kd4.b(str, "number");
                this.a = j;
                this.b = str;
                this.c = i;
            }

            @DexIgnore
            public final long a() {
                return this.a;
            }

            @DexIgnore
            public final String b() {
                return this.b;
            }

            @DexIgnore
            public final int c() {
                return this.c;
            }

            @DexIgnore
            /* JADX WARNING: Illegal instructions before constructor call */
            public /* synthetic */ a(PhoneCallObserver phoneCallObserver, long j, String str, int i, long j2, int i2, fd4 fd4) {
                throw null;
                // this(phoneCallObserver, r0, (i2 & 2) != 0 ? "" : str, (i2 & 4) != 0 ? 0 : i, (i2 & 8) != 0 ? 0 : j2);
                // long j3 = (i2 & 1) != 0 ? -1 : j;
            }
        }

        @DexIgnore
        public PhoneCallObserver() {
            super((Handler) null);
            throw null;
            // fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (yb4) null), 3, (Object) null);
        }

        @DexIgnore
        public void onChange(boolean z, Uri uri) {
            b();
        }

        @DexIgnore
        public final void b() {
            throw null;
            // ns3.a aVar = ns3.a;
            // Context applicationContext = PortfolioApp.W.c().getApplicationContext();
            // kd4.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            // if (!aVar.d(applicationContext)) {
            //     FLogger.INSTANCE.getLocal().d(DianaNotificationComponent.this.b(), "processMissedCall() is not executed because permissions has not granted");
            // } else {
            //     fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new DianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1(this, (yb4) null), 3, (Object) null);
            // }
        }

        @DexIgnore
        public void onChange(boolean z) {
            b();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0065, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
            com.fossil.blesdk.obfuscated.qc4.a(r4, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0069, code lost:
            throw r1;
         */
        @DexIgnore
        public final a a() {
            throw null;
            // Cursor query = PortfolioApp.W.c().getContentResolver().query(CallLog.Calls.CONTENT_URI, new String[]{FieldType.FOREIGN_ID_FIELD_SUFFIX, "number", "date", "type"}, (String) null, (String[]) null, "date DESC LIMIT 1");
            // if (query != null) {
            //     if (query.moveToFirst()) {
            //         long j = query.getLong(query.getColumnIndex(FieldType.FOREIGN_ID_FIELD_SUFFIX));
            //         String string = query.getString(query.getColumnIndex("number"));
            //         int i = query.getInt(query.getColumnIndex("type"));
            //         long j2 = query.getLong(query.getColumnIndex("date"));
            //         query.close();
            //         kd4.a((Object) string, "number");
            //         a aVar = new a(this, j, string, i, j2);
            //         try {
            //             qc4.a(query, (Throwable) null);
            //             return aVar;
            //         } catch (Exception e) {
            //             e.printStackTrace();
            //             FLogger.INSTANCE.getLocal().e(DianaNotificationComponent.this.b(), e.getMessage());
            //         }
            //     } else {
            //         query.close();
            //         qa4 qa4 = qa4.a;
            //         qc4.a(query, (Throwable) null);
            //     }
            // }
            // return null;
        }
    }

    @DexIgnore
    public enum PhoneStateEnum {
        RINGING,
        PICKED,
        MISSED,
        END
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final HashMap<String, b> a() {
            return DianaNotificationComponent.j;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ DianaNotificationObj.AApplicationName a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public b(int i, DianaNotificationObj.AApplicationName aApplicationName, boolean z, boolean z2) {
            kd4.b(aApplicationName, "notificationApp");
            this.a = aApplicationName;
            this.b = z2;
        }

        @DexIgnore
        public final DianaNotificationObj.AApplicationName a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public String a;
        @DexIgnore
        public int b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public long g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public boolean i;
        @DexIgnore
        public int j;
        @DexIgnore
        public boolean k;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            new a((fd4) null);
        }
        */

        @DexIgnore
        public d() {
            this.a = "";
            this.c = "";
            this.d = "";
            this.e = "";
            this.f = "";
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.c;
        }

        @DexIgnore
        public final void c(String str) {
            kd4.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final String d() {
            return this.a;
        }

        @DexIgnore
        public final String e() {
            return this.e;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof d)) {
                return false;
            }
            d dVar = (d) obj;
            if (!kd4.a((Object) this.a, (Object) dVar.a) || this.g != dVar.g) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public final String f() {
            return this.f;
        }

        @DexIgnore
        public final String g() {
            return this.d;
        }

        @DexIgnore
        public final long h() {
            return this.g;
        }

        @DexIgnore
        public int hashCode() {
            return 0;
        }

        @DexIgnore
        public final boolean i() {
            return this.k;
        }

        @DexIgnore
        public final boolean j() {
            return this.i;
        }

        @DexIgnore
        public final boolean k() {
            return this.h;
        }

        @DexIgnore
        public final void a(int i2) {
            this.b = i2;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.i = z;
        }

        @DexIgnore
        public final void c(boolean z) {
            this.h = z;
        }

        @DexIgnore
        public final void d(String str) {
            kd4.b(str, "<set-?>");
            this.a = str;
        }

        @DexIgnore
        public final void e(String str) {
            kd4.b(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final void f(String str) {
            kd4.b(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final void g(String str) {
            kd4.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void a(long j2) {
            this.g = j2;
        }

        @DexIgnore
        public final String b(String str) {
            throw null;
            // String str2 = (String) kb4.f(StringsKt__StringsKt.a((CharSequence) str, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0, 6, (Object) null));
            // PackageManager packageManager = PortfolioApp.W.c().getPackageManager();
            // try {
            //     ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 0);
            //     return applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo).toString() : str2;
            // } catch (PackageManager.NameNotFoundException e2) {
            //     FLogger.INSTANCE.getLocal().e("NotificationStatus", e2.getMessage());
            //     return str2;
            // }
        }

        @DexIgnore
        public final int c() {
            return this.j;
        }

        @DexIgnore
        public final void a(boolean z) {
            this.k = z;
        }

        @DexIgnore
        public final boolean c(StatusBarNotification statusBarNotification) {
            return (statusBarNotification.getNotification().flags & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) == 512;
        }

        @DexIgnore
        public final boolean a(StatusBarNotification statusBarNotification) {
            String tag = statusBarNotification.getTag();
            if (tag == null || (!StringsKt__StringsKt.a((CharSequence) tag, (CharSequence) "sms", true) && !StringsKt__StringsKt.a((CharSequence) tag, (CharSequence) "mms", true) && !StringsKt__StringsKt.a((CharSequence) tag, (CharSequence) "rcs", true))) {
                return TextUtils.equals(statusBarNotification.getNotification().category, "msg");
            }
            return true;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        /* JADX WARNING: Code restructure failed: missing block: B:5:0x003c, code lost:
            if (r0 != null) goto L_0x0045;
         */
        @DexIgnore
        public d(StatusBarNotification statusBarNotification) {
            this();
            throw null;
            // CharSequence charSequence;
            // String str;
            // CharSequence charSequence2;
            // kd4.b(statusBarNotification, "sbn");
            // this.a = a((long) statusBarNotification.getId(), statusBarNotification.getTag());
            // String packageName = statusBarNotification.getPackageName();
            // kd4.a((Object) packageName, "sbn.packageName");
            // this.c = packageName;
            // b bVar = DianaNotificationComponent.l.a().get(this.c);
            // if (bVar != null) {
            //     DianaNotificationObj.AApplicationName a2 = bVar.a();
            //     if (a2 != null) {
            //         charSequence = a2.getAppName();
            //     }
            // }
            // charSequence = b(this.c);
            // CharSequence charSequence3 = statusBarNotification.getNotification().extras.getCharSequence("android.title");
            // this.d = (charSequence3 != null ? charSequence3 : charSequence).toString();
            // this.e = a(this.d);
            // CharSequence charSequence4 = statusBarNotification.getNotification().extras.getCharSequence("android.bigText");
            // if (!(charSequence4 == null || qf4.a(charSequence4))) {
            //     Notification notification = statusBarNotification.getNotification();
            //     if (notification != null) {
            //         Bundle bundle = notification.extras;
            //         if (bundle != null) {
            //             charSequence2 = bundle.getCharSequence("android.bigText");
            //             str = a(String.valueOf(charSequence2));
            //         }
            //     }
            //     charSequence2 = null;
            //     str = a(String.valueOf(charSequence2));
            // } else {
            //     String charSequence5 = statusBarNotification.getNotification().extras.getCharSequence("android.text");
            //     str = a((charSequence5 == null ? "empty" : charSequence5).toString());
            // }
            // this.f = str;
            // this.h = c(statusBarNotification);
            // this.i = b(statusBarNotification);
            // this.g = statusBarNotification.getNotification().when;
            // this.j = statusBarNotification.getNotification().priority;
            // this.k = statusBarNotification.getNotification().extras.getInt("android.progressMax", 0) != 0;
            // if (a(statusBarNotification)) {
            //     CharSequence charSequence6 = statusBarNotification.getNotification().tickerText;
            //     if (charSequence6 != null) {
            //         int length = charSequence6.length();
            //         int i2 = 0;
            //         while (true) {
            //             if (i2 >= length) {
            //                 i2 = -1;
            //                 break;
            //             } else if (kd4.a((Object) String.valueOf(charSequence6.charAt(i2)), (Object) ":")) {
            //                 break;
            //             } else {
            //                 i2++;
            //             }
            //         }
            //         if (i2 != -1) {
            //             String obj = charSequence6.subSequence(0, i2).toString();
            //             if (obj != null) {
            //                 this.e = StringsKt__StringsKt.d(obj).toString();
            //                 String obj2 = charSequence6.subSequence(i2 + 1, charSequence6.length()).toString();
            //                 if (obj2 != null) {
            //                     this.f = StringsKt__StringsKt.d(obj2).toString();
            //                     return;
            //                 }
            //                 throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            //             }
            //             throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            //         }
            //     }
            // }
        }

        @DexIgnore
        public final String a(String str) {
            String replace = new Regex("\\p{C}").replace((CharSequence) str, "");
            int length = replace.length() - 1;
            int i2 = 0;
            boolean z = false;
            while (i2 <= length) {
                boolean z2 = replace.charAt(!z ? i2 : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i2++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            return replace.subSequence(i2, length + 1).toString();
        }

        @DexIgnore
        public final boolean b(StatusBarNotification statusBarNotification) {
            return (statusBarNotification.getNotification().flags & 2) == 2;
        }

        @DexIgnore
        public final String a(long j2, String str) {
            return j2 + ':' + str;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends d {
        @DexIgnore
        public String l;

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public final e clone() {
            e eVar = new e();
            eVar.d(d());
            eVar.a(a());
            eVar.c(b());
            eVar.e(e());
            eVar.f(f());
            eVar.g(g());
            eVar.a(h());
            eVar.a(i());
            eVar.c(k());
            eVar.b(j());
            eVar.l = this.l;
            return eVar;
        }

        @DexIgnore
        public final String l() {
            return this.l;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public e(String str, String str2, String str3, String str4, Date date) {
            this();
            kd4.b(str, "packageName");
            kd4.b(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
            kd4.b(str3, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            kd4.b(str4, "message");
            kd4.b(date, GoalPhase.COLUMN_START_DATE);
            d(a(date.getTime(), (String) null));
            c(str);
            g(str2);
            e(str2);
            f(str4);
            a(date.getTime());
            this.l = str3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends d {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public f(String str, String str2, long j) {
            this();
            kd4.b(str, RemoteFLogger.MESSAGE_SENDER_KEY);
            kd4.b(str2, "message");
            d(a(j, (String) null));
            c(DianaNotificationObj.AApplicationName.MESSAGES.getPackageName());
            g(str);
            e(str);
            f(str2);
            a(j);
        }
    }

    /*
    static {
        HashMap<String, b> hashMap = new HashMap<>();
        hashMap.put(DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL.getPackageName(), new b(1, DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL, false, true));
        hashMap.put(DianaNotificationObj.AApplicationName.PHONE_MISSED_CALL.getPackageName(), new b(2, DianaNotificationObj.AApplicationName.PHONE_MISSED_CALL, false, true));
        hashMap.put(DianaNotificationObj.AApplicationName.MESSAGES.getPackageName(), new b(3, DianaNotificationObj.AApplicationName.MESSAGES, false, false));
        hashMap.put(DianaNotificationObj.AApplicationName.HANGOUTS.getPackageName(), new b(4, DianaNotificationObj.AApplicationName.HANGOUTS, false, true));
        hashMap.put(DianaNotificationObj.AApplicationName.GOOGLE_CALENDAR.getPackageName(), new b(5, DianaNotificationObj.AApplicationName.GOOGLE_CALENDAR, false, true));
        hashMap.put(DianaNotificationObj.AApplicationName.GMAIL.getPackageName(), new b(6, DianaNotificationObj.AApplicationName.GMAIL, false, true));
        hashMap.put(DianaNotificationObj.AApplicationName.WHATSAPP.getPackageName(), new b(7, DianaNotificationObj.AApplicationName.WHATSAPP, true, true));
        hashMap.put(DianaNotificationObj.AApplicationName.FACEBOOK.getPackageName(), new b(8, DianaNotificationObj.AApplicationName.FACEBOOK, false, false));
        hashMap.put(DianaNotificationObj.AApplicationName.MESSENGER.getPackageName(), new b(8, DianaNotificationObj.AApplicationName.MESSENGER, false, false));
        hashMap.put(DianaNotificationObj.AApplicationName.TWITTER.getPackageName(), new b(9, DianaNotificationObj.AApplicationName.TWITTER, false, false));
        hashMap.put(DianaNotificationObj.AApplicationName.INSTAGRAM.getPackageName(), new b(10, DianaNotificationObj.AApplicationName.INSTAGRAM, false, false));
        hashMap.put(DianaNotificationObj.AApplicationName.SNAPCHAT.getPackageName(), new b(11, DianaNotificationObj.AApplicationName.SNAPCHAT, false, false));
        hashMap.put(DianaNotificationObj.AApplicationName.WECHAT.getPackageName(), new b(12, DianaNotificationObj.AApplicationName.WECHAT, false, false));
        hashMap.put(DianaNotificationObj.AApplicationName.WEIBO.getPackageName(), new b(13, DianaNotificationObj.AApplicationName.WEIBO, false, false));
        hashMap.put(DianaNotificationObj.AApplicationName.LINE.getPackageName(), new b(14, DianaNotificationObj.AApplicationName.LINE, false, false));
        hashMap.put(DianaNotificationObj.AApplicationName.GOOGLE_FIT.getPackageName(), new b(15, DianaNotificationObj.AApplicationName.GOOGLE_FIT, false, false));
        j = hashMap;
        HashMap<String, g> hashMap2 = new HashMap<>();
        hashMap2.put("com.facebook.orca", new g("com.facebook.orca", "SMS"));
        k = hashMap2;
    }
    */

    @DexIgnore
    public DianaNotificationComponent(en2 en2, DNDSettingsDatabase dNDSettingsDatabase, NotificationSettingsDatabase notificationSettingsDatabase) {
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(dNDSettingsDatabase, "mDNDndSettingsDatabase");
        kd4.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.g = en2;
        this.h = dNDSettingsDatabase;
        this.i = notificationSettingsDatabase;
        String simpleName = DianaNotificationComponent.class.getSimpleName();
        kd4.a((Object) simpleName, "DianaNotificationComponent::class.java.simpleName");
        this.a = simpleName;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0104, code lost:
        if (r6 < r0) goto L_0x012d;
     */
    @DexIgnore
    public final boolean c() {
        if (this.g.E()) {
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "Calendar.getInstance()");
            Date n = rk2.n(instance.getTime());
            kd4.a((Object) n, "DateHelper.getStartOfDay\u2026endar.getInstance().time)");
            long time = n.getTime();
            Calendar instance2 = Calendar.getInstance();
            kd4.a((Object) instance2, "Calendar.getInstance()");
            Date time2 = instance2.getTime();
            kd4.a((Object) time2, "Calendar.getInstance().time");
            long time3 = (time2.getTime() - time) / ((long) 60000);
            List<DNDScheduledTimeModel> listDNDScheduledTimeModel = this.h.getDNDScheduledTimeDao().getListDNDScheduledTimeModel();
            if (!listDNDScheduledTimeModel.isEmpty()) {
                for (DNDScheduledTimeModel next : listDNDScheduledTimeModel) {
                    int component2 = next.component2();
                    if (next.component3() == 0) {
                        this.c = component2;
                    } else {
                        this.d = component2;
                    }
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.d(str, "currentMinutesOfDay = " + a((int) time3) + " -- mDNDStartTime = " + a(this.c) + " -- mDNDEndTime = " + a(this.d));
            int i2 = this.d;
            int i3 = this.c;
            if (i2 > i3) {
                FLogger.INSTANCE.getLocal().d(this.a, "case mDNDEndTime greater than mDNDStartTime");
                long j2 = (long) this.d;
                if (((long) this.c) <= time3 && j2 >= time3) {
                    FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is valid - ignore notification ");
                    return true;
                }
            } else if (i2 < i3) {
                FLogger.INSTANCE.getLocal().d(this.a, "case mDNDEndTime smaller than mDNDStartTime");
                long j3 = (long) DateTimeConstants.MINUTES_PER_DAY;
                if (((long) this.c) > time3 || j3 < time3) {
                    long j4 = (long) this.d;
                    if (0 <= time3) {
                    }
                }
                FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is valid - ignore notification ");
                return true;
            } else if (i2 == i3) {
                FLogger.INSTANCE.getLocal().d(this.a, "case mDNDEndTime equals mDNDStartTime");
                FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is valid - ignore notification ");
                return true;
            }
        }
        FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is not valid - show notification ");
        return false;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ List<d> a; // = new ArrayList();
        @DexIgnore
        public int b; // = this.c.o();
        @DexIgnore
        public /* final */ en2 c;

        @DexIgnore
        public c(en2 en2) {
            kd4.b(en2, "mSharedPref");
            this.c = en2;
        }

        @DexIgnore
        public final void a(int i) {
            if (i >= Integer.MAX_VALUE) {
                i = 0;
            }
            this.b = i;
            this.c.c(this.b);
        }

        @DexIgnore
        public final int a() {
            int i = this.b;
            if (i < 0) {
                return 0;
            }
            return i;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a7, code lost:
            return r11;
         */
        @DexIgnore
        public final synchronized d a(d dVar) {
            throw null;
            // T t;
            // boolean z;
            // kd4.b(dVar, "notificationStatus");
            // if (this.a.contains(dVar)) {
            //     dVar = null;
            // } else if (qf4.b(dVar.b(), Telephony.Sms.getDefaultSmsPackage(PortfolioApp.W.c()), true)) {
            //     Iterator<T> it = this.a.iterator();
            //     while (true) {
            //         if (!it.hasNext()) {
            //             t = null;
            //             break;
            //         }
            //         t = it.next();
            //         d dVar2 = (d) t;
            //         if (!qf4.b(dVar2.f(), dVar.f(), true) || (dVar2.h() != dVar.h() && dVar.h() - dVar2.h() > 1)) {
            //             z = false;
            //             continue;
            //         } else {
            //             z = true;
            //             continue;
            //         }
            //         if (z) {
            //             break;
            //         }
            //     }
            //     d dVar3 = (d) t;
            //     if (dVar3 != null) {
            //         dVar3.d(dVar.d());
            //         dVar3.a(dVar.h());
            //         return null;
            //     }
            //     dVar.a(a());
            //     this.a.add(dVar);
            //     a(a() + 1);
            // } else {
            //     dVar.a(a());
            //     this.a.add(dVar);
            //     a(a() + 1);
            // }
        }

        @DexIgnore
        public final synchronized List<d> a(String str, String str2) {
            throw null;
            // ArrayList arrayList;
            // kd4.b(str, "realId");
            // kd4.b(str2, "packageName");
            // List<d> list = this.a;
            // arrayList = new ArrayList();
            // for (T next : list) {
            //     d dVar = (d) next;
            //     if (kd4.a((Object) dVar.d(), (Object) str) && kd4.a((Object) dVar.b(), (Object) str2)) {
            //         arrayList.add(next);
            //     }
            // }
            // this.a.removeAll(arrayList);
            // return arrayList;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g {
        @DexIgnore
        public String a;

        @DexIgnore
        public g() {
            this.a = "";
        }

        @DexIgnore
        public final boolean a(String str) {
            kd4.b(str, "realId");
            return StringsKt__StringsKt.a((CharSequence) str, (CharSequence) this.a, true);
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public g(String str, String str2) {
            this();
            kd4.b(str, "packageName");
            kd4.b(str2, "tag");
            this.a = str2;
        }
    }

    @DexIgnore
    public final synchronized void d(d dVar) {
        kd4.b(dVar, "notification");
        b bVar = j.get(dVar.b());
        if (bVar == null || (!dVar.j() && dVar.k())) {
            FLogger.INSTANCE.getLocal().d(this.a, "processNotificationRemoved() not supported............");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.d(str, "  Notification Posted: " + dVar.b());
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.a;
            local2.d(str2, "  Id: " + dVar.d());
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = this.a;
            local3.d(str3, "  Sender: " + dVar.e());
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = this.a;
            local4.d(str4, "  Title: " + dVar.g());
            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            String str5 = this.a;
            local5.d(str5, "  Text: " + dVar.f());
            ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
            String str6 = this.a;
            local6.d(str6, "  Summary: " + dVar.k());
            ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
            String str7 = this.a;
            local7.d(str7, "  IsOngoing: " + dVar.j());
            ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
            String str8 = this.a;
            local8.d(str8, "  When: " + dVar.h());
            ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
            String str9 = this.a;
            local9.d(str9, "  IsDownloadEvent: " + dVar.i());
        } else {
            FLogger.INSTANCE.getLocal().d(this.a, "processNotificationRemoved() supported............");
            ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
            String str10 = this.a;
            local10.d(str10, "  Notification Posted: " + dVar.b());
            ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
            String str11 = this.a;
            local11.d(str11, "  Id: " + dVar.d());
            ILocalFLogger local12 = FLogger.INSTANCE.getLocal();
            String str12 = this.a;
            local12.d(str12, "  Sender: " + dVar.e());
            ILocalFLogger local13 = FLogger.INSTANCE.getLocal();
            String str13 = this.a;
            local13.d(str13, "  Title: " + dVar.g());
            ILocalFLogger local14 = FLogger.INSTANCE.getLocal();
            String str14 = this.a;
            local14.d(str14, "  Text: " + dVar.f());
            ILocalFLogger local15 = FLogger.INSTANCE.getLocal();
            String str15 = this.a;
            local15.d(str15, "  Summary: " + dVar.k());
            ILocalFLogger local16 = FLogger.INSTANCE.getLocal();
            String str16 = this.a;
            local16.d(str16, "  IsOngoing: " + dVar.j());
            ILocalFLogger local17 = FLogger.INSTANCE.getLocal();
            String str17 = this.a;
            local17.d(str17, "  When: " + dVar.h());
            ILocalFLogger local18 = FLogger.INSTANCE.getLocal();
            String str18 = this.a;
            local18.d(str18, "  IsDownloadEvent: " + dVar.i());
            for (d dVar2 : this.b.a(dVar.d(), dVar.b())) {
                PortfolioApp.W.c().a(a(), (NotificationBaseObj) new DianaNotificationObj(dVar2.a(), NotificationBaseObj.ANotificationType.REMOVED, bVar.a(), dVar2.g(), dVar2.e(), dVar2.f(), cb4.d(NotificationBaseObj.ANotificationFlag.IMPORTANT)));
            }
        }
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final fi4 b(StatusBarNotification statusBarNotification) {
        throw null;
        // kd4.b(statusBarNotification, "sbn");
        // return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new DianaNotificationComponent$processAppNotification$Anon1(this, statusBarNotification, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final boolean b(d dVar, b bVar) {
        if (!bVar.b() && dVar.c() <= -2) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String a() {
        return PortfolioApp.W.c().e();
    }

    @DexIgnore
    public final fi4 a(String str, Date date, PhoneStateEnum phoneStateEnum) {
        throw null;
        // kd4.b(str, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
        // kd4.b(date, SampleRaw.COLUMN_START_TIME);
        // kd4.b(phoneStateEnum, "state");
        // return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new DianaNotificationComponent$processPhoneCall$Anon1(this, phoneStateEnum, str, date, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void b(d dVar) {
        NotificationBaseObj.ANotificationType aNotificationType;
        b bVar = j.get(dVar.b());
        if (bVar == null || ((!dVar.j() && dVar.k()) || !b(dVar, bVar) || dVar.i())) {
            FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() app not supported............");
            if (bVar != null) {
                DianaNotificationObj.AApplicationName a2 = bVar.a();
                if (a2 != null) {
                    aNotificationType = a2.getNotificationType();
                    a(dVar, aNotificationType);
                    return;
                }
            }
            aNotificationType = null;
            a(dVar, aNotificationType);
            return;
        }
        FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() supported............");
        a(dVar, bVar.a().getNotificationType());
        a(dVar, bVar);
    }

    @DexIgnore
    public final fi4 a(String str, String str2, long j2) {
        throw null;
        // kd4.b(str, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
        // kd4.b(str2, "content");
        // return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new DianaNotificationComponent$processSmsMms$Anon1(this, str, str2, j2, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final String a(int i2) {
        if (i2 < 0) {
            return "invalid time";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(i2 / 60);
        sb.append(':');
        sb.append(i2 % 60);
        return sb.toString();
    }

    @DexIgnore
    public final fi4 a(StatusBarNotification statusBarNotification) {
        throw null;
        // kd4.b(statusBarNotification, "sbn");
        // return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new DianaNotificationComponent$onNotificationAppRemoved$Anon1(this, statusBarNotification, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final boolean a(String str, boolean z) {
        int i2;
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = this.i.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(z);
        boolean z2 = true;
        if (notificationSettingsWithIsCallNoLiveData != null) {
            i2 = notificationSettingsWithIsCallNoLiveData.getSettingsType();
            if (i2 != 0) {
                if (i2 == 1) {
                    z2 = a(str);
                } else if (i2 == 2) {
                    z2 = false;
                }
            }
        } else {
            i2 = 0;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local.d(str2, "isAllowCallOrMessageEvent() - from sender " + str + " is " + z2 + " because type = type = " + i2);
        return z2;
    }

    @DexIgnore
    public final boolean b(String str) {
        List<ContactGroup> allContactGroups = dn2.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups.isEmpty()) {
            return false;
        }
        for (ContactGroup next : allContactGroups) {
            kd4.a((Object) next, "contactGroup");
            Contact contact = next.getContacts().get(0);
            kd4.a((Object) contact, "contactGroup.contacts[0]");
            if (qf4.b(contact.getDisplayName(), str, true)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean a(String str) {
        List<ContactGroup> allContactGroups = dn2.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups.isEmpty()) {
            return false;
        }
        for (ContactGroup contactWithPhoneNumber : allContactGroups) {
            if (contactWithPhoneNumber.getContactWithPhoneNumber(str) != null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void b(Context context) {
        FLogger.INSTANCE.getLocal().d(this.a, "unregisterPhoneCallObserver");
        try {
            PhoneCallObserver phoneCallObserver = this.f;
            if (phoneCallObserver != null) {
                context.getContentResolver().unregisterContentObserver(phoneCallObserver);
                this.f = null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.d(str, "unregisterPhoneCallObserver e=" + e2);
        }
    }

    @DexIgnore
    public final void a(d dVar) {
        b bVar;
        boolean z = false;
        if (kd4.a((Object) dVar.b(), (Object) Telephony.Sms.getDefaultSmsPackage(PortfolioApp.W.c()))) {
            g gVar = k.get(dVar.b());
            if (gVar == null || gVar.a(dVar.d())) {
                z = true;
                bVar = j.get(DianaNotificationObj.AApplicationName.MESSAGES.getPackageName());
            } else {
                bVar = j.get(dVar.b());
            }
        } else {
            bVar = j.get(dVar.b());
        }
        if (bVar == null) {
            return;
        }
        if (bVar.a() == DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL || bVar.a() == DianaNotificationObj.AApplicationName.PHONE_MISSED_CALL) {
            FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() app not supported............");
            return;
        }
        if (z && b(dVar, bVar) && c(dVar.e())) {
            FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() supported - handle for sms - new solution");
            a(dVar, bVar);
        } else if (z || ((!dVar.j() && dVar.k()) || !b(dVar, bVar) || dVar.i())) {
            FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() app not supported............");
        } else {
            FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() supported............");
            a(dVar, bVar);
        }
        a(dVar, bVar.a().getNotificationType());
    }

    @DexIgnore
    public final synchronized void c(d dVar) {
        if (this.g.M()) {
            a(dVar);
        } else {
            b(dVar);
        }
    }

    @DexIgnore
    public final boolean c(String str) {
        int i2;
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = this.i.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
        boolean z = true;
        if (notificationSettingsWithIsCallNoLiveData != null) {
            i2 = notificationSettingsWithIsCallNoLiveData.getSettingsType();
            if (i2 != 0) {
                if (i2 == 1) {
                    z = b(str);
                } else if (i2 == 2) {
                    z = false;
                }
            }
        } else {
            i2 = 0;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local.d(str2, "isAllowMessageFromContactName() - from sender " + str + " is " + z + ", type = type = " + i2);
        return z;
    }

    @DexIgnore
    public final void c(d dVar, b bVar) {
        NotificationBaseObj.ANotificationFlag aNotificationFlag;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "sendToDevice() - sender = " + dVar.e() + " - text = " + dVar.f());
        PortfolioApp c2 = PortfolioApp.W.c();
        String a2 = a();
        int a3 = dVar.a();
        NotificationBaseObj.ANotificationType notificationType = bVar.a().getNotificationType();
        DianaNotificationObj.AApplicationName a4 = bVar.a();
        String g2 = dVar.g();
        String e2 = dVar.e();
        String f2 = dVar.f();
        NotificationBaseObj.ANotificationFlag[] aNotificationFlagArr = new NotificationBaseObj.ANotificationFlag[1];
        if (DeviceHelper.o.l()) {
            aNotificationFlag = NotificationBaseObj.ANotificationFlag.ALLOW_USER_ACTION;
        } else {
            aNotificationFlag = NotificationBaseObj.ANotificationFlag.IMPORTANT;
        }
        aNotificationFlagArr[0] = aNotificationFlag;
        c2.a(a2, (NotificationBaseObj) new DianaNotificationObj(a3, notificationType, a4, g2, e2, f2, cb4.d(aNotificationFlagArr)));
    }

    @DexIgnore
    public final String d(String str) {
        String str2;
        Exception e2;
        if (ns3.a.e(PortfolioApp.W.c())) {
            Cursor query = PortfolioApp.W.c().getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(str)), new String[]{"display_name"}, (String) null, (String[]) null, (String) null);
            if (query == null || query.getCount() <= 0) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = this.a;
                local.d(str3, "readDisplayNameFromPhoneNumber(), number=" + str + ", no display name.");
            } else {
                try {
                    query.moveToFirst();
                    str2 = query.getString(0);
                    kd4.a((Object) str2, "c.getString(0)");
                    try {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str4 = this.a;
                        local2.d(str4, "readDisplayNameFromPhoneNumber(), number=" + str + ", displayName=" + str2);
                    } catch (Exception e3) {
                        e2 = e3;
                    }
                } catch (Exception e4) {
                    str2 = str;
                    e2 = e4;
                    try {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str5 = this.a;
                        local3.d(str5, "readDisplayNameFromPhoneNumber(), exception while read display name, ex=" + e2);
                        query.close();
                        return str2;
                    } catch (Throwable th) {
                        query.close();
                        throw th;
                    }
                }
                query.close();
                return str2;
            }
        } else {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str6 = this.a;
            local4.d(str6, "readDisplayNameFromPhoneNumber(), number=" + str + ", no read contact permission.");
        }
        return str;
    }

    @DexIgnore
    public final void a(d dVar, b bVar) {
        throw null;
        // d a2 = this.b.a(dVar);
        // if (a2 == null) {
        //     FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() - this notification existed.");
        // } else if (c()) {
        // } else {
        //     if (!qf4.a(a2.e()) || !qf4.a(a2.f())) {
        //         c(a2, bVar);
        //     }
        // }
    }

    @DexIgnore
    public final void a(d dVar, NotificationBaseObj.ANotificationType aNotificationType) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "  Notification Posted: " + dVar.b());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local2.d(str2, "  Id: " + dVar.d());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = this.a;
        local3.d(str3, "  Sender: " + dVar.e());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = this.a;
        local4.d(str4, "  Title: " + dVar.g());
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = this.a;
        local5.d(str5, "  Text: " + dVar.f());
        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
        String str6 = this.a;
        local6.d(str6, "  Summary: " + dVar.k());
        ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
        String str7 = this.a;
        local7.d(str7, "  IsOngoing: " + dVar.j());
        ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
        String str8 = this.a;
        local8.d(str8, "  When: " + dVar.h());
        ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
        String str9 = this.a;
        local9.d(str9, "  Priority: " + dVar.c());
        ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
        String str10 = this.a;
        StringBuilder sb = new StringBuilder();
        sb.append("  Type: ");
        sb.append(aNotificationType != null ? aNotificationType.name() : null);
        local10.d(str10, sb.toString());
        ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
        String str11 = this.a;
        local11.d(str11, "  IsDownloadEvent: " + dVar.i());
    }

    @DexIgnore
    public final void a(Context context) {
        if (ns3.a.d(context)) {
            this.f = new PhoneCallObserver();
            ContentResolver contentResolver = context.getContentResolver();
            Uri uri = CallLog.Calls.CONTENT_URI;
            PhoneCallObserver phoneCallObserver = this.f;
            if (phoneCallObserver != null) {
                contentResolver.registerContentObserver(uri, false, phoneCallObserver);
                FLogger.INSTANCE.getLocal().d(this.a, "registerPhoneCallObserver success");
                return;
            }
            kd4.a();
            throw null;
        }
    }
}
