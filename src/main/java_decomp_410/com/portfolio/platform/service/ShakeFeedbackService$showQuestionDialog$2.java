package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ShakeFeedbackService$showQuestionDialog$2 implements android.view.View.OnClickListener {

    @DexIgnore
    /* renamed from: e */
    public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService f21466e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2$1", mo27670f = "ShakeFeedbackService.kt", mo27671l = {262, 263}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2$1 */
    public static final class C60361 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ android.graphics.Bitmap $bitmap;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21467p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2$1$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2$1$1", mo27670f = "ShakeFeedbackService.kt", mo27671l = {}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2$1$1 */
        public static final class C60371 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f21468p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2.C60361 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C60371(com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2.C60361 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2.C60361.C60371 r0 = new com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2.C60361.C60371(this.this$0, yb4);
                r0.f21468p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2.C60361.C60371) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                com.fossil.blesdk.obfuscated.cc4.m20546a();
                if (this.label == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.sr1 c = this.this$0.this$0.f21466e.f21442d;
                    if (c != null) {
                        c.dismiss();
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C60361(com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2 shakeFeedbackService$showQuestionDialog$2, android.graphics.Bitmap bitmap, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = shakeFeedbackService$showQuestionDialog$2;
            this.$bitmap = bitmap;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2.C60361 r0 = new com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2.C60361(this.this$0, this.$bitmap, yb4);
            r0.f21467p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2.C60361) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.zg4 zg4;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                zg4 = this.f21467p$;
                com.portfolio.platform.service.ShakeFeedbackService shakeFeedbackService = this.this$0.f21466e;
                android.graphics.Bitmap bitmap = this.$bitmap;
                this.L$0 = zg4;
                this.label = 1;
                if (shakeFeedbackService.mo39881a(bitmap, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else if (i == 2) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
            com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2.C60361.C60371 r3 = new com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2.C60361.C60371(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 2;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r3, this) == a) {
                return a;
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public ShakeFeedbackService$showQuestionDialog$2(com.portfolio.platform.service.ShakeFeedbackService shakeFeedbackService) {
        this.f21466e = shakeFeedbackService;
    }

    @DexIgnore
    public final void onClick(android.view.View view) {
        com.fossil.blesdk.obfuscated.ns3.C4755a aVar = com.fossil.blesdk.obfuscated.ns3.f17135a;
        java.lang.ref.WeakReference b = this.f21466e.f21439a;
        if (b != null) {
            java.lang.Object obj = b.get();
            if (obj == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.app.Activity");
            } else if (aVar.mo29717c((android.app.Activity) obj, 123)) {
                java.lang.ref.WeakReference b2 = this.f21466e.f21439a;
                if (b2 != null) {
                    java.lang.Object obj2 = b2.get();
                    if (obj2 != null) {
                        android.view.Window window = ((android.app.Activity) obj2).getWindow();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) window, "(contextWeakReference!!.get() as Activity).window");
                        android.view.View decorView = window.getDecorView();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                        android.view.View rootView = decorView.getRootView();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) rootView, "v1");
                        rootView.setDrawingCacheEnabled(true);
                        android.graphics.Bitmap createBitmap = android.graphics.Bitmap.createBitmap(rootView.getDrawingCache());
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                        rootView.setDrawingCacheEnabled(false);
                        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2.C60361(this, createBitmap, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                        return;
                    }
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.app.Activity");
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        } else {
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        }
    }
}
