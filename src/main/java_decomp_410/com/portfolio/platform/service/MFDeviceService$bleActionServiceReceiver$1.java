package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFDeviceService$bleActionServiceReceiver$1 extends android.content.BroadcastReceiver {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.service.MFDeviceService f21423a;

    @DexIgnore
    public MFDeviceService$bleActionServiceReceiver$1(com.portfolio.platform.service.MFDeviceService mFDeviceService) {
        this.f21423a = mFDeviceService;
    }

    @DexIgnore
    public void onReceive(android.content.Context context, android.content.Intent intent) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(context, "context");
        com.fossil.blesdk.obfuscated.kd4.m24411b(intent, "intent");
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25691a()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1(this, intent, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }
}
