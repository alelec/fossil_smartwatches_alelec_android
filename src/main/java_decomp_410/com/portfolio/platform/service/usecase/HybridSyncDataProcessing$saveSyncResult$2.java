package com.portfolio.platform.service.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2", mo27670f = "HybridSyncDataProcessing.kt", mo27671l = {148}, mo27672m = "invokeSuspend")
public final class HybridSyncDataProcessing$saveSyncResult$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.fi4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.usecase.HybridSyncDataProcessing.C6095a $finalResult;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository $thirdPartyRepository;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21672p$;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2$1", mo27670f = "HybridSyncDataProcessing.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2$1 */
    public static final class C60961 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.fi4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $listGFitSample;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $listSleepSession;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $listUASample;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21673p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C60961(com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2 hybridSyncDataProcessing$saveSyncResult$2, java.util.List list, java.util.List list2, java.util.List list3, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = hybridSyncDataProcessing$saveSyncResult$2;
            this.$listGFitSample = list;
            this.$listUASample = list2;
            this.$listSleepSession = list3;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2.C60961 r1 = new com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2.C60961(this.this$0, this.$listGFitSample, this.$listUASample, this.$listSleepSession, yb4);
            r1.f21673p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r1;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2.C60961) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.$thirdPartyRepository.saveData(this.$listGFitSample, this.$listUASample, (com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime) null, com.fossil.blesdk.obfuscated.cb4.m20538a(), com.fossil.blesdk.obfuscated.cb4.m20538a(), this.$listSleepSession);
                return com.portfolio.platform.data.source.ThirdPartyRepository.uploadData$default(this.this$0.$thirdPartyRepository, (com.portfolio.platform.data.source.ThirdPartyRepository.PushPendingThirdPartyDataCallback) null, 1, (java.lang.Object) null);
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridSyncDataProcessing$saveSyncResult$2(com.portfolio.platform.service.usecase.HybridSyncDataProcessing.C6095a aVar, com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$finalResult = aVar;
        this.$thirdPartyRepository = thirdPartyRepository;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2 hybridSyncDataProcessing$saveSyncResult$2 = new com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2(this.$finalResult, this.$thirdPartyRepository, yb4);
        hybridSyncDataProcessing$saveSyncResult$2.f21672p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return hybridSyncDataProcessing$saveSyncResult$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21672p$;
            java.util.List<com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample> a2 = this.$finalResult.mo40161a();
            java.util.ArrayList arrayList = new java.util.ArrayList();
            for (com.portfolio.platform.data.model.room.fitness.ActivitySample next : this.$finalResult.mo40163c()) {
                org.joda.time.DateTime component3 = next.component3();
                next.component4();
                double component5 = next.component5();
                com.portfolio.platform.data.model.thirdparty.p005ua.UASample uASample = new com.portfolio.platform.data.model.thirdparty.p005ua.UASample(com.fossil.blesdk.obfuscated.td4.m28231a(component5), next.component7(), next.component6(), component3.getMillis() / ((long) 1000));
                arrayList.add(uASample);
            }
            java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession> d = this.$finalResult.mo40164d();
            com.fossil.blesdk.obfuscated.ug4 b = com.fossil.blesdk.obfuscated.nh4.m25692b();
            com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2.C60961 r0 = new com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2.C60961(this, a2, arrayList, d, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = a2;
            this.L$2 = arrayList;
            this.L$3 = d;
            this.label = 1;
            java.lang.Object a3 = com.fossil.blesdk.obfuscated.yf4.m30997a(b, r0, this);
            return a3 == a ? a : a3;
        } else if (i == 1) {
            java.util.List list = (java.util.List) this.L$3;
            java.util.List list2 = (java.util.List) this.L$2;
            java.util.List list3 = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return obj;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
