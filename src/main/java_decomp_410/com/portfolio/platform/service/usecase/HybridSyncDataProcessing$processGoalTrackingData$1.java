package com.portfolio.platform.service.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing", mo27670f = "HybridSyncDataProcessing.kt", mo27671l = {322}, mo27672m = "processGoalTrackingData")
public final class HybridSyncDataProcessing$processGoalTrackingData$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public java.lang.Object L$7;
    @DexIgnore
    public java.lang.Object L$8;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.usecase.HybridSyncDataProcessing this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridSyncDataProcessing$processGoalTrackingData$1(com.portfolio.platform.service.usecase.HybridSyncDataProcessing hybridSyncDataProcessing, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = hybridSyncDataProcessing;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.mo40156a((java.util.List<? extends com.portfolio.platform.service.syncmodel.WrapperTapEventSummary>) null, (java.lang.String) null, (com.portfolio.platform.data.source.HybridPresetRepository) null, (com.portfolio.platform.data.source.GoalTrackingRepository) null, (com.portfolio.platform.data.source.UserRepository) null, this);
    }
}
