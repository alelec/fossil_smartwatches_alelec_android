package com.portfolio.platform.service.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaSyncDataProcessing$getHeartRateData$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper, java.lang.Long> {
    @DexIgnore
    public static /* final */ com.portfolio.platform.service.usecase.DianaSyncDataProcessing$getHeartRateData$2 INSTANCE; // = new com.portfolio.platform.service.usecase.DianaSyncDataProcessing$getHeartRateData$2();

    @DexIgnore
    public DianaSyncDataProcessing$getHeartRateData$2() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Long.valueOf(invoke((com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) obj));
    }

    @DexIgnore
    public final long invoke(com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper fitnessDataWrapper) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(fitnessDataWrapper, "it");
        return fitnessDataWrapper.getStartLongTime();
    }
}
