package com.portfolio.platform.service.usecase;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.xk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yk2;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.syncmodel.WrapperTapEventSummary;
import com.portfolio.platform.util.SyncDataExtensionsKt;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import kotlin.Triple;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridSyncDataProcessing {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ HybridSyncDataProcessing b; // = new HybridSyncDataProcessing();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ List<WrapperTapEventSummary> b;
        @DexIgnore
        public /* final */ List<ActivitySample> c;
        @DexIgnore
        public /* final */ List<GFitSample> d;
        @DexIgnore
        public /* final */ List<ActivitySummary> e;
        @DexIgnore
        public /* final */ List<MFSleepSession> f;

        @DexIgnore
        public a(long j, List<WrapperTapEventSummary> list, List<ActivitySample> list2, List<GFitSample> list3, List<ActivitySummary> list4, List<MFSleepSession> list5) {
            kd4.b(list, "tapEventSummaryList");
            kd4.b(list2, "sampleRawList");
            kd4.b(list3, "gFitSampleList");
            kd4.b(list4, "summaryList");
            kd4.b(list5, "sleepSessionList");
            this.a = j;
            this.b = list;
            this.c = list2;
            this.d = list3;
            this.e = list4;
            this.f = list5;
        }

        @DexIgnore
        public final List<GFitSample> a() {
            return this.d;
        }

        @DexIgnore
        public final long b() {
            return this.a;
        }

        @DexIgnore
        public final List<ActivitySample> c() {
            return this.c;
        }

        @DexIgnore
        public final List<MFSleepSession> d() {
            return this.f;
        }

        @DexIgnore
        public final List<ActivitySummary> e() {
            return this.e;
        }

        @DexIgnore
        public final List<WrapperTapEventSummary> f() {
            return this.b;
        }
    }

    /*
    static {
        String simpleName = HybridSyncDataProcessing.class.getSimpleName();
        kd4.a((Object) simpleName, "HybridSyncDataProcessing::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final a a(String str, List<FitnessDataWrapper> list, MFUser mFUser, UserProfile userProfile, long j, long j2, PortfolioApp portfolioApp) {
        String str2 = str;
        List<FitnessDataWrapper> list2 = list;
        long j3 = j;
        PortfolioApp portfolioApp2 = portfolioApp;
        kd4.b(str2, "serial");
        kd4.b(list2, "syncData");
        kd4.b(mFUser, "user");
        kd4.b(userProfile, "userProfile");
        kd4.b(portfolioApp2, "portfolioApp");
        FLogger.INSTANCE.getLocal().d(a, ".buildSyncResult(), get all data files, synctime=" + j3 + ", data=" + list2);
        a aVar = new a(j2, new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList());
        if (!list.isEmpty()) {
            portfolioApp2.a(CommunicateMode.SYNC, str2, "Calculating sleep and activity...");
            String userId = mFUser.getUserId();
            kd4.a((Object) userId, "user.userId");
            Triple<List<ActivitySample>, List<ActivitySummary>, List<GFitSample>> a2 = SyncDataExtensionsKt.a(list2, str2, userId, j3 * 1000);
            List<ActivitySample> first = a2.getFirst();
            List second = a2.getSecond();
            List third = a2.getThird();
            List<MFSleepSession> a3 = SyncDataExtensionsKt.a(list2, str2);
            List<WrapperTapEventSummary> b2 = SyncDataExtensionsKt.b(list);
            int size = a3.size();
            double d = 0.0d;
            double d2 = 0.0d;
            double d3 = 0.0d;
            double d4 = 0.0d;
            for (ActivitySample activitySample : first) {
                d3 += activitySample.getCalories();
                d4 += activitySample.getDistance();
                d += activitySample.getSteps();
                activitySample.getActiveTime();
                Boolean s = rk2.s(activitySample.getDate());
                kd4.a((Object) s, "DateHelper.isToday(it.date)");
                if (s.booleanValue()) {
                    d2 += activitySample.getSteps();
                    activitySample.getActiveTime();
                }
            }
            pd4 pd4 = pd4.a;
            Object[] objArr = {String.valueOf(size)};
            String format = String.format("Done calculating sleep: total %s sleep session(s)", Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(format, *args)");
            a(str2, format);
            pd4 pd42 = pd4.a;
            Object[] objArr2 = {Double.valueOf(d), Double.valueOf(d2), Long.valueOf(j2), Long.valueOf(userProfile.getCurrentSteps()), Double.valueOf(d3), Double.valueOf(d4), c(b2)};
            String format2 = String.format("After calculation: steps=%s, todayStep=%s, realTimeSteps=%s, lastRealtimeSteps=%s. Calories=%s. DistanceWrapper=%s. Taps=%s", Arrays.copyOf(objArr2, objArr2.length));
            kd4.a((Object) format2, "java.lang.String.format(format, *args)");
            a(str2, format2);
            FLogger.INSTANCE.getLocal().d(a, "Release=" + yk2.a());
            if (!yk2.a()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = a;
                StringBuilder sb = new StringBuilder();
                sb.append("onSyncCompleted - Sleep session details: ");
                sb.append(a3.isEmpty() ? b(a3) : ", no sleep data");
                local.d(str3, sb.toString());
                FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Minute data details: " + a(first));
                FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Tap event details: " + c(b2));
            }
            return new a(j2, b2, first, third, second, a3);
        }
        a(str2, "Sync data is empty");
        return aVar;
    }

    @DexIgnore
    public final String b(List<MFSleepSession> list) {
        if (list.isEmpty()) {
            return "empty\n";
        }
        StringBuilder sb = new StringBuilder();
        for (MFSleepSession next : list) {
            sb.append(next.toString());
            sb.append("\n");
            sb.append("State: ");
            String sleepStates = next.getSleepStates();
            int length = sleepStates.length();
            for (int i = 0; i < length; i++) {
                sb.append(String.valueOf(sleepStates.charAt(i)));
                sb.append(" -- ");
            }
            sb.append("\n");
        }
        sb.append("\n");
        String sb2 = sb.toString();
        kd4.a((Object) sb2, "buffer.toString()");
        return sb2;
    }

    @DexIgnore
    public final String c(List<? extends WrapperTapEventSummary> list) {
        if (list == null || list.isEmpty()) {
            return "null\n";
        }
        StringBuilder sb = new StringBuilder();
        for (WrapperTapEventSummary wrapperTapEventSummary : list) {
            sb.append("[startTime:");
            sb.append(wrapperTapEventSummary.startTime);
            sb.append(", timezoneOffsetInSecond=");
            sb.append(wrapperTapEventSummary.timezoneOffsetInSecond);
            sb.append(", goalTrackingIds=");
            sb.append(wrapperTapEventSummary.goalId);
            sb.append("]");
            sb.append("\n");
        }
        String sb2 = sb.toString();
        kd4.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0036  */
    public final Object a(a aVar, String str, SleepSessionsRepository sleepSessionsRepository, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, FitnessDataRepository fitnessDataRepository, ActivitiesRepository activitiesRepository, HybridPresetRepository hybridPresetRepository, GoalTrackingRepository goalTrackingRepository, ThirdPartyRepository thirdPartyRepository, UserRepository userRepository, PortfolioApp portfolioApp, xk2 xk2, yb4<? super qa4> yb4) {
        HybridSyncDataProcessing$saveSyncResult$Anon1 hybridSyncDataProcessing$saveSyncResult$Anon1;
        int i;
        FitnessDataRepository fitnessDataRepository2;
        SleepSummariesRepository sleepSummariesRepository2;
        PortfolioApp portfolioApp2;
        HybridSyncDataProcessing hybridSyncDataProcessing;
        SleepSessionsRepository sleepSessionsRepository2;
        SummariesRepository summariesRepository2;
        SleepSummariesRepository sleepSummariesRepository3;
        FitnessDataRepository fitnessDataRepository3;
        ActivitiesRepository activitiesRepository2;
        a aVar2 = aVar;
        String str2 = str;
        SleepSessionsRepository sleepSessionsRepository3 = sleepSessionsRepository;
        SummariesRepository summariesRepository3 = summariesRepository;
        ActivitiesRepository activitiesRepository3 = activitiesRepository;
        ThirdPartyRepository thirdPartyRepository2 = thirdPartyRepository;
        xk2 xk22 = xk2;
        yb4<? super qa4> yb42 = yb4;
        if (yb42 instanceof HybridSyncDataProcessing$saveSyncResult$Anon1) {
            hybridSyncDataProcessing$saveSyncResult$Anon1 = (HybridSyncDataProcessing$saveSyncResult$Anon1) yb42;
            int i2 = hybridSyncDataProcessing$saveSyncResult$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridSyncDataProcessing$saveSyncResult$Anon1.label = i2 - Integer.MIN_VALUE;
                HybridSyncDataProcessing$saveSyncResult$Anon1 hybridSyncDataProcessing$saveSyncResult$Anon12 = hybridSyncDataProcessing$saveSyncResult$Anon1;
                Object obj = hybridSyncDataProcessing$saveSyncResult$Anon12.result;
                Object a2 = cc4.a();
                i = hybridSyncDataProcessing$saveSyncResult$Anon12.label;
                if (i != 0) {
                    na4.a(obj);
                    String str3 = "Save sync result - size of sleepSessions=" + aVar.d().size() + ", size of sampleRaws=" + aVar.c().size() + ", realTimeSteps=" + aVar.b();
                    FLogger.INSTANCE.getLocal().i(a, str3);
                    a(str2, str3);
                    if (qf4.a(PortfolioApp.W.c().e()) || !qf4.b(PortfolioApp.W.c().e(), str2, true)) {
                        FLogger.INSTANCE.getLocal().e(a, "Error inside " + a + ".saveSyncResult - Sync data does not match any user's device");
                        return qa4.a;
                    }
                    a(str2, "Saving sleep data");
                    try {
                        sleepSessionsRepository3.insertFromDevice(aVar.d());
                    } catch (Exception e) {
                        a(str2, "Saving sleep data. error=" + e.getMessage());
                    }
                    a(str2, "Saving sleep data. OK");
                    FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Upload sleep sessions to server");
                    a(str2, "Saving activity data");
                    gh4 unused = ag4.a(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new HybridSyncDataProcessing$saveSyncResult$Anon2(aVar2, thirdPartyRepository2, (yb4) null), 3, (Object) null);
                    xk22.a(new Date(), aVar.b());
                    FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update heartbeat step by syncing");
                    try {
                        activitiesRepository3.insertFromDevice(aVar.c());
                        a(str2, "Saving activity data. OK");
                    } catch (Exception e2) {
                        a(str2, "Saving activity data. error=" + e2.getMessage());
                    }
                    FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update activities summary by syncing");
                    try {
                        summariesRepository3.insertFromDevice(aVar.e());
                        a(str2, "Saving activity summaries data. OK");
                    } catch (Exception e3) {
                        a(str2, "Saving activity summaries data. error=" + e3.getMessage());
                    }
                    a(str2, "Saving goal tracking data");
                    try {
                        if (!aVar.f().isEmpty()) {
                            List<WrapperTapEventSummary> f = aVar.f();
                            hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon0 = this;
                            hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon1 = aVar2;
                            hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon2 = str2;
                            hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon3 = sleepSessionsRepository3;
                            hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon4 = summariesRepository3;
                            sleepSummariesRepository2 = sleepSummariesRepository;
                            try {
                                hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon5 = sleepSummariesRepository2;
                                fitnessDataRepository2 = fitnessDataRepository;
                                try {
                                    hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon6 = fitnessDataRepository2;
                                    hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon7 = activitiesRepository3;
                                    hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon8 = hybridPresetRepository;
                                    hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon9 = goalTrackingRepository;
                                    hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon10 = thirdPartyRepository2;
                                    hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon11 = userRepository;
                                    hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon12 = portfolioApp;
                                    hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon13 = xk22;
                                    hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon14 = str3;
                                    hybridSyncDataProcessing$saveSyncResult$Anon12.label = 1;
                                    if (a(f, str, hybridPresetRepository, goalTrackingRepository, userRepository, hybridSyncDataProcessing$saveSyncResult$Anon12) == a2) {
                                        return a2;
                                    }
                                    portfolioApp2 = portfolioApp;
                                    hybridSyncDataProcessing = this;
                                    sleepSessionsRepository2 = sleepSessionsRepository3;
                                    summariesRepository2 = summariesRepository3;
                                    activitiesRepository2 = activitiesRepository3;
                                    sleepSummariesRepository3 = sleepSummariesRepository2;
                                    fitnessDataRepository3 = fitnessDataRepository2;
                                } catch (Exception e4) {
                                    e = e4;
                                    portfolioApp2 = portfolioApp;
                                    hybridSyncDataProcessing = this;
                                    hybridSyncDataProcessing.a(str2, "Saving goal tracking data. error=" + e.getMessage());
                                    gh4 unused2 = ag4.a(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new HybridSyncDataProcessing$saveSyncResult$Anon3(fitnessDataRepository2, activitiesRepository3, summariesRepository3, sleepSessionsRepository3, sleepSummariesRepository2, (yb4) null), 3, (Object) null);
                                    FLogger.INSTANCE.getLocal().i(a, "DONE save to database, delete data file");
                                    portfolioApp2.b(str2);
                                    return qa4.a;
                                }
                            } catch (Exception e5) {
                                e = e5;
                                fitnessDataRepository2 = fitnessDataRepository;
                                portfolioApp2 = portfolioApp;
                                hybridSyncDataProcessing = this;
                                hybridSyncDataProcessing.a(str2, "Saving goal tracking data. error=" + e.getMessage());
                                gh4 unused3 = ag4.a(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new HybridSyncDataProcessing$saveSyncResult$Anon3(fitnessDataRepository2, activitiesRepository3, summariesRepository3, sleepSessionsRepository3, sleepSummariesRepository2, (yb4) null), 3, (Object) null);
                                FLogger.INSTANCE.getLocal().i(a, "DONE save to database, delete data file");
                                portfolioApp2.b(str2);
                                return qa4.a;
                            }
                        } else {
                            sleepSummariesRepository2 = sleepSummariesRepository;
                            fitnessDataRepository2 = fitnessDataRepository;
                            a(str2, "No goal tracking data");
                            portfolioApp2 = portfolioApp;
                            gh4 unused4 = ag4.a(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new HybridSyncDataProcessing$saveSyncResult$Anon3(fitnessDataRepository2, activitiesRepository3, summariesRepository3, sleepSessionsRepository3, sleepSummariesRepository2, (yb4) null), 3, (Object) null);
                            FLogger.INSTANCE.getLocal().i(a, "DONE save to database, delete data file");
                            portfolioApp2.b(str2);
                            return qa4.a;
                        }
                    } catch (Exception e6) {
                        e = e6;
                        sleepSummariesRepository2 = sleepSummariesRepository;
                        fitnessDataRepository2 = fitnessDataRepository;
                        portfolioApp2 = portfolioApp;
                        hybridSyncDataProcessing = this;
                        hybridSyncDataProcessing.a(str2, "Saving goal tracking data. error=" + e.getMessage());
                        gh4 unused5 = ag4.a(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new HybridSyncDataProcessing$saveSyncResult$Anon3(fitnessDataRepository2, activitiesRepository3, summariesRepository3, sleepSessionsRepository3, sleepSummariesRepository2, (yb4) null), 3, (Object) null);
                        FLogger.INSTANCE.getLocal().i(a, "DONE save to database, delete data file");
                        portfolioApp2.b(str2);
                        return qa4.a;
                    }
                } else if (i == 1) {
                    String str4 = (String) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon14;
                    xk2 xk23 = (xk2) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon13;
                    portfolioApp2 = (PortfolioApp) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon12;
                    UserRepository userRepository2 = (UserRepository) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon11;
                    ThirdPartyRepository thirdPartyRepository3 = (ThirdPartyRepository) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon10;
                    GoalTrackingRepository goalTrackingRepository2 = (GoalTrackingRepository) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon9;
                    HybridPresetRepository hybridPresetRepository2 = (HybridPresetRepository) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon8;
                    activitiesRepository2 = (ActivitiesRepository) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon7;
                    fitnessDataRepository3 = (FitnessDataRepository) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon6;
                    sleepSummariesRepository3 = (SleepSummariesRepository) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon5;
                    summariesRepository2 = (SummariesRepository) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon4;
                    sleepSessionsRepository2 = (SleepSessionsRepository) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon3;
                    str2 = (String) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon2;
                    a aVar3 = (a) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon1;
                    hybridSyncDataProcessing = (HybridSyncDataProcessing) hybridSyncDataProcessing$saveSyncResult$Anon12.L$Anon0;
                    try {
                        na4.a(obj);
                    } catch (Exception e7) {
                        e = e7;
                        activitiesRepository3 = activitiesRepository2;
                        fitnessDataRepository2 = fitnessDataRepository3;
                        sleepSummariesRepository2 = sleepSummariesRepository3;
                        summariesRepository3 = summariesRepository2;
                        sleepSessionsRepository3 = sleepSessionsRepository2;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                hybridSyncDataProcessing.a(str2, "Saving goal tracking data. OK");
                activitiesRepository3 = activitiesRepository2;
                fitnessDataRepository2 = fitnessDataRepository3;
                sleepSummariesRepository2 = sleepSummariesRepository3;
                summariesRepository3 = summariesRepository2;
                sleepSessionsRepository3 = sleepSessionsRepository2;
                gh4 unused6 = ag4.a(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new HybridSyncDataProcessing$saveSyncResult$Anon3(fitnessDataRepository2, activitiesRepository3, summariesRepository3, sleepSessionsRepository3, sleepSummariesRepository2, (yb4) null), 3, (Object) null);
                FLogger.INSTANCE.getLocal().i(a, "DONE save to database, delete data file");
                portfolioApp2.b(str2);
                return qa4.a;
            }
        }
        hybridSyncDataProcessing$saveSyncResult$Anon1 = new HybridSyncDataProcessing$saveSyncResult$Anon1(this, yb42);
        HybridSyncDataProcessing$saveSyncResult$Anon1 hybridSyncDataProcessing$saveSyncResult$Anon122 = hybridSyncDataProcessing$saveSyncResult$Anon1;
        Object obj2 = hybridSyncDataProcessing$saveSyncResult$Anon122.result;
        Object a22 = cc4.a();
        i = hybridSyncDataProcessing$saveSyncResult$Anon122.label;
        if (i != 0) {
        }
        hybridSyncDataProcessing.a(str2, "Saving goal tracking data. OK");
        activitiesRepository3 = activitiesRepository2;
        fitnessDataRepository2 = fitnessDataRepository3;
        sleepSummariesRepository2 = sleepSummariesRepository3;
        summariesRepository3 = summariesRepository2;
        sleepSessionsRepository3 = sleepSessionsRepository2;
        gh4 unused7 = ag4.a(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new HybridSyncDataProcessing$saveSyncResult$Anon3(fitnessDataRepository2, activitiesRepository3, summariesRepository3, sleepSessionsRepository3, sleepSummariesRepository2, (yb4) null), 3, (Object) null);
        FLogger.INSTANCE.getLocal().i(a, "DONE save to database, delete data file");
        portfolioApp2.b(str2);
        return qa4.a;
    }

    @DexIgnore
    public final String a(List<ActivitySample> list) {
        if (list.isEmpty()) {
            return "empty\n";
        }
        StringBuilder sb = new StringBuilder();
        for (ActivitySample next : list) {
            sb.append("[");
            sb.append("startTime:");
            sb.append(next.getStartTime());
            sb.append(", step:");
            sb.append(next.getSteps());
            sb.append("]");
        }
        String sb2 = sb.toString();
        kd4.a((Object) sb2, "buffer.toString()");
        return sb2;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v27, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v30, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: com.portfolio.platform.service.usecase.HybridSyncDataProcessing} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    public final /* synthetic */ Object a(List<? extends WrapperTapEventSummary> list, String str, HybridPresetRepository hybridPresetRepository, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, yb4<? super qa4> yb4) {
        HybridSyncDataProcessing$processGoalTrackingData$Anon1 hybridSyncDataProcessing$processGoalTrackingData$Anon1;
        int i;
        HybridSyncDataProcessing hybridSyncDataProcessing;
        String str2 = str;
        HybridPresetRepository hybridPresetRepository2 = hybridPresetRepository;
        GoalTrackingRepository goalTrackingRepository2 = goalTrackingRepository;
        yb4<? super qa4> yb42 = yb4;
        if (yb42 instanceof HybridSyncDataProcessing$processGoalTrackingData$Anon1) {
            hybridSyncDataProcessing$processGoalTrackingData$Anon1 = (HybridSyncDataProcessing$processGoalTrackingData$Anon1) yb42;
            int i2 = hybridSyncDataProcessing$processGoalTrackingData$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridSyncDataProcessing$processGoalTrackingData$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = hybridSyncDataProcessing$processGoalTrackingData$Anon1.result;
                Object a2 = cc4.a();
                i = hybridSyncDataProcessing$processGoalTrackingData$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d(a, "start process goal data, get active preset to check goal tracking");
                    PortfolioApp c = PortfolioApp.W.c();
                    HybridPreset activePresetBySerial = hybridPresetRepository2.getActivePresetBySerial(str2);
                    if (activePresetBySerial != null) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str3 = a;
                        local.d(str3, "processGoalTrackingData getHybridActivePreset success activePreset=" + activePresetBySerial);
                        ArrayList arrayList = new ArrayList();
                        for (WrapperTapEventSummary wrapperTapEventSummary : list) {
                            Date date = new Date(((long) wrapperTapEventSummary.startTime) * 1000);
                            String uuid = UUID.randomUUID().toString();
                            kd4.a((Object) uuid, "UUID.randomUUID().toString()");
                            DateTime a3 = rk2.a(date, wrapperTapEventSummary.timezoneOffsetInSecond);
                            kd4.a((Object) a3, "DateHelper.createDateTim\u2026y.timezoneOffsetInSecond)");
                            Date date2 = date;
                            arrayList.add(new GoalTrackingData(uuid, a3, wrapperTapEventSummary.timezoneOffsetInSecond, date2, new Date().getTime(), new Date().getTime()));
                        }
                        try {
                            List d = kb4.d(arrayList);
                            hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon0 = this;
                            hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon1 = list;
                            hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon2 = str2;
                            hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon3 = hybridPresetRepository2;
                            hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon4 = goalTrackingRepository2;
                            hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon5 = userRepository;
                            hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon6 = c;
                            hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon7 = activePresetBySerial;
                            hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon8 = arrayList;
                            hybridSyncDataProcessing$processGoalTrackingData$Anon1.label = 1;
                            if (goalTrackingRepository2.insertFromDevice(d, hybridSyncDataProcessing$processGoalTrackingData$Anon1) == a2) {
                                return a2;
                            }
                            hybridSyncDataProcessing = this;
                        } catch (Exception e) {
                            e = e;
                            hybridSyncDataProcessing = this;
                            hybridSyncDataProcessing.a(str2, "Saving goal tracking data. error=" + e.getMessage());
                            return qa4.a;
                        }
                    } else {
                        FLogger.INSTANCE.getLocal().d(a, "processGoalTrackingData getHybridActivePreset onFail");
                        c.a(CommunicateMode.SYNC, str2, "Goal tracking is not set as active app since user don't have active device.");
                        return qa4.a;
                    }
                } else if (i == 1) {
                    ArrayList arrayList2 = (ArrayList) hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon8;
                    HybridPreset hybridPreset = (HybridPreset) hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon7;
                    PortfolioApp portfolioApp = (PortfolioApp) hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon6;
                    UserRepository userRepository2 = (UserRepository) hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon5;
                    GoalTrackingRepository goalTrackingRepository3 = (GoalTrackingRepository) hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon4;
                    HybridPresetRepository hybridPresetRepository3 = (HybridPresetRepository) hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon3;
                    str2 = hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon2;
                    List list2 = (List) hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon1;
                    hybridSyncDataProcessing = hybridSyncDataProcessing$processGoalTrackingData$Anon1.L$Anon0;
                    try {
                        na4.a(obj);
                    } catch (Exception e2) {
                        e = e2;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                hybridSyncDataProcessing.a(str2, "Saving goal tracking data. OK");
                return qa4.a;
            }
        }
        hybridSyncDataProcessing$processGoalTrackingData$Anon1 = new HybridSyncDataProcessing$processGoalTrackingData$Anon1(this, yb42);
        Object obj2 = hybridSyncDataProcessing$processGoalTrackingData$Anon1.result;
        Object a22 = cc4.a();
        i = hybridSyncDataProcessing$processGoalTrackingData$Anon1.label;
        if (i != 0) {
        }
        hybridSyncDataProcessing.a(str2, "Saving goal tracking data. OK");
        return qa4.a;
    }

    @DexIgnore
    public final void a(String str, String str2) {
        PortfolioApp.W.c().a(CommunicateMode.SYNC, str, str2);
        FLogger.INSTANCE.getLocal().d(a, str2);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str, a, str2);
    }
}
