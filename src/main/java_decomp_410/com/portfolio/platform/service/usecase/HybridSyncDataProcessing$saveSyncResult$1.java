package com.portfolio.platform.service.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing", mo27670f = "HybridSyncDataProcessing.kt", mo27671l = {177}, mo27672m = "saveSyncResult")
public final class HybridSyncDataProcessing$saveSyncResult$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$10;
    @DexIgnore
    public java.lang.Object L$11;
    @DexIgnore
    public java.lang.Object L$12;
    @DexIgnore
    public java.lang.Object L$13;
    @DexIgnore
    public java.lang.Object L$14;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public java.lang.Object L$7;
    @DexIgnore
    public java.lang.Object L$8;
    @DexIgnore
    public java.lang.Object L$9;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.usecase.HybridSyncDataProcessing this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridSyncDataProcessing$saveSyncResult$1(com.portfolio.platform.service.usecase.HybridSyncDataProcessing hybridSyncDataProcessing, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = hybridSyncDataProcessing;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.mo40155a((com.portfolio.platform.service.usecase.HybridSyncDataProcessing.C6095a) null, (java.lang.String) null, (com.portfolio.platform.data.source.SleepSessionsRepository) null, (com.portfolio.platform.data.source.SummariesRepository) null, (com.portfolio.platform.data.source.SleepSummariesRepository) null, (com.portfolio.platform.data.source.FitnessDataRepository) null, (com.portfolio.platform.data.source.ActivitiesRepository) null, (com.portfolio.platform.data.source.HybridPresetRepository) null, (com.portfolio.platform.data.source.GoalTrackingRepository) null, (com.portfolio.platform.data.source.ThirdPartyRepository) null, (com.portfolio.platform.data.source.UserRepository) null, (com.portfolio.platform.PortfolioApp) null, (com.fossil.blesdk.obfuscated.xk2) null, this);
    }
}
