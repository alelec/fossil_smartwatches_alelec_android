package com.portfolio.platform.service.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2", mo27670f = "DianaSyncDataProcessing.kt", mo27671l = {186}, mo27672m = "invokeSuspend")
public final class DianaSyncDataProcessing$saveSyncResult$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.fi4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.usecase.DianaSyncDataProcessing.C6089a $finalResult;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository $thirdPartyRepository;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public java.lang.Object L$7;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21659p$;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2$1", mo27670f = "DianaSyncDataProcessing.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2$1 */
    public static final class C60921 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.fi4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $gFitActiveTime;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $listGFitHeartRate;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $listGFitSample;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $listGFitWorkoutSession;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $listMFSleepSession;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $listUASample;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21660p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C60921(com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2 dianaSyncDataProcessing$saveSyncResult$2, java.util.List list, java.util.List list2, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef, java.util.List list3, java.util.List list4, java.util.List list5, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = dianaSyncDataProcessing$saveSyncResult$2;
            this.$listGFitSample = list;
            this.$listUASample = list2;
            this.$gFitActiveTime = ref$ObjectRef;
            this.$listGFitHeartRate = list3;
            this.$listGFitWorkoutSession = list4;
            this.$listMFSleepSession = list5;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2.C60921 r1 = new com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2.C60921(this.this$0, this.$listGFitSample, this.$listUASample, this.$gFitActiveTime, this.$listGFitHeartRate, this.$listGFitWorkoutSession, this.$listMFSleepSession, yb4);
            r1.f21660p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r1;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2.C60921) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.$thirdPartyRepository.saveData(this.$listGFitSample, this.$listUASample, (com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime) this.$gFitActiveTime.element, this.$listGFitHeartRate, this.$listGFitWorkoutSession, this.$listMFSleepSession);
                return com.portfolio.platform.data.source.ThirdPartyRepository.uploadData$default(this.this$0.$thirdPartyRepository, (com.portfolio.platform.data.source.ThirdPartyRepository.PushPendingThirdPartyDataCallback) null, 1, (java.lang.Object) null);
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaSyncDataProcessing$saveSyncResult$2(com.portfolio.platform.service.usecase.DianaSyncDataProcessing.C6089a aVar, com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$finalResult = aVar;
        this.$thirdPartyRepository = thirdPartyRepository;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2 dianaSyncDataProcessing$saveSyncResult$2 = new com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2(this.$finalResult, this.$thirdPartyRepository, yb4);
        dianaSyncDataProcessing$saveSyncResult$2.f21659p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dianaSyncDataProcessing$saveSyncResult$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21659p$;
            java.util.List<com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample> c = this.$finalResult.mo40141c();
            java.util.ArrayList arrayList = new java.util.ArrayList();
            for (com.portfolio.platform.data.model.room.fitness.ActivitySample next : this.$finalResult.mo40146h()) {
                org.joda.time.DateTime component3 = next.component3();
                next.component4();
                double component5 = next.component5();
                com.portfolio.platform.data.model.thirdparty.p005ua.UASample uASample = new com.portfolio.platform.data.model.thirdparty.p005ua.UASample(com.fossil.blesdk.obfuscated.td4.m28231a(component5), next.component7(), next.component6(), component3.getMillis() / ((long) 1000));
                arrayList.add(uASample);
            }
            kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef = new kotlin.jvm.internal.Ref$ObjectRef();
            ref$ObjectRef.element = null;
            java.util.List<kotlin.Pair<java.lang.Long, java.lang.Long>> a2 = this.$finalResult.mo40139a();
            java.util.ArrayList arrayList2 = new java.util.ArrayList(com.fossil.blesdk.obfuscated.db4.m20831a(a2, 10));
            for (kotlin.Pair pair : a2) {
                arrayList2.add(com.fossil.blesdk.obfuscated.cb4.m20542c((java.lang.Long) pair.getFirst(), (java.lang.Long) pair.getSecond()));
            }
            java.util.List k = com.fossil.blesdk.obfuscated.kb4.m24391k(com.fossil.blesdk.obfuscated.db4.m20833a(arrayList2));
            if (!k.isEmpty()) {
                ref$ObjectRef.element = new com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime(k);
            }
            java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession> i2 = this.$finalResult.mo40147i();
            java.util.List<com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate> b = this.$finalResult.mo40140b();
            java.util.List<com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession> d = this.$finalResult.mo40142d();
            com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2.C60921 r23 = r0;
            com.fossil.blesdk.obfuscated.ug4 b2 = com.fossil.blesdk.obfuscated.nh4.m25692b();
            java.util.List<com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession> list = d;
            com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2.C60921 r0 = new com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2.C60921(this, c, arrayList, ref$ObjectRef, b, list, i2, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = c;
            this.L$2 = arrayList;
            this.L$3 = ref$ObjectRef;
            this.L$4 = k;
            this.L$5 = i2;
            this.L$6 = b;
            this.L$7 = list;
            this.label = 1;
            java.lang.Object a3 = com.fossil.blesdk.obfuscated.yf4.m30997a(b2, r23, this);
            java.lang.Object obj2 = a;
            return a3 == obj2 ? obj2 : a3;
        } else if (i == 1) {
            java.util.List list2 = (java.util.List) this.L$7;
            java.util.List list3 = (java.util.List) this.L$6;
            java.util.List list4 = (java.util.List) this.L$5;
            java.util.List list5 = (java.util.List) this.L$4;
            kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2 = (kotlin.jvm.internal.Ref$ObjectRef) this.L$3;
            java.util.List list6 = (java.util.List) this.L$2;
            java.util.List list7 = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return obj;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
