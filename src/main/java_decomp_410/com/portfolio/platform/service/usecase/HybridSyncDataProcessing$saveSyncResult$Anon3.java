package com.portfolio.platform.service.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import java.util.Date;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$Anon3", f = "HybridSyncDataProcessing.kt", l = {188, 195}, m = "invokeSuspend")
public final class HybridSyncDataProcessing$saveSyncResult$Anon3 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository $activityRepository;
    @DexIgnore
    public /* final */ /* synthetic */ FitnessDataRepository $fitnessDataRepository;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository $sleepSessionsRepository;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository $sleepSummariesRepository;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository $summaryRepository;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$Anon3$Anon1", f = "HybridSyncDataProcessing.kt", l = {204}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super ActivityStatistic>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef $startDate;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridSyncDataProcessing$saveSyncResult$Anon3 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$Anon3$Anon1$Anon1")
        @gc4(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$Anon3$Anon1$Anon1", f = "HybridSyncDataProcessing.kt", l = {205, 206, 208, 209, 210}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$Anon3$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0128Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super ActivityStatistic>, Object> {
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0128Anon1(Anon1 anon1, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0128Anon1 anon1 = new C0128Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0128Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:21:0x00c7 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x012e A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:31:0x013f A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
            public final Object invokeSuspend(Object obj) {
                zg4 zg4;
                SleepSummariesRepository sleepSummariesRepository;
                Date date;
                Date date2;
                zg4 zg42;
                SummariesRepository summariesRepository;
                Date date3;
                Date date4;
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 zg43 = this.p$;
                    Anon1 anon1 = this.this$Anon0;
                    ActivitiesRepository activitiesRepository = anon1.this$Anon0.$activityRepository;
                    Date date5 = ((DateTime) anon1.$startDate.element).toDate();
                    kd4.a((Object) date5, "startDate.toDate()");
                    Date date6 = ((DateTime) this.this$Anon0.$endDate.element).toDate();
                    kd4.a((Object) date6, "endDate.toDate()");
                    this.L$Anon0 = zg43;
                    this.label = 1;
                    zg4 zg44 = zg43;
                    if (ActivitiesRepository.fetchActivitySamples$default(activitiesRepository, date5, date6, 0, 0, this, 12, (Object) null) == a) {
                        return a;
                    }
                    zg42 = zg44;
                    Anon1 anon12 = this.this$Anon0;
                    summariesRepository = anon12.this$Anon0.$summaryRepository;
                    date3 = ((DateTime) anon12.$startDate.element).toDate();
                    kd4.a((Object) date3, "startDate.toDate()");
                    date4 = ((DateTime) this.this$Anon0.$endDate.element).toDate();
                    kd4.a((Object) date4, "endDate.toDate()");
                    this.L$Anon0 = zg42;
                    this.label = 2;
                    if (summariesRepository.loadSummaries(date3, date4, this) == a) {
                    }
                } else if (i == 1) {
                    zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                    Anon1 anon122 = this.this$Anon0;
                    summariesRepository = anon122.this$Anon0.$summaryRepository;
                    date3 = ((DateTime) anon122.$startDate.element).toDate();
                    kd4.a((Object) date3, "startDate.toDate()");
                    date4 = ((DateTime) this.this$Anon0.$endDate.element).toDate();
                    kd4.a((Object) date4, "endDate.toDate()");
                    this.L$Anon0 = zg42;
                    this.label = 2;
                    if (summariesRepository.loadSummaries(date3, date4, this) == a) {
                        return a;
                    }
                } else if (i == 2) {
                    zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else if (i == 3) {
                    zg4 = (zg4) this.L$Anon0;
                    na4.a(obj);
                    Anon1 anon13 = this.this$Anon0;
                    sleepSummariesRepository = anon13.this$Anon0.$sleepSummariesRepository;
                    date = ((DateTime) anon13.$startDate.element).toDate();
                    kd4.a((Object) date, "startDate.toDate()");
                    date2 = ((DateTime) this.this$Anon0.$endDate.element).toDate();
                    kd4.a((Object) date2, "endDate.toDate()");
                    this.L$Anon0 = zg4;
                    this.label = 4;
                    if (sleepSummariesRepository.fetchSleepSummaries(date, date2, this) == a) {
                        return a;
                    }
                    SummariesRepository summariesRepository2 = this.this$Anon0.this$Anon0.$summaryRepository;
                    this.L$Anon0 = zg4;
                    this.label = 5;
                    Object fetchActivityStatistic = summariesRepository2.fetchActivityStatistic(this);
                    if (fetchActivityStatistic == a) {
                    }
                } else if (i == 4) {
                    zg4 = (zg4) this.L$Anon0;
                    na4.a(obj);
                    SummariesRepository summariesRepository22 = this.this$Anon0.this$Anon0.$summaryRepository;
                    this.L$Anon0 = zg4;
                    this.label = 5;
                    Object fetchActivityStatistic2 = summariesRepository22.fetchActivityStatistic(this);
                    return fetchActivityStatistic2 == a ? a : fetchActivityStatistic2;
                } else if (i == 5) {
                    zg4 zg45 = (zg4) this.L$Anon0;
                    na4.a(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                zg4 zg46 = zg42;
                Anon1 anon14 = this.this$Anon0;
                SleepSessionsRepository sleepSessionsRepository = anon14.this$Anon0.$sleepSessionsRepository;
                Date date7 = ((DateTime) anon14.$startDate.element).toDate();
                kd4.a((Object) date7, "startDate.toDate()");
                Date date8 = ((DateTime) this.this$Anon0.$endDate.element).toDate();
                kd4.a((Object) date8, "endDate.toDate()");
                this.L$Anon0 = zg46;
                this.label = 3;
                if (SleepSessionsRepository.fetchSleepSessions$default(sleepSessionsRepository, date7, date8, 0, 0, this, 12, (Object) null) == a) {
                    return a;
                }
                zg4 = zg46;
                Anon1 anon132 = this.this$Anon0;
                sleepSummariesRepository = anon132.this$Anon0.$sleepSummariesRepository;
                date = ((DateTime) anon132.$startDate.element).toDate();
                kd4.a((Object) date, "startDate.toDate()");
                date2 = ((DateTime) this.this$Anon0.$endDate.element).toDate();
                kd4.a((Object) date2, "endDate.toDate()");
                this.L$Anon0 = zg4;
                this.label = 4;
                if (sleepSummariesRepository.fetchSleepSummaries(date, date2, this) == a) {
                }
                SummariesRepository summariesRepository222 = this.this$Anon0.this$Anon0.$summaryRepository;
                this.L$Anon0 = zg4;
                this.label = 5;
                Object fetchActivityStatistic22 = summariesRepository222.fetchActivityStatistic(this);
                if (fetchActivityStatistic22 == a) {
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HybridSyncDataProcessing$saveSyncResult$Anon3 hybridSyncDataProcessing$saveSyncResult$Anon3, List list, Ref$ObjectRef ref$ObjectRef, Ref$ObjectRef ref$ObjectRef2, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = hybridSyncDataProcessing$saveSyncResult$Anon3;
            this.$fitnessDataList = list;
            this.$startDate = ref$ObjectRef;
            this.$endDate = ref$ObjectRef2;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$fitnessDataList, this.$startDate, this.$endDate, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                for (FitnessDataWrapper fitnessDataWrapper : this.$fitnessDataList) {
                    if (fitnessDataWrapper.getStartLongTime() < ((DateTime) this.$startDate.element).getMillis()) {
                        this.$startDate.element = fitnessDataWrapper.getStartTimeTZ();
                    }
                    if (fitnessDataWrapper.getEndLongTime() > ((DateTime) this.$endDate.element).getMillis()) {
                        this.$endDate.element = fitnessDataWrapper.getEndTimeTZ();
                    }
                }
                ug4 b = nh4.b();
                C0128Anon1 anon1 = new C0128Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = yf4.a(b, anon1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridSyncDataProcessing$saveSyncResult$Anon3(FitnessDataRepository fitnessDataRepository, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, SleepSessionsRepository sleepSessionsRepository, SleepSummariesRepository sleepSummariesRepository, yb4 yb4) {
        super(2, yb4);
        this.$fitnessDataRepository = fitnessDataRepository;
        this.$activityRepository = activitiesRepository;
        this.$summaryRepository = summariesRepository;
        this.$sleepSessionsRepository = sleepSessionsRepository;
        this.$sleepSummariesRepository = sleepSummariesRepository;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HybridSyncDataProcessing$saveSyncResult$Anon3 hybridSyncDataProcessing$saveSyncResult$Anon3 = new HybridSyncDataProcessing$saveSyncResult$Anon3(this.$fitnessDataRepository, this.$activityRepository, this.$summaryRepository, this.$sleepSessionsRepository, this.$sleepSummariesRepository, yb4);
        hybridSyncDataProcessing$saveSyncResult$Anon3.p$ = (zg4) obj;
        return hybridSyncDataProcessing$saveSyncResult$Anon3;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HybridSyncDataProcessing$saveSyncResult$Anon3) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            FitnessDataRepository fitnessDataRepository = this.$fitnessDataRepository;
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = fitnessDataRepository.pushPendingFitnessData(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            Ref$ObjectRef ref$ObjectRef = (Ref$ObjectRef) this.L$Anon4;
            Ref$ObjectRef ref$ObjectRef2 = (Ref$ObjectRef) this.L$Anon3;
            List list = (List) this.L$Anon2;
            qo2 qo2 = (qo2) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo22 = (qo2) obj;
        if (qo22 instanceof ro2) {
            List list2 = (List) ((ro2) qo22).a();
            if (list2 != null && (true ^ list2.isEmpty())) {
                Ref$ObjectRef ref$ObjectRef3 = new Ref$ObjectRef();
                ref$ObjectRef3.element = ((FitnessDataWrapper) list2.get(0)).getStartTimeTZ();
                Ref$ObjectRef ref$ObjectRef4 = new Ref$ObjectRef();
                ref$ObjectRef4.element = ((FitnessDataWrapper) list2.get(0)).getEndTimeTZ();
                ug4 a2 = nh4.a();
                Anon1 anon1 = new Anon1(this, list2, ref$ObjectRef3, ref$ObjectRef4, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = qo22;
                this.L$Anon2 = list2;
                this.L$Anon3 = ref$ObjectRef3;
                this.L$Anon4 = ref$ObjectRef4;
                this.label = 2;
                if (yf4.a(a2, anon1, this) == a) {
                    return a;
                }
            }
        } else {
            boolean z = qo22 instanceof po2;
        }
        return qa4.a;
    }
}
