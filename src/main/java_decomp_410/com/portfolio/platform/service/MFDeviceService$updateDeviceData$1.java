package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.MFDeviceService$updateDeviceData$1", mo27670f = "MFDeviceService.kt", mo27671l = {859, 868}, mo27672m = "invokeSuspend")
public final class MFDeviceService$updateDeviceData$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile $deviceProfile;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isNeedUpdateRemote;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serial;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21436p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.MFDeviceService this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$updateDeviceData$1(com.portfolio.platform.service.MFDeviceService mFDeviceService, java.lang.String str, boolean z, com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile misfitDeviceProfile, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = mFDeviceService;
        this.$serial = str;
        this.$isNeedUpdateRemote = z;
        this.$deviceProfile = misfitDeviceProfile;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.MFDeviceService$updateDeviceData$1 mFDeviceService$updateDeviceData$1 = new com.portfolio.platform.service.MFDeviceService$updateDeviceData$1(this.this$0, this.$serial, this.$isNeedUpdateRemote, this.$deviceProfile, yb4);
        mFDeviceService$updateDeviceData$1.f21436p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return mFDeviceService$updateDeviceData$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.MFDeviceService$updateDeviceData$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0225 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0230  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.data.model.Device device;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        com.portfolio.platform.data.model.Device device2;
        com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile misfitDeviceProfile;
        com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj vibrationStrengthObj;
        com.portfolio.platform.data.source.DeviceRepository d;
        boolean z;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f21436p$;
            device2 = this.this$0.mo39833d().getDeviceBySerial(this.$serial);
            if (device2 != null) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String b = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                local.mo33255d(b, "updateDeviceData localDevice " + device2 + " forceUpdateRemote " + this.$isNeedUpdateRemote);
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile misfitDeviceProfile2 = this.$deviceProfile;
                if (misfitDeviceProfile2 != null) {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String b2 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                    local2.mo33255d(b2, "updateDeviceData batteryLevel " + this.$deviceProfile.getBatteryLevel() + " fwVersion " + this.$deviceProfile.getFirmwareVersion() + " major " + this.$deviceProfile.getMicroAppMajorVersion() + " minor " + this.$deviceProfile.getMicroAppMinorVersion());
                    vibrationStrengthObj = this.$deviceProfile.getVibrationStrength();
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String b3 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                    java.lang.StringBuilder sb = new java.lang.StringBuilder();
                    sb.append("updateDeviceData - isDefaultValue: ");
                    sb.append(vibrationStrengthObj.isDefaultValue());
                    local3.mo33255d(b3, sb.toString());
                    if (!vibrationStrengthObj.isDefaultValue()) {
                        int b4 = com.fossil.blesdk.obfuscated.yk2.m31035b(vibrationStrengthObj.getVibrationStrengthLevel());
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String b5 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                        local4.mo33255d(b5, "updateDeviceData - newVibrationLvl: " + b4 + " - device: " + device2);
                        device2.setVibrationStrength(com.fossil.blesdk.obfuscated.dc4.m20843a(b4));
                    }
                    if (this.$deviceProfile.getBatteryLevel() > 0) {
                        device2.setBatteryLevel(this.$deviceProfile.getBatteryLevel());
                        if (device2.getBatteryLevel() > 100) {
                            device2.setBatteryLevel(100);
                        }
                    }
                    device2.setFirmwareRevision(this.$deviceProfile.getFirmwareVersion());
                    device2.setSku(this.$deviceProfile.getDeviceModel());
                    device2.setMacAddress(this.$deviceProfile.getAddress());
                    if ((device2.getMajor() == this.$deviceProfile.getMicroAppMajorVersion() && device2.getMinor() == this.$deviceProfile.getMicroAppMinorVersion()) ? false : true) {
                        com.portfolio.platform.data.source.MicroAppRepository k = this.this$0.mo39840k();
                        java.lang.String str = this.$serial;
                        java.lang.String valueOf = java.lang.String.valueOf(this.$deviceProfile.getMicroAppMajorVersion());
                        java.lang.String valueOf2 = java.lang.String.valueOf(this.$deviceProfile.getMicroAppMinorVersion());
                        this.L$0 = zg4;
                        this.L$1 = device2;
                        this.L$2 = device2;
                        this.L$3 = misfitDeviceProfile2;
                        this.L$4 = vibrationStrengthObj;
                        this.label = 1;
                        if (k.downloadMicroAppVariant(str, valueOf, valueOf2, this) == a) {
                            return a;
                        }
                        misfitDeviceProfile = misfitDeviceProfile2;
                        device = device2;
                    } else {
                        misfitDeviceProfile = misfitDeviceProfile2;
                        device = device2;
                        device2.setMajor(this.$deviceProfile.getMicroAppMajorVersion());
                        device2.setMinor(this.$deviceProfile.getMicroAppMinorVersion());
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String b6 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                        local5.mo33255d(b6, "update device data " + device2);
                        d = this.this$0.mo39833d();
                        z = this.$isNeedUpdateRemote;
                        this.L$0 = zg4;
                        this.L$1 = device2;
                        this.L$2 = device;
                        this.L$3 = misfitDeviceProfile;
                        this.L$4 = vibrationStrengthObj;
                        this.label = 2;
                        if (d.updateDevice(device2, z, this) == a) {
                            return a;
                        }
                        if (this.$deviceProfile.getHeartRateMode() != com.misfit.frameworks.buttonservice.enums.HeartRateMode.NONE) {
                        }
                    }
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            vibrationStrengthObj = (com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj) this.L$4;
            misfitDeviceProfile = (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) this.L$3;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            device = (com.portfolio.platform.data.model.Device) this.L$2;
            device2 = (com.portfolio.platform.data.model.Device) this.L$1;
        } else if (i == 2) {
            com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj vibrationStrengthObj2 = (com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj) this.L$4;
            com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile misfitDeviceProfile3 = (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) this.L$3;
            com.portfolio.platform.data.model.Device device3 = (com.portfolio.platform.data.model.Device) this.L$2;
            com.portfolio.platform.data.model.Device device4 = (com.portfolio.platform.data.model.Device) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            if (this.$deviceProfile.getHeartRateMode() != com.misfit.frameworks.buttonservice.enums.HeartRateMode.NONE) {
                this.this$0.mo39842m().mo26995a(this.$deviceProfile.getHeartRateMode());
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.mo39842m().mo26999a(this.$serial, 0, false);
        device2.setMajor(this.$deviceProfile.getMicroAppMajorVersion());
        device2.setMinor(this.$deviceProfile.getMicroAppMinorVersion());
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local52 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String b62 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
        local52.mo33255d(b62, "update device data " + device2);
        d = this.this$0.mo39833d();
        z = this.$isNeedUpdateRemote;
        this.L$0 = zg4;
        this.L$1 = device2;
        this.L$2 = device;
        this.L$3 = misfitDeviceProfile;
        this.L$4 = vibrationStrengthObj;
        this.label = 2;
        if (d.updateDevice(device2, z, this) == a) {
        }
        if (this.$deviceProfile.getHeartRateMode() != com.misfit.frameworks.buttonservice.enums.HeartRateMode.NONE) {
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
