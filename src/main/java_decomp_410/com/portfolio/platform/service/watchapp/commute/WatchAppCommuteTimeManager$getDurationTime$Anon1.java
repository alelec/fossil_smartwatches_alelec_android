package com.portfolio.platform.service.watchapp.commute;

import android.location.Location;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager", f = "WatchAppCommuteTimeManager.kt", l = {192}, m = "getDurationTime")
public final class WatchAppCommuteTimeManager$getDurationTime$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppCommuteTimeManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppCommuteTimeManager$getDurationTime$Anon1(WatchAppCommuteTimeManager watchAppCommuteTimeManager, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = watchAppCommuteTimeManager;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((Location) null, (yb4<? super qa4>) this);
    }
}
