package com.portfolio.platform.service.watchapp.commute;

import android.location.Location;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.diana.commutetime.TrafficRequest;
import com.portfolio.platform.data.model.diana.commutetime.TrafficResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<TrafficResponse>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ yb4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Location $location$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ TrafficRequest $trafficRequest;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppCommuteTimeManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$Anon1(TrafficRequest trafficRequest, yb4 yb4, WatchAppCommuteTimeManager watchAppCommuteTimeManager, Location location, yb4 yb42) {
        super(1, yb4);
        this.$trafficRequest = trafficRequest;
        this.this$Anon0 = watchAppCommuteTimeManager;
        this.$location$inlined = location;
        this.$continuation$inlined = yb42;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$Anon1(this.$trafficRequest, yb4, this.this$Anon0, this.$location$inlined, this.$continuation$inlined);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            ApiServiceV2 a2 = this.this$Anon0.a();
            TrafficRequest trafficRequest = this.$trafficRequest;
            this.label = 1;
            obj = a2.getTrafficStatus(trafficRequest, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
