package com.portfolio.platform.service.watchapp.commute;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.model.diana.commutetime.TrafficResponse>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.yb4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ android.location.Location $location$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.diana.commutetime.TrafficRequest $trafficRequest;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$1(com.portfolio.platform.data.model.diana.commutetime.TrafficRequest trafficRequest, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager watchAppCommuteTimeManager, android.location.Location location, com.fossil.blesdk.obfuscated.yb4 yb42) {
        super(1, yb4);
        this.$trafficRequest = trafficRequest;
        this.this$0 = watchAppCommuteTimeManager;
        this.$location$inlined = location;
        this.$continuation$inlined = yb42;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$1 watchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$1 = new com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$1(this.$trafficRequest, yb4, this.this$0, this.$location$inlined, this.$continuation$inlined);
        return watchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj) {
        return ((com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$1) create((com.fossil.blesdk.obfuscated.yb4) obj)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.source.remote.ApiServiceV2 a2 = this.this$0.mo40167a();
            com.portfolio.platform.data.model.diana.commutetime.TrafficRequest trafficRequest = this.$trafficRequest;
            this.label = 1;
            obj = a2.getTrafficStatus(trafficRequest, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
