package com.portfolio.platform.service.watchapp.commute;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.ShareConstants;
import com.fossil.blesdk.model.enumerate.CommuteTimeWatchAppAction;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lr2;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rc;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sl2;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.td4;
import com.fossil.blesdk.obfuscated.ul2;
import com.fossil.blesdk.obfuscated.xp2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.diana.commutetime.Address;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.diana.commutetime.TrafficRequest;
import com.portfolio.platform.data.model.diana.commutetime.TrafficResponse;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.enums.SyncErrorCode;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.response.ResponseKt;
import java.util.Arrays;
import kotlin.NoWhenBranchMatchedException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppCommuteTimeManager {
    @DexIgnore
    public static WatchAppCommuteTimeManager r;
    @DexIgnore
    public static /* final */ a s; // = new a((fd4) null);
    @DexIgnore
    public PortfolioApp a;
    @DexIgnore
    public ApiServiceV2 b;
    @DexIgnore
    public LocationSource c;
    @DexIgnore
    public UserRepository d;
    @DexIgnore
    public en2 e;
    @DexIgnore
    public rc f;
    @DexIgnore
    public lr2 g;
    @DexIgnore
    public DianaPresetRepository h;
    @DexIgnore
    public String i;
    @DexIgnore
    public TrafficResponse j;
    @DexIgnore
    public Address k;
    @DexIgnore
    public AddressWrapper l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public AddressWrapper n;
    @DexIgnore
    public Listener o;
    @DexIgnore
    public Listener p;
    @DexIgnore
    public boolean q;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Listener implements LocationSource.LocationListener {
        @DexIgnore
        public Listener() {
        }

        @DexIgnore
        public void onLocationResult(Location location) {
            kd4.b(location, PlaceFields.LOCATION);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppCommuteTimeManager", "onLocationResult lastLocation=" + location);
            fi4 unused = ag4.b(ah4.a(nh4.a()), nh4.c(), (CoroutineStart) null, new WatchAppCommuteTimeManager$Listener$onLocationResult$Anon1(this, location, (yb4) null), 2, (Object) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final synchronized WatchAppCommuteTimeManager a() {
            WatchAppCommuteTimeManager i;
            if (WatchAppCommuteTimeManager.r == null) {
                WatchAppCommuteTimeManager.r = new WatchAppCommuteTimeManager((fd4) null);
            }
            i = WatchAppCommuteTimeManager.r;
            if (i == null) {
                kd4.a();
                throw null;
            }
            return i;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public WatchAppCommuteTimeManager() {
        this.o = new Listener();
        this.p = new Listener();
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final void d() {
        String str;
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "sendArriveToDestinationResponse");
        AddressWrapper addressWrapper = this.n;
        if (addressWrapper != null) {
            try {
                this.q = true;
                int i2 = xp2.a[addressWrapper.getType().ordinal()];
                if (i2 == 1) {
                    PortfolioApp portfolioApp = this.a;
                    if (portfolioApp != null) {
                        str = sm2.a((Context) portfolioApp, (int) R.string.commute_time_message_welcome_home);
                    } else {
                        kd4.d("mPortfolioApp");
                        throw null;
                    }
                } else if (i2 == 2) {
                    PortfolioApp portfolioApp2 = this.a;
                    if (portfolioApp2 != null) {
                        str = sm2.a((Context) portfolioApp2, (int) R.string.commute_time_message_welcome_work);
                    } else {
                        kd4.d("mPortfolioApp");
                        throw null;
                    }
                } else if (i2 == 3) {
                    pd4 pd4 = pd4.a;
                    PortfolioApp portfolioApp3 = this.a;
                    if (portfolioApp3 != null) {
                        String a2 = sm2.a((Context) portfolioApp3, (int) R.string.commute_time_message_arrive_to_destination);
                        kd4.a((Object) a2, "LanguageHelper.getString\u2026ge_arrive_to_destination)");
                        Object[] objArr = {addressWrapper.getName()};
                        str = String.format(a2, Arrays.copyOf(objArr, objArr.length));
                        kd4.a((Object) str, "java.lang.String.format(format, *args)");
                    } else {
                        kd4.d("mPortfolioApp");
                        throw null;
                    }
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                kd4.a((Object) str, "message");
                CommuteTimeWatchAppMessage commuteTimeWatchAppMessage = new CommuteTimeWatchAppMessage(str);
                PortfolioApp c2 = PortfolioApp.W.c();
                String str2 = this.i;
                if (str2 != null) {
                    c2.a((DeviceAppResponse) commuteTimeWatchAppMessage, str2);
                    JSONObject jSONObject = new JSONObject();
                    AddressWrapper addressWrapper2 = this.n;
                    jSONObject.put("des", addressWrapper2 != null ? addressWrapper2.getId() : null);
                    jSONObject.put("type", "end");
                    jSONObject.put("end_type", "arrived");
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
                    String str3 = this.i;
                    if (str3 != null) {
                        String jSONObject2 = jSONObject.toString();
                        kd4.a((Object) jSONObject2, "jsonObject.toString()");
                        remote.i(component, session, str3, "WatchAppCommuteTimeManager", jSONObject2);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            } catch (IllegalArgumentException e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppCommuteTimeManager", "sendArriveToDestinationResponse exception exception=" + e2.getMessage());
            }
        }
    }

    @DexIgnore
    public final void e() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "startMonitoringLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            PortfolioApp portfolioApp = this.a;
            if (portfolioApp != null) {
                locationSource.observerLocation(portfolioApp, this.p, false);
            } else {
                kd4.d("mPortfolioApp");
                throw null;
            }
        } else {
            kd4.d("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void f() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "startSignificantLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            PortfolioApp portfolioApp = this.a;
            if (portfolioApp != null) {
                locationSource.observerLocation(portfolioApp, this.o, true);
            } else {
                kd4.d("mPortfolioApp");
                throw null;
            }
        } else {
            kd4.d("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void g() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "stopMonitoringLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            locationSource.unObserverLocation(this.p, false);
        } else {
            kd4.d("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "stopSignificantLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            locationSource.unObserverLocation(this.o, true);
        } else {
            kd4.d("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final DianaPresetRepository b() {
        DianaPresetRepository dianaPresetRepository = this.h;
        if (dianaPresetRepository != null) {
            return dianaPresetRepository;
        }
        kd4.d("mDianaPresetRepository");
        throw null;
    }

    @DexIgnore
    public final void c() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final void b(LocationSource.ErrorState errorState) {
        ul2 c2 = AnalyticsHelper.f.c("commute-time");
        if (c2 != null) {
            pd4 pd4 = pd4.a;
            Object[] objArr = {AnalyticsHelper.f.d("commute-time")};
            String format = String.format("update_%s_optional_error", Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(format, *args)");
            sl2 a2 = AnalyticsHelper.f.a(format);
            a2.a("error_code", a(errorState));
            c2.a(a2);
        }
    }

    @DexIgnore
    public final ApiServiceV2 a() {
        ApiServiceV2 apiServiceV2 = this.b;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        kd4.d("mApiServiceV2");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ WatchAppCommuteTimeManager(fd4 fd4) {
        this();
    }

    @DexIgnore
    public final void a(String str, String str2, int i2) {
        kd4.b(str, "serial");
        kd4.b(str2, ShareConstants.DESTINATION);
        if (i2 == CommuteTimeWatchAppAction.START.ordinal()) {
            a(str, str2);
        } else if (i2 == CommuteTimeWatchAppAction.STOP.ordinal()) {
            JSONObject jSONObject = new JSONObject();
            AddressWrapper addressWrapper = this.n;
            jSONObject.put("des", addressWrapper != null ? addressWrapper.getId() : null);
            jSONObject.put("type", "end");
            jSONObject.put("end_type", "user_exit");
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
            String jSONObject2 = jSONObject.toString();
            kd4.a((Object) jSONObject2, "jsonObject.toString()");
            remote.i(component, session, str, "WatchAppCommuteTimeManager", jSONObject2);
            FLogger.INSTANCE.getRemote().summary(this.q ? 0 : FailureCode.USER_CANCELLED, FLogger.Component.APP, FLogger.Session.DIANA_COMMUTE_TIME, str, "WatchAppCommuteTimeManager");
            this.n = null;
            g();
            h();
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppCommuteTimeManager", "getCommuteTimeForWatchApp serial " + str + " destination " + str2);
        this.i = str;
        this.q = false;
        fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1(this, str2, str, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0133  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final /* synthetic */ Object a(yb4<? super qa4> yb4) {
        WatchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1 watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1;
        int i2;
        WatchAppCommuteTimeManager watchAppCommuteTimeManager;
        LocationSource.Result result;
        if (yb4 instanceof WatchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1) {
            watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1 = (WatchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1) yb4;
            int i3 = watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.result;
                Object a2 = cc4.a();
                i2 = watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.label;
                if (i2 != 0) {
                    na4.a(obj);
                    LocationSource locationSource = this.c;
                    if (locationSource != null) {
                        PortfolioApp portfolioApp = this.a;
                        if (portfolioApp != null) {
                            watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.L$Anon0 = this;
                            watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.label = 1;
                            obj = locationSource.getLocation(portfolioApp, false, watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1);
                            if (obj == a2) {
                                return a2;
                            }
                            watchAppCommuteTimeManager = this;
                        } else {
                            kd4.d("mPortfolioApp");
                            throw null;
                        }
                    } else {
                        kd4.d("mLocationSource");
                        throw null;
                    }
                } else if (i2 == 1) {
                    watchAppCommuteTimeManager = (WatchAppCommuteTimeManager) watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.L$Anon0;
                    na4.a(obj);
                } else if (i2 == 2) {
                    Location location = (Location) watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.L$Anon2;
                    LocationSource.Result result2 = (LocationSource.Result) watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.L$Anon1;
                    WatchAppCommuteTimeManager watchAppCommuteTimeManager2 = (WatchAppCommuteTimeManager) watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                result = (LocationSource.Result) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppCommuteTimeManager", "getDurationTimeBaseOnLocation " + result);
                watchAppCommuteTimeManager.m = result.getFromCache();
                if (result.getErrorState() != LocationSource.ErrorState.SUCCESS) {
                    Location location2 = result.getLocation();
                    if (location2 != null) {
                        if (!(watchAppCommuteTimeManager.k == null || watchAppCommuteTimeManager.j == null || !kd4.a((Object) watchAppCommuteTimeManager.l, (Object) watchAppCommuteTimeManager.n))) {
                            Address address = new Address(location2.getLatitude(), location2.getLongitude());
                            Address address2 = watchAppCommuteTimeManager.k;
                            if (address2 != null) {
                                Float a3 = watchAppCommuteTimeManager.a(address, address2);
                                if (a3 != null) {
                                    a3.floatValue();
                                    if (a3.floatValue() < ((float) 300)) {
                                        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "getDurationTimeBaseOnLocation fromCache");
                                        AddressWrapper addressWrapper = watchAppCommuteTimeManager.n;
                                        if (addressWrapper != null) {
                                            String name = addressWrapper.getName();
                                            TrafficResponse trafficResponse = watchAppCommuteTimeManager.j;
                                            if (trafficResponse != null) {
                                                long durationInTraffic = trafficResponse.getDurationInTraffic();
                                                TrafficResponse trafficResponse2 = watchAppCommuteTimeManager.j;
                                                if (trafficResponse2 != null) {
                                                    watchAppCommuteTimeManager.a(name, durationInTraffic, trafficResponse2.getTrafficStatus());
                                                    Address address3 = watchAppCommuteTimeManager.k;
                                                    if (address3 != null) {
                                                        double lat = address3.getLat();
                                                        Address address4 = watchAppCommuteTimeManager.k;
                                                        if (address4 != null) {
                                                            watchAppCommuteTimeManager.a(lat, address4.getLng());
                                                            return qa4.a;
                                                        }
                                                        kd4.a();
                                                        throw null;
                                                    }
                                                    kd4.a();
                                                    throw null;
                                                }
                                                kd4.a();
                                                throw null;
                                            }
                                            kd4.a();
                                            throw null;
                                        }
                                        kd4.a();
                                        throw null;
                                    }
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        }
                        watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.L$Anon0 = watchAppCommuteTimeManager;
                        watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.L$Anon1 = result;
                        watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.L$Anon2 = location2;
                        watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.label = 2;
                        obj = watchAppCommuteTimeManager.a(location2, (yb4<? super qa4>) watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1);
                        return obj == a2 ? a2 : obj;
                    }
                    kd4.a();
                    throw null;
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("WatchAppCommuteTimeManager", "getDurationTimeBaseOnLocation getLocation error=" + result.getErrorState());
                watchAppCommuteTimeManager.b(result.getErrorState());
                FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "getDurationTimeBaseOnLocation - current location is null, stop service.");
                return qa4.a;
            }
        }
        watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1 = new WatchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1(this, yb4);
        Object obj2 = watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.result;
        Object a22 = cc4.a();
        i2 = watchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1.label;
        if (i2 != 0) {
        }
        result = (LocationSource.Result) obj2;
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        local3.d("WatchAppCommuteTimeManager", "getDurationTimeBaseOnLocation " + result);
        watchAppCommuteTimeManager.m = result.getFromCache();
        if (result.getErrorState() != LocationSource.ErrorState.SUCCESS) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x010f A[Catch:{ Exception -> 0x01b6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0114 A[Catch:{ Exception -> 0x01b6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x012b A[Catch:{ Exception -> 0x01b6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01ad A[Catch:{ Exception -> 0x01b4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    public final /* synthetic */ Object a(Location location, yb4<? super qa4> yb4) {
        WatchAppCommuteTimeManager$getDurationTime$Anon1 watchAppCommuteTimeManager$getDurationTime$Anon1;
        int i2;
        String str;
        String str2;
        AddressWrapper addressWrapper;
        Location location2;
        WatchAppCommuteTimeManager watchAppCommuteTimeManager;
        String str3;
        yb4<? super qa4> yb42 = yb4;
        if (yb42 instanceof WatchAppCommuteTimeManager$getDurationTime$Anon1) {
            watchAppCommuteTimeManager$getDurationTime$Anon1 = (WatchAppCommuteTimeManager$getDurationTime$Anon1) yb42;
            int i3 = watchAppCommuteTimeManager$getDurationTime$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                watchAppCommuteTimeManager$getDurationTime$Anon1.label = i3 - Integer.MIN_VALUE;
                WatchAppCommuteTimeManager$getDurationTime$Anon1 watchAppCommuteTimeManager$getDurationTime$Anon12 = watchAppCommuteTimeManager$getDurationTime$Anon1;
                Object obj = watchAppCommuteTimeManager$getDurationTime$Anon12.result;
                Object a2 = cc4.a();
                i2 = watchAppCommuteTimeManager$getDurationTime$Anon12.label;
                if (i2 != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppCommuteTimeManager", "getDurationTime location lng=" + location.getLongitude() + " lat=" + location.getLatitude());
                    addressWrapper = this.n;
                    if (addressWrapper != null) {
                        str2 = "WatchAppCommuteTimeManager";
                        TrafficRequest trafficRequest = new TrafficRequest(addressWrapper.getAvoidTolls(), new Address(addressWrapper.getLat(), addressWrapper.getLng()), new Address(location.getLatitude(), location.getLongitude()));
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("des", addressWrapper != null ? addressWrapper.getId() : null);
                        jSONObject.put("type", "begin_request");
                        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                        FLogger.Component component = FLogger.Component.APP;
                        FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
                        String str4 = this.i;
                        if (str4 != null) {
                            String jSONObject2 = jSONObject.toString();
                            kd4.a((Object) jSONObject2, "jsonObject.toString()");
                            remote.i(component, session, str4, "WatchAppCommuteTimeManager", jSONObject2);
                            try {
                                WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$Anon1 watchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$Anon1 = new WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$Anon1(trafficRequest, (yb4) null, this, location, watchAppCommuteTimeManager$getDurationTime$Anon12);
                                watchAppCommuteTimeManager$getDurationTime$Anon12.L$Anon0 = this;
                                location2 = location;
                                watchAppCommuteTimeManager$getDurationTime$Anon12.L$Anon1 = location2;
                                watchAppCommuteTimeManager$getDurationTime$Anon12.L$Anon2 = addressWrapper;
                                watchAppCommuteTimeManager$getDurationTime$Anon12.L$Anon3 = trafficRequest;
                                watchAppCommuteTimeManager$getDurationTime$Anon12.L$Anon4 = jSONObject;
                                watchAppCommuteTimeManager$getDurationTime$Anon12.label = 1;
                                obj = ResponseKt.a(watchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$Anon1, watchAppCommuteTimeManager$getDurationTime$Anon12);
                                if (obj == a2) {
                                    return a2;
                                }
                                watchAppCommuteTimeManager = this;
                            } catch (Exception e2) {
                                e = e2;
                                str = str2;
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                local2.e(str, "getDurationTime exception=" + e);
                                e.printStackTrace();
                                qa4 qa4 = qa4.a;
                                return qa4.a;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    return qa4.a;
                } else if (i2 == 1) {
                    JSONObject jSONObject3 = (JSONObject) watchAppCommuteTimeManager$getDurationTime$Anon12.L$Anon4;
                    TrafficRequest trafficRequest2 = (TrafficRequest) watchAppCommuteTimeManager$getDurationTime$Anon12.L$Anon3;
                    AddressWrapper addressWrapper2 = (AddressWrapper) watchAppCommuteTimeManager$getDurationTime$Anon12.L$Anon2;
                    location2 = (Location) watchAppCommuteTimeManager$getDurationTime$Anon12.L$Anon1;
                    watchAppCommuteTimeManager = (WatchAppCommuteTimeManager) watchAppCommuteTimeManager$getDurationTime$Anon12.L$Anon0;
                    try {
                        na4.a(obj);
                        addressWrapper = addressWrapper2;
                        str2 = "WatchAppCommuteTimeManager";
                    } catch (Exception e3) {
                        e = e3;
                        str = "WatchAppCommuteTimeManager";
                        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                        local22.e(str, "getDurationTime exception=" + e);
                        e.printStackTrace();
                        qa4 qa42 = qa4.a;
                        return qa4.a;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 qo2 = (qo2) obj;
                JSONObject jSONObject4 = new JSONObject();
                jSONObject4.put("des", addressWrapper == null ? addressWrapper.getId() : null);
                jSONObject4.put("type", "response_received");
                IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                FLogger.Component component2 = FLogger.Component.APP;
                FLogger.Session session2 = FLogger.Session.DIANA_COMMUTE_TIME;
                str3 = watchAppCommuteTimeManager.i;
                if (str3 == null) {
                    String jSONObject5 = jSONObject4.toString();
                    kd4.a((Object) jSONObject5, "json.toString()");
                    remote2.i(component2, session2, str3, "WatchAppCommuteTimeManager", jSONObject5);
                    if (qo2 instanceof ro2) {
                        TrafficResponse trafficResponse = (TrafficResponse) ((ro2) qo2).a();
                        if (trafficResponse != null) {
                            watchAppCommuteTimeManager.l = addressWrapper;
                            watchAppCommuteTimeManager.j = trafficResponse;
                            watchAppCommuteTimeManager.k = new Address(location2.getLatitude(), location2.getLongitude());
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            str = str2;
                            try {
                                local3.d(str, "getDurationTime trafficResponse " + trafficResponse);
                                watchAppCommuteTimeManager.a(addressWrapper.getName(), trafficResponse.getDurationInTraffic(), trafficResponse.getTrafficStatus());
                                watchAppCommuteTimeManager.a(location2.getLatitude(), location2.getLongitude());
                                qa4 qa43 = qa4.a;
                            } catch (Exception e4) {
                                e = e4;
                                ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
                                local222.e(str, "getDurationTime exception=" + e);
                                e.printStackTrace();
                                qa4 qa422 = qa4.a;
                                return qa4.a;
                            }
                        }
                    } else {
                        String str5 = str2;
                        if (qo2 instanceof po2) {
                            FLogger.INSTANCE.getLocal().d(str5, "getDurationTime failed");
                            qa4 qa44 = qa4.a;
                        } else {
                            throw new NoWhenBranchMatchedException();
                        }
                    }
                    return qa4.a;
                }
                String str6 = str2;
                kd4.a();
                throw null;
            }
        }
        watchAppCommuteTimeManager$getDurationTime$Anon1 = new WatchAppCommuteTimeManager$getDurationTime$Anon1(this, yb42);
        WatchAppCommuteTimeManager$getDurationTime$Anon1 watchAppCommuteTimeManager$getDurationTime$Anon122 = watchAppCommuteTimeManager$getDurationTime$Anon1;
        Object obj2 = watchAppCommuteTimeManager$getDurationTime$Anon122.result;
        Object a22 = cc4.a();
        i2 = watchAppCommuteTimeManager$getDurationTime$Anon122.label;
        if (i2 != 0) {
        }
        qo2 qo22 = (qo2) obj2;
        JSONObject jSONObject42 = new JSONObject();
        jSONObject42.put("des", addressWrapper == null ? addressWrapper.getId() : null);
        jSONObject42.put("type", "response_received");
        IRemoteFLogger remote22 = FLogger.INSTANCE.getRemote();
        FLogger.Component component22 = FLogger.Component.APP;
        FLogger.Session session22 = FLogger.Session.DIANA_COMMUTE_TIME;
        str3 = watchAppCommuteTimeManager.i;
        if (str3 == null) {
        }
    }

    @DexIgnore
    public final void a(String str, long j2, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppCommuteTimeManager", "sendCommuteTimeAppResponse destination " + str + ", durationInTraffic " + j2 + ", trafficStatus " + str2);
        AddressWrapper addressWrapper = this.n;
        if (addressWrapper == null) {
            return;
        }
        if (addressWrapper == null) {
            kd4.a();
            throw null;
        } else if (!TextUtils.isEmpty(addressWrapper.getName())) {
            int a2 = td4.a(((float) j2) / 60.0f);
            if (!(!kd4.a((Object) str2, (Object) "unknown")) && !(!kd4.a((Object) str2, (Object) AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN))) {
                str2 = "--";
            }
            try {
                CommuteTimeWatchAppInfo commuteTimeWatchAppInfo = new CommuteTimeWatchAppInfo(str, a2, str2);
                PortfolioApp c2 = PortfolioApp.W.c();
                String str3 = this.i;
                if (str3 != null) {
                    c2.a((DeviceAppResponse) commuteTimeWatchAppInfo, str3);
                    ul2 c3 = AnalyticsHelper.f.c("commute-time");
                    if (c3 != null) {
                        c3.a(this.m, "");
                    }
                    AnalyticsHelper.f.e("commute-time");
                    return;
                }
                kd4.a();
                throw null;
            } catch (IllegalArgumentException e2) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("WatchAppCommuteTimeManager", "sendCommuteTimeAppResponse exception exception=" + e2.getMessage());
            }
        }
    }

    @DexIgnore
    public final Float a(Address address, Address address2) {
        float[] fArr = new float[1];
        Location.distanceBetween(address2.getLat(), address2.getLng(), address.getLat(), address.getLng(), fArr);
        return Float.valueOf(fArr[0]);
    }

    @DexIgnore
    public final void a(double d2, double d3) {
        Float f2;
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "updateObserver updateObserver=" + d2 + ',' + d3);
        AddressWrapper addressWrapper = this.n;
        if (addressWrapper == null) {
            f2 = null;
        } else if (addressWrapper != null) {
            double lat = addressWrapper.getLat();
            AddressWrapper addressWrapper2 = this.n;
            if (addressWrapper2 != null) {
                f2 = a(new Address(lat, addressWrapper2.getLng()), new Address(d2, d3));
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "updateObserver distance=" + f2);
        JSONObject jSONObject = new JSONObject();
        AddressWrapper addressWrapper3 = this.n;
        jSONObject.put("des", addressWrapper3 != null ? addressWrapper3.getId() : null);
        jSONObject.put("type", "loc_update");
        jSONObject.put("dis_to_des", f2);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
        String str = this.i;
        if (str != null) {
            String jSONObject2 = jSONObject.toString();
            kd4.a((Object) jSONObject2, "jsonObject.toString()");
            remote.i(component, session, str, "WatchAppCommuteTimeManager", jSONObject2);
            if (f2 != null) {
                if (f2 == null) {
                    kd4.a();
                    throw null;
                } else if (f2.floatValue() < ((float) FailureCode.FAILED_TO_ENABLE_MAINTAINING_CONNECTION)) {
                    if (f2 == null) {
                        kd4.a();
                        throw null;
                    } else if (f2.floatValue() < ((float) 100)) {
                        g();
                        h();
                        d();
                        return;
                    } else {
                        h();
                        e();
                        return;
                    }
                }
            }
            g();
            f();
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final String a(LocationSource.ErrorState errorState) {
        int i2 = xp2.b[errorState.ordinal()];
        if (i2 == 1) {
            return SyncErrorCode.LOCATION_ACCESS_DISABLED.getCode();
        }
        if (i2 != 2) {
            return SyncErrorCode.UNKNOWN.getCode();
        }
        return SyncErrorCode.LOCATION_SERVICE_DISABLED.getCode();
    }
}
