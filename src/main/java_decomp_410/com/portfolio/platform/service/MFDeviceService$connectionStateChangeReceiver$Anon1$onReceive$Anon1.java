package com.portfolio.platform.service;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.f42;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.ij2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rc;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1", f = "MFDeviceService.kt", l = {}, m = "invokeSuspend")
public final class MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Context $context;
    @DexIgnore
    public /* final */ /* synthetic */ Intent $intent;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService$connectionStateChangeReceiver$Anon1 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements f42.b {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public a(MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1 mFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1, String str) {
            this.a = mFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1;
            this.b = str;
        }

        @DexIgnore
        public void a(Location location, int i) {
            MFDeviceService mFDeviceService = this.a.this$Anon0.a;
            String str = this.b;
            kd4.a((Object) str, "serial");
            mFDeviceService.a(str, location, i);
            if (location != null) {
                f42.a((Context) this.a.this$Anon0.a.c()).b((f42.b) this);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1(MFDeviceService$connectionStateChangeReceiver$Anon1 mFDeviceService$connectionStateChangeReceiver$Anon1, Intent intent, Context context, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = mFDeviceService$connectionStateChangeReceiver$Anon1;
        this.$intent = intent;
        this.$context = context;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1 mFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1 = new MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1(this.this$Anon0, this.$intent, this.$context, yb4);
        mFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1.p$ = (zg4) obj;
        return mFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            String stringExtra = this.$intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = this.$intent.getIntExtra(Constants.CONNECTION_STATE, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b = MFDeviceService.U.b();
            local.d(b, "---Inside .connectionStateChangeReceiver " + stringExtra + " status " + intExtra);
            if (intExtra == ConnectionStateChange.GATT_ON.ordinal()) {
                DeviceHelper e = DeviceHelper.o.e();
                kd4.a((Object) stringExtra, "serial");
                MisfitDeviceProfile a2 = e.a(stringExtra);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String b2 = MFDeviceService.U.b();
                local2.d(b2, "---Inside .connectionStateChangeReceiver " + stringExtra + " deviceProfile=" + a2);
                this.this$Anon0.a.a(a2, stringExtra, true);
                if (!TextUtils.isEmpty(stringExtra) && kd4.a((Object) stringExtra, (Object) PortfolioApp.W.c().e())) {
                    this.this$Anon0.a.x();
                }
                this.this$Anon0.a.z();
                if (((MusicControlComponent) MusicControlComponent.o.a(this.$context)).b(stringExtra)) {
                    ((MusicControlComponent) MusicControlComponent.o.a(this.$context)).b();
                }
            } else if (intExtra == ConnectionStateChange.GATT_OFF.ordinal() && !TextUtils.isEmpty(stringExtra) && kd4.a((Object) stringExtra, (Object) PortfolioApp.W.c().e())) {
                this.this$Anon0.a.y();
            }
            PortfolioApp.W.a((Object) new ij2(stringExtra, intExtra));
            MFDeviceService mFDeviceService = this.this$Anon0.a;
            kd4.a((Object) stringExtra, "serial");
            mFDeviceService.c(stringExtra);
            if (this.this$Anon0.a.m().F()) {
                FLogger.INSTANCE.getLocal().d(MFDeviceService.U.b(), "Collect location data only user enable locate feature");
                f42.a((Context) this.this$Anon0.a).a((f42.b) new a(this, stringExtra));
            }
            FossilNotificationBar.c.a(this.this$Anon0.a);
            rc.a((Context) this.this$Anon0.a.c()).a(this.$intent);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
