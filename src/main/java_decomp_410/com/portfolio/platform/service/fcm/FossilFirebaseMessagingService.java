package com.portfolio.platform.service.fcm;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.ak3;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.jz1;
import com.fossil.blesdk.obfuscated.kd4;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FossilFirebaseMessagingService extends FirebaseMessagingService {
    @DexIgnore
    public ak3 k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void a(jz1 jz1) {
        String str;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("From: ");
        if (jz1 != null) {
            String I = jz1.I();
            if (I != null) {
                sb.append(I);
                local.d("FossilFirebaseMessagingService", sb.toString());
                if (jz1.H() != null) {
                    Map<String, String> H = jz1.H();
                    kd4.a((Object) H, "remoteMessage.data");
                    if (!H.isEmpty()) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("FossilFirebaseMessagingService", "Message data payload: " + jz1.H());
                    }
                }
                if (jz1.J() != null) {
                    jz1.a J = jz1.J();
                    if (J != null) {
                        kd4.a((Object) J, "remoteMessage.notification!!");
                        if (!TextUtils.isEmpty(J.b())) {
                            jz1.a J2 = jz1.J();
                            if (J2 != null) {
                                kd4.a((Object) J2, "remoteMessage.notification!!");
                                str = J2.b();
                                if (str == null) {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            str = "";
                        }
                        kd4.a((Object) str, "if (!TextUtils.isEmpty(r\u2026ication!!.title!! else \"\"");
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Message Notification title: ");
                        sb2.append(str);
                        sb2.append("\n body: ");
                        jz1.a J3 = jz1.J();
                        if (J3 != null) {
                            kd4.a((Object) J3, "remoteMessage.notification!!");
                            String a2 = J3.a();
                            if (a2 != null) {
                                sb2.append(a2);
                                local3.d("FossilFirebaseMessagingService", sb2.toString());
                                ak3 ak3 = this.k;
                                if (ak3 != null) {
                                    jz1.a J4 = jz1.J();
                                    if (J4 != null) {
                                        kd4.a((Object) J4, "remoteMessage.notification!!");
                                        String a3 = J4.a();
                                        if (a3 != null) {
                                            kd4.a((Object) a3, "remoteMessage.notification!!.body!!");
                                            ak3.a(str, a3);
                                            return;
                                        }
                                        kd4.a();
                                        throw null;
                                    }
                                    kd4.a();
                                    throw null;
                                }
                                kd4.d("mInAppNotificationManager");
                                throw null;
                            }
                            kd4.a();
                            throw null;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
                return;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        PortfolioApp.W.c().g().a(this);
    }
}
