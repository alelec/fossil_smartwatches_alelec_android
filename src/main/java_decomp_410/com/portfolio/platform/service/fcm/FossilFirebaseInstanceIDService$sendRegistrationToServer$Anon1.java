package com.portfolio.platform.service.fcm;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.PortfolioApp;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.fcm.FossilFirebaseInstanceIDService$sendRegistrationToServer$Anon1", f = "FossilFirebaseInstanceIDService.kt", l = {71}, m = "invokeSuspend")
public final class FossilFirebaseInstanceIDService$sendRegistrationToServer$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    public FossilFirebaseInstanceIDService$sendRegistrationToServer$Anon1(yb4 yb4) {
        super(2, yb4);
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // FossilFirebaseInstanceIDService$sendRegistrationToServer$Anon1 fossilFirebaseInstanceIDService$sendRegistrationToServer$Anon1 = new FossilFirebaseInstanceIDService$sendRegistrationToServer$Anon1(yb4);
        // fossilFirebaseInstanceIDService$sendRegistrationToServer$Anon1.p$ = (zg4) obj;
        // return fossilFirebaseInstanceIDService$sendRegistrationToServer$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((FossilFirebaseInstanceIDService$sendRegistrationToServer$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            PortfolioApp c = PortfolioApp.W.c();
            this.L$Anon0 = zg4;
            this.label = 1;
            if (c.a((yb4<? super qa4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
