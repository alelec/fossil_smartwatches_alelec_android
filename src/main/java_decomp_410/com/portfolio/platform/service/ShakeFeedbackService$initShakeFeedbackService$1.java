package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ShakeFeedbackService$initShakeFeedbackService$1 implements com.fossil.blesdk.obfuscated.hp2.C4383a {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService f21447a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$2")
    /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$2 */
    public static final class C60202 implements android.view.View.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1 f21448e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$2$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$2$1", mo27670f = "ShakeFeedbackService.kt", mo27671l = {125, 126}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$2$1 */
        public static final class C60211 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ android.graphics.Bitmap $bitmap;
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f21449p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60202 this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$2$1$1")
            @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$2$1$1", mo27670f = "ShakeFeedbackService.kt", mo27671l = {}, mo27672m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$2$1$1 */
            public static final class C60221 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
                @DexIgnore
                public int label;

                @DexIgnore
                /* renamed from: p$ */
                public com.fossil.blesdk.obfuscated.zg4 f21450p$;
                @DexIgnore
                public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60202.C60211 this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C60221(com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60202.C60211 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                    super(2, yb4);
                    this.this$0 = r1;
                }

                @DexIgnore
                public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                    com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                    com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60202.C60211.C60221 r0 = new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60202.C60211.C60221(this.this$0, yb4);
                    r0.f21450p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                    return r0;
                }

                @DexIgnore
                public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                    return ((com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60202.C60211.C60221) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
                }

                @DexIgnore
                public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                    com.fossil.blesdk.obfuscated.cc4.m20546a();
                    if (this.label == 0) {
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        com.fossil.blesdk.obfuscated.sr1 a = this.this$0.this$0.f21448e.f21447a.f21441c;
                        if (a != null) {
                            a.dismiss();
                            return com.fossil.blesdk.obfuscated.qa4.f17909a;
                        }
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C60211(com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60202 r1, android.graphics.Bitmap bitmap, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$bitmap = bitmap;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60202.C60211 r0 = new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60202.C60211(this.this$0, this.$bitmap, yb4);
                r0.f21449p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60202.C60211) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                com.fossil.blesdk.obfuscated.zg4 zg4;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    zg4 = this.f21449p$;
                    com.portfolio.platform.service.ShakeFeedbackService shakeFeedbackService = this.this$0.f21448e.f21447a;
                    android.graphics.Bitmap bitmap = this.$bitmap;
                    this.L$0 = zg4;
                    this.label = 1;
                    if (shakeFeedbackService.mo39881a(bitmap, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else if (i == 2) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
                com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60202.C60211.C60221 r3 = new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60202.C60211.C60221(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 2;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r3, this) == a) {
                    return a;
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        public C60202(com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1 shakeFeedbackService$initShakeFeedbackService$1) {
            this.f21448e = shakeFeedbackService$initShakeFeedbackService$1;
        }

        @DexIgnore
        public final void onClick(android.view.View view) {
            com.fossil.blesdk.obfuscated.ns3.C4755a aVar = com.fossil.blesdk.obfuscated.ns3.f17135a;
            java.lang.ref.WeakReference b = this.f21448e.f21447a.f21439a;
            if (b != null) {
                java.lang.Object obj = b.get();
                if (obj == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.mo29717c((android.app.Activity) obj, 123)) {
                    java.lang.ref.WeakReference b2 = this.f21448e.f21447a.f21439a;
                    if (b2 != null) {
                        java.lang.Object obj2 = b2.get();
                        if (obj2 != null) {
                            android.view.Window window = ((android.app.Activity) obj2).getWindow();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) window, "(contextWeakReference!!.get() as Activity).window");
                            android.view.View decorView = window.getDecorView();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                            android.view.View rootView = decorView.getRootView();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) rootView, "v1");
                            rootView.setDrawingCacheEnabled(true);
                            android.graphics.Bitmap createBitmap = android.graphics.Bitmap.createBitmap(rootView.getDrawingCache());
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                            rootView.setDrawingCacheEnabled(false);
                            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60202.C60211(this, createBitmap, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                            return;
                        }
                        throw new kotlin.TypeCastException("null cannot be cast to non-null type android.app.Activity");
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$3")
    /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$3 */
    public static final class C60233 implements android.view.View.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1 f21451e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$3$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$3$1", mo27670f = "ShakeFeedbackService.kt", mo27671l = {141, 142}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$3$1 */
        public static final class C60241 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ android.graphics.Bitmap $bitmap;
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f21452p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60233 this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$3$1$1")
            @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$3$1$1", mo27670f = "ShakeFeedbackService.kt", mo27671l = {}, mo27672m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$3$1$1 */
            public static final class C60251 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
                @DexIgnore
                public int label;

                @DexIgnore
                /* renamed from: p$ */
                public com.fossil.blesdk.obfuscated.zg4 f21453p$;
                @DexIgnore
                public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60233.C60241 this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C60251(com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60233.C60241 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                    super(2, yb4);
                    this.this$0 = r1;
                }

                @DexIgnore
                public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                    com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                    com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60233.C60241.C60251 r0 = new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60233.C60241.C60251(this.this$0, yb4);
                    r0.f21453p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                    return r0;
                }

                @DexIgnore
                public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                    return ((com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60233.C60241.C60251) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
                }

                @DexIgnore
                public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                    com.fossil.blesdk.obfuscated.cc4.m20546a();
                    if (this.label == 0) {
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        com.fossil.blesdk.obfuscated.sr1 a = this.this$0.this$0.f21451e.f21447a.f21441c;
                        if (a != null) {
                            a.dismiss();
                            return com.fossil.blesdk.obfuscated.qa4.f17909a;
                        }
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C60241(com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60233 r1, android.graphics.Bitmap bitmap, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$bitmap = bitmap;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60233.C60241 r0 = new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60233.C60241(this.this$0, this.$bitmap, yb4);
                r0.f21452p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60233.C60241) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                com.fossil.blesdk.obfuscated.zg4 zg4;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    zg4 = this.f21452p$;
                    com.portfolio.platform.service.ShakeFeedbackService shakeFeedbackService = this.this$0.f21451e.f21447a;
                    android.graphics.Bitmap bitmap = this.$bitmap;
                    this.L$0 = zg4;
                    this.label = 1;
                    if (shakeFeedbackService.mo39881a(bitmap, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else if (i == 2) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
                com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60233.C60241.C60251 r3 = new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60233.C60241.C60251(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 2;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r3, this) == a) {
                    return a;
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        public C60233(com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1 shakeFeedbackService$initShakeFeedbackService$1) {
            this.f21451e = shakeFeedbackService$initShakeFeedbackService$1;
        }

        @DexIgnore
        public final void onClick(android.view.View view) {
            this.f21451e.f21447a.f21444f = 0;
            com.fossil.blesdk.obfuscated.ns3.C4755a aVar = com.fossil.blesdk.obfuscated.ns3.f17135a;
            java.lang.ref.WeakReference b = this.f21451e.f21447a.f21439a;
            if (b != null) {
                java.lang.Object obj = b.get();
                if (obj == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.mo29717c((android.app.Activity) obj, 123)) {
                    java.lang.ref.WeakReference b2 = this.f21451e.f21447a.f21439a;
                    if (b2 != null) {
                        java.lang.Object obj2 = b2.get();
                        if (obj2 != null) {
                            android.view.Window window = ((android.app.Activity) obj2).getWindow();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) window, "(contextWeakReference!!.get() as Activity).window");
                            android.view.View decorView = window.getDecorView();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                            android.view.View rootView = decorView.getRootView();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) rootView, "v1");
                            rootView.setDrawingCacheEnabled(true);
                            android.graphics.Bitmap createBitmap = android.graphics.Bitmap.createBitmap(rootView.getDrawingCache());
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                            rootView.setDrawingCacheEnabled(false);
                            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60233.C60241(this, createBitmap, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                            return;
                        }
                        throw new kotlin.TypeCastException("null cannot be cast to non-null type android.app.Activity");
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$4")
    /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$4 */
    public static final class C60264 implements android.view.View.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1 f21454e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$4$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$4$1", mo27670f = "ShakeFeedbackService.kt", mo27671l = {159}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$4$1 */
        public static final class C60271 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ android.graphics.Bitmap $bitmap;
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f21455p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60264 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C60271(com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60264 r1, android.graphics.Bitmap bitmap, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$bitmap = bitmap;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60264.C60271 r0 = new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60264.C60271(this.this$0, this.$bitmap, yb4);
                r0.f21455p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60264.C60271) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21455p$;
                    com.portfolio.platform.service.ShakeFeedbackService shakeFeedbackService = this.this$0.f21454e.f21447a;
                    android.graphics.Bitmap bitmap = this.$bitmap;
                    this.L$0 = zg4;
                    this.label = 1;
                    if (shakeFeedbackService.mo39881a(bitmap, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        public C60264(com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1 shakeFeedbackService$initShakeFeedbackService$1) {
            this.f21454e = shakeFeedbackService$initShakeFeedbackService$1;
        }

        @DexIgnore
        public final void onClick(android.view.View view) {
            this.f21454e.f21447a.f21444f = 1;
            com.fossil.blesdk.obfuscated.ns3.C4755a aVar = com.fossil.blesdk.obfuscated.ns3.f17135a;
            java.lang.ref.WeakReference b = this.f21454e.f21447a.f21439a;
            if (b != null) {
                java.lang.Object obj = b.get();
                if (obj == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.mo29717c((android.app.Activity) obj, 123)) {
                    java.lang.ref.WeakReference b2 = this.f21454e.f21447a.f21439a;
                    if (b2 != null) {
                        java.lang.Object obj2 = b2.get();
                        if (obj2 != null) {
                            android.view.Window window = ((android.app.Activity) obj2).getWindow();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) window, "(contextWeakReference!!.get() as Activity).window");
                            android.view.View decorView = window.getDecorView();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                            android.view.View rootView = decorView.getRootView();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) rootView, "v1");
                            rootView.setDrawingCacheEnabled(true);
                            if (rootView.getDrawingCache() != null) {
                                android.graphics.Bitmap createBitmap = android.graphics.Bitmap.createBitmap(rootView.getDrawingCache());
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                                rootView.setDrawingCacheEnabled(false);
                                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60264.C60271(this, createBitmap, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                            }
                            com.fossil.blesdk.obfuscated.sr1 a = this.f21454e.f21447a.f21441c;
                            if (a != null) {
                                a.dismiss();
                            } else {
                                com.fossil.blesdk.obfuscated.kd4.m24405a();
                                throw null;
                            }
                        } else {
                            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.app.Activity");
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$5")
    /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$5 */
    public static final class C60285 implements android.view.View.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1 f21456e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$5$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$5$1", mo27670f = "ShakeFeedbackService.kt", mo27671l = {178}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$5$1 */
        public static final class C60291 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ android.graphics.Bitmap $bitmap;
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f21457p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60285 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C60291(com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60285 r1, android.graphics.Bitmap bitmap, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$bitmap = bitmap;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60285.C60291 r0 = new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60285.C60291(this.this$0, this.$bitmap, yb4);
                r0.f21457p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60285.C60291) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21457p$;
                    com.portfolio.platform.service.ShakeFeedbackService shakeFeedbackService = this.this$0.f21456e.f21447a;
                    android.graphics.Bitmap bitmap = this.$bitmap;
                    this.L$0 = zg4;
                    this.label = 1;
                    if (shakeFeedbackService.mo39881a(bitmap, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        public C60285(com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1 shakeFeedbackService$initShakeFeedbackService$1) {
            this.f21456e = shakeFeedbackService$initShakeFeedbackService$1;
        }

        @DexIgnore
        public final void onClick(android.view.View view) {
            this.f21456e.f21447a.f21444f = 2;
            com.fossil.blesdk.obfuscated.ns3.C4755a aVar = com.fossil.blesdk.obfuscated.ns3.f17135a;
            java.lang.ref.WeakReference b = this.f21456e.f21447a.f21439a;
            if (b != null) {
                java.lang.Object obj = b.get();
                if (obj == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.mo29717c((android.app.Activity) obj, 123)) {
                    java.lang.ref.WeakReference b2 = this.f21456e.f21447a.f21439a;
                    if (b2 != null) {
                        java.lang.Object obj2 = b2.get();
                        if (obj2 != null) {
                            android.view.Window window = ((android.app.Activity) obj2).getWindow();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) window, "(contextWeakReference!!.get() as Activity).window");
                            android.view.View decorView = window.getDecorView();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                            android.view.View rootView = decorView.getRootView();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) rootView, "v1");
                            rootView.setDrawingCacheEnabled(true);
                            if (rootView.getDrawingCache() != null) {
                                android.graphics.Bitmap createBitmap = android.graphics.Bitmap.createBitmap(rootView.getDrawingCache());
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                                rootView.setDrawingCacheEnabled(false);
                                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60285.C60291(this, createBitmap, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                            }
                            com.fossil.blesdk.obfuscated.sr1 a = this.f21456e.f21447a.f21441c;
                            if (a != null) {
                                a.dismiss();
                            } else {
                                com.fossil.blesdk.obfuscated.kd4.m24405a();
                                throw null;
                            }
                        } else {
                            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.app.Activity");
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$6")
    /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$6 */
    public static final class C60306 implements android.view.View.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1 f21458e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$6$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$6$1", mo27670f = "ShakeFeedbackService.kt", mo27671l = {191}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$6$1 */
        public static final class C60311 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f21459p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60306 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C60311(com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60306 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60306.C60311 r0 = new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60306.C60311(this.this$0, yb4);
                r0.f21459p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60306.C60311) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21459p$;
                    com.portfolio.platform.service.ShakeFeedbackService shakeFeedbackService = this.this$0.f21458e.f21447a;
                    this.L$0 = zg4;
                    this.label = 1;
                    if (shakeFeedbackService.mo39882a((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                com.fossil.blesdk.obfuscated.sr1 a2 = this.this$0.f21458e.f21447a.f21441c;
                if (a2 != null) {
                    a2.dismiss();
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }

        @DexIgnore
        public C60306(com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1 shakeFeedbackService$initShakeFeedbackService$1) {
            this.f21458e = shakeFeedbackService$initShakeFeedbackService$1;
        }

        @DexIgnore
        public final void onClick(android.view.View view) {
            this.f21458e.f21447a.f21444f = 1;
            com.fossil.blesdk.obfuscated.ns3.C4755a aVar = com.fossil.blesdk.obfuscated.ns3.f17135a;
            java.lang.ref.WeakReference b = this.f21458e.f21447a.f21439a;
            if (b != null) {
                java.lang.Object obj = b.get();
                if (obj == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.mo29717c((android.app.Activity) obj, 123)) {
                    com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60306.C60311(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$a")
    /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$a */
    public static final class C6032a implements android.view.View.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1 f21460e;

        @DexIgnore
        public C6032a(com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1 shakeFeedbackService$initShakeFeedbackService$1) {
            this.f21460e = shakeFeedbackService$initShakeFeedbackService$1;
        }

        @DexIgnore
        public final void onClick(android.view.View view) {
            this.f21460e.f21447a.f21444f = 1;
            com.fossil.blesdk.obfuscated.ns3.C4755a aVar = com.fossil.blesdk.obfuscated.ns3.f17135a;
            java.lang.ref.WeakReference b = this.f21460e.f21447a.f21439a;
            if (b != null) {
                java.lang.Object obj = b.get();
                if (obj == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.mo29717c((android.app.Activity) obj, 123)) {
                    this.f21460e.f21447a.mo39888d();
                    com.fossil.blesdk.obfuscated.sr1 a = this.f21460e.f21447a.f21441c;
                    if (a != null) {
                        a.dismiss();
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$b")
    /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$b */
    public static final class C6033b implements android.view.View.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1 f21461e;

        @DexIgnore
        public C6033b(com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1 shakeFeedbackService$initShakeFeedbackService$1) {
            this.f21461e = shakeFeedbackService$initShakeFeedbackService$1;
        }

        @DexIgnore
        public final void onClick(android.view.View view) {
            com.portfolio.platform.p007ui.debug.DebugActivity.C6109a aVar = com.portfolio.platform.p007ui.debug.DebugActivity.f21735P;
            java.lang.ref.WeakReference b = this.f21461e.f21447a.f21439a;
            if (b != null) {
                java.lang.Object obj = b.get();
                if (obj != null) {
                    com.fossil.blesdk.obfuscated.kd4.m24407a(obj, "contextWeakReference!!.get()!!");
                    aVar.mo40249a((android.content.Context) obj);
                    com.fossil.blesdk.obfuscated.sr1 a = this.f21461e.f21447a.f21441c;
                    if (a != null) {
                        a.dismiss();
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
    }

    @DexIgnore
    public ShakeFeedbackService$initShakeFeedbackService$1(com.portfolio.platform.service.ShakeFeedbackService shakeFeedbackService) {
        this.f21447a = shakeFeedbackService;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x013c  */
    /* renamed from: a */
    public final void mo28054a() {
        com.fossil.blesdk.obfuscated.sr1 a;
        java.lang.ref.WeakReference b = this.f21447a.f21439a;
        if (b == null) {
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        } else if (com.fossil.blesdk.obfuscated.uk2.m28789a((android.content.Context) b.get()) && !this.f21447a.mo39887c()) {
            if (this.f21447a.f21441c != null) {
                com.fossil.blesdk.obfuscated.sr1 a2 = this.f21447a.f21441c;
                if (a2 != null) {
                    a2.dismiss();
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            }
            java.lang.ref.WeakReference b2 = this.f21447a.f21439a;
            if (b2 != null) {
                java.lang.Object obj = b2.get();
                if (obj != null) {
                    java.lang.Object systemService = ((android.content.Context) obj).getSystemService("layout_inflater");
                    if (systemService != null) {
                        android.view.View inflate = ((android.view.LayoutInflater) systemService).inflate(com.fossil.wearables.fossil.R.layout.debug_option_sheet, (android.view.ViewGroup) null);
                        com.portfolio.platform.service.ShakeFeedbackService shakeFeedbackService = this.f21447a;
                        java.lang.ref.WeakReference b3 = shakeFeedbackService.f21439a;
                        if (b3 != null) {
                            java.lang.Object obj2 = b3.get();
                            if (obj2 != null) {
                                shakeFeedbackService.f21441c = new com.fossil.blesdk.obfuscated.sr1((android.content.Context) obj2);
                                com.fossil.blesdk.obfuscated.sr1 a3 = this.f21447a.f21441c;
                                if (a3 != null) {
                                    a3.setContentView(inflate);
                                    android.view.View findViewById = inflate.findViewById(com.fossil.wearables.fossil.R.id.tv_version_name);
                                    if (findViewById != null) {
                                        ((android.widget.TextView) findViewById).setText("4.1.2");
                                        inflate.findViewById(com.fossil.wearables.fossil.R.id.send_sw_question_feedback).setOnClickListener(new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C6032a(this));
                                        inflate.findViewById(com.fossil.wearables.fossil.R.id.send_sw_feedback).setOnClickListener(new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60202(this));
                                        inflate.findViewById(com.fossil.wearables.fossil.R.id.send_beta_feedback).setOnClickListener(new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60233(this));
                                        inflate.findViewById(com.fossil.wearables.fossil.R.id.send_general_feedback).setOnClickListener(new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60264(this));
                                        inflate.findViewById(com.fossil.wearables.fossil.R.id.send_uat_feedback).setOnClickListener(new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60285(this));
                                        inflate.findViewById(com.fossil.wearables.fossil.R.id.send_fw_log).setOnClickListener(new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C60306(this));
                                        android.view.View findViewById2 = inflate.findViewById(com.fossil.wearables.fossil.R.id.ll_open_debug_screen);
                                        java.lang.ref.WeakReference b4 = this.f21447a.f21439a;
                                        if (b4 != null) {
                                            if (!(b4.get() instanceof com.portfolio.platform.p007ui.debug.DebugActivity)) {
                                                java.lang.ref.WeakReference b5 = this.f21447a.f21439a;
                                                if (b5 == null) {
                                                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                                                    throw null;
                                                } else if (!(b5.get() instanceof com.portfolio.platform.p007ui.debug.LogcatActivity)) {
                                                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) findViewById2, "llOpenDebug");
                                                    findViewById2.setVisibility(0);
                                                    inflate.findViewById(com.fossil.wearables.fossil.R.id.open_debug_screen).setOnClickListener(new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C6033b(this));
                                                    a = this.f21447a.f21441c;
                                                    if (a == null) {
                                                        a.show();
                                                        return;
                                                    } else {
                                                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                                                        throw null;
                                                    }
                                                }
                                            }
                                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) findViewById2, "llOpenDebug");
                                            findViewById2.setVisibility(8);
                                            inflate.findViewById(com.fossil.wearables.fossil.R.id.open_debug_screen).setOnClickListener(new com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1.C6033b(this));
                                            a = this.f21447a.f21441c;
                                            if (a == null) {
                                            }
                                        } else {
                                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                                            throw null;
                                        }
                                    } else {
                                        throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
                                    }
                                } else {
                                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                                    throw null;
                                }
                            } else {
                                com.fossil.blesdk.obfuscated.kd4.m24405a();
                                throw null;
                            }
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                    } else {
                        throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.LayoutInflater");
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
    }
}
