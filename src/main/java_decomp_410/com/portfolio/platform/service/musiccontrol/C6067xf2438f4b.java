package com.portfolio.platform.service.musiccontrol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$2 */
public final class C6067xf2438f4b extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super android.support.p000v4.media.session.MediaControllerCompat>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.support.p000v4.media.MediaBrowserCompat $it;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.zg4 $this_launch$inlined;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21570p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6067xf2438f4b(android.support.p000v4.media.MediaBrowserCompat mediaBrowserCompat, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1 musicControlComponent$OldVersionMusicController$connect$1, com.fossil.blesdk.obfuscated.zg4 zg4) {
        super(2, yb4);
        this.$it = mediaBrowserCompat;
        this.this$0 = musicControlComponent$OldVersionMusicController$connect$1;
        this.$this_launch$inlined = zg4;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.musiccontrol.C6067xf2438f4b musicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$2 = new com.portfolio.platform.service.musiccontrol.C6067xf2438f4b(this.$it, yb4, this.this$0, this.$this_launch$inlined);
        musicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$2.f21570p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return musicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.musiccontrol.C6067xf2438f4b) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return this.this$0.this$0.mo39996b(this.$it);
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
