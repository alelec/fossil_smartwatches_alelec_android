package com.portfolio.platform.service.musiccontrol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicControlComponent$enableNotificationListener$2$isHasThisController$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.portfolio.platform.service.musiccontrol.MusicControlComponent.a, java.lang.Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ android.media.session.MediaController $activeMediaController;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$enableNotificationListener$2$isHasThisController$1(android.media.session.MediaController mediaController) {
        super(1);
        this.$activeMediaController = mediaController;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke((com.portfolio.platform.service.musiccontrol.MusicControlComponent.a) obj));
    }

    @DexIgnore
    public final boolean invoke(com.portfolio.platform.service.musiccontrol.MusicControlComponent.a aVar) {
        com.fossil.blesdk.obfuscated.kd4.b(aVar, "musicController");
        java.lang.String c = aVar.c();
        android.media.session.MediaController mediaController = this.$activeMediaController;
        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) mediaController, "activeMediaController");
        return com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) c, (java.lang.Object) mediaController.getPackageName());
    }
}
