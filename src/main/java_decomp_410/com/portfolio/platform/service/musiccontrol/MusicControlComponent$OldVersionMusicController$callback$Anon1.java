package com.portfolio.platform.service.musiccontrol;

import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import com.facebook.internal.AnalyticsEvents;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qj2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicControlComponent$OldVersionMusicController$callback$Anon1 extends MediaControllerCompat.a {
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.OldVersionMusicController d;
    @DexIgnore
    public /* final */ /* synthetic */ String e;

    @DexIgnore
    public MusicControlComponent$OldVersionMusicController$callback$Anon1(MusicControlComponent.OldVersionMusicController oldVersionMusicController, String str) {
        this.d = oldVersionMusicController;
        this.e = str;
    }

    @DexIgnore
    public void a(PlaybackStateCompat playbackStateCompat) {
        super.a(playbackStateCompat);
        int a = this.d.a(playbackStateCompat);
        if (a != this.d.h()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MusicControlComponent.o.a();
            StringBuilder sb = new StringBuilder();
            sb.append(".onPlaybackStateChanged, PlaybackStateCompat = ");
            sb.append(playbackStateCompat != null ? Integer.valueOf(playbackStateCompat.getState()) : null);
            local.d(a2, sb.toString());
            int h = this.d.h();
            this.d.a(a);
            MusicControlComponent.OldVersionMusicController oldVersionMusicController = this.d;
            oldVersionMusicController.a(h, a, (MusicControlComponent.a) oldVersionMusicController);
        }
    }

    @DexIgnore
    public void b() {
        super.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a = MusicControlComponent.o.a();
        local.d(a, ".onSessionReady, package=" + this.d.c());
    }

    @DexIgnore
    public void a(MediaMetadataCompat mediaMetadataCompat) {
        super.a(mediaMetadataCompat);
        if (mediaMetadataCompat != null) {
            String a = qj2.a(mediaMetadataCompat.a("android.media.metadata.TITLE"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            String a2 = qj2.a(mediaMetadataCompat.a("android.media.metadata.ARTIST"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            String a3 = qj2.a(mediaMetadataCompat.a("android.media.metadata.ALBUM"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a4 = MusicControlComponent.o.a();
            StringBuilder sb = new StringBuilder();
            sb.append(".onMetadataChanged, title=");
            sb.append(a);
            sb.append(", artist=");
            sb.append(a2);
            sb.append(", album=");
            sb.append(a3);
            sb.append(", state=");
            MediaControllerCompat j = this.d.j();
            sb.append(j != null ? j.b() : null);
            local.d(a4, sb.toString());
            MusicControlComponent.b bVar = new MusicControlComponent.b(this.d.c(), this.e, a, a2, a3);
            if (!kd4.a((Object) bVar, (Object) this.d.g())) {
                MusicControlComponent.b g = this.d.g();
                this.d.a(bVar);
                this.d.a(g, bVar);
            }
        }
    }

    @DexIgnore
    public void a() {
        super.a();
        MusicControlComponent.OldVersionMusicController oldVersionMusicController = this.d;
        oldVersionMusicController.a((MusicControlComponent.a) oldVersionMusicController);
        fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new MusicControlComponent$OldVersionMusicController$callback$Anon1$onSessionDestroyed$Anon1(this, (yb4) null), 3, (Object) null);
    }
}
