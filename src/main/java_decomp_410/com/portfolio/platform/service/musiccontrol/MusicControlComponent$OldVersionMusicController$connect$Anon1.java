package com.portfolio.platform.service.musiccontrol;

import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaControllerCompat;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dl4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$Anon1", f = "MusicControlComponent.kt", l = {872, 558, 565, 569}, m = "invokeSuspend")
public final class MusicControlComponent$OldVersionMusicController$connect$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.OldVersionMusicController this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$OldVersionMusicController$connect$Anon1(MusicControlComponent.OldVersionMusicController oldVersionMusicController, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = oldVersionMusicController;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MusicControlComponent$OldVersionMusicController$connect$Anon1 musicControlComponent$OldVersionMusicController$connect$Anon1 = new MusicControlComponent$OldVersionMusicController$connect$Anon1(this.this$Anon0, yb4);
        musicControlComponent$OldVersionMusicController$connect$Anon1.p$ = (zg4) obj;
        return musicControlComponent$OldVersionMusicController$connect$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MusicControlComponent$OldVersionMusicController$connect$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e1 A[Catch:{ all -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0120 A[Catch:{ all -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0141 A[Catch:{ all -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0148 A[Catch:{ all -> 0x0065 }] */
    public final Object invokeSuspend(Object obj) {
        dl4 dl4;
        dl4 dl42;
        zg4 zg4;
        gh4 gh4;
        MediaBrowserCompat mediaBrowserCompat;
        MusicControlComponent.OldVersionMusicController oldVersionMusicController;
        MusicControlComponent.OldVersionMusicController oldVersionMusicController2;
        MediaBrowserCompat i;
        zg4 zg42;
        dl4 dl43;
        Object a = cc4.a();
        int i2 = this.label;
        if (i2 == 0) {
            na4.a(obj);
            zg42 = this.p$;
            dl43 = ((MusicControlComponent) MusicControlComponent.o.a(PortfolioApp.W.c())).f();
            this.L$Anon0 = zg42;
            this.L$Anon1 = dl43;
            this.label = 1;
            if (dl43.a((Object) null, this) == a) {
                return a;
            }
        } else if (i2 == 1) {
            dl43 = (dl4) this.L$Anon1;
            na4.a(obj);
            zg42 = (zg4) this.L$Anon0;
        } else if (i2 == 2) {
            oldVersionMusicController2 = (MusicControlComponent.OldVersionMusicController) this.L$Anon3;
            gh4 = (gh4) this.L$Anon2;
            dl4 = (dl4) this.L$Anon1;
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            oldVersionMusicController2.a((MediaBrowserCompat) obj);
            i = this.this$Anon0.i();
            if (i != null) {
                this.this$Anon0.a((MusicControlComponent.OldVersionMusicController) null);
                dl42 = dl4;
                qa4 qa4 = qa4.a;
                dl42.a((Object) null);
                return qa4.a;
            } else if (!i.d()) {
                this.this$Anon0.a((MusicControlComponent.OldVersionMusicController) null);
                qa4 qa42 = qa4.a;
                dl4.a((Object) null);
                return qa42;
            } else {
                oldVersionMusicController = this.this$Anon0;
                pi4 c = nh4.c();
                MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2 musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2 = new MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2(i, (yb4) null, this, zg4);
                this.L$Anon0 = zg4;
                this.L$Anon1 = dl4;
                this.L$Anon2 = gh4;
                this.L$Anon3 = i;
                this.L$Anon4 = oldVersionMusicController;
                this.label = 3;
                Object a2 = yf4.a(c, musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2, this);
                if (a2 == a) {
                    return a;
                }
                Object obj2 = a2;
                mediaBrowserCompat = i;
                obj = obj2;
                oldVersionMusicController.a((MediaControllerCompat) obj);
                if (this.this$Anon0.j() == null) {
                }
                dl42 = dl4;
                qa4 qa43 = qa4.a;
                dl42.a((Object) null);
                return qa4.a;
            }
        } else if (i2 == 3) {
            oldVersionMusicController = (MusicControlComponent.OldVersionMusicController) this.L$Anon4;
            mediaBrowserCompat = (MediaBrowserCompat) this.L$Anon3;
            gh4 = (gh4) this.L$Anon2;
            dl4 = (dl4) this.L$Anon1;
            zg4 = (zg4) this.L$Anon0;
            try {
                na4.a(obj);
                oldVersionMusicController.a((MediaControllerCompat) obj);
                if (this.this$Anon0.j() == null) {
                    this.this$Anon0.a(this.this$Anon0);
                    pi4 c2 = nh4.c();
                    MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3 musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3 = new MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3((yb4) null, this, zg4);
                    this.L$Anon0 = zg4;
                    this.L$Anon1 = dl4;
                    this.L$Anon2 = gh4;
                    this.L$Anon3 = mediaBrowserCompat;
                    this.label = 4;
                    if (yf4.a(c2, musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3, this) == a) {
                        return a;
                    }
                } else {
                    this.this$Anon0.a((MusicControlComponent.OldVersionMusicController) null);
                }
                dl42 = dl4;
                qa4 qa432 = qa4.a;
                dl42.a((Object) null);
                return qa4.a;
            } catch (Throwable th) {
                th = th;
            }
        } else if (i2 == 4) {
            MediaBrowserCompat mediaBrowserCompat2 = (MediaBrowserCompat) this.L$Anon3;
            gh4 gh42 = (gh4) this.L$Anon2;
            dl42 = (dl4) this.L$Anon1;
            zg4 zg43 = (zg4) this.L$Anon0;
            try {
                na4.a(obj);
                dl4 = dl42;
                dl42 = dl4;
                qa4 qa4322 = qa4.a;
                dl42.a((Object) null);
                return qa4.a;
            } catch (Throwable th2) {
                th = th2;
                dl4 = dl42;
                dl4.a((Object) null);
                throw th;
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            FLogger.INSTANCE.getLocal().d(MusicControlComponent.o.a(), "connecting");
            gh4 a3 = ag4.a(zg42, nh4.c(), (CoroutineStart) null, new MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1((yb4) null, this, zg42), 2, (Object) null);
            MusicControlComponent.OldVersionMusicController oldVersionMusicController3 = this.this$Anon0;
            this.L$Anon0 = zg42;
            this.L$Anon1 = dl43;
            this.L$Anon2 = a3;
            this.L$Anon3 = oldVersionMusicController3;
            this.label = 2;
            Object a4 = a3.a(this);
            if (a4 == a) {
                return a;
            }
            MusicControlComponent.OldVersionMusicController oldVersionMusicController4 = oldVersionMusicController3;
            zg4 = zg42;
            obj = a4;
            gh4 = a3;
            dl4 = dl43;
            oldVersionMusicController2 = oldVersionMusicController4;
            oldVersionMusicController2.a((MediaBrowserCompat) obj);
            i = this.this$Anon0.i();
            if (i != null) {
            }
        } catch (Throwable th3) {
            th = th3;
            dl4 = dl43;
            dl4.a((Object) null);
            throw th;
        }
    }
}
