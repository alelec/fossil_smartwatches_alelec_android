package com.portfolio.platform.service.musiccontrol;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$NewNotificationMusicController$playbackState$Anon1", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
public final class MusicControlComponent$NewNotificationMusicController$playbackState$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Integer $currentState;
    @DexIgnore
    public /* final */ /* synthetic */ int $oldState;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.NewNotificationMusicController this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$NewNotificationMusicController$playbackState$Anon1(MusicControlComponent.NewNotificationMusicController newNotificationMusicController, int i, Integer num, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = newNotificationMusicController;
        this.$oldState = i;
        this.$currentState = num;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // MusicControlComponent$NewNotificationMusicController$playbackState$Anon1 musicControlComponent$NewNotificationMusicController$playbackState$Anon1 = new MusicControlComponent$NewNotificationMusicController$playbackState$Anon1(this.this$Anon0, this.$oldState, this.$currentState, yb4);
        // musicControlComponent$NewNotificationMusicController$playbackState$Anon1.p$ = (zg4) obj;
        // return musicControlComponent$NewNotificationMusicController$playbackState$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((MusicControlComponent$NewNotificationMusicController$playbackState$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        throw null;
        // cc4.a();
        // if (this.label == 0) {
        //     na4.a(obj);
        //     this.this$Anon0.a(this.$oldState, this.$currentState.intValue(), this.this$Anon0);
        //     return qa4.a;
        // }
        // throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
