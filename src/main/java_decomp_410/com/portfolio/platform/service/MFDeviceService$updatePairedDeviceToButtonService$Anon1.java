package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.MFDeviceService$updatePairedDeviceToButtonService$Anon1", f = "MFDeviceService.kt", l = {}, m = "invokeSuspend")
public final class MFDeviceService$updatePairedDeviceToButtonService$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$updatePairedDeviceToButtonService$Anon1(MFDeviceService mFDeviceService, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = mFDeviceService;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MFDeviceService$updatePairedDeviceToButtonService$Anon1 mFDeviceService$updatePairedDeviceToButtonService$Anon1 = new MFDeviceService$updatePairedDeviceToButtonService$Anon1(this.this$Anon0, yb4);
        mFDeviceService$updatePairedDeviceToButtonService$Anon1.p$ = (zg4) obj;
        return mFDeviceService$updatePairedDeviceToButtonService$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFDeviceService$updatePairedDeviceToButtonService$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            List<Device> allDevice = this.this$Anon0.d().getAllDevice();
            if (!allDevice.isEmpty()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = MFDeviceService.U.b();
                local.d(b, "Get all device success devicesList=" + allDevice);
                if (this.this$Anon0.r().getCurrentUser() != null) {
                    for (Device next : allDevice) {
                        this.this$Anon0.c().d(next.component1(), next.component2());
                    }
                }
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
