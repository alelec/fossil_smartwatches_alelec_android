package com.portfolio.platform.service;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.hp2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.ns3;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.sr1;
import com.fossil.blesdk.obfuscated.uk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.ui.debug.LogcatActivity;
import java.lang.ref.WeakReference;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ShakeFeedbackService$initShakeFeedbackService$Anon1 implements hp2.a {
    @DexIgnore
    public /* final */ /* synthetic */ ShakeFeedbackService a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService$initShakeFeedbackService$Anon1 e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @gc4(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$Anon1$Anon2$Anon1", f = "ShakeFeedbackService.kt", l = {125, 126}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Bitmap $bitmap;
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon2 this$Anon0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$Anon1$Anon2$Anon1$Anon1")
            @gc4(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$Anon1$Anon2$Anon1$Anon1", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$Anon1$Anon2$Anon1$Anon1  reason: collision with other inner class name */
            public static final class C0122Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public zg4 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Anon1 this$Anon0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0122Anon1(Anon1 anon1, yb4 yb4) {
                    super(2, yb4);
                    this.this$Anon0 = anon1;
                }

                @DexIgnore
                public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                    kd4.b(yb4, "completion");
                    C0122Anon1 anon1 = new C0122Anon1(this.this$Anon0, yb4);
                    anon1.p$ = (zg4) obj;
                    return anon1;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0122Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    cc4.a();
                    if (this.label == 0) {
                        na4.a(obj);
                        sr1 a = this.this$Anon0.this$Anon0.e.a.c;
                        if (a != null) {
                            a.dismiss();
                            return qa4.a;
                        }
                        kd4.a();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon2 anon2, Bitmap bitmap, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon2;
                this.$bitmap = bitmap;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, this.$bitmap, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                zg4 zg4;
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 = this.p$;
                    ShakeFeedbackService shakeFeedbackService = this.this$Anon0.e.a;
                    Bitmap bitmap = this.$bitmap;
                    this.L$Anon0 = zg4;
                    this.label = 1;
                    if (shakeFeedbackService.a(bitmap, (yb4<? super qa4>) this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    zg4 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else if (i == 2) {
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                    return qa4.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                pi4 c = nh4.c();
                C0122Anon1 anon1 = new C0122Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 2;
                if (yf4.a(c, anon1, this) == a) {
                    return a;
                }
                return qa4.a;
            }
        }

        @DexIgnore
        public Anon2(ShakeFeedbackService$initShakeFeedbackService$Anon1 shakeFeedbackService$initShakeFeedbackService$Anon1) {
            this.e = shakeFeedbackService$initShakeFeedbackService$Anon1;
        }

        @DexIgnore
        public final void onClick(View view) {
            ns3.a aVar = ns3.a;
            WeakReference b = this.e.a.a;
            if (b != null) {
                Object obj = b.get();
                if (obj == null) {
                    throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.c((Activity) obj, 123)) {
                    WeakReference b2 = this.e.a.a;
                    if (b2 != null) {
                        Object obj2 = b2.get();
                        if (obj2 != null) {
                            Window window = ((Activity) obj2).getWindow();
                            kd4.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                            View decorView = window.getDecorView();
                            kd4.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                            View rootView = decorView.getRootView();
                            kd4.a((Object) rootView, "v1");
                            rootView.setDrawingCacheEnabled(true);
                            Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                            kd4.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                            rootView.setDrawingCacheEnabled(false);
                            fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, createBitmap, (yb4) null), 3, (Object) null);
                            return;
                        }
                        throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
                    }
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3 implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService$initShakeFeedbackService$Anon1 e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @gc4(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$Anon1$Anon3$Anon1", f = "ShakeFeedbackService.kt", l = {141, 142}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Bitmap $bitmap;
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon3 this$Anon0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$Anon1$Anon3$Anon1$Anon1")
            @gc4(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$Anon1$Anon3$Anon1$Anon1", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$Anon1$Anon3$Anon1$Anon1  reason: collision with other inner class name */
            public static final class C0123Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public zg4 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Anon1 this$Anon0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0123Anon1(Anon1 anon1, yb4 yb4) {
                    super(2, yb4);
                    this.this$Anon0 = anon1;
                }

                @DexIgnore
                public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                    kd4.b(yb4, "completion");
                    C0123Anon1 anon1 = new C0123Anon1(this.this$Anon0, yb4);
                    anon1.p$ = (zg4) obj;
                    return anon1;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0123Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    cc4.a();
                    if (this.label == 0) {
                        na4.a(obj);
                        sr1 a = this.this$Anon0.this$Anon0.e.a.c;
                        if (a != null) {
                            a.dismiss();
                            return qa4.a;
                        }
                        kd4.a();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon3 anon3, Bitmap bitmap, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon3;
                this.$bitmap = bitmap;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, this.$bitmap, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                zg4 zg4;
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 = this.p$;
                    ShakeFeedbackService shakeFeedbackService = this.this$Anon0.e.a;
                    Bitmap bitmap = this.$bitmap;
                    this.L$Anon0 = zg4;
                    this.label = 1;
                    if (shakeFeedbackService.a(bitmap, (yb4<? super qa4>) this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    zg4 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else if (i == 2) {
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                    return qa4.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                pi4 c = nh4.c();
                C0123Anon1 anon1 = new C0123Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 2;
                if (yf4.a(c, anon1, this) == a) {
                    return a;
                }
                return qa4.a;
            }
        }

        @DexIgnore
        public Anon3(ShakeFeedbackService$initShakeFeedbackService$Anon1 shakeFeedbackService$initShakeFeedbackService$Anon1) {
            this.e = shakeFeedbackService$initShakeFeedbackService$Anon1;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.a.f = 0;
            ns3.a aVar = ns3.a;
            WeakReference b = this.e.a.a;
            if (b != null) {
                Object obj = b.get();
                if (obj == null) {
                    throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.c((Activity) obj, 123)) {
                    WeakReference b2 = this.e.a.a;
                    if (b2 != null) {
                        Object obj2 = b2.get();
                        if (obj2 != null) {
                            Window window = ((Activity) obj2).getWindow();
                            kd4.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                            View decorView = window.getDecorView();
                            kd4.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                            View rootView = decorView.getRootView();
                            kd4.a((Object) rootView, "v1");
                            rootView.setDrawingCacheEnabled(true);
                            Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                            kd4.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                            rootView.setDrawingCacheEnabled(false);
                            fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, createBitmap, (yb4) null), 3, (Object) null);
                            return;
                        }
                        throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
                    }
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon4 implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService$initShakeFeedbackService$Anon1 e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @gc4(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$Anon1$Anon4$Anon1", f = "ShakeFeedbackService.kt", l = {159}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Bitmap $bitmap;
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon4 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon4 anon4, Bitmap bitmap, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon4;
                this.$bitmap = bitmap;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, this.$bitmap, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 zg4 = this.p$;
                    ShakeFeedbackService shakeFeedbackService = this.this$Anon0.e.a;
                    Bitmap bitmap = this.$bitmap;
                    this.L$Anon0 = zg4;
                    this.label = 1;
                    if (shakeFeedbackService.a(bitmap, (yb4<? super qa4>) this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return qa4.a;
            }
        }

        @DexIgnore
        public Anon4(ShakeFeedbackService$initShakeFeedbackService$Anon1 shakeFeedbackService$initShakeFeedbackService$Anon1) {
            this.e = shakeFeedbackService$initShakeFeedbackService$Anon1;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.a.f = 1;
            ns3.a aVar = ns3.a;
            WeakReference b = this.e.a.a;
            if (b != null) {
                Object obj = b.get();
                if (obj == null) {
                    throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.c((Activity) obj, 123)) {
                    WeakReference b2 = this.e.a.a;
                    if (b2 != null) {
                        Object obj2 = b2.get();
                        if (obj2 != null) {
                            Window window = ((Activity) obj2).getWindow();
                            kd4.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                            View decorView = window.getDecorView();
                            kd4.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                            View rootView = decorView.getRootView();
                            kd4.a((Object) rootView, "v1");
                            rootView.setDrawingCacheEnabled(true);
                            if (rootView.getDrawingCache() != null) {
                                Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                                kd4.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                                rootView.setDrawingCacheEnabled(false);
                                fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, createBitmap, (yb4) null), 3, (Object) null);
                            }
                            sr1 a = this.e.a.c;
                            if (a != null) {
                                a.dismiss();
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon5 implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService$initShakeFeedbackService$Anon1 e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @gc4(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$Anon1$Anon5$Anon1", f = "ShakeFeedbackService.kt", l = {178}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Bitmap $bitmap;
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon5 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon5 anon5, Bitmap bitmap, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon5;
                this.$bitmap = bitmap;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, this.$bitmap, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 zg4 = this.p$;
                    ShakeFeedbackService shakeFeedbackService = this.this$Anon0.e.a;
                    Bitmap bitmap = this.$bitmap;
                    this.L$Anon0 = zg4;
                    this.label = 1;
                    if (shakeFeedbackService.a(bitmap, (yb4<? super qa4>) this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return qa4.a;
            }
        }

        @DexIgnore
        public Anon5(ShakeFeedbackService$initShakeFeedbackService$Anon1 shakeFeedbackService$initShakeFeedbackService$Anon1) {
            this.e = shakeFeedbackService$initShakeFeedbackService$Anon1;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.a.f = 2;
            ns3.a aVar = ns3.a;
            WeakReference b = this.e.a.a;
            if (b != null) {
                Object obj = b.get();
                if (obj == null) {
                    throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.c((Activity) obj, 123)) {
                    WeakReference b2 = this.e.a.a;
                    if (b2 != null) {
                        Object obj2 = b2.get();
                        if (obj2 != null) {
                            Window window = ((Activity) obj2).getWindow();
                            kd4.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                            View decorView = window.getDecorView();
                            kd4.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                            View rootView = decorView.getRootView();
                            kd4.a((Object) rootView, "v1");
                            rootView.setDrawingCacheEnabled(true);
                            if (rootView.getDrawingCache() != null) {
                                Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                                kd4.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                                rootView.setDrawingCacheEnabled(false);
                                fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, createBitmap, (yb4) null), 3, (Object) null);
                            }
                            sr1 a = this.e.a.c;
                            if (a != null) {
                                a.dismiss();
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon6 implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService$initShakeFeedbackService$Anon1 e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @gc4(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$Anon1$Anon6$Anon1", f = "ShakeFeedbackService.kt", l = {191}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon6 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon6 anon6, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon6;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 zg4 = this.p$;
                    ShakeFeedbackService shakeFeedbackService = this.this$Anon0.e.a;
                    this.L$Anon0 = zg4;
                    this.label = 1;
                    if (shakeFeedbackService.a((yb4<? super qa4>) this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                sr1 a2 = this.this$Anon0.e.a.c;
                if (a2 != null) {
                    a2.dismiss();
                    return qa4.a;
                }
                kd4.a();
                throw null;
            }
        }

        @DexIgnore
        public Anon6(ShakeFeedbackService$initShakeFeedbackService$Anon1 shakeFeedbackService$initShakeFeedbackService$Anon1) {
            this.e = shakeFeedbackService$initShakeFeedbackService$Anon1;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.a.f = 1;
            ns3.a aVar = ns3.a;
            WeakReference b = this.e.a.a;
            if (b != null) {
                Object obj = b.get();
                if (obj == null) {
                    throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.c((Activity) obj, 123)) {
                    fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (yb4) null), 3, (Object) null);
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService$initShakeFeedbackService$Anon1 e;

        @DexIgnore
        public a(ShakeFeedbackService$initShakeFeedbackService$Anon1 shakeFeedbackService$initShakeFeedbackService$Anon1) {
            this.e = shakeFeedbackService$initShakeFeedbackService$Anon1;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.a.f = 1;
            ns3.a aVar = ns3.a;
            WeakReference b = this.e.a.a;
            if (b != null) {
                Object obj = b.get();
                if (obj == null) {
                    throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.c((Activity) obj, 123)) {
                    this.e.a.d();
                    sr1 a = this.e.a.c;
                    if (a != null) {
                        a.dismiss();
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService$initShakeFeedbackService$Anon1 e;

        @DexIgnore
        public b(ShakeFeedbackService$initShakeFeedbackService$Anon1 shakeFeedbackService$initShakeFeedbackService$Anon1) {
            this.e = shakeFeedbackService$initShakeFeedbackService$Anon1;
        }

        @DexIgnore
        public final void onClick(View view) {
            DebugActivity.a aVar = DebugActivity.P;
            WeakReference b = this.e.a.a;
            if (b != null) {
                Object obj = b.get();
                if (obj != null) {
                    kd4.a(obj, "contextWeakReference!!.get()!!");
                    aVar.a((Context) obj);
                    sr1 a = this.e.a.c;
                    if (a != null) {
                        a.dismiss();
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public ShakeFeedbackService$initShakeFeedbackService$Anon1(ShakeFeedbackService shakeFeedbackService) {
        this.a = shakeFeedbackService;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x013c  */
    public final void a() {
        sr1 a2;
        WeakReference b2 = this.a.a;
        if (b2 == null) {
            kd4.a();
            throw null;
        } else if (uk2.a((Context) b2.get()) && !this.a.c()) {
            if (this.a.c != null) {
                sr1 a3 = this.a.c;
                if (a3 != null) {
                    a3.dismiss();
                } else {
                    kd4.a();
                    throw null;
                }
            }
            WeakReference b3 = this.a.a;
            if (b3 != null) {
                Object obj = b3.get();
                if (obj != null) {
                    Object systemService = ((Context) obj).getSystemService("layout_inflater");
                    if (systemService != null) {
                        View inflate = ((LayoutInflater) systemService).inflate(R.layout.debug_option_sheet, (ViewGroup) null);
                        ShakeFeedbackService shakeFeedbackService = this.a;
                        WeakReference b4 = shakeFeedbackService.a;
                        if (b4 != null) {
                            Object obj2 = b4.get();
                            if (obj2 != null) {
                                shakeFeedbackService.c = new sr1((Context) obj2);
                                sr1 a4 = this.a.c;
                                if (a4 != null) {
                                    a4.setContentView(inflate);
                                    View findViewById = inflate.findViewById(R.id.tv_version_name);
                                    if (findViewById != null) {
                                        ((TextView) findViewById).setText("4.1.2");
                                        inflate.findViewById(R.id.send_sw_question_feedback).setOnClickListener(new a(this));
                                        inflate.findViewById(R.id.send_sw_feedback).setOnClickListener(new Anon2(this));
                                        inflate.findViewById(R.id.send_beta_feedback).setOnClickListener(new Anon3(this));
                                        inflate.findViewById(R.id.send_general_feedback).setOnClickListener(new Anon4(this));
                                        inflate.findViewById(R.id.send_uat_feedback).setOnClickListener(new Anon5(this));
                                        inflate.findViewById(R.id.send_fw_log).setOnClickListener(new Anon6(this));
                                        View findViewById2 = inflate.findViewById(R.id.ll_open_debug_screen);
                                        WeakReference b5 = this.a.a;
                                        if (b5 != null) {
                                            if (!(b5.get() instanceof DebugActivity)) {
                                                WeakReference b6 = this.a.a;
                                                if (b6 == null) {
                                                    kd4.a();
                                                    throw null;
                                                } else if (!(b6.get() instanceof LogcatActivity)) {
                                                    kd4.a((Object) findViewById2, "llOpenDebug");
                                                    findViewById2.setVisibility(0);
                                                    inflate.findViewById(R.id.open_debug_screen).setOnClickListener(new b(this));
                                                    a2 = this.a.c;
                                                    if (a2 == null) {
                                                        a2.show();
                                                        return;
                                                    } else {
                                                        kd4.a();
                                                        throw null;
                                                    }
                                                }
                                            }
                                            kd4.a((Object) findViewById2, "llOpenDebug");
                                            findViewById2.setVisibility(8);
                                            inflate.findViewById(R.id.open_debug_screen).setOnClickListener(new b(this));
                                            a2 = this.a.c;
                                            if (a2 == null) {
                                            }
                                        } else {
                                            kd4.a();
                                            throw null;
                                        }
                                    } else {
                                        throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
                                    }
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type android.view.LayoutInflater");
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }
}
