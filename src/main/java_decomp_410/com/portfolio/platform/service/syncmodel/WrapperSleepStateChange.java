package com.portfolio.platform.service.syncmodel;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.List;
import org.parceler.Parcel;
import org.parceler.ParcelConstructor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@Parcel
public class WrapperSleepStateChange {
    @DexIgnore
    public long index;
    @DexIgnore
    public int state;

    @DexIgnore
    public enum SleepState {
        WAKE(0),
        SLEEP(1),
        DEEP_SLEEP(2);
        
        @DexIgnore
        public int value;

        @DexIgnore
        SleepState(int i) {
            this.value = i;
        }

        @DexIgnore
        public static SleepState getSleepStateFromValue(int i) {
            for (SleepState sleepState : values()) {
                if (sleepState.value == i) {
                    return sleepState;
                }
            }
            return WAKE;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends TypeToken<List<WrapperSleepStateChange>> {
    }

    @DexIgnore
    @ParcelConstructor
    public WrapperSleepStateChange(int i, long j) {
        this.state = i;
        this.index = j;
    }

    @DexIgnore
    public static List<WrapperSleepStateChange> getSleepStateChanges(String str, int i) {
        List<WrapperSleepStateChange> list = (List) new Gson().a(str, new a().getType());
        list.add(new WrapperSleepStateChange(-1, (long) i));
        return list;
    }

    @DexIgnore
    public long getIndex() {
        return this.index;
    }

    @DexIgnore
    public SleepState getSleepState() {
        return SleepState.getSleepStateFromValue(this.state);
    }

    @DexIgnore
    public int getState() {
        return this.state;
    }
}
