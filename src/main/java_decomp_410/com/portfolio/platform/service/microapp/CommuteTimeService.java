package com.portfolio.platform.service.microapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import com.facebook.applinks.AppLinkData;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lr2;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.oc1;
import com.fossil.blesdk.obfuscated.pp2;
import com.fossil.blesdk.obfuscated.qc1;
import com.fossil.blesdk.obfuscated.rc1;
import com.fossil.blesdk.obfuscated.rn1;
import com.fossil.blesdk.obfuscated.rp2;
import com.fossil.blesdk.obfuscated.sc1;
import com.fossil.blesdk.obfuscated.sn1;
import com.fossil.blesdk.obfuscated.tc1;
import com.fossil.blesdk.obfuscated.tn1;
import com.fossil.blesdk.obfuscated.uc1;
import com.fossil.blesdk.obfuscated.wn1;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc1;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.Mapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.microapp.CommuteTimeETAMicroAppResponse;
import com.misfit.frameworks.buttonservice.model.microapp.CommuteTimeTravelMicroAppResponse;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import java.util.Calendar;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeService extends pp2 implements rc1, LocationListener {
    @DexIgnore
    public static /* final */ a A; // = new a((fd4) null);
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ int x; // = 120000;
    @DexIgnore
    public static /* final */ int y; // = y;
    @DexIgnore
    public static /* final */ float z; // = 50.0f;
    @DexIgnore
    public Location h;
    @DexIgnore
    public Location i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public String k;
    @DexIgnore
    public long l;
    @DexIgnore
    public CommuteTimeSetting m;
    @DexIgnore
    public LocationManager n;
    @DexIgnore
    public String o;
    @DexIgnore
    public Handler p;
    @DexIgnore
    public oc1 q;
    @DexIgnore
    public yc1 r;
    @DexIgnore
    public LocationRequest s;
    @DexIgnore
    public tc1 t;
    @DexIgnore
    public qc1 u;
    @DexIgnore
    public lr2 v;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return CommuteTimeService.w;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final void a(Context context, Bundle bundle, rp2 rp2) {
            kd4.b(context, "context");
            kd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
            kd4.b(rp2, "listener");
            Intent intent = new Intent(context, CommuteTimeService.class);
            intent.putExtras(bundle);
            context.startService(intent);
            pp2.a(rp2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qc1 {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        public b(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        public void onLocationResult(LocationResult locationResult) {
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "onLocationResult");
            super.onLocationResult(locationResult);
            CommuteTimeService commuteTimeService = this.a;
            if (locationResult != null) {
                commuteTimeService.b(locationResult.H());
                if (this.a.i() != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = CommuteTimeService.A.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("onLocationResult lastLocation=");
                    Location i = this.a.i();
                    if (i != null) {
                        sb.append(i);
                        local.d(a2, sb.toString());
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "onLocationResult lastLocation is null");
                return;
            }
            kd4.a();
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<TResult> implements rn1<Location> {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onComplete(wn1<Location> wn1) {
                kd4.b(wn1, "task");
                if (!wn1.e() || wn1.b() == null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = CommuteTimeService.A.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("getLastLocation:exception");
                    Exception a3 = wn1.a();
                    if (a3 != null) {
                        sb.append(a3);
                        local.d(a2, sb.toString());
                        this.a.e.a();
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                CommuteTimeService commuteTimeService = this.a.e;
                Location i = commuteTimeService.i();
                if (i != null) {
                    commuteTimeService.b(commuteTimeService.a(i, wn1.b()));
                    long currentTimeMillis = System.currentTimeMillis();
                    Location i2 = this.a.e.i();
                    if (i2 == null) {
                        kd4.a();
                        throw null;
                    } else if (currentTimeMillis - i2.getTime() > ((long) CommuteTimeService.y)) {
                        FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "Runnable after 5s over 5 mins not trust");
                        this.a.e.a();
                    } else {
                        CommuteTimeService commuteTimeService2 = this.a.e;
                        Location i3 = commuteTimeService2.i();
                        if (i3 != null) {
                            commuteTimeService2.a(i3);
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public c(CommuteTimeService commuteTimeService) {
            this.e = commuteTimeService;
        }

        @DexIgnore
        public final void run() {
            throw null;
            // FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "Runnable after 5s");
            // if (this.e.h == null && this.e.n != null) {
            //     LocationManager f = this.e.n;
            //     if (f != null) {
            //         f.removeUpdates(this.e);
            //         if (k6.a((Context) PortfolioApp.W.c(), "android.permission.ACCESS_FINE_LOCATION") == 0 && k6.a((Context) PortfolioApp.W.c(), "android.permission.ACCESS_COARSE_LOCATION") == 0 && LocationUtils.isLocationEnable(PortfolioApp.W.c())) {
            //             CommuteTimeService commuteTimeService = this.e;
            //             LocationManager f2 = commuteTimeService.n;
            //             if (f2 != null) {
            //                 commuteTimeService.b(f2.getLastKnownLocation(this.e.o));
            //                 if (this.e.i() != null) {
            //                     oc1 c = this.e.q;
            //                     if (c != null) {
            //                         wn1<Location> i = c.i();
            //                         if (i != null) {
            //                             i.a((rn1<Location>) new a(this));
            //                             return;
            //                         }
            //                         return;
            //                     }
            //                     return;
            //                 }
            //                 this.e.a();
            //                 return;
            //             }
            //             kd4.a();
            //             throw null;
            //         }
            //         FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "Runnable after 5s permission not granted");
            //         this.e.a();
            //         return;
            //     }
            //     kd4.a();
            //     throw null;
            // }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<TResult> implements tn1<uc1> {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        public d(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onSuccess(uc1 uc1) {
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "All location settings are satisfied");
            if (k6.a((Context) this.a, "android.permission.ACCESS_FINE_LOCATION") == 0 || k6.a((Context) this.a, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
                oc1 c = this.a.q;
                if (c != null) {
                    c.a(this.a.s, this.a.u, Looper.myLooper());
                    return;
                }
                return;
            }
            this.a.a();
            this.a.a(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements sn1 {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        public e(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        public final void onFailure(Exception exc) {
            kd4.b(exc, "exception");
            int statusCode = ((ApiException) exc).getStatusCode();
            if (statusCode == 6) {
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "Location settings are not satisfied. Attempting to upgrade location settings ");
                this.a.a();
            } else if (statusCode == 8502) {
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "Location settings are inadequate, and cannot be fixed here. Fix in Settings.");
                this.a.a(false);
                this.a.a();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<TResult> implements rn1<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        public f(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        public final void onComplete(wn1<Void> wn1) {
            kd4.b(wn1, "it");
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "stopLocationUpdates success");
            this.a.a(false);
        }
    }

    /*
    static {
        String simpleName = CommuteTimeService.class.getSimpleName();
        kd4.a((Object) simpleName, "CommuteTimeService::class.java.simpleName");
        w = simpleName;
    }
    */

    @DexIgnore
    public final void j() {
        throw null;
        // FLogger.INSTANCE.getLocal().d(w, "initLocationManager");
        // if (k6.a((Context) PortfolioApp.W.c(), "android.permission.ACCESS_FINE_LOCATION") == 0 || k6.a((Context) PortfolioApp.W.c(), "android.permission.ACCESS_COARSE_LOCATION") == 0) {
        //     Criteria criteria = new Criteria();
        //     criteria.setPowerRequirement(1);
        //     Object systemService = getSystemService(PlaceFields.LOCATION);
        //     if (systemService != null) {
        //         this.n = (LocationManager) systemService;
        //         boolean z2 = false;
        //         LocationManager locationManager = this.n;
        //         if (locationManager != null) {
        //             if (locationManager != null) {
        //                 z2 = locationManager.isProviderEnabled("network");
        //             } else {
        //                 kd4.a();
        //                 throw null;
        //             }
        //         }
        //         if (z2) {
        //             criteria.setAccuracy(2);
        //         } else {
        //             criteria.setAccuracy(1);
        //         }
        //         LocationManager locationManager2 = this.n;
        //         if (locationManager2 != null) {
        //             this.o = locationManager2.getBestProvider(criteria, true);
        //             if (this.n != null && this.o != null) {
        //                 FLogger.INSTANCE.getLocal().d(w, "mLocationManager");
        //                 LocationManager locationManager3 = this.n;
        //                 if (locationManager3 != null) {
        //                     locationManager3.requestLocationUpdates(this.o, 0, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this);
        //                 } else {
        //                     kd4.a();
        //                     throw null;
        //                 }
        //             }
        //         } else {
        //             kd4.a();
        //             throw null;
        //         }
        //     } else {
        //         throw new TypeCastException("null cannot be cast to non-null type android.location.LocationManager");
        //     }
        // } else {
        //     a();
        // }
    }

    @DexIgnore
    public final void k() {
        FLogger.INSTANCE.getLocal().d(w, "playHands");
        CommuteTimeSetting commuteTimeSetting = this.m;
        if (commuteTimeSetting == null) {
            kd4.a();
            throw null;
        } else if (kd4.a((Object) commuteTimeSetting.getFormat(), (Object) "travel")) {
            m();
        } else {
            l();
        }
    }

    @DexIgnore
    public final void l() {
        FLogger.INSTANCE.getLocal().d(w, "playHandsETA");
        long currentTimeMillis = System.currentTimeMillis() + (this.l * ((long) 1000));
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "calendar");
        instance.setTimeInMillis(currentTimeMillis);
        @SuppressLint("WrongConstant") int i2 = instance.get(11) % 12;
        @SuppressLint("WrongConstant") int i3 = instance.get(12);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "playHandsETA - duration=" + this.l + ", hour=" + i2 + ", minute=" + i3);
        try {
            PortfolioApp c2 = PortfolioApp.W.c();
            String str2 = this.k;
            if (str2 != null) {
                c2.a(str2, (DeviceAppResponse) new CommuteTimeETAMicroAppResponse(i2, i3));
                a();
                return;
            }
            kd4.a();
            throw null;
        } catch (IllegalArgumentException e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = w;
            local2.d(str3, "playHandsETA exception exception=" + e2.getMessage());
        }
    }

    @DexIgnore
    public final void m() {
        FLogger.INSTANCE.getLocal().d(w, "playHandsMinute");
        int round = Math.round(((float) this.l) / 60.0f);
        try {
            PortfolioApp c2 = PortfolioApp.W.c();
            String str = this.k;
            if (str != null) {
                c2.a(str, (DeviceAppResponse) new CommuteTimeTravelMicroAppResponse(round));
                a();
                return;
            }
            kd4.a();
            throw null;
        } catch (IllegalArgumentException e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = w;
            local.d(str2, "playHandsMinute exception exception=" + e2.getMessage());
        }
    }

    @DexIgnore
    public final void n() {
        FLogger.INSTANCE.getLocal().d(w, "startLocationUpdates");
        if (k6.a((Context) PortfolioApp.W.c(), "android.permission.ACCESS_FINE_LOCATION") == 0 && k6.a((Context) PortfolioApp.W.c(), "android.permission.ACCESS_COARSE_LOCATION") == 0 && LocationUtils.isLocationEnable(PortfolioApp.W.c())) {
            this.j = true;
            yc1 yc1 = this.r;
            if (yc1 != null) {
                wn1<uc1> a2 = yc1.a(this.t);
                a2.a((tn1<? super uc1>) new d(this));
                a2.a((sn1) new e(this));
                return;
            }
            kd4.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(w, "startLocationUpdates permission not granted");
        a();
        this.j = false;
    }

    @DexIgnore
    public final void o() {
        if (!this.j) {
            FLogger.INSTANCE.getLocal().d(w, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }
        oc1 oc1 = this.q;
        if (oc1 != null) {
            wn1<Void> a2 = oc1.a(this.u);
            if (a2 != null) {
                a2.a((rn1<Void>) new f(this));
            }
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        kd4.b(intent, "intent");
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        FLogger.INSTANCE.getLocal().d(w, "onCreate");
        PortfolioApp.W.c().g().a(this);
        this.p = new Handler();
        this.k = PortfolioApp.W.c().e();
        this.q = sc1.a(this);
        this.r = sc1.b(this);
        f();
        g();
        e();
        j();
        n();
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(w, "onDestroy");
        super.onDestroy();
        o();
        Handler handler = this.p;
        if (handler != null) {
            handler.removeCallbacksAndMessages((Object) null);
            LocationManager locationManager = this.n;
            if (locationManager == null) {
                return;
            }
            if (locationManager != null) {
                locationManager.removeUpdates(this);
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void onLocationChanged(Location location) {
        FLogger.INSTANCE.getLocal().d(w, "onLocationChanged");
        if (!(this.m == null || location == null || this.h != null)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "onLocationChanged location=lat: " + location.getLatitude() + " long: " + location.getLongitude());
            this.h = location;
            Location location2 = this.h;
            if (location2 != null) {
                a(location2);
            } else {
                kd4.a();
                throw null;
            }
        }
        LocationManager locationManager = this.n;
        if (locationManager == null) {
            return;
        }
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void onProviderDisabled(String str) {
        kd4.b(str, "provider");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = w;
        local.d(str2, "onProviderDisabled provider=" + str);
    }

    @DexIgnore
    public void onProviderEnabled(String str) {
        kd4.b(str, "provider");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = w;
        local.d(str2, "onProviderEnabled provider=" + str);
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        throw null;
        // kd4.b(intent, "intent");
        // FLogger.INSTANCE.getLocal().d(w, "onStartCommand");
        // this.e = Action.MicroAppAction.SHOW_COMMUTE;
        // super.d();
        // if (this.m != null) {
        //     return 2;
        // }
        // Bundle extras = intent.getExtras();
        // if (extras == null) {
        //     return 2;
        // }
        // String string = extras.getString(Constants.EXTRA_INFO);
        // ILocalFLogger local = FLogger.INSTANCE.getLocal();
        // String str = w;
        // StringBuilder sb = new StringBuilder();
        // sb.append("onStartCommand json=");
        // if (string != null) {
        //     sb.append(string);
        //     local.d(str, sb.toString());
        //     this.m = (CommuteTimeSetting) new Gson().a(string, CommuteTimeSetting.class);
        //     Handler handler = this.p;
        //     if (handler != null) {
        //         handler.postDelayed(new c(this), 5000);
        //         return 2;
        //     }
        //     kd4.a();
        //     throw null;
        // }
        // kd4.a();
        // throw null;
    }

    @DexIgnore
    public void onStatusChanged(String str, int i2, Bundle bundle) {
        kd4.b(str, "provider");
        kd4.b(bundle, AppLinkData.ARGUMENTS_EXTRAS_KEY);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = w;
        local.d(str2, "onStatusChanged status=" + i2);
    }

    @DexIgnore
    public final void b(Location location) {
        this.i = location;
    }

    @DexIgnore
    public final void e() {
        tc1.a aVar = new tc1.a();
        LocationRequest locationRequest = this.s;
        if (locationRequest != null) {
            if (locationRequest != null) {
                aVar.a(locationRequest);
            } else {
                kd4.a();
                throw null;
            }
        }
        this.t = aVar.a();
    }

    @DexIgnore
    public final void f() {
        this.u = new b(this);
    }

    @DexIgnore
    public final void g() {
        FLogger.INSTANCE.getLocal().d(w, "createLocationRequest");
        this.s = new LocationRequest();
        LocationRequest locationRequest = this.s;
        if (locationRequest != null) {
            locationRequest.j(1000);
            LocationRequest locationRequest2 = this.s;
            if (locationRequest2 != null) {
                locationRequest2.i(1000);
                LocationRequest locationRequest3 = this.s;
                if (locationRequest3 != null) {
                    locationRequest3.g(100);
                    LocationRequest locationRequest4 = this.s;
                    if (locationRequest4 != null) {
                        locationRequest4.a(z);
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final lr2 h() {
        lr2 lr2 = this.v;
        if (lr2 != null) {
            return lr2;
        }
        kd4.d("mDurationUtils");
        throw null;
    }

    @DexIgnore
    public final Location i() {
        return this.i;
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.j = z2;
    }

    @DexIgnore
    public void b() {
        FLogger.INSTANCE.getLocal().d(w, "forceStop");
        a();
    }

    @DexIgnore
    public final void a(Location location) {
        throw null;
        // kd4.b(location, PlaceFields.LOCATION);
        // ILocalFLogger local = FLogger.INSTANCE.getLocal();
        // String str = w;
        // local.d(str, "getDurationTime location long=" + location.getLongitude() + " lat=" + location.getLatitude());
        // fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new CommuteTimeService$getDurationTime$Anon1(this, location, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a() {
        FLogger.INSTANCE.getLocal().d(w, VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        super.a();
        stopSelf();
    }

    @DexIgnore
    public final Location a(Location location, Location location2) {
        kd4.b(location, PlaceFields.LOCATION);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "isBetterLocation location long=" + location.getLongitude() + " lat=" + location.getLatitude() + " time=" + location.getTime());
        if (location2 == null) {
            FLogger.INSTANCE.getLocal().d(w, "isBetterLocation currentBestLocation null");
            return location;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = w;
        local2.d(str2, "isBetterLocation currentBestLocation long=" + location2.getLongitude() + " lat=" + location2.getLatitude() + " time=" + location2.getTime());
        long time = location.getTime() - location2.getTime();
        boolean z2 = true;
        boolean z3 = time > ((long) x);
        boolean z4 = time < ((long) (-x));
        boolean z5 = time > 0;
        if (z3) {
            FLogger.INSTANCE.getLocal().d(w, "isBetterLocation isSignificantlyNewer");
            return location;
        } else if (z4) {
            FLogger.INSTANCE.getLocal().d(w, "isBetterLocation isSignificantlyOlder");
            return location2;
        } else {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = w;
            local3.d(str3, "isBetterLocation accuracy location=" + location.getAccuracy() + " currentBestLocation=" + location2.getAccuracy());
            int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
            boolean z6 = accuracy > 0;
            boolean z7 = accuracy < 0;
            if (accuracy <= 200) {
                z2 = false;
            }
            boolean a2 = a(location.getProvider(), location2.getProvider());
            if (z7) {
                FLogger.INSTANCE.getLocal().d(w, "isBetterLocation isMoreAccurate");
                return location;
            } else if (z5 && !z6) {
                FLogger.INSTANCE.getLocal().d(w, "isBetterLocation isNewer && isLessAccurate=false");
                return location;
            } else if (!z5 || z2 || !a2) {
                return location2;
            } else {
                FLogger.INSTANCE.getLocal().d(w, "isBetterLocation isNewer && isSignificantlyLessAccurate=false && isFromSameProvider");
                return location;
            }
        }
    }

    @DexIgnore
    public final boolean a(String str, String str2) {
        FLogger.INSTANCE.getLocal().d(w, "isSameProvider");
        if (str == null) {
            return str2 == null;
        }
        return kd4.a((Object) str, (Object) str2);
    }
}
