package com.portfolio.platform.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFDeviceService$bleActionServiceReceiver$Anon1 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService a;

    @DexIgnore
    public MFDeviceService$bleActionServiceReceiver$Anon1(MFDeviceService mFDeviceService) {
        this.a = mFDeviceService;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        kd4.b(context, "context");
        kd4.b(intent, "intent");
        fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1(this, intent, (yb4) null), 3, (Object) null);
    }
}
