package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.MFDeviceService$updatePairedDeviceToButtonService$1", mo27670f = "MFDeviceService.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class MFDeviceService$updatePairedDeviceToButtonService$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21437p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.MFDeviceService this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$updatePairedDeviceToButtonService$1(com.portfolio.platform.service.MFDeviceService mFDeviceService, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = mFDeviceService;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.MFDeviceService$updatePairedDeviceToButtonService$1 mFDeviceService$updatePairedDeviceToButtonService$1 = new com.portfolio.platform.service.MFDeviceService$updatePairedDeviceToButtonService$1(this.this$0, yb4);
        mFDeviceService$updatePairedDeviceToButtonService$1.f21437p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return mFDeviceService$updatePairedDeviceToButtonService$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.MFDeviceService$updatePairedDeviceToButtonService$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.util.List<com.portfolio.platform.data.model.Device> allDevice = this.this$0.mo39833d().getAllDevice();
            if (!allDevice.isEmpty()) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String b = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                local.mo33255d(b, "Get all device success devicesList=" + allDevice);
                if (this.this$0.mo39852r().getCurrentUser() != null) {
                    for (com.portfolio.platform.data.model.Device next : allDevice) {
                        this.this$0.mo39831c().mo34531d(next.component1(), next.component2());
                    }
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
