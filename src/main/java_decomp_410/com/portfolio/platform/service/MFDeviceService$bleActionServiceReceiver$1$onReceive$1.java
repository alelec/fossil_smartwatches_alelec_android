package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1", mo27670f = "MFDeviceService.kt", mo27671l = {275, 286, 379, 418, 463, 481}, mo27672m = "invokeSuspend")
public final class MFDeviceService$bleActionServiceReceiver$1$onReceive$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.content.Intent $intent;
    @DexIgnore
    public int I$0;
    @DexIgnore
    public int I$1;
    @DexIgnore
    public int I$2;
    @DexIgnore
    public int I$3;
    @DexIgnore
    public int I$4;
    @DexIgnore
    public int I$5;
    @DexIgnore
    public int I$6;
    @DexIgnore
    public long J$0;
    @DexIgnore
    public long J$1;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$10;
    @DexIgnore
    public java.lang.Object L$11;
    @DexIgnore
    public java.lang.Object L$12;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public java.lang.Object L$7;
    @DexIgnore
    public java.lang.Object L$8;
    @DexIgnore
    public java.lang.Object L$9;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21424p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1 this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$7")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$7", mo27670f = "MFDeviceService.kt", mo27671l = {519, 520}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$7 */
    public static final class C60167 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.lang.String $secretKey;
        @DexIgnore
        public /* final */ /* synthetic */ java.lang.String $serial;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public java.lang.Object L$1;
        @DexIgnore
        public java.lang.Object L$2;
        @DexIgnore
        public java.lang.Object L$3;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21425p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C60167(com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1 mFDeviceService$bleActionServiceReceiver$1$onReceive$1, java.lang.String str, java.lang.String str2, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = mFDeviceService$bleActionServiceReceiver$1$onReceive$1;
            this.$serial = str;
            this.$secretKey = str2;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1.C60167 r0 = new com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1.C60167(this.this$0, this.$serial, this.$secretKey, yb4);
            r0.f21425p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1.C60167) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.portfolio.platform.data.model.MFUser mFUser;
            com.fossil.blesdk.obfuscated.zg4 zg4;
            com.portfolio.platform.data.model.MFUser mFUser2;
            java.lang.String str;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                zg4 = this.f21425p$;
                mFUser = this.this$0.this$0.f21423a.mo39852r().getCurrentUser();
                if (mFUser != null) {
                    str = mFUser.getUserId() + ':' + this.$serial;
                    com.fossil.blesdk.obfuscated.mr3 e = this.this$0.this$0.f21423a.mo39834e();
                    java.lang.String str2 = this.$secretKey;
                    if (str2 != null) {
                        com.fossil.blesdk.obfuscated.mr3.C4692b bVar = new com.fossil.blesdk.obfuscated.mr3.C4692b(str, str2);
                        this.L$0 = zg4;
                        this.L$1 = mFUser;
                        this.L$2 = mFUser;
                        this.L$3 = str;
                        this.label = 1;
                        if (com.fossil.blesdk.obfuscated.w52.m29539a(e, bVar, this) == a) {
                            return a;
                        }
                        mFUser2 = mFUser;
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else if (i == 1) {
                str = (java.lang.String) this.L$3;
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
            } else if (i == 2) {
                java.lang.String str3 = (java.lang.String) this.L$3;
                com.portfolio.platform.data.model.MFUser mFUser3 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                com.portfolio.platform.data.model.MFUser mFUser4 = (com.portfolio.platform.data.model.MFUser) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            com.portfolio.platform.data.source.DeviceRepository d = this.this$0.this$0.f21423a.mo39833d();
            java.lang.String str4 = this.$serial;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str4, "serial");
            java.lang.String str5 = this.$secretKey;
            this.L$0 = zg4;
            this.L$1 = mFUser;
            this.L$2 = mFUser2;
            this.L$3 = str;
            this.label = 2;
            if (d.updateDeviceSecretKey(str4, str5, this) == a) {
                return a;
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$a")
    /* renamed from: com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$a */
    public static final class C6017a implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.usecase.VerifySecretKeyUseCase.C6923d, com.portfolio.platform.usecase.VerifySecretKeyUseCase.C6922c> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1 f21426a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ java.lang.String f21427b;

        @DexIgnore
        public C6017a(com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1 mFDeviceService$bleActionServiceReceiver$1$onReceive$1, java.lang.String str) {
            this.f21426a = mFDeviceService$bleActionServiceReceiver$1$onReceive$1;
            this.f21427b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(com.portfolio.platform.usecase.VerifySecretKeyUseCase.C6923d dVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
            com.portfolio.platform.PortfolioApp c = this.f21426a.this$0.f21423a.mo39831c();
            java.lang.String str = this.f21427b;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str, "serial");
            c.mo34522b(str, dVar.mo41902a());
        }

        @DexIgnore
        /* renamed from: a */
        public void mo29641a(com.portfolio.platform.usecase.VerifySecretKeyUseCase.C6922c cVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(cVar, "errorValue");
            com.portfolio.platform.PortfolioApp c = this.f21426a.this$0.f21423a.mo39831c();
            java.lang.String str = this.f21427b;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str, "serial");
            c.mo34522b(str, (java.lang.String) null);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$bleActionServiceReceiver$1$onReceive$1(com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1 mFDeviceService$bleActionServiceReceiver$1, android.content.Intent intent, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = mFDeviceService$bleActionServiceReceiver$1;
        this.$intent = intent;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1 mFDeviceService$bleActionServiceReceiver$1$onReceive$1 = new com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1(this.this$0, this.$intent, yb4);
        mFDeviceService$bleActionServiceReceiver$1$onReceive$1.f21424p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return mFDeviceService$bleActionServiceReceiver$1$onReceive$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v55, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v21, resolved type: java.lang.String} */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x05e6, code lost:
        r0 = (com.fossil.blesdk.obfuscated.qo2) r0;
        r2 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x05fa, code lost:
        r6 = r6;
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x0610, code lost:
        r0.putExtra(r17, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x0613, code lost:
        if (r6 == null) goto L_0x061e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0615, code lost:
        r15.this$0.f21423a.mo39827a("ota_success", (java.util.Map<java.lang.String, java.lang.String>) r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x061e, code lost:
        if (r2 == null) goto L_0x066c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x0620, code lost:
        r2.mo31555a(r23);
        r1 = com.fossil.blesdk.obfuscated.qa4.f17909a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x066c, code lost:
        com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39521e(r18);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b(), "Inside .OTA receiver, broadcast local in app");
        r1 = com.portfolio.platform.service.BleCommandResultManager.f21345d;
        r2 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.OTA;
        r1.mo39769a(r2, new com.portfolio.platform.service.BleCommandResultManager.C5998a(r2, r9, r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0144, code lost:
        r42 = "Inside ";
        r44 = "sync_session";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x070e, code lost:
        r0 = com.fossil.blesdk.obfuscated.qa4.f17909a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x0710, code lost:
        r8 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:0x0b8a, code lost:
        r7 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:271:0x0d04, code lost:
        if (r0 == 0) goto L_0x0d3f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:272:0x0d06, code lost:
        r8 = r53;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:274:?, code lost:
        r8.this$0.f21423a.mo39831c().mo34488a(com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC, r7, "Process data OK.");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().mo33266i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC, r7, com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b(), "Process data OK.");
        r8.this$0.f21423a.mo39831c().mo34528c(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:275:0x0d3b, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:276:0x0d3c, code lost:
        r12 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:277:0x0d3f, code lost:
        r8 = r53;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:279:?, code lost:
        r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r1 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
        r0.mo33255d(r1, r42 + com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b() + ".processBleResult - data is empty.");
        r8.this$0.f21423a.mo39831c().mo34488a(com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC, r7, "data is empty.");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().mo33266i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC, r7, com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b(), "data is empty.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:280:0x0d99, code lost:
        r5 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:282:?, code lost:
        r8.this$0.f21423a.mo39831c().mo34528c(true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:283:0x0d9f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:284:0x0da1, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:285:0x0da2, code lost:
        r5 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:286:0x0da3, code lost:
        r12 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x02bd, code lost:
        r0 = (com.fossil.blesdk.obfuscated.qo2) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x02c1, code lost:
        if ((r0 instanceof com.fossil.blesdk.obfuscated.ro2) == false) goto L_0x02ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x02c3, code lost:
        r3 = ((com.fossil.blesdk.obfuscated.ro2) r0).mo30673a();
        r11 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x02d0, code lost:
        if ((r0 instanceof com.fossil.blesdk.obfuscated.po2) == false) goto L_0x02ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x02d2, code lost:
        r11 = ((com.fossil.blesdk.obfuscated.po2) r0).mo30176a();
        r3 = r23;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x02da, code lost:
        r0 = r15.this$0.f21423a.mo39831c();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r9, "serial");
        r0.mo34505a(r9, r3, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x02ef, code lost:
        throw new kotlin.NoWhenBranchMatchedException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0343, code lost:
        r0 = (com.fossil.blesdk.obfuscated.qo2) r0;
        r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r2 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
        r1.mo33255d(r2, "swap pairing key " + r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0367, code lost:
        if ((r0 instanceof com.fossil.blesdk.obfuscated.ro2) == false) goto L_0x0389;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0369, code lost:
        r1 = r15.this$0.f21423a.mo39831c();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r9, "serial");
        r0 = ((com.fossil.blesdk.obfuscated.ro2) r0).mo30673a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x037a, code lost:
        if (r0 == null) goto L_0x0384;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x037c, code lost:
        r1.mo34523b(r9, (java.lang.String) r0, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0384, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0388, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x038b, code lost:
        if ((r0 instanceof com.fossil.blesdk.obfuscated.po2) == false) goto L_0x0710;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x038d, code lost:
        r1 = r15.this$0.f21423a.mo39831c();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r9, "serial");
        r1.mo34523b(r9, r23, ((com.fossil.blesdk.obfuscated.po2) r0).mo30176a());
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.String str;
        com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1 mFDeviceService$bleActionServiceReceiver$1$onReceive$1;
        java.lang.String str2;
        boolean z;
        java.lang.String str3;
        int i;
        java.lang.String str4;
        java.lang.String str5;
        java.lang.String str6;
        android.content.Intent intent;
        java.lang.String str7;
        java.lang.String str8;
        com.fossil.blesdk.obfuscated.ul2 ul2;
        java.util.HashMap hashMap;
        android.content.Intent intent2;
        com.fossil.blesdk.obfuscated.ul2 ul22;
        java.lang.Object obj2;
        java.lang.String str9;
        java.lang.Object obj3;
        java.lang.Object obj4;
        java.lang.String str10;
        java.lang.String str11;
        java.lang.String str12;
        int i2;
        java.lang.String str13;
        int i3;
        java.util.ArrayList<java.lang.Integer> arrayList;
        java.util.List<com.misfit.frameworks.buttonservice.p003db.DataFile> list;
        com.fossil.fitness.FitnessData fitnessData;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local;
        java.lang.String b;
        int i4;
        com.fossil.blesdk.obfuscated.ul2 ul23;
        java.util.HashMap hashMap2;
        java.lang.String str14;
        java.util.HashMap hashMap3;
        java.lang.String str15;
        java.lang.String str16;
        com.portfolio.platform.data.source.DeviceRepository deviceRepository;
        java.lang.String str17;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        switch (this.label) {
            case 0:
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21424p$;
                int intExtra = this.$intent.getIntExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getSERVICE_BLE_PHASE(), com.misfit.frameworks.buttonservice.communite.CommunicateMode.IDLE.ordinal());
                com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode = com.misfit.frameworks.buttonservice.communite.CommunicateMode.values()[intExtra];
                str6 = this.$intent.getStringExtra(com.misfit.frameworks.common.constants.Constants.SERIAL_NUMBER);
                int intExtra2 = this.$intent.getIntExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
                int intExtra3 = this.$intent.getIntExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), com.misfit.frameworks.buttonservice.log.FailureCode.UNKNOWN_ERROR);
                java.util.ArrayList<java.lang.Integer> integerArrayListExtra = this.$intent.getIntegerArrayListExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new java.util.ArrayList<>();
                }
                java.util.ArrayList<java.lang.Integer> arrayList2 = integerArrayListExtra;
                android.os.Bundle extras = this.$intent.getExtras();
                if (extras == null) {
                    extras = new android.os.Bundle();
                }
                java.lang.String str18 = "Inside ";
                android.os.Bundle bundle = extras;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) bundle, "intent.extras ?: Bundle()");
                java.lang.String str19 = "sync_session";
                com.portfolio.platform.data.model.SKUModel skuModelBySerialPrefix = this.this$0.f21423a.mo39833d().getSkuModelBySerialPrefix(com.portfolio.platform.helper.DeviceHelper.f21188o.mo39559b(str6));
                str5 = "ota_session";
                str8 = "OTA_RESULT";
                int i5 = bundle.getInt(com.misfit.frameworks.buttonservice.ButtonService.Companion.getSYNC_MODE(), -1);
                int i6 = bundle.getInt(com.misfit.frameworks.buttonservice.ButtonService.Companion.getORIGINAL_SYNC_MODE(), 10);
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                str9 = "";
                java.lang.String b2 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                java.lang.Object obj5 = a;
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                int i7 = i6;
                sb.append("Inside .bleActionServiceReceiver phase ");
                sb.append(communicateMode);
                sb.append(" result ");
                sb.append(intExtra2);
                sb.append(" extra info ");
                sb.append(bundle);
                local2.mo33255d(b2, sb.toString());
                int i8 = bundle.getInt(com.misfit.frameworks.buttonservice.ButtonService.Companion.getLOG_ID(), -1);
                switch (com.fossil.blesdk.obfuscated.fp2.f14870a[communicateMode.ordinal()]) {
                    case 1:
                        this.this$0.f21423a.mo39842m().mo27009b(java.lang.System.currentTimeMillis());
                        break;
                    case 2:
                        java.lang.String str20 = "error_code";
                        int i9 = i8;
                        int i10 = i7;
                        int i11 = i5;
                        java.lang.String str21 = "device";
                        com.fossil.blesdk.obfuscated.ul2 c = com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39516c(str19);
                        int i12 = intExtra;
                        com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode2 = communicateMode;
                        this.this$0.f21423a.mo39842m().mo27010b(java.lang.System.currentTimeMillis(), str6);
                        if (intExtra2 == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.ASK_FOR_STOP_WORKOUT.ordinal()) {
                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b(), "sync is pending because workout is working");
                            this.this$0.f21423a.mo39823a(str6, 5, intExtra3, arrayList2, com.fossil.blesdk.obfuscated.dc4.m20843a(i10));
                        } else if (intExtra2 == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.PROCESSING.ordinal()) {
                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b(), "sync status is SYNCING");
                            com.portfolio.platform.service.MFDeviceService.m32279a(this.this$0.f21423a, str6, 0, 0, (java.util.ArrayList) null, (java.lang.Integer) null, 24, (java.lang.Object) null);
                        } else if (intExtra2 == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.UNALLOWED_ACTION.ordinal()) {
                            this.this$0.f21423a.mo39842m().mo27008b(0);
                            this.this$0.f21423a.mo39823a(str6, 4, intExtra3, arrayList2, com.fossil.blesdk.obfuscated.dc4.m20843a(i10));
                        } else if (intExtra2 == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED.ordinal()) {
                            this.this$0.f21423a.mo39842m().mo27008b(0);
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String b3 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                            local3.mo33255d(b3, "sync status is FAILED - current sync success number = " + this.this$0.f21423a.mo39842m().mo27033e());
                            this.this$0.f21423a.mo39823a(str6, 2, intExtra3, arrayList2, com.fossil.blesdk.obfuscated.dc4.m20843a(i10));
                            this.this$0.f21423a.mo39821a(skuModelBySerialPrefix, i11, 2);
                            com.fossil.blesdk.obfuscated.sl2 a2 = com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39510a("sync_session_optional_error");
                            a2.mo30871a(str20, java.lang.String.valueOf(intExtra3));
                            if (c != null) {
                                c.mo31554a(a2);
                                com.fossil.blesdk.obfuscated.qa4 qa4 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                            }
                            if (c != null) {
                                c.mo31555a(java.lang.String.valueOf(intExtra3));
                                com.fossil.blesdk.obfuscated.qa4 qa42 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                            }
                            com.portfolio.platform.news.notifications.FossilNotificationBar.f21301c.mo39716a(this.this$0.f21423a);
                        } else if (intExtra2 == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.RECEIVED_DATA.ordinal()) {
                            if (!bundle.isEmpty()) {
                                if (!(str6 == null || com.fossil.blesdk.obfuscated.qf4.m26950a(str6))) {
                                    java.lang.String str22 = str6;
                                    long j = bundle.getLong(com.misfit.frameworks.common.constants.Constants.SYNC_RESULT, -1);
                                    long j2 = bundle.getLong(com.misfit.frameworks.buttonservice.ButtonService.Companion.getREALTIME_STEPS(), -1);
                                    com.misfit.frameworks.buttonservice.model.UserProfile j3 = this.this$0.f21423a.mo39831c().mo34543j();
                                    com.portfolio.platform.data.model.MFUser currentUser = this.this$0.f21423a.mo39852r().getCurrentUser();
                                    com.portfolio.platform.PortfolioApp c2 = this.this$0.f21423a.mo39831c();
                                    com.fossil.blesdk.obfuscated.ul2 ul24 = c;
                                    com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode3 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC;
                                    int i13 = i9;
                                    java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
                                    int i14 = i10;
                                    sb2.append("Sync time=");
                                    android.os.Bundle bundle2 = bundle;
                                    com.portfolio.platform.data.model.SKUModel sKUModel = skuModelBySerialPrefix;
                                    long j4 = j;
                                    sb2.append(j4);
                                    int i15 = i11;
                                    java.lang.String str23 = str22;
                                    c2.mo34488a(communicateMode3, str23, sb2.toString());
                                    if (j4 <= ((long) -1) || j3 == null || currentUser == null) {
                                        java.lang.String str24 = str23;
                                        mFDeviceService$bleActionServiceReceiver$1$onReceive$1 = this;
                                        java.lang.String str25 = str18;
                                        str = str19;
                                        if (j4 < 0) {
                                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                            java.lang.String b4 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                            local4.mo33256e(b4, str25 + com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b() + ".processBleResult - syncTime wrong: " + j4);
                                            com.portfolio.platform.PortfolioApp c3 = mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c();
                                            com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode4 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC;
                                            str10 = str24;
                                            c3.mo34488a(communicateMode4, str10, "syncTime value wrong: " + j4);
                                            com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                                            com.misfit.frameworks.buttonservice.log.FLogger.Component component = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                                            com.misfit.frameworks.buttonservice.log.FLogger.Session session = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC;
                                            java.lang.String b5 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                            remote.mo33266i(component, session, str10, b5, "syncTime value wrong: " + j4);
                                        } else {
                                            str10 = str24;
                                        }
                                        if (j3 == null) {
                                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                            java.lang.String b6 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                            local5.mo33256e(b6, str25 + com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b() + ".processBleResult - userProfile empty");
                                            mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c().mo34488a(com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC, str10, "userProfile is empty.");
                                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().mo33266i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC, str10, com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b(), "userProfile is empty.");
                                        }
                                        if (currentUser == null) {
                                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                            java.lang.String b7 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                            local6.mo33256e(b7, str25 + com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b() + ".processBleResult - user data empty");
                                            mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c().mo34488a(com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC, str10, "user data is empty.");
                                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().mo33266i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC, str10, com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b(), "user data is empty.");
                                        }
                                        mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c().mo34528c(true);
                                    } else {
                                        try {
                                            this.this$0.f21423a.mo39831c().mo34488a(com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC, str23, "Save sync result to DB");
                                            java.util.ArrayList arrayList3 = new java.util.ArrayList();
                                            java.util.List<com.misfit.frameworks.buttonservice.p003db.DataFile> allDataFiles = com.misfit.frameworks.buttonservice.p003db.DataFileProvider.getInstance(this.this$0.f21423a).getAllDataFiles(j4, str23);
                                            com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                                            com.misfit.frameworks.buttonservice.log.FLogger.Component component2 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                                            com.misfit.frameworks.buttonservice.log.FLogger.Session session2 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC;
                                            java.lang.String b8 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                            java.lang.StringBuilder sb3 = new java.lang.StringBuilder();
                                            android.os.Bundle bundle3 = bundle2;
                                            sb3.append("[SYNC] Process sync data from BS dataSize ");
                                            sb3.append(allDataFiles.size());
                                            remote2.mo33266i(component2, session2, str23, b8, sb3.toString());
                                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) allDataFiles, "cacheData");
                                            java.util.Iterator<T> it = allDataFiles.iterator();
                                            while (it.hasNext()) {
                                                try {
                                                    com.misfit.frameworks.buttonservice.p003db.DataFile dataFile = (com.misfit.frameworks.buttonservice.p003db.DataFile) it.next();
                                                    java.util.Iterator<T> it2 = it;
                                                    try {
                                                        com.google.gson.Gson a3 = this.this$0.f21423a.f21383F;
                                                        list = allDataFiles;
                                                        try {
                                                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) dataFile, "it");
                                                            fitnessData = (com.fossil.fitness.FitnessData) a3.mo23093a(dataFile.getDataFile(), com.fossil.fitness.FitnessData.class);
                                                            local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                                            b = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                            arrayList = arrayList2;
                                                        } catch (java.lang.Exception e) {
                                                            e = e;
                                                            i3 = intExtra3;
                                                            arrayList = arrayList2;
                                                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                                            java.lang.String b9 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                            local7.mo33255d(b9, "Parse fitnessData. exception=" + e);
                                                            com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                                                            com.misfit.frameworks.buttonservice.log.FLogger.Component component3 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                                                            com.misfit.frameworks.buttonservice.log.FLogger.Session session3 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC;
                                                            java.lang.String b10 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                            remote3.mo33266i(component3, session3, str23, b10, "[Sync Data Received] Exception when parse data " + e);
                                                            it = it2;
                                                            allDataFiles = list;
                                                            arrayList2 = arrayList;
                                                            intExtra3 = i3;
                                                        }
                                                        try {
                                                            java.lang.StringBuilder sb4 = new java.lang.StringBuilder();
                                                            i3 = intExtra3;
                                                            try {
                                                                sb4.append("[SYNC] Add fitness data ");
                                                                sb4.append(fitnessData);
                                                                sb4.append(" to sync data");
                                                                local.mo33255d(b, sb4.toString());
                                                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) fitnessData, "fitnessData");
                                                                arrayList3.add(fitnessData);
                                                            } catch (java.lang.Exception e2) {
                                                                e = e2;
                                                            }
                                                        } catch (java.lang.Exception e3) {
                                                            e = e3;
                                                            i3 = intExtra3;
                                                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local72 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                                            java.lang.String b92 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                            local72.mo33255d(b92, "Parse fitnessData. exception=" + e);
                                                            com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote32 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                                                            com.misfit.frameworks.buttonservice.log.FLogger.Component component32 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                                                            com.misfit.frameworks.buttonservice.log.FLogger.Session session32 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC;
                                                            java.lang.String b102 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                            remote32.mo33266i(component32, session32, str23, b102, "[Sync Data Received] Exception when parse data " + e);
                                                            it = it2;
                                                            allDataFiles = list;
                                                            arrayList2 = arrayList;
                                                            intExtra3 = i3;
                                                        }
                                                    } catch (java.lang.Exception e4) {
                                                        e = e4;
                                                        list = allDataFiles;
                                                        i3 = intExtra3;
                                                        arrayList = arrayList2;
                                                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local722 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                                        java.lang.String b922 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                        local722.mo33255d(b922, "Parse fitnessData. exception=" + e);
                                                        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote322 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                                                        com.misfit.frameworks.buttonservice.log.FLogger.Component component322 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                                                        com.misfit.frameworks.buttonservice.log.FLogger.Session session322 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC;
                                                        java.lang.String b1022 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                        remote322.mo33266i(component322, session322, str23, b1022, "[Sync Data Received] Exception when parse data " + e);
                                                        it = it2;
                                                        allDataFiles = list;
                                                        arrayList2 = arrayList;
                                                        intExtra3 = i3;
                                                    }
                                                    it = it2;
                                                    allDataFiles = list;
                                                    arrayList2 = arrayList;
                                                    intExtra3 = i3;
                                                } catch (java.lang.Exception e5) {
                                                    e = e5;
                                                    str2 = str23;
                                                    mFDeviceService$bleActionServiceReceiver$1$onReceive$1 = this;
                                                    str = str19;
                                                    break;
                                                }
                                            }
                                            java.util.List<com.misfit.frameworks.buttonservice.p003db.DataFile> list2 = allDataFiles;
                                            int i16 = intExtra3;
                                            java.util.ArrayList<java.lang.Integer> arrayList4 = arrayList2;
                                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                            java.lang.String b11 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                            local8.mo33255d(b11, "[SYNC] Process data done,syncData size " + arrayList3.size());
                                            com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                                            com.misfit.frameworks.buttonservice.log.FLogger.Component component4 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                                            com.misfit.frameworks.buttonservice.log.FLogger.Session session4 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC;
                                            java.lang.String b12 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                            remote4.mo33266i(component4, session4, str23, b12, "[Sync Data Received] After get data, size " + arrayList3.size());
                                            i = !arrayList3.isEmpty() ? 1 : 0;
                                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                            java.lang.String b13 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                            local9.mo33255d(b13, ".onReadDataFilesSuccess(), sdk = " + this.this$0.f21423a.f21383F.mo23096a((java.lang.Object) arrayList3));
                                            if (!com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(str23)) {
                                                java.lang.String str26 = str23;
                                                android.os.Bundle bundle4 = bundle3;
                                                str3 = str18;
                                                str = str19;
                                                java.lang.Object obj6 = obj5;
                                                int i17 = i12;
                                                com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode5 = communicateMode2;
                                                int i18 = i15;
                                                java.util.List<com.misfit.frameworks.buttonservice.p003db.DataFile> list3 = list2;
                                                com.fossil.blesdk.obfuscated.ul2 ul25 = ul24;
                                                int i19 = i13;
                                                int i20 = i14;
                                                com.portfolio.platform.data.model.SKUModel sKUModel2 = sKUModel;
                                                java.util.ArrayList<java.lang.Integer> arrayList5 = arrayList4;
                                                int i21 = i16;
                                                if (i != 0) {
                                                    android.os.Bundle bundle5 = bundle4;
                                                    com.portfolio.platform.data.model.SKUModel sKUModel3 = sKUModel2;
                                                    try {
                                                        int i22 = i;
                                                        java.util.ArrayList<java.lang.Integer> arrayList6 = arrayList5;
                                                        java.lang.String str27 = str26;
                                                        try {
                                                            java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> a4 = this.this$0.f21423a.mo39818a((java.util.List<com.fossil.fitness.FitnessData>) arrayList3, str27, new org.joda.time.DateTime());
                                                            this.this$0.f21423a.mo39835f().saveFitnessData(a4);
                                                            com.portfolio.platform.service.usecase.HybridSyncDataProcessing.C6095a a5 = com.portfolio.platform.service.usecase.HybridSyncDataProcessing.f21665b.mo40154a(str27, a4, currentUser, j3, j4, j2, this.this$0.f21423a.mo39831c());
                                                            com.portfolio.platform.service.usecase.HybridSyncDataProcessing hybridSyncDataProcessing = com.portfolio.platform.service.usecase.HybridSyncDataProcessing.f21665b;
                                                            com.portfolio.platform.service.usecase.HybridSyncDataProcessing.C6095a aVar = a5;
                                                            com.portfolio.platform.data.source.SleepSessionsRepository n = this.this$0.f21423a.mo39843n();
                                                            com.portfolio.platform.data.source.SummariesRepository p = this.this$0.f21423a.mo39850p();
                                                            com.portfolio.platform.data.source.SleepSummariesRepository o = this.this$0.f21423a.mo39844o();
                                                            com.portfolio.platform.data.source.FitnessDataRepository f = this.this$0.f21423a.mo39835f();
                                                            com.portfolio.platform.data.source.ActivitiesRepository b14 = this.this$0.f21423a.mo39829b();
                                                            com.portfolio.platform.data.source.HybridPresetRepository l = this.this$0.f21423a.mo39841l();
                                                            com.portfolio.platform.data.source.GoalTrackingRepository h = this.this$0.f21423a.mo39837h();
                                                            com.portfolio.platform.data.source.ThirdPartyRepository q = this.this$0.f21423a.mo39851q();
                                                            com.portfolio.platform.data.source.UserRepository r = this.this$0.f21423a.mo39852r();
                                                            com.portfolio.platform.PortfolioApp c4 = this.this$0.f21423a.mo39831c();
                                                            com.fossil.blesdk.obfuscated.xk2 g = this.this$0.f21423a.mo39836g();
                                                            this.L$0 = zg4;
                                                            this.I$0 = i17;
                                                            this.L$1 = communicateMode5;
                                                            this.L$2 = str27;
                                                            this.I$1 = intExtra2;
                                                            this.I$2 = i21;
                                                            this.L$3 = arrayList6;
                                                            this.L$4 = bundle5;
                                                            this.L$5 = sKUModel3;
                                                            this.I$3 = i18;
                                                            this.I$4 = i20;
                                                            this.I$5 = i19;
                                                            this.L$6 = ul25;
                                                            this.J$0 = j4;
                                                            this.J$1 = j2;
                                                            this.L$7 = j3;
                                                            this.L$8 = currentUser;
                                                            this.L$9 = arrayList3;
                                                            this.L$10 = list3;
                                                            int i23 = i22;
                                                            this.I$6 = i23;
                                                            this.L$11 = a4;
                                                            com.portfolio.platform.service.usecase.HybridSyncDataProcessing.C6095a aVar2 = aVar;
                                                            this.L$12 = aVar2;
                                                            this.label = 2;
                                                            str11 = str27;
                                                            int i24 = i23;
                                                            try {
                                                                java.lang.Object obj7 = obj6;
                                                                if (hybridSyncDataProcessing.mo40155a(aVar2, str27, n, p, o, f, b14, l, h, q, r, c4, g, this) != obj7) {
                                                                    i = i24;
                                                                    str4 = str11;
                                                                    break;
                                                                } else {
                                                                    return obj7;
                                                                }
                                                            } catch (java.lang.Exception e6) {
                                                                e = e6;
                                                                z = true;
                                                                mFDeviceService$bleActionServiceReceiver$1$onReceive$1 = this;
                                                                str2 = str11;
                                                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                                                java.lang.String b15 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                                java.lang.StringBuilder sb5 = new java.lang.StringBuilder();
                                                                sb5.append("Error while processing sync data - e=");
                                                                e.printStackTrace();
                                                                sb5.append(com.fossil.blesdk.obfuscated.qa4.f17909a);
                                                                local10.mo33256e(b15, sb5.toString());
                                                                e.printStackTrace();
                                                                com.portfolio.platform.PortfolioApp c5 = mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c();
                                                                com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode6 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC;
                                                                c5.mo34488a(communicateMode6, str2, "Sync data result OK but has exception error e=" + e);
                                                                com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                                                                com.misfit.frameworks.buttonservice.log.FLogger.Component component5 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                                                                com.misfit.frameworks.buttonservice.log.FLogger.Session session5 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC;
                                                                java.lang.String b16 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                                remote5.mo33266i(component5, session5, str2, b16, "Sync data result OK but has exception error e=" + e);
                                                                mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c().mo34528c(z);
                                                                com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39521e(str);
                                                                com.portfolio.platform.news.notifications.FossilNotificationBar.f21301c.mo39716a(mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a);
                                                                return com.fossil.blesdk.obfuscated.qa4.f17909a;
                                                            }
                                                        } catch (java.lang.Exception e7) {
                                                            e = e7;
                                                            str11 = str27;
                                                            z = true;
                                                            mFDeviceService$bleActionServiceReceiver$1$onReceive$1 = this;
                                                            str2 = str11;
                                                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local102 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                                            java.lang.String b152 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                            java.lang.StringBuilder sb52 = new java.lang.StringBuilder();
                                                            sb52.append("Error while processing sync data - e=");
                                                            e.printStackTrace();
                                                            sb52.append(com.fossil.blesdk.obfuscated.qa4.f17909a);
                                                            local102.mo33256e(b152, sb52.toString());
                                                            e.printStackTrace();
                                                            com.portfolio.platform.PortfolioApp c52 = mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c();
                                                            com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode62 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC;
                                                            c52.mo34488a(communicateMode62, str2, "Sync data result OK but has exception error e=" + e);
                                                            com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote52 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                                                            com.misfit.frameworks.buttonservice.log.FLogger.Component component52 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                                                            com.misfit.frameworks.buttonservice.log.FLogger.Session session52 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC;
                                                            java.lang.String b162 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                            remote52.mo33266i(component52, session52, str2, b162, "Sync data result OK but has exception error e=" + e);
                                                            mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c().mo34528c(z);
                                                            com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39521e(str);
                                                            com.portfolio.platform.news.notifications.FossilNotificationBar.f21301c.mo39716a(mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a);
                                                            return com.fossil.blesdk.obfuscated.qa4.f17909a;
                                                        }
                                                    } catch (java.lang.Exception e8) {
                                                        e = e8;
                                                        str11 = str26;
                                                        z = true;
                                                        mFDeviceService$bleActionServiceReceiver$1$onReceive$1 = this;
                                                        str2 = str11;
                                                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local1022 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                                        java.lang.String b1522 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                        java.lang.StringBuilder sb522 = new java.lang.StringBuilder();
                                                        sb522.append("Error while processing sync data - e=");
                                                        e.printStackTrace();
                                                        sb522.append(com.fossil.blesdk.obfuscated.qa4.f17909a);
                                                        local1022.mo33256e(b1522, sb522.toString());
                                                        e.printStackTrace();
                                                        com.portfolio.platform.PortfolioApp c522 = mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c();
                                                        com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode622 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC;
                                                        c522.mo34488a(communicateMode622, str2, "Sync data result OK but has exception error e=" + e);
                                                        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote522 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                                                        com.misfit.frameworks.buttonservice.log.FLogger.Component component522 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                                                        com.misfit.frameworks.buttonservice.log.FLogger.Session session522 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC;
                                                        java.lang.String b1622 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                        remote522.mo33266i(component522, session522, str2, b1622, "Sync data result OK but has exception error e=" + e);
                                                        mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c().mo34528c(z);
                                                        com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39521e(str);
                                                        com.portfolio.platform.news.notifications.FossilNotificationBar.f21301c.mo39716a(mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a);
                                                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                                                    }
                                                } else {
                                                    i2 = i;
                                                    str12 = str26;
                                                }
                                            } else if (i != 0) {
                                                try {
                                                    java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> a6 = this.this$0.f21423a.mo39818a((java.util.List<com.fossil.fitness.FitnessData>) arrayList3, str23, new org.joda.time.DateTime());
                                                    this.this$0.f21423a.mo39835f().saveFitnessData(a6);
                                                    com.portfolio.platform.service.usecase.DianaSyncDataProcessing.C6089a a7 = com.portfolio.platform.service.usecase.DianaSyncDataProcessing.f21647b.mo40131a(str23, a6, currentUser, j3, j4, j2, this.this$0.f21423a.mo39831c());
                                                    com.portfolio.platform.service.usecase.DianaSyncDataProcessing dianaSyncDataProcessing = com.portfolio.platform.service.usecase.DianaSyncDataProcessing.f21647b;
                                                    com.portfolio.platform.data.source.SleepSessionsRepository n2 = this.this$0.f21423a.mo39843n();
                                                    com.portfolio.platform.data.source.SummariesRepository p2 = this.this$0.f21423a.mo39850p();
                                                    com.portfolio.platform.data.source.SleepSummariesRepository o2 = this.this$0.f21423a.mo39844o();
                                                    com.portfolio.platform.data.source.HeartRateSampleRepository i25 = this.this$0.f21423a.mo39838i();
                                                    com.portfolio.platform.data.source.HeartRateSummaryRepository j5 = this.this$0.f21423a.mo39839j();
                                                    com.portfolio.platform.data.source.WorkoutSessionRepository u = this.this$0.f21423a.mo39855u();
                                                    com.portfolio.platform.data.source.FitnessDataRepository f2 = this.this$0.f21423a.mo39835f();
                                                    com.portfolio.platform.data.source.ActivitiesRepository b17 = this.this$0.f21423a.mo39829b();
                                                    com.portfolio.platform.data.source.ThirdPartyRepository q2 = this.this$0.f21423a.mo39851q();
                                                    com.portfolio.platform.PortfolioApp c6 = this.this$0.f21423a.mo39831c();
                                                    com.fossil.blesdk.obfuscated.xk2 g2 = this.this$0.f21423a.mo39836g();
                                                    this.L$0 = zg4;
                                                    this.I$0 = i12;
                                                    this.L$1 = communicateMode2;
                                                    this.L$2 = str23;
                                                    this.I$1 = intExtra2;
                                                    this.I$2 = i16;
                                                    this.L$3 = arrayList4;
                                                    this.L$4 = bundle3;
                                                    this.L$5 = sKUModel;
                                                    this.I$3 = i15;
                                                    this.I$4 = i14;
                                                    this.I$5 = i13;
                                                    this.L$6 = ul24;
                                                    this.J$0 = j4;
                                                    this.J$1 = j2;
                                                    this.L$7 = j3;
                                                    this.L$8 = currentUser;
                                                    this.L$9 = arrayList3;
                                                    this.L$10 = list2;
                                                    this.I$6 = i;
                                                    this.L$11 = a6;
                                                    this.L$12 = a7;
                                                    this.label = 1;
                                                    str3 = str18;
                                                    str13 = str23;
                                                    str = str19;
                                                    java.lang.Object obj8 = obj5;
                                                    try {
                                                        java.lang.Object a8 = dianaSyncDataProcessing.mo40132a(a7, str23, n2, p2, o2, i25, j5, u, f2, b17, q2, c6, g2, this);
                                                        java.lang.Object obj9 = obj8;
                                                        if (a8 != obj9) {
                                                            str4 = str13;
                                                            break;
                                                        } else {
                                                            return obj9;
                                                        }
                                                    } catch (java.lang.Exception e9) {
                                                        e = e9;
                                                        z = true;
                                                        mFDeviceService$bleActionServiceReceiver$1$onReceive$1 = this;
                                                        str2 = str13;
                                                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local10222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                                        java.lang.String b15222 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                        java.lang.StringBuilder sb5222 = new java.lang.StringBuilder();
                                                        sb5222.append("Error while processing sync data - e=");
                                                        e.printStackTrace();
                                                        sb5222.append(com.fossil.blesdk.obfuscated.qa4.f17909a);
                                                        local10222.mo33256e(b15222, sb5222.toString());
                                                        e.printStackTrace();
                                                        com.portfolio.platform.PortfolioApp c5222 = mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c();
                                                        com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode6222 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC;
                                                        c5222.mo34488a(communicateMode6222, str2, "Sync data result OK but has exception error e=" + e);
                                                        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote5222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                                                        com.misfit.frameworks.buttonservice.log.FLogger.Component component5222 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                                                        com.misfit.frameworks.buttonservice.log.FLogger.Session session5222 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC;
                                                        java.lang.String b16222 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                        remote5222.mo33266i(component5222, session5222, str2, b16222, "Sync data result OK but has exception error e=" + e);
                                                        mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c().mo34528c(z);
                                                        com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39521e(str);
                                                        com.portfolio.platform.news.notifications.FossilNotificationBar.f21301c.mo39716a(mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a);
                                                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                                                    }
                                                } catch (java.lang.Exception e10) {
                                                    e = e10;
                                                    str13 = str23;
                                                    str = str19;
                                                    z = true;
                                                    mFDeviceService$bleActionServiceReceiver$1$onReceive$1 = this;
                                                    str2 = str13;
                                                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local102222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                                    java.lang.String b152222 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                    java.lang.StringBuilder sb52222 = new java.lang.StringBuilder();
                                                    sb52222.append("Error while processing sync data - e=");
                                                    e.printStackTrace();
                                                    sb52222.append(com.fossil.blesdk.obfuscated.qa4.f17909a);
                                                    local102222.mo33256e(b152222, sb52222.toString());
                                                    e.printStackTrace();
                                                    com.portfolio.platform.PortfolioApp c52222 = mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c();
                                                    com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode62222 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC;
                                                    c52222.mo34488a(communicateMode62222, str2, "Sync data result OK but has exception error e=" + e);
                                                    com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote52222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                                                    com.misfit.frameworks.buttonservice.log.FLogger.Component component52222 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                                                    com.misfit.frameworks.buttonservice.log.FLogger.Session session52222 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC;
                                                    java.lang.String b162222 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                                    remote52222.mo33266i(component52222, session52222, str2, b162222, "Sync data result OK but has exception error e=" + e);
                                                    mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c().mo34528c(z);
                                                    com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39521e(str);
                                                    com.portfolio.platform.news.notifications.FossilNotificationBar.f21301c.mo39716a(mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a);
                                                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                                                }
                                            } else {
                                                str3 = str18;
                                                str = str19;
                                                i2 = i;
                                                str12 = str23;
                                            }
                                            i = i2;
                                            java.lang.String str28 = str12;
                                            break;
                                        } catch (java.lang.Exception e11) {
                                            e = e11;
                                            str11 = str23;
                                            mFDeviceService$bleActionServiceReceiver$1$onReceive$1 = this;
                                            str = str19;
                                            z = true;
                                            str2 = str11;
                                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local1022222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                            java.lang.String b1522222 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                            java.lang.StringBuilder sb522222 = new java.lang.StringBuilder();
                                            sb522222.append("Error while processing sync data - e=");
                                            e.printStackTrace();
                                            sb522222.append(com.fossil.blesdk.obfuscated.qa4.f17909a);
                                            local1022222.mo33256e(b1522222, sb522222.toString());
                                            e.printStackTrace();
                                            com.portfolio.platform.PortfolioApp c522222 = mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c();
                                            com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode622222 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC;
                                            c522222.mo34488a(communicateMode622222, str2, "Sync data result OK but has exception error e=" + e);
                                            com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote522222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                                            com.misfit.frameworks.buttonservice.log.FLogger.Component component522222 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                                            com.misfit.frameworks.buttonservice.log.FLogger.Session session522222 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC;
                                            java.lang.String b1622222 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                            remote522222.mo33266i(component522222, session522222, str2, b1622222, "Sync data result OK but has exception error e=" + e);
                                            mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c().mo34528c(z);
                                            com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39521e(str);
                                            com.portfolio.platform.news.notifications.FossilNotificationBar.f21301c.mo39716a(mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a);
                                            return com.fossil.blesdk.obfuscated.qa4.f17909a;
                                        }
                                    }
                                }
                            }
                            java.lang.String str29 = str6;
                            mFDeviceService$bleActionServiceReceiver$1$onReceive$1 = this;
                            str = str19;
                            if (bundle.isEmpty()) {
                                com.portfolio.platform.PortfolioApp c7 = mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c();
                                com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode7 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC;
                                if (str29 != null) {
                                    c7.mo34488a(communicateMode7, str29, "Sync data result OK but extra info is EMPTY");
                                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().mo33266i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC, str29, com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b(), "Sync data result OK but extra info is EMPTY");
                                } else {
                                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                                    throw null;
                                }
                            } else {
                                com.portfolio.platform.PortfolioApp c8 = mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c();
                                com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode8 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC;
                                if (str29 != null) {
                                    c8.mo34488a(communicateMode8, str29, "Sync data result OK but serial is " + str29);
                                    com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                                    com.misfit.frameworks.buttonservice.log.FLogger.Component component6 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                                    com.misfit.frameworks.buttonservice.log.FLogger.Session session6 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC;
                                    java.lang.String b18 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                    remote6.mo33266i(component6, session6, str29, b18, "Sync data result OK but serial is " + str29);
                                } else {
                                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                                    throw null;
                                }
                            }
                            mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c().mo34528c(true);
                        } else {
                            com.fossil.blesdk.obfuscated.ul2 ul26 = c;
                            java.lang.String str30 = str6;
                            android.os.Bundle bundle6 = bundle;
                            com.portfolio.platform.data.model.SKUModel sKUModel4 = skuModelBySerialPrefix;
                            mFDeviceService$bleActionServiceReceiver$1$onReceive$1 = this;
                            str = str19;
                            com.fossil.blesdk.obfuscated.en2 m = mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39842m();
                            m.mo27008b(m.mo27033e() + 1);
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String b19 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                            local11.mo33255d(b19, "sync status is successful - current sync success number = " + mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39842m().mo27033e());
                            mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39842m().mo27023c(java.lang.System.currentTimeMillis(), str30);
                            if (!bundle6.isEmpty() && str30 != null) {
                                mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39819a((com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) bundle6.getParcelable(str21));
                                com.portfolio.platform.service.MFDeviceService mFDeviceService = mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a;
                                mFDeviceService.mo39816a(mFDeviceService.mo39817a(), str30, false);
                                if (i11 == 13 && !mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39842m().mo27053k(str30)) {
                                    mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39842m().mo26999a(str30, bundle6.getLong(com.misfit.frameworks.common.constants.Constants.SYNC_ID), true);
                                }
                            }
                            com.portfolio.platform.service.MFDeviceService.m32279a(mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a, str30, 1, 0, (java.util.ArrayList) null, (java.lang.Integer) null, 24, (java.lang.Object) null);
                            mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39821a(sKUModel4, i11, 1);
                            if (ul26 != null) {
                                ul26.mo31555a(str9);
                                com.fossil.blesdk.obfuscated.qa4 qa43 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                            }
                            com.portfolio.platform.news.notifications.FossilNotificationBar.f21301c.mo39716a(mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a);
                            mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39858x();
                        }
                        mFDeviceService$bleActionServiceReceiver$1$onReceive$1 = this;
                        str = str19;
                        break;
                    case 3:
                        java.lang.Object obj10 = obj5;
                        int i26 = i7;
                        int i27 = i8;
                        int i28 = i5;
                        int i29 = i27;
                        if (intExtra2 == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED.ordinal() && !bundle.isEmpty()) {
                            int i30 = bundle.getInt(com.misfit.frameworks.common.constants.Constants.BATTERY);
                            java.lang.Object obj11 = obj10;
                            com.portfolio.platform.data.source.DeviceRepository d = this.this$0.f21423a.mo39833d();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str6, "serial");
                            com.portfolio.platform.data.model.Device deviceBySerial = d.getDeviceBySerial(str6);
                            if (deviceBySerial != null) {
                                if (i30 > 0) {
                                    deviceBySerial.setBatteryLevel(i30);
                                    int i31 = i29;
                                    if (deviceBySerial.getBatteryLevel() > 100) {
                                        deviceBySerial.setBatteryLevel(100);
                                    }
                                    com.fossil.blesdk.obfuscated.ug4 b20 = com.fossil.blesdk.obfuscated.nh4.m25692b();
                                    com.portfolio.platform.service.C6014xf1ad14e1 mFDeviceService$bleActionServiceReceiver$1$onReceive$1$invokeSuspend$$inlined$let$lambda$1 = new com.portfolio.platform.service.C6014xf1ad14e1((com.fossil.blesdk.obfuscated.yb4) null, this, i30, deviceBySerial);
                                    this.L$0 = zg4;
                                    this.I$0 = intExtra;
                                    this.L$1 = communicateMode;
                                    this.L$2 = str6;
                                    this.I$1 = intExtra2;
                                    this.I$2 = intExtra3;
                                    this.L$3 = arrayList2;
                                    this.L$4 = bundle;
                                    this.L$5 = skuModelBySerialPrefix;
                                    this.I$3 = i28;
                                    this.I$4 = i26;
                                    this.I$5 = i31;
                                    this.I$6 = i30;
                                    this.L$6 = deviceBySerial;
                                    this.L$7 = deviceBySerial;
                                    this.label = 3;
                                    java.lang.Object obj12 = obj11;
                                    if (com.fossil.blesdk.obfuscated.yf4.m30997a(b20, mFDeviceService$bleActionServiceReceiver$1$onReceive$1$invokeSuspend$$inlined$let$lambda$1, this) == obj12) {
                                        return obj12;
                                    }
                                }
                            }
                        }
                        break;
                    case 4:
                        java.lang.String str31 = "error_code";
                        int i32 = i8;
                        java.lang.String str32 = str9;
                        java.lang.Object obj13 = obj5;
                        int i33 = i7;
                        int i34 = i32;
                        android.content.Intent intent3 = new android.content.Intent("BROADCAST_OTA_COMPLETE");
                        com.fossil.blesdk.obfuscated.ul2 c9 = com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39516c(str5);
                        if (skuModelBySerialPrefix != null) {
                            hashMap2 = new java.util.HashMap();
                            java.lang.String sku = skuModelBySerialPrefix.getSku();
                            if (sku != null) {
                                i4 = i33;
                                java.lang.String str33 = sku;
                                ul23 = c9;
                                str16 = str33;
                            } else {
                                ul23 = c9;
                                i4 = i33;
                                str16 = str32;
                            }
                            hashMap2.put("Style_Number", str16);
                            java.lang.String deviceName = skuModelBySerialPrefix.getDeviceName();
                            if (deviceName == null) {
                                deviceName = str32;
                            }
                            hashMap2.put("Device_Name", deviceName);
                        } else {
                            ul23 = c9;
                            i4 = i33;
                            hashMap2 = null;
                        }
                        if (intExtra2 != com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED.ordinal()) {
                            android.content.Intent intent4 = intent3;
                            java.util.HashMap hashMap4 = hashMap2;
                            java.lang.String str34 = str8;
                            com.fossil.blesdk.obfuscated.ul2 ul27 = ul23;
                            if (hashMap4 != null) {
                                str14 = str31;
                                hashMap4.put(str14, java.lang.String.valueOf(intExtra3));
                                this.this$0.f21423a.mo39827a("ota_fail", (java.util.Map<java.lang.String, java.lang.String>) hashMap4);
                            } else {
                                str14 = str31;
                            }
                            com.fossil.blesdk.obfuscated.sl2 a9 = com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39510a("ota_session_optional_error");
                            a9.mo30871a(str14, java.lang.String.valueOf(intExtra3));
                            if (ul27 != null) {
                                ul27.mo31554a(a9);
                                com.fossil.blesdk.obfuscated.qa4 qa44 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                            }
                            if (ul27 != null) {
                                ul27.mo31555a(java.lang.String.valueOf(intExtra3));
                                com.fossil.blesdk.obfuscated.qa4 qa45 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                            }
                            intent4.putExtra(str34, false);
                            intent = intent4;
                            break;
                        } else {
                            if (!bundle.isEmpty()) {
                                this.this$0.f21423a.mo39819a((com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) bundle.getParcelable("device"));
                            }
                            if (this.this$0.f21423a.mo39817a() != null) {
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                java.lang.String b21 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                java.lang.StringBuilder sb6 = new java.lang.StringBuilder();
                                hashMap3 = hashMap2;
                                sb6.append("OTA complete, update fw version from device=");
                                sb6.append(str32);
                                local12.mo33255d(b21, sb6.toString());
                                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile a10 = this.this$0.f21423a.mo39817a();
                                if (a10 != null) {
                                    str15 = a10.getFirmwareVersion();
                                } else {
                                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                                    throw null;
                                }
                            } else {
                                hashMap3 = hashMap2;
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                java.lang.String b22 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                local13.mo33255d(b22, "OTA complete, update fw version from temp=" + str32);
                                str15 = this.this$0.f21423a.mo39842m().mo27045i(str6);
                            }
                            if (android.text.TextUtils.isEmpty(str15)) {
                                str7 = str32;
                                java.util.HashMap hashMap5 = hashMap3;
                                intent = intent3;
                                ul2 = ul23;
                                break;
                            } else {
                                com.portfolio.platform.data.source.DeviceRepository d2 = this.this$0.f21423a.mo39833d();
                                if (str6 != null) {
                                    com.portfolio.platform.data.model.Device deviceBySerial2 = d2.getDeviceBySerial(str6);
                                    if (deviceBySerial2 == null) {
                                        intent2 = intent3;
                                        str7 = str32;
                                        hashMap = hashMap3;
                                        ul2 = ul23;
                                        break;
                                    } else {
                                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                        java.lang.String b23 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                        str7 = str32;
                                        java.lang.StringBuilder sb7 = new java.lang.StringBuilder();
                                        int i35 = i5;
                                        sb7.append("OTA complete, update fw version=");
                                        if (str15 != null) {
                                            sb7.append(str15);
                                            local14.mo33255d(b23, sb7.toString());
                                            deviceBySerial2.setFirmwareRevision(str15);
                                            com.portfolio.platform.data.source.DeviceRepository d3 = this.this$0.f21423a.mo39833d();
                                            this.L$0 = zg4;
                                            this.I$0 = intExtra;
                                            this.L$1 = communicateMode;
                                            this.L$2 = str6;
                                            this.I$1 = intExtra2;
                                            this.I$2 = intExtra3;
                                            this.L$3 = arrayList2;
                                            this.L$4 = bundle;
                                            this.L$5 = skuModelBySerialPrefix;
                                            this.I$3 = i35;
                                            this.I$4 = i4;
                                            this.I$5 = i34;
                                            intent2 = intent3;
                                            this.L$6 = intent2;
                                            com.fossil.blesdk.obfuscated.ul2 ul28 = ul23;
                                            this.L$7 = ul28;
                                            hashMap = hashMap3;
                                            this.L$8 = hashMap;
                                            this.L$9 = str15;
                                            this.L$10 = str15;
                                            this.L$11 = deviceBySerial2;
                                            this.L$12 = deviceBySerial2;
                                            this.label = 4;
                                            obj2 = d3.updateDevice(deviceBySerial2, false, this);
                                            java.lang.Object obj14 = obj13;
                                            if (obj2 != obj14) {
                                                ul22 = ul28;
                                                break;
                                            } else {
                                                return obj14;
                                            }
                                        } else {
                                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                                            throw null;
                                        }
                                    }
                                } else {
                                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                                    throw null;
                                }
                            }
                        }
                    case 5:
                        com.portfolio.platform.PortfolioApp.f20941W.mo34583a((java.lang.Object) new com.fossil.blesdk.obfuscated.fj2(str6, intExtra2 == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED.ordinal()));
                        break;
                    case 6:
                        if (intExtra2 != com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED.ordinal()) {
                            com.portfolio.platform.PortfolioApp.f20941W.mo34583a((java.lang.Object) new com.fossil.blesdk.obfuscated.aj2(str6, false));
                            break;
                        } else {
                            com.portfolio.platform.PortfolioApp.f20941W.mo34583a((java.lang.Object) new com.fossil.blesdk.obfuscated.aj2(str6, true));
                            break;
                        }
                    case 7:
                        if (!bundle.isEmpty()) {
                            this.this$0.f21423a.mo39819a((com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) bundle.getParcelable("device"));
                            com.portfolio.platform.service.MFDeviceService mFDeviceService2 = this.this$0.f21423a;
                            com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile a11 = mFDeviceService2.mo39817a();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str6, "serial");
                            mFDeviceService2.mo39820a(a11, str6);
                            break;
                        }
                        break;
                    case 8:
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String b24 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                        java.lang.StringBuilder sb8 = new java.lang.StringBuilder();
                        int i36 = i8;
                        sb8.append("onExchangeSecretKey result ");
                        sb8.append(intExtra2);
                        local15.mo33255d(b24, sb8.toString());
                        if (intExtra2 != com.misfit.frameworks.buttonservice.enums.ServiceActionResult.ASK_FOR_RANDOM_KEY.ordinal()) {
                            java.lang.Object obj15 = obj5;
                            int i37 = i7;
                            int i38 = i36;
                            if (intExtra2 != com.misfit.frameworks.buttonservice.enums.ServiceActionResult.ASK_FOR_SERVER_SECRET_KEY.ordinal()) {
                                if (intExtra2 != com.misfit.frameworks.buttonservice.enums.ServiceActionResult.ASK_FOR_CURRENT_SECRET_KEY.ordinal()) {
                                    if (intExtra2 == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED.ordinal() && !bundle.isEmpty()) {
                                        java.lang.String string = bundle.getString(com.misfit.frameworks.buttonservice.ButtonService.DEVICE_SECRET_KEY);
                                        if (!android.text.TextUtils.isEmpty(string)) {
                                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local16 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                            java.lang.String b25 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                                            local16.mo33255d(b25, "encrypt secret key " + string + " for " + str6);
                                            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1.C60167(this, str6, string, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                                            break;
                                        }
                                    }
                                } else {
                                    com.portfolio.platform.PortfolioApp c10 = this.this$0.f21423a.mo39831c();
                                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str6, "serial");
                                    c10.mo34542i(str6);
                                    this.this$0.f21423a.mo39853s().mo34435a(new com.portfolio.platform.usecase.VerifySecretKeyUseCase.C6921b(str6), new com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1.C6017a(this, str6));
                                    break;
                                }
                            } else {
                                java.lang.String string2 = bundle.getString(com.misfit.frameworks.buttonservice.ButtonService.DEVICE_RANDOM_KEY);
                                java.lang.Object obj16 = obj15;
                                com.portfolio.platform.data.source.DeviceRepository d4 = this.this$0.f21423a.mo39833d();
                                if (string2 != null) {
                                    deviceRepository = d4;
                                    str17 = string2;
                                } else {
                                    deviceRepository = d4;
                                    str17 = str9;
                                }
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str6, "serial");
                                this.L$0 = zg4;
                                this.I$0 = intExtra;
                                this.L$1 = communicateMode;
                                this.L$2 = str6;
                                this.I$1 = intExtra2;
                                this.I$2 = intExtra3;
                                this.L$3 = arrayList2;
                                this.L$4 = bundle;
                                this.L$5 = skuModelBySerialPrefix;
                                this.I$3 = i5;
                                this.I$4 = i37;
                                this.I$5 = i38;
                                this.L$6 = string2;
                                this.label = 6;
                                obj3 = deviceRepository.swapPairingKey(str17, str6, this);
                                java.lang.Object obj17 = obj16;
                                if (obj3 == obj17) {
                                    return obj17;
                                }
                            }
                        } else {
                            com.portfolio.platform.data.source.DeviceRepository d5 = this.this$0.f21423a.mo39833d();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str6, "serial");
                            this.L$0 = zg4;
                            this.I$0 = intExtra;
                            this.L$1 = communicateMode;
                            this.L$2 = str6;
                            this.I$1 = intExtra2;
                            this.I$2 = intExtra3;
                            this.L$3 = arrayList2;
                            this.L$4 = bundle;
                            this.L$5 = skuModelBySerialPrefix;
                            this.I$3 = i5;
                            this.I$4 = i7;
                            this.I$5 = i36;
                            this.label = 5;
                            obj4 = d5.generatePairingKey(str6, this);
                            java.lang.Object obj18 = obj5;
                            if (obj4 == obj18) {
                                return obj18;
                            }
                        }
                        break;
                    case 9:
                        if (intExtra2 == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.ASK_FOR_WATCH_PARAMS.ordinal()) {
                            com.portfolio.platform.service.MFDeviceService mFDeviceService3 = this.this$0.f21423a;
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str6, "serial");
                            mFDeviceService3.mo39825a(str6, bundle);
                            break;
                        }
                        break;
                    default:
                        com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode9 = communicateMode;
                        com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39769a(communicateMode9, new com.portfolio.platform.service.BleCommandResultManager.C5998a(communicateMode9, str6, this.$intent));
                        break;
                }
            case 1:
                com.portfolio.platform.service.usecase.DianaSyncDataProcessing.C6089a aVar3 = (com.portfolio.platform.service.usecase.DianaSyncDataProcessing.C6089a) this.L$12;
                java.util.List list4 = (java.util.List) this.L$11;
                i = this.I$6;
                java.util.List list5 = (java.util.List) this.L$10;
                java.util.List list6 = (java.util.List) this.L$9;
                com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) this.L$8;
                com.misfit.frameworks.buttonservice.model.UserProfile userProfile = (com.misfit.frameworks.buttonservice.model.UserProfile) this.L$7;
                com.fossil.blesdk.obfuscated.ul2 ul29 = (com.fossil.blesdk.obfuscated.ul2) this.L$6;
                com.portfolio.platform.data.model.SKUModel sKUModel5 = (com.portfolio.platform.data.model.SKUModel) this.L$5;
                android.os.Bundle bundle7 = (android.os.Bundle) this.L$4;
                java.util.ArrayList arrayList7 = (java.util.ArrayList) this.L$3;
                str4 = (java.lang.String) this.L$2;
                com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode10 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 2:
                com.portfolio.platform.service.usecase.HybridSyncDataProcessing.C6095a aVar4 = (com.portfolio.platform.service.usecase.HybridSyncDataProcessing.C6095a) this.L$12;
                java.util.List list7 = (java.util.List) this.L$11;
                i = this.I$6;
                java.util.List list8 = (java.util.List) this.L$10;
                java.util.List list9 = (java.util.List) this.L$9;
                com.portfolio.platform.data.model.MFUser mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$8;
                com.misfit.frameworks.buttonservice.model.UserProfile userProfile2 = (com.misfit.frameworks.buttonservice.model.UserProfile) this.L$7;
                com.fossil.blesdk.obfuscated.ul2 ul210 = (com.fossil.blesdk.obfuscated.ul2) this.L$6;
                com.portfolio.platform.data.model.SKUModel sKUModel6 = (com.portfolio.platform.data.model.SKUModel) this.L$5;
                android.os.Bundle bundle8 = (android.os.Bundle) this.L$4;
                java.util.ArrayList arrayList8 = (java.util.ArrayList) this.L$3;
                str4 = (java.lang.String) this.L$2;
                com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode11 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                try {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    break;
                } catch (java.lang.Exception e12) {
                    e = e12;
                    str2 = str4;
                    str = "sync_session";
                    mFDeviceService$bleActionServiceReceiver$1$onReceive$1 = this;
                    break;
                }
            case 3:
                com.portfolio.platform.data.model.Device device = (com.portfolio.platform.data.model.Device) this.L$7;
                com.portfolio.platform.data.model.Device device2 = (com.portfolio.platform.data.model.Device) this.L$6;
                com.portfolio.platform.data.model.SKUModel sKUModel7 = (com.portfolio.platform.data.model.SKUModel) this.L$5;
                android.os.Bundle bundle9 = (android.os.Bundle) this.L$4;
                java.util.ArrayList arrayList9 = (java.util.ArrayList) this.L$3;
                java.lang.String str35 = (java.lang.String) this.L$2;
                com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode12 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg44 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            case 4:
                com.portfolio.platform.data.model.Device device3 = (com.portfolio.platform.data.model.Device) this.L$12;
                com.portfolio.platform.data.model.Device device4 = (com.portfolio.platform.data.model.Device) this.L$11;
                java.lang.String str36 = (java.lang.String) this.L$10;
                java.lang.String str37 = (java.lang.String) this.L$9;
                ul22 = (com.fossil.blesdk.obfuscated.ul2) this.L$7;
                com.portfolio.platform.data.model.SKUModel sKUModel8 = (com.portfolio.platform.data.model.SKUModel) this.L$5;
                android.os.Bundle bundle10 = (android.os.Bundle) this.L$4;
                java.util.ArrayList arrayList10 = (java.util.ArrayList) this.L$3;
                com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode13 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg45 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                str5 = "ota_session";
                str8 = "OTA_RESULT";
                str7 = "";
                intent2 = (android.content.Intent) this.L$6;
                str6 = (java.lang.String) this.L$2;
                hashMap = (java.util.HashMap) this.L$8;
                obj2 = obj;
                break;
            case 5:
                com.portfolio.platform.data.model.SKUModel sKUModel9 = (com.portfolio.platform.data.model.SKUModel) this.L$5;
                android.os.Bundle bundle11 = (android.os.Bundle) this.L$4;
                java.util.ArrayList arrayList11 = (java.util.ArrayList) this.L$3;
                com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode14 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg46 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                str6 = (java.lang.String) this.L$2;
                str9 = "";
                obj4 = obj;
                break;
            case 6:
                java.lang.String str38 = (java.lang.String) this.L$6;
                com.portfolio.platform.data.model.SKUModel sKUModel10 = (com.portfolio.platform.data.model.SKUModel) this.L$5;
                android.os.Bundle bundle12 = (android.os.Bundle) this.L$4;
                java.util.ArrayList arrayList12 = (java.util.ArrayList) this.L$3;
                com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode15 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg47 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                str6 = (java.lang.String) this.L$2;
                str9 = "";
                obj3 = obj;
                break;
            default:
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        z = true;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local10222222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String b15222222 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
        java.lang.StringBuilder sb5222222 = new java.lang.StringBuilder();
        sb5222222.append("Error while processing sync data - e=");
        e.printStackTrace();
        sb5222222.append(com.fossil.blesdk.obfuscated.qa4.f17909a);
        local10222222.mo33256e(b15222222, sb5222222.toString());
        e.printStackTrace();
        com.portfolio.platform.PortfolioApp c5222222 = mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c();
        com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode6222222 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC;
        c5222222.mo34488a(communicateMode6222222, str2, "Sync data result OK but has exception error e=" + e);
        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote5222222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
        com.misfit.frameworks.buttonservice.log.FLogger.Component component5222222 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
        com.misfit.frameworks.buttonservice.log.FLogger.Session session5222222 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC;
        java.lang.String b16222222 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
        remote5222222.mo33266i(component5222222, session5222222, str2, b16222222, "Sync data result OK but has exception error e=" + e);
        mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a.mo39831c().mo34528c(z);
        com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39521e(str);
        com.portfolio.platform.news.notifications.FossilNotificationBar.f21301c.mo39716a(mFDeviceService$bleActionServiceReceiver$1$onReceive$1.this$0.f21423a);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
