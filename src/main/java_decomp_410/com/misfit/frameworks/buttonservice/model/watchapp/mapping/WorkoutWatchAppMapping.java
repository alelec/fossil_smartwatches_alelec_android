package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import com.fossil.blesdk.device.data.watchapp.WatchApp;
import com.fossil.blesdk.device.data.watchapp.WorkoutWatchApp;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutWatchAppMapping extends WatchAppMapping {
    @DexIgnore
    public WorkoutWatchAppMapping() {
        super(WatchAppMapping.WatchAppMappingType.INSTANCE.getWORK_OUT());
    }

    @DexIgnore
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        kd4.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    public WatchApp toSDKSetting() {
        return new WorkoutWatchApp();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutWatchAppMapping(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
    }
}
