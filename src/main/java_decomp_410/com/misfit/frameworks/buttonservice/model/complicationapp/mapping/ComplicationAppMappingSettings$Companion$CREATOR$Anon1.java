package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationAppMappingSettings$Companion$CREATOR$Anon1 implements Parcelable.Creator<ComplicationAppMappingSettings> {
    @DexIgnore
    public ComplicationAppMappingSettings createFromParcel(Parcel parcel) {
        kd4.b(parcel, "parcel");
        return new ComplicationAppMappingSettings(parcel, (fd4) null);
    }

    @DexIgnore
    public ComplicationAppMappingSettings[] newArray(int i) {
        return new ComplicationAppMappingSettings[i];
    }
}
