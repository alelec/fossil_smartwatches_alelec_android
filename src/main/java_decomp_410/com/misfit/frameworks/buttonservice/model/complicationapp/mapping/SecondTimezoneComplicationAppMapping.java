package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import com.facebook.places.model.PlaceFields;
import com.fossil.blesdk.device.data.complication.Complication;
import com.fossil.blesdk.device.data.complication.TimeZoneTwoComplication;
import com.fossil.blesdk.model.complication.config.data.TimeZoneTwoComplicationDataConfig;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SecondTimezoneComplicationAppMapping extends ComplicationAppMapping {
    @DexIgnore
    public String location;
    @DexIgnore
    public int utcOffsetInMinutes;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SecondTimezoneComplicationAppMapping(String str, int i) {
        super(ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getTIMEZONE_2_TYPE());
        kd4.b(str, PlaceFields.LOCATION);
        this.location = str;
        this.utcOffsetInMinutes = i;
    }

    @DexIgnore
    public String getHash() {
        String str = getMType() + ":" + this.location + ":" + this.utcOffsetInMinutes;
        kd4.a((Object) str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public Complication toSDKSetting() {
        return new TimeZoneTwoComplication(new TimeZoneTwoComplicationDataConfig(this.location, this.utcOffsetInMinutes));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.location);
        parcel.writeInt(this.utcOffsetInMinutes);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SecondTimezoneComplicationAppMapping(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        this.location = readString == null ? "" : readString;
        this.utcOffsetInMinutes = parcel.readInt();
    }
}
