package com.misfit.frameworks.buttonservice.model;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MisfitDeviceProfile$Companion$CREATOR$1 implements android.os.Parcelable.Creator<com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile> {
    @DexIgnore
    public com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile createFromParcel(android.os.Parcel parcel) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(parcel, "in");
        return new com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile(parcel);
    }

    @DexIgnore
    public com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile[] newArray(int i) {
        return new com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile[i];
    }
}
