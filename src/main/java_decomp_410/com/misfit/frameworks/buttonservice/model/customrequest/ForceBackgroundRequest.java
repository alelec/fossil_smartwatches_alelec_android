package com.misfit.frameworks.buttonservice.model.customrequest;

import android.os.Parcel;
import com.fossil.blesdk.device.data.config.SecondTimezoneOffsetConfig;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.google.gson.Gson;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ForceBackgroundRequest extends CustomRequest {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static byte requestId;
    @DexIgnore
    public BackgroundRequestType backgroundRequestType;

    @DexIgnore
    public enum BackgroundRequestType {
        SET_OTA_APPLICATION,
        SET_OTA_BOOT_LOADER,
        GET_ACTIVITY_FILE,
        GET_HW_LOG,
        SET_MUSIC_CONTROL_ANDROID,
        SET_UI_SCRIPT,
        SET_BACKGROUND_AND_COMPLICATION_IMAGE,
        SET_NOTIFICATION_IMAGE,
        SET_LOCALIZATION_FILE,
        SET_CONFIG_FILE,
        GET_CONFIG_FILE,
        SET_NOTIFICATION_ANDROID,
        SET_MULTI_ALARM,
        GET_MULTI_ALARM,
        GET_INFO_FILE,
        SET_NOTIFICATION_FILTER,
        GET_NOTIFICATION_FILTER,
        SET_WATCH_PARAMS_FILE
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final byte getRequestId() {
            byte access$getRequestId$cp = ForceBackgroundRequest.requestId;
            ForceBackgroundRequest.requestId = (byte) (access$getRequestId$cp + 1);
            if (ForceBackgroundRequest.requestId >= Byte.MAX_VALUE) {
                ForceBackgroundRequest.requestId = (byte) 0;
            }
            return access$getRequestId$cp;
        }

        @DexIgnore
        public final void setRequestId(byte b) {
            ForceBackgroundRequest.requestId = b;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = new int[BackgroundRequestType.values().length];

        /*
        static {
            $EnumSwitchMapping$Anon0[BackgroundRequestType.SET_OTA_APPLICATION.ordinal()] = 1;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.SET_OTA_BOOT_LOADER.ordinal()] = 2;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.GET_ACTIVITY_FILE.ordinal()] = 3;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.GET_HW_LOG.ordinal()] = 4;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.SET_MUSIC_CONTROL_ANDROID.ordinal()] = 5;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.SET_UI_SCRIPT.ordinal()] = 6;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.SET_BACKGROUND_AND_COMPLICATION_IMAGE.ordinal()] = 7;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.SET_NOTIFICATION_IMAGE.ordinal()] = 8;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.SET_LOCALIZATION_FILE.ordinal()] = 9;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.SET_CONFIG_FILE.ordinal()] = 10;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.GET_CONFIG_FILE.ordinal()] = 11;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.SET_NOTIFICATION_ANDROID.ordinal()] = 12;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.SET_MULTI_ALARM.ordinal()] = 13;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.GET_MULTI_ALARM.ordinal()] = 14;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.GET_INFO_FILE.ordinal()] = 15;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.SET_NOTIFICATION_FILTER.ordinal()] = 16;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.GET_NOTIFICATION_FILTER.ordinal()] = 17;
            $EnumSwitchMapping$Anon0[BackgroundRequestType.SET_WATCH_PARAMS_FILE.ordinal()] = 18;
        }
        */
    }

    @DexIgnore
    public ForceBackgroundRequest(BackgroundRequestType backgroundRequestType2) {
        kd4.b(backgroundRequestType2, "backgroundRequestType");
        this.backgroundRequestType = backgroundRequestType2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final BackgroundRequestType getBackgroundRequestType() {
        return this.backgroundRequestType;
    }

    @DexIgnore
    public byte[] getCustomCommand() {
        ByteBuffer allocate = ByteBuffer.allocate(12);
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        allocate.put((byte) 2);
        allocate.put((byte) 241);
        allocate.put((byte) 82);
        allocate.put((byte) 78);
        byte b = (byte) 0;
        allocate.put(b);
        byte b2 = (byte) 1;
        allocate.put(b2);
        allocate.put((byte) 6);
        allocate.put(Companion.getRequestId());
        allocate.put((byte) 3);
        switch (WhenMappings.$EnumSwitchMapping$Anon0[this.backgroundRequestType.ordinal()]) {
            case 1:
                allocate.put(b2);
                allocate.putShort(1);
                break;
            case 2:
                allocate.put(b2);
                allocate.putShort(2);
                break;
            case 3:
                allocate.put(b);
                allocate.putShort(511);
                break;
            case 4:
                allocate.put(b);
                allocate.putShort(767);
                break;
            case 5:
                allocate.put(b2);
                allocate.putShort(SecondTimezoneOffsetConfig.DISABLE_TIMEZONE_OFFSET_IN_MINUTE);
                break;
            case 6:
                allocate.put(b2);
                allocate.putShort(1280);
                break;
            case 7:
                allocate.put(b2);
                allocate.putShort(1792);
                break;
            case 8:
                allocate.put(b2);
                allocate.putShort(1793);
                break;
            case 9:
                allocate.put(b2);
                allocate.putShort(1794);
                break;
            case 10:
                allocate.put(b2);
                allocate.putShort(2048);
                break;
            case 11:
                allocate.put(b);
                allocate.putShort(2303);
                break;
            case 12:
                allocate.put(b2);
                allocate.putShort(2304);
                break;
            case 13:
                allocate.put(b2);
                allocate.putShort(2560);
                break;
            case 14:
                allocate.put(b);
                allocate.putShort(2815);
                break;
            case 15:
                allocate.put(b);
                allocate.putShort(3071);
                break;
            case 16:
                allocate.put(b2);
                allocate.putShort(3072);
                break;
            case 17:
                allocate.put(b);
                allocate.putShort(3327);
                break;
            case 18:
                allocate.put(b2);
                allocate.putShort(3584);
                break;
        }
        byte[] array = allocate.array();
        kd4.a((Object) array, "buffer.array()");
        return array;
    }

    @DexIgnore
    public final void setBackgroundRequestType(BackgroundRequestType backgroundRequestType2) {
        kd4.b(backgroundRequestType2, "<set-?>");
        this.backgroundRequestType = backgroundRequestType2;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        kd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.backgroundRequestType.ordinal());
    }

    @DexIgnore
    public ForceBackgroundRequest(Parcel parcel) {
        this.backgroundRequestType = BackgroundRequestType.values()[parcel.readInt()];
    }
}
