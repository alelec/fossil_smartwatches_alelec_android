package com.misfit.frameworks.buttonservice.model;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class DianaDevice extends Device {
    @DexIgnore
    public static /* final */ int DIANA_OTA_RESET_TIME; // = 30000;

    @DexIgnore
    public int getOTAResetTime() {
        return 30000;
    }
}
