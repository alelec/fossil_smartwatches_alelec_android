package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FNotification implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    public /* final */ String appName;
    @DexIgnore
    public /* final */ long bundleCrc;
    @DexIgnore
    public /* final */ byte groupId;
    @DexIgnore
    public /* final */ String iconFwPath;
    @DexIgnore
    public /* final */ int iconResourceId;
    @DexIgnore
    public /* final */ NotificationBaseObj.ANotificationType notificationType;
    @DexIgnore
    public /* final */ String packageName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<FNotification> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public FNotification createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new FNotification(parcel);
        }

        @DexIgnore
        public FNotification[] newArray(int i) {
            return new FNotification[i];
        }
    }

    @DexIgnore
    public FNotification(String str, long j, byte b, String str2, int i, String str3, NotificationBaseObj.ANotificationType aNotificationType) {
        kd4.b(str, "appName");
        kd4.b(str2, "packageName");
        kd4.b(str3, "iconFwPath");
        kd4.b(aNotificationType, "notificationType");
        this.appName = str;
        this.bundleCrc = j;
        this.groupId = b;
        this.packageName = str2;
        this.iconResourceId = i;
        this.iconFwPath = str3;
        this.notificationType = aNotificationType;
    }

    @DexIgnore
    public static /* synthetic */ FNotification copy$default(FNotification fNotification, String str, long j, byte b, String str2, int i, String str3, NotificationBaseObj.ANotificationType aNotificationType, int i2, Object obj) {
        FNotification fNotification2 = fNotification;
        return fNotification.copy((i2 & 1) != 0 ? fNotification2.appName : str, (i2 & 2) != 0 ? fNotification2.bundleCrc : j, (i2 & 4) != 0 ? fNotification2.groupId : b, (i2 & 8) != 0 ? fNotification2.packageName : str2, (i2 & 16) != 0 ? fNotification2.iconResourceId : i, (i2 & 32) != 0 ? fNotification2.iconFwPath : str3, (i2 & 64) != 0 ? fNotification2.notificationType : aNotificationType);
    }

    @DexIgnore
    public final String component1() {
        return this.appName;
    }

    @DexIgnore
    public final long component2() {
        return this.bundleCrc;
    }

    @DexIgnore
    public final byte component3() {
        return this.groupId;
    }

    @DexIgnore
    public final String component4() {
        return this.packageName;
    }

    @DexIgnore
    public final int component5() {
        return this.iconResourceId;
    }

    @DexIgnore
    public final String component6() {
        return this.iconFwPath;
    }

    @DexIgnore
    public final NotificationBaseObj.ANotificationType component7() {
        return this.notificationType;
    }

    @DexIgnore
    public final FNotification copy(String str, long j, byte b, String str2, int i, String str3, NotificationBaseObj.ANotificationType aNotificationType) {
        kd4.b(str, "appName");
        kd4.b(str2, "packageName");
        String str4 = str3;
        kd4.b(str4, "iconFwPath");
        NotificationBaseObj.ANotificationType aNotificationType2 = aNotificationType;
        kd4.b(aNotificationType2, "notificationType");
        return new FNotification(str, j, b, str2, i, str4, aNotificationType2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof FNotification) {
                FNotification fNotification = (FNotification) obj;
                if (kd4.a((Object) this.appName, (Object) fNotification.appName)) {
                    if (this.bundleCrc == fNotification.bundleCrc) {
                        if ((this.groupId == fNotification.groupId) && kd4.a((Object) this.packageName, (Object) fNotification.packageName)) {
                            if (!(this.iconResourceId == fNotification.iconResourceId) || !kd4.a((Object) this.iconFwPath, (Object) fNotification.iconFwPath) || !kd4.a((Object) this.notificationType, (Object) fNotification.notificationType)) {
                                return false;
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getAppName() {
        return this.appName;
    }

    @DexIgnore
    public final long getBundleCrc() {
        return this.bundleCrc;
    }

    @DexIgnore
    public final byte getGroupId() {
        return this.groupId;
    }

    @DexIgnore
    public final String getIconFwPath() {
        return this.iconFwPath;
    }

    @DexIgnore
    public final int getIconResourceId() {
        return this.iconResourceId;
    }

    @DexIgnore
    public final NotificationBaseObj.ANotificationType getNotificationType() {
        return this.notificationType;
    }

    @DexIgnore
    public final String getPackageName() {
        return this.packageName;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.appName;
        int i = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        long j = this.bundleCrc;
        int i2 = ((((hashCode * 31) + ((int) (j ^ (j >>> 32)))) * 31) + this.groupId) * 31;
        String str2 = this.packageName;
        int hashCode2 = (((i2 + (str2 != null ? str2.hashCode() : 0)) * 31) + this.iconResourceId) * 31;
        String str3 = this.iconFwPath;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        NotificationBaseObj.ANotificationType aNotificationType = this.notificationType;
        if (aNotificationType != null) {
            i = aNotificationType.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public String toString() {
        return "FNotification(appName=" + this.appName + ", bundleCrc=" + this.bundleCrc + ", groupId=" + this.groupId + ", packageName=" + this.packageName + ", iconResourceId=" + this.iconResourceId + ", iconFwPath=" + this.iconFwPath + ", notificationType=" + this.notificationType + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(this.appName);
        parcel.writeLong(this.bundleCrc);
        parcel.writeByte(this.groupId);
        parcel.writeString(this.packageName);
        parcel.writeInt(this.iconResourceId);
        parcel.writeString(this.iconFwPath);
        parcel.writeInt(this.notificationType.ordinal());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public FNotification(Parcel parcel) {
        throw null;
/*        this(r3, r4, r6, r7, r8, r9, NotificationBaseObj.ANotificationType.values()[parcel.readInt()]);
        String str;
        String str2;
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        String str3 = readString != null ? readString : "";
        long readLong = parcel.readLong();
        byte readByte = parcel.readByte();
        String readString2 = parcel.readString();
        if (readString2 != null) {
            str = readString2;
        } else {
            str = "";
        }
        int readInt = parcel.readInt();
        String readString3 = parcel.readString();
        if (readString3 != null) {
            str2 = readString3;
        } else {
            str2 = "";
        }
    }

    @DexIgnore
    public FNotification() {
        this("", 1, (byte) 1, "", 1, "", NotificationBaseObj.ANotificationType.values()[1]);
*/    }
}
