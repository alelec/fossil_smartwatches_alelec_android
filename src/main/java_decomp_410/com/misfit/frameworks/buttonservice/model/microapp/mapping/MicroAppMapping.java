package com.misfit.frameworks.buttonservice.model.microapp.mapping;

import android.os.Parcel;
import android.util.Base64;
import com.fossil.blesdk.model.microapp.declaration.MicroAppDeclaration;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppButton;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLENonCustomization;
import com.misfit.frameworks.common.enums.Gesture;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MicroAppMapping extends BLEMapping implements Comparable {
    @DexIgnore
    public static /* final */ String TAG; // = "MicroAppMapping";
    @DexIgnore
    public BLECustomization customization;
    @DexIgnore
    public String[] declarationFiles;
    @DexIgnore
    public Gesture gesture;
    @DexIgnore
    public String microAppId;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Anon1 {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$common$enums$Gesture; // = new int[Gesture.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(42:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|(3:41|42|44)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(44:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|44) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0086 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0092 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x009e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x00aa */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x00b6 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x00c2 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x00ce */
        /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x00da */
        /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x00e6 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00f2 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_PRESSED.ordinal()] = 1;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_SINGLE_PRESS.ordinal()] = 2;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_SINGLE_PRESS_AND_HOLD.ordinal()] = 3;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_DOUBLE_PRESS.ordinal()] = 4;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_DOUBLE_PRESS_AND_HOLD.ordinal()] = 5;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_TRIPLE_PRESS.ordinal()] = 6;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_TRIPLE_PRESS_AND_HOLD.ordinal()] = 7;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_PRESSED.ordinal()] = 8;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_SINGLE_PRESS.ordinal()] = 9;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_SINGLE_PRESS_AND_HOLD.ordinal()] = 10;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_DOUBLE_PRESS.ordinal()] = 11;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_DOUBLE_PRESS_AND_HOLD.ordinal()] = 12;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_TRIPLE_PRESS.ordinal()] = 13;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_TRIPLE_PRESS_AND_HOLD.ordinal()] = 14;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_PRESSED.ordinal()] = 15;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_SINGLE_PRESS.ordinal()] = 16;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_SINGLE_PRESS_AND_HOLD.ordinal()] = 17;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_DOUBLE_PRESS.ordinal()] = 18;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_DOUBLE_PRESS_AND_HOLD.ordinal()] = 19;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_TRIPLE_PRESS.ordinal()] = 20;
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_TRIPLE_PRESS_AND_HOLD.ordinal()] = 21;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public MicroAppMapping(Gesture gesture2, String str, String str2, Long l) {
        super(2, l.longValue());
        this.gesture = gesture2;
        this.microAppId = str;
        this.declarationFiles = new String[1];
        this.declarationFiles[0] = str2;
        this.customization = new BLENonCustomization();
    }

    @DexIgnore
    public static List<BLEMapping> convertToBLEMapping(List<MicroAppMapping> list) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(list);
        return arrayList;
    }

    @DexIgnore
    public static List<MicroAppMapping> convertToMicroAppMapping(List<BLEMapping> list) {
        ArrayList arrayList = new ArrayList();
        Iterator<BLEMapping> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add((MicroAppMapping) it.next());
        }
        return arrayList;
    }

    @DexIgnore
    public static List<com.fossil.blesdk.model.microapp.MicroAppMapping> convertToSDKMapping(List<BLEMapping> list) {
        ArrayList arrayList = new ArrayList();
        Iterator<BLEMapping> it = list.iterator();
        while (it.hasNext()) {
            MicroAppMapping microAppMapping = (MicroAppMapping) it.next();
            ArrayList arrayList2 = new ArrayList();
            byte[] bArr = new byte[0];
            for (String str : microAppMapping.declarationFiles) {
                if (str != null) {
                    bArr = Base64.decode(str, 0);
                }
                FLogger.INSTANCE.getLocal().d(TAG, "convertToMicroAppMapping appId is " + microAppMapping.microAppId + " and extraInfo=" + str);
                arrayList2.add(new MicroAppDeclaration(bArr, microAppMapping.customization.getCustomizationFrame()));
            }
            arrayList.add(new com.fossil.blesdk.model.microapp.MicroAppMapping(microAppMapping.getButton(), (MicroAppDeclaration[]) arrayList2.toArray(new MicroAppDeclaration[0])));
        }
        return arrayList;
    }

    @DexIgnore
    public int compareTo(Object obj) {
        if (obj != null && (obj instanceof MicroAppMapping)) {
            MicroAppMapping microAppMapping = (MicroAppMapping) obj;
            if (microAppMapping.getGesture() == getGesture()) {
                return 0;
            }
            if (microAppMapping.getGesture().getValue() > getGesture().getValue()) {
                return -1;
            }
        }
        return 1;
    }

    @DexIgnore
    public MicroAppButton getButton() {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture[this.gesture.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                return MicroAppButton.TOP;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
                return MicroAppButton.MIDDLE;
            default:
                return MicroAppButton.BOTTOM;
        }
    }

    @DexIgnore
    public BLECustomization getCustomization() {
        return this.customization;
    }

    @DexIgnore
    public String[] getDeclarationFiles() {
        return this.declarationFiles;
    }

    @DexIgnore
    public Gesture getGesture() {
        return this.gesture;
    }

    @DexIgnore
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.mType);
        sb.append(":");
        sb.append(this.gesture);
        sb.append(":");
        sb.append(this.microAppId);
        sb.append(":");
        for (String append : this.declarationFiles) {
            sb.append(append);
            sb.append(":");
        }
        sb.append(this.customization.getHash());
        return sb.toString();
    }

    @DexIgnore
    public String getMicroAppId() {
        return this.microAppId;
    }

    @DexIgnore
    public boolean isNeedHID() {
        return MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.microAppId).isNeedHID();
    }

    @DexIgnore
    public boolean isNeedStreaming() {
        return MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.microAppId).isNeedStreaming();
    }

    @DexIgnore
    public void setCustomization(BLECustomization bLECustomization) {
        this.customization = bLECustomization;
    }

    @DexIgnore
    public void setGesture(Gesture gesture2) {
        this.gesture = gesture2;
    }

    @DexIgnore
    public void setMicroAppId(String str) {
        this.microAppId = str;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.gesture.getValue());
        parcel.writeString(this.microAppId);
        parcel.writeInt(this.declarationFiles.length);
        for (String writeString : this.declarationFiles) {
            parcel.writeString(writeString);
        }
        this.customization.writeToParcel(parcel, i);
    }

    @DexIgnore
    public MicroAppMapping(Gesture gesture2, String str, String[] strArr, Long l) {
        super(2, l.longValue());
        this.gesture = gesture2;
        this.microAppId = str;
        this.declarationFiles = strArr;
        this.customization = new BLENonCustomization();
    }

    @DexIgnore
    public MicroAppMapping(Gesture gesture2, String str, String[] strArr, BLECustomization bLECustomization, Long l) {
        this(gesture2, str, strArr, l);
        this.customization = bLECustomization;
    }

    @DexIgnore
    public MicroAppMapping(Parcel parcel) {
        super(parcel);
        this.gesture = Gesture.fromInt(parcel.readInt());
        this.microAppId = parcel.readString();
        int readInt = parcel.readInt();
        this.declarationFiles = new String[readInt];
        for (int i = 0; i < readInt; i++) {
            this.declarationFiles[i] = parcel.readString();
        }
        this.customization = BLECustomization.Companion.getCREATOR().createFromParcel(parcel);
    }
}
