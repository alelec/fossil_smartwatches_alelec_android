package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ShineDevice extends Device implements Parcelable, Comparable<Object> {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ShineDevice> CREATOR; // = new ShineDevice$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final ShineDevice clone(ScannedDevice scannedDevice) {
            kd4.b(scannedDevice, "device");
            return new ShineDevice(scannedDevice.getDeviceSerial(), scannedDevice.getDeviceName(), scannedDevice.getDeviceMACAddress(), scannedDevice.getRssi());
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ShineDevice(String str, String str2, String str3, int i) {
        super(str, str2, str3, i);
        kd4.b(str, "serial");
        kd4.b(str2, "name");
        kd4.b(str3, "macAddress");
    }

    @DexIgnore
    public int compareTo(Object obj) {
        kd4.b(obj, FacebookRequestErrorClassification.KEY_OTHER);
        if (!(obj instanceof ShineDevice)) {
            return 1;
        }
        ShineDevice shineDevice = (ShineDevice) obj;
        if (shineDevice.getRssi() == getRssi()) {
            return 0;
        }
        if (shineDevice.getRssi() > getRssi()) {
            return 1;
        }
        return -1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "dest");
        parcel.writeString(this.serial);
        parcel.writeString(this.name);
        parcel.writeString(this.macAddress);
        parcel.writeInt(this.rssi);
    }

    @DexIgnore
    public ShineDevice(Parcel parcel) {
        kd4.b(parcel, "parcel");
        this.serial = parcel.readString();
        this.name = parcel.readString();
        this.macAddress = parcel.readString();
        this.rssi = parcel.readInt();
    }
}
