package com.misfit.frameworks.buttonservice.model;

import com.fossil.blesdk.obfuscated.kd4;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LifeTimeObject {
    @DexIgnore
    public /* final */ long mLifeTime;
    @DexIgnore
    public long mStartTime; // = -1;

    @DexIgnore
    public LifeTimeObject(long j) {
        this.mLifeTime = j;
    }

    @DexIgnore
    public final boolean isExpire() {
        long j = this.mStartTime + this.mLifeTime;
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        return j < instance.getTimeInMillis();
    }

    @DexIgnore
    public final long life() {
        long j = this.mStartTime + this.mLifeTime;
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        return j - instance.getTimeInMillis();
    }

    @DexIgnore
    public final void startExpireTimeCountDown() {
        if (this.mStartTime == -1) {
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "Calendar.getInstance()");
            this.mStartTime = instance.getTimeInMillis();
        }
    }
}
