package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppMappingSettings$Companion$CREATOR$Anon1 implements Parcelable.Creator<WatchAppMappingSettings> {
    @DexIgnore
    public WatchAppMappingSettings createFromParcel(Parcel parcel) {
        kd4.b(parcel, "parcel");
        return new WatchAppMappingSettings(parcel, (fd4) null);
    }

    @DexIgnore
    public WatchAppMappingSettings[] newArray(int i) {
        return new WatchAppMappingSettings[i];
    }
}
