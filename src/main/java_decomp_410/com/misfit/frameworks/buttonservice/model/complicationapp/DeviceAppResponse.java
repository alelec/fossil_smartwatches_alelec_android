package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.model.LifeTimeObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class DeviceAppResponse implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<DeviceAppResponse> CREATOR; // = new DeviceAppResponse$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ long LIFE_TIME; // = 8000;
    @DexIgnore
    public DeviceEventId deviceEventId;
    @DexIgnore
    public boolean isForceUpdate;
    @DexIgnore
    public /* final */ LifeTimeObject lifeTimeObject;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public DeviceAppResponse(DeviceEventId deviceEventId2) {
        kd4.b(deviceEventId2, "deviceEventId");
        this.deviceEventId = deviceEventId2;
        this.lifeTimeObject = new LifeTimeObject(LIFE_TIME);
    }

    @DexIgnore
    public static /* synthetic */ DeviceData getSDKDeviceResponse$default(DeviceAppResponse deviceAppResponse, DeviceRequest deviceRequest, Version version, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                version = null;
            }
            return deviceAppResponse.getSDKDeviceResponse(deviceRequest, version);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getSDKDeviceResponse");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final DeviceEventId getDeviceEventId() {
        return this.deviceEventId;
    }

    @DexIgnore
    public final LifeTimeObject getLifeTimeObject() {
        return this.lifeTimeObject;
    }

    @DexIgnore
    public abstract DeviceData getSDKDeviceData();

    @DexIgnore
    public abstract DeviceData getSDKDeviceResponse(DeviceRequest deviceRequest, Version version);

    @DexIgnore
    public final boolean isForceUpdate() {
        return this.isForceUpdate;
    }

    @DexIgnore
    public final void setDeviceEventId(DeviceEventId deviceEventId2) {
        kd4.b(deviceEventId2, "<set-?>");
        this.deviceEventId = deviceEventId2;
    }

    @DexIgnore
    public final void setForceUpdate(boolean z) {
        this.isForceUpdate = z;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        kd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(getClass().getName());
        parcel.writeInt(this.deviceEventId.ordinal());
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DeviceAppResponse(Parcel parcel) {
        this(DeviceEventId.values()[parcel.readInt()]);
        kd4.b(parcel, "parcel");
    }
}
