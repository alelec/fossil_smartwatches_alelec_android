package com.misfit.frameworks.buttonservice.model;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LifeCountDownObject {
    @DexIgnore
    public int mCount;

    @DexIgnore
    public LifeCountDownObject(int i) {
        this.mCount = i;
    }

    @DexIgnore
    public final int getCount() {
        return this.mCount;
    }

    @DexIgnore
    public final void goDown() {
        this.mCount--;
    }

    @DexIgnore
    public final boolean isExpire() {
        return this.mCount <= 0;
    }
}
