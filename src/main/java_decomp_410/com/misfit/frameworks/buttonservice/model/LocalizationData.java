package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.model.file.LocalizationFile;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qc4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.FileInputStream;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LocalizationData implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<LocalizationData> CREATOR; // = new LocalizationData$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public String checkSum;
    @DexIgnore
    public byte[] data;
    @DexIgnore
    public /* final */ String filePath;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        private final String bytesToString(byte[] bArr) {
            StringBuilder sb = new StringBuilder();
            int length = bArr.length;
            int i = 0;
            while (i < length) {
                String num = Integer.toString((bArr[i] & FileType.MASKED_INDEX) + 256, 16);
                kd4.a((Object) num, "Integer.toString((bInput\u2026t() and 255) + 0x100, 16)");
                if (num != null) {
                    String substring = num.substring(1);
                    kd4.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                    sb.append(substring);
                    i++;
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
            String sb2 = sb.toString();
            kd4.a((Object) sb2, "ret.toString()");
            if (sb2 != null) {
                String lowerCase = sb2.toLowerCase();
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                return lowerCase;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final String getLocalizationDataCheckSum(LocalizationData localizationData) {
            kd4.b(localizationData, "localizationData");
            if (kd4.a((Object) localizationData.getCheckSum(), (Object) "")) {
                return bytesToString(localizationData.toLocalizationFile().getData());
            }
            return localizationData.getCheckSum();
        }

        @DexIgnore
        public final boolean isTheSameFile(LocalizationData localizationData, LocalizationData localizationData2) {
            kd4.b(localizationData2, "newLocalizationData");
            if (localizationData == null) {
                return false;
            }
            String localizationDataCheckSum = getLocalizationDataCheckSum(localizationData2);
            if (!kd4.a((Object) localizationData.getFilePath(), (Object) localizationData2.getFilePath()) || !kd4.a((Object) localizationData.getCheckSum(), (Object) localizationDataCheckSum)) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public LocalizationData(String str, String str2) {
        kd4.b(str, "filePath");
        kd4.b(str2, "checkSum");
        this.filePath = str;
        this.checkSum = str2;
        initialize();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getCheckSum() {
        return this.checkSum;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.data;
    }

    @DexIgnore
    public final String getFilePath() {
        return this.filePath;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        com.fossil.blesdk.obfuscated.qc4.a(r0, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001a, code lost:
        throw r2;
     */
    @DexIgnore
    public final void initialize() {
        try {
            FileInputStream fileInputStream = new FileInputStream(this.filePath);
            this.data = pc4.a(fileInputStream);
            qa4 qa4 = qa4.a;
            qc4.a(fileInputStream, (Throwable) null);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            e.printStackTrace();
            local.e("LocalizationData", String.valueOf(qa4.a));
        }
    }

    @DexIgnore
    public final boolean isDataValid() {
        return this.data != null;
    }

    @DexIgnore
    public final void setCheckSum(String str) {
        kd4.b(str, "<set-?>");
        this.checkSum = str;
    }

    @DexIgnore
    public final void setData(byte[] bArr) {
        this.data = bArr;
    }

    @DexIgnore
    public final LocalizationFile toLocalizationFile() {
        byte[] bArr = this.data;
        if (bArr != null) {
            return new LocalizationFile(bArr);
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        kd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(LocalizationData.class.getName());
        parcel.writeString(this.filePath);
        parcel.writeString(this.checkSum);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ LocalizationData(String str, String str2, int i, fd4 fd4) {
        this(str, (i & 2) != 0 ? "" : str2);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public LocalizationData(Parcel parcel) {
        throw null;
/*        this(r0, r3 == null ? "" : r3);
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        readString = readString == null ? "" : readString;
        String readString2 = parcel.readString();
        initialize();
*/    }
}
