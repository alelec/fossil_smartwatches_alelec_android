package com.misfit.frameworks.buttonservice.model.watchapp.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.model.LifeCountDownObject;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class MusicResponse implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<MusicResponse> CREATOR; // = new MusicResponse$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ int LIFE_COUNT_DOWN; // = 7;
    @DexIgnore
    public static /* final */ String TYPE_MUSIC_EVENT; // = "Music Event";
    @DexIgnore
    public static /* final */ String TYPE_MUSIC_TRACK_INFO; // = "Music Track Info";
    @DexIgnore
    public /* final */ long createdTime;
    @DexIgnore
    public /* final */ LifeCountDownObject lifeCountDownObject; // = new LifeCountDownObject(7);
    @DexIgnore
    public /* final */ String type;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public MusicResponse(String str) {
        kd4.b(str, "type");
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        this.createdTime = instance.getTimeInMillis();
        this.type = str;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof MusicResponse)) {
            return false;
        }
        return kd4.a((Object) getHash(), (Object) ((MusicResponse) obj).getHash());
    }

    @DexIgnore
    public final long getCreatedTime() {
        return this.createdTime;
    }

    @DexIgnore
    public abstract String getHash();

    @DexIgnore
    public final LifeCountDownObject getLifeCountDownObject() {
        return this.lifeCountDownObject;
    }

    @DexIgnore
    public final String getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public String toRemoteLogString() {
        return "Type: " + this.type + ", createdTime: " + this.createdTime;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        kd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(getClass().getName());
        parcel.writeString(this.type);
    }

    @DexIgnore
    public MusicResponse(Parcel parcel) {
        kd4.b(parcel, "parcel");
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        this.createdTime = instance.getTimeInMillis();
        String readString = parcel.readString();
        this.type = readString == null ? "" : readString;
    }
}
