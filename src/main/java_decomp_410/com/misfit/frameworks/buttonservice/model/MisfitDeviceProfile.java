package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.config.VibeStrengthConfig;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.communite.ble.BleAdapter;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MisfitDeviceProfile implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<MisfitDeviceProfile> CREATOR; // = new MisfitDeviceProfile$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public String address;
    @DexIgnore
    public int batteryLevel;
    @DexIgnore
    public String deviceModel;
    @DexIgnore
    public String deviceSerial;
    @DexIgnore
    public String firmwareVersion;
    @DexIgnore
    public int gattState;
    @DexIgnore
    public HeartRateMode heartRateMode;
    @DexIgnore
    public int hidState;
    @DexIgnore
    public String locale;
    @DexIgnore
    public String localeVersion;
    @DexIgnore
    public short microAppMajorVersion;
    @DexIgnore
    public short microAppMinorVersion;
    @DexIgnore
    public String productName;
    @DexIgnore
    public VibrationStrengthObj vibrationStrength;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final MisfitDeviceProfile cloneFrom(BleAdapter bleAdapter) {
            kd4.b(bleAdapter, "bleAdapter");
            String nameBySerial = DeviceIdentityUtils.getNameBySerial(bleAdapter.getSerial());
            String firmwareVersion = bleAdapter.getFirmwareVersion();
            String deviceModel = bleAdapter.getDeviceModel();
            short microAppMajorVersion = bleAdapter.getMicroAppMajorVersion();
            short microAppMinorVersion = bleAdapter.getMicroAppMinorVersion();
            String macAddress = bleAdapter.getMacAddress();
            kd4.a((Object) nameBySerial, "productName");
            return new MisfitDeviceProfile(macAddress, nameBySerial, bleAdapter.getSerial(), deviceModel, firmwareVersion, bleAdapter.getBatteryLevel(), bleAdapter.getLocale(), bleAdapter.getGattState(), bleAdapter.getHidState(), microAppMajorVersion, microAppMinorVersion, bleAdapter.getHeartRateMode(), bleAdapter.getVibrationStrength(), bleAdapter.getLocaleVersion());
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ MisfitDeviceProfile(String str, String str2, String str3, String str4, String str5, int i, String str6, int i2, int i3, short s, short s2, HeartRateMode heartRateMode2, VibrationStrengthObj vibrationStrengthObj, String str7, int i4, fd4 fd4) {
        throw null;
        // this(str, str2, str3, str4, str5, i, str6, i2, i3, s, s2, (r0 & 2048) != 0 ? HeartRateMode.NONE : heartRateMode2, (r0 & 4096) != 0 ? new VibrationStrengthObj(2, false, 2, (fd4) null) : vibrationStrengthObj, str7);
        // int i5 = i4;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getAddress() {
        return this.address;
    }

    @DexIgnore
    public final int getBatteryLevel() {
        return this.batteryLevel;
    }

    @DexIgnore
    public final String getDeviceModel() {
        return this.deviceModel;
    }

    @DexIgnore
    public final String getDeviceSerial() {
        return this.deviceSerial;
    }

    @DexIgnore
    public final String getFirmwareVersion() {
        return this.firmwareVersion;
    }

    @DexIgnore
    public final int getGattState() {
        return this.gattState;
    }

    @DexIgnore
    public final HeartRateMode getHeartRateMode() {
        return this.heartRateMode;
    }

    @DexIgnore
    public final int getHidState() {
        return this.hidState;
    }

    @DexIgnore
    public final String getLocale() {
        return this.locale;
    }

    @DexIgnore
    public final String getLocaleVersion() {
        return this.localeVersion;
    }

    @DexIgnore
    public final short getMicroAppMajorVersion() {
        return this.microAppMajorVersion;
    }

    @DexIgnore
    public final short getMicroAppMinorVersion() {
        return this.microAppMinorVersion;
    }

    @DexIgnore
    public final String getProductName() {
        return this.productName;
    }

    @DexIgnore
    public final VibrationStrengthObj getVibrationStrength() {
        return this.vibrationStrength;
    }

    @DexIgnore
    public String toString() {
        return "[MisfitDeviceProfile: address=" + this.address + ", serial=" + this.deviceSerial + ", name=" + this.productName + ", deviceModel=" + this.deviceModel + ", firmware=" + this.firmwareVersion + ", microAppMajorVersion=" + this.microAppMajorVersion + ", microAppMinorVersion=" + this.microAppMinorVersion + ", heartRateMode=" + this.heartRateMode + ", batteryLevel=" + this.batteryLevel + ", locale=" + this.locale + ", localeVersion=" + this.localeVersion + ", vibrationStrength=" + VibrationStrengthObj.Companion.toString() + "]";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "dest");
        parcel.writeString(this.address);
        parcel.writeString(this.productName);
        parcel.writeString(this.deviceSerial);
        parcel.writeString(this.deviceModel);
        parcel.writeString(this.firmwareVersion);
        parcel.writeInt(this.batteryLevel);
        parcel.writeString(this.locale);
        parcel.writeInt(this.gattState);
        parcel.writeInt(this.hidState);
        parcel.writeInt(this.microAppMajorVersion);
        parcel.writeInt(this.microAppMinorVersion);
        parcel.writeInt(this.heartRateMode.getValue());
        parcel.writeParcelable(this.vibrationStrength, i);
        parcel.writeString(this.localeVersion);
    }

    @DexIgnore
    public MisfitDeviceProfile(String str, String str2, String str3, String str4, String str5, int i, String str6, int i2, int i3, short s, short s2, HeartRateMode heartRateMode2, VibrationStrengthObj vibrationStrengthObj, String str7) {
        kd4.b(str, "address");
        kd4.b(str2, "productName");
        kd4.b(str3, DeviceLocation.COLUMN_DEVICE_SERIAL);
        kd4.b(str4, "deviceModel");
        kd4.b(str5, "firmwareVersion");
        kd4.b(str6, "locale");
        kd4.b(heartRateMode2, "heartRateMode");
        kd4.b(vibrationStrengthObj, "vibrationStrength");
        kd4.b(str7, "localeVersion");
        this.address = str;
        this.productName = str2;
        this.deviceSerial = str3;
        this.deviceModel = str4;
        this.firmwareVersion = str5;
        this.batteryLevel = i;
        this.locale = str6;
        this.gattState = i2;
        this.hidState = i3;
        this.microAppMajorVersion = s;
        this.microAppMinorVersion = s2;
        this.heartRateMode = heartRateMode2;
        this.vibrationStrength = vibrationStrengthObj;
        this.localeVersion = str7;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public MisfitDeviceProfile(String str, String str2, String str3, String str4, String str5, int i, String str6, int i2, int i3, short s, short s2, String str7) {
        throw null;
/*        this(r2, r3, r4, r5, r6, i, r8, i2, i3, s, s2, HeartRateMode.NONE, new VibrationStrengthObj(2, false, 2, (fd4) null), r15);
        String str8 = str;
        kd4.b(str8, "address");
        String str9 = str2;
        kd4.b(str9, "productName");
        String str10 = str3;
        kd4.b(str10, DeviceLocation.COLUMN_DEVICE_SERIAL);
        String str11 = str4;
        kd4.b(str11, "deviceModel");
        String str12 = str5;
        kd4.b(str12, "firmwareVersion");
        String str13 = str6;
        kd4.b(str13, "locale");
        String str14 = str7;
        kd4.b(str14, "localeVersion");
    }

    @DexIgnore
    public MisfitDeviceProfile(Parcel parcel) {
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        this.address = readString == null ? "" : readString;
        String readString2 = parcel.readString();
        this.productName = readString2 == null ? "" : readString2;
        String readString3 = parcel.readString();
        this.deviceSerial = readString3 == null ? "" : readString3;
        String readString4 = parcel.readString();
        this.deviceModel = readString4 == null ? "" : readString4;
        String readString5 = parcel.readString();
        this.firmwareVersion = readString5 == null ? "" : readString5;
        this.batteryLevel = parcel.readInt();
        String readString6 = parcel.readString();
        this.locale = readString6 == null ? "" : readString6;
        this.gattState = parcel.readInt();
        this.hidState = parcel.readInt();
        this.microAppMajorVersion = (short) parcel.readInt();
        this.microAppMinorVersion = (short) parcel.readInt();
        this.heartRateMode = HeartRateMode.Companion.fromValue(parcel.readInt());
        VibrationStrengthObj vibrationStrengthObj = (VibrationStrengthObj) parcel.readParcelable(VibrationStrengthObj.class.getClassLoader());
        this.vibrationStrength = vibrationStrengthObj == null ? VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel(VibeStrengthConfig.VibeStrengthLevel.MEDIUM, true) : vibrationStrengthObj;
        String readString7 = parcel.readString();
        this.localeVersion = readString7 == null ? "" : readString7;
*/    }
}
