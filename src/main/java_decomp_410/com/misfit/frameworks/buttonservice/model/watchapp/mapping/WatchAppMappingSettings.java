package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.watchapp.WatchAppConfig;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppMappingSettings implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<WatchAppMappingSettings> CREATOR; // = new WatchAppMappingSettings$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public WatchAppMapping bottomAppMapping;
    @DexIgnore
    public WatchAppMapping middleAppMapping;
    @DexIgnore
    public /* final */ long timeStamp;
    @DexIgnore
    public WatchAppMapping topAppMapping;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final long compareTimeStamp(WatchAppMappingSettings watchAppMappingSettings, WatchAppMappingSettings watchAppMappingSettings2) {
            long j = 0;
            long timeStamp = watchAppMappingSettings != null ? watchAppMappingSettings.getTimeStamp() : 0;
            if (watchAppMappingSettings2 != null) {
                j = watchAppMappingSettings2.getTimeStamp();
            }
            return timeStamp - j;
        }

        @DexIgnore
        public final boolean isSettingsSame(WatchAppMappingSettings watchAppMappingSettings, WatchAppMappingSettings watchAppMappingSettings2) {
            if ((watchAppMappingSettings != null || watchAppMappingSettings2 == null) && (watchAppMappingSettings == null || watchAppMappingSettings2 != null)) {
                return kd4.a((Object) watchAppMappingSettings, (Object) watchAppMappingSettings2);
            }
            return false;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public /* synthetic */ WatchAppMappingSettings(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof WatchAppMappingSettings)) {
            return false;
        }
        return kd4.a((Object) getHash(), (Object) ((WatchAppMappingSettings) obj).getHash());
    }

    @DexIgnore
    public final WatchAppMapping getBottomAppMapping() {
        return this.bottomAppMapping;
    }

    @DexIgnore
    public final String getHash() {
        String str = this.topAppMapping.getHash() + ":" + this.middleAppMapping.getHash() + ":" + this.bottomAppMapping.getHash() + ":";
        kd4.a((Object) str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public final long getTimeStamp() {
        return this.timeStamp;
    }

    @DexIgnore
    public final void setBottomAppMapping(WatchAppMapping watchAppMapping) {
        kd4.b(watchAppMapping, "<set-?>");
        this.bottomAppMapping = watchAppMapping;
    }

    @DexIgnore
    public final WatchAppConfig toSDKSetting() {
        return new WatchAppConfig(this.topAppMapping.toSDKSetting(), this.middleAppMapping.toSDKSetting(), this.bottomAppMapping.toSDKSetting());
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        kd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeLong(this.timeStamp);
        parcel.writeParcelable(this.topAppMapping, i);
        parcel.writeParcelable(this.middleAppMapping, i);
        parcel.writeParcelable(this.bottomAppMapping, i);
    }

    @DexIgnore
    public WatchAppMappingSettings(WatchAppMapping watchAppMapping, WatchAppMapping watchAppMapping2, WatchAppMapping watchAppMapping3, long j) {
        kd4.b(watchAppMapping, "topAppMapping");
        kd4.b(watchAppMapping2, "middleAppMapping");
        kd4.b(watchAppMapping3, "bottomAppMapping");
        this.topAppMapping = watchAppMapping;
        this.middleAppMapping = watchAppMapping2;
        this.bottomAppMapping = watchAppMapping3;
        this.timeStamp = j;
    }

    @DexIgnore
    public WatchAppMappingSettings(Parcel parcel) {
        this.timeStamp = parcel.readLong();
        WatchAppMapping watchAppMapping = (WatchAppMapping) parcel.readParcelable(WatchAppMapping.class.getClassLoader());
        this.topAppMapping = watchAppMapping == null ? new MusicWatchAppMapping() : watchAppMapping;
        WatchAppMapping watchAppMapping2 = (WatchAppMapping) parcel.readParcelable(WatchAppMapping.class.getClassLoader());
        this.middleAppMapping = watchAppMapping2 == null ? new NoneWatchAppMapping() : watchAppMapping2;
        WatchAppMapping watchAppMapping3 = (WatchAppMapping) parcel.readParcelable(WatchAppMapping.class.getClassLoader());
        this.bottomAppMapping = watchAppMapping3 == null ? new DiagnosticsWatchAppMapping() : watchAppMapping3;
    }
}
