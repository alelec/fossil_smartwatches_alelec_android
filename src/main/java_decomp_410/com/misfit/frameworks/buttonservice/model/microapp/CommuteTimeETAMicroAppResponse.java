package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.device.event.request.CommuteTimeETAMicroAppRequest;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.model.devicedata.CommuteTimeETAMicroAppData;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeETAMicroAppResponse extends DeviceAppResponse {
    @DexIgnore
    public int mHour;
    @DexIgnore
    public int mMinute;

    @DexIgnore
    public CommuteTimeETAMicroAppResponse(int i, int i2) {
        super(DeviceEventId.COMMUTE_TIME_ETA_MICRO_APP);
        this.mHour = i;
        this.mMinute = i2;
    }

    @DexIgnore
    public DeviceData getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    public DeviceData getSDKDeviceResponse(DeviceRequest deviceRequest, Version version) {
        kd4.b(deviceRequest, "deviceRequest");
        if (!(deviceRequest instanceof CommuteTimeETAMicroAppRequest) || version == null) {
            return null;
        }
        return new CommuteTimeETAMicroAppData((CommuteTimeETAMicroAppRequest) deviceRequest, version, this.mHour, this.mMinute);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.mHour);
        parcel.writeInt(this.mMinute);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeETAMicroAppResponse(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
        this.mHour = parcel.readInt();
        this.mMinute = parcel.readInt();
    }
}
