package com.misfit.frameworks.buttonservice.model.watchparams;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.file.WatchParameterFile;
import com.fossil.blesdk.obfuscated.bf4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wp4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchParamsFileMapping implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    public byte[] data;
    @DexIgnore
    public /* final */ String dataContent;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WatchParamsFileMapping> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public WatchParamsFileMapping createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            String readString = parcel.readString();
            if (readString != null) {
                try {
                    Class<?> cls = Class.forName(readString);
                    kd4.a((Object) cls, "Class.forName(dynamicClassName!!)");
                    Constructor<?> declaredConstructor = cls.getDeclaredConstructor(new Class[]{Parcel.class});
                    kd4.a((Object) declaredConstructor, "dynamicClass.getDeclared\u2026uctor(Parcel::class.java)");
                    declaredConstructor.setAccessible(true);
                    Object newInstance = declaredConstructor.newInstance(new Object[]{parcel});
                    if (newInstance != null) {
                        return (WatchParamsFileMapping) newInstance;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    return null;
                } catch (NoSuchMethodException e2) {
                    e2.printStackTrace();
                    return null;
                } catch (IllegalAccessException e3) {
                    e3.printStackTrace();
                    return null;
                } catch (InstantiationException e4) {
                    e4.printStackTrace();
                    return null;
                } catch (InvocationTargetException e5) {
                    e5.printStackTrace();
                    return null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }

        @DexIgnore
        public WatchParamsFileMapping[] newArray(int i) {
            return new WatchParamsFileMapping[i];
        }
    }

    @DexIgnore
    public WatchParamsFileMapping(String str) {
        kd4.b(str, "dataContent");
        this.dataContent = str;
        initialize();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final void initialize() {
        String str = this.dataContent;
        Charset charset = bf4.a;
        if (str != null) {
            byte[] bytes = str.getBytes(charset);
            kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            this.data = wp4.a(bytes);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            byte[] bArr = this.data;
            if (bArr != null) {
                throw null;
                // local.d("WatchParamsFileMapping", k90.a(bArr, (String) null, 1, (Object) null));
            } else {
                kd4.a();
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
    }

    @DexIgnore
    public final WatchParameterFile toSDKSetting() {
        byte[] bArr = this.data;
        if (bArr != null) {
            return new WatchParameterFile(bArr);
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(WatchParamsFileMapping.class.getName());
        parcel.writeString(this.dataContent);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public WatchParamsFileMapping(Parcel parcel) {
        throw null;
/*        this(r2 == null ? "" : r2);
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        initialize();
*/    }
}
