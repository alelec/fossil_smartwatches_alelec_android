package com.misfit.frameworks.buttonservice.model.watchapp.mapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppMappingSettings$Companion$CREATOR$1 implements android.os.Parcelable.Creator<com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings> {
    @DexIgnore
    public com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings createFromParcel(android.os.Parcel parcel) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(parcel, "parcel");
        return new com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings(parcel, (com.fossil.blesdk.obfuscated.fd4) null);
    }

    @DexIgnore
    public com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings[] newArray(int i) {
        return new com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings[i];
    }
}
