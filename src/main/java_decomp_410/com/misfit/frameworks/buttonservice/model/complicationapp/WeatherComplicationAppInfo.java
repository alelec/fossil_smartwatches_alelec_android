package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.weather.CompactWeatherInfo;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.device.event.request.WeatherComplicationRequest;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.model.devicedata.WeatherComplicationData;
import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.data.model.MFUser;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public /* final */ long expiredAt;
    @DexIgnore
    public float temperature;
    @DexIgnore
    public TemperatureUnit temperatureUnit;
    @DexIgnore
    public WeatherCondition weatherCondition;

    @DexIgnore
    public enum TemperatureUnit {
        C,
        F;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = null;

            /*
            static {
                $EnumSwitchMapping$Anon0 = new int[TemperatureUnit.values().length];
                $EnumSwitchMapping$Anon0[TemperatureUnit.C.ordinal()] = 1;
                $EnumSwitchMapping$Anon0[TemperatureUnit.F.ordinal()] = 2;
            }
            */
        }

        @DexIgnore
        public final com.fossil.blesdk.device.data.enumerate.TemperatureUnit toSdkTemperatureUnit() {
            int i = WhenMappings.$EnumSwitchMapping$Anon0[ordinal()];
            if (i == 1) {
                return com.fossil.blesdk.device.data.enumerate.TemperatureUnit.C;
            }
            if (i == 2) {
                return com.fossil.blesdk.device.data.enumerate.TemperatureUnit.F;
            }
            throw new NoWhenBranchMatchedException();
        }
    }

    @DexIgnore
    public enum WeatherCondition {
        CLEAR_DAY,
        CLEAR_NIGHT,
        RAIN,
        SNOW,
        SLEET,
        WIND,
        FOG,
        CLOUDY,
        PARTLY_CLOUDY_DAY,
        PARTLY_CLOUDY_NIGHT;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = null;

            /*
            static {
                $EnumSwitchMapping$Anon0 = new int[WeatherCondition.values().length];
                $EnumSwitchMapping$Anon0[WeatherCondition.CLEAR_DAY.ordinal()] = 1;
                $EnumSwitchMapping$Anon0[WeatherCondition.CLEAR_NIGHT.ordinal()] = 2;
                $EnumSwitchMapping$Anon0[WeatherCondition.RAIN.ordinal()] = 3;
                $EnumSwitchMapping$Anon0[WeatherCondition.SNOW.ordinal()] = 4;
                $EnumSwitchMapping$Anon0[WeatherCondition.SLEET.ordinal()] = 5;
                $EnumSwitchMapping$Anon0[WeatherCondition.WIND.ordinal()] = 6;
                $EnumSwitchMapping$Anon0[WeatherCondition.FOG.ordinal()] = 7;
                $EnumSwitchMapping$Anon0[WeatherCondition.CLOUDY.ordinal()] = 8;
                $EnumSwitchMapping$Anon0[WeatherCondition.PARTLY_CLOUDY_DAY.ordinal()] = 9;
                $EnumSwitchMapping$Anon0[WeatherCondition.PARTLY_CLOUDY_NIGHT.ordinal()] = 10;
            }
            */
        }

        @DexIgnore
        public final com.fossil.blesdk.device.data.enumerate.WeatherCondition toSdkWeatherCondition() {
            switch (WhenMappings.$EnumSwitchMapping$Anon0[ordinal()]) {
                case 1:
                    return com.fossil.blesdk.device.data.enumerate.WeatherCondition.CLEAR_DAY;
                case 2:
                    return com.fossil.blesdk.device.data.enumerate.WeatherCondition.CLEAR_NIGHT;
                case 3:
                    return com.fossil.blesdk.device.data.enumerate.WeatherCondition.RAIN;
                case 4:
                    return com.fossil.blesdk.device.data.enumerate.WeatherCondition.SNOW;
                case 5:
                    return com.fossil.blesdk.device.data.enumerate.WeatherCondition.SLEET;
                case 6:
                    return com.fossil.blesdk.device.data.enumerate.WeatherCondition.WIND;
                case 7:
                    return com.fossil.blesdk.device.data.enumerate.WeatherCondition.FOG;
                case 8:
                    return com.fossil.blesdk.device.data.enumerate.WeatherCondition.CLOUDY;
                case 9:
                    return com.fossil.blesdk.device.data.enumerate.WeatherCondition.PARTLY_CLOUDY_DAY;
                case 10:
                    return com.fossil.blesdk.device.data.enumerate.WeatherCondition.PARTLY_CLOUDY_NIGHT;
                default:
                    throw new NoWhenBranchMatchedException();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherComplicationAppInfo(float f, TemperatureUnit temperatureUnit2, WeatherCondition weatherCondition2, long j) {
        super(DeviceEventId.WEATHER_COMPLICATION);
        kd4.b(temperatureUnit2, MFUser.TEMPERATURE_UNIT);
        kd4.b(weatherCondition2, "weatherCondition");
        this.temperature = f;
        this.temperatureUnit = temperatureUnit2;
        this.weatherCondition = weatherCondition2;
        this.expiredAt = j;
    }

    @DexIgnore
    public DeviceData getSDKDeviceData() {
        return new WeatherComplicationData(new CompactWeatherInfo(this.expiredAt, this.temperatureUnit.toSdkTemperatureUnit(), this.temperature, this.weatherCondition.toSdkWeatherCondition()));
    }

    @DexIgnore
    public DeviceData getSDKDeviceResponse(DeviceRequest deviceRequest, Version version) {
        kd4.b(deviceRequest, "deviceRequest");
        if (!(deviceRequest instanceof WeatherComplicationRequest)) {
            return null;
        }
        return new WeatherComplicationData((WeatherComplicationRequest) deviceRequest, new CompactWeatherInfo(this.expiredAt, this.temperatureUnit.toSdkTemperatureUnit(), this.temperature, this.weatherCondition.toSdkWeatherCondition()));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeFloat(this.temperature);
        parcel.writeInt(this.temperatureUnit.ordinal());
        parcel.writeInt(this.weatherCondition.ordinal());
        parcel.writeLong(this.expiredAt);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherComplicationAppInfo(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
        this.temperature = parcel.readFloat();
        this.temperatureUnit = TemperatureUnit.values()[parcel.readInt()];
        this.weatherCondition = WeatherCondition.values()[parcel.readInt()];
        this.expiredAt = parcel.readLong();
    }
}
