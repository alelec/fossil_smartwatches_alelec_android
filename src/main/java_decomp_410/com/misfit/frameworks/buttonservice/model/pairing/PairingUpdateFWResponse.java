package com.misfit.frameworks.buttonservice.model.pairing;

import android.os.Parcel;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.model.FirmwareData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PairingUpdateFWResponse extends PairingResponse {
    @DexIgnore
    public /* final */ FirmwareData firmwareData;

    @DexIgnore
    public PairingUpdateFWResponse(FirmwareData firmwareData2) {
        kd4.b(firmwareData2, "firmwareData");
        this.firmwareData = firmwareData2;
    }

    @DexIgnore
    public final FirmwareData getFirmwareData() {
        return this.firmwareData;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.firmwareData, i);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingUpdateFWResponse(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
        FirmwareData firmwareData2 = (FirmwareData) parcel.readParcelable(FirmwareData.class.getClassLoader());
        this.firmwareData = firmwareData2 == null ? new FirmwareData() : firmwareData2;
    }
}
