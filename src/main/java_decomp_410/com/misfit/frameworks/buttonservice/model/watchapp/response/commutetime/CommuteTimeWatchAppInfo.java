package com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime;

import android.os.Parcel;
import com.facebook.share.internal.ShareConstants;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.commutetime.CommuteTimeInfo;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.device.event.request.CommuteTimeWatchAppRequest;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.model.devicedata.CommuteTimeWatchAppData;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeWatchAppInfo extends DeviceAppResponse {
    @DexIgnore
    public int commuteTimeInMinute;
    @DexIgnore
    public String destination; // = "";
    @DexIgnore
    public String traffic; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppInfo(String str, int i, String str2) {
        super(DeviceEventId.COMMUTE_TIME_WATCH_APP);
        kd4.b(str, ShareConstants.DESTINATION);
        kd4.b(str2, "traffic");
        this.destination = str;
        this.commuteTimeInMinute = i;
        this.traffic = str2;
    }

    @DexIgnore
    public DeviceData getSDKDeviceData() {
        return new CommuteTimeWatchAppData(new CommuteTimeInfo(this.destination, this.commuteTimeInMinute, this.traffic));
    }

    @DexIgnore
    public DeviceData getSDKDeviceResponse(DeviceRequest deviceRequest, Version version) {
        kd4.b(deviceRequest, "deviceRequest");
        if (deviceRequest instanceof CommuteTimeWatchAppRequest) {
            return new CommuteTimeWatchAppData((CommuteTimeWatchAppRequest) deviceRequest, new CommuteTimeInfo(this.destination, this.commuteTimeInMinute, this.traffic));
        }
        return null;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.destination);
        parcel.writeInt(this.commuteTimeInMinute);
        parcel.writeString(this.traffic);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppInfo(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        this.destination = readString == null ? "" : readString;
        this.commuteTimeInMinute = parcel.readInt();
        String readString2 = parcel.readString();
        this.traffic = readString2 == null ? "" : readString2;
    }
}
