package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.kd4;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BLECustomization$Companion$CREATOR$Anon1 implements Parcelable.Creator<BLECustomization> {
    @DexIgnore
    public BLECustomization createFromParcel(Parcel parcel) {
        kd4.b(parcel, "in");
        String readString = parcel.readString();
        if (readString != null) {
            try {
                Class<?> cls = Class.forName(readString);
                kd4.a((Object) cls, "Class.forName(dynamicClassName!!)");
                Constructor<?> declaredConstructor = cls.getDeclaredConstructor(new Class[]{Parcel.class});
                kd4.a((Object) declaredConstructor, "dynamicClass.getDeclared\u2026uctor(Parcel::class.java)");
                declaredConstructor.setAccessible(true);
                Object newInstance = declaredConstructor.newInstance(new Object[]{parcel});
                if (newInstance != null) {
                    return (BLECustomization) newInstance;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (NoSuchMethodException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
                return null;
            } catch (InstantiationException e4) {
                e4.printStackTrace();
                return null;
            } catch (InvocationTargetException e5) {
                e5.printStackTrace();
                return null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public BLECustomization[] newArray(int i) {
        return new BLECustomization[i];
    }
}
