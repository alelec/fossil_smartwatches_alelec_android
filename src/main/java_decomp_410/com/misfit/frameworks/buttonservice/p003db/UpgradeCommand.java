package com.misfit.frameworks.buttonservice.p003db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.db.UpgradeCommand */
public interface UpgradeCommand {
    @DexIgnore
    void execute(android.database.sqlite.SQLiteDatabase sQLiteDatabase);
}
