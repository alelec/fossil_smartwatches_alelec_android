package com.misfit.frameworks.buttonservice.p003db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.db.DataLogService */
public class DataLogService {
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_CONTENT; // = "content";
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_CREATE_AT; // = "createAt";
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_LOG_STYLE; // = "logStyle";
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_STATUS; // = "status";
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_UPDATE_AT; // = "updateAt";
    @DexIgnore
    public static /* final */ int DEF_MF_LOG; // = 0;
    @DexIgnore
    public static /* final */ int DEF_MF_OTA_LOG; // = 3;
    @DexIgnore
    public static /* final */ int DEF_MF_SETUP_LOG; // = 2;
    @DexIgnore
    public static /* final */ int DEF_MF_SYNC_LOG; // = 1;
    @DexIgnore
    public static /* final */ int DEF_STATUS_MERGED; // = 2;
    @DexIgnore
    public static /* final */ int DEF_STATUS_NOT_SENT; // = 0;
    @DexIgnore
    public static /* final */ int DEF_STATUS_SENDING; // = 1;
    @DexIgnore
    @com.j256.ormlite.field.DatabaseField(columnName = "content")
    public java.lang.String content;
    @DexIgnore
    @com.j256.ormlite.field.DatabaseField(columnName = "createAt")
    public long createAt;
    @com.j256.ormlite.field.DatabaseField(columnName = "id", mo24082id = true)

    @DexIgnore
    /* renamed from: id */
    public int f13227id;
    @DexIgnore
    @com.j256.ormlite.field.DatabaseField(columnName = "logStyle")
    public int logStyle;
    @DexIgnore
    @com.j256.ormlite.field.DatabaseField(columnName = "status")
    public int status;
    @DexIgnore
    @com.j256.ormlite.field.DatabaseField(columnName = "updateAt")
    public long updateAt;

    @DexIgnore
    public DataLogService() {
    }

    @DexIgnore
    public java.lang.String getContent() {
        return this.content;
    }

    @DexIgnore
    public long getCreateAt() {
        return this.createAt;
    }

    @DexIgnore
    public int getId() {
        return this.f13227id;
    }

    @DexIgnore
    public int getLogStyle() {
        return this.logStyle;
    }

    @DexIgnore
    public int getStatus() {
        return this.status;
    }

    @DexIgnore
    public long getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public void setContent(java.lang.String str) {
        this.content = str;
    }

    @DexIgnore
    public void setCreateAt(long j) {
        this.createAt = j;
    }

    @DexIgnore
    public void setId(int i) {
        this.f13227id = i;
    }

    @DexIgnore
    public void setLogStyle(int i) {
        this.logStyle = i;
    }

    @DexIgnore
    public void setStatus(int i) {
        this.status = i;
    }

    @DexIgnore
    public void setUpdateAt(long j) {
        this.updateAt = j;
    }

    @DexIgnore
    public DataLogService(int i, int i2, java.lang.String str, int i3, long j, long j2) {
        this.f13227id = i;
        this.status = i2;
        this.content = str;
        this.logStyle = i3;
        this.createAt = j;
        this.updateAt = j2;
    }
}
