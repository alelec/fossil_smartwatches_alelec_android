package com.misfit.frameworks.buttonservice.p003db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.db.DataFile */
public class DataFile implements java.io.Serializable {
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_DATA_FILE; // = "dataFile";
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_KEY; // = "key";
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_SERIAL; // = "serial";
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_SYNC_TIME; // = "syncTime";
    @DexIgnore
    @com.j256.ormlite.field.DatabaseField(columnName = "dataFile")
    public java.lang.String dataFile;
    @DexIgnore
    @com.j256.ormlite.field.DatabaseField(columnName = "key", mo24082id = true)
    public java.lang.String key;
    @DexIgnore
    @com.j256.ormlite.field.DatabaseField(columnName = "serial")
    public java.lang.String serial;
    @DexIgnore
    @com.j256.ormlite.field.DatabaseField(columnName = "syncTime")
    public long syncTime;

    @DexIgnore
    public DataFile() {
    }

    @DexIgnore
    public java.lang.String getDataFile() {
        return this.dataFile;
    }

    @DexIgnore
    public java.lang.String getKey() {
        return this.key;
    }

    @DexIgnore
    public java.lang.String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public long getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public java.lang.String toString() {
        return new com.google.gson.Gson().mo23096a((java.lang.Object) this);
    }

    @DexIgnore
    public DataFile(java.lang.String str, java.lang.String str2, java.lang.String str3, long j) {
        this.key = str;
        this.dataFile = str2;
        this.serial = str3;
        this.syncTime = j;
    }
}
