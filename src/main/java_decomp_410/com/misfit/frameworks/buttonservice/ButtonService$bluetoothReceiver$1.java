package com.misfit.frameworks.buttonservice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ButtonService$bluetoothReceiver$1 extends android.content.BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.ButtonService this$0;

    @DexIgnore
    public ButtonService$bluetoothReceiver$1(com.misfit.frameworks.buttonservice.ButtonService buttonService) {
        this.this$0 = buttonService;
    }

    @DexIgnore
    public void onReceive(android.content.Context context, android.content.Intent intent) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(context, "context");
        com.fossil.blesdk.obfuscated.kd4.m24411b(intent, "intent");
        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) "android.bluetooth.adapter.action.STATE_CHANGED", (java.lang.Object) intent.getAction())) {
            this.this$0.state = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE);
            switch (this.this$0.state) {
                case 10:
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33260v(com.misfit.frameworks.buttonservice.ButtonService.TAG, "Bluetooth off");
                    for (java.lang.String next : com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils.getAllActiveButtonSerial(this.this$0)) {
                        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.misfit.frameworks.buttonservice.ButtonService.TAG, "Bluetooth is off. Terminate all running sessions!!");
                        com.misfit.frameworks.buttonservice.ButtonService buttonService = this.this$0;
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) next, "key");
                        buttonService.addLogToActiveLog(next, "Bluetooth is off. Terminate all running sessions!!");
                        ((com.misfit.frameworks.buttonservice.communite.CommunicateManager) com.misfit.frameworks.buttonservice.communite.CommunicateManager.Companion.getInstance(context)).clearCommunicatorSessionQueue(next);
                    }
                    com.misfit.frameworks.buttonservice.ButtonService.access$getConnectQueue$p(this.this$0).clear();
                    return;
                case 11:
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33260v(com.misfit.frameworks.buttonservice.ButtonService.TAG, "Turning Bluetooth on...");
                    return;
                case 12:
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String access$getTAG$cp = com.misfit.frameworks.buttonservice.ButtonService.TAG;
                    local.mo33260v(access$getTAG$cp, "Bluetooth on - Wait " + (com.misfit.frameworks.buttonservice.ButtonService.WAITING_AFTER_BLUETOOTH_ON / 1000) + " seconds before connecting all devices");
                    this.this$0.lastBluetoothOn = java.lang.System.currentTimeMillis();
                    new android.os.Handler(this.this$0.getMainLooper()).postDelayed(new com.misfit.frameworks.buttonservice.ButtonService$bluetoothReceiver$1$onReceive$1(this), (long) com.misfit.frameworks.buttonservice.ButtonService.WAITING_AFTER_BLUETOOTH_ON);
                    return;
                case 13:
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33260v(com.misfit.frameworks.buttonservice.ButtonService.TAG, "Turning Bluetooth off...");
                    return;
                default:
                    return;
            }
        }
    }
}
