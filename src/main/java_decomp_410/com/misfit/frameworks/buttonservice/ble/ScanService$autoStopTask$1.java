package com.misfit.frameworks.buttonservice.ble;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ScanService$autoStopTask$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.ble.ScanService this$0;

    @DexIgnore
    public ScanService$autoStopTask$1(com.misfit.frameworks.buttonservice.ble.ScanService scanService) {
        this.this$0 = scanService;
    }

    @DexIgnore
    public final void run() {
        this.this$0.stopScan();
    }
}
