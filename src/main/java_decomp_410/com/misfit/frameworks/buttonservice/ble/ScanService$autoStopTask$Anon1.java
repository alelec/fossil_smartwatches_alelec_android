package com.misfit.frameworks.buttonservice.ble;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ScanService$autoStopTask$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ScanService this$Anon0;

    @DexIgnore
    public ScanService$autoStopTask$Anon1(ScanService scanService) {
        this.this$Anon0 = scanService;
    }

    @DexIgnore
    public final void run() {
        this.this$Anon0.stopScan();
    }
}
