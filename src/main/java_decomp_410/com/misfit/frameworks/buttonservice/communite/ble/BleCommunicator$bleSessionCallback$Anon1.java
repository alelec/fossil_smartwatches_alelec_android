package com.misfit.frameworks.buttonservice.communite.ble;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.Mapping;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleCommunicator$bleSessionCallback$Anon1 implements BleSession.BleSessionCallback {
    @DexIgnore
    public /* final */ /* synthetic */ BleCommunicator this$Anon0;

    @DexIgnore
    public BleCommunicator$bleSessionCallback$Anon1(BleCommunicator bleCommunicator) {
        this.this$Anon0 = bleCommunicator;
    }

    @DexIgnore
    public void broadcastExchangeSecretKeySuccess(String str, String str2) {
        kd4.b(str, "serial");
        kd4.b(str2, "secretKey");
        this.this$Anon0.getCommunicationResultCallback().onExchangeSecretKeySuccess(str, str2);
    }

    @DexIgnore
    public void onAskForCurrentSecretKey(String str) {
        kd4.b(str, "serial");
        this.this$Anon0.getCommunicationResultCallback().onAskForCurrentSecretKey(str);
    }

    @DexIgnore
    public void onAskForLinkServer(CommunicateMode communicateMode, Bundle bundle) {
        kd4.b(communicateMode, "communicateMode");
        kd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        if (this.this$Anon0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK || this.this$Anon0.getCurrentSession().getCommunicateMode() == CommunicateMode.SWITCH_DEVICE) {
            this.this$Anon0.getCommunicationResultCallback().onAskForLinkServer(this.this$Anon0.getSerial(), communicateMode, bundle);
        }
    }

    @DexIgnore
    public void onAskForRandomKey(String str) {
        kd4.b(str, "serial");
        this.this$Anon0.getCommunicationResultCallback().onAskForRandomKey(str);
    }

    @DexIgnore
    public void onAskForSecretKey(Bundle bundle) {
        kd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        this.this$Anon0.getCommunicationResultCallback().onAskForServerSecretKey(this.this$Anon0.getSerial(), bundle);
    }

    @DexIgnore
    public void onAskForStopWorkout(String str) {
        kd4.b(str, "serial");
        this.this$Anon0.getCommunicationResultCallback().onAskForStopWorkout(str);
    }

    @DexIgnore
    public void onBleStateResult(int i, Bundle bundle) {
        kd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        if (!BleSession.Companion.isNull(this.this$Anon0.getCurrentSession())) {
            this.this$Anon0.getCommunicationResultCallback().onCommunicatorResult(this.this$Anon0.getCurrentSession().getCommunicateMode(), this.this$Anon0.getSerial(), i, new ArrayList(), bundle);
        }
    }

    @DexIgnore
    public void onFirmwareLatest() {
        if (this.this$Anon0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK) {
            this.this$Anon0.getCommunicationResultCallback().onFirmwareLatest(this.this$Anon0.getSerial());
        }
    }

    @DexIgnore
    public void onReceivedSyncData(Bundle bundle) {
        kd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        if (this.this$Anon0.getCurrentSession().getCommunicateMode() == CommunicateMode.SYNC) {
            this.this$Anon0.getCommunicationResultCallback().onReceivedSyncData(this.this$Anon0.getSerial(), bundle);
        }
    }

    @DexIgnore
    public void onRequestLatestFirmware(Bundle bundle) {
        kd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        if (this.this$Anon0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK) {
            this.this$Anon0.getCommunicationResultCallback().onRequestLatestFirmware(this.this$Anon0.getSerial(), bundle);
        }
    }

    @DexIgnore
    public void onRequestLatestWatchParams(String str, Bundle bundle) {
        kd4.b(str, "serial");
        kd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        this.this$Anon0.getCommunicationResultCallback().onRequestLatestWatchParams(str, bundle);
    }

    @DexIgnore
    public void onStop(int i, List<Integer> list, Bundle bundle, BleSession bleSession) {
        kd4.b(list, "requiredPermissionCodes");
        kd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        kd4.b(bleSession, Constants.SESSION);
        if (kd4.a((Object) bleSession, (Object) this.this$Anon0.getCurrentSession()) || bleSession.requireBroadCastInAnyCase()) {
            this.this$Anon0.getCommunicationResultCallback().onCommunicatorResult(bleSession.getCommunicateMode(), this.this$Anon0.getSerial(), i, list, bundle);
        }
        if (kd4.a((Object) bleSession, (Object) this.this$Anon0.getCurrentSession())) {
            this.this$Anon0.setNullCurrentSession();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = this.this$Anon0.getTAG();
            local.d(tag, "Inside " + this.this$Anon0.getTAG() + ".bleSessionCallback.onStop");
            this.this$Anon0.startSessionInQueue();
        }
    }

    @DexIgnore
    public void onUpdateFirmwareFailed() {
        if (this.this$Anon0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK) {
            this.this$Anon0.getCommunicationResultCallback().onUpdateFirmwareFailed(this.this$Anon0.getSerial());
        }
    }

    @DexIgnore
    public void onUpdateFirmwareSuccess() {
        if (this.this$Anon0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK) {
            this.this$Anon0.getCommunicationResultCallback().onUpdateFirmwareSuccess(this.this$Anon0.getSerial());
        }
    }
}
