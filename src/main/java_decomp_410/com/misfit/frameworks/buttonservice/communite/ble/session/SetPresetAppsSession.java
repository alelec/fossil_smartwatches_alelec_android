package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetPresetAppsSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ BackgroundConfig mBackgroundImageConfig;
    @DexIgnore
    public /* final */ ComplicationAppMappingSettings mComplicationAppMappingSettings;
    @DexIgnore
    public /* final */ WatchAppMappingSettings mWatchAppMappingSettings;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetBackgroundImageConfigState extends BleStateAbs {
        @DexIgnore
        public g90<qa4> task;

        @DexIgnore
        public SetBackgroundImageConfigState() {
            super(SetPresetAppsSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            if (SetPresetAppsSession.this.mBackgroundImageConfig != null) {
                this.task = SetPresetAppsSession.this.getBleAdapter().setBackgroundImage(SetPresetAppsSession.this.getLogSession(), SetPresetAppsSession.this.mBackgroundImageConfig, this);
                if (this.task == null) {
                    SetPresetAppsSession.this.stop(10000);
                    return true;
                }
                startTimeout();
                return true;
            }
            SetPresetAppsSession.this.log("mWatchAppMappingSettings mComplicationAppMappingSettings is null, skip state");
            SetPresetAppsSession.this.stop(0);
            return true;
        }

        @DexIgnore
        public void onSetBackgroundImageFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            if (!retry(SetPresetAppsSession.this.getContext(), SetPresetAppsSession.this.getSerial())) {
                SetPresetAppsSession.this.log("Reach the limit retry. Stop.");
                SetPresetAppsSession.this.stop(FailureCode.FAILED_TO_SET_BACKGROUND_IMAGE_CONFIG);
            }
        }

        @DexIgnore
        public void onSetBackgroundImageSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoBackgroundImageConfig(SetPresetAppsSession.this.getBleAdapter().getContext(), SetPresetAppsSession.this.getBleAdapter().getSerial(), new Gson().a((Object) SetPresetAppsSession.this.mBackgroundImageConfig));
            SetPresetAppsSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            g90<qa4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetComplicationsState extends BleStateAbs {
        @DexIgnore
        public g90<qa4> task;

        @DexIgnore
        public SetComplicationsState() {
            super(SetPresetAppsSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            if (SetPresetAppsSession.this.mComplicationAppMappingSettings != null) {
                this.task = SetPresetAppsSession.this.getBleAdapter().setComplications(SetPresetAppsSession.this.getLogSession(), SetPresetAppsSession.this.mComplicationAppMappingSettings, this);
                if (this.task == null) {
                    SetPresetAppsSession.this.stop(10000);
                    return true;
                }
                startTimeout();
                return true;
            }
            SetPresetAppsSession.this.log("SetComplicationsState mComplicationAppMappingSettings is null, skip state");
            SetPresetAppsSession setPresetAppsSession = SetPresetAppsSession.this;
            setPresetAppsSession.enterStateWithDelayTime(setPresetAppsSession.createConcreteState(BleSessionAbs.SessionState.SET_WATCH_APPS_STATE), 500);
            return true;
        }

        @DexIgnore
        public void onSetComplicationFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            if (!retry(SetPresetAppsSession.this.getContext(), SetPresetAppsSession.this.getSerial())) {
                SetPresetAppsSession.this.log("Reach the limit retry. Stop.");
                SetPresetAppsSession.this.stop(1920);
            }
        }

        @DexIgnore
        public void onSetComplicationSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoComplicationAppSettings(SetPresetAppsSession.this.getBleAdapter().getContext(), SetPresetAppsSession.this.getBleAdapter().getSerial(), new Gson().a((Object) SetPresetAppsSession.this.mComplicationAppMappingSettings));
            SetPresetAppsSession setPresetAppsSession = SetPresetAppsSession.this;
            setPresetAppsSession.enterStateAsync(setPresetAppsSession.createConcreteState(BleSessionAbs.SessionState.SET_WATCH_APPS_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            g90<qa4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchAppsState extends BleStateAbs {
        @DexIgnore
        public g90<qa4> task;

        @DexIgnore
        public SetWatchAppsState() {
            super(SetPresetAppsSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            if (SetPresetAppsSession.this.mWatchAppMappingSettings != null) {
                this.task = SetPresetAppsSession.this.getBleAdapter().setWatchApps(SetPresetAppsSession.this.getLogSession(), SetPresetAppsSession.this.mWatchAppMappingSettings, this);
                if (this.task == null) {
                    SetPresetAppsSession.this.stop(10000);
                    return true;
                }
                startTimeout();
                return true;
            }
            SetPresetAppsSession.this.log("mWatchAppMappingSettings mComplicationAppMappingSettings is null, skip state");
            SetPresetAppsSession setPresetAppsSession = SetPresetAppsSession.this;
            setPresetAppsSession.enterStateWithDelayTime(setPresetAppsSession.createConcreteState(BleSessionAbs.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE), 500);
            return true;
        }

        @DexIgnore
        public void onSetWatchAppFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            if (!retry(SetPresetAppsSession.this.getContext(), SetPresetAppsSession.this.getSerial())) {
                SetPresetAppsSession.this.log("Reach the limit retry. Stop.");
                SetPresetAppsSession.this.stop(FailureCode.FAILED_TO_SET_WATCH_APPS);
            }
        }

        @DexIgnore
        public void onSetWatchAppSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoWatchAppSettings(SetPresetAppsSession.this.getBleAdapter().getContext(), SetPresetAppsSession.this.getBleAdapter().getSerial(), new Gson().a((Object) SetPresetAppsSession.this.mWatchAppMappingSettings));
            SetPresetAppsSession setPresetAppsSession = SetPresetAppsSession.this;
            setPresetAppsSession.enterStateWithDelayTime(setPresetAppsSession.createConcreteState(BleSessionAbs.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE), 500);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            g90<qa4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetPresetAppsSession(ComplicationAppMappingSettings complicationAppMappingSettings, WatchAppMappingSettings watchAppMappingSettings, BackgroundConfig backgroundConfig, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_PRESET_APPS_DATA, bleAdapterImpl, bleSessionCallback);
        kd4.b(bleAdapterImpl, "bleAdapter");
        this.mComplicationAppMappingSettings = complicationAppMappingSettings;
        this.mWatchAppMappingSettings = watchAppMappingSettings;
        this.mBackgroundImageConfig = backgroundConfig;
        setLogSession(FLogger.Session.SET_PRESET_APPS);
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        kd4.b(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_COMPLICATION_APPS || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_WATCH_APPS || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_BACKGROUND_IMAGE_CONFIG) ? false : true;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetPresetAppsSession setPresetAppsSession = new SetPresetAppsSession(this.mComplicationAppMappingSettings, this.mWatchAppMappingSettings, this.mBackgroundImageConfig, getBleAdapter(), getBleSessionCallback());
        setPresetAppsSession.setDevice(getDevice());
        return setPresetAppsSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_COMPLICATIONS_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_COMPLICATIONS_STATE;
        String name = SetComplicationsState.class.getName();
        kd4.a((Object) name, "SetComplicationsState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_WATCH_APPS_STATE;
        String name2 = SetWatchAppsState.class.getName();
        kd4.a((Object) name2, "SetWatchAppsState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap3 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState3 = BleSessionAbs.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE;
        String name3 = SetBackgroundImageConfigState.class.getName();
        kd4.a((Object) name3, "SetBackgroundImageConfigState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
    }
}
