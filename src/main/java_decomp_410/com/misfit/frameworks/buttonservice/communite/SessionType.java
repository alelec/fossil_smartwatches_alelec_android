package com.misfit.frameworks.buttonservice.communite;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum SessionType {
    CONNECT_WITHOUT_TIMEOUT,
    BACK_GROUND,
    CONNECT,
    URGENT,
    DEVICE_SETTING,
    SYNC,
    UI,
    SPECIAL
}
