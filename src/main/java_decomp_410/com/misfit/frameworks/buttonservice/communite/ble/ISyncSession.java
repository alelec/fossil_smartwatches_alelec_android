package com.misfit.frameworks.buttonservice.communite.ble;

import com.misfit.frameworks.buttonservice.model.UserProfile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ISyncSession {
    @DexIgnore
    int getSyncMode();

    @DexIgnore
    void updateCurrentStepAndStepGoalFromApp(boolean z, UserProfile userProfile);
}
