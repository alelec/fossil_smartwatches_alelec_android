package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.config.builder.DeviceConfigBuilder;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetStepGoalSession extends EnableMaintainingSession {
    @DexIgnore
    public /* final */ int stepGoal;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetStepGoalState extends BleStateAbs {
        @DexIgnore
        public g90<DeviceConfigKey[]> task;

        @DexIgnore
        public SetStepGoalState() {
            super(SetStepGoalSession.this.getTAG());
        }

        @DexIgnore
        private final DeviceConfigItem[] prepareConfigData() {
            DeviceConfigBuilder deviceConfigBuilder = new DeviceConfigBuilder();
            deviceConfigBuilder.e((long) SetStepGoalSession.this.stepGoal);
            return deviceConfigBuilder.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            SetStepGoalSession setStepGoalSession = SetStepGoalSession.this;
            setStepGoalSession.log("Set Step Goal: " + SetStepGoalSession.this.stepGoal);
            this.task = SetStepGoalSession.this.getBleAdapter().setDeviceConfig(SetStepGoalSession.this.getLogSession(), prepareConfigData(), this);
            if (this.task == null) {
                SetStepGoalSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            if (!retry(SetStepGoalSession.this.getContext(), SetStepGoalSession.this.getSerial())) {
                SetStepGoalSession.this.log("Reach the limit retry. Stop.");
                SetStepGoalSession.this.stop(FailureCode.FAILED_TO_SET_STEP_GOAL);
            }
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetStepGoalSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            g90<DeviceConfigKey[]> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetStepGoalSession(int i, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.URGENT, CommunicateMode.SET_STEP_GOAL, bleAdapterImpl, bleSessionCallback);
        kd4.b(bleAdapterImpl, "bleAdapter");
        this.stepGoal = i;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetStepGoalSession setStepGoalSession = new SetStepGoalSession(this.stepGoal, getBleAdapter(), getBleSessionCallback());
        setStepGoalSession.setDevice(getDevice());
        return setStepGoalSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.SET_STEP_GOAL_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_STEP_GOAL_STATE;
        String name = SetStepGoalState.class.getName();
        kd4.a((Object) name, "SetStepGoalState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
