package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class StopCurrentWorkoutSessionSession extends EnableMaintainingSession {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class StopCurrentWorkoutState extends BleStateAbs {
        @DexIgnore
        public g90<qa4> task;

        @DexIgnore
        public StopCurrentWorkoutState() {
            super(StopCurrentWorkoutSessionSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = StopCurrentWorkoutSessionSession.this.getBleAdapter().stopCurrentWorkoutSession(StopCurrentWorkoutSessionSession.this.getLogSession(), this);
            if (this.task == null) {
                StopCurrentWorkoutSessionSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onStopCurrentWorkoutSessionFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            StopCurrentWorkoutSessionSession.this.stop(FailureCode.FAILED_TO_STOP_CURRENT_WORKOUT_SESSION);
        }

        @DexIgnore
        public void onStopCurrentWorkoutSessionSuccess() {
            stopTimeout();
            StopCurrentWorkoutSessionSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            g90<qa4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StopCurrentWorkoutSessionSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.STOP_CURRENT_WORKOUT_SESSION, bleAdapterImpl, bleSessionCallback);
        kd4.b(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        StopCurrentWorkoutSessionSession stopCurrentWorkoutSessionSession = new StopCurrentWorkoutSessionSession(getBleAdapter(), getBleSessionCallback());
        stopCurrentWorkoutSessionSession.setDevice(getDevice());
        return stopCurrentWorkoutSessionSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE;
        String name = StopCurrentWorkoutState.class.getName();
        kd4.a((Object) name, "StopCurrentWorkoutState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
