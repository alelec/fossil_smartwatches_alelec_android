package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.config.builder.DeviceConfigBuilder;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetHeartRateModeSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ HeartRateMode heartRateMode;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetHeartRateModeState extends BleStateAbs {
        @DexIgnore
        public g90<DeviceConfigKey[]> task;

        @DexIgnore
        public SetHeartRateModeState() {
            super(SetHeartRateModeSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            if (SetHeartRateModeSession.this.heartRateMode != HeartRateMode.NONE) {
                SetHeartRateModeSession setHeartRateModeSession = SetHeartRateModeSession.this;
                setHeartRateModeSession.log("Set Heart Rate Mode: " + SetHeartRateModeSession.this.heartRateMode);
                DeviceConfigBuilder deviceConfigBuilder = new DeviceConfigBuilder();
                deviceConfigBuilder.a(SetHeartRateModeSession.this.heartRateMode.toSDKHeartRateMode());
                this.task = SetHeartRateModeSession.this.getBleAdapter().setDeviceConfig(SetHeartRateModeSession.this.getLogSession(), deviceConfigBuilder.a(), this);
                if (this.task == null) {
                    SetHeartRateModeSession.this.stop(10000);
                    return true;
                }
                startTimeout();
                return true;
            }
            SetHeartRateModeSession setHeartRateModeSession2 = SetHeartRateModeSession.this;
            setHeartRateModeSession2.log("Not Set Heart Rate Mode: " + SetHeartRateModeSession.this.heartRateMode);
            SetHeartRateModeSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            SetHeartRateModeSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetHeartRateModeSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            g90<DeviceConfigKey[]> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetHeartRateModeSession(HeartRateMode heartRateMode2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_HEART_RATE_MODE, bleAdapterImpl, bleSessionCallback);
        kd4.b(heartRateMode2, "heartRateMode");
        kd4.b(bleAdapterImpl, "bleAdapterV2");
        this.heartRateMode = heartRateMode2;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetHeartRateModeSession setHeartRateModeSession = new SetHeartRateModeSession(this.heartRateMode, getBleAdapter(), getBleSessionCallback());
        setHeartRateModeSession.setDevice(getDevice());
        return setHeartRateModeSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_HEART_RATE_MODE_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_HEART_RATE_MODE_STATE;
        String name = SetHeartRateModeState.class.getName();
        kd4.a((Object) name, "SetHeartRateModeState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
