package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ClearLinkMappingSession extends EnableMaintainingSession {
    @DexIgnore
    public /* final */ List<BLEMapping> mMappings;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneCleanLinkMappingState extends BleStateAbs {
        @DexIgnore
        public DoneCleanLinkMappingState() {
            super(ClearLinkMappingSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            DevicePreferenceUtils.clearAutoSetMapping(ClearLinkMappingSession.this.getBleAdapter().getContext(), ClearLinkMappingSession.this.getBleAdapter().getSerial());
            ClearLinkMappingSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ClearLinkMappingSession(List<BLEMapping> list, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.CLEAN_LINK_MAPPINGS, bleAdapterImpl, bleSessionCallback);
        kd4.b(list, "mMappings");
        kd4.b(bleAdapterImpl, "bleAdapter");
        this.mMappings = list;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        ClearLinkMappingSession clearLinkMappingSession = new ClearLinkMappingSession(this.mMappings, getBleAdapter(), getBleSessionCallback());
        clearLinkMappingSession.setDevice(getDevice());
        return clearLinkMappingSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name = DoneCleanLinkMappingState.class.getName();
        kd4.a((Object) name, "DoneCleanLinkMappingState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
