package com.misfit.frameworks.buttonservice.communite.ble;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleState$timeoutTask$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ BleState this$Anon0;

    @DexIgnore
    public BleState$timeoutTask$Anon1(BleState bleState) {
        this.this$Anon0 = bleState;
    }

    @DexIgnore
    public final void run() {
        this.this$Anon0.onTimeout();
    }
}
