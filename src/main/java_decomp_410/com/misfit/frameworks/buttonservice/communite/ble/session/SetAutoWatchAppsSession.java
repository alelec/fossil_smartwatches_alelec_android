package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.p40;
import com.fossil.blesdk.obfuscated.qa4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetAutoWatchAppsSession extends SetAutoSettingsSession {
    @DexIgnore
    public /* final */ WatchAppMappingSettings mNewWatchAppMappingSettings;
    @DexIgnore
    public WatchAppMappingSettings mOldWatchAppMappingSettings;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        public DoneState() {
            super(SetAutoWatchAppsSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoWatchAppsSession.this.stop(0);
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchAppsState extends BleStateAbs {
        @DexIgnore
        public g90<qa4> task;

        @DexIgnore
        public SetWatchAppsState() {
            super(SetAutoWatchAppsSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetAutoWatchAppsSession.this.getBleAdapter().setWatchApps(SetAutoWatchAppsSession.this.getLogSession(), SetAutoWatchAppsSession.this.mNewWatchAppMappingSettings, this);
            if (this.task == null) {
                SetAutoWatchAppsSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetWatchAppFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            if (!retry(SetAutoWatchAppsSession.this.getContext(), SetAutoWatchAppsSession.this.getSerial())) {
                SetAutoWatchAppsSession.this.log("Reach the limit retry. Stop.");
                SetAutoWatchAppsSession setAutoWatchAppsSession = SetAutoWatchAppsSession.this;
                setAutoWatchAppsSession.storeMappings(setAutoWatchAppsSession.mNewWatchAppMappingSettings, true);
                SetAutoWatchAppsSession.this.stop(FailureCode.FAILED_TO_SET_WATCH_APPS);
            }
        }

        @DexIgnore
        public void onSetWatchAppSuccess() {
            stopTimeout();
            SetAutoWatchAppsSession setAutoWatchAppsSession = SetAutoWatchAppsSession.this;
            setAutoWatchAppsSession.storeMappings(setAutoWatchAppsSession.mNewWatchAppMappingSettings, false);
            SetAutoWatchAppsSession setAutoWatchAppsSession2 = SetAutoWatchAppsSession.this;
            setAutoWatchAppsSession2.enterStateAsync(setAutoWatchAppsSession2.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            g90<qa4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoWatchAppsSession(WatchAppMappingSettings watchAppMappingSettings, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_WATCH_APPS, bleAdapterImpl, bleSessionCallback);
        kd4.b(watchAppMappingSettings, "mNewWatchAppMappingSettings");
        kd4.b(bleAdapterImpl, "bleAdapter");
        this.mNewWatchAppMappingSettings = watchAppMappingSettings;
        setLogSession(FLogger.Session.SET_WATCH_APPS);
    }

    @DexIgnore
    private final void storeMappings(WatchAppMappingSettings watchAppMappingSettings, boolean z) {
        DevicePreferenceUtils.setAutoWatchAppSettings(getBleAdapter().getContext(), getBleAdapter().getSerial(), new Gson().a((Object) watchAppMappingSettings));
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.WATCH_APPS);
        }
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetAutoWatchAppsSession setAutoWatchAppsSession = new SetAutoWatchAppsSession(this.mNewWatchAppMappingSettings, getBleAdapter(), getBleSessionCallback());
        setAutoWatchAppsSession.setDevice(getDevice());
        return setAutoWatchAppsSession;
    }

    @DexIgnore
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    public void initSettings() {
        BleState bleState;
        super.initSettings();
        this.mOldWatchAppMappingSettings = DevicePreferenceUtils.getAutoWatchAppSettings(getContext(), getSerial());
        if (getBleAdapter().isSupportedFeature(p40.class) == null) {
            log("This device does not support set complication apps.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (WatchAppMappingSettings.Companion.isSettingsSame(this.mOldWatchAppMappingSettings, this.mNewWatchAppMappingSettings)) {
            log("New complication settings and complication settings are the same, no need to store again.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (WatchAppMappingSettings.Companion.compareTimeStamp(this.mNewWatchAppMappingSettings, this.mOldWatchAppMappingSettings) > 0) {
            storeMappings(this.mNewWatchAppMappingSettings, true);
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_WATCH_APPS_STATE);
        } else {
            log("Old complication settings timestamp is greater than the new one, no need to store again.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
        setStartState(bleState);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_WATCH_APPS_STATE;
        String name = SetWatchAppsState.class.getName();
        kd4.a((Object) name, "SetWatchAppsState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        kd4.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        kd4.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
