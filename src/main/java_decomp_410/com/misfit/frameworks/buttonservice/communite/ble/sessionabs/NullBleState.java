package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NullBleState extends BleStateAbs {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NullBleState(String str) {
        super(str);
        kd4.b(str, "tag");
    }
}
