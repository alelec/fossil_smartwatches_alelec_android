package com.misfit.frameworks.buttonservice.communite.ble.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceCommunicator$sendResponseToWatch$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.qa4, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse $response;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator$sendResponseToWatch$1(com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator deviceCommunicator, com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse deviceAppResponse) {
        super(1);
        this.this$0 = deviceCommunicator;
        this.$response = deviceAppResponse;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.obfuscated.qa4) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.obfuscated.qa4 qa4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(qa4, "it");
        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
        com.misfit.frameworks.buttonservice.log.FLogger.Component component = com.misfit.frameworks.buttonservice.log.FLogger.Component.BLE;
        com.misfit.frameworks.buttonservice.log.FLogger.Session session = com.misfit.frameworks.buttonservice.log.FLogger.Session.HANDLE_WATCH_REQUEST;
        java.lang.String serial = this.this$0.getSerial();
        java.lang.String access$getTAG$p = this.this$0.getTAG();
        remote.mo33266i(component, session, serial, access$getTAG$p, "Send respond: " + this.$response.getDeviceEventId().name() + " Success");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String access$getTAG$p2 = this.this$0.getTAG();
        local.mo33255d(access$getTAG$p2, "device with serial = " + this.this$0.getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue() = " + this.$response.toString() + ", result success");
    }
}
