package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleAdapterImpl$configureMicroApp$$inlined$let$lambda$Anon1 extends Lambda implements xc4<qa4, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $mappings$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$configureMicroApp$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, List list, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$Anon0 = bleAdapterImpl;
        this.$mappings$inlined = list;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((qa4) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(qa4 qa4) {
        kd4.b(qa4, "it");
        this.this$Anon0.log(this.$logSession$inlined, "Configure MicroApp Success");
        this.$callback$inlined.onConfigureMicroAppSuccess();
    }
}
