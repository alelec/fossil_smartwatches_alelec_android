package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon1 extends Lambda implements xc4<DeviceConfigKey[], qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceConfigItem[] $deviceConfigItems$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, DeviceConfigItem[] deviceConfigItemArr, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$Anon0 = bleAdapterImpl;
        this.$deviceConfigItems$inlined = deviceConfigItemArr;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((DeviceConfigKey[]) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(DeviceConfigKey[] deviceConfigKeyArr) {
        kd4.b(deviceConfigKeyArr, "it");
        this.this$Anon0.log(this.$logSession$inlined, "Set Device Config Success");
        this.$callback$inlined.onSetDeviceConfigSuccess();
    }
}
