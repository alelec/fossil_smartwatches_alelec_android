package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import android.content.Context;
import com.fossil.blesdk.adapter.ScanError;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.workoutsession.WorkoutSession;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.fitness.FitnessData;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class BleStateAbs extends BleState implements ISessionSdkCallback {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ int MAKE_DEVICE_READY_TIMEOUT; // = 30000;
    @DexIgnore
    public static /* final */ int OTA_SUCCESS_TIMEOUT; // = 60000;
    @DexIgnore
    public static /* final */ int PROGRESS_TASK_MAXIMUM_RETRY; // = 3;
    @DexIgnore
    public static /* final */ int PROGRESS_UPDATE_TIMEOUT; // = 30000;
    @DexIgnore
    public static /* final */ int TASK_MAXIMUM_RETRY; // = 3;
    @DexIgnore
    public int maxRetries; // = 3;
    @DexIgnore
    public int retryCounter;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleStateAbs(String str) {
        super(str);
        kd4.b(str, "tagName");
        setTimeout(30000);
    }

    @DexIgnore
    public final int getMaxRetries() {
        return this.maxRetries;
    }

    @DexIgnore
    public final int getRetryCounter() {
        return this.retryCounter;
    }

    @DexIgnore
    public void onApplyHandPositionFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onApplyHandPositionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onAuthenticateDeviceFail(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onAuthenticateDeviceSuccess(byte[] bArr) {
        kd4.b(bArr, "randomKey");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onConfigureMicroAppFail(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onConfigureMicroAppSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferCompleted() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferProgressChange(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDeviceFound(Device device, int i) {
        kd4.b(device, "device");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseDataFilesFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseDataFilesSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseHWLogFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onExchangeSecretKeyFail(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onExchangeSecretKeySuccess(byte[] bArr) {
        kd4.b(bArr, "secretKey");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onFetchDeviceInfoFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onFetchDeviceInfoSuccess(DeviceInformation deviceInformation) {
        kd4.b(deviceInformation, "deviceInformation");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onGetDeviceConfigFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onGetDeviceConfigSuccess(HashMap<DeviceConfigKey, DeviceConfigItem> hashMap) {
        kd4.b(hashMap, "deviceConfiguration");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onGetWatchParamsFail() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onMoveHandFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onMoveHandSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onNextSession() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onPlayAnimationFail(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onPlayAnimationSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onPlayDeviceAnimation(boolean z, h90 h90) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadCurrentWorkoutSessionFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadCurrentWorkoutSessionSuccess(WorkoutSession workoutSession) {
        kd4.b(workoutSession, "workoutSession");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesSuccess(FitnessData[] fitnessDataArr) {
        kd4.b(fitnessDataArr, "data");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadRssiFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadRssiSuccess(int i) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReleaseHandControlFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReleaseHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onRequestHandControlFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onRequestHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onResetHandsFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onResetHandsSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onScanFail(ScanError scanError) {
        kd4.b(scanError, "scanError");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSendMicroAppDataFail(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSendMicroAppDataSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetAlarmFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetAlarmSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetBackgroundImageFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetBackgroundImageSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetComplicationFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetComplicationSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetDeviceConfigFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetDeviceConfigSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetFrontLightFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetFrontLightSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetLocalizationDataFail(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetLocalizationDataSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetNotificationFilterFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetNotificationFilterProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetNotificationFilterSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchAppFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchAppSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchParamsFail(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchParamsSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onStopCurrentWorkoutSessionFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onStopCurrentWorkoutSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onVerifySecretKeyFail(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onVerifySecretKeySuccess(boolean z) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public final boolean retry(Context context, String str) {
        kd4.b(context, "context");
        kd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "Retry state: " + getTAG() + ", counter=" + this.retryCounter + ", maxRetries=" + this.maxRetries);
        MFLogManager instance = MFLogManager.getInstance(context);
        instance.addLogForActiveLog(str, "Retry state: " + getTAG() + ", counter=" + this.retryCounter + ", maxRetries=" + this.maxRetries);
        int i = this.retryCounter;
        if (i >= this.maxRetries) {
            return false;
        }
        this.retryCounter = i + 1;
        onEnter();
        return true;
    }

    @DexIgnore
    public final void setMaxRetries(int i) {
        this.maxRetries = i;
    }

    @DexIgnore
    public final void setRetryCounter(int i) {
        this.retryCounter = i;
    }
}
