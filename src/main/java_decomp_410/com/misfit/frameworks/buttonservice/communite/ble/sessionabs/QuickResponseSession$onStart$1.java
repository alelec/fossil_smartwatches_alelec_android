package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class QuickResponseSession$onStart$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession this$0;

    @DexIgnore
    public QuickResponseSession$onStart$1(com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession quickResponseSession) {
        this.this$0 = quickResponseSession;
    }

    @DexIgnore
    public final void run() {
        this.this$0.stop(com.misfit.frameworks.buttonservice.log.FailureCode.FAILED_BY_DEVICE_DISCONNECTED);
    }
}
