package com.misfit.frameworks.buttonservice.communite.ble;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleSession$enterTaskWithDelayTime$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.Runnable $task;

    @DexIgnore
    public BleSession$enterTaskWithDelayTime$1(java.lang.Runnable runnable) {
        this.$task = runnable;
    }

    @DexIgnore
    public final void run() {
        this.$task.run();
    }
}
