package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.blesdk.adapter.ScanError;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.WatchParametersFileVersion;
import com.fossil.blesdk.device.data.calibration.HandId;
import com.fossil.blesdk.device.data.calibration.HandMovingConfig;
import com.fossil.blesdk.device.data.calibration.HandMovingDirection;
import com.fossil.blesdk.device.data.calibration.HandMovingSpeed;
import com.fossil.blesdk.device.data.calibration.HandMovingType;
import com.fossil.blesdk.device.data.config.BatteryConfig;
import com.fossil.blesdk.device.data.config.BiometricProfile;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.config.HeartRateModeConfig;
import com.fossil.blesdk.device.data.config.VibeStrengthConfig;
import com.fossil.blesdk.device.data.enumerate.Channel;
import com.fossil.blesdk.device.data.music.MusicEvent;
import com.fossil.blesdk.device.data.music.TrackInfo;
import com.fossil.blesdk.device.data.notification.NotificationFilter;
import com.fossil.blesdk.device.data.workoutsession.WorkoutSession;
import com.fossil.blesdk.error.Error;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.model.microapp.MicroAppMapping;
import com.fossil.blesdk.obfuscated.a40;
import com.fossil.blesdk.obfuscated.b40;
import com.fossil.blesdk.obfuscated.c40;
import com.fossil.blesdk.obfuscated.d40;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.e40;
import com.fossil.blesdk.obfuscated.f30;
import com.fossil.blesdk.obfuscated.f40;
import com.fossil.blesdk.obfuscated.f90;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.g30;
import com.fossil.blesdk.obfuscated.g40;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h30;
import com.fossil.blesdk.obfuscated.h40;
import com.fossil.blesdk.obfuscated.hb4;
import com.fossil.blesdk.obfuscated.i40;
import com.fossil.blesdk.obfuscated.j30;
import com.fossil.blesdk.obfuscated.j40;
import com.fossil.blesdk.obfuscated.k30;
import com.fossil.blesdk.obfuscated.k40;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l30;
import com.fossil.blesdk.obfuscated.l40;
import com.fossil.blesdk.obfuscated.m30;
import com.fossil.blesdk.obfuscated.m40;
import com.fossil.blesdk.obfuscated.o30;
import com.fossil.blesdk.obfuscated.o40;
import com.fossil.blesdk.obfuscated.p40;
import com.fossil.blesdk.obfuscated.q40;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.r40;
import com.fossil.blesdk.obfuscated.s30;
import com.fossil.blesdk.obfuscated.s40;
import com.fossil.blesdk.obfuscated.t30;
import com.fossil.blesdk.obfuscated.t40;
import com.fossil.blesdk.obfuscated.u30;
import com.fossil.blesdk.obfuscated.u40;
import com.fossil.blesdk.obfuscated.v30;
import com.fossil.blesdk.obfuscated.x30;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.y30;
import com.fossil.blesdk.obfuscated.z30;
import com.fossil.fitness.FitnessData;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import com.misfit.frameworks.buttonservice.communite.ble.BleAdapter;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.extensions.AlarmExtensionKt;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.calibration.DianaCalibrationObj;
import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleAdapterImpl extends BleAdapter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public Device deviceObj;
    @DexIgnore
    public boolean isScanning;
    @DexIgnore
    public HashMap<DeviceConfigKey, DeviceConfigItem> mDeviceConfiguration;
    @DexIgnore
    public Device.a mDeviceStateListener;
    @DexIgnore
    public ScanService scanService;
    @DexIgnore
    public byte[] tSecretKey;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class BleScanServiceCallback implements ScanService.Callback {
        @DexIgnore
        public /* final */ ISessionSdkCallback callback;

        @DexIgnore
        public BleScanServiceCallback(ISessionSdkCallback iSessionSdkCallback) {
            this.callback = iSessionSdkCallback;
        }

        @DexIgnore
        public final ISessionSdkCallback getCallback() {
            return this.callback;
        }

        @DexIgnore
        public void onDeviceFound(Device device, int i) {
            kd4.b(device, "device");
            if (device.getState() == Device.State.CONNECTED || device.getState() == Device.State.UPGRADING_FIRMWARE) {
                i = 0;
            } else if (device.getBondState() == Device.BondState.BONDED) {
                i = ScanService.RETRIEVE_DEVICE_BOND_RSSI_MARK;
            }
            ISessionSdkCallback iSessionSdkCallback = this.callback;
            if (iSessionSdkCallback != null) {
                iSessionSdkCallback.onDeviceFound(device, i);
            }
        }

        @DexIgnore
        public void onScanFail(ScanError scanError) {
            kd4.b(scanError, "scanError");
            ISessionSdkCallback iSessionSdkCallback = this.callback;
            if (iSessionSdkCallback != null) {
                iSessionSdkCallback.onScanFail(scanError);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = new int[Device.State.values().length];

        /*
        static {
            $EnumSwitchMapping$Anon0[Device.State.DISCONNECTED.ordinal()] = 1;
            $EnumSwitchMapping$Anon0[Device.State.CONNECTED.ordinal()] = 2;
            $EnumSwitchMapping$Anon0[Device.State.UPGRADING_FIRMWARE.ordinal()] = 3;
            $EnumSwitchMapping$Anon0[Device.State.CONNECTING.ordinal()] = 4;
            $EnumSwitchMapping$Anon0[Device.State.DISCONNECTING.ordinal()] = 5;
        }
        */
    }

    /*
    static {
        String simpleName = BleAdapterImpl.class.getSimpleName();
        kd4.a((Object) simpleName, "BleAdapterImpl::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl(Context context, String str, String str2) {
        super(context, str, str2);
        kd4.b(context, "context");
        kd4.b(str, "serial");
        kd4.b(str2, "macAddress");
    }

    @DexIgnore
    public final Device buildDeviceBySerial(String str, String str2, long j) {
        kd4.b(str, "serial");
        kd4.b(str2, "macAddress");
        if (this.scanService == null) {
            this.scanService = new ScanService(getContext(), (ScanService.Callback) null, j);
        }
        ScanService scanService2 = this.scanService;
        if (scanService2 != null) {
            return scanService2.buildDeviceBySerial(str, str2);
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<qa4> calibrationApplyHandPosition(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Apply Hands Positions");
        Device device = this.deviceObj;
        if (device != null) {
            h40 h40 = (h40) device.a(h40.class);
            if (h40 != null) {
                return h40.l().e(new BleAdapterImpl$calibrationApplyHandPosition$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).c(new BleAdapterImpl$calibrationApplyHandPosition$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingCalibrationPositionFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<qa4> calibrationMoveHand(FLogger.Session session, HandCalibrationObj handCalibrationObj, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(handCalibrationObj, "handCalibrationObj");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        DianaCalibrationObj consume = DianaCalibrationObj.Companion.consume(handCalibrationObj);
        HandMovingConfig[] handMovingConfigArr = {new HandMovingConfig(consume.getHandId(), consume.getDegree(), consume.getHandMovingDirection(), consume.getHandMovingSpeed())};
        log(session, "Move Hand: handMovingType=" + consume.getHandMovingType() + ", handMovingConfigs=" + handMovingConfigArr + ' ');
        Device device = this.deviceObj;
        if (device != null) {
            v30 v30 = (v30) device.a(v30.class);
            if (v30 != null) {
                DianaCalibrationObj dianaCalibrationObj = consume;
                HandMovingConfig[] handMovingConfigArr2 = handMovingConfigArr;
                FLogger.Session session2 = session;
                ISessionSdkCallback iSessionSdkCallback2 = iSessionSdkCallback;
                return v30.a(consume.getHandMovingType(), handMovingConfigArr).e(new BleAdapterImpl$calibrationMoveHand$$inlined$let$lambda$Anon1(this, dianaCalibrationObj, handMovingConfigArr2, session2, iSessionSdkCallback2)).c(new BleAdapterImpl$calibrationMoveHand$$inlined$let$lambda$Anon2(this, dianaCalibrationObj, handMovingConfigArr2, session2, iSessionSdkCallback2));
            }
            log(session, "Device[" + getSerial() + "] does not support MovingHandsFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<qa4> calibrationReleaseHandControl(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Release Hand Control");
        Device device = this.deviceObj;
        if (device != null) {
            a40 a40 = (a40) device.a(a40.class);
            if (a40 != null) {
                return a40.h().e(new BleAdapterImpl$calibrationReleaseHandControl$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).c(new BleAdapterImpl$calibrationReleaseHandControl$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support ReleasingHandsFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<qa4> calibrationRequestHandControl(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Request Hand Control");
        Device device = this.deviceObj;
        if (device != null) {
            b40 b40 = (b40) device.a(b40.class);
            if (b40 != null) {
                return b40.i().e(new BleAdapterImpl$calibrationRequestHandControl$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).c(new BleAdapterImpl$calibrationRequestHandControl$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support RequestingHandsFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<qa4> calibrationResetHandToZeroDegree(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Reset Hands to 0 degrees.");
        HandMovingConfig[] handMovingConfigArr = {new HandMovingConfig(HandId.HOUR, 0, HandMovingDirection.SHORTEST_PATH, HandMovingSpeed.FULL), new HandMovingConfig(HandId.MINUTE, 0, HandMovingDirection.SHORTEST_PATH, HandMovingSpeed.FULL), new HandMovingConfig(HandId.SUB_EYE, 0, HandMovingDirection.SHORTEST_PATH, HandMovingSpeed.FULL)};
        Device device = this.deviceObj;
        if (device != null) {
            v30 v30 = (v30) device.a(v30.class);
            if (v30 != null) {
                return v30.a(HandMovingType.POSITION, handMovingConfigArr).e(new BleAdapterImpl$calibrationResetHandToZeroDegree$$inlined$let$lambda$Anon1(this, handMovingConfigArr, session, iSessionSdkCallback)).c(new BleAdapterImpl$calibrationResetHandToZeroDegree$$inlined$let$lambda$Anon2(this, handMovingConfigArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support MovingHandsFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void closeConnection(FLogger.Session session, boolean z) {
        kd4.b(session, "logSession");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Close Connection, serial=" + getSerial());
        Device device = this.deviceObj;
        if (device != null) {
            j30 j30 = (j30) device.a(j30.class);
            if (j30 != null) {
                j30.a();
                log(session, "Close Connection, serial=" + getSerial() + ", OK");
                return;
            }
            log(session, "Device[" + getSerial() + "] does not support DisconnectingFeature");
            return;
        }
        logAppError(session, "closeConnection failed. DeviceObject is null. Connect has not been established.", ErrorCodeBuilder.Step.DISCONNECT, ErrorCodeBuilder.AppError.UNKNOWN);
    }

    @DexIgnore
    public final f90<qa4> configureMicroApp(FLogger.Session session, List<MicroAppMapping> list, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(list, "mappings");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Configure MicroApp, mapping=" + list);
        Device device = this.deviceObj;
        if (device != null) {
            g30 g30 = (g30) device.a(g30.class);
            if (g30 != null) {
                Object[] array = list.toArray(new MicroAppMapping[0]);
                if (array != null) {
                    return g30.a((MicroAppMapping[]) array).h(new BleAdapterImpl$configureMicroApp$$inlined$let$lambda$Anon1(this, list, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$configureMicroApp$$inlined$let$lambda$Anon2(this, list, session, iSessionSdkCallback));
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
            log(session, "Device[" + getSerial() + "] does not support ConfiguringMicroAppFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final f90<String> doOTA(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(bArr, "firmwareData");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Do OTA");
        Device device = this.deviceObj;
        if (device != null) {
            y30 y30 = (y30) device.a(y30.class);
            if (y30 != null) {
                return y30.b(bArr).i(new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).h(new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon3(this, bArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support OTAFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final Boolean enableMaintainConnection(FLogger.Session session) {
        kd4.b(session, "logSession");
        log(session, "Enable Maintain Connection");
        Device device = this.deviceObj;
        if (device != null) {
            k30 k30 = (k30) device.a(k30.class);
            if (k30 != null) {
                return Boolean.valueOf(k30.f());
            }
            log(session, "Device[" + getSerial() + "] does not support EnablingMaintainConnectionFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<qa4> eraseDataFile(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Erase DataFiles");
        Device device = this.deviceObj;
        if (device != null) {
            f30 f30 = (f30) device.a(f30.class);
            if (f30 != null) {
                return f30.cleanUp().h(new BleAdapterImpl$eraseDataFile$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$eraseDataFile$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support CleanUpDeviceFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void error(FLogger.Session session, String str, ErrorCodeBuilder.Step step, String str2) {
        kd4.b(session, "logSession");
        kd4.b(str, "errorCode");
        kd4.b(step, "step");
        kd4.b(str2, "errorMessage");
        MFLog activeLog = MFLogManager.getInstance(getContext()).getActiveLog(getSerial());
        if (activeLog != null) {
            activeLog.error('[' + getSerial() + "] " + str2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, '[' + getSerial() + "] error=" + str + ' ' + str2);
        FLogger.INSTANCE.getRemote().e(FLogger.Component.BLE, session, getSerial(), TAG, str, step, str2);
    }

    @DexIgnore
    public final g90<byte[]> exchangeSecretKey(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(bArr, "data");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Exchange Secret Key");
        Device device = this.deviceObj;
        if (device != null) {
            l30 l30 = (l30) device.a(l30.class);
            if (l30 != null) {
                g90<byte[]> g = l30.d(bArr).e(new BleAdapterImpl$exchangeSecretKey$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).c(new BleAdapterImpl$exchangeSecretKey$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback));
                if (g != null) {
                    return g;
                }
            }
            log(session, "Device[" + getSerial() + "] does not support ExchangingSecretKeyFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<DeviceInformation> fetchDeviceInfo(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Fetch Device Information");
        Device device = this.deviceObj;
        if (device != null) {
            m30 m30 = (m30) device.a(m30.class);
            if (m30 != null) {
                return m30.c().h(new BleAdapterImpl$fetchDeviceInfo$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$fetchDeviceInfo$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support FetchingDeviceInformationFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public int getBatteryLevel() {
        HashMap<DeviceConfigKey, DeviceConfigItem> hashMap = this.mDeviceConfiguration;
        if (hashMap == null || !hashMap.containsKey(DeviceConfigKey.BATTERY)) {
            return -1;
        }
        DeviceConfigItem deviceConfigItem = hashMap.get(DeviceConfigKey.BATTERY);
        if (deviceConfigItem != null) {
            return ((BatteryConfig) deviceConfigItem).getPercentage();
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BatteryConfig");
    }

    @DexIgnore
    public String getCurrentWatchParamVersion() {
        Device device = this.deviceObj;
        if (device != null) {
            DeviceInformation deviceInformation = device.getDeviceInformation();
            if (deviceInformation != null) {
                WatchParametersFileVersion watchParametersFileVersion = deviceInformation.getWatchParametersFileVersion();
                if (watchParametersFileVersion != null) {
                    Version current = watchParametersFileVersion.getCurrent();
                    if (current != null) {
                        String shortDescription = current.getShortDescription();
                        if (shortDescription != null) {
                            return shortDescription;
                        }
                    }
                }
            }
        }
        return "0.0";
    }

    @DexIgnore
    public final g90<HashMap<DeviceConfigKey, DeviceConfigItem>> getDeviceConfig(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Get Device Configuration");
        Device device = this.deviceObj;
        if (device != null) {
            o30 o30 = (o30) device.a(o30.class);
            if (o30 != null) {
                return o30.j().h(new BleAdapterImpl$getDeviceConfig$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$getDeviceConfig$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support GettingConfigurationsFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public String getDeviceModel() {
        Device device = this.deviceObj;
        if (device != null) {
            DeviceInformation deviceInformation = device.getDeviceInformation();
            if (deviceInformation != null) {
                String modelNumber = deviceInformation.getModelNumber();
                if (modelNumber != null) {
                    return modelNumber;
                }
            }
        }
        return "";
    }

    @DexIgnore
    public final Device getDeviceObj() {
        return this.deviceObj;
    }

    @DexIgnore
    public String getFirmwareVersion() {
        Device device = this.deviceObj;
        if (device != null) {
            DeviceInformation deviceInformation = device.getDeviceInformation();
            if (deviceInformation != null) {
                String firmwareVersion = deviceInformation.getFirmwareVersion();
                if (firmwareVersion != null) {
                    return firmwareVersion;
                }
            }
        }
        return "";
    }

    @DexIgnore
    public int getGattState() {
        return getGattState(this.deviceObj);
    }

    @DexIgnore
    public HeartRateMode getHeartRateMode() {
        HeartRateMode heartRateMode;
        HashMap<DeviceConfigKey, DeviceConfigItem> hashMap = this.mDeviceConfiguration;
        if (hashMap != null) {
            if (hashMap.containsKey(DeviceConfigKey.HEART_RATE_MODE)) {
                HeartRateMode.Companion companion = HeartRateMode.Companion;
                DeviceConfigItem deviceConfigItem = hashMap.get(DeviceConfigKey.HEART_RATE_MODE);
                if (deviceConfigItem != null) {
                    heartRateMode = companion.consume(((HeartRateModeConfig) deviceConfigItem).getHeartRateMode());
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.HeartRateModeConfig");
                }
            } else {
                heartRateMode = HeartRateMode.NONE;
            }
            if (heartRateMode != null) {
                return heartRateMode;
            }
        }
        return HeartRateMode.NONE;
    }

    @DexIgnore
    public int getHidState() {
        return 0;
    }

    @DexIgnore
    public String getLocale() {
        Device device = this.deviceObj;
        if (device != null) {
            DeviceInformation deviceInformation = device.getDeviceInformation();
            if (deviceInformation != null) {
                String localeString = deviceInformation.getLocaleString();
                if (localeString != null) {
                    return localeString;
                }
            }
        }
        return "";
    }

    @DexIgnore
    public String getLocaleVersion() {
        Version version;
        Device device = this.deviceObj;
        if (device != null) {
            DeviceInformation deviceInformation = device.getDeviceInformation();
            if (deviceInformation != null) {
                version = deviceInformation.getLocaleVersion();
                return String.valueOf(version);
            }
        }
        version = null;
        return String.valueOf(version);
    }

    @DexIgnore
    public short getMicroAppMajorVersion() {
        Device device = this.deviceObj;
        if (device != null) {
            DeviceInformation deviceInformation = device.getDeviceInformation();
            if (deviceInformation != null) {
                Version microAppVersion = deviceInformation.getMicroAppVersion();
                if (microAppVersion != null) {
                    return (short) microAppVersion.getMajor();
                }
            }
        }
        return 255;
    }

    @DexIgnore
    public short getMicroAppMinorVersion() {
        Device device = this.deviceObj;
        if (device != null) {
            DeviceInformation deviceInformation = device.getDeviceInformation();
            if (deviceInformation != null) {
                Version microAppVersion = deviceInformation.getMicroAppVersion();
                if (microAppVersion != null) {
                    return (short) microAppVersion.getMinor();
                }
            }
        }
        return 255;
    }

    @DexIgnore
    public final byte[] getSecretKeyThroughSDK(FLogger.Session session) {
        kd4.b(session, "logSession");
        log(session, "Get Secret Key Through SDK");
        Device device = this.deviceObj;
        if (device != null) {
            s30 s30 = (s30) device.a(s30.class);
            if (s30 != null) {
                setTSecretKey(s30.e());
                log(session, "Secret key from SDK " + getTSecretKey());
                return getTSecretKey();
            }
            log(session, "Device[" + getSerial() + "] does not support GettingSecretKeyFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public byte getSupportedWatchParamMajor() {
        Device device = this.deviceObj;
        if (device != null) {
            DeviceInformation deviceInformation = device.getDeviceInformation();
            if (deviceInformation != null) {
                WatchParametersFileVersion watchParametersFileVersion = deviceInformation.getWatchParametersFileVersion();
                if (watchParametersFileVersion != null) {
                    Version supported = watchParametersFileVersion.getSupported();
                    if (supported != null) {
                        return supported.getMajor();
                    }
                }
            }
        }
        return 0;
    }

    @DexIgnore
    public byte getSupportedWatchParamMinor() {
        Device device = this.deviceObj;
        if (device != null) {
            DeviceInformation deviceInformation = device.getDeviceInformation();
            if (deviceInformation != null) {
                WatchParametersFileVersion watchParametersFileVersion = deviceInformation.getWatchParametersFileVersion();
                if (watchParametersFileVersion != null) {
                    Version supported = watchParametersFileVersion.getSupported();
                    if (supported != null) {
                        return supported.getMinor();
                    }
                }
            }
        }
        return 0;
    }

    @DexIgnore
    public String getSupportedWatchParamVersion() {
        Device device = this.deviceObj;
        if (device != null) {
            DeviceInformation deviceInformation = device.getDeviceInformation();
            if (deviceInformation != null) {
                WatchParametersFileVersion watchParametersFileVersion = deviceInformation.getWatchParametersFileVersion();
                if (watchParametersFileVersion != null) {
                    Version supported = watchParametersFileVersion.getSupported();
                    if (supported != null) {
                        String shortDescription = supported.getShortDescription();
                        if (shortDescription != null) {
                            return shortDescription;
                        }
                    }
                }
            }
        }
        return "0.0";
    }

    @DexIgnore
    public byte[] getTSecretKey() {
        return this.tSecretKey;
    }

    @DexIgnore
    public VibrationStrengthObj getVibrationStrength() {
        VibrationStrengthObj vibrationStrengthObj;
        HashMap<DeviceConfigKey, DeviceConfigItem> hashMap = this.mDeviceConfiguration;
        if (hashMap != null) {
            if (hashMap.containsKey(DeviceConfigKey.VIBE_STRENGTH)) {
                DeviceConfigItem deviceConfigItem = hashMap.get(DeviceConfigKey.VIBE_STRENGTH);
                if (deviceConfigItem != null) {
                    VibeStrengthConfig.VibeStrengthLevel vibeStrengthLevel = ((VibeStrengthConfig) deviceConfigItem).getVibeStrengthLevel();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.e(str, "Device config not null, vibeStrengthConfig not null, return vibrationStrength: value = " + vibeStrengthLevel);
                    vibrationStrengthObj = VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel$default(VibrationStrengthObj.Companion, vibeStrengthLevel, false, 2, (Object) null);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.VibeStrengthConfig");
                }
            } else {
                FLogger.INSTANCE.getLocal().e(TAG, "Device config not null but vibeStrengthConfig is null, vibrationStrength return default value MEDIUM");
                vibrationStrengthObj = VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel(VibeStrengthConfig.VibeStrengthLevel.MEDIUM, true);
            }
            if (vibrationStrengthObj != null) {
                return vibrationStrengthObj;
            }
        }
        FLogger.INSTANCE.getLocal().e(TAG, "Device config null, vibrationStrength return default value MEDIUM");
        return VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel(VibeStrengthConfig.VibeStrengthLevel.MEDIUM, true);
    }

    @DexIgnore
    public boolean isDeviceReady() {
        Device device = this.deviceObj;
        boolean z = false;
        if (device != null) {
            if (device.getState() == Device.State.CONNECTED) {
                z = true;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append(".isDeviceReady() - serial=");
            sb.append(getSerial());
            sb.append(", state=");
            sb.append(device.getState());
            sb.append(", ready=");
            sb.append(!z ? "NO" : "YES");
            local.d(str, sb.toString());
            return z;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.e(str2, "Is device ready " + getSerial() + ". FAILED: deviceObject is NULL");
        return false;
    }

    @DexIgnore
    public <T> T isSupportedFeature(Class<T> cls) {
        kd4.b(cls, "feature");
        Device device = this.deviceObj;
        if (device != null) {
            return device.a(cls);
        }
        return null;
    }

    @DexIgnore
    public final void logAppError(FLogger.Session session, String str, ErrorCodeBuilder.Step step, ErrorCodeBuilder.AppError appError) {
        kd4.b(session, "logSession");
        kd4.b(str, "message");
        kd4.b(step, "step");
        kd4.b(appError, "error");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, '[' + getSerial() + "] " + str + ", appError=" + appError);
        error(session, ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, appError), step, str);
    }

    @DexIgnore
    public final void logSdkError(FLogger.Session session, String str, ErrorCodeBuilder.Step step, Error error) {
        kd4.b(session, "logSession");
        kd4.b(str, "message");
        kd4.b(step, "step");
        kd4.b(error, "sdkError");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, '[' + getSerial() + "] " + str + ", sdkError=" + error.getErrorCode());
        error(session, ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, error), step, str);
    }

    @DexIgnore
    public final g90<qa4> notifyMusicEvent(FLogger.Session session, MusicEvent musicEvent) {
        kd4.b(session, "logSession");
        kd4.b(musicEvent, "musicEvent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Notify Music Event, musicEvent=" + musicEvent);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Notify Music Event");
        Device device = this.deviceObj;
        if (device != null) {
            x30 x30 = (x30) device.a(x30.class);
            if (x30 != null) {
                return x30.a(musicEvent);
            }
            log(session, "Device[" + getSerial() + "] does not support NotifyingMusicEventFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<qa4> playDeviceAnimation(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Play Device Animation");
        Device device = this.deviceObj;
        if (device != null) {
            u30 u30 = (u30) device.a(u30.class);
            if (u30 != null) {
                return u30.b().e(new BleAdapterImpl$playDeviceAnimation$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).c(new BleAdapterImpl$playDeviceAnimation$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support HandAnimationFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<WorkoutSession> readCurrentWorkoutSession(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Get Workout Session");
        Device device = this.deviceObj;
        if (device != null) {
            t30 t30 = (t30) device.a(t30.class);
            if (t30 != null) {
                g90<WorkoutSession> g = t30.k().e(new BleAdapterImpl$readCurrentWorkoutSession$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).c(new BleAdapterImpl$readCurrentWorkoutSession$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
                if (g != null) {
                    return g;
                }
            }
            log(session, "Device[" + getSerial() + "] does not support GettingWorkoutSessionFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final f90<FitnessData[]> readDataFile(FLogger.Session session, BiometricProfile biometricProfile, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(biometricProfile, "biometricProfile");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Read DataFiles");
        Device device = this.deviceObj;
        if (device != null) {
            t40 t40 = (t40) device.a(t40.class);
            if (t40 != null) {
                return t40.a(biometricProfile).i(new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon1(this, biometricProfile, session, iSessionSdkCallback)).h(new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon2(this, biometricProfile, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon3(this, biometricProfile, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SynchronizationFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<Integer> readRssi(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Read Rssi");
        Device device = this.deviceObj;
        if (device != null) {
            z30 z30 = (z30) device.a(z30.class);
            if (z30 != null) {
                return z30.g().e(new BleAdapterImpl$readRssi$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).c(new BleAdapterImpl$readRssi$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support ReadingRSSIFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void registerBluetoothStateCallback(Device.a aVar) {
        kd4.b(aVar, Constants.CALLBACK);
        this.mDeviceStateListener = aVar;
        Device device = this.deviceObj;
        if (device != null) {
            device.a(this.mDeviceStateListener);
        }
    }

    @DexIgnore
    public final void sendCustomCommand(CustomRequest customRequest) {
        kd4.b(customRequest, Constants.COMMAND);
        FLogger.Session session = FLogger.Session.OTHER;
        log(session, "Send Custom Command: " + customRequest);
        Device device = this.deviceObj;
        if (!(device instanceof h30)) {
            device = null;
        }
        h30 h30 = (h30) device;
        if (h30 != null) {
            g90<qa4> a = h30.a(customRequest.getCustomCommand(), Channel.DC);
            if (a != null) {
                g90<qa4> h = a.e(new BleAdapterImpl$sendCustomCommand$Anon1(this, customRequest));
                if (!(h == null || h.c(new BleAdapterImpl$sendCustomCommand$Anon2(this)) == null)) {
                    return;
                }
            }
        }
        log(FLogger.Session.OTHER, "Send Custom Command not support");
        qa4 qa4 = qa4.a;
    }

    @DexIgnore
    public final g90<qa4> sendNotification(FLogger.Session session, NotificationBaseObj notificationBaseObj) {
        kd4.b(session, "logSession");
        kd4.b(notificationBaseObj, "notification");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Notification, notification=" + notificationBaseObj);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Notification");
        Device device = this.deviceObj;
        if (device != null) {
            c40 c40 = (c40) device.a(c40.class);
            if (c40 != null) {
                return c40.a(notificationBaseObj.toSDKNotification());
            }
            log(session, "Device[" + getSerial() + "] does not support SendingAppNotificationFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<qa4> sendRespond(FLogger.Session session, DeviceData deviceData) {
        kd4.b(session, "logSession");
        kd4.b(deviceData, "deviceData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Respond, deviceData=" + deviceData);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Respond");
        Device device = this.deviceObj;
        if (device != null) {
            d40 d40 = (d40) device.a(d40.class);
            if (d40 != null) {
                return d40.a(deviceData);
            }
            log(session, "Device[" + getSerial() + "] does not support SendingDataFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<qa4> sendTrackInfo(FLogger.Session session, TrackInfo trackInfo) {
        kd4.b(session, "logSession");
        kd4.b(trackInfo, "trackInfo");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Track Info, trackInfo=" + trackInfo);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Track Info");
        Device device = this.deviceObj;
        if (device != null) {
            e40 e40 = (e40) device.a(e40.class);
            if (e40 != null) {
                return e40.a(trackInfo);
            }
            log(session, "Device[" + getSerial() + "] does not support SendingTrackInfoFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<qa4> setAlarms(FLogger.Session session, List<AlarmSetting> list, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(list, "alarms");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set List Alarms: " + list);
        Device device = this.deviceObj;
        if (device != null) {
            f40 f40 = (f40) device.a(f40.class);
            if (f40 != null) {
                return f40.a(AlarmExtensionKt.toSDKV2Setting(list)).h(new BleAdapterImpl$setAlarms$$inlined$let$lambda$Anon1(this, list, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$setAlarms$$inlined$let$lambda$Anon2(this, list, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingAlarmFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<qa4> setBackgroundImage(FLogger.Session session, BackgroundConfig backgroundConfig, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(backgroundConfig, "backgroundConfig");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Background Image: " + backgroundConfig);
        Device device = this.deviceObj;
        if (device != null) {
            g40 g40 = (g40) device.a(g40.class);
            if (g40 != null) {
                return g40.a(backgroundConfig.toSDKBackgroundImageConfig()).h(new BleAdapterImpl$setBackgroundImage$$inlined$let$lambda$Anon1(this, backgroundConfig, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$setBackgroundImage$$inlined$let$lambda$Anon2(this, backgroundConfig, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingBackgroundImageFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<qa4> setComplications(FLogger.Session session, ComplicationAppMappingSettings complicationAppMappingSettings, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Complication: " + complicationAppMappingSettings);
        Device device = this.deviceObj;
        if (device != null) {
            i40 i40 = (i40) device.a(i40.class);
            if (i40 != null) {
                return i40.a(complicationAppMappingSettings.toSDKSetting()).h(new BleAdapterImpl$setComplications$$inlined$let$lambda$Anon1(this, complicationAppMappingSettings, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$setComplications$$inlined$let$lambda$Anon2(this, complicationAppMappingSettings, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingComplicationFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final boolean setDevice(Device device) {
        kd4.b(device, "deviceObj");
        setDeviceObj(device);
        return true;
    }

    @DexIgnore
    public final g90<DeviceConfigKey[]> setDeviceConfig(FLogger.Session session, DeviceConfigItem[] deviceConfigItemArr, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(deviceConfigItemArr, "deviceConfigItems");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Device Config: " + new Gson().a((Object) deviceConfigItemArr));
        Device device = this.deviceObj;
        if (device != null) {
            j40 j40 = (j40) device.a(j40.class);
            if (j40 != null) {
                return j40.a(deviceConfigItemArr).h(new BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon1(this, deviceConfigItemArr, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon2(this, deviceConfigItemArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingConfigurationsFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void setDeviceObj(Device device) {
        this.deviceObj = device;
        if (device != null) {
            if (!TextUtils.isEmpty(device.getDeviceInformation().getMacAddress())) {
                setMacAddress(device.getDeviceInformation().getMacAddress());
            }
            device.a(this.mDeviceStateListener);
        }
    }

    @DexIgnore
    public final g90<qa4> setFrontLightEnable(FLogger.Session session, boolean z, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Front Light Enable: " + z);
        Device device = this.deviceObj;
        if (device != null) {
            k40 k40 = (k40) device.a(k40.class);
            if (k40 != null) {
                return k40.a(z).h(new BleAdapterImpl$setFrontLightEnable$$inlined$let$lambda$Anon1(this, z, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$setFrontLightEnable$$inlined$let$lambda$Anon2(this, z, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingFrontLightFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final f90<String> setLocalizationData(LocalizationData localizationData, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(localizationData, "localizationData");
        kd4.b(session, "logSession");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Localization");
        Device device = this.deviceObj;
        if (device != null) {
            l40 l40 = (l40) device.a(l40.class);
            if (l40 != null) {
                return l40.a(localizationData.toLocalizationFile()).h(new BleAdapterImpl$setLocalizationData$$inlined$let$lambda$Anon1(this, localizationData, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$setLocalizationData$$inlined$let$lambda$Anon2(this, localizationData, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingLocalizationFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final f90<qa4> setNotificationFilters(FLogger.Session session, List<AppNotificationFilter> list, ISessionSdkCallback iSessionSdkCallback) {
        NotificationFilter notificationFilter;
        kd4.b(session, "logSession");
        kd4.b(list, "notificationFilters");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (T next : list) {
            Long valueOf = Long.valueOf(((AppNotificationFilter) next).getBundleCrc());
            Object obj = linkedHashMap.get(valueOf);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap.put(valueOf, obj);
            }
            ((List) obj).add(next);
        }
        ArrayList arrayList = new ArrayList();
        for (Map.Entry value : linkedHashMap.entrySet()) {
            Iterable<AppNotificationFilter> iterable = (Iterable) value.getValue();
            ArrayList arrayList2 = new ArrayList(db4.a(iterable, 10));
            boolean z = false;
            for (AppNotificationFilter appNotificationFilter : iterable) {
                if (appNotificationFilter.getHandMovingConfig() == null) {
                    notificationFilter = appNotificationFilter.toSDKNotificationFilter(getContext(), !z);
                    if (!z) {
                        z = notificationFilter.getIconConfig() != null;
                    }
                } else {
                    notificationFilter = appNotificationFilter.toSDKNotificationFilter(getContext(), false);
                }
                arrayList2.add(notificationFilter);
            }
            hb4.a(arrayList, arrayList2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Set Notification Filter: " + list);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Set Notification Filter");
        Device device = this.deviceObj;
        if (device != null) {
            m40 m40 = (m40) device.a(m40.class);
            if (m40 != null) {
                Object[] array = arrayList.toArray(new NotificationFilter[0]);
                if (array != null) {
                    return m40.a((NotificationFilter[]) array).i(new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon1(this, arrayList, session, iSessionSdkCallback)).h(new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon2(this, arrayList, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon3(this, arrayList, session, iSessionSdkCallback));
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
            log(session, "Device[" + getSerial() + "] does not support SettingLocalizationFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void setSecretKey(FLogger.Session session, byte[] bArr) {
        kd4.b(session, "logSession");
        kd4.b(bArr, "secretKey");
        log(session, "Set Secret Key");
        Device device = this.deviceObj;
        if (device != null) {
            o40 o40 = (o40) device.a(o40.class);
            if (o40 != null) {
                o40.e(bArr);
                setTSecretKey(bArr);
                return;
            }
            log(session, "Device[" + getSerial() + "] does not support GettingSecretKeyFeature");
            return;
        }
        FLogger.INSTANCE.getLocal().e(TAG, "Set Secret Key. FAILED: deviceObject is NULL");
    }

    @DexIgnore
    public void setTSecretKey(byte[] bArr) {
        this.tSecretKey = bArr;
    }

    @DexIgnore
    public final g90<qa4> setWatchApps(FLogger.Session session, WatchAppMappingSettings watchAppMappingSettings, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(watchAppMappingSettings, "watchAppMappingSettings");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Watch Apps: " + watchAppMappingSettings);
        Device device = this.deviceObj;
        if (device != null) {
            p40 p40 = (p40) device.a(p40.class);
            if (p40 != null) {
                return p40.a(watchAppMappingSettings.toSDKSetting()).h(new BleAdapterImpl$setWatchApps$$inlined$let$lambda$Anon1(this, watchAppMappingSettings, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$setWatchApps$$inlined$let$lambda$Anon2(this, watchAppMappingSettings, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingWatchAppFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<qa4> setWatchParams(FLogger.Session session, WatchParamsFileMapping watchParamsFileMapping, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(watchParamsFileMapping, "watchParamFileMapping");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set WatchParams: " + watchParamsFileMapping);
        Device device = this.deviceObj;
        if (device != null) {
            q40 q40 = (q40) device.a(q40.class);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "Setting watchParam with file value = " + watchParamsFileMapping.toSDKSetting());
            if (q40 != null) {
                return q40.a(watchParamsFileMapping.toSDKSetting()).h(new BleAdapterImpl$setWatchParams$$inlined$let$lambda$Anon1(this, watchParamsFileMapping, session, iSessionSdkCallback)).g((xc4<? super Error, qa4>) new BleAdapterImpl$setWatchParams$$inlined$let$lambda$Anon2(this, watchParamsFileMapping, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingWatchParameterFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final g90<byte[]> startAuthenticate(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(bArr, "phoneRandomNumber");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Start Authenticate phoneRandomNumber " + ConversionUtils.byteArrayToString(bArr));
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Start Authenticate phoneRandomNumber");
        Device device = this.deviceObj;
        if (device != null) {
            r40 r40 = (r40) device.a(r40.class);
            if (r40 != null) {
                return r40.a(bArr).e(new BleAdapterImpl$startAuthenticate$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).c(new BleAdapterImpl$startAuthenticate$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support StartingAuthenticateFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void startScanning(FLogger.Session session, long j, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Start scanning...");
        this.isScanning = true;
        this.scanService = new ScanService(getContext(), new BleScanServiceCallback(iSessionSdkCallback), j);
        ScanService scanService2 = this.scanService;
        if (scanService2 != null) {
            scanService2.setActiveDeviceLog(getSerial());
            ScanService scanService3 = this.scanService;
            if (scanService3 != null) {
                scanService3.startScanWithAutoStopTimer();
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final g90<qa4> stopCurrentWorkoutSession(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Stop Current Workout Session");
        Device device = this.deviceObj;
        if (device != null) {
            s40 s40 = (s40) device.a(s40.class);
            if (s40 != null) {
                return s40.d().e(new BleAdapterImpl$stopCurrentWorkoutSession$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).c(new BleAdapterImpl$stopCurrentWorkoutSession$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support StoppingWorkoutSessionFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void stopScanning(FLogger.Session session) {
        kd4.b(session, "logSession");
        ScanService scanService2 = this.scanService;
        if (scanService2 != null) {
            scanService2.stopScan();
        }
        if (this.isScanning) {
            log(session, "Stop scanning for " + getSerial());
        }
        this.isScanning = false;
    }

    @DexIgnore
    public final g90<Boolean> verifySecretKey(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        kd4.b(session, "logSession");
        kd4.b(bArr, "secretKey");
        kd4.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Verify Secret Key");
        Device device = this.deviceObj;
        if (device != null) {
            u40 u40 = (u40) device.a(u40.class);
            if (u40 != null) {
                g90<Boolean> g = u40.c(bArr).e(new BleAdapterImpl$verifySecretKey$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).c(new BleAdapterImpl$verifySecretKey$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback));
                if (g != null) {
                    return g;
                }
            }
            log(session, "Device[" + getSerial() + "] does not support VerifyingSecretKeyFeature");
            return null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    private final int getGattState(Device device) {
        if (device != null) {
            int i = WhenMappings.$EnumSwitchMapping$Anon0[device.getState().ordinal()];
            if (i == 1) {
                return 0;
            }
            if (i == 2 || i == 3) {
                return 2;
            }
            if (i != 4) {
                return i != 5 ? 0 : 3;
            }
            return 1;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.e(str, "Inside " + TAG + ".getGattState - serial=" + getSerial() + ", shineDevice=NULL");
        return 0;
    }
}
