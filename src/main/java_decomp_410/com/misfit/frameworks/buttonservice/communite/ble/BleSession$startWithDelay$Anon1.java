package com.misfit.frameworks.buttonservice.communite.ble;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleSession$startWithDelay$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Object[] $params;
    @DexIgnore
    public /* final */ /* synthetic */ BleSession this$Anon0;

    @DexIgnore
    public BleSession$startWithDelay$Anon1(BleSession bleSession, Object[] objArr) {
        this.this$Anon0 = bleSession;
        this.$params = objArr;
    }

    @DexIgnore
    public final void run() {
        BleSession bleSession = this.this$Anon0;
        Object[] objArr = this.$params;
        bleSession.start(Arrays.copyOf(objArr, objArr.length));
    }
}
