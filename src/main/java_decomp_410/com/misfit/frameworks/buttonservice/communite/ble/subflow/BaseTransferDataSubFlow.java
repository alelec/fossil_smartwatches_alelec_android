package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import android.os.Bundle;
import com.fossil.blesdk.device.data.config.DailyActiveMinuteGoalConfig;
import com.fossil.blesdk.device.data.config.DailyCalorieConfig;
import com.fossil.blesdk.device.data.config.DailyCalorieGoalConfig;
import com.fossil.blesdk.device.data.config.DailyDistanceConfig;
import com.fossil.blesdk.device.data.config.DailySleepConfig;
import com.fossil.blesdk.device.data.config.DailyStepConfig;
import com.fossil.blesdk.device.data.config.DailyStepGoalConfig;
import com.fossil.blesdk.device.data.config.DailyTotalActiveMinuteConfig;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.f90;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.hb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.fitness.FitnessData;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.db.DataCollectorHelper;
import com.misfit.frameworks.buttonservice.db.DataFile;
import com.misfit.frameworks.buttonservice.db.DataFileProvider;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFSyncLog;
import com.misfit.frameworks.buttonservice.log.model.SessionDetailInfo;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class BaseTransferDataSubFlow extends SubFlow {
    @DexIgnore
    public int activeMinute;
    @DexIgnore
    public int activeMinuteGoal;
    @DexIgnore
    public int awakeMin;
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public long calorie;
    @DexIgnore
    public long calorieGoal;
    @DexIgnore
    public long dailyStepInfo;
    @DexIgnore
    public int deepMin;
    @DexIgnore
    public long distanceInCentimeter;
    @DexIgnore
    public /* final */ boolean isClearDataOptional;
    @DexIgnore
    public boolean isNewDevice;
    @DexIgnore
    public int lightMin;
    @DexIgnore
    public List<FitnessData> mSyncData;
    @DexIgnore
    public /* final */ MFLog mflog;
    @DexIgnore
    public long realTimeSteps;
    @DexIgnore
    public long realTimeStepsInfo;
    @DexIgnore
    public long stepGoal;
    @DexIgnore
    public MFSyncLog syncLog;
    @DexIgnore
    public long syncTime;
    @DexIgnore
    public int totalSleepInMin;
    @DexIgnore
    public /* final */ UserProfile userProfile;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class EraseDataFileState extends BleStateAbs {
        @DexIgnore
        public g90<qa4> task;

        @DexIgnore
        public EraseDataFileState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = BaseTransferDataSubFlow.this.getBleAdapter().eraseDataFile(BaseTransferDataSubFlow.this.getLogSession(), this);
            if (this.task == null) {
                BaseTransferDataSubFlow.this.stopSubFlow(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onEraseDataFilesFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            if (BaseTransferDataSubFlow.this.isClearDataOptional()) {
                BaseTransferDataSubFlow.this.addFailureCode(FailureCode.FAILED_TO_CLEAR_DATA);
                BaseTransferDataSubFlow.this.stopSubFlow(0);
                return;
            }
            BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_CLEAR_DATA);
        }

        @DexIgnore
        public void onEraseDataFilesSuccess() {
            stopTimeout();
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Erase DataFiles timeout. Cancel.");
            g90<qa4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public g90<HashMap<DeviceConfigKey, DeviceConfigItem>> task;

        @DexIgnore
        public GetDeviceConfigState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        private final void logConfiguration(HashMap<DeviceConfigKey, DeviceConfigItem> hashMap) {
            throw null;
            // BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            // pd4 pd4 = pd4.a;
            // Object[] objArr = {hashMap.get(DeviceConfigKey.TIME), hashMap.get(DeviceConfigKey.BATTERY), hashMap.get(DeviceConfigKey.BIOMETRIC_PROFILE), hashMap.get(DeviceConfigKey.DAILY_STEP), hashMap.get(DeviceConfigKey.DAILY_STEP_GOAL), hashMap.get(DeviceConfigKey.DAILY_CALORIE), hashMap.get(DeviceConfigKey.DAILY_CALORIE_GOAL), hashMap.get(DeviceConfigKey.DAILY_TOTAL_ACTIVE_MINUTE), hashMap.get(DeviceConfigKey.DAILY_ACTIVE_MINUTE_GOAL), hashMap.get(DeviceConfigKey.DAILY_DISTANCE), hashMap.get(DeviceConfigKey.DAILY_SLEEP), hashMap.get(DeviceConfigKey.DAILY_DISTANCE), hashMap.get(DeviceConfigKey.DAILY_DISTANCE), hashMap.get(DeviceConfigKey.INACTIVE_NUDGE), hashMap.get(DeviceConfigKey.VIBE_STRENGTH), hashMap.get(DeviceConfigKey.DO_NOT_DISTURB_SCHEDULE)};
            // String format = String.format("Get configuration  " + BaseTransferDataSubFlow.this.getSerial() + "\n" + "\t[timeConfiguration: \n" + "\ttime = %s,\n" + "\tbattery = %s,\n" + "\tbiometric = %s,\n" + "\tdaily steps = %s,\n" + "\tdaily step goal = %s, \n" + "\tdaily calorie: %s, \n" + "\tdaily calorie goal: %s, \n" + "\tdaily total active minute: %s, \n" + "\tdaily active minute goal: %s, \n" + "\tdaily distance: %s, \n" + "\tdaily sleep: %s, \n" + "\tinactive nudge: %s, \n" + "\tvibration strength: %s, \n" + "\tdo not disturb schedule: %s, \n" + "\t]", Arrays.copyOf(objArr, objArr.length));
            // kd4.a((Object) format, "java.lang.String.format(format, *args)");
            // baseTransferDataSubFlow.log(format);
        }

        @DexIgnore
        private final void readConfig(HashMap<DeviceConfigKey, DeviceConfigItem> hashMap) {
            if (!BaseTransferDataSubFlow.this.getUserProfile().isNewDevice()) {
                DeviceConfigItem deviceConfigItem = hashMap.get(DeviceConfigKey.DAILY_STEP_GOAL);
                if (deviceConfigItem != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                    if (deviceConfigItem != null) {
                        baseTransferDataSubFlow.stepGoal = ((DailyStepGoalConfig) deviceConfigItem).getStep();
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepGoalConfig");
                    }
                }
                DeviceConfigItem deviceConfigItem2 = hashMap.get(DeviceConfigKey.DAILY_STEP);
                if (deviceConfigItem2 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                    if (deviceConfigItem2 != null) {
                        baseTransferDataSubFlow2.realTimeSteps = ((DailyStepConfig) deviceConfigItem2).getStep();
                        BaseTransferDataSubFlow baseTransferDataSubFlow3 = BaseTransferDataSubFlow.this;
                        baseTransferDataSubFlow3.realTimeStepsInfo = baseTransferDataSubFlow3.realTimeSteps;
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepConfig");
                    }
                }
                DeviceConfigItem deviceConfigItem3 = hashMap.get(DeviceConfigKey.DAILY_ACTIVE_MINUTE_GOAL);
                if (deviceConfigItem3 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow4 = BaseTransferDataSubFlow.this;
                    if (deviceConfigItem3 != null) {
                        baseTransferDataSubFlow4.activeMinuteGoal = ((DailyActiveMinuteGoalConfig) deviceConfigItem3).getMinute();
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyActiveMinuteGoalConfig");
                    }
                }
                DeviceConfigItem deviceConfigItem4 = hashMap.get(DeviceConfigKey.DAILY_TOTAL_ACTIVE_MINUTE);
                if (deviceConfigItem4 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow5 = BaseTransferDataSubFlow.this;
                    if (deviceConfigItem4 != null) {
                        baseTransferDataSubFlow5.activeMinute = ((DailyTotalActiveMinuteConfig) deviceConfigItem4).getMinute();
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyTotalActiveMinuteConfig");
                    }
                }
                DeviceConfigItem deviceConfigItem5 = hashMap.get(DeviceConfigKey.DAILY_CALORIE_GOAL);
                if (deviceConfigItem5 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow6 = BaseTransferDataSubFlow.this;
                    if (deviceConfigItem5 != null) {
                        baseTransferDataSubFlow6.calorieGoal = ((DailyCalorieGoalConfig) deviceConfigItem5).getCalorie();
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyCalorieGoalConfig");
                    }
                }
                DeviceConfigItem deviceConfigItem6 = hashMap.get(DeviceConfigKey.DAILY_CALORIE);
                if (deviceConfigItem6 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow7 = BaseTransferDataSubFlow.this;
                    if (deviceConfigItem6 != null) {
                        baseTransferDataSubFlow7.calorie = ((DailyCalorieConfig) deviceConfigItem6).getCalorie();
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyCalorieConfig");
                    }
                }
                DeviceConfigItem deviceConfigItem7 = hashMap.get(DeviceConfigKey.DAILY_DISTANCE);
                if (deviceConfigItem7 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow8 = BaseTransferDataSubFlow.this;
                    if (deviceConfigItem7 != null) {
                        baseTransferDataSubFlow8.distanceInCentimeter = ((DailyDistanceConfig) deviceConfigItem7).getCentimeter();
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyDistanceConfig");
                    }
                }
                DeviceConfigItem deviceConfigItem8 = hashMap.get(DeviceConfigKey.DAILY_SLEEP);
                if (deviceConfigItem8 != null) {
                    if (!(deviceConfigItem8 instanceof DailySleepConfig)) {
                        deviceConfigItem8 = null;
                    }
                    DailySleepConfig dailySleepConfig = (DailySleepConfig) deviceConfigItem8;
                    if (dailySleepConfig != null) {
                        BaseTransferDataSubFlow.this.totalSleepInMin = dailySleepConfig.getTotalSleepInMinute();
                        BaseTransferDataSubFlow.this.awakeMin = dailySleepConfig.getAwakeInMinute();
                        BaseTransferDataSubFlow.this.lightMin = dailySleepConfig.getLightSleepInMinute();
                        BaseTransferDataSubFlow.this.deepMin = dailySleepConfig.getDeepSleepInMinute();
                        return;
                    }
                    return;
                }
                return;
            }
            BaseTransferDataSubFlow.this.log("We skip read real time steps cause by full sync");
        }

        @DexIgnore
        public boolean onEnter() {
            this.task = BaseTransferDataSubFlow.this.getBleAdapter().getDeviceConfig(BaseTransferDataSubFlow.this.getLogSession(), this);
            if (this.task != null) {
                startTimeout();
                return true;
            } else if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return true;
            } else {
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
                return true;
            }
        }

        @DexIgnore
        public void onGetDeviceConfigFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            BaseTransferDataSubFlow.this.addFailureCode(FailureCode.FAILED_TO_GET_CONFIG);
            if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return;
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
        }

        @DexIgnore
        public void onGetDeviceConfigSuccess(HashMap<DeviceConfigKey, DeviceConfigItem> hashMap) {
            kd4.b(hashMap, "deviceConfiguration");
            stopTimeout();
            logConfiguration(hashMap);
            readConfig(hashMap);
            if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return;
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Get device configuration timeout. Cancel.");
            g90<HashMap<DeviceConfigKey, DeviceConfigItem>> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class PlayDeviceAnimationState extends BleStateAbs {
        @DexIgnore
        public g90<qa4> task;

        @DexIgnore
        public PlayDeviceAnimationState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = BaseTransferDataSubFlow.this.getBleAdapter().playDeviceAnimation(BaseTransferDataSubFlow.this.getLogSession(), this);
            if (this.task == null) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.GET_DEVICE_CONFIG_STATE));
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onPlayDeviceAnimation(boolean z, h90 h90) {
            stopTimeout();
            if (!z) {
                BaseTransferDataSubFlow.this.addFailureCode(201);
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.GET_DEVICE_CONFIG_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Play device animation timeout. Cancel.");
            g90<qa4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ProcessAndStoreDataState extends BleStateAbs {
        @DexIgnore
        public ProcessAndStoreDataState() {
            super(BaseTransferDataSubFlow.this.getTAG());
            setTimeout(CommuteTimeService.y);
            BaseTransferDataSubFlow.this.log("Process and store data of device with serial " + BaseTransferDataSubFlow.this.getSerial());
        }

        @DexIgnore
        private final Bundle bundleData() {
            Bundle bundle = new Bundle();
            bundle.putLong(Constants.SYNC_RESULT, BaseTransferDataSubFlow.this.getSyncTime());
            bundle.putLong(ButtonService.Companion.getREALTIME_STEPS(), BaseTransferDataSubFlow.this.realTimeSteps);
            return bundle;
        }

        @DexIgnore
        private final List<FitnessData> readDataFromCache() {
            ArrayList arrayList = new ArrayList();
            List<DataFile> allDataFiles = DataFileProvider.getInstance(BaseTransferDataSubFlow.this.getBleAdapter().getContext()).getAllDataFiles(BaseTransferDataSubFlow.this.getSyncTime(), BaseTransferDataSubFlow.this.getSerial());
            Gson gson = new Gson();
            if (allDataFiles.size() > 0) {
                kd4.a((Object) allDataFiles, "cacheData");
                long j = -1;
                int i = 0;
                for (DataFile dataFile : allDataFiles) {
                    try {
                        kd4.a((Object) dataFile, "it");
                        FitnessData fitnessData = (FitnessData) gson.a(dataFile.getDataFile(), FitnessData.class);
                        kd4.a((Object) fitnessData, "fitnessData");
                        arrayList.add(fitnessData);
                        if (dataFile.getSyncTime() != j) {
                            if (j != -1) {
                                BaseTransferDataSubFlow.this.log("Got cache data of " + j + ", fitness data size=" + i);
                            }
                            j = dataFile.getSyncTime();
                            i = 0;
                        }
                        i++;
                    } catch (Exception e) {
                        BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Got cache data of ");
                        kd4.a((Object) dataFile, "it");
                        sb.append(dataFile.getSyncTime());
                        sb.append(" exception=");
                        sb.append(e);
                        baseTransferDataSubFlow.log(sb.toString());
                    }
                }
                if (j != -1 && i > 0) {
                    BaseTransferDataSubFlow.this.log("Got cache data of " + j + ", fitness data size=" + i);
                }
            } else {
                BaseTransferDataSubFlow.this.log("No cache data.");
            }
            return arrayList;
        }

        @DexIgnore
        private final void saveAsCache(List<FitnessData> list) {
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("Save latest sync data as cache, sync time=" + BaseTransferDataSubFlow.this.getSyncTime() + ", fitness data size=" + list.size());
            int i = 0;
            for (FitnessData next : list) {
                int i2 = i + 1;
                if (i >= 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(BaseTransferDataSubFlow.this.getSyncTime());
                    sb.append('_');
                    sb.append(i);
                    DataCollectorHelper.saveDataFile(BaseTransferDataSubFlow.this.getBleAdapter().getContext(), new DataFile(sb.toString(), new Gson().a((Object) (FitnessData) next), BaseTransferDataSubFlow.this.getSerial(), BaseTransferDataSubFlow.this.getSyncTime()));
                    i = i2;
                } else {
                    cb4.c();
                    throw null;
                }
            }
        }

        @DexIgnore
        public boolean onEnter() {
            FLogger.INSTANCE.getLocal().d(getTAG(), "onEnter ProcessAndStoreDataState");
            List<FitnessData> readDataFromCache = readDataFromCache();
            saveAsCache(BaseTransferDataSubFlow.this.mSyncData);
            readDataFromCache.addAll(BaseTransferDataSubFlow.this.mSyncData);
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("set syncdata from cache data with size " + readDataFromCache.size());
            BaseTransferDataSubFlow.this.mSyncData = readDataFromCache;
            startTimeout();
            BleSession.BleSessionCallback bleSessionCallback = BaseTransferDataSubFlow.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                bleSessionCallback.onReceivedSyncData(bundleData());
                return true;
            }
            kd4.a();
            throw null;
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Process and Store data timeout.");
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        public final void updateCurrentStepsAndStepGoal(boolean z, UserProfile userProfile) {
            kd4.b(userProfile, "newUserProfile");
            if (z) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.log("Set percentage goal progress session " + BaseTransferDataSubFlow.this.getSerial() + ", keep setting in device");
            } else {
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.log("Set percentage goal progress session " + BaseTransferDataSubFlow.this.getSerial() + ", " + "{" + "currentSteps=" + userProfile.getCurrentSteps() + ", stepGoal=" + userProfile.getGoalSteps() + ',' + "activeMinute=" + userProfile.getActiveMinute() + ", activeMinuteGoal=" + userProfile.getActiveMinuteGoal() + ',' + "calories=" + userProfile.getCalories() + ", caloriesGoal=" + userProfile.getCaloriesGoal() + ',' + "distanceInCentimeter=" + userProfile.getDistanceInCentimeter() + "totalSleepMin=" + userProfile.getTotalSleepInMinute() + "awakeMin=" + userProfile.getAwakeInMinute() + "lightMin=" + userProfile.getLightSleepInMinute() + "deepMin=" + userProfile.getDeepSleepInMinute() + "}");
            }
            stopTimeout();
            if (!z) {
                BaseTransferDataSubFlow.this.getUserProfile().setCurrentSteps(userProfile.getCurrentSteps());
                BaseTransferDataSubFlow.this.getUserProfile().setGoalSteps(userProfile.getGoalSteps());
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinute(userProfile.getActiveMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinuteGoal(userProfile.getActiveMinuteGoal());
                BaseTransferDataSubFlow.this.getUserProfile().setCalories(userProfile.getCalories());
                BaseTransferDataSubFlow.this.getUserProfile().setCaloriesGoal(userProfile.getCaloriesGoal());
                BaseTransferDataSubFlow.this.getUserProfile().setDistanceInCentimeter(userProfile.getDistanceInCentimeter());
                BaseTransferDataSubFlow.this.getUserProfile().setTotalSleepInMinute(userProfile.getTotalSleepInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setAwakeInMinute(userProfile.getAwakeInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setLightSleepInMinute(userProfile.getLightSleepInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setDeepSleepInMinute(userProfile.getDeepSleepInMinute());
            } else {
                BaseTransferDataSubFlow.this.getUserProfile().setCurrentSteps(BaseTransferDataSubFlow.this.realTimeSteps);
                BaseTransferDataSubFlow.this.getUserProfile().setGoalSteps(BaseTransferDataSubFlow.this.stepGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinute(BaseTransferDataSubFlow.this.activeMinute);
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinuteGoal(BaseTransferDataSubFlow.this.activeMinuteGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setCalories(BaseTransferDataSubFlow.this.calorie);
                BaseTransferDataSubFlow.this.getUserProfile().setCaloriesGoal(BaseTransferDataSubFlow.this.calorieGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setDistanceInCentimeter(BaseTransferDataSubFlow.this.distanceInCentimeter);
                BaseTransferDataSubFlow.this.getUserProfile().setTotalSleepInMinute(BaseTransferDataSubFlow.this.totalSleepInMin);
                BaseTransferDataSubFlow.this.getUserProfile().setAwakeInMinute(BaseTransferDataSubFlow.this.awakeMin);
                BaseTransferDataSubFlow.this.getUserProfile().setLightSleepInMinute(BaseTransferDataSubFlow.this.lightMin);
                BaseTransferDataSubFlow.this.getUserProfile().setDeepSleepInMinute(BaseTransferDataSubFlow.this.deepMin);
            }
            FLogger.INSTANCE.updateSessionDetailInfo(new SessionDetailInfo(BaseTransferDataSubFlow.this.getBleAdapter().getBatteryLevel(), (int) BaseTransferDataSubFlow.this.realTimeStepsInfo, (int) BaseTransferDataSubFlow.this.dailyStepInfo));
            MFSyncLog syncLog = BaseTransferDataSubFlow.this.getSyncLog();
            if (syncLog != null) {
                syncLog.setRealTimeStep(BaseTransferDataSubFlow.this.getUserProfile().getCurrentSteps());
            }
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadDataFileState extends BleStateAbs {
        @DexIgnore
        public f90<FitnessData[]> task;

        @DexIgnore
        public ReadDataFileState() {
            super(BaseTransferDataSubFlow.this.getTAG());
            setMaxRetries(3);
        }

        @DexIgnore
        public void cancelCurrentBleTask() {
            super.cancelCurrentBleTask();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "cancelCurrentBleTask is " + this.task);
            f90<FitnessData[]> f90 = this.task;
            if (f90 != null) {
                f90.e();
            }
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            try {
                this.task = BaseTransferDataSubFlow.this.getBleAdapter().readDataFile(BaseTransferDataSubFlow.this.getLogSession(), BaseTransferDataSubFlow.this.getUserProfile().getUserBiometricData().toSDKBiometricProfile(), this);
                if (this.task == null) {
                    BaseTransferDataSubFlow.this.stopSubFlow(10000);
                    return true;
                }
                startTimeout();
                return true;
            } catch (Exception e) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.log("Read Data Files:  ex=" + e.getMessage());
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.errorLog("ReadDataFileState: catchException=" + e.getMessage(), FLogger.Component.BLE, ErrorCodeBuilder.Step.UNKNOWN, ErrorCodeBuilder.AppError.UNKNOWN);
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SYNC);
                return true;
            }
        }

        @DexIgnore
        public void onReadDataFilesFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            if (!retry(BaseTransferDataSubFlow.this.getBleAdapter().getContext(), BaseTransferDataSubFlow.this.getSerial())) {
                BaseTransferDataSubFlow.this.log("Reach the limit retry. Stop.");
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SYNC);
            }
        }

        @DexIgnore
        public void onReadDataFilesProgressChanged(float f) {
            setTimeout(30000);
            startTimeout();
        }

        @DexIgnore
        public void onReadDataFilesSuccess(FitnessData[] fitnessDataArr) {
            kd4.b(fitnessDataArr, "data");
            stopTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, ".onReadDataFilesSuccess(), sdk = " + new Gson().a((Object) fitnessDataArr));
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("data size from SDK " + fitnessDataArr.length);
            hb4.a(BaseTransferDataSubFlow.this.mSyncData, fitnessDataArr);
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.PROCESS_AND_STORE_DATA_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Read DataFiles timeout. Cancel.");
            f90<FitnessData[]> f90 = this.task;
            if (f90 != null) {
                f90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseTransferDataSubFlow(String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl, UserProfile userProfile2, BleSession.BleSessionCallback bleSessionCallback2, boolean z) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        kd4.b(str, "tagName");
        kd4.b(bleSession, "bleSession");
        kd4.b(session, "logSession");
        kd4.b(str2, "serial");
        kd4.b(bleAdapterImpl, "bleAdapter");
        kd4.b(userProfile2, "userProfile");
        this.mflog = mFLog;
        this.userProfile = userProfile2;
        this.bleSessionCallback = bleSessionCallback2;
        this.isClearDataOptional = z;
        MFLog mFLog2 = this.mflog;
        this.syncLog = (MFSyncLog) (!(mFLog2 instanceof MFSyncLog) ? null : mFLog2);
        this.realTimeSteps = this.userProfile.getCurrentSteps();
        this.stepGoal = this.userProfile.getGoalSteps();
        this.activeMinute = this.userProfile.getActiveMinute();
        this.activeMinuteGoal = this.userProfile.getActiveMinuteGoal();
        this.calorie = this.userProfile.getCalories();
        this.calorieGoal = this.userProfile.getCaloriesGoal();
        this.distanceInCentimeter = this.userProfile.getDistanceInCentimeter();
        this.totalSleepInMin = this.userProfile.getTotalSleepInMinute();
        this.awakeMin = this.userProfile.getAwakeInMinute();
        this.lightMin = this.userProfile.getLightSleepInMinute();
        this.deepMin = this.userProfile.getDeepSleepInMinute();
        this.isNewDevice = this.userProfile.isNewDevice();
        this.realTimeStepsInfo = this.userProfile.getCurrentSteps();
        this.dailyStepInfo = this.userProfile.getCurrentSteps();
        this.mSyncData = new ArrayList();
        this.syncTime = System.currentTimeMillis() / ((long) 1000);
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final MFLog getMflog() {
        return this.mflog;
    }

    @DexIgnore
    public final List<FitnessData> getSyncData() {
        log("getSyncData dataSize " + this.mSyncData.size());
        return this.mSyncData;
    }

    @DexIgnore
    public final MFSyncLog getSyncLog() {
        return this.syncLog;
    }

    @DexIgnore
    public final long getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.PLAY_DEVICE_ANIMATION_STATE;
        String name = PlayDeviceAnimationState.class.getName();
        kd4.a((Object) name, "PlayDeviceAnimationState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.GET_DEVICE_CONFIG_STATE;
        String name2 = GetDeviceConfigState.class.getName();
        kd4.a((Object) name2, "GetDeviceConfigState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<SubFlow.SessionState, String> sessionStateMap3 = getSessionStateMap();
        SubFlow.SessionState sessionState3 = SubFlow.SessionState.ERASE_DATA_FILE_STATE;
        String name3 = EraseDataFileState.class.getName();
        kd4.a((Object) name3, "EraseDataFileState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<SubFlow.SessionState, String> sessionStateMap4 = getSessionStateMap();
        SubFlow.SessionState sessionState4 = SubFlow.SessionState.READ_DATA_FILE_STATE;
        String name4 = ReadDataFileState.class.getName();
        kd4.a((Object) name4, "ReadDataFileState::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<SubFlow.SessionState, String> sessionStateMap5 = getSessionStateMap();
        SubFlow.SessionState sessionState5 = SubFlow.SessionState.PROCESS_AND_STORE_DATA_STATE;
        String name5 = ProcessAndStoreDataState.class.getName();
        kd4.a((Object) name5, "ProcessAndStoreDataState::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
    }

    @DexIgnore
    public final boolean isClearDataOptional() {
        return this.isClearDataOptional;
    }

    @DexIgnore
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.PLAY_DEVICE_ANIMATION_STATE));
        return true;
    }

    @DexIgnore
    public final void setSyncLog(MFSyncLog mFSyncLog) {
        this.syncLog = mFSyncLog;
    }

    @DexIgnore
    public final void setSyncTime(long j) {
        this.syncTime = j;
    }

    @DexIgnore
    public final void updateCurrentStepAndStepGoalFromApp(boolean z, UserProfile userProfile2) {
        kd4.b(userProfile2, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".updateCurrentStepAndStepGoalFromApp() - currentState=" + getMCurrentState());
        if (getMCurrentState() instanceof ProcessAndStoreDataState) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((ProcessAndStoreDataState) mCurrentState).updateCurrentStepsAndStepGoal(z, userProfile2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow.ProcessAndStoreDataState");
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String tag2 = getTAG();
        local2.d(tag2, "Inside " + getTAG() + ".updateCurrentStepAndStepGoalFromApp - cannot update current steps and step goal " + "to device caused by current state null or not an instance of ProcessAndStoreDataState.");
    }
}
