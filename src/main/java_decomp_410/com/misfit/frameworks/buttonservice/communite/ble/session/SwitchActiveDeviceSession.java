package com.misfit.frameworks.buttonservice.communite.ble.session;

import android.os.Bundle;
import com.fossil.blesdk.device.FeatureErrorCode;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.ISwitchDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BaseSwitchActiveDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseExchangeSecretKeySubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SwitchActiveDeviceSession extends BaseSwitchActiveDeviceSession implements IExchangeKeySession, ISwitchDeviceSession {
    @DexIgnore
    public BackgroundConfig backgroundConfig;
    @DexIgnore
    public ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public LocalizationData localizationData;
    @DexIgnore
    public List<? extends MicroAppMapping> microAppMappings;
    @DexIgnore
    public List<AlarmSetting> multiAlarmSettings;
    @DexIgnore
    public AppNotificationFilterSettings notificationFilterSettings;
    @DexIgnore
    public int secondTimezoneOffset;
    @DexIgnore
    public WatchAppMappingSettings watchAppMappingSettings;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class CloseConnectionState extends BleStateAbs {
        @DexIgnore
        public int failureCode;

        @DexIgnore
        public CloseConnectionState() {
            super(SwitchActiveDeviceSession.this.getTAG());
        }

        @DexIgnore
        public final int getFailureCode() {
            return this.failureCode;
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            SwitchActiveDeviceSession.this.getBleAdapter().closeConnection(SwitchActiveDeviceSession.this.getLogSession(), true);
            SwitchActiveDeviceSession.this.stop(this.failureCode);
            return true;
        }

        @DexIgnore
        public final void setFailureCode(int i) {
            this.failureCode = i;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DianaExchangeSecretKeySubFlow extends BaseExchangeSecretKeySubFlow {
        @DexIgnore
        public DianaExchangeSecretKeySubFlow() {
            super(CommunicateMode.LINK, SwitchActiveDeviceSession.this.getTAG(), SwitchActiveDeviceSession.this, SwitchActiveDeviceSession.this.getMfLog(), SwitchActiveDeviceSession.this.getLogSession(), SwitchActiveDeviceSession.this.getSerial(), SwitchActiveDeviceSession.this.getBleAdapter(), SwitchActiveDeviceSession.this.getBleSessionCallback());
        }

        @DexIgnore
        public void onStop(int i) {
            if (i == FeatureErrorCode.REQUEST_UNSUPPORTED.getCode() || i == 0) {
                SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
                switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState(BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE));
                return;
            }
            SwitchActiveDeviceSession.this.stop(i);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class LinkServerState extends BleStateAbs {
        @DexIgnore
        public LinkServerState() {
            super(SwitchActiveDeviceSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            BleSession.BleSessionCallback access$getBleSessionCallback$p = SwitchActiveDeviceSession.this.getBleSessionCallback();
            if (access$getBleSessionCallback$p == null) {
                return true;
            }
            CommunicateMode communicateMode = CommunicateMode.SWITCH_DEVICE;
            Bundle bundle = new Bundle();
            bundle.putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(SwitchActiveDeviceSession.this.getBleAdapter()));
            access$getBleSessionCallback$p.onAskForLinkServer(communicateMode, bundle);
            return true;
        }

        @DexIgnore
        public final void onLinkServerCompleted(boolean z, int i) {
            if (z) {
                SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
                switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
                return;
            }
            BleState access$createConcreteState = SwitchActiveDeviceSession.this.createConcreteState(BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE);
            if (access$createConcreteState instanceof CloseConnectionState) {
                ((CloseConnectionState) access$createConcreteState).setFailureCode(FailureCode.FAILED_TO_CLEAR_LINK_MAPPING);
            }
            SwitchActiveDeviceSession.this.enterStateAsync(access$createConcreteState);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class StopCurrentWorkoutState extends BleStateAbs {
        @DexIgnore
        public g90<qa4> task;

        @DexIgnore
        public StopCurrentWorkoutState() {
            super(SwitchActiveDeviceSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SwitchActiveDeviceSession.this.getBleAdapter().stopCurrentWorkoutSession(SwitchActiveDeviceSession.this.getLogSession(), this);
            if (this.task == null) {
                SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
                switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW));
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onStopCurrentWorkoutSessionFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            if (!(h90 instanceof FeatureErrorCode) || h90 != FeatureErrorCode.REQUEST_UNSUPPORTED) {
                SwitchActiveDeviceSession.this.stop(FailureCode.FAILED_TO_STOP_CURRENT_WORKOUT_SESSION);
                return;
            }
            SwitchActiveDeviceSession.this.log("Stop current workout request is unsupported. Skip it.");
            SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
            switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW));
        }

        @DexIgnore
        public void onStopCurrentWorkoutSessionSuccess() {
            stopTimeout();
            SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
            switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            g90<qa4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferDataSubFlow extends BaseTransferDataSubFlow {
        @DexIgnore
        public TransferDataSubFlow() {
            super(SwitchActiveDeviceSession.this.getTAG(), SwitchActiveDeviceSession.this, SwitchActiveDeviceSession.this.getMfLog(), SwitchActiveDeviceSession.this.getLogSession(), SwitchActiveDeviceSession.this.getSerial(), SwitchActiveDeviceSession.this.getBleAdapter(), SwitchActiveDeviceSession.this.getUserProfile(), SwitchActiveDeviceSession.this.getBleSessionCallback(), false);
        }

        @DexIgnore
        public void onStop(int i) {
            if (i != 0) {
                BleState access$createConcreteState = SwitchActiveDeviceSession.this.createConcreteState(BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE);
                if (access$createConcreteState != null) {
                    CloseConnectionState closeConnectionState = (CloseConnectionState) access$createConcreteState;
                    closeConnectionState.setFailureCode(FailureCode.FAILED_TO_CLEAR_DATA);
                    SwitchActiveDeviceSession.this.enterStateAsync(closeConnectionState);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SwitchActiveDeviceSession.CloseConnectionState");
            }
            SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
            switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState(BleSessionAbs.SessionState.LINK_SERVER));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferSettingsSubFlow extends BaseTransferSettingsSubFlow {
        @DexIgnore
        public /* final */ /* synthetic */ SwitchActiveDeviceSession this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public TransferSettingsSubFlow(SwitchActiveDeviceSession switchActiveDeviceSession) {
            super(switchActiveDeviceSession.getTAG(), switchActiveDeviceSession, switchActiveDeviceSession.getMfLog(), switchActiveDeviceSession.getLogSession(), true, switchActiveDeviceSession.getSerial(), switchActiveDeviceSession.getBleAdapter(), switchActiveDeviceSession.getUserProfile(), switchActiveDeviceSession.multiAlarmSettings, switchActiveDeviceSession.complicationAppMappingSettings, switchActiveDeviceSession.watchAppMappingSettings, switchActiveDeviceSession.backgroundConfig, switchActiveDeviceSession.notificationFilterSettings, switchActiveDeviceSession.localizationData, switchActiveDeviceSession.microAppMappings, switchActiveDeviceSession.secondTimezoneOffset, switchActiveDeviceSession.getBleSessionCallback());
            this.this$Anon0 = switchActiveDeviceSession;
        }

        @DexIgnore
        public void onStop(int i) {
            this.this$Anon0.stop(i);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class VerifySecretKeySessionState extends BleStateAbs {
        @DexIgnore
        public g90<Boolean> task;

        @DexIgnore
        public VerifySecretKeySessionState() {
            super(SwitchActiveDeviceSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            byte[] tSecretKey = SwitchActiveDeviceSession.this.getBleAdapter().getTSecretKey();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "enter VerifySecretKeySessionState currentSecretKey " + tSecretKey);
            if (tSecretKey == null) {
                SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
                switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState(BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW));
                return true;
            }
            this.task = SwitchActiveDeviceSession.this.getBleAdapter().verifySecretKey(SwitchActiveDeviceSession.this.getLogSession(), tSecretKey, this);
            if (this.task == null) {
                SwitchActiveDeviceSession switchActiveDeviceSession2 = SwitchActiveDeviceSession.this;
                switchActiveDeviceSession2.enterStateAsync(switchActiveDeviceSession2.createConcreteState(BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW));
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            g90<Boolean> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }

        @DexIgnore
        public void onVerifySecretKeyFail(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "Error code " + h90.getCode());
            if (h90.getCode() == FeatureErrorCode.REQUEST_UNSUPPORTED.getCode()) {
                SwitchActiveDeviceSession.this.stop(FeatureErrorCode.REQUEST_UNSUPPORTED.getCode());
                return;
            }
            SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
            switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState(BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW));
        }

        @DexIgnore
        public void onVerifySecretKeySuccess(boolean z) {
            stopTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "onVerifySecretKeySuccess isValid " + z);
            if (z) {
                SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
                switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState(BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE));
                return;
            }
            SwitchActiveDeviceSession switchActiveDeviceSession2 = SwitchActiveDeviceSession.this;
            switchActiveDeviceSession2.enterStateAsync(switchActiveDeviceSession2.createConcreteState(BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceSession(UserProfile userProfile, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        super(userProfile, bleAdapterImpl, bleSessionCallback, communicationResultCallback);
        kd4.b(userProfile, "userProfile");
        kd4.b(bleAdapterImpl, "bleAdapter");
        setLogSession(FLogger.Session.SWITCH_DEVICE);
    }

    @DexIgnore
    public BleSession copyObject() {
        SwitchActiveDeviceSession switchActiveDeviceSession = new SwitchActiveDeviceSession(getUserProfile(), getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback());
        switchActiveDeviceSession.setDevice(getDevice());
        return switchActiveDeviceSession;
    }

    @DexIgnore
    public void doNextState() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "doNextState, serial=" + getSerial());
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SwitchActiveDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    public void initSettings() {
        super.initSettings();
        this.multiAlarmSettings = DevicePreferenceUtils.getAutoListAlarm(getContext());
        this.complicationAppMappingSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(getContext(), getSerial());
        this.watchAppMappingSettings = DevicePreferenceUtils.getAutoWatchAppSettings(getContext(), getSerial());
        this.backgroundConfig = DevicePreferenceUtils.getAutoBackgroundImageConfig(getContext(), getSerial());
        this.notificationFilterSettings = DevicePreferenceUtils.getAutoNotificationFiltersConfig(getContext(), getSerial());
        this.localizationData = DevicePreferenceUtils.getAutoLocalizationDataSettings(getContext(), getSerial());
        this.microAppMappings = MicroAppMapping.convertToMicroAppMapping(DevicePreferenceUtils.getAutoMapping(getContext(), getSerial()));
        this.secondTimezoneOffset = DevicePreferenceUtils.getAutoSecondTimezone(getContext());
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.VERIFY_SECRET_KEY;
        String name = VerifySecretKeySessionState.class.getName();
        kd4.a((Object) name, "VerifySecretKeySessionState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW;
        String name2 = DianaExchangeSecretKeySubFlow.class.getName();
        kd4.a((Object) name2, "DianaExchangeSecretKeySubFlow::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap3 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState3 = BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE;
        String name3 = StopCurrentWorkoutState.class.getName();
        kd4.a((Object) name3, "StopCurrentWorkoutState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap4 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState4 = BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW;
        String name4 = TransferDataSubFlow.class.getName();
        kd4.a((Object) name4, "TransferDataSubFlow::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap5 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState5 = BleSessionAbs.SessionState.LINK_SERVER;
        String name5 = LinkServerState.class.getName();
        kd4.a((Object) name5, "LinkServerState::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap6 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState6 = BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW;
        String name6 = TransferSettingsSubFlow.class.getName();
        kd4.a((Object) name6, "TransferSettingsSubFlow::class.java.name");
        sessionStateMap6.put(sessionState6, name6);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap7 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState7 = BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE;
        String name7 = CloseConnectionState.class.getName();
        kd4.a((Object) name7, "CloseConnectionState::class.java.name");
        sessionStateMap7.put(sessionState7, name7);
    }

    @DexIgnore
    public void onGetWatchParamFailed() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed");
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).onGetWatchParamFailed();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SwitchActiveDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    public void onLinkServerSuccess(boolean z, int i) {
        BleState currentState = getCurrentState();
        if (currentState instanceof LinkServerState) {
            ((LinkServerState) currentState).onLinkServerCompleted(z, i);
        }
    }

    @DexIgnore
    public void onReceiveRandomKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveRandomKey randomKey " + bArr + " state " + currentState);
        if (currentState instanceof DianaExchangeSecretKeySubFlow) {
            ((DianaExchangeSecretKeySubFlow) currentState).onReceiveRandomKey(bArr, i);
        }
    }

    @DexIgnore
    public void onReceiveServerSecretKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerSecretKey secretKey " + bArr + " state " + currentState);
        if (currentState instanceof DianaExchangeSecretKeySubFlow) {
            ((DianaExchangeSecretKeySubFlow) currentState).onReceiveServerSecretKey(bArr, i);
        }
    }

    @DexIgnore
    public void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        kd4.b(str, "serial");
        kd4.b(watchParamsFileMapping, "watchParamsData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setLatestWatchParam, serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String tag2 = getTAG();
        remote.d(component, session, str, tag2, "setLatestWatchParam(), serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setLatestWatchParam(str, watchParamsFileMapping);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SwitchActiveDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, can't set WatchParams because currentState is not an instance of TransferSettingSubFlow");
    }
}
