package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.data.config.DeviceConfigKey[], com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.data.config.DeviceConfigItem[] $deviceConfigItems$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.log.FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$1(com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl bleAdapterImpl, com.fossil.blesdk.device.data.config.DeviceConfigItem[] deviceConfigItemArr, com.misfit.frameworks.buttonservice.log.FLogger.Session session, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$deviceConfigItems$inlined = deviceConfigItemArr;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.data.config.DeviceConfigKey[]) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.data.config.DeviceConfigKey[] deviceConfigKeyArr) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceConfigKeyArr, "it");
        this.this$0.log(this.$logSession$inlined, "Set Device Config Success");
        this.$callback$inlined.onSetDeviceConfigSuccess();
    }
}
