package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleAdapterImpl$doOTA$$inlined$let$lambda$Anon1 extends Lambda implements xc4<Float, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] $firmwareData$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$doOTA$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, byte[] bArr, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$Anon0 = bleAdapterImpl;
        this.$firmwareData$inlined = bArr;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke(((Number) obj).floatValue());
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(float f) {
        int i = ((int) f) * 100;
        if (i % 10 == 0 || i > 90) {
            BleAdapterImpl bleAdapterImpl = this.this$Anon0;
            FLogger.Session session = this.$logSession$inlined;
            bleAdapterImpl.log(session, "Do OTA: percentage->" + f);
        }
        this.$callback$inlined.onDataTransferProgressChange(f);
    }
}
