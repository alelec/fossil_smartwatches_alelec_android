package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$getAllLogsExceptSyncing$2", mo27670f = "DBLogWriter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class DBLogWriter$getAllLogsExceptSyncing$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<com.misfit.frameworks.buttonservice.log.LogEvent>>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20850p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.log.DBLogWriter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.misfit.frameworks.buttonservice.log.DBLogWriter$getAllLogsExceptSyncing$2$1")
    /* renamed from: com.misfit.frameworks.buttonservice.log.DBLogWriter$getAllLogsExceptSyncing$2$1 */
    public static final class C55681 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.misfit.frameworks.buttonservice.log.p004db.Log, com.misfit.frameworks.buttonservice.log.LogEvent> {
        @DexIgnore
        public /* final */ /* synthetic */ com.google.gson.Gson $gson;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C55681(com.google.gson.Gson gson) {
            super(1);
            this.$gson = gson;
        }

        @DexIgnore
        public final com.misfit.frameworks.buttonservice.log.LogEvent invoke(com.misfit.frameworks.buttonservice.log.p004db.Log log) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(log, "it");
            try {
                com.misfit.frameworks.buttonservice.log.LogEvent logEvent = (com.misfit.frameworks.buttonservice.log.LogEvent) this.$gson.mo23093a(log.getContent(), com.misfit.frameworks.buttonservice.log.LogEvent.class);
                logEvent.setTag(java.lang.Integer.valueOf(log.getId()));
                return logEvent;
            } catch (java.lang.Exception e) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String access$getTAG$cp = com.misfit.frameworks.buttonservice.log.DBLogWriter.TAG;
                local.mo33256e(access$getTAG$cp, ", getAllLogs(), ex: " + e.getMessage());
                return null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$getAllLogsExceptSyncing$2(com.misfit.frameworks.buttonservice.log.DBLogWriter dBLogWriter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = dBLogWriter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.misfit.frameworks.buttonservice.log.DBLogWriter$getAllLogsExceptSyncing$2 dBLogWriter$getAllLogsExceptSyncing$2 = new com.misfit.frameworks.buttonservice.log.DBLogWriter$getAllLogsExceptSyncing$2(this.this$0, yb4);
        dBLogWriter$getAllLogsExceptSyncing$2.f20850p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dBLogWriter$getAllLogsExceptSyncing$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.misfit.frameworks.buttonservice.log.DBLogWriter$getAllLogsExceptSyncing$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return kotlin.sequences.SequencesKt___SequencesKt.m37038g(kotlin.sequences.SequencesKt___SequencesKt.m37034d(com.fossil.blesdk.obfuscated.kb4.m24372b(this.this$0.logDao.getAllLogEventsExcept(com.misfit.frameworks.buttonservice.log.p004db.Log.Flag.SYNCING)), new com.misfit.frameworks.buttonservice.log.DBLogWriter$getAllLogsExceptSyncing$2.C55681(new com.google.gson.Gson())));
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
