package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FLogUtils$getGsonForLogEvent$1<T> implements com.fossil.blesdk.obfuscated.b02<java.lang.Long> {
    @DexIgnore
    public static /* final */ com.misfit.frameworks.buttonservice.log.FLogUtils$getGsonForLogEvent$1 INSTANCE; // = new com.misfit.frameworks.buttonservice.log.FLogUtils$getGsonForLogEvent$1();

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.zz1 serialize(java.lang.Long l, java.lang.reflect.Type type, com.fossil.blesdk.obfuscated.a02 a02) {
        return new com.fossil.blesdk.obfuscated.zz1((java.lang.Number) new java.math.BigDecimal(((double) l.longValue()) / ((double) 1000)).setScale(6, java.math.RoundingMode.HALF_UP));
    }
}
