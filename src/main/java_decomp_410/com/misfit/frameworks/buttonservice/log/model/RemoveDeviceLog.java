package com.misfit.frameworks.buttonservice.log.model;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.kd4;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RemoveDeviceLog {
    @DexIgnore
    @f02("current_device")
    public String currentDevice;

    @DexIgnore
    public RemoveDeviceLog(String str) {
        kd4.b(str, "currentDevice");
        this.currentDevice = str;
    }

    @DexIgnore
    public final String getCurrentDevice() {
        return this.currentDevice;
    }

    @DexIgnore
    public final void setCurrentDevice(String str) {
        kd4.b(str, "<set-?>");
        this.currentDevice = str;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        kd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }
}
