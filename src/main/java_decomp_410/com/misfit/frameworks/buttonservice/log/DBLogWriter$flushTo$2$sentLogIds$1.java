package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DBLogWriter$flushTo$2$sentLogIds$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.misfit.frameworks.buttonservice.log.LogEvent, java.lang.Integer> {
    @DexIgnore
    public static /* final */ com.misfit.frameworks.buttonservice.log.DBLogWriter$flushTo$2$sentLogIds$1 INSTANCE; // = new com.misfit.frameworks.buttonservice.log.DBLogWriter$flushTo$2$sentLogIds$1();

    @DexIgnore
    public DBLogWriter$flushTo$2$sentLogIds$1() {
        super(1);
    }

    @DexIgnore
    public final java.lang.Integer invoke(com.misfit.frameworks.buttonservice.log.LogEvent logEvent) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(logEvent, "it");
        java.lang.Object tag = logEvent.getTag();
        if (!(tag instanceof java.lang.Integer)) {
            tag = null;
        }
        return (java.lang.Integer) tag;
    }
}
