package com.misfit.frameworks.buttonservice.log;

import android.content.Context;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ILocalFLogger {
    @DexIgnore
    void d(String str, String str2);

    @DexIgnore
    void e(String str, String str2);

    @DexIgnore
    List<File> exportAppLogs();

    @DexIgnore
    void i(String str, String str2);

    @DexIgnore
    void init(Context context, String str, boolean z);

    @DexIgnore
    void v(String str, String str2);
}
