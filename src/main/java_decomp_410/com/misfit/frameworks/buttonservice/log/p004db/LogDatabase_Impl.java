package com.misfit.frameworks.buttonservice.log.p004db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.log.db.LogDatabase_Impl */
public final class LogDatabase_Impl extends com.misfit.frameworks.buttonservice.log.p004db.LogDatabase {
    @DexIgnore
    public volatile com.misfit.frameworks.buttonservice.log.p004db.LogDao _logDao;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.misfit.frameworks.buttonservice.log.db.LogDatabase_Impl$1")
    /* renamed from: com.misfit.frameworks.buttonservice.log.db.LogDatabase_Impl$1 */
    public class C55721 extends com.fossil.blesdk.obfuscated.C2962tf.C2963a {
        @DexIgnore
        public C55721(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
            ggVar.mo11230b("CREATE TABLE IF NOT EXISTS `log` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timeStamp` INTEGER NOT NULL, `content` TEXT NOT NULL, `cloudFlag` TEXT NOT NULL)");
            ggVar.mo11230b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.mo11230b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '1e76ae5948872f78452bd16b18a13dc4')");
        }

        @DexIgnore
        public void dropAllTables(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
            ggVar.mo11230b("DROP TABLE IF EXISTS `log`");
        }

        @DexIgnore
        public void onCreate(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
            if (com.misfit.frameworks.buttonservice.log.p004db.LogDatabase_Impl.this.mCallbacks != null) {
                int size = com.misfit.frameworks.buttonservice.log.p004db.LogDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((androidx.room.RoomDatabase.C0273b) com.misfit.frameworks.buttonservice.log.p004db.LogDatabase_Impl.this.mCallbacks.get(i)).mo3294a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
            com.fossil.blesdk.obfuscated.C1874gg unused = com.misfit.frameworks.buttonservice.log.p004db.LogDatabase_Impl.this.mDatabase = ggVar;
            com.misfit.frameworks.buttonservice.log.p004db.LogDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (com.misfit.frameworks.buttonservice.log.p004db.LogDatabase_Impl.this.mCallbacks != null) {
                int size = com.misfit.frameworks.buttonservice.log.p004db.LogDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((androidx.room.RoomDatabase.C0273b) com.misfit.frameworks.buttonservice.log.p004db.LogDatabase_Impl.this.mCallbacks.get(i)).mo3295b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
            com.fossil.blesdk.obfuscated.C1482bg.m4960a(ggVar);
        }

        @DexIgnore
        public void validateMigration(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
            java.util.HashMap hashMap = new java.util.HashMap(4);
            hashMap.put("id", new com.fossil.blesdk.obfuscated.C1726eg.C1727a("id", "INTEGER", true, 1));
            hashMap.put("timeStamp", new com.fossil.blesdk.obfuscated.C1726eg.C1727a("timeStamp", "INTEGER", true, 0));
            hashMap.put("content", new com.fossil.blesdk.obfuscated.C1726eg.C1727a("content", "TEXT", true, 0));
            hashMap.put("cloudFlag", new com.fossil.blesdk.obfuscated.C1726eg.C1727a("cloudFlag", "TEXT", true, 0));
            com.fossil.blesdk.obfuscated.C1726eg egVar = new com.fossil.blesdk.obfuscated.C1726eg("log", hashMap, new java.util.HashSet(0), new java.util.HashSet(0));
            com.fossil.blesdk.obfuscated.C1726eg a = com.fossil.blesdk.obfuscated.C1726eg.m6441a(ggVar, "log");
            if (!egVar.equals(a)) {
                throw new java.lang.IllegalStateException("Migration didn't properly handle log(com.misfit.frameworks.buttonservice.log.db.Log).\n Expected:\n" + egVar + "\n Found:\n" + a);
            }
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        com.fossil.blesdk.obfuscated.C1874gg a = super.getOpenHelper().mo11634a();
        try {
            super.beginTransaction();
            a.mo11230b("DELETE FROM `log`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.mo11232d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.mo11239x()) {
                a.mo11230b("VACUUM");
            }
        }
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2647pf createInvalidationTracker() {
        return new com.fossil.blesdk.obfuscated.C2647pf(this, new java.util.HashMap(0), new java.util.HashMap(0), "log");
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C1945hg createOpenHelper(com.fossil.blesdk.obfuscated.C2124jf jfVar) {
        com.fossil.blesdk.obfuscated.C2962tf tfVar = new com.fossil.blesdk.obfuscated.C2962tf(jfVar, new com.misfit.frameworks.buttonservice.log.p004db.LogDatabase_Impl.C55721(1), "1e76ae5948872f78452bd16b18a13dc4", "3ef7199be598e6814da70cf3eed15c90");
        com.fossil.blesdk.obfuscated.C1945hg.C1947b.C1948a a = com.fossil.blesdk.obfuscated.C1945hg.C1947b.m7875a(jfVar.f6411b);
        a.mo11646a(jfVar.f6412c);
        a.mo11645a((com.fossil.blesdk.obfuscated.C1945hg.C1946a) tfVar);
        return jfVar.f6410a.mo11648a(a.mo11647a());
    }

    @DexIgnore
    public com.misfit.frameworks.buttonservice.log.p004db.LogDao getLogDao() {
        com.misfit.frameworks.buttonservice.log.p004db.LogDao logDao;
        if (this._logDao != null) {
            return this._logDao;
        }
        synchronized (this) {
            if (this._logDao == null) {
                this._logDao = new com.misfit.frameworks.buttonservice.log.p004db.LogDao_Impl(this);
            }
            logDao = this._logDao;
        }
        return logDao;
    }
}
