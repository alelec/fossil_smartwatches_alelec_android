package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$flush$1", mo27670f = "RemoteFLogger.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class RemoteFLogger$flush$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20855p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.log.RemoteFLogger this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$flush$1(com.misfit.frameworks.buttonservice.log.RemoteFLogger remoteFLogger, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = remoteFLogger;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.misfit.frameworks.buttonservice.log.RemoteFLogger$flush$1 remoteFLogger$flush$1 = new com.misfit.frameworks.buttonservice.log.RemoteFLogger$flush$1(this.this$0, yb4);
        remoteFLogger$flush$1.f20855p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return remoteFLogger$flush$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.misfit.frameworks.buttonservice.log.RemoteFLogger$flush$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            if (this.this$0.isMainFLogger && !this.this$0.isFlushing) {
                this.this$0.isFlushing = true;
                com.misfit.frameworks.buttonservice.log.RemoteFLogger remoteFLogger = this.this$0;
                remoteFLogger.sendInternalMessage(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_ACTION_FLUSH, remoteFLogger.floggerName, com.misfit.frameworks.buttonservice.log.RemoteFLogger.MessageTarget.TO_ALL_EXCEPT_MAIN_FLOGGER, new android.os.Bundle());
                this.this$0.isFlushing = false;
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
