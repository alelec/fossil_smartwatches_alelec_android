package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.misfit.frameworks.buttonservice.log.BufferLogWriter$run$2", mo27670f = "BufferLogWriter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class BufferLogWriter$run$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20848p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.log.BufferLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$run$2(com.misfit.frameworks.buttonservice.log.BufferLogWriter bufferLogWriter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = bufferLogWriter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.misfit.frameworks.buttonservice.log.BufferLogWriter$run$2 bufferLogWriter$run$2 = new com.misfit.frameworks.buttonservice.log.BufferLogWriter$run$2(this.this$0, yb4);
        bufferLogWriter$run$2.f20848p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return bufferLogWriter$run$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.misfit.frameworks.buttonservice.log.BufferLogWriter$run$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.misfit.frameworks.buttonservice.log.BufferDebugOption access$getDebugOption$p = this.this$0.debugOption;
            if (access$getDebugOption$p != null) {
                com.misfit.frameworks.buttonservice.log.IFinishCallback callback = access$getDebugOption$p.getCallback();
                if (callback != null) {
                    callback.onFinish();
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
