package com.misfit.frameworks.buttonservice.log;

import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FlushLogEvent extends LogEvent {
    @DexIgnore
    public FlushLogEvent() {
        super(FLogger.LogLevel.DEBUG, System.currentTimeMillis(), "", "", "", "", "", "", "", "", "", FLogger.Component.APP, FLogger.Session.OTHER, "", "");
    }
}
