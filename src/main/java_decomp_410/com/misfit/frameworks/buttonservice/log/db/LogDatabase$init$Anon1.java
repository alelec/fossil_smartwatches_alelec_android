package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.misfit.frameworks.buttonservice.log.db.LogDatabase$init$Anon1", f = "LogDatabase.kt", l = {}, m = "invokeSuspend")
public final class LogDatabase$init$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LogDatabase this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ LogDatabase$init$Anon1 this$Anon0;

        @DexIgnore
        public Anon1(LogDatabase$init$Anon1 logDatabase$init$Anon1) {
            this.this$Anon0 = logDatabase$init$Anon1;
        }

        @DexIgnore
        public final void run() {
            hg openHelper = this.this$Anon0.this$Anon0.getOpenHelper();
            kd4.a((Object) openHelper, "openHelper");
            openHelper.a().b("CREATE TRIGGER IF NOT EXISTS delete_keep_2000 after insert on log WHEN (select count(*) from log) >= 2000 BEGIN DELETE FROM log WHERE id NOT IN  (SELECT id FROM log ORDER BY timeStamp desc limit 2000); END");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LogDatabase$init$Anon1(LogDatabase logDatabase, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = logDatabase;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // LogDatabase$init$Anon1 logDatabase$init$Anon1 = new LogDatabase$init$Anon1(this.this$Anon0, yb4);
        // logDatabase$init$Anon1.p$ = (zg4) obj;
        // return logDatabase$init$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((LogDatabase$init$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            this.this$Anon0.runInTransaction((Runnable) new Anon1(this));
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
