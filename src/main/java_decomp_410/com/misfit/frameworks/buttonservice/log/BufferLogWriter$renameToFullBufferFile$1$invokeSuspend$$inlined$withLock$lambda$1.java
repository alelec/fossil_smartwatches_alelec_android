package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BufferLogWriter$renameToFullBufferFile$1$invokeSuspend$$inlined$withLock$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $logs;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.zg4 $this_launch$inlined;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$renameToFullBufferFile$1$invokeSuspend$$inlined$withLock$lambda$1(java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4, com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1 bufferLogWriter$renameToFullBufferFile$1, com.fossil.blesdk.obfuscated.zg4 zg4) {
        super(2, yb4);
        this.$logs = list;
        this.this$0 = bufferLogWriter$renameToFullBufferFile$1;
        this.$this_launch$inlined = zg4;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1$invokeSuspend$$inlined$withLock$lambda$1 bufferLogWriter$renameToFullBufferFile$1$invokeSuspend$$inlined$withLock$lambda$1 = new com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1$invokeSuspend$$inlined$withLock$lambda$1(this.$logs, yb4, this.this$0, this.$this_launch$inlined);
        bufferLogWriter$renameToFullBufferFile$1$invokeSuspend$$inlined$withLock$lambda$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return bufferLogWriter$renameToFullBufferFile$1$invokeSuspend$$inlined$withLock$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1$invokeSuspend$$inlined$withLock$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.misfit.frameworks.buttonservice.log.BufferLogWriter.IBufferLogCallback callback = this.this$0.this$0.getCallback();
            if (callback == null) {
                return null;
            }
            callback.onFullBuffer(this.this$0.this$0.toLogEvents(this.$logs), this.this$0.$forceFlush);
            return com.fossil.blesdk.obfuscated.qa4.a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
