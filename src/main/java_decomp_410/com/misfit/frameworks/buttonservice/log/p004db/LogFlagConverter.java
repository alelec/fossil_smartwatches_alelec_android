package com.misfit.frameworks.buttonservice.log.p004db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.log.db.LogFlagConverter */
public final class LogFlagConverter {
    @DexIgnore
    public final java.lang.String logFlagEnumToString(com.misfit.frameworks.buttonservice.log.p004db.Log.Flag flag) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(flag, "value");
        return flag.getValue();
    }

    @DexIgnore
    public final com.misfit.frameworks.buttonservice.log.p004db.Log.Flag stringToLogFlag(java.lang.String str) {
        if (str != null) {
            com.misfit.frameworks.buttonservice.log.p004db.Log.Flag fromValue = com.misfit.frameworks.buttonservice.log.p004db.Log.Flag.Companion.fromValue(str);
            if (fromValue != null) {
                return fromValue;
            }
        }
        return com.misfit.frameworks.buttonservice.log.p004db.Log.Flag.ADD;
    }
}
