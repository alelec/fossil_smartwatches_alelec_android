package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.bt4;
import com.fossil.blesdk.obfuscated.ps4;
import com.fossil.blesdk.obfuscated.xz1;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface LogApiService {
    @DexIgnore
    @bt4("app_log/event")
    Call<xz1> sendLogs(@ps4 xz1 xz1);
}
