package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$deleteLogs$Anon2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$deleteLogs$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super Integer>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $logIds;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$deleteLogs$Anon2(DBLogWriter dBLogWriter, List list, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = dBLogWriter;
        this.$logIds = list;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DBLogWriter$deleteLogs$Anon2 dBLogWriter$deleteLogs$Anon2 = new DBLogWriter$deleteLogs$Anon2(this.this$Anon0, this.$logIds, yb4);
        dBLogWriter$deleteLogs$Anon2.p$ = (zg4) obj;
        return dBLogWriter$deleteLogs$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DBLogWriter$deleteLogs$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            int i = 0;
            for (List d : kb4.b(this.$logIds, 500)) {
                i += this.this$Anon0.logDao.delete(kb4.d(d));
            }
            return dc4.a(i);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
