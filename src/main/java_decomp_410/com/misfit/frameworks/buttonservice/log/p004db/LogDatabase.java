package com.misfit.frameworks.buttonservice.log.p004db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.log.db.LogDatabase */
public abstract class LogDatabase extends androidx.room.RoomDatabase {
    @DexIgnore
    public static /* final */ com.misfit.frameworks.buttonservice.log.p004db.LogDatabase.Companion Companion; // = new com.misfit.frameworks.buttonservice.log.p004db.LogDatabase.Companion((com.fossil.blesdk.obfuscated.fd4) null);
    @DexIgnore
    public static /* final */ java.lang.String TAG; // = "LogDatabase";

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.misfit.frameworks.buttonservice.log.db.LogDatabase$Companion")
    /* renamed from: com.misfit.frameworks.buttonservice.log.db.LogDatabase$Companion */
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public abstract com.misfit.frameworks.buttonservice.log.p004db.LogDao getLogDao();

    @DexIgnore
    public void init(com.fossil.blesdk.obfuscated.C2124jf jfVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(jfVar, "configuration");
        super.init(jfVar);
        try {
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.misfit.frameworks.buttonservice.log.p004db.LogDatabase$init$1(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d(TAG, "exception when init database " + e);
        }
    }
}
