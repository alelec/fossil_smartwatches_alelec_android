package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.em4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.google.gson.Gson;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import java.net.SocketTimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class RepoResponse<T> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final <T> Failure<T> create(Throwable th) {
            kd4.b(th, "error");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("create=");
            th.printStackTrace();
            sb.append(qa4.a);
            local.d("RepoResponse", sb.toString());
            if (th instanceof SocketTimeoutException) {
                return new Failure(MFNetworkReturnCode.CLIENT_TIMEOUT, (ServerError) null, th, (String) null, 8, (fd4) null);
            }
            return new Failure(601, (ServerError) null, th, (String) null, 8, (fd4) null);
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0039, code lost:
            if (r0 != null) goto L_0x0040;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x0087, code lost:
            if (r0 != null) goto L_0x008e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b7, code lost:
            if (r0 != null) goto L_0x00be;
         */
        @DexIgnore
        public final <T> RepoResponse<T> create(qr4<T> qr4) {
            String str;
            String str2;
            String str3;
            kd4.b(qr4, "response");
            if (qr4.d()) {
                return new Success(qr4.a());
            }
            int b = qr4.b();
            if (b == 504 || b == 503 || b == 500 || b == 401 || b == 429) {
                ServerError serverError = new ServerError();
                serverError.setCode(Integer.valueOf(b));
                em4 c = qr4.c();
                if (c != null) {
                    str = c.F();
                }
                str = qr4.e();
                serverError.setMessage(str);
                return new Failure(b, serverError, (Throwable) null, (String) null, 8, (fd4) null);
            }
            em4 c2 = qr4.c();
            if (c2 != null) {
                str2 = c2.F();
            }
            str2 = qr4.e();
            try {
                ServerError serverError2 = (ServerError) new Gson().a(str2, ServerError.class);
                if (serverError2 != null) {
                    Integer code = serverError2.getCode();
                    if (code != null) {
                        if (code.intValue() == 0) {
                        }
                    }
                    return new Failure(qr4.b(), serverError2, (Throwable) null, (String) null, 8, (fd4) null);
                }
                return new Failure(qr4.b(), (ServerError) null, (Throwable) null, str2);
            } catch (Exception unused) {
                em4 c3 = qr4.c();
                if (c3 != null) {
                    str3 = c3.F();
                }
                str3 = qr4.e();
                return new Failure(qr4.b(), new ServerError(b, str3), (Throwable) null, (String) null, 8, (fd4) null);
            }
        }
    }

    @DexIgnore
    public RepoResponse() {
    }

    @DexIgnore
    public /* synthetic */ RepoResponse(fd4 fd4) {
        this();
    }
}
