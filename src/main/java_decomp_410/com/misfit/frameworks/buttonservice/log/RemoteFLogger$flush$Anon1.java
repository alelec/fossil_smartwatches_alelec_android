package com.misfit.frameworks.buttonservice.log;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$flush$Anon1", f = "RemoteFLogger.kt", l = {}, m = "invokeSuspend")
public final class RemoteFLogger$flush$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$flush$Anon1(RemoteFLogger remoteFLogger, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = remoteFLogger;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        RemoteFLogger$flush$Anon1 remoteFLogger$flush$Anon1 = new RemoteFLogger$flush$Anon1(this.this$Anon0, yb4);
        remoteFLogger$flush$Anon1.p$ = (zg4) obj;
        return remoteFLogger$flush$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((RemoteFLogger$flush$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            if (this.this$Anon0.isMainFLogger && !this.this$Anon0.isFlushing) {
                this.this$Anon0.isFlushing = true;
                RemoteFLogger remoteFLogger = this.this$Anon0;
                remoteFLogger.sendInternalMessage(RemoteFLogger.MESSAGE_ACTION_FLUSH, remoteFLogger.floggerName, RemoteFLogger.MessageTarget.TO_ALL_EXCEPT_MAIN_FLOGGER, new Bundle());
                this.this$Anon0.isFlushing = false;
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
