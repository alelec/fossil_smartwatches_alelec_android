package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DBLogWriter$writeLog$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.misfit.frameworks.buttonservice.log.LogEvent, com.misfit.frameworks.buttonservice.log.p004db.Log> {
    @DexIgnore
    public static /* final */ com.misfit.frameworks.buttonservice.log.DBLogWriter$writeLog$1 INSTANCE; // = new com.misfit.frameworks.buttonservice.log.DBLogWriter$writeLog$1();

    @DexIgnore
    public DBLogWriter$writeLog$1() {
        super(1);
    }

    @DexIgnore
    public final com.misfit.frameworks.buttonservice.log.p004db.Log invoke(com.misfit.frameworks.buttonservice.log.LogEvent logEvent) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(logEvent, "it");
        return new com.misfit.frameworks.buttonservice.log.p004db.Log(logEvent.getTimestamp(), logEvent.toString(), com.misfit.frameworks.buttonservice.log.p004db.Log.Flag.ADD);
    }
}
