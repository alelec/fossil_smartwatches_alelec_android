package com.misfit.frameworks.buttonservice.log;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.am4;
import com.fossil.blesdk.obfuscated.dm4;
import com.fossil.blesdk.obfuscated.em4;
import com.fossil.blesdk.obfuscated.kd4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.zendesk.sdk.network.Constants;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Calendar;
import okhttp3.Interceptor;
import okhttp3.Protocol;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogInterceptor implements Interceptor {
    @DexIgnore
    public /* final */ String accessKey;
    @DexIgnore
    public /* final */ String logBrandName;
    @DexIgnore
    public /* final */ String secretKey;

    @DexIgnore
    public LogInterceptor(String str, String str2, String str3) {
        kd4.b(str, "logBrandName");
        kd4.b(str2, "accessKey");
        kd4.b(str3, "secretKey");
        this.logBrandName = str;
        this.accessKey = str2;
        this.secretKey = str3;
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) {
        ServerError serverError;
        kd4.b(chain, "chain");
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        long timeInMillis = instance.getTimeInMillis() / ((long) 1000);
        dm4 n = chain.n();
        dm4.a f = n.f();
        f.b("X-Cyc-Brand", this.logBrandName);
        f.b("X-Cyc-Timestamp", String.valueOf(timeInMillis));
        f.b("X-Cyc-Auth-Method", "signature");
        f.b("X-Cyc-Access-Key-Id", this.accessKey);
        StringBuilder sb = new StringBuilder();
        sb.append("Signature=");
        sb.append(ConversionUtils.SHA1(timeInMillis + this.secretKey));
        f.b("Authorization", sb.toString());
        f.b(GraphRequest.CONTENT_TYPE_HEADER, Constants.APPLICATION_JSON);
        f.a(n.e(), n.a());
        dm4 a = f.a();
        try {
            Response a2 = chain.a(a);
            kd4.a((Object) a2, "chain.proceed(request)");
            return a2;
        } catch (Exception e) {
            if (e instanceof ServerErrorException) {
                serverError = ((ServerErrorException) e).getServerError();
            } else if (e instanceof UnknownHostException) {
                serverError = new ServerError(601, "");
            } else if (e instanceof SocketTimeoutException) {
                serverError = new ServerError(MFNetworkReturnCode.CLIENT_TIMEOUT, "");
            } else {
                serverError = new ServerError(600, "");
            }
            Response.a aVar = new Response.a();
            aVar.a(a);
            aVar.a(Protocol.HTTP_1_1);
            Integer code = serverError.getCode();
            kd4.a((Object) code, "serverError.code");
            aVar.a(code.intValue());
            String message = serverError.getMessage();
            if (message == null) {
                message = "";
            }
            aVar.a(message);
            aVar.a(em4.a(am4.b(Constants.APPLICATION_JSON), new Gson().a((Object) serverError)));
            aVar.a(GraphRequest.CONTENT_TYPE_HEADER, com.misfit.frameworks.common.constants.Constants.CONTENT_TYPE);
            Response a3 = aVar.a();
            kd4.a((Object) a3, "okhttp3.Response.Builder\u2026                 .build()");
            return a3;
        }
    }
}
