package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.bf4;
import com.fossil.blesdk.obfuscated.dl4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.fl4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kv1;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.za4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.extensions.SynchronizeQueue;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BufferLogWriter extends Thread {
    @DexIgnore
    public static /* final */ String ALL_BUFFER_FILE_NAME_REGEX_PATTERN; // = "\\w*buffer_log\\w*\\.txt";
    @DexIgnore
    public static /* final */ String BUFFER_FILE_NAME; // = "%s_buffer_log.txt";
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String FULL_BUFFER_FILE_NAME_PATTERN; // = "%s_buffer_log_%s.txt";
    @DexIgnore
    public static /* final */ String FULL_BUFFER_FILE_NAME_REGEX_PATTERN; // = "\\w*buffer_log_\\d+\\.txt";
    @DexIgnore
    public static /* final */ String LOG_FOLDER; // = "buffer_logs";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public IBufferLogCallback callback;
    @DexIgnore
    public BufferDebugOption debugOption;
    @DexIgnore
    public String directoryPath; // = "";
    @DexIgnore
    public /* final */ String floggerName;
    @DexIgnore
    public volatile boolean isMainFLogger;
    @DexIgnore
    public volatile boolean isRunning;
    @DexIgnore
    public /* final */ SynchronizeQueue<LogEvent> logEventQueue; // = new SynchronizeQueue<>();
    @DexIgnore
    public String logFilePath; // = "";
    @DexIgnore
    public /* final */ Object mLocker; // = new Object();
    @DexIgnore
    public /* final */ dl4 mMutex; // = fl4.a(false, 1, (Object) null);
    @DexIgnore
    public /* final */ int threshold;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface IBufferLogCallback {
        @DexIgnore
        void onFullBuffer(List<LogEvent> list, boolean z);

        @DexIgnore
        void onWrittenSummaryLog(LogEvent logEvent);
    }

    /*
    static {
        String name = BufferLogWriter.class.getName();
        kd4.a((Object) name, "BufferLogWriter::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    public BufferLogWriter(String str, int i) {
        kd4.b(str, "floggerName");
        this.floggerName = str;
        this.threshold = i;
    }

    @DexIgnore
    private final String getFullBufferLogFileName(long j) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.directoryPath);
        sb.append(File.separatorChar);
        pd4 pd4 = pd4.a;
        Object[] objArr = {this.floggerName, Long.valueOf(j), Locale.US};
        String format = String.format(FULL_BUFFER_FILE_NAME_PATTERN, Arrays.copyOf(objArr, objArr.length));
        kd4.a((Object) format, "java.lang.String.format(format, *args)");
        sb.append(format);
        return sb.toString();
    }

    @DexIgnore
    private final void renameToFullBufferFile(File file, boolean z) {
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        file.renameTo(new File(getFullBufferLogFileName(instance.getTimeInMillis())));
        fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new BufferLogWriter$renameToFullBufferFile$Anon1(this, z, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    private final List<LogEvent> toLogEvents(List<String> list) {
        return SequencesKt___SequencesKt.g(SequencesKt___SequencesKt.d(kb4.b(list), new BufferLogWriter$toLogEvents$Anon1(new Gson())));
    }

    @DexIgnore
    public final List<File> exportLogs() {
        File[] listFiles = new File(this.directoryPath).listFiles(BufferLogWriter$exportLogs$Anon1.INSTANCE);
        kd4.a((Object) listFiles, "parentFolder.listFiles {\u2026EGEX_PATTERN.toRegex()) }");
        return za4.f(listFiles);
    }

    @DexIgnore
    public final synchronized void forceFlushBuffer() {
        writeLog(new FlushLogEvent());
    }

    @DexIgnore
    public final IBufferLogCallback getCallback() {
        return this.callback;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00e8, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00e9, code lost:
        java.lang.System.out.print(r1.toString());
        java.lang.Thread.currentThread().interrupt();
        r13.isRunning = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return;
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0049 A[Catch:{ Exception -> 0x00e8 }] */
    public void run() {
        int i;
        if (!qf4.a(this.logFilePath)) {
            File file = new File(this.logFilePath);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            if (!file.exists()) {
                file.createNewFile();
            } else {
                List<String> b = kv1.b(file, Charset.defaultCharset());
                kd4.a((Object) b, "logLines");
                i = b.size();
                if (i >= this.threshold) {
                    renameToFullBufferFile(file, false);
                }
                while (this.isRunning) {
                    LogEvent poll = this.logEventQueue.poll();
                    boolean z = poll instanceof FlushLogEvent;
                    if (poll != null && !(poll instanceof FinishLogEvent) && !z) {
                        FileOutputStream fileOutputStream = new FileOutputStream(file, true);
                        String str = poll.toString() + "\n";
                        Charset charset = bf4.a;
                        if (str != null) {
                            byte[] bytes = str.getBytes(charset);
                            kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                            fileOutputStream.write(bytes);
                            fileOutputStream.flush();
                            fileOutputStream.close();
                            i++;
                            if (poll.getLogLevel() == FLogger.LogLevel.SUMMARY) {
                                fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new BufferLogWriter$run$Anon1(this, poll, (yb4) null), 3, (Object) null);
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    if (i >= this.threshold || z) {
                        renameToFullBufferFile(file, z);
                        i = 0;
                    }
                    if (poll instanceof FinishLogEvent) {
                        fi4 unused2 = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new BufferLogWriter$run$Anon2(this, (yb4) null), 3, (Object) null);
                    }
                    if (poll == null) {
                        synchronized (this.mLocker) {
                            this.mLocker.wait();
                            qa4 qa4 = qa4.a;
                        }
                    }
                }
            }
            i = 0;
            while (this.isRunning) {
            }
        }
    }

    @DexIgnore
    public final void setCallback(IBufferLogCallback iBufferLogCallback) {
        this.callback = iBufferLogCallback;
    }

    @DexIgnore
    public final void startWriter(String str, boolean z, IBufferLogCallback iBufferLogCallback) {
        String str2;
        kd4.b(str, "directoryPath");
        kd4.b(iBufferLogCallback, Constants.CALLBACK);
        this.callback = iBufferLogCallback;
        if (!this.isRunning) {
            this.directoryPath = str + File.separatorChar + LOG_FOLDER;
            if (!qf4.a(this.directoryPath)) {
                StringBuilder sb = new StringBuilder();
                sb.append(this.directoryPath);
                sb.append(File.separatorChar);
                pd4 pd4 = pd4.a;
                Object[] objArr = {this.floggerName, Locale.US};
                String format = String.format(BUFFER_FILE_NAME, Arrays.copyOf(objArr, objArr.length));
                kd4.a((Object) format, "java.lang.String.format(format, *args)");
                sb.append(format);
                str2 = sb.toString();
            } else {
                str2 = "";
            }
            this.logFilePath = str2;
            this.isMainFLogger = z;
            this.isRunning = true;
            start();
        }
    }

    @DexIgnore
    public final void stopWriter() {
        this.isRunning = false;
    }

    @DexIgnore
    public final synchronized void writeLog(LogEvent logEvent) {
        kd4.b(logEvent, "logEvent");
        this.logEventQueue.add(logEvent);
        synchronized (this.mLocker) {
            this.mLocker.notifyAll();
            qa4 qa4 = qa4.a;
        }
    }

    @DexIgnore
    public final void startWriter(String str, boolean z, IBufferLogCallback iBufferLogCallback, BufferDebugOption bufferDebugOption) {
        kd4.b(str, "directoryPath");
        kd4.b(iBufferLogCallback, Constants.CALLBACK);
        this.debugOption = bufferDebugOption;
        startWriter(str, z, iBufferLogCallback);
    }
}
