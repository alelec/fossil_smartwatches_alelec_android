package com.misfit.frameworks.buttonservice.log.p004db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.log.db.LogDao_Impl */
public final class LogDao_Impl implements com.misfit.frameworks.buttonservice.log.p004db.LogDao {
    @DexIgnore
    public /* final */ androidx.room.RoomDatabase __db;
    @DexIgnore
    public /* final */ com.fossil.blesdk.obfuscated.C2322lf __insertionAdapterOfLog;
    @DexIgnore
    public /* final */ com.misfit.frameworks.buttonservice.log.p004db.LogFlagConverter __logFlagConverter; // = new com.misfit.frameworks.buttonservice.log.p004db.LogFlagConverter();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.misfit.frameworks.buttonservice.log.db.LogDao_Impl$1")
    /* renamed from: com.misfit.frameworks.buttonservice.log.db.LogDao_Impl$1 */
    public class C55701 extends com.fossil.blesdk.obfuscated.C2322lf<com.misfit.frameworks.buttonservice.log.p004db.Log> {
        @DexIgnore
        public C55701(androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "INSERT OR REPLACE INTO `log`(`id`,`timeStamp`,`content`,`cloudFlag`) VALUES (nullif(?, 0),?,?,?)";
        }

        @DexIgnore
        public void bind(com.fossil.blesdk.obfuscated.C2221kg kgVar, com.misfit.frameworks.buttonservice.log.p004db.Log log) {
            kgVar.mo11934b(1, (long) log.getId());
            kgVar.mo11934b(2, log.getTimeStamp());
            if (log.getContent() == null) {
                kgVar.mo11930a(3);
            } else {
                kgVar.mo11932a(3, log.getContent());
            }
            java.lang.String logFlagEnumToString = com.misfit.frameworks.buttonservice.log.p004db.LogDao_Impl.this.__logFlagConverter.logFlagEnumToString(log.getCloudFlag());
            if (logFlagEnumToString == null) {
                kgVar.mo11930a(4);
            } else {
                kgVar.mo11932a(4, logFlagEnumToString);
            }
        }
    }

    @DexIgnore
    public LogDao_Impl(androidx.room.RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfLog = new com.misfit.frameworks.buttonservice.log.p004db.LogDao_Impl.C55701(roomDatabase);
    }

    @DexIgnore
    public int countExcept(com.misfit.frameworks.buttonservice.log.p004db.Log.Flag flag) {
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT COUNT(id) FROM log WHERE cloudFlag != ?", 1);
        java.lang.String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            b.mo11930a(1);
        } else {
            b.mo11932a(1, logFlagEnumToString);
        }
        this.__db.assertNotSuspendingTransaction();
        int i = 0;
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.__db, b, false);
        try {
            if (a.moveToFirst()) {
                i = a.getInt(0);
            }
            return i;
        } finally {
            a.close();
            b.mo16767c();
        }
    }

    @DexIgnore
    public int delete(java.util.List<java.lang.Integer> list) {
        this.__db.assertNotSuspendingTransaction();
        java.lang.StringBuilder a = com.fossil.blesdk.obfuscated.C1632dg.m5847a();
        a.append("DELETE FROM log WHERE id IN (");
        com.fossil.blesdk.obfuscated.C1632dg.m5848a(a, list.size());
        a.append(")");
        com.fossil.blesdk.obfuscated.C2221kg compileStatement = this.__db.compileStatement(a.toString());
        int i = 1;
        for (java.lang.Integer next : list) {
            if (next == null) {
                compileStatement.mo11930a(i);
            } else {
                compileStatement.mo11934b(i, (long) next.intValue());
            }
            i++;
        }
        this.__db.beginTransaction();
        try {
            int n = compileStatement.mo12781n();
            this.__db.setTransactionSuccessful();
            return n;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public java.util.List<com.misfit.frameworks.buttonservice.log.p004db.Log> getAllLogEventsExcept(com.misfit.frameworks.buttonservice.log.p004db.Log.Flag flag) {
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT * FROM log WHERE cloudFlag != ?", 1);
        java.lang.String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            b.mo11930a(1);
        } else {
            b.mo11932a(1, logFlagEnumToString);
        }
        this.__db.assertNotSuspendingTransaction();
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.__db, b, false);
        try {
            int b2 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "id");
            int b3 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "timeStamp");
            int b4 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "content");
            int b5 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "cloudFlag");
            java.util.ArrayList arrayList = new java.util.ArrayList(a.getCount());
            while (a.moveToNext()) {
                com.misfit.frameworks.buttonservice.log.p004db.Log log = new com.misfit.frameworks.buttonservice.log.p004db.Log(a.getLong(b3), a.getString(b4), this.__logFlagConverter.stringToLogFlag(a.getString(b5)));
                log.setId(a.getInt(b2));
                arrayList.add(log);
            }
            return arrayList;
        } finally {
            a.close();
            b.mo16767c();
        }
    }

    @DexIgnore
    public void insertLogEvent(java.util.List<com.misfit.frameworks.buttonservice.log.p004db.Log> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfLog.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void updateCloudFlagByIds(java.util.List<java.lang.Integer> list, com.misfit.frameworks.buttonservice.log.p004db.Log.Flag flag) {
        this.__db.assertNotSuspendingTransaction();
        java.lang.StringBuilder a = com.fossil.blesdk.obfuscated.C1632dg.m5847a();
        a.append("UPDATE log SET cloudFlag = ");
        a.append("?");
        a.append(" WHERE id IN (");
        com.fossil.blesdk.obfuscated.C1632dg.m5848a(a, list.size());
        a.append(")");
        com.fossil.blesdk.obfuscated.C2221kg compileStatement = this.__db.compileStatement(a.toString());
        java.lang.String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            compileStatement.mo11930a(1);
        } else {
            compileStatement.mo11932a(1, logFlagEnumToString);
        }
        int i = 2;
        for (java.lang.Integer next : list) {
            if (next == null) {
                compileStatement.mo11930a(i);
            } else {
                compileStatement.mo11934b(i, (long) next.intValue());
            }
            i++;
        }
        this.__db.beginTransaction();
        try {
            compileStatement.mo12781n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
