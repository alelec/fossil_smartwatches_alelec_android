package com.misfit.frameworks.buttonservice.log.db;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.dg;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.misfit.frameworks.buttonservice.log.db.Log;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogDao_Impl implements LogDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfLog;
    @DexIgnore
    public /* final */ LogFlagConverter __logFlagConverter; // = new LogFlagConverter();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<Log> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `log`(`id`,`timeStamp`,`content`,`cloudFlag`) VALUES (nullif(?, 0),?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, Log log) {
            kgVar.b(1, (long) log.getId());
            kgVar.b(2, log.getTimeStamp());
            if (log.getContent() == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, log.getContent());
            }
            String logFlagEnumToString = LogDao_Impl.this.__logFlagConverter.logFlagEnumToString(log.getCloudFlag());
            if (logFlagEnumToString == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, logFlagEnumToString);
            }
        }
    }

    @DexIgnore
    public LogDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfLog = new Anon1(roomDatabase);
    }

    @DexIgnore
    public int countExcept(Log.Flag flag) {
        uf b = uf.b("SELECT COUNT(id) FROM log WHERE cloudFlag != ?", 1);
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            b.a(1);
        } else {
            b.a(1, logFlagEnumToString);
        }
        this.__db.assertNotSuspendingTransaction();
        int i = 0;
        Cursor a = bg.a(this.__db, b, false);
        try {
            if (a.moveToFirst()) {
                i = a.getInt(0);
            }
            return i;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public int delete(List<Integer> list) {
        this.__db.assertNotSuspendingTransaction();
        StringBuilder a = dg.a();
        a.append("DELETE FROM log WHERE id IN (");
        dg.a(a, list.size());
        a.append(")");
        kg compileStatement = this.__db.compileStatement(a.toString());
        int i = 1;
        for (Integer next : list) {
            if (next == null) {
                compileStatement.a(i);
            } else {
                compileStatement.b(i, (long) next.intValue());
            }
            i++;
        }
        this.__db.beginTransaction();
        try {
            int n = compileStatement.n();
            this.__db.setTransactionSuccessful();
            return n;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<Log> getAllLogEventsExcept(Log.Flag flag) {
        uf b = uf.b("SELECT * FROM log WHERE cloudFlag != ?", 1);
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            b.a(1);
        } else {
            b.a(1, logFlagEnumToString);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "timeStamp");
            int b4 = ag.b(a, "content");
            int b5 = ag.b(a, "cloudFlag");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                Log log = new Log(a.getLong(b3), a.getString(b4), this.__logFlagConverter.stringToLogFlag(a.getString(b5)));
                log.setId(a.getInt(b2));
                arrayList.add(log);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertLogEvent(List<Log> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfLog.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void updateCloudFlagByIds(List<Integer> list, Log.Flag flag) {
        this.__db.assertNotSuspendingTransaction();
        StringBuilder a = dg.a();
        a.append("UPDATE log SET cloudFlag = ");
        a.append("?");
        a.append(" WHERE id IN (");
        dg.a(a, list.size());
        a.append(")");
        kg compileStatement = this.__db.compileStatement(a.toString());
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            compileStatement.a(1);
        } else {
            compileStatement.a(1, logFlagEnumToString);
        }
        int i = 2;
        for (Integer next : list) {
            if (next == null) {
                compileStatement.a(i);
            } else {
                compileStatement.b(i, (long) next.intValue());
            }
            i++;
        }
        this.__db.beginTransaction();
        try {
            compileStatement.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
