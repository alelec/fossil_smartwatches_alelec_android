package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DBLogWriter$flushTo$Anon2$syncingLogIds$Anon1 extends Lambda implements xc4<LogEvent, Integer> {
    @DexIgnore
    public static /* final */ DBLogWriter$flushTo$Anon2$syncingLogIds$Anon1 INSTANCE; // = new DBLogWriter$flushTo$Anon2$syncingLogIds$Anon1();

    @DexIgnore
    public DBLogWriter$flushTo$Anon2$syncingLogIds$Anon1() {
        super(1);
    }

    @DexIgnore
    public final Integer invoke(LogEvent logEvent) {
        kd4.b(logEvent, "it");
        Object tag = logEvent.getTag();
        if (!(tag instanceof Integer)) {
            tag = null;
        }
        return (Integer) tag;
    }
}
