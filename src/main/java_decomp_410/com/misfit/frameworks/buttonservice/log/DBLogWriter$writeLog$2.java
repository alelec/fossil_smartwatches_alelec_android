package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$writeLog$2", mo27670f = "DBLogWriter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class DBLogWriter$writeLog$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20852p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.log.DBLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$writeLog$2(com.misfit.frameworks.buttonservice.log.DBLogWriter dBLogWriter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = dBLogWriter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.misfit.frameworks.buttonservice.log.DBLogWriter$writeLog$2 dBLogWriter$writeLog$2 = new com.misfit.frameworks.buttonservice.log.DBLogWriter$writeLog$2(this.this$0, yb4);
        dBLogWriter$writeLog$2.f20852p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dBLogWriter$writeLog$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.misfit.frameworks.buttonservice.log.DBLogWriter$writeLog$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            int countExcept = this.this$0.logDao.countExcept(com.misfit.frameworks.buttonservice.log.p004db.Log.Flag.SYNCING);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String access$getTAG$cp = com.misfit.frameworks.buttonservice.log.DBLogWriter.TAG;
            local.mo33255d(access$getTAG$cp, ".writeLog(), dbCount=" + countExcept);
            if (countExcept >= this.this$0.thresholdValue) {
                com.misfit.frameworks.buttonservice.log.DBLogWriter.IDBLogWriterCallback access$getCallback$p = this.this$0.callback;
                if (access$getCallback$p != null) {
                    access$getCallback$p.onReachDBThreshold();
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
