package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.bc4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.ic4;
import com.fossil.blesdk.obfuscated.yb4;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogEndPointKt {
    @DexIgnore
    public static final <T> Object await(Call<T> call, yb4<? super RepoResponse<T>> yb4) {
        bc4 bc4 = new bc4(IntrinsicsKt__IntrinsicsJvmKt.a(yb4));
        call.a(new LogEndPointKt$await$Anon2$Anon1(bc4));
        Object a = bc4.a();
        if (a == cc4.a()) {
            ic4.c(yb4);
        }
        return a;
    }
}
