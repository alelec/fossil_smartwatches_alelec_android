package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.BufferLogWriter;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BufferLogWriter$renameToFullBufferFile$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $logs;
    @DexIgnore
    public /* final */ /* synthetic */ zg4 $this_launch$inlined;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BufferLogWriter$renameToFullBufferFile$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$renameToFullBufferFile$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(List list, yb4 yb4, BufferLogWriter$renameToFullBufferFile$Anon1 bufferLogWriter$renameToFullBufferFile$Anon1, zg4 zg4) {
        super(2, yb4);
        this.$logs = list;
        this.this$Anon0 = bufferLogWriter$renameToFullBufferFile$Anon1;
        this.$this_launch$inlined = zg4;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        BufferLogWriter$renameToFullBufferFile$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 bufferLogWriter$renameToFullBufferFile$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 = new BufferLogWriter$renameToFullBufferFile$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(this.$logs, yb4, this.this$Anon0, this.$this_launch$inlined);
        bufferLogWriter$renameToFullBufferFile$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1.p$ = (zg4) obj;
        return bufferLogWriter$renameToFullBufferFile$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BufferLogWriter$renameToFullBufferFile$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            BufferLogWriter.IBufferLogCallback callback = this.this$Anon0.this$Anon0.getCallback();
            if (callback == null) {
                return null;
            }
            callback.onFullBuffer(this.this$Anon0.this$Anon0.toLogEvents(this.$logs), this.this$Anon0.$forceFlush);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
