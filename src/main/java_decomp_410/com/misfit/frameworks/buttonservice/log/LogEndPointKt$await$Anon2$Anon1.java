package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.er4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.yb4;
import kotlin.Result;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogEndPointKt$await$Anon2$Anon1 implements er4<T> {
    @DexIgnore
    public /* final */ /* synthetic */ yb4 $continuation;

    @DexIgnore
    public LogEndPointKt$await$Anon2$Anon1(yb4 yb4) {
        this.$continuation = yb4;
    }

    @DexIgnore
    public void onFailure(Call<T> call, Throwable th) {
        kd4.b(th, "t");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("await", "onFailure=" + th);
        yb4 yb4 = this.$continuation;
        Failure create = RepoResponse.Companion.create(th);
        Result.a aVar = Result.Companion;
        yb4.resumeWith(Result.m3constructorimpl(create));
    }

    @DexIgnore
    public void onResponse(Call<T> call, qr4<T> qr4) {
        kd4.b(qr4, "response");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("await", "onResponse=" + qr4);
        yb4 yb4 = this.$continuation;
        RepoResponse<T> create = RepoResponse.Companion.create(qr4);
        Result.a aVar = Result.Companion;
        yb4.resumeWith(Result.m3constructorimpl(create));
    }
}
