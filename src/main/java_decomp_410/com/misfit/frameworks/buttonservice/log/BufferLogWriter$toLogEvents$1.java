package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BufferLogWriter$toLogEvents$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<java.lang.String, com.misfit.frameworks.buttonservice.log.LogEvent> {
    @DexIgnore
    public /* final */ /* synthetic */ com.google.gson.Gson $gson;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$toLogEvents$1(com.google.gson.Gson gson) {
        super(1);
        this.$gson = gson;
    }

    @DexIgnore
    public final com.misfit.frameworks.buttonservice.log.LogEvent invoke(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "it");
        try {
            return (com.misfit.frameworks.buttonservice.log.LogEvent) this.$gson.mo23093a(str, com.misfit.frameworks.buttonservice.log.LogEvent.class);
        } catch (java.lang.Exception unused) {
            return null;
        }
    }
}
