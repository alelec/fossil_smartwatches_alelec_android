package com.misfit.frameworks.buttonservice.log.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CloudLogConfig implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    public /* final */ String accessKey;
    @DexIgnore
    public /* final */ String endPointBaseUrl;
    @DexIgnore
    public /* final */ String logBrandName;
    @DexIgnore
    public /* final */ String secretKey;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<CloudLogConfig> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public CloudLogConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new CloudLogConfig(parcel);
        }

        @DexIgnore
        public CloudLogConfig[] newArray(int i) {
            return new CloudLogConfig[i];
        }
    }

    @DexIgnore
    public CloudLogConfig(String str, String str2, String str3, String str4) {
        kd4.b(str, "logBrandName");
        kd4.b(str2, "endPointBaseUrl");
        kd4.b(str3, "accessKey");
        kd4.b(str4, "secretKey");
        this.logBrandName = str;
        this.endPointBaseUrl = str2;
        this.accessKey = str3;
        this.secretKey = str4;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getAccessKey() {
        return this.accessKey;
    }

    @DexIgnore
    public final String getEndPointBaseUrl() {
        return this.endPointBaseUrl;
    }

    @DexIgnore
    public final String getLogBrandName() {
        return this.logBrandName;
    }

    @DexIgnore
    public final String getSecretKey() {
        return this.secretKey;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(this.logBrandName);
        parcel.writeString(this.endPointBaseUrl);
        parcel.writeString(this.accessKey);
        parcel.writeString(this.secretKey);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public CloudLogConfig(Parcel parcel) {
        throw null;
/*        this(r0, r2, r3, r5 == null ? "" : r5);
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        readString = readString == null ? "" : readString;
        String readString2 = parcel.readString();
        readString2 = readString2 == null ? "" : readString2;
        String readString3 = parcel.readString();
        readString3 = readString3 == null ? "" : readString3;
        String readString4 = parcel.readString();
*/    }
}
