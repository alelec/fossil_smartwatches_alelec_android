package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogEndPointKt$await$2$1 implements com.fossil.blesdk.obfuscated.er4<T> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.yb4 $continuation;

    @DexIgnore
    public LogEndPointKt$await$2$1(com.fossil.blesdk.obfuscated.yb4 yb4) {
        this.$continuation = yb4;
    }

    @DexIgnore
    public void onFailure(retrofit2.Call<T> call, java.lang.Throwable th) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(th, "t");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d("await", "onFailure=" + th);
        com.fossil.blesdk.obfuscated.yb4 yb4 = this.$continuation;
        com.misfit.frameworks.buttonservice.log.Failure create = com.misfit.frameworks.buttonservice.log.RepoResponse.Companion.create(th);
        kotlin.Result.C7350a aVar = kotlin.Result.Companion;
        yb4.resumeWith(kotlin.Result.m37419constructorimpl(create));
    }

    @DexIgnore
    public void onResponse(retrofit2.Call<T> call, com.fossil.blesdk.obfuscated.qr4<T> qr4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(qr4, "response");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d("await", "onResponse=" + qr4);
        com.fossil.blesdk.obfuscated.yb4 yb4 = this.$continuation;
        com.misfit.frameworks.buttonservice.log.RepoResponse<T> create = com.misfit.frameworks.buttonservice.log.RepoResponse.Companion.create(qr4);
        kotlin.Result.C7350a aVar = kotlin.Result.Companion;
        yb4.resumeWith(kotlin.Result.m37419constructorimpl(create));
    }
}
