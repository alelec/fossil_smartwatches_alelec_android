package com.misfit.frameworks.buttonservice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ButtonService$buttonServiceHub$1 extends com.misfit.frameworks.buttonservice.IButtonConnectivity.Stub {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.ButtonService this$0;

    @DexIgnore
    public ButtonService$buttonServiceHub$1(com.misfit.frameworks.buttonservice.ButtonService buttonService) {
        this.this$0 = buttonService;
    }

    @DexIgnore
    public void addLog(int i, java.lang.String str, java.lang.String str2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str2, "message");
        if (this.this$0.isBleSupported(str)) {
            this.this$0.addLog(i, str, str2);
        }
    }

    @DexIgnore
    public void cancelPairDevice(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            this.this$0.cancelPairDevice(str);
        }
    }

    @DexIgnore
    public void changePendingLogKey(int i, java.lang.String str, int i2, java.lang.String str2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "curSerial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str2, "newSerial");
        if (this.this$0.isBleSupported(str2)) {
            this.this$0.changePendingLogKey(i, str, i2, str2);
        }
    }

    @DexIgnore
    public void confirmStopWorkout(java.lang.String str, boolean z) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (this.this$0.isBleSupported()) {
            this.this$0.confirmStopWorkout(str, z);
        }
    }

    @DexIgnore
    public void connectAllButton() throws android.os.RemoteException {
        if (this.this$0.isBleSupported()) {
            this.this$0.connectAllButton();
        }
    }

    @DexIgnore
    public void deleteDataFiles(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            this.this$0.deleteDataFiles(str);
        }
    }

    @DexIgnore
    public void deleteHeartRateFiles(java.util.List<java.lang.String> list, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "fileIds");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        this.this$0.deleteHeartRateFiles(list);
    }

    @DexIgnore
    public long deviceCancelCalibration(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.cancelCalibration(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.EXIT_CALIBRATION, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceClearMapping(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceClearMapping(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.CLEAN_LINK_MAPPINGS, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceCompleteCalibration(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceCompleteCalibration(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.APPLY_HAND_POSITION, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void deviceDisconnect(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.DISCONNECT, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        } else {
            this.this$0.deviceDisconnect(str);
        }
    }

    @DexIgnore
    public long deviceForceReconnect(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.forceConnect(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.FORCE_CONNECT, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceGetBatteryLevel(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        return this.this$0.isBleSupported(str) ? this.this$0.deviceGetBatteryLevel(str) : com.misfit.frameworks.buttonservice.ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public long deviceGetCountDown(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.GET_COUNTDOWN, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceGetRssi(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        return this.this$0.isBleSupported(str) ? this.this$0.deviceGetRssi(str) : com.misfit.frameworks.buttonservice.ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public void deviceMovingHand(java.lang.String str, com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj handCalibrationObj) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(handCalibrationObj, "handCalibrationObj");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.MOVE_HAND, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        } else {
            this.this$0.deviceMovingHand(str, handCalibrationObj);
        }
    }

    @DexIgnore
    public long deviceOta(java.lang.String str, com.misfit.frameworks.buttonservice.model.FirmwareData firmwareData, com.misfit.frameworks.buttonservice.model.UserProfile userProfile) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str) && !android.text.TextUtils.isEmpty(str) && firmwareData != null && userProfile != null) {
            return this.this$0.deviceOta(str, firmwareData, userProfile);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.OTA, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long devicePlayAnimation(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        return this.this$0.isBleSupported(str) ? this.this$0.devicePlayAnimation(str) : com.misfit.frameworks.buttonservice.ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public long deviceReadRealTimeStep(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        return this.this$0.isBleSupported(str) ? this.this$0.deviceReadRealTimeStep(str) : com.misfit.frameworks.buttonservice.ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public long deviceSendNotification(java.lang.String str, com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj notificationBaseObj) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(notificationBaseObj, "newNotification");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceSendNotification(str, notificationBaseObj);
        }
        return com.misfit.frameworks.buttonservice.ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public void deviceSetAutoCountdownSetting(long j, long j2) {
    }

    @DexIgnore
    public void deviceSetAutoListAlarm(java.util.List<? extends com.misfit.frameworks.buttonservice.model.Alarm> list) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "alarmList");
        this.this$0.deviceSetAutoListAlarm(list);
    }

    @DexIgnore
    public void deviceSetAutoSecondTimezone(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "secondTimezoneId");
        this.this$0.deviceSetAutoSecondTimezone(str);
    }

    @DexIgnore
    public long deviceSetDisableCountDown(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_COUNTDOWN, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceSetEnableCountDown(java.lang.String str, long j, long j2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_COUNTDOWN, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceSetInactiveNudgeConfig(java.lang.String str, com.misfit.frameworks.buttonservice.model.InactiveNudgeData inactiveNudgeData) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(inactiveNudgeData, "inactiveNudgeData");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceSetInactiveNudgeConfig(str, inactiveNudgeData);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_INACTIVE_NUDGE_CONFIG, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceSetListAlarm(java.lang.String str, java.util.List<? extends com.misfit.frameworks.buttonservice.model.Alarm> list) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "alarms");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceSetListAlarm(str, list);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_LIST_ALARM, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceSetMapping(java.lang.String str, java.util.List<? extends com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping> list) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "mappings");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceSetMapping(str, list);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_LINK_MAPPING, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceSetSecondTimeZone(java.lang.String str, java.lang.String str2) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str2, "secondTimezoneId");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceSetSecondTimeZone(str, str2);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_SECOND_TIMEZONE, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceSetVibrationStrength(java.lang.String str, com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj vibrationStrengthObj) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(vibrationStrengthObj, "vibrationStrengthLevelObj");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceSetVibrationStrength(str, vibrationStrengthObj);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_VIBRATION_STRENGTH, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceStartCalibration(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceStartCalibration(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.ENTER_CALIBRATION, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void deviceStartScan() throws android.os.RemoteException {
        if (this.this$0.isBleSupported()) {
            com.misfit.frameworks.buttonservice.ButtonService.access$getScanServiceInstance$p(this.this$0).startScan();
        }
    }

    @DexIgnore
    public long deviceStartSync(java.lang.String str, com.misfit.frameworks.buttonservice.model.UserProfile userProfile) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(userProfile, "profile");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceStartSync(str, userProfile);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void deviceStopScan() throws android.os.RemoteException {
        if (this.this$0.isBleSupported()) {
            com.misfit.frameworks.buttonservice.ButtonService.access$getScanServiceInstance$p(this.this$0).stopScan();
        }
    }

    @DexIgnore
    public long deviceUnlink(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceUnlink(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.UNLINK, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceUpdateGoalStep(java.lang.String str, int i) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        return this.this$0.isBleSupported(str) ? this.this$0.deviceUpdateGoalStep(str, i) : com.misfit.frameworks.buttonservice.ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public long disableHeartRateNotification(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceDisableHeartRate(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.DISABLE_HEART_RATE_NOTIFICATION, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long enableHeartRateNotification(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceEnableHeartRate(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.ENABLE_HEART_RATE_NOTIFICATION, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public int endLog(int i, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.endLog(i, str);
        }
        return 0;
    }

    @DexIgnore
    public boolean forceSwitchDeviceWithoutErase(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "newActiveDeviceSerial");
        if (!this.this$0.isBleSupported(str)) {
            return false;
        }
        this.this$0.forceSwitchDeviceWithoutErase(str);
        return true;
    }

    @DexIgnore
    public void forceUpdateDeviceData(com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse deviceAppResponse, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceAppResponse, "deviceAppResponse");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        this.this$0.sendDeviceAppResponse(deviceAppResponse, str, true);
    }

    @DexIgnore
    public java.util.List<java.lang.String> getActiveSerial() throws android.os.RemoteException {
        java.util.List<java.lang.String> allActiveButtonSerial = com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils.getAllActiveButtonSerial(this.this$0);
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) allActiveButtonSerial, "DevicePreferenceUtils.ge\u2026erial(this@ButtonService)");
        return allActiveButtonSerial;
    }

    @DexIgnore
    public java.util.List<com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping> getAutoMapping(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.getAutoMapping(str);
        }
        return new java.util.ArrayList();
    }

    @DexIgnore
    public int getCommunicatorModeBySerial(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.getActiveCommunicatorBySerial(str);
        }
        return com.misfit.frameworks.buttonservice.communite.CommunicateMode.IDLE.getValue();
    }

    @DexIgnore
    public com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile getDeviceProfile(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.getDeviceProfile(str);
        }
        return null;
    }

    @DexIgnore
    public int getGattState(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.getGattState(str);
        }
        return 0;
    }

    @DexIgnore
    public int getHIDState(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.getHIDState(str);
        }
        return 0;
    }

    @DexIgnore
    public int[] getListActiveCommunicator() throws android.os.RemoteException {
        return this.this$0.getActiveListCommunicator();
    }

    @DexIgnore
    public java.util.List<com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile> getPairedDevice() throws android.os.RemoteException {
        if (this.this$0.isBleSupported()) {
            return this.this$0.getPairedDevice();
        }
        return new java.util.ArrayList();
    }

    @DexIgnore
    public java.util.List<java.lang.String> getPairedSerial() throws android.os.RemoteException {
        java.util.List<java.lang.String> allPairedButtonSerial = com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils.getAllPairedButtonSerial(this.this$0);
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) allPairedButtonSerial, "DevicePreferenceUtils.ge\u2026erial(this@ButtonService)");
        return allPairedButtonSerial;
    }

    @DexIgnore
    public java.util.List<com.fossil.fitness.FitnessData> getSyncData(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.getSyncData(str);
        }
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.misfit.frameworks.buttonservice.ButtonService.TAG, ".getSyncData(), device is not support");
        return new java.util.ArrayList();
    }

    @DexIgnore
    public void init(java.lang.String str, java.lang.String str2, java.lang.String str3, char c, com.misfit.frameworks.buttonservice.log.model.AppLogInfo appLogInfo, com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo activeDeviceInfo, com.misfit.frameworks.buttonservice.log.model.CloudLogConfig cloudLogConfig) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "sdkLogV2EndPoint");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str2, "sdkLogV2AccessKey");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str3, "sdkLogV2SecretKey");
        com.fossil.blesdk.obfuscated.kd4.m24411b(appLogInfo, "appLogInfo");
        com.fossil.blesdk.obfuscated.kd4.m24411b(activeDeviceInfo, "activeDeviceInfo");
        com.fossil.blesdk.obfuscated.kd4.m24411b(cloudLogConfig, "cloudLogConfig");
        com.misfit.frameworks.buttonservice.ButtonService.fossilBrand = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.BRAND.fromPrefix(c);
        com.misfit.frameworks.buttonservice.ButtonService.Companion.setAppInfo(new com.misfit.frameworks.buttonservice.model.AppInfo(appLogInfo.getAppVersion(), appLogInfo.getPlatformVersion()));
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.updateAppLogInfo(appLogInfo);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.updateActiveDeviceInfo(activeDeviceInfo);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.updateCloudLogConfig(cloudLogConfig);
        if ((!com.fossil.blesdk.obfuscated.qf4.m26950a(str)) && (!com.fossil.blesdk.obfuscated.qf4.m26950a(str2)) && (!com.fossil.blesdk.obfuscated.qf4.m26950a(str3))) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String access$getTAG$cp = com.misfit.frameworks.buttonservice.ButtonService.TAG;
            local.mo33255d(access$getTAG$cp, "init sdk end point " + str);
            com.fossil.blesdk.obfuscated.xa0.f10879c.mo17641a(new com.fossil.blesdk.obfuscated.na0(str, str2, str3));
        }
    }

    @DexIgnore
    public void interrupt(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.INTERRUPT, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        } else {
            this.this$0.interrupt(str);
        }
    }

    @DexIgnore
    public void interruptCurrentSession(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        this.this$0.interruptCurrentSession(str);
    }

    @DexIgnore
    public boolean isLinking(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        return this.this$0.isBleSupported(str) && this.this$0.isLinking(str);
    }

    @DexIgnore
    public boolean isSyncing(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        return this.this$0.isSyncing(str);
    }

    @DexIgnore
    public boolean isUpdatingFirmware(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        return this.this$0.isBleSupported(str) && this.this$0.isUpdatingFirmware(str);
    }

    @DexIgnore
    public void logOut() throws android.os.RemoteException {
        this.this$0.logOut();
    }

    @DexIgnore
    public long onPing(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        return this.this$0.isBleSupported(str) ? this.this$0.onPing(str) : java.lang.System.currentTimeMillis();
    }

    @DexIgnore
    public void onSetWatchParamResponse(java.lang.String str, boolean z, com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping watchParamsFileMapping) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        this.this$0.onSetWatchParamResponse(str, z, watchParamsFileMapping);
    }

    @DexIgnore
    public long pairDevice(java.lang.String str, java.lang.String str2, com.misfit.frameworks.buttonservice.model.UserProfile userProfile) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str2, "macAddress");
        com.fossil.blesdk.obfuscated.kd4.m24411b(userProfile, "userProfile");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.pairDevice(str, str2, userProfile);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.LINK, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long pairDeviceResponse(java.lang.String str, com.misfit.frameworks.buttonservice.model.pairing.PairingResponse pairingResponse) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(pairingResponse, "response");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.pairDeviceResponse(str, pairingResponse);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.LINK, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long playVibration(java.lang.String str, int i, int i2, boolean z) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.misfit.frameworks.buttonservice.ButtonService.TAG, "playVibration");
        return com.misfit.frameworks.buttonservice.ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public long readCurrentWorkoutSession(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.readCurrentWorkoutSession(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.READ_CURRENT_WORKOUT_SESSION, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void removeActiveSerial(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils.removeActiveButtonSerial(this.this$0, str);
        }
    }

    @DexIgnore
    public void removePairedSerial(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils.removePairedButtonSerial(this.this$0, str);
        }
    }

    @DexIgnore
    public void resetDeviceSettingToDefault(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
    }

    @DexIgnore
    public long resetHandsToZeroDegree(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceResetHandsToZeroDegree(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.RESET_HAND, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long sendCurrentSecretKey(java.lang.String str, java.lang.String str2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.receiveCurrentSecretKey(str, str2);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.EXCHANGE_SECRET_KEY, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void sendCustomCommand(java.lang.String str, com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest customRequest) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(customRequest, com.misfit.frameworks.common.constants.Constants.COMMAND);
        java.lang.System.currentTimeMillis();
        if (!this.this$0.isBleSupported(str)) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String access$getTAG$cp = com.misfit.frameworks.buttonservice.ButtonService.TAG;
            local.mo33255d(access$getTAG$cp, ".sendCustomRequest() with " + str + " is not supported");
            return;
        }
        this.this$0.sendCustomCommand(str, customRequest);
    }

    @DexIgnore
    public void sendDeviceAppResponse(com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse deviceAppResponse, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceAppResponse, "deviceAppResponse");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.misfit.frameworks.buttonservice.ButtonService.sendDeviceAppResponse$default(this.this$0, deviceAppResponse, str, false, 4, (java.lang.Object) null);
    }

    @DexIgnore
    public void sendMicroAppRemoteActivity(java.lang.String str, com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse deviceAppResponse) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceAppResponse, "deviceAppResponse");
        if (this.this$0.isBleSupported(str)) {
            this.this$0.sendMicroAppRemoteActivity(str, deviceAppResponse);
        }
    }

    @DexIgnore
    public void sendMusicAppResponse(com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse musicResponse, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(musicResponse, "musicResponse");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        this.this$0.sendMusicAppResponse(musicResponse, str);
    }

    @DexIgnore
    public long sendRandomKey(java.lang.String str, java.lang.String str2, int i) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str2, "randomKey");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.receiveRandomKey(str, str2, i);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.EXCHANGE_SECRET_KEY, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long sendServerSecretKey(java.lang.String str, java.lang.String str2, int i) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str2, "serverSecretKey");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.receiveServerSecretKey(str, str2, i);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.EXCHANGE_SECRET_KEY, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void setActiveSerial(java.lang.String str, java.lang.String str2) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str2, "macAddress");
        if (this.this$0.isBleSupported(str)) {
            this.this$0.setActiveSerial(str, str2);
        }
    }

    @DexIgnore
    public void setAutoBackgroundImageConfig(com.misfit.frameworks.buttonservice.model.background.BackgroundConfig backgroundConfig, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(backgroundConfig, "backgroundConfig");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_AUTO_BACKGROUND_IMAGE_CONFIG, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        } else {
            this.this$0.setAutoBackgroundImageConfig(backgroundConfig, str);
        }
    }

    @DexIgnore
    public void setAutoComplicationAppSettings(com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings complicationAppMappingSettings, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(complicationAppMappingSettings, "complicationAppMappingSettings");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_AUTO_COMPLICATION_APPS, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        } else {
            this.this$0.setAutoComplicationApps(complicationAppMappingSettings, str);
        }
    }

    @DexIgnore
    public void setAutoMapping(java.lang.String str, java.util.List<? extends com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping> list) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "mappings");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_AUTO_MAPPING, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        } else {
            this.this$0.setAutoMapping(str, list);
        }
    }

    @DexIgnore
    public void setAutoNotificationFilterSettings(com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings appNotificationFilterSettings, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(appNotificationFilterSettings, "notificationFilterSettings");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        } else {
            this.this$0.setAutoNotificationFilterSettings(appNotificationFilterSettings, str);
        }
    }

    @DexIgnore
    public void setAutoUserBiometricData(com.misfit.frameworks.buttonservice.model.UserProfile userProfile) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(userProfile, "userProfile");
        this.this$0.setAutoUserBiometricData(userProfile);
    }

    @DexIgnore
    public void setAutoWatchAppSettings(com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings watchAppMappingSettings, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(watchAppMappingSettings, "watchAppMappingSettings");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_AUTO_WATCH_APPS, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        } else {
            this.this$0.setAutoWatchApps(watchAppMappingSettings, str);
        }
    }

    @DexIgnore
    public long setBackgroundImageConfig(com.misfit.frameworks.buttonservice.model.background.BackgroundConfig backgroundConfig, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(backgroundConfig, "backgroundConfig");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setBackgroundImageConfig(backgroundConfig, str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_BACKGROUND_IMAGE_CONFIG, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long setComplicationApps(com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings complicationAppMappingSettings, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(complicationAppMappingSettings, "complicationAppMappingSettings");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setCompilationApps(complicationAppMappingSettings, str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_COMPLICATION_APPS, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long setFrontLightEnable(java.lang.String str, boolean z) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setFrontLightEnable(str, z);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_FRONT_LIGHT_ENABLE, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long setHeartRateMode(java.lang.String str, int i) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        com.misfit.frameworks.buttonservice.enums.HeartRateMode fromValue = com.misfit.frameworks.buttonservice.enums.HeartRateMode.Companion.fromValue(i);
        if (this.this$0.isBleSupported(str) && fromValue != com.misfit.frameworks.buttonservice.enums.HeartRateMode.NONE) {
            return this.this$0.setHeartRateMode(str, fromValue);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_HEART_RATE_MODE, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void setImplicitDeviceConfig(com.misfit.frameworks.buttonservice.model.UserProfile userProfile, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(userProfile, "userProfile");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_IMPLICIT_DEVICE_CONFIG, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        } else {
            this.this$0.setImplicitDeviceConfig(userProfile, str);
        }
    }

    @DexIgnore
    public void setImplicitDisplayUnitSettings(com.misfit.frameworks.buttonservice.model.UserDisplayUnit userDisplayUnit, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(userDisplayUnit, "userDisplayUnit");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_IMPLICIT_DISPLAY_UNIT, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        } else {
            this.this$0.setImplicitDisplayUnitSettings(userDisplayUnit, str);
        }
    }

    @DexIgnore
    public void setLocalizationData(com.misfit.frameworks.buttonservice.model.LocalizationData localizationData) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(localizationData, "localizationData");
        this.this$0.setLocalizationData(localizationData);
    }

    @DexIgnore
    public long setNotificationFilterSettings(com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings appNotificationFilterSettings, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(appNotificationFilterSettings, "notificationFilterSettings");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setNotificationFilterSettings(appNotificationFilterSettings, str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_NOTIFICATION_FILTERS, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void setPairedSerial(java.lang.String str, java.lang.String str2) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str2, "macAddress");
        if (this.this$0.isBleSupported(str)) {
            this.this$0.setPairedDevice(str, str2);
        }
    }

    @DexIgnore
    public long setPresetApps(com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings watchAppMappingSettings, com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings complicationAppMappingSettings, com.misfit.frameworks.buttonservice.model.background.BackgroundConfig backgroundConfig, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setPresetApps(watchAppMappingSettings, complicationAppMappingSettings, backgroundConfig, str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_PRESET_APPS_DATA, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void setSecretKey(java.lang.String str, java.lang.String str2) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        this.this$0.setSecretKey(str, str2);
    }

    @DexIgnore
    public long setWatchApps(com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings watchAppMappingSettings, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(watchAppMappingSettings, "watchAppMappingSettings");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setWatchApps(watchAppMappingSettings, str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_WATCH_APPS, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void simulateDisconnection(java.lang.String str, int i, int i2, int i3, int i4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
    }

    @DexIgnore
    public void simulatePusherEvent(java.lang.String str, int i, int i2, int i3, int i4, int i5) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
    }

    @DexIgnore
    public int startLog(int i, java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.startLog(i, str);
        }
        return 0;
    }

    @DexIgnore
    public long stopCurrentWorkoutSession(java.lang.String str) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.stopCurrentWorkoutSession(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.STOP_CURRENT_WORKOUT_SESSION, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void stopLogService(int i) {
        this.this$0.stopLogService(i);
    }

    @DexIgnore
    public boolean switchActiveDevice(java.lang.String str, com.misfit.frameworks.buttonservice.model.UserProfile userProfile) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "newActiveDeviceSerial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(userProfile, "userProfile");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.switchActiveDevice(str, userProfile);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SWITCH_DEVICE, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return false;
    }

    @DexIgnore
    public long switchDeviceResponse(java.lang.String str, boolean z, int i) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.switchDeviceResponse(str, z, i);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SWITCH_DEVICE, com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED, (android.os.Bundle) null);
        return com.misfit.frameworks.buttonservice.ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public void updateActiveDeviceInfoLog(com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo activeDeviceInfo) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(activeDeviceInfo, "activeDeviceInfo");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.updateActiveDeviceInfo(activeDeviceInfo);
    }

    @DexIgnore
    public void updateAppInfo(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "appInfoJson");
        this.this$0.updateAppInfo(str);
    }

    @DexIgnore
    public void updateAppLogInfo(com.misfit.frameworks.buttonservice.log.model.AppLogInfo appLogInfo) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(appLogInfo, "appLogInfo");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.updateAppLogInfo(appLogInfo);
    }

    @DexIgnore
    public void updatePercentageGoalProgress(java.lang.String str, boolean z, com.misfit.frameworks.buttonservice.model.UserProfile userProfile) throws android.os.RemoteException {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(userProfile, "userProfile");
        if (this.this$0.isBleSupported(str)) {
            this.this$0.updatePercentageGoalProgress(str, z, userProfile);
        }
    }

    @DexIgnore
    public void updateUserId(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, com.misfit.frameworks.buttonservice.ButtonService.USER_ID);
        this.this$0.updateUserId(str);
    }
}
