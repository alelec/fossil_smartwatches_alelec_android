package com.misfit.frameworks.buttonservice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ButtonService$dequeue$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ButtonService this$Anon0;

    @DexIgnore
    public ButtonService$dequeue$Anon1(ButtonService buttonService) {
        this.this$Anon0 = buttonService;
    }

    @DexIgnore
    public final void run() {
        this.this$Anon0.dequeue();
    }
}
