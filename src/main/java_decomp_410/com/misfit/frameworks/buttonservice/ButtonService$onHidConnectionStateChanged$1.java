package com.misfit.frameworks.buttonservice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ButtonService$onHidConnectionStateChanged$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.ButtonService this$0;

    @DexIgnore
    public ButtonService$onHidConnectionStateChanged$1(com.misfit.frameworks.buttonservice.ButtonService buttonService, java.lang.String str) {
        this.this$0 = buttonService;
        this.$serialNumber = str;
    }

    @DexIgnore
    public final void run() {
        this.this$0.enqueue(this.$serialNumber);
    }
}
