package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MicroAppEventLogger {
    @DexIgnore
    public static /* final */ String LOG_NAME; // = "micro_app_event";
    @DexIgnore
    public static /* final */ String TAG; // = "MicroAppEventLogger";
    @DexIgnore
    public static boolean isDebuggable;
    @DexIgnore
    public static boolean isInitialized;
    @DexIgnore
    public static MicroAppFileHelper microAppFileHelper;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class MicroAppFileHelper {
        @DexIgnore
        public static /* final */ String FILE_NAME_PATTERN; // = "log_%s_%s.txt";
        @DexIgnore
        public static /* final */ long FILE_SIZE; // = 102400;
        @DexIgnore
        public static /* final */ String LOG_PATTERN; // = "%s|%s|%s|%s|%s\n";
        @DexIgnore
        public static /* final */ int MAX_FILE_NUMBER; // = 3;
        @DexIgnore
        public /* final */ String TAG; // = MicroAppFileHelper.class.getSimpleName();
        @DexIgnore
        public String directoryPath;
        @DexIgnore
        public String logName;

        @DexIgnore
        public MicroAppFileHelper(Context context, String str) {
            this.directoryPath = context.getFilesDir().toString();
            this.logName = str;
        }

        @DexIgnore
        private List<File> exportLogs() {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < 3; i++) {
                File file = new File(getFilePath(String.format(FILE_NAME_PATTERN, new Object[]{this.logName, Integer.valueOf(i)})));
                if (file.exists()) {
                    arrayList.add(file);
                }
            }
            return arrayList;
        }

        @DexIgnore
        private String getFilePath(String str) {
            return this.directoryPath + str;
        }

        @DexIgnore
        private void resetLogFiles() {
            for (int i = 2; i >= 0; i--) {
                File file = new File(getFilePath(String.format(FILE_NAME_PATTERN, new Object[]{this.logName, Integer.valueOf(i)})));
                if (file.exists()) {
                    file.delete();
                }
            }
        }

        @DexIgnore
        private synchronized void rotateFiles() throws Exception {
            Log.d(this.TAG, "Inside rotateFiles - Start rotating files...");
            for (int i = 2; i >= 0; i--) {
                File file = new File(getFilePath(String.format(FILE_NAME_PATTERN, new Object[]{this.logName, Integer.valueOf(i)})));
                if (file.exists()) {
                    FileChannel channel = new RandomAccessFile(file, "rw").getChannel();
                    FileLock lock = channel.lock();
                    Log.d(this.TAG, "Inside rotateFiles - Locking " + file.getName());
                    if (i >= 2) {
                        Log.d(this.TAG, "Inside rotateFiles - Deleting " + String.format(FILE_NAME_PATTERN, new Object[]{this.logName, Integer.valueOf(i)}));
                        file.delete();
                    } else {
                        String str = this.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Inside rotateFiles - Renaming ");
                        sb.append(String.format(FILE_NAME_PATTERN, new Object[]{this.logName, Integer.valueOf(i)}));
                        sb.append(" to ");
                        int i2 = i + 1;
                        sb.append(String.format(FILE_NAME_PATTERN, new Object[]{this.logName, Integer.valueOf(i2)}));
                        Log.d(str, sb.toString());
                        file.renameTo(new File(getFilePath(String.format(FILE_NAME_PATTERN, new Object[]{this.logName, Integer.valueOf(i2)}))));
                    }
                    Log.d(this.TAG, "Inside rotateFiles - Unlocking " + file.getName());
                    lock.release();
                    channel.close();
                }
            }
            Log.d(this.TAG, "Inside rotateFiles - Creating new " + getFilePath(String.format(FILE_NAME_PATTERN, new Object[]{this.logName, 0})));
            new File(getFilePath(String.format(FILE_NAME_PATTERN, new Object[]{this.logName, 0}))).createNewFile();
            Log.d(this.TAG, "Inside rotateFiles - End Rotating files");
        }

        @DexIgnore
        private synchronized void writeLog(String str) {
            File file = new File(getFilePath(String.format(FILE_NAME_PATTERN, new Object[]{this.logName, 0})));
            try {
                if (!file.exists()) {
                    Log.d(this.TAG, "Inside writeLog - Creating file...");
                    file.createNewFile();
                } else if (file.length() >= FILE_SIZE) {
                    Log.d(this.TAG, "Inside writeLog - Rotating files...");
                    rotateFiles();
                }
                FileOutputStream fileOutputStream = new FileOutputStream(file, true);
                fileOutputStream.write(str.getBytes());
                fileOutputStream.close();
            } catch (Exception e) {
                String str2 = this.TAG;
                Log.e(str2, "Error inside writeLog - e=" + e + ", fileName=" + file.getAbsolutePath());
            }
            return;
        }

        @DexIgnore
        public void log(String str, String str2, String str3, String str4) {
            writeLog(String.format(Locale.US, LOG_PATTERN, new Object[]{Long.valueOf(Calendar.getInstance().getTimeInMillis()), str, str2, str3, str4}));
        }
    }

    @DexIgnore
    public static List<File> exportLogFiles() {
        if (!isDebuggable || !isInitialized) {
            return new ArrayList();
        }
        return microAppFileHelper.exportLogs();
    }

    @DexIgnore
    public static void initialize(Context context) {
        Context applicationContext = context.getApplicationContext();
        boolean z = true;
        isInitialized = true;
        if ((context.getApplicationInfo().flags & 2) == 0) {
            z = false;
        }
        isDebuggable = z;
        String str = TAG;
        Log.d(str, "Inside " + TAG + ".initialize - debuggable=" + isDebuggable);
        microAppFileHelper = new MicroAppFileHelper(applicationContext, LOG_NAME);
    }

    @DexIgnore
    public static boolean isInitialized() {
        return isInitialized;
    }

    @DexIgnore
    public static void log(String str, String str2, String str3, String str4) {
        if (isDebuggable && isInitialized) {
            microAppFileHelper.log(str, str2, str3, str4);
        }
    }

    @DexIgnore
    public static void resetLogFiles() {
        if (isDebuggable && isInitialized) {
            microAppFileHelper.resetLogFiles();
        }
    }
}
