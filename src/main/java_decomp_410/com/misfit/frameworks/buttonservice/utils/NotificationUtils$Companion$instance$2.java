package com.misfit.frameworks.buttonservice.utils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationUtils$Companion$instance$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.misfit.frameworks.buttonservice.utils.NotificationUtils> {
    @DexIgnore
    public static /* final */ com.misfit.frameworks.buttonservice.utils.NotificationUtils$Companion$instance$2 INSTANCE; // = new com.misfit.frameworks.buttonservice.utils.NotificationUtils$Companion$instance$2();

    @DexIgnore
    public NotificationUtils$Companion$instance$2() {
        super(0);
    }

    @DexIgnore
    public final com.misfit.frameworks.buttonservice.utils.NotificationUtils invoke() {
        return com.misfit.frameworks.buttonservice.utils.NotificationUtils.Singleton.INSTANCE.getINSTANCE();
    }
}
