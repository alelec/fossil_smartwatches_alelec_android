package com.misfit.frameworks.buttonservice.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.os.Build;
import com.fossil.blesdk.obfuscated.d6;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.la4;
import com.fossil.blesdk.obfuscated.le4;
import com.fossil.blesdk.obfuscated.ma4;
import com.fossil.blesdk.obfuscated.md4;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import kotlin.TypeCastException;
import kotlin.jvm.internal.PropertyReference1;
import kotlin.jvm.internal.PropertyReference1Impl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationUtils {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String DEFAULT_CHANNEL_WATCH_SERVICE; // = "com.fossil.wearables.fossil.channel.defaultService";
    @DexIgnore
    public static /* final */ String DEFAULT_CHANNEL_WATCH_SERVICE_NAME; // = "Sync service";
    @DexIgnore
    public static /* final */ int DEVICE_STATUS_NOTIFICATION_ID; // = 1;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ la4 instance$delegate; // = ma4.a(NotificationUtils$Companion$instance$Anon2.INSTANCE);
    @DexIgnore
    public static d6.c mNotificationBuilder;
    @DexIgnore
    public String mLastContent;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public static /* final */ /* synthetic */ le4[] $$delegatedProperties;

        /*
        static {
            PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(md4.a(Companion.class), "instance", "getInstance()Lcom/misfit/frameworks/buttonservice/utils/NotificationUtils;");
            md4.a((PropertyReference1) propertyReference1Impl);
            $$delegatedProperties = new le4[]{propertyReference1Impl};
        }
        */

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public static /* synthetic */ void instance$annotations() {
        }

        @DexIgnore
        public final NotificationUtils getInstance() {
            la4 access$getInstance$cp = NotificationUtils.instance$delegate;
            Companion companion = NotificationUtils.Companion;
            le4 le4 = $$delegatedProperties[0];
            return (NotificationUtils) access$getInstance$cp.getValue();
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Singleton {
        @DexIgnore
        public static /* final */ Singleton INSTANCE; // = new Singleton();

        @DexIgnore
        /* renamed from: INSTANCE  reason: collision with other field name */
        public static /* final */ NotificationUtils f1INSTANCE; // = new NotificationUtils((fd4) null);

        @DexIgnore
        public final NotificationUtils getINSTANCE() {
            return f1INSTANCE;
        }
    }

    /*
    static {
        String simpleName = NotificationUtils$Companion$TAG$Anon1.INSTANCE.getClass().getSimpleName();
        kd4.a((Object) simpleName, "NotificationUtils::javaClass.javaClass.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public NotificationUtils() {
        this.mLastContent = "";
    }

    @DexIgnore
    private final Notification createDefaultDeviceStatusNotification(Context context, String str, String str2) {
        if (!(str.length() == 0)) {
            this.mLastContent = str;
        }
        d6.c cVar = new d6.c(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        ArrayList<d6.a> arrayList = cVar.b;
        if (arrayList != null) {
            arrayList.clear();
        }
        if (Build.VERSION.SDK_INT < 24) {
            cVar.b((CharSequence) context.getResources().getString(R.string.app_name));
            cVar.a((CharSequence) str2);
            cVar.c(R.drawable.ic_launcher_transparent);
            cVar.a(Constants.SERVICE);
            cVar.c(false);
            cVar.d(false);
            cVar.a((d6.d) null);
            cVar.b(-2);
            Notification a = cVar.a();
            kd4.a((Object) a, "builder\n                \u2026                 .build()");
            return a;
        }
        cVar.c(R.drawable.ic_launcher_transparent);
        cVar.a(Constants.SERVICE);
        cVar.b((CharSequence) str2);
        cVar.a((d6.d) null);
        cVar.c(false);
        cVar.d(false);
        cVar.b(-2);
        Notification a2 = cVar.a();
        kd4.a((Object) a2, "builder\n                \u2026                 .build()");
        return a2;
    }

    @DexIgnore
    private final Notification createDeviceStatusNotification(Context context, String str, PendingIntent pendingIntent, PendingIntent pendingIntent2) {
        if (!(str.length() == 0)) {
            this.mLastContent = str;
        }
        if (mNotificationBuilder == null) {
            mNotificationBuilder = new d6.c(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        }
        d6.c cVar = mNotificationBuilder;
        if (cVar != null) {
            ArrayList<d6.a> arrayList = cVar.b;
            if (arrayList != null) {
                arrayList.clear();
            }
        }
        if (Build.VERSION.SDK_INT < 24) {
            d6.c cVar2 = mNotificationBuilder;
            if (cVar2 != null) {
                cVar2.b((CharSequence) context.getResources().getString(R.string.app_name));
                cVar2.a(Constants.SERVICE);
                cVar2.c(false);
                cVar2.a((CharSequence) str);
                cVar2.a((d6.d) null);
                cVar2.c(R.drawable.ic_launcher_transparent);
                cVar2.d(false);
                cVar2.b(-2);
                Notification a = cVar2.a();
                kd4.a((Object) a, "mNotificationBuilder!!\n \u2026                 .build()");
                return a;
            }
            kd4.a();
            throw null;
        }
        d6.c cVar3 = mNotificationBuilder;
        if (cVar3 != null) {
            cVar3.c(R.drawable.ic_launcher_transparent);
            cVar3.a(Constants.SERVICE);
            cVar3.b((CharSequence) str);
            cVar3.a((d6.d) null);
            cVar3.c(false);
            cVar3.d(false);
            cVar3.b(-2);
            if (pendingIntent2 != null) {
                cVar3.a(R.drawable.ic_refresh, context.getString(R.string.sync), pendingIntent2);
            }
            if (pendingIntent != null) {
                cVar3.a(R.drawable.ic_refresh, context.getString(R.string.home), pendingIntent);
            }
            Notification a2 = cVar3.a();
            kd4.a((Object) a2, "builder.build()");
            return a2;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public static /* synthetic */ Notification createDeviceStatusNotification$default(NotificationUtils notificationUtils, Context context, String str, PendingIntent pendingIntent, PendingIntent pendingIntent2, int i, Object obj) {
        if ((i & 4) != 0) {
            pendingIntent = null;
        }
        if ((i & 8) != 0) {
            pendingIntent2 = null;
        }
        return notificationUtils.createDeviceStatusNotification(context, str, pendingIntent, pendingIntent2);
    }

    @DexIgnore
    private final Notification createDeviceStatusWithRichTextNotification(Context context, String str, String str2, PendingIntent pendingIntent, PendingIntent pendingIntent2) {
        if (!(str2.length() == 0)) {
            this.mLastContent = str2;
        }
        if (mNotificationBuilder == null) {
            mNotificationBuilder = new d6.c(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        }
        d6.c cVar = mNotificationBuilder;
        if (cVar != null) {
            ArrayList<d6.a> arrayList = cVar.b;
            if (arrayList != null) {
                arrayList.clear();
            }
        }
        if (Build.VERSION.SDK_INT < 24) {
            d6.c cVar2 = mNotificationBuilder;
            if (cVar2 != null) {
                cVar2.b((CharSequence) context.getResources().getString(R.string.app_name));
                cVar2.a(Constants.SERVICE);
                d6.b bVar = new d6.b();
                bVar.a((CharSequence) str2);
                cVar2.a((d6.d) bVar);
                cVar2.c(false);
                cVar2.a((CharSequence) str);
                cVar2.c(R.drawable.ic_launcher_transparent);
                cVar2.d(false);
                cVar2.b(-2);
                Notification a = cVar2.a();
                kd4.a((Object) a, "mNotificationBuilder!!\n \u2026                 .build()");
                return a;
            }
            kd4.a();
            throw null;
        }
        d6.c cVar3 = mNotificationBuilder;
        if (cVar3 != null) {
            cVar3.c(R.drawable.ic_launcher_transparent);
            cVar3.a(Constants.SERVICE);
            d6.b bVar2 = new d6.b();
            bVar2.a((CharSequence) str2);
            cVar3.a((d6.d) bVar2);
            cVar3.b((CharSequence) str);
            cVar3.c(false);
            cVar3.d(false);
            cVar3.b(-2);
            if (pendingIntent2 != null) {
                cVar3.a(R.drawable.ic_refresh, context.getString(R.string.sync), pendingIntent2);
            }
            if (pendingIntent != null) {
                cVar3.a(R.drawable.ic_refresh, context.getString(R.string.home), pendingIntent);
            }
            Notification a2 = cVar3.a();
            kd4.a((Object) a2, "builder.build()");
            return a2;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public static /* synthetic */ Notification createDeviceStatusWithRichTextNotification$default(NotificationUtils notificationUtils, Context context, String str, String str2, PendingIntent pendingIntent, PendingIntent pendingIntent2, int i, Object obj) {
        return notificationUtils.createDeviceStatusWithRichTextNotification(context, str, str2, (i & 8) != 0 ? null : pendingIntent, (i & 16) != 0 ? null : pendingIntent2);
    }

    @DexIgnore
    private final NotificationManager createNotificationChannels(Context context) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "createNotificationChannels - context=" + context);
        Object systemService = context.getSystemService("notification");
        if (systemService != null) {
            NotificationManager notificationManager = (NotificationManager) systemService;
            if (Build.VERSION.SDK_INT >= 26) {
                NotificationChannel notificationChannel = notificationManager.getNotificationChannel(DEFAULT_CHANNEL_WATCH_SERVICE);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "createNotificationChannels - currentChannel=" + notificationChannel);
                if (notificationChannel == null) {
                    NotificationChannel notificationChannel2 = new NotificationChannel(DEFAULT_CHANNEL_WATCH_SERVICE, DEFAULT_CHANNEL_WATCH_SERVICE_NAME, 1);
                    notificationChannel2.setShowBadge(false);
                    notificationManager.createNotificationChannel(notificationChannel2);
                }
            }
            return notificationManager;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.app.NotificationManager");
    }

    @DexIgnore
    public static final NotificationUtils getInstance() {
        return Companion.getInstance();
    }

    @DexIgnore
    public static /* synthetic */ void updateNotification$default(NotificationUtils notificationUtils, Context context, int i, String str, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z, int i2, Object obj) {
        notificationUtils.updateNotification(context, i, str, (i2 & 8) != 0 ? null : pendingIntent, (i2 & 16) != 0 ? null : pendingIntent2, z);
    }

    @DexIgnore
    public static /* synthetic */ void updateRichTextNotification$default(NotificationUtils notificationUtils, Context context, int i, String str, String str2, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z, int i2, Object obj) {
        notificationUtils.updateRichTextNotification(context, i, str, str2, (i2 & 16) != 0 ? null : pendingIntent, (i2 & 32) != 0 ? null : pendingIntent2, z);
    }

    @DexIgnore
    public final void startForegroundNotification(Context context, Service service, String str, String str2, boolean z) {
        kd4.b(context, "context");
        kd4.b(service, Constants.SERVICE);
        kd4.b(str, "content");
        kd4.b(str2, "subText");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, "startForegroundNotification - context=" + context + ',' + " service=" + service + ", content=" + str + ", subText=" + str2 + ", isStopForeground = " + z);
        createNotificationChannels(context);
        service.startForeground(1, createDefaultDeviceStatusNotification(context, str, str2));
        if (z) {
            FLogger.INSTANCE.getLocal().d(TAG, "startForegroundNotification() - stop foreground service and remove notification");
            service.stopForeground(true);
        }
    }

    @DexIgnore
    public final void updateNotification(Context context, int i, String str, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z) {
        kd4.b(context, "context");
        kd4.b(str, "content");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "updateNotification - context=" + context + ", content=" + str + " notificationId=" + i + ", isReset=" + z);
        if (!(str.length() == 0) || z) {
            this.mLastContent = str;
        }
        createNotificationChannels(context).notify(i, createDeviceStatusNotification(context, str, pendingIntent, pendingIntent2));
    }

    @DexIgnore
    public final void updateRichTextNotification(Context context, int i, String str, String str2, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z) {
        kd4.b(context, "context");
        kd4.b(str, "title");
        kd4.b(str2, "content");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, "updateNotification - context=" + context + ", title=" + str + ", content=" + str2 + ", notificationId=" + i + ", isReset=" + z);
        if ((str2.length() > 0) || z) {
            this.mLastContent = str2;
        }
        createNotificationChannels(context).notify(i, createDeviceStatusWithRichTextNotification(context, str, str2, pendingIntent, pendingIntent2));
    }

    @DexIgnore
    public /* synthetic */ NotificationUtils(fd4 fd4) {
        this();
    }
}
