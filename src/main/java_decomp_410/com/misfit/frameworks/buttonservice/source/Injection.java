package com.misfit.frameworks.buttonservice.source;

import android.content.Context;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Injection {
    @DexIgnore
    public static /* final */ Injection INSTANCE; // = new Injection();

    @DexIgnore
    public final FirmwareFileRepository provideFilesRepository(Context context) {
        kd4.b(context, "context");
        return FirmwareFileRepository.Companion.getInstance(context, new FirmwareFileLocalSource());
    }
}
