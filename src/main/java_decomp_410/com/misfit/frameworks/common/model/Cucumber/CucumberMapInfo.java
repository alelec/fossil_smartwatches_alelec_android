package com.misfit.frameworks.common.model.Cucumber;

import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class CucumberMapInfo {
    @DexIgnore
    public long latitude;
    @DexIgnore
    public long longtitude;
    @DexIgnore
    public long timestamp;

    @DexIgnore
    public long getLatitude() {
        return this.latitude;
    }

    @DexIgnore
    public long getLongtitude() {
        return this.longtitude;
    }

    @DexIgnore
    public long getTimestamp() {
        return this.timestamp;
    }

    @DexIgnore
    public void setLatitude(long j) {
        this.latitude = j;
    }

    @DexIgnore
    public void setLongtitude(long j) {
        this.longtitude = j;
    }

    @DexIgnore
    public void setTimestamp(long j) {
        this.timestamp = j;
    }

    @DexIgnore
    public JSONArray toJson() {
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(this.timestamp);
        jSONArray.put(this.latitude);
        jSONArray.put(this.longtitude);
        return jSONArray;
    }
}
