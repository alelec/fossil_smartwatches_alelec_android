package com.misfit.frameworks.common.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class BleModeCommand {
    @DexIgnore
    public static /* final */ int ACTIVITY; // = 5;
    @DexIgnore
    public static /* final */ int BOLT_CONTROL; // = 6;
    @DexIgnore
    public static /* final */ int CUSTOM; // = 7;
    @DexIgnore
    public static /* final */ int MUSIC; // = 2;
    @DexIgnore
    public static /* final */ int NONE; // = 0;
    @DexIgnore
    public static /* final */ int PLUTO_TRACKER; // = 50;
    @DexIgnore
    public static /* final */ int PRESENTATION; // = 3;
    @DexIgnore
    public static /* final */ int SELFIE; // = 1;
}
