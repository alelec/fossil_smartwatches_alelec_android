package com.misfit.frameworks.common.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum StravaPostResultCode {
    NOT_EXIST_ACCESS_TOKEN(-3),
    NETWORK_ERROR(-2),
    WRONG_FORMAT(-1),
    POSTING(0),
    POSTED_SUCCESSFULLY(1);
    
    @DexIgnore
    public int value;

    @DexIgnore
    StravaPostResultCode(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
