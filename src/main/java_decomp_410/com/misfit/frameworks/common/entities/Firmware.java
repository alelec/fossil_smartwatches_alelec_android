package com.misfit.frameworks.common.entities;

import java.io.Serializable;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class Firmware implements Serializable {
    @DexIgnore
    public String changeLog;
    @DexIgnore
    public String checksum;
    @DexIgnore
    public Date createdAt;
    @DexIgnore
    public String deviceModel;
    @DexIgnore
    public String downloadUrl;
    @DexIgnore
    public long id;
    @DexIgnore
    public boolean isLatest;
    @DexIgnore
    public Date updatedAt;
    @DexIgnore
    public String versionNum;

    @DexIgnore
    public String getChangeLog() {
        return this.changeLog;
    }

    @DexIgnore
    public String getChecksum() {
        return this.checksum;
    }

    @DexIgnore
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public String getDeviceModel() {
        return this.deviceModel;
    }

    @DexIgnore
    public String getDownloadUrl() {
        return this.downloadUrl;
    }

    @DexIgnore
    public long getId() {
        return this.id;
    }

    @DexIgnore
    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public String getVersionNum() {
        return this.versionNum;
    }

    @DexIgnore
    public boolean isLatest() {
        return this.isLatest;
    }

    @DexIgnore
    public void setChangeLog(String str) {
        this.changeLog = str;
    }

    @DexIgnore
    public void setChecksum(String str) {
        this.checksum = str;
    }

    @DexIgnore
    public void setCreatedAt(Date date) {
        this.createdAt = date;
    }

    @DexIgnore
    public void setDeviceModel(String str) {
        this.deviceModel = str;
    }

    @DexIgnore
    public void setDownloadUrl(String str) {
        this.downloadUrl = str;
    }

    @DexIgnore
    public void setId(long j) {
        this.id = j;
    }

    @DexIgnore
    public void setIsLatest(boolean z) {
        this.isLatest = z;
    }

    @DexIgnore
    public void setUpdatedAt(Date date) {
        this.updatedAt = date;
    }

    @DexIgnore
    public void setVersionNum(String str) {
        this.versionNum = str;
    }

    @DexIgnore
    public String toString() {
        return "[Firmware: changeLog=" + this.changeLog + ", checksum=" + this.checksum + ", createdAt=" + this.createdAt + ", deviceModel=" + this.deviceModel + ", downloadUrl=" + this.downloadUrl + ", isLatest=" + this.isLatest + ", updatedAt=" + this.updatedAt + ", versionNum=" + this.versionNum + "]";
    }
}
