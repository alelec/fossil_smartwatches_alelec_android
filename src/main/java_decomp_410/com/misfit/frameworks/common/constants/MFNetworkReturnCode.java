package com.misfit.frameworks.common.constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MFNetworkReturnCode {
    @DexIgnore
    public static /* final */ int BAD_REQUEST; // = 400;
    @DexIgnore
    public static /* final */ int CLIENT_TIMEOUT; // = 408;
    @DexIgnore
    public static /* final */ int CONTENT_TYPE_ERROR; // = 415;
    @DexIgnore
    public static /* final */ int INTERNAL_SERVER_ERROR; // = 500;
    @DexIgnore
    public static /* final */ int ITEM_NAME_IN_USED; // = 409;
    @DexIgnore
    public static /* final */ int NOT_FOUND; // = 404;
    @DexIgnore
    public static /* final */ int NO_INTERNET_CONNECTION; // = 601;
    @DexIgnore
    public static /* final */ int RATE_LIMIT_EXEEDED; // = 429;
    @DexIgnore
    public static /* final */ int REJECTED_REQUEST; // = 666;
    @DexIgnore
    public static /* final */ int REQUEST_NOT_FOUND; // = 8000;
    @DexIgnore
    public static /* final */ int RESPONSE_OK; // = 200;
    @DexIgnore
    public static /* final */ int SERVER_NOT_RESPONDING; // = 504;
    @DexIgnore
    public static /* final */ int UNAUTHORIZED; // = 401;
    @DexIgnore
    public static /* final */ int UNAVAILABLE; // = 503;
    @DexIgnore
    public static /* final */ int UNKNOWN_ERROR; // = 600;
    @DexIgnore
    public static /* final */ int WRONG_PASSWORD; // = 403;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class InternalErrorCode {
        @DexIgnore
        public static /* final */ int TOKEN_EXCHANGE_ERROR; // = 4012;
        @DexIgnore
        public static /* final */ int TOKEN_EXPIRE; // = 401;
        @DexIgnore
        public static /* final */ int WRONG_EMAIL_OR_PASSWORD; // = 4011;

        @DexIgnore
        public InternalErrorCode() {
        }
    }
}
