package com.misfit.frameworks.network.responses;

import android.text.TextUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Command;
import com.misfit.frameworks.common.log.MFLogger;
import java.io.Serializable;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MFResponse implements Serializable {
    @DexIgnore
    public static /* final */ String TAG; // = MFResponse.class.getSimpleName();
    @DexIgnore
    public String action;
    @DexIgnore
    public String code;
    @DexIgnore
    public Command command; // = Command.UNKNOWN;
    @DexIgnore
    public String developerMessage;
    @DexIgnore
    public long etag;
    @DexIgnore
    public int httpReturnCode;
    @DexIgnore
    public int limit;
    @DexIgnore
    public String message;
    @DexIgnore
    public String moreInfo;
    @DexIgnore
    public int offset;
    @DexIgnore
    public int requestId;
    @DexIgnore
    public int status;
    @DexIgnore
    public int total;

    @DexIgnore
    public String getAction() {
        return this.action;
    }

    @DexIgnore
    public Command getCommand() {
        return this.command;
    }

    @DexIgnore
    public String getDeveloperMessage() {
        return this.developerMessage;
    }

    @DexIgnore
    public long getEtag() {
        return this.etag;
    }

    @DexIgnore
    public int getHttpReturnCode() {
        return this.httpReturnCode;
    }

    @DexIgnore
    public int getInternalErrorCode() {
        if (!TextUtils.isEmpty(this.code)) {
            return Integer.valueOf(this.code).intValue();
        }
        return -1;
    }

    @DexIgnore
    public int getLimit() {
        return this.limit;
    }

    @DexIgnore
    public String getMessage() {
        return this.message;
    }

    @DexIgnore
    public String getMoreInfo() {
        return this.moreInfo;
    }

    @DexIgnore
    public int getOffset() {
        return this.offset;
    }

    @DexIgnore
    public int getRequestId() {
        return this.requestId;
    }

    @DexIgnore
    public int getStatus() {
        return this.status;
    }

    @DexIgnore
    public int getTotal() {
        return this.total;
    }

    @DexIgnore
    public void parse(JSONArray jSONArray) {
    }

    @DexIgnore
    public void parse(JSONObject jSONObject) {
        try {
            if (jSONObject.has("code")) {
                this.code = jSONObject.getString("code");
            }
            if (jSONObject.has(Constants.ERROR_RESPONSE_KEY_DEVELOPE_MESSAGE)) {
                this.developerMessage = jSONObject.getString(Constants.ERROR_RESPONSE_KEY_DEVELOPE_MESSAGE);
            }
            if (jSONObject.has("message")) {
                this.message = jSONObject.getString("message");
            }
            if (jSONObject.has(Constants.ERROR_RESPONSE_KEY_MORE_INFO)) {
                this.moreInfo = jSONObject.getString(Constants.ERROR_RESPONSE_KEY_MORE_INFO);
            }
            if (jSONObject.has("status")) {
                this.status = jSONObject.getInt("status");
            }
            if (jSONObject.has(Constants.JSON_KEY_META)) {
                JSONObject jSONObject2 = jSONObject.getJSONObject(Constants.JSON_KEY_META);
                if (jSONObject2.has(Constants.JSON_KEY_ETAG)) {
                    this.etag = jSONObject2.getLong(Constants.JSON_KEY_ETAG);
                }
                if (jSONObject2.has("action")) {
                    this.action = jSONObject2.getString("action");
                }
                if (jSONObject2.has("limit")) {
                    this.limit = jSONObject2.getInt("limit");
                }
                if (jSONObject2.has(Constants.JSON_KEY_TOTAL)) {
                    this.total = jSONObject2.getInt(Constants.JSON_KEY_TOTAL);
                }
                if (jSONObject2.has(Constants.JSON_KEY_OFFSET)) {
                    this.offset = jSONObject2.getInt(Constants.JSON_KEY_OFFSET);
                }
            }
        } catch (Exception e) {
            String str = TAG;
            MFLogger.e(str, "Error inside MFResponse.parse - e=" + e);
            this.status = 600;
        }
    }

    @DexIgnore
    public void setCommand(Command command2) {
        this.command = command2;
    }

    @DexIgnore
    public void setHttpReturnCode(int i) {
        this.httpReturnCode = i;
    }

    @DexIgnore
    public void setRequestId(int i) {
        this.requestId = i;
    }

    @DexIgnore
    public String toString() {
        return "[MFResponse: requestId=" + this.requestId + ", code=" + this.code + ", msg=" + this.message;
    }
}
