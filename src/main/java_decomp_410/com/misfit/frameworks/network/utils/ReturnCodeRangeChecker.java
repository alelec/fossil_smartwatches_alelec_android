package com.misfit.frameworks.network.utils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ReturnCodeRangeChecker {
    @DexIgnore
    public static boolean isSuccessReturnCode(int i) {
        return i >= 200 && i <= 299;
    }
}
