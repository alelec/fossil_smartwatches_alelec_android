package com.misfit.frameworks.network.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum MFServer {
    LINK,
    SHINE,
    HOME,
    CUCUMBER
}
