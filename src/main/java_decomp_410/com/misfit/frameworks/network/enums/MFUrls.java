package com.misfit.frameworks.network.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MFUrls {
    @DexIgnore
    public static /* final */ String EXCHANGE_TOKEN; // = "/auth/session";
    @DexIgnore
    public static /* final */ String URL_CHANGE_PASSWORD; // = "/auth/password";
    @DexIgnore
    public static /* final */ String URL_CURRENT_USER; // = "/user/me";
    @DexIgnore
    public static /* final */ String URL_CURRENT_USER_CHANGE_NEW_PASSWORD; // = "/user/me/password";
    @DexIgnore
    public static /* final */ String URL_GET_LASTEST_APP_VERSION_IN_PLAY_STORE; // = "/settings/app-android-ver";
    @DexIgnore
    public static /* final */ String URL_LOGIN_WITH_EMAIL; // = "/auth/token";
    @DexIgnore
    public static /* final */ String URL_SEND_RESET_PASSWORD; // = "/auth/password";
    @DexIgnore
    public static /* final */ String URL_SIGNUP_WITH_EMAIL; // = "/auth/registration";
    @DexIgnore
    public static /* final */ String URL_SIGN_IN_FACEBOOK; // = "/oauth/facebook/token";
    @DexIgnore
    public static /* final */ String URL_SIGN_IN_GOOGLE; // = "/oauth/google/token";
    @DexIgnore
    public static /* final */ String URL_SIGN_UP_FACEBOOK; // = "/oauth/facebook/registration";
    @DexIgnore
    public static /* final */ String URL_SIGN_UP_GOOGLE; // = "/oauth/google/registration";
}
