package com.fossil.blesdk.device.data.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.D90;
import com.fossil.Dm7;
import com.fossil.Fq7;
import com.fossil.G80;
import com.fossil.Hd0;
import com.fossil.Hy1;
import com.fossil.Ix1;
import com.fossil.Iy1;
import com.fossil.Jd0;
import com.fossil.Ox1;
import com.fossil.Tb;
import com.mapped.NotificationHandMovingConfig;
import com.mapped.NotificationIconConfig;
import com.mapped.NotificationVibePattern;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationFilter extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public long b;
    @DexIgnore
    public byte c;
    @DexIgnore
    public String d;
    @DexIgnore
    public short e;
    @DexIgnore
    public NotificationHandMovingConfig f;
    @DexIgnore
    public NotificationIconConfig g;
    @DexIgnore
    public NotificationVibePattern h;
    @DexIgnore
    public /* final */ String i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<NotificationFilter> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public NotificationFilter createFromParcel(Parcel parcel) {
            return new NotificationFilter(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public NotificationFilter[] newArray(int i) {
            return new NotificationFilter[i];
        }
    }

    /*
    static {
        Hy1.d(Fq7.a);
        Hy1.c(Fq7.a);
    }
    */

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ NotificationFilter(android.os.Parcel r3, com.mapped.Qg6 r4) {
        /*
            r2 = this;
            java.lang.String r0 = r3.readString()
            if (r0 == 0) goto L_0x0053
            java.lang.String r1 = "parcel.readString()!!"
            com.mapped.Wg6.b(r0, r1)
            r2.<init>(r0)
            long r0 = r3.readLong()
            r2.b = r0
            byte r0 = r3.readByte()
            r2.c = r0
            java.lang.String r0 = r3.readString()
            if (r0 == 0) goto L_0x0050
        L_0x0020:
            r2.m1setSender(r0)
            int r0 = r3.readInt()
            short r0 = (short) r0
            r2.m0setPriority(r0)
            java.lang.Class<com.mapped.NotificationHandMovingConfig> r0 = com.mapped.NotificationHandMovingConfig.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r3.readParcelable(r0)
            com.mapped.NotificationHandMovingConfig r0 = (com.mapped.NotificationHandMovingConfig) r0
            r2.f = r0
            java.io.Serializable r0 = r3.readSerializable()
            com.mapped.NotificationVibePattern r0 = (com.mapped.NotificationVibePattern) r0
            r2.h = r0
            java.lang.Class<com.mapped.NotificationIconConfig> r0 = com.mapped.NotificationIconConfig.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r3.readParcelable(r0)
            com.mapped.NotificationIconConfig r0 = (com.mapped.NotificationIconConfig) r0
            r2.g = r0
            return
        L_0x0050:
            java.lang.String r0 = ""
            goto L_0x0020
        L_0x0053:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.blesdk.device.data.notification.NotificationFilter.<init>(android.os.Parcel, com.mapped.Qg6):void");
    }

    @DexIgnore
    public NotificationFilter(String str) {
        this.i = str;
        Ix1 ix1 = Ix1.a;
        Charset c2 = Hd0.y.c();
        if (str != null) {
            byte[] bytes = str.getBytes(c2);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            this.b = ix1.b(Dm7.p(bytes, (byte) 0), Ix1.Ai.CRC32);
            this.d = "";
            this.e = (short) 255;
            return;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public NotificationFilter(String str, NotificationHandMovingConfig notificationHandMovingConfig, NotificationVibePattern notificationVibePattern) {
        this(str);
        this.f = notificationHandMovingConfig;
        this.h = notificationVibePattern;
    }

    @DexIgnore
    public final byte[] a() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte b2 = Tb.d.b;
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.b).array();
        Wg6.b(array, "ByteBuffer.allocate(4)\n \u2026                 .array()");
        byteArrayOutputStream.write(a(b2, array));
        byteArrayOutputStream.write(a(Tb.e.b, new byte[]{this.c}));
        if (this.d.length() > 0) {
            byte b3 = Tb.c.b;
            String a2 = Iy1.a(this.d);
            Charset c2 = Hd0.y.c();
            if (a2 != null) {
                byte[] bytes = a2.getBytes(c2);
                Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
                byteArrayOutputStream.write(a(b3, bytes));
            } else {
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        short s = this.e;
        if (s != ((short) -1)) {
            byteArrayOutputStream.write(a(Tb.g.b, new byte[]{(byte) s}));
        }
        NotificationHandMovingConfig notificationHandMovingConfig = this.f;
        if (notificationHandMovingConfig != null) {
            byteArrayOutputStream.write(a(Tb.h.b, notificationHandMovingConfig.a()));
        }
        NotificationVibePattern notificationVibePattern = this.h;
        if (notificationVibePattern != null) {
            byteArrayOutputStream.write(a(Tb.i.b, new byte[]{notificationVibePattern.a()}));
        }
        NotificationIconConfig notificationIconConfig = this.g;
        if (notificationIconConfig != null) {
            byteArrayOutputStream.write(a(Tb.f.b, notificationIconConfig.b()));
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byte[] array2 = ByteBuffer.allocate(byteArray.length + 2).order(ByteOrder.LITTLE_ENDIAN).putShort((short) byteArray.length).put(byteArray).array();
        Wg6.b(array2, "ByteBuffer\n             \u2026\n                .array()");
        return array2;
    }

    @DexIgnore
    public final byte[] a(byte b2, byte[] bArr) {
        if (bArr.length + 2 > 100) {
            return new byte[0];
        }
        byte[] array = ByteBuffer.allocate(bArr.length + 2).order(ByteOrder.LITTLE_ENDIAN).put(b2).put((byte) bArr.length).put(bArr).array();
        Wg6.b(array, "ByteBuffer.allocate(HEAD\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(NotificationFilter.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            NotificationFilter notificationFilter = (NotificationFilter) obj;
            return this.b == notificationFilter.b && this.c == notificationFilter.c && !(Wg6.a(this.d, notificationFilter.d) ^ true) && this.e == notificationFilter.e && !(Wg6.a(this.f, notificationFilter.f) ^ true) && this.h == notificationFilter.h && !(Wg6.a(this.g, notificationFilter.g) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.NotificationFilter");
    }

    @DexIgnore
    public final long getAppBundleCrc() {
        return this.b;
    }

    @DexIgnore
    public final String getAppPackageName() {
        return this.i;
    }

    @DexIgnore
    public final NotificationHandMovingConfig getHandMovingConfig() {
        return this.f;
    }

    @DexIgnore
    public final NotificationIconConfig getIconConfig() {
        return this.g;
    }

    @DexIgnore
    public final short getPriority() {
        return this.e;
    }

    @DexIgnore
    public final String getSender() {
        return this.d;
    }

    @DexIgnore
    public final NotificationVibePattern getVibePattern() {
        return this.h;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((Long.valueOf(this.b).hashCode() * 31) + this.c) * 31) + this.d.hashCode()) * 31) + this.e;
        NotificationHandMovingConfig notificationHandMovingConfig = this.f;
        if (notificationHandMovingConfig != null) {
            hashCode = (hashCode * 31) + notificationHandMovingConfig.hashCode();
        }
        NotificationVibePattern notificationVibePattern = this.h;
        if (notificationVibePattern != null) {
            hashCode = (hashCode * 31) + notificationVibePattern.hashCode();
        }
        NotificationIconConfig notificationIconConfig = this.g;
        return notificationIconConfig != null ? (hashCode * 31) + notificationIconConfig.hashCode() : hashCode;
    }

    @DexIgnore
    public final NotificationFilter setHandMovingConfig(NotificationHandMovingConfig notificationHandMovingConfig) {
        this.f = notificationHandMovingConfig;
        return this;
    }

    @DexIgnore
    public final NotificationFilter setIconConfig(NotificationIconConfig notificationIconConfig) {
        this.g = notificationIconConfig;
        return this;
    }

    @DexIgnore
    public final NotificationFilter setPriority(short s) {
        m0setPriority(s);
        return this;
    }

    @DexIgnore
    /* renamed from: setPriority  reason: collision with other method in class */
    public final void m0setPriority(short s) {
        short d2 = Hy1.d(Fq7.a);
        short c2 = Hy1.c(Fq7.a);
        if (d2 > s || c2 < s) {
            s = -1;
        }
        this.e = (short) s;
    }

    @DexIgnore
    public final NotificationFilter setSender(String str) {
        throw null;
//        this.d = Iy1.e(str, 97, null, null, 6, null);
//        return this;
    }

    @DexIgnore
    /* renamed from: setSender  reason: collision with other method in class */
    public final void m1setSender(String str) {
        throw null;
//        this.d = Iy1.e(str, 97, null, null, 6, null);
    }

    @DexIgnore
    public final NotificationFilter setVibePatternConfig(NotificationVibePattern notificationVibePattern) {
        this.h = notificationVibePattern;
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        throw null;
//        JSONObject jSONObject = null;
//        JSONObject jSONObject2 = new JSONObject();
//        try {
//            G80.k(jSONObject2, Jd0.X2, this.i);
//            G80.k(jSONObject2, Jd0.W2, Hy1.k((int) this.b, null, 1, null));
//            G80.k(jSONObject2, Jd0.T, Byte.valueOf(this.c));
//            G80.k(jSONObject2, Jd0.i, this.d);
//            G80.k(jSONObject2, Jd0.Q, Short.valueOf(this.e));
//            Jd0 jd0 = Jd0.B2;
//            NotificationHandMovingConfig notificationHandMovingConfig = this.f;
//            G80.k(jSONObject2, jd0, notificationHandMovingConfig != null ? notificationHandMovingConfig.toJSONObject() : null);
//            Jd0 jd02 = Jd0.C2;
//            NotificationVibePattern notificationVibePattern = this.h;
//            G80.k(jSONObject2, jd02, notificationVibePattern != null ? Byte.valueOf(notificationVibePattern.a()) : null);
//            Jd0 jd03 = Jd0.b3;
//            NotificationIconConfig notificationIconConfig = this.g;
//            if (notificationIconConfig != null) {
//                jSONObject = notificationIconConfig.toJSONObject();
//            }
//            G80.k(jSONObject2, jd03, jSONObject);
//        } catch (JSONException e2) {
//            D90.i.i(e2);
//        }
//        return jSONObject2;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.i);
        }
        if (parcel != null) {
            parcel.writeLong(this.b);
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.f, i2);
        }
        if (parcel != null) {
            parcel.writeSerializable(this.h);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.g, i2);
        }
    }
}
