package com.portfolio.platform.uirenew.customview.imagecropper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Eq0;
import com.fossil.Py5;
import com.fossil.Qy5;
import com.fossil.Ry5;
import com.fossil.Sy5;
import com.fossil.Ty5;
import com.fossil.imagefilters.FilterType;
import com.mapped.X24;
import com.portfolio.platform.uirenew.customview.imagecropper.CropOverlayView;
import java.lang.ref.WeakReference;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
public class CropImageView extends FrameLayout {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public int C;
    @DexIgnore
    public g D;
    @DexIgnore
    public f E;
    @DexIgnore
    public h F;
    @DexIgnore
    public i G;
    @DexIgnore
    public e H;
    @DexIgnore
    public Uri I;
    @DexIgnore
    public int J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public RectF N;
    @DexIgnore
    public int O;
    @DexIgnore
    public boolean P;
    @DexIgnore
    public Uri Q;
    @DexIgnore
    public WeakReference<Qy5> R;
    @DexIgnore
    public WeakReference<Py5> S;
    @DexIgnore
    public Bitmap T;
    @DexIgnore
    public /* final */ ImageView b;
    @DexIgnore
    public /* final */ CropOverlayView c;
    @DexIgnore
    public /* final */ Matrix d;
    @DexIgnore
    public /* final */ Matrix e;
    @DexIgnore
    public /* final */ float[] f;
    @DexIgnore
    public /* final */ float[] g;
    @DexIgnore
    public Sy5 h;
    @DexIgnore
    public Bitmap i;
    @DexIgnore
    public Bitmap j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public k w;
    @DexIgnore
    public FilterType x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements CropOverlayView.b {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.uirenew.customview.imagecropper.CropOverlayView.b
        public void a(boolean z) {
            CropImageView.this.j(z, true);
            g gVar = CropImageView.this.D;
            if (gVar != null && !z) {
                gVar.E3(CropImageView.this.getCropRect());
            }
            f fVar = CropImageView.this.E;
            if (fVar != null && z) {
                fVar.a(CropImageView.this.getCropRect());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ Uri b;
        @DexIgnore
        public /* final */ Bitmap c;
        @DexIgnore
        public /* final */ Uri d;
        @DexIgnore
        public /* final */ Rect e;
        @DexIgnore
        public /* final */ Rect f;

        @DexIgnore
        public b(Bitmap bitmap, Uri uri, Bitmap bitmap2, Uri uri2, Exception exc, float[] fArr, Rect rect, Rect rect2, int i, int i2) {
            this.a = bitmap;
            this.b = uri;
            this.c = bitmap2;
            this.d = uri2;
            this.e = rect;
            this.f = rect2;
        }
    }

    @DexIgnore
    public enum c {
        RECTANGLE,
        OVAL
    }

    @DexIgnore
    public enum d {
        OFF,
        ON_TOUCH,
        ON
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(CropImageView cropImageView, b bVar);
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        void a(Rect rect);
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void E3(Rect rect);
    }

    @DexIgnore
    public interface h {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore
    public interface i {
        @DexIgnore
        void K3(CropImageView cropImageView, Uri uri, Exception exc);
    }

    @DexIgnore
    public enum j {
        NONE,
        SAMPLING,
        RESIZE_INSIDE,
        RESIZE_FIT,
        RESIZE_EXACT
    }

    @DexIgnore
    public enum k {
        FIT_CENTER,
        CENTER,
        CENTER_CROP,
        CENTER_INSIDE
    }

    @DexIgnore
    public CropImageView(Context context) {
        this(context, null);
    }

    @SuppressLint("ResourceType")
    @DexIgnore
    /* JADX INFO: finally extract failed */
    public CropImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Ty5 ty5;
        Bundle bundleExtra;
        this.d = new Matrix();
        this.e = new Matrix();
        this.f = new float[8];
        this.g = new float[8];
        this.x = FilterType.ATKINSON_DITHERING;
        this.y = false;
        this.z = true;
        this.A = true;
        this.B = true;
        this.J = 1;
        this.K = 1.0f;
        Intent intent = context instanceof Activity ? ((Activity) context).getIntent() : null;
        Ty5 ty52 = (intent == null || (bundleExtra = intent.getBundleExtra("CROP_IMAGE_EXTRA_BUNDLE")) == null) ? null : (Ty5) bundleExtra.getParcelable("CROP_IMAGE_EXTRA_OPTIONS");
        if (ty52 == null) {
            Ty5 ty53 = new Ty5();
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.CropImageView, 0, 0);
                try {
//                    ty53.m = obtainStyledAttributes.getBoolean(10, ty53.m);
//                    ty53.s = obtainStyledAttributes.getInteger(0, ty53.s);
//                    ty53.t = obtainStyledAttributes.getInteger(1, ty53.t);
//                    ty53.f = k.values()[obtainStyledAttributes.getInt(26, ty53.f.ordinal())];
//                    ty53.i = obtainStyledAttributes.getBoolean(2, ty53.i);
//                    ty53.j = obtainStyledAttributes.getBoolean(24, ty53.j);
//                    ty53.k = obtainStyledAttributes.getInteger(19, ty53.k);
//                    ty53.b = c.values()[obtainStyledAttributes.getInt(27, ty53.b.ordinal())];
//                    ty53.e = d.values()[obtainStyledAttributes.getInt(13, ty53.e.ordinal())];
//                    ty53.c = obtainStyledAttributes.getDimension(30, ty53.c);
//                    ty53.d = obtainStyledAttributes.getDimension(31, ty53.d);
//                    ty53.l = obtainStyledAttributes.getFloat(16, ty53.l);
//                    ty53.u = obtainStyledAttributes.getDimension(8, ty53.u);
//                    ty53.v = obtainStyledAttributes.getInteger(9, ty53.v);
//                    ty53.w = obtainStyledAttributes.getDimension(7, ty53.w);
//                    ty53.x = obtainStyledAttributes.getDimension(6, ty53.x);
//                    ty53.y = obtainStyledAttributes.getDimension(5, ty53.y);
//                    ty53.z = obtainStyledAttributes.getInteger(4, ty53.z);
//                    ty53.A = obtainStyledAttributes.getDimension(15, ty53.A);
//                    ty53.B = obtainStyledAttributes.getInteger(14, ty53.B);
//                    ty53.C = obtainStyledAttributes.getInteger(3, ty53.C);
//                    ty53.g = obtainStyledAttributes.getBoolean(28, this.z);
//                    ty53.h = obtainStyledAttributes.getBoolean(29, this.A);
//                    ty53.w = obtainStyledAttributes.getDimension(7, ty53.w);
//                    ty53.D = (int) obtainStyledAttributes.getDimension(23, (float) ty53.D);
//                    ty53.E = (int) obtainStyledAttributes.getDimension(22, (float) ty53.E);
//                    ty53.F = (int) obtainStyledAttributes.getFloat(21, (float) ty53.F);
//                    ty53.G = (int) obtainStyledAttributes.getFloat(20, (float) ty53.G);
//                    ty53.H = (int) obtainStyledAttributes.getFloat(18, (float) ty53.H);
//                    ty53.I = (int) obtainStyledAttributes.getFloat(17, (float) ty53.I);
//                    ty53.Y = obtainStyledAttributes.getBoolean(11, ty53.Y);
//                    ty53.Z = obtainStyledAttributes.getBoolean(11, ty53.Z);
//                    this.y = obtainStyledAttributes.getBoolean(25, this.y);
//                    if (obtainStyledAttributes.hasValue(0) && obtainStyledAttributes.hasValue(0) && !obtainStyledAttributes.hasValue(10)) {
//                        ty53.m = true;
//                    }
//                    obtainStyledAttributes.recycle();
                    throw null;
//                    ty5 = ty53;
                } catch (Throwable th) {
                    obtainStyledAttributes.recycle();
                    throw th;
                }
            } else {
                ty5 = ty53;
            }
        } else {
            ty5 = ty52;
        }
        ty5.a();
        this.w = ty5.f;
        this.B = ty5.i;
        this.C = ty5.k;
        this.z = ty5.g;
        this.A = ty5.h;
        this.m = ty5.Y;
        this.s = ty5.Z;
        View inflate = LayoutInflater.from(context).inflate(2131558459, (ViewGroup) this, true);
        ImageView imageView = (ImageView) inflate.findViewById(2131361803);
        this.b = imageView;
        imageView.setScaleType(ImageView.ScaleType.MATRIX);
        CropOverlayView cropOverlayView = (CropOverlayView) inflate.findViewById(2131361798);
        this.c = cropOverlayView;
        cropOverlayView.setCropWindowChangeListener(new a());
        this.c.setInitialAttributeValues(ty5);
        z();
    }

    @DexIgnore
    public static int i(int i2, int i3, int i4) {
        return i2 == 1073741824 ? i3 : i2 == Integer.MIN_VALUE ? Math.min(i4, i3) : i4;
    }

    @DexIgnore
    public void A(int i2, int i3, j jVar, Uri uri, Bitmap.CompressFormat compressFormat, int i4) {
        Bitmap bitmap = this.i;
        if (bitmap != null) {
            this.b.clearAnimation();
            WeakReference<Py5> weakReference = this.S;
            Py5 py5 = weakReference != null ? weakReference.get() : null;
            if (py5 != null) {
                py5.cancel(true);
            }
            throw null;
//            int i5 = jVar != j.NONE ? i2 : 0;
//            int i6 = jVar != j.NONE ? i3 : 0;
//            int width = bitmap.getWidth();
//            int i7 = this.J;
//            int height = bitmap.getHeight();
//            int i8 = this.J;
//            if (this.I == null || (i8 <= 1 && jVar != j.SAMPLING)) {
//                this.S = new WeakReference<>(new Py5(this, bitmap, getCropPoints(), this.l, this.c.m(), this.c.getAspectRatioX(), this.c.getAspectRatioY(), i5, i6, this.m, this.s, jVar, uri, compressFormat, i4));
//            } else {
//                this.S = new WeakReference<>(new Py5(this, this.I, getCropPoints(), this.l, i7 * width, height * i8, this.c.m(), this.c.getAspectRatioX(), this.c.getAspectRatioY(), i5, i6, this.m, this.s, jVar, uri, compressFormat, i4));
//            }
//            this.S.get().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
//            z();
        }
    }

    @DexIgnore
    public final void B(boolean z2) {
        if (this.i != null && !z2) {
            this.c.t((float) getWidth(), (float) getHeight(), (((float) this.J) * 100.0f) / Ry5.C(this.g), (((float) this.J) * 100.0f) / Ry5.y(this.g));
        }
        this.c.s(z2 ? null : this.f, getWidth(), getHeight());
    }

    @DexIgnore
    public final void d(float f2, float f3, boolean z2, boolean z3) {
        float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (this.i != null && f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && f3 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.invert(this.e);
            RectF cropWindowRect = this.c.getCropWindowRect();
            this.e.mapRect(cropWindowRect);
            this.d.reset();
            this.d.postTranslate((f2 - ((float) this.i.getWidth())) / 2.0f, (f3 - ((float) this.i.getHeight())) / 2.0f);
            n();
            int i2 = this.l;
            if (i2 > 0) {
                this.d.postRotate((float) i2, Ry5.v(this.f), Ry5.w(this.f));
                n();
            }
            float min = Math.min(f2 / Ry5.C(this.f), f3 / Ry5.y(this.f));
            k kVar = this.w;
//            if (kVar == k.FIT_CENTER || ((kVar == k.CENTER_INSIDE && min < 1.0f) || (min > 1.0f && this.B))) {
//                this.d.postScale(min, min, Ry5.v(this.f), Ry5.w(this.f));
//                n();
//            }
//            float f5 = this.m ? -this.K : this.K;
//            float f6 = this.s ? -this.K : this.K;
//            this.d.postScale(f5, f6, Ry5.v(this.f), Ry5.w(this.f));
//            n();
//            this.d.mapRect(cropWindowRect);
//            if (z2) {
//                this.L = f2 > Ry5.C(this.f) ? 0.0f : Math.max(Math.min((f2 / 2.0f) - cropWindowRect.centerX(), -Ry5.z(this.f)), ((float) getWidth()) - Ry5.A(this.f)) / f5;
//                if (f3 <= Ry5.y(this.f)) {
//                    f4 = Math.max(Math.min((f3 / 2.0f) - cropWindowRect.centerY(), -Ry5.B(this.f)), ((float) getHeight()) - Ry5.u(this.f)) / f6;
//                }
//                this.M = f4;
//            } else {
//                this.L = Math.min(Math.max(this.L * f5, -cropWindowRect.left), (-cropWindowRect.right) + f2) / f5;
//                this.M = Math.min(Math.max(this.M * f6, -cropWindowRect.top), (-cropWindowRect.bottom) + f3) / f6;
//            }
//            this.d.postTranslate(this.L * f5, this.M * f6);
//            cropWindowRect.offset(f5 * this.L, f6 * this.M);
//            this.c.setCropWindowRect(cropWindowRect);
//            n();
//            this.c.invalidate();
//            if (z3) {
//                this.h.d(this.f, this.d);
//                this.b.startAnimation(this.h);
//            } else {
//                this.b.setImageMatrix(this.d);
//            }
            throw null;
//            B(false);
        }
    }

    @DexIgnore
    public void e() {
        this.F = null;
        this.E = null;
        this.D = null;
        this.H = null;
        this.G = null;
    }

    @DexIgnore
    public final void f() {
        if (this.i != null && (this.v > 0 || this.I != null)) {
            this.i.recycle();
        }
        this.i = null;
        this.v = 0;
        this.I = null;
        this.J = 1;
        this.l = 0;
        this.K = 1.0f;
        this.L = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.M = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.d.reset();
        this.Q = null;
        this.b.setImageBitmap(null);
        u();
    }

    @DexIgnore
    public Bitmap g(int i2, int i3, j jVar) {
        if (this.i == null) {
            return null;
        }
        this.b.clearAnimation();
        int i4 = 0;
        throw null;
//        int i5 = jVar != j.NONE ? i2 : 0;
//        if (jVar != j.NONE) {
//            i4 = i3;
//        }
//        return Ry5.D((this.I == null || (this.J <= 1 && jVar != j.SAMPLING)) ? Ry5.h(this.i, getCropPoints(), this.l, this.c.m(), this.c.getAspectRatioX(), this.c.getAspectRatioY(), this.m, this.s).a : Ry5.e(getContext(), this.I, getCropPoints(), this.l, this.i.getWidth() * this.J, this.i.getHeight() * this.J, this.c.m(), this.c.getAspectRatioX(), this.c.getAspectRatioY(), i5, i4, this.m, this.s).a, i5, i4, jVar);
    }

    @DexIgnore
    public Pair<Integer, Integer> getAspectRatio() {
        return new Pair<>(Integer.valueOf(this.c.getAspectRatioX()), Integer.valueOf(this.c.getAspectRatioY()));
    }

    @DexIgnore
    public Bitmap getBitmap() {
        return this.i;
    }

    @DexIgnore
    public float[] getCropPoints() {
        RectF cropWindowRect = this.c.getCropWindowRect();
        float f2 = cropWindowRect.left;
        float f3 = cropWindowRect.top;
        float f4 = cropWindowRect.right;
        float f5 = cropWindowRect.bottom;
        float[] fArr = {f2, f3, f4, f3, f4, f5, f2, f5};
        this.d.invert(this.e);
        this.e.mapPoints(fArr);
        for (int i2 = 0; i2 < 8; i2++) {
            fArr[i2] = fArr[i2] * ((float) this.J);
        }
        return fArr;
    }

    @DexIgnore
    public Rect getCropRect() {
        int i2 = this.J;
        Bitmap bitmap = this.i;
        if (bitmap == null) {
            return null;
        }
        return Ry5.x(getCropPoints(), bitmap.getWidth() * i2, i2 * bitmap.getHeight(), this.c.m(), this.c.getAspectRatioX(), this.c.getAspectRatioY());
    }

    @DexIgnore
    public c getCropShape() {
        return this.c.getCropShape();
    }

    @DexIgnore
    public RectF getCropWindowRect() {
        CropOverlayView cropOverlayView = this.c;
        if (cropOverlayView == null) {
            return null;
        }
        return cropOverlayView.getCropWindowRect();
    }

    @DexIgnore
    public Bitmap getCroppedImage() {
        throw null;
//        return g(0, 0, j.NONE);
    }

    @DexIgnore
    public void getCroppedImageAsync() {
        throw null;
//        h(0, 0, j.NONE);
    }

    @DexIgnore
    public d getGuidelines() {
        return this.c.getGuidelines();
    }

    @DexIgnore
    public int getImageResource() {
        return this.v;
    }

    @DexIgnore
    public Uri getImageUri() {
        return this.I;
    }

    @DexIgnore
    public Bitmap getInitializeBitmap() {
        return this.j;
    }

    @DexIgnore
    public int getLoadedSampleSize() {
        return this.J;
    }

    @DexIgnore
    public int getMaxZoom() {
        return this.C;
    }

    @DexIgnore
    public int getRotatedDegrees() {
        return this.l;
    }

    @DexIgnore
    public k getScaleType() {
        return this.w;
    }

    @DexIgnore
    public Rect getWholeImageRect() {
        int i2 = this.J;
        Bitmap bitmap = this.i;
        if (bitmap == null) {
            return null;
        }
        return new Rect(0, 0, bitmap.getWidth() * i2, i2 * bitmap.getHeight());
    }

    @DexIgnore
    public void h(int i2, int i3, j jVar) {
        A(i2, i3, jVar, null, null, 0);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void j(boolean r13, boolean r14) {
        /*
        // Method dump skipped, instructions count: 261
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.customview.imagecropper.CropImageView.j(boolean, boolean):void");
    }

    @DexIgnore
    public boolean k() {
        return this.c.m();
    }

    @DexIgnore
    public boolean l() {
        return this.m;
    }

    @DexIgnore
    public boolean m() {
        return this.s;
    }

    @DexIgnore
    public final void n() {
        float[] fArr = this.f;
        fArr[0] = 0.0f;
        fArr[1] = 0.0f;
        fArr[2] = (float) this.i.getWidth();
        float[] fArr2 = this.f;
        fArr2[3] = 0.0f;
        fArr2[4] = (float) this.i.getWidth();
        this.f[5] = (float) this.i.getHeight();
        float[] fArr3 = this.f;
        fArr3[6] = 0.0f;
        fArr3[7] = (float) this.i.getHeight();
        this.d.mapPoints(this.f);
        float[] fArr4 = this.g;
        fArr4[0] = 0.0f;
        fArr4[1] = 0.0f;
        fArr4[2] = 100.0f;
        fArr4[3] = 0.0f;
        fArr4[4] = 100.0f;
        fArr4[5] = 100.0f;
        fArr4[6] = 0.0f;
        fArr4[7] = 100.0f;
        this.d.mapPoints(fArr4);
    }

    @DexIgnore
    public void o(Py5.Ai ai) {
        this.S = null;
        z();
        e eVar = this.H;
        if (eVar != null) {
            eVar.a(this, new b(this.i, this.I, ai.a, ai.b, ai.c, getCropPoints(), getCropRect(), getWholeImageRect(), this.l, ai.d));
        }
    }

    @DexReplace
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        super.onLayout(z2, i2, i3, i4, i5);
        if (this.t <= 0 || this.u <= 0) {
            B(true);
            return;
        }
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.width = this.t;
        layoutParams.height = this.u;
        setLayoutParams(layoutParams);
        if (this.i != null) {
            float f2 = (float) (i4 - i2);
            float f3 = (float) (i5 - i3);
            d(f2, f3, true, false);
            if (this.N != null) {
                int i6 = this.O;
                if (i6 != this.k) {
                    this.l = i6;
                    d(f2, f3, true, false);
                }
                this.d.mapRect(this.N);
                this.c.setCropWindowRect(this.N);
                j(false, false);
                this.c.i();
                this.N = null;
            } else if (this.P) {
                this.P = false;
                j(false, false);
            }
        } else {
            B(true);
        }
        CropOverlayView cov = this.c;
        cov.maximiseCrop();
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        super.onMeasure(i2, i3);
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        Bitmap bitmap = this.i;
        if (bitmap != null) {
            if (size2 == 0) {
                size2 = bitmap.getHeight();
            }
            double width = size < this.i.getWidth() ? ((double) size) / ((double) this.i.getWidth()) : Double.POSITIVE_INFINITY;
            double height = size2 < this.i.getHeight() ? ((double) size2) / ((double) this.i.getHeight()) : Double.POSITIVE_INFINITY;
            if (width == Double.POSITIVE_INFINITY && height == Double.POSITIVE_INFINITY) {
                i5 = this.i.getWidth();
                i4 = this.i.getHeight();
            } else if (width <= height) {
                i4 = (int) (width * ((double) this.i.getHeight()));
                i5 = size;
            } else {
                i5 = (int) (((double) this.i.getWidth()) * height);
                i4 = size2;
            }
            int i6 = i(mode, size, i5);
            int i7 = i(mode2, size2, i4);
            this.t = i6;
            this.u = i7;
            setMeasuredDimension(i6, i7);
            return;
        }
        setMeasuredDimension(size, size2);
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            if (this.R == null && this.I == null && this.i == null && this.v == 0) {
                FilterType filterType = (FilterType) bundle.getSerializable("FILTER_TYPE");
                Uri uri = (Uri) bundle.getParcelable("LOADED_IMAGE_URI");
                if (uri != null) {
                    String string = bundle.getString("LOADED_IMAGE_STATE_BITMAP_KEY");
                    if (string != null) {
                        Pair<String, WeakReference<Bitmap>> pair = Ry5.g;
                        Bitmap bitmap = (pair == null || !((String) pair.first).equals(string)) ? null : (Bitmap) ((WeakReference) Ry5.g.second).get();
                        Ry5.g = null;
                        if (bitmap != null && !bitmap.isRecycled()) {
                            t(bitmap, 0, uri, bundle.getInt("LOADED_SAMPLE_SIZE"), 0);
                        }
                    }
                    if (this.I == null) {
                        w(uri, filterType);
                    }
                } else {
                    int i2 = bundle.getInt("LOADED_IMAGE_RESOURCE");
                    if (i2 > 0) {
                        setImageResource(i2);
                    } else {
                        Uri uri2 = (Uri) bundle.getParcelable("LOADING_IMAGE_URI");
                        if (uri2 != null) {
                            w(uri2, filterType);
                        }
                    }
                }
                int i3 = bundle.getInt("DEGREES_ROTATED");
                this.O = i3;
                this.l = i3;
                Rect rect = (Rect) bundle.getParcelable("INITIAL_CROP_RECT");
                if (rect != null && (rect.width() > 0 || rect.height() > 0)) {
                    this.c.setInitialCropWindowRect(rect);
                }
                RectF rectF = (RectF) bundle.getParcelable("CROP_WINDOW_RECT");
                if (rectF != null && (rectF.width() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || rectF.height() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
                    this.N = rectF;
                }
                throw null;
//                this.c.setCropShape(c.valueOf(bundle.getString("CROP_SHAPE")));
//                this.B = bundle.getBoolean("CROP_AUTO_ZOOM_ENABLED");
//                this.C = bundle.getInt("CROP_MAX_ZOOM");
//                this.m = bundle.getBoolean("CROP_FLIP_HORIZONTALLY");
//                this.s = bundle.getBoolean("CROP_FLIP_VERTICALLY");
            }
            super.onRestoreInstanceState(bundle.getParcelable("instanceState"));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Qy5 qy5;
        if (this.I == null && this.i == null && this.v < 1) {
            return super.onSaveInstanceState();
        }
        Bundle bundle = new Bundle();
        Uri uri = this.I;
        if (this.y && uri == null && this.v < 1) {
            uri = Ry5.J(getContext(), this.i, this.Q);
            this.Q = uri;
        }
        if (!(uri == null || this.i == null)) {
            String uuid = UUID.randomUUID().toString();
            Ry5.g = new Pair<>(uuid, new WeakReference(this.i));
            bundle.putString("LOADED_IMAGE_STATE_BITMAP_KEY", uuid);
        }
        WeakReference<Qy5> weakReference = this.R;
        if (!(weakReference == null || (qy5 = weakReference.get()) == null)) {
            bundle.putParcelable("LOADING_IMAGE_URI", qy5.b());
        }
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putParcelable("LOADED_IMAGE_URI", uri);
        bundle.putSerializable("FILTER_TYPE", this.x);
        bundle.putInt("LOADED_IMAGE_RESOURCE", this.v);
        bundle.putInt("LOADED_SAMPLE_SIZE", this.J);
        bundle.putInt("DEGREES_ROTATED", this.l);
        bundle.putParcelable("INITIAL_CROP_RECT", this.c.getInitialCropWindowRect());
        Ry5.c.set(this.c.getCropWindowRect());
        this.d.invert(this.e);
        this.e.mapRect(Ry5.c);
        bundle.putParcelable("CROP_WINDOW_RECT", Ry5.c);
        bundle.putString("CROP_SHAPE", this.c.getCropShape().name());
        bundle.putBoolean("CROP_AUTO_ZOOM_ENABLED", this.B);
        bundle.putInt("CROP_MAX_ZOOM", this.C);
        bundle.putBoolean("CROP_FLIP_HORIZONTALLY", this.m);
        bundle.putBoolean("CROP_FLIP_VERTICALLY", this.s);
        return bundle;
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.P = i4 > 0 && i5 > 0;
    }

    @DexIgnore
    public void p(Qy5.Ai ai) {
        this.R = null;
        z();
        if (ai.e == null) {
            this.k = ai.d;
            if (this.j == null) {
                Bitmap bitmap = ai.b;
                this.j = bitmap.copy(bitmap.getConfig(), false);
            }
            t(ai.f, 0, ai.a, ai.c, ai.d);
        }
        i iVar = this.G;
        if (iVar != null) {
            iVar.K3(this, ai.a, ai.e);
        }
    }

    @DexIgnore
    public void q(Bitmap bitmap) {
        this.b.clearAnimation();
        if (this.i != null && (this.v > 0 || this.I != null)) {
            this.i.recycle();
        }
        this.i = null;
        this.i = bitmap;
        this.b.setImageBitmap(bitmap);
    }

    @DexIgnore
    public void r(int i2) {
        if (this.i != null) {
            int i3 = i2 < 0 ? (i2 % 360) + 360 : i2 % 360;
            boolean z2 = !this.c.m() && ((i3 > 45 && i3 < 135) || (i3 > 215 && i3 < 305));
            Ry5.c.set(this.c.getCropWindowRect());
            RectF rectF = Ry5.c;
            float height = (z2 ? rectF.height() : rectF.width()) / 2.0f;
            RectF rectF2 = Ry5.c;
            float width = (z2 ? rectF2.width() : rectF2.height()) / 2.0f;
            if (z2) {
                boolean z3 = this.m;
                this.m = this.s;
                this.s = z3;
            }
            this.d.invert(this.e);
            Ry5.d[0] = Ry5.c.centerX();
            Ry5.d[1] = Ry5.c.centerY();
            float[] fArr = Ry5.d;
            fArr[2] = 0.0f;
            fArr[3] = 0.0f;
            fArr[4] = 1.0f;
            fArr[5] = 0.0f;
            this.e.mapPoints(fArr);
            this.l = (i3 + this.l) % 360;
            d((float) getWidth(), (float) getHeight(), true, false);
            this.d.mapPoints(Ry5.e, Ry5.d);
            float[] fArr2 = Ry5.e;
            double pow = Math.pow((double) (fArr2[4] - fArr2[2]), 2.0d);
            float[] fArr3 = Ry5.e;
            float sqrt = (float) (((double) this.K) / Math.sqrt(pow + Math.pow((double) (fArr3[5] - fArr3[3]), 2.0d)));
            this.K = sqrt;
            this.K = Math.max(sqrt, 1.0f);
            d((float) getWidth(), (float) getHeight(), true, false);
            this.d.mapPoints(Ry5.e, Ry5.d);
            float[] fArr4 = Ry5.e;
            double pow2 = Math.pow((double) (fArr4[4] - fArr4[2]), 2.0d);
            float[] fArr5 = Ry5.e;
            double sqrt2 = Math.sqrt(pow2 + Math.pow((double) (fArr5[5] - fArr5[3]), 2.0d));
            float f2 = (float) (((double) height) * sqrt2);
            float f3 = (float) (sqrt2 * ((double) width));
            RectF rectF3 = Ry5.c;
            float[] fArr6 = Ry5.e;
            rectF3.set(fArr6[0] - f2, fArr6[1] - f3, f2 + fArr6[0], f3 + fArr6[1]);
            this.c.r();
            this.c.setCropWindowRect(Ry5.c);
            d((float) getWidth(), (float) getHeight(), true, false);
            j(false, false);
            this.c.i();
        }
    }

    @DexIgnore
    public void s(int i2, int i3) {
        this.c.setAspectRatioX(i2);
        this.c.setAspectRatioY(i3);
        setFixedAspectRatio(true);
    }

    @DexIgnore
    public void setAutoZoomEnabled(boolean z2) {
        if (this.B != z2) {
            this.B = z2;
            j(false, false);
            this.c.invalidate();
        }
    }

    @DexIgnore
    public void setCropRect(Rect rect) {
        this.c.setInitialCropWindowRect(rect);
    }

    @DexIgnore
    public void setCropShape(c cVar) {
        this.c.setCropShape(cVar);
    }

    @DexIgnore
    public void setFixedAspectRatio(boolean z2) {
        this.c.setFixedAspectRatio(z2);
    }

    @DexIgnore
    public void setFlippedHorizontally(boolean z2) {
        if (this.m != z2) {
            this.m = z2;
            d((float) getWidth(), (float) getHeight(), true, false);
        }
    }

    @DexIgnore
    public void setFlippedVertically(boolean z2) {
        if (this.s != z2) {
            this.s = z2;
            d((float) getWidth(), (float) getHeight(), true, false);
        }
    }

    @DexIgnore
    public void setGuidelines(d dVar) {
        this.c.setGuidelines(dVar);
    }

    @DexIgnore
    public void setImageBitmap(Bitmap bitmap) {
        this.c.setInitialCropWindowRect(null);
        t(bitmap, 0, null, 1, 0);
    }

    @DexIgnore
    public void setImageResource(int i2) {
        if (i2 != 0) {
            this.c.setInitialCropWindowRect(null);
            t(BitmapFactory.decodeResource(getResources(), i2), i2, null, 1, 0);
        }
    }

    @DexIgnore
    public void setMaxZoom(int i2) {
        if (this.C != i2 && i2 > 0) {
            this.C = i2;
            j(false, false);
            this.c.invalidate();
        }
    }

    @DexIgnore
    public void setMultiTouchEnabled(boolean z2) {
        if (this.c.w(z2)) {
            j(false, false);
            this.c.invalidate();
        }
    }

    @DexIgnore
    public void setOnCropImageCompleteListener(e eVar) {
        this.H = eVar;
    }

    @DexIgnore
    public void setOnCropWindowChangedListener(h hVar) {
        this.F = hVar;
    }

    @DexIgnore
    public void setOnSetCropOverlayMovedListener(f fVar) {
        this.E = fVar;
    }

    @DexIgnore
    public void setOnSetCropOverlayReleasedListener(g gVar) {
        this.D = gVar;
    }

    @DexIgnore
    public void setOnSetImageUriCompleteListener(i iVar) {
        this.G = iVar;
    }

    @DexIgnore
    public void setRotatedDegrees(int i2) {
        int i3 = this.l;
        if (i3 != i2) {
            r(i2 - i3);
        }
    }

    @DexIgnore
    public void setSaveBitmapToInstanceState(boolean z2) {
        this.y = z2;
    }

    @DexIgnore
    public void setScaleType(k kVar) {
        if (kVar != this.w) {
            this.w = kVar;
            this.K = 1.0f;
            this.M = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.L = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.c.r();
            requestLayout();
        }
    }

    @DexIgnore
    public void setShowCropOverlay(boolean z2) {
        if (this.z != z2) {
            this.z = z2;
            u();
        }
    }

    @DexIgnore
    public void setShowProgressBar(boolean z2) {
        if (this.A != z2) {
            this.A = z2;
            z();
        }
    }

    @DexIgnore
    public void setSnapRadius(float f2) {
        if (f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.c.setSnapRadius(f2);
        }
    }

    @DexIgnore
    public final void t(Bitmap bitmap, int i2, Uri uri, int i3, int i4) {
        Bitmap bitmap2 = this.i;
        if (bitmap2 == null || !bitmap2.equals(bitmap)) {
            this.b.clearAnimation();
            f();
            this.T = bitmap.copy(bitmap.getConfig(), true);
            this.i = bitmap;
            this.b.setImageBitmap(bitmap);
            this.I = uri;
            this.v = i2;
            this.J = i3;
            this.l = i4;
            d((float) getWidth(), (float) getHeight(), true, false);
            CropOverlayView cropOverlayView = this.c;
            if (cropOverlayView != null) {
                cropOverlayView.r();
                u();
            }
        }
    }

    @SuppressLint("WrongConstant")
    @DexIgnore
    public final void u() {
        CropOverlayView cropOverlayView = this.c;
        if (cropOverlayView != null) {
            cropOverlayView.setVisibility((!this.z || this.i == null) ? 4 : 0);
        }
    }

    @DexIgnore
    public void v(Bitmap bitmap, Eq0 eq0) {
        int i2;
        Bitmap bitmap2;
        if (bitmap == null || eq0 == null) {
            i2 = 0;
            bitmap2 = bitmap;
        } else {
            Ry5.Bi G2 = Ry5.G(bitmap, eq0);
            bitmap2 = G2.a;
            i2 = G2.b;
            this.k = i2;
        }
        this.c.setInitialCropWindowRect(null);
        t(bitmap2, 0, null, 1, i2);
    }

    @DexIgnore
    public void w(Uri uri, FilterType filterType) {
        if (uri != null) {
            this.x = filterType;
            WeakReference<Qy5> weakReference = this.R;
            Qy5 qy5 = weakReference != null ? weakReference.get() : null;
            if (qy5 != null) {
                qy5.cancel(true);
            }
            f();
            this.N = null;
            this.O = 0;
            this.c.setInitialCropWindowRect(null);
            WeakReference<Qy5> weakReference2 = new WeakReference<>(new Qy5(this, uri, filterType));
            this.R = weakReference2;
            weakReference2.get().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            z();
        }
    }

    @DexIgnore
    public void x(int i2, int i3) {
        this.c.u(i2, i3);
    }

    @DexIgnore
    public void y(int i2, int i3) {
        this.c.v(i2, i3);
    }

    @DexIgnore
    public final void z() {
        if (!this.A) {
            return;
        }
        if (this.i != null || this.R == null) {
            WeakReference<Py5> weakReference = this.S;
        }
    }
}
