package com.portfolio.platform.uirenew.home.alerts.diana;

import android.content.Context;
import android.text.SpannableString;
import android.text.format.DateFormat;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.A06;
import com.fossil.D26;
import com.fossil.D47;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Hr7;
import com.fossil.Jl5;
import com.fossil.Jn5;
import com.fossil.Ko7;
import com.fossil.Lm7;
import com.fossil.Ls0;
import com.fossil.Mn7;
import com.fossil.Qi5;
import com.fossil.Tc7;
import com.fossil.Tq4;
import com.fossil.U06;
import com.fossil.Uh5;
import com.fossil.Um5;
import com.fossil.Uq4;
import com.fossil.V36;
import com.fossil.Yn7;
import com.fossil.Zz5;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.mapped.An4;
import com.mapped.AppWrapper;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.HomeAlertsFragment;
import com.mapped.Il6;
import com.mapped.Jh6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexAppend;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;
import lanchon.dexpatcher.annotation.DexWrap;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeAlertsPresenter extends Zz5 {
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ Ai x; // = new Ai(null);
    @DexIgnore
    public LiveData<String> e; // = PortfolioApp.get.instance().K();
    @DexIgnore
    public /* final */ LiveData<List<DNDScheduledTimeModel>> f; // = this.v.getDNDScheduledTimeDao().getListDNDScheduledTime();
    @DexIgnore
    public ArrayList<Alarm> g; // = new ArrayList<>();
    @DexIgnore
    public boolean h;
    @DexIgnore
    public volatile boolean i;
    @DexIgnore
    public List<AppWrapper> j; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> k; // = new ArrayList();
    @DexIgnore
    public /* final */ A06 l;  // HomeAlertsFragment
    @DexIgnore
    public /* final */ Uq4 m;
    @DexIgnore
    public /* final */ AlarmHelper n;
    @DexIgnore
    public /* final */ V36 o;
    @DexIgnore
    public /* final */ D26 p;
    @DexIgnore
    public /* final */ U06 q;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase r;
    @DexIgnore
    public /* final */ SetAlarms s;
    @DexIgnore
    public /* final */ AlarmsRepository t;
    @DexIgnore
    public /* final */ An4 u;
    @DexIgnore
    public /* final */ DNDSettingsDatabase v;

    // Ensure changes are reflected in HomeAlertsHybridPresenter
    @DexAdd
    public Alarm alarmToAdd = null;
    @DexAdd
    public boolean closeOnFinish = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return HomeAlertsPresenter.w;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ei<SetAlarms.Di, SetAlarms.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ Alarm b;

        @DexIgnore
        public Bi(HomeAlertsPresenter homeAlertsPresenter, Alarm alarm) {
            this.a = homeAlertsPresenter;
            this.b = alarm;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(SetAlarms.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(SetAlarms.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.l.a();
            int c = bi.c();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsPresenter.x.a();
            local.d(a2, "enableAlarm() - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.a.l.c();
                } else if (!(c == 1112 || c == 1113)) {
                    this.a.l.v0();
                }
                this.a.N(bi.a(), false);
                return;
            }
            List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(bi.b());
            Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            A06 a06 = this.a.l;
            Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
            if (array != null) {
                Uh5[] uh5Arr = (Uh5[]) array;
                a06.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                this.a.N(bi.a(), false);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

//        @DexIgnore
//        public void c(SetAlarms.Di di) {
//            Wg6.c(di, "responseValue");
//            ILocalFLogger local = FLogger.INSTANCE.getLocal();
//            String a2 = HomeAlertsPresenter.x.a();
//            local.d(a2, "enableAlarm - onSuccess: alarmUri = " + di.a().getUri() + ", alarmId = " + di.a().getId());
//            this.a.l.a();
//            this.a.N(this.b, true);
//        }

        // Ensure changes are reflected in HomeAlertsHybridPresenter
        @DexAppend
        public void c(SetAlarms.Di di) {
            if (this.a.closeOnFinish) {
                HomeAlertsFragment f = (HomeAlertsFragment) this.a.l;
                FragmentActivity activity = f.getActivity();
                if (activity != null) {
                    activity.finish();
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(SetAlarms.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Tq4.Di<U06.Bi, Tq4.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ci(HomeAlertsPresenter homeAlertsPresenter) {
            this.a = homeAlertsPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void a(Tq4.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(Tq4.Ai ai) {
            FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), ".Inside mSaveAppsNotification onError");
            String c = Um5.c(PortfolioApp.get.instance(), 2131886996);
            A06 a06 = this.a.l;
            Wg6.b(c, "notificationAppOverView");
            a06.o1(c);
        }

        @DexIgnore
        public void c(U06.Bi bi) {
            FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), ".Inside mSaveAppsNotification onSuccess");
            String c = Um5.c(PortfolioApp.get.instance(), 2131886503);
            A06 a06 = this.a.l;
            Wg6.b(c, "notificationAppOverView");
            a06.o1(c);
            this.a.U();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void onSuccess(U06.Bi bi) {
            c(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1", f = "HomeAlertsPresenter.kt", l = {435, 437}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1", f = "HomeAlertsPresenter.kt", l = {356, 362, 371, 379}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends AppNotificationFilter>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $notificationSettings;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii implements Runnable {
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii b;

                    @DexIgnore
                    public Aiiii(Aiii aiii) {
                        this.b = aiii;
                    }

                    @DexIgnore
                    public final void run() {
                        this.b.this$0.this$0.this$0.r.getNotificationSettingsDao().insertListNotificationSettings(this.b.$notificationSettings);
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, List list, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                    this.$notificationSettings = list;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$notificationSettings, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        this.this$0.this$0.this$0.r.runInTransaction(new Aiiii(this));
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$2", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Biii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Jh6 $contactMessageAppFilters;
                @DexIgnore
                public /* final */ /* synthetic */ List<NotificationSettingsModel> $listNotificationSettings;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Aii aii, List list, Jh6 jh6, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                    this.$listNotificationSettings = list;
                    this.$contactMessageAppFilters = jh6;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Biii biii = new Biii(this.this$0, this.$listNotificationSettings, this.$contactMessageAppFilters, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
                    //return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        throw null;
//                        for (NotificationSettingsModel notificationSettingsModel : this.$listNotificationSettings) {
//                            int component2 = notificationSettingsModel.component2();
//                            if (notificationSettingsModel.component3()) {
//                                String P = this.this$0.this$0.this$0.P(component2);
//                                FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "CALL settingsTypeName=" + P);
//                                if (component2 == 0) {
//                                    FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL());
//                                    DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
//                                    this.$contactMessageAppFilters.element.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
//                                } else if (component2 == 1) {
//                                    int size = this.this$0.this$0.this$0.k.size();
//                                    for (int i = 0; i < size; i++) {
//                                        ContactGroup contactGroup = (ContactGroup) this.this$0.this$0.this$0.k.get(i);
//                                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
//                                        String a2 = HomeAlertsPresenter.x.a();
//                                        StringBuilder sb = new StringBuilder();
//                                        sb.append("mListAppNotificationFilter add PHONE item - ");
//                                        sb.append(i);
//                                        sb.append(" name = ");
//                                        Contact contact = contactGroup.getContacts().get(0);
//                                        Wg6.b(contact, "item.contacts[0]");
//                                        sb.append(contact.getDisplayName());
//                                        local.d(a2, sb.toString());
//                                        DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
//                                        FNotification fNotification = new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType());
//                                        List<Contact> contacts = contactGroup.getContacts();
//                                        Wg6.b(contacts, "item.contacts");
//                                        if (!contacts.isEmpty()) {
//                                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification);
//                                            Contact contact2 = contactGroup.getContacts().get(0);
//                                            Wg6.b(contact2, "item.contacts[0]");
//                                            appNotificationFilter.setSender(contact2.getDisplayName());
//                                            this.$contactMessageAppFilters.element.add(appNotificationFilter);
//                                        }
//                                    }
//                                }
//                            } else {
//                                String P2 = this.this$0.this$0.this$0.P(component2);
//                                FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "MESSAGE settingsTypeName=" + P2);
//                                if (component2 == 0) {
//                                    FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getMESSAGES());
//                                    DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
//                                    this.$contactMessageAppFilters.element.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
//                                } else if (component2 == 1) {
//                                    int size2 = this.this$0.this$0.this$0.k.size();
//                                    for (int i2 = 0; i2 < size2; i2++) {
//                                        ContactGroup contactGroup2 = (ContactGroup) this.this$0.this$0.this$0.k.get(i2);
//                                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
//                                        String a3 = HomeAlertsPresenter.x.a();
//                                        StringBuilder sb2 = new StringBuilder();
//                                        sb2.append("mListAppNotificationFilter add MESSAGE item - ");
//                                        sb2.append(i2);
//                                        sb2.append(" name = ");
//                                        Contact contact3 = contactGroup2.getContacts().get(0);
//                                        Wg6.b(contact3, "item.contacts[0]");
//                                        sb2.append(contact3.getDisplayName());
//                                        local2.d(a3, sb2.toString());
//                                        DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
//                                        FNotification fNotification2 = new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType());
//                                        List<Contact> contacts2 = contactGroup2.getContacts();
//                                        Wg6.b(contacts2, "item.contacts");
//                                        if (!contacts2.isEmpty()) {
//                                            AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification2);
//                                            Contact contact4 = contactGroup2.getContacts().get(0);
//                                            Wg6.b(contact4, "item.contacts[0]");
//                                            appNotificationFilter2.setSender(contact4.getDisplayName());
//                                            this.$contactMessageAppFilters.element.add(appNotificationFilter2);
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$listNotificationSettings$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Ciii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends NotificationSettingsModel>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Ciii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Ciii ciii = new Ciii(this.this$0, xe6);
                    ciii.p$ = (Il6) obj;
                    throw null;
                    //return ciii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super List<? extends NotificationSettingsModel>> xe6) {
                    throw null;
                    //return ((Ciii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        return this.this$0.this$0.this$0.r.getNotificationSettingsDao().getListNotificationSettingsNoLiveData();
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends AppNotificationFilter>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:16:0x005e  */
            /* JADX WARNING: Removed duplicated region for block: B:22:0x00b1  */
            /* JADX WARNING: Removed duplicated region for block: B:30:0x0146  */
            /* JADX WARNING: Removed duplicated region for block: B:33:0x018b  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r15) {
                /*
                // Method dump skipped, instructions count: 422
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.Di.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$otherAppDeffer$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends AppNotificationFilter>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends AppNotificationFilter>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return D47.c(this.this$0.this$0.O(), false, 1, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(HomeAlertsPresenter homeAlertsPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeAlertsPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v15, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x00cf  */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 361
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.Di.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1", f = "HomeAlertsPresenter.kt", l = {85}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii implements CoroutineUseCase.Ei<V36.Ai, CoroutineUseCase.Ai> {
                @DexIgnore
                public /* final */ /* synthetic */ Aii a;

                @DexIgnore
                /* JADX WARN: Incorrect args count in method signature: ()V */
                public Aiii(Aii aii) {
                    this.a = aii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // com.portfolio.platform.CoroutineUseCase.Ei
                public /* bridge */ /* synthetic */ void a(CoroutineUseCase.Ai ai) {
                    b(ai);
                }

                @DexIgnore
                public void b(CoroutineUseCase.Ai ai) {
                    Wg6.c(ai, "errorValue");
                    FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "GetApps onError");
                }

                @DexIgnore
                /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r1v3 int), ('/' char), (wrap: int : 0x0156: INVOKE  (r1v5 int) = (r4v0 java.util.ArrayList) type: INTERFACE call: java.util.List.size():int)] */
                public void c(V36.Ai ai) {
                    String sb;
                    Wg6.c(ai, "responseValue");
                    FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "GetApps onSuccess");
                    ArrayList<AppWrapper> arrayList = new ArrayList();
                    arrayList.addAll(ai.a());
                    if (this.a.this$0.a.u.W()) {
                        ArrayList arrayList2 = new ArrayList();
                        for (AppWrapper appWrapper : arrayList) {
                            InstalledApp installedApp = appWrapper.getInstalledApp();
                            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                            if (isSelected == null) {
                                Wg6.i();
                                throw null;
                            } else if (!isSelected.booleanValue()) {
                                arrayList2.add(appWrapper);
                            }
                        }
                        this.a.this$0.a.O().clear();
                        this.a.this$0.a.O().addAll(arrayList);
                        if (!arrayList2.isEmpty()) {
                            this.a.this$0.a.T(arrayList2);
                            return;
                        }
                        String c = Um5.c(PortfolioApp.get.instance(), 2131886503);
                        A06 a06 = this.a.this$0.a.l;
                        Wg6.b(c, "notificationAppOverView");
                        a06.o1(c);
                        HomeAlertsPresenter homeAlertsPresenter = this.a.this$0.a;
                        if (homeAlertsPresenter.R(homeAlertsPresenter.O(), arrayList)) {
                            this.a.this$0.a.U();
                            return;
                        }
                        return;
                    }
                    this.a.this$0.a.O().clear();
                    int i = 0;
                    for (AppWrapper appWrapper2 : arrayList) {
                        InstalledApp installedApp2 = appWrapper2.getInstalledApp();
                        Boolean isSelected2 = installedApp2 != null ? installedApp2.isSelected() : null;
                        if (isSelected2 == null) {
                            Wg6.i();
                            throw null;
                        } else if (isSelected2.booleanValue()) {
                            this.a.this$0.a.O().add(appWrapper2);
                            i++;
                        }
                    }
                    if (i == ai.a().size()) {
                        sb = Um5.c(PortfolioApp.get.instance(), 2131886503);
                    } else if (i == 0) {
                        sb = Um5.c(PortfolioApp.get.instance(), 2131886996);
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(i);
                        sb2.append('/');
                        sb2.append(arrayList.size());
                        sb = sb2.toString();
                    }
                    A06 a062 = this.a.this$0.a.l;
                    Wg6.b(sb, "notificationAppOverView");
                    a062.o1(sb);
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // com.portfolio.platform.CoroutineUseCase.Ei
                public /* bridge */ /* synthetic */ void onSuccess(V36.Ai ai) {
                    c(ai);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Biii<T> implements Ls0<List<? extends DNDScheduledTimeModel>> {
                @DexIgnore
                public /* final */ /* synthetic */ Aii a;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3$1", f = "HomeAlertsPresenter.kt", l = {166}, m = "invokeSuspend")
                public static final class Aiiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ List $lDndScheduledTimeModel;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Biii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Biii biii, List list, Xe6 xe6) {
                        super(2, xe6);
                        this.this$0 = biii;
                        this.$lDndScheduledTimeModel = list;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        Aiiii aiiii = new Aiiii(this.this$0, this.$lDndScheduledTimeModel, xe6);
                        aiiii.p$ = (Il6) obj;
                        throw null;
                        //return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                        throw null;
                        //return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = Yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            El7.b(obj);
                            Il6 il6 = this.p$;
                            Dv7 i2 = this.this$0.a.this$0.a.i();
                            throw null;
//                            HomeAlertsPresenter$e$a$b$a$a homeAlertsPresenter$e$a$b$a$a = new HomeAlertsPresenter$e$a$b$a$a(this, null);
//                            this.L$0 = il6;
//                            this.label = 1;
//                            if (Eu7.g(i2, homeAlertsPresenter$e$a$b$a$a, this) == d) {
//                                return d;
//                            }
                        } else if (i == 1) {
                            Il6 il62 = (Il6) this.L$0;
                            El7.b(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        return Cd6.a;
                    }
                }

                @DexIgnore
                public Biii(Aii aii) {
                    this.a = aii;
                }

                @DexIgnore
                public final void a(List<DNDScheduledTimeModel> list) {
                    if (list == null || list.isEmpty()) {
                        ArrayList arrayList = new ArrayList();
                        DNDScheduledTimeModel dNDScheduledTimeModel = new DNDScheduledTimeModel("Start", 1380, 0);
                        DNDScheduledTimeModel dNDScheduledTimeModel2 = new DNDScheduledTimeModel("End", 1140, 1);
                        arrayList.add(dNDScheduledTimeModel);
                        arrayList.add(dNDScheduledTimeModel2);
                        Rm6 unused = Gu7.d(this.a.this$0.a.k(), null, null, new Aiiii(this, arrayList, null), 3, null);
                        return;
                    }
                    for (DNDScheduledTimeModel t : list) {
                        if (t.getScheduledTimeType() == 0) {
                            this.a.this$0.a.l.q2(this.a.this$0.a.Q(t.getMinutes()));
                        } else {
                            this.a.this$0.a.l.J0(this.a.this$0.a.Q(t.getMinutes()));
                        }
                    }
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // com.fossil.Ls0
                public /* bridge */ /* synthetic */ void onChanged(List<? extends DNDScheduledTimeModel> list) {
                    throw null;
//                    a(list);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$allAlarms$1", f = "HomeAlertsPresenter.kt", l = {86, 91}, m = "invokeSuspend")
            public static final class Ciii extends Ko7 implements Coroutine<Il6, Xe6<? super List<Alarm>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Ciii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Ciii ciii = new Ciii(this.this$0, xe6);
                    ciii.p$ = (Il6) obj;
                    throw null;
                    //return ciii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super List<Alarm>> xe6) {
                    throw null;
                    //return ((Ciii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Il6 il6;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        il6 = this.p$;
                        AlarmHelper alarmHelper = this.this$0.this$0.a.n;
                        this.L$0 = il6;
                        this.label = 1;
                        if (alarmHelper.j(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        il6 = (Il6) this.L$0;
                        El7.b(obj);
                    } else if (i == 2) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (!this.this$0.this$0.a.i) {
                        this.this$0.this$0.a.i = true;
                        AlarmHelper alarmHelper2 = this.this$0.this$0.a.n;
                        Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                        Wg6.b(applicationContext, "PortfolioApp.instance.applicationContext");
                        alarmHelper2.g(applicationContext);
                    }
                    AlarmsRepository alarmsRepository = this.this$0.this$0.a.t;
                    this.L$0 = il6;
                    this.label = 2;
                    Object allAlarmIgnoreDeletePinType = alarmsRepository.getAllAlarmIgnoreDeletePinType(this);
                    return allAlarmIgnoreDeletePinType == d ? d : allAlarmIgnoreDeletePinType;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Diii<T> implements Comparator<T> {
                @DexIgnore
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    throw null;
//                    return Mn7.c(Integer.valueOf(t.getTotalMinutes()), Integer.valueOf(t2.getTotalMinutes()));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            // Ensure changes are reflected in HomeAlertsHybridPresenter
            @DexWrap
            @Override
            public final Object invokeSuspend(Object obj) {
                int i = this.label;
                Object ret = invokeSuspend(obj);
                if (i == 1) {
                    // alarms are loaded
                    HomeAlertsPresenter ap = this.this$0.a;
                    if (ap.alarmToAdd != null) {
                        Alarm alarmToAdd = ap.alarmToAdd;
                        ap.alarmToAdd = null;
                        ap.closeOnFinish = true;

                        ArrayList<Alarm> list = ap.g;
                        list.add(alarmToAdd);
                        ap.o(alarmToAdd, true); // function with 'enableAlarm - alarmTotalMinu'
                    }
                }
                return ret;
            }

//            @DexIgnore
//            @Override // com.fossil.Zn7
//            public final Object invokeSuspend(Object obj) {
//                Object g;
//                Object d = Yn7.d();
//                int i = this.label;
//                if (i == 0) {
//                    El7.b(obj);
//                    Il6 il6 = this.p$;
//                    Dv7 i2 = this.this$0.a.i();
//                    Ciii ciii = new Ciii(this, null);
//                    this.L$0 = il6;
//                    this.label = 1;
//                    g = Eu7.g(i2, ciii, this);
//                    if (g == d) {
//                        return d;
//                    }
//                } else if (i == 1) {
//                    Il6 il62 = (Il6) this.L$0;
//                    El7.b(obj);
//                    g = obj;
//                } else {
//                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
//                }
//                List<Alarm> list = (List) g;
//                ILocalFLogger local = FLogger.INSTANCE.getLocal();
//                String a2 = HomeAlertsPresenter.x.a();
//                local.d(a2, "GetAlarms onSuccess: size = " + list.size());
//                this.this$0.a.g.clear();
//                for (Alarm alarm : list) {
//                    this.this$0.a.g.add(Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
//                }
//                ArrayList arrayList = this.this$0.a.g;
//                if (arrayList.size() > 1) {
//                    Lm7.r(arrayList, new Diii());
//                }
//                this.this$0.a.l.p0(this.this$0.a.g);
//                this.this$0.a.o.e(null, new Aiii(this));
//                HomeAlertsPresenter homeAlertsPresenter = this.this$0.a;
//                homeAlertsPresenter.h = homeAlertsPresenter.u.a0();
//                this.this$0.a.l.n3(this.this$0.a.h);
//                this.this$0.a.f.h((LifecycleOwner) this.this$0.a.l, new Biii(this));
//                return Cd6.a;
//            }
        }

        @DexIgnore
        public Ei(HomeAlertsPresenter homeAlertsPresenter) {
            this.a = homeAlertsPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            if (str == null || str.length() == 0) {
                this.a.l.r(true);
            } else {
                Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;

        @DexIgnore
        public Fi(HomeAlertsPresenter homeAlertsPresenter) {
            this.a = homeAlertsPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            A06 unused = this.a.l;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> implements Ls0<List<? extends DNDScheduledTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;

        @DexIgnore
        public Gi(HomeAlertsPresenter homeAlertsPresenter) {
            this.a = homeAlertsPresenter;
        }

        @DexIgnore
        public final void a(List<DNDScheduledTimeModel> list) {
            A06 unused = this.a.l;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends DNDScheduledTimeModel> list) {
            throw null;
//            a(list);
        }
    }

    /*
    static {
        String simpleName = HomeAlertsPresenter.class.getSimpleName();
        Wg6.b(simpleName, "HomeAlertsPresenter::class.java.simpleName");
        w = simpleName;
    }
    */

    @DexIgnore
    public HomeAlertsPresenter(A06 a06, Uq4 uq4, AlarmHelper alarmHelper, V36 v36, D26 d26, U06 u06, NotificationSettingsDatabase notificationSettingsDatabase, SetAlarms setAlarms, AlarmsRepository alarmsRepository, An4 an4, DNDSettingsDatabase dNDSettingsDatabase) {
        Wg6.c(a06, "mView");
        Wg6.c(uq4, "mUseCaseHandler");
        Wg6.c(alarmHelper, "mAlarmHelper");
        Wg6.c(v36, "mGetApps");
        Wg6.c(d26, "mGetAllContactGroup");
        Wg6.c(u06, "mSaveAppsNotification");
        Wg6.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        Wg6.c(setAlarms, "mSetAlarms");
        Wg6.c(alarmsRepository, "mAlarmRepository");
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(dNDSettingsDatabase, "mDNDSettingsDatabase");
        this.l = a06;
        this.m = uq4;
        this.n = alarmHelper;
        this.o = v36;
        this.p = d26;
        this.q = u06;
        this.r = notificationSettingsDatabase;
        this.s = setAlarms;
        this.t = alarmsRepository;
        this.u = an4;
        this.v = dNDSettingsDatabase;
    }

    @DexIgnore
    public final void N(Alarm alarm, boolean z) {
        Wg6.c(alarm, "editAlarm");
        Iterator<Alarm> it = this.g.iterator();
        while (it.hasNext()) {
            Alarm next = it.next();
            if (Wg6.a(next.getUri(), alarm.getUri())) {
                ArrayList<Alarm> arrayList = this.g;
                throw null;
//                arrayList.set(arrayList.indexOf(next), Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
//                if (!z) {
//                    break;
//                }
//                S();
            }
        }
        this.l.p0(this.g);
    }

    @DexIgnore
    public final List<AppWrapper> O() {
        return this.j;
    }

    @DexIgnore
    public final String P(int i2) {
        if (i2 == 0) {
            String c = Um5.c(PortfolioApp.get.instance(), 2131886089);
            Wg6.b(c, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return c;
        } else if (i2 != 1) {
            String c2 = Um5.c(PortfolioApp.get.instance(), 2131886091);
            Wg6.b(c2, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return c2;
        } else {
            String c3 = Um5.c(PortfolioApp.get.instance(), 2131886090);
            Wg6.b(c3, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return c3;
        }
    }

    @DexIgnore
    public final SpannableString Q(int i2) {
        int i3 = 12;
        int i4 = i2 / 60;
        int i5 = i2 % 60;
        if (DateFormat.is24HourFormat(PortfolioApp.get.instance())) {
            StringBuilder sb = new StringBuilder();
            Hr7 hr7 = Hr7.a;
            Locale locale = Locale.US;
            Wg6.b(locale, "Locale.US");
            String format = String.format(locale, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i4)}, 1));
            Wg6.b(format, "java.lang.String.format(locale, format, *args)");
            sb.append(format);
            sb.append(':');
            Hr7 hr72 = Hr7.a;
            Locale locale2 = Locale.US;
            Wg6.b(locale2, "Locale.US");
            String format2 = String.format(locale2, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i5)}, 1));
            Wg6.b(format2, "java.lang.String.format(locale, format, *args)");
            sb.append(format2);
            return new SpannableString(sb.toString());
        } else if (i2 < 720) {
            if (i4 != 0) {
                i3 = i4;
            }
            Jl5 jl5 = Jl5.b;
            StringBuilder sb2 = new StringBuilder();
            Hr7 hr73 = Hr7.a;
            Locale locale3 = Locale.US;
            Wg6.b(locale3, "Locale.US");
            String format3 = String.format(locale3, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i3)}, 1));
            Wg6.b(format3, "java.lang.String.format(locale, format, *args)");
            sb2.append(format3);
            sb2.append(':');
            Hr7 hr74 = Hr7.a;
            Locale locale4 = Locale.US;
            Wg6.b(locale4, "Locale.US");
            String format4 = String.format(locale4, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i5)}, 1));
            Wg6.b(format4, "java.lang.String.format(locale, format, *args)");
            sb2.append(format4);
            String sb3 = sb2.toString();
            String c = Um5.c(PortfolioApp.get.instance(), 2131886102);
            Wg6.b(c, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
            return jl5.g(sb3, c, 1.0f);
        } else {
            if (i4 > 12) {
                i3 = i4 - 12;
            }
            Jl5 jl52 = Jl5.b;
            StringBuilder sb4 = new StringBuilder();
            Hr7 hr75 = Hr7.a;
            Locale locale5 = Locale.US;
            Wg6.b(locale5, "Locale.US");
            String format5 = String.format(locale5, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i3)}, 1));
            Wg6.b(format5, "java.lang.String.format(locale, format, *args)");
            sb4.append(format5);
            sb4.append(':');
            Hr7 hr76 = Hr7.a;
            Locale locale6 = Locale.US;
            Wg6.b(locale6, "Locale.US");
            String format6 = String.format(locale6, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i5)}, 1));
            Wg6.b(format6, "java.lang.String.format(locale, format, *args)");
            sb4.append(format6);
            String sb5 = sb4.toString();
            String c2 = Um5.c(PortfolioApp.get.instance(), 2131886104);
            Wg6.b(c2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
            return jl52.g(sb5, c2, 1.0f);
        }
    }

    @DexIgnore
    public final boolean R(List<AppWrapper> list, List<AppWrapper> list2) {
        AppWrapper t2;
        Wg6.c(list, "listDatabaseAppWrapper");
        Wg6.c(list2, "listAppWrapper");
        if (list.size() != list2.size()) {
            return true;
        }
        boolean z = false;
        for (AppWrapper t3 : list) {
            Iterator<AppWrapper> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                AppWrapper next = it.next();
                InstalledApp installedApp = next.getInstalledApp();
                String identifier = installedApp != null ? installedApp.getIdentifier() : null;
                InstalledApp installedApp2 = t3.getInstalledApp();
                if (Wg6.a(identifier, installedApp2 != null ? installedApp2.getIdentifier() : null)) {
                    t2 = next;
                    break;
                }
            }
            AppWrapper t4 = t2;
            if (t4 != null) {
                InstalledApp installedApp3 = t4.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                InstalledApp installedApp4 = t3.getInstalledApp();
                if (!(!Wg6.a(isSelected, installedApp4 != null ? installedApp4.isSelected() : null))) {
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final void S() {
        FLogger.INSTANCE.getLocal().d(w, "onSetAlarmsSuccess");
        this.n.g(PortfolioApp.get.instance());
        String e2 = this.e.e();
        if (e2 != null) {
            PortfolioApp instance = PortfolioApp.get.instance();
            Wg6.b(e2, "it");
            instance.P0(e2);
        }
    }

    @DexIgnore
    public final void T(List<AppWrapper> list) {
        Wg6.c(list, "listAppWrapperNotEnabled");
        for (AppWrapper appWrapper : list) {
            InstalledApp installedApp = appWrapper.getInstalledApp();
            if (installedApp != null) {
                installedApp.setSelected(true);
            }
        }
        this.m.a(this.q, new U06.Ai(list), new Ci(this));
    }

    @DexIgnore
    public final void U() {
        Jn5 jn5 = Jn5.b;
        A06 a06 = this.l;
        throw null;
//        if (a06 == null) {
//            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
//        } else if (Jn5.c(jn5, ((HomeAlertsFragment) a06).getContext(), Jn5.Ai.SET_BLE_COMMAND, false, false, false, null, 60, null)) {
//            Rm6 unused = Gu7.d(k(), null, null, new Di(this, null), 3, null);
//        }
    }

    @DexIgnore
    public void V() {
        this.l.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(w, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.s.s();
        PortfolioApp.get.h(this);
        LiveData<String> liveData = this.e;
        A06 a06 = this.l;
        if (a06 != null) {
            liveData.h((HomeAlertsFragment) a06, new Ei(this));
            BleCommandResultManager.d.g(CommunicateMode.SET_LIST_ALARM);
            A06 a062 = this.l;
            throw null;
//            a062.F(!Jn5.c(Jn5.b, ((HomeAlertsFragment) a062).getContext(), Jn5.Ai.NOTIFICATION_DIANA, false, false, false, null, 56, null));
//            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(w, "stop");
        this.e.m(new Fi(this));
        this.f.m(new Gi(this));
        this.s.w();
        PortfolioApp.get.l(this);
    }

    @DexIgnore
    @Override // com.fossil.Zz5
    public void n() {
        Jn5 jn5 = Jn5.b;
        A06 a06 = this.l;
        if (a06 != null) {
            throw null;
//            Jn5.c(jn5, ((HomeAlertsFragment) a06).getContext(), Jn5.Ai.NOTIFICATION_DIANA, false, false, false, null, 60, null);
//            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
    }

    @DexIgnore
    @Override // com.fossil.Zz5
    public void o(Alarm alarm, boolean z) {
        Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        String e2 = this.e.e();
        if (!(e2 == null || e2.length() == 0)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "enableAlarm - alarmTotalMinue: " + alarm.getTotalMinutes() + " - enable: " + z);
            alarm.setActive(z);
            this.l.b();
            SetAlarms setAlarms = this.s;
            String e3 = this.e.e();
            if (e3 != null) {
                Wg6.b(e3, "mActiveSerial.value!!");
                setAlarms.e(new SetAlarms.Ci(e3, this.g, alarm), new Bi(this, alarm));
                return;
            }
            Wg6.i();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(w, "enableAlarm - Current Active Device Serial Is Empty");
    }

    @DexIgnore
    @Tc7
    public final void onSetAlarmEventEndComplete(Qi5 qi5) {
        if (qi5 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "onSetAlarmEventEndComplete() - event = " + qi5);
            if (qi5.b()) {
                String a2 = qi5.a();
                Iterator<Alarm> it = this.g.iterator();
                while (it.hasNext()) {
                    Alarm next = it.next();
                    if (Wg6.a(next.getUri(), a2)) {
                        next.setActive(false);
                    }
                }
                this.l.W();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Zz5
    public void p() {
        boolean z = !this.h;
        this.h = z;
        this.u.W0(z);
        this.l.n3(this.h);
    }

    @DexIgnore
    @Override // com.fossil.Zz5
    public void q(Alarm alarm) {
        String e2 = this.e.e();
        if (e2 == null || e2.length() == 0) {
            FLogger.INSTANCE.getLocal().d("HomeAlertsFragment", "Current Active Device Serial Is Empty");
        } else if (alarm != null || this.g.size() < 32) {
            A06 a06 = this.l;
            String e3 = this.e.e();
            if (e3 != null) {
                Wg6.b(e3, "mActiveSerial.value!!");
                a06.l0(e3, this.g, alarm); // Start AlarmActivity
                return;
            }
            Wg6.i();
            throw null;
        } else {
            this.l.U();
        }
    }
}
