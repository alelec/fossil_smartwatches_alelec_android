package com.portfolio.platform.manager;

import android.content.Context;
import android.util.Base64;
import com.google.gson.Gson;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.AccessGroup;
import java.nio.charset.StandardCharsets;
import java.util.Random;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public class SoLibraryLoader {
    @DexIgnore
    public static SoLibraryLoader c;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public char[][] f4165a; // = {new char[]{'A', 1, '1', 'D', 'E', 'F'}, new char[]{'F', 2, 'H', 'L', 'K', '~'}, new char[]{'#', 'O', 'P', 'Q', 'R', 'S'}, new char[]{'T', '1', 'V', 'W', ')', 'Y'}, new char[]{'$', 'I', 'D', 'O', '0', 'E'}, new char[]{'X', 'V', '%', '^', 'G', '3'}, new char[]{'a', 'b', 'c', '&', 'e', 'f'}, new char[]{'g', 'h', 'i', 'j', 'l', 'k'}, new char[]{'k', 'm', 'n', 'o', 'p', 'q'}, new char[]{'1', '2', '\b', '4', '5', '6'}, new char[]{'7', '(', '9', '0', '+', '-'}, new char[]{',', '.', '!', '@', '#', '$'}, new char[]{'^', '&', '|', ']', '[', ';'}};
    @DexIgnore
    public Gson b; // = new Gson();

    /*
    static {
        System.loadLibrary("res-c");
    }
    */

    @DexIgnore
    public static SoLibraryLoader f() {
        if (c == null) {
            synchronized (SoLibraryLoader.class) {
                try {
                    if (c == null) {
                        c = new SoLibraryLoader();
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return c;
    }

    @DexIgnore
    public final String a(String str, String str2) throws Exception {
        String[] split = str.split("]");
        if (split.length == 3) {
            byte[] decode = Base64.decode(split[0], 0);
            byte[] decode2 = Base64.decode(split[1], 0);
            byte[] decode3 = Base64.decode(split[2], 0);
            SecretKey g = g(decode, str2);
            Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
            instance.init(2, g, new GCMParameterSpec(128, decode2));
            return new String(instance.doFinal(decode3), "UTF-8");
        }
        throw new IllegalArgumentException("Invalid encypted text format");
    }

    @DexIgnore
    public final String b(String str, int i) {
        char[][] cArr = this.f4165a;
        char c2 = cArr[1][5];
        char c3 = cArr[0][1];
        char c4 = cArr[1][1];
        char c5 = cArr[2][0];
        char c6 = cArr[4][0];
        char c7 = cArr[5][2];
        char c8 = cArr[5][3];
        char c9 = cArr[6][3];
        char c10 = cArr[9][2];
        char c11 = cArr[10][1];
        char c12 = cArr[3][4];
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < str.length(); i2++) {
            sb.append((char) (str.charAt(i2) ^ new char[]{c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, 0}[(i2 + i) % 12]));
        }
        return sb.toString();
    }

    @DexReplace
    public Access c(Context context) {
        Access access;
        synchronized (this) {
            try {
                int e = e();
                access = null;
                try {
                    access = d(context, e);
                } catch (Exception e2) {
                    if (e == 0) {
                        access = d(context, 4);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                return new Access(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            }
        }
        return access;
    }

    @DexReplace
    public final Access d(Context context, int i) throws Exception {
        int i2;
        Random random = new Random();
        int nextInt = random.nextInt(1000);
        while (true) {
            i2 = nextInt + 1;
            if (h(i2)) {
                break;
            }
            nextInt = random.nextInt(1000);
        }
        AccessGroup accessGroup = null;
        try {
            String str = new String(getData(i2, i), StandardCharsets.UTF_8);
            accessGroup = (AccessGroup) this.b.k(a(b(str, i2), b(new String(find(context, i2), StandardCharsets.UTF_8), i2).trim()), AccessGroup.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (accessGroup == null) {
            return new Access(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }
        if (i != 0) {
            if (i == 1) {
                return accessGroup.getOreo();
            }
            if (i != 4) {
                return accessGroup.getLollipop();
            }
        }
        return accessGroup.getPie();
    }

    @DexIgnore
    public final int e() {
        return 0;
    }

    @DexIgnore
    public final native byte[] find(Context context, int i);

    @DexIgnore
    public final SecretKey g(byte[] bArr, String str) throws Exception {
        return new SecretKeySpec(SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1", "BC").generateSecret(new PBEKeySpec(str.toCharArray(), bArr, 1000, 128)).getEncoded(), "AES");
    }

    @DexIgnore
    public final native byte[] getData(int i, int i2);

    @DexIgnore
    public final boolean h(int i) {
        boolean z = false;
        int i2 = 3;
        if (i <= 3 || i % 2 == 0) {
            if (i == 2 || i == 3) {
                z = true;
            }
            return z;
        }
        while (((double) i2) <= Math.sqrt((double) i) && i % i2 != 0) {
            i2 += 2;
        }
        return i % i2 != 0;
    }
}
