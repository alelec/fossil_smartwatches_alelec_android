package com.portfolio.platform;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.webkit.MimeTypeMap;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.facebook.places.model.PlaceFields;
import com.facebook.stetho.Stetho;
import com.fossil.Ao7;
import com.fossil.Aq5;
import com.fossil.Bw7;
import com.fossil.Bx1;
import com.fossil.Cc7;
import com.fossil.Cj5;
import com.fossil.Cl5;
import com.fossil.Ct0;
import com.fossil.Dl7;
import com.fossil.El7;
import com.fossil.Em7;
import com.fossil.Eu7;
import com.fossil.G11;
import com.fossil.Go7;
import com.fossil.Gq5;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Hr7;
import com.fossil.Ic7;
import com.fossil.Im7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Kq4;
import com.fossil.Kq5;
import com.fossil.L37;
import com.fossil.Ll5;
import com.fossil.Lu7;
import com.fossil.Lw1;
import com.fossil.Mq4;
import com.fossil.Nc7;
import com.fossil.Ns0;
import com.fossil.O01;
import com.fossil.Ob7;
import com.fossil.Ph5;
import com.fossil.Pm0;
import com.fossil.Pm7;
import com.fossil.Q27;
import com.fossil.Q88;
import com.fossil.Qq7;
import com.fossil.Qt0;
import com.fossil.Rq4;
import com.fossil.Rt0;
import com.fossil.Ry1;
import com.fossil.Sl5;
import com.fossil.Tt5;
import com.fossil.Uc7;
import com.fossil.Ul5;
import com.fossil.Um5;
import com.fossil.Uo4;
import com.fossil.Uq4;
import com.fossil.Ux7;
import com.fossil.V74;
import com.fossil.Vl5;
import com.fossil.Vt7;
import com.fossil.Wt7;
import com.fossil.Xn7;
import com.fossil.Yn7;
import com.fossil.Yx1;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.mapped.An4;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Iface;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Lk6;
import com.mapped.Mj6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.ServiceUtils;
import com.mapped.TimeUtils;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMapping;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;
import com.misfit.frameworks.buttonservice.model.watchparams.Version;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.buttonservice.utils.ThemeExtentionKt;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.buddy_challenge.analytic.BCAnalytic;
import com.portfolio.platform.cloudimage.ResolutionHelper;
import com.portfolio.platform.data.model.Installation;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppDataRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.helper.FitnessHelper;
import com.portfolio.platform.localization.LocalizationManager;
import com.portfolio.platform.manager.validation.DataValidationManager;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import com.portfolio.platform.receiver.AppPackageInstallReceiver;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.receiver.SmsMmsReceiver;
import com.portfolio.platform.response.ResponseKt;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.service.workout.WorkoutTetherGpsManager;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.uirenew.splash.SplashScreenActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.usecase.GeneratePassphraseUseCase;
import com.portfolio.platform.workers.PushPendingDataWorker;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.FlutterEngineCache;
import io.flutter.embedding.engine.dart.DartExecutor;
import io.flutter.view.FlutterMain;
import java.io.File;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import net.sqlcipher.database.SQLiteDatabase;
import android.content.BroadcastReceiver;
import android.content.SharedPreferences;
import android.os.SystemClock;
import com.misfit.frameworks.common.enums.Action;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexReplace;
import retrofit.Endpoints;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAppend;
import lanchon.dexpatcher.annotation.DexAction;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioApp extends Rt0 implements Application.ActivityLifecycleCallbacks {
    @DexIgnore
    public static /* final */ String b0;
    @DexIgnore
    public static boolean c0;
    @DexIgnore
    public static PortfolioApp d0;
    @DexIgnore
    public static boolean e0;
    @DexIgnore
    public static Nc7 f0;
    @DexIgnore
    public static IButtonConnectivity g0;
    @DexIgnore
    public static /* final */ inner get; // = new inner(null);
    @DexIgnore
    public DataValidationManager A;
    @DexIgnore
    public UserRepository B;
    @DexIgnore
    public WatchFaceRepository C;
    @DexIgnore
    public QuickResponseRepository D;
    @DexIgnore
    public WorkoutSettingRepository E;
    @DexIgnore
    public WatchAppDataRepository F;
    @DexIgnore
    public WorkoutTetherGpsManager G;
    @DexIgnore
    public MutableLiveData<String> H; // = new MutableLiveData<>();
    @DexIgnore
    public boolean I;
    @DexIgnore
    public boolean J; // = true;
    @DexIgnore
    public /* final */ Handler K; // = new Handler();
    @DexIgnore
    public Runnable L;
    @DexIgnore
    public Iface M;
    @DexIgnore
    public Thread.UncaughtExceptionHandler N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public ServerError P;
    @DexIgnore
    public Ul5 Q;
    @DexIgnore
    public Vl5 R;
    @DexIgnore
    public NetworkChangedReceiver S;
    @DexIgnore
    public /* final */ Gq5 T; // = new Gq5();
    @DexIgnore
    public Aq5 U;
    @DexIgnore
    public SmsMmsReceiver V;
    @DexIgnore
    public ThemeRepository W;
    @DexIgnore
    public Q27 X;
    @DexIgnore
    public GeneratePassphraseUseCase Y;
    @DexIgnore
    public BCAnalytic Z;
    @DexIgnore
    public /* final */ Il6 a0; // = Jv7.a(Ux7.b(null, 1, null).plus(Bw7.b()));
    @DexIgnore
    public LocalizationManager b;
    @DexIgnore
    public AppPackageInstallReceiver c;
    @DexIgnore
    public An4 d;
    @DexIgnore
    public SummariesRepository e;
    @DexIgnore
    public SleepSummariesRepository f;
    @DexIgnore
    public AlarmHelper g;
    @DexIgnore
    public GuestApiService h;
    @DexIgnore
    public U04 i;
    @DexIgnore
    public ApiServiceV2 j;
    @DexIgnore
    public L37 k;
    @DexIgnore
    public Uq4 l;
    @DexIgnore
    public DeleteLogoutUserUseCase m;
    @DexIgnore
    public AnalyticsHelper s;
    @DexIgnore
    public ApplicationEventListener t;
    @DexIgnore
    public DeviceRepository u;
    @DexIgnore
    public FitnessHelper v;
    @DexIgnore
    public ShakeFeedbackService w;
    @DexIgnore
    public DianaPresetRepository x;
    @DexIgnore
    public Ic7 y;
    @DexIgnore
    public WatchLocalizationRepository z;

    @DexIgnore
    public enum ActivityState {
        CREATE,
        START,
        RESUME,
        PAUSE,
        STOP,
        DESTROY
    }

    @DexAdd
    public static Thread inject_key_for;

    @DexAdd
    public PackageManager getPackageManager() {
        if ((inject_key_for != null) && (inject_key_for == Thread.currentThread())) {
            return new PackageManagerWrapper(super.getPackageManager());
        } else {
            return super.getPackageManager();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {1047}, m = "switchActiveDevice")
    public static final class a0 extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a0(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.X1(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b0 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Lk6 $continuation;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isMigration$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Ob7 $wfThemeData$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

//        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
//        public static final class a extends Qq7 implements Hg6<Lw1, Cd6> {
//            @DexIgnore
//            public /* final */ /* synthetic */ b0 this$0;
//
//            @DexIgnore
//            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
//            public a(b0 b0Var) {
//                super(1);
//                this.this$0 = b0Var;
//            }
//
//            @DexIgnore
//            /* Return type fixed from 'java.lang.Object' to match base method */
//            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
//            @Override // com.mapped.Hg6
//            public /* bridge */ /* synthetic */ Cd6 invoke(Lw1 lw1) {
//                invoke(lw1);
//                return Cd6.a;
//            }
//
//            @DexIgnore
//            public final void invoke(Lw1 lw1) {
//                Wg6.c(lw1, "it");
//                Lk6 lk6 = this.this$0.$continuation;
//                Dl7.Ai ai = Dl7.Companion;
//                lk6.resumeWith(Dl7.constructor-impl(new Lc6(lw1.getData(), 0)));
//            }
//        }

//        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
//        public static final class b extends Qq7 implements Hg6<Yx1, Cd6> {
//            @DexIgnore
//            public /* final */ /* synthetic */ b0 this$0;
//
//            @DexIgnore
//            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
//            public b(b0 b0Var) {
//                super(1);
//                this.this$0 = b0Var;
//            }
//
//            @DexIgnore
//            /* Return type fixed from 'java.lang.Object' to match base method */
//            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
//            @Override // com.mapped.Hg6
//            public /* bridge */ /* synthetic */ Cd6 invoke(Yx1 yx1) {
//                invoke(yx1);
//                return Cd6.a;
//            }
//
//            @DexIgnore
//            public final void invoke(Yx1 yx1) {
//                Wg6.c(yx1, "error");
//                ILocalFLogger local = FLogger.INSTANCE.getLocal();
//                String d = PortfolioApp.get.d();
//                local.e(d, "themeDataToBinaryData - onError: " + yx1 + " - " + yx1.getErrorCode());
//                if (yx1.getErrorCode() instanceof Bx1) {
//                    int i = yx1.getErrorCode() == Bx1.NETWORK_UNAVAILABLE ? 601 : 500;
//                    Lk6 lk6 = this.this$0.$continuation;
//                    Dl7.Ai ai = Dl7.Companion;
//                    lk6.resumeWith(Dl7.constructor-impl(new Lc6(null, Integer.valueOf(i))));
//                    return;
//                }
//                Lk6 lk62 = this.this$0.$continuation;
//                Dl7.Ai ai2 = Dl7.Companion;
//                lk62.resumeWith(Dl7.constructor-impl(new Lc6(null, -1)));
//            }
//        }

//        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
//        public static final class c extends Qq7 implements Hg6<Yx1, Cd6> {
//            @DexIgnore
//            public /* final */ /* synthetic */ b0 this$0;
//
//            @DexIgnore
//            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
//            public c(b0 b0Var) {
//                super(1);
//                this.this$0 = b0Var;
//            }
//
//            @DexIgnore
//            /* Return type fixed from 'java.lang.Object' to match base method */
//            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
//            @Override // com.mapped.Hg6
//            public /* bridge */ /* synthetic */ Cd6 invoke(Yx1 yx1) {
//                invoke(yx1);
//                return Cd6.a;
//            }
//
//            @DexIgnore
//            public final void invoke(Yx1 yx1) {
//                Wg6.c(yx1, "it");
//                Lk6 lk6 = this.this$0.$continuation;
//                Dl7.Ai ai = Dl7.Companion;
//                lk6.resumeWith(Dl7.constructor-impl(new Lc6(null, -1)));
//            }
//        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b0(Lk6 lk6, Xe6 xe6, PortfolioApp portfolioApp, boolean z, Ob7 ob7) {
            super(2, xe6);
            this.$continuation = lk6;
            this.this$0 = portfolioApp;
            this.$isMigration$inlined = z;
            this.$wfThemeData$inlined = ob7;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            b0 b0Var = new b0(this.$continuation, xe6, this.this$0, this.$isMigration$inlined, this.$wfThemeData$inlined);
            b0Var.p$ = (Il6) obj;
            throw null;
            //return b0Var;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((b0) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            IButtonConnectivity b2;
            Version uiPackageOsVersion;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                PortfolioApp portfolioApp = this.this$0;
                this.L$0 = il6;
                this.label = 1;
                if (portfolioApp.s0(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Ry1 ry1 = null;
            throw null;
//            if (!(this.$isMigration$inlined || (b2 = PortfolioApp.get.b()) == null || (uiPackageOsVersion = b2.getUiPackageOsVersion(this.this$0.J())) == null)) {
//                ry1 = new Ry1(uiPackageOsVersion.getMajor(), uiPackageOsVersion.getMinor());
//            }
//            ILocalFLogger local = FLogger.INSTANCE.getLocal();
//            String d2 = PortfolioApp.get.d();
//            local.d(d2, "themeDataToBinaryData - isMigration: " + this.$isMigration$inlined + " - sdkVersion: " + ry1);
//            ThemeExtentionKt.toThemeEditor(Cc7.h(this.$wfThemeData$inlined)).c(ry1).s(new a(this)).r(new b(this)).q(new c(this));
//            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2180}, m = "apply")
    public static final class c extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.q(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$updateActiveDeviceInfoLog$1", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class c0 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        public c0(Xe6 xe6) {
            super(2, xe6);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            c0 c0Var = new c0(xe6);
            c0Var.p$ = (Il6) obj;
            throw null;
            //return c0Var;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((c0) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                ActiveDeviceInfo d = AppHelper.g.c().d();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d2 = PortfolioApp.get.d();
                local.d(d2, ".updateActiveDeviceInfoLog(), " + new Gson().t(d));
                FLogger.INSTANCE.getRemote().updateActiveDeviceInfo(d);
                try {
                    IButtonConnectivity b = PortfolioApp.get.b();
                    if (b != null) {
                        b.updateActiveDeviceInfoLog(d);
                    }
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String d3 = PortfolioApp.get.d();
                    local2.e(d3, ".updateActiveDeviceInfoToButtonService(), error=" + e);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2164}, m = "applyByThemeFile")
    public static final class d extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.r(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2490}, m = "updateAppLogInfo")
    public static final class d0 extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d0(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g2(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$configureWorkManager$2", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class e extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(PortfolioApp portfolioApp, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            e eVar = new e(this.this$0, xe6);
            eVar.p$ = (Il6) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((e) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                O01.Ai ai = new O01.Ai();
                ai.b(this.this$0.a0());
                O01 a2 = ai.a();
                Wg6.b(a2, "Configuration.Builder()\n\u2026\n                .build()");
                G11.f(this.this$0, a2);
                PushPendingDataWorker.H.a();
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {772}, m = "updateCrashlyticsUserInformation")
    public static final class e0 extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e0(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h2(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$deleteInstanceId$2", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class f extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        public f(Xe6 xe6) {
            super(2, xe6);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            f fVar = new f(xe6);
            fVar.p$ = (Il6) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((f) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                try {
                    FirebaseInstanceId.m().g();
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String d = PortfolioApp.get.d();
                    local.d(d, "deleteInstanceId ex-" + e);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {1381, FailureCode.FAILED_TO_SET_STOP_WATCH_SETTING}, m = "updatePercentageGoalProgress")
    public static final class f0 extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f0(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.j2(false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {334}, m = "downloadWatchAppData")
    public static final class g extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.D(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$executeSetLocalization$1", f = "PortfolioApp.kt", l = {FailureCode.FAILED_TO_SET_NOTIFICATION}, m = "invokeSuspend")
    public static final class h extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(PortfolioApp portfolioApp, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            h hVar = new h(this.this$0, this.$serial, xe6);
            hVar.p$ = (Il6) obj;
            throw null;
            //return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((h) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object watchLocalizationFromServer;
            IButtonConnectivity b;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                WatchLocalizationRepository f0 = this.this$0.f0();
                this.L$0 = il6;
                this.label = 1;
                watchLocalizationFromServer = f0.getWatchLocalizationFromServer(true, this);
                if (watchLocalizationFromServer == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                watchLocalizationFromServer = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str = (String) watchLocalizationFromServer;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d2 = PortfolioApp.get.d();
            local.d(d2, "path of localization file: " + str);
            if (!(str == null || (b = PortfolioApp.get.b()) == null)) {
                throw null;
//                b.setLocalizationData(new LocalizationData(str, null, 2, null), this.$serial);
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements CoroutineUseCase.Ei<DeleteLogoutUserUseCase.Di, DeleteLogoutUserUseCase.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(PortfolioApp portfolioApp) {
            this.a = portfolioApp;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(DeleteLogoutUserUseCase.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(DeleteLogoutUserUseCase.Ci ci) {
            Wg6.c(ci, "errorValue");
            FLogger.INSTANCE.getLocal().d(PortfolioApp.get.d(), "logout unsuccessfully");
            this.a.C1(null);
            this.a.B1(false);
        }

        @DexIgnore
        public void c(DeleteLogoutUserUseCase.Di di) {
            Wg6.c(di, "responseValue");
            FLogger.INSTANCE.getLocal().d(PortfolioApp.get.d(), "logout successfully - start welcome activity");
            Intent intent = new Intent(this.a, WelcomeActivity.class);
            intent.addFlags(268468224);
            this.a.startActivity(intent);
            this.a.C1(null);
            this.a.B1(false);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(DeleteLogoutUserUseCase.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class inner {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.PortfolioApp$Companion", f = "PortfolioApp.kt", l = {2542, 2544}, m = "updateButtonService")
        public static final class a extends Jf6 {
            @DexIgnore
            public char C$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public int label;
            @DexIgnore
            public /* synthetic */ Object result;
            @DexIgnore
            public /* final */ /* synthetic */ inner this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(inner inner, Xe6 xe6) {
                super(xe6);
                this.this$0 = inner;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                this.result = obj;
                this.label |= RecyclerView.UNDEFINED_DURATION;
                return this.this$0.m(null, this);
            }
        }

        @DexIgnore
        public inner() {
        }

        @DexIgnore
        public /* synthetic */ inner(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Nc7 a() {
            return PortfolioApp.f0;
        }

        @DexIgnore
        public final IButtonConnectivity b() {
            return PortfolioApp.g0;
        }

        @DexIgnore
        public final String d() {
            return PortfolioApp.b0;
        }

        @DexIgnore
        public final boolean e() {
            return PortfolioApp.e0;
        }

        @DexIgnore
        public final boolean f() {
            return PortfolioApp.c0;
        }

        @DexIgnore
        public final void g(Object obj) {
            Wg6.c(obj, Constants.EVENT);
            Nc7 a2 = a();
            if (a2 != null) {
                a2.i(obj);
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public final void h(Object obj) {
            Wg6.c(obj, "o");
            try {
                Nc7 a2 = a();
                if (a2 != null) {
                    a2.j(obj);
                } else {
                    Wg6.i();
                    throw null;
                }
            } catch (Exception e) {
            }
        }

        @DexIgnore
        public final void i(IButtonConnectivity iButtonConnectivity) {
            PortfolioApp.g0 = iButtonConnectivity;
        }

        @DexIgnore
        public final PortfolioApp instance() {
            PortfolioApp portfolioApp = PortfolioApp.d0;
            if (portfolioApp != null) {
                return portfolioApp;
            }
            Wg6.n("instance");
            throw null;
        }

        @DexIgnore
        public final void j(boolean z) {
            PortfolioApp.l(z);
        }

        @DexIgnore
        public final void k(MFDeviceService.b bVar) {
            PortfolioApp.m(bVar);
        }

        @DexIgnore
        public final void l(Object obj) {
            Wg6.c(obj, "o");
            try {
                Nc7 a2 = a();
                if (a2 != null) {
                    a2.l(obj);
                } else {
                    Wg6.i();
                    throw null;
                }
            } catch (Exception e) {
            }
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00cb  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00cf  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object m(com.misfit.frameworks.buttonservice.IButtonConnectivity r18, com.mapped.Xe6<? super com.mapped.Cd6> r19) {
            /*
            // Method dump skipped, instructions count: 424
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.inner.m(com.misfit.frameworks.buttonservice.IButtonConnectivity, com.mapped.Xe6):java.lang.Object");
        }

        @DexIgnore
        public final void n(MFDeviceService.b bVar) {
            Wg6.c(bVar, Constants.SERVICE);
            k(bVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$forceReconnectDevice$2", f = "PortfolioApp.kt", l = {1910}, m = "invokeSuspend")
    public static final class j extends Ko7 implements Coroutine<Il6, Xe6<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(PortfolioApp portfolioApp, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            j jVar = new j(this.this$0, this.$serial, xe6);
            jVar.p$ = (Il6) obj;
            throw null;
            //return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Object> xe6) {
            throw null;
            //return ((j) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            IButtonConnectivity b;
            String str;
            Object S;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d2 = PortfolioApp.get.d();
                local.d(d2, "Inside " + PortfolioApp.get.d() + ".forceReconnectDevice");
                b = PortfolioApp.get.b();
                if (b != null) {
                    str = this.$serial;
                    PortfolioApp portfolioApp = this.this$0;
                    this.L$0 = il6;
                    this.L$1 = b;
                    this.L$2 = str;
                    this.label = 1;
                    S = portfolioApp.S(this);
                    if (S == d) {
                        return d;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else if (i == 1) {
                String str2 = (String) this.L$2;
                b = (IButtonConnectivity) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                try {
                    El7.b(obj);
                    str = str2;
                    S = obj;
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String d3 = PortfolioApp.get.d();
                    local2.e(d3, "Error inside " + PortfolioApp.get.d() + ".deviceReconnect - e=" + e);
                    return Cd6.a;
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            throw null;
//            return Ao7.f(b.deviceForceReconnect(str, (UserProfile) S));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {375, 387, 388, 389, 390, 427, 428}, m = "getCurrentUserProfile")
    public static final class k extends Jf6 {
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public float F$0;
        @DexIgnore
        public float F$1;
        @DexIgnore
        public float F$2;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public int I$2;
        @DexIgnore
        public int I$3;
        @DexIgnore
        public int I$4;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.S(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {555, 562}, m = "getPassphrase")
    public static final class l extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {353}, m = "getUserRegisterDate")
    public static final class m extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.n0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {446}, m = "getWorkoutDetectionSetting")
    public static final class n extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2188}, m = "initSdkForThemeParsing")
    public static final class o extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$onActiveDeviceStealed$2", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class p extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(PortfolioApp portfolioApp, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            p pVar = new p(this.this$0, xe6);
            pVar.p$ = (Il6) obj;
            throw null;
            //return pVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((p) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = PortfolioApp.get.d();
                local.d(d, "activeDevice=" + this.this$0.J() + ", was stealed remove it!!!");
                DeviceHelper.o.x(this.this$0.J());
                PortfolioApp portfolioApp = this.this$0;
                portfolioApp.b2(portfolioApp.J());
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp b;

        @DexIgnore
        public q(PortfolioApp portfolioApp) {
            this.b = portfolioApp;
        }

        @DexIgnore
        public final void run() {
            if (!this.b.u0() || !this.b.J) {
                PortfolioApp.get.j(false);
                FLogger.INSTANCE.getLocal().d(PortfolioApp.get.d(), "still foreground");
                return;
            }
            this.b.p1(false);
            PortfolioApp.get.j(true);
            FLogger.INSTANCE.getLocal().d(PortfolioApp.get.d(), "went background");
            this.b.C0();
            Ul5 ul5 = this.b.Q;
            if (ul5 != null) {
                ul5.c("");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$onCreate$1", f = "PortfolioApp.kt", l = {604, 605, 606, 610, 625, 636}, m = "invokeSuspend")
    public static final class r extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public r(PortfolioApp portfolioApp, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            r rVar = new r(this.this$0, xe6);
            rVar.p$ = (Il6) obj;
            throw null;
            //return rVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((r) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00a3  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00f2  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00fd  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x011f  */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x0173  */
        /* JADX WARNING: Removed duplicated region for block: B:51:0x018e  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x01a5  */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x01c3  */
        /* JADX WARNING: Removed duplicated region for block: B:65:0x01d5  */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x01dc  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 498
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.r.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s implements Thread.UncaughtExceptionHandler {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp a;

        @DexIgnore
        public s(PortfolioApp portfolioApp) {
            this.a = portfolioApp;
        }

        @DexIgnore
        public final void uncaughtException(Thread thread, Throwable th) {
            FLogger.INSTANCE.getLocal().e(PortfolioApp.get.d(), "uncaughtException - ex=" + th);
            th.printStackTrace();
            Wg6.b(th, "e");
            StackTraceElement[] stackTrace = th.getStackTrace();
            int length = stackTrace != null ? stackTrace.length : 0;
            if (length > 0) {
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (stackTrace != null) {
                        StackTraceElement stackTraceElement = stackTrace[i];
                        Wg6.b(stackTraceElement, "element");
                        String className = stackTraceElement.getClassName();
                        Wg6.b(className, "element.className");
                        String simpleName = ButtonService.class.getSimpleName();
                        Wg6.b(simpleName, "ButtonService::class.java.simpleName");
                        if (Wt7.v(className, simpleName, false, 2, null)) {
                            FLogger.INSTANCE.getLocal().e(PortfolioApp.get.d(), "uncaughtException - stopLogService");
                            this.a.U1(FailureCode.APP_CRASH_FROM_BUTTON_SERVICE);
                            break;
                        }
                        i++;
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
            long currentTimeMillis = System.currentTimeMillis();
            this.a.k0().w1(currentTimeMillis);
            FLogger.INSTANCE.getLocal().d(PortfolioApp.get.d(), "Inside .uncaughtException - currentTime = " + currentTimeMillis);
            Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.a.N;
            if (uncaughtExceptionHandler != null) {
                uncaughtExceptionHandler.uncaughtException(thread, th);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$pairDevice$2", f = "PortfolioApp.kt", l = {FailureCode.UNEXPECTED_DISCONNECT}, m = "invokeSuspend")
    public static final class t extends Ko7 implements Coroutine<Il6, Xe6<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $macAddress;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public t(PortfolioApp portfolioApp, String str, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
            this.$serial = str;
            this.$macAddress = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            t tVar = new t(this.this$0, this.$serial, this.$macAddress, xe6);
            tVar.p$ = (Il6) obj;
            throw null;
            //return tVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Object> xe6) {
            throw null;
            //return ((t) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            IButtonConnectivity b;
            String str;
            Object S;
            String str2;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d2 = PortfolioApp.get.d();
                local.d(d2, "Inside " + PortfolioApp.get.d() + ".pairDevice");
                b = PortfolioApp.get.b();
                if (b != null) {
                    str = this.$serial;
                    String str3 = this.$macAddress;
                    PortfolioApp portfolioApp = this.this$0;
                    this.L$0 = il6;
                    this.L$1 = b;
                    this.L$2 = str;
                    this.L$3 = str3;
                    this.label = 1;
                    S = portfolioApp.S(this);
                    if (S == d) {
                        return d;
                    }
                    str2 = str3;
                } else {
                    Wg6.i();
                    throw null;
                }
            } else if (i == 1) {
                String str4 = (String) this.L$3;
                str = (String) this.L$2;
                IButtonConnectivity iButtonConnectivity = (IButtonConnectivity) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                try {
                    El7.b(obj);
                    S = obj;
                    str2 = str4;
                    b = iButtonConnectivity;
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String d3 = PortfolioApp.get.d();
                    local2.e(d3, "Error inside " + PortfolioApp.get.d() + ".pairDevice - e=" + e);
                    return Cd6.a;
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            throw null;
//            return Ao7.f(b.pairDevice(str, str2, (UserProfile) S));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2156}, m = "preview")
    public static final class u extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public u(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.Q0(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2150}, m = "previewByThemeFile")
    public static final class v extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public v(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.R0(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {788}, m = "registerContactObserver")
    public static final class w extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public w(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.V0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$saveInstallation$2", f = "PortfolioApp.kt", l = {1134, 1177}, m = "invokeSuspend")
    public static final class x extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.PortfolioApp$saveInstallation$2$1", f = "PortfolioApp.kt", l = {1178}, m = "invokeSuspend")
        public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Installation $installation;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ x this$0;

//            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
//            @Lf6(c = "com.portfolio.platform.PortfolioApp$saveInstallation$2$1$response$1", f = "PortfolioApp.kt", l = {1178}, m = "invokeSuspend")
//            public static final class a extends Ko7 implements Hg6<Xe6<? super Q88<Installation>>, Object> {
//                @DexIgnore
//                public int label;
//                @DexIgnore
//                public /* final */ /* synthetic */ a this$0;
//
//                @DexIgnore
//                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
//                public a(a aVar, Xe6 xe6) {
//                    super(1, xe6);
//                    this.this$0 = aVar;
//                }
//
//                @DexIgnore
//                @Override // com.fossil.Zn7
//                public final Xe6<Cd6> create(Xe6<?> xe6) {
//                    Wg6.c(xe6, "completion");
//                    return new a(this.this$0, xe6);
//                }
//
//                @DexIgnore
//                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
//                @Override // com.mapped.Hg6
//                public final Object invoke(Xe6<? super Q88<Installation>> xe6) {
//                    throw null;
//                    //return ((a) create(xe6)).invokeSuspend(Cd6.a);
//                }
//
//                @DexIgnore
//                @Override // com.fossil.Zn7
//                public final Object invokeSuspend(Object obj) {
//                    Object d = Yn7.d();
//                    int i = this.label;
//                    if (i == 0) {
//                        El7.b(obj);
//                        ApiServiceV2 Z = this.this$0.this$0.this$0.Z();
//                        Installation installation = this.this$0.$installation;
//                        this.label = 1;
//                        Object insertInstallation = Z.insertInstallation(installation, this);
//                        return insertInstallation == d ? d : insertInstallation;
//                    } else if (i == 1) {
//                        El7.b(obj);
//                        return obj;
//                    } else {
//                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
//                    }
//                }
//            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(x xVar, Installation installation, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = xVar;
                this.$installation = installation;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                a aVar = new a(this.this$0, this.$installation, xe6);
                aVar.p$ = (Il6) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d;
                Object d2 = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    throw null;
//                    a aVar = new a(this, null);
//                    this.L$0 = il6;
//                    this.label = 1;
//                    d = ResponseKt.d(aVar, this);
//                    if (d == d2) {
//                        return d2;
//                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    d = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                Ap4 ap4 = (Ap4) d;
                if (ap4 instanceof Kq5) {
                    Kq5 kq5 = (Kq5) ap4;
                    if (kq5.a() != null) {
                        this.this$0.this$0.k0().F0(((Installation) kq5.a()).getInstallationId());
                    }
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public x(PortfolioApp portfolioApp, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            x xVar = new x(this.this$0, xe6);
            xVar.p$ = (Il6) obj;
            throw null;
            //return xVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((x) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x003c  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 285
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.x.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2243}, m = "setImplicitDeviceConfig")
    public static final class y extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public y(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.x1(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2143}, m = "setUpSDK")
    public static final class z extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public z(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.J1(this);
        }
    }

    /*
    static {
        String simpleName = PortfolioApp.class.getSimpleName();
        Wg6.b(simpleName, "PortfolioApp::class.java.simpleName");
        b0 = simpleName;
    }
    */

    @DexIgnore
    public static final IButtonConnectivity O() {
        return g0;
    }

    @DexIgnore
    public static /* synthetic */ void W1(PortfolioApp portfolioApp, String str, Ob7 ob7, String str2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = null;
        }
        if ((i2 & 2) != 0) {
            ob7 = null;
        }
        portfolioApp.V1(str, ob7, str2);
    }

    @DexIgnore
    public static /* synthetic */ Object a2(PortfolioApp portfolioApp, Ob7 ob7, boolean z2, Xe6 xe6, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        return portfolioApp.Z1(ob7, z2, xe6);
    }

    @DexIgnore
    public static final /* synthetic */ void l(boolean z2) {
    }

    @DexIgnore
    public static final /* synthetic */ void m(MFDeviceService.b bVar) {
    }

    @DexIgnore
    public final Object A(Xe6<? super Cd6> xe6) {
        Object g2 = Eu7.g(Bw7.b(), new f(null), xe6);
        throw null;
//        return g2 == Yn7.d() ? g2 : Cd6.a;
    }

    @DexIgnore
    public final boolean A0(String str) {
        Wg6.c(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.isSyncing(str);
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            return false;
        }
    }

    @DexIgnore
    public final void A1(String str) {
        MisfitDeviceProfile deviceProfile;
        MisfitDeviceProfile deviceProfile2;
        Wg6.c(str, "serial");
        try {
            String Y2 = Y();
            IButtonConnectivity iButtonConnectivity = g0;
            String locale = (iButtonConnectivity == null || (deviceProfile2 = iButtonConnectivity.getDeviceProfile(this.H.e())) == null) ? null : deviceProfile2.getLocale();
            An4 an4 = this.d;
            if (an4 != null) {
                String E2 = an4.E(Y2);
                IButtonConnectivity iButtonConnectivity2 = g0;
                String localeVersion = (iButtonConnectivity2 == null || (deviceProfile = iButtonConnectivity2.getDeviceProfile(this.H.e())) == null) ? null : deviceProfile.getLocaleVersion();
                An4 an42 = this.d;
                if (an42 != null) {
                    String D2 = an42.D();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = b0;
                    local.d(str2, "phoneLocale " + Y2 + " - watchLocale: " + locale + " - standardLocale: " + E2 + "\nwatchLocaleVersion: " + localeVersion + " - standardLocaleVersion: " + D2);
                    if (!Wg6.a(E2, locale)) {
                        E(Y2, str);
                    } else if (!Wg6.a(localeVersion, D2)) {
                        E(Y2, str);
                    } else {
                        FLogger.INSTANCE.getLocal().d(b0, "don't need to set to watch");
                    }
                } else {
                    Wg6.n("sharedPreferencesManager");
                    throw null;
                }
            } else {
                Wg6.n("sharedPreferencesManager");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void B(String str, FirmwareData firmwareData, UserProfile userProfile) {
        Wg6.c(str, "serial");
        Wg6.c(firmwareData, "firmwareData");
        Wg6.c(userProfile, "userProfile");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "Inside " + b0 + ".otaDevice");
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceOta(str, firmwareData, userProfile);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "Error inside " + b0 + ".otaDevice - e=" + e2);
        }
    }

    @DexIgnore
    public final Object B0(byte[] bArr, Xe6<? super Boolean> xe6) {
        Boolean a2;
        IButtonConnectivity iButtonConnectivity = g0;
        throw null;
//        return Ao7.a((iButtonConnectivity == null || (a2 = Ao7.a(iButtonConnectivity.isThemePackageEditable(bArr))) == null) ? false : a2.booleanValue());
    }

    @DexIgnore
    public final void B1(boolean z2) {
        this.O = z2;
    }

    @DexIgnore
    public final Object C(String str, Xe6<? super Boolean> xe6) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "downloadWatchAppData(), uiVersion = " + str);
        WatchAppDataRepository watchAppDataRepository = this.F;
        if (watchAppDataRepository != null) {
            return watchAppDataRepository.downloadWatchAppData(P(), str, xe6);
        }
        Wg6.n("mWatchAppDataRepository");
        throw null;
    }

    @DexIgnore
    public final void C0() {
        Ul5 f2 = AnalyticsHelper.f.f("ota_session");
        Ul5 f3 = AnalyticsHelper.f.f("sync_session");
        Ul5 f4 = AnalyticsHelper.f.f("setup_device_session");
        if (f2 != null && f2.f()) {
            f2.a(AnalyticsHelper.f.b("ota_session_go_to_background"));
        } else if (f3 != null && f3.f()) {
            f3.a(AnalyticsHelper.f.b("sync_session_go_to_background"));
        } else if (f4 != null && f4.f()) {
            f4.a(AnalyticsHelper.f.b("setup_device_session_go_to_background"));
        }
    }

    @DexIgnore
    public final void C1(ServerError serverError) {
        this.P = serverError;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object D(com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.PortfolioApp.g
            if (r0 == 0) goto L_0x0038
            r0 = r7
            com.portfolio.platform.PortfolioApp$g r0 = (com.portfolio.platform.PortfolioApp.g) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0047
            if (r0 != r5) goto L_0x003f
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x002c:
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            com.fossil.Ao7.a(r0)
        L_0x0035:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0037:
            return r0
        L_0x0038:
            com.portfolio.platform.PortfolioApp$g r0 = new com.portfolio.platform.PortfolioApp$g
            r0.<init>(r6, r7)
            r1 = r0
            goto L_0x0014
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.b0
            java.lang.String r4 = "downloadWatchAppData()"
            r0.d(r2, r4)
            java.lang.String r0 = r6.m0()
            if (r0 == 0) goto L_0x0035
            com.portfolio.platform.data.source.WatchAppDataRepository r2 = r6.F
            if (r2 == 0) goto L_0x0073
            java.lang.String r4 = r6.P()
            r1.L$0 = r6
            r1.L$1 = r0
            r1.label = r5
            java.lang.Object r0 = r2.downloadWatchAppData(r4, r0, r1)
            if (r0 != r3) goto L_0x002c
            r0 = r3
            goto L_0x0037
        L_0x0073:
            java.lang.String r0 = "mWatchAppDataRepository"
            com.mapped.Wg6.n(r0)
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.D(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void D0() {
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.logOut();
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final long D1(String str, List<? extends MicroAppMapping> list) {
        Wg6.c(str, "serial");
        Wg6.c(list, "mappings");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            for (MicroAppMapping microAppMapping : list) {
                if (microAppMapping != null) {
                    StringBuilder sb = new StringBuilder("Inside .setMappings set mapping of deviceId=" + str + ", gesture=" + microAppMapping.getGesture() + ", appID=" + microAppMapping.getMicroAppId() + ", declarationFiles=");
                    String[] declarationFiles = microAppMapping.getDeclarationFiles();
                    for (String str2 : declarationFiles) {
                        sb.append("||");
                        sb.append(str2);
                    }
                    FLogger.INSTANCE.getLocal().d(b0, sb.toString());
                    FLogger.INSTANCE.getLocal().d(b0, "Inside .setMappings set mapping of deviceId=" + str + ", gesture=" + microAppMapping.getGesture() + ", appID=" + microAppMapping.getMicroAppId() + ", extraInfo=" + Arrays.toString(microAppMapping.getDeclarationFiles()));
                }
            }
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetMapping(str, MicroAppMapping.convertToBLEMapping((List<MicroAppMapping>) list));
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void E(String str, String str2) {
        String str3;
        String str4 = "localization_" + str + '.';
        File[] listFiles = new File(getFilesDir() + "/localization").listFiles();
        int length = listFiles.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                str3 = "";
                break;
            }
            File file = listFiles[i2];
            Wg6.b(file, "file");
            String path = file.getPath();
            Wg6.b(path, "file.path");
            throw null;
//            if (Wt7.v(path, str4, false, 2, null)) {
//                str3 = file.getPath();
//                Wg6.b(str3, "file.path");
//                break;
//            }
//            i2++;
        }
        FLogger.INSTANCE.getLocal().d(b0, "setLocalization - neededLocalizationFilePath: " + str3);
        if (TextUtils.isEmpty(str3)) {
            throw null;
//            Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new h(this, str2, null), 3, null);
//            return;
        }
        IButtonConnectivity iButtonConnectivity = g0;
        if (iButtonConnectivity != null) {
            throw null;
//            iButtonConnectivity.setLocalizationData(new LocalizationData(str3, null, 2, null), str2);
        }
    }

    @DexIgnore
    public final long E0(String str, NotificationBaseObj notificationBaseObj) {
        Wg6.c(str, "serial");
        Wg6.c(notificationBaseObj, "notification");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "notifyNotificationControlEvent - notification=" + notificationBaseObj);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.notifyNotificationEvent(notificationBaseObj, str);
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".notifyNotificationControlEvent(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long E1(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        Wg6.c(appNotificationFilterSettings, "notificationFilterSettings");
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setNotificationFilterSettings - notificationFilterSettings=" + appNotificationFilterSettings);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setNotificationFilterSettings(appNotificationFilterSettings, str);
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".setNotificationFilterSettings(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void F(ServerError serverError) {
        FLogger.INSTANCE.getLocal().d(b0, "forceLogout");
        if (this.I) {
            DeleteLogoutUserUseCase deleteLogoutUserUseCase = this.m;
            if (deleteLogoutUserUseCase != null) {
                deleteLogoutUserUseCase.e(new DeleteLogoutUserUseCase.Bi(2, null), new i(this));
            } else {
                Wg6.n("mDeleteLogoutUserUseCase");
                throw null;
            }
        } else {
            this.P = serverError;
            this.O = true;
        }
    }

    @DexIgnore
    public final void F0(String str, boolean z2) {
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "notifyWatchAppDataReady(), serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.notifyWatchAppFilesReady(str, z2);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "notifyWatchAppDataReady() - e=" + e2);
        }
    }

    @DexIgnore
    public final void F1(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b0;
        local.d(str3, ".setPairedSerial - serial=" + str + ", macaddress=" + str2);
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setPairedSerial(str, str2);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, "setPairedSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    public final Object G(String str, Xe6<Object> xe6) {
        return Eu7.g(Bw7.b(), new j(this, str, null), xe6);
    }

    @DexIgnore
    public final Object G0(Xe6<? super Cd6> xe6) {
        Object g2 = Eu7.g(Bw7.b(), new p(this, null), xe6);
        throw null;
//        return g2 == Yn7.d() ? g2 : Cd6.a;
    }

    @DexIgnore
    public final long G1(ReplyMessageMappingGroup replyMessageMappingGroup, String str) {
        Wg6.c(replyMessageMappingGroup, "replyMessageGroup");
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setReplyMessageMapping - replyMessageGroup=" + replyMessageMappingGroup);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setReplyMessageMappingSetting(replyMessageMappingGroup, str);
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".setReplyMessageMappingSetting(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final boolean H(String str) {
        Wg6.c(str, "newActiveDeviceSerial");
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                boolean forceSwitchDeviceWithoutErase = iButtonConnectivity.forceSwitchDeviceWithoutErase(str);
                if (!forceSwitchDeviceWithoutErase) {
                    return forceSwitchDeviceWithoutErase;
                }
                J0(str);
                return forceSwitchDeviceWithoutErase;
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    public final void H0(String str, boolean z2, WatchParamsFileMapping watchParamsFileMapping) {
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "onGetWatchParamResponse -serial =" + str + ", content=" + watchParamsFileMapping);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.onSetWatchParamResponse(str, z2, watchParamsFileMapping);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "onSetWatchParamResponse - serial " + str + " exception " + e2);
        }
    }

    @DexIgnore
    public final void H1(String str) {
        Wg6.c(str, ButtonService.USER_ID);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.updateUserId(str);
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "exception when set userId to SDK " + str);
        }
    }

    @DexIgnore
    public final String I() {
        String str;
        String h2;
        String J2 = J();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "generateDeviceNotificationContent activeSerial=" + J2);
        if (TextUtils.isEmpty(J2)) {
            PortfolioApp portfolioApp = d0;
            if (portfolioApp != null) {
                String c2 = Um5.c(portfolioApp, 2131887059);
                Wg6.b(c2, "LanguageHelper.getString\u2026Dashboard_CTA__PairWatch)");
                return c2;
            }
            Wg6.n("instance");
            throw null;
        }
        String c3 = Um5.c(this, 2131887173);
        DeviceRepository deviceRepository = this.u;
        if (deviceRepository != null) {
            String deviceNameBySerial = deviceRepository.getDeviceNameBySerial(J2);
            try {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = b0;
                StringBuilder sb = new StringBuilder();
                sb.append("generateDeviceNotificationContent gattState=");
                IButtonConnectivity iButtonConnectivity = g0;
                if (iButtonConnectivity != null) {
                    sb.append(iButtonConnectivity.getGattState(J2));
                    local2.d(str3, sb.toString());
                    IButtonConnectivity iButtonConnectivity2 = g0;
                    if (iButtonConnectivity2 != null) {
                        if (iButtonConnectivity2.getGattState(J2) == 2) {
                            PortfolioApp portfolioApp2 = d0;
                            if (portfolioApp2 == null) {
                                Wg6.n("instance");
                                throw null;
                            } else if (portfolioApp2.A0(J2)) {
                                PortfolioApp portfolioApp3 = d0;
                                if (portfolioApp3 != null) {
                                    str = Um5.c(portfolioApp3, 2131886784);
                                } else {
                                    Wg6.n("instance");
                                    throw null;
                                }
                            } else {
                                An4 an4 = this.d;
                                if (an4 != null) {
                                    long C2 = an4.C(J2);
                                    if (C2 == 0) {
                                        h2 = "";
                                    } else if (System.currentTimeMillis() - C2 < 60000) {
                                        throw null;
//                                        Hr7 hr7 = Hr7.a;
//                                        PortfolioApp portfolioApp4 = d0;
//                                        if (portfolioApp4 != null) {
//                                            String c4 = Um5.c(portfolioApp4, 2131887191);
//                                            Wg6.b(c4, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
//                                            h2 = String.format(c4, Arrays.copyOf(new Object[]{1}, 1));
//                                            Wg6.b(h2, "java.lang.String.format(format, *args)");
//                                        } else {
//                                            Wg6.n("instance");
//                                            throw null;
//                                        }
                                    } else {
                                        h2 = TimeUtils.h(C2);
                                    }
                                    throw null;
//                                    Hr7 hr72 = Hr7.a;
//                                    PortfolioApp portfolioApp5 = d0;
//                                    if (portfolioApp5 != null) {
//                                        String c5 = Um5.c(portfolioApp5, 2131887179);
//                                        Wg6.b(c5, "LanguageHelper.getString\u2026_Text__LastSyncedDayTime)");
//                                        str = String.format(c5, Arrays.copyOf(new Object[]{h2}, 1));
//                                        Wg6.b(str, "java.lang.String.format(format, *args)");
//                                    } else {
//                                        Wg6.n("instance");
//                                        throw null;
//                                    }
                                } else {
                                    Wg6.n("sharedPreferencesManager");
                                    throw null;
                                }
                            }
                        } else {
                            str = c3;
                        }
                        return deviceNameBySerial + " : " + str;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            } catch (Exception e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str4 = b0;
                local3.d(str4, "generateDeviceNotificationContent e=" + e2);
                str = c3;
            }
        } else {
            Wg6.n("mDeviceRepository");
            throw null;
        }
        throw null;
    }

    @DexIgnore
    public final void I0(String str) {
        Wg6.c(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, ".onPing(), serial=" + str);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.onPing(str);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".onPing() - e=" + e2);
        }
    }

    @DexAdd
    public String secretKey = "";


    @DexAdd
    String convertKeyForGadgetbridge(String key) {
        // https://github.com/Freeyourgadget/Gadgetbridge/wiki/Fossil-Hybrid-HR#running-the-fossil-app-with-mitmproxy-enabled
        byte[] bytes = Base64.decode(key, 0);

        StringBuilder sb = new StringBuilder(16 * 2);
        sb.append("0x");
        for (int j = 0; j < 16; j++) {
            int v = bytes[j] & 0xFF;
            sb.append(String.format("%02x", v));
        }
        return sb.toString();
    }

    @DexReplace
    public final void I1(String key, String str2) {
        Wg6.c(str2, "serial");
        if (!TextUtils.isEmpty(key)) {
            secretKey = convertKeyForGadgetbridge(key);
        }
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local.d(str3, ".setSecretKeyToDevice(), secretKey=" + key);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setSecretKey(str2, key);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, ".setSecretKeyToDevice() - e=" + e2);
        }
    }

    @DexIgnore
    public final String J() {
        An4 an4 = this.d;
        if (an4 != null) {
            String g2 = an4.g();
            return g2 != null ? g2 : "";
        }
        Wg6.n("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final void J0(String str) {
        Wg6.c(str, "newActiveDeviceSerial");
        String J2 = J();
        An4 an4 = this.d;
        if (an4 != null) {
            an4.E0(str);
            this.H.l(str);
            FossilNotificationBar.c.d(this);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "onSwitchActiveDeviceSuccess - current=" + J2 + ", new=" + str);
            return;
        }
        Wg6.n("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object J1(com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.PortfolioApp.z
            if (r0 == 0) goto L_0x004a
            r0 = r7
            com.portfolio.platform.PortfolioApp$z r0 = (com.portfolio.platform.PortfolioApp.z) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x004a
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0058
            if (r3 != r4) goto L_0x0050
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0028:
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x0047
            com.misfit.frameworks.buttonservice.IButtonConnectivity r1 = com.portfolio.platform.PortfolioApp.g0
            if (r1 == 0) goto L_0x006b
            com.fossil.H37 r2 = com.fossil.H37.b
            java.lang.String r2 = r2.d()
            java.lang.String r3 = r0.getUserId()
            com.portfolio.platform.data.model.MFUser$Auth r0 = r0.getAuth()
            java.lang.String r0 = r0.getRefreshToken()
            java.lang.String r4 = "theme/fonts"
            r1.setUpSDKTheme(r2, r3, r0, r4)
        L_0x0047:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0049:
            return r0
        L_0x004a:
            com.portfolio.platform.PortfolioApp$z r0 = new com.portfolio.platform.PortfolioApp$z
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0050:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0058:
            com.fossil.El7.b(r1)
            com.portfolio.platform.data.source.UserRepository r1 = r6.B
            if (r1 == 0) goto L_0x006f
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r1.getCurrentUser(r0)
            if (r0 != r2) goto L_0x0028
            r0 = r2
            goto L_0x0049
        L_0x006b:
            com.mapped.Wg6.i()
            throw r5
        L_0x006f:
            java.lang.String r0 = "mUserRepository"
            com.mapped.Wg6.n(r0)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.J1(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final MutableLiveData<String> K() {
        An4 an4 = this.d;
        if (an4 != null) {
            String g2 = an4.g();
            if (!Wg6.a(g2, this.H.e())) {
                this.H.l(g2);
            }
            return this.H;
        }
        Wg6.n("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final void K0(String str, boolean z2) {
        Wg6.c(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, ".onUpdateSecretKeyToServerResponse(), isSuccess=" + z2);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendPushSecretKeyResponse(str, z2);
                throw null;
//                Cd6 cd6 = Cd6.a;
//                return;
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".onUpdateSecretKeyToServerResponse() - e=" + e2);
            throw null;
//            Cd6 cd62 = Cd6.a;
        }
    }

    @DexIgnore
    public final long K1(String str, int i2, int i3, int i4) {
        Wg6.c(str, "serial");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "Inside " + b0 + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2);
            if (!TextUtils.isEmpty(str)) {
                IButtonConnectivity iButtonConnectivity = g0;
                if (iButtonConnectivity != null) {
                    return iButtonConnectivity.deviceUpdateActivityGoals(str, i2, i3, i4);
                }
                Wg6.i();
                throw null;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "Error Inside " + b0 + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2 + ", caloriesGoal=" + i3 + ", activeTimeGoal=" + i4);
            return time_stamp_for_non_executable_method;
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local3.e(str4, "Error Inside " + b0 + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2 + ", ex=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final boolean L() {
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            return iButtonConnectivity != null && iButtonConnectivity.getGattState(J()) == 2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b0;
            local.d(str, "exception when get gatt state " + e2);
            return false;
        }
    }

    @DexIgnore
    public final Object L0(String str, String str2, Xe6<Object> xe6) {
        return Eu7.g(Bw7.b(), new t(this, str, str2, null), xe6);
    }

    @DexIgnore
    public final void L1(String str, VibrationStrengthObj vibrationStrengthObj) {
        Wg6.c(str, "serial");
        Wg6.c(vibrationStrengthObj, "vibrationStrengthObj");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "Inside " + b0 + ".setVibrationStrength");
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSetVibrationStrength(str, vibrationStrengthObj);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "Error inside " + b0 + ".setVibrationStrength - e=" + e2);
        }
    }

    @DexIgnore
    public final boolean M0(String str, PairingResponse pairingResponse) {
        Wg6.c(str, "serial");
        Wg6.c(pairingResponse, "response");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, ".pairDeviceResponse(), response=" + pairingResponse);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.pairDeviceResponse(str, pairingResponse);
                return true;
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".pairDeviceResponse() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final long M1(WatchAppMappingSettings watchAppMappingSettings, String str) {
        Wg6.c(str, "serial");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "setWatchApps " + watchAppMappingSettings);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setWatchApps(watchAppMappingSettings, str);
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final String N(String str) {
        Wg6.c(str, "packageName");
        throw null;
//        String str2 = (String) Pm7.P(Wt7.Y(str, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0, 6, null));
//        PortfolioApp portfolioApp = d0;
//        if (portfolioApp != null) {
//            PackageManager packageManager = portfolioApp.getPackageManager();
//            try {
//                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 0);
//                return applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo).toString() : str2;
//            } catch (PackageManager.NameNotFoundException e2) {
//                return str2;
//            }
//        } else {
//            Wg6.n("instance");
//            throw null;
//        }
    }

    @DexIgnore
    public final Object N0(byte[] bArr, Xe6<? super Ob7> xe6) {
        ThemeData parseBinaryToThemeData;
        IButtonConnectivity iButtonConnectivity = g0;
        throw null;
//        if (iButtonConnectivity == null || (parseBinaryToThemeData = iButtonConnectivity.parseBinaryToThemeData(J(), bArr)) == null) {
//            return null;
//        }
//        return Cc7.q(parseBinaryToThemeData);
    }

    @DexIgnore
    public final void N1(WorkoutConfigData workoutConfigData, String str) {
        Wg6.c(workoutConfigData, "workoutConfigData");
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setWorkoutConfig - workoutConfigData=" + workoutConfigData);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setWorkoutConfig(workoutConfigData, str);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final long O0(String str, int i2, int i3, boolean z2) {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "Inside " + b0 + ".playVibeNotification");
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.playVibration(str, i2, i3, true);
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "Error inside " + b0 + ".playVibeNotification - e=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long O1(String str, WorkoutDetectionSetting workoutDetectionSetting) {
        Wg6.c(str, "serial");
        Wg6.c(workoutDetectionSetting, "workoutDetectionSetting");
        FLogger.INSTANCE.getLocal().d(b0, "setWorkoutDetectionSetting()");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setWorkoutDetectionSetting(workoutDetectionSetting, str);
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, "setWorkoutDetectionSetting(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final String P() {
        List e2;
        boolean z2;
        boolean z3 = true;
        if (e0) {
            List<String> split = new Mj6(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).split("4.6.0", 0);
            if (!split.isEmpty()) {
                ListIterator<String> listIterator = split.listIterator(split.size());
                while (true) {
                    if (!listIterator.hasPrevious()) {
                        break;
                    }
                    if (listIterator.previous().length() == 0) {
                        z2 = true;
//                        continue;
                    } else {
                        z2 = false;
//                        continue;
                    }
                    if (!z2) {
                        e2 = Pm7.d0(split, listIterator.nextIndex() + 1);
                        break;
                    }
                }
            }
            e2 = Hm7.e();
            Object[] array = e2.toArray(new String[0]);
            if (array != null) {
                String[] strArr = (String[]) array;
                if (strArr.length != 0) {
                    z3 = false;
                }
                if (!z3) {
                    return strArr[0];
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return "4.6.0";
    }

    @DexIgnore
    public final long P0(String str) {
        Wg6.c(str, "serial");
        return O0(str, 1, 1, true);
    }

    @DexIgnore
    public final long P1(String str, Location location) {
        Wg6.c(str, "serial");
        Wg6.c(location, PlaceFields.LOCATION);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setWorkoutGPSDataSession(str, location);
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, ".setWorkoutGPSDataSession - e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final String Q() {
        String name = Ph5.fromInt(Integer.parseInt(Rq4.G.d())).getName();
        Wg6.b(name, "FossilBrand.fromInt(Inte\u2026onfig.brandId)).getName()");
        return name;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object Q0(com.fossil.Ob7 r8, com.mapped.Xe6<? super com.mapped.Cd6> r9) {
        /*
            r7 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.PortfolioApp.u
            if (r0 == 0) goto L_0x0061
            r0 = r9
            com.portfolio.platform.PortfolioApp$u r0 = (com.portfolio.platform.PortfolioApp.u) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0061
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0070
            if (r3 != r4) goto L_0x0068
            java.lang.Object r0 = r1.L$1
            com.fossil.Ob7 r0 = (com.fossil.Ob7) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.PortfolioApp r1 = (com.portfolio.platform.PortfolioApp) r1
            com.fossil.El7.b(r2)
        L_0x002b:
            com.misfit.frameworks.buttonservice.model.watchface.ThemeData r2 = com.fossil.Cc7.h(r0)
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "preview with wfThemeData "
            r5.append(r6)
            r5.append(r0)
            java.lang.String r0 = " buttonThemeData "
            r5.append(r0)
            r5.append(r2)
            java.lang.String r0 = r5.toString()
            r3.d(r4, r0)
            com.misfit.frameworks.buttonservice.IButtonConnectivity r0 = com.portfolio.platform.PortfolioApp.g0
            if (r0 == 0) goto L_0x0082
            java.lang.String r1 = r1.J()
            r0.previewTheme(r1, r2)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0060:
            return r0
        L_0x0061:
            com.portfolio.platform.PortfolioApp$u r0 = new com.portfolio.platform.PortfolioApp$u
            r0.<init>(r7, r9)
            r1 = r0
            goto L_0x0014
        L_0x0068:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0070:
            com.fossil.El7.b(r2)
            r1.L$0 = r7
            r1.L$1 = r8
            r1.label = r4
            java.lang.Object r1 = r7.J1(r1)
            if (r1 == r0) goto L_0x0060
            r0 = r8
            r1 = r7
            goto L_0x002b
        L_0x0082:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.Q0(com.fossil.Ob7, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void Q1(MFUser mFUser) {
        if (mFUser == null) {
            AnalyticsHelper.f.g().s(false);
            return;
        }
        AnalyticsHelper.f.g().w(mFUser.getUserId());
        AnalyticsHelper.f.g().s(mFUser.getDiagnosticEnabled());
    }

    @DexIgnore
    public final int R(String str) {
        Wg6.c(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.getCommunicatorModeBySerial(str);
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            return -1;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object R0(java.lang.String r7, com.mapped.Xe6<? super com.mapped.Cd6> r8) {
        /*
            r6 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.PortfolioApp.v
            if (r0 == 0) goto L_0x0066
            r0 = r8
            com.portfolio.platform.PortfolioApp$v r0 = (com.portfolio.platform.PortfolioApp.v) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0066
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0075
            if (r3 != r4) goto L_0x006d
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.PortfolioApp r1 = (com.portfolio.platform.PortfolioApp) r1
            com.fossil.El7.b(r2)
        L_0x002b:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "previewByThemeFile "
            r4.append(r5)
            r4.append(r0)
            java.lang.String r5 = " isExist "
            r4.append(r5)
            java.io.File r5 = new java.io.File
            r5.<init>(r0)
            boolean r5 = r5.exists()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.d(r3, r4)
            com.misfit.frameworks.buttonservice.IButtonConnectivity r2 = com.portfolio.platform.PortfolioApp.g0
            if (r2 == 0) goto L_0x0063
            java.lang.String r1 = r1.J()
            r2.previewThemeByFile(r1, r0)
        L_0x0063:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0065:
            return r0
        L_0x0066:
            com.portfolio.platform.PortfolioApp$v r0 = new com.portfolio.platform.PortfolioApp$v
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0014
        L_0x006d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0075:
            com.fossil.El7.b(r2)
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r4
            java.lang.Object r1 = r6.J1(r1)
            if (r1 == r0) goto L_0x0065
            r0 = r7
            r1 = r6
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.R0(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void R1(int i2, int i3, int i4, int i5) {
        String J2 = J();
        int min = Math.min(i2, 65535);
        int min2 = Math.min(i3, 65535);
        int min3 = Math.min(i4, 65535);
        int min4 = Math.min(i5, 4294967);
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b0;
            local.d(str, "Inside simulateDisconnection - delay=" + min + ", duration=" + min2);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.simulateDisconnection(J2, min, min2, min3, min4);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local2.e(str2, "Error inside " + b0 + ".simulateDisconnection - e=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x05a7  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x05cc  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x05de  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x05e5  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x05ec  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x05f3  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0601  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x060e A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x01d7  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x01db  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x023e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0253  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x029d  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x02a3  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x02f8  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x02fc  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0330  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0363  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0391  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x03ce  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0408  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x043c  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x044c  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x04a4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0024  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0504  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0554  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x055c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object S(com.mapped.Xe6<? super com.misfit.frameworks.buttonservice.model.UserProfile> r48) {
        /*
        // Method dump skipped, instructions count: 1580
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.S(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final long S0(String str) {
        Wg6.c(str, "serial");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.readCurrentWorkoutSession(str);
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, ".readCurrentWorkoutSession - e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void S1(Cj4 cj4, boolean z2, int i2) {
        Wg6.c(cj4, "factory");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.d(str, "startDeviceSync, isNewDevice=" + z2 + ", syncMode=" + i2);
        Cl5.c.e();
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", 0);
        intent.putExtra("SERIAL", J());
        Ct0.b(this).d(intent);
        String J2 = J();
        if (A0(J2)) {
            FLogger.INSTANCE.getLocal().d(b0, "Device is syncing, Skip this sync.");
        } else {
            cj4.b(J2).e(new Tt5(i2, J2, z2), null);
        }
    }

    @DexIgnore
    public final DataValidationManager T() {
        DataValidationManager dataValidationManager = this.A;
        if (dataValidationManager != null) {
            return dataValidationManager;
        }
        Wg6.n("mDataValidationManager");
        throw null;
    }

    @DexIgnore
    public final long T0() {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceReadRealTimeStep(J());
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final boolean T1(String str, UserProfile userProfile) {
        Wg6.c(str, "serial");
        Wg6.c(userProfile, "userProfile");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        boolean z2 = false;
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "startDeviceSyncInButtonService - serial=" + str);
            if (!TextUtils.isEmpty(str)) {
                IButtonConnectivity iButtonConnectivity = g0;
                if (iButtonConnectivity != null) {
                    time_stamp_for_non_executable_method = iButtonConnectivity.deviceStartSync(str, userProfile);
                    z2 = true;
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = b0;
                local2.e(str3, "startDeviceSyncInButtonService - serial " + str);
            }
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local3.e(str4, "startDeviceSyncInButtonService - serial " + str + " exception " + e2);
        }
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str5 = b0;
        local4.d(str5, "put timestamp " + time_stamp_for_non_executable_method + " for sync session");
        return z2;
    }

    @DexIgnore
    public final String U() {
        String Q2 = Q();
        return Wg6.a(Q2, Ph5.CITIZEN.getName()) ? "citizen" : Wg6.a(Q2, Ph5.UNIVERSAL.getName()) ? "universal" : Endpoints.DEFAULT_NAME;
    }

    @DexIgnore
    public final void U0() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
        intentFilter.addDataScheme(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY);
        registerReceiver(this.c, intentFilter);
    }

    @DexIgnore
    public final void U1(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.d(str, "stopLogService - failureCode=" + i2);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.stopLogService(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local2.e(str2, "stopLogService - ex=" + e2);
        }
    }

    @DexIgnore
    public final FossilDeviceSerialPatternUtil.BRAND V() {
        String Q2 = Q();
        return Wg6.a(Q2, Ph5.CHAPS.getName()) ? FossilDeviceSerialPatternUtil.BRAND.CHAPS : Wg6.a(Q2, Ph5.DIESEL.getName()) ? FossilDeviceSerialPatternUtil.BRAND.DIESEL : Wg6.a(Q2, Ph5.EA.getName()) ? FossilDeviceSerialPatternUtil.BRAND.EA : Wg6.a(Q2, Ph5.KATESPADE.getName()) ? FossilDeviceSerialPatternUtil.BRAND.KATE_SPADE : Wg6.a(Q2, Ph5.MICHAELKORS.getName()) ? FossilDeviceSerialPatternUtil.BRAND.MICHAEL_KORS : Wg6.a(Q2, Ph5.SKAGEN.getName()) ? FossilDeviceSerialPatternUtil.BRAND.SKAGEN : Wg6.a(Q2, Ph5.AX.getName()) ? FossilDeviceSerialPatternUtil.BRAND.ARMANI_EXCHANGE : Wg6.a(Q2, Ph5.RELIC.getName()) ? FossilDeviceSerialPatternUtil.BRAND.RELIC : Wg6.a(Q2, Ph5.MJ.getName()) ? FossilDeviceSerialPatternUtil.BRAND.MARC_JACOBS : Wg6.a(Q2, Ph5.FOSSIL.getName()) ? FossilDeviceSerialPatternUtil.BRAND.FOSSIL : Wg6.a(Q2, Ph5.UNIVERSAL.getName()) ? FossilDeviceSerialPatternUtil.BRAND.UNIVERSAL : Wg6.a(Q2, Ph5.CITIZEN.getName()) ? FossilDeviceSerialPatternUtil.BRAND.CITIZEN : FossilDeviceSerialPatternUtil.BRAND.UNKNOWN;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object V0(com.mapped.Xe6<? super com.mapped.Cd6> r8) {
        /*
            r7 = this;
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = 0
            r5 = 1
            boolean r0 = r8 instanceof com.portfolio.platform.PortfolioApp.w
            if (r0 == 0) goto L_0x007c
            r0 = r8
            com.portfolio.platform.PortfolioApp$w r0 = (com.portfolio.platform.PortfolioApp.w) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x007c
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x008a
            if (r3 != r5) goto L_0x0082
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.El7.b(r1)
            r7 = r0
        L_0x0028:
            r0 = r1
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x00cc
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "registerContactObserver currentUser="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.d(r2, r0)
            com.mapped.PermissionUtils$Ai r0 = com.mapped.PermissionUtils.a
            boolean r0 = r0.l(r7)
            if (r0 == 0) goto L_0x00af
            com.fossil.L37 r0 = r7.k
            if (r0 == 0) goto L_0x00a9
            r0.b()
            android.content.ContentResolver r0 = r7.getContentResolver()
            android.net.Uri r1 = android.provider.ContactsContract.Contacts.CONTENT_URI
            com.fossil.L37 r2 = r7.k
            if (r2 == 0) goto L_0x00a3
            r0.registerContentObserver(r1, r5, r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.PortfolioApp.b0
            java.lang.String r2 = "registerContactObserver success"
            r0.d(r1, r2)
            com.mapped.An4 r0 = r7.d
            if (r0 == 0) goto L_0x009d
            r0.r1(r5)
        L_0x0079:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x007b:
            return r0
        L_0x007c:
            com.portfolio.platform.PortfolioApp$w r0 = new com.portfolio.platform.PortfolioApp$w
            r0.<init>(r7, r8)
            goto L_0x0014
        L_0x0082:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x008a:
            com.fossil.El7.b(r1)
            com.portfolio.platform.data.source.UserRepository r1 = r7.B
            if (r1 == 0) goto L_0x00cf
            r0.L$0 = r7
            r0.label = r5
            java.lang.Object r1 = r1.getCurrentUser(r0)
            if (r1 != r2) goto L_0x0028
            r0 = r2
            goto L_0x007b
        L_0x009d:
            java.lang.String r0 = "sharedPreferencesManager"
            com.mapped.Wg6.n(r0)
            throw r6
        L_0x00a3:
            java.lang.String r0 = "mContactObserver"
            com.mapped.Wg6.n(r0)
            throw r6
        L_0x00a9:
            java.lang.String r0 = "mContactObserver"
            com.mapped.Wg6.n(r0)
            throw r6
        L_0x00af:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "registerContactObserver fail due to enough Permission ="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.d(r2, r0)
            goto L_0x0079
        L_0x00cc:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
            goto L_0x007b
        L_0x00cf:
            java.lang.String r0 = "mUserRepository"
            com.mapped.Wg6.n(r0)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.V0(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void V1(String str, Ob7 ob7, String str2) {
        ThemeData themeData = null;
        Wg6.c(str2, "serial");
        if (str == null && ob7 == null) {
            throw null;
//            throw new Exception("themeFilePath & wfThemeData are not both null - or just themeFilePath null or just wfThemeData null");
        }
        IButtonConnectivity iButtonConnectivity = g0;
        if (iButtonConnectivity != null) {
            if (ob7 != null) {
                themeData = Cc7.h(ob7);
            }
            throw null;
//            iButtonConnectivity.storeThemeConfig(str2, str, themeData);
//            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final int W(String str) {
        Wg6.c(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.getGattState(str);
            }
            return 0;
        } catch (Exception e2) {
            return 0;
        }
    }

    @DexIgnore
    public final void W0() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.provider.Telephony.WAP_PUSH_RECEIVED");
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        SmsMmsReceiver smsMmsReceiver = this.V;
        if (smsMmsReceiver != null) {
            registerReceiver(smsMmsReceiver, intentFilter);
        } else {
            Wg6.n("mMessageReceiver");
            throw null;
        }
    }

    @DexIgnore
    public final String X() {
        Locale locale = Locale.getDefault();
        Wg6.b(locale, "locale");
        String language = locale.getLanguage();
        String country = locale.getCountry();
        if (TextUtils.isEmpty(language)) {
            return "";
        }
        if (Wg6.a(language, "iw")) {
            language = "he";
        }
        if (Wg6.a(language, "in")) {
            language = "id";
        }
        if (Wg6.a(language, "ji")) {
            language = "yi";
        }
        if (!TextUtils.isEmpty(country)) {
            throw null;
//            Hr7 hr7 = Hr7.a;
//            Locale locale2 = Locale.US;
//            Wg6.b(locale2, "Locale.US");
//            language = String.format(locale2, "%s-%s", Arrays.copyOf(new Object[]{language, country}, 2));
//            Wg6.b(language, "java.lang.String.format(locale, format, *args)");
        }
        Wg6.b(language, "localeString");
        return language;
    }

    @DexIgnore
    public final void X0() {
        try {
            Aq5 aq5 = this.U;
            if (aq5 != null) {
                registerReceiver(aq5, new IntentFilter("android.intent.action.PHONE_STATE"));
                W0();
                return;
            }
            Wg6.n("mPhoneCallReceiver");
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b0;
            local.e(str, "registerTelephonyReceiver throw exception: " + e2.getMessage());
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f A[SYNTHETIC, Splitter:B:18:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object X1(java.lang.String r7, com.mapped.Xe6<? super java.lang.Boolean> r8) {
        /*
            r6 = this;
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 1
            r4 = 0
            boolean r0 = r8 instanceof com.portfolio.platform.PortfolioApp.a0
            if (r0 == 0) goto L_0x005c
            r0 = r8
            com.portfolio.platform.PortfolioApp$a0 r0 = (com.portfolio.platform.PortfolioApp.a0) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x005c
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x006b
            if (r3 != r5) goto L_0x0063
            boolean r3 = r1.Z$0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.PortfolioApp r1 = (com.portfolio.platform.PortfolioApp) r1
            com.fossil.El7.b(r2)
            r7 = r0
        L_0x002f:
            r0 = r2
            com.misfit.frameworks.buttonservice.model.UserProfile r0 = (com.misfit.frameworks.buttonservice.model.UserProfile) r0
            if (r0 != 0) goto L_0x007f
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "Error inside "
            r2.append(r4)
            java.lang.String r4 = com.portfolio.platform.PortfolioApp.b0
            r2.append(r4)
            java.lang.String r4 = ".switchActiveDevice - user is null"
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            r0.e(r1, r2)
            java.lang.Boolean r0 = com.fossil.Ao7.a(r3)
        L_0x005b:
            return r0
        L_0x005c:
            com.portfolio.platform.PortfolioApp$a0 r0 = new com.portfolio.platform.PortfolioApp$a0
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0015
        L_0x0063:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x006b:
            com.fossil.El7.b(r2)
            r1.L$0 = r6
            r1.L$1 = r7
            r1.Z$0 = r4
            r1.label = r5
            java.lang.Object r2 = r6.S(r1)
            if (r2 == r0) goto L_0x005b
            r3 = r4
            r1 = r6
            goto L_0x002f
        L_0x007f:
            com.misfit.frameworks.buttonservice.IButtonConnectivity r2 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x00a2 }
            if (r2 == 0) goto L_0x009d
            boolean r0 = r2.switchActiveDevice(r7, r0)     // Catch:{ Exception -> 0x00a2 }
            if (r0 == 0) goto L_0x0095
            int r2 = r7.length()     // Catch:{ Exception -> 0x00a2 }
            if (r2 != 0) goto L_0x009b
            r2 = r5
        L_0x0090:
            if (r2 == 0) goto L_0x0095
            r1.J0(r7)     // Catch:{ Exception -> 0x00a2 }
        L_0x0095:
            r4 = r0
        L_0x0096:
            java.lang.Boolean r0 = com.fossil.Ao7.a(r4)
            goto L_0x005b
        L_0x009b:
            r2 = r4
            goto L_0x0090
        L_0x009d:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        L_0x00a2:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0096
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.X1(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final String Y() {
        Resources resources = getResources();
        Wg6.b(resources, "resources");
        String languageTag = Pm0.a(resources.getConfiguration()).c(0).toLanguageTag();
        Wg6.b(languageTag, "ConfigurationCompat.getL\u2026ation)[0].toLanguageTag()");
        return languageTag;
    }

    @DexIgnore
    public final void Y0(int i2) {
    }

    @DexIgnore
    public final boolean Y1(String str, boolean z2, int i2) {
        Wg6.c(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, ".switchDeviceResponse(), isSuccess=" + z2 + ", failureCode=" + i2);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.switchDeviceResponse(str, z2, i2);
                return true;
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".pairDeviceResponse() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final ApiServiceV2 Z() {
        ApiServiceV2 apiServiceV2 = this.j;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        Wg6.n("mApiService");
        throw null;
    }

    @DexIgnore
    public final void Z0(String str) {
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, ".removeActivePreferenceSerial - serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.removeActiveSerial(str);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "removeActivePreferenceSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    public final Object Z1(Ob7 ob7, boolean z2, Xe6<? super Lc6<byte[], Integer>> xe6) {
        Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
        throw null;
//        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new b0(lu7, null, this, z2, ob7), 3, null);
//        Object t2 = lu7.t();
//        if (t2 == Yn7.d()) {
//            Go7.c(xe6);
//        }
//        return t2;
    }

    @DexIgnore
    public final Ic7 a0() {
        Ic7 ic7 = this.y;
        if (ic7 != null) {
            return ic7;
        }
        Wg6.n("mDaggerAwareWorkerFactory");
        throw null;
    }

    @DexIgnore
    public final void a1(String str) {
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, ".removePairedPreferenceSerial - serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.removePairedSerial(str);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "removePairedPreferenceSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Rt0
    public void attachBaseContext(Context context) {
        Wg6.c(context, "base");
        super.attachBaseContext(context);
        Qt0.l(this);
    }

    @DexIgnore
    public final LocalizationManager b0() {
        return this.b;
    }

    @DexIgnore
    public final void b1() {
        AlarmHelper alarmHelper = this.g;
        if (alarmHelper != null) {
            alarmHelper.a(this);
            AlarmHelper alarmHelper2 = this.g;
            if (alarmHelper2 != null) {
                alarmHelper2.g(this);
            } else {
                Wg6.n("mAlarmHelper");
                throw null;
            }
        } else {
            Wg6.n("mAlarmHelper");
            throw null;
        }
    }

    @DexIgnore
    public final void b2(String str) {
        Wg6.c(str, "serial");
        if (!TextUtils.isEmpty(str)) {
            try {
                if (Vt7.j(str, J(), true)) {
                    An4 an4 = this.d;
                    if (an4 != null) {
                        an4.E0("");
                        this.H.l("");
                    } else {
                        Wg6.n("sharedPreferencesManager");
                        throw null;
                    }
                }
                IButtonConnectivity iButtonConnectivity = g0;
                if (iButtonConnectivity != null) {
                    iButtonConnectivity.deviceUnlink(str);
                    An4 an42 = this.d;
                    if (an42 != null) {
                        an42.Z0(str, 0, false);
                    } else {
                        Wg6.n("sharedPreferencesManager");
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = b0;
                local.e(str2, "Inside " + b0 + ".unlinkDevice - serial=" + str + ", ex=" + e2);
            }
        }
    }

    @DexIgnore
    public final String c0() {
        try {
            int myPid = Process.myPid();
            Object systemService = getSystemService(Context.ACTIVITY_SERVICE);
            if (systemService != null) {
                for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : ((ActivityManager) systemService).getRunningAppProcesses()) {
                    if (runningAppProcessInfo.pid == myPid) {
                        return runningAppProcessInfo.processName;
                    }
                }
                return null;
            }
            throw new Rc6("null cannot be cast to non-null type android.app.ActivityManager");
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d(b0, "exception when get process name");
        }
        throw null;
    }

    @DexIgnore
    public final void c1() {
        String J2 = J();
        try {
            FLogger.INSTANCE.getLocal().d(b0, "Inside resetDeviceSettingToDefault");
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.resetDeviceSettingToDefault(J2);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b0;
            local.e(str, "Error inside " + b0 + ".resetDeviceSettingToDefault - e=" + e2);
        }
    }

    @DexIgnore
    public final void c2() {
        FLogger.INSTANCE.getLocal().d(b0, "unregisterContactObserver");
        try {
            L37 l37 = this.k;
            if (l37 != null) {
                l37.j();
                ContentResolver contentResolver = getContentResolver();
                L37 l372 = this.k;
                if (l372 != null) {
                    contentResolver.unregisterContentObserver(l372);
                    An4 an4 = this.d;
                    if (an4 != null) {
                        an4.r1(false);
                    } else {
                        Wg6.n("sharedPreferencesManager");
                        throw null;
                    }
                } else {
                    Wg6.n("mContactObserver");
                    throw null;
                }
            } else {
                Wg6.n("mContactObserver");
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b0;
            local.d(str, "unregisterContactObserver e=" + e2);
        }
    }

    @DexIgnore
    public final ThemeRepository d0() {
        ThemeRepository themeRepository = this.W;
        if (themeRepository != null) {
            return themeRepository;
        }
        Wg6.n("mThemeRepository");
        throw null;
    }

    @DexIgnore
    public final Object d1(Xe6<? super Cd6> xe6) {
        Object g2 = Eu7.g(Bw7.b(), new x(this, null), xe6);
        throw null;
//        return g2 == Yn7.d() ? g2 : Cd6.a;
    }

    @DexIgnore
    public final void d2() {
        try {
            Aq5 aq5 = this.U;
            if (aq5 != null) {
                unregisterReceiver(aq5);
                SmsMmsReceiver smsMmsReceiver = this.V;
                if (smsMmsReceiver != null) {
                    unregisterReceiver(smsMmsReceiver);
                } else {
                    Wg6.n("mMessageReceiver");
                    throw null;
                }
            } else {
                Wg6.n("mPhoneCallReceiver");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b0;
            local.e(str, "unregisterTelephonyReceiver throw exception: " + e2.getMessage());
        }
    }

    @DexIgnore
    public final UserRepository e0() {
        UserRepository userRepository = this.B;
        if (userRepository != null) {
            return userRepository;
        }
        Wg6.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final boolean e1(String str, String str2) {
        Wg6.c(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local.d(str3, ".sendCurrentSecretKeyToDevice(), secretKey=" + str2);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendCurrentSecretKey(str, str2);
                return true;
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, ".sendCurrentSecretKeyToDevice() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final Rm6 e2() {
        throw null;
//        return Gu7.d(Jv7.a(Bw7.b()), null, null, new c0(null), 3, null);
    }

    @DexIgnore
    public final WatchLocalizationRepository f0() {
        WatchLocalizationRepository watchLocalizationRepository = this.z;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        Wg6.n("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public final long f1(String str, CustomRequest customRequest) {
        Wg6.c(str, "serial");
        Wg6.c(customRequest, Constants.COMMAND);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "sendCustomCommand() - serial=" + str + ", command=" + customRequest);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendCustomCommand(str, customRequest);
                return time_stamp_for_non_executable_method;
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        throw null;
//
    }

    @DexIgnore
    public final void f2(Vl5 vl5) {
        Ul5 ul5;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.d(str, "Active view tracer old: " + this.R + " \n new: " + vl5);
        if (vl5 != null && Wg6.a(vl5.e(), "view_appearance")) {
            Vl5 vl52 = this.R;
            if (!(vl52 == null || vl52 == null || vl52.k(vl5) || (ul5 = this.Q) == null)) {
                Sl5 b2 = AnalyticsHelper.f.b("app_appearance_view_navigate");
                Vl5 vl53 = this.R;
                if (vl53 != null) {
                    String j2 = vl53.j();
                    if (j2 != null) {
                        b2.a("prev_view", j2);
                        String j3 = vl5.j();
                        if (j3 != null) {
                            b2.a("next_view", j3);
                            ul5.a(b2);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            this.R = vl5;
        }
    }

    @DexIgnore
    public final WorkoutTetherGpsManager g0() {
        WorkoutTetherGpsManager workoutTetherGpsManager = this.G;
        if (workoutTetherGpsManager != null) {
            return workoutTetherGpsManager;
        }
        Wg6.n("mWorkoutGpsManager");
        throw null;
    }

    @DexIgnore
    public final void g1(DeviceAppResponse deviceAppResponse, String str) {
        Wg6.c(deviceAppResponse, "deviceAppResponse");
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "sendComplicationAppInfo - deviceAppResponse=" + deviceAppResponse);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendDeviceAppResponse(deviceAppResponse, str);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0032 A[Catch:{ Exception -> 0x005b }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g2(com.mapped.Xe6<? super com.mapped.Cd6> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.PortfolioApp.d0
            if (r0 == 0) goto L_0x0038
            r0 = r6
            com.portfolio.platform.PortfolioApp$d0 r0 = (com.portfolio.platform.PortfolioApp.d0) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0046
            if (r3 != r4) goto L_0x003e
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.misfit.frameworks.buttonservice.log.model.AppLogInfo r0 = (com.misfit.frameworks.buttonservice.log.model.AppLogInfo) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            r1.updateAppLogInfo(r0)
            com.misfit.frameworks.buttonservice.IButtonConnectivity r1 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x005b }
            if (r1 == 0) goto L_0x0035
            r1.updateAppLogInfo(r0)     // Catch:{ Exception -> 0x005b }
        L_0x0035:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0037:
            return r0
        L_0x0038:
            com.portfolio.platform.PortfolioApp$d0 r0 = new com.portfolio.platform.PortfolioApp$d0
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x003e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0046:
            com.fossil.El7.b(r1)
            com.portfolio.platform.helper.AppHelper$Ai r1 = com.portfolio.platform.helper.AppHelper.g
            com.portfolio.platform.helper.AppHelper r1 = r1.c()
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.e(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0037
        L_0x005b:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = ".updateAppLogInfo(), error="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.e(r2, r0)
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.g2(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Iface getIface() {
        Iface iface = this.M;
        if (iface != null) {
            return iface;
        }
        Wg6.n("applicationComponent");
        throw null;
    }

    @DexIgnore
    public final String h0(Uri uri) {
        if (Wg6.a(uri.getScheme(), "content")) {
            return getContentResolver().getType(uri);
        }
        String path = uri.getPath();
        if (path != null) {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(path)).toString()));
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void h1(String str, NotificationBaseObj notificationBaseObj) {
        Wg6.c(str, "serial");
        Wg6.c(notificationBaseObj, "notification");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "sendNotificationToDevice packageName " + notificationBaseObj);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSendNotification(str, notificationBaseObj);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".sendDianaNotification() - e=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object h2(com.mapped.Xe6<? super com.mapped.Cd6> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.PortfolioApp.e0
            if (r0 == 0) goto L_0x0062
            r0 = r6
            com.portfolio.platform.PortfolioApp$e0 r0 = (com.portfolio.platform.PortfolioApp.e0) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0062
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0070
            if (r3 != r4) goto L_0x0068
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x0083
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "updateCrashlyticsUserInformation currentUser="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            com.fossil.V74 r1 = com.fossil.V74.a()
            java.lang.String r2 = r0.getUserId()
            r1.d(r2)
            com.fossil.V74 r1 = com.fossil.V74.a()
            java.lang.String r2 = "UserId"
            java.lang.String r0 = r0.getUserId()
            r1.c(r2, r0)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0061:
            return r0
        L_0x0062:
            com.portfolio.platform.PortfolioApp$e0 r0 = new com.portfolio.platform.PortfolioApp$e0
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0068:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0070:
            com.fossil.El7.b(r1)
            com.portfolio.platform.data.source.UserRepository r1 = r5.B
            if (r1 == 0) goto L_0x0086
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.getCurrentUser(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0061
        L_0x0083:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
            goto L_0x0061
        L_0x0086:
            java.lang.String r0 = "mUserRepository"
            com.mapped.Wg6.n(r0)
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.h2(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object i0(com.mapped.Xe6<? super char[]> r10) {
        /*
        // Method dump skipped, instructions count: 237
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.i0(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void i1(String str, DeviceAppResponse deviceAppResponse) {
        Wg6.c(str, "serial");
        Wg6.c(deviceAppResponse, "deviceAppResponse");
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendMicroAppRemoteActivity(str, deviceAppResponse);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, "Error inside " + b0 + ".sendMicroAppRemoteActivity - e=" + e2);
        }
    }

    @DexIgnore
    public final void i2(Installation installation) {
        installation.setLocaleIdentifier(X());
    }

    @DexIgnore
    public final /* synthetic */ Object j0(Xe6<? super ReplyMessageMappingGroup> xe6) {
        QuickResponseRepository quickResponseRepository = this.D;
        if (quickResponseRepository != null) {
            List<QuickResponseMessage> allQuickResponse = quickResponseRepository.getAllQuickResponse();
            ArrayList<QuickResponseMessage> arrayList = new ArrayList();
            throw null;
//            for (T t2 : allQuickResponse) {
//                if (!Ao7.a(t2.getResponse().length() == 0).booleanValue()) {
//                    arrayList.add(t2);
//                }
//            }
//            ArrayList arrayList2 = new ArrayList(Im7.m(arrayList, 10));
//            for (QuickResponseMessage quickResponseMessage : arrayList) {
//                arrayList2.add(new ReplyMessageMapping(String.valueOf(quickResponseMessage.getId()), quickResponseMessage.getResponse()));
//            }
//            return new ReplyMessageMappingGroup(Pm7.j0(arrayList2), "icMessage.icon");
        }
        Wg6.n("mQuickResponseRepository");
        throw null;
    }

    @DexIgnore
    public final void j1(MusicResponse musicResponse, String str) {
        Wg6.c(musicResponse, "musicAppResponse");
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "sendMusicAppResponse - musicAppResponse=" + musicResponse);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendMusicAppResponse(musicResponse, str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object j2(boolean r11, com.mapped.Xe6<? super com.mapped.Cd6> r12) {
        /*
        // Method dump skipped, instructions count: 202
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.j2(boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final An4 k0() {
        An4 an4 = this.d;
        if (an4 != null) {
            return an4;
        }
        Wg6.n("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final void k1(String str, String str2, int i2) {
        Wg6.c(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local.d(str3, ".sendRandomKeyToDevice(), randomKey=" + str2);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendRandomKey(str, str2, i2);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, ".sendRandomKeyToDevice() - e=" + e2);
        }
    }

    @DexIgnore
    public final long k2(String str) {
        Wg6.c(str, "serial");
        FLogger.INSTANCE.getLocal().d(b0, "verifySecretKey()");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.verifySecretKeySession(str);
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, ".verifySecretKey(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final String l0() {
        An4 an4 = this.d;
        if (an4 != null) {
            String N2 = an4.N();
            return N2 != null ? N2 : "";
        }
        Wg6.n("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final boolean l1(String str, String str2, int i2) {
        Wg6.c(str, "serial");
        Wg6.c(str2, "serverSecretKey");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local.d(str3, ".sendServerSecretKeyToDevice(), serverSecretKey=" + str2);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendServerSecretKey(str, str2, i2);
                return true;
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, ".sendServerSecretKeyToDevice() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r2v1 int), ('.' char), (r1v6 int)] */
    public final String m0() {
        MisfitDeviceProfile m2;
        Ry1 uiPackageOSVersion;
        if (!(!Vt7.l(J())) || (m2 = DeviceHelper.o.j().m(J())) == null || (uiPackageOSVersion = m2.getUiPackageOSVersion()) == null) {
            return null;
        }
        int major = uiPackageOSVersion.getMajor();
        int minor = uiPackageOSVersion.getMinor();
        if (major == 0 && minor == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(major);
        sb.append('.');
        sb.append(minor);
        return sb.toString();
    }

    @DexIgnore
    public final long m1(String str) {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        if (str == null) {
            FLogger.INSTANCE.getLocal().e(b0, "sendingEncryptedDataSession - encryptedData is null");
            An4 an4 = this.d;
            if (an4 != null) {
                an4.m0(Boolean.TRUE);
                return time_stamp_for_non_executable_method;
            }
            Wg6.n("sharedPreferencesManager");
            throw null;
        }
        try {
            byte[] decode = Base64.decode(str, 0);
            An4 an42 = this.d;
            if (an42 != null) {
                Boolean b2 = an42.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = b0;
                local.e(str2, "sendingEncryptedDataSession - encryptedData: " + str + " - latestBCStatus: " + b2);
                IButtonConnectivity iButtonConnectivity = g0;
                if (iButtonConnectivity != null) {
                    String J2 = J();
                    Wg6.b(b2, "latestBCStatus");
                    return iButtonConnectivity.sendingEncryptedDataSession(decode, J2, b2.booleanValue());
                }
                Wg6.i();
                throw null;
            }
            Wg6.n("sharedPreferencesManager");
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".sendingEncryptedDataSession - e=" + e2);
            e2.printStackTrace();
            An4 an43 = this.d;
            if (an43 != null) {
                an43.m0(Boolean.TRUE);
                return time_stamp_for_non_executable_method;
            }
            Wg6.n("sharedPreferencesManager");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object n0(com.mapped.Xe6<? super java.util.Date> r7) {
        /*
            r6 = this;
            r5 = 0
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 1
            boolean r0 = r7 instanceof com.portfolio.platform.PortfolioApp.m
            if (r0 == 0) goto L_0x0046
            r0 = r7
            com.portfolio.platform.PortfolioApp$m r0 = (com.portfolio.platform.PortfolioApp.m) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0046
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0054
            if (r3 != r4) goto L_0x004c
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0028:
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x006b
            java.lang.String r0 = r0.getCreatedAt()
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x006b
            if (r0 == 0) goto L_0x0067
            java.util.Date r0 = com.mapped.TimeUtils.q0(r0)
            java.util.Date r0 = com.fossil.Kl5.b(r0)
            java.lang.String r1 = "TimeHelper.getStartOfDay(registeredDate)"
            com.mapped.Wg6.b(r0, r1)
        L_0x0045:
            return r0
        L_0x0046:
            com.portfolio.platform.PortfolioApp$m r0 = new com.portfolio.platform.PortfolioApp$m
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x004c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0054:
            com.fossil.El7.b(r1)
            com.portfolio.platform.data.source.UserRepository r1 = r6.B
            if (r1 == 0) goto L_0x007e
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r1.getCurrentUser(r0)
            if (r0 != r2) goto L_0x0028
            r0 = r2
            goto L_0x0045
        L_0x0067:
            com.mapped.Wg6.i()
            throw r5
        L_0x006b:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.PortfolioApp.b0
            java.lang.String r2 = "Fail to getCurrentUserRegisteringDate, return new Date(1, 1, 1)"
            r0.d(r1, r2)
            java.util.Date r0 = new java.util.Date
            r0.<init>(r4, r4, r4)
            goto L_0x0045
        L_0x007e:
            java.lang.String r0 = "mUserRepository"
            com.mapped.Wg6.n(r0)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.n0(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void n1(String str, String str2) {
        Wg6.c(str, "serial");
        if (TextUtils.isEmpty(str) || DeviceHelper.o.v(str)) {
            String J2 = J();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local.d(str3, "Inside " + b0 + ".setActiveDeviceSerial - current=" + J2 + ", new=" + str + ", newDevice mac address=" + str2);
            if (str2 == null) {
                str2 = "";
            }
            An4 an4 = this.d;
            if (an4 != null) {
                an4.E0(str);
                if (J2 != str) {
                    this.H.l(str);
                }
                e2();
                try {
                    IButtonConnectivity iButtonConnectivity = g0;
                    if (iButtonConnectivity != null) {
                        iButtonConnectivity.setActiveSerial(str, str2);
                        FossilNotificationBar.c.d(this);
                        return;
                    }
                    Wg6.i();
                    throw null;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                Wg6.n("sharedPreferencesManager");
                throw null;
            }
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.d(str4, "Ignore legacy device serial=" + str);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object o0(com.mapped.Xe6<? super com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.PortfolioApp.n
            if (r0 == 0) goto L_0x002e
            r0 = r7
            com.portfolio.platform.PortfolioApp$n r0 = (com.portfolio.platform.PortfolioApp.n) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002e
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x003c
            if (r3 != r5) goto L_0x0034
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            java.util.List r0 = (java.util.List) r0
            com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting r0 = com.fossil.R47.a(r0)
        L_0x002d:
            return r0
        L_0x002e:
            com.portfolio.platform.PortfolioApp$n r0 = new com.portfolio.platform.PortfolioApp$n
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x0034:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003c:
            com.fossil.El7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = com.portfolio.platform.PortfolioApp.b0
            java.lang.String r4 = "getWorkoutDetectionSetting()"
            r1.d(r3, r4)
            com.portfolio.platform.data.source.WorkoutSettingRepository r1 = r6.E
            if (r1 == 0) goto L_0x005c
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r0 = r1.getWorkoutSettingList(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x002d
        L_0x005c:
            java.lang.String r0 = "mWorkoutSettingRepository"
            com.mapped.Wg6.n(r0)
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.o0(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final long o1(String str, List<? extends Alarm> list) {
        Wg6.c(str, "serial");
        Wg6.c(list, "alarms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setMultipleAlarms - alarms size=" + list.size());
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetListAlarm(str, (List<Alarm>) list);
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "setMultipleAlarms - ex=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public void onActivityCreated(Activity activity, Bundle bundle) {
        Wg6.c(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.CREATE;
    }

    @DexIgnore
    public void onActivityDestroyed(Activity activity) {
        Wg6.c(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.DESTROY;
    }

    @DexIgnore
    public void onActivityPaused(Activity activity) {
        Wg6.c(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.PAUSE;
        if (e0) {
            ShakeFeedbackService shakeFeedbackService = this.w;
            if (shakeFeedbackService != null) {
                shakeFeedbackService.w();
            } else {
                Wg6.n("mShakeFeedbackService");
                throw null;
            }
        }
        this.J = true;
        Runnable runnable = this.L;
        if (runnable != null) {
            this.K.removeCallbacks(runnable);
        }
        this.K.postDelayed(new q(this), 500);
    }

    @DexIgnore
    public void onActivityResumed(Activity activity) {
        Wg6.c(activity, Constants.ACTIVITY);
        if (e0) {
            ShakeFeedbackService shakeFeedbackService = this.w;
            if (shakeFeedbackService != null) {
                shakeFeedbackService.n(activity);
            } else {
                Wg6.n("mShakeFeedbackService");
                throw null;
            }
        }
        ActivityState activityState = ActivityState.RESUME;
        this.J = false;
        boolean z2 = this.I;
        this.I = true;
        Runnable runnable = this.L;
        if (runnable != null) {
            this.K.removeCallbacks(runnable);
        }
        if (!z2) {
            c0 = true;
            FLogger.INSTANCE.getLocal().d(b0, "from background");
            Ul5 ul5 = this.Q;
            if (ul5 != null) {
                ul5.i();
            }
        } else {
            c0 = false;
            FLogger.INSTANCE.getLocal().d(b0, "still foreground");
        }
        if (this.O) {
            F(this.P);
        }
    }

    @DexIgnore
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        Wg6.c(activity, Constants.ACTIVITY);
    }

    @DexIgnore
    public void onActivityStarted(Activity activity) {
        Wg6.c(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.START;
    }

    @DexIgnore
    public void onActivityStopped(Activity activity) {
        Wg6.c(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.STOP;
    }
    @DexAdd
    public class AutoSyncSettingsListener implements SharedPreferences.OnSharedPreferenceChangeListener {
        PortfolioApp p;
        AutoSyncSettingsListener(PortfolioApp _p) {
            p = _p;
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            autoSyncEnabled = AlelecPrefs.getAutoSync(this.p);
            FLogger.INSTANCE.getLocal().v(b0, "autoSyncEnabled: " + autoSyncEnabled);
            // if (AlelecPrefs.getAutoSync(this.p)) {
            //     startAutoSync();
            //     autoSyncEnabled = true
            // } else {
            //     stopAutoSync();
            // }
        }
    }
    @DexAdd
    public boolean autoSyncEnabled = false;
    @DexAdd
    public ScreenReceiver mScreenReceiver = null;
    @DexAdd
    public PendingIntent alarmIntent = null;

    @DexAdd
    public void triggerSync() {
        AlarmManager alarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        if (alarmMgr != null) {
            FLogger.INSTANCE.getLocal().v(b0, "startAutoSync");
            Intent intent = new Intent(this, NotificationReceiver.class);
            intent.setAction(".news.notifications.NotificationReceiver");
            intent.putExtra("ACTION_EVENT", 1);
            if (alarmIntent == null) {
                alarmIntent = PendingIntent.getBroadcast(this, Action.DisplayMode.DATE, intent, FLAG_UPDATE_CURRENT);
            }
            alarmMgr.setExact(AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime() + 3 * 1000,
                    alarmIntent);
        }
    }

    @DexAdd
    public class ScreenReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_SCREEN_OFF.equals(intent.getAction())) {
                // ignore
            } else if (Intent.ACTION_SCREEN_ON.equals(intent.getAction())) {
                FLogger.INSTANCE.getLocal().v(b0, "screen on, autoSyncEnabled: " + autoSyncEnabled);
                autoSyncEnabled = AlelecPrefs.getAutoSync(PortfolioApp.this);
                if (autoSyncEnabled) {
                    triggerSync();
                }
            }
        }

    }

    @DexAdd
    public AutoSyncSettingsListener autoSyncSettingsListener = null;

    @SuppressLint("MissingSuperCall")
    @DexAppend
    public void onCreate() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().build());

        if (autoSyncSettingsListener == null) {
            autoSyncSettingsListener = new AutoSyncSettingsListener(this);
            // if (AlelecPrefs.getAutoSync(this)) {
            //     startAutoSync();
            // }
            autoSyncEnabled = AlelecPrefs.getAutoSync(this);
            AlelecPrefs.registerAutoSync(this, autoSyncSettingsListener);
        }
        if (mScreenReceiver == null) {
            IntentFilter mScreenIntentFilter = new IntentFilter(Intent.ACTION_SCREEN_ON);
            // mScreenIntentFilter.addAction(Intent.ACTION_SCREEN_OFF);
            mScreenReceiver = new ScreenReceiver();
            registerReceiver(mScreenReceiver, mScreenIntentFilter);
        }
        AppCenter.start(this, "21c262cb-8ed6-4a0b-989c-6091cefd5f0b",
                Analytics.class, Crashes.class);
    }

    /*
    public void onCreate() {
        super.onCreate();
        d0 = this;
        e0 = (getApplicationContext().getApplicationInfo().flags & 2) != 0;
        if (x0()) {
            Uo4 uo4 = new Uo4(this);
            Kq4.Zi O3 = Kq4.O3();
            O3.a(uo4);
            Iface b2 = O3.b();
            Wg6.b(b2, "DaggerApplicationCompone\u2026\n                .build()");
            this.M = b2;
            if (b2 != null) {
                b2.H0(this);
                BCAnalytic bCAnalytic = this.Z;
                if (bCAnalytic != null) {
                    bCAnalytic.u(this);
                    System.loadLibrary("FitnessAlgorithm");
                    LifecycleOwner h2 = Ns0.h();
                    Wg6.b(h2, "ProcessLifecycleOwner.get()");
                    Lifecycle lifecycle = h2.getLifecycle();
                    ApplicationEventListener applicationEventListener = this.t;
                    if (applicationEventListener != null) {
                        lifecycle.a(applicationEventListener);
                        MicroAppEventLogger.initialize(this);
                        if (!z0()) {
                            Stetho.initializeWithDefaults(this);
                        }
                        Rm6 unused = Gu7.d(this.a0, null, null, new r(this, null), 3, null);
                        FLogger.INSTANCE.getLocal().d(b0, "On app create");
                        f0 = new Cj5(Uc7.a);
                        MutableLiveData<String> mutableLiveData = this.H;
                        An4 an4 = this.d;
                        if (an4 != null) {
                            mutableLiveData.o(an4.g());
                            registerActivityLifecycleCallbacks(this);
                            String str = "4.6.0";
                            if (e0) {
                                Object[] array = new Mj6(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).split("4.6.0", 2).toArray(new String[0]);
                                if (array != null) {
                                    str = ((String[]) array)[0];
                                } else {
                                    throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                                }
                            }
                            GuestApiService guestApiService = this.h;
                            if (guestApiService != null) {
                                this.b = new LocalizationManager(this, str, guestApiService);
                                this.c = new AppPackageInstallReceiver();
                                Mq4.a();
                                registerReceiver(this.b, new IntentFilter("android.intent.action.LOCALE_CHANGED"));
                                U0();
                                LocalizationManager localizationManager = this.b;
                                if (localizationManager != null) {
                                    registerActivityLifecycleCallbacks(localizationManager.g());
                                    LocalizationManager localizationManager2 = this.b;
                                    if (localizationManager2 != null) {
                                        String simpleName = SplashScreenActivity.class.getSimpleName();
                                        Wg6.b(simpleName, "SplashScreenActivity::class.java.simpleName");
                                        localizationManager2.t(simpleName);
                                        LocalizationManager localizationManager3 = this.b;
                                        if (localizationManager3 != null) {
                                            localizationManager3.q();
                                            ResolutionHelper.INSTANCE.initDeviceDensity(this);
                                            try {
                                                if (Build.VERSION.SDK_INT >= 24) {
                                                    NetworkChangedReceiver networkChangedReceiver = new NetworkChangedReceiver();
                                                    this.S = networkChangedReceiver;
                                                    registerReceiver(networkChangedReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                                                }
                                            } catch (Exception e2) {
                                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                                String str2 = b0;
                                                local.e(str2, "onCreate - jobScheduler - ex=" + e2);
                                            }
                                            IntentFilter intentFilter = new IntentFilter();
                                            intentFilter.addAction("android.intent.action.TIME_TICK");
                                            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
                                            registerReceiver(this.T, intentFilter);
                                            X0();
                                            this.N = Thread.getDefaultUncaughtExceptionHandler();
                                            Thread.setDefaultUncaughtExceptionHandler(new s(this));
                                            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                                            StrictMode.setVmPolicy(builder.build());
                                            builder.detectFileUriExposure();
                                            AlarmHelper alarmHelper = this.g;
                                            if (alarmHelper != null) {
                                                alarmHelper.f(this);
                                                AlarmHelper alarmHelper2 = this.g;
                                                if (alarmHelper2 != null) {
                                                    alarmHelper2.g(this);
                                                    this.Q = AnalyticsHelper.f.c("app_appearance");
                                                    AnalyticsHelper analyticsHelper = this.s;
                                                    if (analyticsHelper != null) {
                                                        analyticsHelper.q("diana_sdk_version", ButtonService.Companion.getSdkVersionV2());
                                                        if (!TextUtils.isEmpty(J())) {
                                                            FLogger.INSTANCE.getLocal().d(b0, "Service Tracking - startForegroundService in PortfolioApp onCreate");
                                                            ServiceUtils.Ai.e(ServiceUtils.a, this, MFDeviceService.class, null, 4, null);
                                                            ServiceUtils.Ai.e(ServiceUtils.a, this, ButtonService.class, null, 4, null);
                                                        }
                                                        FlutterEngine flutterEngine = new FlutterEngine(this);
                                                        flutterEngine.getDartExecutor().executeDartEntrypoint(new DartExecutor.DartEntrypoint(FlutterMain.findAppBundlePath(), Constants.MAP));
                                                        FlutterEngineCache.getInstance().put("map_engine_id", flutterEngine);
                                                        FlutterEngine flutterEngine2 = new FlutterEngine(this);
                                                        flutterEngine2.getDartExecutor().executeDartEntrypoint(new DartExecutor.DartEntrypoint(FlutterMain.findAppBundlePath(), "screenShotMap"));
                                                        FlutterEngineCache.getInstance().put("screenshot_engine_id", flutterEngine2);
                                                        return;
                                                    }
                                                    Wg6.n("mAnalyticsHelper");
                                                    throw null;
                                                }
                                                Wg6.n("mAlarmHelper");
                                                throw null;
                                            }
                                            Wg6.n("mAlarmHelper");
                                            throw null;
                                        }
                                        Wg6.i();
                                        throw null;
                                    }
                                    Wg6.i();
                                    throw null;
                                }
                                Wg6.i();
                                throw null;
                            }
                            Wg6.n("mGuestApiService");
                            throw null;
                        }
                        Wg6.n("sharedPreferencesManager");
                        throw null;
                    }
                    Wg6.n("mAppEventManager");
                    throw null;
                }
                Wg6.n("bcAnalytic");
                throw null;
            }
            Wg6.n("applicationComponent");
            throw null;
        }
    }
     */

    @DexIgnore
    public void onLowMemory() {
        super.onLowMemory();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.d(str, "Inside " + b0 + ".onLowMemory");
        System.runFinalization();
    }

    @DexIgnore
    public void onTerminate() {
        super.onTerminate();
        FLogger.INSTANCE.getLocal().d(b0, "---Inside .onTerminate of Application");
        d2();
        unregisterReceiver(this.b);
        c2();
        LocalizationManager localizationManager = this.b;
        unregisterActivityLifecycleCallbacks(localizationManager != null ? localizationManager.g() : null);
        if (Build.VERSION.SDK_INT >= 24) {
            FLogger.INSTANCE.getLocal().d(b0, "unregister NetworkChangedReceiver");
            unregisterReceiver(this.S);
        }
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.d(str, "Inside " + b0 + ".onTrimMemory");
        super.onTrimMemory(i2);
        System.runFinalization();
    }

    @DexIgnore
    public final void p(CommunicateMode communicateMode, String str, String str2) {
        Wg6.c(communicateMode, "communicateMode");
        Wg6.c(str, "serial");
        Wg6.c(str2, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b0;
        local.d(str3, "addLog - communicateMode=" + communicateMode + ", serial=" + str + ", message=" + str2);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.addLog(communicateMode.ordinal(), str, str2);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, "addLog - ex=" + e2);
        }
    }

    @DexIgnore
    public final boolean p0() {
        Object systemService = getSystemService("connectivity");
        if (systemService != null) {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) systemService).getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
        }
        throw new Rc6("null cannot be cast to non-null type android.net.ConnectivityManager");
    }

    @DexIgnore
    public final void p1(boolean z2) {
        this.I = z2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object q(com.fossil.Ob7 r8, com.mapped.Xe6<? super com.mapped.Cd6> r9) {
        /*
            r7 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.PortfolioApp.c
            if (r0 == 0) goto L_0x0061
            r0 = r9
            com.portfolio.platform.PortfolioApp$c r0 = (com.portfolio.platform.PortfolioApp.c) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0061
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0070
            if (r3 != r4) goto L_0x0068
            java.lang.Object r0 = r1.L$1
            com.fossil.Ob7 r0 = (com.fossil.Ob7) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.PortfolioApp r1 = (com.portfolio.platform.PortfolioApp) r1
            com.fossil.El7.b(r2)
        L_0x002b:
            com.misfit.frameworks.buttonservice.model.watchface.ThemeData r2 = com.fossil.Cc7.h(r0)
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "apply with wfThemeData "
            r5.append(r6)
            r5.append(r0)
            java.lang.String r0 = " buttonThemeData "
            r5.append(r0)
            r5.append(r2)
            java.lang.String r0 = r5.toString()
            r3.d(r4, r0)
            com.misfit.frameworks.buttonservice.IButtonConnectivity r0 = com.portfolio.platform.PortfolioApp.g0
            if (r0 == 0) goto L_0x0082
            java.lang.String r1 = r1.J()
            r0.applyTheme(r1, r2)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0060:
            return r0
        L_0x0061:
            com.portfolio.platform.PortfolioApp$c r0 = new com.portfolio.platform.PortfolioApp$c
            r0.<init>(r7, r9)
            r1 = r0
            goto L_0x0014
        L_0x0068:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0070:
            com.fossil.El7.b(r2)
            r1.L$0 = r7
            r1.L$1 = r8
            r1.label = r4
            java.lang.Object r1 = r7.J1(r1)
            if (r1 == r0) goto L_0x0060
            r0 = r8
            r1 = r7
            goto L_0x002b
        L_0x0082:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.q(com.fossil.Ob7, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final boolean q0() {
        WorkoutTetherGpsManager workoutTetherGpsManager = this.G;
        if (workoutTetherGpsManager != null) {
            return workoutTetherGpsManager.l();
        }
        Wg6.n("mWorkoutGpsManager");
        throw null;
    }

    @DexIgnore
    public final void q1(List<? extends Alarm> list) {
        Wg6.c(list, "alarms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.e(str, "setAutoAlarms - alarms=" + list);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSetAutoListAlarm((List<Alarm>) list);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local2.e(str2, "setAutoAlarms - e=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object r(java.lang.String r7, com.mapped.Xe6<? super com.mapped.Cd6> r8) {
        /*
            r6 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.PortfolioApp.d
            if (r0 == 0) goto L_0x0066
            r0 = r8
            com.portfolio.platform.PortfolioApp$d r0 = (com.portfolio.platform.PortfolioApp.d) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0066
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0075
            if (r3 != r4) goto L_0x006d
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.PortfolioApp r1 = (com.portfolio.platform.PortfolioApp) r1
            com.fossil.El7.b(r2)
        L_0x002b:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "applyByThemeFile "
            r4.append(r5)
            r4.append(r0)
            java.lang.String r5 = " isExist "
            r4.append(r5)
            java.io.File r5 = new java.io.File
            r5.<init>(r0)
            boolean r5 = r5.exists()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.e(r3, r4)
            com.misfit.frameworks.buttonservice.IButtonConnectivity r2 = com.portfolio.platform.PortfolioApp.g0
            if (r2 == 0) goto L_0x0063
            java.lang.String r1 = r1.J()
            r2.applyThemeByFile(r1, r0)
        L_0x0063:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0065:
            return r0
        L_0x0066:
            com.portfolio.platform.PortfolioApp$d r0 = new com.portfolio.platform.PortfolioApp$d
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0014
        L_0x006d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0075:
            com.fossil.El7.b(r2)
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r4
            java.lang.Object r1 = r6.J1(r1)
            if (r1 == r0) goto L_0x0065
            r0 = r7
            r1 = r6
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.r(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object r0(Xe6<? super Cd6> xe6) {
        FLogger.INSTANCE.getLocal().d(b0, "initCrashlytics");
        V74.a().c("SDK Version V2", ButtonService.Companion.getSdkVersionV2());
        V74.a().c("Locale", Locale.getDefault().toString());
        Object h2 = h2(xe6);
        return h2 == Yn7.d() ? h2 : Cd6.a;
    }

    @DexIgnore
    public final void r1(String str, List<? extends BLEMapping> list) {
        Wg6.c(str, "serial");
        Wg6.c(list, "mappings");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setAutoMapping serial=" + str + "mappingsSize=" + list.size());
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoMapping(str, (List<BLEMapping>) list);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void s(String str) {
        Wg6.c(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.cancelPairDevice(str);
                Cd6 cd6 = Cd6.a;
                return;
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, ".cancelPairDevice() - e=" + e2);
            Cd6 cd62 = Cd6.a;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object s0(com.mapped.Xe6<? super com.mapped.Cd6> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.PortfolioApp.o
            if (r0 == 0) goto L_0x0065
            r0 = r6
            com.portfolio.platform.PortfolioApp$o r0 = (com.portfolio.platform.PortfolioApp.o) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0065
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0073
            if (r3 != r4) goto L_0x006b
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.El7.b(r1)
            r5 = r0
        L_0x0027:
            r0 = r1
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x0062
            com.fossil.Cx1 r1 = com.fossil.Cx1.f
            r1.k(r5)
            com.fossil.Cx1 r1 = com.fossil.Cx1.f
            com.fossil.Ft1 r2 = new com.fossil.Ft1
            java.lang.String r3 = r0.getUserId()
            com.portfolio.platform.data.model.MFUser$Auth r4 = r0.getAuth()
            java.lang.String r4 = r4.getRefreshToken()
            r2.<init>(r3, r4)
            r1.o(r2)
            com.fossil.Cx1 r1 = com.fossil.Cx1.f
            com.fossil.H37 r2 = com.fossil.H37.b
            java.lang.String r2 = r2.d()
            r1.l(r2)
            com.fossil.Cx1 r1 = com.fossil.Cx1.f
            java.lang.String r2 = "theme/fonts"
            r1.m(r2)
            com.fossil.Cx1 r1 = com.fossil.Cx1.f
            java.lang.String r0 = r0.getUserId()
            r1.p(r0)
        L_0x0062:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0064:
            return r0
        L_0x0065:
            com.portfolio.platform.PortfolioApp$o r0 = new com.portfolio.platform.PortfolioApp$o
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x006b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0073:
            com.fossil.El7.b(r1)
            com.portfolio.platform.data.source.UserRepository r1 = r5.B
            if (r1 == 0) goto L_0x0086
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r1 = r1.getCurrentUser(r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0064
        L_0x0086:
            java.lang.String r0 = "mUserRepository"
            com.mapped.Wg6.n(r0)
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.s0(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void s1(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        Wg6.c(appNotificationFilterSettings, "notificationFilterSettings");
        Wg6.c(str, "serial");
        throw null;
//        Iterator<T> it = appNotificationFilterSettings.getNotificationFilters().iterator();
//        while (it.hasNext()) {
//            ILocalFLogger local = FLogger.INSTANCE.getLocal();
//            String str2 = b0;
//            local.d(str2, "setAutoNotificationFilterSettings " + ((Object) it.next()));
//        }
//        try {
//            IButtonConnectivity iButtonConnectivity = g0;
//            if (iButtonConnectivity != null) {
//                iButtonConnectivity.setAutoNotificationFilterSettings(appNotificationFilterSettings, str);
//            } else {
//                Wg6.i();
//                throw null;
//            }
//        } catch (Exception e2) {
//            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
//            String str3 = b0;
//            local2.e(str3, ".setAutoNotificationFilterSettings(), e=" + e2);
//            e2.printStackTrace();
//        }
    }

    @DexIgnore
    public final void t(CommunicateMode communicateMode, String str, CommunicateMode communicateMode2, String str2) {
        Wg6.c(communicateMode, "curCommunicateMode");
        Wg6.c(str, "curSerial");
        Wg6.c(communicateMode2, "newCommunicateMode");
        Wg6.c(str2, "newSerial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b0;
        local.d(str3, "changePendingLogKey - curCommunicateMode=" + communicateMode + ", curSerial=" + str + ", newCommunicateMode=" + communicateMode2 + ", newSerial=" + str2);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.changePendingLogKey(communicateMode.ordinal(), str, communicateMode2.ordinal(), str2);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, "changePendingLogKey - ex=" + e2);
        }
    }

    @DexIgnore
    public final void t0() {
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.interruptCurrentSession(J());
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b0;
            local.e(str, "Error while interruptCurrentSession - e=" + e2);
        }
    }

    @DexIgnore
    public final void t1(String str) {
        int j2;
        String str2;
        if (str == null) {
            j2 = 1024;
            str2 = "";
        } else {
            j2 = Ll5.j(str);
            str2 = str;
        }
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local.d(str3, "Inside " + b0 + ".setAutoSecondTimezone - offsetMinutes=" + j2 + ", mSecondTimezoneId=" + str2);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                if (str == null) {
                    str = "";
                }
                iButtonConnectivity.deviceSetAutoSecondTimezone(str);
                return;
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, "Inside " + b0 + ".setAutoSecondTimezone - ex=" + e2);
        }
    }

    @DexIgnore
    public final void u() {
        An4 an4 = this.d;
        if (an4 != null) {
            an4.K0(53);
            File cacheDir = getCacheDir();
            Wg6.b(cacheDir, "cacheDirectory");
            File file = new File(cacheDir.getParent());
            if (file.exists()) {
                String[] list = file.list();
                for (String str : list) {
                    if (!Wg6.a(str, "lib")) {
                        z(new File(file, str));
                    }
                }
            }
            PendingIntent activity = PendingIntent.getActivity(this, 123456, new Intent(this, SplashScreenActivity.class), 268435456);
            Object systemService = getSystemService(Context.ALARM_SERVICE);
            if (systemService != null) {
                ((AlarmManager) systemService).set(1, System.currentTimeMillis() + ((long) 1000), activity);
                if (19 <= Build.VERSION.SDK_INT) {
                    Object systemService2 = getSystemService(Context.ACTIVITY_SERVICE);
                    if (systemService2 != null) {
                        ((ActivityManager) systemService2).clearApplicationUserData();
                        return;
                    }
                    throw new Rc6("null cannot be cast to non-null type android.app.ActivityManager");
                }
                try {
                    Runtime.getRuntime().exec("pm clear " + getPackageName());
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type android.app.AlarmManager");
            }
        } else {
            Wg6.n("sharedPreferencesManager");
            throw null;
        }
    }

    @DexIgnore
    public final boolean u0() {
        return this.I;
    }

    @DexIgnore
    public final void u1(UserProfile userProfile) {
        Wg6.c(userProfile, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.d(str, "setAutoUserBiometricData - userProfile=" + userProfile);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoUserBiometricData(userProfile);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final /* synthetic */ Object v(Xe6<? super Cd6> xe6) {
        Object g2 = Eu7.g(Bw7.b(), new e(this, null), xe6);
        return g2 == Yn7.d() ? g2 : Cd6.a;
    }

    @DexIgnore
    public final boolean v0() {
        Boolean bool;
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            int[] listActiveCommunicator = iButtonConnectivity != null ? iButtonConnectivity.getListActiveCommunicator() : null;
            List<Integer> c02 = listActiveCommunicator != null ? Em7.c0(listActiveCommunicator) : null;
            if (c02 != null) {
                bool = Boolean.valueOf(!c02.isEmpty());
            } else {
                bool = null;
            }
            if (bool != null) {
                return bool.booleanValue() && c02.contains(Integer.valueOf(CommunicateMode.OTA.getValue()));
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    public final void v1(WatchAppMappingSettings watchAppMappingSettings, String str) {
        Wg6.c(watchAppMappingSettings, "watchAppMappingSettings");
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setAutoWatchAppSettings - watchAppMappingSettings=" + watchAppMappingSettings);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoWatchAppSettings(watchAppMappingSettings, str);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void w(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.e(str, "confirmBcStatusToButtonService - bcStatus: " + z2);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.confirmBCStatus(z2);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0057, code lost:
        if (com.fossil.Wt7.v(r0, "image", false, 2, null) == false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0067, code lost:
        if (com.fossil.Wt7.v(r2, "image", false, 2, null) != false) goto L_0x0069;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean w0(android.content.Intent r10, android.net.Uri r11) {
        /*
            r9 = this;
            r8 = 2
            r1 = 0
            r7 = 0
            java.lang.String r0 = "fileUri"
            com.mapped.Wg6.c(r11, r0)
            java.lang.String r2 = r9.h0(r11)
            if (r10 == 0) goto L_0x006b
            java.lang.String r0 = r10.getType()
        L_0x0012:
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "isImageFile intentMimeType="
            r5.append(r6)
            r5.append(r0)
            java.lang.String r6 = ", uriMimeType="
            r5.append(r6)
            r5.append(r2)
            java.lang.String r5 = r5.toString()
            r3.d(r4, r5)
            java.lang.String r3 = r11.getPath()
            if (r3 == 0) goto L_0x0076
            java.lang.String r4 = "fileUri.path!!"
            com.mapped.Wg6.b(r3, r4)
            java.lang.String r4 = "pickerImage"
            boolean r3 = com.fossil.Wt7.v(r3, r4, r1, r8, r7)
            if (r3 != 0) goto L_0x0069
            boolean r3 = android.text.TextUtils.isEmpty(r0)
            if (r3 != 0) goto L_0x0059
            if (r0 == 0) goto L_0x006e
            java.lang.String r3 = "image"
            boolean r0 = com.fossil.Wt7.v(r0, r3, r1, r8, r7)
            if (r0 != 0) goto L_0x0069
        L_0x0059:
            boolean r0 = android.text.TextUtils.isEmpty(r2)
            if (r0 != 0) goto L_0x007a
            if (r2 == 0) goto L_0x0072
            java.lang.String r0 = "image"
            boolean r0 = com.fossil.Wt7.v(r2, r0, r1, r8, r7)
            if (r0 == 0) goto L_0x007a
        L_0x0069:
            r0 = 1
        L_0x006a:
            return r0
        L_0x006b:
            java.lang.String r0 = ""
            goto L_0x0012
        L_0x006e:
            com.mapped.Wg6.i()
            throw r7
        L_0x0072:
            com.mapped.Wg6.i()
            throw r7
        L_0x0076:
            com.mapped.Wg6.i()
            throw r7
        L_0x007a:
            r0 = r1
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.w0(android.content.Intent, android.net.Uri):boolean");
    }

    @DexIgnore
    public final long w1(String str, boolean z2) {
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setFrontLightEnable() - serial=" + str + ", isFrontLightEnable=" + z2);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setFrontLightEnable(str, z2);
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void x(String str, boolean z2) {
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "confirmStopWorkout -serial =" + str + ", stopWorkout=" + z2);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.confirmStopWorkout(str, z2);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "confirmStopWorkout - serial " + str + " exception " + e2);
        }
    }

    @DexIgnore
    public final boolean x0() {
        return Wg6.a(getPackageName(), c0());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object x1(java.lang.String r6, com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.PortfolioApp.y
            if (r0 == 0) goto L_0x0058
            r0 = r7
            com.portfolio.platform.PortfolioApp$y r0 = (com.portfolio.platform.PortfolioApp.y) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0058
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0067
            if (r3 != r4) goto L_0x005f
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.PortfolioApp r1 = (com.portfolio.platform.PortfolioApp) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.misfit.frameworks.buttonservice.model.UserProfile r0 = (com.misfit.frameworks.buttonservice.model.UserProfile) r0
            if (r0 == 0) goto L_0x009d
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "setImplicitDeviceConfig - currentUserProfile="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            com.misfit.frameworks.buttonservice.IButtonConnectivity r1 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x007c }
            if (r1 == 0) goto L_0x0077
            r1.setImplicitDeviceConfig(r0, r6)     // Catch:{ Exception -> 0x007c }
        L_0x0055:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0057:
            return r0
        L_0x0058:
            com.portfolio.platform.PortfolioApp$y r0 = new com.portfolio.platform.PortfolioApp$y
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x005f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0067:
            com.fossil.El7.b(r2)
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r5.S(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0057
        L_0x0077:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        L_0x007c:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = ".setImplicitDisplayUnitSettings(), e="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.e(r2, r3)
            r0.printStackTrace()
            goto L_0x0055
        L_0x009d:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.PortfolioApp.b0
            java.lang.String r2 = "setImplicitDeviceConfig - currentUserProfile is NULL"
            r0.e(r1, r2)
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.x1(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void y(String str) {
        Wg6.c(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deleteDataFiles(str);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, "Error while deleting data file - e=" + e2);
        }
    }

    @DexIgnore
    public final boolean y0() {
        String string = Settings.Secure.getString(getContentResolver(), "enabled_notification_listeners");
        String str = getPackageName() + "/" + FossilNotificationListenerService.class.getCanonicalName();
        FLogger.INSTANCE.getLocal().d(b0, "isNotificationListenerEnabled() - notificationServicePath = " + str);
        if (!TextUtils.isEmpty(string)) {
            FLogger.INSTANCE.getLocal().d(b0, "isNotificationListenerEnabled() enabledNotificationListeners = " + string);
        }
        if (TextUtils.isEmpty(string)) {
            return false;
        }
        Wg6.b(string, "enabledNotificationListeners");
        return Wt7.v(string, str, false, 2, null);
    }

    @DexIgnore
    public final void y1(UserDisplayUnit userDisplayUnit, String str) {
        Wg6.c(userDisplayUnit, "userDisplayUnit");
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setImplicitDisplayUnitSettings - userDisplayUnit=" + userDisplayUnit);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setImplicitDisplayUnitSettings(userDisplayUnit, str);
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".setImplicitDisplayUnitSettings(), e=" + e2);
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final boolean z(File file) {
        if (file == null) {
            return true;
        }
        if (!file.isDirectory()) {
            return file.delete();
        }
        String[] list = file.list();
        int length = list.length;
        boolean z2 = true;
        for (int i2 = 0; i2 < length; i2++) {
            z2 = z(new File(file, list[i2])) && z2;
        }
        return z2;
    }

    @DexIgnore
    public final boolean z0() {
        return Vt7.j("release", "release", true);
    }

    @DexIgnore
    public final long z1(String str, InactiveNudgeData inactiveNudgeData) {
        Wg6.c(str, "serial");
        Wg6.c(inactiveNudgeData, "inactiveNudgeData");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetInactiveNudgeConfig(str, inactiveNudgeData);
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, ".setInactiveNudgeConfig - e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }
}
