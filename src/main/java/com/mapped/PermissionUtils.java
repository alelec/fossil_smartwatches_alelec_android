package com.mapped;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import com.facebook.places.model.PlaceFields;
import com.fossil.Rk0;
import com.fossil.V78;
import com.fossil.Vt7;
import com.fossil.Wt7;
import com.fossil.Zm7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.service.FossilNotificationListenerService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PermissionUtils {
    @DexIgnore
    public static /* final */ Ai a; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String[] a() {
            HashMap<String, Boolean> b = b();
            Set<String> keySet = b.keySet();
            Wg6.b(keySet, "permissionList.keys");
            ArrayList arrayList = new ArrayList();
            throw null;
//            for (T t : keySet) {
//                T t2 = t;
//                Wg6.b(t2, "it");
//                if (((Boolean) Zm7.h(b, t2)).booleanValue()) {
//                    arrayList.add(t);
//                }
//            }
//            Object[] array = arrayList.toArray(new String[0]);
//            if (array != null) {
//                return (String[]) array;
//            }
//            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public final HashMap<String, Boolean> b() {
            PortfolioApp instance = PortfolioApp.get.instance();
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            linkedHashMap.put(InAppPermission.ACCESS_BACKGROUND_LOCATION, Boolean.valueOf(c(instance)));
            linkedHashMap.put(InAppPermission.ACCESS_FINE_LOCATION, Boolean.valueOf(d(instance)));
            linkedHashMap.put(InAppPermission.LOCATION_SERVICE, Boolean.valueOf(g()));
            linkedHashMap.put(InAppPermission.BLUETOOTH, Boolean.valueOf(e()));
            linkedHashMap.put(InAppPermission.READ_CONTACTS, Boolean.valueOf(l(instance)));
            linkedHashMap.put(InAppPermission.READ_PHONE_STATE, Boolean.valueOf(m(instance)));
            linkedHashMap.put(InAppPermission.READ_SMS, Boolean.valueOf(n(instance)));
            linkedHashMap.put(InAppPermission.NOTIFICATION_ACCESS, Boolean.valueOf(h()));
            linkedHashMap.put(InAppPermission.CAMERA, Boolean.valueOf(f(instance)));
            linkedHashMap.put(InAppPermission.WRITE_EXTERNAL_STORAGE, Boolean.valueOf(o(instance)));
            return linkedHashMap;
        }

        @DexIgnore
        public final boolean c(Context context) {
            Wg6.c(context, "context");
            return i(context, "android.permission.ACCESS_BACKGROUND_LOCATION");
        }

        @DexIgnore
        public final boolean d(Context context) {
            Wg6.c(context, "context");
            return i(context, "android.permission.ACCESS_FINE_LOCATION");
        }

        @DexIgnore
        public final boolean e() {
            BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
            return defaultAdapter != null && defaultAdapter.isEnabled();
        }

        @DexIgnore
        public final boolean f(Context context) {
            return i(context, "android.permission.CAMERA");
        }

        @DexIgnore
        public final boolean g() {
            LocationManager locationManager = (LocationManager) PortfolioApp.get.instance().getSystemService(PlaceFields.LOCATION);
            if (locationManager == null) {
                return false;
            }
            String bestProvider = locationManager.getBestProvider(new Criteria(), true);
            if (!(Bv6.a(bestProvider) || Wg6.a("passive", bestProvider) || (Vt7.j(LocationUtils.HUAWEI_MODEL, Build.MANUFACTURER, true) && Vt7.j(LocationUtils.HUAWEI_LOCAL_PROVIDER, bestProvider, true)))) {
                return true;
            }
            try {
                if (Settings.Secure.getInt(PortfolioApp.get.instance().getContentResolver(), "location_mode") != 0) {
                    return true;
                }
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            return false;
        }

        @DexIgnore
        public final boolean h() {
            PortfolioApp instance = PortfolioApp.get.instance();
            String string = Settings.Secure.getString(instance.getContentResolver(), "enabled_notification_listeners");
            String str = instance.getPackageName() + "/" + FossilNotificationListenerService.class.getCanonicalName();
            FLogger.INSTANCE.getLocal().d("PermissionUtils", "isNotificationListenerEnabled - notificationServicePath=" + str + ", enabledNotificationListeners=" + string);
            if (TextUtils.isEmpty(string)) {
                return false;
            }
            Wg6.b(string, "enabledNotificationListeners");
            return Wt7.v(string, str, false, 2, null);
        }

        @DexIgnore
        public final boolean i(Context context, String str) {
            return Build.VERSION.SDK_INT < 23 || W6.a(context, str) == 0;
        }

        @DexIgnore
        public final boolean j(Context context, String... strArr) {
            Wg6.c(context, "context");
            Wg6.c(strArr, "perms");
            return V78.a(context, (String[]) Arrays.copyOf(strArr, strArr.length));
        }

        @DexIgnore
        public final boolean k(Context context) {
            Wg6.c(context, "context");
            return i(context, "android.permission.READ_CALL_LOG");
        }

        @DexIgnore
        public final boolean l(Context context) {
            Wg6.c(context, "context");
            return i(context, "android.permission.READ_CONTACTS");
        }

        @DexIgnore
        public final boolean m(Context context) {
            return i(context, "android.permission.READ_PHONE_STATE");
        }

        @DexIgnore
        public final boolean n(Context context) {
            return i(context, "android.permission.READ_SMS");
        }

        @DexIgnore
        public final boolean o(Context context) {
            Wg6.c(context, "context");
            return i(context, "android.permission.WRITE_EXTERNAL_STORAGE");
        }

        @DexIgnore
        public final void p(Object obj, int i, String... strArr) {
            int[] iArr = new int[strArr.length];
            int length = strArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                iArr[i2] = 0;
            }
            V78.c(i, strArr, iArr, obj);
        }

        @DexIgnore
        public final boolean q(Activity activity, int i) {
            Wg6.c(activity, Constants.ACTIVITY);
            if (c(activity)) {
                return true;
            }
            Rk0.u(activity, new String[]{"android.permission.ACCESS_BACKGROUND_LOCATION"}, i);
            return false;
        }

        @DexIgnore
        public final boolean r(Activity activity, int i) {
            Wg6.c(activity, Constants.ACTIVITY);
            if (d(activity)) {
                return true;
            }
            Rk0.u(activity, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, i);
            return false;
        }

        @DexIgnore
        public final boolean s(Activity activity, int i) {
            Wg6.c(activity, Constants.ACTIVITY);
            if (o(activity)) {
                return true;
            }
            Rk0.u(activity, new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"}, i);
            return false;
        }

        @DexIgnore
        public final void t(Fragment fragment, int i, String... strArr) {
            Wg6.c(fragment, "fragment");
            Wg6.c(strArr, "perms");
            if (V78.a(fragment.requireContext(), (String[]) Arrays.copyOf(strArr, strArr.length))) {
                p(fragment, i, (String[]) Arrays.copyOf(strArr, strArr.length));
            } else {
                fragment.requestPermissions(strArr, i);
            }
        }
    }
}
