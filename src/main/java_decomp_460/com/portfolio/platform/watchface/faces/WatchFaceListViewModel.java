package com.portfolio.platform.watchface.faces;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.b77;
import com.fossil.bn5;
import com.fossil.bw7;
import com.fossil.co7;
import com.fossil.dr7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gi0;
import com.fossil.gu7;
import com.fossil.hq4;
import com.fossil.hq5;
import com.fossil.hs0;
import com.fossil.il7;
import com.fossil.iq4;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.j37;
import com.fossil.jo5;
import com.fossil.ko7;
import com.fossil.kq5;
import com.fossil.kz4;
import com.fossil.mo5;
import com.fossil.nk5;
import com.fossil.oo5;
import com.fossil.or0;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.ss0;
import com.fossil.tl7;
import com.fossil.uh5;
import com.fossil.um5;
import com.fossil.uo5;
import com.fossil.us0;
import com.fossil.vp7;
import com.fossil.wb7;
import com.fossil.xw7;
import com.fossil.ym5;
import com.fossil.yn7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceOrder;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import java.io.File;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceListViewModel extends hq4 {
    @DexIgnore
    public /* final */ LiveData<List<DianaWatchFaceUser>> h;
    @DexIgnore
    public /* final */ LiveData<Integer> i;
    @DexIgnore
    public /* final */ MutableLiveData<String> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ DianaWatchFaceRepository l;
    @DexIgnore
    public /* final */ FileRepository m;
    @DexIgnore
    public /* final */ uo5 n;
    @DexIgnore
    public /* final */ wb7 o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class WatchFaceDownloadResponse {
        @DexIgnore
        public String checksumFace;
        @DexIgnore
        public String downloadFaceUrl;
        @DexIgnore
        public String name;
        @DexIgnore
        public String previewFaceUrl;

        @DexIgnore
        public WatchFaceDownloadResponse(String str, String str2, String str3, String str4) {
            this.downloadFaceUrl = str;
            this.checksumFace = str2;
            this.name = str3;
            this.previewFaceUrl = str4;
        }

        @DexIgnore
        public static /* synthetic */ WatchFaceDownloadResponse copy$default(WatchFaceDownloadResponse watchFaceDownloadResponse, String str, String str2, String str3, String str4, int i, Object obj) {
            if ((i & 1) != 0) {
                str = watchFaceDownloadResponse.downloadFaceUrl;
            }
            if ((i & 2) != 0) {
                str2 = watchFaceDownloadResponse.checksumFace;
            }
            if ((i & 4) != 0) {
                str3 = watchFaceDownloadResponse.name;
            }
            if ((i & 8) != 0) {
                str4 = watchFaceDownloadResponse.previewFaceUrl;
            }
            return watchFaceDownloadResponse.copy(str, str2, str3, str4);
        }

        @DexIgnore
        public final String component1() {
            return this.downloadFaceUrl;
        }

        @DexIgnore
        public final String component2() {
            return this.checksumFace;
        }

        @DexIgnore
        public final String component3() {
            return this.name;
        }

        @DexIgnore
        public final String component4() {
            return this.previewFaceUrl;
        }

        @DexIgnore
        public final WatchFaceDownloadResponse copy(String str, String str2, String str3, String str4) {
            return new WatchFaceDownloadResponse(str, str2, str3, str4);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof WatchFaceDownloadResponse) {
                    WatchFaceDownloadResponse watchFaceDownloadResponse = (WatchFaceDownloadResponse) obj;
                    if (!pq7.a(this.downloadFaceUrl, watchFaceDownloadResponse.downloadFaceUrl) || !pq7.a(this.checksumFace, watchFaceDownloadResponse.checksumFace) || !pq7.a(this.name, watchFaceDownloadResponse.name) || !pq7.a(this.previewFaceUrl, watchFaceDownloadResponse.previewFaceUrl)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final String getChecksumFace() {
            return this.checksumFace;
        }

        @DexIgnore
        public final String getDownloadFaceUrl() {
            return this.downloadFaceUrl;
        }

        @DexIgnore
        public final String getName() {
            return this.name;
        }

        @DexIgnore
        public final String getPreviewFaceUrl() {
            return this.previewFaceUrl;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.downloadFaceUrl;
            int hashCode = str != null ? str.hashCode() : 0;
            String str2 = this.checksumFace;
            int hashCode2 = str2 != null ? str2.hashCode() : 0;
            String str3 = this.name;
            int hashCode3 = str3 != null ? str3.hashCode() : 0;
            String str4 = this.previewFaceUrl;
            if (str4 != null) {
                i = str4.hashCode();
            }
            return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
        }

        @DexIgnore
        public final void setChecksumFace(String str) {
            this.checksumFace = str;
        }

        @DexIgnore
        public final void setDownloadFaceUrl(String str) {
            this.downloadFaceUrl = str;
        }

        @DexIgnore
        public final void setName(String str) {
            this.name = str;
        }

        @DexIgnore
        public final void setPreviewFaceUrl(String str) {
            this.previewFaceUrl = str;
        }

        @DexIgnore
        public String toString() {
            return "WatchFaceDownloadResponse(downloadFaceUrl=" + this.downloadFaceUrl + ", checksumFace=" + this.checksumFace + ", name=" + this.name + ", previewFaceUrl=" + this.previewFaceUrl + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<I, O> implements gi0<List<? extends DianaWatchFaceUser>, Integer> {
        @DexIgnore
        @Override // com.fossil.gi0
        public final Integer apply(List<? extends DianaWatchFaceUser> list) {
            return Integer.valueOf(list.size());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$_watchFaceListLiveData$1", f = "WatchFaceListViewModel.kt", l = {59, 59}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<hs0<List<? extends DianaWatchFaceUser>>, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public hs0 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WatchFaceListViewModel watchFaceListViewModel, qn7 qn7) {
            super(2, qn7);
            this.this$0 = watchFaceListViewModel;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (hs0) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(hs0<List<? extends DianaWatchFaceUser>> hs0, qn7<? super tl7> qn7) {
            return ((b) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 2
                r2 = 1
                java.lang.Object r4 = com.fossil.yn7.d()
                int r0 = r6.label
                if (r0 == 0) goto L_0x003c
                if (r0 == r2) goto L_0x0020
                if (r0 != r5) goto L_0x0018
                java.lang.Object r0 = r6.L$0
                com.fossil.hs0 r0 = (com.fossil.hs0) r0
                com.fossil.el7.b(r7)
            L_0x0015:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r6.L$1
                com.fossil.hs0 r0 = (com.fossil.hs0) r0
                java.lang.Object r1 = r6.L$0
                com.fossil.hs0 r1 = (com.fossil.hs0) r1
                com.fossil.el7.b(r7)
                r3 = r0
                r2 = r7
            L_0x002d:
                r0 = r2
                androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                r6.L$0 = r1
                r6.label = r5
                java.lang.Object r0 = r3.a(r0, r6)
                if (r0 != r4) goto L_0x0015
                r0 = r4
                goto L_0x0017
            L_0x003c:
                com.fossil.el7.b(r7)
                com.fossil.hs0 r0 = r6.p$
                com.portfolio.platform.watchface.faces.WatchFaceListViewModel r1 = r6.this$0
                com.portfolio.platform.data.source.DianaWatchFaceRepository r1 = com.portfolio.platform.watchface.faces.WatchFaceListViewModel.r(r1)
                r6.L$0 = r0
                r6.L$1 = r0
                r6.label = r2
                java.lang.Object r2 = r1.getAllDianaWatchFaceUserLiveData(r6)
                if (r2 != r4) goto L_0x0055
                r0 = r4
                goto L_0x0017
            L_0x0055:
                r1 = r0
                r3 = r0
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.faces.WatchFaceListViewModel.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$createWatchFaceAsPresetById$1", f = "WatchFaceListViewModel.kt", l = {88, 90, 97, 112, 123, 134}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $watchFaceId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$createWatchFaceAsPresetById$1$1", f = "WatchFaceListViewModel.kt", l = {124}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ dr7 $presetId;
            @DexIgnore
            public /* final */ /* synthetic */ kz4 $response;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, kz4 kz4, dr7 dr7, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$response = kz4;
                this.$presetId = dr7;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$response, this.$presetId, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    FileType fileType = FileType.WATCH_FACE;
                    String b = ((mo5) this.$response.c()).b();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object downloadAndSaveWithFileName = this.this$0.this$0.m.downloadAndSaveWithFileName(((mo5) this.$response.c()).d(), this.$presetId.element, fileType, b, this);
                    return downloadAndSaveWithFileName == d ? d : downloadAndSaveWithFileName;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$createWatchFaceAsPresetById$1$2", f = "WatchFaceListViewModel.kt", l = {135}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ mo5 $activePreset;
            @DexIgnore
            public /* final */ /* synthetic */ mo5 $preset;
            @DexIgnore
            public /* final */ /* synthetic */ byte[] $watchFaceByteArrayData;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(c cVar, mo5 mo5, mo5 mo52, byte[] bArr, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$preset = mo5;
                this.$activePreset = mo52;
                this.$watchFaceByteArrayData = bArr;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, this.$preset, this.$activePreset, this.$watchFaceByteArrayData, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object v;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    bn5 bn5 = bn5.j;
                    this.L$0 = iv7;
                    this.label = 1;
                    v = bn5.v(this);
                    if (v == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    v = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ((DianaCustomizeDatabase) v).getDianaPresetDao().l(this.$preset);
                b77.f401a.d(this.$activePreset.e());
                FileRepository fileRepository = this.this$0.this$0.m;
                String e = this.$preset.e();
                FileType fileType = FileType.WATCH_FACE;
                byte[] bArr = this.$watchFaceByteArrayData;
                pq7.b(bArr, "watchFaceByteArrayData");
                String d2 = j37.d(bArr);
                byte[] bArr2 = this.$watchFaceByteArrayData;
                pq7.b(bArr2, "watchFaceByteArrayData");
                FileRepository.writeFileWithDir$default(fileRepository, e, fileType, d2, bArr2, null, 16, null);
                return tl7.f3441a;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$c$c")
        @eo7(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$createWatchFaceAsPresetById$1$activePreset$1", f = "WatchFaceListViewModel.kt", l = {91}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.watchface.faces.WatchFaceListViewModel$c$c  reason: collision with other inner class name */
        public static final class C0362c extends ko7 implements vp7<iv7, qn7<? super mo5>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $activeSerial;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0362c(c cVar, String str, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$activeSerial = str;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0362c cVar = new C0362c(this.this$0, this.$activeSerial, qn7);
                cVar.p$ = (iv7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super mo5> qn7) {
                return ((C0362c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    uo5 uo5 = this.this$0.this$0.n;
                    String str = this.$activeSerial;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object b = uo5.b(str, this);
                    return b == d ? d : b;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$createWatchFaceAsPresetById$1$response$1", f = "WatchFaceListViewModel.kt", l = {117}, m = "invokeSuspend")
        public static final class d extends ko7 implements vp7<iv7, qn7<? super kz4<mo5>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ mo5 $activePreset;
            @DexIgnore
            public /* final */ /* synthetic */ String $activeSerial;
            @DexIgnore
            public /* final */ /* synthetic */ String $todayDate;
            @DexIgnore
            public /* final */ /* synthetic */ DianaWatchFaceUser $watchFace;
            @DexIgnore
            public /* final */ /* synthetic */ String $watchFaceThemeData;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(c cVar, mo5 mo5, DianaWatchFaceUser dianaWatchFaceUser, String str, String str2, String str3, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$activePreset = mo5;
                this.$watchFace = dianaWatchFaceUser;
                this.$watchFaceThemeData = str;
                this.$activeSerial = str2;
                this.$todayDate = str3;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                d dVar = new d(this.this$0, this.$activePreset, this.$watchFace, this.$watchFaceThemeData, this.$activeSerial, this.$todayDate, qn7);
                dVar.p$ = (iv7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super kz4<mo5>> qn7) {
                return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                String str;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    String e = ym5.e(24);
                    pq7.b(e, "StringHelper.randomUUID(24)");
                    List<oo5> a2 = this.$activePreset.a();
                    String name = this.$watchFace.getName();
                    String previewURL = this.$watchFace.getPreviewURL();
                    String str2 = this.$watchFaceThemeData;
                    pq7.b(str2, "watchFaceThemeData");
                    String str3 = this.$activeSerial;
                    DianaWatchFaceOrder order = this.$watchFace.getOrder();
                    if (order == null || (str = order.getWatchFaceId()) == null) {
                        str = "";
                    }
                    String str4 = this.$todayDate;
                    jo5 jo5 = new jo5(e, a2, name, previewURL, str2, str3, false, str, str4, str4);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchFaceListViewModel", "createWatchFaceAsPresetById presetDraft name " + this.$watchFace.getName() + " orderId " + this.$watchFace.getOrder());
                    uo5 uo5 = this.this$0.this$0.n;
                    this.L$0 = iv7;
                    this.L$1 = jo5;
                    this.label = 1;
                    Object q = uo5.q(jo5, this);
                    return q == d ? d : q;
                } else if (i == 1) {
                    jo5 jo52 = (jo5) this.L$1;
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$createWatchFaceAsPresetById$1$watchFaceDataFile$1", f = "WatchFaceListViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class e extends ko7 implements vp7<iv7, qn7<? super File>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ DianaWatchFaceUser $watchFace;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public e(c cVar, DianaWatchFaceUser dianaWatchFaceUser, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$watchFace = dianaWatchFaceUser;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                e eVar = new e(this.this$0, this.$watchFace, qn7);
                eVar.p$ = (iv7) obj;
                return eVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super File> qn7) {
                return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.m.getFileByFileName(this.$watchFace.getId());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(WatchFaceListViewModel watchFaceListViewModel, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = watchFaceListViewModel;
            this.$watchFaceId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$watchFaceId, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x00cf  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0169  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x01a9  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0252  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x02b8  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x02c1  */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x0348  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r25) {
            /*
            // Method dump skipped, instructions count: 1104
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.faces.WatchFaceListViewModel.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(qn7 qn7, WatchFaceListViewModel watchFaceListViewModel, String str) {
            super(2, qn7);
            this.this$0 = watchFaceListViewModel;
            this.$id$inlined = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(qn7, this.this$0, this.$id$inlined);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                hq4.d(this.this$0, true, false, null, 6, null);
                DianaWatchFaceRepository dianaWatchFaceRepository = this.this$0.l;
                String str = this.$id$inlined;
                this.L$0 = iv7;
                this.label = 1;
                if (dianaWatchFaceRepository.deleteDianaWatchFaceUserById(str, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            hq4.d(this.this$0, false, true, null, 5, null);
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$downloadShareWatchFace$1", f = "WatchFaceListViewModel.kt", l = {212}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $token;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$downloadShareWatchFace$1$1", f = "WatchFaceListViewModel.kt", l = {222, 229, 238}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ WatchFaceDownloadResponse $wfResponse;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, WatchFaceDownloadResponse watchFaceDownloadResponse, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
                this.$wfResponse = watchFaceDownloadResponse;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$wfResponse, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:17:0x008a  */
            /* JADX WARNING: Removed duplicated region for block: B:29:0x0129  */
            /* JADX WARNING: Removed duplicated region for block: B:53:0x01ce  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r15) {
                /*
                // Method dump skipped, instructions count: 517
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.faces.WatchFaceListViewModel.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(WatchFaceListViewModel watchFaceListViewModel, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = watchFaceListViewModel;
            this.$id = str;
            this.$token = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, this.$id, this.$token, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object j;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                hq4.d(this.this$0, true, false, null, 6, null);
                uo5 uo5 = this.this$0.n;
                String str = this.$id;
                String str2 = this.$token;
                this.L$0 = iv7;
                this.label = 1;
                j = uo5.j(str, str2, this);
                if (j == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                j = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            iq5 iq5 = (iq5) j;
            if (iq5 instanceof kq5) {
                WatchFaceDownloadResponse watchFaceDownloadResponse = (WatchFaceDownloadResponse) new Gson().k(String.valueOf(((kq5) iq5).a()), WatchFaceDownloadResponse.class);
                if (watchFaceDownloadResponse != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchFaceListViewModel", "downloadShareWatchFace wfResponse " + watchFaceDownloadResponse);
                    if (!(watchFaceDownloadResponse.getDownloadFaceUrl() == null || watchFaceDownloadResponse.getChecksumFace() == null || watchFaceDownloadResponse.getName() == null)) {
                        xw7 unused = gu7.d(us0.a(this.this$0), bw7.b(), null, new a(this, watchFaceDownloadResponse, null), 2, null);
                    }
                } else {
                    hq4.d(this.this$0, false, true, null, 5, null);
                    WatchFaceListViewModel watchFaceListViewModel = this.this$0;
                    String c = um5.c(PortfolioApp.h0.c(), 2131886235);
                    pq7.b(c, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    hq4.b(watchFaceListViewModel, 0, c, 1, null);
                }
            } else if (iq5 instanceof hq5) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("downloadShareWatchFace get link failed ");
                hq5 hq5 = (hq5) iq5;
                sb.append(hq5.a());
                local2.d("WatchFaceListViewModel", sb.toString());
                if (hq5.a() == 404 || hq5.a() == 403) {
                    hq4.d(this.this$0, false, true, null, 5, null);
                    WatchFaceListViewModel watchFaceListViewModel2 = this.this$0;
                    String c2 = um5.c(PortfolioApp.h0.c(), 2131886580);
                    pq7.b(c2, "LanguageHelper.getString\u2026yThisWatchFaceWasRemoved)");
                    hq4.b(watchFaceListViewModel2, 0, c2, 1, null);
                } else {
                    hq4.d(this.this$0, false, true, null, 5, null);
                    WatchFaceListViewModel watchFaceListViewModel3 = this.this$0;
                    String c3 = um5.c(PortfolioApp.h0.c(), 2131886235);
                    pq7.b(c3, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    hq4.b(watchFaceListViewModel3, 0, c3, 1, null);
                }
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel", f = "WatchFaceListViewModel.kt", l = {72}, m = "isReachedMaxPreset")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(WatchFaceListViewModel watchFaceListViewModel, qn7 qn7) {
            super(qn7);
            this.this$0 = watchFaceListViewModel;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.D(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$isReachedMaxPreset$totalPreset$1", f = "WatchFaceListViewModel.kt", l = {72}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(WatchFaceListViewModel watchFaceListViewModel, qn7 qn7) {
            super(2, qn7);
            this.this$0 = watchFaceListViewModel;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, qn7);
            gVar.p$ = (iv7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
            return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                uo5 uo5 = this.this$0.n;
                String J = PortfolioApp.h0.c().J();
                this.L$0 = iv7;
                this.label = 1;
                Object i2 = uo5.i(J, this);
                return i2 == d ? d : i2;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $it;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements iq4.e<wb7.c, wb7.a> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ h f4770a;

            @DexIgnore
            public a(h hVar) {
                this.f4770a = hVar;
            }

            @DexIgnore
            /* renamed from: b */
            public void a(wb7.a aVar) {
                pq7.c(aVar, "errorValue");
                this.f4770a.this$0.o.s();
                hq4.d(this.f4770a.this$0, false, true, null, 5, null);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchFaceListViewModel", "preview watch face failed! Error code " + aVar.b());
                int b = aVar.b();
                if (b == 1101 || b == 1112 || b == 1113) {
                    List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(aVar.a());
                    pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                    WatchFaceListViewModel watchFaceListViewModel = this.f4770a.this$0;
                    Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                    if (array != null) {
                        uh5[] uh5Arr = (uh5[]) array;
                        watchFaceListViewModel.e((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                        return;
                    }
                    throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                }
                this.f4770a.this$0.f();
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(wb7.c cVar) {
                pq7.c(cVar, "responseValue");
                FLogger.INSTANCE.getLocal().d("WatchFaceListViewModel", "preview watch face success");
                this.f4770a.this$0.o.s();
                hq4.d(this.f4770a.this$0, false, true, null, 5, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(String str, qn7 qn7, WatchFaceListViewModel watchFaceListViewModel) {
            super(2, qn7);
            this.$it = str;
            this.this$0 = watchFaceListViewModel;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.$it, qn7, this.this$0);
            hVar.p$ = (iv7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                hq4.d(this.this$0, true, false, null, 6, null);
                this.this$0.o.p();
                this.this$0.o.e(new wb7.b(this.$it, null, null, 6, null), new a(this));
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public WatchFaceListViewModel(DianaWatchFaceRepository dianaWatchFaceRepository, FileRepository fileRepository, uo5 uo5, wb7 wb7) {
        pq7.c(dianaWatchFaceRepository, "mDianaWatchFaceRepository");
        pq7.c(fileRepository, "mFileRepository");
        pq7.c(uo5, "mDianaPresetRepository");
        pq7.c(wb7, "mPreviewWatchFaceUseCase");
        this.l = dianaWatchFaceRepository;
        this.m = fileRepository;
        this.n = uo5;
        this.o = wb7;
        LiveData<List<DianaWatchFaceUser>> c2 = or0.c(bw7.b(), 0, new b(this, null), 2, null);
        this.h = c2;
        LiveData<Integer> b2 = ss0.b(c2, new a());
        pq7.b(b2, "Transformations.map(this) { transform(it) }");
        this.i = b2;
    }

    @DexIgnore
    public final MutableLiveData<String> A() {
        return this.j;
    }

    @DexIgnore
    public final MutableLiveData<String> B() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<Integer> C() {
        return this.i;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object D(com.fossil.qn7<? super java.lang.Boolean> r7) {
        /*
            r6 = this;
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 1
            boolean r0 = r7 instanceof com.portfolio.platform.watchface.faces.WatchFaceListViewModel.f
            if (r0 == 0) goto L_0x0054
            r0 = r7
            com.portfolio.platform.watchface.faces.WatchFaceListViewModel$f r0 = (com.portfolio.platform.watchface.faces.WatchFaceListViewModel.f) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0054
            int r1 = r1 + r4
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0062
            if (r4 != r2) goto L_0x005a
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.watchface.faces.WatchFaceListViewModel r0 = (com.portfolio.platform.watchface.faces.WatchFaceListViewModel) r0
            com.fossil.el7.b(r1)
            r6 = r0
        L_0x0027:
            r0 = r1
            java.lang.Number r0 = (java.lang.Number) r0
            int r0 = r0.intValue()
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "isReachedMaxPreset total "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r4 = "WatchFaceListViewModel"
            java.lang.String r3 = r3.toString()
            r1.d(r4, r3)
            r1 = 20
            if (r0 >= r1) goto L_0x007b
            r0 = 0
        L_0x004f:
            java.lang.Boolean r0 = com.fossil.ao7.a(r0)
        L_0x0053:
            return r0
        L_0x0054:
            com.portfolio.platform.watchface.faces.WatchFaceListViewModel$f r0 = new com.portfolio.platform.watchface.faces.WatchFaceListViewModel$f
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x005a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0062:
            com.fossil.el7.b(r1)
            com.fossil.dv7 r1 = com.fossil.bw7.b()
            com.portfolio.platform.watchface.faces.WatchFaceListViewModel$g r4 = new com.portfolio.platform.watchface.faces.WatchFaceListViewModel$g
            r5 = 0
            r4.<init>(r6, r5)
            r0.L$0 = r6
            r0.label = r2
            java.lang.Object r1 = com.fossil.eu7.g(r1, r4, r0)
            if (r1 != r3) goto L_0x0027
            r0 = r3
            goto L_0x0053
        L_0x007b:
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            r1 = 2131886573(0x7f1201ed, float:1.9407729E38)
            java.lang.String r0 = com.fossil.um5.c(r0, r1)
            java.lang.String r1 = "LanguageHelper.getString\u2026t__MaximumPresetsCreated)"
            com.fossil.pq7.b(r0, r1)
            r1 = -1
            r6.a(r1, r0)
            r0 = r2
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.faces.WatchFaceListViewModel.D(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void E(String str) {
        if (str == null || gu7.d(us0.a(this), null, null, new h(str, null, this), 3, null) == null) {
            a(-1, "Can not find this watch face");
            tl7 tl7 = tl7.f3441a;
        }
    }

    @DexIgnore
    public final void x(String str) {
        xw7 unused = gu7.d(us0.a(this), null, null, new c(this, str, null), 3, null);
    }

    @DexIgnore
    public final void y(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceListViewModel", "deleteWatchFaceById id " + str);
        if (str != null) {
            xw7 unused = gu7.d(us0.a(this), null, null, new d(null, this, str), 3, null);
        }
    }

    @DexIgnore
    public final void z(String str, String str2) {
        pq7.c(str, "id");
        pq7.c(str2, "token");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceListViewModel", "downloadShareWatchFace - id " + str + " token " + str2);
        if (!(str.length() == 0)) {
            if (!(str2.length() == 0)) {
                if (nk5.o.x(PortfolioApp.h0.c().J())) {
                    xw7 unused = gu7.d(us0.a(this), null, null, new e(this, str, str2, null), 3, null);
                    return;
                }
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886579);
                pq7.b(c2, "LanguageHelper.getString\u2026keSureYourHybridHrDevice)");
                hq4.b(this, 0, c2, 1, null);
            }
        }
    }
}
