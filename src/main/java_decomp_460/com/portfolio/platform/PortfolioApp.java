package com.portfolio.platform;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.webkit.MimeTypeMap;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.facebook.places.model.PlaceFields;
import com.facebook.stetho.Stetho;
import com.fossil.ao7;
import com.fossil.aq5;
import com.fossil.bk5;
import com.fossil.bo5;
import com.fossil.br5;
import com.fossil.bw7;
import com.fossil.bx1;
import com.fossil.cc7;
import com.fossil.cj5;
import com.fossil.ck5;
import com.fossil.cl5;
import com.fossil.cl7;
import com.fossil.co7;
import com.fossil.ct0;
import com.fossil.dk5;
import com.fossil.dl7;
import com.fossil.dq5;
import com.fossil.el7;
import com.fossil.em7;
import com.fossil.eo5;
import com.fossil.eo7;
import com.fossil.eu7;
import com.fossil.fs5;
import com.fossil.g11;
import com.fossil.go7;
import com.fossil.gq5;
import com.fossil.gu7;
import com.fossil.hm7;
import com.fossil.hr7;
import com.fossil.i47;
import com.fossil.ic7;
import com.fossil.il7;
import com.fossil.im7;
import com.fossil.iq4;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.jq5;
import com.fossil.jt7;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.kq4;
import com.fossil.kq5;
import com.fossil.kq7;
import com.fossil.ku7;
import com.fossil.l37;
import com.fossil.lk5;
import com.fossil.ll5;
import com.fossil.lu7;
import com.fossil.lw1;
import com.fossil.mj5;
import com.fossil.mq4;
import com.fossil.nc7;
import com.fossil.nk5;
import com.fossil.no4;
import com.fossil.ns0;
import com.fossil.o01;
import com.fossil.ob7;
import com.fossil.on5;
import com.fossil.ph5;
import com.fossil.pm0;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.q27;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.qq7;
import com.fossil.qt0;
import com.fossil.ro4;
import com.fossil.rp7;
import com.fossil.rq4;
import com.fossil.rt0;
import com.fossil.ry1;
import com.fossil.sk5;
import com.fossil.sl5;
import com.fossil.t27;
import com.fossil.tl7;
import com.fossil.tt5;
import com.fossil.uc7;
import com.fossil.ul5;
import com.fossil.um5;
import com.fossil.uo4;
import com.fossil.up5;
import com.fossil.uq4;
import com.fossil.ux7;
import com.fossil.v74;
import com.fossil.vl5;
import com.fossil.vm5;
import com.fossil.vp7;
import com.fossil.vt7;
import com.fossil.wt7;
import com.fossil.xn7;
import com.fossil.xw7;
import com.fossil.yn7;
import com.fossil.yr4;
import com.fossil.yx1;
import com.fossil.zu5;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMapping;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;
import com.misfit.frameworks.buttonservice.model.watchparams.Version;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.buttonservice.utils.ThemeExtentionKt;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.cloudimage.ResolutionHelper;
import com.portfolio.platform.data.model.Installation;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppDataRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.uirenew.splash.SplashScreenActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.workers.PushPendingDataWorker;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.FlutterEngineCache;
import io.flutter.embedding.engine.dart.DartExecutor;
import io.flutter.view.FlutterMain;
import java.io.File;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import net.sqlcipher.database.SQLiteDatabase;
import retrofit.Endpoints;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioApp extends rt0 implements Application.ActivityLifecycleCallbacks {
    @DexIgnore
    public static /* final */ String b0;
    @DexIgnore
    public static boolean c0;
    @DexIgnore
    public static PortfolioApp d0;
    @DexIgnore
    public static boolean e0;
    @DexIgnore
    public static nc7 f0;
    @DexIgnore
    public static IButtonConnectivity g0;
    @DexIgnore
    public static /* final */ a h0; // = new a(null);
    @DexIgnore
    public bo5 A;
    @DexIgnore
    public UserRepository B;
    @DexIgnore
    public WatchFaceRepository C;
    @DexIgnore
    public QuickResponseRepository D;
    @DexIgnore
    public WorkoutSettingRepository E;
    @DexIgnore
    public WatchAppDataRepository F;
    @DexIgnore
    public fs5 G;
    @DexIgnore
    public MutableLiveData<String> H; // = new MutableLiveData<>();
    @DexIgnore
    public boolean I;
    @DexIgnore
    public boolean J; // = true;
    @DexIgnore
    public /* final */ Handler K; // = new Handler();
    @DexIgnore
    public Runnable L;
    @DexIgnore
    public ro4 M;
    @DexIgnore
    public Thread.UncaughtExceptionHandler N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public ServerError P;
    @DexIgnore
    public ul5 Q;
    @DexIgnore
    public vl5 R;
    @DexIgnore
    public NetworkChangedReceiver S;
    @DexIgnore
    public /* final */ gq5 T; // = new gq5();
    @DexIgnore
    public aq5 U;
    @DexIgnore
    public dq5 V;
    @DexIgnore
    public ThemeRepository W;
    @DexIgnore
    public q27 X;
    @DexIgnore
    public t27 Y;
    @DexIgnore
    public yr4 Z;
    @DexIgnore
    public /* final */ iv7 a0; // = jv7.a(ux7.b(null, 1, null).plus(bw7.b()));
    @DexIgnore
    public vm5 b;
    @DexIgnore
    public up5 c;
    @DexIgnore
    public on5 d;
    @DexIgnore
    public SummariesRepository e;
    @DexIgnore
    public SleepSummariesRepository f;
    @DexIgnore
    public bk5 g;
    @DexIgnore
    public GuestApiService h;
    @DexIgnore
    public no4 i;
    @DexIgnore
    public ApiServiceV2 j;
    @DexIgnore
    public l37 k;
    @DexIgnore
    public uq4 l;
    @DexIgnore
    public zu5 m;
    @DexIgnore
    public ck5 s;
    @DexIgnore
    public ApplicationEventListener t;
    @DexIgnore
    public DeviceRepository u;
    @DexIgnore
    public sk5 v;
    @DexIgnore
    public br5 w;
    @DexIgnore
    public DianaPresetRepository x;
    @DexIgnore
    public ic7 y;
    @DexIgnore
    public WatchLocalizationRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.PortfolioApp$a$a")
        @eo7(c = "com.portfolio.platform.PortfolioApp$Companion", f = "PortfolioApp.kt", l = {2542, 2544}, m = "updateButtonService")
        /* renamed from: com.portfolio.platform.PortfolioApp$a$a  reason: collision with other inner class name */
        public static final class C0346a extends co7 {
            @DexIgnore
            public char C$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public int label;
            @DexIgnore
            public /* synthetic */ Object result;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0346a(a aVar, qn7 qn7) {
                super(qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                this.result = obj;
                this.label |= RecyclerView.UNDEFINED_DURATION;
                return this.this$0.m(null, this);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final nc7 a() {
            return PortfolioApp.f0;
        }

        @DexIgnore
        public final IButtonConnectivity b() {
            return PortfolioApp.g0;
        }

        @DexIgnore
        public final PortfolioApp c() {
            PortfolioApp portfolioApp = PortfolioApp.d0;
            if (portfolioApp != null) {
                return portfolioApp;
            }
            pq7.n("instance");
            throw null;
        }

        @DexIgnore
        public final String d() {
            return PortfolioApp.b0;
        }

        @DexIgnore
        public final boolean e() {
            return PortfolioApp.e0;
        }

        @DexIgnore
        public final boolean f() {
            return PortfolioApp.c0;
        }

        @DexIgnore
        public final void g(Object obj) {
            pq7.c(obj, Constants.EVENT);
            nc7 a2 = a();
            if (a2 != null) {
                a2.i(obj);
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        public final void h(Object obj) {
            pq7.c(obj, "o");
            try {
                nc7 a2 = a();
                if (a2 != null) {
                    a2.j(obj);
                } else {
                    pq7.i();
                    throw null;
                }
            } catch (Exception e) {
            }
        }

        @DexIgnore
        public final void i(IButtonConnectivity iButtonConnectivity) {
            PortfolioApp.g0 = iButtonConnectivity;
        }

        @DexIgnore
        public final void j(boolean z) {
            PortfolioApp.l(z);
        }

        @DexIgnore
        public final void k(MFDeviceService.b bVar) {
            PortfolioApp.m(bVar);
        }

        @DexIgnore
        public final void l(Object obj) {
            pq7.c(obj, "o");
            try {
                nc7 a2 = a();
                if (a2 != null) {
                    a2.l(obj);
                } else {
                    pq7.i();
                    throw null;
                }
            } catch (Exception e) {
            }
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00cb  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00cf  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object m(com.misfit.frameworks.buttonservice.IButtonConnectivity r18, com.fossil.qn7<? super com.fossil.tl7> r19) {
            /*
            // Method dump skipped, instructions count: 424
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.a.m(com.misfit.frameworks.buttonservice.IButtonConnectivity, com.fossil.qn7):java.lang.Object");
        }

        @DexIgnore
        public final void n(MFDeviceService.b bVar) {
            pq7.c(bVar, Constants.SERVICE);
            k(bVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {1047}, m = "switchActiveDevice")
    public static final class a0 extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a0(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.X1(null, this);
        }
    }

    @DexIgnore
    public enum b {
        CREATE,
        START,
        RESUME,
        PAUSE,
        STOP,
        DESTROY
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b0 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ku7 $continuation;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isMigration$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ ob7 $wfThemeData$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends qq7 implements rp7<lw1, tl7> {
            @DexIgnore
            public /* final */ /* synthetic */ b0 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b0 b0Var) {
                super(1);
                this.this$0 = b0Var;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.rp7
            public /* bridge */ /* synthetic */ tl7 invoke(lw1 lw1) {
                invoke(lw1);
                return tl7.f3441a;
            }

            @DexIgnore
            public final void invoke(lw1 lw1) {
                pq7.c(lw1, "it");
                ku7 ku7 = this.this$0.$continuation;
                dl7.a aVar = dl7.Companion;
                ku7.resumeWith(dl7.m1constructorimpl(new cl7(lw1.getData(), 0)));
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends qq7 implements rp7<yx1, tl7> {
            @DexIgnore
            public /* final */ /* synthetic */ b0 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(b0 b0Var) {
                super(1);
                this.this$0 = b0Var;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.rp7
            public /* bridge */ /* synthetic */ tl7 invoke(yx1 yx1) {
                invoke(yx1);
                return tl7.f3441a;
            }

            @DexIgnore
            public final void invoke(yx1 yx1) {
                pq7.c(yx1, "error");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = PortfolioApp.h0.d();
                local.e(d, "themeDataToBinaryData - onError: " + yx1 + " - " + yx1.getErrorCode());
                if (yx1.getErrorCode() instanceof bx1) {
                    int i = yx1.getErrorCode() == bx1.NETWORK_UNAVAILABLE ? 601 : 500;
                    ku7 ku7 = this.this$0.$continuation;
                    dl7.a aVar = dl7.Companion;
                    ku7.resumeWith(dl7.m1constructorimpl(new cl7(null, Integer.valueOf(i))));
                    return;
                }
                ku7 ku72 = this.this$0.$continuation;
                dl7.a aVar2 = dl7.Companion;
                ku72.resumeWith(dl7.m1constructorimpl(new cl7(null, -1)));
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c extends qq7 implements rp7<yx1, tl7> {
            @DexIgnore
            public /* final */ /* synthetic */ b0 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(b0 b0Var) {
                super(1);
                this.this$0 = b0Var;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.rp7
            public /* bridge */ /* synthetic */ tl7 invoke(yx1 yx1) {
                invoke(yx1);
                return tl7.f3441a;
            }

            @DexIgnore
            public final void invoke(yx1 yx1) {
                pq7.c(yx1, "it");
                ku7 ku7 = this.this$0.$continuation;
                dl7.a aVar = dl7.Companion;
                ku7.resumeWith(dl7.m1constructorimpl(new cl7(null, -1)));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b0(ku7 ku7, qn7 qn7, PortfolioApp portfolioApp, boolean z, ob7 ob7) {
            super(2, qn7);
            this.$continuation = ku7;
            this.this$0 = portfolioApp;
            this.$isMigration$inlined = z;
            this.$wfThemeData$inlined = ob7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b0 b0Var = new b0(this.$continuation, qn7, this.this$0, this.$isMigration$inlined, this.$wfThemeData$inlined);
            b0Var.p$ = (iv7) obj;
            return b0Var;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b0) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            IButtonConnectivity b2;
            Version uiPackageOsVersion;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                PortfolioApp portfolioApp = this.this$0;
                this.L$0 = iv7;
                this.label = 1;
                if (portfolioApp.s0(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ry1 ry1 = null;
            if (!(this.$isMigration$inlined || (b2 = PortfolioApp.h0.b()) == null || (uiPackageOsVersion = b2.getUiPackageOsVersion(this.this$0.J())) == null)) {
                ry1 = new ry1(uiPackageOsVersion.getMajor(), uiPackageOsVersion.getMinor());
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d2 = PortfolioApp.h0.d();
            local.d(d2, "themeDataToBinaryData - isMigration: " + this.$isMigration$inlined + " - sdkVersion: " + ry1);
            ThemeExtentionKt.toThemeEditor(cc7.h(this.$wfThemeData$inlined)).c(ry1).m(new a(this)).l(new b(this)).k(new c(this));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2180}, m = "apply")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.q(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp$updateActiveDeviceInfoLog$1", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class c0 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        public c0(qn7 qn7) {
            super(2, qn7);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c0 c0Var = new c0(qn7);
            c0Var.p$ = (iv7) obj;
            return c0Var;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c0) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                ActiveDeviceInfo d = dk5.g.c().d();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d2 = PortfolioApp.h0.d();
                local.d(d2, ".updateActiveDeviceInfoLog(), " + new Gson().t(d));
                FLogger.INSTANCE.getRemote().updateActiveDeviceInfo(d);
                try {
                    IButtonConnectivity b = PortfolioApp.h0.b();
                    if (b != null) {
                        b.updateActiveDeviceInfoLog(d);
                    }
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String d3 = PortfolioApp.h0.d();
                    local2.e(d3, ".updateActiveDeviceInfoToButtonService(), error=" + e);
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2164}, m = "applyByThemeFile")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.r(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2490}, m = "updateAppLogInfo")
    public static final class d0 extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d0(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g2(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp$configureWorkManager$2", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(PortfolioApp portfolioApp, qn7 qn7) {
            super(2, qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                o01.a aVar = new o01.a();
                aVar.b(this.this$0.a0());
                o01 a2 = aVar.a();
                pq7.b(a2, "Configuration.Builder()\n\u2026\n                .build()");
                g11.f(this.this$0, a2);
                PushPendingDataWorker.H.a();
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {772}, m = "updateCrashlyticsUserInformation")
    public static final class e0 extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e0(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h2(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp$deleteInstanceId$2", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        public f(qn7 qn7) {
            super(2, qn7);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                try {
                    FirebaseInstanceId.m().g();
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String d = PortfolioApp.h0.d();
                    local.d(d, "deleteInstanceId ex-" + e);
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {1381, FailureCode.FAILED_TO_SET_STOP_WATCH_SETTING}, m = "updatePercentageGoalProgress")
    public static final class f0 extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f0(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.j2(false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {334}, m = "downloadWatchAppData")
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.D(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp$executeSetLocalization$1", f = "PortfolioApp.kt", l = {FailureCode.FAILED_TO_SET_NOTIFICATION}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(PortfolioApp portfolioApp, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = portfolioApp;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, this.$serial, qn7);
            hVar.p$ = (iv7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object watchLocalizationFromServer;
            IButtonConnectivity b;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                WatchLocalizationRepository f0 = this.this$0.f0();
                this.L$0 = iv7;
                this.label = 1;
                watchLocalizationFromServer = f0.getWatchLocalizationFromServer(true, this);
                if (watchLocalizationFromServer == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                watchLocalizationFromServer = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str = (String) watchLocalizationFromServer;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d2 = PortfolioApp.h0.d();
            local.d(d2, "path of localization file: " + str);
            if (!(str == null || (b = PortfolioApp.h0.b()) == null)) {
                b.setLocalizationData(new LocalizationData(str, null, 2, null), this.$serial);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements iq4.e<zu5.d, zu5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ PortfolioApp f4684a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(PortfolioApp portfolioApp) {
            this.f4684a = portfolioApp;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(zu5.c cVar) {
            pq7.c(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(PortfolioApp.h0.d(), "logout unsuccessfully");
            this.f4684a.C1(null);
            this.f4684a.B1(false);
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(zu5.d dVar) {
            pq7.c(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(PortfolioApp.h0.d(), "logout successfully - start welcome activity");
            Intent intent = new Intent(this.f4684a, WelcomeActivity.class);
            intent.addFlags(268468224);
            this.f4684a.startActivity(intent);
            this.f4684a.C1(null);
            this.f4684a.B1(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp$forceReconnectDevice$2", f = "PortfolioApp.kt", l = {1910}, m = "invokeSuspend")
    public static final class j extends ko7 implements vp7<iv7, qn7<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(PortfolioApp portfolioApp, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = portfolioApp;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            j jVar = new j(this.this$0, this.$serial, qn7);
            jVar.p$ = (iv7) obj;
            return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Object> qn7) {
            return ((j) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            IButtonConnectivity b;
            String str;
            Object S;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d2 = PortfolioApp.h0.d();
                local.d(d2, "Inside " + PortfolioApp.h0.d() + ".forceReconnectDevice");
                b = PortfolioApp.h0.b();
                if (b != null) {
                    str = this.$serial;
                    PortfolioApp portfolioApp = this.this$0;
                    this.L$0 = iv7;
                    this.L$1 = b;
                    this.L$2 = str;
                    this.label = 1;
                    S = portfolioApp.S(this);
                    if (S == d) {
                        return d;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else if (i == 1) {
                String str2 = (String) this.L$2;
                b = (IButtonConnectivity) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                try {
                    el7.b(obj);
                    str = str2;
                    S = obj;
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String d3 = PortfolioApp.h0.d();
                    local2.e(d3, "Error inside " + PortfolioApp.h0.d() + ".deviceReconnect - e=" + e);
                    return tl7.f3441a;
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return ao7.f(b.deviceForceReconnect(str, (UserProfile) S));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {375, 387, 388, 389, 390, 427, 428}, m = "getCurrentUserProfile")
    public static final class k extends co7 {
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public float F$0;
        @DexIgnore
        public float F$1;
        @DexIgnore
        public float F$2;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public int I$2;
        @DexIgnore
        public int I$3;
        @DexIgnore
        public int I$4;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.S(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {555, 562}, m = "getPassphrase")
    public static final class l extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {353}, m = "getUserRegisterDate")
    public static final class m extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.n0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {446}, m = "getWorkoutDetectionSetting")
    public static final class n extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2188}, m = "initSdkForThemeParsing")
    public static final class o extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp$onActiveDeviceStealed$2", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class p extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(PortfolioApp portfolioApp, qn7 qn7) {
            super(2, qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            p pVar = new p(this.this$0, qn7);
            pVar.p$ = (iv7) obj;
            return pVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((p) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = PortfolioApp.h0.d();
                local.d(d, "activeDevice=" + this.this$0.J() + ", was stealed remove it!!!");
                nk5.o.x(this.this$0.J());
                PortfolioApp portfolioApp = this.this$0;
                portfolioApp.b2(portfolioApp.J());
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp b;

        @DexIgnore
        public q(PortfolioApp portfolioApp) {
            this.b = portfolioApp;
        }

        @DexIgnore
        public final void run() {
            if (!this.b.u0() || !this.b.J) {
                PortfolioApp.h0.j(false);
                FLogger.INSTANCE.getLocal().d(PortfolioApp.h0.d(), "still foreground");
                return;
            }
            this.b.p1(false);
            PortfolioApp.h0.j(true);
            FLogger.INSTANCE.getLocal().d(PortfolioApp.h0.d(), "went background");
            this.b.C0();
            ul5 ul5 = this.b.Q;
            if (ul5 != null) {
                ul5.c("");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp$onCreate$1", f = "PortfolioApp.kt", l = {604, 605, 606, 610, 625, 636}, m = "invokeSuspend")
    public static final class r extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public r(PortfolioApp portfolioApp, qn7 qn7) {
            super(2, qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            r rVar = new r(this.this$0, qn7);
            rVar.p$ = (iv7) obj;
            return rVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((r) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00a3  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00f2  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00fd  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x011f  */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x0173  */
        /* JADX WARNING: Removed duplicated region for block: B:51:0x018e  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x01a5  */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x01c3  */
        /* JADX WARNING: Removed duplicated region for block: B:63:0x01cd  */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x01dc  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 498
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.r.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s implements Thread.UncaughtExceptionHandler {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ PortfolioApp f4685a;

        @DexIgnore
        public s(PortfolioApp portfolioApp) {
            this.f4685a = portfolioApp;
        }

        @DexIgnore
        public final void uncaughtException(Thread thread, Throwable th) {
            FLogger.INSTANCE.getLocal().e(PortfolioApp.h0.d(), "uncaughtException - ex=" + th);
            th.printStackTrace();
            pq7.b(th, "e");
            StackTraceElement[] stackTrace = th.getStackTrace();
            int length = stackTrace != null ? stackTrace.length : 0;
            if (length > 0) {
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (stackTrace != null) {
                        StackTraceElement stackTraceElement = stackTrace[i];
                        pq7.b(stackTraceElement, "element");
                        String className = stackTraceElement.getClassName();
                        pq7.b(className, "element.className");
                        String simpleName = ButtonService.class.getSimpleName();
                        pq7.b(simpleName, "ButtonService::class.java.simpleName");
                        if (wt7.v(className, simpleName, false, 2, null)) {
                            FLogger.INSTANCE.getLocal().e(PortfolioApp.h0.d(), "uncaughtException - stopLogService");
                            this.f4685a.U1(FailureCode.APP_CRASH_FROM_BUTTON_SERVICE);
                            break;
                        }
                        i++;
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
            long currentTimeMillis = System.currentTimeMillis();
            this.f4685a.k0().w1(currentTimeMillis);
            FLogger.INSTANCE.getLocal().d(PortfolioApp.h0.d(), "Inside .uncaughtException - currentTime = " + currentTimeMillis);
            Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.f4685a.N;
            if (uncaughtExceptionHandler != null) {
                uncaughtExceptionHandler.uncaughtException(thread, th);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp$pairDevice$2", f = "PortfolioApp.kt", l = {FailureCode.UNEXPECTED_DISCONNECT}, m = "invokeSuspend")
    public static final class t extends ko7 implements vp7<iv7, qn7<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $macAddress;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public t(PortfolioApp portfolioApp, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = portfolioApp;
            this.$serial = str;
            this.$macAddress = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            t tVar = new t(this.this$0, this.$serial, this.$macAddress, qn7);
            tVar.p$ = (iv7) obj;
            return tVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Object> qn7) {
            return ((t) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            IButtonConnectivity b;
            String str;
            Object S;
            String str2;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d2 = PortfolioApp.h0.d();
                local.d(d2, "Inside " + PortfolioApp.h0.d() + ".pairDevice");
                b = PortfolioApp.h0.b();
                if (b != null) {
                    str = this.$serial;
                    String str3 = this.$macAddress;
                    PortfolioApp portfolioApp = this.this$0;
                    this.L$0 = iv7;
                    this.L$1 = b;
                    this.L$2 = str;
                    this.L$3 = str3;
                    this.label = 1;
                    S = portfolioApp.S(this);
                    if (S == d) {
                        return d;
                    }
                    str2 = str3;
                } else {
                    pq7.i();
                    throw null;
                }
            } else if (i == 1) {
                String str4 = (String) this.L$3;
                str = (String) this.L$2;
                IButtonConnectivity iButtonConnectivity = (IButtonConnectivity) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                try {
                    el7.b(obj);
                    S = obj;
                    str2 = str4;
                    b = iButtonConnectivity;
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String d3 = PortfolioApp.h0.d();
                    local2.e(d3, "Error inside " + PortfolioApp.h0.d() + ".pairDevice - e=" + e);
                    return tl7.f3441a;
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return ao7.f(b.pairDevice(str, str2, (UserProfile) S));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2156}, m = "preview")
    public static final class u extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public u(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.Q0(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2150}, m = "previewByThemeFile")
    public static final class v extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public v(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.R0(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {788}, m = "registerContactObserver")
    public static final class w extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public w(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.V0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp$saveInstallation$2", f = "PortfolioApp.kt", l = {1134, 1177}, m = "invokeSuspend")
    public static final class x extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.PortfolioApp$saveInstallation$2$1", f = "PortfolioApp.kt", l = {1178}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Installation $installation;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ x this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.PortfolioApp$x$a$a")
            @eo7(c = "com.portfolio.platform.PortfolioApp$saveInstallation$2$1$response$1", f = "PortfolioApp.kt", l = {1178}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.PortfolioApp$x$a$a  reason: collision with other inner class name */
            public static final class C0347a extends ko7 implements rp7<qn7<? super q88<Installation>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0347a(a aVar, qn7 qn7) {
                    super(1, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    return new C0347a(this.this$0, qn7);
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // com.fossil.rp7
                public final Object invoke(qn7<? super q88<Installation>> qn7) {
                    return ((C0347a) create(qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        ApiServiceV2 Z = this.this$0.this$0.this$0.Z();
                        Installation installation = this.this$0.$installation;
                        this.label = 1;
                        Object insertInstallation = Z.insertInstallation(installation, this);
                        return insertInstallation == d ? d : insertInstallation;
                    } else if (i == 1) {
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(x xVar, Installation installation, qn7 qn7) {
                super(2, qn7);
                this.this$0 = xVar;
                this.$installation = installation;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$installation, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d;
                Object d2 = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    C0347a aVar = new C0347a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    d = jq5.d(aVar, this);
                    if (d == d2) {
                        return d2;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    d = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                iq5 iq5 = (iq5) d;
                if (iq5 instanceof kq5) {
                    kq5 kq5 = (kq5) iq5;
                    if (kq5.a() != null) {
                        this.this$0.this$0.k0().F0(((Installation) kq5.a()).getInstallationId());
                    }
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public x(PortfolioApp portfolioApp, qn7 qn7) {
            super(2, qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            x xVar = new x(this.this$0, qn7);
            xVar.p$ = (iv7) obj;
            return xVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((x) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x003c  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 285
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.x.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2243}, m = "setImplicitDeviceConfig")
    public static final class y extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public y(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.x1(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2143}, m = "setUpSDK")
    public static final class z extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public z(PortfolioApp portfolioApp, qn7 qn7) {
            super(qn7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.J1(this);
        }
    }

    /*
    static {
        String simpleName = PortfolioApp.class.getSimpleName();
        pq7.b(simpleName, "PortfolioApp::class.java.simpleName");
        b0 = simpleName;
    }
    */

    @DexIgnore
    public static final IButtonConnectivity O() {
        return g0;
    }

    @DexIgnore
    public static /* synthetic */ void W1(PortfolioApp portfolioApp, String str, ob7 ob7, String str2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = null;
        }
        if ((i2 & 2) != 0) {
            ob7 = null;
        }
        portfolioApp.V1(str, ob7, str2);
    }

    @DexIgnore
    public static /* synthetic */ Object a2(PortfolioApp portfolioApp, ob7 ob7, boolean z2, qn7 qn7, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        return portfolioApp.Z1(ob7, z2, qn7);
    }

    @DexIgnore
    public static final /* synthetic */ void l(boolean z2) {
    }

    @DexIgnore
    public static final /* synthetic */ void m(MFDeviceService.b bVar) {
    }

    @DexIgnore
    public final Object A(qn7<? super tl7> qn7) {
        Object g2 = eu7.g(bw7.b(), new f(null), qn7);
        return g2 == yn7.d() ? g2 : tl7.f3441a;
    }

    @DexIgnore
    public final boolean A0(String str) {
        pq7.c(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.isSyncing(str);
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            return false;
        }
    }

    @DexIgnore
    public final void A1(String str) {
        MisfitDeviceProfile deviceProfile;
        MisfitDeviceProfile deviceProfile2;
        pq7.c(str, "serial");
        try {
            String Y2 = Y();
            IButtonConnectivity iButtonConnectivity = g0;
            String locale = (iButtonConnectivity == null || (deviceProfile2 = iButtonConnectivity.getDeviceProfile(this.H.e())) == null) ? null : deviceProfile2.getLocale();
            on5 on5 = this.d;
            if (on5 != null) {
                String E2 = on5.E(Y2);
                IButtonConnectivity iButtonConnectivity2 = g0;
                String localeVersion = (iButtonConnectivity2 == null || (deviceProfile = iButtonConnectivity2.getDeviceProfile(this.H.e())) == null) ? null : deviceProfile.getLocaleVersion();
                on5 on52 = this.d;
                if (on52 != null) {
                    String D2 = on52.D();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = b0;
                    local.d(str2, "phoneLocale " + Y2 + " - watchLocale: " + locale + " - standardLocale: " + E2 + "\nwatchLocaleVersion: " + localeVersion + " - standardLocaleVersion: " + D2);
                    if (!pq7.a(E2, locale)) {
                        E(Y2, str);
                    } else if (!pq7.a(localeVersion, D2)) {
                        E(Y2, str);
                    } else {
                        FLogger.INSTANCE.getLocal().d(b0, "don't need to set to watch");
                    }
                } else {
                    pq7.n("sharedPreferencesManager");
                    throw null;
                }
            } else {
                pq7.n("sharedPreferencesManager");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void B(String str, FirmwareData firmwareData, UserProfile userProfile) {
        pq7.c(str, "serial");
        pq7.c(firmwareData, "firmwareData");
        pq7.c(userProfile, "userProfile");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "Inside " + b0 + ".otaDevice");
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceOta(str, firmwareData, userProfile);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "Error inside " + b0 + ".otaDevice - e=" + e2);
        }
    }

    @DexIgnore
    public final Object B0(byte[] bArr, qn7<? super Boolean> qn7) {
        Boolean a2;
        IButtonConnectivity iButtonConnectivity = g0;
        return ao7.a((iButtonConnectivity == null || (a2 = ao7.a(iButtonConnectivity.isThemePackageEditable(bArr))) == null) ? false : a2.booleanValue());
    }

    @DexIgnore
    public final void B1(boolean z2) {
        this.O = z2;
    }

    @DexIgnore
    public final Object C(String str, qn7<? super Boolean> qn7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "downloadWatchAppData(), uiVersion = " + str);
        WatchAppDataRepository watchAppDataRepository = this.F;
        if (watchAppDataRepository != null) {
            return watchAppDataRepository.downloadWatchAppData(P(), str, qn7);
        }
        pq7.n("mWatchAppDataRepository");
        throw null;
    }

    @DexIgnore
    public final void C0() {
        ul5 f2 = ck5.f.f("ota_session");
        ul5 f3 = ck5.f.f("sync_session");
        ul5 f4 = ck5.f.f("setup_device_session");
        if (f2 != null && f2.f()) {
            f2.a(ck5.f.b("ota_session_go_to_background"));
        } else if (f3 != null && f3.f()) {
            f3.a(ck5.f.b("sync_session_go_to_background"));
        } else if (f4 != null && f4.f()) {
            f4.a(ck5.f.b("setup_device_session_go_to_background"));
        }
    }

    @DexIgnore
    public final void C1(ServerError serverError) {
        this.P = serverError;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object D(com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.PortfolioApp.g
            if (r0 == 0) goto L_0x0038
            r0 = r7
            com.portfolio.platform.PortfolioApp$g r0 = (com.portfolio.platform.PortfolioApp.g) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0047
            if (r0 != r5) goto L_0x003f
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x002c:
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            com.fossil.ao7.a(r0)
        L_0x0035:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0037:
            return r0
        L_0x0038:
            com.portfolio.platform.PortfolioApp$g r0 = new com.portfolio.platform.PortfolioApp$g
            r0.<init>(r6, r7)
            r1 = r0
            goto L_0x0014
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            com.fossil.el7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.b0
            java.lang.String r4 = "downloadWatchAppData()"
            r0.d(r2, r4)
            java.lang.String r0 = r6.m0()
            if (r0 == 0) goto L_0x0035
            com.portfolio.platform.data.source.WatchAppDataRepository r2 = r6.F
            if (r2 == 0) goto L_0x0073
            java.lang.String r4 = r6.P()
            r1.L$0 = r6
            r1.L$1 = r0
            r1.label = r5
            java.lang.Object r0 = r2.downloadWatchAppData(r4, r0, r1)
            if (r0 != r3) goto L_0x002c
            r0 = r3
            goto L_0x0037
        L_0x0073:
            java.lang.String r0 = "mWatchAppDataRepository"
            com.fossil.pq7.n(r0)
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.D(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void D0() {
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.logOut();
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final long D1(String str, List<? extends MicroAppMapping> list) {
        pq7.c(str, "serial");
        pq7.c(list, "mappings");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            for (MicroAppMapping microAppMapping : list) {
                if (microAppMapping != null) {
                    StringBuilder sb = new StringBuilder("Inside .setMappings set mapping of deviceId=" + str + ", gesture=" + microAppMapping.getGesture() + ", appID=" + microAppMapping.getMicroAppId() + ", declarationFiles=");
                    String[] declarationFiles = microAppMapping.getDeclarationFiles();
                    for (String str2 : declarationFiles) {
                        sb.append("||");
                        sb.append(str2);
                    }
                    FLogger.INSTANCE.getLocal().d(b0, sb.toString());
                    FLogger.INSTANCE.getLocal().d(b0, "Inside .setMappings set mapping of deviceId=" + str + ", gesture=" + microAppMapping.getGesture() + ", appID=" + microAppMapping.getMicroAppId() + ", extraInfo=" + Arrays.toString(microAppMapping.getDeclarationFiles()));
                }
            }
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetMapping(str, MicroAppMapping.convertToBLEMapping(list));
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void E(String str, String str2) {
        String str3;
        String str4 = "localization_" + str + '.';
        File[] listFiles = new File(getFilesDir() + "/localization").listFiles();
        int length = listFiles.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                str3 = "";
                break;
            }
            File file = listFiles[i2];
            pq7.b(file, "file");
            String path = file.getPath();
            pq7.b(path, "file.path");
            if (wt7.v(path, str4, false, 2, null)) {
                str3 = file.getPath();
                pq7.b(str3, "file.path");
                break;
            }
            i2++;
        }
        FLogger.INSTANCE.getLocal().d(b0, "setLocalization - neededLocalizationFilePath: " + str3);
        if (TextUtils.isEmpty(str3)) {
            xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new h(this, str2, null), 3, null);
            return;
        }
        IButtonConnectivity iButtonConnectivity = g0;
        if (iButtonConnectivity != null) {
            iButtonConnectivity.setLocalizationData(new LocalizationData(str3, null, 2, null), str2);
        }
    }

    @DexIgnore
    public final long E0(String str, NotificationBaseObj notificationBaseObj) {
        pq7.c(str, "serial");
        pq7.c(notificationBaseObj, "notification");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "notifyNotificationControlEvent - notification=" + notificationBaseObj);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.notifyNotificationEvent(notificationBaseObj, str);
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".notifyNotificationControlEvent(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long E1(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        pq7.c(appNotificationFilterSettings, "notificationFilterSettings");
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setNotificationFilterSettings - notificationFilterSettings=" + appNotificationFilterSettings);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setNotificationFilterSettings(appNotificationFilterSettings, str);
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".setNotificationFilterSettings(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void F(ServerError serverError) {
        FLogger.INSTANCE.getLocal().d(b0, "forceLogout");
        if (this.I) {
            zu5 zu5 = this.m;
            if (zu5 != null) {
                zu5.e(new zu5.b(2, null), new i(this));
            } else {
                pq7.n("mDeleteLogoutUserUseCase");
                throw null;
            }
        } else {
            this.P = serverError;
            this.O = true;
        }
    }

    @DexIgnore
    public final void F0(String str, boolean z2) {
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "notifyWatchAppDataReady(), serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.notifyWatchAppFilesReady(str, z2);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "notifyWatchAppDataReady() - e=" + e2);
        }
    }

    @DexIgnore
    public final void F1(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b0;
        local.d(str3, ".setPairedSerial - serial=" + str + ", macaddress=" + str2);
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setPairedSerial(str, str2);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, "setPairedSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    public final Object G(String str, qn7<Object> qn7) {
        return eu7.g(bw7.b(), new j(this, str, null), qn7);
    }

    @DexIgnore
    public final Object G0(qn7<? super tl7> qn7) {
        Object g2 = eu7.g(bw7.b(), new p(this, null), qn7);
        return g2 == yn7.d() ? g2 : tl7.f3441a;
    }

    @DexIgnore
    public final long G1(ReplyMessageMappingGroup replyMessageMappingGroup, String str) {
        pq7.c(replyMessageMappingGroup, "replyMessageGroup");
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setReplyMessageMapping - replyMessageGroup=" + replyMessageMappingGroup);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setReplyMessageMappingSetting(replyMessageMappingGroup, str);
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".setReplyMessageMappingSetting(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final boolean H(String str) {
        pq7.c(str, "newActiveDeviceSerial");
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                boolean forceSwitchDeviceWithoutErase = iButtonConnectivity.forceSwitchDeviceWithoutErase(str);
                if (!forceSwitchDeviceWithoutErase) {
                    return forceSwitchDeviceWithoutErase;
                }
                J0(str);
                return forceSwitchDeviceWithoutErase;
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    public final void H0(String str, boolean z2, WatchParamsFileMapping watchParamsFileMapping) {
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "onGetWatchParamResponse -serial =" + str + ", content=" + watchParamsFileMapping);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.onSetWatchParamResponse(str, z2, watchParamsFileMapping);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "onSetWatchParamResponse - serial " + str + " exception " + e2);
        }
    }

    @DexIgnore
    public final void H1(String str) {
        pq7.c(str, ButtonService.USER_ID);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.updateUserId(str);
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "exception when set userId to SDK " + str);
        }
    }

    @DexIgnore
    public final String I() {
        String str;
        String h2;
        String J2 = J();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "generateDeviceNotificationContent activeSerial=" + J2);
        if (TextUtils.isEmpty(J2)) {
            PortfolioApp portfolioApp = d0;
            if (portfolioApp != null) {
                String c2 = um5.c(portfolioApp, 2131887059);
                pq7.b(c2, "LanguageHelper.getString\u2026Dashboard_CTA__PairWatch)");
                return c2;
            }
            pq7.n("instance");
            throw null;
        }
        String c3 = um5.c(this, 2131887173);
        DeviceRepository deviceRepository = this.u;
        if (deviceRepository != null) {
            String deviceNameBySerial = deviceRepository.getDeviceNameBySerial(J2);
            try {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = b0;
                StringBuilder sb = new StringBuilder();
                sb.append("generateDeviceNotificationContent gattState=");
                IButtonConnectivity iButtonConnectivity = g0;
                if (iButtonConnectivity != null) {
                    sb.append(iButtonConnectivity.getGattState(J2));
                    local2.d(str3, sb.toString());
                    IButtonConnectivity iButtonConnectivity2 = g0;
                    if (iButtonConnectivity2 != null) {
                        if (iButtonConnectivity2.getGattState(J2) == 2) {
                            PortfolioApp portfolioApp2 = d0;
                            if (portfolioApp2 == null) {
                                pq7.n("instance");
                                throw null;
                            } else if (portfolioApp2.A0(J2)) {
                                PortfolioApp portfolioApp3 = d0;
                                if (portfolioApp3 != null) {
                                    str = um5.c(portfolioApp3, 2131886784);
                                } else {
                                    pq7.n("instance");
                                    throw null;
                                }
                            } else {
                                on5 on5 = this.d;
                                if (on5 != null) {
                                    long C2 = on5.C(J2);
                                    if (C2 == 0) {
                                        h2 = "";
                                    } else if (System.currentTimeMillis() - C2 < 60000) {
                                        hr7 hr7 = hr7.f1520a;
                                        PortfolioApp portfolioApp4 = d0;
                                        if (portfolioApp4 != null) {
                                            String c4 = um5.c(portfolioApp4, 2131887191);
                                            pq7.b(c4, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                                            h2 = String.format(c4, Arrays.copyOf(new Object[]{1}, 1));
                                            pq7.b(h2, "java.lang.String.format(format, *args)");
                                        } else {
                                            pq7.n("instance");
                                            throw null;
                                        }
                                    } else {
                                        h2 = lk5.h(C2);
                                    }
                                    hr7 hr72 = hr7.f1520a;
                                    PortfolioApp portfolioApp5 = d0;
                                    if (portfolioApp5 != null) {
                                        String c5 = um5.c(portfolioApp5, 2131887179);
                                        pq7.b(c5, "LanguageHelper.getString\u2026_Text__LastSyncedDayTime)");
                                        str = String.format(c5, Arrays.copyOf(new Object[]{h2}, 1));
                                        pq7.b(str, "java.lang.String.format(format, *args)");
                                    } else {
                                        pq7.n("instance");
                                        throw null;
                                    }
                                } else {
                                    pq7.n("sharedPreferencesManager");
                                    throw null;
                                }
                            }
                        } else {
                            str = c3;
                        }
                        return deviceNameBySerial + " : " + str;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            } catch (Exception e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str4 = b0;
                local3.d(str4, "generateDeviceNotificationContent e=" + e2);
                str = c3;
            }
        } else {
            pq7.n("mDeviceRepository");
            throw null;
        }
    }

    @DexIgnore
    public final void I0(String str) {
        pq7.c(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, ".onPing(), serial=" + str);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.onPing(str);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".onPing() - e=" + e2);
        }
    }

    @DexIgnore
    public final void I1(String str, String str2) {
        pq7.c(str2, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local.d(str3, ".setSecretKeyToDevice(), secretKey=" + str);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setSecretKey(str2, str);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, ".setSecretKeyToDevice() - e=" + e2);
        }
    }

    @DexIgnore
    public final String J() {
        on5 on5 = this.d;
        if (on5 != null) {
            String g2 = on5.g();
            return g2 != null ? g2 : "";
        }
        pq7.n("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final void J0(String str) {
        pq7.c(str, "newActiveDeviceSerial");
        String J2 = J();
        on5 on5 = this.d;
        if (on5 != null) {
            on5.E0(str);
            this.H.l(str);
            eo5.c.d(this);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "onSwitchActiveDeviceSuccess - current=" + J2 + ", new=" + str);
            return;
        }
        pq7.n("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object J1(com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.PortfolioApp.z
            if (r0 == 0) goto L_0x004a
            r0 = r7
            com.portfolio.platform.PortfolioApp$z r0 = (com.portfolio.platform.PortfolioApp.z) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x004a
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0058
            if (r3 != r4) goto L_0x0050
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0028:
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x0047
            com.misfit.frameworks.buttonservice.IButtonConnectivity r1 = com.portfolio.platform.PortfolioApp.g0
            if (r1 == 0) goto L_0x006b
            com.fossil.h37 r2 = com.fossil.h37.b
            java.lang.String r2 = r2.d()
            java.lang.String r3 = r0.getUserId()
            com.portfolio.platform.data.model.MFUser$Auth r0 = r0.getAuth()
            java.lang.String r0 = r0.getRefreshToken()
            java.lang.String r4 = "theme/fonts"
            r1.setUpSDKTheme(r2, r3, r0, r4)
        L_0x0047:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0049:
            return r0
        L_0x004a:
            com.portfolio.platform.PortfolioApp$z r0 = new com.portfolio.platform.PortfolioApp$z
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0050:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0058:
            com.fossil.el7.b(r1)
            com.portfolio.platform.data.source.UserRepository r1 = r6.B
            if (r1 == 0) goto L_0x006f
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r1.getCurrentUser(r0)
            if (r0 != r2) goto L_0x0028
            r0 = r2
            goto L_0x0049
        L_0x006b:
            com.fossil.pq7.i()
            throw r5
        L_0x006f:
            java.lang.String r0 = "mUserRepository"
            com.fossil.pq7.n(r0)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.J1(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final MutableLiveData<String> K() {
        on5 on5 = this.d;
        if (on5 != null) {
            String g2 = on5.g();
            if (!pq7.a(g2, this.H.e())) {
                this.H.l(g2);
            }
            return this.H;
        }
        pq7.n("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final void K0(String str, boolean z2) {
        pq7.c(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, ".onUpdateSecretKeyToServerResponse(), isSuccess=" + z2);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendPushSecretKeyResponse(str, z2);
                tl7 tl7 = tl7.f3441a;
                return;
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".onUpdateSecretKeyToServerResponse() - e=" + e2);
            tl7 tl72 = tl7.f3441a;
        }
    }

    @DexIgnore
    public final long K1(String str, int i2, int i3, int i4) {
        pq7.c(str, "serial");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "Inside " + b0 + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2);
            if (!TextUtils.isEmpty(str)) {
                IButtonConnectivity iButtonConnectivity = g0;
                if (iButtonConnectivity != null) {
                    return iButtonConnectivity.deviceUpdateActivityGoals(str, i2, i3, i4);
                }
                pq7.i();
                throw null;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "Error Inside " + b0 + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2 + ", caloriesGoal=" + i3 + ", activeTimeGoal=" + i4);
            return time_stamp_for_non_executable_method;
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local3.e(str4, "Error Inside " + b0 + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2 + ", ex=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final boolean L() {
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            return iButtonConnectivity != null && iButtonConnectivity.getGattState(J()) == 2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b0;
            local.d(str, "exception when get gatt state " + e2);
            return false;
        }
    }

    @DexIgnore
    public final Object L0(String str, String str2, qn7<Object> qn7) {
        return eu7.g(bw7.b(), new t(this, str, str2, null), qn7);
    }

    @DexIgnore
    public final void L1(String str, VibrationStrengthObj vibrationStrengthObj) {
        pq7.c(str, "serial");
        pq7.c(vibrationStrengthObj, "vibrationStrengthObj");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "Inside " + b0 + ".setVibrationStrength");
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSetVibrationStrength(str, vibrationStrengthObj);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "Error inside " + b0 + ".setVibrationStrength - e=" + e2);
        }
    }

    @DexIgnore
    public final ro4 M() {
        ro4 ro4 = this.M;
        if (ro4 != null) {
            return ro4;
        }
        pq7.n("applicationComponent");
        throw null;
    }

    @DexIgnore
    public final boolean M0(String str, PairingResponse pairingResponse) {
        pq7.c(str, "serial");
        pq7.c(pairingResponse, "response");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, ".pairDeviceResponse(), response=" + pairingResponse);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.pairDeviceResponse(str, pairingResponse);
                return true;
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".pairDeviceResponse() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final long M1(WatchAppMappingSettings watchAppMappingSettings, String str) {
        pq7.c(str, "serial");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "setWatchApps " + watchAppMappingSettings);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setWatchApps(watchAppMappingSettings, str);
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final String N(String str) {
        pq7.c(str, "packageName");
        String str2 = (String) pm7.P(wt7.Y(str, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0, 6, null));
        PortfolioApp portfolioApp = d0;
        if (portfolioApp != null) {
            PackageManager packageManager = portfolioApp.getPackageManager();
            try {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 0);
                return applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo).toString() : str2;
            } catch (PackageManager.NameNotFoundException e2) {
                return str2;
            }
        } else {
            pq7.n("instance");
            throw null;
        }
    }

    @DexIgnore
    public final Object N0(byte[] bArr, qn7<? super ob7> qn7) {
        ThemeData parseBinaryToThemeData;
        IButtonConnectivity iButtonConnectivity = g0;
        if (iButtonConnectivity == null || (parseBinaryToThemeData = iButtonConnectivity.parseBinaryToThemeData(J(), bArr)) == null) {
            return null;
        }
        return cc7.q(parseBinaryToThemeData);
    }

    @DexIgnore
    public final void N1(WorkoutConfigData workoutConfigData, String str) {
        pq7.c(workoutConfigData, "workoutConfigData");
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setWorkoutConfig - workoutConfigData=" + workoutConfigData);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setWorkoutConfig(workoutConfigData, str);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final long O0(String str, int i2, int i3, boolean z2) {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "Inside " + b0 + ".playVibeNotification");
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.playVibration(str, i2, i3, true);
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "Error inside " + b0 + ".playVibeNotification - e=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long O1(String str, WorkoutDetectionSetting workoutDetectionSetting) {
        pq7.c(str, "serial");
        pq7.c(workoutDetectionSetting, "workoutDetectionSetting");
        FLogger.INSTANCE.getLocal().d(b0, "setWorkoutDetectionSetting()");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setWorkoutDetectionSetting(workoutDetectionSetting, str);
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, "setWorkoutDetectionSetting(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final String P() {
        List e2;
        boolean z2;
        boolean z3 = true;
        if (e0) {
            List<String> split = new jt7(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).split("4.6.0", 0);
            if (!split.isEmpty()) {
                ListIterator<String> listIterator = split.listIterator(split.size());
                while (true) {
                    if (!listIterator.hasPrevious()) {
                        break;
                    }
                    if (listIterator.previous().length() == 0) {
                        z2 = true;
                        continue;
                    } else {
                        z2 = false;
                        continue;
                    }
                    if (!z2) {
                        e2 = pm7.d0(split, listIterator.nextIndex() + 1);
                        break;
                    }
                }
            }
            e2 = hm7.e();
            Object[] array = e2.toArray(new String[0]);
            if (array != null) {
                String[] strArr = (String[]) array;
                if (strArr.length != 0) {
                    z3 = false;
                }
                if (!z3) {
                    return strArr[0];
                }
            } else {
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return "4.6.0";
    }

    @DexIgnore
    public final long P0(String str) {
        pq7.c(str, "serial");
        return O0(str, 1, 1, true);
    }

    @DexIgnore
    public final long P1(String str, Location location) {
        pq7.c(str, "serial");
        pq7.c(location, PlaceFields.LOCATION);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setWorkoutGPSDataSession(str, location);
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, ".setWorkoutGPSDataSession - e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final String Q() {
        String name = ph5.fromInt(Integer.parseInt(rq4.G.d())).getName();
        pq7.b(name, "FossilBrand.fromInt(Inte\u2026onfig.brandId)).getName()");
        return name;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object Q0(com.fossil.ob7 r8, com.fossil.qn7<? super com.fossil.tl7> r9) {
        /*
            r7 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.PortfolioApp.u
            if (r0 == 0) goto L_0x0061
            r0 = r9
            com.portfolio.platform.PortfolioApp$u r0 = (com.portfolio.platform.PortfolioApp.u) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0061
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0070
            if (r3 != r4) goto L_0x0068
            java.lang.Object r0 = r1.L$1
            com.fossil.ob7 r0 = (com.fossil.ob7) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.PortfolioApp r1 = (com.portfolio.platform.PortfolioApp) r1
            com.fossil.el7.b(r2)
        L_0x002b:
            com.misfit.frameworks.buttonservice.model.watchface.ThemeData r2 = com.fossil.cc7.h(r0)
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "preview with wfThemeData "
            r5.append(r6)
            r5.append(r0)
            java.lang.String r0 = " buttonThemeData "
            r5.append(r0)
            r5.append(r2)
            java.lang.String r0 = r5.toString()
            r3.d(r4, r0)
            com.misfit.frameworks.buttonservice.IButtonConnectivity r0 = com.portfolio.platform.PortfolioApp.g0
            if (r0 == 0) goto L_0x0082
            java.lang.String r1 = r1.J()
            r0.previewTheme(r1, r2)
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0060:
            return r0
        L_0x0061:
            com.portfolio.platform.PortfolioApp$u r0 = new com.portfolio.platform.PortfolioApp$u
            r0.<init>(r7, r9)
            r1 = r0
            goto L_0x0014
        L_0x0068:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0070:
            com.fossil.el7.b(r2)
            r1.L$0 = r7
            r1.L$1 = r8
            r1.label = r4
            java.lang.Object r1 = r7.J1(r1)
            if (r1 == r0) goto L_0x0060
            r0 = r8
            r1 = r7
            goto L_0x002b
        L_0x0082:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.Q0(com.fossil.ob7, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void Q1(MFUser mFUser) {
        if (mFUser == null) {
            ck5.f.g().s(false);
            return;
        }
        ck5.f.g().w(mFUser.getUserId());
        ck5.f.g().s(mFUser.getDiagnosticEnabled());
    }

    @DexIgnore
    public final int R(String str) {
        pq7.c(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.getCommunicatorModeBySerial(str);
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            return -1;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object R0(java.lang.String r7, com.fossil.qn7<? super com.fossil.tl7> r8) {
        /*
            r6 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.PortfolioApp.v
            if (r0 == 0) goto L_0x0066
            r0 = r8
            com.portfolio.platform.PortfolioApp$v r0 = (com.portfolio.platform.PortfolioApp.v) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0066
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0075
            if (r3 != r4) goto L_0x006d
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.PortfolioApp r1 = (com.portfolio.platform.PortfolioApp) r1
            com.fossil.el7.b(r2)
        L_0x002b:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "previewByThemeFile "
            r4.append(r5)
            r4.append(r0)
            java.lang.String r5 = " isExist "
            r4.append(r5)
            java.io.File r5 = new java.io.File
            r5.<init>(r0)
            boolean r5 = r5.exists()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.d(r3, r4)
            com.misfit.frameworks.buttonservice.IButtonConnectivity r2 = com.portfolio.platform.PortfolioApp.g0
            if (r2 == 0) goto L_0x0063
            java.lang.String r1 = r1.J()
            r2.previewThemeByFile(r1, r0)
        L_0x0063:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0065:
            return r0
        L_0x0066:
            com.portfolio.platform.PortfolioApp$v r0 = new com.portfolio.platform.PortfolioApp$v
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0014
        L_0x006d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0075:
            com.fossil.el7.b(r2)
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r4
            java.lang.Object r1 = r6.J1(r1)
            if (r1 == r0) goto L_0x0065
            r0 = r7
            r1 = r6
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.R0(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void R1(int i2, int i3, int i4, int i5) {
        String J2 = J();
        int min = Math.min(i2, 65535);
        int min2 = Math.min(i3, 65535);
        int min3 = Math.min(i4, 65535);
        int min4 = Math.min(i5, 4294967);
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b0;
            local.d(str, "Inside simulateDisconnection - delay=" + min + ", duration=" + min2);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.simulateDisconnection(J2, min, min2, min3, min4);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local2.e(str2, "Error inside " + b0 + ".simulateDisconnection - e=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x05ae  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x05ce  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x05e0  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x05e7  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x05ee  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x05f5  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x0610 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x01d3  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x01d7  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x023d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0252  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x02a0  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x02a6  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x02fb  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x02ff  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0335  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0368  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0397  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x03d4  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x040e  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0442  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0452  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0024  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x050a  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x055b  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0563  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object S(com.fossil.qn7<? super com.misfit.frameworks.buttonservice.model.UserProfile> r45) {
        /*
        // Method dump skipped, instructions count: 1582
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.S(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final long S0(String str) {
        pq7.c(str, "serial");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.readCurrentWorkoutSession(str);
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, ".readCurrentWorkoutSession - e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void S1(mj5 mj5, boolean z2, int i2) {
        pq7.c(mj5, "factory");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.d(str, "startDeviceSync, isNewDevice=" + z2 + ", syncMode=" + i2);
        cl5.c.e();
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", 0);
        intent.putExtra("SERIAL", J());
        ct0.b(this).d(intent);
        String J2 = J();
        if (A0(J2)) {
            FLogger.INSTANCE.getLocal().d(b0, "Device is syncing, Skip this sync.");
        } else {
            mj5.b(J2).e(new tt5(i2, J2, z2), null);
        }
    }

    @DexIgnore
    public final bo5 T() {
        bo5 bo5 = this.A;
        if (bo5 != null) {
            return bo5;
        }
        pq7.n("mDataValidationManager");
        throw null;
    }

    @DexIgnore
    public final long T0() {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceReadRealTimeStep(J());
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final boolean T1(String str, UserProfile userProfile) {
        pq7.c(str, "serial");
        pq7.c(userProfile, "userProfile");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        boolean z2 = false;
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "startDeviceSyncInButtonService - serial=" + str);
            if (!TextUtils.isEmpty(str)) {
                IButtonConnectivity iButtonConnectivity = g0;
                if (iButtonConnectivity != null) {
                    time_stamp_for_non_executable_method = iButtonConnectivity.deviceStartSync(str, userProfile);
                    z2 = true;
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = b0;
                local2.e(str3, "startDeviceSyncInButtonService - serial " + str);
            }
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local3.e(str4, "startDeviceSyncInButtonService - serial " + str + " exception " + e2);
        }
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str5 = b0;
        local4.d(str5, "put timestamp " + time_stamp_for_non_executable_method + " for sync session");
        return z2;
    }

    @DexIgnore
    public final String U() {
        String Q2 = Q();
        return pq7.a(Q2, ph5.CITIZEN.getName()) ? "citizen" : pq7.a(Q2, ph5.UNIVERSAL.getName()) ? "universal" : Endpoints.DEFAULT_NAME;
    }

    @DexIgnore
    public final void U0() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
        intentFilter.addDataScheme(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY);
        registerReceiver(this.c, intentFilter);
    }

    @DexIgnore
    public final void U1(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.d(str, "stopLogService - failureCode=" + i2);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.stopLogService(i2);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local2.e(str2, "stopLogService - ex=" + e2);
        }
    }

    @DexIgnore
    public final FossilDeviceSerialPatternUtil.BRAND V() {
        String Q2 = Q();
        return pq7.a(Q2, ph5.CHAPS.getName()) ? FossilDeviceSerialPatternUtil.BRAND.CHAPS : pq7.a(Q2, ph5.DIESEL.getName()) ? FossilDeviceSerialPatternUtil.BRAND.DIESEL : pq7.a(Q2, ph5.EA.getName()) ? FossilDeviceSerialPatternUtil.BRAND.EA : pq7.a(Q2, ph5.KATESPADE.getName()) ? FossilDeviceSerialPatternUtil.BRAND.KATE_SPADE : pq7.a(Q2, ph5.MICHAELKORS.getName()) ? FossilDeviceSerialPatternUtil.BRAND.MICHAEL_KORS : pq7.a(Q2, ph5.SKAGEN.getName()) ? FossilDeviceSerialPatternUtil.BRAND.SKAGEN : pq7.a(Q2, ph5.AX.getName()) ? FossilDeviceSerialPatternUtil.BRAND.ARMANI_EXCHANGE : pq7.a(Q2, ph5.RELIC.getName()) ? FossilDeviceSerialPatternUtil.BRAND.RELIC : pq7.a(Q2, ph5.MJ.getName()) ? FossilDeviceSerialPatternUtil.BRAND.MARC_JACOBS : pq7.a(Q2, ph5.FOSSIL.getName()) ? FossilDeviceSerialPatternUtil.BRAND.FOSSIL : pq7.a(Q2, ph5.UNIVERSAL.getName()) ? FossilDeviceSerialPatternUtil.BRAND.UNIVERSAL : pq7.a(Q2, ph5.CITIZEN.getName()) ? FossilDeviceSerialPatternUtil.BRAND.CITIZEN : FossilDeviceSerialPatternUtil.BRAND.UNKNOWN;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object V0(com.fossil.qn7<? super com.fossil.tl7> r8) {
        /*
            r7 = this;
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = 0
            r5 = 1
            boolean r0 = r8 instanceof com.portfolio.platform.PortfolioApp.w
            if (r0 == 0) goto L_0x007c
            r0 = r8
            com.portfolio.platform.PortfolioApp$w r0 = (com.portfolio.platform.PortfolioApp.w) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x007c
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x008a
            if (r3 != r5) goto L_0x0082
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.el7.b(r1)
            r7 = r0
        L_0x0028:
            r0 = r1
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x00cc
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "registerContactObserver currentUser="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.d(r2, r0)
            com.fossil.g47$a r0 = com.fossil.g47.f1261a
            boolean r0 = r0.l(r7)
            if (r0 == 0) goto L_0x00af
            com.fossil.l37 r0 = r7.k
            if (r0 == 0) goto L_0x00a9
            r0.b()
            android.content.ContentResolver r0 = r7.getContentResolver()
            android.net.Uri r1 = android.provider.ContactsContract.Contacts.CONTENT_URI
            com.fossil.l37 r2 = r7.k
            if (r2 == 0) goto L_0x00a3
            r0.registerContentObserver(r1, r5, r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.PortfolioApp.b0
            java.lang.String r2 = "registerContactObserver success"
            r0.d(r1, r2)
            com.fossil.on5 r0 = r7.d
            if (r0 == 0) goto L_0x009d
            r0.r1(r5)
        L_0x0079:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x007b:
            return r0
        L_0x007c:
            com.portfolio.platform.PortfolioApp$w r0 = new com.portfolio.platform.PortfolioApp$w
            r0.<init>(r7, r8)
            goto L_0x0014
        L_0x0082:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x008a:
            com.fossil.el7.b(r1)
            com.portfolio.platform.data.source.UserRepository r1 = r7.B
            if (r1 == 0) goto L_0x00cf
            r0.L$0 = r7
            r0.label = r5
            java.lang.Object r1 = r1.getCurrentUser(r0)
            if (r1 != r2) goto L_0x0028
            r0 = r2
            goto L_0x007b
        L_0x009d:
            java.lang.String r0 = "sharedPreferencesManager"
            com.fossil.pq7.n(r0)
            throw r6
        L_0x00a3:
            java.lang.String r0 = "mContactObserver"
            com.fossil.pq7.n(r0)
            throw r6
        L_0x00a9:
            java.lang.String r0 = "mContactObserver"
            com.fossil.pq7.n(r0)
            throw r6
        L_0x00af:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "registerContactObserver fail due to enough Permission ="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.d(r2, r0)
            goto L_0x0079
        L_0x00cc:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            goto L_0x007b
        L_0x00cf:
            java.lang.String r0 = "mUserRepository"
            com.fossil.pq7.n(r0)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.V0(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void V1(String str, ob7 ob7, String str2) {
        ThemeData themeData = null;
        pq7.c(str2, "serial");
        if (str == null && ob7 == null) {
            throw new Exception("themeFilePath & wfThemeData are not both null - or just themeFilePath null or just wfThemeData null");
        }
        IButtonConnectivity iButtonConnectivity = g0;
        if (iButtonConnectivity != null) {
            if (ob7 != null) {
                themeData = cc7.h(ob7);
            }
            iButtonConnectivity.storeThemeConfig(str2, str, themeData);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final int W(String str) {
        pq7.c(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.getGattState(str);
            }
            return 0;
        } catch (Exception e2) {
            return 0;
        }
    }

    @DexIgnore
    public final void W0() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.provider.Telephony.WAP_PUSH_RECEIVED");
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        dq5 dq5 = this.V;
        if (dq5 != null) {
            registerReceiver(dq5, intentFilter);
        } else {
            pq7.n("mMessageReceiver");
            throw null;
        }
    }

    @DexIgnore
    public final String X() {
        Locale locale = Locale.getDefault();
        pq7.b(locale, "locale");
        String language = locale.getLanguage();
        String country = locale.getCountry();
        if (TextUtils.isEmpty(language)) {
            return "";
        }
        if (pq7.a(language, "iw")) {
            language = "he";
        }
        if (pq7.a(language, "in")) {
            language = "id";
        }
        if (pq7.a(language, "ji")) {
            language = "yi";
        }
        if (!TextUtils.isEmpty(country)) {
            hr7 hr7 = hr7.f1520a;
            Locale locale2 = Locale.US;
            pq7.b(locale2, "Locale.US");
            language = String.format(locale2, "%s-%s", Arrays.copyOf(new Object[]{language, country}, 2));
            pq7.b(language, "java.lang.String.format(locale, format, *args)");
        }
        pq7.b(language, "localeString");
        return language;
    }

    @DexIgnore
    public final void X0() {
        try {
            aq5 aq5 = this.U;
            if (aq5 != null) {
                registerReceiver(aq5, new IntentFilter("android.intent.action.PHONE_STATE"));
                W0();
                return;
            }
            pq7.n("mPhoneCallReceiver");
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b0;
            local.e(str, "registerTelephonyReceiver throw exception: " + e2.getMessage());
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f A[SYNTHETIC, Splitter:B:18:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object X1(java.lang.String r7, com.fossil.qn7<? super java.lang.Boolean> r8) {
        /*
            r6 = this;
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 1
            r4 = 0
            boolean r0 = r8 instanceof com.portfolio.platform.PortfolioApp.a0
            if (r0 == 0) goto L_0x005c
            r0 = r8
            com.portfolio.platform.PortfolioApp$a0 r0 = (com.portfolio.platform.PortfolioApp.a0) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x005c
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x006b
            if (r3 != r5) goto L_0x0063
            boolean r3 = r1.Z$0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.PortfolioApp r1 = (com.portfolio.platform.PortfolioApp) r1
            com.fossil.el7.b(r2)
            r7 = r0
        L_0x002f:
            r0 = r2
            com.misfit.frameworks.buttonservice.model.UserProfile r0 = (com.misfit.frameworks.buttonservice.model.UserProfile) r0
            if (r0 != 0) goto L_0x007f
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "Error inside "
            r2.append(r4)
            java.lang.String r4 = com.portfolio.platform.PortfolioApp.b0
            r2.append(r4)
            java.lang.String r4 = ".switchActiveDevice - user is null"
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            r0.e(r1, r2)
            java.lang.Boolean r0 = com.fossil.ao7.a(r3)
        L_0x005b:
            return r0
        L_0x005c:
            com.portfolio.platform.PortfolioApp$a0 r0 = new com.portfolio.platform.PortfolioApp$a0
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0015
        L_0x0063:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x006b:
            com.fossil.el7.b(r2)
            r1.L$0 = r6
            r1.L$1 = r7
            r1.Z$0 = r4
            r1.label = r5
            java.lang.Object r2 = r6.S(r1)
            if (r2 == r0) goto L_0x005b
            r3 = r4
            r1 = r6
            goto L_0x002f
        L_0x007f:
            com.misfit.frameworks.buttonservice.IButtonConnectivity r2 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x00a2 }
            if (r2 == 0) goto L_0x009d
            boolean r0 = r2.switchActiveDevice(r7, r0)     // Catch:{ Exception -> 0x00a2 }
            if (r0 == 0) goto L_0x0095
            int r2 = r7.length()     // Catch:{ Exception -> 0x00a2 }
            if (r2 != 0) goto L_0x009b
            r2 = r5
        L_0x0090:
            if (r2 == 0) goto L_0x0095
            r1.J0(r7)     // Catch:{ Exception -> 0x00a2 }
        L_0x0095:
            r4 = r0
        L_0x0096:
            java.lang.Boolean r0 = com.fossil.ao7.a(r4)
            goto L_0x005b
        L_0x009b:
            r2 = r4
            goto L_0x0090
        L_0x009d:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        L_0x00a2:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0096
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.X1(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final String Y() {
        Resources resources = getResources();
        pq7.b(resources, "resources");
        String languageTag = pm0.a(resources.getConfiguration()).c(0).toLanguageTag();
        pq7.b(languageTag, "ConfigurationCompat.getL\u2026ation)[0].toLanguageTag()");
        return languageTag;
    }

    @DexIgnore
    public final void Y0(int i2) {
    }

    @DexIgnore
    public final boolean Y1(String str, boolean z2, int i2) {
        pq7.c(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, ".switchDeviceResponse(), isSuccess=" + z2 + ", failureCode=" + i2);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.switchDeviceResponse(str, z2, i2);
                return true;
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".pairDeviceResponse() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final ApiServiceV2 Z() {
        ApiServiceV2 apiServiceV2 = this.j;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        pq7.n("mApiService");
        throw null;
    }

    @DexIgnore
    public final void Z0(String str) {
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, ".removeActivePreferenceSerial - serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.removeActiveSerial(str);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "removeActivePreferenceSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    public final Object Z1(ob7 ob7, boolean z2, qn7<? super cl7<byte[], Integer>> qn7) {
        lu7 lu7 = new lu7(xn7.c(qn7), 1);
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new b0(lu7, null, this, z2, ob7), 3, null);
        Object t2 = lu7.t();
        if (t2 == yn7.d()) {
            go7.c(qn7);
        }
        return t2;
    }

    @DexIgnore
    public final ic7 a0() {
        ic7 ic7 = this.y;
        if (ic7 != null) {
            return ic7;
        }
        pq7.n("mDaggerAwareWorkerFactory");
        throw null;
    }

    @DexIgnore
    public final void a1(String str) {
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, ".removePairedPreferenceSerial - serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.removePairedSerial(str);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "removePairedPreferenceSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.rt0
    public void attachBaseContext(Context context) {
        pq7.c(context, "base");
        super.attachBaseContext(context);
        qt0.l(this);
    }

    @DexIgnore
    public final vm5 b0() {
        return this.b;
    }

    @DexIgnore
    public final void b1() {
        bk5 bk5 = this.g;
        if (bk5 != null) {
            bk5.a(this);
            bk5 bk52 = this.g;
            if (bk52 != null) {
                bk52.g(this);
            } else {
                pq7.n("mAlarmHelper");
                throw null;
            }
        } else {
            pq7.n("mAlarmHelper");
            throw null;
        }
    }

    @DexIgnore
    public final void b2(String str) {
        pq7.c(str, "serial");
        if (!TextUtils.isEmpty(str)) {
            try {
                if (vt7.j(str, J(), true)) {
                    on5 on5 = this.d;
                    if (on5 != null) {
                        on5.E0("");
                        this.H.l("");
                    } else {
                        pq7.n("sharedPreferencesManager");
                        throw null;
                    }
                }
                IButtonConnectivity iButtonConnectivity = g0;
                if (iButtonConnectivity != null) {
                    iButtonConnectivity.deviceUnlink(str);
                    on5 on52 = this.d;
                    if (on52 != null) {
                        on52.Z0(str, 0, false);
                    } else {
                        pq7.n("sharedPreferencesManager");
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = b0;
                local.e(str2, "Inside " + b0 + ".unlinkDevice - serial=" + str + ", ex=" + e2);
            }
        }
    }

    @DexIgnore
    public final String c0() {
        try {
            int myPid = Process.myPid();
            Object systemService = getSystemService(Constants.ACTIVITY);
            if (systemService != null) {
                for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : ((ActivityManager) systemService).getRunningAppProcesses()) {
                    if (runningAppProcessInfo.pid == myPid) {
                        return runningAppProcessInfo.processName;
                    }
                }
                return null;
            }
            throw new il7("null cannot be cast to non-null type android.app.ActivityManager");
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d(b0, "exception when get process name");
        }
    }

    @DexIgnore
    public final void c1() {
        String J2 = J();
        try {
            FLogger.INSTANCE.getLocal().d(b0, "Inside resetDeviceSettingToDefault");
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.resetDeviceSettingToDefault(J2);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b0;
            local.e(str, "Error inside " + b0 + ".resetDeviceSettingToDefault - e=" + e2);
        }
    }

    @DexIgnore
    public final void c2() {
        FLogger.INSTANCE.getLocal().d(b0, "unregisterContactObserver");
        try {
            l37 l37 = this.k;
            if (l37 != null) {
                l37.j();
                ContentResolver contentResolver = getContentResolver();
                l37 l372 = this.k;
                if (l372 != null) {
                    contentResolver.unregisterContentObserver(l372);
                    on5 on5 = this.d;
                    if (on5 != null) {
                        on5.r1(false);
                    } else {
                        pq7.n("sharedPreferencesManager");
                        throw null;
                    }
                } else {
                    pq7.n("mContactObserver");
                    throw null;
                }
            } else {
                pq7.n("mContactObserver");
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b0;
            local.d(str, "unregisterContactObserver e=" + e2);
        }
    }

    @DexIgnore
    public final ThemeRepository d0() {
        ThemeRepository themeRepository = this.W;
        if (themeRepository != null) {
            return themeRepository;
        }
        pq7.n("mThemeRepository");
        throw null;
    }

    @DexIgnore
    public final Object d1(qn7<? super tl7> qn7) {
        Object g2 = eu7.g(bw7.b(), new x(this, null), qn7);
        return g2 == yn7.d() ? g2 : tl7.f3441a;
    }

    @DexIgnore
    public final void d2() {
        try {
            aq5 aq5 = this.U;
            if (aq5 != null) {
                unregisterReceiver(aq5);
                dq5 dq5 = this.V;
                if (dq5 != null) {
                    unregisterReceiver(dq5);
                } else {
                    pq7.n("mMessageReceiver");
                    throw null;
                }
            } else {
                pq7.n("mPhoneCallReceiver");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b0;
            local.e(str, "unregisterTelephonyReceiver throw exception: " + e2.getMessage());
        }
    }

    @DexIgnore
    public final UserRepository e0() {
        UserRepository userRepository = this.B;
        if (userRepository != null) {
            return userRepository;
        }
        pq7.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final boolean e1(String str, String str2) {
        pq7.c(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local.d(str3, ".sendCurrentSecretKeyToDevice(), secretKey=" + str2);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendCurrentSecretKey(str, str2);
                return true;
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, ".sendCurrentSecretKeyToDevice() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final xw7 e2() {
        return gu7.d(jv7.a(bw7.b()), null, null, new c0(null), 3, null);
    }

    @DexIgnore
    public final WatchLocalizationRepository f0() {
        WatchLocalizationRepository watchLocalizationRepository = this.z;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        pq7.n("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public final long f1(String str, CustomRequest customRequest) {
        pq7.c(str, "serial");
        pq7.c(customRequest, Constants.COMMAND);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "sendCustomCommand() - serial=" + str + ", command=" + customRequest);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendCustomCommand(str, customRequest);
                return time_stamp_for_non_executable_method;
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void f2(vl5 vl5) {
        ul5 ul5;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.d(str, "Active view tracer old: " + this.R + " \n new: " + vl5);
        if (vl5 != null && pq7.a(vl5.e(), "view_appearance")) {
            vl5 vl52 = this.R;
            if (!(vl52 == null || vl52 == null || vl52.k(vl5) || (ul5 = this.Q) == null)) {
                sl5 b2 = ck5.f.b("app_appearance_view_navigate");
                vl5 vl53 = this.R;
                if (vl53 != null) {
                    String j2 = vl53.j();
                    if (j2 != null) {
                        b2.a("prev_view", j2);
                        String j3 = vl5.j();
                        if (j3 != null) {
                            b2.a("next_view", j3);
                            ul5.a(b2);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            this.R = vl5;
        }
    }

    @DexIgnore
    public final fs5 g0() {
        fs5 fs5 = this.G;
        if (fs5 != null) {
            return fs5;
        }
        pq7.n("mWorkoutGpsManager");
        throw null;
    }

    @DexIgnore
    public final void g1(DeviceAppResponse deviceAppResponse, String str) {
        pq7.c(deviceAppResponse, "deviceAppResponse");
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "sendComplicationAppInfo - deviceAppResponse=" + deviceAppResponse);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendDeviceAppResponse(deviceAppResponse, str);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0032 A[Catch:{ Exception -> 0x005b }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g2(com.fossil.qn7<? super com.fossil.tl7> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.PortfolioApp.d0
            if (r0 == 0) goto L_0x0038
            r0 = r6
            com.portfolio.platform.PortfolioApp$d0 r0 = (com.portfolio.platform.PortfolioApp.d0) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0046
            if (r3 != r4) goto L_0x003e
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0027:
            com.misfit.frameworks.buttonservice.log.model.AppLogInfo r0 = (com.misfit.frameworks.buttonservice.log.model.AppLogInfo) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            r1.updateAppLogInfo(r0)
            com.misfit.frameworks.buttonservice.IButtonConnectivity r1 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x005b }
            if (r1 == 0) goto L_0x0035
            r1.updateAppLogInfo(r0)     // Catch:{ Exception -> 0x005b }
        L_0x0035:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0037:
            return r0
        L_0x0038:
            com.portfolio.platform.PortfolioApp$d0 r0 = new com.portfolio.platform.PortfolioApp$d0
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x003e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0046:
            com.fossil.el7.b(r1)
            com.fossil.dk5$a r1 = com.fossil.dk5.g
            com.fossil.dk5 r1 = r1.c()
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.e(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0037
        L_0x005b:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = ".updateAppLogInfo(), error="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.e(r2, r0)
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.g2(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final String h0(Uri uri) {
        if (pq7.a(uri.getScheme(), "content")) {
            return getContentResolver().getType(uri);
        }
        String path = uri.getPath();
        if (path != null) {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(path)).toString()));
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void h1(String str, NotificationBaseObj notificationBaseObj) {
        pq7.c(str, "serial");
        pq7.c(notificationBaseObj, "notification");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "sendNotificationToDevice packageName " + notificationBaseObj);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSendNotification(str, notificationBaseObj);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".sendDianaNotification() - e=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object h2(com.fossil.qn7<? super com.fossil.tl7> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.PortfolioApp.e0
            if (r0 == 0) goto L_0x0062
            r0 = r6
            com.portfolio.platform.PortfolioApp$e0 r0 = (com.portfolio.platform.PortfolioApp.e0) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0062
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0070
            if (r3 != r4) goto L_0x0068
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x0083
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "updateCrashlyticsUserInformation currentUser="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            com.fossil.v74 r1 = com.fossil.v74.a()
            java.lang.String r2 = r0.getUserId()
            r1.d(r2)
            com.fossil.v74 r1 = com.fossil.v74.a()
            java.lang.String r2 = "UserId"
            java.lang.String r0 = r0.getUserId()
            r1.c(r2, r0)
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0061:
            return r0
        L_0x0062:
            com.portfolio.platform.PortfolioApp$e0 r0 = new com.portfolio.platform.PortfolioApp$e0
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0068:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0070:
            com.fossil.el7.b(r1)
            com.portfolio.platform.data.source.UserRepository r1 = r5.B
            if (r1 == 0) goto L_0x0086
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.getCurrentUser(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0061
        L_0x0083:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            goto L_0x0061
        L_0x0086:
            java.lang.String r0 = "mUserRepository"
            com.fossil.pq7.n(r0)
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.h2(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object i0(com.fossil.qn7<? super char[]> r10) {
        /*
        // Method dump skipped, instructions count: 237
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.i0(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void i1(String str, DeviceAppResponse deviceAppResponse) {
        pq7.c(str, "serial");
        pq7.c(deviceAppResponse, "deviceAppResponse");
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendMicroAppRemoteActivity(str, deviceAppResponse);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, "Error inside " + b0 + ".sendMicroAppRemoteActivity - e=" + e2);
        }
    }

    @DexIgnore
    public final void i2(Installation installation) {
        installation.setLocaleIdentifier(X());
    }

    @DexIgnore
    public final /* synthetic */ Object j0(qn7<? super ReplyMessageMappingGroup> qn7) {
        QuickResponseRepository quickResponseRepository = this.D;
        if (quickResponseRepository != null) {
            List<QuickResponseMessage> allQuickResponse = quickResponseRepository.getAllQuickResponse();
            ArrayList<QuickResponseMessage> arrayList = new ArrayList();
            for (T t2 : allQuickResponse) {
                if (!ao7.a(t2.getResponse().length() == 0).booleanValue()) {
                    arrayList.add(t2);
                }
            }
            ArrayList arrayList2 = new ArrayList(im7.m(arrayList, 10));
            for (QuickResponseMessage quickResponseMessage : arrayList) {
                arrayList2.add(new ReplyMessageMapping(String.valueOf(quickResponseMessage.getId()), quickResponseMessage.getResponse()));
            }
            return new ReplyMessageMappingGroup(pm7.j0(arrayList2), "icMessage.icon");
        }
        pq7.n("mQuickResponseRepository");
        throw null;
    }

    @DexIgnore
    public final void j1(MusicResponse musicResponse, String str) {
        pq7.c(musicResponse, "musicAppResponse");
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "sendMusicAppResponse - musicAppResponse=" + musicResponse);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendMusicAppResponse(musicResponse, str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object j2(boolean r11, com.fossil.qn7<? super com.fossil.tl7> r12) {
        /*
        // Method dump skipped, instructions count: 203
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.j2(boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final on5 k0() {
        on5 on5 = this.d;
        if (on5 != null) {
            return on5;
        }
        pq7.n("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final void k1(String str, String str2, int i2) {
        pq7.c(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local.d(str3, ".sendRandomKeyToDevice(), randomKey=" + str2);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendRandomKey(str, str2, i2);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, ".sendRandomKeyToDevice() - e=" + e2);
        }
    }

    @DexIgnore
    public final long k2(String str) {
        pq7.c(str, "serial");
        FLogger.INSTANCE.getLocal().d(b0, "verifySecretKey()");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.verifySecretKeySession(str);
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, ".verifySecretKey(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final String l0() {
        on5 on5 = this.d;
        if (on5 != null) {
            String N2 = on5.N();
            return N2 != null ? N2 : "";
        }
        pq7.n("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final boolean l1(String str, String str2, int i2) {
        pq7.c(str, "serial");
        pq7.c(str2, "serverSecretKey");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local.d(str3, ".sendServerSecretKeyToDevice(), serverSecretKey=" + str2);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendServerSecretKey(str, str2, i2);
                return true;
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, ".sendServerSecretKeyToDevice() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r2v1 int), ('.' char), (r1v6 int)] */
    public final String m0() {
        MisfitDeviceProfile m2;
        ry1 uiPackageOSVersion;
        if (!(!vt7.l(J())) || (m2 = nk5.o.j().m(J())) == null || (uiPackageOSVersion = m2.getUiPackageOSVersion()) == null) {
            return null;
        }
        int major = uiPackageOSVersion.getMajor();
        int minor = uiPackageOSVersion.getMinor();
        if (major == 0 && minor == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(major);
        sb.append('.');
        sb.append(minor);
        return sb.toString();
    }

    @DexIgnore
    public final long m1(String str) {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        if (str == null) {
            FLogger.INSTANCE.getLocal().e(b0, "sendingEncryptedDataSession - encryptedData is null");
            on5 on5 = this.d;
            if (on5 != null) {
                on5.m0(Boolean.TRUE);
                return time_stamp_for_non_executable_method;
            }
            pq7.n("sharedPreferencesManager");
            throw null;
        }
        try {
            byte[] decode = Base64.decode(str, 0);
            on5 on52 = this.d;
            if (on52 != null) {
                Boolean b2 = on52.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = b0;
                local.e(str2, "sendingEncryptedDataSession - encryptedData: " + str + " - latestBCStatus: " + b2);
                IButtonConnectivity iButtonConnectivity = g0;
                if (iButtonConnectivity != null) {
                    String J2 = J();
                    pq7.b(b2, "latestBCStatus");
                    return iButtonConnectivity.sendingEncryptedDataSession(decode, J2, b2.booleanValue());
                }
                pq7.i();
                throw null;
            }
            pq7.n("sharedPreferencesManager");
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".sendingEncryptedDataSession - e=" + e2);
            e2.printStackTrace();
            on5 on53 = this.d;
            if (on53 != null) {
                on53.m0(Boolean.TRUE);
                return time_stamp_for_non_executable_method;
            }
            pq7.n("sharedPreferencesManager");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object n0(com.fossil.qn7<? super java.util.Date> r7) {
        /*
            r6 = this;
            r5 = 0
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 1
            boolean r0 = r7 instanceof com.portfolio.platform.PortfolioApp.m
            if (r0 == 0) goto L_0x0046
            r0 = r7
            com.portfolio.platform.PortfolioApp$m r0 = (com.portfolio.platform.PortfolioApp.m) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0046
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0054
            if (r3 != r4) goto L_0x004c
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0028:
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x006b
            java.lang.String r0 = r0.getCreatedAt()
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x006b
            if (r0 == 0) goto L_0x0067
            java.util.Date r0 = com.fossil.lk5.q0(r0)
            java.util.Date r0 = com.fossil.kl5.b(r0)
            java.lang.String r1 = "TimeHelper.getStartOfDay(registeredDate)"
            com.fossil.pq7.b(r0, r1)
        L_0x0045:
            return r0
        L_0x0046:
            com.portfolio.platform.PortfolioApp$m r0 = new com.portfolio.platform.PortfolioApp$m
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x004c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0054:
            com.fossil.el7.b(r1)
            com.portfolio.platform.data.source.UserRepository r1 = r6.B
            if (r1 == 0) goto L_0x007e
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r1.getCurrentUser(r0)
            if (r0 != r2) goto L_0x0028
            r0 = r2
            goto L_0x0045
        L_0x0067:
            com.fossil.pq7.i()
            throw r5
        L_0x006b:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.PortfolioApp.b0
            java.lang.String r2 = "Fail to getCurrentUserRegisteringDate, return new Date(1, 1, 1)"
            r0.d(r1, r2)
            java.util.Date r0 = new java.util.Date
            r0.<init>(r4, r4, r4)
            goto L_0x0045
        L_0x007e:
            java.lang.String r0 = "mUserRepository"
            com.fossil.pq7.n(r0)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.n0(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void n1(String str, String str2) {
        pq7.c(str, "serial");
        if (TextUtils.isEmpty(str) || nk5.o.v(str)) {
            String J2 = J();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local.d(str3, "Inside " + b0 + ".setActiveDeviceSerial - current=" + J2 + ", new=" + str + ", newDevice mac address=" + str2);
            if (str2 == null) {
                str2 = "";
            }
            on5 on5 = this.d;
            if (on5 != null) {
                on5.E0(str);
                if (J2 != str) {
                    this.H.l(str);
                }
                e2();
                try {
                    IButtonConnectivity iButtonConnectivity = g0;
                    if (iButtonConnectivity != null) {
                        iButtonConnectivity.setActiveSerial(str, str2);
                        eo5.c.d(this);
                        return;
                    }
                    pq7.i();
                    throw null;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                pq7.n("sharedPreferencesManager");
                throw null;
            }
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.d(str4, "Ignore legacy device serial=" + str);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object o0(com.fossil.qn7<? super com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.PortfolioApp.n
            if (r0 == 0) goto L_0x002e
            r0 = r7
            com.portfolio.platform.PortfolioApp$n r0 = (com.portfolio.platform.PortfolioApp.n) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002e
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x003c
            if (r3 != r5) goto L_0x0034
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0027:
            java.util.List r0 = (java.util.List) r0
            com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting r0 = com.fossil.r47.a(r0)
        L_0x002d:
            return r0
        L_0x002e:
            com.portfolio.platform.PortfolioApp$n r0 = new com.portfolio.platform.PortfolioApp$n
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x0034:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003c:
            com.fossil.el7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = com.portfolio.platform.PortfolioApp.b0
            java.lang.String r4 = "getWorkoutDetectionSetting()"
            r1.d(r3, r4)
            com.portfolio.platform.data.source.WorkoutSettingRepository r1 = r6.E
            if (r1 == 0) goto L_0x005c
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r0 = r1.getWorkoutSettingList(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x002d
        L_0x005c:
            java.lang.String r0 = "mWorkoutSettingRepository"
            com.fossil.pq7.n(r0)
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.o0(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final long o1(String str, List<? extends Alarm> list) {
        pq7.c(str, "serial");
        pq7.c(list, "alarms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setMultipleAlarms - alarms size=" + list.size());
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetListAlarm(str, list);
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "setMultipleAlarms - ex=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public void onActivityCreated(Activity activity, Bundle bundle) {
        pq7.c(activity, Constants.ACTIVITY);
        b bVar = b.CREATE;
    }

    @DexIgnore
    public void onActivityDestroyed(Activity activity) {
        pq7.c(activity, Constants.ACTIVITY);
        b bVar = b.DESTROY;
    }

    @DexIgnore
    public void onActivityPaused(Activity activity) {
        pq7.c(activity, Constants.ACTIVITY);
        b bVar = b.PAUSE;
        if (e0) {
            br5 br5 = this.w;
            if (br5 != null) {
                br5.w();
            } else {
                pq7.n("mShakeFeedbackService");
                throw null;
            }
        }
        this.J = true;
        Runnable runnable = this.L;
        if (runnable != null) {
            this.K.removeCallbacks(runnable);
        }
        this.K.postDelayed(new q(this), 500);
    }

    @DexIgnore
    public void onActivityResumed(Activity activity) {
        pq7.c(activity, Constants.ACTIVITY);
        if (e0) {
            br5 br5 = this.w;
            if (br5 != null) {
                br5.n(activity);
            } else {
                pq7.n("mShakeFeedbackService");
                throw null;
            }
        }
        b bVar = b.RESUME;
        this.J = false;
        boolean z2 = this.I;
        this.I = true;
        Runnable runnable = this.L;
        if (runnable != null) {
            this.K.removeCallbacks(runnable);
        }
        if (!z2) {
            c0 = true;
            FLogger.INSTANCE.getLocal().d(b0, "from background");
            ul5 ul5 = this.Q;
            if (ul5 != null) {
                ul5.i();
            }
        } else {
            c0 = false;
            FLogger.INSTANCE.getLocal().d(b0, "still foreground");
        }
        if (this.O) {
            F(this.P);
        }
    }

    @DexIgnore
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        pq7.c(activity, Constants.ACTIVITY);
    }

    @DexIgnore
    public void onActivityStarted(Activity activity) {
        pq7.c(activity, Constants.ACTIVITY);
        b bVar = b.START;
    }

    @DexIgnore
    public void onActivityStopped(Activity activity) {
        pq7.c(activity, Constants.ACTIVITY);
        b bVar = b.STOP;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        d0 = this;
        e0 = (getApplicationContext().getApplicationInfo().flags & 2) != 0;
        if (x0()) {
            uo4 uo4 = new uo4(this);
            kq4.z O3 = kq4.O3();
            O3.a(uo4);
            ro4 b2 = O3.b();
            pq7.b(b2, "DaggerApplicationCompone\u2026\n                .build()");
            this.M = b2;
            if (b2 != null) {
                b2.H0(this);
                yr4 yr4 = this.Z;
                if (yr4 != null) {
                    yr4.u(this);
                    System.loadLibrary("FitnessAlgorithm");
                    LifecycleOwner h2 = ns0.h();
                    pq7.b(h2, "ProcessLifecycleOwner.get()");
                    Lifecycle lifecycle = h2.getLifecycle();
                    ApplicationEventListener applicationEventListener = this.t;
                    if (applicationEventListener != null) {
                        lifecycle.a(applicationEventListener);
                        MicroAppEventLogger.initialize(this);
                        if (!z0()) {
                            Stetho.initializeWithDefaults(this);
                        }
                        xw7 unused = gu7.d(this.a0, null, null, new r(this, null), 3, null);
                        FLogger.INSTANCE.getLocal().d(b0, "On app create");
                        f0 = new cj5(uc7.f3568a);
                        MutableLiveData<String> mutableLiveData = this.H;
                        on5 on5 = this.d;
                        if (on5 != null) {
                            mutableLiveData.o(on5.g());
                            registerActivityLifecycleCallbacks(this);
                            String str = "4.6.0";
                            if (e0) {
                                Object[] array = new jt7(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).split("4.6.0", 2).toArray(new String[0]);
                                if (array != null) {
                                    str = ((String[]) array)[0];
                                } else {
                                    throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                                }
                            }
                            GuestApiService guestApiService = this.h;
                            if (guestApiService != null) {
                                this.b = new vm5(this, str, guestApiService);
                                this.c = new up5();
                                mq4.a();
                                registerReceiver(this.b, new IntentFilter("android.intent.action.LOCALE_CHANGED"));
                                U0();
                                vm5 vm5 = this.b;
                                if (vm5 != null) {
                                    registerActivityLifecycleCallbacks(vm5.g());
                                    vm5 vm52 = this.b;
                                    if (vm52 != null) {
                                        String simpleName = SplashScreenActivity.class.getSimpleName();
                                        pq7.b(simpleName, "SplashScreenActivity::class.java.simpleName");
                                        vm52.t(simpleName);
                                        vm5 vm53 = this.b;
                                        if (vm53 != null) {
                                            vm53.q();
                                            ResolutionHelper.INSTANCE.initDeviceDensity(this);
                                            try {
                                                if (Build.VERSION.SDK_INT >= 24) {
                                                    NetworkChangedReceiver networkChangedReceiver = new NetworkChangedReceiver();
                                                    this.S = networkChangedReceiver;
                                                    registerReceiver(networkChangedReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                                                }
                                            } catch (Exception e2) {
                                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                                String str2 = b0;
                                                local.e(str2, "onCreate - jobScheduler - ex=" + e2);
                                            }
                                            IntentFilter intentFilter = new IntentFilter();
                                            intentFilter.addAction("android.intent.action.TIME_TICK");
                                            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
                                            registerReceiver(this.T, intentFilter);
                                            X0();
                                            this.N = Thread.getDefaultUncaughtExceptionHandler();
                                            Thread.setDefaultUncaughtExceptionHandler(new s(this));
                                            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                                            StrictMode.setVmPolicy(builder.build());
                                            builder.detectFileUriExposure();
                                            bk5 bk5 = this.g;
                                            if (bk5 != null) {
                                                bk5.f(this);
                                                bk5 bk52 = this.g;
                                                if (bk52 != null) {
                                                    bk52.g(this);
                                                    this.Q = ck5.f.c("app_appearance");
                                                    ck5 ck5 = this.s;
                                                    if (ck5 != null) {
                                                        ck5.q("diana_sdk_version", ButtonService.Companion.getSdkVersionV2());
                                                        if (!TextUtils.isEmpty(J())) {
                                                            FLogger.INSTANCE.getLocal().d(b0, "Service Tracking - startForegroundService in PortfolioApp onCreate");
                                                            i47.a.e(i47.f1583a, this, MFDeviceService.class, null, 4, null);
                                                            i47.a.e(i47.f1583a, this, ButtonService.class, null, 4, null);
                                                        }
                                                        FlutterEngine flutterEngine = new FlutterEngine(this);
                                                        flutterEngine.getDartExecutor().executeDartEntrypoint(new DartExecutor.DartEntrypoint(FlutterMain.findAppBundlePath(), Constants.MAP));
                                                        FlutterEngineCache.getInstance().put("map_engine_id", flutterEngine);
                                                        FlutterEngine flutterEngine2 = new FlutterEngine(this);
                                                        flutterEngine2.getDartExecutor().executeDartEntrypoint(new DartExecutor.DartEntrypoint(FlutterMain.findAppBundlePath(), "screenShotMap"));
                                                        FlutterEngineCache.getInstance().put("screenshot_engine_id", flutterEngine2);
                                                        return;
                                                    }
                                                    pq7.n("mAnalyticsHelper");
                                                    throw null;
                                                }
                                                pq7.n("mAlarmHelper");
                                                throw null;
                                            }
                                            pq7.n("mAlarmHelper");
                                            throw null;
                                        }
                                        pq7.i();
                                        throw null;
                                    }
                                    pq7.i();
                                    throw null;
                                }
                                pq7.i();
                                throw null;
                            }
                            pq7.n("mGuestApiService");
                            throw null;
                        }
                        pq7.n("sharedPreferencesManager");
                        throw null;
                    }
                    pq7.n("mAppEventManager");
                    throw null;
                }
                pq7.n("bcAnalytic");
                throw null;
            }
            pq7.n("applicationComponent");
            throw null;
        }
    }

    @DexIgnore
    public void onLowMemory() {
        super.onLowMemory();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.d(str, "Inside " + b0 + ".onLowMemory");
        System.runFinalization();
    }

    @DexIgnore
    public void onTerminate() {
        super.onTerminate();
        FLogger.INSTANCE.getLocal().d(b0, "---Inside .onTerminate of Application");
        d2();
        unregisterReceiver(this.b);
        c2();
        vm5 vm5 = this.b;
        unregisterActivityLifecycleCallbacks(vm5 != null ? vm5.g() : null);
        if (Build.VERSION.SDK_INT >= 24) {
            FLogger.INSTANCE.getLocal().d(b0, "unregister NetworkChangedReceiver");
            unregisterReceiver(this.S);
        }
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.d(str, "Inside " + b0 + ".onTrimMemory");
        super.onTrimMemory(i2);
        System.runFinalization();
    }

    @DexIgnore
    public final void p(CommunicateMode communicateMode, String str, String str2) {
        pq7.c(communicateMode, "communicateMode");
        pq7.c(str, "serial");
        pq7.c(str2, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b0;
        local.d(str3, "addLog - communicateMode=" + communicateMode + ", serial=" + str + ", message=" + str2);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.addLog(communicateMode.ordinal(), str, str2);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, "addLog - ex=" + e2);
        }
    }

    @DexIgnore
    public final boolean p0() {
        Object systemService = getSystemService("connectivity");
        if (systemService != null) {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) systemService).getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
        }
        throw new il7("null cannot be cast to non-null type android.net.ConnectivityManager");
    }

    @DexIgnore
    public final void p1(boolean z2) {
        this.I = z2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object q(com.fossil.ob7 r8, com.fossil.qn7<? super com.fossil.tl7> r9) {
        /*
            r7 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.PortfolioApp.c
            if (r0 == 0) goto L_0x0061
            r0 = r9
            com.portfolio.platform.PortfolioApp$c r0 = (com.portfolio.platform.PortfolioApp.c) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0061
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0070
            if (r3 != r4) goto L_0x0068
            java.lang.Object r0 = r1.L$1
            com.fossil.ob7 r0 = (com.fossil.ob7) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.PortfolioApp r1 = (com.portfolio.platform.PortfolioApp) r1
            com.fossil.el7.b(r2)
        L_0x002b:
            com.misfit.frameworks.buttonservice.model.watchface.ThemeData r2 = com.fossil.cc7.h(r0)
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "apply with wfThemeData "
            r5.append(r6)
            r5.append(r0)
            java.lang.String r0 = " buttonThemeData "
            r5.append(r0)
            r5.append(r2)
            java.lang.String r0 = r5.toString()
            r3.d(r4, r0)
            com.misfit.frameworks.buttonservice.IButtonConnectivity r0 = com.portfolio.platform.PortfolioApp.g0
            if (r0 == 0) goto L_0x0082
            java.lang.String r1 = r1.J()
            r0.applyTheme(r1, r2)
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0060:
            return r0
        L_0x0061:
            com.portfolio.platform.PortfolioApp$c r0 = new com.portfolio.platform.PortfolioApp$c
            r0.<init>(r7, r9)
            r1 = r0
            goto L_0x0014
        L_0x0068:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0070:
            com.fossil.el7.b(r2)
            r1.L$0 = r7
            r1.L$1 = r8
            r1.label = r4
            java.lang.Object r1 = r7.J1(r1)
            if (r1 == r0) goto L_0x0060
            r0 = r8
            r1 = r7
            goto L_0x002b
        L_0x0082:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.q(com.fossil.ob7, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final boolean q0() {
        fs5 fs5 = this.G;
        if (fs5 != null) {
            return fs5.l();
        }
        pq7.n("mWorkoutGpsManager");
        throw null;
    }

    @DexIgnore
    public final void q1(List<? extends Alarm> list) {
        pq7.c(list, "alarms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.e(str, "setAutoAlarms - alarms=" + list);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSetAutoListAlarm(list);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local2.e(str2, "setAutoAlarms - e=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object r(java.lang.String r7, com.fossil.qn7<? super com.fossil.tl7> r8) {
        /*
            r6 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.PortfolioApp.d
            if (r0 == 0) goto L_0x0066
            r0 = r8
            com.portfolio.platform.PortfolioApp$d r0 = (com.portfolio.platform.PortfolioApp.d) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0066
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0075
            if (r3 != r4) goto L_0x006d
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.PortfolioApp r1 = (com.portfolio.platform.PortfolioApp) r1
            com.fossil.el7.b(r2)
        L_0x002b:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "applyByThemeFile "
            r4.append(r5)
            r4.append(r0)
            java.lang.String r5 = " isExist "
            r4.append(r5)
            java.io.File r5 = new java.io.File
            r5.<init>(r0)
            boolean r5 = r5.exists()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.e(r3, r4)
            com.misfit.frameworks.buttonservice.IButtonConnectivity r2 = com.portfolio.platform.PortfolioApp.g0
            if (r2 == 0) goto L_0x0063
            java.lang.String r1 = r1.J()
            r2.applyThemeByFile(r1, r0)
        L_0x0063:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0065:
            return r0
        L_0x0066:
            com.portfolio.platform.PortfolioApp$d r0 = new com.portfolio.platform.PortfolioApp$d
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0014
        L_0x006d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0075:
            com.fossil.el7.b(r2)
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r4
            java.lang.Object r1 = r6.J1(r1)
            if (r1 == r0) goto L_0x0065
            r1 = r6
            r0 = r7
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.r(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object r0(qn7<? super tl7> qn7) {
        FLogger.INSTANCE.getLocal().d(b0, "initCrashlytics");
        v74.a().c("SDK Version V2", ButtonService.Companion.getSdkVersionV2());
        v74.a().c("Locale", Locale.getDefault().toString());
        Object h2 = h2(qn7);
        return h2 == yn7.d() ? h2 : tl7.f3441a;
    }

    @DexIgnore
    public final void r1(String str, List<? extends BLEMapping> list) {
        pq7.c(str, "serial");
        pq7.c(list, "mappings");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setAutoMapping serial=" + str + "mappingsSize=" + list.size());
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoMapping(str, list);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void s(String str) {
        pq7.c(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.cancelPairDevice(str);
                tl7 tl7 = tl7.f3441a;
                return;
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, ".cancelPairDevice() - e=" + e2);
            tl7 tl72 = tl7.f3441a;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object s0(com.fossil.qn7<? super com.fossil.tl7> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.PortfolioApp.o
            if (r0 == 0) goto L_0x0065
            r0 = r6
            com.portfolio.platform.PortfolioApp$o r0 = (com.portfolio.platform.PortfolioApp.o) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0065
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0073
            if (r3 != r4) goto L_0x006b
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.el7.b(r1)
            r5 = r0
        L_0x0027:
            r0 = r1
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x0062
            com.fossil.cx1 r1 = com.fossil.cx1.f
            r1.k(r5)
            com.fossil.cx1 r1 = com.fossil.cx1.f
            com.fossil.ft1 r2 = new com.fossil.ft1
            java.lang.String r3 = r0.getUserId()
            com.portfolio.platform.data.model.MFUser$Auth r4 = r0.getAuth()
            java.lang.String r4 = r4.getRefreshToken()
            r2.<init>(r3, r4)
            r1.o(r2)
            com.fossil.cx1 r1 = com.fossil.cx1.f
            com.fossil.h37 r2 = com.fossil.h37.b
            java.lang.String r2 = r2.d()
            r1.l(r2)
            com.fossil.cx1 r1 = com.fossil.cx1.f
            java.lang.String r2 = "theme/fonts"
            r1.m(r2)
            com.fossil.cx1 r1 = com.fossil.cx1.f
            java.lang.String r0 = r0.getUserId()
            r1.p(r0)
        L_0x0062:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0064:
            return r0
        L_0x0065:
            com.portfolio.platform.PortfolioApp$o r0 = new com.portfolio.platform.PortfolioApp$o
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x006b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0073:
            com.fossil.el7.b(r1)
            com.portfolio.platform.data.source.UserRepository r1 = r5.B
            if (r1 == 0) goto L_0x0086
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r1 = r1.getCurrentUser(r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0064
        L_0x0086:
            java.lang.String r0 = "mUserRepository"
            com.fossil.pq7.n(r0)
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.s0(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void s1(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        pq7.c(appNotificationFilterSettings, "notificationFilterSettings");
        pq7.c(str, "serial");
        Iterator<T> it = appNotificationFilterSettings.getNotificationFilters().iterator();
        while (it.hasNext()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.d(str2, "setAutoNotificationFilterSettings " + ((Object) it.next()));
        }
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoNotificationFilterSettings(appNotificationFilterSettings, str);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".setAutoNotificationFilterSettings(), e=" + e2);
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void t(CommunicateMode communicateMode, String str, CommunicateMode communicateMode2, String str2) {
        pq7.c(communicateMode, "curCommunicateMode");
        pq7.c(str, "curSerial");
        pq7.c(communicateMode2, "newCommunicateMode");
        pq7.c(str2, "newSerial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b0;
        local.d(str3, "changePendingLogKey - curCommunicateMode=" + communicateMode + ", curSerial=" + str + ", newCommunicateMode=" + communicateMode2 + ", newSerial=" + str2);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.changePendingLogKey(communicateMode.ordinal(), str, communicateMode2.ordinal(), str2);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, "changePendingLogKey - ex=" + e2);
        }
    }

    @DexIgnore
    public final void t0() {
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.interruptCurrentSession(J());
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b0;
            local.e(str, "Error while interruptCurrentSession - e=" + e2);
        }
    }

    @DexIgnore
    public final void t1(String str) {
        int j2;
        String str2;
        if (str == null) {
            j2 = 1024;
            str2 = "";
        } else {
            j2 = ll5.j(str);
            str2 = str;
        }
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local.d(str3, "Inside " + b0 + ".setAutoSecondTimezone - offsetMinutes=" + j2 + ", mSecondTimezoneId=" + str2);
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                if (str == null) {
                    str = "";
                }
                iButtonConnectivity.deviceSetAutoSecondTimezone(str);
                return;
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = b0;
            local2.e(str4, "Inside " + b0 + ".setAutoSecondTimezone - ex=" + e2);
        }
    }

    @DexIgnore
    public final void u() {
        on5 on5 = this.d;
        if (on5 != null) {
            on5.K0(53);
            File cacheDir = getCacheDir();
            pq7.b(cacheDir, "cacheDirectory");
            File file = new File(cacheDir.getParent());
            if (file.exists()) {
                String[] list = file.list();
                for (String str : list) {
                    if (!pq7.a(str, "lib")) {
                        z(new File(file, str));
                    }
                }
            }
            PendingIntent activity = PendingIntent.getActivity(this, 123456, new Intent(this, SplashScreenActivity.class), SQLiteDatabase.CREATE_IF_NECESSARY);
            Object systemService = getSystemService(Alarm.TABLE_NAME);
            if (systemService != null) {
                ((AlarmManager) systemService).set(1, System.currentTimeMillis() + ((long) 1000), activity);
                if (19 <= Build.VERSION.SDK_INT) {
                    Object systemService2 = getSystemService(Constants.ACTIVITY);
                    if (systemService2 != null) {
                        ((ActivityManager) systemService2).clearApplicationUserData();
                        return;
                    }
                    throw new il7("null cannot be cast to non-null type android.app.ActivityManager");
                }
                try {
                    Runtime.getRuntime().exec("pm clear " + getPackageName());
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                throw new il7("null cannot be cast to non-null type android.app.AlarmManager");
            }
        } else {
            pq7.n("sharedPreferencesManager");
            throw null;
        }
    }

    @DexIgnore
    public final boolean u0() {
        return this.I;
    }

    @DexIgnore
    public final void u1(UserProfile userProfile) {
        pq7.c(userProfile, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.d(str, "setAutoUserBiometricData - userProfile=" + userProfile);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoUserBiometricData(userProfile);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final /* synthetic */ Object v(qn7<? super tl7> qn7) {
        Object g2 = eu7.g(bw7.b(), new e(this, null), qn7);
        return g2 == yn7.d() ? g2 : tl7.f3441a;
    }

    @DexIgnore
    public final boolean v0() {
        Boolean bool;
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            int[] listActiveCommunicator = iButtonConnectivity != null ? iButtonConnectivity.getListActiveCommunicator() : null;
            List<Integer> c02 = listActiveCommunicator != null ? em7.c0(listActiveCommunicator) : null;
            if (c02 != null) {
                bool = Boolean.valueOf(!c02.isEmpty());
            } else {
                bool = null;
            }
            if (bool != null) {
                return bool.booleanValue() && c02.contains(Integer.valueOf(CommunicateMode.OTA.getValue()));
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    public final void v1(WatchAppMappingSettings watchAppMappingSettings, String str) {
        pq7.c(watchAppMappingSettings, "watchAppMappingSettings");
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setAutoWatchAppSettings - watchAppMappingSettings=" + watchAppMappingSettings);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoWatchAppSettings(watchAppMappingSettings, str);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void w(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.e(str, "confirmBcStatusToButtonService - bcStatus: " + z2);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.confirmBCStatus(z2);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0057, code lost:
        if (com.fossil.wt7.v(r0, "image", false, 2, null) == false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0067, code lost:
        if (com.fossil.wt7.v(r2, "image", false, 2, null) != false) goto L_0x0069;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean w0(android.content.Intent r10, android.net.Uri r11) {
        /*
            r9 = this;
            r8 = 2
            r1 = 0
            r7 = 0
            java.lang.String r0 = "fileUri"
            com.fossil.pq7.c(r11, r0)
            java.lang.String r2 = r9.h0(r11)
            if (r10 == 0) goto L_0x006b
            java.lang.String r0 = r10.getType()
        L_0x0012:
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "isImageFile intentMimeType="
            r5.append(r6)
            r5.append(r0)
            java.lang.String r6 = ", uriMimeType="
            r5.append(r6)
            r5.append(r2)
            java.lang.String r5 = r5.toString()
            r3.d(r4, r5)
            java.lang.String r3 = r11.getPath()
            if (r3 == 0) goto L_0x0076
            java.lang.String r4 = "fileUri.path!!"
            com.fossil.pq7.b(r3, r4)
            java.lang.String r4 = "pickerImage"
            boolean r3 = com.fossil.wt7.v(r3, r4, r1, r8, r7)
            if (r3 != 0) goto L_0x0069
            boolean r3 = android.text.TextUtils.isEmpty(r0)
            if (r3 != 0) goto L_0x0059
            if (r0 == 0) goto L_0x006e
            java.lang.String r3 = "image"
            boolean r0 = com.fossil.wt7.v(r0, r3, r1, r8, r7)
            if (r0 != 0) goto L_0x0069
        L_0x0059:
            boolean r0 = android.text.TextUtils.isEmpty(r2)
            if (r0 != 0) goto L_0x007a
            if (r2 == 0) goto L_0x0072
            java.lang.String r0 = "image"
            boolean r0 = com.fossil.wt7.v(r2, r0, r1, r8, r7)
            if (r0 == 0) goto L_0x007a
        L_0x0069:
            r0 = 1
        L_0x006a:
            return r0
        L_0x006b:
            java.lang.String r0 = ""
            goto L_0x0012
        L_0x006e:
            com.fossil.pq7.i()
            throw r7
        L_0x0072:
            com.fossil.pq7.i()
            throw r7
        L_0x0076:
            com.fossil.pq7.i()
            throw r7
        L_0x007a:
            r0 = r1
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.w0(android.content.Intent, android.net.Uri):boolean");
    }

    @DexIgnore
    public final long w1(String str, boolean z2) {
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setFrontLightEnable() - serial=" + str + ", isFrontLightEnable=" + z2);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setFrontLightEnable(str, z2);
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void x(String str, boolean z2) {
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "confirmStopWorkout -serial =" + str + ", stopWorkout=" + z2);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.confirmStopWorkout(str, z2);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, "confirmStopWorkout - serial " + str + " exception " + e2);
        }
    }

    @DexIgnore
    public final boolean x0() {
        return pq7.a(getPackageName(), c0());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object x1(java.lang.String r6, com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.PortfolioApp.y
            if (r0 == 0) goto L_0x0058
            r0 = r7
            com.portfolio.platform.PortfolioApp$y r0 = (com.portfolio.platform.PortfolioApp.y) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0058
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0067
            if (r3 != r4) goto L_0x005f
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.PortfolioApp r1 = (com.portfolio.platform.PortfolioApp) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.misfit.frameworks.buttonservice.model.UserProfile r0 = (com.misfit.frameworks.buttonservice.model.UserProfile) r0
            if (r0 == 0) goto L_0x009d
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "setImplicitDeviceConfig - currentUserProfile="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            com.misfit.frameworks.buttonservice.IButtonConnectivity r1 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x007c }
            if (r1 == 0) goto L_0x0077
            r1.setImplicitDeviceConfig(r0, r6)     // Catch:{ Exception -> 0x007c }
        L_0x0055:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0057:
            return r0
        L_0x0058:
            com.portfolio.platform.PortfolioApp$y r0 = new com.portfolio.platform.PortfolioApp$y
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x005f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0067:
            com.fossil.el7.b(r2)
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r5.S(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0057
        L_0x0077:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        L_0x007c:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.b0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = ".setImplicitDisplayUnitSettings(), e="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.e(r2, r3)
            r0.printStackTrace()
            goto L_0x0055
        L_0x009d:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.PortfolioApp.b0
            java.lang.String r2 = "setImplicitDeviceConfig - currentUserProfile is NULL"
            r0.e(r1, r2)
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.x1(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void y(String str) {
        pq7.c(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deleteDataFiles(str);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, "Error while deleting data file - e=" + e2);
        }
    }

    @DexIgnore
    public final boolean y0() {
        String string = Settings.Secure.getString(getContentResolver(), "enabled_notification_listeners");
        String str = getPackageName() + "/" + FossilNotificationListenerService.class.getCanonicalName();
        FLogger.INSTANCE.getLocal().d(b0, "isNotificationListenerEnabled() - notificationServicePath = " + str);
        if (!TextUtils.isEmpty(string)) {
            FLogger.INSTANCE.getLocal().d(b0, "isNotificationListenerEnabled() enabledNotificationListeners = " + string);
        }
        if (TextUtils.isEmpty(string)) {
            return false;
        }
        pq7.b(string, "enabledNotificationListeners");
        return wt7.v(string, str, false, 2, null);
    }

    @DexIgnore
    public final void y1(UserDisplayUnit userDisplayUnit, String str) {
        pq7.c(userDisplayUnit, "userDisplayUnit");
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b0;
        local.d(str2, "setImplicitDisplayUnitSettings - userDisplayUnit=" + userDisplayUnit);
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setImplicitDisplayUnitSettings(userDisplayUnit, str);
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b0;
            local2.e(str3, ".setImplicitDisplayUnitSettings(), e=" + e2);
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final boolean z(File file) {
        if (file == null) {
            return true;
        }
        if (!file.isDirectory()) {
            return file.delete();
        }
        String[] list = file.list();
        int length = list.length;
        boolean z2 = true;
        for (int i2 = 0; i2 < length; i2++) {
            z2 = z(new File(file, list[i2])) && z2;
        }
        return z2;
    }

    @DexIgnore
    public final boolean z0() {
        return vt7.j("release", "release", true);
    }

    @DexIgnore
    public final long z1(String str, InactiveNudgeData inactiveNudgeData) {
        pq7.c(str, "serial");
        pq7.c(inactiveNudgeData, "inactiveNudgeData");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = g0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetInactiveNudgeConfig(str, inactiveNudgeData);
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b0;
            local.e(str2, ".setInactiveNudgeConfig - e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }
}
