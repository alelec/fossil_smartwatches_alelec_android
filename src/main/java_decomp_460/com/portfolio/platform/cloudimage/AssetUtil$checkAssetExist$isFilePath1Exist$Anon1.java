package com.portfolio.platform.cloudimage;

import com.fossil.ao7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$isFilePath1Exist$1", f = "AssetUtil.kt", l = {}, m = "invokeSuspend")
public final class AssetUtil$checkAssetExist$isFilePath1Exist$Anon1 extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $filePath1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AssetUtil$checkAssetExist$isFilePath1Exist$Anon1(String str, qn7 qn7) {
        super(2, qn7);
        this.$filePath1 = str;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        AssetUtil$checkAssetExist$isFilePath1Exist$Anon1 assetUtil$checkAssetExist$isFilePath1Exist$Anon1 = new AssetUtil$checkAssetExist$isFilePath1Exist$Anon1(this.$filePath1, qn7);
        assetUtil$checkAssetExist$isFilePath1Exist$Anon1.p$ = (iv7) obj;
        return assetUtil$checkAssetExist$isFilePath1Exist$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
        return ((AssetUtil$checkAssetExist$isFilePath1Exist$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            return ao7.a(AssetUtil.INSTANCE.checkFileExist(this.$filePath1));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
