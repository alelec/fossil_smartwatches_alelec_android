package com.portfolio.platform.cloudimage;

import android.widget.ImageView;
import com.fossil.bw7;
import com.fossil.gu7;
import com.fossil.jv7;
import com.fossil.kq7;
import com.fossil.no4;
import com.fossil.pq7;
import com.fossil.xw7;
import com.fossil.yk7;
import com.fossil.zk7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.Constants;
import java.io.File;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CloudImageHelper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ yk7 instance$delegate; // = zk7.a(CloudImageHelper$Companion$instance$Anon2.INSTANCE);
    @DexIgnore
    public PortfolioApp mApp;
    @DexIgnore
    public no4 mAppExecutors;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void instance$annotations() {
        }

        @DexIgnore
        public final CloudImageHelper getInstance() {
            yk7 yk7 = CloudImageHelper.instance$delegate;
            Companion companion = CloudImageHelper.Companion;
            return (CloudImageHelper) yk7.getValue();
        }

        @DexIgnore
        public final String getTAG() {
            return CloudImageHelper.TAG;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Holder {
        @DexIgnore
        public static /* final */ Holder INSTANCE; // = new Holder();

        @DexIgnore
        /* renamed from: INSTANCE  reason: collision with other field name */
        public static /* final */ CloudImageHelper f1INSTANCE; // = new CloudImageHelper();

        @DexIgnore
        public final CloudImageHelper getINSTANCE() {
            return f1INSTANCE;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ItemImage {
        @DexIgnore
        public Constants.CalibrationType mCalibrationType; // = Constants.CalibrationType.NONE;
        @DexIgnore
        public Constants.DeviceType mDeviceType; // = Constants.DeviceType.NONE;
        @DexIgnore
        public String mFastPairId;
        @DexIgnore
        public File mFile;
        @DexIgnore
        public OnImageCallbackListener mListener;
        @DexIgnore
        public Integer mResourceId; // = -1;
        @DexIgnore
        public String mSerialNumber;
        @DexIgnore
        public String mSerialPrefix;
        @DexIgnore
        public WeakReference<ImageView> mWeakReferenceImageView;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ItemImage() {
        }

        @DexIgnore
        public static /* synthetic */ void mSerialPrefix$annotations() {
        }

        @DexIgnore
        public final void download() {
            xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new CloudImageHelper$ItemImage$download$Anon1(this, null), 3, null);
        }

        @DexIgnore
        public final void downloadForCalibration() {
            xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new CloudImageHelper$ItemImage$downloadForCalibration$Anon1(this, null), 3, null);
        }

        @DexIgnore
        public final void downloadForWearOS() {
            xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new CloudImageHelper$ItemImage$downloadForWearOS$Anon1(this, null), 3, null);
        }

        @DexIgnore
        public final File getFile() {
            return this.mFile;
        }

        @DexIgnore
        public final ItemImage setFastPairId(String str) {
            pq7.c(str, "fastPairId");
            this.mFastPairId = str;
            return this;
        }

        @DexIgnore
        public final ItemImage setFile(File file) {
            this.mFile = file;
            return this;
        }

        @DexIgnore
        public final ItemImage setImageCallback(OnImageCallbackListener onImageCallbackListener) {
            this.mListener = onImageCallbackListener;
            return this;
        }

        @DexIgnore
        public final ItemImage setPlaceHolder(ImageView imageView, int i) {
            pq7.c(imageView, "imageView");
            this.mWeakReferenceImageView = new WeakReference<>(imageView);
            this.mResourceId = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        public final ItemImage setSerialNumber(String str) {
            pq7.c(str, "serialNumber");
            this.mSerialNumber = str;
            return this;
        }

        @DexIgnore
        public final ItemImage setSerialPrefix(String str) {
            pq7.c(str, "serialPrefix");
            this.mSerialPrefix = str;
            return this;
        }

        @DexIgnore
        public final ItemImage setType(Constants.CalibrationType calibrationType) {
            pq7.c(calibrationType, "calibrationType");
            this.mCalibrationType = calibrationType;
            return this;
        }

        @DexIgnore
        public final ItemImage setType(Constants.DeviceType deviceType) {
            pq7.c(deviceType, "deviceType");
            this.mDeviceType = deviceType;
            return this;
        }
    }

    @DexIgnore
    public interface OnForceDownloadCallbackListener {
        @DexIgnore
        void onDownloadCallback(boolean z);
    }

    @DexIgnore
    public interface OnImageCallbackListener {
        @DexIgnore
        void onImageCallback(String str, String str2);
    }

    /*
    static {
        String simpleName = CloudImageHelper$Companion$TAG$Anon1.INSTANCE.getClass().getSimpleName();
        pq7.b(simpleName, "CloudImageHelper::javaClass.javaClass.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public CloudImageHelper() {
        PortfolioApp.h0.c().M().W1(this);
    }

    @DexIgnore
    public static final CloudImageHelper getInstance() {
        return Companion.getInstance();
    }

    @DexIgnore
    public final PortfolioApp getMApp() {
        PortfolioApp portfolioApp = this.mApp;
        if (portfolioApp != null) {
            return portfolioApp;
        }
        pq7.n("mApp");
        throw null;
    }

    @DexIgnore
    public final no4 getMAppExecutors() {
        no4 no4 = this.mAppExecutors;
        if (no4 != null) {
            return no4;
        }
        pq7.n("mAppExecutors");
        throw null;
    }

    @DexIgnore
    public final void setMApp(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "<set-?>");
        this.mApp = portfolioApp;
    }

    @DexIgnore
    public final void setMAppExecutors(no4 no4) {
        pq7.c(no4, "<set-?>");
        this.mAppExecutors = no4;
    }

    @DexIgnore
    public final ItemImage with() {
        ItemImage itemImage = new ItemImage();
        if (itemImage.getFile() == null) {
            xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new CloudImageHelper$with$Anon1(this, itemImage, null), 3, null);
        }
        return itemImage;
    }
}
