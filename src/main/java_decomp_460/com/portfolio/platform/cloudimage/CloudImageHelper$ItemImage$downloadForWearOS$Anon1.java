package com.portfolio.platform.cloudimage;

import android.widget.ImageView;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.il7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import java.io.File;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForWearOS$1", f = "CloudImageHelper.kt", l = {145, 149}, m = "invokeSuspend")
public final class CloudImageHelper$ItemImage$downloadForWearOS$Anon1 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageHelper.ItemImage this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForWearOS$1$1", f = "CloudImageHelper.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageHelper$ItemImage$downloadForWearOS$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(CloudImageHelper$ItemImage$downloadForWearOS$Anon1 cloudImageHelper$ItemImage$downloadForWearOS$Anon1, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cloudImageHelper$ItemImage$downloadForWearOS$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, qn7);
            anon1_Level2.p$ = (iv7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((Anon1_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                if (this.this$0.this$0.mWeakReferenceImageView != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String tag = CloudImageHelper.Companion.getTAG();
                    local.d(tag, "download setDefaultImage first, resourceId=" + this.this$0.this$0.mResourceId);
                    WeakReference weakReference = this.this$0.this$0.mWeakReferenceImageView;
                    if (weakReference != null) {
                        ImageView imageView = (ImageView) weakReference.get();
                        if (imageView != null) {
                            Integer num = this.this$0.this$0.mResourceId;
                            if (num != null) {
                                imageView.setImageResource(num.intValue());
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                    } else {
                        throw new il7("null cannot be cast to non-null type java.lang.ref.WeakReference<android.widget.ImageView>");
                    }
                }
                File file = this.this$0.this$0.mFile;
                if (file != null) {
                    String str = this.this$0.this$0.mFastPairId;
                    if (str != null) {
                        CloudImageHelper.this.getMAppExecutors().b().execute(new CloudImageRunnable(file, str, ResolutionHelper.INSTANCE.getResolutionFromDevice().getResolution(), Constants.DownloadAssetType.WEAR_OS, this.this$0.this$0.mDeviceType.getType(), this.this$0.this$0.mListener));
                        return tl7.f3441a;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageHelper$ItemImage$downloadForWearOS$Anon1(CloudImageHelper.ItemImage itemImage, qn7 qn7) {
        super(2, qn7);
        this.this$0 = itemImage;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        CloudImageHelper$ItemImage$downloadForWearOS$Anon1 cloudImageHelper$ItemImage$downloadForWearOS$Anon1 = new CloudImageHelper$ItemImage$downloadForWearOS$Anon1(this.this$0, qn7);
        cloudImageHelper$ItemImage$downloadForWearOS$Anon1.p$ = (iv7) obj;
        return cloudImageHelper$ItemImage$downloadForWearOS$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((CloudImageHelper$ItemImage$downloadForWearOS$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x008c  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r11) {
        /*
            r10 = this;
            r9 = 2
            r5 = 1
            r8 = 0
            java.lang.Object r7 = com.fossil.yn7.d()
            int r0 = r10.label
            if (r0 == 0) goto L_0x0036
            if (r0 == r5) goto L_0x0021
            if (r0 != r9) goto L_0x0019
            java.lang.Object r0 = r10.L$0
            com.fossil.iv7 r0 = (com.fossil.iv7) r0
            com.fossil.el7.b(r11)
        L_0x0016:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0018:
            return r0
        L_0x0019:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0021:
            java.lang.Object r0 = r10.L$0
            com.fossil.iv7 r0 = (com.fossil.iv7) r0
            com.fossil.el7.b(r11)
            r2 = r0
            r1 = r11
        L_0x002a:
            r0 = r1
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x008c
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            goto L_0x0018
        L_0x0036:
            com.fossil.el7.b(r11)
            com.fossil.iv7 r6 = r10.p$
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r0 = r10.this$0
            java.io.File r0 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMFile$p(r0)
            if (r0 != 0) goto L_0x0052
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r0 = r10.this$0
            com.portfolio.platform.cloudimage.CloudImageHelper r1 = com.portfolio.platform.cloudimage.CloudImageHelper.this
            com.portfolio.platform.PortfolioApp r1 = r1.getMApp()
            java.io.File r1 = r1.getFilesDir()
            com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$setMFile$p(r0, r1)
        L_0x0052:
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r0 = r10.this$0
            java.lang.String r0 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMFastPairId$p(r0)
            if (r0 != 0) goto L_0x005d
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            goto L_0x0018
        L_0x005d:
            com.portfolio.platform.cloudimage.AssetUtil r0 = com.portfolio.platform.cloudimage.AssetUtil.INSTANCE
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r1 = r10.this$0
            java.io.File r1 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMFile$p(r1)
            if (r1 == 0) goto L_0x00a6
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r2 = r10.this$0
            java.lang.String r2 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMFastPairId$p(r2)
            if (r2 == 0) goto L_0x00a2
            com.portfolio.platform.cloudimage.ResolutionHelper r3 = com.portfolio.platform.cloudimage.ResolutionHelper.INSTANCE
            com.portfolio.platform.cloudimage.Constants$Resolution r3 = r3.getResolutionFromDevice()
            java.lang.String r3 = r3.getResolution()
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r4 = r10.this$0
            com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener r4 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMListener$p(r4)
            r10.L$0 = r6
            r10.label = r5
            r5 = r10
            java.lang.Object r1 = r0.checkWearOSAssetExist(r1, r2, r3, r4, r5)
            if (r1 != r7) goto L_0x00aa
            r0 = r7
            goto L_0x0018
        L_0x008c:
            com.fossil.jx7 r0 = com.fossil.bw7.c()
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForWearOS$Anon1$Anon1_Level2 r1 = new com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForWearOS$Anon1$Anon1_Level2
            r1.<init>(r10, r8)
            r10.L$0 = r2
            r10.label = r9
            java.lang.Object r0 = com.fossil.eu7.g(r0, r1, r10)
            if (r0 != r7) goto L_0x0016
            r0 = r7
            goto L_0x0018
        L_0x00a2:
            com.fossil.pq7.i()
            throw r8
        L_0x00a6:
            com.fossil.pq7.i()
            throw r8
        L_0x00aa:
            r2 = r6
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForWearOS$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
