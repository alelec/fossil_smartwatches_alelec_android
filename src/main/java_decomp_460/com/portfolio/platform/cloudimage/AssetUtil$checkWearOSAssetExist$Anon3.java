package com.portfolio.platform.cloudimage;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.cloudimage.CloudImageHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$3", f = "AssetUtil.kt", l = {}, m = "invokeSuspend")
public final class AssetUtil$checkWearOSAssetExist$Anon3 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $fastPairId;
    @DexIgnore
    public /* final */ /* synthetic */ String $filePath2;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageHelper.OnImageCallbackListener $listener;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AssetUtil$checkWearOSAssetExist$Anon3(CloudImageHelper.OnImageCallbackListener onImageCallbackListener, String str, String str2, qn7 qn7) {
        super(2, qn7);
        this.$listener = onImageCallbackListener;
        this.$fastPairId = str;
        this.$filePath2 = str2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        AssetUtil$checkWearOSAssetExist$Anon3 assetUtil$checkWearOSAssetExist$Anon3 = new AssetUtil$checkWearOSAssetExist$Anon3(this.$listener, this.$fastPairId, this.$filePath2, qn7);
        assetUtil$checkWearOSAssetExist$Anon3.p$ = (iv7) obj;
        return assetUtil$checkWearOSAssetExist$Anon3;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((AssetUtil$checkWearOSAssetExist$Anon3) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            CloudImageHelper.OnImageCallbackListener onImageCallbackListener = this.$listener;
            if (onImageCallbackListener == null) {
                return null;
            }
            onImageCallbackListener.onImageCallback(this.$fastPairId, this.$filePath2);
            return tl7.f3441a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
