package com.portfolio.platform.workers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.bk5;
import com.fossil.bw7;
import com.fossil.el7;
import com.fossil.gu7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.on5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.xw7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.microapp.CommuteTimeService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TimeChangeReceiver extends BroadcastReceiver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public bk5 f4785a;
    @DexIgnore
    public on5 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ TimeChangeReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(qn7 qn7, TimeChangeReceiver timeChangeReceiver) {
            super(2, qn7);
            this.this$0 = timeChangeReceiver;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(qn7, this.this$0);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                String J = PortfolioApp.h0.c().J();
                if (J.length() == 0) {
                    return tl7.f3441a;
                }
                long C = this.this$0.b().C(J);
                long A = this.this$0.b().A(J);
                long currentTimeMillis = System.currentTimeMillis();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("TimeChangeReceiver", "currentTime = " + currentTimeMillis + "; lastSyncTimeSuccess = " + C + "; lastReminderSyncTimeSuccess = " + A);
                if (A > currentTimeMillis || C > currentTimeMillis) {
                    on5 b = this.this$0.b();
                    long j = currentTimeMillis - ((long) CommuteTimeService.A);
                    b.A1(j, J);
                    this.this$0.b().y1(j, J);
                    return tl7.f3441a;
                }
                bk5 a2 = this.this$0.a();
                Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
                pq7.b(applicationContext, "PortfolioApp.instance.applicationContext");
                a2.h(applicationContext);
                bk5 a3 = this.this$0.a();
                Context applicationContext2 = PortfolioApp.h0.c().getApplicationContext();
                pq7.b(applicationContext2, "PortfolioApp.instance.applicationContext");
                a3.g(applicationContext2);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public TimeChangeReceiver() {
        PortfolioApp.h0.c().M().J0(this);
    }

    @DexIgnore
    public final bk5 a() {
        bk5 bk5 = this.f4785a;
        if (bk5 != null) {
            return bk5;
        }
        pq7.n("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public final on5 b() {
        on5 on5 = this.b;
        if (on5 != null) {
            return on5;
        }
        pq7.n("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action;
        FLogger.INSTANCE.getLocal().d("TimeChangeReceiver", "onReceive()");
        if (intent != null && (action = intent.getAction()) != null) {
            if (pq7.a(action, "android.intent.action.TIME_SET") || pq7.a(action, "android.intent.action.TIMEZONE_CHANGED") || pq7.a(action, "android.intent.action.TIME_TICK")) {
                xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new a(null, this), 3, null);
            }
        }
    }
}
