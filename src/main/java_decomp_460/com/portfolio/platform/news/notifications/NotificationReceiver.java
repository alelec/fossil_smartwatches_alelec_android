package com.portfolio.platform.news.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.ao7;
import com.fossil.bw7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gu7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.mj5;
import com.fossil.on5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.ux7;
import com.fossil.vp7;
import com.fossil.xw7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationReceiver extends BroadcastReceiver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public on5 f4696a;
    @DexIgnore
    public mj5 b;
    @DexIgnore
    public UserRepository c;
    @DexIgnore
    public /* final */ iv7 d; // = jv7.a(ux7.b(null, 1, null).plus(bw7.b()));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.news.notifications.NotificationReceiver$onReceive$1", f = "NotificationReceiver.kt", l = {52}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Intent $intent;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(NotificationReceiver notificationReceiver, Intent intent, qn7 qn7) {
            super(2, qn7);
            this.this$0 = notificationReceiver;
            this.$intent = intent;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$intent, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object currentUser;
            Integer e;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                UserRepository b = this.this$0.b();
                this.L$0 = iv7;
                this.label = 1;
                currentUser = b.getCurrentUser(this);
                if (currentUser == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                currentUser = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((MFUser) currentUser) == null) {
                FLogger.INSTANCE.getLocal().d("NotificationReceiver", "onReceive - user is NULL!!!");
                return tl7.f3441a;
            }
            Intent intent = this.$intent;
            int intValue = (intent == null || (e = ao7.e(intent.getIntExtra("ACTION_EVENT", 0))) == null) ? 0 : e.intValue();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("NotificationReceiver", "onReceive - actionEvent=" + intValue);
            if (intValue == 1) {
                PortfolioApp.h0.c().S1(this.this$0.a(), false, 10);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public NotificationReceiver() {
        PortfolioApp.h0.c().M().O(this);
    }

    @DexIgnore
    public final mj5 a() {
        mj5 mj5 = this.b;
        if (mj5 != null) {
            return mj5;
        }
        pq7.n("mDeviceSettingFactory");
        throw null;
    }

    @DexIgnore
    public final UserRepository b() {
        UserRepository userRepository = this.c;
        if (userRepository != null) {
            return userRepository;
        }
        pq7.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        FLogger.INSTANCE.getLocal().d("NotificationReceiver", "onReceive");
        xw7 unused = gu7.d(this.d, null, null, new a(this, intent, null), 3, null);
    }
}
