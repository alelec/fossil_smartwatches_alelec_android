package com.portfolio.platform.helper;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.jj4;
import com.fossil.kj4;
import com.fossil.lj4;
import com.fossil.lk5;
import com.fossil.pq7;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConvertDateTime implements dj4<DateTime>, lj4<DateTime> {
    @DexIgnore
    /* renamed from: a */
    public DateTime deserialize(JsonElement jsonElement, Type type, cj4 cj4) {
        pq7.c(jsonElement, "json");
        pq7.c(type, "typeOfT");
        pq7.c(cj4, "context");
        String f = jsonElement.f();
        pq7.b(f, "dateAsString");
        if (f.length() == 0) {
            return new DateTime(0);
        }
        try {
            DateTime R = lk5.R(DateTimeZone.getDefault(), f);
            pq7.b(R, "DateHelper.getServerDate\u2026tDefault(), dateAsString)");
            return R;
        } catch (Exception e) {
            try {
                DateTime M = lk5.M(jsonElement.f());
                pq7.b(M, "DateHelper.getLocalDateT\u2026DateFormat(json.asString)");
                return M;
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("GsonConvertDateTime", "deserialize - json=" + jsonElement.f() + ", e=" + e2);
                e2.printStackTrace();
                return new DateTime(0);
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public JsonElement serialize(DateTime dateTime, Type type, kj4 kj4) {
        String s0;
        pq7.c(type, "typeOfSrc");
        pq7.c(kj4, "context");
        if (dateTime == null) {
            s0 = "";
        } else {
            s0 = lk5.s0(DateTimeZone.UTC, dateTime);
            pq7.b(s0, "DateHelper.printServerDa\u2026at(DateTimeZone.UTC, src)");
        }
        return new jj4(s0);
    }
}
