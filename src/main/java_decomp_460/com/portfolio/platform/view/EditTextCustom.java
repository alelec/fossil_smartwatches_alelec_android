package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class EditTextCustom extends FlexibleEditText {
    @DexIgnore
    public b A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public a D;
    @DexIgnore
    public Drawable w;
    @DexIgnore
    public Drawable x;
    @DexIgnore
    public Drawable y;
    @DexIgnore
    public Drawable z;

    @DexIgnore
    public interface a {

        @DexIgnore
        /* renamed from: com.portfolio.platform.view.EditTextCustom$a$a  reason: collision with other inner class name */
        public enum EnumC0356a {
            TOP,
            BOTTOM,
            LEFT,
            RIGHT
        }

        @DexIgnore
        void a(EnumC0356a aVar);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore
    public EditTextCustom(Context context) {
        super(context);
    }

    @DexIgnore
    public EditTextCustom(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @DexIgnore
    public EditTextCustom(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @DexIgnore
    @Override // java.lang.Object
    public void finalize() throws Throwable {
        this.w = null;
        this.z = null;
        this.x = null;
        this.y = null;
        super.finalize();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.FlexibleEditText
    public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 4 && keyEvent.getAction() == 1) {
            b bVar = this.A;
            if (bVar != null) {
                bVar.a();
            }
            clearFocus();
        }
        return true;
    }

    @DexIgnore
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        a aVar;
        a aVar2;
        if (motionEvent.getAction() == 0) {
            this.B = (int) motionEvent.getX();
            this.C = (int) motionEvent.getY();
            Drawable drawable = this.z;
            if (drawable == null || !drawable.getBounds().contains(this.B, this.C)) {
                Drawable drawable2 = this.y;
                if (drawable2 == null || !drawable2.getBounds().contains(this.B, this.C)) {
                    Drawable drawable3 = this.x;
                    if (drawable3 != null) {
                        Rect bounds = drawable3.getBounds();
                        int i = (int) (((double) (getResources().getDisplayMetrics().density * 13.0f)) + 0.5d);
                        int i2 = this.B;
                        int i3 = this.C;
                        if (!bounds.contains(i2, i3)) {
                            i2 = this.B;
                            int i4 = i2 - i;
                            i3 = this.C - i;
                            if (i4 > 0) {
                                i2 = i4;
                            }
                            if (i3 <= 0) {
                                i3 = this.C;
                            }
                        }
                        if (bounds.contains(i2, i3) && (aVar2 = this.D) != null) {
                            aVar2.a(a.EnumC0356a.LEFT);
                            motionEvent.setAction(3);
                            return false;
                        }
                    }
                    Drawable drawable4 = this.w;
                    if (drawable4 != null) {
                        Rect bounds2 = drawable4.getBounds();
                        int i5 = this.B;
                        int i6 = this.C - 13;
                        int width = getWidth() - (i5 + 13);
                        if (width <= 0) {
                            width += 13;
                        }
                        if (i6 <= 0) {
                            i6 = this.C;
                        }
                        if (!bounds2.contains(width, i6) || (aVar = this.D) == null) {
                            return super.onTouchEvent(motionEvent);
                        }
                        aVar.a(a.EnumC0356a.RIGHT);
                        motionEvent.setAction(3);
                        return false;
                    }
                } else {
                    this.D.a(a.EnumC0356a.TOP);
                    return super.onTouchEvent(motionEvent);
                }
            } else {
                this.D.a(a.EnumC0356a.BOTTOM);
                return super.onTouchEvent(motionEvent);
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    public void setBackPressListener(b bVar) {
        this.A = bVar;
    }

    @DexIgnore
    public void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            this.x = drawable;
        }
        if (drawable3 != null) {
            this.w = drawable3;
        }
        if (drawable2 != null) {
            this.y = drawable2;
        }
        if (drawable4 != null) {
            this.z = drawable4;
        }
        super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
    }

    @DexIgnore
    public void setDrawableClickListener(a aVar) {
        this.D = aVar;
    }

    @DexIgnore
    public void setTypeface(Typeface typeface) {
        super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Fossil_Scout-Regular_10.otf"));
    }
}
