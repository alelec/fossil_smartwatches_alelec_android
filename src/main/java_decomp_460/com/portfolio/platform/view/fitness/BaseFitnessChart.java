package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseFitnessChart extends View implements ViewTreeObserver.OnGlobalLayoutListener {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public TypedArray b;
    @DexIgnore
    public Bitmap c;

    /*
    static {
        String simpleName = BaseFitnessChart.class.getSimpleName();
        pq7.b(simpleName, "BaseFitnessChart::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseFitnessChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        pq7.c(context, "context");
    }

    @DexIgnore
    public final void a() {
        Bitmap bitmap = this.c;
        if (bitmap != null) {
            if (bitmap == null) {
                pq7.n("mStarBitmap");
                throw null;
            } else if (!bitmap.isRecycled()) {
                return;
            }
        }
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), getStarIconResId());
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeResource, getStarSizeInPx(), getStarSizeInPx(), false);
        pq7.b(createScaledBitmap, "Bitmap.createScaledBitma\u2026nPx, starSizeInPx, false)");
        this.c = createScaledBitmap;
        if (createScaledBitmap == null) {
            pq7.n("mStarBitmap");
            throw null;
        } else if (!pq7.a(createScaledBitmap, decodeResource)) {
            decodeResource.recycle();
        }
    }

    @DexIgnore
    public final void b() {
        Bitmap bitmap = this.c;
        if (bitmap == null) {
            return;
        }
        if (bitmap != null) {
            bitmap.recycle();
        } else {
            pq7.n("mStarBitmap");
            throw null;
        }
    }

    @DexIgnore
    public final TypedArray getMTypedArray() {
        return this.b;
    }

    @DexIgnore
    public abstract int getStarIconResId();

    @DexIgnore
    public abstract int getStarSizeInPx();

    @DexIgnore
    public final Bitmap getStartBitmap() {
        if (this.c == null) {
            a();
        }
        Bitmap bitmap = this.c;
        if (bitmap != null) {
            return bitmap;
        }
        pq7.n("mStarBitmap");
        throw null;
    }

    @DexIgnore
    public void onAttachedToWindow() {
        FLogger.INSTANCE.getLocal().d(d, "onAttachedToWindow, initStartBitmap");
        super.onAttachedToWindow();
        a();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        FLogger.INSTANCE.getLocal().d(d, "onDetachedFromWindow, recycleStarBitmap");
        super.onDetachedFromWindow();
        b();
    }

    @DexIgnore
    public final void setMTypedArray(TypedArray typedArray) {
        this.b = typedArray;
    }
}
