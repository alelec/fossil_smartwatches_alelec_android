package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.gl0;
import com.fossil.pq7;
import com.fossil.sq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardVisualizationRings extends View implements ViewTreeObserver.OnGlobalLayoutListener {
    @DexIgnore
    public static /* final */ String U;
    @DexIgnore
    public /* final */ Paint A;
    @DexIgnore
    public /* final */ Paint B;
    @DexIgnore
    public /* final */ Paint C;
    @DexIgnore
    public /* final */ Paint D;
    @DexIgnore
    public /* final */ Paint E;
    @DexIgnore
    public /* final */ int F;
    @DexIgnore
    public /* final */ int G;
    @DexIgnore
    public /* final */ int H;
    @DexIgnore
    public Bitmap I;
    @DexIgnore
    public Bitmap J;
    @DexIgnore
    public Bitmap K;
    @DexIgnore
    public Bitmap L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public float O;
    @DexIgnore
    public float P;
    @DexIgnore
    public float Q;
    @DexIgnore
    public float R;
    @DexIgnore
    public float S;
    @DexIgnore
    public float T;
    @DexIgnore
    public TypedArray b;
    @DexIgnore
    public /* final */ RectF c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ Paint v;
    @DexIgnore
    public /* final */ Paint w;
    @DexIgnore
    public /* final */ Paint x;
    @DexIgnore
    public /* final */ Paint y;
    @DexIgnore
    public /* final */ Paint z;

    /*
    static {
        String simpleName = DashboardVisualizationRings.class.getSimpleName();
        pq7.b(simpleName, "DashboardVisualizationRings::class.java.simpleName");
        U = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DashboardVisualizationRings(Context context) {
        this(context, null, 0);
        pq7.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DashboardVisualizationRings(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        pq7.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardVisualizationRings(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        pq7.c(context, "context");
        this.c = new RectF();
        this.u = new Paint(1);
        this.v = new Paint(1);
        this.w = new Paint(1);
        this.x = new Paint(1);
        this.y = new Paint(1);
        this.z = new Paint(1);
        this.A = new Paint(1);
        this.B = new Paint(1);
        this.C = new Paint(1);
        this.D = new Paint(1);
        this.E = new Paint(1);
        this.F = context.getResources().getDimensionPixelSize(2131165413);
        this.G = context.getResources().getDimensionPixelSize(2131165421);
        this.H = context.getResources().getDimensionPixelSize(2131165396);
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            this.b = context.getTheme().obtainStyledAttributes(attributeSet, sq4.DashboardVisualizationRings, 0, 0);
        }
        int d2 = gl0.d(context, 2131099833);
        this.t = gl0.d(context, 2131099703);
        TypedArray typedArray = this.b;
        this.s = typedArray != null ? typedArray.getColor(2, d2) : d2;
        TypedArray typedArray2 = this.b;
        this.d = typedArray2 != null ? typedArray2.getColor(9, d2) : d2;
        TypedArray typedArray3 = this.b;
        this.e = typedArray3 != null ? typedArray3.getColor(9, d2) : d2;
        TypedArray typedArray4 = this.b;
        this.f = typedArray4 != null ? typedArray4.getColor(0, d2) : d2;
        TypedArray typedArray5 = this.b;
        this.g = typedArray5 != null ? typedArray5.getColor(0, d2) : d2;
        TypedArray typedArray6 = this.b;
        this.h = typedArray6 != null ? typedArray6.getColor(3, d2) : d2;
        TypedArray typedArray7 = this.b;
        this.i = typedArray7 != null ? typedArray7.getColor(3, d2) : d2;
        TypedArray typedArray8 = this.b;
        this.k = typedArray8 != null ? typedArray8.getColor(6, d2) : d2;
        TypedArray typedArray9 = this.b;
        this.l = typedArray9 != null ? typedArray9.getColor(7, d2) : d2;
        TypedArray typedArray10 = this.b;
        this.m = typedArray10 != null ? typedArray10.getColor(8, d2) : d2;
        TypedArray typedArray11 = this.b;
        this.j = typedArray11 != null ? typedArray11.getColor(5, d2) : d2;
    }

    @DexIgnore
    public final void a(Canvas canvas, float f2, float f3, Bitmap bitmap) {
        RectF h2 = h(this.c, (float) this.G);
        Paint paint = this.y;
        e(canvas, h2, f2, f3, bitmap, paint, paint);
    }

    @DexIgnore
    public final void b(Canvas canvas, float f2, float f3, Bitmap bitmap) {
        RectF h2 = h(this.c, ((float) this.G) / ((float) 2));
        Paint paint = this.A;
        e(canvas, h2, f2, f3, bitmap, paint, paint);
    }

    @DexIgnore
    public final void c(Canvas canvas, RectF rectF, boolean z2, float f2, float f3, Paint paint) {
        if (z2) {
            float centerX = rectF.centerX();
            float centerY = rectF.centerY();
            float width = (rectF.width() - ((float) this.F)) / ((float) 2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = U;
            local.d(str, "centerX: " + centerX + ", centerY: " + centerY + ", radius: " + width);
            canvas.drawCircle(centerX, centerY, width, this.u);
        }
        canvas.drawArc(h(rectF, paint.getStrokeWidth() / ((float) 2)), f2, f3, false, paint);
    }

    @DexIgnore
    public final void d(Canvas canvas, Bitmap bitmap, float f2, float f3) {
        canvas.drawBitmap(bitmap, f3, f2, this.v);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(this.s);
            float f2 = this.M;
            float f3 = this.N;
            Bitmap bitmap = this.I;
            if (bitmap != null) {
                g(canvas, f2, f3, bitmap);
                float f4 = this.O;
                float f5 = this.P;
                Bitmap bitmap2 = this.K;
                if (bitmap2 != null) {
                    b(canvas, f4, f5, bitmap2);
                    float f6 = this.Q;
                    float f7 = this.R;
                    Bitmap bitmap3 = this.J;
                    if (bitmap3 != null) {
                        a(canvas, f6, f7, bitmap3);
                        float f8 = this.S;
                        float f9 = this.T;
                        Bitmap bitmap4 = this.L;
                        if (bitmap4 != null) {
                            f(canvas, f8, f9, bitmap4);
                        } else {
                            pq7.n("mSleepBitmap");
                            throw null;
                        }
                    } else {
                        pq7.n("mActiveMinutesBitmap");
                        throw null;
                    }
                } else {
                    pq7.n("mCaloriesBitmap");
                    throw null;
                }
            } else {
                pq7.n("mStepsBitmap");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void e(Canvas canvas, RectF rectF, float f2, float f3, Bitmap bitmap, Paint paint, Paint paint2) {
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        float strokeWidth = paint.getStrokeWidth();
        float f4 = rectF.top;
        float f5 = (float) 2;
        float f6 = ((rectF.left + rectF.right) - ((float) width)) / f5;
        float f7 = (float) height;
        RectF h2 = f7 >= strokeWidth ? h(rectF, (f7 - strokeWidth) / f5) : rectF;
        if (f3 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            c(canvas, h2, true, 270.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, paint);
            d(canvas, bitmap, f4, f6);
            return;
        }
        if (f2 > f3) {
            c(canvas, h2, false, 270.0f, 360.0f, paint);
            c(canvas, h(h2, ((float) 1) + strokeWidth), false, 270.0f, Math.min(Math.min((f2 - f3) / f3, 1.0f) * 360.0f, 360.0f), paint2);
        } else {
            float min = Math.min((f2 / f3) * 360.0f, 360.0f);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = U;
            local.d(str, "sweepAngle: " + min);
            c(canvas, h2, true, 270.0f, min, paint);
        }
        d(canvas, bitmap, f4, f6);
    }

    @DexIgnore
    public final void f(Canvas canvas, float f2, float f3, Bitmap bitmap) {
        e(canvas, h(this.c, ((float) this.G) * 1.5f), f2, f3, bitmap, this.C, this.D);
    }

    @DexIgnore
    public final void g(Canvas canvas, float f2, float f3, Bitmap bitmap) {
        RectF rectF = this.c;
        Paint paint = this.w;
        e(canvas, rectF, f2, f3, bitmap, paint, paint);
    }

    @DexIgnore
    public final RectF h(RectF rectF, float f2) {
        return new RectF(rectF.left + f2, rectF.top + f2, rectF.right - f2, rectF.bottom - f2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003d, code lost:
        if (r0.isRecycled() != false) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006e, code lost:
        if (r0.isRecycled() != false) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000c, code lost:
        if (r0.isRecycled() != false) goto L_0x000e;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void i() {
        /*
            r5 = this;
            r4 = 0
            r3 = 0
            android.graphics.Bitmap r0 = r5.I
            if (r0 == 0) goto L_0x000e
            if (r0 == 0) goto L_0x00c7
            boolean r0 = r0.isRecycled()
            if (r0 == 0) goto L_0x0033
        L_0x000e:
            android.content.res.Resources r0 = r5.getResources()
            r1 = 2131231193(0x7f0801d9, float:1.807846E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            int r1 = r5.H
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createScaledBitmap(r0, r1, r1, r4)
            java.lang.String r2 = "Bitmap.createScaledBitma\u2026Px, mIconSizeInPx, false)"
            com.fossil.pq7.b(r1, r2)
            r5.I = r1
            if (r1 == 0) goto L_0x00f1
            boolean r1 = com.fossil.pq7.a(r0, r1)
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x0033
            r0.recycle()
        L_0x0033:
            android.graphics.Bitmap r0 = r5.J
            if (r0 == 0) goto L_0x003f
            if (r0 == 0) goto L_0x00cd
            boolean r0 = r0.isRecycled()
            if (r0 == 0) goto L_0x0064
        L_0x003f:
            android.content.res.Resources r0 = r5.getResources()
            r1 = 2131231189(0x7f0801d5, float:1.8078452E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            int r1 = r5.H
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createScaledBitmap(r0, r1, r1, r4)
            java.lang.String r2 = "Bitmap.createScaledBitma\u2026Px, mIconSizeInPx, false)"
            com.fossil.pq7.b(r1, r2)
            r5.J = r1
            if (r1 == 0) goto L_0x00eb
            boolean r1 = com.fossil.pq7.a(r0, r1)
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x0064
            r0.recycle()
        L_0x0064:
            android.graphics.Bitmap r0 = r5.K
            if (r0 == 0) goto L_0x0070
            if (r0 == 0) goto L_0x00d3
            boolean r0 = r0.isRecycled()
            if (r0 == 0) goto L_0x0095
        L_0x0070:
            android.content.res.Resources r0 = r5.getResources()
            r1 = 2131231190(0x7f0801d6, float:1.8078454E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            int r1 = r5.H
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createScaledBitmap(r0, r1, r1, r4)
            java.lang.String r2 = "Bitmap.createScaledBitma\u2026Px, mIconSizeInPx, false)"
            com.fossil.pq7.b(r1, r2)
            r5.K = r1
            if (r1 == 0) goto L_0x00e5
            boolean r1 = com.fossil.pq7.a(r0, r1)
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x0095
            r0.recycle()
        L_0x0095:
            android.graphics.Bitmap r0 = r5.L
            if (r0 == 0) goto L_0x00a1
            if (r0 == 0) goto L_0x00d9
            boolean r0 = r0.isRecycled()
            if (r0 == 0) goto L_0x00c6
        L_0x00a1:
            android.content.res.Resources r0 = r5.getResources()
            r1 = 2131231192(0x7f0801d8, float:1.8078458E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            int r1 = r5.H
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createScaledBitmap(r0, r1, r1, r4)
            java.lang.String r2 = "Bitmap.createScaledBitma\u2026Px, mIconSizeInPx, false)"
            com.fossil.pq7.b(r1, r2)
            r5.L = r1
            if (r1 == 0) goto L_0x00df
            boolean r1 = com.fossil.pq7.a(r0, r1)
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x00c6
            r0.recycle()
        L_0x00c6:
            return
        L_0x00c7:
            java.lang.String r0 = "mStepsBitmap"
            com.fossil.pq7.n(r0)
            throw r3
        L_0x00cd:
            java.lang.String r0 = "mActiveMinutesBitmap"
            com.fossil.pq7.n(r0)
            throw r3
        L_0x00d3:
            java.lang.String r0 = "mCaloriesBitmap"
            com.fossil.pq7.n(r0)
            throw r3
        L_0x00d9:
            java.lang.String r0 = "mSleepBitmap"
            com.fossil.pq7.n(r0)
            throw r3
        L_0x00df:
            java.lang.String r0 = "mSleepBitmap"
            com.fossil.pq7.n(r0)
            throw r3
        L_0x00e5:
            java.lang.String r0 = "mCaloriesBitmap"
            com.fossil.pq7.n(r0)
            throw r3
        L_0x00eb:
            java.lang.String r0 = "mActiveMinutesBitmap"
            com.fossil.pq7.n(r0)
            throw r3
        L_0x00f1:
            java.lang.String r0 = "mStepsBitmap"
            com.fossil.pq7.n(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.fitness.DashboardVisualizationRings.i():void");
    }

    @DexIgnore
    public final void j() {
        Bitmap bitmap = this.I;
        if (bitmap != null) {
            if (bitmap != null) {
                bitmap.recycle();
            } else {
                pq7.n("mStepsBitmap");
                throw null;
            }
        }
        Bitmap bitmap2 = this.K;
        if (bitmap2 != null) {
            if (bitmap2 != null) {
                bitmap2.recycle();
            } else {
                pq7.n("mCaloriesBitmap");
                throw null;
            }
        }
        Bitmap bitmap3 = this.J;
        if (bitmap3 != null) {
            if (bitmap3 != null) {
                bitmap3.recycle();
            } else {
                pq7.n("mActiveMinutesBitmap");
                throw null;
            }
        }
        Bitmap bitmap4 = this.L;
        if (bitmap4 == null) {
            return;
        }
        if (bitmap4 != null) {
            bitmap4.recycle();
        } else {
            pq7.n("mSleepBitmap");
            throw null;
        }
    }

    @DexIgnore
    public final void k(Paint paint, int i2, float f2) {
        paint.setColor(i2);
        paint.setDither(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(f2);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeJoin(Paint.Join.ROUND);
    }

    @DexIgnore
    public void onAttachedToWindow() {
        FLogger.INSTANCE.getLocal().d(U, "onAttachedToWindow, initIcons");
        super.onAttachedToWindow();
        i();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        FLogger.INSTANCE.getLocal().d(U, "onDetachedFromWindow, recycleIcons");
        super.onDetachedFromWindow();
        j();
    }

    @DexIgnore
    public void onGlobalLayout() {
        float width = (float) getWidth();
        float height = (float) getHeight();
        if (width > height) {
            float f2 = (width - height) / ((float) 2);
            RectF rectF = this.c;
            rectF.left = f2;
            rectF.right = f2 + height;
            rectF.top = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            rectF.bottom = height;
        } else if (height > width) {
            float f3 = (height - width) / ((float) 2);
            RectF rectF2 = this.c;
            rectF2.left = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            rectF2.right = width;
            rectF2.top = f3;
            rectF2.bottom = width + f3;
        } else {
            RectF rectF3 = this.c;
            rectF3.left = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            rectF3.right = width;
            rectF3.top = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            rectF3.bottom = height;
        }
        float f4 = (float) this.F;
        this.u.setColor(this.j);
        this.u.setDither(true);
        this.u.setStyle(Paint.Style.STROKE);
        this.u.setStrokeWidth(f4);
        this.v.setColor(this.t);
        this.v.setStrokeWidth(f4);
        k(this.w, this.d, f4);
        k(this.x, this.e, f4);
        k(this.A, this.h, f4);
        k(this.B, this.i, f4);
        k(this.y, this.f, f4);
        k(this.z, this.g, f4);
        k(this.C, this.k, f4);
        k(this.D, this.l, f4);
        k(this.E, this.m, f4);
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }
}
