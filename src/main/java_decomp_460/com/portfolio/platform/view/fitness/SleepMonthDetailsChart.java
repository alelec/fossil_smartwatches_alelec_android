package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.b67;
import com.fossil.gl0;
import com.fossil.k37;
import com.fossil.nl0;
import com.fossil.pq7;
import com.fossil.qq7;
import com.fossil.rh5;
import com.fossil.rp7;
import com.fossil.sq4;
import com.fossil.tl7;
import com.fossil.wt7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.sleep.SleepDayData;
import com.portfolio.platform.data.model.sleep.SleepSessionData;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepMonthDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String H;
    @DexIgnore
    public /* final */ int A;
    @DexIgnore
    public /* final */ int B;
    @DexIgnore
    public rh5 C;
    @DexIgnore
    public /* final */ ArrayList<SleepDayData> D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ Paint m;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ Paint v;
    @DexIgnore
    public /* final */ Paint w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<LinkedList<Integer>, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ Canvas $canvas$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ int $chartHeight;
        @DexIgnore
        public /* final */ /* synthetic */ int $chartWidth;
        @DexIgnore
        public /* final */ /* synthetic */ SleepMonthDetailsChart this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(int i, int i2, SleepMonthDetailsChart sleepMonthDetailsChart, Canvas canvas) {
            super(1);
            this.$chartWidth = i;
            this.$chartHeight = i2;
            this.this$0 = sleepMonthDetailsChart;
            this.$canvas$inlined = canvas;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(LinkedList<Integer> linkedList) {
            invoke(linkedList);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(LinkedList<Integer> linkedList) {
            pq7.c(linkedList, "centerXList");
            this.this$0.h(this.$canvas$inlined, linkedList);
            SleepMonthDetailsChart sleepMonthDetailsChart = this.this$0;
            sleepMonthDetailsChart.k(this.$canvas$inlined, linkedList, this.$chartWidth, this.$chartHeight, sleepMonthDetailsChart.z * 2, this.$chartHeight - 4);
        }
    }

    /*
    static {
        String simpleName = SleepMonthDetailsChart.class.getSimpleName();
        pq7.b(simpleName, "SleepMonthDetailsChart::class.java.simpleName");
        H = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepMonthDetailsChart(Context context) {
        this(context, null, 0);
        pq7.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepMonthDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        pq7.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepMonthDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        pq7.c(context, "context");
        this.m = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = new Paint(1);
        this.w = new Paint(1);
        this.x = context.getResources().getDimensionPixelSize(2131165385);
        this.y = context.getResources().getDimensionPixelSize(2131165372);
        this.z = context.getResources().getDimensionPixelSize(2131165420);
        this.A = context.getResources().getDimensionPixelSize(2131165372);
        this.B = context.getResources().getDimensionPixelSize(2131165407);
        this.C = rh5.TOTAL_SLEEP;
        this.D = new ArrayList<>();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, sq4.SleepMonthDetailsChart, 0, 0));
        }
        int d = gl0.d(context, 2131099831);
        TypedArray mTypedArray = getMTypedArray();
        this.e = mTypedArray != null ? mTypedArray.getColor(4, d) : d;
        TypedArray mTypedArray2 = getMTypedArray();
        this.f = mTypedArray2 != null ? mTypedArray2.getColor(3, d) : d;
        TypedArray mTypedArray3 = getMTypedArray();
        this.g = mTypedArray3 != null ? mTypedArray3.getColor(0, d) : d;
        TypedArray mTypedArray4 = getMTypedArray();
        this.h = mTypedArray4 != null ? mTypedArray4.getColor(1, d) : d;
        TypedArray mTypedArray5 = getMTypedArray();
        this.i = mTypedArray5 != null ? mTypedArray5.getColor(2, d) : d;
        TypedArray mTypedArray6 = getMTypedArray();
        this.j = mTypedArray6 != null ? mTypedArray6.getColor(5, d) : d;
        TypedArray mTypedArray7 = getMTypedArray();
        this.k = mTypedArray7 != null ? mTypedArray7.getDimensionPixelSize(7, 40) : 40;
        TypedArray mTypedArray8 = getMTypedArray();
        this.l = mTypedArray8 != null ? mTypedArray8.getResourceId(6, 2131296261) : 2131296261;
        TypedArray mTypedArray9 = getMTypedArray();
        if (mTypedArray9 != null) {
            mTypedArray9.recycle();
        }
    }

    @DexIgnore
    private final int getMChartMax() {
        int i2;
        int i3 = b67.b[this.C.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = H;
            local.d(str, "Max of awake: " + this.G);
            i2 = this.G;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = H;
            local2.d(str2, "Max of light: " + this.F);
            i2 = this.F;
        } else if (i3 != 3) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = H;
            local3.d(str3, "Max of total: " + getMMaxSleepMinutes());
            i2 = getMMaxSleepMinutes();
        } else {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = H;
            local4.d(str4, "Max of restful: " + this.E);
            i2 = this.E;
        }
        return Math.max(i2, getMMaxGoal());
    }

    @DexIgnore
    private final int getMMaxGoal() {
        T t2;
        Iterator<T> it = this.D.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (it.hasNext()) {
                int sleepGoal = next.getSleepGoal();
                while (true) {
                    next = it.next();
                    sleepGoal = next.getSleepGoal();
                    if (sleepGoal >= sleepGoal) {
                        sleepGoal = sleepGoal;
                        next = next;
                    }
                    if (!it.hasNext()) {
                        break;
                    }
                }
            }
            t2 = next;
        }
        T t3 = t2;
        if (t3 != null) {
            return t3.getSleepGoal();
        }
        return 480;
    }

    @DexIgnore
    private final int getMMaxSleepMinutes() {
        T t2;
        Iterator<T> it = this.D.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (it.hasNext()) {
                int totalSleepMinutes = next.getTotalSleepMinutes();
                while (true) {
                    next = it.next();
                    totalSleepMinutes = next.getTotalSleepMinutes();
                    if (totalSleepMinutes >= totalSleepMinutes) {
                        totalSleepMinutes = totalSleepMinutes;
                        next = next;
                    }
                    if (!it.hasNext()) {
                        break;
                    }
                }
            }
            t2 = next;
        }
        T t3 = t2;
        if (t3 != null) {
            return t3.getTotalSleepMinutes();
        }
        return 0;
    }

    @DexIgnore
    private final int getMSleepMode() {
        int i2 = b67.f400a[this.C.ordinal()];
        if (i2 == 1) {
            return 0;
        }
        if (i2 != 2) {
            return i2 != 3 ? 3 : 2;
        }
        return 1;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(-1);
            Rect rect = new Rect();
            this.t.getTextBounds("1", 0, wt7.A("1") > 0 ? wt7.A("1") : 1, rect);
            int height = (int) (((((float) getHeight()) - ((float) rect.height())) - ((float) this.z)) - ((float) this.B));
            int width = getWidth();
            int width2 = getStartBitmap().getWidth();
            int i2 = this.z;
            int i3 = this.A;
            int i4 = ((width - width2) - (i2 * 2)) - i3;
            g(canvas, i4, height, i3, i2 * 2, new a(i4, height, this, canvas));
            i(canvas, 0, height);
        }
    }

    @DexIgnore
    public final void f(Canvas canvas, int i2, int i3, int i4, int i5, int i6, SleepDayData sleepDayData) {
        int i7 = i2 - (i5 / 2);
        int i8 = i7 >= 0 ? i7 : 0;
        int size = (i4 - i6) - ((sleepDayData.getSessionList().size() + -1 > 0 ? sleepDayData.getSessionList().size() - 1 : 0) * this.B);
        Iterator<SleepSessionData> it = sleepDayData.getSessionList().iterator();
        int i9 = 0;
        while (it.hasNext()) {
            int component1 = it.next().component1();
            FLogger.INSTANCE.getLocal().d(H, "session duration: " + component1);
            i9 = component1 + i9;
        }
        int size2 = sleepDayData.getSessionList().size();
        for (int i10 = 0; i10 < size2; i10++) {
            SleepSessionData sleepSessionData = sleepDayData.getSessionList().get(i10);
            pq7.b(sleepSessionData, "sleepDayData.sessionList[sessionIndex]");
            SleepSessionData sleepSessionData2 = sleepSessionData;
            int durationInMinutes = sleepSessionData2.getDurationInMinutes();
            float f2 = (float) durationInMinutes;
            int i11 = (int) ((f2 / ((float) i9)) * ((float) size));
            List<WrapperSleepStateChange> sleepStates = sleepSessionData2.getSleepStates();
            int size3 = sleepStates.size();
            int i12 = 0;
            while (i12 < size3) {
                int i13 = sleepStates.get(i12).state;
                int i14 = (int) ((((float) (i12 < size3 + -1 ? ((int) sleepStates.get(i12 + 1).index) - ((int) sleepStates.get(i12).index) : durationInMinutes - ((int) sleepStates.get(i12).index))) / f2) * ((float) i11));
                if (i14 < 1) {
                    i14 = 1;
                }
                int i15 = i3 - i14;
                canvas.drawRect(new RectF((float) i8, (float) i15, (float) (i8 + i5), (float) i3), i13 != 1 ? i13 != 2 ? this.u : this.w : this.v);
                i12++;
                i3 = i15;
            }
            i3 -= this.B;
        }
    }

    @DexIgnore
    public final void g(Canvas canvas, int i2, int i3, int i4, int i5, rp7<? super LinkedList<Integer>, tl7> rp7) {
        if (!this.D.isEmpty()) {
            int size = i2 / this.D.size();
            int i6 = this.y;
            int i7 = size < i6 ? size : i6;
            int i8 = i7 / 2;
            LinkedList linkedList = new LinkedList();
            float mChartMax = (float) getMChartMax();
            Iterator<SleepDayData> it = this.D.iterator();
            while (it.hasNext()) {
                SleepDayData next = it.next();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = H;
                local.d(str, "Actual sleep: " + next.getTotalSleepMinutes() + ", chart max: " + mChartMax);
                int i9 = i4 + (i8 / 2);
                int totalSleepMinutes = next.getTotalSleepMinutes();
                int mSleepMode = getMSleepMode();
                if (mSleepMode == 0) {
                    int dayAwake = next.getDayAwake();
                    j(canvas, this.u, i9, i3, (int) ((((float) dayAwake) / mChartMax) * ((float) i3)), i8, i5, dayAwake, totalSleepMinutes);
                } else if (mSleepMode == 1) {
                    int dayLight = next.getDayLight();
                    j(canvas, this.v, i9, i3, (int) ((((float) dayLight) / mChartMax) * ((float) i3)), i8, i5, dayLight, totalSleepMinutes);
                } else if (mSleepMode != 2) {
                    pq7.b(next, "sleepDayData");
                    f(canvas, i9, i3, (int) ((((float) totalSleepMinutes) / mChartMax) * ((float) i3)), i8, i5, next);
                } else {
                    int dayRestful = next.getDayRestful();
                    j(canvas, this.w, i9, i3, (int) ((((float) dayRestful) / mChartMax) * ((float) i3)), i8, i5, dayRestful, totalSleepMinutes);
                }
                i4 += i7;
                linkedList.add(Integer.valueOf(i9));
            }
            rp7.invoke(linkedList);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.x;
    }

    @DexIgnore
    public final void h(Canvas canvas, List<Integer> list) {
        Integer[] numArr;
        Exception e2;
        int i2 = 0;
        if (!list.isEmpty()) {
            int size = list.size();
            switch (size) {
                case 28:
                    numArr = new Integer[]{0, 7, 14, 21, 27};
                    break;
                case 29:
                    numArr = new Integer[]{0, 7, 14, 21, 28};
                    break;
                case 30:
                    numArr = new Integer[]{0, 7, 14, 21, 29};
                    break;
                default:
                    numArr = new Integer[]{0, 7, 14, 21, 30};
                    break;
            }
            try {
                int i3 = 0;
                for (Integer num : numArr) {
                    try {
                        i3 = num.intValue();
                        l(canvas, String.valueOf(i3 + 1), list.get(i3).intValue());
                    } catch (Exception e3) {
                        e2 = e3;
                        i2 = i3;
                        FLogger.INSTANCE.getLocal().d(H, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e2);
                    }
                }
            } catch (Exception e4) {
                e2 = e4;
                FLogger.INSTANCE.getLocal().d(H, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e2);
            }
        }
    }

    @DexIgnore
    public final void i(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.m);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.m);
    }

    @DexIgnore
    public final void j(Canvas canvas, Paint paint, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        int i9 = i2 - (i5 / 2);
        if (i9 < 0) {
            i9 = 0;
        }
        RectF rectF = new RectF((float) i9, (float) ((i3 - ((int) ((((float) i7) / ((float) i8)) * ((float) i4)))) + i6), (float) (i9 + i5), (float) i3);
        k37.d(canvas, rectF, paint, rectF.width() / ((float) 3));
    }

    @DexIgnore
    public final void k(Canvas canvas, List<Integer> list, int i2, int i3, int i4, int i5) {
        if ((!this.D.isEmpty()) && this.D.size() > 1) {
            float mChartMax = (float) getMChartMax();
            Path path = new Path();
            float height = (float) getStartBitmap().getHeight();
            int size = list.size();
            for (int i6 = 1; i6 < size; i6++) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = H;
                StringBuilder sb = new StringBuilder();
                sb.append("Previous sleep goal: ");
                int i7 = i6 - 1;
                sb.append(this.D.get(i7).getSleepGoal());
                sb.append(", current sleep goal: ");
                sb.append(this.D.get(i6).getSleepGoal());
                sb.append(", chart max: ");
                sb.append(mChartMax);
                local.d(str, sb.toString());
                float intValue = (float) list.get(i6).intValue();
                float f2 = (float) i3;
                float f3 = (float) i4;
                float f4 = (float) i5;
                height = Math.min(((1.0f - (((float) this.D.get(i6).getSleepGoal()) / mChartMax)) * f2) + f3, f4);
                float intValue2 = (float) list.get(i7).intValue();
                float min = Math.min(((1.0f - (((float) this.D.get(i7).getSleepGoal()) / mChartMax)) * f2) + f3, f4);
                if (height == min) {
                    path.moveTo(intValue2, min);
                    if (i6 == list.size() - 1) {
                        path.lineTo((float) (this.z + i2), height);
                    } else {
                        path.lineTo(intValue, height);
                    }
                    canvas.drawPath(path, this.s);
                } else {
                    path.moveTo(intValue2, min);
                    path.lineTo(intValue, min);
                    canvas.drawPath(path, this.s);
                    path.moveTo(intValue, min);
                    path.lineTo(intValue, height);
                    canvas.drawPath(path, this.s);
                    if (i6 == list.size() - 1) {
                        path.moveTo(intValue, height);
                        path.lineTo((float) (this.z + i2), height);
                        canvas.drawPath(path, this.s);
                    }
                }
            }
            canvas.drawBitmap(getStartBitmap(), (float) (this.z + i2), height - ((float) (getStartBitmap().getHeight() / 2)), this.t);
        }
    }

    @DexIgnore
    public final void l(Canvas canvas, String str, int i2) {
        canvas.drawText(str, ((float) i2) - (this.t.measureText(str, 0, str.length()) / ((float) 2)), (float) ((getHeight() - (new Rect().height() / 2)) - this.B), this.t);
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.m.setColor(this.e);
        float f2 = (float) 4;
        this.m.setStrokeWidth(f2);
        this.t.setColor(this.j);
        this.t.setStyle(Paint.Style.FILL);
        this.t.setTextSize((float) this.k);
        this.t.setTypeface(nl0.b(getContext(), this.l));
        this.s.setColor(this.f);
        this.s.setStyle(Paint.Style.STROKE);
        this.s.setStrokeWidth(f2);
        this.s.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.u.setColor(this.g);
        this.u.setStrokeWidth((float) this.y);
        this.u.setStyle(Paint.Style.FILL);
        this.v.setColor(this.h);
        this.v.setStrokeWidth((float) this.y);
        this.v.setStyle(Paint.Style.FILL);
        this.w.setColor(this.i);
        this.w.setStrokeWidth((float) this.y);
        this.w.setStyle(Paint.Style.FILL);
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }
}
