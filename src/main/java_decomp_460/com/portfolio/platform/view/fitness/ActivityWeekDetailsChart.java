package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.br7;
import com.fossil.cl7;
import com.fossil.dr7;
import com.fossil.gl0;
import com.fossil.k37;
import com.fossil.lq7;
import com.fossil.nl0;
import com.fossil.pq7;
import com.fossil.qq7;
import com.fossil.sq4;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.wt7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityWeekDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String F;
    @DexIgnore
    public /* final */ int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public /* final */ ArrayList<cl7<Float, Float>> C;
    @DexIgnore
    public float D;
    @DexIgnore
    public float E;
    @DexIgnore
    public /* final */ String[] e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ Paint m;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ Paint v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements vp7<Integer, List<? extends Integer>, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ br7 $bottomTextHeight;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $charactersCenterXList;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(br7 br7, dr7 dr7) {
            super(2);
            this.$bottomTextHeight = br7;
            this.$charactersCenterXList = dr7;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public /* bridge */ /* synthetic */ tl7 invoke(Integer num, List<? extends Integer> list) {
            invoke(num.intValue(), (List<Integer>) list);
            return tl7.f3441a;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<java.lang.Integer> */
        /* JADX WARN: Multi-variable type inference failed */
        public final void invoke(int i, List<Integer> list) {
            pq7.c(list, "list");
            this.$bottomTextHeight.element = i;
            this.$charactersCenterXList.element = list;
        }
    }

    /*
    static {
        String simpleName = ActivityWeekDetailsChart.class.getSimpleName();
        pq7.b(simpleName, "ActivityWeekDetailsChart::class.java.simpleName");
        F = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityWeekDetailsChart(Context context) {
        this(context, null, 0);
        pq7.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityWeekDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        pq7.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityWeekDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        pq7.c(context, "context");
        String[] stringArray = context.getResources().getStringArray(2130903040);
        pq7.b(stringArray, "context.resources.getStr\u2026ay.days_of_week_alphabet)");
        this.e = stringArray;
        this.m = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = new Paint(1);
        this.w = context.getResources().getDimensionPixelSize(2131165385);
        this.x = context.getResources().getDimensionPixelSize(2131165385);
        this.y = context.getResources().getDimensionPixelSize(2131165372);
        this.z = context.getResources().getDimensionPixelSize(2131165372);
        this.A = context.getResources().getDimensionPixelSize(2131165420);
        this.B = 4;
        this.C = new ArrayList<>();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, sq4.ActivityWeekDetailsChart, 0, 0));
        }
        int d = gl0.d(context, 2131099831);
        TypedArray mTypedArray = getMTypedArray();
        this.f = mTypedArray != null ? mTypedArray.getColor(3, d) : d;
        TypedArray mTypedArray2 = getMTypedArray();
        this.g = mTypedArray2 != null ? mTypedArray2.getColor(2, d) : d;
        TypedArray mTypedArray3 = getMTypedArray();
        this.h = mTypedArray3 != null ? mTypedArray3.getColor(1, d) : d;
        TypedArray mTypedArray4 = getMTypedArray();
        this.i = mTypedArray4 != null ? mTypedArray4.getColor(0, d) : d;
        TypedArray mTypedArray5 = getMTypedArray();
        this.j = mTypedArray5 != null ? mTypedArray5.getColor(4, d) : d;
        TypedArray mTypedArray6 = getMTypedArray();
        this.l = mTypedArray6 != null ? mTypedArray6.getDimensionPixelSize(6, 40) : 40;
        TypedArray mTypedArray7 = getMTypedArray();
        this.k = mTypedArray7 != null ? mTypedArray7.getResourceId(5, 2131296261) : 2131296261;
        TypedArray mTypedArray8 = getMTypedArray();
        if (mTypedArray8 != null) {
            mTypedArray8.recycle();
        }
    }

    @DexIgnore
    private final float getMChartMax() {
        return Math.max(getMMaxGoal(), getMMaxSleepMinutes());
    }

    @DexIgnore
    private final float getMMaxGoal() {
        T t2;
        Float f2;
        Iterator<T> it = this.C.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (it.hasNext()) {
                float floatValue = ((Number) next.getSecond()).floatValue();
                while (true) {
                    next = it.next();
                    floatValue = ((Number) next.getSecond()).floatValue();
                    if (Float.compare(floatValue, floatValue) >= 0) {
                        floatValue = floatValue;
                        next = next;
                    }
                    if (!it.hasNext()) {
                        break;
                    }
                }
            }
            t2 = next;
        }
        T t3 = t2;
        return (t3 == null || (f2 = (Float) t3.getSecond()) == null) ? this.D : f2.floatValue();
    }

    @DexIgnore
    private final float getMMaxSleepMinutes() {
        T t2;
        Float f2;
        Iterator<T> it = this.C.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (it.hasNext()) {
                float floatValue = ((Number) next.getFirst()).floatValue();
                while (true) {
                    next = it.next();
                    floatValue = ((Number) next.getFirst()).floatValue();
                    if (Float.compare(floatValue, floatValue) >= 0) {
                        floatValue = floatValue;
                        next = next;
                    }
                    if (!it.hasNext()) {
                        break;
                    }
                }
            }
            t2 = next;
        }
        T t3 = t2;
        return (t3 == null || (f2 = (Float) t3.getFirst()) == null) ? this.E : f2.floatValue();
    }

    @DexIgnore
    public final void c(Canvas canvas, List<Integer> list, int i2) {
        FLogger.INSTANCE.getLocal().d(F, "drawBars: " + list.size());
        if (!this.C.isEmpty()) {
            float mChartMax = getMChartMax();
            int size = list.size();
            for (int i3 = 0; i3 < size; i3++) {
                int intValue = list.get(i3).intValue();
                cl7<Float, Float> cl7 = this.C.get(i3);
                pq7.b(cl7, "mDataList[index]");
                float floatValue = cl7.getFirst().floatValue() / mChartMax;
                float f2 = (float) i2;
                int i4 = intValue - (this.z / 2);
                k37.d(canvas, new RectF((float) i4, Math.max(f2 - (floatValue * f2), (float) this.B), (float) (i4 + this.z), f2), floatValue < 1.0f ? this.u : this.v, ((float) this.z) / ((float) 3));
            }
        }
    }

    @DexIgnore
    public final void d(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.m);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.m);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(-1);
            int width = (getWidth() - getStartBitmap().getWidth()) - (this.A * 2);
            br7 br7 = new br7();
            br7.element = 0;
            dr7 dr7 = new dr7();
            dr7.element = null;
            e(canvas, width, new a(br7, dr7));
            int height = (getHeight() - br7.element) - this.y;
            T t2 = dr7.element;
            if (t2 != null) {
                c(canvas, t2, height);
                f(canvas, t2, width, height, height - 4);
            }
            d(canvas, 0, height);
        }
    }

    @DexIgnore
    public final void e(Canvas canvas, int i2, vp7<? super Integer, ? super List<Integer>, tl7> vp7) {
        int length = this.e.length;
        Rect rect = new Rect();
        String str = this.e[0];
        lq7 lq7 = lq7.f2236a;
        Paint paint = this.t;
        pq7.b(str, "sundayCharacter");
        paint.getTextBounds(str, 0, wt7.A(str) > 0 ? wt7.A(str) : 1, rect);
        float measureText = this.t.measureText(str);
        float height = (float) rect.height();
        float f2 = (((float) (i2 - (this.x * 2))) - (((float) length) * measureText)) / ((float) (length - 1));
        float height2 = (float) getHeight();
        float f3 = (float) 2;
        float f4 = height / f3;
        float f5 = (float) this.x;
        LinkedList linkedList = new LinkedList();
        for (String str2 : this.e) {
            canvas.drawText(str2, f5, height2 - f4, this.t);
            linkedList.add(Integer.valueOf((int) ((measureText / f3) + f5)));
            f5 += measureText + f2;
        }
        vp7.invoke(Integer.valueOf((int) height), linkedList);
    }

    @DexIgnore
    public final void f(Canvas canvas, List<Integer> list, int i2, int i3, int i4) {
        if ((!this.C.isEmpty()) && this.C.size() > 1) {
            float mChartMax = getMChartMax();
            if (mChartMax > ((float) 0)) {
                Path path = new Path();
                float height = (float) getStartBitmap().getHeight();
                int size = list.size();
                for (int i5 = 1; i5 < size; i5++) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = F;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Previous goal: ");
                    int i6 = i5 - 1;
                    sb.append(this.C.get(i6).getSecond().floatValue());
                    sb.append(", current goal: ");
                    sb.append(this.C.get(i5).getSecond().floatValue());
                    sb.append(", chart max: ");
                    sb.append(mChartMax);
                    local.d(str, sb.toString());
                    float intValue = (float) list.get(i5).intValue();
                    float f2 = (float) i3;
                    float f3 = (float) i4;
                    height = Math.max(Math.min((1.0f - (this.C.get(i5).getSecond().floatValue() / mChartMax)) * f2, f3), (float) this.B);
                    float intValue2 = (float) list.get(i6).intValue();
                    float max = Math.max(Math.min((1.0f - (this.C.get(i6).getSecond().floatValue() / mChartMax)) * f2, f3), (float) this.B);
                    if (height == max) {
                        path.moveTo(intValue2, max);
                        if (i5 == list.size() - 1) {
                            path.lineTo((float) (this.A + i2), height);
                        } else {
                            path.lineTo(intValue, height);
                        }
                        canvas.drawPath(path, this.s);
                    } else {
                        path.moveTo(intValue2, max);
                        path.lineTo(intValue, max);
                        canvas.drawPath(path, this.s);
                        path.moveTo(intValue, max);
                        path.lineTo(intValue, height);
                        canvas.drawPath(path, this.s);
                        if (i5 == list.size() - 1) {
                            path.moveTo(intValue, height);
                            path.lineTo((float) (this.A + i2), height);
                            canvas.drawPath(path, this.s);
                        }
                    }
                }
                canvas.drawBitmap(getStartBitmap(), (float) (this.A + i2), height - ((float) (getStartBitmap().getHeight() / 2)), this.t);
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.w;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.m.setColor(this.f);
        float f2 = (float) 4;
        this.m.setStrokeWidth(f2);
        this.t.setColor(this.j);
        this.t.setStyle(Paint.Style.FILL);
        this.t.setTextSize((float) this.l);
        this.t.setTypeface(nl0.b(getContext(), this.k));
        this.s.setColor(this.g);
        this.s.setStyle(Paint.Style.STROKE);
        this.s.setStrokeWidth(f2);
        this.s.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.u.setColor(this.h);
        this.u.setStrokeWidth((float) this.z);
        this.u.setStyle(Paint.Style.FILL);
        this.v.setColor(this.i);
        this.v.setStrokeWidth((float) this.z);
        this.v.setStyle(Paint.Style.FILL);
        this.B = getStartBitmap().getHeight() / 2;
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }
}
