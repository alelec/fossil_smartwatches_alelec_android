package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.Keep;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.a67;
import com.fossil.al7;
import com.fossil.cl7;
import com.fossil.nl0;
import com.fossil.pq7;
import com.fossil.sq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepHorizontalBar extends BaseFitnessChart {
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public boolean C;
    @DexIgnore
    public Typeface D;
    @DexIgnore
    public RectF E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public int K;
    @DexIgnore
    public Paint L;
    @DexIgnore
    public /* final */ Paint M;
    @DexIgnore
    public /* final */ Paint N;
    @DexIgnore
    public /* final */ Paint O;
    @DexIgnore
    public /* final */ Paint P;
    @DexIgnore
    public /* final */ Paint Q;
    @DexIgnore
    public ArrayList<LinkedList<cl7<a, Integer>>> e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public int i;
    @DexIgnore
    public String j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore
    public enum a {
        AWAKE,
        SLEEP,
        DEEP
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepHorizontalBar(Context context) {
        this(context, null, 0);
        pq7.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepHorizontalBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        pq7.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepHorizontalBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        TypedArray obtainStyledAttributes;
        pq7.c(context, "context");
        this.e = new ArrayList<>();
        this.j = "";
        this.t = 5.0f;
        this.u = 8.0f;
        this.v = 8.0f;
        this.w = 8.0f;
        this.x = 1.0f;
        this.y = 8;
        this.z = -1;
        this.A = 10;
        this.C = true;
        this.E = new RectF();
        this.J = 8.0f;
        this.M = new Paint(1);
        this.N = new Paint(1);
        this.O = new Paint(1);
        this.P = new Paint(1);
        this.Q = new Paint(1);
        if (!(attributeSet == null || (obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, sq4.SleepHorizontalBar, 0, 0)) == null)) {
            this.i = obtainStyledAttributes.getColor(0, 0);
            this.k = obtainStyledAttributes.getColor(3, 0);
            this.l = obtainStyledAttributes.getColor(2, 0);
            this.m = obtainStyledAttributes.getColor(7, 0);
            this.s = obtainStyledAttributes.getColor(4, 0);
            this.t = (float) obtainStyledAttributes.getDimensionPixelSize(6, 5);
            this.v = (float) obtainStyledAttributes.getDimensionPixelSize(8, 8);
            this.u = (float) obtainStyledAttributes.getDimensionPixelSize(9, 8);
            this.w = (float) obtainStyledAttributes.getDimensionPixelSize(5, 8);
            this.x = obtainStyledAttributes.getFloat(10, 1.0f);
            this.z = obtainStyledAttributes.getResourceId(11, -1);
            this.y = obtainStyledAttributes.getDimensionPixelSize(12, 8);
            String string = obtainStyledAttributes.getString(13);
            this.j = string == null ? "" : string;
            this.B = obtainStyledAttributes.getColor(14, 0);
            this.A = obtainStyledAttributes.getDimensionPixelSize(15, 10);
            try {
                this.D = nl0.b(getContext(), obtainStyledAttributes.getResourceId(1, -1));
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SleepHorizontalBar", "init - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        f();
    }

    @DexIgnore
    public final cl7<Bitmap, Canvas> c(int i2, int i3) {
        Bitmap createBitmap = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
        return new cl7<>(createBitmap, new Canvas(createBitmap));
    }

    @DexIgnore
    public final Bitmap d(Bitmap bitmap, float f2) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawARGB(0, 0, 0, 0);
        Paint paint = new Paint(1);
        canvas.drawRoundRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) bitmap.getWidth(), (float) bitmap.getHeight(), f2, f2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        canvas.drawBitmap(bitmap, rect, rect, paint);
        pq7.b(createBitmap, "output");
        return createBitmap;
    }

    @DexIgnore
    public final RectF e(RectF rectF, float f2) {
        float f3 = rectF.left + f2;
        float f4 = rectF.top + f2;
        float f5 = rectF.right - f2;
        float f6 = rectF.bottom - f2;
        float f7 = (float) 0;
        if (f5 <= f7 || f5 < f3) {
            f5 = f3 + f2;
        }
        if (f6 <= f7 || f6 < f4) {
            f6 = f4 + f2;
        }
        return new RectF(f3, f4, f5, f6);
    }

    @DexIgnore
    public final void f() {
        this.Q.setAntiAlias(true);
        this.Q.setStyle(Paint.Style.STROKE);
        this.M.setColor(this.k);
        this.M.setAntiAlias(true);
        this.M.setStyle(Paint.Style.FILL);
        this.L = new Paint(this.M);
        this.O.setAlpha((int) (this.x * ((float) 255)));
        this.O.setColorFilter(new PorterDuffColorFilter(this.k, PorterDuff.Mode.SRC_IN));
        this.O.setAntiAlias(true);
        this.P.setColor(-1);
        this.P.setAntiAlias(true);
        this.P.setStyle(Paint.Style.FILL);
        this.N.setColor(this.B);
        this.N.setAntiAlias(true);
        this.N.setStyle(Paint.Style.FILL);
        this.N.setTextSize((float) this.A);
        Typeface typeface = this.D;
        if (typeface != null) {
            this.N.setTypeface(typeface);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return this.z;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.y;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        float f2;
        int i2;
        super.onDraw(canvas);
        FLogger.INSTANCE.getLocal().d("SleepHorizontalBar", "onDraw - mTotalValue=" + this.g + ", mGoal=" + this.h);
        this.F = ((float) getWidth()) - this.w;
        if (canvas != null) {
            canvas.drawColor(this.i);
            RectF rectF = this.E;
            float f3 = this.f;
            float f4 = (float) 0;
            float f5 = f3 > f4 ? (this.F * this.g) / f3 : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f6 = this.u;
            float f7 = (float) 2;
            rectF.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f5 + (f6 * f7), f6 * ((float) 3));
            float f8 = this.g;
            if (f8 > f4) {
                if (f8 >= this.h) {
                    Paint paint = this.L;
                    if (paint != null) {
                        paint.setAlpha((int) 15.299999999999999d);
                        RectF rectF2 = this.E;
                        float f9 = (float) 4;
                        float f10 = this.t;
                        Paint paint2 = this.L;
                        if (paint2 != null) {
                            canvas.drawRoundRect(rectF2, f9 * f10, f9 * f10, paint2);
                            Paint paint3 = this.L;
                            if (paint3 != null) {
                                paint3.setAlpha((int) 40.800000000000004d);
                                RectF e2 = e(this.E, this.u / f7);
                                float f11 = this.t;
                                Paint paint4 = this.L;
                                if (paint4 != null) {
                                    canvas.drawRoundRect(e2, 2.0f * f11, f11 * 2.0f, paint4);
                                } else {
                                    pq7.n("mPaintProgressReach");
                                    throw null;
                                }
                            } else {
                                pq7.n("mPaintProgressReach");
                                throw null;
                            }
                        } else {
                            pq7.n("mPaintProgressReach");
                            throw null;
                        }
                    } else {
                        pq7.n("mPaintProgressReach");
                        throw null;
                    }
                }
                if (this.K != 0) {
                    RectF e3 = e(this.E, this.u);
                    float f12 = e3.right;
                    float f13 = e3.left;
                    float f14 = (f12 - f13) - this.J;
                    float f15 = e3.bottom - e3.top;
                    int i3 = this.K;
                    for (int i4 = 0; i4 < i3; i4++) {
                        LinkedList<cl7<a, Integer>> linkedList = this.e.get(i4);
                        pq7.b(linkedList, "mValues[k]");
                        LinkedList<cl7<a, Integer>> linkedList2 = linkedList;
                        Iterator<T> it = linkedList2.iterator();
                        int i5 = 0;
                        while (it.hasNext()) {
                            i5 = ((Number) it.next().getSecond()).intValue() + i5;
                        }
                        float f16 = (((float) i5) * f14) / this.g;
                        if (f16 < ((float) 1)) {
                            f2 = this.u;
                        } else {
                            f2 = this.F;
                            if (f16 <= f2) {
                                f2 = f16;
                            }
                        }
                        cl7<Bitmap, Canvas> c = c((int) f2, (int) f15);
                        Bitmap first = c.getFirst();
                        Canvas second = c.getSecond();
                        int size = linkedList2.size();
                        float f17 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                        int i6 = 0;
                        while (i6 < size) {
                            int i7 = a67.f204a[linkedList2.get(i6).getFirst().ordinal()];
                            if (i7 == 1) {
                                i2 = this.l;
                            } else if (i7 == 2) {
                                i2 = this.m;
                            } else if (i7 == 3) {
                                i2 = this.s;
                            } else {
                                throw new al7();
                            }
                            float floatValue = (linkedList2.get(i6).getSecond().floatValue() * f14) / this.g;
                            this.M.setColor(i2);
                            float f18 = f17 + floatValue;
                            second.drawRect(f17, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f18, f15, this.M);
                            i6++;
                            f17 = f18;
                        }
                        Bitmap d = d(first, this.t);
                        canvas.drawBitmap(d, f13, e3.top, this.M);
                        first.recycle();
                        d.recycle();
                        f13 += this.v + f17;
                    }
                }
            }
            float height = ((float) getHeight()) / 2.0f;
            float f19 = this.f;
            float f20 = f19 > f4 ? (this.F * this.h) / f19 : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f21 = this.u;
            float f22 = f20 + f21;
            this.G = f22;
            if (this.g <= this.h) {
                canvas.drawBitmap(getStartBitmap(), this.G - ((float) getStartBitmap().getWidth()), height - ((float) (getStartBitmap().getHeight() / 2)), this.O);
            } else {
                canvas.drawCircle(f22 - (((float) this.y) / 2.0f), height, f21 / 4.0f, this.P);
            }
            if (this.C) {
                canvas.drawText(this.j, (((float) getWidth()) - this.H) - f7, height + (this.I / f7), this.N);
            }
        }
    }

    @DexIgnore
    public void onGlobalLayout() {
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode != Integer.MIN_VALUE) {
            if (mode == 0) {
                size = (int) (this.u * ((float) 3));
            } else if (mode != 1073741824) {
                size = 0;
            }
        }
        setMeasuredDimension(getWidth(), size);
    }

    @DexIgnore
    @Keep
    public final void setMax(float f2) {
        this.f = f2;
        invalidate();
    }
}
