package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.dd1;
import com.fossil.ej1;
import com.fossil.fj1;
import com.fossil.gb1;
import com.fossil.gl0;
import com.fossil.gp0;
import com.fossil.hk5;
import com.fossil.il7;
import com.fossil.p47;
import com.fossil.pq7;
import com.fossil.qj1;
import com.fossil.sq4;
import com.fossil.tj5;
import com.fossil.wc1;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.wj5;
import com.fossil.yi1;
import com.fossil.yj5;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationSummaryDialView extends ViewGroup {
    @DexIgnore
    public static /* final */ String y;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Paint g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public ViewGroup.LayoutParams m;
    @DexIgnore
    public SparseArray<Bitmap> s; // = new SparseArray<>();
    @DexIgnore
    public /* final */ Handler t; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public Bitmap u;
    @DexIgnore
    public SparseArray<List<BaseFeatureModel>> v; // = new SparseArray<>();
    @DexIgnore
    public SparseArray<List<BaseFeatureModel>> w; // = new SparseArray<>();
    @DexIgnore
    public a x;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSummaryDialView b;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList c;

        @DexIgnore
        public b(NotificationSummaryDialView notificationSummaryDialView, ArrayList arrayList) {
            this.b = notificationSummaryDialView;
            this.c = arrayList;
        }

        @DexIgnore
        public final void run() {
            this.b.g();
            Iterator it = this.c.iterator();
            while (it.hasNext()) {
                AppCompatImageView appCompatImageView = (AppCompatImageView) it.next();
                if (appCompatImageView != null) {
                    try {
                        this.b.addView(appCompatImageView);
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = NotificationSummaryDialView.y;
                        local.d(str, "onLoadBitmapAsyncComplete exception " + e);
                    }
                }
            }
            this.b.requestLayout();
            this.b.w.clear();
            if (this.b.v.size() > 0) {
                FLogger.INSTANCE.getLocal().d(NotificationSummaryDialView.y, "onLoadBitmapAsyncComplete pendingList exists, start set again");
                NotificationSummaryDialView notificationSummaryDialView = this.b;
                SparseArray<List<BaseFeatureModel>> clone = notificationSummaryDialView.v.clone();
                pq7.b(clone, "mPendingData.clone()");
                notificationSummaryDialView.setDataAsync(clone);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSummaryDialView b;

        @DexIgnore
        public c(NotificationSummaryDialView notificationSummaryDialView) {
            this.b = notificationSummaryDialView;
        }

        @DexIgnore
        public final void onClick(View view) {
            a aVar = this.b.x;
            if (aVar != null) {
                Object tag = view.getTag(123456789);
                if (tag != null) {
                    aVar.a(((Integer) tag).intValue());
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Int");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ej1<Bitmap> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSummaryDialView b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ AppCompatImageView d;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList e;

        @DexIgnore
        public d(NotificationSummaryDialView notificationSummaryDialView, int i, AppCompatImageView appCompatImageView, ArrayList arrayList) {
            this.b = notificationSummaryDialView;
            this.c = i;
            this.d = appCompatImageView;
            this.e = arrayList;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean g(Bitmap bitmap, Object obj, qj1<Bitmap> qj1, gb1 gb1, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = NotificationSummaryDialView.y;
            local.d(str, "setDataAsync onResourceReady " + obj);
            if (bitmap != null) {
                this.b.s.put(this.c, bitmap);
                this.d.setImageBitmap(bitmap);
            }
            this.e.add(this.d);
            this.b.m(this.e);
            return false;
        }

        @DexIgnore
        @Override // com.fossil.ej1
        public boolean e(dd1 dd1, Object obj, qj1<Bitmap> qj1, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = NotificationSummaryDialView.y;
            local.d(str, "setDataAsync onLoadFailed " + obj);
            if (this.b.s.get(this.c) != null) {
                this.d.setImageBitmap((Bitmap) this.b.s.get(this.c));
            }
            this.e.add(this.d);
            this.b.m(this.e);
            return false;
        }
    }

    /*
    static {
        String simpleName = NotificationSummaryDialView.class.getSimpleName();
        pq7.b(simpleName, "NotificationSummaryDialView::class.java.simpleName");
        y = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationSummaryDialView(Context context) {
        super(context);
        pq7.c(context, "context");
        l();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationSummaryDialView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        pq7.c(context, "context");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.NotificationSummaryDialView);
        this.b = obtainStyledAttributes.getResourceId(3, 2131231137);
        this.c = obtainStyledAttributes.getColor(4, gl0.d(context, 2131099967));
        this.d = obtainStyledAttributes.getResourceId(1, -1);
        this.e = (int) obtainStyledAttributes.getDimension(2, p47.b(15.0f));
        this.f = obtainStyledAttributes.getResourceId(0, R.drawable.ic_launcher_transparent);
        this.u = BitmapFactory.decodeResource(getResources(), this.f);
        obtainStyledAttributes.recycle();
        l();
        i();
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        pq7.c(canvas, "canvas");
        FLogger.INSTANCE.getLocal().d(y, "dispatchDraw");
        j(canvas);
        super.dispatchDraw(canvas);
        k();
    }

    @DexIgnore
    public final void g() {
        removeAllViews();
    }

    @DexIgnore
    public final void h(AppCompatImageView appCompatImageView, int i2) {
        double d2 = ((double) (i2 - 3)) * 0.5235987755982988d;
        int i3 = this.k;
        int width = (int) (((double) ((getWidth() / 2) - (i3 / 2))) + (((double) this.l) * Math.cos(d2)));
        int sin = (int) ((Math.sin(d2) * ((double) this.l)) + ((double) ((getHeight() / 2) - (i3 / 2))));
        appCompatImageView.layout(width, sin, width + i3, i3 + sin);
    }

    @DexIgnore
    public final void i() {
        setDataAsync(new SparseArray<>());
    }

    @DexIgnore
    public final void j(Canvas canvas) {
        FLogger.INSTANCE.getLocal().d(y, "initClockImage");
        int i2 = this.k;
        float f2 = ((float) i2) * 1.5f;
        float f3 = ((float) this.h) - (((float) i2) * 1.5f);
        float f4 = f3 - f2;
        float f5 = (((float) this.i) - f4) / ((float) 2);
        Bitmap bitmap = this.u;
        if (bitmap != null) {
            Rect rect = new Rect((int) f2, (int) f5, (int) f3, (int) (f5 + f4));
            Paint paint = this.g;
            if (paint != null) {
                canvas.drawBitmap(bitmap, (Rect) null, rect, paint);
            } else {
                pq7.n("mBitmapPaint");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void k() {
        FLogger.INSTANCE.getLocal().d(y, "initParams");
        this.h = getMeasuredWidth();
        this.i = getMeasuredHeight();
        ViewGroup.LayoutParams layoutParams = this.m;
        if (layoutParams != null) {
            layoutParams.width = this.k;
        }
        ViewGroup.LayoutParams layoutParams2 = this.m;
        if (layoutParams2 != null) {
            layoutParams2.height = this.k;
        }
        int min = Math.min(this.h, this.i) / 2;
        this.j = min;
        this.k = (int) (((float) min) / 3.5f);
    }

    @DexIgnore
    public final void l() {
        Paint paint = new Paint(1);
        this.g = paint;
        if (paint != null) {
            paint.setStyle(Paint.Style.FILL);
            this.m = new ViewGroup.LayoutParams(-1, -1);
            return;
        }
        pq7.n("mBitmapPaint");
        throw null;
    }

    @DexIgnore
    public final void m(ArrayList<AppCompatImageView> arrayList) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.d(str, "onLoadBitmapAsyncComplete size " + arrayList.size());
        if (arrayList.size() == 12) {
            this.t.post(new b(this, arrayList));
        }
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        this.t.removeCallbacksAndMessages(null);
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        this.l = (Math.min((i4 - i2) - 100, i5 - i3) / 2) - (this.k / 2);
        destroyDrawingCache();
        buildDrawingCache();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt != null) {
                AppCompatImageView appCompatImageView = (AppCompatImageView) childAt;
                appCompatImageView.setLayoutParams(this.m);
                Object tag = appCompatImageView.getTag(123456789);
                if (tag != null) {
                    h(appCompatImageView, ((Integer) tag).intValue());
                } else {
                    throw new il7("null cannot be cast to non-null type kotlin.Int");
                }
            } else {
                throw new il7("null cannot be cast to non-null type androidx.appcompat.widget.AppCompatImageView");
            }
        }
    }

    @DexIgnore
    public final void setDataAsync(SparseArray<List<BaseFeatureModel>> sparseArray) {
        pq7.c(sparseArray, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.d(str, "setDataAsync data size " + sparseArray.size());
        int size = this.w.size();
        this.v.clear();
        if (size > 0) {
            SparseArray<List<BaseFeatureModel>> clone = sparseArray.clone();
            pq7.b(clone, "data.clone()");
            this.v = clone;
            FLogger.INSTANCE.getLocal().d(y, "setDataAsync executing in progress, put new list in pending list");
            return;
        }
        SparseArray<List<BaseFeatureModel>> clone2 = sparseArray.clone();
        pq7.b(clone2, "data.clone()");
        this.w = clone2;
        ArrayList<AppCompatImageView> arrayList = new ArrayList<>();
        wj5 a2 = tj5.a(getContext());
        pq7.b(a2, "GlideApp.with(context)");
        TypedValue typedValue = new TypedValue();
        ColorStateList valueOf = ColorStateList.valueOf(this.c);
        pq7.b(valueOf, "ColorStateList.valueOf(mDialItemTintColor)");
        yi1<?> m0 = ((fj1) ((fj1) new fj1().o0(new hk5())).l(wc1.f3916a)).m0(true);
        pq7.b(m0, "RequestOptions()\n       \u2026   .skipMemoryCache(true)");
        fj1 fj1 = (fj1) m0;
        Context context = getContext();
        pq7.b(context, "context");
        context.getTheme().resolveAttribute(16843534, typedValue, true);
        for (int i2 = 1; i2 <= 12; i2++) {
            AppCompatImageView appCompatImageView = new AppCompatImageView(getContext());
            appCompatImageView.setLayoutParams(this.m);
            appCompatImageView.setTag(123456789, Integer.valueOf(i2));
            appCompatImageView.setOnClickListener(new c(this));
            if (sparseArray.get(i2) != null) {
                pq7.b(a2.e().M0(new yj5(sparseArray.get(i2))).b1(new d(this, i2, appCompatImageView, arrayList)).u0(fj1).l(wc1.f3916a).Q0(), "glideApp.asBitmap()\n    \u2026                .submit()");
            } else {
                appCompatImageView.setImageResource(this.b);
                int i3 = this.e;
                appCompatImageView.setPadding(i3, i3, i3, i3);
                appCompatImageView.setElevation(50.0f);
                int i4 = this.d;
                if (i4 != -1) {
                    appCompatImageView.setBackgroundResource(i4);
                } else {
                    appCompatImageView.setBackgroundResource(typedValue.resourceId);
                }
                gp0.c(appCompatImageView, valueOf);
                arrayList.add(appCompatImageView);
            }
        }
        m(arrayList);
    }

    @DexIgnore
    public final void setOnItemClickListener(a aVar) {
        pq7.c(aVar, "listener");
        this.x = aVar;
    }
}
