package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.em7;
import com.fossil.gl0;
import com.fossil.jt7;
import com.fossil.p47;
import com.fossil.pq7;
import com.fossil.qn5;
import com.fossil.sq4;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecyclerViewAlphabetIndex extends RecyclerView {
    @DexIgnore
    public String[] b; // = {"#", "A", "B", "C", "D", "E", DeviceIdentityUtils.FLASH_SERIAL_NUMBER_PREFIX, "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX, "T", "U", "V", "W", "X", "Y", "Z"};
    @DexIgnore
    public float c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public Typeface f;
    @DexIgnore
    public a g;
    @DexIgnore
    public LinearLayoutManager h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.g<C0360a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Typeface f4763a;
        @DexIgnore
        public /* final */ Typeface b;
        @DexIgnore
        public String[] c;
        @DexIgnore
        public int d;
        @DexIgnore
        public b e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex$a$a")
        /* renamed from: com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex$a$a  reason: collision with other inner class name */
        public final class C0360a extends RecyclerView.ViewHolder {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ FlexibleTextView f4764a;
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex$a$a$a")
            /* renamed from: com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex$a$a$a  reason: collision with other inner class name */
            public static final class View$OnClickListenerC0361a implements View.OnClickListener {
                @DexIgnore
                public /* final */ /* synthetic */ C0360a b;

                @DexIgnore
                public View$OnClickListenerC0361a(C0360a aVar) {
                    this.b = aVar;
                }

                @DexIgnore
                public final void onClick(View view) {
                    int adapterPosition = this.b.getAdapterPosition();
                    if (adapterPosition != -1) {
                        String str = "" + this.b.a().getText().toString();
                        if (RecyclerViewAlphabetIndex.this.e()) {
                            RecyclerViewAlphabetIndex.this.setLetterToBold(str);
                        }
                        b g = this.b.b.g();
                        if (g != null) {
                            pq7.b(view, "it");
                            g.a(view, adapterPosition, str);
                        }
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0360a(a aVar, View view) {
                super(view);
                pq7.c(view, "itemView");
                this.b = aVar;
                View findViewById = view.findViewById(2131363249);
                if (findViewById != null) {
                    this.f4764a = (FlexibleTextView) findViewById;
                    view.setOnClickListener(new View$OnClickListenerC0361a(this));
                    return;
                }
                pq7.i();
                throw null;
            }

            @DexIgnore
            public final FlexibleTextView a() {
                return this.f4764a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a() {
            Typeface defaultFromStyle = Typeface.defaultFromStyle(0);
            if (defaultFromStyle != null) {
                this.f4763a = defaultFromStyle;
                this.b = Typeface.defaultFromStyle(1);
                return;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        public final b g() {
            return this.e;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public int getItemCount() {
            String[] strArr = this.c;
            if (strArr != null) {
                return strArr.length;
            }
            return 0;
        }

        @DexIgnore
        /* renamed from: h */
        public void onBindViewHolder(C0360a aVar, int i) {
            pq7.c(aVar, "holder");
            String[] strArr = this.c;
            if (strArr != null) {
                aVar.a().setText(strArr[i]);
                if (RecyclerViewAlphabetIndex.this.e()) {
                    aVar.a().setTypeface(i == this.d ? this.b : this.f4763a);
                    aVar.a().setTextSize(2, RecyclerViewAlphabetIndex.this.getMFontSize$app_fossilRelease());
                    if (RecyclerViewAlphabetIndex.this.getMItemColor$app_fossilRelease() != 0) {
                        aVar.a().setTextColor(RecyclerViewAlphabetIndex.this.getMItemColor$app_fossilRelease());
                    }
                    if (RecyclerViewAlphabetIndex.this.getTypeFaceBaseTheme$app_fossilRelease() != null) {
                        aVar.a().setTypeface(RecyclerViewAlphabetIndex.this.getTypeFaceBaseTheme$app_fossilRelease());
                        return;
                    }
                    return;
                }
                return;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* renamed from: i */
        public C0360a onCreateViewHolder(ViewGroup viewGroup, int i) {
            pq7.c(viewGroup, "parent");
            View inflate = LayoutInflater.from(RecyclerViewAlphabetIndex.this.getContext()).inflate(2131558702, viewGroup, false);
            pq7.b(inflate, "view");
            return new C0360a(this, inflate);
        }

        @DexIgnore
        public final void j(b bVar) {
            pq7.c(bVar, "sectionIndexClickListener");
            this.e = bVar;
        }

        @DexIgnore
        public final void k(String[] strArr) {
            pq7.c(strArr, "alphabet");
            this.c = strArr;
            notifyDataSetChanged();
        }

        @DexIgnore
        public final void l(int i) {
            this.d = i;
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(View view, int i, String str);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewAlphabetIndex(Context context) {
        super(context);
        pq7.c(context, "context");
        setOverScrollMode(2);
        d();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewAlphabetIndex(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
        setOverScrollMode(2);
        c(context, attributeSet);
        d();
    }

    @DexIgnore
    private final void setAlphabet(String[] strArr) {
        Arrays.sort(strArr);
        a aVar = this.g;
        if (aVar != null) {
            aVar.k(strArr);
        }
    }

    @DexIgnore
    public final void a() {
        setAlphabet(this.b);
    }

    @DexIgnore
    public final void c(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.RecyclerViewAlphabetIndex);
        obtainStyledAttributes.getDimensionPixelSize(4, 15);
        this.c = obtainStyledAttributes.getDimension(1, (float) p47.m(3.0f));
        this.d = obtainStyledAttributes.getColor(2, gl0.d(context, 2131099967));
        this.e = obtainStyledAttributes.getBoolean(0, false);
        obtainStyledAttributes.recycle();
        String d2 = qn5.l.a().d("primaryText");
        this.f = qn5.l.a().f("descriptionText2");
        if (!TextUtils.isEmpty(d2)) {
            this.d = Color.parseColor(d2);
        }
    }

    @DexIgnore
    public final void d() {
        this.g = new a();
        this.h = new LinearLayoutManager(getContext(), 1, false);
        setHasFixedSize(true);
        setAdapter(this.g);
        setLayoutManager(this.h);
    }

    @DexIgnore
    public final boolean e() {
        return this.e;
    }

    @DexIgnore
    public final float getMFontSize$app_fossilRelease() {
        return this.c;
    }

    @DexIgnore
    public final int getMItemColor$app_fossilRelease() {
        return this.d;
    }

    @DexIgnore
    public final Typeface getTypeFaceBaseTheme$app_fossilRelease() {
        return this.f;
    }

    @DexIgnore
    public final void setCustomizable$app_fossilRelease(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public final void setLetterToBold(String str) {
        pq7.c(str, "letter");
        int Q = em7.Q(this.b, str);
        if (new jt7("[0-9]+").matches(str)) {
            Q = this.b.length - 1;
        }
        LinearLayoutManager linearLayoutManager = this.h;
        if (linearLayoutManager != null) {
            linearLayoutManager.D2(Q, 0);
        }
        a aVar = this.g;
        if (aVar != null) {
            aVar.l(Q);
        }
    }

    @DexIgnore
    public final void setMFontSize$app_fossilRelease(float f2) {
        this.c = f2;
    }

    @DexIgnore
    public final void setMItemColor$app_fossilRelease(int i) {
        this.d = i;
    }

    @DexIgnore
    public final void setOnSectionIndexClickListener(b bVar) {
        pq7.c(bVar, "sectionIndexClickListener");
        a aVar = this.g;
        if (aVar != null) {
            aVar.j(bVar);
        }
    }

    @DexIgnore
    public final void setTypeFaceBaseTheme$app_fossilRelease(Typeface typeface) {
        this.f = typeface;
    }
}
