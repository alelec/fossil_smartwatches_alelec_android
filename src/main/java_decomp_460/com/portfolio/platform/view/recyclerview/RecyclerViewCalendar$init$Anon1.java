package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.o37;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecyclerViewCalendar$init$Anon1 extends GridLayoutManager {
    @DexIgnore
    public /* final */ /* synthetic */ Context R;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewCalendar$init$Anon1(Context context, Context context2, int i, int i2, boolean z) {
        super(context2, i, i2, z);
        this.R = context;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.m, androidx.recyclerview.widget.LinearLayoutManager
    public boolean l() {
        return false;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.LinearLayoutManager
    public int p2(RecyclerView.State state) {
        pq7.c(state, "state");
        return o37.a(this.R);
    }
}
