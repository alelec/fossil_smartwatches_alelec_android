package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Scroller;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.qn5;
import com.fossil.sq4;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NumberPicker extends LinearLayout {
    @DexIgnore
    public /* final */ int[] A;
    @DexIgnore
    public /* final */ Paint B;
    @DexIgnore
    public /* final */ Drawable C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public /* final */ Scroller G;
    @DexIgnore
    public /* final */ Scroller H;
    @DexIgnore
    public int I;
    @DexIgnore
    public e J;
    @DexIgnore
    public d K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public VelocityTracker N;
    @DexIgnore
    public int O;
    @DexIgnore
    public int P;
    @DexIgnore
    public int Q;
    @DexIgnore
    public boolean R;
    @DexIgnore
    public /* final */ int S;
    @DexIgnore
    public /* final */ boolean T;
    @DexIgnore
    public /* final */ Drawable U;
    @DexIgnore
    public /* final */ int V;
    @DexIgnore
    public int W;
    @DexIgnore
    public boolean a0;
    @DexIgnore
    public String b;
    @DexIgnore
    public boolean b0;
    @DexIgnore
    public String c;
    @DexIgnore
    public int c0;
    @DexIgnore
    public /* final */ ImageButton d;
    @DexIgnore
    public int d0;
    @DexIgnore
    public /* final */ ImageButton e;
    @DexIgnore
    public int e0;
    @DexIgnore
    public /* final */ AppCompatEditText f;
    @DexIgnore
    public boolean f0;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public boolean g0;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public i h0;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ h i0;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public int j0;
    @DexIgnore
    public int k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public int s;
    @DexIgnore
    public String[] t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public g x;
    @DexIgnore
    public f y;
    @DexIgnore
    public /* final */ SparseArray<String> z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class CustomEditText extends AppCompatEditText {
        @DexIgnore
        public CustomEditText(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        @DexIgnore
        public void onEditorAction(int i) {
            super.onEditorAction(i);
            if (i == 6) {
                clearFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            NumberPicker.this.i();
            NumberPicker.this.f.clearFocus();
            if (view.getId() == 2131362888) {
                NumberPicker.this.a(true);
            } else {
                NumberPicker.this.a(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnLongClickListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            NumberPicker.this.i();
            NumberPicker.this.f.clearFocus();
            if (view.getId() == 2131362888) {
                NumberPicker.this.t(true, 0);
            } else {
                NumberPicker.this.t(false, 0);
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends AccessibilityNodeProvider {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Rect f4745a; // = new Rect();
        @DexIgnore
        public /* final */ int[] b; // = new int[2];
        @DexIgnore
        public int c; // = RecyclerView.UNDEFINED_DURATION;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public final AccessibilityNodeInfo a(int i, int i2, int i3, int i4) {
            AccessibilityNodeInfo obtain = AccessibilityNodeInfo.obtain();
            obtain.setClassName(NumberPicker.class.getName());
            obtain.setPackageName(NumberPicker.this.getContext().getPackageName());
            obtain.setSource(NumberPicker.this);
            if (g()) {
                obtain.addChild(NumberPicker.this, 3);
            }
            obtain.addChild(NumberPicker.this, 2);
            if (h()) {
                obtain.addChild(NumberPicker.this, 1);
            }
            obtain.setParent((View) NumberPicker.this.getParentForAccessibility());
            obtain.setEnabled(NumberPicker.this.isEnabled());
            obtain.setScrollable(true);
            if (this.c != -1) {
                obtain.addAction(64);
            }
            if (this.c == -1) {
                obtain.addAction(128);
            }
            if (NumberPicker.this.isEnabled()) {
                if (NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() < NumberPicker.this.getMaxValue()) {
                    obtain.addAction(4096);
                }
                if (NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() > NumberPicker.this.getMinValue()) {
                    obtain.addAction(8192);
                }
            }
            return obtain;
        }

        @DexIgnore
        public final AccessibilityNodeInfo b(int i, String str, int i2, int i3, int i4, int i5) {
            AccessibilityNodeInfo obtain = AccessibilityNodeInfo.obtain();
            obtain.setClassName(Button.class.getName());
            obtain.setPackageName(NumberPicker.this.getContext().getPackageName());
            obtain.setSource(NumberPicker.this, i);
            obtain.setParent(NumberPicker.this);
            obtain.setText(str);
            obtain.setClickable(true);
            obtain.setLongClickable(true);
            obtain.setEnabled(NumberPicker.this.isEnabled());
            Rect rect = this.f4745a;
            rect.set(i2, i3, i4, i5);
            obtain.setBoundsInParent(rect);
            int[] iArr = this.b;
            NumberPicker.this.getLocationOnScreen(iArr);
            rect.offset(iArr[0], iArr[1]);
            obtain.setBoundsInScreen(rect);
            if (this.c != i) {
                obtain.addAction(64);
            }
            if (this.c == i) {
                obtain.addAction(128);
            }
            if (NumberPicker.this.isEnabled()) {
                obtain.addAction(16);
            }
            return obtain;
        }

        @DexIgnore
        public final AccessibilityNodeInfo c() {
            AccessibilityNodeInfo createAccessibilityNodeInfo = NumberPicker.this.f.createAccessibilityNodeInfo();
            createAccessibilityNodeInfo.setSource(NumberPicker.this, 2);
            if (this.c != 2) {
                createAccessibilityNodeInfo.addAction(64);
            }
            if (this.c == 2) {
                createAccessibilityNodeInfo.addAction(128);
            }
            return createAccessibilityNodeInfo;
        }

        @DexIgnore
        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            if (i == -1) {
                return a(NumberPicker.this.getScrollX(), NumberPicker.this.getScrollY(), NumberPicker.this.getScrollX() + (NumberPicker.this.getRight() - NumberPicker.this.getLeft()), NumberPicker.this.getScrollY() + (NumberPicker.this.getBottom() - NumberPicker.this.getTop()));
            }
            if (i == 1) {
                String f = f();
                int scrollX = NumberPicker.this.getScrollX();
                NumberPicker numberPicker = NumberPicker.this;
                return b(1, f, scrollX, numberPicker.d0 - numberPicker.V, numberPicker.getScrollX() + (NumberPicker.this.getRight() - NumberPicker.this.getLeft()), NumberPicker.this.getScrollY() + (NumberPicker.this.getBottom() - NumberPicker.this.getTop()));
            } else if (i == 2) {
                return c();
            } else {
                if (i != 3) {
                    return super.createAccessibilityNodeInfo(i);
                }
                String e = e();
                int scrollX2 = NumberPicker.this.getScrollX();
                int scrollY = NumberPicker.this.getScrollY();
                int scrollX3 = NumberPicker.this.getScrollX();
                int right = NumberPicker.this.getRight();
                int left = NumberPicker.this.getLeft();
                NumberPicker numberPicker2 = NumberPicker.this;
                return b(3, e, scrollX2, scrollY, (right - left) + scrollX3, numberPicker2.V + numberPicker2.c0);
            }
        }

        @DexIgnore
        public final void d(String str, int i, List<AccessibilityNodeInfo> list) {
            if (i == 1) {
                String f = f();
                if (!TextUtils.isEmpty(f) && f.toLowerCase().contains(str)) {
                    list.add(createAccessibilityNodeInfo(1));
                }
            } else if (i == 2) {
                Editable text = NumberPicker.this.f.getText();
                if (TextUtils.isEmpty(text) || !text.toString().toLowerCase().contains(str)) {
                    Editable text2 = NumberPicker.this.f.getText();
                    if (!TextUtils.isEmpty(text2) && text2.toString().toLowerCase().contains(str)) {
                        list.add(createAccessibilityNodeInfo(2));
                        return;
                    }
                    return;
                }
                list.add(createAccessibilityNodeInfo(2));
            } else if (i == 3) {
                String e = e();
                if (!TextUtils.isEmpty(e) && e.toLowerCase().contains(str)) {
                    list.add(createAccessibilityNodeInfo(3));
                }
            }
        }

        @DexIgnore
        public final String e() {
            NumberPicker numberPicker = NumberPicker.this;
            int i = numberPicker.w - 1;
            if (numberPicker.R) {
                i = numberPicker.h(i);
            }
            NumberPicker numberPicker2 = NumberPicker.this;
            int i2 = numberPicker2.u;
            if (i < i2) {
                return null;
            }
            String[] strArr = numberPicker2.t;
            return strArr == null ? numberPicker2.f(i) : strArr[i - i2];
        }

        @DexIgnore
        public final String f() {
            NumberPicker numberPicker = NumberPicker.this;
            int i = numberPicker.w + 1;
            if (numberPicker.R) {
                i = numberPicker.h(i);
            }
            NumberPicker numberPicker2 = NumberPicker.this;
            if (i > numberPicker2.v) {
                return null;
            }
            String[] strArr = numberPicker2.t;
            return strArr == null ? numberPicker2.f(i) : strArr[i - numberPicker2.u];
        }

        @DexIgnore
        @Override // android.view.accessibility.AccessibilityNodeProvider
        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            if (TextUtils.isEmpty(str)) {
                return Collections.emptyList();
            }
            String lowerCase = str.toLowerCase();
            ArrayList arrayList = new ArrayList();
            if (i == -1) {
                d(lowerCase, 3, arrayList);
                d(lowerCase, 2, arrayList);
                d(lowerCase, 1, arrayList);
                return arrayList;
            } else if (i != 1 && i != 2 && i != 3) {
                return super.findAccessibilityNodeInfosByText(str, i);
            } else {
                d(lowerCase, i, arrayList);
                return arrayList;
            }
        }

        @DexIgnore
        public final boolean g() {
            return NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() > NumberPicker.this.getMinValue();
        }

        @DexIgnore
        public final boolean h() {
            return NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() < NumberPicker.this.getMaxValue();
        }

        @DexIgnore
        public final void i(int i, int i2, String str) {
            if (((AccessibilityManager) NumberPicker.this.getContext().getSystemService("accessibility")).isEnabled()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
                obtain.setClassName(Button.class.getName());
                obtain.setPackageName(NumberPicker.this.getContext().getPackageName());
                obtain.getText().add(str);
                obtain.setEnabled(NumberPicker.this.isEnabled());
                obtain.setSource(NumberPicker.this, i);
                NumberPicker numberPicker = NumberPicker.this;
                numberPicker.requestSendAccessibilityEvent(numberPicker, obtain);
            }
        }

        @DexIgnore
        public final void j(int i) {
            if (((AccessibilityManager) NumberPicker.this.getContext().getSystemService("accessibility")).isEnabled()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i);
                NumberPicker.this.f.onInitializeAccessibilityEvent(obtain);
                NumberPicker.this.f.onPopulateAccessibilityEvent(obtain);
                obtain.setSource(NumberPicker.this, 2);
                NumberPicker numberPicker = NumberPicker.this;
                numberPicker.requestSendAccessibilityEvent(numberPicker, obtain);
            }
        }

        @DexIgnore
        public void k(int i, int i2) {
            if (i != 1) {
                if (i == 2) {
                    j(i2);
                } else if (i == 3 && g()) {
                    i(i, i2, e());
                }
            } else if (h()) {
                i(i, i2, f());
            }
        }

        @DexIgnore
        public boolean performAction(int i, int i2, Bundle bundle) {
            if (i != -1) {
                if (i != 1) {
                    if (i != 2) {
                        if (i == 3) {
                            if (i2 != 16) {
                                if (i2 != 64) {
                                    if (i2 != 128 || this.c != i) {
                                        return false;
                                    }
                                    this.c = RecyclerView.UNDEFINED_DURATION;
                                    k(i, 65536);
                                    NumberPicker numberPicker = NumberPicker.this;
                                    numberPicker.invalidate(0, 0, numberPicker.getRight(), NumberPicker.this.c0);
                                    return true;
                                } else if (this.c == i) {
                                    return false;
                                } else {
                                    this.c = i;
                                    k(i, 32768);
                                    NumberPicker numberPicker2 = NumberPicker.this;
                                    numberPicker2.invalidate(0, 0, numberPicker2.getRight(), NumberPicker.this.c0);
                                    return true;
                                }
                            } else if (!NumberPicker.this.isEnabled()) {
                                return false;
                            } else {
                                NumberPicker.this.a(false);
                                k(i, 1);
                                return true;
                            }
                        }
                    } else if (i2 != 1) {
                        if (i2 != 2) {
                            if (i2 != 16) {
                                if (i2 != 64) {
                                    if (i2 != 128) {
                                        return NumberPicker.this.f.performAccessibilityAction(i2, bundle);
                                    }
                                    if (this.c != i) {
                                        return false;
                                    }
                                    this.c = RecyclerView.UNDEFINED_DURATION;
                                    k(i, 65536);
                                    NumberPicker.this.f.invalidate();
                                    return true;
                                } else if (this.c == i) {
                                    return false;
                                } else {
                                    this.c = i;
                                    k(i, 32768);
                                    NumberPicker.this.f.invalidate();
                                    return true;
                                }
                            } else if (!NumberPicker.this.isEnabled()) {
                                return false;
                            } else {
                                NumberPicker.this.A();
                                return true;
                            }
                        } else if (!NumberPicker.this.isEnabled() || !NumberPicker.this.f.isFocused()) {
                            return false;
                        } else {
                            NumberPicker.this.f.clearFocus();
                            return true;
                        }
                    } else if (!NumberPicker.this.isEnabled() || NumberPicker.this.f.isFocused()) {
                        return false;
                    } else {
                        return NumberPicker.this.f.requestFocus();
                    }
                } else if (i2 != 16) {
                    if (i2 != 64) {
                        if (i2 != 128 || this.c != i) {
                            return false;
                        }
                        this.c = RecyclerView.UNDEFINED_DURATION;
                        k(i, 65536);
                        NumberPicker numberPicker3 = NumberPicker.this;
                        numberPicker3.invalidate(0, numberPicker3.d0, numberPicker3.getRight(), NumberPicker.this.getBottom());
                        return true;
                    } else if (this.c == i) {
                        return false;
                    } else {
                        this.c = i;
                        k(i, 32768);
                        NumberPicker numberPicker4 = NumberPicker.this;
                        numberPicker4.invalidate(0, numberPicker4.d0, numberPicker4.getRight(), NumberPicker.this.getBottom());
                        return true;
                    }
                } else if (!NumberPicker.this.isEnabled()) {
                    return false;
                } else {
                    NumberPicker.this.a(true);
                    k(i, 1);
                    return true;
                }
            } else if (i2 != 64) {
                if (i2 != 128) {
                    if (i2 != 4096) {
                        if (i2 == 8192) {
                            if (!NumberPicker.this.isEnabled()) {
                                return false;
                            }
                            if (!NumberPicker.this.getWrapSelectorWheel() && NumberPicker.this.getValue() <= NumberPicker.this.getMinValue()) {
                                return false;
                            }
                            NumberPicker.this.a(false);
                            return true;
                        }
                    } else if (!NumberPicker.this.isEnabled()) {
                        return false;
                    } else {
                        if (!NumberPicker.this.getWrapSelectorWheel() && NumberPicker.this.getValue() >= NumberPicker.this.getMaxValue()) {
                            return false;
                        }
                        NumberPicker.this.a(true);
                        return true;
                    }
                } else if (this.c != i) {
                    return false;
                } else {
                    this.c = RecyclerView.UNDEFINED_DURATION;
                    NumberPicker.this.performAccessibilityAction(128, null);
                    return true;
                }
            } else if (this.c == i) {
                return false;
            } else {
                this.c = i;
                NumberPicker.this.performAccessibilityAction(64, null);
                return true;
            }
            return super.performAction(i, i2, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            NumberPicker.this.A();
            NumberPicker.this.a0 = true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public boolean b;

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public void run() {
            NumberPicker.this.a(this.b);
            NumberPicker.this.postDelayed(this, 300);
        }
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        String format(int i);
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(NumberPicker numberPicker, int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements Runnable {
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;

        @DexIgnore
        public h() {
        }

        @DexIgnore
        public void a(int i) {
            c();
            this.c = 1;
            this.b = i;
            NumberPicker.this.postDelayed(this, (long) ViewConfiguration.getTapTimeout());
        }

        @DexIgnore
        public void b(int i) {
            c();
            this.c = 2;
            this.b = i;
            NumberPicker.this.post(this);
        }

        @DexIgnore
        public void c() {
            this.c = 0;
            this.b = 0;
            NumberPicker.this.removeCallbacks(this);
            NumberPicker numberPicker = NumberPicker.this;
            if (numberPicker.f0) {
                numberPicker.f0 = false;
                numberPicker.invalidate(0, numberPicker.d0, numberPicker.getRight(), NumberPicker.this.getBottom());
            }
        }

        @DexIgnore
        public void run() {
            int i = this.c;
            if (i == 1) {
                int i2 = this.b;
                if (i2 == 1) {
                    NumberPicker numberPicker = NumberPicker.this;
                    numberPicker.f0 = true;
                    numberPicker.invalidate(0, numberPicker.d0, numberPicker.getRight(), NumberPicker.this.getBottom());
                } else if (i2 == 2) {
                    NumberPicker numberPicker2 = NumberPicker.this;
                    numberPicker2.g0 = true;
                    numberPicker2.invalidate(0, 0, numberPicker2.getRight(), NumberPicker.this.c0);
                }
            } else if (i == 2) {
                int i3 = this.b;
                if (i3 == 1) {
                    NumberPicker numberPicker3 = NumberPicker.this;
                    if (!numberPicker3.f0) {
                        numberPicker3.postDelayed(this, (long) ViewConfiguration.getPressedStateDuration());
                    }
                    NumberPicker numberPicker4 = NumberPicker.this;
                    numberPicker4.f0 = !numberPicker4.f0;
                    numberPicker4.invalidate(0, numberPicker4.d0, numberPicker4.getRight(), NumberPicker.this.getBottom());
                } else if (i3 == 2) {
                    NumberPicker numberPicker5 = NumberPicker.this;
                    if (!numberPicker5.g0) {
                        numberPicker5.postDelayed(this, (long) ViewConfiguration.getPressedStateDuration());
                    }
                    NumberPicker numberPicker6 = NumberPicker.this;
                    numberPicker6.g0 = !numberPicker6.g0;
                    numberPicker6.invalidate(0, 0, numberPicker6.getRight(), NumberPicker.this.c0);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public c f4746a;

        @DexIgnore
        public i() {
            this.f4746a = new c();
        }

        @DexIgnore
        public boolean a(int i, int i2, Bundle bundle) {
            c cVar = this.f4746a;
            return cVar != null && cVar.performAction(i, i2, bundle);
        }

        @DexIgnore
        public void b(int i, int i2) {
            c cVar = this.f4746a;
            if (cVar != null) {
                cVar.k(i, i2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class j implements f {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ StringBuilder f4747a; // = new StringBuilder();
        @DexIgnore
        public char b;
        @DexIgnore
        public transient Formatter c;
        @DexIgnore
        public /* final */ Object[] d; // = new Object[1];

        @DexIgnore
        public j() {
            c(Locale.getDefault());
        }

        @DexIgnore
        public static char b(Locale locale) {
            return new DecimalFormatSymbols(locale).getZeroDigit();
        }

        @DexIgnore
        public final Formatter a(Locale locale) {
            return new Formatter(this.f4747a, locale);
        }

        @DexIgnore
        public final void c(Locale locale) {
            this.c = a(locale);
            this.b = b(locale);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.f
        public String format(int i) {
            Locale locale = Locale.getDefault();
            if (this.b != b(locale)) {
                c(locale);
            }
            this.d[0] = Integer.valueOf(i);
            StringBuilder sb = this.f4747a;
            sb.delete(0, sb.length());
            this.c.format("%02d", this.d);
            return this.c.toString();
        }
    }

    /*
    static {
        new j();
    }
    */

    @DexIgnore
    public NumberPicker(Context context) {
        this(context, null);
    }

    @DexIgnore
    public NumberPicker(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 2130969482);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NumberPicker(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        Typeface f2;
        String d2;
        boolean z2 = false;
        this.z = new SparseArray<>();
        this.A = new int[3];
        this.E = RecyclerView.UNDEFINED_DURATION;
        this.W = 0;
        this.j0 = -1;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.NumberPicker, i2, 0);
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        this.T = resourceId != 0;
        obtainStyledAttributes.getColor(11, 0);
        this.S = obtainStyledAttributes.getColor(15, 0);
        this.U = obtainStyledAttributes.getDrawable(12);
        this.b = obtainStyledAttributes.getString(7);
        this.c = obtainStyledAttributes.getString(8);
        this.V = obtainStyledAttributes.getDimensionPixelSize(13, (int) TypedValue.applyDimension(1, 2.0f, getResources().getDisplayMetrics()));
        this.g = obtainStyledAttributes.getDimensionPixelSize(14, (int) TypedValue.applyDimension(1, 48.0f, getResources().getDisplayMetrics()));
        this.h = obtainStyledAttributes.getDimensionPixelSize(3, -1);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(1, -1);
        this.i = dimensionPixelSize;
        int i3 = this.h;
        if (i3 == -1 || dimensionPixelSize == -1 || i3 <= dimensionPixelSize) {
            this.j = obtainStyledAttributes.getDimensionPixelSize(4, -1);
            int dimensionPixelSize2 = obtainStyledAttributes.getDimensionPixelSize(2, -1);
            this.k = dimensionPixelSize2;
            int i4 = this.j;
            if (i4 == -1 || dimensionPixelSize2 == -1 || i4 <= dimensionPixelSize2) {
                this.l = this.k == -1 ? true : z2;
                this.C = obtainStyledAttributes.getDrawable(17);
                obtainStyledAttributes.recycle();
                this.i0 = new h();
                setWillNotDraw(!this.T);
                LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
                if (layoutInflater != null) {
                    layoutInflater.inflate(resourceId, (ViewGroup) this, true);
                }
                a aVar = new a();
                b bVar = new b();
                if (!this.T) {
                    ImageButton imageButton = (ImageButton) findViewById(2131362888);
                    this.d = imageButton;
                    imageButton.setOnClickListener(aVar);
                    this.d.setOnLongClickListener(bVar);
                } else {
                    this.d = null;
                }
                if (!this.T) {
                    ImageButton imageButton2 = (ImageButton) findViewById(2131362887);
                    this.e = imageButton2;
                    imageButton2.setOnClickListener(aVar);
                    this.e.setOnLongClickListener(bVar);
                } else {
                    this.e = null;
                }
                this.f = (AppCompatEditText) findViewById(2131362889);
                if (!TextUtils.isEmpty(this.b) && (d2 = qn5.l.a().d(this.b)) != null) {
                    this.f.setTextColor(Color.parseColor(d2));
                }
                if (!TextUtils.isEmpty(this.c) && (f2 = qn5.l.a().f(this.c)) != null) {
                    this.f.setTypeface(f2);
                }
                ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
                this.O = viewConfiguration.getScaledTouchSlop();
                this.P = viewConfiguration.getScaledMinimumFlingVelocity();
                this.Q = viewConfiguration.getScaledMaximumFlingVelocity() / 8;
                this.m = (int) this.f.getTextSize();
                Paint paint = new Paint();
                paint.setAntiAlias(true);
                paint.setTextAlign(Paint.Align.CENTER);
                paint.setTextSize((float) this.m);
                paint.setTypeface(this.f.getTypeface());
                paint.setColor(this.f.getTextColors().getColorForState(LinearLayout.ENABLED_STATE_SET, -1));
                this.B = paint;
                this.G = new Scroller(getContext(), null, true);
                this.H = new Scroller(getContext(), new DecelerateInterpolator(2.5f));
                C();
                if (getImportantForAccessibility() == 0) {
                    setImportantForAccessibility(1);
                    return;
                }
                return;
            }
            throw new IllegalArgumentException("minWidth > maxWidth");
        }
        throw new IllegalArgumentException("minHeight > maxHeight");
    }

    @DexIgnore
    public static String g(int i2) {
        return String.format(Locale.getDefault(), "%d", Integer.valueOf(i2));
    }

    @DexIgnore
    private i getSupportAccessibilityNodeProvider() {
        return new i();
    }

    @DexIgnore
    public static int resolveSizeAndState(int i2, int i3, int i4) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode != Integer.MIN_VALUE) {
            if (mode == 1073741824) {
                i2 = size;
            }
        } else if (size < i2) {
            i2 = 16777216 | size;
        }
        return (-16777216 & i4) | i2;
    }

    @DexIgnore
    public void A() {
    }

    @DexIgnore
    public final void B() {
        int i2;
        int i3 = 0;
        if (this.l) {
            String[] strArr = this.t;
            if (strArr == null) {
                float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                int i4 = 0;
                while (i4 <= 9) {
                    float measureText = this.B.measureText(g(i4));
                    if (measureText <= f2) {
                        measureText = f2;
                    }
                    i4++;
                    f2 = measureText;
                }
                for (int i5 = this.v; i5 > 0; i5 /= 10) {
                    i3++;
                }
                i2 = (int) (((float) i3) * f2);
            } else {
                i2 = 0;
                for (String str : strArr) {
                    float measureText2 = this.B.measureText(str);
                    if (measureText2 > ((float) i2)) {
                        i2 = (int) measureText2;
                    }
                }
            }
            int paddingLeft = i2 + this.f.getPaddingLeft() + this.f.getPaddingRight();
            if (this.k != paddingLeft) {
                int i6 = this.j;
                if (paddingLeft > i6) {
                    this.k = paddingLeft;
                } else {
                    this.k = i6;
                }
                invalidate();
            }
        }
    }

    @DexIgnore
    public final boolean C() {
        String[] strArr = this.t;
        String f2 = strArr == null ? f(this.w) : strArr[this.w - this.u];
        if (TextUtils.isEmpty(f2) || f2.equals(this.f.getText().toString())) {
            return false;
        }
        this.f.setText(f2);
        return true;
    }

    @DexIgnore
    public void a(boolean z2) {
        if (this.T) {
            this.f.setVisibility(4);
            if (!o(this.G)) {
                o(this.H);
            }
            this.I = 0;
            if (z2) {
                this.G.startScroll(0, 0, 0, -this.D, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS);
            } else {
                this.G.startScroll(0, 0, 0, this.D, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS);
            }
            invalidate();
        } else if (z2) {
            z(this.w + 1, true);
        } else {
            z(this.w - 1, true);
        }
    }

    @DexIgnore
    public final void b(int[] iArr) {
        System.arraycopy(iArr, 0, iArr, 1, iArr.length - 1);
        int i2 = iArr[1] - 1;
        if (this.R && i2 < this.u) {
            i2 = this.v;
        }
        iArr[0] = i2;
        c(i2);
    }

    @DexIgnore
    public final void c(int i2) {
        String str;
        SparseArray<String> sparseArray = this.z;
        if (sparseArray.get(i2) == null) {
            int i3 = this.u;
            if (i2 < i3 || i2 > this.v) {
                str = "";
            } else {
                String[] strArr = this.t;
                str = strArr != null ? strArr[i2 - i3] : f(i2);
            }
            sparseArray.put(i2, str);
        }
    }

    @DexIgnore
    public void computeScroll() {
        Scroller scroller = this.G;
        if (scroller.isFinished()) {
            scroller = this.H;
            if (scroller.isFinished()) {
                return;
            }
        }
        scroller.computeScrollOffset();
        int currY = scroller.getCurrY();
        if (this.I == 0) {
            this.I = scroller.getStartY();
        }
        scrollBy(0, currY - this.I);
        this.I = currY;
        if (scroller.isFinished()) {
            r(scroller);
        } else {
            invalidate();
        }
    }

    @DexIgnore
    public final boolean d() {
        int i2 = this.E - this.F;
        if (i2 == 0) {
            return false;
        }
        this.I = 0;
        int abs = Math.abs(i2);
        int i3 = this.D;
        if (abs > i3 / 2) {
            if (i2 > 0) {
                i3 = -i3;
            }
            i2 += i3;
        }
        this.H.startScroll(0, 0, 0, i2, 800);
        invalidate();
        return true;
    }

    @DexIgnore
    public boolean dispatchHoverEvent(MotionEvent motionEvent) {
        if (!this.T) {
            return super.dispatchHoverEvent(motionEvent);
        }
        if (((AccessibilityManager) getContext().getSystemService("accessibility")).isEnabled()) {
            int y2 = (int) motionEvent.getY();
            int i2 = y2 < this.c0 ? 3 : y2 > this.d0 ? 1 : 2;
            int action = motionEvent.getAction() & 255;
            i supportAccessibilityNodeProvider = getSupportAccessibilityNodeProvider();
            if (action == 7) {
                int i3 = this.e0;
                if (!(i3 == i2 || i3 == -1)) {
                    supportAccessibilityNodeProvider.b(i3, 256);
                    supportAccessibilityNodeProvider.b(i2, 128);
                    this.e0 = i2;
                    supportAccessibilityNodeProvider.a(i2, 64, null);
                }
            } else if (action == 9) {
                supportAccessibilityNodeProvider.b(i2, 128);
                this.e0 = i2;
                supportAccessibilityNodeProvider.a(i2, 64, null);
            } else if (action == 10) {
                supportAccessibilityNodeProvider.b(i2, 256);
                this.e0 = -1;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003e, code lost:
        requestFocus();
        r5.j0 = r0;
        u();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004c, code lost:
        if (r5.G.isFinished() == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004e, code lost:
        if (r0 != 20) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0050, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0051, code lost:
        a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005c, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return true;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean dispatchKeyEvent(android.view.KeyEvent r6) {
        /*
            r5 = this;
            r4 = 20
            r1 = 1
            int r0 = r6.getKeyCode()
            r2 = 19
            if (r0 == r2) goto L_0x001e
            if (r0 == r4) goto L_0x001e
            r1 = 23
            if (r0 == r1) goto L_0x001a
            r1 = 66
            if (r0 == r1) goto L_0x001a
        L_0x0015:
            boolean r1 = super.dispatchKeyEvent(r6)
        L_0x0019:
            return r1
        L_0x001a:
            r5.u()
            goto L_0x0015
        L_0x001e:
            boolean r2 = r5.T
            if (r2 == 0) goto L_0x0015
            int r2 = r6.getAction()
            if (r2 == 0) goto L_0x0032
            if (r2 != r1) goto L_0x0015
            int r2 = r5.j0
            if (r2 != r0) goto L_0x0015
            r0 = -1
            r5.j0 = r0
            goto L_0x0019
        L_0x0032:
            boolean r2 = r5.R
            if (r2 != 0) goto L_0x0038
            if (r0 != r4) goto L_0x0055
        L_0x0038:
            int r2 = r5.w
            int r3 = r5.v
            if (r2 >= r3) goto L_0x0015
        L_0x003e:
            r5.requestFocus()
            r5.j0 = r0
            r5.u()
            android.widget.Scroller r2 = r5.G
            boolean r2 = r2.isFinished()
            if (r2 == 0) goto L_0x0019
            if (r0 != r4) goto L_0x005c
            r0 = r1
        L_0x0051:
            r5.a(r0)
            goto L_0x0019
        L_0x0055:
            int r2 = r5.w
            int r3 = r5.u
            if (r2 <= r3) goto L_0x0015
            goto L_0x003e
        L_0x005c:
            r0 = 0
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.NumberPicker.dispatchKeyEvent(android.view.KeyEvent):boolean");
    }

    @DexIgnore
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 3) {
            u();
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    @DexIgnore
    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 3) {
            u();
        }
        return super.dispatchTrackballEvent(motionEvent);
    }

    @DexIgnore
    public final void e(int i2) {
        this.I = 0;
        if (i2 > 0) {
            this.G.fling(0, 0, 0, i2, 0, 0, 0, Integer.MAX_VALUE);
        } else {
            this.G.fling(0, Integer.MAX_VALUE, 0, i2, 0, 0, 0, Integer.MAX_VALUE);
        }
        invalidate();
    }

    @DexIgnore
    public String f(int i2) {
        f fVar = this.y;
        return fVar != null ? fVar.format(i2) : g(i2);
    }

    @DexIgnore
    public AccessibilityNodeProvider getAccessibilityNodeProvider() {
        if (!this.T) {
            return super.getAccessibilityNodeProvider();
        }
        if (this.h0 == null) {
            this.h0 = new i();
        }
        return this.h0.f4746a;
    }

    @DexIgnore
    public float getBottomFadingEdgeStrength() {
        return 0.9f;
    }

    @DexIgnore
    public int getMaxValue() {
        return this.v;
    }

    @DexIgnore
    public int getMinValue() {
        return this.u;
    }

    @DexIgnore
    public int getSolidColor() {
        return this.S;
    }

    @DexIgnore
    public float getTopFadingEdgeStrength() {
        return 0.9f;
    }

    @DexIgnore
    public int getValue() {
        return this.w;
    }

    @DexIgnore
    public boolean getWrapSelectorWheel() {
        return this.R;
    }

    @DexIgnore
    public int h(int i2) {
        int i3 = this.v;
        if (i2 > i3) {
            int i4 = this.u;
            return (((i2 - i3) % (i3 - i4)) + i4) - 1;
        }
        int i5 = this.u;
        return i2 < i5 ? (i3 - ((i5 - i2) % (i3 - i5))) + 1 : i2;
    }

    @DexIgnore
    public void i() {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null && inputMethodManager.isActive(this.f)) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            if (this.T) {
                this.f.setVisibility(4);
            }
        }
    }

    @DexIgnore
    public final void j(int[] iArr) {
        System.arraycopy(iArr, 1, iArr, 0, iArr.length - 1);
        int i2 = iArr[iArr.length - 2] + 1;
        if (this.R && i2 > this.v) {
            i2 = this.u;
        }
        iArr[iArr.length - 1] = i2;
        c(i2);
    }

    @DexIgnore
    public final void k() {
        setVerticalFadingEdgeEnabled(true);
        setFadingEdgeLength(((getBottom() - getTop()) - this.m) / 2);
    }

    @DexIgnore
    public final void l() {
        m();
        int[] iArr = this.A;
        int bottom = (int) ((((float) ((getBottom() - getTop()) - (iArr.length * this.m))) / ((float) iArr.length)) + 0.5f);
        this.s = bottom;
        this.D = bottom + this.m;
        int baseline = (this.f.getBaseline() + this.f.getTop()) - (this.D * 1);
        this.E = baseline;
        this.F = baseline;
        C();
    }

    @DexIgnore
    public final void m() {
        this.z.clear();
        int[] iArr = this.A;
        int i2 = this.w;
        for (int i3 = 0; i3 < this.A.length; i3++) {
            int i4 = (i3 - 1) + i2;
            if (this.R) {
                i4 = h(i4);
            }
            iArr[i3] = i4;
            c(iArr[i3]);
        }
    }

    @DexIgnore
    public final int n(int i2, int i3) {
        if (i3 == -1) {
            return i2;
        }
        int size = View.MeasureSpec.getSize(i2);
        int mode = View.MeasureSpec.getMode(i2);
        if (mode == Integer.MIN_VALUE) {
            return View.MeasureSpec.makeMeasureSpec(Math.min(size, i3), 1073741824);
        }
        if (mode == 0) {
            return View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
        }
        if (mode == 1073741824) {
            return i2;
        }
        throw new IllegalArgumentException("Unknown measure mode: " + mode);
    }

    @DexIgnore
    public final boolean o(Scroller scroller) {
        scroller.forceFinished(true);
        int finalY = scroller.getFinalY() - scroller.getCurrY();
        int i2 = this.E - ((this.F + finalY) % this.D);
        if (i2 == 0) {
            return false;
        }
        int abs = Math.abs(i2);
        int i3 = this.D;
        if (abs > i3 / 2) {
            i2 = i2 > 0 ? i2 - i3 : i2 + i3;
        }
        scrollBy(0, i2 + finalY);
        return true;
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        u();
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (!this.T) {
            super.onDraw(canvas);
            return;
        }
        float right = (float) ((getRight() - getLeft()) / 2);
        float f2 = (float) this.F;
        Drawable drawable = this.C;
        if (drawable != null && this.W == 0) {
            if (this.g0) {
                drawable.setState(LinearLayout.PRESSED_ENABLED_STATE_SET);
                this.C.setBounds(0, 0, getRight(), this.c0);
                this.C.draw(canvas);
            }
            if (this.f0) {
                this.C.setState(LinearLayout.PRESSED_ENABLED_STATE_SET);
                this.C.setBounds(0, this.d0, getRight(), getBottom());
                this.C.draw(canvas);
            }
        }
        int[] iArr = this.A;
        float f3 = f2;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            String str = this.z.get(iArr[i2]);
            if (i2 != 1 || this.f.getVisibility() != 0) {
                y(this.B, (float) getWidth(), str);
                canvas.drawText(str, right, f3, this.B);
            }
            f3 += (float) this.D;
        }
        Drawable drawable2 = this.U;
        if (drawable2 != null) {
            int i3 = this.c0;
            drawable2.setBounds(0, i3, getRight(), this.V + i3);
            this.U.draw(canvas);
            int i4 = this.d0;
            this.U.setBounds(0, i4 - this.V, getRight(), i4);
            this.U.draw(canvas);
        }
    }

    @DexIgnore
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(NumberPicker.class.getName());
        accessibilityEvent.setScrollable(true);
        accessibilityEvent.setScrollY((this.u + this.w) * this.D);
        accessibilityEvent.setMaxScrollY((this.v - this.u) * this.D);
    }

    @DexIgnore
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.T || !isEnabled() || (motionEvent.getAction() & 255) != 0) {
            return false;
        }
        u();
        this.f.setVisibility(4);
        float y2 = motionEvent.getY();
        this.L = y2;
        this.M = y2;
        this.a0 = false;
        this.b0 = false;
        if (y2 < ((float) this.c0)) {
            if (this.W == 0) {
                this.i0.a(2);
            }
        } else if (y2 > ((float) this.d0) && this.W == 0) {
            this.i0.a(1);
        }
        getParent().requestDisallowInterceptTouchEvent(true);
        if (!this.G.isFinished()) {
            this.G.forceFinished(true);
            this.H.forceFinished(true);
            q(0);
            return true;
        } else if (!this.H.isFinished()) {
            this.G.forceFinished(true);
            this.H.forceFinished(true);
            return true;
        } else {
            float f2 = this.L;
            if (f2 < ((float) this.c0)) {
                i();
                t(false, (long) ViewConfiguration.getLongPressTimeout());
                return true;
            } else if (f2 > ((float) this.d0)) {
                i();
                t(true, (long) ViewConfiguration.getLongPressTimeout());
                return true;
            } else {
                this.b0 = true;
                s();
                return true;
            }
        }
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        if (!this.T) {
            super.onLayout(z2, i2, i3, i4, i5);
            return;
        }
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        int measuredWidth2 = this.f.getMeasuredWidth();
        int measuredHeight2 = this.f.getMeasuredHeight();
        int i6 = (measuredWidth - measuredWidth2) / 2;
        int i7 = (measuredHeight - measuredHeight2) / 2;
        this.f.layout(i6, i7, measuredWidth2 + i6, measuredHeight2 + i7);
        if (z2) {
            l();
            k();
            int height = getHeight();
            int i8 = this.g;
            int i9 = this.V;
            int i10 = ((height - i8) / 2) - i9;
            this.c0 = i10;
            this.d0 = i10 + (i9 * 2) + i8;
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (!this.T) {
            super.onMeasure(i2, i3);
            return;
        }
        super.onMeasure(n(i2, this.k), n(i3, this.i));
        setMeasuredDimension(x(this.j, getMeasuredWidth(), i2), x(this.h, getMeasuredHeight(), i3));
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!isEnabled() || !this.T) {
            return false;
        }
        if (this.N == null) {
            this.N = VelocityTracker.obtain();
        }
        this.N.addMovement(motionEvent);
        int action = motionEvent.getAction() & 255;
        if (action == 1) {
            v();
            w();
            this.i0.c();
            VelocityTracker velocityTracker = this.N;
            velocityTracker.computeCurrentVelocity(1000, (float) this.Q);
            int yVelocity = (int) velocityTracker.getYVelocity();
            if (Math.abs(yVelocity) > this.P) {
                e(yVelocity);
                q(2);
            } else {
                int y2 = (int) motionEvent.getY();
                if (((int) Math.abs(((float) y2) - this.L)) > this.O) {
                    d();
                } else if (this.b0) {
                    this.b0 = false;
                    A();
                } else {
                    int i2 = (y2 / this.D) - 1;
                    if (i2 > 0) {
                        a(true);
                        this.i0.b(1);
                    } else if (i2 < 0) {
                        a(false);
                        this.i0.b(2);
                    }
                }
                q(0);
            }
            this.N.recycle();
            this.N = null;
            return true;
        } else if (action != 2 || this.a0) {
            return true;
        } else {
            float y3 = motionEvent.getY();
            if (this.W == 1) {
                scrollBy(0, (int) (y3 - this.M));
                invalidate();
            } else if (((int) Math.abs(y3 - this.L)) > this.O) {
                u();
                q(1);
            }
            this.M = y3;
            return true;
        }
    }

    @DexIgnore
    public final void p(int i2) {
        g gVar = this.x;
        if (gVar != null) {
            gVar.a(this, i2, this.w);
        }
    }

    @DexIgnore
    public final void q(int i2) {
        if (this.W != i2) {
            this.W = i2;
        }
    }

    @DexIgnore
    public final void r(Scroller scroller) {
        if (Objects.equals(scroller, this.G)) {
            if (!d()) {
                C();
            }
            q(0);
        } else if (this.W != 1) {
            C();
        }
    }

    @DexIgnore
    public final void s() {
        d dVar = this.K;
        if (dVar == null) {
            this.K = new d();
        } else {
            removeCallbacks(dVar);
        }
        postDelayed(this.K, (long) ViewConfiguration.getLongPressTimeout());
    }

    @DexIgnore
    public void scrollBy(int i2, int i3) {
        int[] iArr = this.A;
        if (!this.R && i3 > 0 && iArr[1] <= this.u) {
            this.F = this.E;
        } else if (this.R || i3 >= 0 || iArr[1] < this.v) {
            this.F += i3;
            while (true) {
                int i4 = this.F;
                if (i4 - this.E <= this.s) {
                    break;
                }
                this.F = i4 - this.D;
                b(iArr);
                z(iArr[1], true);
                if (!this.R && iArr[1] <= this.u) {
                    this.F = this.E;
                }
            }
            while (true) {
                int i5 = this.F;
                if (i5 - this.E < (-this.s)) {
                    this.F = i5 + this.D;
                    j(iArr);
                    z(iArr[1], true);
                    if (!this.R && iArr[1] >= this.v) {
                        this.F = this.E;
                    }
                } else {
                    return;
                }
            }
        } else {
            this.F = this.E;
        }
    }

    @DexIgnore
    public void setDisplayedValues(String[] strArr) {
        if (!Arrays.equals(this.t, strArr)) {
            this.t = strArr;
            if (strArr != null) {
                this.f.setRawInputType(524289);
            } else {
                this.f.setRawInputType(2);
            }
            C();
            m();
            B();
        }
    }

    @DexIgnore
    public void setEnabled(boolean z2) {
        ImageButton imageButton;
        ImageButton imageButton2;
        super.setEnabled(z2);
        if (!this.T && (imageButton2 = this.d) != null) {
            imageButton2.setEnabled(z2);
        }
        if (!this.T && (imageButton = this.e) != null) {
            imageButton.setEnabled(z2);
        }
        this.f.setEnabled(z2);
    }

    @DexIgnore
    public void setMaxValue(int i2) {
        if (this.v != i2) {
            if (i2 >= 0) {
                this.v = i2;
                if (i2 < this.w) {
                    this.w = i2;
                }
                setWrapSelectorWheel(this.v - this.u > this.A.length);
                m();
                C();
                B();
                invalidate();
                return;
            }
            throw new IllegalArgumentException("maxValue must be >= 0");
        }
    }

    @DexIgnore
    public void setMinValue(int i2) {
        if (this.u != i2) {
            if (i2 >= 0) {
                this.u = i2;
                if (i2 > this.w) {
                    this.w = i2;
                }
                setWrapSelectorWheel(this.v - this.u > this.A.length);
                m();
                C();
                B();
                invalidate();
                return;
            }
            throw new IllegalArgumentException("minValue must be >= 0");
        }
    }

    @DexIgnore
    public void setOnValueChangedListener(g gVar) {
        this.x = gVar;
    }

    @DexIgnore
    public void setValue(int i2) {
        z(i2, false);
    }

    @DexIgnore
    public void setWrapSelectorWheel(boolean z2) {
        boolean z3 = this.v - this.u >= this.A.length;
        if ((!z2 || z3) && z2 != this.R) {
            this.R = z2;
        }
    }

    @DexIgnore
    public void t(boolean z2, long j2) {
        e eVar = this.J;
        if (eVar == null) {
            this.J = new e();
        } else {
            removeCallbacks(eVar);
        }
        this.J.a(z2);
        postDelayed(this.J, j2);
    }

    @DexIgnore
    public final void u() {
        e eVar = this.J;
        if (eVar != null) {
            removeCallbacks(eVar);
        }
        d dVar = this.K;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
        this.i0.c();
    }

    @DexIgnore
    public final void v() {
        d dVar = this.K;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
    }

    @DexIgnore
    public final void w() {
        e eVar = this.J;
        if (eVar != null) {
            removeCallbacks(eVar);
        }
    }

    @DexIgnore
    public final int x(int i2, int i3, int i4) {
        return i2 != -1 ? resolveSizeAndState(Math.max(i2, i3), i4, 0) : i3;
    }

    @DexIgnore
    public final void y(Paint paint, float f2, String str) {
        paint.setTextSize((float) this.m);
        Rect rect = new Rect();
        paint.getTextBounds(str, 0, str.length(), rect);
        if (((float) rect.width()) >= f2) {
            paint.setTextSize((paint.getTextSize() * f2) / ((float) rect.width()));
        }
    }

    @DexIgnore
    public final void z(int i2, boolean z2) {
        if (this.w != i2) {
            int h2 = this.R ? h(i2) : Math.min(Math.max(i2, this.u), this.v);
            int i3 = this.w;
            this.w = h2;
            C();
            if (z2) {
                p(i3);
            }
            m();
            invalidate();
        }
    }
}
