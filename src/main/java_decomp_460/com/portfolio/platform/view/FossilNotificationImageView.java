package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import com.fossil.p47;
import com.fossil.wa1;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FossilNotificationImageView extends ViewGroup {
    @DexIgnore
    public static /* final */ String g; // = FossilNotificationImageView.class.getSimpleName();
    @DexIgnore
    public /* final */ Paint b; // = new Paint();
    @DexIgnore
    public /* final */ Paint c; // = new Paint();
    @DexIgnore
    public /* final */ Rect d; // = new Rect();
    @DexIgnore
    public FossilCircleImageView e;
    @DexIgnore
    public boolean f;

    @DexIgnore
    public FossilNotificationImageView(Context context) {
        super(context);
        a(context, null);
    }

    @DexIgnore
    public FossilNotificationImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            this.e = new FossilCircleImageView(context, attributeSet);
        } else {
            this.e = new FossilCircleImageView(context);
        }
        addView(this.e);
    }

    @DexIgnore
    public void b() {
        this.e.setBorderColor(PortfolioApp.d0.getResources().getColor(2131099827));
        this.e.setBorderWidth(3);
    }

    @DexIgnore
    public void c(Bitmap bitmap, Bitmap bitmap2) {
        if (bitmap == null || bitmap2 == null) {
            FLogger.INSTANCE.getLocal().d(g, "Inside. setImageBitmap 2 bitmap, bitmap == null");
        } else {
            this.e.f(bitmap, bitmap2);
        }
    }

    @DexIgnore
    public void d(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3) {
        if (bitmap == null || bitmap2 == null || bitmap3 == null) {
            FLogger.INSTANCE.getLocal().d(g, "Inside. setImageBitmap 3 bitmap, bitmap == null");
        } else {
            this.e.g(bitmap, bitmap2, bitmap3);
        }
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (!TextUtils.isEmpty(this.e.getHandNumber())) {
            int width = this.f ? getWidth() / 6 : getWidth() / 4;
            this.c.setAntiAlias(true);
            this.c.setFilterBitmap(true);
            this.c.setDither(true);
            this.c.setColor(-65536);
            this.c.setStyle(Paint.Style.FILL);
            if (this.f) {
                canvas.drawCircle((float) (getWidth() - (getWidth() / 6)), (float) (getHeight() / 6), (float) (getWidth() / 6), this.c);
            } else {
                canvas.drawCircle((float) (getWidth() / 2), ((float) (getHeight() - width)) - p47.b(5.0f), (float) width, this.c);
            }
            this.b.setAntiAlias(true);
            this.b.setColor(-1);
            this.b.setStrokeWidth(4.0f);
            this.b.setStyle(Paint.Style.STROKE);
            if (this.f) {
                canvas.drawCircle((float) (getWidth() - (getWidth() / 6)), (float) (getHeight() / 6), (float) (getWidth() / 6), this.b);
            } else {
                canvas.drawCircle((float) (getWidth() / 2), ((float) (getHeight() - width)) - p47.b(5.0f), (float) width, this.b);
            }
            this.c.setColor(-1);
            this.c.setTextSize((float) width);
            this.c.getTextBounds(this.e.getHandNumber(), 0, this.e.getHandNumber().length(), this.d);
            float measureText = this.c.measureText(this.e.getHandNumber());
            int height = this.d.height();
            if (this.f) {
                canvas.drawText(this.e.getHandNumber(), ((float) (getWidth() - (getWidth() / 6))) - (measureText / 2.0f), (float) ((height / 2) + (getHeight() / 6)), this.c);
            } else {
                canvas.drawText(this.e.getHandNumber(), ((float) (getWidth() / 2)) - (measureText / 2.0f), (((float) (getHeight() - width)) - p47.b(5.0f)) + ((float) (height / 2)), this.c);
            }
        }
    }

    @DexIgnore
    public void e(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, Bitmap bitmap4) {
        if (bitmap == null || bitmap2 == null || bitmap3 == null || bitmap4 == null) {
            FLogger.INSTANCE.getLocal().d(g, "Inside. setImageBitmap 4 bitmap, bitmap == null");
        } else {
            this.e.h(bitmap, bitmap2, bitmap3, bitmap4);
        }
    }

    @DexIgnore
    public void f(int i, wa1 wa1) {
        this.e.i(i, wa1);
    }

    @DexIgnore
    public void g(String str, wa1 wa1) {
        this.e.k(str, wa1);
    }

    @DexIgnore
    public FossilCircleImageView getFossilCircleImageView() {
        return this.e;
    }

    @DexIgnore
    public void h() {
        this.e.setBorderColor(PortfolioApp.d0.getResources().getColor(R.color.transparent));
        this.e.setBorderWidth(3);
    }

    @DexIgnore
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        this.e.layout(0, 0, i5, i5);
    }

    @DexIgnore
    public void setDefault(boolean z) {
        this.e.setDefault(z);
    }

    @DexIgnore
    public void setHandNumber(String str) {
        this.e.setHandNumber(str);
    }

    @DexIgnore
    public void setImageDrawable(Drawable drawable) {
        this.e.setImageDrawable(drawable);
    }

    @DexIgnore
    public void setTopRightBadge(boolean z) {
        this.f = z;
    }
}
