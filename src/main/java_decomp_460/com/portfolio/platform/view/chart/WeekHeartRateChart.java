package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.at7;
import com.fossil.hm7;
import com.fossil.im7;
import com.fossil.p47;
import com.fossil.pl0;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.qn5;
import com.fossil.qq7;
import com.fossil.rp7;
import com.fossil.sq4;
import com.fossil.um5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BaseChart;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeekHeartRateChart extends BaseChart {
    @DexIgnore
    public String A; // = "";
    @DexIgnore
    public String B; // = "";
    @DexIgnore
    public String C; // = "";
    @DexIgnore
    public float D;
    @DexIgnore
    public String E; // = "";
    @DexIgnore
    public float F;
    @DexIgnore
    public String G; // = "";
    @DexIgnore
    public String H; // = "";
    @DexIgnore
    public String I; // = "";
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public float O;
    @DexIgnore
    public float P;
    @DexIgnore
    public float Q;
    @DexIgnore
    public float R;
    @DexIgnore
    public float S;
    @DexIgnore
    public short T;
    @DexIgnore
    public short U;
    @DexIgnore
    public Paint V;
    @DexIgnore
    public Paint W;
    @DexIgnore
    public Paint a0;
    @DexIgnore
    public Paint b0;
    @DexIgnore
    public Paint c0;
    @DexIgnore
    public Paint d0;
    @DexIgnore
    public int e0;
    @DexIgnore
    public List<Integer> f0;
    @DexIgnore
    public List<PointF> g0;
    @DexIgnore
    public int h0; // = 1;
    @DexIgnore
    public List<String> i0; // = new ArrayList();
    @DexIgnore
    public String w; // = "";
    @DexIgnore
    public String x; // = "";
    @DexIgnore
    public String y; // = "";
    @DexIgnore
    public float z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<Integer, Boolean> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ Boolean invoke(Integer num) {
            return Boolean.valueOf(invoke(num.intValue()));
        }

        @DexIgnore
        public final boolean invoke(int i) {
            return i > 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements rp7<Integer, Boolean> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ Boolean invoke(Integer num) {
            return Boolean.valueOf(invoke(num.intValue()));
        }

        @DexIgnore
        public final boolean invoke(int i) {
            return i > 0;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeekHeartRateChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Typeface f;
        String d;
        Typeface f2;
        Typeface f3;
        String d2;
        String d3;
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.WeekHeartRateChart);
        String string = obtainStyledAttributes.getString(2);
        this.w = string == null ? "dianaHeartRateTab" : string;
        String string2 = obtainStyledAttributes.getString(4);
        this.x = string2 == null ? "nonBrandDisableCalendarDay" : string2;
        String string3 = obtainStyledAttributes.getString(5);
        this.y = string3 == null ? "calendarDate" : string3;
        this.z = obtainStyledAttributes.getDimension(3, p47.n(13.0f));
        String string4 = obtainStyledAttributes.getString(8);
        this.A = string4 == null ? "onDianaHeartRateTab" : string4;
        String string5 = obtainStyledAttributes.getString(9);
        this.B = string5 == null ? "calendarWeekday" : string5;
        String string6 = obtainStyledAttributes.getString(12);
        this.C = string6 == null ? "calendarWeekday" : string6;
        this.D = obtainStyledAttributes.getDimension(10, p47.n(13.0f));
        String string7 = obtainStyledAttributes.getString(11);
        this.E = string7 == null ? "secondaryText" : string7;
        this.F = obtainStyledAttributes.getDimension(7, p47.b(13.0f));
        String string8 = obtainStyledAttributes.getString(0);
        this.G = string8 == null ? "averageRestingHeartRate" : string8;
        String string9 = obtainStyledAttributes.getString(1);
        this.H = string9 == null ? "aboveAverageRestingHeartRate" : string9;
        String string10 = obtainStyledAttributes.getString(6);
        this.I = string10 == null ? "maxHeartRate" : string10;
        obtainStyledAttributes.recycle();
        this.V = new Paint();
        if (!TextUtils.isEmpty(this.x) && (d3 = qn5.l.a().d(this.w)) != null) {
            this.V.setColor(Color.parseColor(d3));
        }
        this.V.setStrokeWidth(2.0f);
        this.V.setStyle(Paint.Style.STROKE);
        this.W = new Paint(1);
        if (!TextUtils.isEmpty(this.x) && (d2 = qn5.l.a().d(this.x)) != null) {
            this.W.setColor(Color.parseColor(d2));
        }
        this.W.setStyle(Paint.Style.FILL);
        this.W.setTextSize(this.z);
        if (!TextUtils.isEmpty(this.y) && (f3 = qn5.l.a().f(this.y)) != null) {
            this.W.setTypeface(f3);
        }
        Paint paint = new Paint(1);
        this.a0 = paint;
        paint.setColor(Color.parseColor(qn5.l.a().d(this.A)));
        this.a0.setTextSize(this.F);
        this.a0.setStyle(Paint.Style.FILL);
        if (!TextUtils.isEmpty(this.B) && (f2 = qn5.l.a().f(this.B)) != null) {
            this.a0.setTypeface(f2);
        }
        this.b0 = new Paint(1);
        if (!TextUtils.isEmpty(this.E) && (d = qn5.l.a().d(this.E)) != null) {
            this.b0.setColor(Color.parseColor(d));
        }
        this.b0.setStyle(Paint.Style.FILL);
        this.b0.setTextSize(this.D);
        if (!TextUtils.isEmpty(this.C) && (f = qn5.l.a().f(this.C)) != null) {
            this.b0.setTypeface(f);
        }
        Paint paint2 = new Paint(1);
        this.c0 = paint2;
        paint2.setColor(Color.parseColor(qn5.l.a().d(this.G)));
        this.c0.setStyle(Paint.Style.FILL);
        Paint paint3 = new Paint(1);
        this.d0 = paint3;
        paint3.setColor(pl0.h(Color.parseColor(qn5.l.a().d(this.I)), 20));
        this.d0.setStyle(Paint.Style.STROKE);
        this.d0.setStrokeWidth(p47.b(2.0f));
        Rect rect = new Rect();
        this.W.getTextBounds("222", 0, 3, rect);
        this.Q = (float) rect.width();
        this.R = (float) rect.height();
        Rect rect2 = new Rect();
        String c = um5.c(PortfolioApp.h0.c(), 2131887231);
        this.b0.getTextBounds(c, 0, c.length(), rect2);
        this.S = (float) rect2.height();
        ArrayList arrayList = new ArrayList(7);
        for (int i = 0; i < 7; i++) {
            arrayList.add(0);
        }
        this.f0 = arrayList;
        this.g0 = new ArrayList();
        o();
        a();
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void f(Canvas canvas) {
        pq7.c(canvas, "canvas");
        super.f(canvas);
        this.J = ((float) canvas.getHeight()) - (this.S * ((float) 2));
        this.K = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.L = ((float) canvas.getWidth()) - (this.Q / ((float) 3));
        float f = (float) 6;
        this.M = this.D + this.F + (this.S * f);
        float f2 = this.K;
        float b2 = p47.b(10.0f);
        float f3 = this.F;
        this.N = f2 + b2 + f3;
        this.P = (this.J - f3) - (this.D * f);
        this.O = (this.L - (this.Q * 1.5f)) - f3;
        q(canvas);
    }

    @DexIgnore
    public final List<String> getListWeekDays() {
        return this.i0;
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void k(Canvas canvas) {
        pq7.c(canvas, "canvas");
        super.k(canvas);
        if (this.i0.isEmpty()) {
            this.i0.addAll(hm7.i(um5.c(PortfolioApp.h0.c(), 2131886770), um5.c(PortfolioApp.h0.c(), 2131886769), um5.c(PortfolioApp.h0.c(), 2131886772), um5.c(PortfolioApp.h0.c(), 2131886774), um5.c(PortfolioApp.h0.c(), 2131886773), um5.c(PortfolioApp.h0.c(), 2131886768), um5.c(PortfolioApp.h0.c(), 2131886771)));
        }
        s(canvas, this.i0, (this.O - this.N) / ((float) 6), this.J + (((((float) canvas.getHeight()) - this.J) + this.S) / ((float) 2)));
    }

    @DexIgnore
    public final void m(List<Integer> list) {
        pq7.c(list, "heartRatePointList");
        this.f0.clear();
        this.g0.clear();
        List<Integer> list2 = this.f0;
        ArrayList arrayList = new ArrayList(im7.m(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            T next = it.next();
            arrayList.add(Integer.valueOf(next != null ? next.intValue() : 0));
        }
        list2.addAll(arrayList);
        o();
        int size = this.f0.size();
        if (size < 7) {
            int i = 7 - size;
            ArrayList arrayList2 = new ArrayList(i);
            for (int i2 = 0; i2 < i; i2++) {
                arrayList2.add(0);
            }
            this.f0.addAll(arrayList2);
        }
        getMGraph().invalidate();
    }

    @DexIgnore
    public final void n(Canvas canvas, float f, float f2, int i, int i2, Rect rect) {
        this.c0.setColor(i2);
        canvas.drawCircle(f, f2, this.F, this.c0);
        canvas.drawCircle(f, f2, this.F + p47.b(3.0f), this.d0);
        String valueOf = String.valueOf(i);
        this.b0.getTextBounds(valueOf, 0, valueOf.length(), rect);
        canvas.drawText(valueOf, f - (this.a0.measureText(valueOf) / ((float) 2)), ((float) (rect.height() / 2)) + f2, this.a0);
    }

    @DexIgnore
    public final void o() {
        Integer num = (Integer) at7.r(at7.h(pm7.z(this.f0), a.INSTANCE));
        this.T = (short) (num != null ? (short) num.intValue() : 0);
        Integer num2 = (Integer) at7.q(at7.h(pm7.z(this.f0), b.INSTANCE));
        short intValue = num2 != null ? (short) num2.intValue() : 0;
        this.U = (short) intValue;
        if (intValue == ((short) 0)) {
            this.U = (short) 100;
        }
        Iterator<T> it = this.f0.iterator();
        int i = 0;
        int i2 = 0;
        while (it.hasNext()) {
            int intValue2 = it.next().intValue();
            if (intValue2 > 0) {
                i += intValue2;
                i2++;
            }
        }
        int i3 = i2 > 0 ? i / i2 : (this.U - this.T) / 2;
        this.e0 = i3;
        short s = Math.abs(this.U - i3) >= Math.abs(this.T - this.e0) ? this.U : this.T;
        int abs = Math.abs((int) s);
        int i4 = this.e0;
        this.h0 = abs != i4 ? Math.abs(s - i4) * 2 : 1;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        setMWidth(i);
        setMHeight(i2);
        setMLeftPadding(getPaddingLeft());
        setMTopPadding(getPaddingTop());
        setMRightPadding(getPaddingRight());
        setMBottomPadding(getPaddingBottom());
        getMGraph().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
        getMLegend().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
        getMBackground().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
        getMGraphOverlay().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
    }

    @DexIgnore
    public final void p(Canvas canvas) {
        float f;
        short s = this.U;
        int i = this.e0;
        if (s != ((short) i)) {
            float f2 = this.M;
            f = f2 + (((float) (s - i)) * ((this.P - f2) / ((float) this.h0)));
        } else {
            float f3 = this.M;
            f = f3 + ((this.P - f3) / 2.0f);
        }
        canvas.drawLine(this.K, f, (float) canvas.getWidth(), f, this.V);
        canvas.drawText(String.valueOf(this.e0), this.L - this.W.measureText(String.valueOf(this.e0)), (1.5f * this.R) + f, this.W);
    }

    @DexIgnore
    public final void q(Canvas canvas) {
        p(canvas);
        r(canvas, (this.O - this.N) / ((float) 6));
    }

    @DexIgnore
    public final void r(Canvas canvas, float f) {
        qn5 a2;
        String str;
        Rect rect = new Rect();
        int i = 0;
        for (T t : this.f0) {
            if (i >= 0) {
                int intValue = t.intValue();
                float f2 = this.N + (((float) i) * f);
                float f3 = this.M;
                float f4 = this.P;
                float f5 = (f4 - f3) / ((float) this.h0);
                short s = this.U;
                float f6 = (f5 * ((float) (s - intValue))) + f3;
                if (this.T == s) {
                    if (intValue > 0) {
                        f6 = f3 + ((f4 - f3) / 2.0f);
                        n(canvas, f2, f6, intValue, Color.parseColor(qn5.l.a().d(this.G)), rect);
                    }
                } else if (intValue > 0) {
                    if (intValue > this.e0) {
                        a2 = qn5.l.a();
                        str = this.H;
                    } else {
                        a2 = qn5.l.a();
                        str = this.G;
                    }
                    n(canvas, f2, f6, intValue, Color.parseColor(a2.d(str)), rect);
                }
                this.g0.add(new PointF(f2, f6));
                i++;
            } else {
                hm7.l();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void s(Canvas canvas, List<String> list, float f, float f2) {
        int i = 0;
        for (T t : list) {
            if (i >= 0) {
                T t2 = t;
                canvas.drawText(t2, (this.N + (((float) i) * f)) - (this.W.measureText(t2) / ((float) 2)), f2, this.b0);
                i++;
            } else {
                hm7.l();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void setListWeekDays(List<String> list) {
        pq7.c(list, "value");
        this.i0.clear();
        this.i0.addAll(list);
        getMLegend().invalidate();
    }
}
