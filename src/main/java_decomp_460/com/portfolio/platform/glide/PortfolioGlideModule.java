package com.portfolio.platform.glide;

import android.content.Context;
import com.fossil.mi1;
import com.fossil.oa1;
import com.fossil.pj5;
import com.fossil.pq7;
import com.fossil.qj5;
import com.fossil.rj5;
import com.fossil.sj5;
import com.fossil.ua1;
import com.fossil.xj5;
import com.fossil.yj5;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioGlideModule extends mi1 {
    @DexIgnore
    @Override // com.fossil.pi1, com.fossil.ri1
    public void b(Context context, oa1 oa1, ua1 ua1) {
        pq7.c(context, "context");
        pq7.c(oa1, "glide");
        pq7.c(ua1, "registry");
        super.b(context, oa1, ua1);
        ua1.d(qj5.class, InputStream.class, new pj5.b());
        ua1.d(sj5.class, InputStream.class, new rj5.b());
        ua1.d(yj5.class, InputStream.class, new xj5.b());
    }

    @DexIgnore
    @Override // com.fossil.mi1
    public boolean c() {
        return false;
    }
}
