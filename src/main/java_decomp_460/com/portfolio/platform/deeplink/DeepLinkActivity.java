package com.portfolio.platform.deeplink;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.fossil.b77;
import com.fossil.hh5;
import com.fossil.hq4;
import com.fossil.ls0;
import com.fossil.ls5;
import com.fossil.po4;
import com.fossil.pq7;
import com.fossil.t47;
import com.fossil.ts0;
import com.fossil.vs0;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.watchface.faces.WatchFaceListActivity;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeepLinkActivity extends ls5 implements t47.g {
    @DexIgnore
    public po4 A;
    @DexIgnore
    public hh5 B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements ls0<hq4.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ DeepLinkActivity f4693a;

        @DexIgnore
        public a(DeepLinkActivity deepLinkActivity) {
            this.f4693a = deepLinkActivity;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.b bVar) {
            if (bVar.a()) {
                this.f4693a.F();
            }
            if (bVar.b()) {
                this.f4693a.t();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<hh5.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ DeepLinkActivity f4694a;
        @DexIgnore
        public /* final */ /* synthetic */ Uri b;

        @DexIgnore
        public b(DeepLinkActivity deepLinkActivity, Uri uri) {
            this.f4694a = deepLinkActivity;
            this.b = uri;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hh5.a aVar) {
            if (aVar.b()) {
                this.f4694a.finish();
                WatchFaceGalleryActivity.B.b(this.f4694a, this.b);
            }
            if (aVar.c()) {
                this.f4694a.finish();
                WatchFaceListActivity.C.b(this.f4694a, this.b);
            }
            if (aVar.d()) {
                this.f4694a.finish();
                WelcomeActivity.B.c(this.f4694a);
            }
            if (aVar.a()) {
                this.f4694a.finish();
                HomeActivity.B.d(this.f4694a);
            }
        }
    }

    /*
    static {
        pq7.b(DeepLinkActivity.class.getSimpleName(), "DeepLinkActivity::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        PortfolioApp.h0.c().M().S1().a(this);
        po4 po4 = this.A;
        if (po4 != null) {
            ts0 a2 = vs0.f(this, po4).a(hh5.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026inkViewModel::class.java)");
            hh5 hh5 = (hh5) a2;
            this.B = hh5;
            if (hh5 != null) {
                hh5.j().h(this, new a(this));
                Intent intent = getIntent();
                Uri data = intent != null ? intent.getData() : null;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String r = r();
                local.d(r, "uri " + data);
                if (data != null) {
                    String lastPathSegment = data.getLastPathSegment();
                    if (lastPathSegment != null) {
                        b77 b77 = b77.f401a;
                        pq7.b(lastPathSegment, "it");
                        b77.f(lastPathSegment);
                    }
                    hh5 hh52 = this.B;
                    if (hh52 != null) {
                        hh52.t().h(this, new b(this, data));
                        hh5 hh53 = this.B;
                        if (hh53 != null) {
                            hh53.p(data);
                        } else {
                            pq7.n("mViewModel");
                            throw null;
                        }
                    } else {
                        pq7.n("mViewModel");
                        throw null;
                    }
                }
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        } else {
            pq7.n("viewModelFactory");
            throw null;
        }
    }
}
