package com.portfolio.platform.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rj4;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SignUpSocialAuth implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @rj4("acceptedLocationDataSharing")
    public ArrayList<String> acceptedLocationDataSharing;
    @DexIgnore
    @rj4("acceptedPrivacies")
    public ArrayList<String> acceptedPrivacies;
    @DexIgnore
    @rj4("acceptedTermsOfService")
    public ArrayList<String> acceptedTermsOfService;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_BIRTHDAY)
    public String birthday;
    @DexIgnore
    @rj4("clientId")
    public String clientId;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_DIAGNOSTIC_ENABLE)
    public boolean diagnosticEnabled;
    @DexIgnore
    @rj4(Constants.EMAIL)
    public String email;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_FIRST_NAME)
    public String firstName;
    @DexIgnore
    @rj4("gender")
    public String gender;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_LAST_NAME)
    public String lastName;
    @DexIgnore
    @rj4(Constants.SERVICE)
    public String service;
    @DexIgnore
    @rj4("token")
    public String token;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SignUpSocialAuth> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SignUpSocialAuth createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new SignUpSocialAuth(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SignUpSocialAuth[] newArray(int i) {
            return new SignUpSocialAuth[i];
        }
    }

    @DexIgnore
    public SignUpSocialAuth() {
        this("", "", "", "", "", "", "", "", false, new ArrayList(), new ArrayList(), new ArrayList());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SignUpSocialAuth(android.os.Parcel r14) {
        /*
            r13 = this;
            r9 = 0
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r14, r0)
            java.lang.String r1 = r14.readString()
            if (r1 == 0) goto L_0x0055
        L_0x000c:
            java.lang.String r2 = r14.readString()
            if (r2 == 0) goto L_0x0058
        L_0x0012:
            java.lang.String r3 = r14.readString()
            if (r3 == 0) goto L_0x005b
        L_0x0018:
            java.lang.String r4 = r14.readString()
            if (r4 == 0) goto L_0x005e
        L_0x001e:
            java.lang.String r5 = r14.readString()
            if (r5 == 0) goto L_0x0061
        L_0x0024:
            java.lang.String r6 = r14.readString()
            if (r6 == 0) goto L_0x0064
        L_0x002a:
            java.lang.String r7 = r14.readString()
            if (r7 == 0) goto L_0x0067
        L_0x0030:
            java.lang.String r8 = r14.readString()
            if (r8 == 0) goto L_0x006a
        L_0x0036:
            byte r0 = r14.readByte()
            byte r10 = (byte) r9
            if (r0 == r10) goto L_0x003e
            r9 = 1
        L_0x003e:
            java.util.ArrayList r10 = r14.createStringArrayList()
            if (r10 == 0) goto L_0x007d
            java.util.ArrayList r11 = r14.createStringArrayList()
            if (r11 == 0) goto L_0x0075
            java.util.ArrayList r12 = r14.createStringArrayList()
            if (r12 == 0) goto L_0x006d
            r0 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            return
        L_0x0055:
            java.lang.String r1 = ""
            goto L_0x000c
        L_0x0058:
            java.lang.String r2 = ""
            goto L_0x0012
        L_0x005b:
            java.lang.String r3 = ""
            goto L_0x0018
        L_0x005e:
            java.lang.String r4 = ""
            goto L_0x001e
        L_0x0061:
            java.lang.String r5 = ""
            goto L_0x0024
        L_0x0064:
            java.lang.String r6 = ""
            goto L_0x002a
        L_0x0067:
            java.lang.String r7 = ""
            goto L_0x0030
        L_0x006a:
            java.lang.String r8 = ""
            goto L_0x0036
        L_0x006d:
            com.fossil.il7 r0 = new com.fossil.il7
        */
        //  java.lang.String r1 = "null cannot be cast to non-null type kotlin.collections.ArrayList<kotlin.String> /* = java.util.ArrayList<kotlin.String> */"
        /*
            r0.<init>(r1)
            throw r0
        L_0x0075:
            com.fossil.il7 r0 = new com.fossil.il7
        */
        //  java.lang.String r1 = "null cannot be cast to non-null type kotlin.collections.ArrayList<kotlin.String> /* = java.util.ArrayList<kotlin.String> */"
        /*
            r0.<init>(r1)
            throw r0
        L_0x007d:
            com.fossil.il7 r0 = new com.fossil.il7
        */
        //  java.lang.String r1 = "null cannot be cast to non-null type kotlin.collections.ArrayList<kotlin.String> /* = java.util.ArrayList<kotlin.String> */"
        /*
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.SignUpSocialAuth.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public SignUpSocialAuth(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z, ArrayList<String> arrayList, ArrayList<String> arrayList2, ArrayList<String> arrayList3) {
        pq7.c(str, Constants.EMAIL);
        pq7.c(str2, Constants.SERVICE);
        pq7.c(str3, "token");
        pq7.c(str4, "clientId");
        pq7.c(str5, Constants.PROFILE_KEY_FIRST_NAME);
        pq7.c(str6, Constants.PROFILE_KEY_LAST_NAME);
        pq7.c(str7, Constants.PROFILE_KEY_BIRTHDAY);
        pq7.c(str8, "gender");
        pq7.c(arrayList, "acceptedLocationDataSharing");
        pq7.c(arrayList2, "acceptedPrivacies");
        pq7.c(arrayList3, "acceptedTermsOfService");
        this.email = str;
        this.service = str2;
        this.token = str3;
        this.clientId = str4;
        this.firstName = str5;
        this.lastName = str6;
        this.birthday = str7;
        this.gender = str8;
        this.diagnosticEnabled = z;
        this.acceptedLocationDataSharing = arrayList;
        this.acceptedPrivacies = arrayList2;
        this.acceptedTermsOfService = arrayList3;
    }

    @DexIgnore
    public static /* synthetic */ SignUpSocialAuth copy$default(SignUpSocialAuth signUpSocialAuth, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, int i, Object obj) {
        return signUpSocialAuth.copy((i & 1) != 0 ? signUpSocialAuth.email : str, (i & 2) != 0 ? signUpSocialAuth.service : str2, (i & 4) != 0 ? signUpSocialAuth.token : str3, (i & 8) != 0 ? signUpSocialAuth.clientId : str4, (i & 16) != 0 ? signUpSocialAuth.firstName : str5, (i & 32) != 0 ? signUpSocialAuth.lastName : str6, (i & 64) != 0 ? signUpSocialAuth.birthday : str7, (i & 128) != 0 ? signUpSocialAuth.gender : str8, (i & 256) != 0 ? signUpSocialAuth.diagnosticEnabled : z, (i & 512) != 0 ? signUpSocialAuth.acceptedLocationDataSharing : arrayList, (i & 1024) != 0 ? signUpSocialAuth.acceptedPrivacies : arrayList2, (i & 2048) != 0 ? signUpSocialAuth.acceptedTermsOfService : arrayList3);
    }

    @DexIgnore
    public final String component1() {
        return this.email;
    }

    @DexIgnore
    public final ArrayList<String> component10() {
        return this.acceptedLocationDataSharing;
    }

    @DexIgnore
    public final ArrayList<String> component11() {
        return this.acceptedPrivacies;
    }

    @DexIgnore
    public final ArrayList<String> component12() {
        return this.acceptedTermsOfService;
    }

    @DexIgnore
    public final String component2() {
        return this.service;
    }

    @DexIgnore
    public final String component3() {
        return this.token;
    }

    @DexIgnore
    public final String component4() {
        return this.clientId;
    }

    @DexIgnore
    public final String component5() {
        return this.firstName;
    }

    @DexIgnore
    public final String component6() {
        return this.lastName;
    }

    @DexIgnore
    public final String component7() {
        return this.birthday;
    }

    @DexIgnore
    public final String component8() {
        return this.gender;
    }

    @DexIgnore
    public final boolean component9() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public final SignUpSocialAuth copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z, ArrayList<String> arrayList, ArrayList<String> arrayList2, ArrayList<String> arrayList3) {
        pq7.c(str, Constants.EMAIL);
        pq7.c(str2, Constants.SERVICE);
        pq7.c(str3, "token");
        pq7.c(str4, "clientId");
        pq7.c(str5, Constants.PROFILE_KEY_FIRST_NAME);
        pq7.c(str6, Constants.PROFILE_KEY_LAST_NAME);
        pq7.c(str7, Constants.PROFILE_KEY_BIRTHDAY);
        pq7.c(str8, "gender");
        pq7.c(arrayList, "acceptedLocationDataSharing");
        pq7.c(arrayList2, "acceptedPrivacies");
        pq7.c(arrayList3, "acceptedTermsOfService");
        return new SignUpSocialAuth(str, str2, str3, str4, str5, str6, str7, str8, z, arrayList, arrayList2, arrayList3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SignUpSocialAuth) {
                SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) obj;
                if (!pq7.a(this.email, signUpSocialAuth.email) || !pq7.a(this.service, signUpSocialAuth.service) || !pq7.a(this.token, signUpSocialAuth.token) || !pq7.a(this.clientId, signUpSocialAuth.clientId) || !pq7.a(this.firstName, signUpSocialAuth.firstName) || !pq7.a(this.lastName, signUpSocialAuth.lastName) || !pq7.a(this.birthday, signUpSocialAuth.birthday) || !pq7.a(this.gender, signUpSocialAuth.gender) || this.diagnosticEnabled != signUpSocialAuth.diagnosticEnabled || !pq7.a(this.acceptedLocationDataSharing, signUpSocialAuth.acceptedLocationDataSharing) || !pq7.a(this.acceptedPrivacies, signUpSocialAuth.acceptedPrivacies) || !pq7.a(this.acceptedTermsOfService, signUpSocialAuth.acceptedTermsOfService)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ArrayList<String> getAcceptedLocationDataSharing() {
        return this.acceptedLocationDataSharing;
    }

    @DexIgnore
    public final ArrayList<String> getAcceptedPrivacies() {
        return this.acceptedPrivacies;
    }

    @DexIgnore
    public final ArrayList<String> getAcceptedTermsOfService() {
        return this.acceptedTermsOfService;
    }

    @DexIgnore
    public final String getBirthday() {
        return this.birthday;
    }

    @DexIgnore
    public final String getClientId() {
        return this.clientId;
    }

    @DexIgnore
    public final boolean getDiagnosticEnabled() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public final String getEmail() {
        return this.email;
    }

    @DexIgnore
    public final String getFirstName() {
        return this.firstName;
    }

    @DexIgnore
    public final String getGender() {
        return this.gender;
    }

    @DexIgnore
    public final String getLastName() {
        return this.lastName;
    }

    @DexIgnore
    public final String getService() {
        return this.service;
    }

    @DexIgnore
    public final String getToken() {
        return this.token;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.email;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.service;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.token;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.clientId;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.firstName;
        int hashCode5 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.lastName;
        int hashCode6 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.birthday;
        int hashCode7 = str7 != null ? str7.hashCode() : 0;
        String str8 = this.gender;
        int hashCode8 = str8 != null ? str8.hashCode() : 0;
        boolean z = this.diagnosticEnabled;
        if (z) {
            z = true;
        }
        ArrayList<String> arrayList = this.acceptedLocationDataSharing;
        int hashCode9 = arrayList != null ? arrayList.hashCode() : 0;
        ArrayList<String> arrayList2 = this.acceptedPrivacies;
        int hashCode10 = arrayList2 != null ? arrayList2.hashCode() : 0;
        ArrayList<String> arrayList3 = this.acceptedTermsOfService;
        if (arrayList3 != null) {
            i = arrayList3.hashCode();
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + i2) * 31) + hashCode9) * 31) + hashCode10) * 31) + i;
    }

    @DexIgnore
    public final void setAcceptedLocationDataSharing(ArrayList<String> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.acceptedLocationDataSharing = arrayList;
    }

    @DexIgnore
    public final void setAcceptedPrivacies(ArrayList<String> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.acceptedPrivacies = arrayList;
    }

    @DexIgnore
    public final void setAcceptedTermsOfService(ArrayList<String> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.acceptedTermsOfService = arrayList;
    }

    @DexIgnore
    public final void setBirthday(String str) {
        pq7.c(str, "<set-?>");
        this.birthday = str;
    }

    @DexIgnore
    public final void setClientId(String str) {
        pq7.c(str, "<set-?>");
        this.clientId = str;
    }

    @DexIgnore
    public final void setDiagnosticEnabled(boolean z) {
        this.diagnosticEnabled = z;
    }

    @DexIgnore
    public final void setEmail(String str) {
        pq7.c(str, "<set-?>");
        this.email = str;
    }

    @DexIgnore
    public final void setFirstName(String str) {
        pq7.c(str, "<set-?>");
        this.firstName = str;
    }

    @DexIgnore
    public final void setGender(String str) {
        pq7.c(str, "<set-?>");
        this.gender = str;
    }

    @DexIgnore
    public final void setLastName(String str) {
        pq7.c(str, "<set-?>");
        this.lastName = str;
    }

    @DexIgnore
    public final void setService(String str) {
        pq7.c(str, "<set-?>");
        this.service = str;
    }

    @DexIgnore
    public final void setToken(String str) {
        pq7.c(str, "<set-?>");
        this.token = str;
    }

    @DexIgnore
    public String toString() {
        return "SignUpSocialAuth(email=" + this.email + ", service=" + this.service + ", token=" + this.token + ", clientId=" + this.clientId + ", firstName=" + this.firstName + ", lastName=" + this.lastName + ", birthday=" + this.birthday + ", gender=" + this.gender + ", diagnosticEnabled=" + this.diagnosticEnabled + ", acceptedLocationDataSharing=" + this.acceptedLocationDataSharing + ", acceptedPrivacies=" + this.acceptedPrivacies + ", acceptedTermsOfService=" + this.acceptedTermsOfService + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.email);
        parcel.writeString(this.service);
        parcel.writeString(this.token);
        parcel.writeString(this.clientId);
        parcel.writeString(this.firstName);
        parcel.writeString(this.lastName);
        parcel.writeString(this.birthday);
        parcel.writeString(this.gender);
        parcel.writeByte(this.diagnosticEnabled ? (byte) 1 : 0);
        parcel.writeStringList(this.acceptedLocationDataSharing);
        parcel.writeStringList(this.acceptedPrivacies);
        parcel.writeStringList(this.acceptedTermsOfService);
    }
}
