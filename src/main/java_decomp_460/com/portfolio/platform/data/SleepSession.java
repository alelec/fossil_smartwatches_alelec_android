package com.portfolio.platform.data;

import com.fossil.lk5;
import com.fossil.oh5;
import com.fossil.sk5;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SleepSession extends ServerError {
    @DexIgnore
    public static /* final */ String TAG; // = SleepSession.class.getSimpleName();
    @DexIgnore
    public /* final */ Date bookmarkTime;
    @DexIgnore
    public /* final */ String date;
    @DexIgnore
    public /* final */ String deviceSerialNumber;
    @DexIgnore
    public /* final */ Date editedEndTime;
    @DexIgnore
    public /* final */ int editedSleepMinutes;
    @DexIgnore
    public /* final */ int[] editedSleepStateDistInMinute;
    @DexIgnore
    public /* final */ Date editedStartTime;
    @DexIgnore
    public SleepSessionHeartRate heartRate;
    @DexIgnore
    public /* final */ String id;
    @DexIgnore
    public /* final */ Date realEndTime;
    @DexIgnore
    public /* final */ int realSleepMinutes;
    @DexIgnore
    public /* final */ int[] realSleepStateDistInMinute;
    @DexIgnore
    public /* final */ Date realStartTime;
    @DexIgnore
    public /* final */ double sleepQuality;
    @DexIgnore
    public /* final */ List<int[]> sleepStates;
    @DexIgnore
    public /* final */ String source;
    @DexIgnore
    public /* final */ Date syncTime;
    @DexIgnore
    public /* final */ int timezoneOffset;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends TypeToken<List<WrapperSleepStateChange>> {
        @DexIgnore
        public Anon1() {
        }
    }

    @DexIgnore
    public SleepSession(String str, MFSleepSession mFSleepSession) {
        List<WrapperSleepStateChange> list;
        int timezoneOffset2 = mFSleepSession.getTimezoneOffset();
        this.timezoneOffset = timezoneOffset2;
        TimeZone t = lk5.t(timezoneOffset2);
        Calendar instance = Calendar.getInstance();
        instance.setTimeZone(t);
        this.id = sk5.c.b(str, mFSleepSession);
        this.date = lk5.l(new Date(mFSleepSession.getDate()), t);
        this.source = oh5.values()[mFSleepSession.getSource()].getValue().toLowerCase();
        this.deviceSerialNumber = mFSleepSession.getDeviceSerialNumber();
        instance.setTimeInMillis(((long) mFSleepSession.getSyncTime().intValue()) * 1000);
        this.syncTime = instance.getTime();
        instance.setTimeInMillis(((long) mFSleepSession.getBookmarkTime().intValue()) * 1000);
        this.bookmarkTime = instance.getTime();
        instance.setTimeInMillis(((long) mFSleepSession.getRealStartTime()) * 1000);
        this.realStartTime = instance.getTime();
        instance.setTimeInMillis(((long) mFSleepSession.getRealEndTime()) * 1000);
        this.realEndTime = instance.getTime();
        this.realSleepMinutes = mFSleepSession.getRealSleepMinutes();
        this.realSleepStateDistInMinute = mFSleepSession.getRealSleepStateDistInMinute().getArrayDistribution();
        instance.setTimeInMillis(((long) mFSleepSession.getStartTime()) * 1000);
        this.editedStartTime = instance.getTime();
        instance.setTimeInMillis(((long) mFSleepSession.getEndTime()) * 1000);
        this.editedEndTime = instance.getTime();
        this.editedSleepMinutes = mFSleepSession.getSleepMinutes();
        this.editedSleepStateDistInMinute = mFSleepSession.getSleepState().getArrayDistribution();
        this.sleepQuality = mFSleepSession.getNormalizedSleepQuality();
        ArrayList arrayList = new ArrayList();
        try {
            list = (List) new Gson().l(mFSleepSession.getSleepStates(), new Anon1().getType());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.e(str2, "exception=" + e.getMessage());
            e.printStackTrace();
            list = arrayList;
        }
        this.sleepStates = new ArrayList();
        for (WrapperSleepStateChange wrapperSleepStateChange : list) {
            if (wrapperSleepStateChange != null) {
                this.sleepStates.add(new int[]{wrapperSleepStateChange.getState(), (int) wrapperSleepStateChange.getIndex()});
            }
        }
        this.heartRate = mFSleepSession.getHeartRate();
    }

    @DexIgnore
    public Date getRealEndTime() {
        return this.realEndTime;
    }
}
