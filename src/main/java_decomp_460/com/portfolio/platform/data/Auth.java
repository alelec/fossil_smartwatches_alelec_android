package com.portfolio.platform.data;

import com.fossil.rj4;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Auth {
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_ACCESS_TOKEN)
    public /* final */ String mAccessToken;
    @DexIgnore
    @rj4("accessTokenExpiresAt")
    public /* final */ Date mAccessTokenExpiresAt;
    @DexIgnore
    @rj4("accessTokenExpiresIn")
    public /* final */ Integer mAccessTokenExpiresIn;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_REFRESH_TOKEN)
    public /* final */ String mRefreshToken;
    @DexIgnore
    @rj4("uid")
    public /* final */ String mUid;

    @DexIgnore
    public Auth(String str, String str2, Date date, Integer num, String str3) {
        this.mAccessToken = str;
        this.mRefreshToken = str2;
        this.mAccessTokenExpiresAt = date;
        this.mAccessTokenExpiresIn = num;
        this.mUid = str3;
    }

    @DexIgnore
    public final String getAccessToken() {
        return this.mAccessToken;
    }

    @DexIgnore
    public final Date getAccessTokenExpiresAt() {
        return this.mAccessTokenExpiresAt;
    }

    @DexIgnore
    public final Integer getAccessTokenExpiresIn() {
        return this.mAccessTokenExpiresIn;
    }

    @DexIgnore
    public final String getRefreshToken() {
        return this.mRefreshToken;
    }

    @DexIgnore
    public final String getUid() {
        return this.mUid;
    }
}
