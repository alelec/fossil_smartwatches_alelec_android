package com.portfolio.platform.data;

import com.fossil.c;
import com.fossil.pq7;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.model.fitnessdata.CadenceWrapper;
import com.portfolio.platform.data.model.fitnessdata.CalorieWrapper;
import com.portfolio.platform.data.model.fitnessdata.DistanceWrapper;
import com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper;
import com.portfolio.platform.data.model.fitnessdata.PaceWrapper;
import com.portfolio.platform.data.model.fitnessdata.StepWrapper;
import com.portfolio.platform.data.model.fitnessdata.WorkoutStateChangeWrapper;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerWorkoutSessionWrapper {
    @DexIgnore
    public /* final */ CadenceWrapper cadence;
    @DexIgnore
    public /* final */ CalorieWrapper calorie;
    @DexIgnore
    public /* final */ DistanceWrapper distance;
    @DexIgnore
    public /* final */ int duration;
    @DexIgnore
    public /* final */ DateTime endTime;
    @DexIgnore
    public /* final */ String gpsDataPoints;
    @DexIgnore
    public /* final */ HeartRateWrapper heartRate;
    @DexIgnore
    public /* final */ long id;
    @DexIgnore
    public /* final */ int mode;
    @DexIgnore
    public /* final */ PaceWrapper pace;
    @DexIgnore
    public /* final */ DateTime startTime;
    @DexIgnore
    public /* final */ List<WorkoutStateChangeWrapper> stateChanges;
    @DexIgnore
    public /* final */ StepWrapper step;
    @DexIgnore
    public /* final */ int timezoneOffsetInSecond;
    @DexIgnore
    public /* final */ int type;

    @DexIgnore
    public ServerWorkoutSessionWrapper(long j, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, PaceWrapper paceWrapper, CadenceWrapper cadenceWrapper, List<WorkoutStateChangeWrapper> list, String str, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper) {
        pq7.c(dateTime, SampleRaw.COLUMN_START_TIME);
        pq7.c(dateTime2, SampleRaw.COLUMN_END_TIME);
        pq7.c(stepWrapper, "step");
        pq7.c(calorieWrapper, "calorie");
        this.id = j;
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.timezoneOffsetInSecond = i;
        this.duration = i2;
        this.type = i3;
        this.mode = i4;
        this.pace = paceWrapper;
        this.cadence = cadenceWrapper;
        this.stateChanges = list;
        this.gpsDataPoints = str;
        this.step = stepWrapper;
        this.calorie = calorieWrapper;
        this.distance = distanceWrapper;
        this.heartRate = heartRateWrapper;
    }

    @DexIgnore
    public static /* synthetic */ ServerWorkoutSessionWrapper copy$default(ServerWorkoutSessionWrapper serverWorkoutSessionWrapper, long j, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, PaceWrapper paceWrapper, CadenceWrapper cadenceWrapper, List list, String str, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper, int i5, Object obj) {
        return serverWorkoutSessionWrapper.copy((i5 & 1) != 0 ? serverWorkoutSessionWrapper.id : j, (i5 & 2) != 0 ? serverWorkoutSessionWrapper.startTime : dateTime, (i5 & 4) != 0 ? serverWorkoutSessionWrapper.endTime : dateTime2, (i5 & 8) != 0 ? serverWorkoutSessionWrapper.timezoneOffsetInSecond : i, (i5 & 16) != 0 ? serverWorkoutSessionWrapper.duration : i2, (i5 & 32) != 0 ? serverWorkoutSessionWrapper.type : i3, (i5 & 64) != 0 ? serverWorkoutSessionWrapper.mode : i4, (i5 & 128) != 0 ? serverWorkoutSessionWrapper.pace : paceWrapper, (i5 & 256) != 0 ? serverWorkoutSessionWrapper.cadence : cadenceWrapper, (i5 & 512) != 0 ? serverWorkoutSessionWrapper.stateChanges : list, (i5 & 1024) != 0 ? serverWorkoutSessionWrapper.gpsDataPoints : str, (i5 & 2048) != 0 ? serverWorkoutSessionWrapper.step : stepWrapper, (i5 & 4096) != 0 ? serverWorkoutSessionWrapper.calorie : calorieWrapper, (i5 & 8192) != 0 ? serverWorkoutSessionWrapper.distance : distanceWrapper, (i5 & 16384) != 0 ? serverWorkoutSessionWrapper.heartRate : heartRateWrapper);
    }

    @DexIgnore
    public final long component1() {
        return this.id;
    }

    @DexIgnore
    public final List<WorkoutStateChangeWrapper> component10() {
        return this.stateChanges;
    }

    @DexIgnore
    public final String component11() {
        return this.gpsDataPoints;
    }

    @DexIgnore
    public final StepWrapper component12() {
        return this.step;
    }

    @DexIgnore
    public final CalorieWrapper component13() {
        return this.calorie;
    }

    @DexIgnore
    public final DistanceWrapper component14() {
        return this.distance;
    }

    @DexIgnore
    public final HeartRateWrapper component15() {
        return this.heartRate;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.endTime;
    }

    @DexIgnore
    public final int component4() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int component5() {
        return this.duration;
    }

    @DexIgnore
    public final int component6() {
        return this.type;
    }

    @DexIgnore
    public final int component7() {
        return this.mode;
    }

    @DexIgnore
    public final PaceWrapper component8() {
        return this.pace;
    }

    @DexIgnore
    public final CadenceWrapper component9() {
        return this.cadence;
    }

    @DexIgnore
    public final ServerWorkoutSessionWrapper copy(long j, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, PaceWrapper paceWrapper, CadenceWrapper cadenceWrapper, List<WorkoutStateChangeWrapper> list, String str, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper) {
        pq7.c(dateTime, SampleRaw.COLUMN_START_TIME);
        pq7.c(dateTime2, SampleRaw.COLUMN_END_TIME);
        pq7.c(stepWrapper, "step");
        pq7.c(calorieWrapper, "calorie");
        return new ServerWorkoutSessionWrapper(j, dateTime, dateTime2, i, i2, i3, i4, paceWrapper, cadenceWrapper, list, str, stepWrapper, calorieWrapper, distanceWrapper, heartRateWrapper);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ServerWorkoutSessionWrapper) {
                ServerWorkoutSessionWrapper serverWorkoutSessionWrapper = (ServerWorkoutSessionWrapper) obj;
                if (this.id != serverWorkoutSessionWrapper.id || !pq7.a(this.startTime, serverWorkoutSessionWrapper.startTime) || !pq7.a(this.endTime, serverWorkoutSessionWrapper.endTime) || this.timezoneOffsetInSecond != serverWorkoutSessionWrapper.timezoneOffsetInSecond || this.duration != serverWorkoutSessionWrapper.duration || this.type != serverWorkoutSessionWrapper.type || this.mode != serverWorkoutSessionWrapper.mode || !pq7.a(this.pace, serverWorkoutSessionWrapper.pace) || !pq7.a(this.cadence, serverWorkoutSessionWrapper.cadence) || !pq7.a(this.stateChanges, serverWorkoutSessionWrapper.stateChanges) || !pq7.a(this.gpsDataPoints, serverWorkoutSessionWrapper.gpsDataPoints) || !pq7.a(this.step, serverWorkoutSessionWrapper.step) || !pq7.a(this.calorie, serverWorkoutSessionWrapper.calorie) || !pq7.a(this.distance, serverWorkoutSessionWrapper.distance) || !pq7.a(this.heartRate, serverWorkoutSessionWrapper.heartRate)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final CadenceWrapper getCadence() {
        return this.cadence;
    }

    @DexIgnore
    public final CalorieWrapper getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public final DistanceWrapper getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final int getDuration() {
        return this.duration;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final String getGpsDataPoints() {
        return this.gpsDataPoints;
    }

    @DexIgnore
    public final HeartRateWrapper getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final long getId() {
        return this.id;
    }

    @DexIgnore
    public final int getMode() {
        return this.mode;
    }

    @DexIgnore
    public final PaceWrapper getPace() {
        return this.pace;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final List<WorkoutStateChangeWrapper> getStateChanges() {
        return this.stateChanges;
    }

    @DexIgnore
    public final StepWrapper getStep() {
        return this.step;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        int a2 = c.a(this.id);
        DateTime dateTime = this.startTime;
        int i = 0;
        int hashCode = dateTime != null ? dateTime.hashCode() : 0;
        DateTime dateTime2 = this.endTime;
        int hashCode2 = dateTime2 != null ? dateTime2.hashCode() : 0;
        int i2 = this.timezoneOffsetInSecond;
        int i3 = this.duration;
        int i4 = this.type;
        int i5 = this.mode;
        PaceWrapper paceWrapper = this.pace;
        int hashCode3 = paceWrapper != null ? paceWrapper.hashCode() : 0;
        CadenceWrapper cadenceWrapper = this.cadence;
        int hashCode4 = cadenceWrapper != null ? cadenceWrapper.hashCode() : 0;
        List<WorkoutStateChangeWrapper> list = this.stateChanges;
        int hashCode5 = list != null ? list.hashCode() : 0;
        String str = this.gpsDataPoints;
        int hashCode6 = str != null ? str.hashCode() : 0;
        StepWrapper stepWrapper = this.step;
        int hashCode7 = stepWrapper != null ? stepWrapper.hashCode() : 0;
        CalorieWrapper calorieWrapper = this.calorie;
        int hashCode8 = calorieWrapper != null ? calorieWrapper.hashCode() : 0;
        DistanceWrapper distanceWrapper = this.distance;
        int hashCode9 = distanceWrapper != null ? distanceWrapper.hashCode() : 0;
        HeartRateWrapper heartRateWrapper = this.heartRate;
        if (heartRateWrapper != null) {
            i = heartRateWrapper.hashCode();
        }
        return ((((((((((((((((((((((((((hashCode + (a2 * 31)) * 31) + hashCode2) * 31) + i2) * 31) + i3) * 31) + i4) * 31) + i5) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "ServerWorkoutSessionWrapper(id=" + this.id + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", duration=" + this.duration + ", type=" + this.type + ", mode=" + this.mode + ", pace=" + this.pace + ", cadence=" + this.cadence + ", stateChanges=" + this.stateChanges + ", gpsDataPoints=" + this.gpsDataPoints + ", step=" + this.step + ", calorie=" + this.calorie + ", distance=" + this.distance + ", heartRate=" + this.heartRate + ")";
    }
}
