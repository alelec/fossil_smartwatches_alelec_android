package com.portfolio.platform.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.sk5;
import com.fossil.wearables.fsl.fitness.SampleDay;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityIntensities implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public double intense;
    @DexIgnore
    public double light;
    @DexIgnore
    public double moderate;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ActivityIntensities> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActivityIntensities createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new ActivityIntensities(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActivityIntensities[] newArray(int i) {
            return new ActivityIntensities[i];
        }
    }

    @DexIgnore
    public ActivityIntensities() {
        this(0.0d, 0.0d, 0.0d);
    }

    @DexIgnore
    public ActivityIntensities(double d, double d2, double d3) {
        this.light = d;
        this.moderate = d2;
        this.intense = d3;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ActivityIntensities(double d, double d2, double d3, int i, kq7 kq7) {
        this((i & 1) != 0 ? 0.0d : d, (i & 2) != 0 ? 0.0d : d2, (i & 4) != 0 ? 0.0d : d3);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityIntensities(Parcel parcel) {
        this(parcel.readDouble(), parcel.readDouble(), parcel.readDouble());
        pq7.c(parcel, "parcel");
    }

    @DexIgnore
    public static /* synthetic */ ActivityIntensities copy$default(ActivityIntensities activityIntensities, double d, double d2, double d3, int i, Object obj) {
        return activityIntensities.copy((i & 1) != 0 ? activityIntensities.light : d, (i & 2) != 0 ? activityIntensities.moderate : d2, (i & 4) != 0 ? activityIntensities.intense : d3);
    }

    @DexIgnore
    public final double component1() {
        return this.light;
    }

    @DexIgnore
    public final double component2() {
        return this.moderate;
    }

    @DexIgnore
    public final double component3() {
        return this.intense;
    }

    @DexIgnore
    public final ActivityIntensities copy(double d, double d2, double d3) {
        return new ActivityIntensities(d, d2, d3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ActivityIntensities) {
                ActivityIntensities activityIntensities = (ActivityIntensities) obj;
                if (!(Double.compare(this.light, activityIntensities.light) == 0 && Double.compare(this.moderate, activityIntensities.moderate) == 0 && Double.compare(this.intense, activityIntensities.intense) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final double getIntense() {
        return this.intense;
    }

    @DexIgnore
    public final List<Integer> getIntensitiesInArray() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(Integer.valueOf((int) this.light));
        arrayList.add(Integer.valueOf((int) this.moderate));
        arrayList.add(Integer.valueOf((int) this.intense));
        return arrayList;
    }

    @DexIgnore
    public final double getLight() {
        return this.light;
    }

    @DexIgnore
    public final double getModerate() {
        return this.moderate;
    }

    @DexIgnore
    public int hashCode() {
        return (((Double.doubleToLongBits(this.light) * 31) + Double.doubleToLongBits(this.moderate)) * 31) + Double.doubleToLongBits(this.intense);
    }

    @DexIgnore
    public final void setIntense(double d) {
        this.intense = d;
    }

    @DexIgnore
    public final void setIntensity(double d) {
        int a2 = sk5.c.a((long) d);
        if (a2 == 0) {
            this.light += d;
        } else if (a2 == 1) {
            this.moderate += d;
        } else if (a2 == 2) {
            this.intense += d;
        }
    }

    @DexIgnore
    public final void setLight(double d) {
        this.light = d;
    }

    @DexIgnore
    public final void setModerate(double d) {
        this.moderate = d;
    }

    @DexIgnore
    public String toString() {
        return "ActivityIntensities(light=" + this.light + ", moderate=" + this.moderate + ", intense=" + this.intense + ")";
    }

    @DexIgnore
    public final void updateActivityIntensities(ActivityIntensities activityIntensities) {
        pq7.c(activityIntensities, SampleDay.COLUMN_INTENSITIES);
        this.light += activityIntensities.light;
        this.moderate += activityIntensities.moderate;
        this.intense += activityIntensities.intense;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeDouble(this.light);
        parcel.writeDouble(this.moderate);
        parcel.writeDouble(this.intense);
    }
}
