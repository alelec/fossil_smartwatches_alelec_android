package com.portfolio.platform.data;

import androidx.lifecycle.LiveData;
import com.fossil.cu0;
import com.fossil.gp7;
import com.fossil.pq7;
import com.fossil.tl7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Listing<T> {
    @DexIgnore
    public /* final */ LiveData<NetworkState> networkState;
    @DexIgnore
    public /* final */ LiveData<cu0<T>> pagedList;
    @DexIgnore
    public /* final */ gp7<tl7> refresh;
    @DexIgnore
    public /* final */ gp7<tl7> retry;

    @DexIgnore
    public Listing(LiveData<cu0<T>> liveData, LiveData<NetworkState> liveData2, gp7<tl7> gp7, gp7<tl7> gp72) {
        pq7.c(liveData, "pagedList");
        pq7.c(liveData2, "networkState");
        pq7.c(gp7, "refresh");
        pq7.c(gp72, "retry");
        this.pagedList = liveData;
        this.networkState = liveData2;
        this.refresh = gp7;
        this.retry = gp72;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.Listing */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Listing copy$default(Listing listing, LiveData liveData, LiveData liveData2, gp7 gp7, gp7 gp72, int i, Object obj) {
        if ((i & 1) != 0) {
            liveData = listing.pagedList;
        }
        if ((i & 2) != 0) {
            liveData2 = listing.networkState;
        }
        if ((i & 4) != 0) {
            gp7 = listing.refresh;
        }
        if ((i & 8) != 0) {
            gp72 = listing.retry;
        }
        return listing.copy(liveData, liveData2, gp7, gp72);
    }

    @DexIgnore
    public final LiveData<cu0<T>> component1() {
        return this.pagedList;
    }

    @DexIgnore
    public final LiveData<NetworkState> component2() {
        return this.networkState;
    }

    @DexIgnore
    public final gp7<tl7> component3() {
        return this.refresh;
    }

    @DexIgnore
    public final gp7<tl7> component4() {
        return this.retry;
    }

    @DexIgnore
    public final Listing<T> copy(LiveData<cu0<T>> liveData, LiveData<NetworkState> liveData2, gp7<tl7> gp7, gp7<tl7> gp72) {
        pq7.c(liveData, "pagedList");
        pq7.c(liveData2, "networkState");
        pq7.c(gp7, "refresh");
        pq7.c(gp72, "retry");
        return new Listing<>(liveData, liveData2, gp7, gp72);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Listing) {
                Listing listing = (Listing) obj;
                if (!pq7.a(this.pagedList, listing.pagedList) || !pq7.a(this.networkState, listing.networkState) || !pq7.a(this.refresh, listing.refresh) || !pq7.a(this.retry, listing.retry)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final LiveData<NetworkState> getNetworkState() {
        return this.networkState;
    }

    @DexIgnore
    public final LiveData<cu0<T>> getPagedList() {
        return this.pagedList;
    }

    @DexIgnore
    public final gp7<tl7> getRefresh() {
        return this.refresh;
    }

    @DexIgnore
    public final gp7<tl7> getRetry() {
        return this.retry;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        LiveData<cu0<T>> liveData = this.pagedList;
        int hashCode = liveData != null ? liveData.hashCode() : 0;
        LiveData<NetworkState> liveData2 = this.networkState;
        int hashCode2 = liveData2 != null ? liveData2.hashCode() : 0;
        gp7<tl7> gp7 = this.refresh;
        int hashCode3 = gp7 != null ? gp7.hashCode() : 0;
        gp7<tl7> gp72 = this.retry;
        if (gp72 != null) {
            i = gp72.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Listing(pagedList=" + this.pagedList + ", networkState=" + this.networkState + ", refresh=" + this.refresh + ", retry=" + this.retry + ")";
    }
}
