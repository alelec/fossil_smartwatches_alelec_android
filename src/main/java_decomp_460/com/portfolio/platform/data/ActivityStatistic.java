package com.portfolio.platform.data;

import com.fossil.b;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rj4;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityStatistic {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String ID; // = "id";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "activity_statistic";
    @DexIgnore
    @rj4("activeTimeBestDay")
    public /* final */ ActivityDailyBest activeTimeBestDay;
    @DexIgnore
    @rj4("activeTimeBestStreak")
    public /* final */ ActivityDailyBest activeTimeBestStreak;
    @DexIgnore
    @rj4("caloriesBestDay")
    public /* final */ CaloriesBestDay caloriesBestDay;
    @DexIgnore
    @rj4("caloriesBestStreak")
    public /* final */ ActivityDailyBest caloriesBestStreak;
    @DexIgnore
    @rj4("createdAt")
    public /* final */ DateTime createdAt;
    @DexIgnore
    @rj4("id")
    public /* final */ String id;
    @DexIgnore
    @rj4("stepsBestDay")
    public /* final */ ActivityDailyBest stepsBestDay;
    @DexIgnore
    @rj4("stepsBestStreak")
    public /* final */ ActivityDailyBest stepsBestStreak;
    @DexIgnore
    @rj4("totalActiveTime")
    public /* final */ int totalActiveTime;
    @DexIgnore
    @rj4("totalCalories")
    public /* final */ double totalCalories;
    @DexIgnore
    @rj4("totalDays")
    public /* final */ int totalDays;
    @DexIgnore
    @rj4("totalDistance")
    public /* final */ double totalDistance;
    @DexIgnore
    @rj4("totalIntensityDistInStep")
    public /* final */ List<Integer> totalIntensityDistInStep;
    @DexIgnore
    @rj4("totalSteps")
    public /* final */ int totalSteps;
    @DexIgnore
    @rj4("uid")
    public /* final */ String uid;
    @DexIgnore
    @rj4("updatedAt")
    public /* final */ DateTime updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class ActivityDailyBest {
        @DexIgnore
        public /* final */ String activityDailySummaryId;
        @DexIgnore
        public /* final */ Date date;
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public ActivityDailyBest(String str, Date date2, int i) {
            pq7.c(str, "activityDailySummaryId");
            pq7.c(date2, "date");
            this.activityDailySummaryId = str;
            this.date = date2;
            this.value = i;
        }

        @DexIgnore
        public static /* synthetic */ ActivityDailyBest copy$default(ActivityDailyBest activityDailyBest, String str, Date date2, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = activityDailyBest.activityDailySummaryId;
            }
            if ((i2 & 2) != 0) {
                date2 = activityDailyBest.date;
            }
            if ((i2 & 4) != 0) {
                i = activityDailyBest.value;
            }
            return activityDailyBest.copy(str, date2, i);
        }

        @DexIgnore
        public final String component1() {
            return this.activityDailySummaryId;
        }

        @DexIgnore
        public final Date component2() {
            return this.date;
        }

        @DexIgnore
        public final int component3() {
            return this.value;
        }

        @DexIgnore
        public final ActivityDailyBest copy(String str, Date date2, int i) {
            pq7.c(str, "activityDailySummaryId");
            pq7.c(date2, "date");
            return new ActivityDailyBest(str, date2, i);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof ActivityDailyBest) {
                    ActivityDailyBest activityDailyBest = (ActivityDailyBest) obj;
                    if (!pq7.a(this.activityDailySummaryId, activityDailyBest.activityDailySummaryId) || !pq7.a(this.date, activityDailyBest.date) || this.value != activityDailyBest.value) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final String getActivityDailySummaryId() {
            return this.activityDailySummaryId;
        }

        @DexIgnore
        public final Date getDate() {
            return this.date;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.activityDailySummaryId;
            int hashCode = str != null ? str.hashCode() : 0;
            Date date2 = this.date;
            if (date2 != null) {
                i = date2.hashCode();
            }
            return (((hashCode * 31) + i) * 31) + this.value;
        }

        @DexIgnore
        public String toString() {
            return "ActivityDailyBest(activityDailySummaryId=" + this.activityDailySummaryId + ", date=" + this.date + ", value=" + this.value + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CaloriesBestDay {
        @DexIgnore
        public /* final */ String activityDailySummaryId;
        @DexIgnore
        public /* final */ Date date;
        @DexIgnore
        public /* final */ double value;

        @DexIgnore
        public CaloriesBestDay(String str, Date date2, double d) {
            pq7.c(str, "activityDailySummaryId");
            pq7.c(date2, "date");
            this.activityDailySummaryId = str;
            this.date = date2;
            this.value = d;
        }

        @DexIgnore
        public static /* synthetic */ CaloriesBestDay copy$default(CaloriesBestDay caloriesBestDay, String str, Date date2, double d, int i, Object obj) {
            if ((i & 1) != 0) {
                str = caloriesBestDay.activityDailySummaryId;
            }
            if ((i & 2) != 0) {
                date2 = caloriesBestDay.date;
            }
            if ((i & 4) != 0) {
                d = caloriesBestDay.value;
            }
            return caloriesBestDay.copy(str, date2, d);
        }

        @DexIgnore
        public final String component1() {
            return this.activityDailySummaryId;
        }

        @DexIgnore
        public final Date component2() {
            return this.date;
        }

        @DexIgnore
        public final double component3() {
            return this.value;
        }

        @DexIgnore
        public final CaloriesBestDay copy(String str, Date date2, double d) {
            pq7.c(str, "activityDailySummaryId");
            pq7.c(date2, "date");
            return new CaloriesBestDay(str, date2, d);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof CaloriesBestDay) {
                    CaloriesBestDay caloriesBestDay = (CaloriesBestDay) obj;
                    if (!pq7.a(this.activityDailySummaryId, caloriesBestDay.activityDailySummaryId) || !pq7.a(this.date, caloriesBestDay.date) || Double.compare(this.value, caloriesBestDay.value) != 0) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final String getActivityDailySummaryId() {
            return this.activityDailySummaryId;
        }

        @DexIgnore
        public final Date getDate() {
            return this.date;
        }

        @DexIgnore
        public final double getValue() {
            return this.value;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.activityDailySummaryId;
            int hashCode = str != null ? str.hashCode() : 0;
            Date date2 = this.date;
            if (date2 != null) {
                i = date2.hashCode();
            }
            return (((hashCode * 31) + i) * 31) + b.a(this.value);
        }

        @DexIgnore
        public String toString() {
            return "CaloriesBestDay(activityDailySummaryId=" + this.activityDailySummaryId + ", date=" + this.date + ", value=" + this.value + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public ActivityStatistic(String str, String str2, ActivityDailyBest activityDailyBest, ActivityDailyBest activityDailyBest2, CaloriesBestDay caloriesBestDay2, ActivityDailyBest activityDailyBest3, ActivityDailyBest activityDailyBest4, ActivityDailyBest activityDailyBest5, int i, double d, int i2, double d2, int i3, List<Integer> list, DateTime dateTime, DateTime dateTime2) {
        pq7.c(str, "id");
        pq7.c(str2, "uid");
        pq7.c(list, "totalIntensityDistInStep");
        pq7.c(dateTime, "createdAt");
        pq7.c(dateTime2, "updatedAt");
        this.id = str;
        this.uid = str2;
        this.activeTimeBestDay = activityDailyBest;
        this.activeTimeBestStreak = activityDailyBest2;
        this.caloriesBestDay = caloriesBestDay2;
        this.caloriesBestStreak = activityDailyBest3;
        this.stepsBestDay = activityDailyBest4;
        this.stepsBestStreak = activityDailyBest5;
        this.totalActiveTime = i;
        this.totalCalories = d;
        this.totalDays = i2;
        this.totalDistance = d2;
        this.totalSteps = i3;
        this.totalIntensityDistInStep = list;
        this.createdAt = dateTime;
        this.updatedAt = dateTime2;
    }

    @DexIgnore
    public static /* synthetic */ ActivityStatistic copy$default(ActivityStatistic activityStatistic, String str, String str2, ActivityDailyBest activityDailyBest, ActivityDailyBest activityDailyBest2, CaloriesBestDay caloriesBestDay2, ActivityDailyBest activityDailyBest3, ActivityDailyBest activityDailyBest4, ActivityDailyBest activityDailyBest5, int i, double d, int i2, double d2, int i3, List list, DateTime dateTime, DateTime dateTime2, int i4, Object obj) {
        return activityStatistic.copy((i4 & 1) != 0 ? activityStatistic.id : str, (i4 & 2) != 0 ? activityStatistic.uid : str2, (i4 & 4) != 0 ? activityStatistic.activeTimeBestDay : activityDailyBest, (i4 & 8) != 0 ? activityStatistic.activeTimeBestStreak : activityDailyBest2, (i4 & 16) != 0 ? activityStatistic.caloriesBestDay : caloriesBestDay2, (i4 & 32) != 0 ? activityStatistic.caloriesBestStreak : activityDailyBest3, (i4 & 64) != 0 ? activityStatistic.stepsBestDay : activityDailyBest4, (i4 & 128) != 0 ? activityStatistic.stepsBestStreak : activityDailyBest5, (i4 & 256) != 0 ? activityStatistic.totalActiveTime : i, (i4 & 512) != 0 ? activityStatistic.totalCalories : d, (i4 & 1024) != 0 ? activityStatistic.totalDays : i2, (i4 & 2048) != 0 ? activityStatistic.totalDistance : d2, (i4 & 4096) != 0 ? activityStatistic.totalSteps : i3, (i4 & 8192) != 0 ? activityStatistic.totalIntensityDistInStep : list, (i4 & 16384) != 0 ? activityStatistic.createdAt : dateTime, (32768 & i4) != 0 ? activityStatistic.updatedAt : dateTime2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final double component10() {
        return this.totalCalories;
    }

    @DexIgnore
    public final int component11() {
        return this.totalDays;
    }

    @DexIgnore
    public final double component12() {
        return this.totalDistance;
    }

    @DexIgnore
    public final int component13() {
        return this.totalSteps;
    }

    @DexIgnore
    public final List<Integer> component14() {
        return this.totalIntensityDistInStep;
    }

    @DexIgnore
    public final DateTime component15() {
        return this.createdAt;
    }

    @DexIgnore
    public final DateTime component16() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String component2() {
        return this.uid;
    }

    @DexIgnore
    public final ActivityDailyBest component3() {
        return this.activeTimeBestDay;
    }

    @DexIgnore
    public final ActivityDailyBest component4() {
        return this.activeTimeBestStreak;
    }

    @DexIgnore
    public final CaloriesBestDay component5() {
        return this.caloriesBestDay;
    }

    @DexIgnore
    public final ActivityDailyBest component6() {
        return this.caloriesBestStreak;
    }

    @DexIgnore
    public final ActivityDailyBest component7() {
        return this.stepsBestDay;
    }

    @DexIgnore
    public final ActivityDailyBest component8() {
        return this.stepsBestStreak;
    }

    @DexIgnore
    public final int component9() {
        return this.totalActiveTime;
    }

    @DexIgnore
    public final ActivityStatistic copy(String str, String str2, ActivityDailyBest activityDailyBest, ActivityDailyBest activityDailyBest2, CaloriesBestDay caloriesBestDay2, ActivityDailyBest activityDailyBest3, ActivityDailyBest activityDailyBest4, ActivityDailyBest activityDailyBest5, int i, double d, int i2, double d2, int i3, List<Integer> list, DateTime dateTime, DateTime dateTime2) {
        pq7.c(str, "id");
        pq7.c(str2, "uid");
        pq7.c(list, "totalIntensityDistInStep");
        pq7.c(dateTime, "createdAt");
        pq7.c(dateTime2, "updatedAt");
        return new ActivityStatistic(str, str2, activityDailyBest, activityDailyBest2, caloriesBestDay2, activityDailyBest3, activityDailyBest4, activityDailyBest5, i, d, i2, d2, i3, list, dateTime, dateTime2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ActivityStatistic) {
                ActivityStatistic activityStatistic = (ActivityStatistic) obj;
                if (!pq7.a(this.id, activityStatistic.id) || !pq7.a(this.uid, activityStatistic.uid) || !pq7.a(this.activeTimeBestDay, activityStatistic.activeTimeBestDay) || !pq7.a(this.activeTimeBestStreak, activityStatistic.activeTimeBestStreak) || !pq7.a(this.caloriesBestDay, activityStatistic.caloriesBestDay) || !pq7.a(this.caloriesBestStreak, activityStatistic.caloriesBestStreak) || !pq7.a(this.stepsBestDay, activityStatistic.stepsBestDay) || !pq7.a(this.stepsBestStreak, activityStatistic.stepsBestStreak) || this.totalActiveTime != activityStatistic.totalActiveTime || Double.compare(this.totalCalories, activityStatistic.totalCalories) != 0 || this.totalDays != activityStatistic.totalDays || Double.compare(this.totalDistance, activityStatistic.totalDistance) != 0 || this.totalSteps != activityStatistic.totalSteps || !pq7.a(this.totalIntensityDistInStep, activityStatistic.totalIntensityDistInStep) || !pq7.a(this.createdAt, activityStatistic.createdAt) || !pq7.a(this.updatedAt, activityStatistic.updatedAt)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ActivityDailyBest getActiveTimeBestDay() {
        return this.activeTimeBestDay;
    }

    @DexIgnore
    public final ActivityDailyBest getActiveTimeBestStreak() {
        return this.activeTimeBestStreak;
    }

    @DexIgnore
    public final CaloriesBestDay getCaloriesBestDay() {
        return this.caloriesBestDay;
    }

    @DexIgnore
    public final ActivityDailyBest getCaloriesBestStreak() {
        return this.caloriesBestStreak;
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final ActivityDailyBest getStepsBestDay() {
        return this.stepsBestDay;
    }

    @DexIgnore
    public final ActivityDailyBest getStepsBestStreak() {
        return this.stepsBestStreak;
    }

    @DexIgnore
    public final int getTotalActiveTime() {
        return this.totalActiveTime;
    }

    @DexIgnore
    public final double getTotalCalories() {
        return this.totalCalories;
    }

    @DexIgnore
    public final int getTotalDays() {
        return this.totalDays;
    }

    @DexIgnore
    public final double getTotalDistance() {
        return this.totalDistance;
    }

    @DexIgnore
    public final List<Integer> getTotalIntensityDistInStep() {
        return this.totalIntensityDistInStep;
    }

    @DexIgnore
    public final int getTotalSteps() {
        return this.totalSteps;
    }

    @DexIgnore
    public final String getUid() {
        return this.uid;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.uid;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        ActivityDailyBest activityDailyBest = this.activeTimeBestDay;
        int hashCode3 = activityDailyBest != null ? activityDailyBest.hashCode() : 0;
        ActivityDailyBest activityDailyBest2 = this.activeTimeBestStreak;
        int hashCode4 = activityDailyBest2 != null ? activityDailyBest2.hashCode() : 0;
        CaloriesBestDay caloriesBestDay2 = this.caloriesBestDay;
        int hashCode5 = caloriesBestDay2 != null ? caloriesBestDay2.hashCode() : 0;
        ActivityDailyBest activityDailyBest3 = this.caloriesBestStreak;
        int hashCode6 = activityDailyBest3 != null ? activityDailyBest3.hashCode() : 0;
        ActivityDailyBest activityDailyBest4 = this.stepsBestDay;
        int hashCode7 = activityDailyBest4 != null ? activityDailyBest4.hashCode() : 0;
        ActivityDailyBest activityDailyBest5 = this.stepsBestStreak;
        int hashCode8 = activityDailyBest5 != null ? activityDailyBest5.hashCode() : 0;
        int i2 = this.totalActiveTime;
        int a2 = b.a(this.totalCalories);
        int i3 = this.totalDays;
        int a3 = b.a(this.totalDistance);
        int i4 = this.totalSteps;
        List<Integer> list = this.totalIntensityDistInStep;
        int hashCode9 = list != null ? list.hashCode() : 0;
        DateTime dateTime = this.createdAt;
        int hashCode10 = dateTime != null ? dateTime.hashCode() : 0;
        DateTime dateTime2 = this.updatedAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return (((((((((((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + i2) * 31) + a2) * 31) + i3) * 31) + a3) * 31) + i4) * 31) + hashCode9) * 31) + hashCode10) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "ActivityStatistic(id=" + this.id + ", uid=" + this.uid + ", activeTimeBestDay=" + this.activeTimeBestDay + ", activeTimeBestStreak=" + this.activeTimeBestStreak + ", caloriesBestDay=" + this.caloriesBestDay + ", caloriesBestStreak=" + this.caloriesBestStreak + ", stepsBestDay=" + this.stepsBestDay + ", stepsBestStreak=" + this.stepsBestStreak + ", totalActiveTime=" + this.totalActiveTime + ", totalCalories=" + this.totalCalories + ", totalDays=" + this.totalDays + ", totalDistance=" + this.totalDistance + ", totalSteps=" + this.totalSteps + ", totalIntensityDistInStep=" + this.totalIntensityDistInStep + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
