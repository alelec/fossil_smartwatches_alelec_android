package com.portfolio.platform.data;

import com.fossil.pq7;
import com.fossil.rj4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WechatToken {
    @DexIgnore
    @rj4("access_token")
    public /* final */ String mAccessToken;
    @DexIgnore
    @rj4("expires_in")
    public /* final */ int mExpiresIn;
    @DexIgnore
    @rj4("openid")
    public /* final */ String mOpenId;
    @DexIgnore
    @rj4("refresh_token")
    public /* final */ String mRefreshToken;
    @DexIgnore
    @rj4("scope")
    public /* final */ String mScope;

    @DexIgnore
    public WechatToken(String str, int i, String str2, String str3, String str4) {
        pq7.c(str, "mAccessToken");
        pq7.c(str2, "mRefreshToken");
        pq7.c(str3, "mOpenId");
        pq7.c(str4, "mScope");
        this.mAccessToken = str;
        this.mExpiresIn = i;
        this.mRefreshToken = str2;
        this.mOpenId = str3;
        this.mScope = str4;
    }

    @DexIgnore
    public final String getAccessToken() {
        return this.mAccessToken;
    }

    @DexIgnore
    public final int getExpiresIn() {
        return this.mExpiresIn;
    }

    @DexIgnore
    public final String getOpenId() {
        return this.mOpenId;
    }

    @DexIgnore
    public final String getRefreshToken() {
        return this.mRefreshToken;
    }

    @DexIgnore
    public final String getScope() {
        return this.mScope;
    }
}
