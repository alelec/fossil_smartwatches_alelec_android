package com.portfolio.platform.data.model.room.sleep;

import com.fossil.rj4;
import com.fossil.zj5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepRecommendedGoal {
    @DexIgnore
    @zj5
    public int id;
    @DexIgnore
    @rj4("recommendedGoalMinutes")
    public int recommendedSleepGoal;

    @DexIgnore
    public SleepRecommendedGoal(int i) {
        this.recommendedSleepGoal = i;
    }

    @DexIgnore
    public static /* synthetic */ SleepRecommendedGoal copy$default(SleepRecommendedGoal sleepRecommendedGoal, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = sleepRecommendedGoal.recommendedSleepGoal;
        }
        return sleepRecommendedGoal.copy(i);
    }

    @DexIgnore
    public final int component1() {
        return this.recommendedSleepGoal;
    }

    @DexIgnore
    public final SleepRecommendedGoal copy(int i) {
        return new SleepRecommendedGoal(i);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof SleepRecommendedGoal) && this.recommendedSleepGoal == ((SleepRecommendedGoal) obj).recommendedSleepGoal);
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final int getRecommendedSleepGoal() {
        return this.recommendedSleepGoal;
    }

    @DexIgnore
    public int hashCode() {
        return this.recommendedSleepGoal;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setRecommendedSleepGoal(int i) {
        this.recommendedSleepGoal = i;
    }

    @DexIgnore
    public String toString() {
        return "SleepRecommendedGoal(recommendedSleepGoal=" + this.recommendedSleepGoal + ")";
    }
}
