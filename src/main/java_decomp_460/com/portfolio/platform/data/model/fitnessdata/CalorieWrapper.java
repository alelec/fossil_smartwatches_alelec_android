package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.pq7;
import com.fossil.qq7;
import com.fossil.rp7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CalorieWrapper {
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public float total;
    @DexIgnore
    public List<Float> values;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends qq7 implements rp7<Byte, Float> {
        @DexIgnore
        public static /* final */ Anon1 INSTANCE; // = new Anon1();

        @DexIgnore
        public Anon1() {
            super(1);
        }

        @DexIgnore
        public final float invoke(Byte b) {
            return (float) b.byteValue();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ Float invoke(Byte b) {
            return Float.valueOf(invoke(b));
        }
    }

    @DexIgnore
    public CalorieWrapper(int i, List<Float> list, float f) {
        pq7.c(list, "values");
        this.resolutionInSecond = i;
        this.values = list;
        this.total = f;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CalorieWrapper(com.fossil.fitness.Calorie r4) {
        /*
            r3 = this;
            java.lang.String r0 = "calorie"
            com.fossil.pq7.c(r4, r0)
            int r0 = r4.getResolutionInSecond()
            java.util.ArrayList r1 = r4.getValues()
            java.lang.String r2 = "calorie.values"
            com.fossil.pq7.b(r1, r2)
            com.fossil.ts7 r1 = com.fossil.pm7.z(r1)
            com.portfolio.platform.data.model.fitnessdata.CalorieWrapper$Anon1 r2 = com.portfolio.platform.data.model.fitnessdata.CalorieWrapper.Anon1.INSTANCE
            com.fossil.ts7 r1 = com.fossil.at7.o(r1, r2)
            java.util.List r1 = com.fossil.at7.u(r1)
            int r2 = r4.getTotal()
            float r2 = (float) r2
            r3.<init>(r0, r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.fitnessdata.CalorieWrapper.<init>(com.fossil.fitness.Calorie):void");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.model.fitnessdata.CalorieWrapper */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ CalorieWrapper copy$default(CalorieWrapper calorieWrapper, int i, List list, float f, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = calorieWrapper.resolutionInSecond;
        }
        if ((i2 & 2) != 0) {
            list = calorieWrapper.values;
        }
        if ((i2 & 4) != 0) {
            f = calorieWrapper.total;
        }
        return calorieWrapper.copy(i, list, f);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Float> component2() {
        return this.values;
    }

    @DexIgnore
    public final float component3() {
        return this.total;
    }

    @DexIgnore
    public final CalorieWrapper copy(int i, List<Float> list, float f) {
        pq7.c(list, "values");
        return new CalorieWrapper(i, list, f);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof CalorieWrapper) {
                CalorieWrapper calorieWrapper = (CalorieWrapper) obj;
                if (!(this.resolutionInSecond == calorieWrapper.resolutionInSecond && pq7.a(this.values, calorieWrapper.values) && Float.compare(this.total, calorieWrapper.total) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final float getTotal() {
        return this.total;
    }

    @DexIgnore
    public final List<Float> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.resolutionInSecond;
        List<Float> list = this.values;
        return (((list != null ? list.hashCode() : 0) + (i * 31)) * 31) + Float.floatToIntBits(this.total);
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setTotal(float f) {
        this.total = f;
    }

    @DexIgnore
    public final void setValues(List<Float> list) {
        pq7.c(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "CalorieWrapper(resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ", total=" + this.total + ")";
    }
}
