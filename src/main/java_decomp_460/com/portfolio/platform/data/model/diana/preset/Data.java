package com.portfolio.platform.data.model.diana.preset;

import com.fossil.pq7;
import com.fossil.rj4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Data {
    @DexIgnore
    @rj4("previewUrl")
    public String previewUrl;
    @DexIgnore
    @rj4("url")
    public String url;

    @DexIgnore
    public Data(String str, String str2) {
        this.previewUrl = str;
        this.url = str2;
    }

    @DexIgnore
    public static /* synthetic */ Data copy$default(Data data, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = data.previewUrl;
        }
        if ((i & 2) != 0) {
            str2 = data.url;
        }
        return data.copy(str, str2);
    }

    @DexIgnore
    public final String component1() {
        return this.previewUrl;
    }

    @DexIgnore
    public final String component2() {
        return this.url;
    }

    @DexIgnore
    public final Data copy(String str, String str2) {
        return new Data(str, str2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Data) {
                Data data = (Data) obj;
                if (!pq7.a(this.previewUrl, data.previewUrl) || !pq7.a(this.url, data.url)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getPreviewUrl() {
        return this.previewUrl;
    }

    @DexIgnore
    public final String getUrl() {
        return this.url;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.previewUrl;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.url;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public final void setPreviewUrl(String str) {
        this.previewUrl = str;
    }

    @DexIgnore
    public final void setUrl(String str) {
        this.url = str;
    }

    @DexIgnore
    public String toString() {
        return "Data(previewUrl=" + this.previewUrl + ", url=" + this.url + ")";
    }
}
