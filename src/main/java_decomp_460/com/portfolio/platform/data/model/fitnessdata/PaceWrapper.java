package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.fitness.Pace;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PaceWrapper {
    @DexIgnore
    public Float average;
    @DexIgnore
    public Float best;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public PaceWrapper(Pace pace) {
        this(pace.getAverage(), pace.getBest());
        pq7.c(pace, "pace");
    }

    @DexIgnore
    public PaceWrapper(Float f, Float f2) {
        this.average = f;
        this.best = f2;
    }

    @DexIgnore
    public static /* synthetic */ PaceWrapper copy$default(PaceWrapper paceWrapper, Float f, Float f2, int i, Object obj) {
        if ((i & 1) != 0) {
            f = paceWrapper.average;
        }
        if ((i & 2) != 0) {
            f2 = paceWrapper.best;
        }
        return paceWrapper.copy(f, f2);
    }

    @DexIgnore
    public final Float component1() {
        return this.average;
    }

    @DexIgnore
    public final Float component2() {
        return this.best;
    }

    @DexIgnore
    public final PaceWrapper copy(Float f, Float f2) {
        return new PaceWrapper(f, f2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof PaceWrapper) {
                PaceWrapper paceWrapper = (PaceWrapper) obj;
                if (!pq7.a(this.average, paceWrapper.average) || !pq7.a(this.best, paceWrapper.best)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Float getAverage() {
        return this.average;
    }

    @DexIgnore
    public final Float getBest() {
        return this.best;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Float f = this.average;
        int hashCode = f != null ? f.hashCode() : 0;
        Float f2 = this.best;
        if (f2 != null) {
            i = f2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public final void setAverage(Float f) {
        this.average = f;
    }

    @DexIgnore
    public final void setBest(Float f) {
        this.best = f;
    }

    @DexIgnore
    public String toString() {
        return "PaceWrapper(average=" + this.average + ", best=" + this.best + ")";
    }
}
