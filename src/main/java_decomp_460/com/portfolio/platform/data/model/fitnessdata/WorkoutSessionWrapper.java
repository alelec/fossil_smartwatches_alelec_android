package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.c;
import com.fossil.pq7;
import com.fossil.rj4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionWrapper {
    @DexIgnore
    public /* final */ CadenceWrapper cadence;
    @DexIgnore
    public CalorieWrapper calorie;
    @DexIgnore
    public DistanceWrapper distance;
    @DexIgnore
    public int duration;
    @DexIgnore
    @rj4("gpsDataPoints")
    public String encodedGpsData;
    @DexIgnore
    public DateTime endTime;
    @DexIgnore
    public /* final */ List<GpsDataPointWrapper> gpsPoints;
    @DexIgnore
    public HeartRateWrapper heartRate;
    @DexIgnore
    public long id;
    @DexIgnore
    public int mode;
    @DexIgnore
    public PaceWrapper pace;
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public List<WorkoutStateChangeWrapper> stateChanges;
    @DexIgnore
    public StepWrapper step;
    @DexIgnore
    public int timezoneOffsetInSecond;
    @DexIgnore
    public int type;

    @DexIgnore
    public WorkoutSessionWrapper(long j, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, PaceWrapper paceWrapper, CadenceWrapper cadenceWrapper, List<GpsDataPointWrapper> list, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper) {
        pq7.c(dateTime, SampleRaw.COLUMN_START_TIME);
        pq7.c(dateTime2, SampleRaw.COLUMN_END_TIME);
        pq7.c(stepWrapper, "step");
        pq7.c(calorieWrapper, "calorie");
        this.id = j;
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.timezoneOffsetInSecond = i;
        this.duration = i2;
        this.type = i3;
        this.mode = i4;
        this.pace = paceWrapper;
        this.cadence = cadenceWrapper;
        this.gpsPoints = list;
        this.step = stepWrapper;
        this.calorie = calorieWrapper;
        this.distance = distanceWrapper;
        this.heartRate = heartRateWrapper;
        this.stateChanges = new ArrayList();
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public WorkoutSessionWrapper(com.fossil.fitness.WorkoutSession r21) {
        /*
        // Method dump skipped, instructions count: 454
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.fitnessdata.WorkoutSessionWrapper.<init>(com.fossil.fitness.WorkoutSession):void");
    }

    @DexIgnore
    public static /* synthetic */ WorkoutSessionWrapper copy$default(WorkoutSessionWrapper workoutSessionWrapper, long j, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, PaceWrapper paceWrapper, CadenceWrapper cadenceWrapper, List list, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper, int i5, Object obj) {
        return workoutSessionWrapper.copy((i5 & 1) != 0 ? workoutSessionWrapper.id : j, (i5 & 2) != 0 ? workoutSessionWrapper.startTime : dateTime, (i5 & 4) != 0 ? workoutSessionWrapper.endTime : dateTime2, (i5 & 8) != 0 ? workoutSessionWrapper.timezoneOffsetInSecond : i, (i5 & 16) != 0 ? workoutSessionWrapper.duration : i2, (i5 & 32) != 0 ? workoutSessionWrapper.type : i3, (i5 & 64) != 0 ? workoutSessionWrapper.mode : i4, (i5 & 128) != 0 ? workoutSessionWrapper.pace : paceWrapper, (i5 & 256) != 0 ? workoutSessionWrapper.cadence : cadenceWrapper, (i5 & 512) != 0 ? workoutSessionWrapper.gpsPoints : list, (i5 & 1024) != 0 ? workoutSessionWrapper.step : stepWrapper, (i5 & 2048) != 0 ? workoutSessionWrapper.calorie : calorieWrapper, (i5 & 4096) != 0 ? workoutSessionWrapper.distance : distanceWrapper, (i5 & 8192) != 0 ? workoutSessionWrapper.heartRate : heartRateWrapper);
    }

    @DexIgnore
    public final long component1() {
        return this.id;
    }

    @DexIgnore
    public final List<GpsDataPointWrapper> component10() {
        return this.gpsPoints;
    }

    @DexIgnore
    public final StepWrapper component11() {
        return this.step;
    }

    @DexIgnore
    public final CalorieWrapper component12() {
        return this.calorie;
    }

    @DexIgnore
    public final DistanceWrapper component13() {
        return this.distance;
    }

    @DexIgnore
    public final HeartRateWrapper component14() {
        return this.heartRate;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.endTime;
    }

    @DexIgnore
    public final int component4() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int component5() {
        return this.duration;
    }

    @DexIgnore
    public final int component6() {
        return this.type;
    }

    @DexIgnore
    public final int component7() {
        return this.mode;
    }

    @DexIgnore
    public final PaceWrapper component8() {
        return this.pace;
    }

    @DexIgnore
    public final CadenceWrapper component9() {
        return this.cadence;
    }

    @DexIgnore
    public final WorkoutSessionWrapper copy(long j, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, PaceWrapper paceWrapper, CadenceWrapper cadenceWrapper, List<GpsDataPointWrapper> list, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper) {
        pq7.c(dateTime, SampleRaw.COLUMN_START_TIME);
        pq7.c(dateTime2, SampleRaw.COLUMN_END_TIME);
        pq7.c(stepWrapper, "step");
        pq7.c(calorieWrapper, "calorie");
        return new WorkoutSessionWrapper(j, dateTime, dateTime2, i, i2, i3, i4, paceWrapper, cadenceWrapper, list, stepWrapper, calorieWrapper, distanceWrapper, heartRateWrapper);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutSessionWrapper) {
                WorkoutSessionWrapper workoutSessionWrapper = (WorkoutSessionWrapper) obj;
                if (this.id != workoutSessionWrapper.id || !pq7.a(this.startTime, workoutSessionWrapper.startTime) || !pq7.a(this.endTime, workoutSessionWrapper.endTime) || this.timezoneOffsetInSecond != workoutSessionWrapper.timezoneOffsetInSecond || this.duration != workoutSessionWrapper.duration || this.type != workoutSessionWrapper.type || this.mode != workoutSessionWrapper.mode || !pq7.a(this.pace, workoutSessionWrapper.pace) || !pq7.a(this.cadence, workoutSessionWrapper.cadence) || !pq7.a(this.gpsPoints, workoutSessionWrapper.gpsPoints) || !pq7.a(this.step, workoutSessionWrapper.step) || !pq7.a(this.calorie, workoutSessionWrapper.calorie) || !pq7.a(this.distance, workoutSessionWrapper.distance) || !pq7.a(this.heartRate, workoutSessionWrapper.heartRate)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final CadenceWrapper getCadence() {
        return this.cadence;
    }

    @DexIgnore
    public final CalorieWrapper getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public final DistanceWrapper getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final int getDuration() {
        return this.duration;
    }

    @DexIgnore
    public final String getEncodedGpsData() {
        return this.encodedGpsData;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final List<GpsDataPointWrapper> getGpsPoints() {
        return this.gpsPoints;
    }

    @DexIgnore
    public final HeartRateWrapper getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final long getId() {
        return this.id;
    }

    @DexIgnore
    public final int getMode() {
        return this.mode;
    }

    @DexIgnore
    public final PaceWrapper getPace() {
        return this.pace;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final List<WorkoutStateChangeWrapper> getStateChanges() {
        return this.stateChanges;
    }

    @DexIgnore
    public final StepWrapper getStep() {
        return this.step;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int a2 = c.a(this.id);
        DateTime dateTime = this.startTime;
        int hashCode = dateTime != null ? dateTime.hashCode() : 0;
        DateTime dateTime2 = this.endTime;
        int hashCode2 = dateTime2 != null ? dateTime2.hashCode() : 0;
        int i2 = this.timezoneOffsetInSecond;
        int i3 = this.duration;
        int i4 = this.type;
        int i5 = this.mode;
        PaceWrapper paceWrapper = this.pace;
        int hashCode3 = paceWrapper != null ? paceWrapper.hashCode() : 0;
        CadenceWrapper cadenceWrapper = this.cadence;
        int hashCode4 = cadenceWrapper != null ? cadenceWrapper.hashCode() : 0;
        List<GpsDataPointWrapper> list = this.gpsPoints;
        int hashCode5 = list != null ? list.hashCode() : 0;
        StepWrapper stepWrapper = this.step;
        int hashCode6 = stepWrapper != null ? stepWrapper.hashCode() : 0;
        CalorieWrapper calorieWrapper = this.calorie;
        int hashCode7 = calorieWrapper != null ? calorieWrapper.hashCode() : 0;
        DistanceWrapper distanceWrapper = this.distance;
        int hashCode8 = distanceWrapper != null ? distanceWrapper.hashCode() : 0;
        HeartRateWrapper heartRateWrapper = this.heartRate;
        if (heartRateWrapper != null) {
            i = heartRateWrapper.hashCode();
        }
        return ((((((((((((((((((((((((hashCode + (a2 * 31)) * 31) + hashCode2) * 31) + i2) * 31) + i3) * 31) + i4) * 31) + i5) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + i;
    }

    @DexIgnore
    public final void setCalorie(CalorieWrapper calorieWrapper) {
        pq7.c(calorieWrapper, "<set-?>");
        this.calorie = calorieWrapper;
    }

    @DexIgnore
    public final void setDistance(DistanceWrapper distanceWrapper) {
        this.distance = distanceWrapper;
    }

    @DexIgnore
    public final void setDuration(int i) {
        this.duration = i;
    }

    @DexIgnore
    public final void setEncodedGpsData(String str) {
        this.encodedGpsData = str;
    }

    @DexIgnore
    public final void setEndTime(DateTime dateTime) {
        pq7.c(dateTime, "<set-?>");
        this.endTime = dateTime;
    }

    @DexIgnore
    public final void setHeartRate(HeartRateWrapper heartRateWrapper) {
        this.heartRate = heartRateWrapper;
    }

    @DexIgnore
    public final void setId(long j) {
        this.id = j;
    }

    @DexIgnore
    public final void setMode(int i) {
        this.mode = i;
    }

    @DexIgnore
    public final void setPace(PaceWrapper paceWrapper) {
        this.pace = paceWrapper;
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        pq7.c(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setStateChanges(List<WorkoutStateChangeWrapper> list) {
        pq7.c(list, "<set-?>");
        this.stateChanges = list;
    }

    @DexIgnore
    public final void setStep(StepWrapper stepWrapper) {
        pq7.c(stepWrapper, "<set-?>");
        this.step = stepWrapper;
    }

    @DexIgnore
    public final void setTimezoneOffsetInSecond(int i) {
        this.timezoneOffsetInSecond = i;
    }

    @DexIgnore
    public final void setType(int i) {
        this.type = i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSessionWrapper(id=" + this.id + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", duration=" + this.duration + ", type=" + this.type + ", mode=" + this.mode + ", pace=" + this.pace + ", cadence=" + this.cadence + ", gpsPoints=" + this.gpsPoints + ", step=" + this.step + ", calorie=" + this.calorie + ", distance=" + this.distance + ", heartRate=" + this.heartRate + ")";
    }
}
