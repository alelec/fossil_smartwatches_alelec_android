package com.portfolio.platform.data.model.diana.preset;

import com.fossil.pq7;
import com.fossil.rj4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaComplicationRingStyle {
    @DexIgnore
    @rj4("category")
    public String category;
    @DexIgnore
    @rj4("data")
    public Data data;
    @DexIgnore
    @rj4("id")
    public String id;
    @DexIgnore
    @rj4("metadata")
    public MetaData metaData;
    @DexIgnore
    @rj4("name")
    public String name;
    @DexIgnore
    public String serial;

    @DexIgnore
    public DianaComplicationRingStyle(String str, String str2, String str3, Data data2, MetaData metaData2, String str4) {
        pq7.c(str, "id");
        pq7.c(str2, "category");
        pq7.c(str3, "name");
        pq7.c(data2, "data");
        pq7.c(metaData2, "metaData");
        pq7.c(str4, "serial");
        this.id = str;
        this.category = str2;
        this.name = str3;
        this.data = data2;
        this.metaData = metaData2;
        this.serial = str4;
    }

    @DexIgnore
    public static /* synthetic */ DianaComplicationRingStyle copy$default(DianaComplicationRingStyle dianaComplicationRingStyle, String str, String str2, String str3, Data data2, MetaData metaData2, String str4, int i, Object obj) {
        return dianaComplicationRingStyle.copy((i & 1) != 0 ? dianaComplicationRingStyle.id : str, (i & 2) != 0 ? dianaComplicationRingStyle.category : str2, (i & 4) != 0 ? dianaComplicationRingStyle.name : str3, (i & 8) != 0 ? dianaComplicationRingStyle.data : data2, (i & 16) != 0 ? dianaComplicationRingStyle.metaData : metaData2, (i & 32) != 0 ? dianaComplicationRingStyle.serial : str4);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.category;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final Data component4() {
        return this.data;
    }

    @DexIgnore
    public final MetaData component5() {
        return this.metaData;
    }

    @DexIgnore
    public final String component6() {
        return this.serial;
    }

    @DexIgnore
    public final DianaComplicationRingStyle copy(String str, String str2, String str3, Data data2, MetaData metaData2, String str4) {
        pq7.c(str, "id");
        pq7.c(str2, "category");
        pq7.c(str3, "name");
        pq7.c(data2, "data");
        pq7.c(metaData2, "metaData");
        pq7.c(str4, "serial");
        return new DianaComplicationRingStyle(str, str2, str3, data2, metaData2, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DianaComplicationRingStyle) {
                DianaComplicationRingStyle dianaComplicationRingStyle = (DianaComplicationRingStyle) obj;
                if (!pq7.a(this.id, dianaComplicationRingStyle.id) || !pq7.a(this.category, dianaComplicationRingStyle.category) || !pq7.a(this.name, dianaComplicationRingStyle.name) || !pq7.a(this.data, dianaComplicationRingStyle.data) || !pq7.a(this.metaData, dianaComplicationRingStyle.metaData) || !pq7.a(this.serial, dianaComplicationRingStyle.serial)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getCategory() {
        return this.category;
    }

    @DexIgnore
    public final Data getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final MetaData getMetaData() {
        return this.metaData;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.category;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.name;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        Data data2 = this.data;
        int hashCode4 = data2 != null ? data2.hashCode() : 0;
        MetaData metaData2 = this.metaData;
        int hashCode5 = metaData2 != null ? metaData2.hashCode() : 0;
        String str4 = this.serial;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return (((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + i;
    }

    @DexIgnore
    public final void setCategory(String str) {
        pq7.c(str, "<set-?>");
        this.category = str;
    }

    @DexIgnore
    public final void setData(Data data2) {
        pq7.c(data2, "<set-?>");
        this.data = data2;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMetaData(MetaData metaData2) {
        pq7.c(metaData2, "<set-?>");
        this.metaData = metaData2;
    }

    @DexIgnore
    public final void setName(String str) {
        pq7.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setSerial(String str) {
        pq7.c(str, "<set-?>");
        this.serial = str;
    }

    @DexIgnore
    public String toString() {
        return "DianaComplicationRingStyle(id=" + this.id + ", category=" + this.category + ", name=" + this.name + ", data=" + this.data + ", metaData=" + this.metaData + ", serial=" + this.serial + ")";
    }
}
