package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.pq7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DistanceWrapper {
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public double total;
    @DexIgnore
    public List<Double> values;

    @DexIgnore
    public DistanceWrapper(int i, List<Double> list, double d) {
        pq7.c(list, "values");
        this.resolutionInSecond = i;
        this.values = list;
        this.total = d;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.model.fitnessdata.DistanceWrapper */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ DistanceWrapper copy$default(DistanceWrapper distanceWrapper, int i, List list, double d, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = distanceWrapper.resolutionInSecond;
        }
        if ((i2 & 2) != 0) {
            list = distanceWrapper.values;
        }
        if ((i2 & 4) != 0) {
            d = distanceWrapper.total;
        }
        return distanceWrapper.copy(i, list, d);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Double> component2() {
        return this.values;
    }

    @DexIgnore
    public final double component3() {
        return this.total;
    }

    @DexIgnore
    public final DistanceWrapper copy(int i, List<Double> list, double d) {
        pq7.c(list, "values");
        return new DistanceWrapper(i, list, d);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DistanceWrapper) {
                DistanceWrapper distanceWrapper = (DistanceWrapper) obj;
                if (!(this.resolutionInSecond == distanceWrapper.resolutionInSecond && pq7.a(this.values, distanceWrapper.values) && Double.compare(this.total, distanceWrapper.total) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final double getTotal() {
        return this.total;
    }

    @DexIgnore
    public final List<Double> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.resolutionInSecond;
        List<Double> list = this.values;
        return (((list != null ? list.hashCode() : 0) + (i * 31)) * 31) + Double.doubleToLongBits(this.total);
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setTotal(double d) {
        this.total = d;
    }

    @DexIgnore
    public final void setValues(List<Double> list) {
        pq7.c(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "DistanceWrapper(resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ", total=" + this.total + ")";
    }
}
