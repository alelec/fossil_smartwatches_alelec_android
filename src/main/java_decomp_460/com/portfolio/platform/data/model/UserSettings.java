package com.portfolio.platform.data.model;

import com.fossil.pq7;
import com.fossil.rj4;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserSettings {
    @DexIgnore
    @rj4("acceptedLocationDataSharing")
    public ArrayList<String> acceptedLocationDataSharing; // = new ArrayList<>();
    @DexIgnore
    @rj4("acceptedPrivacies")
    public ArrayList<String> acceptedPrivacies; // = new ArrayList<>();
    @DexIgnore
    @rj4("acceptedTermsOfService")
    public ArrayList<String> acceptedTermsOfService; // = new ArrayList<>();
    @DexIgnore
    @rj4("createdAt")
    public String createdAt;
    @DexIgnore
    @rj4("id")
    public String id;
    @DexIgnore
    @rj4("isLatestLocationDataSharingAccepted")
    public boolean isLatestLocationDataSharingAccepted;
    @DexIgnore
    @rj4("isLatestPrivacyAccepted")
    public boolean isLatestPrivacyAccepted;
    @DexIgnore
    @rj4("isLatestTermsOfServiceAccepted")
    public boolean isLatestTermsOfServiceAccepted;
    @DexIgnore
    @rj4("isShowGoalRing")
    public boolean isShowGoalRing; // = true;
    @DexIgnore
    @rj4("latestLocationDataSharingVersion")
    public String latestLocationDataSharingVersion;
    @DexIgnore
    @rj4("latestPrivacyVersion")
    public String latestPrivacyVersion;
    @DexIgnore
    @rj4("latestTermsOfServiceVersion")
    public String latestTermsOfServiceVersion;
    @DexIgnore
    public int pinType; // = 1;
    @DexIgnore
    @rj4("startDayOfWeek")
    public String startDayOfWeek;
    @DexIgnore
    @rj4("uid")
    public String uid; // = "";
    @DexIgnore
    @rj4("updatedAt")
    public String updatedAt;

    @DexIgnore
    public final ArrayList<String> getAcceptedLocationDataSharing() {
        return this.acceptedLocationDataSharing;
    }

    @DexIgnore
    public final ArrayList<String> getAcceptedPrivacies() {
        return this.acceptedPrivacies;
    }

    @DexIgnore
    public final ArrayList<String> getAcceptedTermsOfService() {
        return this.acceptedTermsOfService;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getLatestLocationDataSharingVersion() {
        return this.latestLocationDataSharingVersion;
    }

    @DexIgnore
    public final String getLatestPrivacyVersion() {
        return this.latestPrivacyVersion;
    }

    @DexIgnore
    public final String getLatestTermsOfServiceVersion() {
        return this.latestTermsOfServiceVersion;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getStartDayOfWeek() {
        return this.startDayOfWeek;
    }

    @DexIgnore
    public final String getUid() {
        return this.uid;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final boolean isLatestLocationDataSharingAccepted() {
        return this.isLatestLocationDataSharingAccepted;
    }

    @DexIgnore
    public final boolean isLatestPrivacyAccepted() {
        return this.isLatestPrivacyAccepted;
    }

    @DexIgnore
    public final boolean isLatestTermsOfServiceAccepted() {
        return this.isLatestTermsOfServiceAccepted;
    }

    @DexIgnore
    public final boolean isShowGoalRing() {
        return this.isShowGoalRing;
    }

    @DexIgnore
    public final void setAcceptedLocationDataSharing(ArrayList<String> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.acceptedLocationDataSharing = arrayList;
    }

    @DexIgnore
    public final void setAcceptedPrivacies(ArrayList<String> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.acceptedPrivacies = arrayList;
    }

    @DexIgnore
    public final void setAcceptedTermsOfService(ArrayList<String> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.acceptedTermsOfService = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public final void setLatestLocationDataSharingAccepted(boolean z) {
        this.isLatestLocationDataSharingAccepted = z;
    }

    @DexIgnore
    public final void setLatestLocationDataSharingVersion(String str) {
        this.latestLocationDataSharingVersion = str;
    }

    @DexIgnore
    public final void setLatestPrivacyAccepted(boolean z) {
        this.isLatestPrivacyAccepted = z;
    }

    @DexIgnore
    public final void setLatestPrivacyVersion(String str) {
        this.latestPrivacyVersion = str;
    }

    @DexIgnore
    public final void setLatestTermsOfServiceAccepted(boolean z) {
        this.isLatestTermsOfServiceAccepted = z;
    }

    @DexIgnore
    public final void setLatestTermsOfServiceVersion(String str) {
        this.latestTermsOfServiceVersion = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setShowGoalRing(boolean z) {
        this.isShowGoalRing = z;
    }

    @DexIgnore
    public final void setStartDayOfWeek(String str) {
        this.startDayOfWeek = str;
    }

    @DexIgnore
    public final void setUid(String str) {
        pq7.c(str, "<set-?>");
        this.uid = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        this.updatedAt = str;
    }
}
