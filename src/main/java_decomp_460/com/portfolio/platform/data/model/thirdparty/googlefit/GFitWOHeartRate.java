package com.portfolio.platform.data.model.thirdparty.googlefit;

import com.fossil.c;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitWOHeartRate {
    @DexIgnore
    public long endTime;
    @DexIgnore
    public float heartRate;
    @DexIgnore
    public long startTime;

    @DexIgnore
    public GFitWOHeartRate(float f, long j, long j2) {
        this.heartRate = f;
        this.startTime = j;
        this.endTime = j2;
    }

    @DexIgnore
    public static /* synthetic */ GFitWOHeartRate copy$default(GFitWOHeartRate gFitWOHeartRate, float f, long j, long j2, int i, Object obj) {
        return gFitWOHeartRate.copy((i & 1) != 0 ? gFitWOHeartRate.heartRate : f, (i & 2) != 0 ? gFitWOHeartRate.startTime : j, (i & 4) != 0 ? gFitWOHeartRate.endTime : j2);
    }

    @DexIgnore
    public final float component1() {
        return this.heartRate;
    }

    @DexIgnore
    public final long component2() {
        return this.startTime;
    }

    @DexIgnore
    public final long component3() {
        return this.endTime;
    }

    @DexIgnore
    public final GFitWOHeartRate copy(float f, long j, long j2) {
        return new GFitWOHeartRate(f, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof GFitWOHeartRate) {
                GFitWOHeartRate gFitWOHeartRate = (GFitWOHeartRate) obj;
                if (!(Float.compare(this.heartRate, gFitWOHeartRate.heartRate) == 0 && this.startTime == gFitWOHeartRate.startTime && this.endTime == gFitWOHeartRate.endTime)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final float getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public int hashCode() {
        return (((Float.floatToIntBits(this.heartRate) * 31) + c.a(this.startTime)) * 31) + c.a(this.endTime);
    }

    @DexIgnore
    public final void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public final void setHeartRate(float f) {
        this.heartRate = f;
    }

    @DexIgnore
    public final void setStartTime(long j) {
        this.startTime = j;
    }

    @DexIgnore
    public String toString() {
        return "GFitWOHeartRate(heartRate=" + this.heartRate + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ")";
    }
}
