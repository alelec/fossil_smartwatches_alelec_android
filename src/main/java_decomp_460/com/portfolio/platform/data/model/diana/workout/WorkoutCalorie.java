package com.portfolio.platform.data.model.diana.workout;

import com.fossil.pq7;
import com.portfolio.platform.data.model.fitnessdata.CalorieWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutCalorie {
    @DexIgnore
    public int resolution;
    @DexIgnore
    public float total;
    @DexIgnore
    public List<Float> values;

    @DexIgnore
    public WorkoutCalorie(int i, List<Float> list, float f) {
        pq7.c(list, "values");
        this.resolution = i;
        this.values = list;
        this.total = f;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutCalorie(CalorieWrapper calorieWrapper) {
        this(calorieWrapper.getResolutionInSecond(), calorieWrapper.getValues(), calorieWrapper.getTotal());
        pq7.c(calorieWrapper, "calorie");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.model.diana.workout.WorkoutCalorie */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ WorkoutCalorie copy$default(WorkoutCalorie workoutCalorie, int i, List list, float f, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = workoutCalorie.resolution;
        }
        if ((i2 & 2) != 0) {
            list = workoutCalorie.values;
        }
        if ((i2 & 4) != 0) {
            f = workoutCalorie.total;
        }
        return workoutCalorie.copy(i, list, f);
    }

    @DexIgnore
    public final int component1() {
        return this.resolution;
    }

    @DexIgnore
    public final List<Float> component2() {
        return this.values;
    }

    @DexIgnore
    public final float component3() {
        return this.total;
    }

    @DexIgnore
    public final WorkoutCalorie copy(int i, List<Float> list, float f) {
        pq7.c(list, "values");
        return new WorkoutCalorie(i, list, f);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutCalorie) {
                WorkoutCalorie workoutCalorie = (WorkoutCalorie) obj;
                if (!(this.resolution == workoutCalorie.resolution && pq7.a(this.values, workoutCalorie.values) && Float.compare(this.total, workoutCalorie.total) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getResolution() {
        return this.resolution;
    }

    @DexIgnore
    public final float getTotal() {
        return this.total;
    }

    @DexIgnore
    public final List<Float> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.resolution;
        List<Float> list = this.values;
        return (((list != null ? list.hashCode() : 0) + (i * 31)) * 31) + Float.floatToIntBits(this.total);
    }

    @DexIgnore
    public final void setResolution(int i) {
        this.resolution = i;
    }

    @DexIgnore
    public final void setTotal(float f) {
        this.total = f;
    }

    @DexIgnore
    public final void setValues(List<Float> list) {
        pq7.c(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutCalorie(resolution=" + this.resolution + ", values=" + this.values + ", total=" + this.total + ")";
    }
}
