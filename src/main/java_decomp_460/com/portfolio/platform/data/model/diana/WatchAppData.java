package com.portfolio.platform.data.model.diana;

import com.fossil.pj4;
import com.fossil.pq7;
import com.fossil.rj4;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppData {
    @DexIgnore
    @pj4
    @rj4("createdAt")
    public DateTime createdAt;
    @DexIgnore
    @pj4
    @rj4("executableBinaryDataUrl")
    public String executableBinaryDataUrl;
    @DexIgnore
    @pj4
    @rj4("id")
    public String id;
    @DexIgnore
    @pj4
    @rj4("maxAppVersion")
    public String maxAppVersion;
    @DexIgnore
    @pj4
    @rj4("maxFirmwareOSVersion")
    public String maxFirmwareOSVersion;
    @DexIgnore
    @pj4
    @rj4("minAppVersion")
    public String minAppVersion;
    @DexIgnore
    @pj4
    @rj4("minFirmwareOSVersion")
    public String minFirmwareOSVersion;
    @DexIgnore
    @pj4
    @rj4("type")
    public String type;
    @DexIgnore
    @pj4
    @rj4("updatedAt")
    public DateTime updatedAt;
    @DexIgnore
    @pj4
    @rj4("version")
    public String version;

    @DexIgnore
    public WatchAppData(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, DateTime dateTime, DateTime dateTime2) {
        pq7.c(str, "id");
        pq7.c(str2, "type");
        pq7.c(str3, "maxAppVersion");
        pq7.c(str4, "maxFirmwareOSVersion");
        pq7.c(str5, "minAppVersion");
        pq7.c(str6, "minFirmwareOSVersion");
        pq7.c(str7, "version");
        pq7.c(str8, "executableBinaryDataUrl");
        pq7.c(dateTime, "updatedAt");
        pq7.c(dateTime2, "createdAt");
        this.id = str;
        this.type = str2;
        this.maxAppVersion = str3;
        this.maxFirmwareOSVersion = str4;
        this.minAppVersion = str5;
        this.minFirmwareOSVersion = str6;
        this.version = str7;
        this.executableBinaryDataUrl = str8;
        this.updatedAt = dateTime;
        this.createdAt = dateTime2;
    }

    @DexIgnore
    public static /* synthetic */ WatchAppData copy$default(WatchAppData watchAppData, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, DateTime dateTime, DateTime dateTime2, int i, Object obj) {
        return watchAppData.copy((i & 1) != 0 ? watchAppData.id : str, (i & 2) != 0 ? watchAppData.type : str2, (i & 4) != 0 ? watchAppData.maxAppVersion : str3, (i & 8) != 0 ? watchAppData.maxFirmwareOSVersion : str4, (i & 16) != 0 ? watchAppData.minAppVersion : str5, (i & 32) != 0 ? watchAppData.minFirmwareOSVersion : str6, (i & 64) != 0 ? watchAppData.version : str7, (i & 128) != 0 ? watchAppData.executableBinaryDataUrl : str8, (i & 256) != 0 ? watchAppData.updatedAt : dateTime, (i & 512) != 0 ? watchAppData.createdAt : dateTime2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final DateTime component10() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component2() {
        return this.type;
    }

    @DexIgnore
    public final String component3() {
        return this.maxAppVersion;
    }

    @DexIgnore
    public final String component4() {
        return this.maxFirmwareOSVersion;
    }

    @DexIgnore
    public final String component5() {
        return this.minAppVersion;
    }

    @DexIgnore
    public final String component6() {
        return this.minFirmwareOSVersion;
    }

    @DexIgnore
    public final String component7() {
        return this.version;
    }

    @DexIgnore
    public final String component8() {
        return this.executableBinaryDataUrl;
    }

    @DexIgnore
    public final DateTime component9() {
        return this.updatedAt;
    }

    @DexIgnore
    public final WatchAppData copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, DateTime dateTime, DateTime dateTime2) {
        pq7.c(str, "id");
        pq7.c(str2, "type");
        pq7.c(str3, "maxAppVersion");
        pq7.c(str4, "maxFirmwareOSVersion");
        pq7.c(str5, "minAppVersion");
        pq7.c(str6, "minFirmwareOSVersion");
        pq7.c(str7, "version");
        pq7.c(str8, "executableBinaryDataUrl");
        pq7.c(dateTime, "updatedAt");
        pq7.c(dateTime2, "createdAt");
        return new WatchAppData(str, str2, str3, str4, str5, str6, str7, str8, dateTime, dateTime2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WatchAppData) {
                WatchAppData watchAppData = (WatchAppData) obj;
                if (!pq7.a(this.id, watchAppData.id) || !pq7.a(this.type, watchAppData.type) || !pq7.a(this.maxAppVersion, watchAppData.maxAppVersion) || !pq7.a(this.maxFirmwareOSVersion, watchAppData.maxFirmwareOSVersion) || !pq7.a(this.minAppVersion, watchAppData.minAppVersion) || !pq7.a(this.minFirmwareOSVersion, watchAppData.minFirmwareOSVersion) || !pq7.a(this.version, watchAppData.version) || !pq7.a(this.executableBinaryDataUrl, watchAppData.executableBinaryDataUrl) || !pq7.a(this.updatedAt, watchAppData.updatedAt) || !pq7.a(this.createdAt, watchAppData.createdAt)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getExecutableBinaryDataUrl() {
        return this.executableBinaryDataUrl;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getMaxAppVersion() {
        return this.maxAppVersion;
    }

    @DexIgnore
    public final String getMaxFirmwareOSVersion() {
        return this.maxFirmwareOSVersion;
    }

    @DexIgnore
    public final String getMinAppVersion() {
        return this.minAppVersion;
    }

    @DexIgnore
    public final String getMinFirmwareOSVersion() {
        return this.minFirmwareOSVersion;
    }

    @DexIgnore
    public final String getType() {
        return this.type;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getVersion() {
        return this.version;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.type;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.maxAppVersion;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.maxFirmwareOSVersion;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.minAppVersion;
        int hashCode5 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.minFirmwareOSVersion;
        int hashCode6 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.version;
        int hashCode7 = str7 != null ? str7.hashCode() : 0;
        String str8 = this.executableBinaryDataUrl;
        int hashCode8 = str8 != null ? str8.hashCode() : 0;
        DateTime dateTime = this.updatedAt;
        int hashCode9 = dateTime != null ? dateTime.hashCode() : 0;
        DateTime dateTime2 = this.createdAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return (((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + i;
    }

    @DexIgnore
    public final void setCreatedAt(DateTime dateTime) {
        pq7.c(dateTime, "<set-?>");
        this.createdAt = dateTime;
    }

    @DexIgnore
    public final void setExecutableBinaryDataUrl(String str) {
        pq7.c(str, "<set-?>");
        this.executableBinaryDataUrl = str;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMaxAppVersion(String str) {
        pq7.c(str, "<set-?>");
        this.maxAppVersion = str;
    }

    @DexIgnore
    public final void setMaxFirmwareOSVersion(String str) {
        pq7.c(str, "<set-?>");
        this.maxFirmwareOSVersion = str;
    }

    @DexIgnore
    public final void setMinAppVersion(String str) {
        pq7.c(str, "<set-?>");
        this.minAppVersion = str;
    }

    @DexIgnore
    public final void setMinFirmwareOSVersion(String str) {
        pq7.c(str, "<set-?>");
        this.minFirmwareOSVersion = str;
    }

    @DexIgnore
    public final void setType(String str) {
        pq7.c(str, "<set-?>");
        this.type = str;
    }

    @DexIgnore
    public final void setUpdatedAt(DateTime dateTime) {
        pq7.c(dateTime, "<set-?>");
        this.updatedAt = dateTime;
    }

    @DexIgnore
    public final void setVersion(String str) {
        pq7.c(str, "<set-?>");
        this.version = str;
    }

    @DexIgnore
    public String toString() {
        return "WatchAppData(id=" + this.id + ", type=" + this.type + ", maxAppVersion=" + this.maxAppVersion + ", maxFirmwareOSVersion=" + this.maxFirmwareOSVersion + ", minAppVersion=" + this.minAppVersion + ", minFirmwareOSVersion=" + this.minFirmwareOSVersion + ", version=" + this.version + ", executableBinaryDataUrl=" + this.executableBinaryDataUrl + ", updatedAt=" + this.updatedAt + ", createdAt=" + this.createdAt + ")";
    }
}
