package com.portfolio.platform.data.model.thirdparty.googlefit;

import com.fossil.c;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitSleep {
    @DexIgnore
    public long endTime;
    @DexIgnore
    public int id;
    @DexIgnore
    public int sleepMins;
    @DexIgnore
    public long startTime;

    @DexIgnore
    public GFitSleep(int i, long j, long j2) {
        this.sleepMins = i;
        this.startTime = j;
        this.endTime = j2;
    }

    @DexIgnore
    public static /* synthetic */ GFitSleep copy$default(GFitSleep gFitSleep, int i, long j, long j2, int i2, Object obj) {
        return gFitSleep.copy((i2 & 1) != 0 ? gFitSleep.sleepMins : i, (i2 & 2) != 0 ? gFitSleep.startTime : j, (i2 & 4) != 0 ? gFitSleep.endTime : j2);
    }

    @DexIgnore
    public final int component1() {
        return this.sleepMins;
    }

    @DexIgnore
    public final long component2() {
        return this.startTime;
    }

    @DexIgnore
    public final long component3() {
        return this.endTime;
    }

    @DexIgnore
    public final GFitSleep copy(int i, long j, long j2) {
        return new GFitSleep(i, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof GFitSleep) {
                GFitSleep gFitSleep = (GFitSleep) obj;
                if (!(this.sleepMins == gFitSleep.sleepMins && this.startTime == gFitSleep.startTime && this.endTime == gFitSleep.endTime)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final int getSleepMins() {
        return this.sleepMins;
    }

    @DexIgnore
    public final long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.sleepMins * 31) + c.a(this.startTime)) * 31) + c.a(this.endTime);
    }

    @DexIgnore
    public final void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setSleepMins(int i) {
        this.sleepMins = i;
    }

    @DexIgnore
    public final void setStartTime(long j) {
        this.startTime = j;
    }

    @DexIgnore
    public String toString() {
        return "GFitSleep(sleepMins=" + this.sleepMins + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ")";
    }
}
