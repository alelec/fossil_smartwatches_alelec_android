package com.portfolio.platform.data.model.diana.workout;

import com.fossil.pq7;
import com.fossil.wearables.fsl.sleep.MFSleepSession;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionUpdateWrapper {
    @DexIgnore
    public /* final */ String editedEndTime;
    @DexIgnore
    public /* final */ String editedMode;
    @DexIgnore
    public /* final */ String editedStartTime;
    @DexIgnore
    public /* final */ String editedType;
    @DexIgnore
    public /* final */ String id;

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0053, code lost:
        if (r5 != null) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x003a, code lost:
        if (r4 != null) goto L_0x003c;
     */
    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public WorkoutSessionUpdateWrapper(com.portfolio.platform.data.model.diana.workout.WorkoutSession r7) {
        /*
            r6 = this;
            java.lang.String r0 = "workoutSession"
            com.fossil.pq7.c(r7, r0)
            java.lang.String r1 = r7.getId()
            org.joda.time.DateTime r0 = r7.getEditedEndTime()
            java.lang.String r2 = com.fossil.lk5.t0(r0)
            java.lang.String r0 = "DateHelper.printServerDa\u2026outSession.editedEndTime)"
            com.fossil.pq7.b(r2, r0)
            org.joda.time.DateTime r0 = r7.getEditedStartTime()
            java.lang.String r3 = com.fossil.lk5.t0(r0)
            java.lang.String r0 = "DateHelper.printServerDa\u2026tSession.editedStartTime)"
            com.fossil.pq7.b(r3, r0)
            com.fossil.mi5 r0 = r7.getEditedType()
            if (r0 == 0) goto L_0x0062
            java.lang.String r0 = r0.getMValue()
            if (r0 == 0) goto L_0x0062
            if (r0 == 0) goto L_0x005a
            java.lang.String r4 = r0.toLowerCase()
            java.lang.String r0 = "(this as java.lang.String).toLowerCase()"
            com.fossil.pq7.b(r4, r0)
            if (r4 == 0) goto L_0x0062
        L_0x003c:
            com.fossil.gi5 r0 = r7.getEditedMode()
            if (r0 == 0) goto L_0x007c
            java.lang.String r0 = r0.getMValue()
            if (r0 == 0) goto L_0x007c
            if (r0 == 0) goto L_0x0074
            java.lang.String r5 = r0.toLowerCase()
            java.lang.String r0 = "(this as java.lang.String).toLowerCase()"
            com.fossil.pq7.b(r5, r0)
            if (r5 == 0) goto L_0x007c
        L_0x0055:
            r0 = r6
            r0.<init>(r1, r2, r3, r4, r5)
            return
        L_0x005a:
            com.fossil.il7 r0 = new com.fossil.il7
            java.lang.String r1 = "null cannot be cast to non-null type java.lang.String"
            r0.<init>(r1)
            throw r0
        L_0x0062:
            com.fossil.mi5 r0 = com.fossil.mi5.UNKNOWN
            java.lang.String r0 = r0.getMValue()
            if (r0 == 0) goto L_0x0096
            java.lang.String r4 = r0.toLowerCase()
            java.lang.String r0 = "(this as java.lang.String).toLowerCase()"
            com.fossil.pq7.b(r4, r0)
            goto L_0x003c
        L_0x0074:
            com.fossil.il7 r0 = new com.fossil.il7
            java.lang.String r1 = "null cannot be cast to non-null type java.lang.String"
            r0.<init>(r1)
            throw r0
        L_0x007c:
            com.fossil.gi5 r0 = com.fossil.gi5.INDOOR
            java.lang.String r0 = r0.getMValue()
            if (r0 == 0) goto L_0x008e
            java.lang.String r5 = r0.toLowerCase()
            java.lang.String r0 = "(this as java.lang.String).toLowerCase()"
            com.fossil.pq7.b(r5, r0)
            goto L_0x0055
        L_0x008e:
            com.fossil.il7 r0 = new com.fossil.il7
            java.lang.String r1 = "null cannot be cast to non-null type java.lang.String"
            r0.<init>(r1)
            throw r0
        L_0x0096:
            com.fossil.il7 r0 = new com.fossil.il7
            java.lang.String r1 = "null cannot be cast to non-null type java.lang.String"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.diana.workout.WorkoutSessionUpdateWrapper.<init>(com.portfolio.platform.data.model.diana.workout.WorkoutSession):void");
    }

    @DexIgnore
    public WorkoutSessionUpdateWrapper(String str, String str2, String str3, String str4, String str5) {
        pq7.c(str, "id");
        pq7.c(str2, MFSleepSession.COLUMN_EDITED_END_TIME);
        pq7.c(str3, MFSleepSession.COLUMN_EDITED_START_TIME);
        pq7.c(str4, "editedType");
        this.id = str;
        this.editedEndTime = str2;
        this.editedStartTime = str3;
        this.editedType = str4;
        this.editedMode = str5;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutSessionUpdateWrapper copy$default(WorkoutSessionUpdateWrapper workoutSessionUpdateWrapper, String str, String str2, String str3, String str4, String str5, int i, Object obj) {
        return workoutSessionUpdateWrapper.copy((i & 1) != 0 ? workoutSessionUpdateWrapper.id : str, (i & 2) != 0 ? workoutSessionUpdateWrapper.editedEndTime : str2, (i & 4) != 0 ? workoutSessionUpdateWrapper.editedStartTime : str3, (i & 8) != 0 ? workoutSessionUpdateWrapper.editedType : str4, (i & 16) != 0 ? workoutSessionUpdateWrapper.editedMode : str5);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final String component3() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final String component4() {
        return this.editedType;
    }

    @DexIgnore
    public final String component5() {
        return this.editedMode;
    }

    @DexIgnore
    public final WorkoutSessionUpdateWrapper copy(String str, String str2, String str3, String str4, String str5) {
        pq7.c(str, "id");
        pq7.c(str2, MFSleepSession.COLUMN_EDITED_END_TIME);
        pq7.c(str3, MFSleepSession.COLUMN_EDITED_START_TIME);
        pq7.c(str4, "editedType");
        return new WorkoutSessionUpdateWrapper(str, str2, str3, str4, str5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutSessionUpdateWrapper) {
                WorkoutSessionUpdateWrapper workoutSessionUpdateWrapper = (WorkoutSessionUpdateWrapper) obj;
                if (!pq7.a(this.id, workoutSessionUpdateWrapper.id) || !pq7.a(this.editedEndTime, workoutSessionUpdateWrapper.editedEndTime) || !pq7.a(this.editedStartTime, workoutSessionUpdateWrapper.editedStartTime) || !pq7.a(this.editedType, workoutSessionUpdateWrapper.editedType) || !pq7.a(this.editedMode, workoutSessionUpdateWrapper.editedMode)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getEditedEndTime() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final String getEditedMode() {
        return this.editedMode;
    }

    @DexIgnore
    public final String getEditedStartTime() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final String getEditedType() {
        return this.editedType;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.editedEndTime;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.editedStartTime;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.editedType;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.editedMode;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSessionUpdateWrapper(id=" + this.id + ", editedEndTime=" + this.editedEndTime + ", editedStartTime=" + this.editedStartTime + ", editedType=" + this.editedType + ", editedMode=" + this.editedMode + ")";
    }
}
