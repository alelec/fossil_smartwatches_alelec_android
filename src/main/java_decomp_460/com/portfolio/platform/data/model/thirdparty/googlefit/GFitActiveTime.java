package com.portfolio.platform.data.model.thirdparty.googlefit;

import com.fossil.hm7;
import com.fossil.kq7;
import com.fossil.pq7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitActiveTime {
    @DexIgnore
    public List<Long> activeTimes;
    @DexIgnore
    public int id;

    @DexIgnore
    public GFitActiveTime() {
        this(null, 1, null);
    }

    @DexIgnore
    public GFitActiveTime(List<Long> list) {
        pq7.c(list, "activeTimes");
        this.activeTimes = list;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ GFitActiveTime(List list, int i, kq7 kq7) {
        this((i & 1) != 0 ? hm7.e() : list);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ GFitActiveTime copy$default(GFitActiveTime gFitActiveTime, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = gFitActiveTime.activeTimes;
        }
        return gFitActiveTime.copy(list);
    }

    @DexIgnore
    public final List<Long> component1() {
        return this.activeTimes;
    }

    @DexIgnore
    public final GFitActiveTime copy(List<Long> list) {
        pq7.c(list, "activeTimes");
        return new GFitActiveTime(list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof GFitActiveTime) && pq7.a(this.activeTimes, ((GFitActiveTime) obj).activeTimes));
    }

    @DexIgnore
    public final List<Long> getActiveTimes() {
        return this.activeTimes;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public int hashCode() {
        List<Long> list = this.activeTimes;
        if (list != null) {
            return list.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public final void setActiveTimes(List<Long> list) {
        pq7.c(list, "<set-?>");
        this.activeTimes = list;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public String toString() {
        return "GFitActiveTime(activeTimes=" + this.activeTimes + ")";
    }
}
