package com.portfolio.platform.data.model;

import com.fossil.pq7;
import com.fossil.rj4;
import com.google.gson.Gson;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchParameterResponse {
    @DexIgnore
    @rj4("category")
    public String category;
    @DexIgnore
    @rj4("data")
    public ResponseData data;
    @DexIgnore
    @rj4("id")
    public String id;
    @DexIgnore
    @rj4("metadata")
    public MetaData metaData;
    @DexIgnore
    @rj4("name")
    public String name;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class MetaData {
        @DexIgnore
        @rj4("deviceLongName")
        public String deviceLongName;
        @DexIgnore
        @rj4("deviceShortName")
        public String deviceShortName;
        @DexIgnore
        @rj4("mainHandsFlipped")
        public boolean mainHandsFlipped;
        @DexIgnore
        @rj4("prefixSerialNumbers")
        public ArrayList<String> serialNumbers;
        @DexIgnore
        @rj4("themeMode")
        public String themeMode;
        @DexIgnore
        @rj4("version")
        public Version version;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Version {
            @DexIgnore
            @rj4("major")
            public int versionMajor;
            @DexIgnore
            @rj4("minor")
            public int versionMinor;

            @DexIgnore
            public final int getVersionMajor() {
                return this.versionMajor;
            }

            @DexIgnore
            public final int getVersionMinor() {
                return this.versionMinor;
            }

            @DexIgnore
            public final void setVersionMajor(int i) {
                this.versionMajor = i;
            }

            @DexIgnore
            public final void setVersionMinor(int i) {
                this.versionMinor = i;
            }
        }

        @DexIgnore
        public final String getDeviceLongName() {
            return this.deviceLongName;
        }

        @DexIgnore
        public final String getDeviceShortName() {
            return this.deviceShortName;
        }

        @DexIgnore
        public final boolean getMainHandsFlipped() {
            return this.mainHandsFlipped;
        }

        @DexIgnore
        public final ArrayList<String> getSerialNumbers() {
            return this.serialNumbers;
        }

        @DexIgnore
        public final String getThemeMode() {
            return this.themeMode;
        }

        @DexIgnore
        public final Version getVersion() {
            Version version2 = this.version;
            if (version2 != null) {
                return version2;
            }
            pq7.n("version");
            throw null;
        }

        @DexIgnore
        public final void setDeviceLongName(String str) {
            this.deviceLongName = str;
        }

        @DexIgnore
        public final void setDeviceShortName(String str) {
            this.deviceShortName = str;
        }

        @DexIgnore
        public final void setMainHandsFlipped(boolean z) {
            this.mainHandsFlipped = z;
        }

        @DexIgnore
        public final void setSerialNumbers(ArrayList<String> arrayList) {
            this.serialNumbers = arrayList;
        }

        @DexIgnore
        public final void setThemeMode(String str) {
            this.themeMode = str;
        }

        @DexIgnore
        public final void setVersion(Version version2) {
            pq7.c(version2, "<set-?>");
            this.version = version2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class ResponseData {
        @DexIgnore
        @rj4("content")
        public String content;

        @DexIgnore
        public final String getContent() {
            return this.content;
        }

        @DexIgnore
        public final void setContent(String str) {
            this.content = str;
        }
    }

    @DexIgnore
    public final String getCategory() {
        return this.category;
    }

    @DexIgnore
    public final ResponseData getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final MetaData getMetaData() {
        return this.metaData;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final void setCategory(String str) {
        this.category = str;
    }

    @DexIgnore
    public final void setData(ResponseData responseData) {
        this.data = responseData;
    }

    @DexIgnore
    public final void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public final void setMetaData(MetaData metaData2) {
        this.metaData = metaData2;
    }

    @DexIgnore
    public final void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        pq7.b(t, "gson.toJson(this)");
        return t;
    }

    @DexIgnore
    public final WatchParam toWatchParamModel(String str) {
        String str2 = null;
        pq7.c(str, "serial");
        MetaData metaData2 = this.metaData;
        if (metaData2 != null) {
            int versionMajor = metaData2.getVersion().getVersionMajor();
            MetaData metaData3 = this.metaData;
            if (metaData3 != null) {
                int versionMinor = metaData3.getVersion().getVersionMinor();
                ResponseData responseData = this.data;
                if (responseData != null) {
                    str2 = responseData.getContent();
                }
                return new WatchParam(str, String.valueOf(versionMajor), String.valueOf(versionMinor), str2);
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }
}
