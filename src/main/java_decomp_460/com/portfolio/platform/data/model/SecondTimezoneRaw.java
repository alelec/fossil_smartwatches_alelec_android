package com.portfolio.platform.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rj4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SecondTimezoneRaw implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @rj4("cityCode")
    public String cityCode;
    @DexIgnore
    @rj4("cityName")
    public String cityName;
    @DexIgnore
    @rj4("countryName")
    public String countryName;
    @DexIgnore
    @rj4("timeZoneId")
    public String timeZoneId;
    @DexIgnore
    @rj4("timeZoneName")
    public String timeZoneName;
    @DexIgnore
    @rj4("timeZoneOffset")
    public int timezoneOffset;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SecondTimezoneRaw> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SecondTimezoneRaw createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new SecondTimezoneRaw(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SecondTimezoneRaw[] newArray(int i) {
            return new SecondTimezoneRaw[i];
        }
    }

    @DexIgnore
    public SecondTimezoneRaw() {
        this(null, null, null, null, 0, null, 63, null);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SecondTimezoneRaw(android.os.Parcel r8) {
        /*
            r7 = this;
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r8, r0)
            java.lang.String r1 = r8.readString()
            if (r1 == 0) goto L_0x002c
        L_0x000b:
            java.lang.String r2 = r8.readString()
            if (r2 == 0) goto L_0x002f
        L_0x0011:
            java.lang.String r3 = r8.readString()
            if (r3 == 0) goto L_0x0032
        L_0x0017:
            java.lang.String r4 = r8.readString()
            if (r4 == 0) goto L_0x0035
        L_0x001d:
            int r5 = r8.readInt()
            java.lang.String r6 = r8.readString()
            if (r6 == 0) goto L_0x0038
        L_0x0027:
            r0 = r7
            r0.<init>(r1, r2, r3, r4, r5, r6)
            return
        L_0x002c:
            java.lang.String r1 = ""
            goto L_0x000b
        L_0x002f:
            java.lang.String r2 = ""
            goto L_0x0011
        L_0x0032:
            java.lang.String r3 = ""
            goto L_0x0017
        L_0x0035:
            java.lang.String r4 = ""
            goto L_0x001d
        L_0x0038:
            java.lang.String r6 = ""
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.SecondTimezoneRaw.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public SecondTimezoneRaw(String str, String str2, String str3, String str4, int i, String str5) {
        pq7.c(str, "cityName");
        pq7.c(str2, "countryName");
        pq7.c(str3, "timeZoneName");
        pq7.c(str4, "timeZoneId");
        pq7.c(str5, "cityCode");
        this.cityName = str;
        this.countryName = str2;
        this.timeZoneName = str3;
        this.timeZoneId = str4;
        this.timezoneOffset = i;
        this.cityCode = str5;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ SecondTimezoneRaw(String str, String str2, String str3, String str4, int i, String str5, int i2, kq7 kq7) {
        this((i2 & 1) != 0 ? "" : str, (i2 & 2) != 0 ? "" : str2, (i2 & 4) != 0 ? "" : str3, (i2 & 8) != 0 ? "" : str4, (i2 & 16) != 0 ? 0 : i, (i2 & 32) != 0 ? "" : str5);
    }

    @DexIgnore
    public static /* synthetic */ SecondTimezoneRaw copy$default(SecondTimezoneRaw secondTimezoneRaw, String str, String str2, String str3, String str4, int i, String str5, int i2, Object obj) {
        return secondTimezoneRaw.copy((i2 & 1) != 0 ? secondTimezoneRaw.cityName : str, (i2 & 2) != 0 ? secondTimezoneRaw.countryName : str2, (i2 & 4) != 0 ? secondTimezoneRaw.timeZoneName : str3, (i2 & 8) != 0 ? secondTimezoneRaw.timeZoneId : str4, (i2 & 16) != 0 ? secondTimezoneRaw.timezoneOffset : i, (i2 & 32) != 0 ? secondTimezoneRaw.cityCode : str5);
    }

    @DexIgnore
    public final String component1() {
        return this.cityName;
    }

    @DexIgnore
    public final String component2() {
        return this.countryName;
    }

    @DexIgnore
    public final String component3() {
        return this.timeZoneName;
    }

    @DexIgnore
    public final String component4() {
        return this.timeZoneId;
    }

    @DexIgnore
    public final int component5() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public final String component6() {
        return this.cityCode;
    }

    @DexIgnore
    public final SecondTimezoneRaw copy(String str, String str2, String str3, String str4, int i, String str5) {
        pq7.c(str, "cityName");
        pq7.c(str2, "countryName");
        pq7.c(str3, "timeZoneName");
        pq7.c(str4, "timeZoneId");
        pq7.c(str5, "cityCode");
        return new SecondTimezoneRaw(str, str2, str3, str4, i, str5);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SecondTimezoneRaw) {
                SecondTimezoneRaw secondTimezoneRaw = (SecondTimezoneRaw) obj;
                if (!pq7.a(this.cityName, secondTimezoneRaw.cityName) || !pq7.a(this.countryName, secondTimezoneRaw.countryName) || !pq7.a(this.timeZoneName, secondTimezoneRaw.timeZoneName) || !pq7.a(this.timeZoneId, secondTimezoneRaw.timeZoneId) || this.timezoneOffset != secondTimezoneRaw.timezoneOffset || !pq7.a(this.cityCode, secondTimezoneRaw.cityCode)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getCityCode() {
        return this.cityCode;
    }

    @DexIgnore
    public final String getCityName() {
        return this.cityName;
    }

    @DexIgnore
    public final String getCountryName() {
        return this.countryName;
    }

    @DexIgnore
    public final String getTimeZoneId() {
        return this.timeZoneId;
    }

    @DexIgnore
    public final String getTimeZoneName() {
        return this.timeZoneName;
    }

    @DexIgnore
    public final int getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.cityName;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.countryName;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.timeZoneName;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.timeZoneId;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        int i2 = this.timezoneOffset;
        String str5 = this.cityCode;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return (((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i2) * 31) + i;
    }

    @DexIgnore
    public final void setCityCode(String str) {
        pq7.c(str, "<set-?>");
        this.cityCode = str;
    }

    @DexIgnore
    public final void setCityName(String str) {
        pq7.c(str, "<set-?>");
        this.cityName = str;
    }

    @DexIgnore
    public final void setCountryName(String str) {
        pq7.c(str, "<set-?>");
        this.countryName = str;
    }

    @DexIgnore
    public final void setTimeZoneId(String str) {
        pq7.c(str, "<set-?>");
        this.timeZoneId = str;
    }

    @DexIgnore
    public final void setTimeZoneName(String str) {
        pq7.c(str, "<set-?>");
        this.timeZoneName = str;
    }

    @DexIgnore
    public final void setTimezoneOffset(int i) {
        this.timezoneOffset = i;
    }

    @DexIgnore
    public String toString() {
        return "SecondTimezoneRaw(cityName=" + this.cityName + ", countryName=" + this.countryName + ", timeZoneName=" + this.timeZoneName + ", timeZoneId=" + this.timeZoneId + ", timezoneOffset=" + this.timezoneOffset + ", cityCode=" + this.cityCode + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.cityName);
        parcel.writeString(this.countryName);
        parcel.writeString(this.timeZoneName);
        parcel.writeString(this.timeZoneId);
        parcel.writeInt(this.timezoneOffset);
        parcel.writeString(this.cityCode);
    }
}
