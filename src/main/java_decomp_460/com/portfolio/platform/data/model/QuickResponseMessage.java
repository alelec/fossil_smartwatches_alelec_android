package com.portfolio.platform.data.model;

import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseMessage {
    @DexIgnore
    public int id;
    @DexIgnore
    public String response;

    @DexIgnore
    public QuickResponseMessage(String str) {
        pq7.c(str, "response");
        this.response = str;
    }

    @DexIgnore
    public static /* synthetic */ QuickResponseMessage copy$default(QuickResponseMessage quickResponseMessage, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = quickResponseMessage.response;
        }
        return quickResponseMessage.copy(str);
    }

    @DexIgnore
    public final String component1() {
        return this.response;
    }

    @DexIgnore
    public final QuickResponseMessage copy(String str) {
        pq7.c(str, "response");
        return new QuickResponseMessage(str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof QuickResponseMessage) && pq7.a(this.response, ((QuickResponseMessage) obj).response));
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final String getResponse() {
        return this.response;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.response;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setResponse(String str) {
        pq7.c(str, "<set-?>");
        this.response = str;
    }

    @DexIgnore
    public String toString() {
        return this.id + ' ' + this.response;
    }
}
