package com.portfolio.platform.data.model.room.microapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rj4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ButtonMapping implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @rj4("button")
    public String button;
    @DexIgnore
    @rj4("appId")
    public String microAppId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ButtonMapping> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ButtonMapping createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new ButtonMapping(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ButtonMapping[] newArray(int i) {
            return new ButtonMapping[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ButtonMapping(android.os.Parcel r4) {
        /*
            r3 = this;
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r4, r0)
            java.lang.String r0 = r4.readString()
            java.lang.String r2 = ""
            if (r0 == 0) goto L_0x0017
        L_0x000d:
            java.lang.String r1 = r4.readString()
            if (r1 == 0) goto L_0x001a
        L_0x0013:
            r3.<init>(r0, r1)
            return
        L_0x0017:
            java.lang.String r0 = ""
            goto L_0x000d
        L_0x001a:
            r1 = r2
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.room.microapp.ButtonMapping.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public ButtonMapping(String str, String str2) {
        pq7.c(str, "button");
        pq7.c(str2, "microAppId");
        this.button = str;
        this.microAppId = str2;
    }

    @DexIgnore
    public static /* synthetic */ ButtonMapping copy$default(ButtonMapping buttonMapping, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = buttonMapping.button;
        }
        if ((i & 2) != 0) {
            str2 = buttonMapping.microAppId;
        }
        return buttonMapping.copy(str, str2);
    }

    @DexIgnore
    public final String component1() {
        return this.button;
    }

    @DexIgnore
    public final String component2() {
        return this.microAppId;
    }

    @DexIgnore
    public final ButtonMapping copy(String str, String str2) {
        pq7.c(str, "button");
        pq7.c(str2, "microAppId");
        return new ButtonMapping(str, str2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ButtonMapping) {
                ButtonMapping buttonMapping = (ButtonMapping) obj;
                if (!pq7.a(this.button, buttonMapping.button) || !pq7.a(this.microAppId, buttonMapping.microAppId)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getButton() {
        return this.button;
    }

    @DexIgnore
    public final String getMicroAppId() {
        return this.microAppId;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.button;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.microAppId;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public final void setButton(String str) {
        pq7.c(str, "<set-?>");
        this.button = str;
    }

    @DexIgnore
    public final void setMicroAppId(String str) {
        pq7.c(str, "<set-?>");
        this.microAppId = str;
    }

    @DexIgnore
    public String toString() {
        return "ButtonMapping(button=" + this.button + ", microAppId=" + this.microAppId + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.button);
        parcel.writeString(this.microAppId);
    }
}
