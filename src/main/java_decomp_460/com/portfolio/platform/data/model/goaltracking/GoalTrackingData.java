package com.portfolio.platform.data.model.goaltracking;

import com.fossil.c;
import com.fossil.pq7;
import com.fossil.rj4;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import java.io.Serializable;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingData implements Serializable {
    @DexIgnore
    @rj4("createdAt")
    public long createdAt;
    @DexIgnore
    @rj4("date")
    public Date date;
    @DexIgnore
    @rj4("id")
    public String id;
    @DexIgnore
    public int pinType;
    @DexIgnore
    @rj4("timezoneOffset")
    public /* final */ int timezoneOffsetInSecond;
    @DexIgnore
    @rj4(GoalTrackingEvent.COLUMN_TRACKED_AT)
    public /* final */ DateTime trackedAt;
    @DexIgnore
    @rj4("updatedAt")
    public long updatedAt;

    @DexIgnore
    public GoalTrackingData(String str, DateTime dateTime, int i, Date date2, long j, long j2) {
        pq7.c(str, "id");
        pq7.c(dateTime, GoalTrackingEvent.COLUMN_TRACKED_AT);
        pq7.c(date2, "date");
        this.id = str;
        this.trackedAt = dateTime;
        this.timezoneOffsetInSecond = i;
        this.date = date2;
        this.createdAt = j;
        this.updatedAt = j2;
    }

    @DexIgnore
    public static /* synthetic */ GoalTrackingData copy$default(GoalTrackingData goalTrackingData, String str, DateTime dateTime, int i, Date date2, long j, long j2, int i2, Object obj) {
        return goalTrackingData.copy((i2 & 1) != 0 ? goalTrackingData.id : str, (i2 & 2) != 0 ? goalTrackingData.trackedAt : dateTime, (i2 & 4) != 0 ? goalTrackingData.timezoneOffsetInSecond : i, (i2 & 8) != 0 ? goalTrackingData.date : date2, (i2 & 16) != 0 ? goalTrackingData.createdAt : j, (i2 & 32) != 0 ? goalTrackingData.updatedAt : j2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.trackedAt;
    }

    @DexIgnore
    public final int component3() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final Date component4() {
        return this.date;
    }

    @DexIgnore
    public final long component5() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component6() {
        return this.updatedAt;
    }

    @DexIgnore
    public final GoalTrackingData copy(String str, DateTime dateTime, int i, Date date2, long j, long j2) {
        pq7.c(str, "id");
        pq7.c(dateTime, GoalTrackingEvent.COLUMN_TRACKED_AT);
        pq7.c(date2, "date");
        return new GoalTrackingData(str, dateTime, i, date2, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof GoalTrackingData) {
                GoalTrackingData goalTrackingData = (GoalTrackingData) obj;
                if (!pq7.a(this.id, goalTrackingData.id) || !pq7.a(this.trackedAt, goalTrackingData.trackedAt) || this.timezoneOffsetInSecond != goalTrackingData.timezoneOffsetInSecond || !pq7.a(this.date, goalTrackingData.date) || this.createdAt != goalTrackingData.createdAt || this.updatedAt != goalTrackingData.updatedAt) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final DateTime getTrackedAt() {
        return this.trackedAt;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        DateTime dateTime = this.trackedAt;
        int hashCode2 = dateTime != null ? dateTime.hashCode() : 0;
        int i2 = this.timezoneOffsetInSecond;
        Date date2 = this.date;
        if (date2 != null) {
            i = date2.hashCode();
        }
        return (((((((((hashCode * 31) + hashCode2) * 31) + i2) * 31) + i) * 31) + c.a(this.createdAt)) * 31) + c.a(this.updatedAt);
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        pq7.c(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "GoalTrackingData(id=" + this.id + ", trackedAt=" + this.trackedAt + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", date=" + this.date + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
