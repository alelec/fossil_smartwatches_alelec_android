package com.portfolio.platform.data.model.diana.workout;

import com.fossil.pq7;
import com.fossil.rj4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import java.io.Serializable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutMapDataWrapper implements Serializable {
    @DexIgnore
    @rj4("activeDurationInSecond")
    public Double activeDurationInSecond;
    @DexIgnore
    @rj4("avgHr")
    public Double avgHr;
    @DexIgnore
    @rj4("avgPace")
    public Double avgPace;
    @DexIgnore
    @rj4("bestPace")
    public Double bestPace;
    @DexIgnore
    @rj4("cadence")
    public Double cadence;
    @DexIgnore
    @rj4("calories")
    public Double calories;
    @DexIgnore
    @rj4("distanceInMeter")
    public Double distanceInMeter;
    @DexIgnore
    @rj4("durationInSeconds")
    public Double durationInSeconds;
    @DexIgnore
    @rj4(SampleRaw.COLUMN_END_TIME)
    public Integer endTime;
    @DexIgnore
    @rj4("listCoor")
    public List<WorkoutRouterGpsWrapper> listCoor;
    @DexIgnore
    @rj4("maxHr")
    public Double maxHr;
    @DexIgnore
    public int pinType;
    @DexIgnore
    @rj4(SampleRaw.COLUMN_START_TIME)
    public Integer startTime;
    @DexIgnore
    @rj4("timezoneOffset")
    public Double timezoneOffset;
    @DexIgnore
    @rj4("unitType")
    public String unitType;
    @DexIgnore
    @rj4("workoutType")
    public String workoutType;

    @DexIgnore
    public WorkoutMapDataWrapper(String str, String str2, Double d, Double d2, Double d3, Double d4, Double d5, Double d6, Double d7, Double d8, Integer num, Integer num2, Double d9, Double d10, List<WorkoutRouterGpsWrapper> list) {
        pq7.c(str, "unitType");
        pq7.c(str2, "workoutType");
        this.unitType = str;
        this.workoutType = str2;
        this.distanceInMeter = d;
        this.durationInSeconds = d2;
        this.avgPace = d3;
        this.bestPace = d4;
        this.calories = d5;
        this.cadence = d6;
        this.avgHr = d7;
        this.maxHr = d8;
        this.startTime = num;
        this.endTime = num2;
        this.activeDurationInSecond = d9;
        this.timezoneOffset = d10;
        this.listCoor = list;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutMapDataWrapper copy$default(WorkoutMapDataWrapper workoutMapDataWrapper, String str, String str2, Double d, Double d2, Double d3, Double d4, Double d5, Double d6, Double d7, Double d8, Integer num, Integer num2, Double d9, Double d10, List list, int i, Object obj) {
        return workoutMapDataWrapper.copy((i & 1) != 0 ? workoutMapDataWrapper.unitType : str, (i & 2) != 0 ? workoutMapDataWrapper.workoutType : str2, (i & 4) != 0 ? workoutMapDataWrapper.distanceInMeter : d, (i & 8) != 0 ? workoutMapDataWrapper.durationInSeconds : d2, (i & 16) != 0 ? workoutMapDataWrapper.avgPace : d3, (i & 32) != 0 ? workoutMapDataWrapper.bestPace : d4, (i & 64) != 0 ? workoutMapDataWrapper.calories : d5, (i & 128) != 0 ? workoutMapDataWrapper.cadence : d6, (i & 256) != 0 ? workoutMapDataWrapper.avgHr : d7, (i & 512) != 0 ? workoutMapDataWrapper.maxHr : d8, (i & 1024) != 0 ? workoutMapDataWrapper.startTime : num, (i & 2048) != 0 ? workoutMapDataWrapper.endTime : num2, (i & 4096) != 0 ? workoutMapDataWrapper.activeDurationInSecond : d9, (i & 8192) != 0 ? workoutMapDataWrapper.timezoneOffset : d10, (i & 16384) != 0 ? workoutMapDataWrapper.listCoor : list);
    }

    @DexIgnore
    public final String component1() {
        return this.unitType;
    }

    @DexIgnore
    public final Double component10() {
        return this.maxHr;
    }

    @DexIgnore
    public final Integer component11() {
        return this.startTime;
    }

    @DexIgnore
    public final Integer component12() {
        return this.endTime;
    }

    @DexIgnore
    public final Double component13() {
        return this.activeDurationInSecond;
    }

    @DexIgnore
    public final Double component14() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public final List<WorkoutRouterGpsWrapper> component15() {
        return this.listCoor;
    }

    @DexIgnore
    public final String component2() {
        return this.workoutType;
    }

    @DexIgnore
    public final Double component3() {
        return this.distanceInMeter;
    }

    @DexIgnore
    public final Double component4() {
        return this.durationInSeconds;
    }

    @DexIgnore
    public final Double component5() {
        return this.avgPace;
    }

    @DexIgnore
    public final Double component6() {
        return this.bestPace;
    }

    @DexIgnore
    public final Double component7() {
        return this.calories;
    }

    @DexIgnore
    public final Double component8() {
        return this.cadence;
    }

    @DexIgnore
    public final Double component9() {
        return this.avgHr;
    }

    @DexIgnore
    public final WorkoutMapDataWrapper copy(String str, String str2, Double d, Double d2, Double d3, Double d4, Double d5, Double d6, Double d7, Double d8, Integer num, Integer num2, Double d9, Double d10, List<WorkoutRouterGpsWrapper> list) {
        pq7.c(str, "unitType");
        pq7.c(str2, "workoutType");
        return new WorkoutMapDataWrapper(str, str2, d, d2, d3, d4, d5, d6, d7, d8, num, num2, d9, d10, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutMapDataWrapper) {
                WorkoutMapDataWrapper workoutMapDataWrapper = (WorkoutMapDataWrapper) obj;
                if (!pq7.a(this.unitType, workoutMapDataWrapper.unitType) || !pq7.a(this.workoutType, workoutMapDataWrapper.workoutType) || !pq7.a(this.distanceInMeter, workoutMapDataWrapper.distanceInMeter) || !pq7.a(this.durationInSeconds, workoutMapDataWrapper.durationInSeconds) || !pq7.a(this.avgPace, workoutMapDataWrapper.avgPace) || !pq7.a(this.bestPace, workoutMapDataWrapper.bestPace) || !pq7.a(this.calories, workoutMapDataWrapper.calories) || !pq7.a(this.cadence, workoutMapDataWrapper.cadence) || !pq7.a(this.avgHr, workoutMapDataWrapper.avgHr) || !pq7.a(this.maxHr, workoutMapDataWrapper.maxHr) || !pq7.a(this.startTime, workoutMapDataWrapper.startTime) || !pq7.a(this.endTime, workoutMapDataWrapper.endTime) || !pq7.a(this.activeDurationInSecond, workoutMapDataWrapper.activeDurationInSecond) || !pq7.a(this.timezoneOffset, workoutMapDataWrapper.timezoneOffset) || !pq7.a(this.listCoor, workoutMapDataWrapper.listCoor)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Double getActiveDurationInSecond() {
        return this.activeDurationInSecond;
    }

    @DexIgnore
    public final Double getAvgHr() {
        return this.avgHr;
    }

    @DexIgnore
    public final Double getAvgPace() {
        return this.avgPace;
    }

    @DexIgnore
    public final Double getBestPace() {
        return this.bestPace;
    }

    @DexIgnore
    public final Double getCadence() {
        return this.cadence;
    }

    @DexIgnore
    public final Double getCalories() {
        return this.calories;
    }

    @DexIgnore
    public final Double getDistanceInMeter() {
        return this.distanceInMeter;
    }

    @DexIgnore
    public final Double getDurationInSeconds() {
        return this.durationInSeconds;
    }

    @DexIgnore
    public final Integer getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final List<WorkoutRouterGpsWrapper> getListCoor() {
        return this.listCoor;
    }

    @DexIgnore
    public final Double getMaxHr() {
        return this.maxHr;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final Integer getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final Double getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public final String getUnitType() {
        return this.unitType;
    }

    @DexIgnore
    public final String getWorkoutType() {
        return this.workoutType;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.unitType;
        int i = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.workoutType;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        Double d = this.distanceInMeter;
        int hashCode3 = d != null ? d.hashCode() : 0;
        Double d2 = this.durationInSeconds;
        int hashCode4 = d2 != null ? d2.hashCode() : 0;
        Double d3 = this.avgPace;
        int hashCode5 = d3 != null ? d3.hashCode() : 0;
        Double d4 = this.bestPace;
        int hashCode6 = d4 != null ? d4.hashCode() : 0;
        Double d5 = this.calories;
        int hashCode7 = d5 != null ? d5.hashCode() : 0;
        Double d6 = this.cadence;
        int hashCode8 = d6 != null ? d6.hashCode() : 0;
        Double d7 = this.avgHr;
        int hashCode9 = d7 != null ? d7.hashCode() : 0;
        Double d8 = this.maxHr;
        int hashCode10 = d8 != null ? d8.hashCode() : 0;
        Integer num = this.startTime;
        int hashCode11 = num != null ? num.hashCode() : 0;
        Integer num2 = this.endTime;
        int hashCode12 = num2 != null ? num2.hashCode() : 0;
        Double d9 = this.activeDurationInSecond;
        int hashCode13 = d9 != null ? d9.hashCode() : 0;
        Double d10 = this.timezoneOffset;
        int hashCode14 = d10 != null ? d10.hashCode() : 0;
        List<WorkoutRouterGpsWrapper> list = this.listCoor;
        if (list != null) {
            i = list.hashCode();
        }
        return (((((((((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + hashCode10) * 31) + hashCode11) * 31) + hashCode12) * 31) + hashCode13) * 31) + hashCode14) * 31) + i;
    }

    @DexIgnore
    public final void setActiveDurationInSecond(Double d) {
        this.activeDurationInSecond = d;
    }

    @DexIgnore
    public final void setAvgHr(Double d) {
        this.avgHr = d;
    }

    @DexIgnore
    public final void setAvgPace(Double d) {
        this.avgPace = d;
    }

    @DexIgnore
    public final void setBestPace(Double d) {
        this.bestPace = d;
    }

    @DexIgnore
    public final void setCadence(Double d) {
        this.cadence = d;
    }

    @DexIgnore
    public final void setCalories(Double d) {
        this.calories = d;
    }

    @DexIgnore
    public final void setDistanceInMeter(Double d) {
        this.distanceInMeter = d;
    }

    @DexIgnore
    public final void setDurationInSeconds(Double d) {
        this.durationInSeconds = d;
    }

    @DexIgnore
    public final void setEndTime(Integer num) {
        this.endTime = num;
    }

    @DexIgnore
    public final void setListCoor(List<WorkoutRouterGpsWrapper> list) {
        this.listCoor = list;
    }

    @DexIgnore
    public final void setMaxHr(Double d) {
        this.maxHr = d;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setStartTime(Integer num) {
        this.startTime = num;
    }

    @DexIgnore
    public final void setTimezoneOffset(Double d) {
        this.timezoneOffset = d;
    }

    @DexIgnore
    public final void setUnitType(String str) {
        pq7.c(str, "<set-?>");
        this.unitType = str;
    }

    @DexIgnore
    public final void setWorkoutType(String str) {
        pq7.c(str, "<set-?>");
        this.workoutType = str;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutMapDataWrapper(unitType=" + this.unitType + ", workoutType=" + this.workoutType + ", distanceInMeter=" + this.distanceInMeter + ", durationInSeconds=" + this.durationInSeconds + ", avgPace=" + this.avgPace + ", bestPace=" + this.bestPace + ", calories=" + this.calories + ", cadence=" + this.cadence + ", avgHr=" + this.avgHr + ", maxHr=" + this.maxHr + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", activeDurationInSecond=" + this.activeDurationInSecond + ", timezoneOffset=" + this.timezoneOffset + ", listCoor=" + this.listCoor + ")";
    }
}
