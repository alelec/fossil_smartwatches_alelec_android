package com.portfolio.platform.data.model.diana.workout;

import com.portfolio.platform.data.model.fitnessdata.DistanceWrapper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDistanceKt {
    @DexIgnore
    public static final WorkoutDistance toWorkoutDistance(DistanceWrapper distanceWrapper) {
        if (distanceWrapper != null) {
            return new WorkoutDistance(distanceWrapper.getResolutionInSecond(), distanceWrapper.getValues(), distanceWrapper.getTotal());
        }
        return null;
    }
}
