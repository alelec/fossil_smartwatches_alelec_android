package com.portfolio.platform.data.model;

import com.fossil.rj4;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Range {
    @DexIgnore
    @rj4("hasNext")
    public boolean hasNext;
    @DexIgnore
    @rj4("limit")
    public int limit;
    @DexIgnore
    @rj4(Constants.JSON_KEY_OFFSET)
    public int offset;

    @DexIgnore
    public int getLimit() {
        return this.limit;
    }

    @DexIgnore
    public int getOffset() {
        return this.offset;
    }

    @DexIgnore
    public boolean isHasNext() {
        return this.hasNext;
    }

    @DexIgnore
    public void setHasNext(boolean z) {
        this.hasNext = z;
    }

    @DexIgnore
    public void setOffset(int i) {
        this.offset = i;
    }
}
