package com.portfolio.platform.data.model.room.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.VideoUploader;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.ActivityIntensities;
import java.net.URI;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SampleRaw implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public int activeTime;
    @DexIgnore
    public double calories;
    @DexIgnore
    public double distance;
    @DexIgnore
    public Date endTime;
    @DexIgnore
    public String id;
    @DexIgnore
    public String intensity;
    @DexIgnore
    public ActivityIntensities intensityDistInSteps;
    @DexIgnore
    public String movementTypeValue;
    @DexIgnore
    public int pinType;
    @DexIgnore
    public String sourceId;
    @DexIgnore
    public String sourceTypeValue;
    @DexIgnore
    public Date startTime;
    @DexIgnore
    public double steps;
    @DexIgnore
    public String timeZoneID;
    @DexIgnore
    public int uaPinType;
    @DexIgnore
    public String uri;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SampleRaw> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SampleRaw createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new SampleRaw(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SampleRaw[] newArray(int i) {
            return new SampleRaw[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SampleRaw(android.os.Parcel r27) {
        /*
            r26 = this;
            java.lang.String r2 = "parcel"
            r0 = r27
            com.fossil.pq7.c(r0, r2)
            java.io.Serializable r2 = r27.readSerializable()
            if (r2 == 0) goto L_0x00b9
            r19 = r2
            java.util.Date r19 = (java.util.Date) r19
            java.io.Serializable r2 = r27.readSerializable()
            if (r2 == 0) goto L_0x00b1
            r20 = r2
            java.util.Date r20 = (java.util.Date) r20
            java.lang.String r2 = r27.readString()
            if (r2 == 0) goto L_0x0094
            r21 = r2
        L_0x0023:
            java.lang.String r2 = r27.readString()
            if (r2 == 0) goto L_0x0099
            r22 = r2
        L_0x002b:
            java.lang.String r23 = r27.readString()
            double r24 = r27.readDouble()
            double r10 = r27.readDouble()
            double r12 = r27.readDouble()
            int r14 = r27.readInt()
            java.lang.Class<com.portfolio.platform.data.ActivityIntensities> r2 = com.portfolio.platform.data.ActivityIntensities.class
            java.lang.ClassLoader r2 = r2.getClassLoader()
            r0 = r27
            android.os.Parcelable r2 = r0.readParcelable(r2)
            com.portfolio.platform.data.ActivityIntensities r2 = (com.portfolio.platform.data.ActivityIntensities) r2
            if (r2 == 0) goto L_0x009e
            r15 = r2
        L_0x0050:
            r16 = 0
            r17 = 1024(0x400, float:1.435E-42)
            r18 = 0
            r2 = r26
            r3 = r19
            r4 = r20
            r5 = r21
            r6 = r22
            r7 = r23
            r8 = r24
            r2.<init>(r3, r4, r5, r6, r7, r8, r10, r12, r14, r15, r16, r17, r18)
            java.lang.String r2 = r27.readString()
            if (r2 == 0) goto L_0x00ab
        L_0x006d:
            r0 = r26
            r0.id = r2
            java.lang.String r2 = r27.readString()
            if (r2 == 0) goto L_0x00ae
        L_0x0077:
            r0 = r26
            r0.uri = r2
            int r2 = r27.readInt()
            r0 = r26
            r0.pinType = r2
            int r2 = r27.readInt()
            r0 = r26
            r0.uaPinType = r2
            java.lang.String r2 = r27.readString()
            r0 = r26
            r0.intensity = r2
            return
        L_0x0094:
            java.lang.String r2 = ""
            r21 = r2
            goto L_0x0023
        L_0x0099:
            java.lang.String r2 = ""
            r22 = r2
            goto L_0x002b
        L_0x009e:
            com.portfolio.platform.data.ActivityIntensities r3 = new com.portfolio.platform.data.ActivityIntensities
            r4 = 0
            r6 = 0
            r8 = 0
            r3.<init>(r4, r6, r8)
            r15 = r3
            goto L_0x0050
        L_0x00ab:
            java.lang.String r2 = ""
            goto L_0x006d
        L_0x00ae:
            java.lang.String r2 = ""
            goto L_0x0077
        L_0x00b1:
            com.fossil.il7 r2 = new com.fossil.il7
            java.lang.String r3 = "null cannot be cast to non-null type java.util.Date"
            r2.<init>(r3)
            throw r2
        L_0x00b9:
            com.fossil.il7 r2 = new com.fossil.il7
            java.lang.String r3 = "null cannot be cast to non-null type java.util.Date"
            r2.<init>(r3)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.room.fitness.SampleRaw.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public SampleRaw(Date date, Date date2, String str, String str2, String str3, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, String str4) {
        pq7.c(date, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
        pq7.c(date2, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
        pq7.c(str, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
        pq7.c(str2, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
        pq7.c(activityIntensities, "intensityDistInSteps");
        this.startTime = date;
        this.endTime = date2;
        this.sourceId = str;
        this.sourceTypeValue = str2;
        this.movementTypeValue = str3;
        this.steps = d;
        this.calories = d2;
        this.distance = d3;
        this.activeTime = i;
        this.intensityDistInSteps = activityIntensities;
        this.timeZoneID = str4;
        TimeZone timeZone = TimeZone.getTimeZone(str4);
        if (timeZone == null || (!pq7.a(timeZone.getID(), this.timeZoneID))) {
            TimeZone timeZone2 = TimeZone.getDefault();
            pq7.b(timeZone2, "TimeZone.getDefault()");
            this.timeZoneID = timeZone2.getID();
        }
        String aSCIIString = generateSampleRawURI().toASCIIString();
        pq7.b(aSCIIString, "generateSampleRawURI().toASCIIString()");
        this.uri = aSCIIString;
        this.id = aSCIIString;
        this.pinType = 0;
        this.uaPinType = 0;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ SampleRaw(java.util.Date r19, java.util.Date r20, java.lang.String r21, java.lang.String r22, java.lang.String r23, double r24, double r26, double r28, int r30, com.portfolio.platform.data.ActivityIntensities r31, java.lang.String r32, int r33, com.fossil.kq7 r34) {
        /*
            r18 = this;
            r0 = r33
            r2 = r0 & 1024(0x400, float:1.435E-42)
            if (r2 == 0) goto L_0x002d
            java.util.TimeZone r2 = java.util.TimeZone.getDefault()
            java.lang.String r3 = "TimeZone.getDefault()"
            com.fossil.pq7.b(r2, r3)
            java.lang.String r16 = r2.getID()
        L_0x0013:
            r2 = r18
            r3 = r19
            r4 = r20
            r5 = r21
            r6 = r22
            r7 = r23
            r8 = r24
            r10 = r26
            r12 = r28
            r14 = r30
            r15 = r31
            r2.<init>(r3, r4, r5, r6, r7, r8, r10, r12, r14, r15, r16)
            return
        L_0x002d:
            r16 = r32
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.room.fitness.SampleRaw.<init>(java.util.Date, java.util.Date, java.lang.String, java.lang.String, java.lang.String, double, double, double, int, com.portfolio.platform.data.ActivityIntensities, java.lang.String, int, com.fossil.kq7):void");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SampleRaw(Date date, Date date2, String str, String str2, String str3, String str4, double d, double d2, double d3, int i, ActivityIntensities activityIntensities) {
        this(date, date2, str2, str3, str4, d, d2, d3, i, activityIntensities, str);
        pq7.c(date, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        pq7.c(date2, "end");
        pq7.c(str, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
        pq7.c(str2, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
        pq7.c(str3, "sourceType");
        pq7.c(activityIntensities, "intensityDistInSteps");
        TimeZone timeZone = TimeZone.getTimeZone(str);
        if (timeZone == null || (!pq7.a(timeZone.getID(), str))) {
            TimeZone timeZone2 = TimeZone.getDefault();
            pq7.b(timeZone2, "TimeZone.getDefault()");
            this.timeZoneID = timeZone2.getID();
            return;
        }
        this.timeZoneID = timeZone.getID();
    }

    @DexIgnore
    public static /* synthetic */ SampleRaw copy$default(SampleRaw sampleRaw, Date date, Date date2, String str, String str2, String str3, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, String str4, int i2, Object obj) {
        return sampleRaw.copy((i2 & 1) != 0 ? sampleRaw.startTime : date, (i2 & 2) != 0 ? sampleRaw.endTime : date2, (i2 & 4) != 0 ? sampleRaw.sourceId : str, (i2 & 8) != 0 ? sampleRaw.sourceTypeValue : str2, (i2 & 16) != 0 ? sampleRaw.movementTypeValue : str3, (i2 & 32) != 0 ? sampleRaw.steps : d, (i2 & 64) != 0 ? sampleRaw.calories : d2, (i2 & 128) != 0 ? sampleRaw.distance : d3, (i2 & 256) != 0 ? sampleRaw.activeTime : i, (i2 & 512) != 0 ? sampleRaw.intensityDistInSteps : activityIntensities, (i2 & 1024) != 0 ? sampleRaw.timeZoneID : str4);
    }

    @DexIgnore
    private final URI generateSampleRawURI() {
        StringBuilder sb = new StringBuilder();
        sb.append("sample:");
        String str = this.sourceTypeValue;
        if (str != null) {
            String lowerCase = str.toLowerCase();
            pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
            sb.append(lowerCase);
            String sb2 = sb.toString();
            String w0 = lk5.w0(getStartTimeLocal());
            URI create = URI.create("urn:fsl:fitness:" + sb2 + ':' + this.sourceId + ':' + w0);
            pq7.b(create, "URI.create(\"urn:fsl:fitn\u2026ty:$sourceId:$timestamp\")");
            return create;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    private final Date getEndTimeLocal() {
        Date x = lk5.x(this.endTime, TimeZone.getTimeZone(this.timeZoneID));
        pq7.b(x, "DateHelper.getDateByTime\u2026.getTimeZone(timeZoneID))");
        return x;
    }

    @DexIgnore
    private final Date getStartTimeLocal() {
        Date x = lk5.x(this.startTime, TimeZone.getTimeZone(this.timeZoneID));
        pq7.b(x, "DateHelper.getDateByTime\u2026.getTimeZone(timeZoneID))");
        return x;
    }

    @DexIgnore
    private final int getTimeZoneOffset() {
        return lk5.f0(this.timeZoneID, this.startTime, false);
    }

    @DexIgnore
    public final Date component1() {
        return this.startTime;
    }

    @DexIgnore
    public final ActivityIntensities component10() {
        return this.intensityDistInSteps;
    }

    @DexIgnore
    public final String component11() {
        return this.timeZoneID;
    }

    @DexIgnore
    public final Date component2() {
        return this.endTime;
    }

    @DexIgnore
    public final String component3() {
        return this.sourceId;
    }

    @DexIgnore
    public final String component4() {
        return this.sourceTypeValue;
    }

    @DexIgnore
    public final String component5() {
        return this.movementTypeValue;
    }

    @DexIgnore
    public final double component6() {
        return this.steps;
    }

    @DexIgnore
    public final double component7() {
        return this.calories;
    }

    @DexIgnore
    public final double component8() {
        return this.distance;
    }

    @DexIgnore
    public final int component9() {
        return this.activeTime;
    }

    @DexIgnore
    public final SampleRaw copy(Date date, Date date2, String str, String str2, String str3, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, String str4) {
        pq7.c(date, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
        pq7.c(date2, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
        pq7.c(str, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
        pq7.c(str2, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
        pq7.c(activityIntensities, "intensityDistInSteps");
        return new SampleRaw(date, date2, str, str2, str3, d, d2, d3, i, activityIntensities, str4);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SampleRaw) {
                SampleRaw sampleRaw = (SampleRaw) obj;
                if (!pq7.a(this.startTime, sampleRaw.startTime) || !pq7.a(this.endTime, sampleRaw.endTime) || !pq7.a(this.sourceId, sampleRaw.sourceId) || !pq7.a(this.sourceTypeValue, sampleRaw.sourceTypeValue) || !pq7.a(this.movementTypeValue, sampleRaw.movementTypeValue) || Double.compare(this.steps, sampleRaw.steps) != 0 || Double.compare(this.calories, sampleRaw.calories) != 0 || Double.compare(this.distance, sampleRaw.distance) != 0 || this.activeTime != sampleRaw.activeTime || !pq7.a(this.intensityDistInSteps, sampleRaw.intensityDistInSteps) || !pq7.a(this.timeZoneID, sampleRaw.timeZoneID)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getActiveTime() {
        return this.activeTime;
    }

    @DexIgnore
    public final double getAverageSteps() {
        return this.steps / ((double) TimeUnit.MILLISECONDS.toMinutes(this.endTime.getTime() - this.startTime.getTime()));
    }

    @DexIgnore
    public final double getCalories() {
        return this.calories;
    }

    @DexIgnore
    public final double getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final Date getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final ActivityIntensities getIntensityDistInSteps() {
        return this.intensityDistInSteps;
    }

    @DexIgnore
    public final String getMovementTypeValue() {
        return this.movementTypeValue;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSourceId() {
        return this.sourceId;
    }

    @DexIgnore
    public final String getSourceTypeValue() {
        return this.sourceTypeValue;
    }

    @DexIgnore
    public final Date getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final double getSteps() {
        return this.steps;
    }

    @DexIgnore
    public final String getTimeZoneID() {
        return this.timeZoneID;
    }

    @DexIgnore
    public final int getUaPinType() {
        return this.uaPinType;
    }

    @DexIgnore
    public final String getUri() {
        return this.uri;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Date date = this.startTime;
        int hashCode = date != null ? date.hashCode() : 0;
        Date date2 = this.endTime;
        int hashCode2 = date2 != null ? date2.hashCode() : 0;
        String str = this.sourceId;
        int hashCode3 = str != null ? str.hashCode() : 0;
        String str2 = this.sourceTypeValue;
        int hashCode4 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.movementTypeValue;
        int hashCode5 = str3 != null ? str3.hashCode() : 0;
        int doubleToLongBits = Double.doubleToLongBits(this.steps);
        int doubleToLongBits2 = Double.doubleToLongBits(this.calories);
        int doubleToLongBits3 = Double.doubleToLongBits(this.distance);
        int i2 = this.activeTime;
        ActivityIntensities activityIntensities = this.intensityDistInSteps;
        int hashCode6 = activityIntensities != null ? activityIntensities.hashCode() : 0;
        String str4 = this.timeZoneID;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return (((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + doubleToLongBits) * 31) + doubleToLongBits2) * 31) + doubleToLongBits3) * 31) + i2) * 31) + hashCode6) * 31) + i;
    }

    @DexIgnore
    public final void setActiveTime(int i) {
        this.activeTime = i;
    }

    @DexIgnore
    public final void setCalories(double d) {
        this.calories = d;
    }

    @DexIgnore
    public final void setDistance(double d) {
        this.distance = d;
    }

    @DexIgnore
    public final void setEndTime(Date date) {
        pq7.c(date, "<set-?>");
        this.endTime = date;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setIntensityDistInSteps(ActivityIntensities activityIntensities) {
        pq7.c(activityIntensities, "<set-?>");
        this.intensityDistInSteps = activityIntensities;
    }

    @DexIgnore
    public final void setMovementTypeValue(String str) {
        this.movementTypeValue = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSourceId(String str) {
        pq7.c(str, "<set-?>");
        this.sourceId = str;
    }

    @DexIgnore
    public final void setSourceTypeValue(String str) {
        pq7.c(str, "<set-?>");
        this.sourceTypeValue = str;
    }

    @DexIgnore
    public final void setStartTime(Date date) {
        pq7.c(date, "<set-?>");
        this.startTime = date;
    }

    @DexIgnore
    public final void setSteps(double d) {
        this.steps = d;
    }

    @DexIgnore
    public final void setTimeZoneID(String str) {
        this.timeZoneID = str;
    }

    @DexIgnore
    public final void setUaPinType(int i) {
        this.uaPinType = i;
    }

    @DexIgnore
    public final void setUri(String str) {
        pq7.c(str, "<set-?>");
        this.uri = str;
    }

    @DexIgnore
    public final ActivitySample toActivitySample() {
        FLogger.INSTANCE.getLocal().d("SampleRaw", "toActivitySample");
        DateTimeZone forTimeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone(this.timeZoneID));
        return new ActivitySample("", this.startTime, new DateTime(getStartTimeLocal().getTime(), forTimeZone), new DateTime(getEndTimeLocal(), forTimeZone), this.steps, this.calories, this.distance, this.activeTime, this.intensityDistInSteps, getTimeZoneOffset(), this.sourceId, 0, 0, 0);
    }

    @DexIgnore
    public String toString() {
        return "SampleRaw(startTime=" + this.startTime + ", endTime=" + this.endTime + ", sourceId=" + this.sourceId + ", sourceTypeValue=" + this.sourceTypeValue + ", movementTypeValue=" + this.movementTypeValue + ", steps=" + this.steps + ", calories=" + this.calories + ", distance=" + this.distance + ", activeTime=" + this.activeTime + ", intensityDistInSteps=" + this.intensityDistInSteps + ", timeZoneID=" + this.timeZoneID + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeSerializable(this.startTime);
        parcel.writeSerializable(this.endTime);
        parcel.writeString(this.sourceId);
        parcel.writeString(this.sourceTypeValue);
        parcel.writeString(this.movementTypeValue);
        parcel.writeDouble(this.steps);
        parcel.writeDouble(this.calories);
        parcel.writeDouble(this.distance);
        parcel.writeInt(this.activeTime);
        parcel.writeParcelable(this.intensityDistInSteps, i);
        parcel.writeString(this.id);
        parcel.writeString(this.uri);
        parcel.writeInt(this.pinType);
        parcel.writeInt(this.uaPinType);
        parcel.writeString(this.intensity);
    }
}
