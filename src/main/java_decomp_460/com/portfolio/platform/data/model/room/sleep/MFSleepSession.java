package com.portfolio.platform.data.model.room.sleep;

import com.fossil.c;
import com.fossil.kq7;
import com.fossil.ll5;
import com.fossil.pq7;
import com.google.gson.Gson;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MFSleepSession implements Serializable {
    @DexIgnore
    public Integer bookmarkTime;
    @DexIgnore
    public DateTime createdAt;
    @DexIgnore
    public long date;
    @DexIgnore
    public Date day;
    @DexIgnore
    public String deviceSerialNumber;
    @DexIgnore
    public Integer editedEndTime;
    @DexIgnore
    public Integer editedSleepMinutes;
    @DexIgnore
    public SleepDistribution editedSleepStateDistInMinute;
    @DexIgnore
    public Integer editedStartTime;
    @DexIgnore
    public SleepSessionHeartRate heartRate;
    @DexIgnore
    public double normalizedSleepQuality;
    @DexIgnore
    public int pinType;
    @DexIgnore
    public int realEndTime;
    @DexIgnore
    public int realSleepMinutes;
    @DexIgnore
    public SleepDistribution realSleepStateDistInMinute;
    @DexIgnore
    public int realStartTime;
    @DexIgnore
    public String sleepStates;
    @DexIgnore
    public int source;
    @DexIgnore
    public Integer syncTime;
    @DexIgnore
    public int timezoneOffset;
    @DexIgnore
    public DateTime updatedAt;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public MFSleepSession(long j, Date date2, int i, String str, int i2, int i3, double d, int i4, int i5, int i6, int i7, SleepDistribution sleepDistribution, String str2, SleepSessionHeartRate sleepSessionHeartRate, DateTime dateTime) {
        this(j, date2, str, Integer.valueOf(i2), Integer.valueOf(i3), d, i4, i5, i6, i7, sleepDistribution, Integer.valueOf(i5), Integer.valueOf(i6), Integer.valueOf(i7), sleepDistribution, str2, sleepSessionHeartRate, new DateTime(), dateTime, i);
        pq7.c(date2, "day");
        pq7.c(str, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
        pq7.c(sleepDistribution, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
        pq7.c(str2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
        pq7.c(dateTime, "updatedAt");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public MFSleepSession(long j, Date date2, int i, String str, Integer num, Integer num2, double d, int i2, int i3, int i4, int i5, SleepDistribution sleepDistribution, Integer num3, Integer num4, Integer num5, SleepDistribution sleepDistribution2, String str2, SleepSessionHeartRate sleepSessionHeartRate, DateTime dateTime, DateTime dateTime2) {
        this(j, date2, str, num, num2, d, i2, i3, i4, i5, sleepDistribution, num3, num4, num5, sleepDistribution2, str2, sleepSessionHeartRate, dateTime, dateTime2, i);
        pq7.c(date2, "day");
        pq7.c(sleepDistribution, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
        pq7.c(str2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
        pq7.c(dateTime, "createdAt");
        pq7.c(dateTime2, "updatedAt");
    }

    @DexIgnore
    public MFSleepSession(long j, Date date2, String str, Integer num, Integer num2, double d, int i, int i2, int i3, int i4, SleepDistribution sleepDistribution, Integer num3, Integer num4, Integer num5, SleepDistribution sleepDistribution2, String str2, SleepSessionHeartRate sleepSessionHeartRate, DateTime dateTime, DateTime dateTime2, int i5) {
        pq7.c(date2, "day");
        pq7.c(sleepDistribution, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
        pq7.c(str2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
        pq7.c(dateTime, "createdAt");
        pq7.c(dateTime2, "updatedAt");
        this.date = j;
        this.day = date2;
        this.deviceSerialNumber = str;
        this.syncTime = num;
        this.bookmarkTime = num2;
        this.normalizedSleepQuality = d;
        this.source = i;
        this.realStartTime = i2;
        this.realEndTime = i3;
        this.realSleepMinutes = i4;
        this.realSleepStateDistInMinute = sleepDistribution;
        this.editedStartTime = num3;
        this.editedEndTime = num4;
        this.editedSleepMinutes = num5;
        this.editedSleepStateDistInMinute = sleepDistribution2;
        this.sleepStates = str2;
        this.heartRate = sleepSessionHeartRate;
        this.createdAt = dateTime;
        this.updatedAt = dateTime2;
        this.timezoneOffset = i5;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MFSleepSession(long j, Date date2, String str, Integer num, Integer num2, double d, int i, int i2, int i3, int i4, SleepDistribution sleepDistribution, Integer num3, Integer num4, Integer num5, SleepDistribution sleepDistribution2, String str2, SleepSessionHeartRate sleepSessionHeartRate, DateTime dateTime, DateTime dateTime2, int i5, int i6, kq7 kq7) {
        this(j, date2, str, num, num2, d, i, i2, i3, i4, sleepDistribution, num3, num4, num5, sleepDistribution2, str2, sleepSessionHeartRate, dateTime, dateTime2, (524288 & i6) != 0 ? ll5.i() : i5);
    }

    @DexIgnore
    public static /* synthetic */ MFSleepSession copy$default(MFSleepSession mFSleepSession, long j, Date date2, String str, Integer num, Integer num2, double d, int i, int i2, int i3, int i4, SleepDistribution sleepDistribution, Integer num3, Integer num4, Integer num5, SleepDistribution sleepDistribution2, String str2, SleepSessionHeartRate sleepSessionHeartRate, DateTime dateTime, DateTime dateTime2, int i5, int i6, Object obj) {
        return mFSleepSession.copy((i6 & 1) != 0 ? mFSleepSession.date : j, (i6 & 2) != 0 ? mFSleepSession.day : date2, (i6 & 4) != 0 ? mFSleepSession.deviceSerialNumber : str, (i6 & 8) != 0 ? mFSleepSession.syncTime : num, (i6 & 16) != 0 ? mFSleepSession.bookmarkTime : num2, (i6 & 32) != 0 ? mFSleepSession.normalizedSleepQuality : d, (i6 & 64) != 0 ? mFSleepSession.source : i, (i6 & 128) != 0 ? mFSleepSession.realStartTime : i2, (i6 & 256) != 0 ? mFSleepSession.realEndTime : i3, (i6 & 512) != 0 ? mFSleepSession.realSleepMinutes : i4, (i6 & 1024) != 0 ? mFSleepSession.realSleepStateDistInMinute : sleepDistribution, (i6 & 2048) != 0 ? mFSleepSession.editedStartTime : num3, (i6 & 4096) != 0 ? mFSleepSession.editedEndTime : num4, (i6 & 8192) != 0 ? mFSleepSession.editedSleepMinutes : num5, (i6 & 16384) != 0 ? mFSleepSession.editedSleepStateDistInMinute : sleepDistribution2, (32768 & i6) != 0 ? mFSleepSession.sleepStates : str2, (65536 & i6) != 0 ? mFSleepSession.heartRate : sleepSessionHeartRate, (131072 & i6) != 0 ? mFSleepSession.createdAt : dateTime, (262144 & i6) != 0 ? mFSleepSession.updatedAt : dateTime2, (524288 & i6) != 0 ? mFSleepSession.timezoneOffset : i5);
    }

    @DexIgnore
    public final long component1() {
        return this.date;
    }

    @DexIgnore
    public final int component10() {
        return this.realSleepMinutes;
    }

    @DexIgnore
    public final SleepDistribution component11() {
        return this.realSleepStateDistInMinute;
    }

    @DexIgnore
    public final Integer component12() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final Integer component13() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final Integer component14() {
        return this.editedSleepMinutes;
    }

    @DexIgnore
    public final SleepDistribution component15() {
        return this.editedSleepStateDistInMinute;
    }

    @DexIgnore
    public final String component16() {
        return this.sleepStates;
    }

    @DexIgnore
    public final SleepSessionHeartRate component17() {
        return this.heartRate;
    }

    @DexIgnore
    public final DateTime component18() {
        return this.createdAt;
    }

    @DexIgnore
    public final DateTime component19() {
        return this.updatedAt;
    }

    @DexIgnore
    public final Date component2() {
        return this.day;
    }

    @DexIgnore
    public final int component20() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public final String component3() {
        return this.deviceSerialNumber;
    }

    @DexIgnore
    public final Integer component4() {
        return this.syncTime;
    }

    @DexIgnore
    public final Integer component5() {
        return this.bookmarkTime;
    }

    @DexIgnore
    public final double component6() {
        return this.normalizedSleepQuality;
    }

    @DexIgnore
    public final int component7() {
        return this.source;
    }

    @DexIgnore
    public final int component8() {
        return this.realStartTime;
    }

    @DexIgnore
    public final int component9() {
        return this.realEndTime;
    }

    @DexIgnore
    public final MFSleepSession copy(long j, Date date2, String str, Integer num, Integer num2, double d, int i, int i2, int i3, int i4, SleepDistribution sleepDistribution, Integer num3, Integer num4, Integer num5, SleepDistribution sleepDistribution2, String str2, SleepSessionHeartRate sleepSessionHeartRate, DateTime dateTime, DateTime dateTime2, int i5) {
        pq7.c(date2, "day");
        pq7.c(sleepDistribution, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
        pq7.c(str2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
        pq7.c(dateTime, "createdAt");
        pq7.c(dateTime2, "updatedAt");
        return new MFSleepSession(j, date2, str, num, num2, d, i, i2, i3, i4, sleepDistribution, num3, num4, num5, sleepDistribution2, str2, sleepSessionHeartRate, dateTime, dateTime2, i5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof MFSleepSession) {
                MFSleepSession mFSleepSession = (MFSleepSession) obj;
                if (this.date != mFSleepSession.date || !pq7.a(this.day, mFSleepSession.day) || !pq7.a(this.deviceSerialNumber, mFSleepSession.deviceSerialNumber) || !pq7.a(this.syncTime, mFSleepSession.syncTime) || !pq7.a(this.bookmarkTime, mFSleepSession.bookmarkTime) || Double.compare(this.normalizedSleepQuality, mFSleepSession.normalizedSleepQuality) != 0 || this.source != mFSleepSession.source || this.realStartTime != mFSleepSession.realStartTime || this.realEndTime != mFSleepSession.realEndTime || this.realSleepMinutes != mFSleepSession.realSleepMinutes || !pq7.a(this.realSleepStateDistInMinute, mFSleepSession.realSleepStateDistInMinute) || !pq7.a(this.editedStartTime, mFSleepSession.editedStartTime) || !pq7.a(this.editedEndTime, mFSleepSession.editedEndTime) || !pq7.a(this.editedSleepMinutes, mFSleepSession.editedSleepMinutes) || !pq7.a(this.editedSleepStateDistInMinute, mFSleepSession.editedSleepStateDistInMinute) || !pq7.a(this.sleepStates, mFSleepSession.sleepStates) || !pq7.a(this.heartRate, mFSleepSession.heartRate) || !pq7.a(this.createdAt, mFSleepSession.createdAt) || !pq7.a(this.updatedAt, mFSleepSession.updatedAt) || this.timezoneOffset != mFSleepSession.timezoneOffset) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Integer getBookmarkTime() {
        return this.bookmarkTime;
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final long getDate() {
        return this.date;
    }

    @DexIgnore
    public final Date getDay() {
        return this.day;
    }

    @DexIgnore
    public final String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }

    @DexIgnore
    public final Integer getEditedEndTime() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final Integer getEditedSleepMinutes() {
        return this.editedSleepMinutes;
    }

    @DexIgnore
    public final SleepDistribution getEditedSleepStateDistInMinute() {
        return this.editedSleepStateDistInMinute;
    }

    @DexIgnore
    public final Integer getEditedStartTime() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final int getEndTime() {
        Integer num = this.editedEndTime;
        return num != null ? num.intValue() : this.realEndTime;
    }

    @DexIgnore
    public final SleepSessionHeartRate getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final double getNormalizedSleepQuality() {
        return this.normalizedSleepQuality;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getRealEndTime() {
        return this.realEndTime;
    }

    @DexIgnore
    public final int getRealSleepMinutes() {
        return this.realSleepMinutes;
    }

    @DexIgnore
    public final SleepDistribution getRealSleepStateDistInMinute() {
        return this.realSleepStateDistInMinute;
    }

    @DexIgnore
    public final int getRealStartTime() {
        return this.realStartTime;
    }

    @DexIgnore
    public final int getSleepMinutes() {
        Integer num = this.editedSleepMinutes;
        return num != null ? num.intValue() : this.realSleepMinutes;
    }

    @DexIgnore
    public final SleepDistribution getSleepState() {
        SleepDistribution sleepDistribution = this.editedSleepStateDistInMinute;
        return sleepDistribution != null ? sleepDistribution : this.realSleepStateDistInMinute;
    }

    @DexIgnore
    public final List<WrapperSleepStateChange> getSleepStateChange() {
        try {
            return (List) new Gson().l(this.sleepStates, new MFSleepSession$getSleepStateChange$Anon1().getType());
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList();
        }
    }

    @DexIgnore
    public final String getSleepStates() {
        return this.sleepStates;
    }

    @DexIgnore
    public final int getSource() {
        return this.source;
    }

    @DexIgnore
    public final int getStartTime() {
        Integer num = this.editedStartTime;
        return num != null ? num.intValue() : this.realStartTime;
    }

    @DexIgnore
    public final Integer getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public final int getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int a2 = c.a(this.date);
        Date date2 = this.day;
        int i = 0;
        int hashCode = date2 != null ? date2.hashCode() : 0;
        String str = this.deviceSerialNumber;
        int hashCode2 = str != null ? str.hashCode() : 0;
        Integer num = this.syncTime;
        int hashCode3 = num != null ? num.hashCode() : 0;
        Integer num2 = this.bookmarkTime;
        int hashCode4 = num2 != null ? num2.hashCode() : 0;
        int doubleToLongBits = Double.doubleToLongBits(this.normalizedSleepQuality);
        int i2 = this.source;
        int i3 = this.realStartTime;
        int i4 = this.realEndTime;
        int i5 = this.realSleepMinutes;
        SleepDistribution sleepDistribution = this.realSleepStateDistInMinute;
        int hashCode5 = sleepDistribution != null ? sleepDistribution.hashCode() : 0;
        Integer num3 = this.editedStartTime;
        int hashCode6 = num3 != null ? num3.hashCode() : 0;
        Integer num4 = this.editedEndTime;
        int hashCode7 = num4 != null ? num4.hashCode() : 0;
        Integer num5 = this.editedSleepMinutes;
        int hashCode8 = num5 != null ? num5.hashCode() : 0;
        SleepDistribution sleepDistribution2 = this.editedSleepStateDistInMinute;
        int hashCode9 = sleepDistribution2 != null ? sleepDistribution2.hashCode() : 0;
        String str2 = this.sleepStates;
        int hashCode10 = str2 != null ? str2.hashCode() : 0;
        SleepSessionHeartRate sleepSessionHeartRate = this.heartRate;
        int hashCode11 = sleepSessionHeartRate != null ? sleepSessionHeartRate.hashCode() : 0;
        DateTime dateTime = this.createdAt;
        int hashCode12 = dateTime != null ? dateTime.hashCode() : 0;
        DateTime dateTime2 = this.updatedAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return ((((((((((((((((((((((((((((((((((((hashCode + (a2 * 31)) * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + doubleToLongBits) * 31) + i2) * 31) + i3) * 31) + i4) * 31) + i5) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + hashCode10) * 31) + hashCode11) * 31) + hashCode12) * 31) + i) * 31) + this.timezoneOffset;
    }

    @DexIgnore
    public final void setBookmarkTime(Integer num) {
        this.bookmarkTime = num;
    }

    @DexIgnore
    public final void setCreatedAt(DateTime dateTime) {
        pq7.c(dateTime, "<set-?>");
        this.createdAt = dateTime;
    }

    @DexIgnore
    public final void setDate(long j) {
        this.date = j;
    }

    @DexIgnore
    public final void setDay(Date date2) {
        pq7.c(date2, "<set-?>");
        this.day = date2;
    }

    @DexIgnore
    public final void setDeviceSerialNumber(String str) {
        this.deviceSerialNumber = str;
    }

    @DexIgnore
    public final void setEditedEndTime(Integer num) {
        this.editedEndTime = num;
    }

    @DexIgnore
    public final void setEditedSleepMinutes(Integer num) {
        this.editedSleepMinutes = num;
    }

    @DexIgnore
    public final void setEditedSleepStateDistInMinute(SleepDistribution sleepDistribution) {
        this.editedSleepStateDistInMinute = sleepDistribution;
    }

    @DexIgnore
    public final void setEditedStartTime(Integer num) {
        this.editedStartTime = num;
    }

    @DexIgnore
    public final void setHeartRate(SleepSessionHeartRate sleepSessionHeartRate) {
        this.heartRate = sleepSessionHeartRate;
    }

    @DexIgnore
    public final void setNormalizedSleepQuality(double d) {
        this.normalizedSleepQuality = d;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setRealEndTime(int i) {
        this.realEndTime = i;
    }

    @DexIgnore
    public final void setRealSleepMinutes(int i) {
        this.realSleepMinutes = i;
    }

    @DexIgnore
    public final void setRealSleepStateDistInMinute(SleepDistribution sleepDistribution) {
        pq7.c(sleepDistribution, "<set-?>");
        this.realSleepStateDistInMinute = sleepDistribution;
    }

    @DexIgnore
    public final void setRealStartTime(int i) {
        this.realStartTime = i;
    }

    @DexIgnore
    public final void setSleepStates(String str) {
        pq7.c(str, "<set-?>");
        this.sleepStates = str;
    }

    @DexIgnore
    public final void setSource(int i) {
        this.source = i;
    }

    @DexIgnore
    public final void setSyncTime(Integer num) {
        this.syncTime = num;
    }

    @DexIgnore
    public final void setTimezoneOffset(int i) {
        this.timezoneOffset = i;
    }

    @DexIgnore
    public final void setUpdatedAt(DateTime dateTime) {
        pq7.c(dateTime, "<set-?>");
        this.updatedAt = dateTime;
    }

    @DexIgnore
    public String toString() {
        return "MFSleepSession(date=" + this.date + ", day=" + this.day + ", deviceSerialNumber=" + this.deviceSerialNumber + ", syncTime=" + this.syncTime + ", bookmarkTime=" + this.bookmarkTime + ", normalizedSleepQuality=" + this.normalizedSleepQuality + ", source=" + this.source + ", realStartTime=" + this.realStartTime + ", realEndTime=" + this.realEndTime + ", realSleepMinutes=" + this.realSleepMinutes + ", realSleepStateDistInMinute=" + this.realSleepStateDistInMinute + ", editedStartTime=" + this.editedStartTime + ", editedEndTime=" + this.editedEndTime + ", editedSleepMinutes=" + this.editedSleepMinutes + ", editedSleepStateDistInMinute=" + this.editedSleepStateDistInMinute + ", sleepStates=" + this.sleepStates + ", heartRate=" + this.heartRate + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", timezoneOffset=" + this.timezoneOffset + ")";
    }
}
