package com.portfolio.platform.data.model.diana.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.b;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rj4;
import com.fossil.zj5;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherLocationWrapper implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @rj4("fullName")
    public String fullName;
    @DexIgnore
    @rj4("id")
    public String id;
    @DexIgnore
    @zj5
    public boolean isEnableLocation;
    @DexIgnore
    @zj5
    public boolean isUseCurrentLocation;
    @DexIgnore
    @rj4(Constants.LAT)
    public double lat;
    @DexIgnore
    @rj4("lng")
    public double lng;
    @DexIgnore
    @rj4("name")
    public String name;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherLocationWrapper> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherLocationWrapper createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new WeatherLocationWrapper(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherLocationWrapper[] newArray(int i) {
            return new WeatherLocationWrapper[i];
        }
    }

    @DexIgnore
    public WeatherLocationWrapper() {
        this(null, 0.0d, 0.0d, null, null, false, false, 127, null);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public WeatherLocationWrapper(android.os.Parcel r11) {
        /*
            r10 = this;
            java.lang.String r0 = "in"
            com.fossil.pq7.c(r11, r0)
            java.lang.String r1 = r11.readString()
            if (r1 == 0) goto L_0x0046
        L_0x000b:
            double r2 = r11.readDouble()
            double r4 = r11.readDouble()
            java.lang.String r6 = r11.readString()
            if (r6 == 0) goto L_0x0049
        L_0x0019:
            java.lang.String r7 = r11.readString()
            if (r7 == 0) goto L_0x004c
        L_0x001f:
            java.lang.String r0 = r11.readString()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            java.lang.String r8 = "java.lang.Boolean.valueOf(`in`.readString())"
            com.fossil.pq7.b(r0, r8)
            boolean r8 = r0.booleanValue()
            java.lang.String r0 = r11.readString()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            java.lang.String r9 = "java.lang.Boolean.valueOf(`in`.readString())"
            com.fossil.pq7.b(r0, r9)
            boolean r9 = r0.booleanValue()
            r0 = r10
            r0.<init>(r1, r2, r4, r6, r7, r8, r9)
            return
        L_0x0046:
            java.lang.String r1 = ""
            goto L_0x000b
        L_0x0049:
            java.lang.String r6 = ""
            goto L_0x0019
        L_0x004c:
            java.lang.String r7 = ""
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public WeatherLocationWrapper(String str, double d, double d2, String str2, String str3, boolean z, boolean z2) {
        pq7.c(str, "id");
        pq7.c(str2, "name");
        pq7.c(str3, "fullName");
        this.id = str;
        this.lat = d;
        this.lng = d2;
        this.name = str2;
        this.fullName = str3;
        this.isUseCurrentLocation = z;
        this.isEnableLocation = z2;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WeatherLocationWrapper(String str, double d, double d2, String str2, String str3, boolean z, boolean z2, int i, kq7 kq7) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? 0.0d : d, (i & 4) == 0 ? d2 : 0.0d, (i & 8) != 0 ? "" : str2, (i & 16) == 0 ? str3 : "", (i & 32) != 0 ? false : z, (i & 64) != 0 ? true : z2);
    }

    @DexIgnore
    public static /* synthetic */ WeatherLocationWrapper copy$default(WeatherLocationWrapper weatherLocationWrapper, String str, double d, double d2, String str2, String str3, boolean z, boolean z2, int i, Object obj) {
        return weatherLocationWrapper.copy((i & 1) != 0 ? weatherLocationWrapper.id : str, (i & 2) != 0 ? weatherLocationWrapper.lat : d, (i & 4) != 0 ? weatherLocationWrapper.lng : d2, (i & 8) != 0 ? weatherLocationWrapper.name : str2, (i & 16) != 0 ? weatherLocationWrapper.fullName : str3, (i & 32) != 0 ? weatherLocationWrapper.isUseCurrentLocation : z, (i & 64) != 0 ? weatherLocationWrapper.isEnableLocation : z2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final double component2() {
        return this.lat;
    }

    @DexIgnore
    public final double component3() {
        return this.lng;
    }

    @DexIgnore
    public final String component4() {
        return this.name;
    }

    @DexIgnore
    public final String component5() {
        return this.fullName;
    }

    @DexIgnore
    public final boolean component6() {
        return this.isUseCurrentLocation;
    }

    @DexIgnore
    public final boolean component7() {
        return this.isEnableLocation;
    }

    @DexIgnore
    public final WeatherLocationWrapper copy(String str, double d, double d2, String str2, String str3, boolean z, boolean z2) {
        pq7.c(str, "id");
        pq7.c(str2, "name");
        pq7.c(str3, "fullName");
        return new WeatherLocationWrapper(str, d, d2, str2, str3, z, z2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WeatherLocationWrapper) {
                WeatherLocationWrapper weatherLocationWrapper = (WeatherLocationWrapper) obj;
                if (!(pq7.a(this.id, weatherLocationWrapper.id) && Double.compare(this.lat, weatherLocationWrapper.lat) == 0 && Double.compare(this.lng, weatherLocationWrapper.lng) == 0 && pq7.a(this.name, weatherLocationWrapper.name) && pq7.a(this.fullName, weatherLocationWrapper.fullName) && this.isUseCurrentLocation == weatherLocationWrapper.isUseCurrentLocation && this.isEnableLocation == weatherLocationWrapper.isEnableLocation)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getFullName() {
        return this.fullName;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final double getLat() {
        return this.lat;
    }

    @DexIgnore
    public final double getLng() {
        return this.lng;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public int hashCode() {
        int i = 1;
        int i2 = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        int a2 = b.a(this.lat);
        int a3 = b.a(this.lng);
        String str2 = this.name;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.fullName;
        if (str3 != null) {
            i2 = str3.hashCode();
        }
        boolean z = this.isUseCurrentLocation;
        if (z) {
            z = true;
        }
        boolean z2 = this.isEnableLocation;
        if (!z2) {
            i = z2 ? 1 : 0;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        return (((((((((((hashCode * 31) + a2) * 31) + a3) * 31) + hashCode2) * 31) + i2) * 31) + i3) * 31) + i;
    }

    @DexIgnore
    public final boolean isEnableLocation() {
        return this.isEnableLocation;
    }

    @DexIgnore
    public final boolean isUseCurrentLocation() {
        return this.isUseCurrentLocation;
    }

    @DexIgnore
    public final void setEnableLocation(boolean z) {
        this.isEnableLocation = z;
    }

    @DexIgnore
    public final void setFullName(String str) {
        pq7.c(str, "<set-?>");
        this.fullName = str;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setLat(double d) {
        this.lat = d;
    }

    @DexIgnore
    public final void setLng(double d) {
        this.lng = d;
    }

    @DexIgnore
    public final void setName(String str) {
        pq7.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setUseCurrentLocation(boolean z) {
        this.isUseCurrentLocation = z;
    }

    @DexIgnore
    public String toString() {
        return "WeatherLocationWrapper(id=" + this.id + ", lat=" + this.lat + ", lng=" + this.lng + ", name=" + this.name + ", fullName=" + this.fullName + ", isUseCurrentLocation=" + this.isUseCurrentLocation + ", isEnableLocation=" + this.isEnableLocation + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeDouble(this.lat);
        parcel.writeDouble(this.lng);
        parcel.writeString(this.name);
        parcel.writeString(this.fullName);
        parcel.writeString(String.valueOf(this.isUseCurrentLocation));
        parcel.writeString(String.valueOf(this.isEnableLocation));
    }
}
