package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.fitness.Cadence;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CadenceWrapper {
    @DexIgnore
    public Integer average;
    @DexIgnore
    public Integer maximum;
    @DexIgnore
    public int unit;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CadenceWrapper(Cadence cadence) {
        this(cadence.getAverage(), cadence.getMaximum(), cadence.getUnit().ordinal());
        pq7.c(cadence, "cadence");
    }

    @DexIgnore
    public CadenceWrapper(Integer num, Integer num2, int i) {
        this.average = num;
        this.maximum = num2;
        this.unit = i;
    }

    @DexIgnore
    public static /* synthetic */ CadenceWrapper copy$default(CadenceWrapper cadenceWrapper, Integer num, Integer num2, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            num = cadenceWrapper.average;
        }
        if ((i2 & 2) != 0) {
            num2 = cadenceWrapper.maximum;
        }
        if ((i2 & 4) != 0) {
            i = cadenceWrapper.unit;
        }
        return cadenceWrapper.copy(num, num2, i);
    }

    @DexIgnore
    public final Integer component1() {
        return this.average;
    }

    @DexIgnore
    public final Integer component2() {
        return this.maximum;
    }

    @DexIgnore
    public final int component3() {
        return this.unit;
    }

    @DexIgnore
    public final CadenceWrapper copy(Integer num, Integer num2, int i) {
        return new CadenceWrapper(num, num2, i);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof CadenceWrapper) {
                CadenceWrapper cadenceWrapper = (CadenceWrapper) obj;
                if (!pq7.a(this.average, cadenceWrapper.average) || !pq7.a(this.maximum, cadenceWrapper.maximum) || this.unit != cadenceWrapper.unit) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Integer getAverage() {
        return this.average;
    }

    @DexIgnore
    public final Integer getMaximum() {
        return this.maximum;
    }

    @DexIgnore
    public final int getUnit() {
        return this.unit;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Integer num = this.average;
        int hashCode = num != null ? num.hashCode() : 0;
        Integer num2 = this.maximum;
        if (num2 != null) {
            i = num2.hashCode();
        }
        return (((hashCode * 31) + i) * 31) + this.unit;
    }

    @DexIgnore
    public final void setAverage(Integer num) {
        this.average = num;
    }

    @DexIgnore
    public final void setMaximum(Integer num) {
        this.maximum = num;
    }

    @DexIgnore
    public final void setUnit(int i) {
        this.unit = i;
    }

    @DexIgnore
    public String toString() {
        return "CadenceWrapper(average=" + this.average + ", maximum=" + this.maximum + ", unit=" + this.unit + ")";
    }
}
