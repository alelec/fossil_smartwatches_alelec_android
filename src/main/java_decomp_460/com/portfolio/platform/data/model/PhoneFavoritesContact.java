package com.portfolio.platform.data.model;

import com.fossil.rj4;
import com.google.gson.Gson;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = PhoneFavoritesContact.TABLE_NAME)
public class PhoneFavoritesContact {
    @DexIgnore
    public static /* final */ String COLUMN_PHONE_NUMBER; // = "phoneNumber";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "PhoneFavoritesContact";
    @DexIgnore
    @DatabaseField(columnName = COLUMN_PHONE_NUMBER, id = true)
    @rj4(COLUMN_PHONE_NUMBER)
    public String phoneNumber;

    @DexIgnore
    public PhoneFavoritesContact() {
    }

    @DexIgnore
    public PhoneFavoritesContact(String str) {
        this.phoneNumber = str;
    }

    @DexIgnore
    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    @DexIgnore
    public void setPhoneNumber(String str) {
        this.phoneNumber = str;
    }

    @DexIgnore
    public String toString() {
        return new Gson().t(this);
    }
}
