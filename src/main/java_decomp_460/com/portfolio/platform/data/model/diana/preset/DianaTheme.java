package com.portfolio.platform.data.model.diana.preset;

import com.fossil.pq7;
import com.fossil.rj4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaTheme {
    @DexIgnore
    @rj4("watchFaceId")
    public String backgroundId;
    @DexIgnore
    @rj4("ringStyleId")
    public String ringStyleId;
    @DexIgnore
    @rj4("themeId")
    public String themeId;
    @DexIgnore
    @rj4("wallpaperId")
    public String wallpaperId;

    @DexIgnore
    public DianaTheme() {
        this("watch_face_1", "ringStyle1", "dark", "wallpaper1");
    }

    @DexIgnore
    public DianaTheme(String str, String str2, String str3, String str4) {
        pq7.c(str, "backgroundId");
        pq7.c(str2, "ringStyleId");
        pq7.c(str3, "themeId");
        pq7.c(str4, "wallpaperId");
        this.backgroundId = str;
        this.ringStyleId = str2;
        this.themeId = str3;
        this.wallpaperId = str4;
    }

    @DexIgnore
    public static /* synthetic */ DianaTheme copy$default(DianaTheme dianaTheme, String str, String str2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = dianaTheme.backgroundId;
        }
        if ((i & 2) != 0) {
            str2 = dianaTheme.ringStyleId;
        }
        if ((i & 4) != 0) {
            str3 = dianaTheme.themeId;
        }
        if ((i & 8) != 0) {
            str4 = dianaTheme.wallpaperId;
        }
        return dianaTheme.copy(str, str2, str3, str4);
    }

    @DexIgnore
    public final String component1() {
        return this.backgroundId;
    }

    @DexIgnore
    public final String component2() {
        return this.ringStyleId;
    }

    @DexIgnore
    public final String component3() {
        return this.themeId;
    }

    @DexIgnore
    public final String component4() {
        return this.wallpaperId;
    }

    @DexIgnore
    public final DianaTheme copy(String str, String str2, String str3, String str4) {
        pq7.c(str, "backgroundId");
        pq7.c(str2, "ringStyleId");
        pq7.c(str3, "themeId");
        pq7.c(str4, "wallpaperId");
        return new DianaTheme(str, str2, str3, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DianaTheme) {
                DianaTheme dianaTheme = (DianaTheme) obj;
                if (!pq7.a(this.backgroundId, dianaTheme.backgroundId) || !pq7.a(this.ringStyleId, dianaTheme.ringStyleId) || !pq7.a(this.themeId, dianaTheme.themeId) || !pq7.a(this.wallpaperId, dianaTheme.wallpaperId)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getBackgroundId() {
        return this.backgroundId;
    }

    @DexIgnore
    public final String getRingStyleId() {
        return this.ringStyleId;
    }

    @DexIgnore
    public final String getThemeId() {
        return this.themeId;
    }

    @DexIgnore
    public final String getWallpaperId() {
        return this.wallpaperId;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.backgroundId;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.ringStyleId;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.themeId;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.wallpaperId;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public final void setBackgroundId(String str) {
        pq7.c(str, "<set-?>");
        this.backgroundId = str;
    }

    @DexIgnore
    public final void setRingStyleId(String str) {
        pq7.c(str, "<set-?>");
        this.ringStyleId = str;
    }

    @DexIgnore
    public final void setThemeId(String str) {
        pq7.c(str, "<set-?>");
        this.themeId = str;
    }

    @DexIgnore
    public final void setWallpaperId(String str) {
        pq7.c(str, "<set-?>");
        this.wallpaperId = str;
    }

    @DexIgnore
    public String toString() {
        return "DianaTheme(backgroundId=" + this.backgroundId + ", ringStyleId=" + this.ringStyleId + ", themeId=" + this.themeId + ", wallpaperId=" + this.wallpaperId + ")";
    }
}
