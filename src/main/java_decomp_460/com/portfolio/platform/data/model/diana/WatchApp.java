package com.portfolio.platform.data.model.diana;

import com.fossil.kq7;
import com.fossil.pj4;
import com.fossil.pq7;
import com.fossil.rj4;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchApp {
    @DexIgnore
    @pj4
    @rj4("categoryIds")
    public ArrayList<String> categories;
    @DexIgnore
    @pj4
    @rj4("createdAt")
    public String createdAt;
    @DexIgnore
    @pj4
    @rj4("englishDescription")
    public String description;
    @DexIgnore
    @pj4
    @rj4("description")
    public String descriptionKey;
    @DexIgnore
    @pj4
    public String icon;
    @DexIgnore
    @pj4
    @rj4("englishName")
    public String name;
    @DexIgnore
    @pj4
    @rj4("name")
    public String nameKey;
    @DexIgnore
    @pj4
    @rj4("updatedAt")
    public String updatedAt;
    @DexIgnore
    @pj4
    @rj4("id")
    public String watchappId;

    @DexIgnore
    public WatchApp(String str, String str2, String str3, String str4, String str5, ArrayList<String> arrayList, String str6, String str7, String str8) {
        pq7.c(str, "watchappId");
        pq7.c(str2, "name");
        pq7.c(str3, "nameKey");
        pq7.c(str4, "description");
        pq7.c(str5, "descriptionKey");
        pq7.c(arrayList, "categories");
        pq7.c(str7, "updatedAt");
        pq7.c(str8, "createdAt");
        this.watchappId = str;
        this.name = str2;
        this.nameKey = str3;
        this.description = str4;
        this.descriptionKey = str5;
        this.categories = arrayList;
        this.icon = str6;
        this.updatedAt = str7;
        this.createdAt = str8;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WatchApp(String str, String str2, String str3, String str4, String str5, ArrayList arrayList, String str6, String str7, String str8, int i, kq7 kq7) {
        this(str, str2, str3, str4, str5, arrayList, (i & 64) != 0 ? "" : str6, str7, str8);
    }

    @DexIgnore
    public static /* synthetic */ WatchApp copy$default(WatchApp watchApp, String str, String str2, String str3, String str4, String str5, ArrayList arrayList, String str6, String str7, String str8, int i, Object obj) {
        return watchApp.copy((i & 1) != 0 ? watchApp.watchappId : str, (i & 2) != 0 ? watchApp.name : str2, (i & 4) != 0 ? watchApp.nameKey : str3, (i & 8) != 0 ? watchApp.description : str4, (i & 16) != 0 ? watchApp.descriptionKey : str5, (i & 32) != 0 ? watchApp.categories : arrayList, (i & 64) != 0 ? watchApp.icon : str6, (i & 128) != 0 ? watchApp.updatedAt : str7, (i & 256) != 0 ? watchApp.createdAt : str8);
    }

    @DexIgnore
    public final String component1() {
        return this.watchappId;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final String component3() {
        return this.nameKey;
    }

    @DexIgnore
    public final String component4() {
        return this.description;
    }

    @DexIgnore
    public final String component5() {
        return this.descriptionKey;
    }

    @DexIgnore
    public final ArrayList<String> component6() {
        return this.categories;
    }

    @DexIgnore
    public final String component7() {
        return this.icon;
    }

    @DexIgnore
    public final String component8() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String component9() {
        return this.createdAt;
    }

    @DexIgnore
    public final WatchApp copy(String str, String str2, String str3, String str4, String str5, ArrayList<String> arrayList, String str6, String str7, String str8) {
        pq7.c(str, "watchappId");
        pq7.c(str2, "name");
        pq7.c(str3, "nameKey");
        pq7.c(str4, "description");
        pq7.c(str5, "descriptionKey");
        pq7.c(arrayList, "categories");
        pq7.c(str7, "updatedAt");
        pq7.c(str8, "createdAt");
        return new WatchApp(str, str2, str3, str4, str5, arrayList, str6, str7, str8);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WatchApp) {
                WatchApp watchApp = (WatchApp) obj;
                if (!pq7.a(this.watchappId, watchApp.watchappId) || !pq7.a(this.name, watchApp.name) || !pq7.a(this.nameKey, watchApp.nameKey) || !pq7.a(this.description, watchApp.description) || !pq7.a(this.descriptionKey, watchApp.descriptionKey) || !pq7.a(this.categories, watchApp.categories) || !pq7.a(this.icon, watchApp.icon) || !pq7.a(this.updatedAt, watchApp.updatedAt) || !pq7.a(this.createdAt, watchApp.createdAt)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ArrayList<String> getCategories() {
        return this.categories;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getDescription() {
        return this.description;
    }

    @DexIgnore
    public final String getDescriptionKey() {
        return this.descriptionKey;
    }

    @DexIgnore
    public final String getIcon() {
        return this.icon;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getNameKey() {
        return this.nameKey;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getWatchappId() {
        return this.watchappId;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.watchappId;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.name;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.nameKey;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.description;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.descriptionKey;
        int hashCode5 = str5 != null ? str5.hashCode() : 0;
        ArrayList<String> arrayList = this.categories;
        int hashCode6 = arrayList != null ? arrayList.hashCode() : 0;
        String str6 = this.icon;
        int hashCode7 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.updatedAt;
        int hashCode8 = str7 != null ? str7.hashCode() : 0;
        String str8 = this.createdAt;
        if (str8 != null) {
            i = str8.hashCode();
        }
        return (((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + i;
    }

    @DexIgnore
    public final void setCategories(ArrayList<String> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.categories = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        pq7.c(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDescription(String str) {
        pq7.c(str, "<set-?>");
        this.description = str;
    }

    @DexIgnore
    public final void setDescriptionKey(String str) {
        pq7.c(str, "<set-?>");
        this.descriptionKey = str;
    }

    @DexIgnore
    public final void setIcon(String str) {
        this.icon = str;
    }

    @DexIgnore
    public final void setName(String str) {
        pq7.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setNameKey(String str) {
        pq7.c(str, "<set-?>");
        this.nameKey = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        pq7.c(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setWatchappId(String str) {
        pq7.c(str, "<set-?>");
        this.watchappId = str;
    }

    @DexIgnore
    public String toString() {
        return "WatchApp(watchappId=" + this.watchappId + ", name=" + this.name + ", nameKey=" + this.nameKey + ", description=" + this.description + ", descriptionKey=" + this.descriptionKey + ", categories=" + this.categories + ", icon=" + this.icon + ", updatedAt=" + this.updatedAt + ", createdAt=" + this.createdAt + ")";
    }
}
