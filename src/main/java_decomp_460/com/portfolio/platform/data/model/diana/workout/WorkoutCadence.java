package com.portfolio.platform.data.model.diana.workout;

import com.fossil.ei5;
import com.fossil.pq7;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.fitnessdata.CadenceWrapper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutCadence {
    @DexIgnore
    public Integer average;
    @DexIgnore
    public Integer maximum;
    @DexIgnore
    public ei5 unit;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutCadence(CadenceWrapper cadenceWrapper) {
        this(cadenceWrapper.getAverage(), cadenceWrapper.getMaximum(), ei5.Companion.a(Integer.valueOf(cadenceWrapper.getUnit())));
        pq7.c(cadenceWrapper, "cadence");
    }

    @DexIgnore
    public WorkoutCadence(Integer num, Integer num2, ei5 ei5) {
        pq7.c(ei5, Constants.PROFILE_KEY_UNIT);
        this.average = num;
        this.maximum = num2;
        this.unit = ei5;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutCadence copy$default(WorkoutCadence workoutCadence, Integer num, Integer num2, ei5 ei5, int i, Object obj) {
        if ((i & 1) != 0) {
            num = workoutCadence.average;
        }
        if ((i & 2) != 0) {
            num2 = workoutCadence.maximum;
        }
        if ((i & 4) != 0) {
            ei5 = workoutCadence.unit;
        }
        return workoutCadence.copy(num, num2, ei5);
    }

    @DexIgnore
    public final Integer component1() {
        return this.average;
    }

    @DexIgnore
    public final Integer component2() {
        return this.maximum;
    }

    @DexIgnore
    public final ei5 component3() {
        return this.unit;
    }

    @DexIgnore
    public final WorkoutCadence copy(Integer num, Integer num2, ei5 ei5) {
        pq7.c(ei5, Constants.PROFILE_KEY_UNIT);
        return new WorkoutCadence(num, num2, ei5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutCadence) {
                WorkoutCadence workoutCadence = (WorkoutCadence) obj;
                if (!pq7.a(this.average, workoutCadence.average) || !pq7.a(this.maximum, workoutCadence.maximum) || !pq7.a(this.unit, workoutCadence.unit)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Integer getAverage() {
        return this.average;
    }

    @DexIgnore
    public final Integer getMaximum() {
        return this.maximum;
    }

    @DexIgnore
    public final ei5 getUnit() {
        return this.unit;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Integer num = this.average;
        int hashCode = num != null ? num.hashCode() : 0;
        Integer num2 = this.maximum;
        int hashCode2 = num2 != null ? num2.hashCode() : 0;
        ei5 ei5 = this.unit;
        if (ei5 != null) {
            i = ei5.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public final void setAverage(Integer num) {
        this.average = num;
    }

    @DexIgnore
    public final void setMaximum(Integer num) {
        this.maximum = num;
    }

    @DexIgnore
    public final void setUnit(ei5 ei5) {
        pq7.c(ei5, "<set-?>");
        this.unit = ei5;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutCadence(average=" + this.average + ", maximum=" + this.maximum + ", unit=" + this.unit + ")";
    }
}
