package com.portfolio.platform.data.model.diana.workout;

import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rj4;
import java.io.Serializable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutScreenshotWrapper implements Serializable {
    @DexIgnore
    @rj4("height")
    public /* final */ int height;
    @DexIgnore
    @rj4("listCoor")
    public List<WorkoutRouterGpsWrapper> listCoor;
    @DexIgnore
    public int pinType;
    @DexIgnore
    @rj4("resetState")
    public boolean resetState;
    @DexIgnore
    @rj4("width")
    public int width;
    @DexIgnore
    @rj4("workoutId")
    public String workoutId;

    @DexIgnore
    public WorkoutScreenshotWrapper(String str, int i, int i2, boolean z, List<WorkoutRouterGpsWrapper> list) {
        pq7.c(str, "workoutId");
        this.workoutId = str;
        this.width = i;
        this.height = i2;
        this.resetState = z;
        this.listCoor = list;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WorkoutScreenshotWrapper(String str, int i, int i2, boolean z, List list, int i3, kq7 kq7) {
        this((i3 & 1) != 0 ? "0" : str, i, i2, (i3 & 8) != 0 ? true : z, list);
    }

    @DexIgnore
    public static /* synthetic */ WorkoutScreenshotWrapper copy$default(WorkoutScreenshotWrapper workoutScreenshotWrapper, String str, int i, int i2, boolean z, List list, int i3, Object obj) {
        return workoutScreenshotWrapper.copy((i3 & 1) != 0 ? workoutScreenshotWrapper.workoutId : str, (i3 & 2) != 0 ? workoutScreenshotWrapper.width : i, (i3 & 4) != 0 ? workoutScreenshotWrapper.height : i2, (i3 & 8) != 0 ? workoutScreenshotWrapper.resetState : z, (i3 & 16) != 0 ? workoutScreenshotWrapper.listCoor : list);
    }

    @DexIgnore
    public final String component1() {
        return this.workoutId;
    }

    @DexIgnore
    public final int component2() {
        return this.width;
    }

    @DexIgnore
    public final int component3() {
        return this.height;
    }

    @DexIgnore
    public final boolean component4() {
        return this.resetState;
    }

    @DexIgnore
    public final List<WorkoutRouterGpsWrapper> component5() {
        return this.listCoor;
    }

    @DexIgnore
    public final WorkoutScreenshotWrapper copy(String str, int i, int i2, boolean z, List<WorkoutRouterGpsWrapper> list) {
        pq7.c(str, "workoutId");
        return new WorkoutScreenshotWrapper(str, i, i2, z, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutScreenshotWrapper) {
                WorkoutScreenshotWrapper workoutScreenshotWrapper = (WorkoutScreenshotWrapper) obj;
                if (!(pq7.a(this.workoutId, workoutScreenshotWrapper.workoutId) && this.width == workoutScreenshotWrapper.width && this.height == workoutScreenshotWrapper.height && this.resetState == workoutScreenshotWrapper.resetState && pq7.a(this.listCoor, workoutScreenshotWrapper.listCoor))) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getHeight() {
        return this.height;
    }

    @DexIgnore
    public final List<WorkoutRouterGpsWrapper> getListCoor() {
        return this.listCoor;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final boolean getResetState() {
        return this.resetState;
    }

    @DexIgnore
    public final int getWidth() {
        return this.width;
    }

    @DexIgnore
    public final String getWorkoutId() {
        return this.workoutId;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.workoutId;
        int hashCode = str != null ? str.hashCode() : 0;
        int i2 = this.width;
        int i3 = this.height;
        boolean z = this.resetState;
        if (z) {
            z = true;
        }
        List<WorkoutRouterGpsWrapper> list = this.listCoor;
        if (list != null) {
            i = list.hashCode();
        }
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = z ? 1 : 0;
        return (((((((hashCode * 31) + i2) * 31) + i3) * 31) + i4) * 31) + i;
    }

    @DexIgnore
    public final void setListCoor(List<WorkoutRouterGpsWrapper> list) {
        this.listCoor = list;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setResetState(boolean z) {
        this.resetState = z;
    }

    @DexIgnore
    public final void setWidth(int i) {
        this.width = i;
    }

    @DexIgnore
    public final void setWorkoutId(String str) {
        pq7.c(str, "<set-?>");
        this.workoutId = str;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutScreenshotWrapper(workoutId=" + this.workoutId + ", width=" + this.width + ", height=" + this.height + ", resetState=" + this.resetState + ", listCoor=" + this.listCoor + ")";
    }
}
