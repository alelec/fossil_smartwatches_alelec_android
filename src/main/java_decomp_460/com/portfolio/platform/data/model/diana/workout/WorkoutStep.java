package com.portfolio.platform.data.model.diana.workout;

import com.fossil.pq7;
import com.portfolio.platform.data.model.fitnessdata.StepWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutStep {
    @DexIgnore
    public int resolution;
    @DexIgnore
    public int total;
    @DexIgnore
    public List<Short> values;

    @DexIgnore
    public WorkoutStep(int i, List<Short> list, int i2) {
        pq7.c(list, "values");
        this.resolution = i;
        this.values = list;
        this.total = i2;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutStep(StepWrapper stepWrapper) {
        this(stepWrapper.getResolutionInSecond(), stepWrapper.getValues(), stepWrapper.getTotal());
        pq7.c(stepWrapper, "step");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.model.diana.workout.WorkoutStep */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ WorkoutStep copy$default(WorkoutStep workoutStep, int i, List list, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = workoutStep.resolution;
        }
        if ((i3 & 2) != 0) {
            list = workoutStep.values;
        }
        if ((i3 & 4) != 0) {
            i2 = workoutStep.total;
        }
        return workoutStep.copy(i, list, i2);
    }

    @DexIgnore
    public final int component1() {
        return this.resolution;
    }

    @DexIgnore
    public final List<Short> component2() {
        return this.values;
    }

    @DexIgnore
    public final int component3() {
        return this.total;
    }

    @DexIgnore
    public final WorkoutStep copy(int i, List<Short> list, int i2) {
        pq7.c(list, "values");
        return new WorkoutStep(i, list, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutStep) {
                WorkoutStep workoutStep = (WorkoutStep) obj;
                if (!(this.resolution == workoutStep.resolution && pq7.a(this.values, workoutStep.values) && this.total == workoutStep.total)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getResolution() {
        return this.resolution;
    }

    @DexIgnore
    public final int getTotal() {
        return this.total;
    }

    @DexIgnore
    public final List<Short> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.resolution;
        List<Short> list = this.values;
        return (((list != null ? list.hashCode() : 0) + (i * 31)) * 31) + this.total;
    }

    @DexIgnore
    public final void setResolution(int i) {
        this.resolution = i;
    }

    @DexIgnore
    public final void setTotal(int i) {
        this.total = i;
    }

    @DexIgnore
    public final void setValues(List<Short> list) {
        pq7.c(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutStep(resolution=" + this.resolution + ", values=" + this.values + ", total=" + this.total + ")";
    }
}
