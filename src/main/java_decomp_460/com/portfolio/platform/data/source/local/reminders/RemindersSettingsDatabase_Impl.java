package com.portfolio.platform.data.source.local.reminders;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemindersSettingsDatabase_Impl extends RemindersSettingsDatabase {
    @DexIgnore
    public volatile InactivityNudgeTimeDao _inactivityNudgeTimeDao;
    @DexIgnore
    public volatile RemindTimeDao _remindTimeDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `inactivityNudgeTimeModel` (`nudgeTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, `nudgeTimeType` INTEGER NOT NULL, PRIMARY KEY(`nudgeTimeName`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `remindTimeModel` (`remindTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, PRIMARY KEY(`remindTimeName`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '0ee434ba350bbb753b81bda456b5c107')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `inactivityNudgeTimeModel`");
            lx0.execSQL("DROP TABLE IF EXISTS `remindTimeModel`");
            if (RemindersSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = RemindersSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) RemindersSettingsDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (RemindersSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = RemindersSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) RemindersSettingsDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            RemindersSettingsDatabase_Impl.this.mDatabase = lx0;
            RemindersSettingsDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (RemindersSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = RemindersSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) RemindersSettingsDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("nudgeTimeName", new ix0.a("nudgeTimeName", "TEXT", true, 1, null, 1));
            hashMap.put("minutes", new ix0.a("minutes", "INTEGER", true, 0, null, 1));
            hashMap.put("nudgeTimeType", new ix0.a("nudgeTimeType", "INTEGER", true, 0, null, 1));
            ix0 ix0 = new ix0("inactivityNudgeTimeModel", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "inactivityNudgeTimeModel");
            if (!ix0.equals(a2)) {
                return new sw0.b(false, "inactivityNudgeTimeModel(com.portfolio.platform.data.InactivityNudgeTimeModel).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(2);
            hashMap2.put("remindTimeName", new ix0.a("remindTimeName", "TEXT", true, 1, null, 1));
            hashMap2.put("minutes", new ix0.a("minutes", "INTEGER", true, 0, null, 1));
            ix0 ix02 = new ix0("remindTimeModel", hashMap2, new HashSet(0), new HashSet(0));
            ix0 a3 = ix0.a(lx0, "remindTimeModel");
            if (ix02.equals(a3)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "remindTimeModel(com.portfolio.platform.data.RemindTimeModel).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `inactivityNudgeTimeModel`");
            writableDatabase.execSQL("DELETE FROM `remindTimeModel`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "inactivityNudgeTimeModel", "remindTimeModel");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(1), "0ee434ba350bbb753b81bda456b5c107", "7e66671f5f316f81e6558b840c854b68");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase
    public InactivityNudgeTimeDao getInactivityNudgeTimeDao() {
        InactivityNudgeTimeDao inactivityNudgeTimeDao;
        if (this._inactivityNudgeTimeDao != null) {
            return this._inactivityNudgeTimeDao;
        }
        synchronized (this) {
            if (this._inactivityNudgeTimeDao == null) {
                this._inactivityNudgeTimeDao = new InactivityNudgeTimeDao_Impl(this);
            }
            inactivityNudgeTimeDao = this._inactivityNudgeTimeDao;
        }
        return inactivityNudgeTimeDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase
    public RemindTimeDao getRemindTimeDao() {
        RemindTimeDao remindTimeDao;
        if (this._remindTimeDao != null) {
            return this._remindTimeDao;
        }
        synchronized (this) {
            if (this._remindTimeDao == null) {
                this._remindTimeDao = new RemindTimeDao_Impl(this);
            }
            remindTimeDao = this._remindTimeDao;
        }
        return remindTimeDao;
    }
}
