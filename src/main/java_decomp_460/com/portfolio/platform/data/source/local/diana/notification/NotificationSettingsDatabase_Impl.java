package com.portfolio.platform.data.source.local.diana.notification;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationSettingsDatabase_Impl extends NotificationSettingsDatabase {
    @DexIgnore
    public volatile NotificationSettingsDao _notificationSettingsDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `notificationSettings` (`settingsName` TEXT NOT NULL, `settingsType` INTEGER NOT NULL, `isCall` INTEGER NOT NULL, PRIMARY KEY(`settingsName`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '563d2dcf0170d812960699bb88bdb35d')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `notificationSettings`");
            if (NotificationSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = NotificationSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) NotificationSettingsDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (NotificationSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = NotificationSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) NotificationSettingsDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            NotificationSettingsDatabase_Impl.this.mDatabase = lx0;
            NotificationSettingsDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (NotificationSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = NotificationSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) NotificationSettingsDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("settingsName", new ix0.a("settingsName", "TEXT", true, 1, null, 1));
            hashMap.put("settingsType", new ix0.a("settingsType", "INTEGER", true, 0, null, 1));
            hashMap.put("isCall", new ix0.a("isCall", "INTEGER", true, 0, null, 1));
            ix0 ix0 = new ix0("notificationSettings", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "notificationSettings");
            if (ix0.equals(a2)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "notificationSettings(com.portfolio.platform.data.model.NotificationSettingsModel).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `notificationSettings`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "notificationSettings");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(1), "563d2dcf0170d812960699bb88bdb35d", "d29e13e7c48fbcf2227b41f4f512e273");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase
    public NotificationSettingsDao getNotificationSettingsDao() {
        NotificationSettingsDao notificationSettingsDao;
        if (this._notificationSettingsDao != null) {
            return this._notificationSettingsDao;
        }
        synchronized (this) {
            if (this._notificationSettingsDao == null) {
                this._notificationSettingsDao = new NotificationSettingsDao_Impl(this);
            }
            notificationSettingsDao = this._notificationSettingsDao;
        }
        return notificationSettingsDao;
    }
}
