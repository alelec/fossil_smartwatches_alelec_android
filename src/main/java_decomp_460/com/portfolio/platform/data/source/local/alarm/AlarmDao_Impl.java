package com.portfolio.platform.data.source.local.alarm;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nz4;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.fossil.xw0;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDao_Impl implements AlarmDao {
    @DexIgnore
    public /* final */ nz4 __alarmConverter; // = new nz4();
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<Alarm> __insertionAdapterOfAlarm;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfRemoveAlarm;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<Alarm> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, Alarm alarm) {
            if (alarm.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, alarm.getId());
            }
            if (alarm.getUri() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, alarm.getUri());
            }
            if (alarm.getTitle() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, alarm.getTitle());
            }
            if (alarm.getMessage() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, alarm.getMessage());
            }
            px0.bindLong(5, (long) alarm.getHour());
            px0.bindLong(6, (long) alarm.getMinute());
            String a2 = AlarmDao_Impl.this.__alarmConverter.a(alarm.getDays());
            if (a2 == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, a2);
            }
            px0.bindLong(8, alarm.isActive() ? 1 : 0);
            px0.bindLong(9, alarm.isRepeated() ? 1 : 0);
            if (alarm.getCreatedAt() == null) {
                px0.bindNull(10);
            } else {
                px0.bindString(10, alarm.getCreatedAt());
            }
            if (alarm.getUpdatedAt() == null) {
                px0.bindNull(11);
            } else {
                px0.bindString(11, alarm.getUpdatedAt());
            }
            px0.bindLong(12, (long) alarm.getPinType());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `alarm` (`id`,`uri`,`title`,`message`,`hour`,`minute`,`days`,`isActive`,`isRepeated`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM alarm WHERE uri =?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM alarm";
        }
    }

    @DexIgnore
    public AlarmDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfAlarm = new Anon1(qw0);
        this.__preparedStmtOfRemoveAlarm = new Anon2(qw0);
        this.__preparedStmtOfCleanUp = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getActiveAlarms() {
        tw0 f = tw0.f("SELECT *FROM alarm WHERE isActive = 1 and pinType <> 3", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "uri");
            int c3 = dx0.c(b, "title");
            int c4 = dx0.c(b, "message");
            int c5 = dx0.c(b, AppFilter.COLUMN_HOUR);
            int c6 = dx0.c(b, MFSleepGoal.COLUMN_MINUTE);
            int c7 = dx0.c(b, Alarm.COLUMN_DAYS);
            int c8 = dx0.c(b, "isActive");
            int c9 = dx0.c(b, "isRepeated");
            int c10 = dx0.c(b, "createdAt");
            int c11 = dx0.c(b, "updatedAt");
            int c12 = dx0.c(b, "pinType");
            try {
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new Alarm(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getInt(c5), b.getInt(c6), this.__alarmConverter.b(b.getString(c7)), b.getInt(c8) != 0, b.getInt(c9) != 0, b.getString(c10), b.getString(c11), b.getInt(c12)));
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public Alarm getAlarmWithUri(String str) {
        tw0 f = tw0.f("SELECT * FROM alarm WHERE uri =?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Alarm alarm = null;
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "uri");
            int c3 = dx0.c(b, "title");
            int c4 = dx0.c(b, "message");
            int c5 = dx0.c(b, AppFilter.COLUMN_HOUR);
            int c6 = dx0.c(b, MFSleepGoal.COLUMN_MINUTE);
            int c7 = dx0.c(b, Alarm.COLUMN_DAYS);
            int c8 = dx0.c(b, "isActive");
            int c9 = dx0.c(b, "isRepeated");
            int c10 = dx0.c(b, "createdAt");
            int c11 = dx0.c(b, "updatedAt");
            int c12 = dx0.c(b, "pinType");
            if (b.moveToFirst()) {
                alarm = new Alarm(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getInt(c5), b.getInt(c6), this.__alarmConverter.b(b.getString(c7)), b.getInt(c8) != 0, b.getInt(c9) != 0, b.getString(c10), b.getString(c11), b.getInt(c12));
            }
            return alarm;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getAlarms() {
        tw0 f = tw0.f("SELECT * FROM alarm", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "uri");
            int c3 = dx0.c(b, "title");
            int c4 = dx0.c(b, "message");
            int c5 = dx0.c(b, AppFilter.COLUMN_HOUR);
            int c6 = dx0.c(b, MFSleepGoal.COLUMN_MINUTE);
            int c7 = dx0.c(b, Alarm.COLUMN_DAYS);
            int c8 = dx0.c(b, "isActive");
            int c9 = dx0.c(b, "isRepeated");
            int c10 = dx0.c(b, "createdAt");
            int c11 = dx0.c(b, "updatedAt");
            int c12 = dx0.c(b, "pinType");
            try {
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new Alarm(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getInt(c5), b.getInt(c6), this.__alarmConverter.b(b.getString(c7)), b.getInt(c8) != 0, b.getInt(c9) != 0, b.getString(c10), b.getString(c11), b.getInt(c12)));
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getAlarmsIgnoreDeleted() {
        tw0 f = tw0.f("SELECT * FROM alarm WHERE pinType <> 3 ORDER BY isActive DESC, hour, minute ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "uri");
            int c3 = dx0.c(b, "title");
            int c4 = dx0.c(b, "message");
            int c5 = dx0.c(b, AppFilter.COLUMN_HOUR);
            int c6 = dx0.c(b, MFSleepGoal.COLUMN_MINUTE);
            int c7 = dx0.c(b, Alarm.COLUMN_DAYS);
            int c8 = dx0.c(b, "isActive");
            int c9 = dx0.c(b, "isRepeated");
            int c10 = dx0.c(b, "createdAt");
            int c11 = dx0.c(b, "updatedAt");
            int c12 = dx0.c(b, "pinType");
            try {
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new Alarm(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getInt(c5), b.getInt(c6), this.__alarmConverter.b(b.getString(c7)), b.getInt(c8) != 0, b.getInt(c9) != 0, b.getString(c10), b.getString(c11), b.getInt(c12)));
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public Alarm getInComingActiveAlarm(int i) {
        tw0 f = tw0.f("SELECT * FROM alarm WHERE isActive = 1 and minute + hour * 60 > ? ORDER BY hour, minute ASC LIMIT 1", 1);
        f.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Alarm alarm = null;
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "uri");
            int c3 = dx0.c(b, "title");
            int c4 = dx0.c(b, "message");
            int c5 = dx0.c(b, AppFilter.COLUMN_HOUR);
            int c6 = dx0.c(b, MFSleepGoal.COLUMN_MINUTE);
            int c7 = dx0.c(b, Alarm.COLUMN_DAYS);
            int c8 = dx0.c(b, "isActive");
            int c9 = dx0.c(b, "isRepeated");
            int c10 = dx0.c(b, "createdAt");
            int c11 = dx0.c(b, "updatedAt");
            int c12 = dx0.c(b, "pinType");
            if (b.moveToFirst()) {
                alarm = new Alarm(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getInt(c5), b.getInt(c6), this.__alarmConverter.b(b.getString(c7)), b.getInt(c8) != 0, b.getInt(c9) != 0, b.getString(c10), b.getString(c11), b.getInt(c12));
            }
            return alarm;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getInComingActiveAlarms() {
        tw0 f = tw0.f("SELECT * FROM alarm WHERE isActive = 1 and pinType <> 3 ORDER BY hour, minute ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "uri");
            int c3 = dx0.c(b, "title");
            int c4 = dx0.c(b, "message");
            int c5 = dx0.c(b, AppFilter.COLUMN_HOUR);
            int c6 = dx0.c(b, MFSleepGoal.COLUMN_MINUTE);
            int c7 = dx0.c(b, Alarm.COLUMN_DAYS);
            int c8 = dx0.c(b, "isActive");
            int c9 = dx0.c(b, "isRepeated");
            int c10 = dx0.c(b, "createdAt");
            int c11 = dx0.c(b, "updatedAt");
            int c12 = dx0.c(b, "pinType");
            try {
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new Alarm(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getInt(c5), b.getInt(c6), this.__alarmConverter.b(b.getString(c7)), b.getInt(c8) != 0, b.getInt(c9) != 0, b.getString(c10), b.getString(c11), b.getInt(c12)));
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getPendingAlarms() {
        tw0 f = tw0.f("SELECT*FROM alarm where pinType <> 0 ORDER BY isActive DESC, hour, minute ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "uri");
            int c3 = dx0.c(b, "title");
            int c4 = dx0.c(b, "message");
            int c5 = dx0.c(b, AppFilter.COLUMN_HOUR);
            int c6 = dx0.c(b, MFSleepGoal.COLUMN_MINUTE);
            int c7 = dx0.c(b, Alarm.COLUMN_DAYS);
            int c8 = dx0.c(b, "isActive");
            int c9 = dx0.c(b, "isRepeated");
            int c10 = dx0.c(b, "createdAt");
            int c11 = dx0.c(b, "updatedAt");
            int c12 = dx0.c(b, "pinType");
            try {
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new Alarm(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getInt(c5), b.getInt(c6), this.__alarmConverter.b(b.getString(c7)), b.getInt(c8) != 0, b.getInt(c9) != 0, b.getString(c10), b.getString(c11), b.getInt(c12)));
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public long insertAlarm(Alarm alarm) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfAlarm.insertAndReturnId(alarm);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public Long[] insertAlarms(List<Alarm> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.__insertionAdapterOfAlarm.insertAndReturnIdsArrayBox(list);
            this.__db.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public int removeAlarm(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfRemoveAlarm.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAlarm.release(acquire);
        }
    }
}
