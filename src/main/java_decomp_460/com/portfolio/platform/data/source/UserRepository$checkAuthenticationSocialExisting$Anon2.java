package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.UserRepository$checkAuthenticationSocialExisting$2", f = "UserRepository.kt", l = {273}, m = "invokeSuspend")
public final class UserRepository$checkAuthenticationSocialExisting$Anon2 extends ko7 implements vp7<iv7, qn7<? super iq5<Boolean>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $service;
    @DexIgnore
    public /* final */ /* synthetic */ String $token;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRepository$checkAuthenticationSocialExisting$Anon2(UserRepository userRepository, String str, String str2, qn7 qn7) {
        super(2, qn7);
        this.this$0 = userRepository;
        this.$service = str;
        this.$token = str2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        UserRepository$checkAuthenticationSocialExisting$Anon2 userRepository$checkAuthenticationSocialExisting$Anon2 = new UserRepository$checkAuthenticationSocialExisting$Anon2(this.this$0, this.$service, this.$token, qn7);
        userRepository$checkAuthenticationSocialExisting$Anon2.p$ = (iv7) obj;
        return userRepository$checkAuthenticationSocialExisting$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<Boolean>> qn7) {
        return ((UserRepository$checkAuthenticationSocialExisting$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            UserRemoteDataSource userRemoteDataSource = this.this$0.mUserRemoteDataSource;
            String str = this.$service;
            String str2 = this.$token;
            this.L$0 = iv7;
            this.label = 1;
            Object checkAuthenticationSocialExisting = userRemoteDataSource.checkAuthenticationSocialExisting(str, str2, this);
            return checkAuthenticationSocialExisting == d ? d : checkAuthenticationSocialExisting;
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
