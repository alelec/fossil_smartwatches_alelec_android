package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory implements Factory<HybridCustomizeDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static HybridCustomizeDatabase provideHybridCustomizeDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        HybridCustomizeDatabase provideHybridCustomizeDatabase = portfolioDatabaseModule.provideHybridCustomizeDatabase(portfolioApp);
        lk7.c(provideHybridCustomizeDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideHybridCustomizeDatabase;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public HybridCustomizeDatabase get() {
        return provideHybridCustomizeDatabase(this.module, this.appProvider.get());
    }
}
