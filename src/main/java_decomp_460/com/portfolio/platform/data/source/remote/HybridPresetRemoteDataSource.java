package com.portfolio.platform.data.source.remote;

import com.fossil.kq7;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "HybridPresetRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public HybridPresetRemoteDataSource(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deletePreset(com.portfolio.platform.data.model.room.microapp.HybridPreset r9, com.fossil.qn7<? super com.fossil.iq5<java.lang.Void>> r10) {
        /*
            r8 = this;
            r6 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$Anon1
            if (r0 == 0) goto L_0x0043
            r0 = r10
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$Anon1 r0 = (com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x0043
            int r1 = r1 + r4
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r4 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0052
            if (r0 != r6) goto L_0x004a
            java.lang.Object r0 = r1.L$3
            com.fossil.bj4 r0 = (com.fossil.bj4) r0
            java.lang.Object r0 = r1.L$2
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x0035:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0082
            com.fossil.kq5 r0 = new com.fossil.kq5
            r1 = 0
            r2 = 2
            r0.<init>(r3, r1, r2, r3)
        L_0x0042:
            return r0
        L_0x0043:
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$Anon1 r0 = new com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$Anon1
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x004a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0052:
            com.fossil.el7.b(r2)
            com.fossil.gj4 r0 = new com.fossil.gj4
            r0.<init>()
            com.fossil.bj4 r2 = new com.fossil.bj4
            r2.<init>()
            java.lang.String r5 = r9.getId()
            r2.l(r5)
            java.lang.String r5 = "_ids"
            r0.k(r5, r2)
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$response$Anon1 r5 = new com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$response$Anon1
            r5.<init>(r8, r0, r3)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r0
            r1.L$3 = r2
            r1.label = r6
            java.lang.Object r0 = com.fossil.jq5.d(r5, r1)
            if (r0 != r4) goto L_0x0035
            r0 = r4
            goto L_0x0042
        L_0x0082:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x009c
            r2 = r0
            com.fossil.hq5 r2 = (com.fossil.hq5) r2
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r2.a()
            com.portfolio.platform.data.model.ServerError r2 = r2.c()
            r6 = 28
            r4 = r3
            r5 = r3
            r7 = r3
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0042
        L_0x009c:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource.deletePreset(com.portfolio.platform.data.model.room.microapp.HybridPreset, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadHybridPresetList(java.lang.String r9, com.fossil.qn7<? super com.fossil.iq5<java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridPreset>>> r10) {
        /*
            r8 = this;
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1
            if (r0 == 0) goto L_0x0069
            r0 = r10
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1 r0 = (com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x0069
            int r1 = r1 + r4
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r4 = r1.label
            if (r4 == 0) goto L_0x0078
            if (r4 != r5) goto L_0x0070
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource r1 = (com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r9 = r0
        L_0x002e:
            r0 = r1
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x009c
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            boolean r1 = r0.b()
            if (r1 != 0) goto L_0x0091
            java.lang.Object r1 = r0.a()
            if (r1 == 0) goto L_0x008d
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            java.util.List r1 = r1.get_items()
            java.util.Iterator r3 = r1.iterator()
        L_0x0052:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x0091
            java.lang.Object r1 = r3.next()
            com.portfolio.platform.data.model.room.microapp.HybridPreset r1 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r1
            r4 = 0
            r1.setPinType(r4)
            r1.setSerialNumber(r9)
            r2.add(r1)
            goto L_0x0052
        L_0x0069:
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1 r0 = new com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0070:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0078:
            com.fossil.el7.b(r2)
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1 r2 = new com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1
            r2.<init>(r8, r9, r3)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.label = r5
            java.lang.Object r1 = com.fossil.jq5.d(r2, r1)
            if (r1 != r0) goto L_0x002e
        L_0x008c:
            return r0
        L_0x008d:
            com.fossil.pq7.i()
            throw r3
        L_0x0091:
            com.fossil.kq5 r1 = new com.fossil.kq5
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
            goto L_0x008c
        L_0x009c:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00b6
            r2 = r0
            com.fossil.hq5 r2 = (com.fossil.hq5) r2
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r2.a()
            com.portfolio.platform.data.model.ServerError r2 = r2.c()
            r6 = 28
            r4 = r3
            r5 = r3
            r7 = r3
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x008c
        L_0x00b6:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource.downloadHybridPresetList(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadHybridRecommendPresetList(java.lang.String r9, com.fossil.qn7<? super com.fossil.iq5<java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset>>> r10) {
        /*
        // Method dump skipped, instructions count: 315
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource.downloadHybridRecommendPresetList(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object replaceHybridPresetList(java.util.List<com.portfolio.platform.data.model.room.microapp.HybridPreset> r11, com.fossil.qn7<? super com.fossil.iq5<java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridPreset>>> r12) {
        /*
        // Method dump skipped, instructions count: 333
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource.replaceHybridPresetList(java.util.List, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object upsertHybridPresetList(java.util.List<com.portfolio.platform.data.model.room.microapp.HybridPreset> r11, com.fossil.qn7<? super com.fossil.iq5<java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridPreset>>> r12) {
        /*
        // Method dump skipped, instructions count: 333
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource.upsertHybridPresetList(java.util.List, com.fossil.qn7):java.lang.Object");
    }
}
