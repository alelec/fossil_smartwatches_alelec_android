package com.portfolio.platform.data.source.local.alarm;

import android.database.Cursor;
import com.fossil.ax0;
import com.fossil.lx0;
import com.fossil.pq7;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDatabase$Companion$migrating3Or4To5$Anon1 extends ax0 {
    @DexIgnore
    public /* final */ /* synthetic */ int $previous;
    @DexIgnore
    public /* final */ /* synthetic */ String $userId;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmDatabase$Companion$migrating3Or4To5$Anon1(int i, String str, int i2, int i3) {
        super(i2, i3);
        this.$previous = i;
        this.$userId = str;
    }

    @DexIgnore
    @Override // com.fossil.ax0
    public void migrate(lx0 lx0) {
        pq7.c(lx0, "database");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = AlarmDatabase.TAG;
        local.d(str, "migrating3Or4To5 - previous: " + this.$previous);
        lx0.beginTransaction();
        try {
            lx0.execSQL("CREATE TABLE alarm_temp (id TEXT, uri TEXT PRIMARY KEY NOT NULL, title TEXT NOT NULL, hour INTEGER NOT NULL, minute INTEGER NOT NULL, days TEXT, isActive INTEGER NOT NULL, isRepeated INTEGER NOT NULL, createdAt TEXT, updatedAt TEXT NOT NULL, pinType INTEGER NOT NULL DEFAULT 1)");
            if (this.$previous == 4) {
                lx0.execSQL("INSERT INTO alarm_temp (id, uri, title, hour, minute, days, isActive, isRepeated, createdAt, updatedAt, pinType) SELECT objectId, uri, alarmTitle, 0, alarmMinute, days, isActiveAlarm, isRepeat, createdAt, updatedAt, pinType FROM alarm");
            } else {
                lx0.execSQL("INSERT INTO alarm_temp (id, uri, title, hour, minute, days, isActive, isRepeated, createdAt, updatedAt, pinType) SELECT objectId, uri, alarmTitle, 0, alarmMinute, days, isActiveAlarm, isRepeat, createdAt, updatedAt FROM alarm");
            }
            lx0.execSQL("UPDATE alarm_temp SET hour = minute/60, minute = minute%60");
            lx0.execSQL("DROP TABLE alarm");
            lx0.execSQL("ALTER TABLE alarm_temp RENAME TO alarm");
            Cursor query = lx0.query("SELECT*FROM alarm WHERE instr(uri, 'uri:') > 0 or instr(uri, ':') = 0");
            query.moveToFirst();
            while (true) {
                pq7.b(query, "cursor");
                if (query.isAfterLast()) {
                    break;
                }
                StringBuilder sb = new StringBuilder();
                sb.append(this.$userId);
                sb.append(':');
                Calendar instance = Calendar.getInstance();
                pq7.b(instance, "Calendar.getInstance()");
                sb.append(instance.getTimeInMillis());
                String sb2 = sb.toString();
                String string = query.getString(query.getColumnIndex("title"));
                int i = query.getInt(query.getColumnIndex(AppFilter.COLUMN_HOUR));
                int i2 = query.getInt(query.getColumnIndex(MFSleepGoal.COLUMN_MINUTE));
                String string2 = query.getString(query.getColumnIndex(Alarm.COLUMN_DAYS));
                int i3 = query.getInt(query.getColumnIndex("isActive"));
                int i4 = query.getInt(query.getColumnIndex("isRepeated"));
                String string3 = query.getString(query.getColumnIndex("createdAt"));
                String string4 = query.getString(query.getColumnIndex("updatedAt"));
                lx0.execSQL("INSERT INTO alarm(id, uri, title, hour, minute, days, isActive, isRepeated, createdAt, updatedAt, pinType) VALUES (null, '" + sb2 + "', '" + string + "', " + i + ", " + i2 + ", '" + string2 + "', " + i3 + ", " + i4 + ", '" + string3 + "', '" + string4 + "', 1)");
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = AlarmDatabase.TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("migrating3Or4To5 uri: ");
                sb3.append(sb2);
                sb3.append(" - title: ");
                sb3.append(string);
                local2.d(str2, sb3.toString());
                query.moveToNext();
            }
            query.close();
            lx0.execSQL("UPDATE alarm SET pinType = 3 WHERE instr(uri, 'uri:') > 0 or instr(uri, ':') = 0");
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().e(AlarmDatabase.TAG, "migration is failed!");
            lx0.execSQL("DROP TABLE IF EXISTS alarm_temp");
            lx0.execSQL("DROP TABLE IF EXISTS alarm");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS  alarm (id TEXT, uri TEXT PRIMARY KEY NOT NULL, title TEXT NOT NULL, hour INTEGER NOT NULL, minute INTEGER NOT NULL, days TEXT, isActive INTEGER NOT NULL, isRepeated INTEGER NOT NULL, createdAt TEXT, updatedAt TEXT NOT NULL, pinType INTEGER NOT NULL DEFAULT 1)");
        }
        lx0.setTransactionSuccessful();
        lx0.endTransaction();
    }
}
