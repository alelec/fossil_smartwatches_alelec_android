package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bw7;
import com.fossil.eu7;
import com.fossil.fl5;
import com.fossil.gj4;
import com.fossil.h47;
import com.fossil.iq5;
import com.fossil.kq7;
import com.fossil.no4;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.sk5;
import com.fossil.tl7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "SummariesRepository";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;
    @DexIgnore
    public /* final */ sk5 mFitnessHelper;
    @DexIgnore
    public List<ActivitySummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public SummariesRepository(ApiServiceV2 apiServiceV2, sk5 sk5) {
        pq7.c(apiServiceV2, "mApiServiceV2");
        pq7.c(sk5, "mFitnessHelper");
        this.mApiServiceV2 = apiServiceV2;
        this.mFitnessHelper = sk5;
    }

    @DexIgnore
    public final Object cleanUp(qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new SummariesRepository$cleanUp$Anon2(null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final Object downloadRecommendedGoals(int i, int i2, int i3, String str, qn7<? super iq5<ActivityRecommendedGoals>> qn7) {
        return eu7.g(bw7.b(), new SummariesRepository$downloadRecommendedGoals$Anon2(this, i, i2, i3, str, null), qn7);
    }

    @DexIgnore
    public final Object fetchActivitySettings(qn7<? super iq5<ActivitySettings>> qn7) {
        return eu7.g(bw7.b(), new SummariesRepository$fetchActivitySettings$Anon2(this, null), qn7);
    }

    @DexIgnore
    public final Object fetchActivityStatistic(qn7<? super ActivityStatistic> qn7) {
        return eu7.g(bw7.b(), new SummariesRepository$fetchActivityStatistic$Anon2(this, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getActivitySettings(com.fossil.qn7<? super com.portfolio.platform.data.model.room.fitness.ActivitySettings> r11) {
        /*
        // Method dump skipped, instructions count: 274
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.getActivitySettings(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final LiveData<h47<ActivityStatistic>> getActivityStatistic(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getActivityStatistic - shouldFetch=" + z);
        return new SummariesRepository$getActivityStatistic$Anon1(this, z).asLiveData();
    }

    @DexIgnore
    public final Object getActivityStatisticAwait(qn7<? super ActivityStatistic> qn7) {
        return eu7.g(bw7.b(), new SummariesRepository$getActivityStatisticAwait$Anon2(this, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object getActivityStatisticDB(com.fossil.qn7<? super com.portfolio.platform.data.ActivityStatistic> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticDB$Anon1
            if (r0 == 0) goto L_0x0032
            r0 = r7
            com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticDB$Anon1 r0 = (com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticDB$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0040
            if (r3 != r5) goto L_0x0038
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SummariesRepository r0 = (com.portfolio.platform.data.source.SummariesRepository) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao r0 = r0.activitySummaryDao()
            com.portfolio.platform.data.ActivityStatistic r0 = r0.getActivityStatistic()
        L_0x0031:
            return r0
        L_0x0032:
            com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticDB$Anon1 r0 = new com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticDB$Anon1
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x0038:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0040:
            com.fossil.el7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = "SummariesRepository"
            java.lang.String r4 = "getActivityStatisticDB"
            r1.d(r3, r4)
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r0 = r1.y(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.getActivityStatisticDB(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object getCurrentActivitySettings(qn7<? super ActivitySettings> qn7) {
        return eu7.g(bw7.b(), new SummariesRepository$getCurrentActivitySettings$Anon2(null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSummaries(java.util.Date r11, java.util.Date r12, boolean r13, com.fossil.qn7<? super androidx.lifecycle.LiveData<com.fossil.h47<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>>>> r14) {
        /*
            r10 = this;
            r9 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1
            if (r0 == 0) goto L_0x0038
            r0 = r14
            com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1 r0 = (com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.label = r1
            r6 = r0
        L_0x0014:
            java.lang.Object r1 = r6.result
            java.lang.Object r7 = com.fossil.yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x0047
            if (r0 != r9) goto L_0x003f
            boolean r0 = r6.Z$0
            java.lang.Object r0 = r6.L$2
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$1
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$0
            com.portfolio.platform.data.source.SummariesRepository r0 = (com.portfolio.platform.data.source.SummariesRepository) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0032:
            java.lang.String r1 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.pq7.b(r0, r1)
        L_0x0037:
            return r0
        L_0x0038:
            com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1 r0 = new com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1
            r0.<init>(r10, r14)
            r6 = r0
            goto L_0x0014
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            com.fossil.el7.b(r1)
            com.fossil.jx7 r8 = com.fossil.bw7.c()
            com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2 r0 = new com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2
            r5 = 0
            r1 = r10
            r2 = r11
            r3 = r12
            r4 = r13
            r0.<init>(r1, r2, r3, r4, r5)
            r6.L$0 = r10
            r6.L$1 = r11
            r6.L$2 = r12
            r6.Z$0 = r13
            r6.label = r9
            java.lang.Object r0 = com.fossil.eu7.g(r8, r0, r6)
            if (r0 != r7) goto L_0x0032
            r0 = r7
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.getSummaries(java.util.Date, java.util.Date, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object getSummariesPaging(FitnessDataRepository fitnessDataRepository, Date date, no4 no4, fl5.a aVar, qn7<? super Listing<ActivitySummary>> qn7) {
        return eu7.g(bw7.c(), new SummariesRepository$getSummariesPaging$Anon2(this, date, fitnessDataRepository, no4, aVar, null), qn7);
    }

    @DexIgnore
    public final Object getSummary(Calendar calendar, qn7<? super ActivitySummary> qn7) {
        return eu7.g(bw7.b(), new SummariesRepository$getSummary$Anon4(calendar, null), qn7);
    }

    @DexIgnore
    public final Object getSummary(Date date, qn7<? super LiveData<h47<ActivitySummary>>> qn7) {
        return eu7.g(bw7.c(), new SummariesRepository$getSummary$Anon2(this, date, null), qn7);
    }

    @DexIgnore
    public final Object insertFromDevice(List<ActivitySummary> list, qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new SummariesRepository$insertFromDevice$Anon2(list, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final Object loadSummaries(Date date, Date date2, qn7<? super iq5<gj4>> qn7) {
        return eu7.g(bw7.b(), new SummariesRepository$loadSummaries$Anon2(this, date, date2, null), qn7);
    }

    @DexIgnore
    public final Object pushActivitySettingsToServer(ActivitySettings activitySettings, qn7<? super q88<ActivitySettings>> qn7) {
        return eu7.g(bw7.b(), new SummariesRepository$pushActivitySettingsToServer$Anon2(this, activitySettings, null), qn7);
    }

    @DexIgnore
    public final void removePagingListener() {
        for (ActivitySummaryDataSourceFactory activitySummaryDataSourceFactory : this.mSourceFactoryList) {
            ActivitySummaryLocalDataSource localDataSource = activitySummaryDataSourceFactory.getLocalDataSource();
            if (localDataSource != null) {
                localDataSource.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x018b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x018f  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0204  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveActivitySettingsToDB(java.util.Date r24, com.portfolio.platform.data.model.room.fitness.ActivitySettings r25, com.fossil.qn7<? super com.fossil.tl7> r26) {
        /*
        // Method dump skipped, instructions count: 525
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.saveActivitySettingsToDB(java.util.Date, com.portfolio.platform.data.model.room.fitness.ActivitySettings, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object updateActivitySettings(com.portfolio.platform.data.model.room.fitness.ActivitySettings r11, com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.model.room.fitness.ActivitySettings>> r12) {
        /*
        // Method dump skipped, instructions count: 294
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.updateActivitySettings(com.portfolio.platform.data.model.room.fitness.ActivitySettings, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object upsertRecommendGoals(ActivityRecommendedGoals activityRecommendedGoals, qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new SummariesRepository$upsertRecommendGoals$Anon2(activityRecommendedGoals, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }
}
