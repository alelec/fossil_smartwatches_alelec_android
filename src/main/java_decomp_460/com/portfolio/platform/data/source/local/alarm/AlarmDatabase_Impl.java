package com.portfolio.platform.data.source.local.alarm;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDatabase_Impl extends AlarmDatabase {
    @DexIgnore
    public volatile AlarmDao _alarmDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `alarm` (`id` TEXT, `uri` TEXT NOT NULL, `title` TEXT NOT NULL, `message` TEXT NOT NULL, `hour` INTEGER NOT NULL, `minute` INTEGER NOT NULL, `days` TEXT, `isActive` INTEGER NOT NULL, `isRepeated` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`uri`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '0b8d5ee1081d415aedfc0c7985d749f7')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `alarm`");
            if (AlarmDatabase_Impl.this.mCallbacks != null) {
                int size = AlarmDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) AlarmDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (AlarmDatabase_Impl.this.mCallbacks != null) {
                int size = AlarmDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) AlarmDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            AlarmDatabase_Impl.this.mDatabase = lx0;
            AlarmDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (AlarmDatabase_Impl.this.mCallbacks != null) {
                int size = AlarmDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) AlarmDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(12);
            hashMap.put("id", new ix0.a("id", "TEXT", false, 0, null, 1));
            hashMap.put("uri", new ix0.a("uri", "TEXT", true, 1, null, 1));
            hashMap.put("title", new ix0.a("title", "TEXT", true, 0, null, 1));
            hashMap.put("message", new ix0.a("message", "TEXT", true, 0, null, 1));
            hashMap.put(AppFilter.COLUMN_HOUR, new ix0.a(AppFilter.COLUMN_HOUR, "INTEGER", true, 0, null, 1));
            hashMap.put(MFSleepGoal.COLUMN_MINUTE, new ix0.a(MFSleepGoal.COLUMN_MINUTE, "INTEGER", true, 0, null, 1));
            hashMap.put(Alarm.COLUMN_DAYS, new ix0.a(Alarm.COLUMN_DAYS, "TEXT", false, 0, null, 1));
            hashMap.put("isActive", new ix0.a("isActive", "INTEGER", true, 0, null, 1));
            hashMap.put("isRepeated", new ix0.a("isRepeated", "INTEGER", true, 0, null, 1));
            hashMap.put("createdAt", new ix0.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            ix0 ix0 = new ix0(Alarm.TABLE_NAME, hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, Alarm.TABLE_NAME);
            if (ix0.equals(a2)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "alarm(com.portfolio.platform.data.source.local.alarm.Alarm).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDatabase
    public AlarmDao alarmDao() {
        AlarmDao alarmDao;
        if (this._alarmDao != null) {
            return this._alarmDao;
        }
        synchronized (this) {
            if (this._alarmDao == null) {
                this._alarmDao = new AlarmDao_Impl(this);
            }
            alarmDao = this._alarmDao;
        }
        return alarmDao;
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `alarm`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), Alarm.TABLE_NAME);
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(6), "0b8d5ee1081d415aedfc0c7985d749f7", "7e35df0dd907bb841b2567561f725af0");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }
}
