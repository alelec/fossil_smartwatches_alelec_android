package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceOrder;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceUserDao_Impl implements DianaWatchFaceUserDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<DianaWatchFaceUser> __insertionAdapterOfDianaWatchFaceUser;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteAll;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteDianaWatchFaceUserById;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfUpdatePinType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<DianaWatchFaceUser> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, DianaWatchFaceUser dianaWatchFaceUser) {
            if (dianaWatchFaceUser.getCreatedAt() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, dianaWatchFaceUser.getCreatedAt());
            }
            if (dianaWatchFaceUser.getUpdatedAt() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, dianaWatchFaceUser.getUpdatedAt());
            }
            px0.bindLong(3, (long) dianaWatchFaceUser.getPinType());
            if (dianaWatchFaceUser.getId() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, dianaWatchFaceUser.getId());
            }
            if (dianaWatchFaceUser.getDownloadURL() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, dianaWatchFaceUser.getDownloadURL());
            }
            if (dianaWatchFaceUser.getName() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, dianaWatchFaceUser.getName());
            }
            if (dianaWatchFaceUser.getChecksum() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, dianaWatchFaceUser.getChecksum());
            }
            if (dianaWatchFaceUser.getPreviewURL() == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, dianaWatchFaceUser.getPreviewURL());
            }
            if (dianaWatchFaceUser.getUid() == null) {
                px0.bindNull(9);
            } else {
                px0.bindString(9, dianaWatchFaceUser.getUid());
            }
            DianaWatchFaceOrder order = dianaWatchFaceUser.getOrder();
            if (order != null) {
                if (order.getOrderId() == null) {
                    px0.bindNull(10);
                } else {
                    px0.bindString(10, order.getOrderId());
                }
                if (order.getWatchFaceId() == null) {
                    px0.bindNull(11);
                } else {
                    px0.bindString(11, order.getWatchFaceId());
                }
                if (order.getPackageVersion() == null) {
                    px0.bindNull(12);
                } else {
                    px0.bindString(12, order.getPackageVersion());
                }
            } else {
                px0.bindNull(10);
                px0.bindNull(11);
                px0.bindNull(12);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaWatchfaceUser` (`createdAt`,`updatedAt`,`pinType`,`id`,`downloadURL`,`name`,`checksum`,`previewURL`,`uid`,`orderId`,`watchFaceId`,`packageVersion`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "UPDATE dianaWatchfaceUser SET pinType=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM dianaWatchfaceUser WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends xw0 {
        @DexIgnore
        public Anon4(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM dianaWatchfaceUser";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 implements Callable<List<DianaWatchFaceUser>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon5(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DianaWatchFaceUser> call() throws Exception {
            Cursor b = ex0.b(DianaWatchFaceUserDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "createdAt");
                int c2 = dx0.c(b, "updatedAt");
                int c3 = dx0.c(b, "pinType");
                int c4 = dx0.c(b, "id");
                int c5 = dx0.c(b, "downloadURL");
                int c6 = dx0.c(b, "name");
                int c7 = dx0.c(b, "checksum");
                int c8 = dx0.c(b, "previewURL");
                int c9 = dx0.c(b, "uid");
                int c10 = dx0.c(b, "orderId");
                int c11 = dx0.c(b, "watchFaceId");
                int c12 = dx0.c(b, "packageVersion");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    String string = b.getString(c4);
                    String string2 = b.getString(c5);
                    String string3 = b.getString(c6);
                    String string4 = b.getString(c7);
                    String string5 = b.getString(c8);
                    String string6 = b.getString(c9);
                    DianaWatchFaceOrder dianaWatchFaceOrder = (!b.isNull(c10) || !b.isNull(c11) || !b.isNull(c12)) ? new DianaWatchFaceOrder(b.getString(c10), b.getString(c11), b.getString(c12)) : null;
                    DianaWatchFaceUser dianaWatchFaceUser = new DianaWatchFaceUser(string, string2, string3, string4, string5, string6);
                    dianaWatchFaceUser.setCreatedAt(b.getString(c));
                    dianaWatchFaceUser.setUpdatedAt(b.getString(c2));
                    dianaWatchFaceUser.setPinType(b.getInt(c3));
                    dianaWatchFaceUser.setOrder(dianaWatchFaceOrder);
                    arrayList.add(dianaWatchFaceUser);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public DianaWatchFaceUserDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfDianaWatchFaceUser = new Anon1(qw0);
        this.__preparedStmtOfUpdatePinType = new Anon2(qw0);
        this.__preparedStmtOfDeleteDianaWatchFaceUserById = new Anon3(qw0);
        this.__preparedStmtOfDeleteAll = new Anon4(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public void deleteAll() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public void deleteDianaWatchFaceUserById(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteDianaWatchFaceUserById.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteDianaWatchFaceUserById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public List<DianaWatchFaceUser> getAllDianaWatchFaceUser() {
        tw0 f = tw0.f("SELECT * FROM dianaWatchfaceUser WHERE pinType <> 3", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "downloadURL");
            int c6 = dx0.c(b, "name");
            int c7 = dx0.c(b, "checksum");
            int c8 = dx0.c(b, "previewURL");
            int c9 = dx0.c(b, "uid");
            int c10 = dx0.c(b, "orderId");
            int c11 = dx0.c(b, "watchFaceId");
            int c12 = dx0.c(b, "packageVersion");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                String string = b.getString(c4);
                String string2 = b.getString(c5);
                String string3 = b.getString(c6);
                String string4 = b.getString(c7);
                String string5 = b.getString(c8);
                String string6 = b.getString(c9);
                DianaWatchFaceOrder dianaWatchFaceOrder = (!b.isNull(c10) || !b.isNull(c11) || !b.isNull(c12)) ? new DianaWatchFaceOrder(b.getString(c10), b.getString(c11), b.getString(c12)) : null;
                DianaWatchFaceUser dianaWatchFaceUser = new DianaWatchFaceUser(string, string2, string3, string4, string5, string6);
                dianaWatchFaceUser.setCreatedAt(b.getString(c));
                dianaWatchFaceUser.setUpdatedAt(b.getString(c2));
                dianaWatchFaceUser.setPinType(b.getInt(c3));
                dianaWatchFaceUser.setOrder(dianaWatchFaceOrder);
                arrayList.add(dianaWatchFaceUser);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public LiveData<List<DianaWatchFaceUser>> getAllDianaWatchFaceUserLiveData() {
        tw0 f = tw0.f("SELECT * FROM dianaWatchfaceUser WHERE pinType <> 3", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon5 anon5 = new Anon5(f);
        return invalidationTracker.d(new String[]{"dianaWatchfaceUser"}, false, anon5);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public DianaWatchFaceUser getDianaWatchFaceUserById(String str) {
        tw0 f = tw0.f("SELECT * FROM dianaWatchfaceUser WHERE id=? AND pinType <> 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        DianaWatchFaceUser dianaWatchFaceUser = null;
        DianaWatchFaceOrder dianaWatchFaceOrder = null;
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "downloadURL");
            int c6 = dx0.c(b, "name");
            int c7 = dx0.c(b, "checksum");
            int c8 = dx0.c(b, "previewURL");
            int c9 = dx0.c(b, "uid");
            int c10 = dx0.c(b, "orderId");
            int c11 = dx0.c(b, "watchFaceId");
            int c12 = dx0.c(b, "packageVersion");
            if (b.moveToFirst()) {
                String string = b.getString(c4);
                String string2 = b.getString(c5);
                String string3 = b.getString(c6);
                String string4 = b.getString(c7);
                String string5 = b.getString(c8);
                String string6 = b.getString(c9);
                if (!b.isNull(c10) || !b.isNull(c11) || !b.isNull(c12)) {
                    dianaWatchFaceOrder = new DianaWatchFaceOrder(b.getString(c10), b.getString(c11), b.getString(c12));
                }
                dianaWatchFaceUser = new DianaWatchFaceUser(string, string2, string3, string4, string5, string6);
                dianaWatchFaceUser.setCreatedAt(b.getString(c));
                dianaWatchFaceUser.setUpdatedAt(b.getString(c2));
                dianaWatchFaceUser.setPinType(b.getInt(c3));
                dianaWatchFaceUser.setOrder(dianaWatchFaceOrder);
            }
            return dianaWatchFaceUser;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public DianaWatchFaceUser getDianaWatchFaceUserByOrderId(String str) {
        tw0 f = tw0.f("SELECT * FROM dianaWatchfaceUser WHERE orderId=? AND pinType <> 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        DianaWatchFaceUser dianaWatchFaceUser = null;
        DianaWatchFaceOrder dianaWatchFaceOrder = null;
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "downloadURL");
            int c6 = dx0.c(b, "name");
            int c7 = dx0.c(b, "checksum");
            int c8 = dx0.c(b, "previewURL");
            int c9 = dx0.c(b, "uid");
            int c10 = dx0.c(b, "orderId");
            int c11 = dx0.c(b, "watchFaceId");
            int c12 = dx0.c(b, "packageVersion");
            if (b.moveToFirst()) {
                String string = b.getString(c4);
                String string2 = b.getString(c5);
                String string3 = b.getString(c6);
                String string4 = b.getString(c7);
                String string5 = b.getString(c8);
                String string6 = b.getString(c9);
                if (!b.isNull(c10) || !b.isNull(c11) || !b.isNull(c12)) {
                    dianaWatchFaceOrder = new DianaWatchFaceOrder(b.getString(c10), b.getString(c11), b.getString(c12));
                }
                dianaWatchFaceUser = new DianaWatchFaceUser(string, string2, string3, string4, string5, string6);
                dianaWatchFaceUser.setCreatedAt(b.getString(c));
                dianaWatchFaceUser.setUpdatedAt(b.getString(c2));
                dianaWatchFaceUser.setPinType(b.getInt(c3));
                dianaWatchFaceUser.setOrder(dianaWatchFaceOrder);
            }
            return dianaWatchFaceUser;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public List<DianaWatchFaceUser> getPendingDianaWatchFace() {
        tw0 f = tw0.f("SELECT * FROM dianaWatchfaceUser WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "downloadURL");
            int c6 = dx0.c(b, "name");
            int c7 = dx0.c(b, "checksum");
            int c8 = dx0.c(b, "previewURL");
            int c9 = dx0.c(b, "uid");
            int c10 = dx0.c(b, "orderId");
            int c11 = dx0.c(b, "watchFaceId");
            int c12 = dx0.c(b, "packageVersion");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                String string = b.getString(c4);
                String string2 = b.getString(c5);
                String string3 = b.getString(c6);
                String string4 = b.getString(c7);
                String string5 = b.getString(c8);
                String string6 = b.getString(c9);
                DianaWatchFaceOrder dianaWatchFaceOrder = (!b.isNull(c10) || !b.isNull(c11) || !b.isNull(c12)) ? new DianaWatchFaceOrder(b.getString(c10), b.getString(c11), b.getString(c12)) : null;
                DianaWatchFaceUser dianaWatchFaceUser = new DianaWatchFaceUser(string, string2, string3, string4, string5, string6);
                dianaWatchFaceUser.setCreatedAt(b.getString(c));
                dianaWatchFaceUser.setUpdatedAt(b.getString(c2));
                dianaWatchFaceUser.setPinType(b.getInt(c3));
                dianaWatchFaceUser.setOrder(dianaWatchFaceOrder);
                arrayList.add(dianaWatchFaceUser);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public DianaWatchFaceUser getWatchFaceByOrderWatchFaceId(String str) {
        tw0 f = tw0.f("SELECT * FROM dianaWatchfaceUser WHERE watchFaceId=? AND pinType <> 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        DianaWatchFaceUser dianaWatchFaceUser = null;
        DianaWatchFaceOrder dianaWatchFaceOrder = null;
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "downloadURL");
            int c6 = dx0.c(b, "name");
            int c7 = dx0.c(b, "checksum");
            int c8 = dx0.c(b, "previewURL");
            int c9 = dx0.c(b, "uid");
            int c10 = dx0.c(b, "orderId");
            int c11 = dx0.c(b, "watchFaceId");
            int c12 = dx0.c(b, "packageVersion");
            if (b.moveToFirst()) {
                String string = b.getString(c4);
                String string2 = b.getString(c5);
                String string3 = b.getString(c6);
                String string4 = b.getString(c7);
                String string5 = b.getString(c8);
                String string6 = b.getString(c9);
                if (!b.isNull(c10) || !b.isNull(c11) || !b.isNull(c12)) {
                    dianaWatchFaceOrder = new DianaWatchFaceOrder(b.getString(c10), b.getString(c11), b.getString(c12));
                }
                dianaWatchFaceUser = new DianaWatchFaceUser(string, string2, string3, string4, string5, string6);
                dianaWatchFaceUser.setCreatedAt(b.getString(c));
                dianaWatchFaceUser.setUpdatedAt(b.getString(c2));
                dianaWatchFaceUser.setPinType(b.getInt(c3));
                dianaWatchFaceUser.setOrder(dianaWatchFaceOrder);
            }
            return dianaWatchFaceUser;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public void updatePinType(String str, int i) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfUpdatePinType.acquire();
        acquire.bindLong(1, (long) i);
        if (str == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfUpdatePinType.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public void upsertDianaWatchFaceUser(DianaWatchFaceUser dianaWatchFaceUser) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaWatchFaceUser.insert((jw0<DianaWatchFaceUser>) dianaWatchFaceUser);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public void upsertDianaWatchFaceUser(List<DianaWatchFaceUser> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaWatchFaceUser.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
