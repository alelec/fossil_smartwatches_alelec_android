package com.portfolio.platform.data.source.local.fitness;

import android.database.sqlite.SQLiteConstraintException;
import androidx.lifecycle.LiveData;
import com.fossil.hm7;
import com.fossil.im7;
import com.fossil.kq7;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ActivitySummaryDao {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    /*
    static {
        String simpleName = ActivitySummaryDao.class.getSimpleName();
        pq7.b(simpleName, "ActivitySummaryDao::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    private final void calculateSummary(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        List<Integer> j0;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateSummary - currentSummary=" + activitySummary + ", newSummary=" + activitySummary2);
        double steps = activitySummary2.getSteps();
        double steps2 = activitySummary.getSteps();
        double calories = activitySummary2.getCalories();
        double calories2 = activitySummary.getCalories();
        double distance = activitySummary2.getDistance();
        double distance2 = activitySummary.getDistance();
        int activeTime = activitySummary2.getActiveTime();
        int activeTime2 = activitySummary.getActiveTime();
        List<Integer> intensities = activitySummary2.getIntensities();
        List<Integer> intensities2 = activitySummary.getIntensities();
        if (intensities.size() > intensities2.size()) {
            ArrayList arrayList = new ArrayList(im7.m(intensities, 10));
            int i = 0;
            for (T t : intensities) {
                if (i >= 0) {
                    arrayList.add(Integer.valueOf((i < intensities2.size() ? intensities2.get(i).intValue() : 0) + t.intValue()));
                    i++;
                } else {
                    hm7.l();
                    throw null;
                }
            }
            j0 = pm7.j0(arrayList);
        } else {
            ArrayList arrayList2 = new ArrayList(im7.m(intensities2, 10));
            int i2 = 0;
            for (T t2 : intensities2) {
                if (i2 >= 0) {
                    arrayList2.add(Integer.valueOf((i2 < intensities.size() ? intensities.get(i2).intValue() : 0) + t2.intValue()));
                    i2++;
                } else {
                    hm7.l();
                    throw null;
                }
            }
            j0 = pm7.j0(arrayList2);
        }
        activitySummary2.setSteps(steps + steps2);
        activitySummary2.setCalories(calories + calories2);
        activitySummary2.setDistance(distance + distance2);
        activitySummary2.setActiveTime(activeTime + activeTime2);
        activitySummary2.setCreatedAt(activitySummary.getCreatedAt());
        activitySummary2.setIntensities(j0);
        activitySummary2.setStepGoal(activitySummary.getStepGoal());
        activitySummary2.setCaloriesGoal(activitySummary.getCaloriesGoal());
        activitySummary2.setActiveTimeGoal(activitySummary.getActiveTimeGoal());
        if (activitySummary.getSteps() != activitySummary2.getSteps()) {
            activitySummary2.setUpdatedAt(new DateTime());
        }
    }

    @DexIgnore
    public abstract void deleteAllActivitySummaries();

    @DexIgnore
    public abstract ActivitySettings getActivitySetting();

    @DexIgnore
    public abstract LiveData<ActivitySettings> getActivitySettingLiveData();

    @DexIgnore
    public abstract ActivityStatistic getActivityStatistic();

    @DexIgnore
    public abstract LiveData<ActivityStatistic> getActivityStatisticLiveData();

    @DexIgnore
    public abstract List<ActivitySummary> getActivitySummariesDesc(int i, int i2, int i3, int i4, int i5, int i6);

    @DexIgnore
    public final List<ActivitySummary> getActivitySummariesDesc(Date date, Date date2) {
        pq7.c(date, GoalPhase.COLUMN_START_DATE);
        pq7.c(date2, GoalPhase.COLUMN_END_DATE);
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "startCalendar");
        instance.setTime(date);
        Calendar instance2 = Calendar.getInstance();
        pq7.b(instance2, "endCalendar");
        instance2.setTime(date2);
        return getActivitySummariesDesc(instance.get(5), instance.get(2) + 1, instance.get(1), instance2.get(5), instance2.get(2) + 1, instance2.get(1));
    }

    @DexIgnore
    public abstract LiveData<List<ActivitySummary>> getActivitySummariesLiveData(int i, int i2, int i3, int i4, int i5, int i6);

    @DexIgnore
    public final LiveData<List<ActivitySummary>> getActivitySummariesLiveData(Date date, Date date2) {
        pq7.c(date, GoalPhase.COLUMN_START_DATE);
        pq7.c(date2, GoalPhase.COLUMN_END_DATE);
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "startCalendar");
        instance.setTime(date);
        Calendar instance2 = Calendar.getInstance();
        pq7.b(instance2, "endCalendar");
        instance2.setTime(date2);
        return getActivitySummariesLiveData(instance.get(5), instance.get(2) + 1, instance.get(1), instance2.get(5), instance2.get(2) + 1, instance2.get(1));
    }

    @DexIgnore
    public abstract ActivitySummary getActivitySummary(int i, int i2, int i3);

    @DexIgnore
    public final ActivitySummary getActivitySummary(Date date) {
        pq7.c(date, "date");
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "dateCalendar");
        instance.setTime(date);
        return getActivitySummary(instance.get(1), instance.get(2) + 1, instance.get(5));
    }

    @DexIgnore
    public abstract LiveData<ActivitySummary> getActivitySummaryLiveData(int i, int i2, int i3);

    @DexIgnore
    public final Date getLastDate() {
        Calendar instance = Calendar.getInstance();
        ActivitySummary lastSummary = getLastSummary();
        if (lastSummary == null) {
            return null;
        }
        instance.set(lastSummary.getYear(), lastSummary.getMonth() - 1, lastSummary.getDay());
        pq7.b(instance, "lastDate");
        return instance.getTime();
    }

    @DexIgnore
    public abstract ActivitySummary getLastSummary();

    @DexIgnore
    public abstract ActivitySummary getNearestSampleDayFromDate(int i, int i2, int i3);

    @DexIgnore
    public abstract ActivitySummary.TotalValuesOfWeek getTotalValuesOfWeek(int i, int i2, int i3, int i4, int i5, int i6);

    @DexIgnore
    public final ActivitySummary.TotalValuesOfWeek getTotalValuesOfWeek(Date date, Date date2) {
        pq7.c(date, GoalPhase.COLUMN_START_DATE);
        pq7.c(date2, GoalPhase.COLUMN_END_DATE);
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "startCalendar");
        instance.setTime(date);
        Calendar instance2 = Calendar.getInstance();
        pq7.b(instance2, "endCalendar");
        instance2.setTime(date2);
        return getTotalValuesOfWeek(instance.get(5), instance.get(2) + 1, instance.get(1), instance2.get(5), instance2.get(2) + 1, instance2.get(1));
    }

    @DexIgnore
    public abstract long insertActivitySettings(ActivitySettings activitySettings);

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0102  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void insertActivitySummaries(java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary> r8) {
        /*
            r7 = this;
            r2 = 0
            java.lang.String r0 = "summaries"
            com.fossil.pq7.c(r8, r0)
            java.util.Iterator r3 = r8.iterator()
        L_0x000a:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0105
            java.lang.Object r0 = r3.next()
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r0 = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "addActivitySummary summary="
            r5.append(r6)
            r5.append(r0)
            java.lang.String r5 = r5.toString()
            r1.d(r4, r5)
            int r1 = r0.getYear()
            int r4 = r0.getMonth()
            int r5 = r0.getDay()
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r1 = r7.getActivitySummary(r1, r4, r5)
            if (r1 == 0) goto L_0x0064
            r7.calculateSummary(r1, r0)
        L_0x0047:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "XXX- insert summary - summary="
            r5.append(r6)
            r5.append(r0)
            java.lang.String r0 = r5.toString()
            r1.d(r4, r0)
            goto L_0x000a
        L_0x0064:
            java.util.Calendar r1 = java.util.Calendar.getInstance()
            int r4 = r0.getYear()
            int r5 = r0.getMonth()
            int r5 = r5 + -1
            int r6 = r0.getDay()
            r1.set(r4, r5, r6)
            r4 = 1
            int r4 = r1.get(r4)
            r5 = 2
            int r5 = r1.get(r5)
            r6 = 5
            int r1 = r1.get(r6)
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r4 = r7.getNearestSampleDayFromDate(r4, r5, r1)
            com.portfolio.platform.data.model.room.fitness.ActivitySettings r5 = r7.getActivitySetting()
            if (r4 == 0) goto L_0x00db
            int r1 = r4.getStepGoal()
        L_0x0096:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
        L_0x009a:
            if (r1 == 0) goto L_0x00e4
            int r1 = r1.intValue()
        L_0x00a0:
            r0.setStepGoal(r1)
            if (r4 == 0) goto L_0x00e7
            int r1 = r4.getActiveTimeGoal()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
        L_0x00ad:
            if (r1 == 0) goto L_0x00f4
            int r1 = r1.intValue()
        L_0x00b3:
            r0.setActiveTimeGoal(r1)
            if (r4 == 0) goto L_0x00f7
            int r1 = r4.getCaloriesGoal()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
        L_0x00c0:
            if (r1 == 0) goto L_0x0102
            int r1 = r1.intValue()
        L_0x00c6:
            r0.setCaloriesGoal(r1)
            org.joda.time.DateTime r1 = new org.joda.time.DateTime
            r1.<init>()
            r0.setCreatedAt(r1)
            org.joda.time.DateTime r1 = new org.joda.time.DateTime
            r1.<init>()
            r0.setUpdatedAt(r1)
            goto L_0x0047
        L_0x00db:
            if (r5 == 0) goto L_0x00e2
            int r1 = r5.getCurrentStepGoal()
            goto L_0x0096
        L_0x00e2:
            r1 = r2
            goto L_0x009a
        L_0x00e4:
            r1 = 5000(0x1388, float:7.006E-42)
            goto L_0x00a0
        L_0x00e7:
            if (r5 == 0) goto L_0x00f2
            int r1 = r5.getCurrentActiveTimeGoal()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            goto L_0x00ad
        L_0x00f2:
            r1 = r2
            goto L_0x00ad
        L_0x00f4:
            r1 = 30
            goto L_0x00b3
        L_0x00f7:
            if (r5 == 0) goto L_0x0109
            int r1 = r5.getCurrentCaloriesGoal()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            goto L_0x00c0
        L_0x0102:
            r1 = 140(0x8c, float:1.96E-43)
            goto L_0x00c6
        L_0x0105:
            r7.upsertActivitySummaries(r8)
            return
        L_0x0109:
            r1 = r2
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao.insertActivitySummaries(java.util.List):void");
    }

    @DexIgnore
    public abstract void updateActivitySettings(int i, int i2, int i3);

    @DexIgnore
    public abstract void upsertActivityRecommendedGoals(ActivityRecommendedGoals activityRecommendedGoals);

    @DexIgnore
    public final void upsertActivitySettings(ActivitySettings activitySettings) {
        pq7.c(activitySettings, com.fossil.wearables.fsl.fitness.ActivitySettings.TABLE_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "upsertActivitySettings stepGoal=" + activitySettings.getCurrentStepGoal() + " caloriesGoal=" + activitySettings.getCurrentCaloriesGoal() + " activeTimeGoal=" + activitySettings.getCurrentActiveTimeGoal());
        try {
            insertActivitySettings(activitySettings);
        } catch (SQLiteConstraintException e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "upsertActivitySettings exception " + e);
            updateActivitySettings(activitySettings.getCurrentStepGoal(), activitySettings.getCurrentCaloriesGoal(), activitySettings.getCurrentActiveTimeGoal());
        }
    }

    @DexIgnore
    public abstract long upsertActivityStatistic(ActivityStatistic activityStatistic);

    @DexIgnore
    public abstract void upsertActivitySummaries(List<ActivitySummary> list);

    @DexIgnore
    public abstract void upsertActivitySummary(ActivitySummary activitySummary);
}
