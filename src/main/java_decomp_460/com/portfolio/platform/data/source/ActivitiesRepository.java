package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bw7;
import com.fossil.eu7;
import com.fossil.il7;
import com.fossil.iq5;
import com.fossil.kq7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.sk5;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.source.local.fitness.ActivitySampleDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitiesRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ sk5 mFitnessHelper;
    @DexIgnore
    public /* final */ UserRepository mUserRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ActivitiesRepository.TAG;
        }
    }

    @DexIgnore
    public interface PushPendingActivitiesCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(List<ActivitySample> list);
    }

    /*
    static {
        String simpleName = ActivitiesRepository.class.getSimpleName();
        pq7.b(simpleName, "ActivitiesRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ActivitiesRepository(ApiServiceV2 apiServiceV2, UserRepository userRepository, sk5 sk5) {
        pq7.c(apiServiceV2, "mApiService");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(sk5, "mFitnessHelper");
        this.mApiService = apiServiceV2;
        this.mUserRepository = userRepository;
        this.mFitnessHelper = sk5;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchActivitySamples$default(ActivitiesRepository activitiesRepository, Date date, Date date2, int i, int i2, qn7 qn7, int i3, Object obj) {
        return activitiesRepository.fetchActivitySamples(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, qn7);
    }

    @DexIgnore
    private final LiveData<List<ActivitySample>> getActivitySamplesInDate(Date date, ActivitySampleDao activitySampleDao) {
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "calendarStart");
        instance.setTime(date);
        lk5.T(instance);
        Object clone = instance.clone();
        if (clone != null) {
            Calendar calendar = (Calendar) clone;
            lk5.C(calendar);
            pq7.b(calendar, "DateHelper.getEndOfDay(calendarEnd)");
            Date time = instance.getTime();
            pq7.b(time, "calendarStart.time");
            Date time2 = calendar.getTime();
            pq7.b(time2, "calendarEnd.time");
            return activitySampleDao.getActivitySamplesLiveData(time, time2);
        }
        throw new il7("null cannot be cast to non-null type java.util.Calendar");
    }

    @DexIgnore
    public final Object cleanUp(qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new ActivitiesRepository$cleanUp$Anon2(null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final Object fetchActivitySamples(Date date, Date date2, int i, int i2, qn7<? super iq5<ApiResponse<Activity>>> qn7) {
        return eu7.g(bw7.b(), new ActivitiesRepository$fetchActivitySamples$Anon2(this, date, date2, i, i2, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getActivityList(java.util.Date r11, java.util.Date r12, boolean r13, com.fossil.qn7<? super androidx.lifecycle.LiveData<com.fossil.h47<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>>>> r14) {
        /*
            r10 = this;
            r9 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon1
            if (r0 == 0) goto L_0x0038
            r0 = r14
            com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon1 r0 = (com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.label = r1
            r6 = r0
        L_0x0014:
            java.lang.Object r1 = r6.result
            java.lang.Object r7 = com.fossil.yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x0047
            if (r0 != r9) goto L_0x003f
            boolean r0 = r6.Z$0
            java.lang.Object r0 = r6.L$2
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$1
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$0
            com.portfolio.platform.data.source.ActivitiesRepository r0 = (com.portfolio.platform.data.source.ActivitiesRepository) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0032:
            java.lang.String r1 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.pq7.b(r0, r1)
        L_0x0037:
            return r0
        L_0x0038:
            com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon1 r0 = new com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon1
            r0.<init>(r10, r14)
            r6 = r0
            goto L_0x0014
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            com.fossil.el7.b(r1)
            com.fossil.jx7 r8 = com.fossil.bw7.c()
            com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2 r0 = new com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2
            r5 = 0
            r1 = r10
            r2 = r11
            r3 = r12
            r4 = r13
            r0.<init>(r1, r2, r3, r4, r5)
            r6.L$0 = r10
            r6.L$1 = r11
            r6.L$2 = r12
            r6.Z$0 = r13
            r6.label = r9
            java.lang.Object r0 = com.fossil.eu7.g(r8, r0, r6)
            if (r0 != r7) goto L_0x0032
            r0 = r7
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ActivitiesRepository.getActivityList(java.util.Date, java.util.Date, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object getPendingActivities(Date date, Date date2, qn7<? super List<SampleRaw>> qn7) {
        return eu7.g(bw7.b(), new ActivitiesRepository$getPendingActivities$Anon2(date, date2, null), qn7);
    }

    @DexIgnore
    public final Object insert(List<SampleRaw> list, qn7<? super iq5<List<ActivitySample>>> qn7) {
        return eu7.g(bw7.b(), new ActivitiesRepository$insert$Anon2(this, list, null), qn7);
    }

    @DexIgnore
    public final Object insertFromDevice(List<ActivitySample> list, qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new ActivitiesRepository$insertFromDevice$Anon2(list, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final Object pushPendingActivities(PushPendingActivitiesCallback pushPendingActivitiesCallback, qn7<? super tl7> qn7) {
        return eu7.g(bw7.b(), new ActivitiesRepository$pushPendingActivities$Anon2(this, pushPendingActivitiesCallback, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0180  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01c5  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01d5  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x02e6 A[LOOP:1: B:79:0x02e0->B:81:0x02e6, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x02fd  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x030c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveActivitiesToServer(java.util.List<com.portfolio.platform.data.model.room.fitness.SampleRaw> r21, com.portfolio.platform.data.source.ActivitiesRepository.PushPendingActivitiesCallback r22, com.fossil.qn7<? super com.fossil.tl7> r23) {
        /*
        // Method dump skipped, instructions count: 806
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ActivitiesRepository.saveActivitiesToServer(java.util.List, com.portfolio.platform.data.source.ActivitiesRepository$PushPendingActivitiesCallback, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object updateActivityPinType(List<SampleRaw> list, int i, qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new ActivitiesRepository$updateActivityPinType$Anon2(list, i, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }
}
