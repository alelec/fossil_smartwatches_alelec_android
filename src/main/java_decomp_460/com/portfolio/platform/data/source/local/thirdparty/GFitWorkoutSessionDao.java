package com.portfolio.platform.data.source.local.thirdparty;

import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface GFitWorkoutSessionDao {
    @DexIgnore
    Object clearAll();  // void declaration

    @DexIgnore
    void deleteGFitWorkoutSession(GFitWorkoutSession gFitWorkoutSession);

    @DexIgnore
    List<GFitWorkoutSession> getAllGFitWorkoutSession();

    @DexIgnore
    void insertGFitWorkoutSession(GFitWorkoutSession gFitWorkoutSession);

    @DexIgnore
    void insertListGFitWorkoutSession(List<GFitWorkoutSession> list);
}
