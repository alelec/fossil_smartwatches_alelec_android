package com.portfolio.platform.data.source;

import com.fossil.nn7;
import com.fossil.tn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$$special$$inlined$CoroutineExceptionHandler$Anon1 extends nn7 implements CoroutineExceptionHandler {
    @DexIgnore
    public ThirdPartyRepository$$special$$inlined$CoroutineExceptionHandler$Anon1(tn7.c cVar) {
        super(cVar);
    }

    @DexIgnore
    @Override // kotlinx.coroutines.CoroutineExceptionHandler
    public void handleException(tn7 tn7, Throwable th) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(ThirdPartyRepository.TAG, "exception when push third party database " + th);
    }
}
