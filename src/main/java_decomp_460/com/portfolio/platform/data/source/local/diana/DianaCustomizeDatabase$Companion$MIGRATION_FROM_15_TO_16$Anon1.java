package com.portfolio.platform.data.source.local.diana;

import android.util.Log;
import com.fossil.ax0;
import com.fossil.lx0;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeDatabase$Companion$MIGRATION_FROM_15_TO_16$Anon1 extends ax0 {
    @DexIgnore
    public DianaCustomizeDatabase$Companion$MIGRATION_FROM_15_TO_16$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.ax0
    public void migrate(lx0 lx0) {
        pq7.c(lx0, "database");
        Log.d(DianaCustomizeDatabase.TAG, "MIGRATION_FROM_15_TO_16 - start");
        lx0.beginTransaction();
        try {
            Log.d(DianaCustomizeDatabase.TAG, "MIGRATION_FROM_15_TO_16 - start");
            lx0.execSQL("CREATE TABLE `diana_recommended_preset` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `button` TEXT NOT NULL, `isDefault` INTEGER NOT NULL, `faceUrl` TEXT, `checkSum` TEXT, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `serial` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE `wf_background_photo` (`id` TEXT NOT NULL, `downloadUrl` TEXT NOT NULL, `checkSum` TEXT NOT NULL, `uid` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `localPath` TEXT, `pinType` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE `wf_asset` (`id` TEXT NOT NULL, `category` TEXT NOT NULL, `name` TEXT NOT NULL, `data` TEXT NOT NULL, `isNew` INTEGER NOT NULL, `meta` TEXT, `assetType` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE `dianaWatchfaceTemplate` (`createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `id` TEXT NOT NULL, `downloadURL` TEXT NOT NULL, `checksum` TEXT NOT NULL, `firmwareOSVersion` TEXT NOT NULL, `packageVersion` TEXT NOT NULL, `themeClass` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE `dianaWatchFaceRing` (`createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `id` TEXT NOT NULL, `category` TEXT NOT NULL, `name` TEXT NOT NULL, `data` TEXT NOT NULL, `metaData` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE `dianaWatchfaceUser` (`createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `downloadURL` TEXT NOT NULL, `name` TEXT NOT NULL, `checksum` TEXT NOT NULL, `previewURL` TEXT NOT NULL, `uid` TEXT NOT NULL, `orderId` TEXT, `watchFaceId` TEXT, `packageVersion` TEXT, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE `DianaAppSetting` (`pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `appId` TEXT NOT NULL, `category` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`appId`, `category`))");
            lx0.execSQL("CREATE TABLE `diana_watchface_preset` (`createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `buttons` TEXT, `checksumFace` TEXT NOT NULL, `faceUrl` TEXT NOT NULL, `previewFaceUrl` TEXT, `isActive` INTEGER NOT NULL, `name` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `uid` TEXT NOT NULL, `originalItemIdInStore` TEXT, `isNew` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        } catch (Exception e) {
            Log.d(DianaCustomizeDatabase.TAG, "MIGRATION_FROM_15_TO_16 - exception " + e);
        }
        lx0.setTransactionSuccessful();
        lx0.endTransaction();
        Log.d(DianaCustomizeDatabase.TAG, "MIGRATION_FROM_15_TO_16 - done");
    }
}
