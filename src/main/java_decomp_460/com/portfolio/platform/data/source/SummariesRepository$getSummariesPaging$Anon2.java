package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bw7;
import com.fossil.cu0;
import com.fossil.dv7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.eu7;
import com.fossil.fl5;
import com.fossil.gi0;
import com.fossil.gp7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.mx0;
import com.fossil.no4;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.qq7;
import com.fossil.sk5;
import com.fossil.ss0;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.fossil.zt0;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SummariesRepository$getSummariesPaging$2", f = "SummariesRepository.kt", l = {356}, m = "invokeSuspend")
public final class SummariesRepository$getSummariesPaging$Anon2 extends ko7 implements vp7<iv7, qn7<? super Listing<ActivitySummary>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ no4 $appExecutors;
    @DexIgnore
    public /* final */ /* synthetic */ Date $createdDate;
    @DexIgnore
    public /* final */ /* synthetic */ FitnessDataRepository $fitnessDataRepository;
    @DexIgnore
    public /* final */ /* synthetic */ fl5.a $listener;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements gi0<X, LiveData<Y>> {
        @DexIgnore
        public static /* final */ Anon1_Level2 INSTANCE; // = new Anon1_Level2();

        @DexIgnore
        public final LiveData<NetworkState> apply(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
            return activitySummaryLocalDataSource.getMNetworkState();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2_Level2 extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivitySummaryDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(ActivitySummaryDataSourceFactory activitySummaryDataSourceFactory) {
            super(0);
            this.$sourceFactory = activitySummaryDataSourceFactory;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            ActivitySummaryLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
            if (e != null) {
                e.invalidate();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3_Level2 extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivitySummaryDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(ActivitySummaryDataSourceFactory activitySummaryDataSourceFactory) {
            super(0);
            this.$sourceFactory = activitySummaryDataSourceFactory;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            fl5 mHelper;
            ActivitySummaryLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
            if (e != null && (mHelper = e.getMHelper()) != null) {
                mHelper.g();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummariesPaging$Anon2(SummariesRepository summariesRepository, Date date, FitnessDataRepository fitnessDataRepository, no4 no4, fl5.a aVar, qn7 qn7) {
        super(2, qn7);
        this.this$0 = summariesRepository;
        this.$createdDate = date;
        this.$fitnessDataRepository = fitnessDataRepository;
        this.$appExecutors = no4;
        this.$listener = aVar;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SummariesRepository$getSummariesPaging$Anon2 summariesRepository$getSummariesPaging$Anon2 = new SummariesRepository$getSummariesPaging$Anon2(this.this$0, this.$createdDate, this.$fitnessDataRepository, this.$appExecutors, this.$listener, qn7);
        summariesRepository$getSummariesPaging$Anon2.p$ = (iv7) obj;
        return summariesRepository$getSummariesPaging$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super Listing<ActivitySummary>> qn7) {
        return ((SummariesRepository$getSummariesPaging$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Calendar instance;
        Object g;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            ActivitySummaryLocalDataSource.Companion companion = ActivitySummaryLocalDataSource.Companion;
            Calendar instance2 = Calendar.getInstance();
            pq7.b(instance2, "Calendar.getInstance()");
            Date time = instance2.getTime();
            pq7.b(time, "Calendar.getInstance().time");
            Date calculateNextKey = companion.calculateNextKey(time, this.$createdDate);
            instance = Calendar.getInstance();
            pq7.b(instance, "calendar");
            instance.setTime(calculateNextKey);
            dv7 b = bw7.b();
            SummariesRepository$getSummariesPaging$Anon2$fitnessDatabase$Anon1_Level2 summariesRepository$getSummariesPaging$Anon2$fitnessDatabase$Anon1_Level2 = new SummariesRepository$getSummariesPaging$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = iv7;
            this.L$1 = calculateNextKey;
            this.L$2 = instance;
            this.label = 1;
            g = eu7.g(b, summariesRepository$getSummariesPaging$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (g == d) {
                return d;
            }
        } else if (i == 1) {
            Date date = (Date) this.L$1;
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            instance = (Calendar) this.L$2;
            g = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) g;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("XXX- getSummariesPaging - createdDate=");
        sb.append(this.$createdDate);
        sb.append(" DBNAME ");
        mx0 openHelper = fitnessDatabase.getOpenHelper();
        pq7.b(openHelper, "fitnessDatabase.openHelper");
        sb.append(openHelper.getDatabaseName());
        local.d(SummariesRepository.TAG, sb.toString());
        SummariesRepository summariesRepository = this.this$0;
        sk5 sk5 = summariesRepository.mFitnessHelper;
        FitnessDataRepository fitnessDataRepository = this.$fitnessDataRepository;
        Date date2 = this.$createdDate;
        no4 no4 = this.$appExecutors;
        fl5.a aVar = this.$listener;
        pq7.b(instance, "calendar");
        ActivitySummaryDataSourceFactory activitySummaryDataSourceFactory = new ActivitySummaryDataSourceFactory(summariesRepository, sk5, fitnessDataRepository, fitnessDatabase, date2, no4, aVar, instance);
        this.this$0.mSourceFactoryList.add(activitySummaryDataSourceFactory);
        cu0.f.a aVar2 = new cu0.f.a();
        aVar2.c(30);
        aVar2.b(false);
        aVar2.d(30);
        aVar2.e(5);
        cu0.f a2 = aVar2.a();
        pq7.b(a2, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a3 = new zt0(activitySummaryDataSourceFactory, a2).a();
        pq7.b(a3, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData c = ss0.c(activitySummaryDataSourceFactory.getSourceLiveData(), Anon1_Level2.INSTANCE);
        pq7.b(c, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing(a3, c, new Anon2_Level2(activitySummaryDataSourceFactory), new Anon3_Level2(activitySummaryDataSourceFactory));
    }
}
