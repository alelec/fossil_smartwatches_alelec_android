package com.portfolio.platform.data.source;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserSettingDatabase_Impl extends UserSettingDatabase {
    @DexIgnore
    public volatile UserSettingDao _userSettingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `userSettings` (`isShowGoalRing` INTEGER NOT NULL, `acceptedLocationDataSharing` TEXT NOT NULL, `acceptedPrivacies` TEXT NOT NULL, `acceptedTermsOfService` TEXT NOT NULL, `uid` TEXT NOT NULL, `id` TEXT, `isLatestLocationDataSharingAccepted` INTEGER NOT NULL, `isLatestPrivacyAccepted` INTEGER NOT NULL, `isLatestTermsOfServiceAccepted` INTEGER NOT NULL, `latestLocationDataSharingVersion` TEXT, `latestPrivacyVersion` TEXT, `latestTermsOfServiceVersion` TEXT, `startDayOfWeek` TEXT, `createdAt` TEXT, `updatedAt` TEXT, `pinType` INTEGER NOT NULL, PRIMARY KEY(`uid`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'acf19a46886838b5899188682e5ab582')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `userSettings`");
            if (UserSettingDatabase_Impl.this.mCallbacks != null) {
                int size = UserSettingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) UserSettingDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (UserSettingDatabase_Impl.this.mCallbacks != null) {
                int size = UserSettingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) UserSettingDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            UserSettingDatabase_Impl.this.mDatabase = lx0;
            UserSettingDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (UserSettingDatabase_Impl.this.mCallbacks != null) {
                int size = UserSettingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) UserSettingDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(16);
            hashMap.put("isShowGoalRing", new ix0.a("isShowGoalRing", "INTEGER", true, 0, null, 1));
            hashMap.put("acceptedLocationDataSharing", new ix0.a("acceptedLocationDataSharing", "TEXT", true, 0, null, 1));
            hashMap.put("acceptedPrivacies", new ix0.a("acceptedPrivacies", "TEXT", true, 0, null, 1));
            hashMap.put("acceptedTermsOfService", new ix0.a("acceptedTermsOfService", "TEXT", true, 0, null, 1));
            hashMap.put("uid", new ix0.a("uid", "TEXT", true, 1, null, 1));
            hashMap.put("id", new ix0.a("id", "TEXT", false, 0, null, 1));
            hashMap.put("isLatestLocationDataSharingAccepted", new ix0.a("isLatestLocationDataSharingAccepted", "INTEGER", true, 0, null, 1));
            hashMap.put("isLatestPrivacyAccepted", new ix0.a("isLatestPrivacyAccepted", "INTEGER", true, 0, null, 1));
            hashMap.put("isLatestTermsOfServiceAccepted", new ix0.a("isLatestTermsOfServiceAccepted", "INTEGER", true, 0, null, 1));
            hashMap.put("latestLocationDataSharingVersion", new ix0.a("latestLocationDataSharingVersion", "TEXT", false, 0, null, 1));
            hashMap.put("latestPrivacyVersion", new ix0.a("latestPrivacyVersion", "TEXT", false, 0, null, 1));
            hashMap.put("latestTermsOfServiceVersion", new ix0.a("latestTermsOfServiceVersion", "TEXT", false, 0, null, 1));
            hashMap.put("startDayOfWeek", new ix0.a("startDayOfWeek", "TEXT", false, 0, null, 1));
            hashMap.put("createdAt", new ix0.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap.put("updatedAt", new ix0.a("updatedAt", "TEXT", false, 0, null, 1));
            hashMap.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            ix0 ix0 = new ix0("userSettings", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "userSettings");
            if (ix0.equals(a2)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "userSettings(com.portfolio.platform.data.model.UserSettings).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `userSettings`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "userSettings");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(1), "acf19a46886838b5899188682e5ab582", "c5f82b465e65adc1cedba3c9f8395495");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDatabase
    public UserSettingDao userSettingDao() {
        UserSettingDao userSettingDao;
        if (this._userSettingDao != null) {
            return this._userSettingDao;
        }
        synchronized (this) {
            if (this._userSettingDao == null) {
                this._userSettingDao = new UserSettingDao_Impl(this);
            }
            userSettingDao = this._userSettingDao;
        }
        return userSettingDao;
    }
}
