package com.portfolio.platform.data.source;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.Device;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDao_Impl implements DeviceDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<Device> __insertionAdapterOfDevice;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfRemoveDeviceByDeviceId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<Device> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, Device device) {
            px0.bindLong(1, (long) device.getMajor());
            px0.bindLong(2, (long) device.getMinor());
            if (device.getCreatedAt() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, device.getCreatedAt());
            }
            if (device.getUpdatedAt() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, device.getUpdatedAt());
            }
            if (device.getOwner() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, device.getOwner());
            }
            if (device.getProductDisplayName() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, device.getProductDisplayName());
            }
            if (device.getManufacturer() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, device.getManufacturer());
            }
            if (device.getSoftwareRevision() == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, device.getSoftwareRevision());
            }
            if (device.getHardwareRevision() == null) {
                px0.bindNull(9);
            } else {
                px0.bindString(9, device.getHardwareRevision());
            }
            if (device.getActivationDate() == null) {
                px0.bindNull(10);
            } else {
                px0.bindString(10, device.getActivationDate());
            }
            if (device.getDeviceId() == null) {
                px0.bindNull(11);
            } else {
                px0.bindString(11, device.getDeviceId());
            }
            if (device.getMacAddress() == null) {
                px0.bindNull(12);
            } else {
                px0.bindString(12, device.getMacAddress());
            }
            if (device.getSku() == null) {
                px0.bindNull(13);
            } else {
                px0.bindString(13, device.getSku());
            }
            if (device.getFirmwareRevision() == null) {
                px0.bindNull(14);
            } else {
                px0.bindString(14, device.getFirmwareRevision());
            }
            px0.bindLong(15, (long) device.getBatteryLevel());
            if (device.getVibrationStrength() == null) {
                px0.bindNull(16);
            } else {
                px0.bindLong(16, (long) device.getVibrationStrength().intValue());
            }
            px0.bindLong(17, device.isActive() ? 1 : 0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `device` (`major`,`minor`,`createdAt`,`updatedAt`,`owner`,`productDisplayName`,`manufacturer`,`softwareRevision`,`hardwareRevision`,`activationDate`,`deviceId`,`macAddress`,`sku`,`firmwareRevision`,`batteryLevel`,`vibrationStrength`,`isActive`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM device WHERE deviceId=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM device";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<Device>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon4(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<Device> call() throws Exception {
            Cursor b = ex0.b(DeviceDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "major");
                int c2 = dx0.c(b, "minor");
                int c3 = dx0.c(b, "createdAt");
                int c4 = dx0.c(b, "updatedAt");
                int c5 = dx0.c(b, "owner");
                int c6 = dx0.c(b, "productDisplayName");
                int c7 = dx0.c(b, "manufacturer");
                int c8 = dx0.c(b, "softwareRevision");
                int c9 = dx0.c(b, "hardwareRevision");
                int c10 = dx0.c(b, "activationDate");
                int c11 = dx0.c(b, "deviceId");
                int c12 = dx0.c(b, "macAddress");
                int c13 = dx0.c(b, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
                int c14 = dx0.c(b, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
                int c15 = dx0.c(b, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
                int c16 = dx0.c(b, "vibrationStrength");
                int c17 = dx0.c(b, "isActive");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    Device device = new Device(b.getString(c11), b.getString(c12), b.getString(c13), b.getString(c14), b.getInt(c15), b.isNull(c16) ? null : Integer.valueOf(b.getInt(c16)), b.getInt(c17) != 0);
                    device.setMajor(b.getInt(c));
                    device.setMinor(b.getInt(c2));
                    device.setCreatedAt(b.getString(c3));
                    device.setUpdatedAt(b.getString(c4));
                    device.setOwner(b.getString(c5));
                    device.setProductDisplayName(b.getString(c6));
                    device.setManufacturer(b.getString(c7));
                    device.setSoftwareRevision(b.getString(c8));
                    device.setHardwareRevision(b.getString(c9));
                    device.setActivationDate(b.getString(c10));
                    arrayList.add(device);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 implements Callable<Device> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon5(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Device call() throws Exception {
            Device device;
            Cursor b = ex0.b(DeviceDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "major");
                int c2 = dx0.c(b, "minor");
                int c3 = dx0.c(b, "createdAt");
                int c4 = dx0.c(b, "updatedAt");
                int c5 = dx0.c(b, "owner");
                int c6 = dx0.c(b, "productDisplayName");
                int c7 = dx0.c(b, "manufacturer");
                int c8 = dx0.c(b, "softwareRevision");
                int c9 = dx0.c(b, "hardwareRevision");
                int c10 = dx0.c(b, "activationDate");
                int c11 = dx0.c(b, "deviceId");
                int c12 = dx0.c(b, "macAddress");
                int c13 = dx0.c(b, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
                int c14 = dx0.c(b, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
                int c15 = dx0.c(b, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
                int c16 = dx0.c(b, "vibrationStrength");
                int c17 = dx0.c(b, "isActive");
                if (b.moveToFirst()) {
                    device = new Device(b.getString(c11), b.getString(c12), b.getString(c13), b.getString(c14), b.getInt(c15), b.isNull(c16) ? null : Integer.valueOf(b.getInt(c16)), b.getInt(c17) != 0);
                    device.setMajor(b.getInt(c));
                    device.setMinor(b.getInt(c2));
                    device.setCreatedAt(b.getString(c3));
                    device.setUpdatedAt(b.getString(c4));
                    device.setOwner(b.getString(c5));
                    device.setProductDisplayName(b.getString(c6));
                    device.setManufacturer(b.getString(c7));
                    device.setSoftwareRevision(b.getString(c8));
                    device.setHardwareRevision(b.getString(c9));
                    device.setActivationDate(b.getString(c10));
                } else {
                    device = null;
                }
                return device;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public DeviceDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfDevice = new Anon1(qw0);
        this.__preparedStmtOfRemoveDeviceByDeviceId = new Anon2(qw0);
        this.__preparedStmtOfCleanUp = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public void addAllDevice(List<Device> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDevice.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public void addOrUpdateDevice(Device device) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDevice.insert((jw0<Device>) device);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public List<Device> getAllDevice() {
        tw0 f = tw0.f("SELECT * FROM device", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "major");
            int c2 = dx0.c(b, "minor");
            int c3 = dx0.c(b, "createdAt");
            int c4 = dx0.c(b, "updatedAt");
            int c5 = dx0.c(b, "owner");
            int c6 = dx0.c(b, "productDisplayName");
            int c7 = dx0.c(b, "manufacturer");
            int c8 = dx0.c(b, "softwareRevision");
            int c9 = dx0.c(b, "hardwareRevision");
            int c10 = dx0.c(b, "activationDate");
            int c11 = dx0.c(b, "deviceId");
            int c12 = dx0.c(b, "macAddress");
            int c13 = dx0.c(b, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int c14 = dx0.c(b, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
            try {
                int c15 = dx0.c(b, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
                int c16 = dx0.c(b, "vibrationStrength");
                int c17 = dx0.c(b, "isActive");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    Device device = new Device(b.getString(c11), b.getString(c12), b.getString(c13), b.getString(c14), b.getInt(c15), b.isNull(c16) ? null : Integer.valueOf(b.getInt(c16)), b.getInt(c17) != 0);
                    device.setMajor(b.getInt(c));
                    device.setMinor(b.getInt(c2));
                    device.setCreatedAt(b.getString(c3));
                    device.setUpdatedAt(b.getString(c4));
                    device.setOwner(b.getString(c5));
                    device.setProductDisplayName(b.getString(c6));
                    device.setManufacturer(b.getString(c7));
                    device.setSoftwareRevision(b.getString(c8));
                    device.setHardwareRevision(b.getString(c9));
                    device.setActivationDate(b.getString(c10));
                    arrayList.add(device);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public LiveData<List<Device>> getAllDeviceAsLiveData() {
        tw0 f = tw0.f("SELECT * FROM device", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"device"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public Device getDeviceByDeviceId(String str) {
        Throwable th;
        Device device;
        tw0 f = tw0.f("SELECT * FROM device WHERE deviceId=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "major");
            int c2 = dx0.c(b, "minor");
            int c3 = dx0.c(b, "createdAt");
            int c4 = dx0.c(b, "updatedAt");
            int c5 = dx0.c(b, "owner");
            int c6 = dx0.c(b, "productDisplayName");
            int c7 = dx0.c(b, "manufacturer");
            int c8 = dx0.c(b, "softwareRevision");
            int c9 = dx0.c(b, "hardwareRevision");
            int c10 = dx0.c(b, "activationDate");
            int c11 = dx0.c(b, "deviceId");
            int c12 = dx0.c(b, "macAddress");
            int c13 = dx0.c(b, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int c14 = dx0.c(b, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
            try {
                int c15 = dx0.c(b, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
                int c16 = dx0.c(b, "vibrationStrength");
                int c17 = dx0.c(b, "isActive");
                if (b.moveToFirst()) {
                    device = new Device(b.getString(c11), b.getString(c12), b.getString(c13), b.getString(c14), b.getInt(c15), b.isNull(c16) ? null : Integer.valueOf(b.getInt(c16)), b.getInt(c17) != 0);
                    device.setMajor(b.getInt(c));
                    device.setMinor(b.getInt(c2));
                    device.setCreatedAt(b.getString(c3));
                    device.setUpdatedAt(b.getString(c4));
                    device.setOwner(b.getString(c5));
                    device.setProductDisplayName(b.getString(c6));
                    device.setManufacturer(b.getString(c7));
                    device.setSoftwareRevision(b.getString(c8));
                    device.setHardwareRevision(b.getString(c9));
                    device.setActivationDate(b.getString(c10));
                } else {
                    device = null;
                }
                b.close();
                f.m();
                return device;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public LiveData<Device> getDeviceBySerialAsLiveData(String str) {
        tw0 f = tw0.f("SELECT * FROM device WHERE deviceId=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon5 anon5 = new Anon5(f);
        return invalidationTracker.d(new String[]{"device"}, false, anon5);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public void removeDeviceByDeviceId(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfRemoveDeviceByDeviceId.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveDeviceByDeviceId.release(acquire);
        }
    }
}
