package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bw7;
import com.fossil.eu7;
import com.fossil.gj4;
import com.fossil.h47;
import com.fossil.iq5;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public List<SleepSummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    /*
    static {
        String simpleName = SleepSummariesRepository.class.getSimpleName();
        pq7.b(simpleName, "SleepSummariesRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public SleepSummariesRepository(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "mApiService");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public final Object cleanUp(qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new SleepSummariesRepository$cleanUp$Anon2(this, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final Object fetchLastSleepGoal(qn7<? super iq5<MFSleepSettings>> qn7) {
        return eu7.g(bw7.b(), new SleepSummariesRepository$fetchLastSleepGoal$Anon2(this, null), qn7);
    }

    @DexIgnore
    public final Object fetchSleepSummaries(Date date, Date date2, qn7<? super iq5<gj4>> qn7) {
        return eu7.g(bw7.b(), new SleepSummariesRepository$fetchSleepSummaries$Anon2(this, date, date2, null), qn7);
    }

    @DexIgnore
    public final Object getCurrentLastSleepGoal(qn7<? super Integer> qn7) {
        return eu7.g(bw7.b(), new SleepSummariesRepository$getCurrentLastSleepGoal$Anon2(null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00d1  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getLastSleepGoal(com.fossil.qn7<? super java.lang.Integer> r12) {
        /*
        // Method dump skipped, instructions count: 273
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.getLastSleepGoal(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object getSleepStatistic(boolean z, qn7<? super LiveData<h47<SleepStatistic>>> qn7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSleepStatistic - shouldFetch=" + z);
        return new SleepSummariesRepository$getSleepStatistic$Anon2(this, z).asLiveData();
    }

    @DexIgnore
    public final Object getSleepStatisticAwait(qn7<? super SleepStatistic> qn7) {
        return eu7.g(bw7.b(), new SleepSummariesRepository$getSleepStatisticAwait$Anon2(this, null), qn7);
    }

    @DexIgnore
    public final /* synthetic */ Object getSleepStatisticDB(qn7<? super SleepStatistic> qn7) {
        return eu7.g(bw7.b(), new SleepSummariesRepository$getSleepStatisticDB$Anon2(null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSleepSummaries(java.util.Date r11, java.util.Date r12, boolean r13, com.fossil.qn7<? super androidx.lifecycle.LiveData<com.fossil.h47<java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepDay>>>> r14) {
        /*
            r10 = this;
            r9 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon1
            if (r0 == 0) goto L_0x0038
            r0 = r14
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon1 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.label = r1
            r6 = r0
        L_0x0014:
            java.lang.Object r1 = r6.result
            java.lang.Object r7 = com.fossil.yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x0047
            if (r0 != r9) goto L_0x003f
            boolean r0 = r6.Z$0
            java.lang.Object r0 = r6.L$2
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$1
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r0 = (com.portfolio.platform.data.source.SleepSummariesRepository) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0032:
            java.lang.String r1 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.pq7.b(r0, r1)
        L_0x0037:
            return r0
        L_0x0038:
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon1 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon1
            r0.<init>(r10, r14)
            r6 = r0
            goto L_0x0014
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            com.fossil.el7.b(r1)
            com.fossil.jx7 r8 = com.fossil.bw7.c()
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2
            r5 = 0
            r1 = r10
            r2 = r11
            r3 = r12
            r4 = r13
            r0.<init>(r1, r2, r3, r4, r5)
            r6.L$0 = r10
            r6.L$1 = r11
            r6.L$2 = r12
            r6.Z$0 = r13
            r6.label = r9
            java.lang.Object r0 = com.fossil.eu7.g(r8, r0, r6)
            if (r0 != r7) goto L_0x0032
            r0 = r7
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.getSleepSummaries(java.util.Date, java.util.Date, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSleepSummary(java.util.Date r7, com.fossil.qn7<? super androidx.lifecycle.LiveData<com.fossil.h47<com.portfolio.platform.data.model.room.sleep.MFSleepDay>>> r8) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon1
            if (r0 == 0) goto L_0x0032
            r0 = r8
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon1 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0041
            if (r0 != r5) goto L_0x0039
            java.lang.Object r0 = r1.L$1
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r0 = (com.portfolio.platform.data.source.SleepSummariesRepository) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x002c:
            java.lang.String r1 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.pq7.b(r0, r1)
        L_0x0031:
            return r0
        L_0x0032:
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon1 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon1
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r2)
            com.fossil.jx7 r0 = com.fossil.bw7.c()
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon2 r2 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon2
            r4 = 0
            r2.<init>(r6, r7, r4)
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r5
            java.lang.Object r0 = com.fossil.eu7.g(r0, r2, r1)
            if (r0 != r3) goto L_0x002c
            r0 = r3
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.getSleepSummary(java.util.Date, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object getSleepSummaryFromDb(Date date, qn7<? super MFSleepDay> qn7) {
        return eu7.g(bw7.b(), new SleepSummariesRepository$getSleepSummaryFromDb$Anon2(date, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSummariesPaging(com.portfolio.platform.data.source.SleepSessionsRepository r12, com.portfolio.platform.data.source.FitnessDataRepository r13, java.util.Date r14, com.fossil.no4 r15, com.fossil.fl5.a r16, com.fossil.qn7<? super com.portfolio.platform.data.Listing<com.portfolio.platform.data.SleepSummary>> r17) {
        /*
        // Method dump skipped, instructions count: 311
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.getSummariesPaging(com.portfolio.platform.data.source.SleepSessionsRepository, com.portfolio.platform.data.source.FitnessDataRepository, java.util.Date, com.fossil.no4, com.fossil.fl5$a, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object pushLastSleepGoalToServer(int i, qn7<? super iq5<MFSleepSettings>> qn7) {
        return eu7.g(bw7.b(), new SleepSummariesRepository$pushLastSleepGoalToServer$Anon2(this, i, null), qn7);
    }

    @DexIgnore
    public final void removePagingListener() {
        for (SleepSummaryDataSourceFactory sleepSummaryDataSourceFactory : this.mSourceFactoryList) {
            SleepSummaryLocalDataSource localSource = sleepSummaryDataSourceFactory.getLocalSource();
            if (localSource != null) {
                localSource.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveSleepSettingToDB(int r11, com.fossil.qn7<? super com.fossil.tl7> r12) {
        /*
            r10 = this;
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r8 = 1
            r3 = 0
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSettingToDB$Anon1
            if (r0 == 0) goto L_0x0085
            r0 = r12
            com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSettingToDB$Anon1 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSettingToDB$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x0085
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0093
            if (r4 != r8) goto L_0x008b
            int r2 = r0.I$0
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r0 = (com.portfolio.platform.data.source.SleepSummariesRepository) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x002a:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r0
            com.portfolio.platform.data.source.local.sleep.SleepDao r7 = r0.sleepDao()
            com.portfolio.platform.data.model.room.sleep.MFSleepSettings r0 = r7.getSleepSettings()
            if (r0 != 0) goto L_0x00c2
            com.portfolio.platform.data.model.room.sleep.MFSleepSettings r0 = new com.portfolio.platform.data.model.room.sleep.MFSleepSettings
            r0.<init>(r2)
            r0.setId(r8)
        L_0x003e:
            r7.upsertSleepSettings(r0)
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            com.portfolio.platform.data.model.room.sleep.MFSleepDay r0 = r7.getSleepDay(r0)
            if (r0 != 0) goto L_0x00c7
            com.portfolio.platform.data.model.room.sleep.SleepDistribution r4 = new com.portfolio.platform.data.model.room.sleep.SleepDistribution
            r4.<init>(r3, r3, r3)
            java.util.Date r1 = new java.util.Date
            r1.<init>()
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            java.lang.String r5 = "Calendar.getInstance()"
            com.fossil.pq7.b(r0, r5)
            org.joda.time.DateTime r5 = new org.joda.time.DateTime
            long r8 = r0.getTimeInMillis()
            r5.<init>(r8)
            java.util.Calendar r8 = java.util.Calendar.getInstance()
            java.lang.String r0 = "Calendar.getInstance()"
            com.fossil.pq7.b(r8, r0)
            com.portfolio.platform.data.model.room.sleep.MFSleepDay r0 = new com.portfolio.platform.data.model.room.sleep.MFSleepDay
            org.joda.time.DateTime r6 = new org.joda.time.DateTime
            long r8 = r8.getTimeInMillis()
            r6.<init>(r8)
            r0.<init>(r1, r2, r3, r4, r5, r6)
        L_0x007f:
            r7.upsertSleepDay(r0)
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0084:
            return r0
        L_0x0085:
            com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSettingToDB$Anon1 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSettingToDB$Anon1
            r0.<init>(r10, r12)
            goto L_0x0014
        L_0x008b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0093:
            com.fossil.el7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "save sleep goal "
            r5.append(r6)
            r5.append(r11)
            java.lang.String r5 = r5.toString()
            r1.d(r4, r5)
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r0.L$0 = r10
            r0.I$0 = r11
            r0.label = r8
            java.lang.Object r0 = r1.D(r0)
            if (r0 != r2) goto L_0x00cb
            r0 = r2
            goto L_0x0084
        L_0x00c2:
            r0.setSleepGoal(r2)
            goto L_0x003e
        L_0x00c7:
            r0.setGoalMinutes(r2)
            goto L_0x007f
        L_0x00cb:
            r2 = r11
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.saveSleepSettingToDB(int, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveSleepSummaries(com.fossil.gj4 r14, java.util.Date r15, java.util.Date r16, com.fossil.cl7<? extends java.util.Date, ? extends java.util.Date> r17, com.fossil.qn7<? super com.fossil.tl7> r18) {
        /*
        // Method dump skipped, instructions count: 386
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.saveSleepSummaries(com.fossil.gj4, java.util.Date, java.util.Date, com.fossil.cl7, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveSleepSummary(com.fossil.gj4 r13, java.util.Date r14, com.fossil.qn7<? super com.fossil.tl7> r15) {
        /*
        // Method dump skipped, instructions count: 338
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.saveSleepSummary(com.fossil.gj4, java.util.Date, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object updateLastSleepGoal(int r11, com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.model.room.sleep.MFSleepSettings>> r12) {
        /*
        // Method dump skipped, instructions count: 292
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.updateLastSleepGoal(int, com.fossil.qn7):java.lang.Object");
    }
}
