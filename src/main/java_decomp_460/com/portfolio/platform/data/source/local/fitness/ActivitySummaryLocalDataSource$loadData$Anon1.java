package com.portfolio.platform.data.source.local.fitness;

import com.fossil.eo7;
import com.fossil.fl5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource$loadData$1", f = "ActivitySummaryLocalDataSource.kt", l = {181, 185}, m = "invokeSuspend")
public final class ActivitySummaryLocalDataSource$loadData$Anon1 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ fl5.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ fl5.d $requestType;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitySummaryLocalDataSource$loadData$Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource, Date date, Date date2, fl5.d dVar, fl5.b.a aVar, qn7 qn7) {
        super(2, qn7);
        this.this$0 = activitySummaryLocalDataSource;
        this.$startDate = date;
        this.$endDate = date2;
        this.$requestType = dVar;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ActivitySummaryLocalDataSource$loadData$Anon1 activitySummaryLocalDataSource$loadData$Anon1 = new ActivitySummaryLocalDataSource$loadData$Anon1(this.this$0, this.$startDate, this.$endDate, this.$requestType, this.$helperCallback, qn7);
        activitySummaryLocalDataSource$loadData$Anon1.p$ = (iv7) obj;
        return activitySummaryLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((ActivitySummaryLocalDataSource$loadData$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e1  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r9) {
        /*
        // Method dump skipped, instructions count: 235
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource$loadData$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
