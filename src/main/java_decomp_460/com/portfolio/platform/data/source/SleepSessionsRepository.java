package com.portfolio.platform.data.source;

import com.fossil.bw7;
import com.fossil.eu7;
import com.fossil.gj4;
import com.fossil.iq5;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionsRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ UserRepository mUserRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingSleepSessionsCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(List<MFSleepSession> list);
    }

    /*
    static {
        String simpleName = SleepSessionsRepository.class.getSimpleName();
        pq7.b(simpleName, "SleepSessionsRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public SleepSessionsRepository(UserRepository userRepository, ApiServiceV2 apiServiceV2) {
        pq7.c(userRepository, "mUserRepository");
        pq7.c(apiServiceV2, "mApiService");
        this.mUserRepository = userRepository;
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchSleepSessions$default(SleepSessionsRepository sleepSessionsRepository, Date date, Date date2, int i, int i2, qn7 qn7, int i3, Object obj) {
        return sleepSessionsRepository.fetchSleepSessions(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, qn7);
    }

    @DexIgnore
    public final Object cleanUp(qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new SleepSessionsRepository$cleanUp$Anon2(null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadRecommendedGoals(int r10, int r11, int r12, java.lang.String r13, com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal>> r14) {
        /*
            r9 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$Anon1
            if (r0 == 0) goto L_0x004d
            r0 = r14
            com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$Anon1 r0 = (com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = r2 & r1
            if (r2 == 0) goto L_0x004d
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            int r1 = r1 + r2
            r0.label = r1
            r7 = r0
        L_0x0014:
            java.lang.Object r1 = r7.result
            java.lang.Object r8 = com.fossil.yn7.d()
            int r0 = r7.label
            if (r0 == 0) goto L_0x005c
            r2 = 1
            if (r0 != r2) goto L_0x0054
            java.lang.Object r0 = r7.L$1
            java.lang.String r0 = (java.lang.String) r0
            int r0 = r7.I$2
            int r0 = r7.I$1
            int r0 = r7.I$0
            java.lang.Object r0 = r7.L$0
            com.portfolio.platform.data.source.SleepSessionsRepository r0 = (com.portfolio.platform.data.source.SleepSessionsRepository) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0033:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0084
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r0 = r0.a()
            if (r0 == 0) goto L_0x007f
            com.fossil.kq5 r1 = new com.fossil.kq5
            com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal r0 = (com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal) r0
            r2 = 0
            r3 = 2
            r4 = 0
            r1.<init>(r0, r2, r3, r4)
            r0 = r1
        L_0x004c:
            return r0
        L_0x004d:
            com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$Anon1 r0 = new com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$Anon1
            r0.<init>(r9, r14)
            r7 = r0
            goto L_0x0014
        L_0x0054:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005c:
            com.fossil.el7.b(r1)
            com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$response$Anon1 r0 = new com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$response$Anon1
            r6 = 0
            r1 = r9
            r2 = r10
            r3 = r11
            r4 = r12
            r5 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r7.L$0 = r9
            r7.I$0 = r10
            r7.I$1 = r11
            r7.I$2 = r12
            r7.L$1 = r13
            r1 = 1
            r7.label = r1
            java.lang.Object r0 = com.fossil.jq5.d(r0, r7)
            if (r0 != r8) goto L_0x0033
            r0 = r8
            goto L_0x004c
        L_0x007f:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        L_0x0084:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x009f
            r2 = r0
            com.fossil.hq5 r2 = (com.fossil.hq5) r2
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r2.a()
            com.portfolio.platform.data.model.ServerError r2 = r2.c()
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 16
            r7 = 0
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x004c
        L_0x009f:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.downloadRecommendedGoals(int, int, int, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object fetchSleepSessions(Date date, Date date2, int i, int i2, qn7<? super iq5<gj4>> qn7) {
        return eu7.g(bw7.b(), new SleepSessionsRepository$fetchSleepSessions$Anon2(this, date, date2, i, i2, null), qn7);
    }

    @DexIgnore
    public final Object getPendingSleepSessions(Date date, Date date2, qn7<? super List<MFSleepSession>> qn7) {
        return eu7.g(bw7.b(), new SleepSessionsRepository$getPendingSleepSessions$Anon2(date, date2, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSleepSessionList(java.util.Date r11, java.util.Date r12, boolean r13, com.fossil.qn7<? super androidx.lifecycle.LiveData<com.fossil.h47<java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession>>>> r14) {
        /*
            r10 = this;
            r9 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon1
            if (r0 == 0) goto L_0x0038
            r0 = r14
            com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon1 r0 = (com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.label = r1
            r6 = r0
        L_0x0014:
            java.lang.Object r1 = r6.result
            java.lang.Object r7 = com.fossil.yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x0047
            if (r0 != r9) goto L_0x003f
            boolean r0 = r6.Z$0
            java.lang.Object r0 = r6.L$2
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$1
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$0
            com.portfolio.platform.data.source.SleepSessionsRepository r0 = (com.portfolio.platform.data.source.SleepSessionsRepository) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0032:
            java.lang.String r1 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.pq7.b(r0, r1)
        L_0x0037:
            return r0
        L_0x0038:
            com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon1 r0 = new com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon1
            r0.<init>(r10, r14)
            r6 = r0
            goto L_0x0014
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            com.fossil.el7.b(r1)
            com.fossil.jx7 r8 = com.fossil.bw7.c()
            com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2 r0 = new com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2
            r5 = 0
            r1 = r10
            r2 = r11
            r3 = r12
            r4 = r13
            r0.<init>(r1, r2, r3, r4, r5)
            r6.L$0 = r10
            r6.L$1 = r11
            r6.L$2 = r12
            r6.Z$0 = r13
            r6.label = r9
            java.lang.Object r0 = com.fossil.eu7.g(r8, r0, r6)
            if (r0 != r7) goto L_0x0032
            r0 = r7
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.getSleepSessionList(java.util.Date, java.util.Date, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object insert(java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession> r11, com.fossil.qn7<? super com.fossil.tl7> r12) {
        /*
        // Method dump skipped, instructions count: 379
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.insert(java.util.List, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object insertFromDevice(List<MFSleepSession> list, qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new SleepSessionsRepository$insertFromDevice$Anon2(this, list, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0133  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object insertSleepSessionList(java.lang.String r13, java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession> r14, com.fossil.qn7<? super com.fossil.iq5<java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession>>> r15) {
        /*
        // Method dump skipped, instructions count: 398
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.insertSleepSessionList(java.lang.String, java.util.List, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object isExistsSleepSession(com.portfolio.platform.data.model.room.sleep.MFSleepSession r12, com.fossil.qn7<? super java.lang.Boolean> r13) {
        /*
            r11 = this;
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r10 = 1
            boolean r0 = r13 instanceof com.portfolio.platform.data.source.SleepSessionsRepository$isExistsSleepSession$Anon1
            if (r0 == 0) goto L_0x0089
            r0 = r13
            com.portfolio.platform.data.source.SleepSessionsRepository$isExistsSleepSession$Anon1 r0 = (com.portfolio.platform.data.source.SleepSessionsRepository$isExistsSleepSession$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0089
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0098
            if (r3 != r10) goto L_0x0090
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.data.model.room.sleep.MFSleepSession r0 = (com.portfolio.platform.data.model.room.sleep.MFSleepSession) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.SleepSessionsRepository r1 = (com.portfolio.platform.data.source.SleepSessionsRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r12 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r0
            com.portfolio.platform.data.source.local.sleep.SleepDao r0 = r0.sleepDao()
            java.util.Date r1 = r12.getDay()
            long r2 = r1.getTime()
            java.util.List r0 = r0.getSleepSessions(r2)
            int r1 = r12.getStartTime()
            long r2 = (long) r1
            int r1 = r12.getEndTime()
            long r4 = (long) r1
            java.util.Iterator r1 = r0.iterator()
        L_0x004e:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00aa
            java.lang.Object r0 = r1.next()
            com.portfolio.platform.data.model.room.sleep.MFSleepSession r0 = (com.portfolio.platform.data.model.room.sleep.MFSleepSession) r0
            int r6 = r0.getStartTime()
            long r6 = (long) r6
            int r0 = r0.getEndTime()
            long r8 = (long) r0
            int r0 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r0 > 0) goto L_0x006c
            int r0 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x0084
        L_0x006c:
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 > 0) goto L_0x0074
            int r0 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r0 >= 0) goto L_0x0084
        L_0x0074:
            int r0 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r0 > 0) goto L_0x007c
            int r0 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0084
        L_0x007c:
            int r0 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r0 > 0) goto L_0x004e
            int r0 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r0 < 0) goto L_0x004e
        L_0x0084:
            java.lang.Boolean r0 = com.fossil.ao7.a(r10)
        L_0x0088:
            return r0
        L_0x0089:
            com.portfolio.platform.data.source.SleepSessionsRepository$isExistsSleepSession$Anon1 r0 = new com.portfolio.platform.data.source.SleepSessionsRepository$isExistsSleepSession$Anon1
            r0.<init>(r11, r13)
            r1 = r0
            goto L_0x0014
        L_0x0090:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0098:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r11
            r1.L$1 = r12
            r1.label = r10
            java.lang.Object r1 = r2.D(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0088
        L_0x00aa:
            r0 = 0
            java.lang.Boolean r0 = com.fossil.ao7.a(r0)
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.isExistsSleepSession(com.portfolio.platform.data.model.room.sleep.MFSleepSession, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object pushPendingSleepSessions(PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback, qn7<? super tl7> qn7) {
        return eu7.g(bw7.b(), new SleepSessionsRepository$pushPendingSleepSessions$Anon2(this, pushPendingSleepSessionsCallback, null), qn7);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v33, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x013d  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0179  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0204  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0213  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0336  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0340  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x034c  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveSleepSessionsToServer(java.lang.String r22, java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession> r23, com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback r24, com.fossil.qn7<? super com.fossil.tl7> r25) {
        /*
        // Method dump skipped, instructions count: 864
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.saveSleepSessionsToServer(java.lang.String, java.util.List, com.portfolio.platform.data.source.SleepSessionsRepository$PushPendingSleepSessionsCallback, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object upsertRecommendedGoals(SleepRecommendedGoal sleepRecommendedGoal, qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new SleepSessionsRepository$upsertRecommendedGoals$Anon2(sleepRecommendedGoal, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }
}
