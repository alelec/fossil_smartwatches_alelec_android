package com.portfolio.platform.data.source.remote;

import com.fossil.gj4;
import com.fossil.i98;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.t98;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface AuthApiUserService {
    @DexIgnore
    @t98("rpc/auth/password/change")
    Call<gj4> changePassword(@i98 gj4 gj4);

    @DexIgnore
    @t98("rpc/auth/logout")
    Object logout(@i98 gj4 gj4, qn7<? super q88<Void>> qn7);
}
