package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.a05;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.iw0;
import com.fossil.jw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.xw0;
import com.fossil.xz4;
import com.fossil.yz4;
import com.fossil.zz4;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitWorkoutSessionDao_Impl implements GFitWorkoutSessionDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ iw0<GFitWorkoutSession> __deletionAdapterOfGFitWorkoutSession;
    @DexIgnore
    public /* final */ xz4 __gFitWOCaloriesConverter; // = new xz4();
    @DexIgnore
    public /* final */ yz4 __gFitWODistancesConverter; // = new yz4();
    @DexIgnore
    public /* final */ zz4 __gFitWOHeartRatesConverter; // = new zz4();
    @DexIgnore
    public /* final */ a05 __gFitWOStepsConverter; // = new a05();
    @DexIgnore
    public /* final */ jw0<GFitWorkoutSession> __insertionAdapterOfGFitWorkoutSession;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<GFitWorkoutSession> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, GFitWorkoutSession gFitWorkoutSession) {
            px0.bindLong(1, (long) gFitWorkoutSession.getId());
            px0.bindLong(2, gFitWorkoutSession.getStartTime());
            px0.bindLong(3, gFitWorkoutSession.getEndTime());
            px0.bindLong(4, (long) gFitWorkoutSession.getWorkoutType());
            String b = GFitWorkoutSessionDao_Impl.this.__gFitWOStepsConverter.b(gFitWorkoutSession.getSteps());
            if (b == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, b);
            }
            String b2 = GFitWorkoutSessionDao_Impl.this.__gFitWOCaloriesConverter.b(gFitWorkoutSession.getCalories());
            if (b2 == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, b2);
            }
            String b3 = GFitWorkoutSessionDao_Impl.this.__gFitWODistancesConverter.b(gFitWorkoutSession.getDistances());
            if (b3 == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, b3);
            }
            String b4 = GFitWorkoutSessionDao_Impl.this.__gFitWOHeartRatesConverter.b(gFitWorkoutSession.getHeartRates());
            if (b4 == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, b4);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitWorkoutSession` (`id`,`startTime`,`endTime`,`workoutType`,`steps`,`calories`,`distances`,`heartRates`) VALUES (nullif(?, 0),?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends iw0<GFitWorkoutSession> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, GFitWorkoutSession gFitWorkoutSession) {
            px0.bindLong(1, (long) gFitWorkoutSession.getId());
        }

        @DexIgnore
        @Override // com.fossil.xw0, com.fossil.iw0
        public String createQuery() {
            return "DELETE FROM `gFitWorkoutSession` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM gFitWorkoutSession";
        }
    }

    @DexIgnore
    public GFitWorkoutSessionDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfGFitWorkoutSession = new Anon1(qw0);
        this.__deletionAdapterOfGFitWorkoutSession = new Anon2(qw0);
        this.__preparedStmtOfClearAll = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public void deleteGFitWorkoutSession(GFitWorkoutSession gFitWorkoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitWorkoutSession.handle(gFitWorkoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public List<GFitWorkoutSession> getAllGFitWorkoutSession() {
        tw0 f = tw0.f("SELECT * FROM gFitWorkoutSession", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c3 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
            int c4 = dx0.c(b, "workoutType");
            int c5 = dx0.c(b, "steps");
            int c6 = dx0.c(b, "calories");
            int c7 = dx0.c(b, "distances");
            int c8 = dx0.c(b, "heartRates");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GFitWorkoutSession gFitWorkoutSession = new GFitWorkoutSession(b.getLong(c2), b.getLong(c3), b.getInt(c4), this.__gFitWOStepsConverter.a(b.getString(c5)), this.__gFitWOCaloriesConverter.a(b.getString(c6)), this.__gFitWODistancesConverter.a(b.getString(c7)), this.__gFitWOHeartRatesConverter.a(b.getString(c8)));
                gFitWorkoutSession.setId(b.getInt(c));
                arrayList.add(gFitWorkoutSession);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public void insertGFitWorkoutSession(GFitWorkoutSession gFitWorkoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitWorkoutSession.insert((jw0<GFitWorkoutSession>) gFitWorkoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public void insertListGFitWorkoutSession(List<GFitWorkoutSession> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitWorkoutSession.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
