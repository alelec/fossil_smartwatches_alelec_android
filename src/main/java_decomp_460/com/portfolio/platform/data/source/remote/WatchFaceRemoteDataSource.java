package com.portfolio.platform.data.source.remote;

import com.fossil.iq5;
import com.fossil.jq5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.w18;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceRemoteDataSource {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 api;

    @DexIgnore
    public WatchFaceRemoteDataSource(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        this.api = apiServiceV2;
        String simpleName = WatchFaceRemoteDataSource.class.getSimpleName();
        pq7.b(simpleName, "WatchFaceRemoteDataSource::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    public final Object downloadWatchFaceFile(String str, String str2, qn7<? super iq5<w18>> qn7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = this.TAG;
        local.d(str3, "download: " + str + " path: " + str2);
        return jq5.d(new WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2(this, str, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWatchFacesFromServer(java.lang.String r9, com.fossil.qn7<? super com.fossil.iq5<java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.WatchFace>>> r10) {
        /*
            r8 = this;
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1
            if (r0 == 0) goto L_0x0087
            r0 = r10
            com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1 r0 = (com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x0087
            int r1 = r1 + r4
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r4 = r1.label
            if (r4 == 0) goto L_0x0096
            if (r4 != r5) goto L_0x008e
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource) r0
            com.fossil.el7.b(r2)
            r1 = r2
            r8 = r0
        L_0x002e:
            r0 = r1
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x00af
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r1.getLocal()
            java.lang.String r4 = r8.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r1 = "data: "
            r5.append(r1)
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r1 = r0.a()
            if (r1 == 0) goto L_0x00ab
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            java.util.List r1 = r1.get_items()
            r5.append(r1)
            java.lang.String r1 = " - isFromCache: "
            r5.append(r1)
            boolean r1 = r0.b()
            r5.append(r1)
            java.lang.String r1 = r5.toString()
            r2.d(r4, r1)
            java.lang.Object r1 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            java.util.List r1 = r1.get_items()
            boolean r2 = r1 instanceof java.util.ArrayList
            if (r2 != 0) goto L_0x007a
            r1 = r3
        L_0x007a:
            com.fossil.kq5 r2 = new com.fossil.kq5
            java.util.ArrayList r1 = (java.util.ArrayList) r1
            boolean r0 = r0.b()
            r2.<init>(r1, r0)
            r0 = r2
        L_0x0086:
            return r0
        L_0x0087:
            com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1 r0 = new com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x008e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0096:
            com.fossil.el7.b(r2)
            com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1 r2 = new com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1
            r2.<init>(r8, r9, r3)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.label = r5
            java.lang.Object r1 = com.fossil.jq5.d(r2, r1)
            if (r1 != r0) goto L_0x002e
            goto L_0x0086
        L_0x00ab:
            com.fossil.pq7.i()
            throw r3
        L_0x00af:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00c9
            r2 = r0
            com.fossil.hq5 r2 = (com.fossil.hq5) r2
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r2.a()
            com.portfolio.platform.data.model.ServerError r2 = r2.c()
            r6 = 28
            r4 = r3
            r5 = r3
            r7 = r3
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0086
        L_0x00c9:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource.getWatchFacesFromServer(java.lang.String, com.fossil.qn7):java.lang.Object");
    }
}
