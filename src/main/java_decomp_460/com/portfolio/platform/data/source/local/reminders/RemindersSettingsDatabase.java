package com.portfolio.platform.data.source.local.reminders;

import com.fossil.qw0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class RemindersSettingsDatabase extends qw0 {
    @DexIgnore
    public abstract InactivityNudgeTimeDao getInactivityNudgeTimeDao();

    @DexIgnore
    public abstract RemindTimeDao getRemindTimeDao();
}
