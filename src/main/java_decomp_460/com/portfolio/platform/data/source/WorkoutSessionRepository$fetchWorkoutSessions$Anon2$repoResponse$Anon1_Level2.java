package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.ko7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.WorkoutSessionRepository$fetchWorkoutSessions$2$repoResponse$1", f = "WorkoutSessionRepository.kt", l = {204}, m = "invokeSuspend")
public final class WorkoutSessionRepository$fetchWorkoutSessions$Anon2$repoResponse$Anon1_Level2 extends ko7 implements rp7<qn7<? super q88<ApiResponse<ServerWorkoutSession>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository$fetchWorkoutSessions$Anon2 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$fetchWorkoutSessions$Anon2$repoResponse$Anon1_Level2(WorkoutSessionRepository$fetchWorkoutSessions$Anon2 workoutSessionRepository$fetchWorkoutSessions$Anon2, qn7 qn7) {
        super(1, qn7);
        this.this$0 = workoutSessionRepository$fetchWorkoutSessions$Anon2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(qn7<?> qn7) {
        pq7.c(qn7, "completion");
        return new WorkoutSessionRepository$fetchWorkoutSessions$Anon2$repoResponse$Anon1_Level2(this.this$0, qn7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public final Object invoke(qn7<? super q88<ApiResponse<ServerWorkoutSession>>> qn7) {
        return ((WorkoutSessionRepository$fetchWorkoutSessions$Anon2$repoResponse$Anon1_Level2) create(qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.this$0.mApiService;
            String k = lk5.k(this.this$0.$start);
            pq7.b(k, "DateHelper.formatShortDate(start)");
            String k2 = lk5.k(this.this$0.$end);
            pq7.b(k2, "DateHelper.formatShortDate(end)");
            WorkoutSessionRepository$fetchWorkoutSessions$Anon2 workoutSessionRepository$fetchWorkoutSessions$Anon2 = this.this$0;
            int i2 = workoutSessionRepository$fetchWorkoutSessions$Anon2.$offset;
            int i3 = workoutSessionRepository$fetchWorkoutSessions$Anon2.$limit;
            this.label = 1;
            Object workoutSessions = apiServiceV2.getWorkoutSessions(k, k2, i2, i3, this);
            return workoutSessions == d ? d : workoutSessions;
        } else if (i == 1) {
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
