package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.facebook.share.internal.VideoUploader;
import com.fossil.au0;
import com.fossil.bw7;
import com.fossil.cl7;
import com.fossil.fl5;
import com.fossil.gl5;
import com.fossil.gu7;
import com.fossil.hm7;
import com.fossil.jv7;
import com.fossil.kq7;
import com.fossil.lk5;
import com.fossil.mx0;
import com.fossil.no4;
import com.fossil.nw0;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.sk5;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.fossil.xw7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryLocalDataSource extends au0<Date, ActivitySummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ fl5.a listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public /* final */ sk5 mFitnessHelper;
    @DexIgnore
    public fl5 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ nw0.c mObserver;
    @DexIgnore
    public List<cl7<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public Date mStartDate; // = new Date();
    @DexIgnore
    public /* final */ SummariesRepository mSummariesRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends nw0.c {
        @DexIgnore
        public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = activitySummaryLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.nw0.c
        public void onInvalidated(Set<String> set) {
            pq7.c(set, "tables");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ActivitySummaryLocalDataSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "XXX-invalidate " + set);
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            pq7.c(date, "date");
            pq7.c(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(getTAG$app_fossilRelease(), "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar I = lk5.I(instance);
            if (lk5.j0(date2, I.getTime())) {
                I.setTime(date2);
            }
            Date time = I.getTime();
            pq7.b(time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ActivitySummaryLocalDataSource.TAG;
        }
    }

    /*
    static {
        String simpleName = ActivitySummaryLocalDataSource.class.getSimpleName();
        pq7.b(simpleName, "ActivitySummaryLocalData\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ActivitySummaryLocalDataSource(SummariesRepository summariesRepository, sk5 sk5, FitnessDataRepository fitnessDataRepository, FitnessDatabase fitnessDatabase, Date date, no4 no4, fl5.a aVar, Calendar calendar) {
        pq7.c(summariesRepository, "mSummariesRepository");
        pq7.c(sk5, "mFitnessHelper");
        pq7.c(fitnessDataRepository, "mFitnessDataRepository");
        pq7.c(fitnessDatabase, "mFitnessDatabase");
        pq7.c(date, "mCreatedDate");
        pq7.c(no4, "appExecutors");
        pq7.c(aVar, "listener");
        pq7.c(calendar, "key");
        this.mSummariesRepository = summariesRepository;
        this.mFitnessHelper = sk5;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mFitnessDatabase = fitnessDatabase;
        this.mCreatedDate = date;
        this.listener = aVar;
        this.key = calendar;
        fl5 fl5 = new fl5(no4.a());
        this.mHelper = fl5;
        this.mNetworkState = gl5.b(fl5);
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, SampleDay.TABLE_NAME, new String[0]);
        this.mFitnessDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar I = lk5.I(instance);
        I.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + I.getTime());
        Date time = I.getTime();
        pq7.b(time, "calendar.time");
        this.mStartDate = time;
        if (lk5.j0(date, time)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final void calculateSummaries(List<ActivitySummary> list) {
        int i;
        int i2;
        int i3;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "endCalendar");
            instance.setTime(((ActivitySummary) pm7.P(list)).getDate());
            if (instance.get(7) != 1) {
                Calendar b0 = lk5.b0(instance.getTime());
                pq7.b(b0, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                ActivitySummaryDao activitySummaryDao = this.mFitnessDatabase.activitySummaryDao();
                Date time = b0.getTime();
                pq7.b(time, "startCalendar.time");
                Date time2 = instance.getTime();
                pq7.b(time2, "endCalendar.time");
                ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = activitySummaryDao.getTotalValuesOfWeek(time, time2);
                i = totalValuesOfWeek.getTotalActiveTimeOfWeek();
                i2 = (int) totalValuesOfWeek.getTotalCaloriesOfWeek();
                i3 = (int) totalValuesOfWeek.getTotalStepsOfWeek();
            } else {
                i = 0;
                i2 = 0;
                i3 = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            pq7.b(instance2, "calendar");
            instance2.setTime(((ActivitySummary) pm7.F(list)).getDate());
            Calendar b02 = lk5.b0(instance2.getTime());
            pq7.b(b02, "DateHelper.getStartOfWeek(calendar.time)");
            b02.add(5, -1);
            int i4 = 0;
            int i5 = 0;
            double d = 0.0d;
            double d2 = 0.0d;
            int i6 = 0;
            for (T t : list) {
                if (i5 >= 0) {
                    T t2 = t;
                    if (lk5.m0(t2.getDate(), b02.getTime())) {
                        list.get(i4).setTotalValuesOfWeek(new ActivitySummary.TotalValuesOfWeek(d, d2, i6));
                        b02.add(5, -7);
                        d = 0.0d;
                        d2 = 0.0d;
                        i6 = 0;
                        i4 = i5;
                    }
                    d += t2.getSteps();
                    d2 += t2.getCalories();
                    i6 += t2.getActiveTime();
                    if (i5 == list.size() - 1) {
                        d += (double) i3;
                        d2 += (double) i2;
                        i6 += i;
                    }
                    i5++;
                } else {
                    hm7.l();
                    throw null;
                }
            }
            list.get(i4).setTotalValuesOfWeek(new ActivitySummary.TotalValuesOfWeek(d, d2, i6));
        }
        FLogger.INSTANCE.getLocal().d(TAG, "calculateSummaries summaries.size=" + list.size());
    }

    @DexIgnore
    private final ActivitySummary dummySummary(ActivitySummary activitySummary, Date date) {
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "calendar");
        instance.setTime(date);
        int i = instance.get(1);
        int i2 = instance.get(2);
        ActivitySummary activitySummary2 = new ActivitySummary(i, i2 + 1, instance.get(5), activitySummary.getTimezoneName(), activitySummary.getDstOffset(), 0.0d, 0.0d, 0.0d, hm7.i(0, 0, 0), 0, 0, 0, 0, 7680, null);
        activitySummary2.setCreatedAt(DateTime.now());
        activitySummary2.setUpdatedAt(DateTime.now());
        return activitySummary2;
    }

    @DexIgnore
    private final List<ActivitySummary> getDataInDatabase(Date date, Date date2) {
        ActivitySummary activitySummary;
        ActivitySummary activitySummary2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("getDataInDatabase - startDate=");
        sb.append(date);
        sb.append(", endDate=");
        sb.append(date2);
        sb.append(" DBNAME=");
        mx0 openHelper = this.mFitnessDatabase.getOpenHelper();
        pq7.b(openHelper, "mFitnessDatabase.openHelper");
        sb.append(openHelper.getDatabaseName());
        local.d(str, sb.toString());
        List<ActivitySummary> activitySummariesDesc = this.mFitnessDatabase.activitySummaryDao().getActivitySummariesDesc(lk5.j0(date, date2) ? date2 : date, date2);
        if (!activitySummariesDesc.isEmpty()) {
            Boolean p0 = lk5.p0(((ActivitySummary) pm7.F(activitySummariesDesc)).getDate());
            pq7.b(p0, "DateHelper.isToday(summaries.first().getDate())");
            if (p0.booleanValue()) {
                ((ActivitySummary) pm7.F(activitySummariesDesc)).setSteps(Math.max((double) this.mFitnessHelper.d(new Date()), ((ActivitySummary) pm7.F(activitySummariesDesc)).getSteps()));
            }
        }
        calculateSummaries(activitySummariesDesc);
        ArrayList arrayList = new ArrayList();
        Date lastDate = this.mFitnessDatabase.activitySummaryDao().getLastDate();
        Date date3 = lastDate != null ? lastDate : date;
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(activitySummariesDesc);
        ActivitySummary activitySummary3 = this.mFitnessDatabase.activitySummaryDao().getActivitySummary(date2);
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "endCalendar");
        instance.setTime(date2);
        if (activitySummary3 == null) {
            ActivitySummary activitySummary4 = new ActivitySummary(instance.get(1), instance.get(2) + 1, instance.get(5), "", 0, 0.0d, 0.0d, 0.0d, hm7.i(0, 0, 0), 0, 0, 0, 0, 7680, null);
            activitySummary4.setActiveTimeGoal(30);
            activitySummary4.setStepGoal(VideoUploader.RETRY_DELAY_UNIT_MS);
            activitySummary4.setCaloriesGoal(ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            activitySummary = activitySummary4;
        } else {
            activitySummary = activitySummary3;
        }
        if (!lk5.j0(date, date3)) {
            date = date3;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "getDataInDatabase - summaries.size=" + activitySummariesDesc.size() + ", summaryParent=" + activitySummary + ", lastDate=" + date3 + ", startDateToFill=" + date);
        while (lk5.k0(date2, date)) {
            Iterator it = arrayList2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    activitySummary2 = null;
                    break;
                }
                Object next = it.next();
                if (lk5.m0(((ActivitySummary) next).getDate(), date2)) {
                    activitySummary2 = next;
                    break;
                }
            }
            ActivitySummary activitySummary5 = activitySummary2;
            if (activitySummary5 == null) {
                arrayList.add(dummySummary(activitySummary, date2));
            } else {
                arrayList.add(activitySummary5);
                arrayList2.remove(activitySummary5);
            }
            date2 = lk5.P(date2);
            pq7.b(date2, "DateHelper.getPrevDate(endDateToFill)");
        }
        if (!arrayList.isEmpty()) {
            ActivitySummary activitySummary6 = (ActivitySummary) pm7.F(arrayList);
            Boolean p02 = lk5.p0(activitySummary6.getDate());
            pq7.b(p02, "DateHelper.isToday(todaySummary.getDate())");
            if (p02.booleanValue()) {
                arrayList.add(0, new ActivitySummary(activitySummary6));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final void loadData(fl5.d dVar, Date date, Date date2, fl5.b.a aVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadData start=" + date + ", end=" + date2);
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new ActivitySummaryLocalDataSource$loadData$Anon1(this, date, date2, dVar, aVar, null), 3, null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final fl5 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public boolean isInvalid() {
        this.mFitnessDatabase.getInvalidationTracker().h();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.au0
    public void loadAfter(au0.f<Date> fVar, au0.a<Date, ActivitySummary> aVar) {
        pq7.c(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Object) fVar.f326a));
        if (lk5.j0(fVar.f326a, this.mCreatedDate)) {
            Key key2 = fVar.f326a;
            pq7.b(key2, "params.key");
            Key key3 = key2;
            Companion companion = Companion;
            Key key4 = fVar.f326a;
            pq7.b(key4, "params.key");
            Date calculateNextKey = companion.calculateNextKey(key4, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date O = lk5.m0(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : lk5.O(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + O + ", endQueryDate=" + ((Object) key3));
            pq7.b(O, "startQueryDate");
            aVar.a(getDataInDatabase(O, key3), calculateNextKey);
            if (lk5.j0(this.mStartDate, key3)) {
                this.mEndDate = key3;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.d(str3, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new cl7<>(this.mStartDate, this.mEndDate));
                this.mHelper.h(fl5.d.AFTER, new ActivitySummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.au0
    public void loadBefore(au0.f<Date> fVar, au0.a<Date, ActivitySummary> aVar) {
        pq7.c(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.fossil.au0
    public void loadInitial(au0.e<Date> eVar, au0.c<Date, ActivitySummary> cVar) {
        pq7.c(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date O = lk5.m0(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : lk5.O(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + O + ", endQueryDate=" + date);
        pq7.b(O, "startQueryDate");
        cVar.a(getDataInDatabase(O, date), null, this.key.getTime());
        this.mHelper.h(fl5.d.INITIAL, new ActivitySummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.f(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().j(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        pq7.c(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(fl5 fl5) {
        pq7.c(fl5, "<set-?>");
        this.mHelper = fl5;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        pq7.c(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        pq7.c(date, "<set-?>");
        this.mStartDate = date;
    }
}
