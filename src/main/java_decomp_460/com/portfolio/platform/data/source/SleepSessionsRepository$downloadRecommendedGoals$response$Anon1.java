package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$response$1", f = "SleepSessionsRepository.kt", l = {366}, m = "invokeSuspend")
public final class SleepSessionsRepository$downloadRecommendedGoals$response$Anon1 extends ko7 implements rp7<qn7<? super q88<SleepRecommendedGoal>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $age;
    @DexIgnore
    public /* final */ /* synthetic */ String $gender;
    @DexIgnore
    public /* final */ /* synthetic */ int $heightInCentimeters;
    @DexIgnore
    public /* final */ /* synthetic */ int $weightInGrams;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$downloadRecommendedGoals$response$Anon1(SleepSessionsRepository sleepSessionsRepository, int i, int i2, int i3, String str, qn7 qn7) {
        super(1, qn7);
        this.this$0 = sleepSessionsRepository;
        this.$age = i;
        this.$weightInGrams = i2;
        this.$heightInCentimeters = i3;
        this.$gender = str;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(qn7<?> qn7) {
        pq7.c(qn7, "completion");
        return new SleepSessionsRepository$downloadRecommendedGoals$response$Anon1(this.this$0, this.$age, this.$weightInGrams, this.$heightInCentimeters, this.$gender, qn7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public final Object invoke(qn7<? super q88<SleepRecommendedGoal>> qn7) {
        return ((SleepSessionsRepository$downloadRecommendedGoals$response$Anon1) create(qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiService;
            int i2 = this.$age;
            int i3 = this.$weightInGrams;
            int i4 = this.$heightInCentimeters;
            String str = this.$gender;
            this.label = 1;
            Object recommendedSleepGoalRaw = apiServiceV2.getRecommendedSleepGoalRaw(i2, i3, i4, str, this);
            return recommendedSleepGoalRaw == d ? d : recommendedSleepGoalRaw;
        } else if (i == 1) {
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
