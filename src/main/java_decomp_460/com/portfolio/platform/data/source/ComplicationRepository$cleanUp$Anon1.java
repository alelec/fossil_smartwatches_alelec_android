package com.portfolio.platform.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.co7;
import com.fossil.eo7;
import com.fossil.qn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.ComplicationRepository", f = "ComplicationRepository.kt", l = {69}, m = "cleanUp")
public final class ComplicationRepository$cleanUp$Anon1 extends co7 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ ComplicationRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ComplicationRepository$cleanUp$Anon1(ComplicationRepository complicationRepository, qn7 qn7) {
        super(qn7);
        this.this$0 = complicationRepository;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.cleanUp(this);
    }
}
