package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.AlarmsRepository$updateAlarm$2", f = "AlarmsRepository.kt", l = {135, 137}, m = "invokeSuspend")
public final class AlarmsRepository$updateAlarm$Anon2 extends ko7 implements vp7<iv7, qn7<? super iq5<Alarm>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Alarm $alarm;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$updateAlarm$Anon2(AlarmsRepository alarmsRepository, Alarm alarm, qn7 qn7) {
        super(2, qn7);
        this.this$0 = alarmsRepository;
        this.$alarm = alarm;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        AlarmsRepository$updateAlarm$Anon2 alarmsRepository$updateAlarm$Anon2 = new AlarmsRepository$updateAlarm$Anon2(this.this$0, this.$alarm, qn7);
        alarmsRepository$updateAlarm$Anon2.p$ = (iv7) obj;
        return alarmsRepository$updateAlarm$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<Alarm>> qn7) {
        return ((AlarmsRepository$updateAlarm$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object u;
        iv7 iv7;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv72 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = AlarmsRepository.TAG;
            local.d(str, "updateAlarm - alarmId=" + this.$alarm.getId());
            if (this.$alarm.getPinType() != 1) {
                this.$alarm.setPinType(2);
            }
            bn5 bn5 = bn5.j;
            this.L$0 = iv72;
            this.label = 1;
            u = bn5.u(this);
            if (u == d) {
                return d;
            }
            iv7 = iv72;
        } else if (i == 1) {
            el7.b(obj);
            iv7 = (iv7) this.L$0;
            u = obj;
        } else if (i == 2) {
            iv7 iv73 = (iv7) this.L$0;
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ((AlarmDatabase) u).alarmDao().insertAlarm(this.$alarm);
        AlarmsRepository alarmsRepository = this.this$0;
        Alarm alarm = this.$alarm;
        this.L$0 = iv7;
        this.label = 2;
        Object upsertAlarm = alarmsRepository.upsertAlarm(alarm, this);
        return upsertAlarm == d ? d : upsertAlarm;
    }
}
