package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.AlarmsRepository$getAllAlarmIgnoreDeletePinType$2", f = "AlarmsRepository.kt", l = {84}, m = "invokeSuspend")
public final class AlarmsRepository$getAllAlarmIgnoreDeletePinType$Anon2 extends ko7 implements vp7<iv7, qn7<? super List<Alarm>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    public AlarmsRepository$getAllAlarmIgnoreDeletePinType$Anon2(qn7 qn7) {
        super(2, qn7);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        AlarmsRepository$getAllAlarmIgnoreDeletePinType$Anon2 alarmsRepository$getAllAlarmIgnoreDeletePinType$Anon2 = new AlarmsRepository$getAllAlarmIgnoreDeletePinType$Anon2(qn7);
        alarmsRepository$getAllAlarmIgnoreDeletePinType$Anon2.p$ = (iv7) obj;
        return alarmsRepository$getAllAlarmIgnoreDeletePinType$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super List<Alarm>> qn7) {
        return ((AlarmsRepository$getAllAlarmIgnoreDeletePinType$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object u;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.label = 1;
            u = bn5.u(this);
            if (u == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            u = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return ((AlarmDatabase) u).alarmDao().getAlarmsIgnoreDeleted();
    }
}
