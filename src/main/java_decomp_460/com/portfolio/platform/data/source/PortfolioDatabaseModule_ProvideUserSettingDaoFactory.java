package com.portfolio.platform.data.source;

import com.fossil.lk7;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideUserSettingDaoFactory implements Factory<UserSettingDao> {
    @DexIgnore
    public /* final */ Provider<UserSettingDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideUserSettingDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<UserSettingDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideUserSettingDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<UserSettingDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideUserSettingDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static UserSettingDao provideUserSettingDao(PortfolioDatabaseModule portfolioDatabaseModule, UserSettingDatabase userSettingDatabase) {
        UserSettingDao provideUserSettingDao = portfolioDatabaseModule.provideUserSettingDao(userSettingDatabase);
        lk7.c(provideUserSettingDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideUserSettingDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public UserSettingDao get() {
        return provideUserSettingDao(this.module, this.dbProvider.get());
    }
}
