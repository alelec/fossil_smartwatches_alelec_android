package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.Theme;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.ThemeRepository$getCurrentTheme$2", f = "ThemeRepository.kt", l = {}, m = "invokeSuspend")
public final class ThemeRepository$getCurrentTheme$Anon2 extends ko7 implements vp7<iv7, qn7<? super Theme>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThemeRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThemeRepository$getCurrentTheme$Anon2(ThemeRepository themeRepository, qn7 qn7) {
        super(2, qn7);
        this.this$0 = themeRepository;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ThemeRepository$getCurrentTheme$Anon2 themeRepository$getCurrentTheme$Anon2 = new ThemeRepository$getCurrentTheme$Anon2(this.this$0, qn7);
        themeRepository$getCurrentTheme$Anon2.p$ = (iv7) obj;
        return themeRepository$getCurrentTheme$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super Theme> qn7) {
        return ((ThemeRepository$getCurrentTheme$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            return this.this$0.mThemeDao.getCurrentTheme();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
