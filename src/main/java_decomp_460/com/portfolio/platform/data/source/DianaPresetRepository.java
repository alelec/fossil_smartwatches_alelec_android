package com.portfolio.platform.data.source;

import com.fossil.kq7;
import com.fossil.pq7;
import com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ DianaPresetRemoteDataSource mDianaPresetRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return DianaPresetRepository.TAG;
        }
    }

    /*
    static {
        String simpleName = DianaPresetRepository.class.getSimpleName();
        pq7.b(simpleName, "DianaPresetRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public DianaPresetRepository(DianaPresetRemoteDataSource dianaPresetRemoteDataSource) {
        pq7.c(dianaPresetRemoteDataSource, "mDianaPresetRemoteDataSource");
        this.mDianaPresetRemoteDataSource = dianaPresetRemoteDataSource;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object cleanUp(com.fossil.qn7<? super com.fossil.tl7> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.DianaPresetRepository$cleanUp$Anon1
            if (r0 == 0) goto L_0x0036
            r0 = r6
            com.portfolio.platform.data.source.DianaPresetRepository$cleanUp$Anon1 r0 = (com.portfolio.platform.data.source.DianaPresetRepository$cleanUp$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0036
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0044
            if (r3 != r4) goto L_0x003c
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.DianaPresetRepository r0 = (com.portfolio.platform.data.source.DianaPresetRepository) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaPresetDao r0 = r0.getPresetDao()
            r0.clearDianaPresetTable()
            r0.clearDianaRecommendPresetTable()
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0035:
            return r0
        L_0x0036:
            com.portfolio.platform.data.source.DianaPresetRepository$cleanUp$Anon1 r0 = new com.portfolio.platform.data.source.DianaPresetRepository$cleanUp$Anon1
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x003c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0044:
            com.fossil.el7.b(r1)
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.cleanUp(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deleteAllPresetBySerial(java.lang.String r6, com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.DianaPresetRepository$deleteAllPresetBySerial$Anon1
            if (r0 == 0) goto L_0x003d
            r0 = r7
            com.portfolio.platform.data.source.DianaPresetRepository$deleteAllPresetBySerial$Anon1 r0 = (com.portfolio.platform.data.source.DianaPresetRepository$deleteAllPresetBySerial$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003d
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x004c
            if (r3 != r4) goto L_0x0044
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.DianaPresetRepository r1 = (com.portfolio.platform.data.source.DianaPresetRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaPresetDao r0 = r0.getPresetDao()
            r0.clearAllPresetBySerial(r6)
            r0.clearDianaRecommendPresetTable()
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x003c:
            return r0
        L_0x003d:
            com.portfolio.platform.data.source.DianaPresetRepository$deleteAllPresetBySerial$Anon1 r0 = new com.portfolio.platform.data.source.DianaPresetRepository$deleteAllPresetBySerial$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0044:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004c:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.deleteAllPresetBySerial(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deletePresetById(java.lang.String r13, com.fossil.qn7<? super com.fossil.tl7> r14) {
        /*
        // Method dump skipped, instructions count: 307
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.deletePresetById(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0111  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x016b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadPresetList(java.lang.String r11, com.fossil.qn7<? super com.fossil.iq5<java.util.List<com.portfolio.platform.data.model.diana.preset.DianaPreset>>> r12) {
        /*
        // Method dump skipped, instructions count: 391
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.downloadPresetList(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadRecommendPresetList(java.lang.String r10, com.fossil.qn7<? super com.fossil.tl7> r11) {
        /*
        // Method dump skipped, instructions count: 310
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.downloadRecommendPresetList(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0151  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object executePendingRequest(java.lang.String r12, com.fossil.qn7<? super com.fossil.iq5<java.util.List<com.portfolio.platform.data.model.diana.preset.DianaPreset>>> r13) {
        /*
        // Method dump skipped, instructions count: 366
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.executePendingRequest(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getActivePresetBySerial(java.lang.String r6, com.fossil.qn7<? super com.portfolio.platform.data.model.diana.preset.DianaPreset> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.DianaPresetRepository$getActivePresetBySerial$Anon1
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.data.source.DianaPresetRepository$getActivePresetBySerial$Anon1 r0 = (com.portfolio.platform.data.source.DianaPresetRepository$getActivePresetBySerial$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.DianaPresetRepository r1 = (com.portfolio.platform.data.source.DianaPresetRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaPresetDao r0 = r0.getPresetDao()
            com.portfolio.platform.data.model.diana.preset.DianaPreset r0 = r0.getActivePresetBySerial(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.data.source.DianaPresetRepository$getActivePresetBySerial$Anon1 r0 = new com.portfolio.platform.data.source.DianaPresetRepository$getActivePresetBySerial$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.getActivePresetBySerial(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getActivePresetBySerialLiveData(java.lang.String r6, com.fossil.qn7<? super androidx.lifecycle.LiveData<com.portfolio.platform.data.model.diana.preset.DianaPreset>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.DianaPresetRepository$getActivePresetBySerialLiveData$Anon1
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.data.source.DianaPresetRepository$getActivePresetBySerialLiveData$Anon1 r0 = (com.portfolio.platform.data.source.DianaPresetRepository$getActivePresetBySerialLiveData$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.DianaPresetRepository r1 = (com.portfolio.platform.data.source.DianaPresetRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaPresetDao r0 = r0.getPresetDao()
            androidx.lifecycle.LiveData r0 = r0.getActivePresetBySerialLiveData(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.data.source.DianaPresetRepository$getActivePresetBySerialLiveData$Anon1 r0 = new com.portfolio.platform.data.source.DianaPresetRepository$getActivePresetBySerialLiveData$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.getActivePresetBySerialLiveData(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getAllPresets(com.fossil.qn7<? super java.util.List<com.portfolio.platform.data.model.diana.preset.DianaPreset>> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.DianaPresetRepository$getAllPresets$Anon2
            if (r0 == 0) goto L_0x0032
            r0 = r6
            com.portfolio.platform.data.source.DianaPresetRepository$getAllPresets$Anon2 r0 = (com.portfolio.platform.data.source.DianaPresetRepository$getAllPresets$Anon2) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0040
            if (r3 != r4) goto L_0x0038
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.DianaPresetRepository r0 = (com.portfolio.platform.data.source.DianaPresetRepository) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaPresetDao r0 = r0.getPresetDao()
            java.util.List r0 = r0.getAllPresets()
        L_0x0031:
            return r0
        L_0x0032:
            com.portfolio.platform.data.source.DianaPresetRepository$getAllPresets$Anon2 r0 = new com.portfolio.platform.data.source.DianaPresetRepository$getAllPresets$Anon2
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0038:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0040:
            com.fossil.el7.b(r1)
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.getAllPresets(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getAllPresets(java.lang.String r6, com.fossil.qn7<? super java.util.List<com.portfolio.platform.data.model.diana.preset.DianaPreset>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.DianaPresetRepository$getAllPresets$Anon1
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.data.source.DianaPresetRepository$getAllPresets$Anon1 r0 = (com.portfolio.platform.data.source.DianaPresetRepository$getAllPresets$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.DianaPresetRepository r1 = (com.portfolio.platform.data.source.DianaPresetRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaPresetDao r0 = r0.getPresetDao()
            java.util.List r0 = r0.getAllPreset(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.data.source.DianaPresetRepository$getAllPresets$Anon1 r0 = new com.portfolio.platform.data.source.DianaPresetRepository$getAllPresets$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.getAllPresets(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getPresetById(java.lang.String r6, com.fossil.qn7<? super com.portfolio.platform.data.model.diana.preset.DianaPreset> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.DianaPresetRepository$getPresetById$Anon1
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.data.source.DianaPresetRepository$getPresetById$Anon1 r0 = (com.portfolio.platform.data.source.DianaPresetRepository$getPresetById$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.DianaPresetRepository r1 = (com.portfolio.platform.data.source.DianaPresetRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaPresetDao r0 = r0.getPresetDao()
            com.portfolio.platform.data.model.diana.preset.DianaPreset r0 = r0.getPresetById(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.data.source.DianaPresetRepository$getPresetById$Anon1 r0 = new com.portfolio.platform.data.source.DianaPresetRepository$getPresetById$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.getPresetById(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getPresetByIdLive(java.lang.String r6, com.fossil.qn7<? super androidx.lifecycle.LiveData<com.portfolio.platform.data.model.diana.preset.DianaPreset>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.DianaPresetRepository$getPresetByIdLive$Anon1
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.data.source.DianaPresetRepository$getPresetByIdLive$Anon1 r0 = (com.portfolio.platform.data.source.DianaPresetRepository$getPresetByIdLive$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.DianaPresetRepository r1 = (com.portfolio.platform.data.source.DianaPresetRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaPresetDao r0 = r0.getPresetDao()
            androidx.lifecycle.LiveData r0 = r0.getPresetByIdLive(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.data.source.DianaPresetRepository$getPresetByIdLive$Anon1 r0 = new com.portfolio.platform.data.source.DianaPresetRepository$getPresetByIdLive$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.getPresetByIdLive(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getPresetList(java.lang.String r6, com.fossil.qn7<? super java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.DianaPreset>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.DianaPresetRepository$getPresetList$Anon1
            if (r0 == 0) goto L_0x003d
            r0 = r7
            com.portfolio.platform.data.source.DianaPresetRepository$getPresetList$Anon1 r0 = (com.portfolio.platform.data.source.DianaPresetRepository$getPresetList$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003d
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x004c
            if (r3 != r4) goto L_0x0044
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.DianaPresetRepository r1 = (com.portfolio.platform.data.source.DianaPresetRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaPresetDao r0 = r0.getPresetDao()
            java.util.List r0 = r0.getAllPreset(r6)
            if (r0 == 0) goto L_0x005e
            java.util.ArrayList r0 = (java.util.ArrayList) r0
        L_0x003c:
            return r0
        L_0x003d:
            com.portfolio.platform.data.source.DianaPresetRepository$getPresetList$Anon1 r0 = new com.portfolio.platform.data.source.DianaPresetRepository$getPresetList$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0044:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004c:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x003c
        L_0x005e:
            com.fossil.il7 r0 = new com.fossil.il7
            java.lang.String r1 = "null cannot be cast to non-null type java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.DianaPreset>"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.getPresetList(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getPresetListAsLiveData(java.lang.String r6, com.fossil.qn7<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.diana.preset.DianaPreset>>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.DianaPresetRepository$getPresetListAsLiveData$Anon1
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.data.source.DianaPresetRepository$getPresetListAsLiveData$Anon1 r0 = (com.portfolio.platform.data.source.DianaPresetRepository$getPresetListAsLiveData$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.DianaPresetRepository r1 = (com.portfolio.platform.data.source.DianaPresetRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaPresetDao r0 = r0.getPresetDao()
            androidx.lifecycle.LiveData r0 = r0.getAllPresetAsLiveData(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.data.source.DianaPresetRepository$getPresetListAsLiveData$Anon1 r0 = new com.portfolio.platform.data.source.DianaPresetRepository$getPresetListAsLiveData$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.getPresetListAsLiveData(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getRecommendPresetList(java.lang.String r6, com.fossil.qn7<? super java.util.List<com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.DianaPresetRepository$getRecommendPresetList$Anon1
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.data.source.DianaPresetRepository$getRecommendPresetList$Anon1 r0 = (com.portfolio.platform.data.source.DianaPresetRepository$getRecommendPresetList$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.DianaPresetRepository r1 = (com.portfolio.platform.data.source.DianaPresetRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaPresetDao r0 = r0.getPresetDao()
            java.util.List r0 = r0.getDianaRecommendPresetList(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.data.source.DianaPresetRepository$getRecommendPresetList$Anon1 r0 = new com.portfolio.platform.data.source.DianaPresetRepository$getRecommendPresetList$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.getRecommendPresetList(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x019b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object upsertPreset(com.portfolio.platform.data.model.diana.preset.DianaPreset r13, com.fossil.qn7<? super com.fossil.tl7> r14) {
        /*
        // Method dump skipped, instructions count: 422
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.upsertPreset(com.portfolio.platform.data.model.diana.preset.DianaPreset, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object upsertPresetList(java.util.List<com.portfolio.platform.data.model.diana.preset.DianaPreset> r9, com.fossil.qn7<? super com.fossil.tl7> r10) {
        /*
        // Method dump skipped, instructions count: 244
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaPresetRepository.upsertPresetList(java.util.List, com.fossil.qn7):java.lang.Object");
    }
}
