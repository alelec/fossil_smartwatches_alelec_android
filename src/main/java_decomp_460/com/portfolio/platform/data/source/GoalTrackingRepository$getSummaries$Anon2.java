package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.c47;
import com.fossil.cl7;
import com.fossil.eo7;
import com.fossil.gi0;
import com.fossil.h47;
import com.fossil.im7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.lk5;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingDataKt;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$2", f = "GoalTrackingRepository.kt", l = {210, 211}, m = "invokeSuspend")
public final class GoalTrackingRepository$getSummaries$Anon2 extends ko7 implements vp7<iv7, qn7<? super LiveData<h47<? extends List<GoalTrackingSummary>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements gi0<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDatabase $goalTrackingDb;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingRepository$getSummaries$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends c47<List<GoalTrackingSummary>, ApiResponse<GoalDailySummary>> {
            @DexIgnore
            public /* final */ /* synthetic */ cl7 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, cl7 cl7) {
                this.this$0 = anon1_Level2;
                this.$downloadingDate = cl7;
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object createCall(qn7<? super q88<ApiResponse<GoalDailySummary>>> qn7) {
                Date date;
                Date date2;
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiServiceV2;
                cl7 cl7 = this.$downloadingDate;
                if (cl7 == null || (date = (Date) cl7.getFirst()) == null) {
                    date = this.this$0.this$0.$startDate;
                }
                String k = lk5.k(date);
                pq7.b(k, "DateHelper.formatShortDa\u2026            ?: startDate)");
                cl7 cl72 = this.$downloadingDate;
                if (cl72 == null || (date2 = (Date) cl72.getSecond()) == null) {
                    date2 = this.this$0.this$0.$endDate;
                }
                String k2 = lk5.k(date2);
                pq7.b(k2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return apiServiceV2.getGoalTrackingSummaries(k, k2, 0, 100, qn7);
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object loadFromDb(qn7<? super LiveData<List<GoalTrackingSummary>>> qn7) {
                GoalTrackingDao goalTrackingDao = this.this$0.$goalTrackingDb.getGoalTrackingDao();
                GoalTrackingRepository$getSummaries$Anon2 goalTrackingRepository$getSummaries$Anon2 = this.this$0.this$0;
                return goalTrackingDao.getGoalTrackingSummariesLiveData(goalTrackingRepository$getSummaries$Anon2.$startDate, goalTrackingRepository$getSummaries$Anon2.$endDate);
            }

            @DexIgnore
            @Override // com.fossil.c47
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().e(GoalTrackingRepository.Companion.getTAG(), "getSummaries onFetchFailed");
            }

            @DexIgnore
            public Object saveCallResult(ApiResponse<GoalDailySummary> apiResponse, qn7<? super tl7> qn7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = GoalTrackingRepository.Companion.getTAG();
                local.d(tag, "getSummaries startDate=" + this.this$0.this$0.$startDate + ", endDate=" + this.this$0.this$0.$endDate + " saveCallResult onResponse: response = " + apiResponse);
                try {
                    List<GoalDailySummary> list = apiResponse.get_items();
                    ArrayList arrayList = new ArrayList(im7.m(list, 10));
                    Iterator<T> it = list.iterator();
                    while (it.hasNext()) {
                        GoalTrackingSummary goalTrackingSummary = it.next().toGoalTrackingSummary();
                        if (goalTrackingSummary != null) {
                            arrayList.add(goalTrackingSummary);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                    this.this$0.$goalTrackingDb.getGoalTrackingDao().upsertGoalTrackingSummaries(pm7.j0(arrayList));
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String tag2 = GoalTrackingRepository.Companion.getTAG();
                    local2.e(tag2, "getSummaries startDate=" + this.this$0.this$0.$startDate + ", endDate=" + this.this$0.this$0.$endDate + " exception=" + e + '}');
                    e.printStackTrace();
                }
                return tl7.f3441a;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
            @Override // com.fossil.c47
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<GoalDailySummary> apiResponse, qn7 qn7) {
                return saveCallResult(apiResponse, (qn7<? super tl7>) qn7);
            }

            @DexIgnore
            public boolean shouldFetch(List<GoalTrackingSummary> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(GoalTrackingRepository$getSummaries$Anon2 goalTrackingRepository$getSummaries$Anon2, GoalTrackingDatabase goalTrackingDatabase) {
            this.this$0 = goalTrackingRepository$getSummaries$Anon2;
            this.$goalTrackingDb = goalTrackingDatabase;
        }

        @DexIgnore
        public final LiveData<h47<List<GoalTrackingSummary>>> apply(List<GoalTrackingData> list) {
            pq7.b(list, "pendingList");
            GoalTrackingRepository$getSummaries$Anon2 goalTrackingRepository$getSummaries$Anon2 = this.this$0;
            return new Anon1_Level3(this, GoalTrackingDataKt.calculateRangeDownload(list, goalTrackingRepository$getSummaries$Anon2.$startDate, goalTrackingRepository$getSummaries$Anon2.$endDate)).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getSummaries$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, boolean z, qn7 qn7) {
        super(2, qn7);
        this.this$0 = goalTrackingRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        GoalTrackingRepository$getSummaries$Anon2 goalTrackingRepository$getSummaries$Anon2 = new GoalTrackingRepository$getSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, this.$shouldFetch, qn7);
        goalTrackingRepository$getSummaries$Anon2.p$ = (iv7) obj;
        return goalTrackingRepository$getSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super LiveData<h47<? extends List<GoalTrackingSummary>>>> qn7) {
        return ((GoalTrackingRepository$getSummaries$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0097  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r9) {
        /*
            r8 = this;
            r7 = 2
            r6 = 1
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r8.label
            if (r0 == 0) goto L_0x0050
            if (r0 == r6) goto L_0x0030
            if (r0 != r7) goto L_0x0028
            java.lang.Object r0 = r8.L$1
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r0
            java.lang.Object r1 = r8.L$0
            com.fossil.iv7 r1 = (com.fossil.iv7) r1
            com.fossil.el7.b(r9)
            r1 = r9
            r2 = r0
        L_0x001b:
            r0 = r1
            androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon2$Anon1_Level2 r1 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon2$Anon1_Level2
            r1.<init>(r8, r2)
            androidx.lifecycle.LiveData r0 = com.fossil.ss0.c(r0, r1)
        L_0x0027:
            return r0
        L_0x0028:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0030:
            java.lang.Object r0 = r8.L$0
            com.fossil.iv7 r0 = (com.fossil.iv7) r0
            com.fossil.el7.b(r9)
            r2 = r0
            r1 = r9
        L_0x0039:
            r0 = r1
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r0
            com.portfolio.platform.data.source.GoalTrackingRepository r1 = r8.this$0
            java.util.Date r4 = r8.$startDate
            java.util.Date r5 = r8.$endDate
            r8.L$0 = r2
            r8.L$1 = r0
            r8.label = r7
            java.lang.Object r1 = r1.getPendingGoalTrackingDataListLiveData(r4, r5, r8)
            if (r1 != r3) goto L_0x0097
            r0 = r3
            goto L_0x0027
        L_0x0050:
            com.fossil.el7.b(r9)
            com.fossil.iv7 r0 = r8.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            com.portfolio.platform.data.source.GoalTrackingRepository$Companion r2 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
            java.lang.String r2 = r2.getTAG()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "getSummaries startDate="
            r4.append(r5)
            java.util.Date r5 = r8.$startDate
            r4.append(r5)
            java.lang.String r5 = ", endDate="
            r4.append(r5)
            java.util.Date r5 = r8.$endDate
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.d(r2, r4)
            com.fossil.dv7 r1 = com.fossil.bw7.b()
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon2$goalTrackingDb$Anon1_Level2 r2 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon2$goalTrackingDb$Anon1_Level2
            r4 = 0
            r2.<init>(r4)
            r8.L$0 = r0
            r8.label = r6
            java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r8)
            if (r1 != r3) goto L_0x0099
            r0 = r3
            goto L_0x0027
        L_0x0097:
            r2 = r0
            goto L_0x001b
        L_0x0099:
            r2 = r0
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
