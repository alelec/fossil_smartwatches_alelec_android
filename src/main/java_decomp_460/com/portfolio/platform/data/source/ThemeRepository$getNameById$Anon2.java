package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.ThemeRepository$getNameById$2", f = "ThemeRepository.kt", l = {}, m = "invokeSuspend")
public final class ThemeRepository$getNameById$Anon2 extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $id;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThemeRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThemeRepository$getNameById$Anon2(ThemeRepository themeRepository, String str, qn7 qn7) {
        super(2, qn7);
        this.this$0 = themeRepository;
        this.$id = str;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ThemeRepository$getNameById$Anon2 themeRepository$getNameById$Anon2 = new ThemeRepository$getNameById$Anon2(this.this$0, this.$id, qn7);
        themeRepository$getNameById$Anon2.p$ = (iv7) obj;
        return themeRepository$getNameById$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
        return ((ThemeRepository$getNameById$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            return this.this$0.mThemeDao.getNameById(this.$id);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
