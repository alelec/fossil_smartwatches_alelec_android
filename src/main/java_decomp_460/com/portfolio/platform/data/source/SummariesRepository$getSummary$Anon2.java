package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bw7;
import com.fossil.dv7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.eu7;
import com.fossil.h47;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.ss0;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SummariesRepository$getSummary$2", f = "SummariesRepository.kt", l = {160}, m = "invokeSuspend")
public final class SummariesRepository$getSummary$Anon2 extends ko7 implements vp7<iv7, qn7<? super LiveData<h47<? extends ActivitySummary>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummary$Anon2(SummariesRepository summariesRepository, Date date, qn7 qn7) {
        super(2, qn7);
        this.this$0 = summariesRepository;
        this.$date = date;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SummariesRepository$getSummary$Anon2 summariesRepository$getSummary$Anon2 = new SummariesRepository$getSummary$Anon2(this.this$0, this.$date, qn7);
        summariesRepository$getSummary$Anon2.p$ = (iv7) obj;
        return summariesRepository$getSummary$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super LiveData<h47<? extends ActivitySummary>>> qn7) {
        return ((SummariesRepository$getSummary$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object g;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "getSummary - date=" + this.$date);
            dv7 b = bw7.b();
            SummariesRepository$getSummary$Anon2$fitnessDatabase$Anon1_Level2 summariesRepository$getSummary$Anon2$fitnessDatabase$Anon1_Level2 = new SummariesRepository$getSummary$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = iv7;
            this.label = 1;
            g = eu7.g(b, summariesRepository$getSummary$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (g == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            g = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) g;
        FitnessDataDao fitnessDataDao = fitnessDatabase.getFitnessDataDao();
        Date date = this.$date;
        LiveData c = ss0.c(fitnessDataDao.getFitnessDataLiveData(date, date), new SummariesRepository$getSummary$Anon2$result$Anon1_Level2(this, fitnessDatabase));
        pq7.b(c, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(SummariesRepository.TAG, "XXX- summaryLiveObj " + c);
        return c;
    }
}
