package com.portfolio.platform.data.source.local.label;

import com.fossil.pq7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Metadata {
    @DexIgnore
    public /* final */ List<String> prefixSerialNumbers;
    @DexIgnore
    public /* final */ String version;

    @DexIgnore
    public Metadata(List<String> list, String str) {
        this.prefixSerialNumbers = list;
        this.version = str;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.source.local.label.Metadata */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Metadata copy$default(Metadata metadata, List list, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            list = metadata.prefixSerialNumbers;
        }
        if ((i & 2) != 0) {
            str = metadata.version;
        }
        return metadata.copy(list, str);
    }

    @DexIgnore
    public final List<String> component1() {
        return this.prefixSerialNumbers;
    }

    @DexIgnore
    public final String component2() {
        return this.version;
    }

    @DexIgnore
    public final Metadata copy(List<String> list, String str) {
        return new Metadata(list, str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Metadata) {
                Metadata metadata = (Metadata) obj;
                if (!pq7.a(this.prefixSerialNumbers, metadata.prefixSerialNumbers) || !pq7.a(this.version, metadata.version)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final List<String> getPrefixSerialNumbers() {
        return this.prefixSerialNumbers;
    }

    @DexIgnore
    public final String getVersion() {
        return this.version;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        List<String> list = this.prefixSerialNumbers;
        int hashCode = list != null ? list.hashCode() : 0;
        String str = this.version;
        if (str != null) {
            i = str.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Metadata(prefixSerialNumbers=" + this.prefixSerialNumbers + ", version=" + this.version + ")";
    }
}
