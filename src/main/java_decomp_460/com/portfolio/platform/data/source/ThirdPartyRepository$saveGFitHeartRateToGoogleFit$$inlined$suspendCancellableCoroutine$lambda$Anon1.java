package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.br7;
import com.fossil.bw7;
import com.fossil.dl7;
import com.fossil.el7;
import com.fossil.gu7;
import com.fossil.iv7;
import com.fossil.jt3;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.ku7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.rh2;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.xw7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1<TResult> implements jt3<Void> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ku7 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ br7 $countSizeOfList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $gFitHeartRateList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ rh2 $historyClient$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $sampleList;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfGFitList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ ThirdPartyDatabase $db;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(ThirdPartyDatabase thirdPartyDatabase, Anon1_Level2 anon1_Level2) {
                this.$db = thirdPartyDatabase;
                this.this$0 = anon1_Level2;
            }

            @DexIgnore
            public final void run() {
                this.$db.getGFitHeartRateDao().deleteListGFitHeartRate(this.this$0.this$0.$sampleList);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1, qn7 qn7) {
            super(2, qn7);
            this.this$0 = thirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, qn7);
            anon1_Level2.p$ = (iv7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((Anon1_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object F;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                bn5 bn5 = bn5.j;
                this.L$0 = iv7;
                this.label = 1;
                F = bn5.F(this);
                if (F == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                F = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ThirdPartyDatabase thirdPartyDatabase = (ThirdPartyDatabase) F;
            thirdPartyDatabase.runInTransaction(new Anon1_Level3(thirdPartyDatabase, this));
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(List list, rh2 rh2, br7 br7, int i, ku7 ku7, ThirdPartyRepository thirdPartyRepository, List list2, String str) {
        this.$sampleList = list;
        this.$historyClient$inlined = rh2;
        this.$countSizeOfList$inlined = br7;
        this.$sizeOfGFitList$inlined = i;
        this.$continuation$inlined = ku7;
        this.this$0 = thirdPartyRepository;
        this.$gFitHeartRateList$inlined = list2;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    public final void onSuccess(Void r7) {
        FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "saveGFitHeartRateToGoogleFit - Success");
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new Anon1_Level2(this, null), 3, null);
        br7 br7 = this.$countSizeOfList$inlined;
        int i = br7.element + 1;
        br7.element = i;
        if (i >= this.$sizeOfGFitList$inlined && this.$continuation$inlined.isActive()) {
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveGFitHeartRateToGoogleFit with Success");
            ku7 ku7 = this.$continuation$inlined;
            dl7.a aVar = dl7.Companion;
            ku7.resumeWith(dl7.m1constructorimpl(null));
        }
    }
}
