package com.portfolio.platform.data.source.local.diana.workout;

import androidx.lifecycle.MutableLiveData;
import com.fossil.fl5;
import com.fossil.no4;
import com.fossil.pq7;
import com.fossil.xt0;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionDataSourceFactory extends xt0.b<Long, WorkoutSession> {
    @DexIgnore
    public /* final */ no4 appExecutors;
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public /* final */ fl5.a listener;
    @DexIgnore
    public WorkoutSessionLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ MutableLiveData<WorkoutSessionLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ FitnessDatabase workoutDatabase;
    @DexIgnore
    public /* final */ WorkoutSessionRepository workoutSessionRepository;

    @DexIgnore
    public WorkoutSessionDataSourceFactory(WorkoutSessionRepository workoutSessionRepository2, FitnessDatabase fitnessDatabase, Date date, no4 no4, fl5.a aVar) {
        pq7.c(workoutSessionRepository2, "workoutSessionRepository");
        pq7.c(fitnessDatabase, "workoutDatabase");
        pq7.c(date, "currentDate");
        pq7.c(no4, "appExecutors");
        pq7.c(aVar, "listener");
        this.workoutSessionRepository = workoutSessionRepository2;
        this.workoutDatabase = fitnessDatabase;
        this.currentDate = date;
        this.appExecutors = no4;
        this.listener = aVar;
    }

    @DexIgnore
    @Override // com.fossil.xt0.b
    public xt0<Long, WorkoutSession> create() {
        WorkoutSessionLocalDataSource workoutSessionLocalDataSource = new WorkoutSessionLocalDataSource(this.workoutSessionRepository, this.workoutDatabase, this.currentDate, this.appExecutors, this.listener);
        this.localDataSource = workoutSessionLocalDataSource;
        this.sourceLiveData.l(workoutSessionLocalDataSource);
        WorkoutSessionLocalDataSource workoutSessionLocalDataSource2 = this.localDataSource;
        if (workoutSessionLocalDataSource2 != null) {
            return workoutSessionLocalDataSource2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final WorkoutSessionLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<WorkoutSessionLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(WorkoutSessionLocalDataSource workoutSessionLocalDataSource) {
        this.localDataSource = workoutSessionLocalDataSource;
    }
}
