package com.portfolio.platform.data.source;

import com.fossil.iq5;
import com.fossil.qn7;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface AlarmsDataSource {
    @DexIgnore
    Object cleanUp();  // void declaration

    @DexIgnore
    Object deleteAlarm(Alarm alarm, qn7<? super iq5<Alarm>> qn7);

    @DexIgnore
    Object findAlarm(String str, qn7<? super Alarm> qn7);

    @DexIgnore
    Object getActiveAlarms(qn7<? super List<Alarm>> qn7);

    @DexIgnore
    Object getAlarms(qn7<? super iq5<List<Alarm>>> qn7);

    @DexIgnore
    Object getAllAlarmIgnorePinType(qn7<? super List<Alarm>> qn7);

    @DexIgnore
    Object setAlarm(Alarm alarm, qn7<? super iq5<Alarm>> qn7);
}
