package com.portfolio.platform.data.source;

import com.fossil.cn5;
import com.fossil.dl7;
import com.fossil.ku7;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.data.model.LocalFile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileRepository$downloadAndSaveWithRemoteUrl$$inlined$suspendCancellableCoroutine$lambda$Anon1 implements cn5.b {
    @DexIgnore
    public /* final */ /* synthetic */ String $checksum$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ku7 $continuation;
    @DexIgnore
    public /* final */ /* synthetic */ String $remoteUrl$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FileType $type$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FileRepository this$0;

    @DexIgnore
    public FileRepository$downloadAndSaveWithRemoteUrl$$inlined$suspendCancellableCoroutine$lambda$Anon1(ku7 ku7, FileRepository fileRepository, String str, String str2, FileType fileType) {
        this.$continuation = ku7;
        this.this$0 = fileRepository;
        this.$remoteUrl$inlined = str;
        this.$checksum$inlined = str2;
        this.$type$inlined = fileType;
    }

    @DexIgnore
    @Override // com.fossil.cn5.b
    public void onComplete(boolean z, LocalFile localFile) {
        pq7.c(localFile, "file");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = FileRepository.Companion.getTAG();
        local.d(tag, "downloadFromURL with remoteUrl " + this.$remoteUrl$inlined + " isSuccess " + z);
        if (z) {
            this.this$0.mFileDao.upsertLocalFile(localFile);
        }
        if (this.$continuation.isActive()) {
            ku7 ku7 = this.$continuation;
            dl7.a aVar = dl7.Companion;
            ku7.resumeWith(dl7.m1constructorimpl(Boolean.valueOf(z)));
        }
    }
}
