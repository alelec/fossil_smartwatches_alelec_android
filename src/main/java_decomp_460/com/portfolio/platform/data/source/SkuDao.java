package com.portfolio.platform.data.source;

import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.WatchParam;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface SkuDao {
    @DexIgnore
    void addOrUpdateSkuList(List<SKUModel> list);

    @DexIgnore
    void addOrUpdateWatchParam(WatchParam watchParam);

    @DexIgnore
    Object cleanUpSku();  // void declaration

    @DexIgnore
    Object cleanUpWatchParam();  // void declaration

    @DexIgnore
    List<SKUModel> getAllSkus();

    @DexIgnore
    SKUModel getSkuByDeviceIdPrefix(String str);

    @DexIgnore
    WatchParam getWatchParamById(String str);
}
