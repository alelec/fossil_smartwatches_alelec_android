package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideQuickResponseMessageDaoFactory implements Factory<QuickResponseMessageDao> {
    @DexIgnore
    public /* final */ Provider<QuickResponseDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideQuickResponseMessageDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<QuickResponseDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideQuickResponseMessageDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<QuickResponseDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideQuickResponseMessageDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static QuickResponseMessageDao provideQuickResponseMessageDao(PortfolioDatabaseModule portfolioDatabaseModule, QuickResponseDatabase quickResponseDatabase) {
        QuickResponseMessageDao provideQuickResponseMessageDao = portfolioDatabaseModule.provideQuickResponseMessageDao(quickResponseDatabase);
        lk7.c(provideQuickResponseMessageDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideQuickResponseMessageDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public QuickResponseMessageDao get() {
        return provideQuickResponseMessageDao(this.module, this.dbProvider.get());
    }
}
