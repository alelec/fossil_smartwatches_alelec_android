package com.portfolio.platform.data.source.remote;

import com.fossil.gj4;
import com.fossil.i98;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.t98;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface AuthApiGuestService {
    @DexIgnore
    @t98("rpc/auth/check-account-existence-by-email")
    Object checkAuthenticationEmailExisting(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("rpc/auth/check-account-existence-by-social")
    Object checkAuthenticationSocialExisting(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("rpc/auth/login")
    Object loginWithEmail(@i98 gj4 gj4, qn7<? super q88<Auth>> qn7);

    @DexIgnore
    @t98("rpc/auth/login-with")
    Object loginWithSocial(@i98 gj4 gj4, qn7<? super q88<Auth>> qn7);

    @DexIgnore
    @t98("rpc/auth/password/request-reset")
    Object passwordRequestReset(@i98 gj4 gj4, qn7<? super q88<Void>> qn7);

    @DexIgnore
    @t98("rpc/auth/register")
    Object registerEmail(@i98 SignUpEmailAuth signUpEmailAuth, qn7<? super q88<Auth>> qn7);

    @DexIgnore
    @t98("rpc/auth/register-with")
    Object registerSocial(@i98 SignUpSocialAuth signUpSocialAuth, qn7<? super q88<Auth>> qn7);

    @DexIgnore
    @t98("rpc/auth/request-email-otp")
    Object requestEmailOtp(@i98 gj4 gj4, qn7<? super q88<Void>> qn7);

    @DexIgnore
    @t98("rpc/auth/token/exchange-legacy")
    Call<Auth> tokenExchangeLegacy(@i98 gj4 gj4);

    @DexIgnore
    @t98("rpc/auth/token/refresh")
    Call<Auth> tokenRefresh(@i98 gj4 gj4);

    @DexIgnore
    @t98("rpc/auth/verify-email-otp")
    Object verifyEmailOtp(@i98 gj4 gj4, qn7<? super q88<Void>> qn7);
}
