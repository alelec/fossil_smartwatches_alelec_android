package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.kz4;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.DianaAppSettingRepository$upsertDianaAppSetting$2", f = "DianaAppSettingRepository.kt", l = {74, 80, 88}, m = "invokeSuspend")
public final class DianaAppSettingRepository$upsertDianaAppSetting$Anon2 extends ko7 implements vp7<iv7, qn7<? super kz4<List<DianaAppSetting>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $settings;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaAppSettingRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaAppSettingRepository$upsertDianaAppSetting$Anon2(DianaAppSettingRepository dianaAppSettingRepository, List list, qn7 qn7) {
        super(2, qn7);
        this.this$0 = dianaAppSettingRepository;
        this.$settings = list;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        DianaAppSettingRepository$upsertDianaAppSetting$Anon2 dianaAppSettingRepository$upsertDianaAppSetting$Anon2 = new DianaAppSettingRepository$upsertDianaAppSetting$Anon2(this.this$0, this.$settings, qn7);
        dianaAppSettingRepository$upsertDianaAppSetting$Anon2.p$ = (iv7) obj;
        return dianaAppSettingRepository$upsertDianaAppSetting$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super kz4<List<DianaAppSetting>>> qn7) {
        return ((DianaAppSettingRepository$upsertDianaAppSetting$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x01a2  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01a6  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r14) {
        /*
        // Method dump skipped, instructions count: 522
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaAppSettingRepository$upsertDianaAppSetting$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
