package com.portfolio.platform.data.source.local.sleep;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import com.fossil.wearables.fsl.sleep.MFSleepDay;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.portfolio.platform.data.SleepStatistic;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDatabase_Impl extends SleepDatabase {
    @DexIgnore
    public volatile SleepDao _sleepDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `sleep_session` (`pinType` INTEGER NOT NULL, `date` INTEGER NOT NULL, `day` TEXT NOT NULL, `deviceSerialNumber` TEXT, `syncTime` INTEGER, `bookmarkTime` INTEGER, `normalizedSleepQuality` REAL NOT NULL, `source` INTEGER NOT NULL, `realStartTime` INTEGER NOT NULL, `realEndTime` INTEGER NOT NULL, `realSleepMinutes` INTEGER NOT NULL, `realSleepStateDistInMinute` TEXT NOT NULL, `editedStartTime` INTEGER, `editedEndTime` INTEGER, `editedSleepMinutes` INTEGER, `editedSleepStateDistInMinute` TEXT, `sleepStates` TEXT NOT NULL, `heartRate` TEXT, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `timezoneOffset` INTEGER NOT NULL, PRIMARY KEY(`realEndTime`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `sleep_date` (`pinType` INTEGER NOT NULL, `timezoneOffset` INTEGER NOT NULL, `date` TEXT NOT NULL, `goalMinutes` INTEGER NOT NULL, `sleepMinutes` INTEGER NOT NULL, `sleepStateDistInMinute` TEXT, `createdAt` INTEGER, `updatedAt` INTEGER, PRIMARY KEY(`date`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `sleep_settings` (`id` INTEGER NOT NULL, `sleepGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `sleepRecommendedGoals` (`id` INTEGER NOT NULL, `recommendedSleepGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `sleep_statistic` (`id` TEXT NOT NULL, `uid` TEXT NOT NULL, `sleepTimeBestDay` TEXT, `sleepTimeBestStreak` TEXT, `totalDays` INTEGER NOT NULL, `totalSleeps` INTEGER NOT NULL, `totalSleepMinutes` INTEGER NOT NULL, `totalSleepStateDistInMinute` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '6563ab2be16ee1f30194e9dfab2b7513')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `sleep_session`");
            lx0.execSQL("DROP TABLE IF EXISTS `sleep_date`");
            lx0.execSQL("DROP TABLE IF EXISTS `sleep_settings`");
            lx0.execSQL("DROP TABLE IF EXISTS `sleepRecommendedGoals`");
            lx0.execSQL("DROP TABLE IF EXISTS `sleep_statistic`");
            if (SleepDatabase_Impl.this.mCallbacks != null) {
                int size = SleepDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) SleepDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (SleepDatabase_Impl.this.mCallbacks != null) {
                int size = SleepDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) SleepDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            SleepDatabase_Impl.this.mDatabase = lx0;
            SleepDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (SleepDatabase_Impl.this.mCallbacks != null) {
                int size = SleepDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) SleepDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(21);
            hashMap.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap.put("date", new ix0.a("date", "INTEGER", true, 0, null, 1));
            hashMap.put("day", new ix0.a("day", "TEXT", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, new ix0.a(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, "TEXT", false, 0, null, 1));
            hashMap.put("syncTime", new ix0.a("syncTime", "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_BOOKMARK_TIME, new ix0.a(MFSleepSession.COLUMN_BOOKMARK_TIME, "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY, new ix0.a(MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY, "REAL", true, 0, null, 1));
            hashMap.put("source", new ix0.a("source", "INTEGER", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_REAL_START_TIME, new ix0.a(MFSleepSession.COLUMN_REAL_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_REAL_END_TIME, new ix0.a(MFSleepSession.COLUMN_REAL_END_TIME, "INTEGER", true, 1, null, 1));
            hashMap.put(MFSleepSession.COLUMN_REAL_SLEEP_MINUTES, new ix0.a(MFSleepSession.COLUMN_REAL_SLEEP_MINUTES, "INTEGER", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE, new ix0.a(MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE, "TEXT", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_EDITED_START_TIME, new ix0.a(MFSleepSession.COLUMN_EDITED_START_TIME, "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_EDITED_END_TIME, new ix0.a(MFSleepSession.COLUMN_EDITED_END_TIME, "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES, new ix0.a(MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES, "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE, new ix0.a(MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE, "TEXT", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_SLEEP_STATES, new ix0.a(MFSleepSession.COLUMN_SLEEP_STATES, "TEXT", true, 0, null, 1));
            hashMap.put("heartRate", new ix0.a("heartRate", "TEXT", false, 0, null, 1));
            hashMap.put("createdAt", new ix0.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap.put("updatedAt", new ix0.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap.put("timezoneOffset", new ix0.a("timezoneOffset", "INTEGER", true, 0, null, 1));
            ix0 ix0 = new ix0(MFSleepSession.TABLE_NAME, hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, MFSleepSession.TABLE_NAME);
            if (!ix0.equals(a2)) {
                return new sw0.b(false, "sleep_session(com.portfolio.platform.data.model.room.sleep.MFSleepSession).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(8);
            hashMap2.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap2.put("timezoneOffset", new ix0.a("timezoneOffset", "INTEGER", true, 0, null, 1));
            hashMap2.put("date", new ix0.a("date", "TEXT", true, 1, null, 1));
            hashMap2.put(MFSleepDay.COLUMN_GOAL_MINUTES, new ix0.a(MFSleepDay.COLUMN_GOAL_MINUTES, "INTEGER", true, 0, null, 1));
            hashMap2.put(MFSleepDay.COLUMN_SLEEP_MINUTES, new ix0.a(MFSleepDay.COLUMN_SLEEP_MINUTES, "INTEGER", true, 0, null, 1));
            hashMap2.put(MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE, new ix0.a(MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE, "TEXT", false, 0, null, 1));
            hashMap2.put("createdAt", new ix0.a("createdAt", "INTEGER", false, 0, null, 1));
            hashMap2.put("updatedAt", new ix0.a("updatedAt", "INTEGER", false, 0, null, 1));
            ix0 ix02 = new ix0(MFSleepDay.TABLE_NAME, hashMap2, new HashSet(0), new HashSet(0));
            ix0 a3 = ix0.a(lx0, MFSleepDay.TABLE_NAME);
            if (!ix02.equals(a3)) {
                return new sw0.b(false, "sleep_date(com.portfolio.platform.data.model.room.sleep.MFSleepDay).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
            }
            HashMap hashMap3 = new HashMap(2);
            hashMap3.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap3.put("sleepGoal", new ix0.a("sleepGoal", "INTEGER", true, 0, null, 1));
            ix0 ix03 = new ix0("sleep_settings", hashMap3, new HashSet(0), new HashSet(0));
            ix0 a4 = ix0.a(lx0, "sleep_settings");
            if (!ix03.equals(a4)) {
                return new sw0.b(false, "sleep_settings(com.portfolio.platform.data.model.room.sleep.MFSleepSettings).\n Expected:\n" + ix03 + "\n Found:\n" + a4);
            }
            HashMap hashMap4 = new HashMap(2);
            hashMap4.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap4.put("recommendedSleepGoal", new ix0.a("recommendedSleepGoal", "INTEGER", true, 0, null, 1));
            ix0 ix04 = new ix0("sleepRecommendedGoals", hashMap4, new HashSet(0), new HashSet(0));
            ix0 a5 = ix0.a(lx0, "sleepRecommendedGoals");
            if (!ix04.equals(a5)) {
                return new sw0.b(false, "sleepRecommendedGoals(com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal).\n Expected:\n" + ix04 + "\n Found:\n" + a5);
            }
            HashMap hashMap5 = new HashMap(10);
            hashMap5.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap5.put("uid", new ix0.a("uid", "TEXT", true, 0, null, 1));
            hashMap5.put("sleepTimeBestDay", new ix0.a("sleepTimeBestDay", "TEXT", false, 0, null, 1));
            hashMap5.put("sleepTimeBestStreak", new ix0.a("sleepTimeBestStreak", "TEXT", false, 0, null, 1));
            hashMap5.put("totalDays", new ix0.a("totalDays", "INTEGER", true, 0, null, 1));
            hashMap5.put("totalSleeps", new ix0.a("totalSleeps", "INTEGER", true, 0, null, 1));
            hashMap5.put("totalSleepMinutes", new ix0.a("totalSleepMinutes", "INTEGER", true, 0, null, 1));
            hashMap5.put("totalSleepStateDistInMinute", new ix0.a("totalSleepStateDistInMinute", "TEXT", true, 0, null, 1));
            hashMap5.put("createdAt", new ix0.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap5.put("updatedAt", new ix0.a("updatedAt", "INTEGER", true, 0, null, 1));
            ix0 ix05 = new ix0(SleepStatistic.TABLE_NAME, hashMap5, new HashSet(0), new HashSet(0));
            ix0 a6 = ix0.a(lx0, SleepStatistic.TABLE_NAME);
            if (ix05.equals(a6)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "sleep_statistic(com.portfolio.platform.data.SleepStatistic).\n Expected:\n" + ix05 + "\n Found:\n" + a6);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `sleep_session`");
            writableDatabase.execSQL("DELETE FROM `sleep_date`");
            writableDatabase.execSQL("DELETE FROM `sleep_settings`");
            writableDatabase.execSQL("DELETE FROM `sleepRecommendedGoals`");
            writableDatabase.execSQL("DELETE FROM `sleep_statistic`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), MFSleepSession.TABLE_NAME, MFSleepDay.TABLE_NAME, "sleep_settings", "sleepRecommendedGoals", SleepStatistic.TABLE_NAME);
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(9), "6563ab2be16ee1f30194e9dfab2b7513", "318ff9dd57f69c3e478563bc453dc851");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDatabase
    public SleepDao sleepDao() {
        SleepDao sleepDao;
        if (this._sleepDao != null) {
            return this._sleepDao;
        }
        synchronized (this) {
            if (this._sleepDao == null) {
                this._sleepDao = new SleepDao_Impl(this);
            }
            sleepDao = this._sleepDao;
        }
        return sleepDao;
    }
}
