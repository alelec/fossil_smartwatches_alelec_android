package com.portfolio.platform.data.source;

import com.fossil.bw7;
import com.fossil.gu7;
import com.fossil.jv7;
import com.fossil.on5;
import com.fossil.pq7;
import com.fossil.rv7;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchLocalizationRepository {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 api;
    @DexIgnore
    public /* final */ on5 sharedPreferencesManager;

    @DexIgnore
    public WatchLocalizationRepository(ApiServiceV2 apiServiceV2, on5 on5) {
        pq7.c(apiServiceV2, "api");
        pq7.c(on5, "sharedPreferencesManager");
        this.api = apiServiceV2;
        this.sharedPreferencesManager = on5;
        String simpleName = WatchLocalizationRepository.class.getSimpleName();
        pq7.b(simpleName, "WatchLocalizationRepository::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    private final rv7<String> processDownloadAndStore(String str, String str2) {
        return gu7.b(jv7.a(bw7.b()), null, null, new WatchLocalizationRepository$processDownloadAndStore$Anon1(this, str, str2, null), 3, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0165, code lost:
        if (com.fossil.qk5.f2994a.a(r4 + java.io.File.separator + r3) == false) goto L_0x0167;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01ab  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01eb  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWatchLocalizationFromServer(boolean r19, com.fossil.qn7<? super java.lang.String> r20) {
        /*
        // Method dump skipped, instructions count: 509
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchLocalizationRepository.getWatchLocalizationFromServer(boolean, com.fossil.qn7):java.lang.Object");
    }
}
