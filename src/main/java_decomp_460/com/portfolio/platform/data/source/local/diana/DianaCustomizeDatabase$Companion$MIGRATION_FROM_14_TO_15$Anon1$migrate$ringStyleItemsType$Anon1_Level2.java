package com.portfolio.platform.data.source.local.diana;

import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeDatabase$Companion$MIGRATION_FROM_14_TO_15$Anon1$migrate$ringStyleItemsType$Anon1_Level2 extends TypeToken<List<? extends RingStyleItem>> {
}
