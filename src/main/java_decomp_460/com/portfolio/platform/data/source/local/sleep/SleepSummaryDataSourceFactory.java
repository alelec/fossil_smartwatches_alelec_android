package com.portfolio.platform.data.source.local.sleep;

import androidx.lifecycle.MutableLiveData;
import com.fossil.fl5;
import com.fossil.no4;
import com.fossil.pq7;
import com.fossil.xt0;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummaryDataSourceFactory extends xt0.b<Date, SleepSummary> {
    @DexIgnore
    public SleepSummaryLocalDataSource localSource;
    @DexIgnore
    public /* final */ no4 mAppExecutors;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ fl5.a mListener;
    @DexIgnore
    public /* final */ SleepDatabase mSleepDatabase;
    @DexIgnore
    public /* final */ SleepSessionsRepository mSleepSessionsRepository;
    @DexIgnore
    public /* final */ SleepSummariesRepository mSleepSummariesRepository;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<SleepSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public SleepSummaryDataSourceFactory(SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDatabase sleepDatabase, Date date, no4 no4, fl5.a aVar, Calendar calendar) {
        pq7.c(sleepSummariesRepository, "mSleepSummariesRepository");
        pq7.c(sleepSessionsRepository, "mSleepSessionsRepository");
        pq7.c(fitnessDataRepository, "mFitnessDataRepository");
        pq7.c(sleepDatabase, "mSleepDatabase");
        pq7.c(date, "mCreatedDate");
        pq7.c(no4, "mAppExecutors");
        pq7.c(aVar, "mListener");
        pq7.c(calendar, "mStartCalendar");
        this.mSleepSummariesRepository = sleepSummariesRepository;
        this.mSleepSessionsRepository = sleepSessionsRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mSleepDatabase = sleepDatabase;
        this.mCreatedDate = date;
        this.mAppExecutors = no4;
        this.mListener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    @Override // com.fossil.xt0.b
    public xt0<Date, SleepSummary> create() {
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource = new SleepSummaryLocalDataSource(this.mSleepSummariesRepository, this.mSleepSessionsRepository, this.mFitnessDataRepository, this.mSleepDatabase, this.mCreatedDate, this.mAppExecutors, this.mListener, this.mStartCalendar);
        this.localSource = sleepSummaryLocalDataSource;
        this.sourceLiveData.l(sleepSummaryLocalDataSource);
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource2 = this.localSource;
        if (sleepSummaryLocalDataSource2 != null) {
            return sleepSummaryLocalDataSource2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final SleepSummaryLocalDataSource getLocalSource() {
        return this.localSource;
    }

    @DexIgnore
    public final MutableLiveData<SleepSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalSource(SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        this.localSource = sleepSummaryLocalDataSource;
    }
}
