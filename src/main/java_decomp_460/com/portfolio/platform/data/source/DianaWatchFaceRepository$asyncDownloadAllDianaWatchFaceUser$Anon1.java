package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.DianaWatchFaceRepository$asyncDownloadAllDianaWatchFaceUser$1", f = "DianaWatchFaceRepository.kt", l = {99}, m = "invokeSuspend")
public final class DianaWatchFaceRepository$asyncDownloadAllDianaWatchFaceUser$Anon1 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaWatchFaceRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaWatchFaceRepository$asyncDownloadAllDianaWatchFaceUser$Anon1(DianaWatchFaceRepository dianaWatchFaceRepository, int i, qn7 qn7) {
        super(2, qn7);
        this.this$0 = dianaWatchFaceRepository;
        this.$offset = i;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        DianaWatchFaceRepository$asyncDownloadAllDianaWatchFaceUser$Anon1 dianaWatchFaceRepository$asyncDownloadAllDianaWatchFaceUser$Anon1 = new DianaWatchFaceRepository$asyncDownloadAllDianaWatchFaceUser$Anon1(this.this$0, this.$offset, qn7);
        dianaWatchFaceRepository$asyncDownloadAllDianaWatchFaceUser$Anon1.p$ = (iv7) obj;
        return dianaWatchFaceRepository$asyncDownloadAllDianaWatchFaceUser$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((DianaWatchFaceRepository$asyncDownloadAllDianaWatchFaceUser$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            DianaWatchFaceRepository dianaWatchFaceRepository = this.this$0;
            int i2 = this.$offset;
            this.L$0 = iv7;
            this.label = 1;
            if (dianaWatchFaceRepository.downloadAllDianaWatchFaceUser(i2, this) == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return tl7.f3441a;
    }
}
