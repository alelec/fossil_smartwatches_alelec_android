package com.portfolio.platform.data.source.local.workoutsetting;

import com.fossil.ax0;
import com.fossil.lx0;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1 extends ax0 {
    @DexIgnore
    public WorkoutSettingDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.ax0
    public void migrate(lx0 lx0) {
        pq7.c(lx0, "database");
        FLogger.INSTANCE.getLocal().d(WorkoutSettingDatabase.TAG, "Migration 1 to 2 Start");
        lx0.beginTransaction();
        try {
            lx0.execSQL("UPDATE `workoutSetting` SET `enable` = 1, `askMeFirst` = 1, `pinType` = 2");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(WorkoutSettingDatabase.TAG, "Migration 1 to 2 with exception: " + e.getMessage());
            lx0.execSQL("DROP TABLE IF EXISTS `workoutSetting`");
            lx0.execSQL("CREATE TABLE `workoutSetting` (`type` TEXT NOT NULL, `mode` TEXT, `enable` INTEGER NOT NULL, `askMeFirst` INTEGER NOT NULL, `startLatency` INTEGER NOT NULL, `pauseLatency` INTEGER NOT NULL, `resumeLatency` INTEGER NOT NULL, `stopLatency` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`type`))");
        }
        lx0.setTransactionSuccessful();
        lx0.endTransaction();
        FLogger.INSTANCE.getLocal().d(WorkoutSettingDatabase.TAG, "Migration 1 to 2 Done");
    }
}
