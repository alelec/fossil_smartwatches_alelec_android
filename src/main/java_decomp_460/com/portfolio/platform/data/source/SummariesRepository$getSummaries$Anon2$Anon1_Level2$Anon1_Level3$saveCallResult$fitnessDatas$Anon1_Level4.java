package com.portfolio.platform.data.source;

import com.fossil.cl7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SummariesRepository$getSummaries$2$1$1$saveCallResult$fitnessDatas$1", f = "SummariesRepository.kt", l = {}, m = "invokeSuspend")
public final class SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4 extends ko7 implements vp7<iv7, qn7<? super List<FitnessDataWrapper>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon2.Anon1_Level2.Anon1_Level3 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4(SummariesRepository$getSummaries$Anon2.Anon1_Level2.Anon1_Level3 anon1_Level3, qn7 qn7) {
        super(2, qn7);
        this.this$0 = anon1_Level3;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4 summariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4 = new SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4(this.this$0, qn7);
        summariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4.p$ = (iv7) obj;
        return summariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super List<FitnessDataWrapper>> qn7) {
        return ((SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Date date;
        Date date2;
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            FitnessDataDao fitnessDataDao = this.this$0.this$0.$fitnessDatabase.getFitnessDataDao();
            cl7 cl7 = this.this$0.$downloadingDate;
            Date date3 = (cl7 == null || (date2 = (Date) cl7.getFirst()) == null) ? this.this$0.this$0.this$0.$startDate : date2;
            cl7 cl72 = this.this$0.$downloadingDate;
            if (cl72 == null || (date = (Date) cl72.getSecond()) == null) {
                date = this.this$0.this$0.this$0.$endDate;
            }
            return fitnessDataDao.getFitnessData(date3, date);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
