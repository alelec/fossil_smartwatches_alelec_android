package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bw7;
import com.fossil.c47;
import com.fossil.cl7;
import com.fossil.dv7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.eu7;
import com.fossil.gi0;
import com.fossil.gj4;
import com.fossil.h47;
import com.fossil.im7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.lk5;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.ss0;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.fossil.zi4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConvertDateTimeToLong;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$2", f = "HeartRateSummaryRepository.kt", l = {53}, m = "invokeSuspend")
public final class HeartRateSummaryRepository$getHeartRateSummaries$Anon2 extends ko7 implements vp7<iv7, qn7<? super LiveData<h47<? extends List<DailyHeartRateSummary>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements gi0<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ FitnessDatabase $fitnessDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSummaryRepository$getHeartRateSummaries$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends c47<List<DailyHeartRateSummary>, ApiResponse<gj4>> {
            @DexIgnore
            public /* final */ /* synthetic */ cl7 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, cl7 cl7) {
                this.this$0 = anon1_Level2;
                this.$downloadingDate = cl7;
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object createCall(qn7<? super q88<ApiResponse<gj4>>> qn7) {
                Date date;
                Date date2;
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiService;
                cl7 cl7 = this.$downloadingDate;
                if (cl7 == null || (date = (Date) cl7.getFirst()) == null) {
                    date = this.this$0.this$0.$startDate;
                }
                String k = lk5.k(date);
                pq7.b(k, "DateHelper.formatShortDa\u2026            ?: startDate)");
                cl7 cl72 = this.$downloadingDate;
                if (cl72 == null || (date2 = (Date) cl72.getSecond()) == null) {
                    date2 = this.this$0.this$0.$endDate;
                }
                String k2 = lk5.k(date2);
                pq7.b(k2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return apiServiceV2.getDailyHeartRateSummaries(k, k2, 0, 100, qn7);
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object loadFromDb(qn7<? super LiveData<List<DailyHeartRateSummary>>> qn7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = HeartRateSummaryRepository.TAG;
                local.d(str, "getHeartRateSummaries isNotToday startDate = " + this.this$0.this$0.$startDate + ", endDate = " + this.this$0.this$0.$endDate);
                HeartRateDailySummaryDao heartRateDailySummaryDao = this.this$0.$fitnessDatabase.getHeartRateDailySummaryDao();
                HeartRateSummaryRepository$getHeartRateSummaries$Anon2 heartRateSummaryRepository$getHeartRateSummaries$Anon2 = this.this$0.this$0;
                return heartRateDailySummaryDao.getDailyHeartRateSummariesLiveData(heartRateSummaryRepository$getHeartRateSummaries$Anon2.$startDate, heartRateSummaryRepository$getHeartRateSummaries$Anon2.$endDate);
            }

            @DexIgnore
            @Override // com.fossil.c47
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(HeartRateSummaryRepository.TAG, "getHeartRateSummaries onFetchFailed");
            }

            @DexIgnore
            public Object saveCallResult(ApiResponse<gj4> apiResponse, qn7<? super tl7> qn7) {
                Date date;
                Date date2;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = HeartRateSummaryRepository.TAG;
                local.d(str, "saveCallResult onResponse: response = " + apiResponse);
                try {
                    if (!apiResponse.get_items().isEmpty()) {
                        zi4 zi4 = new zi4();
                        zi4.f(Long.TYPE, new GsonConvertDateTimeToLong());
                        zi4.f(DateTime.class, new GsonConvertDateTime());
                        zi4.f(Date.class, new GsonConverterShortDate());
                        Gson d = zi4.d();
                        List<gj4> list = apiResponse.get_items();
                        ArrayList arrayList = new ArrayList(im7.m(list, 10));
                        Iterator<T> it = list.iterator();
                        while (it.hasNext()) {
                            arrayList.add((DailyHeartRateSummary) d.h(it.next(), new HeartRateSummaryRepository$getHeartRateSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$summaries$Anon1_Level4$Anon1_Level5().getType()));
                        }
                        List<DailyHeartRateSummary> j0 = pm7.j0(arrayList);
                        FLogger.INSTANCE.getLocal().d(HeartRateSummaryRepository.TAG, String.valueOf(j0));
                        this.this$0.$fitnessDatabase.getHeartRateDailySummaryDao().insertListDailyHeartRateSummary(j0);
                        FitnessDataDao fitnessDataDao = this.this$0.$fitnessDatabase.getFitnessDataDao();
                        cl7 cl7 = this.$downloadingDate;
                        Date date3 = (cl7 == null || (date2 = (Date) cl7.getFirst()) == null) ? this.this$0.this$0.$startDate : date2;
                        cl7 cl72 = this.$downloadingDate;
                        if (cl72 == null || (date = (Date) cl72.getSecond()) == null) {
                            date = this.this$0.this$0.$endDate;
                        }
                        List<FitnessDataWrapper> fitnessData = fitnessDataDao.getFitnessData(date3, date);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = HeartRateSummaryRepository.TAG;
                        local2.d(str2, "heartrate summary " + j0 + " fitnessDataSize " + fitnessData.size());
                        if (fitnessData.isEmpty()) {
                            this.this$0.$fitnessDatabase.getHeartRateDailySummaryDao().insertListDailyHeartRateSummary(j0);
                        }
                    }
                } catch (Exception e) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = HeartRateSummaryRepository.TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("saveCallResult exception=");
                    e.printStackTrace();
                    sb.append(tl7.f3441a);
                    local3.e(str3, sb.toString());
                }
                return tl7.f3441a;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
            @Override // com.fossil.c47
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<gj4> apiResponse, qn7 qn7) {
                return saveCallResult(apiResponse, (qn7<? super tl7>) qn7);
            }

            @DexIgnore
            public boolean shouldFetch(List<DailyHeartRateSummary> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(HeartRateSummaryRepository$getHeartRateSummaries$Anon2 heartRateSummaryRepository$getHeartRateSummaries$Anon2, FitnessDatabase fitnessDatabase) {
            this.this$0 = heartRateSummaryRepository$getHeartRateSummaries$Anon2;
            this.$fitnessDatabase = fitnessDatabase;
        }

        @DexIgnore
        public final LiveData<h47<List<DailyHeartRateSummary>>> apply(List<FitnessDataWrapper> list) {
            pq7.b(list, "fitnessDataList");
            HeartRateSummaryRepository$getHeartRateSummaries$Anon2 heartRateSummaryRepository$getHeartRateSummaries$Anon2 = this.this$0;
            return new Anon1_Level3(this, FitnessDataWrapperKt.calculateRangeDownload(list, heartRateSummaryRepository$getHeartRateSummaries$Anon2.$startDate, heartRateSummaryRepository$getHeartRateSummaries$Anon2.$endDate)).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSummaryRepository$getHeartRateSummaries$Anon2(HeartRateSummaryRepository heartRateSummaryRepository, Date date, Date date2, boolean z, qn7 qn7) {
        super(2, qn7);
        this.this$0 = heartRateSummaryRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        HeartRateSummaryRepository$getHeartRateSummaries$Anon2 heartRateSummaryRepository$getHeartRateSummaries$Anon2 = new HeartRateSummaryRepository$getHeartRateSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, this.$shouldFetch, qn7);
        heartRateSummaryRepository$getHeartRateSummaries$Anon2.p$ = (iv7) obj;
        return heartRateSummaryRepository$getHeartRateSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super LiveData<h47<? extends List<DailyHeartRateSummary>>>> qn7) {
        return ((HeartRateSummaryRepository$getHeartRateSummaries$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object g;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = HeartRateSummaryRepository.TAG;
            local.d(str, "getHeartRateSummaries: startDate = " + this.$startDate + ", endDate = " + this.$endDate);
            dv7 b = bw7.b();
            HeartRateSummaryRepository$getHeartRateSummaries$Anon2$fitnessDatabase$Anon1_Level2 heartRateSummaryRepository$getHeartRateSummaries$Anon2$fitnessDatabase$Anon1_Level2 = new HeartRateSummaryRepository$getHeartRateSummaries$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = iv7;
            this.label = 1;
            g = eu7.g(b, heartRateSummaryRepository$getHeartRateSummaries$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (g == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            g = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) g;
        return ss0.c(fitnessDatabase.getFitnessDataDao().getFitnessDataLiveData(this.$startDate, this.$endDate), new Anon1_Level2(this, fitnessDatabase));
    }
}
