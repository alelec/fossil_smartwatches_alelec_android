package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.i05;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyleDao_Impl implements RingStyleDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<DianaComplicationRingStyle> __insertionAdapterOfDianaComplicationRingStyle;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAllData;
    @DexIgnore
    public /* final */ i05 __ringStyleConverter; // = new i05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<DianaComplicationRingStyle> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, DianaComplicationRingStyle dianaComplicationRingStyle) {
            if (dianaComplicationRingStyle.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, dianaComplicationRingStyle.getId());
            }
            if (dianaComplicationRingStyle.getCategory() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, dianaComplicationRingStyle.getCategory());
            }
            if (dianaComplicationRingStyle.getName() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, dianaComplicationRingStyle.getName());
            }
            String a2 = RingStyleDao_Impl.this.__ringStyleConverter.a(dianaComplicationRingStyle.getData());
            if (a2 == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, a2);
            }
            String d = RingStyleDao_Impl.this.__ringStyleConverter.d(dianaComplicationRingStyle.getMetaData());
            if (d == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, d);
            }
            if (dianaComplicationRingStyle.getSerial() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, dianaComplicationRingStyle.getSerial());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `DianaComplicationRingStyle` (`id`,`category`,`name`,`data`,`metaData`,`serial`) VALUES (?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM DianaComplicationRingStyle";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<DianaComplicationRingStyle>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon3(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DianaComplicationRingStyle> call() throws Exception {
            Cursor b = ex0.b(RingStyleDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "id");
                int c2 = dx0.c(b, "category");
                int c3 = dx0.c(b, "name");
                int c4 = dx0.c(b, "data");
                int c5 = dx0.c(b, "metaData");
                int c6 = dx0.c(b, "serial");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new DianaComplicationRingStyle(b.getString(c), b.getString(c2), b.getString(c3), RingStyleDao_Impl.this.__ringStyleConverter.b(b.getString(c4)), RingStyleDao_Impl.this.__ringStyleConverter.c(b.getString(c5)), b.getString(c6)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public RingStyleDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfDianaComplicationRingStyle = new Anon1(qw0);
        this.__preparedStmtOfClearAllData = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public void clearAllData() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAllData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public List<DianaComplicationRingStyle> getRingStyles() {
        tw0 f = tw0.f("SELECT * FROM DianaComplicationRingStyle", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "category");
            int c3 = dx0.c(b, "name");
            int c4 = dx0.c(b, "data");
            int c5 = dx0.c(b, "metaData");
            int c6 = dx0.c(b, "serial");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new DianaComplicationRingStyle(b.getString(c), b.getString(c2), b.getString(c3), this.__ringStyleConverter.b(b.getString(c4)), this.__ringStyleConverter.c(b.getString(c5)), b.getString(c6)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public LiveData<List<DianaComplicationRingStyle>> getRingStylesAsLiveData() {
        tw0 f = tw0.f("SELECT * FROM DianaComplicationRingStyle", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"DianaComplicationRingStyle"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public DianaComplicationRingStyle getRingStylesBySerial(String str) {
        DianaComplicationRingStyle dianaComplicationRingStyle = null;
        tw0 f = tw0.f("SELECT * FROM DianaComplicationRingStyle WHERE serial = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "category");
            int c3 = dx0.c(b, "name");
            int c4 = dx0.c(b, "data");
            int c5 = dx0.c(b, "metaData");
            int c6 = dx0.c(b, "serial");
            if (b.moveToFirst()) {
                dianaComplicationRingStyle = new DianaComplicationRingStyle(b.getString(c), b.getString(c2), b.getString(c3), this.__ringStyleConverter.b(b.getString(c4)), this.__ringStyleConverter.c(b.getString(c5)), b.getString(c6));
            }
            return dianaComplicationRingStyle;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public void insertRingStyle(DianaComplicationRingStyle dianaComplicationRingStyle) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaComplicationRingStyle.insert((jw0<DianaComplicationRingStyle>) dianaComplicationRingStyle);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public void insertRingStyles(List<DianaComplicationRingStyle> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaComplicationRingStyle.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
