package com.portfolio.platform.data.source.local.diana.heartrate;

import com.fossil.fl5;
import com.fossil.pq7;
import com.fossil.xw7;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryLocalDataSource$loadInitial$Anon1 implements fl5.b {
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryLocalDataSource this$0;

    @DexIgnore
    public HeartRateSummaryLocalDataSource$loadInitial$Anon1(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        this.this$0 = heartRateSummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.fossil.fl5.b
    public final void run(fl5.b.a aVar) {
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource = this.this$0;
        heartRateSummaryLocalDataSource.calculateStartDate(heartRateSummaryLocalDataSource.mCreatedDate);
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource2 = this.this$0;
        Date mStartDate = heartRateSummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        pq7.b(aVar, "helperCallback");
        xw7 unused = heartRateSummaryLocalDataSource2.loadData(mStartDate, mEndDate, aVar);
    }
}
