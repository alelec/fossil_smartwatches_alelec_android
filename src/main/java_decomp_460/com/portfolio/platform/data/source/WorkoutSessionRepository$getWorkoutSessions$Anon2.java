package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ao7;
import com.fossil.bn5;
import com.fossil.br7;
import com.fossil.c47;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gi0;
import com.fossil.h47;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.ss0;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$2", f = "WorkoutSessionRepository.kt", l = {53}, m = "invokeSuspend")
public final class WorkoutSessionRepository$getWorkoutSessions$Anon2 extends ko7 implements vp7<iv7, qn7<? super LiveData<h47<? extends List<WorkoutSession>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements gi0<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ FitnessDatabase $fitnessDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startDate;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionRepository$getWorkoutSessions$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends c47<List<WorkoutSession>, ApiResponse<ServerWorkoutSession>> {
            @DexIgnore
            public /* final */ /* synthetic */ List $fitnessDataList;
            @DexIgnore
            public /* final */ /* synthetic */ int $limit;
            @DexIgnore
            public /* final */ /* synthetic */ br7 $offset;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, br7 br7, int i, List list) {
                this.this$0 = anon1_Level2;
                this.$offset = br7;
                this.$limit = i;
                this.$fitnessDataList = list;
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object createCall(qn7<? super q88<ApiResponse<ServerWorkoutSession>>> qn7) {
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiService;
                String k = lk5.k(this.this$0.$startDate);
                pq7.b(k, "DateHelper.formatShortDate(startDate)");
                String k2 = lk5.k(this.this$0.$endDate);
                pq7.b(k2, "DateHelper.formatShortDate(endDate)");
                return apiServiceV2.getWorkoutSessions(k, k2, this.$offset.element, this.$limit, qn7);
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object loadFromDb(qn7<? super LiveData<List<WorkoutSession>>> qn7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
                local.d(tAG$app_fossilRelease, "getWorkoutSessions - loadFromDb -- end=" + this.this$0.this$0.$end + ", startDate=" + this.this$0.$startDate + ", endDate=" + this.this$0.$endDate);
                WorkoutDao workoutDao = this.this$0.$fitnessDatabase.getWorkoutDao();
                Date date = this.this$0.$startDate;
                pq7.b(date, GoalPhase.COLUMN_START_DATE);
                Date date2 = this.this$0.$endDate;
                pq7.b(date2, GoalPhase.COLUMN_END_DATE);
                return workoutDao.getWorkoutSessionsDesc(date, date2);
            }

            @DexIgnore
            @Override // com.fossil.c47
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - onFetchFailed");
            }

            @DexIgnore
            public Object processContinueFetching(ApiResponse<ServerWorkoutSession> apiResponse, qn7<? super Boolean> qn7) {
                Boolean a2;
                Range range = apiResponse.get_range();
                if (range == null || (a2 = ao7.a(range.isHasNext())) == null || !a2.booleanValue()) {
                    return ao7.a(false);
                }
                FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - processContinueFetching -- hasNext=TRUE");
                this.$offset.element += this.$limit;
                return ao7.a(true);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
            @Override // com.fossil.c47
            public /* bridge */ /* synthetic */ Object processContinueFetching(ApiResponse<ServerWorkoutSession> apiResponse, qn7 qn7) {
                return processContinueFetching(apiResponse, (qn7<? super Boolean>) qn7);
            }

            @DexIgnore
            public Object saveCallResult(ApiResponse<ServerWorkoutSession> apiResponse, qn7<? super tl7> qn7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
                StringBuilder sb = new StringBuilder();
                sb.append("getWorkoutSessions - saveCallResult -- item.size=");
                sb.append(apiResponse.get_items().size());
                sb.append(", hasNext=");
                Range range = apiResponse.get_range();
                sb.append(range != null ? ao7.a(range.isHasNext()) : null);
                local.d(tAG$app_fossilRelease, sb.toString());
                ArrayList arrayList = new ArrayList();
                Iterator<T> it = apiResponse.get_items().iterator();
                while (it.hasNext()) {
                    WorkoutSession workoutSession = it.next().toWorkoutSession();
                    if (workoutSession != null) {
                        arrayList.add(workoutSession);
                    }
                }
                if (!apiResponse.get_items().isEmpty()) {
                    this.this$0.$fitnessDatabase.getWorkoutDao().upsertListWorkoutSession(arrayList);
                }
                FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - saveCallResult -- DONE!!!");
                return tl7.f3441a;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
            @Override // com.fossil.c47
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<ServerWorkoutSession> apiResponse, qn7 qn7) {
                return saveCallResult(apiResponse, (qn7<? super tl7>) qn7);
            }

            @DexIgnore
            public boolean shouldFetch(List<WorkoutSession> list) {
                return this.this$0.this$0.$shouldFetch && this.$fitnessDataList.isEmpty();
            }
        }

        @DexIgnore
        public Anon1_Level2(WorkoutSessionRepository$getWorkoutSessions$Anon2 workoutSessionRepository$getWorkoutSessions$Anon2, FitnessDatabase fitnessDatabase, Date date, Date date2) {
            this.this$0 = workoutSessionRepository$getWorkoutSessions$Anon2;
            this.$fitnessDatabase = fitnessDatabase;
            this.$startDate = date;
            this.$endDate = date2;
        }

        @DexIgnore
        public final LiveData<h47<List<WorkoutSession>>> apply(List<FitnessDataWrapper> list) {
            br7 br7 = new br7();
            br7.element = 0;
            return new Anon1_Level3(this, br7, 100, list).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$getWorkoutSessions$Anon2(WorkoutSessionRepository workoutSessionRepository, Date date, Date date2, boolean z, qn7 qn7) {
        super(2, qn7);
        this.this$0 = workoutSessionRepository;
        this.$start = date;
        this.$end = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        WorkoutSessionRepository$getWorkoutSessions$Anon2 workoutSessionRepository$getWorkoutSessions$Anon2 = new WorkoutSessionRepository$getWorkoutSessions$Anon2(this.this$0, this.$start, this.$end, this.$shouldFetch, qn7);
        workoutSessionRepository$getWorkoutSessions$Anon2.p$ = (iv7) obj;
        return workoutSessionRepository$getWorkoutSessions$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super LiveData<h47<? extends List<WorkoutSession>>>> qn7) {
        return ((WorkoutSessionRepository$getWorkoutSessions$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Date V;
        Object y;
        Date date;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            V = lk5.V(this.$start);
            Date E = lk5.E(this.$end);
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.L$1 = V;
            this.L$2 = E;
            this.label = 1;
            y = bn5.y(this);
            if (y == d) {
                return d;
            }
            date = E;
        } else if (i == 1) {
            V = (Date) this.L$1;
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            date = (Date) this.L$2;
            y = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) y;
        FitnessDataDao fitnessDataDao = fitnessDatabase.getFitnessDataDao();
        pq7.b(V, GoalPhase.COLUMN_START_DATE);
        pq7.b(date, GoalPhase.COLUMN_END_DATE);
        return ss0.c(fitnessDataDao.getFitnessDataLiveData(V, date), new Anon1_Level2(this, fitnessDatabase, V, date));
    }
}
