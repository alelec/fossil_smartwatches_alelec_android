package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.fossil.r77;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesWFTemplateRemoteFactory implements Factory<r77> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesWFTemplateRemoteFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        this.module = portfolioDatabaseModule;
        this.apiProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesWFTemplateRemoteFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        return new PortfolioDatabaseModule_ProvidesWFTemplateRemoteFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static r77 providesWFTemplateRemote(PortfolioDatabaseModule portfolioDatabaseModule, ApiServiceV2 apiServiceV2) {
        r77 providesWFTemplateRemote = portfolioDatabaseModule.providesWFTemplateRemote(apiServiceV2);
        lk7.c(providesWFTemplateRemote, "Cannot return null from a non-@Nullable @Provides method");
        return providesWFTemplateRemote;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public r77 get() {
        return providesWFTemplateRemote(this.module, this.apiProvider.get());
    }
}
