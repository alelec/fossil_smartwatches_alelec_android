package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.ActivitiesRepository$updateActivityPinType$2", f = "ActivitiesRepository.kt", l = {183}, m = "invokeSuspend")
public final class ActivitiesRepository$updateActivityPinType$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $activitySampleList;
    @DexIgnore
    public /* final */ /* synthetic */ int $pinType;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$updateActivityPinType$Anon2(List list, int i, qn7 qn7) {
        super(2, qn7);
        this.$activitySampleList = list;
        this.$pinType = i;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ActivitiesRepository$updateActivityPinType$Anon2 activitiesRepository$updateActivityPinType$Anon2 = new ActivitiesRepository$updateActivityPinType$Anon2(this.$activitySampleList, this.$pinType, qn7);
        activitiesRepository$updateActivityPinType$Anon2.p$ = (iv7) obj;
        return activitiesRepository$updateActivityPinType$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((ActivitiesRepository$updateActivityPinType$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object y;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            for (SampleRaw sampleRaw : this.$activitySampleList) {
                sampleRaw.setPinType(this.$pinType);
            }
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.label = 1;
            y = bn5.y(this);
            if (y == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            y = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ((FitnessDatabase) y).sampleRawDao().upsertListActivitySample(this.$activitySampleList);
        return tl7.f3441a;
    }
}
