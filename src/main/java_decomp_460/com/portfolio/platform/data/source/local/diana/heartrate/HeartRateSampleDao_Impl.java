package com.portfolio.platform.data.source.local.diana.heartrate;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.h05;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.qz4;
import com.fossil.sz4;
import com.fossil.tw0;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.fossil.xw0;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSampleDao_Impl extends HeartRateSampleDao {
    @DexIgnore
    public /* final */ qz4 __dateShortStringConverter; // = new qz4();
    @DexIgnore
    public /* final */ sz4 __dateTimeISOStringConverter; // = new sz4();
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<HeartRateSample> __insertionAdapterOfHeartRateSample;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteAllHeartRateSamples;
    @DexIgnore
    public /* final */ h05 __restingConverter; // = new h05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<HeartRateSample> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, HeartRateSample heartRateSample) {
            if (heartRateSample.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, heartRateSample.getId());
            }
            px0.bindDouble(2, (double) heartRateSample.getAverage());
            String a2 = HeartRateSampleDao_Impl.this.__dateShortStringConverter.a(heartRateSample.getDate());
            if (a2 == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, a2);
            }
            px0.bindLong(4, heartRateSample.getCreatedAt());
            px0.bindLong(5, heartRateSample.getUpdatedAt());
            String a3 = HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(heartRateSample.getEndTime());
            if (a3 == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, a3);
            }
            String a4 = HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(heartRateSample.getStartTime());
            if (a4 == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, a4);
            }
            px0.bindLong(8, (long) heartRateSample.getTimezoneOffsetInSecond());
            px0.bindLong(9, (long) heartRateSample.getMin());
            px0.bindLong(10, (long) heartRateSample.getMax());
            px0.bindLong(11, (long) heartRateSample.getMinuteCount());
            String b = HeartRateSampleDao_Impl.this.__restingConverter.b(heartRateSample.getResting());
            if (b == null) {
                px0.bindNull(12);
            } else {
                px0.bindString(12, b);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `heart_rate_sample` (`id`,`average`,`date`,`createdAt`,`updatedAt`,`endTime`,`startTime`,`timezoneOffset`,`min`,`max`,`minuteCount`,`resting`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM heart_rate_sample";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<HeartRateSample>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon3(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<HeartRateSample> call() throws Exception {
            Cursor b = ex0.b(HeartRateSampleDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "id");
                int c2 = dx0.c(b, GoalTrackingSummary.COLUMN_AVERAGE);
                int c3 = dx0.c(b, "date");
                int c4 = dx0.c(b, "createdAt");
                int c5 = dx0.c(b, "updatedAt");
                int c6 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
                int c7 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
                int c8 = dx0.c(b, "timezoneOffset");
                int c9 = dx0.c(b, "min");
                int c10 = dx0.c(b, "max");
                int c11 = dx0.c(b, "minuteCount");
                int c12 = dx0.c(b, "resting");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new HeartRateSample(b.getString(c), b.getFloat(c2), HeartRateSampleDao_Impl.this.__dateShortStringConverter.b(b.getString(c3)), b.getLong(c4), b.getLong(c5), HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c6)), HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c7)), b.getInt(c8), b.getInt(c9), b.getInt(c10), b.getInt(c11), HeartRateSampleDao_Impl.this.__restingConverter.a(b.getString(c12))));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public HeartRateSampleDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfHeartRateSample = new Anon1(qw0);
        this.__preparedStmtOfDeleteAllHeartRateSamples = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public void deleteAllHeartRateSamples() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteAllHeartRateSamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllHeartRateSamples.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public HeartRateSample getHeartRateSample(String str) {
        tw0 f = tw0.f("SELECT * FROM heart_rate_sample WHERE id = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        HeartRateSample heartRateSample = null;
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, GoalTrackingSummary.COLUMN_AVERAGE);
            int c3 = dx0.c(b, "date");
            int c4 = dx0.c(b, "createdAt");
            int c5 = dx0.c(b, "updatedAt");
            int c6 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
            int c7 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c8 = dx0.c(b, "timezoneOffset");
            int c9 = dx0.c(b, "min");
            int c10 = dx0.c(b, "max");
            int c11 = dx0.c(b, "minuteCount");
            int c12 = dx0.c(b, "resting");
            if (b.moveToFirst()) {
                heartRateSample = new HeartRateSample(b.getString(c), b.getFloat(c2), this.__dateShortStringConverter.b(b.getString(c3)), b.getLong(c4), b.getLong(c5), this.__dateTimeISOStringConverter.b(b.getString(c6)), this.__dateTimeISOStringConverter.b(b.getString(c7)), b.getInt(c8), b.getInt(c9), b.getInt(c10), b.getInt(c11), this.__restingConverter.a(b.getString(c12)));
            }
            return heartRateSample;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public LiveData<List<HeartRateSample>> getHeartRateSamples(Date date, Date date2) {
        tw0 f = tw0.f("SELECT * FROM heart_rate_sample WHERE date >= ? AND date <= ? ORDER BY startTime ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"heart_rate_sample"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public void insertHeartRateSample(HeartRateSample heartRateSample) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHeartRateSample.insert((jw0<HeartRateSample>) heartRateSample);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public void upsertHeartRateSampleList(List<HeartRateSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHeartRateSample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
