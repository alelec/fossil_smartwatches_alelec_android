package com.portfolio.platform.data.source;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.br7;
import com.fossil.bs7;
import com.fossil.bw7;
import com.fossil.dl7;
import com.fossil.eu7;
import com.fossil.fitness.WorkoutType;
import com.fossil.go7;
import com.fossil.gu7;
import com.fossil.h42;
import com.fossil.hm7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.kq7;
import com.fossil.lu7;
import com.fossil.nt3;
import com.fossil.oh2;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.rh2;
import com.fossil.tl7;
import com.fossil.uh2;
import com.fossil.uk5;
import com.fossil.ur7;
import com.fossil.ux7;
import com.fossil.vh2;
import com.fossil.wh2;
import com.fossil.wi2;
import com.fossil.xn7;
import com.fossil.xw7;
import com.fossil.yn7;
import com.fossil.zh2;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOCalorie;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWODistance;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOStep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.CoroutineExceptionHandler;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "ThirdPartyRepository";
    @DexIgnore
    public ActivitiesRepository mActivitiesRepository;
    @DexIgnore
    public /* final */ CoroutineExceptionHandler mExceptionHandling; // = new ThirdPartyRepository$$special$$inlined$CoroutineExceptionHandler$Anon1(CoroutineExceptionHandler.q);
    @DexIgnore
    public uk5 mGoogleFitHelper;
    @DexIgnore
    public PortfolioApp mPortfolioApp;
    @DexIgnore
    public /* final */ iv7 mPushDataSupervisorJob; // = jv7.a(ux7.b(null, 1, null).plus(bw7.b()).plus(this.mExceptionHandling));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingThirdPartyDataCallback {
        @DexIgnore
        Object onComplete();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[WorkoutType.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[WorkoutType.RUNNING.ordinal()] = 1;
            $EnumSwitchMapping$0[WorkoutType.CYCLING.ordinal()] = 2;
            $EnumSwitchMapping$0[WorkoutType.TREADMILL.ordinal()] = 3;
            $EnumSwitchMapping$0[WorkoutType.ELLIPTICAL.ordinal()] = 4;
            $EnumSwitchMapping$0[WorkoutType.WEIGHTS.ordinal()] = 5;
            $EnumSwitchMapping$0[WorkoutType.UNKNOWN.ordinal()] = 6;
        }
        */
    }

    @DexIgnore
    public ThirdPartyRepository(uk5 uk5, ActivitiesRepository activitiesRepository, PortfolioApp portfolioApp) {
        pq7.c(uk5, "mGoogleFitHelper");
        pq7.c(activitiesRepository, "mActivitiesRepository");
        pq7.c(portfolioApp, "mPortfolioApp");
        this.mGoogleFitHelper = uk5;
        this.mActivitiesRepository = activitiesRepository;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    private final List<GFitSleep> convertListMFSleepSessionToListGFitSleep(List<MFSleepSession> list) {
        ArrayList arrayList = new ArrayList();
        for (MFSleepSession mFSleepSession : list) {
            long j = (long) 1000;
            arrayList.add(new GFitSleep(mFSleepSession.getRealSleepMinutes(), ((long) mFSleepSession.getRealStartTime()) * j, ((long) mFSleepSession.getRealEndTime()) * j));
        }
        return arrayList;
    }

    @DexIgnore
    private final DataSet createDataSetForActiveMinutes(GFitActiveTime gFitActiveTime, String str) {
        uh2.a aVar = new uh2.a();
        aVar.b(this.mPortfolioApp);
        aVar.d(DataType.k);
        aVar.f(this.mPortfolioApp.getString(2131887274));
        aVar.g(0);
        aVar.e(new vh2(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(aVar.a());
        List a0 = pm7.a0(gFitActiveTime.getActiveTimes());
        ur7 l = bs7.l(bs7.m(0, a0.size()), 2);
        int a2 = l.a();
        int b = l.b();
        int c = l.c();
        if (c < 0 ? a2 >= b : a2 <= b) {
            while (true) {
                DataPoint h = f.h();
                h.o0(((Number) a0.get(a2)).longValue(), ((Number) a0.get(a2 + 1)).longValue(), TimeUnit.MILLISECONDS);
                h.L(wh2.e).A("unknown");
                f.c(h);
                if (a2 == b) {
                    break;
                }
                a2 += c;
            }
        }
        pq7.b(f, "dataSetForActiveMinutes");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForCalories(List<GFitSample> list, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForCalories samples " + list);
        uh2.a aVar = new uh2.a();
        aVar.b(this.mPortfolioApp);
        aVar.d(DataType.m);
        aVar.f(this.mPortfolioApp.getString(2131887274));
        aVar.g(0);
        aVar.e(new vh2(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(aVar.a());
        for (GFitSample gFitSample : list) {
            float component3 = gFitSample.component3();
            long component4 = gFitSample.component4();
            long component5 = gFitSample.component5();
            try {
                DataPoint h = f.h();
                h.o0(component4, component5, TimeUnit.MILLISECONDS);
                h.L(wh2.J).D(component3);
                f.c(h);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d(TAG, "add calories " + component3 + " as data point " + h);
            } catch (Exception e) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.e(TAG, "createDataSetForCalories() fall into error=" + e.getMessage());
            }
        }
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        local4.d(TAG, "createDataSetForCalories " + f);
        pq7.b(f, "dataSetForCalories");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForDistances(List<GFitSample> list, String str) {
        uh2.a aVar = new uh2.a();
        aVar.b(this.mPortfolioApp);
        aVar.d(DataType.A);
        aVar.f(this.mPortfolioApp.getString(2131887274));
        aVar.g(0);
        aVar.e(new vh2(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(aVar.a());
        for (GFitSample gFitSample : list) {
            float component2 = gFitSample.component2();
            long component4 = gFitSample.component4();
            long component5 = gFitSample.component5();
            DataPoint h = f.h();
            h.o0(component4, component5, TimeUnit.MILLISECONDS);
            h.L(wh2.x).D(component2);
            f.c(h);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForDistances " + f);
        pq7.b(f, "dataSetForDistances");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForHeartRates(List<GFitHeartRate> list, String str) {
        uh2.a aVar = new uh2.a();
        aVar.b(this.mPortfolioApp);
        aVar.d(DataType.x);
        aVar.f(this.mPortfolioApp.getString(2131887274));
        aVar.g(0);
        aVar.e(new vh2(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(aVar.a());
        for (GFitHeartRate gFitHeartRate : list) {
            float component1 = gFitHeartRate.component1();
            long component2 = gFitHeartRate.component2();
            long component3 = gFitHeartRate.component3();
            DataPoint h = f.h();
            h.o0(component2, component3, TimeUnit.MILLISECONDS);
            h.L(wh2.s).D(component1);
            f.c(h);
        }
        pq7.b(f, "dataSet");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForSteps(List<GFitSample> list, String str) {
        uh2.a aVar = new uh2.a();
        aVar.b(this.mPortfolioApp);
        aVar.d(DataType.f);
        aVar.f(this.mPortfolioApp.getString(2131887274));
        aVar.g(0);
        aVar.e(new vh2(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(aVar.a());
        for (GFitSample gFitSample : list) {
            int component1 = gFitSample.component1();
            long component4 = gFitSample.component4();
            long component5 = gFitSample.component5();
            DataPoint h = f.h();
            h.o0(component4, component5, TimeUnit.MILLISECONDS);
            h.L(wh2.h).F(component1);
            f.c(h);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForSteps " + f);
        pq7.b(f, "dataSetForSteps");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForWOCalories(List<GFitWOCalorie> list, String str, long j, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForWOCalories steps " + list + " startTime " + j + " endTime " + j2);
        uh2.a aVar = new uh2.a();
        aVar.b(this.mPortfolioApp);
        aVar.d(DataType.m);
        aVar.f(this.mPortfolioApp.getString(2131887274));
        aVar.g(0);
        aVar.e(new vh2(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(aVar.a());
        try {
            for (GFitWOCalorie gFitWOCalorie : list) {
                if (gFitWOCalorie.getStartTime() >= j && gFitWOCalorie.getEndTime() <= j2) {
                    DataPoint h = f.h();
                    h.o0(gFitWOCalorie.getStartTime(), gFitWOCalorie.getEndTime(), TimeUnit.MILLISECONDS);
                    h.L(wh2.J).D(gFitWOCalorie.getCalorie());
                    f.c(h);
                }
            }
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "exception when add workout calories data point " + e);
        }
        pq7.b(f, "dataSetForCalories");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForWODistances(List<GFitWODistance> list, String str, long j, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForWODistances listDistances " + list + " startTime " + j + " endTime " + j2);
        uh2.a aVar = new uh2.a();
        aVar.b(this.mPortfolioApp);
        aVar.d(DataType.A);
        aVar.f(this.mPortfolioApp.getString(2131887274));
        aVar.g(0);
        aVar.e(new vh2(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(aVar.a());
        for (GFitWODistance gFitWODistance : list) {
            if (gFitWODistance.getStartTime() >= j && gFitWODistance.getEndTime() <= j2) {
                DataPoint h = f.h();
                h.o0(gFitWODistance.getStartTime(), gFitWODistance.getEndTime(), TimeUnit.MILLISECONDS);
                h.L(wh2.x).D(gFitWODistance.getDistance());
                f.c(h);
            }
        }
        pq7.b(f, "dataSetForDistances");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForWOHeartRates(List<GFitWOHeartRate> list, String str, long j, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForWOHeartRates listHeartRates " + list + " startTime " + j + " endTime " + j2);
        uh2.a aVar = new uh2.a();
        aVar.b(this.mPortfolioApp);
        aVar.d(DataType.x);
        aVar.f(this.mPortfolioApp.getString(2131887274));
        aVar.g(0);
        aVar.e(new vh2(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(aVar.a());
        try {
            for (GFitWOHeartRate gFitWOHeartRate : list) {
                if (gFitWOHeartRate.getStartTime() >= j && gFitWOHeartRate.getEndTime() <= j2) {
                    DataPoint h = f.h();
                    h.o0(gFitWOHeartRate.getStartTime(), gFitWOHeartRate.getEndTime(), TimeUnit.MILLISECONDS);
                    h.L(wh2.s).D(gFitWOHeartRate.getHeartRate());
                    f.c(h);
                }
            }
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "exception when add workout heartrate data point " + e);
        }
        pq7.b(f, "dataSet");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForWOSteps(List<GFitWOStep> list, String str, long j, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForWOSteps steps " + list + " startTime " + j + " endTime " + j2);
        uh2.a aVar = new uh2.a();
        aVar.b(this.mPortfolioApp);
        aVar.d(DataType.f);
        aVar.f(this.mPortfolioApp.getString(2131887274));
        aVar.g(0);
        aVar.e(new vh2(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(aVar.a());
        try {
            for (GFitWOStep gFitWOStep : list) {
                if (gFitWOStep.getStartTime() >= j && gFitWOStep.getEndTime() <= j2) {
                    DataPoint h = f.h();
                    h.o0(gFitWOStep.getStartTime(), gFitWOStep.getEndTime(), TimeUnit.MILLISECONDS);
                    h.L(wh2.h).F(gFitWOStep.getStep());
                    f.c(h);
                }
            }
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "exception when add workout steps data point " + e);
        }
        pq7.b(f, "dataSetForSteps");
        return f;
    }

    @DexIgnore
    private final wi2 createWorkoutSession(GFitWorkoutSession gFitWorkoutSession, String str) {
        DataSet createDataSetForWOSteps = createDataSetForWOSteps(gFitWorkoutSession.getSteps(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWOCalories = createDataSetForWOCalories(gFitWorkoutSession.getCalories(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWODistances = createDataSetForWODistances(gFitWorkoutSession.getDistances(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWOHeartRates = createDataSetForWOHeartRates(gFitWorkoutSession.getHeartRates(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createWorkoutSession dataSetForSteps " + createDataSetForWOSteps + " dataSetForCalories " + createDataSetForWOCalories + " dataSetForDistances " + createDataSetForWODistances + " dataSetForHeartRates " + createDataSetForWOHeartRates);
        WorkoutType workoutType = getWorkoutType(gFitWorkoutSession.getWorkoutType());
        zh2.a aVar = new zh2.a();
        aVar.f(workoutType.name());
        aVar.b(getFitnessActivitiesType(workoutType));
        aVar.g(gFitWorkoutSession.getStartTime(), TimeUnit.MILLISECONDS);
        aVar.d(gFitWorkoutSession.getEndTime(), TimeUnit.MILLISECONDS);
        zh2 a2 = aVar.a();
        wi2.a aVar2 = new wi2.a();
        aVar2.c(a2);
        if (!createDataSetForWOSteps.isEmpty()) {
            aVar2.a(createDataSetForWOSteps);
        }
        if (!createDataSetForWOCalories.isEmpty()) {
            aVar2.a(createDataSetForWOCalories);
        }
        if (!createDataSetForWODistances.isEmpty()) {
            aVar2.a(createDataSetForWODistances);
        }
        if (!createDataSetForWOHeartRates.isEmpty()) {
            aVar2.a(createDataSetForWOHeartRates);
        }
        wi2 b = aVar2.b();
        pq7.b(b, "sessionBuilder.build()");
        return b;
    }

    @DexIgnore
    private final String getFitnessActivitiesType(WorkoutType workoutType) {
        switch (WhenMappings.$EnumSwitchMapping$0[workoutType.ordinal()]) {
            case 1:
                return "running";
            case 2:
                return "biking";
            case 3:
                return "walking.treadmill";
            case 4:
                return "elliptical";
            case 5:
                return "weightlifting";
            case 6:
                return "unknown";
            default:
                return FacebookRequestErrorClassification.KEY_OTHER;
        }
    }

    @DexIgnore
    private final WorkoutType getWorkoutType(int i) {
        return i < WorkoutType.values().length ? WorkoutType.values()[i] : WorkoutType.UNKNOWN;
    }

    @DexIgnore
    public static /* synthetic */ Object saveData$default(ThirdPartyRepository thirdPartyRepository, List list, GFitActiveTime gFitActiveTime, List list2, List list3, List list4, qn7 qn7, int i, Object obj) {
        return thirdPartyRepository.saveData((i & 1) != 0 ? hm7.e() : list, (i & 2) != 0 ? null : gFitActiveTime, (i & 4) != 0 ? hm7.e() : list2, (i & 8) != 0 ? hm7.e() : list3, (i & 16) != 0 ? hm7.e() : list4, qn7);
    }

    @DexIgnore
    public static /* synthetic */ Object uploadData$default(ThirdPartyRepository thirdPartyRepository, PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, qn7 qn7, int i, Object obj) {
        if ((i & 1) != 0) {
            pushPendingThirdPartyDataCallback = null;
        }
        return thirdPartyRepository.uploadData(pushPendingThirdPartyDataCallback, qn7);
    }

    @DexIgnore
    public final DataSet createDataSetForSleepData(GFitSleep gFitSleep, vh2 vh2) {
        pq7.c(gFitSleep, "gFitSleep");
        pq7.c(vh2, "device");
        uh2.a aVar = new uh2.a();
        aVar.b(this.mPortfolioApp);
        aVar.d(DataType.k);
        aVar.f(this.mPortfolioApp.getString(2131887274));
        aVar.g(0);
        aVar.e(vh2);
        DataSet f = DataSet.f(aVar.a());
        long startTime = gFitSleep.getStartTime();
        long endTime = gFitSleep.getEndTime();
        DataPoint h = f.h();
        h.o0(startTime, endTime, TimeUnit.MILLISECONDS);
        h.L(wh2.e).F(gFitSleep.getSleepMins());
        f.c(h);
        pq7.b(f, "dataSetForSleep");
        return f;
    }

    @DexIgnore
    public final ActivitiesRepository getMActivitiesRepository() {
        return this.mActivitiesRepository;
    }

    @DexIgnore
    public final PortfolioApp getMPortfolioApp() {
        return this.mPortfolioApp;
    }

    @DexIgnore
    public final Object pushPendingData(PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, qn7<? super xw7> qn7) {
        return eu7.g(bw7.b(), new ThirdPartyRepository$pushPendingData$Anon2(this, pushPendingThirdPartyDataCallback, null), qn7);
    }

    @DexIgnore
    public final Object saveData(List<GFitSample> list, GFitActiveTime gFitActiveTime, List<GFitHeartRate> list2, List<GFitWorkoutSession> list3, List<MFSleepSession> list4, qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new ThirdPartyRepository$saveData$Anon2(this, list, gFitActiveTime, list2, list3, list4, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final /* synthetic */ Object saveGFitActiveTimeToGoogleFit(List<GFitActiveTime> list, String str, qn7<Object> qn7) {
        lu7 lu7 = new lu7(xn7.c(qn7), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitActiveTimeToGoogleFit");
        GoogleSignInAccount b = h42.b(getMPortfolioApp());
        if (b == null) {
            FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitActiveTimeToGoogleFit");
            if (lu7.isActive()) {
                dl7.a aVar = dl7.Companion;
                lu7.resumeWith(dl7.m1constructorimpl(null));
            }
        } else {
            rh2 a2 = oh2.a(getMPortfolioApp(), b);
            FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitActiveTime to Google Fit");
            int size = list.size();
            br7 br7 = new br7();
            br7.element = 0;
            for (T t : list) {
                DataSet createDataSetForActiveMinutes = createDataSetForActiveMinutes(t, str);
                List<DataPoint> k = createDataSetForActiveMinutes.k();
                pq7.b(k, "dataSet.dataPoints");
                Iterator<T> it = k.iterator();
                while (it.hasNext()) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "saveGFitActiveTimeToGoogleFit send data " + ((Object) it.next()));
                }
                nt3<Void> s = a2.s(createDataSetForActiveMinutes);
                s.f(new ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(t, a2, br7, size, lu7, this, list, str));
                s.d(new ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(a2, br7, size, lu7, this, list, str));
            }
        }
        Object t2 = lu7.t();
        if (t2 == yn7.d()) {
            go7.c(qn7);
        }
        return t2;
    }

    @DexIgnore
    public final /* synthetic */ Object saveGFitHeartRateToGoogleFit(List<GFitHeartRate> list, String str, qn7<Object> qn7) {
        lu7 lu7 = new lu7(xn7.c(qn7), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitHeartRateToGoogleFit");
        GoogleSignInAccount b = h42.b(getMPortfolioApp());
        if (b == null) {
            FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitHeartRateToGoogleFit");
            if (lu7.isActive()) {
                dl7.a aVar = dl7.Companion;
                lu7.resumeWith(dl7.m1constructorimpl(null));
            }
        } else {
            rh2 a2 = oh2.a(getMPortfolioApp(), b);
            FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitHeartRate to Google Fit");
            List<List> A = pm7.A(list, 500);
            int size = A.size();
            br7 br7 = new br7();
            br7.element = 0;
            for (List list2 : A) {
                DataSet createDataSetForHeartRates = createDataSetForHeartRates(list2, str);
                List<DataPoint> k = createDataSetForHeartRates.k();
                pq7.b(k, "dataSet.dataPoints");
                Iterator<T> it = k.iterator();
                while (it.hasNext()) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "saveGFitHeartRateToGoogleFit send data " + ((Object) it.next()));
                }
                nt3<Void> s = a2.s(createDataSetForHeartRates);
                s.f(new ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(list2, a2, br7, size, lu7, this, list, str));
                s.d(new ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(a2, br7, size, lu7, this, list, str));
            }
        }
        Object t = lu7.t();
        if (t == yn7.d()) {
            go7.c(qn7);
        }
        return t;
    }

    @DexIgnore
    public final /* synthetic */ Object saveGFitSampleToGoogleFit(List<GFitSample> list, String str, qn7<Object> qn7) {
        lu7 lu7 = new lu7(xn7.c(qn7), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitSampleToGoogleFit");
        GoogleSignInAccount b = h42.b(getMPortfolioApp());
        if (b == null) {
            FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitSampleToGoogleFit");
            if (lu7.isActive()) {
                dl7.a aVar = dl7.Companion;
                lu7.resumeWith(dl7.m1constructorimpl(null));
            }
        } else {
            rh2 a2 = oh2.a(getMPortfolioApp(), b);
            FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitSample to Google Fit");
            List<List> A = pm7.A(list, 500);
            int size = A.size();
            br7 br7 = new br7();
            int i = 0;
            br7.element = 0;
            for (List list2 : A) {
                ArrayList arrayList = new ArrayList();
                DataSet createDataSetForSteps = createDataSetForSteps(list2, str);
                DataSet createDataSetForDistances = createDataSetForDistances(list2, str);
                DataSet createDataSetForCalories = createDataSetForCalories(list2, str);
                arrayList.add(createDataSetForSteps);
                arrayList.add(createDataSetForDistances);
                arrayList.add(createDataSetForCalories);
                int size2 = arrayList.size();
                br7 br72 = new br7();
                br72.element = i;
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    DataSet dataSet = (DataSet) it.next();
                    pq7.b(dataSet, "dataSet");
                    List<DataPoint> k = dataSet.k();
                    pq7.b(k, "dataSet.dataPoints");
                    if (!k.isEmpty()) {
                        List<DataPoint> k2 = dataSet.k();
                        pq7.b(k2, "dataSet.dataPoints");
                        Iterator<T> it2 = k2.iterator();
                        while (it2.hasNext()) {
                            FLogger.INSTANCE.getLocal().d(TAG, "saveGFitSampleToGoogleFit send data " + ((Object) it2.next()));
                        }
                        nt3<Void> s = a2.s(dataSet);
                        s.f(new ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(br72, size2, list2, a2, br7, lu7, size, this, list, str));
                        s.d(new ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(size2, a2, br7, lu7, size, this, list, str));
                        pq7.b(s, "historyClient.insertData\u2026                        }");
                    } else {
                        br7.element++;
                    }
                    i = 0;
                }
            }
        }
        Object t = lu7.t();
        if (t == yn7.d()) {
            go7.c(qn7);
        }
        return t;
    }

    @DexIgnore
    public final Object saveGFitSleepDataToGoogleFit(List<GFitSleep> list, String str, qn7<Object> qn7) {
        lu7 lu7 = new lu7(xn7.c(qn7), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitSleepDataToGoogleFit");
        GoogleSignInAccount b = h42.b(getMPortfolioApp());
        if (b != null) {
            int size = list.size();
            br7 br7 = new br7();
            br7.element = 0;
            vh2 vh2 = new vh2(getMPortfolioApp().getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3);
            ArrayList arrayList = new ArrayList();
            for (T t : list) {
                DataSet createDataSetForSleepData = createDataSetForSleepData(t, vh2);
                zh2.a aVar = new zh2.a();
                aVar.e(str + new DateTime());
                aVar.f(getMPortfolioApp().getString(2131887274));
                aVar.c("User Sleep");
                aVar.g(t.getStartTime(), TimeUnit.MILLISECONDS);
                aVar.d(t.getEndTime(), TimeUnit.MILLISECONDS);
                aVar.b("sleep");
                zh2 a2 = aVar.a();
                wi2.a aVar2 = new wi2.a();
                aVar2.c(a2);
                aVar2.a(createDataSetForSleepData);
                nt3<Void> s = oh2.b(getMPortfolioApp(), b).s(aVar2.b());
                s.f(new ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(t, vh2, b, br7, arrayList, size, lu7, this, list, str));
                s.d(new ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(vh2, b, br7, arrayList, size, lu7, this, list, str));
            }
        } else {
            FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitSleepDataToGoogleFit");
            if (lu7.isActive()) {
                dl7.a aVar3 = dl7.Companion;
                lu7.resumeWith(dl7.m1constructorimpl(null));
            }
        }
        Object t2 = lu7.t();
        if (t2 == yn7.d()) {
            go7.c(qn7);
        }
        return t2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0192  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveGFitWorkoutSessionToGoogleFit(java.util.List<com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession> r14, java.lang.String r15, com.fossil.qn7<java.lang.Object> r16) {
        /*
        // Method dump skipped, instructions count: 449
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ThirdPartyRepository.saveGFitWorkoutSessionToGoogleFit(java.util.List, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void setMActivitiesRepository(ActivitiesRepository activitiesRepository) {
        pq7.c(activitiesRepository, "<set-?>");
        this.mActivitiesRepository = activitiesRepository;
    }

    @DexIgnore
    public final void setMPortfolioApp(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "<set-?>");
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final Object uploadData(PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, qn7<? super xw7> qn7) {
        return gu7.d(this.mPushDataSupervisorJob, null, null, new ThirdPartyRepository$uploadData$Anon2(this, pushPendingThirdPartyDataCallback, null), 3, null);
    }
}
