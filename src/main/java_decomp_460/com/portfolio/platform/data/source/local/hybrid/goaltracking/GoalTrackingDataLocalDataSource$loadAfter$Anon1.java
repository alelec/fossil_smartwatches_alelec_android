package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.fl5;
import com.fossil.pq7;
import com.fossil.xw7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataLocalDataSource$loadAfter$Anon1 implements fl5.b {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource this$0;

    @DexIgnore
    public GoalTrackingDataLocalDataSource$loadAfter$Anon1(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
        this.this$0 = goalTrackingDataLocalDataSource;
    }

    @DexIgnore
    @Override // com.fossil.fl5.b
    public final void run(fl5.b.a aVar) {
        GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = this.this$0;
        fl5.d dVar = fl5.d.AFTER;
        pq7.b(aVar, "helperCallback");
        xw7 unused = goalTrackingDataLocalDataSource.loadData(dVar, aVar, this.this$0.mOffset);
    }
}
