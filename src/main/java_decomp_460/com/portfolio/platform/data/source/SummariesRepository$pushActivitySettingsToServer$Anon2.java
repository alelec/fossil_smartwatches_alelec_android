package com.portfolio.platform.data.source;

import com.fossil.ak5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gj4;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.fossil.zi4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SummariesRepository$pushActivitySettingsToServer$2", f = "SummariesRepository.kt", l = {137}, m = "invokeSuspend")
public final class SummariesRepository$pushActivitySettingsToServer$Anon2 extends ko7 implements vp7<iv7, qn7<? super q88<ActivitySettings>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySettings $activitySettings;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$pushActivitySettingsToServer$Anon2(SummariesRepository summariesRepository, ActivitySettings activitySettings, qn7 qn7) {
        super(2, qn7);
        this.this$0 = summariesRepository;
        this.$activitySettings = activitySettings;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SummariesRepository$pushActivitySettingsToServer$Anon2 summariesRepository$pushActivitySettingsToServer$Anon2 = new SummariesRepository$pushActivitySettingsToServer$Anon2(this.this$0, this.$activitySettings, qn7);
        summariesRepository$pushActivitySettingsToServer$Anon2.p$ = (iv7) obj;
        return summariesRepository$pushActivitySettingsToServer$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super q88<ActivitySettings>> qn7) {
        return ((SummariesRepository$pushActivitySettingsToServer$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "pushActivitySettingsToServer - settings=" + this.$activitySettings);
            zi4 zi4 = new zi4();
            zi4.b(new ak5());
            JsonElement z = zi4.d().z(this.$activitySettings);
            pq7.b(z, "GsonBuilder()\n          \u2026sonTree(activitySettings)");
            gj4 d2 = z.d();
            ApiServiceV2 apiServiceV2 = this.this$0.mApiServiceV2;
            pq7.b(d2, "jsonObject");
            this.L$0 = iv7;
            this.L$1 = d2;
            this.label = 1;
            Object updateActivitySetting = apiServiceV2.updateActivitySetting(d2, this);
            return updateActivitySetting == d ? d : updateActivitySetting;
        } else if (i == 1) {
            gj4 gj4 = (gj4) this.L$1;
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
