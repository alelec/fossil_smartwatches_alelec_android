package com.portfolio.platform.data.source;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.WatchParam;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SkuDao_Impl implements SkuDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<SKUModel> __insertionAdapterOfSKUModel;
    @DexIgnore
    public /* final */ jw0<WatchParam> __insertionAdapterOfWatchParam;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfCleanUpSku;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfCleanUpWatchParam;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<SKUModel> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, SKUModel sKUModel) {
            if (sKUModel.getCreatedAt() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, sKUModel.getCreatedAt());
            }
            if (sKUModel.getUpdatedAt() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, sKUModel.getUpdatedAt());
            }
            if (sKUModel.getSerialNumberPrefix() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, sKUModel.getSerialNumberPrefix());
            }
            if (sKUModel.getSku() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, sKUModel.getSku());
            }
            if (sKUModel.getDeviceName() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, sKUModel.getDeviceName());
            }
            if (sKUModel.getGroupName() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, sKUModel.getGroupName());
            }
            if (sKUModel.getGender() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, sKUModel.getGender());
            }
            if (sKUModel.getDeviceType() == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, sKUModel.getDeviceType());
            }
            if (sKUModel.getFastPairId() == null) {
                px0.bindNull(9);
            } else {
                px0.bindString(9, sKUModel.getFastPairId());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `SKU` (`createdAt`,`updatedAt`,`serialNumberPrefix`,`sku`,`deviceName`,`groupName`,`gender`,`deviceType`,`fastPairId`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends jw0<WatchParam> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, WatchParam watchParam) {
            if (watchParam.getPrefixSerial() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, watchParam.getPrefixSerial());
            }
            if (watchParam.getVersionMajor() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, watchParam.getVersionMajor());
            }
            if (watchParam.getVersionMinor() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, watchParam.getVersionMinor());
            }
            if (watchParam.getData() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, watchParam.getData());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchParam` (`prefixSerial`,`versionMajor`,`versionMinor`,`data`) VALUES (?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM SKU";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends xw0 {
        @DexIgnore
        public Anon4(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM watchParam";
        }
    }

    @DexIgnore
    public SkuDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfSKUModel = new Anon1(qw0);
        this.__insertionAdapterOfWatchParam = new Anon2(qw0);
        this.__preparedStmtOfCleanUpSku = new Anon3(qw0);
        this.__preparedStmtOfCleanUpWatchParam = new Anon4(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public void addOrUpdateSkuList(List<SKUModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSKUModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public void addOrUpdateWatchParam(WatchParam watchParam) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchParam.insert((jw0<WatchParam>) watchParam);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public void cleanUpSku() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfCleanUpSku.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUpSku.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public void cleanUpWatchParam() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfCleanUpWatchParam.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUpWatchParam.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public List<SKUModel> getAllSkus() {
        tw0 f = tw0.f("SELECT * FROM SKU", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "serialNumberPrefix");
            int c4 = dx0.c(b, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int c5 = dx0.c(b, "deviceName");
            int c6 = dx0.c(b, "groupName");
            int c7 = dx0.c(b, "gender");
            int c8 = dx0.c(b, "deviceType");
            int c9 = dx0.c(b, "fastPairId");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                SKUModel sKUModel = new SKUModel(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8), b.getString(c9));
                sKUModel.setCreatedAt(b.getString(c));
                sKUModel.setUpdatedAt(b.getString(c2));
                arrayList.add(sKUModel);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public SKUModel getSkuByDeviceIdPrefix(String str) {
        SKUModel sKUModel = null;
        tw0 f = tw0.f("SELECT * FROM SKU WHERE serialNumberPrefix=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "serialNumberPrefix");
            int c4 = dx0.c(b, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int c5 = dx0.c(b, "deviceName");
            int c6 = dx0.c(b, "groupName");
            int c7 = dx0.c(b, "gender");
            int c8 = dx0.c(b, "deviceType");
            int c9 = dx0.c(b, "fastPairId");
            if (b.moveToFirst()) {
                sKUModel = new SKUModel(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8), b.getString(c9));
                sKUModel.setCreatedAt(b.getString(c));
                sKUModel.setUpdatedAt(b.getString(c2));
            }
            return sKUModel;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public WatchParam getWatchParamById(String str) {
        WatchParam watchParam = null;
        tw0 f = tw0.f("SELECT * FROM watchParam WHERE prefixSerial =?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "prefixSerial");
            int c2 = dx0.c(b, "versionMajor");
            int c3 = dx0.c(b, "versionMinor");
            int c4 = dx0.c(b, "data");
            if (b.moveToFirst()) {
                watchParam = new WatchParam(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4));
            }
            return watchParam;
        } finally {
            b.close();
            f.m();
        }
    }
}
