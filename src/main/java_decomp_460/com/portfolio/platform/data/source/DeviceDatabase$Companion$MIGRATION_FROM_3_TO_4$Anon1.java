package com.portfolio.platform.data.source;

import com.fossil.ax0;
import com.fossil.lx0;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDatabase$Companion$MIGRATION_FROM_3_TO_4$Anon1 extends ax0 {
    @DexIgnore
    public DeviceDatabase$Companion$MIGRATION_FROM_3_TO_4$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.ax0
    public void migrate(lx0 lx0) {
        pq7.c(lx0, "database");
        lx0.beginTransaction();
        lx0.execSQL("ALTER TABLE device ADD COLUMN activationDate TEXT");
        lx0.setTransactionSuccessful();
        lx0.endTransaction();
    }
}
