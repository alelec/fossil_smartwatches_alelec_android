package com.portfolio.platform.data.source.local.diana;

import com.fossil.qn7;
import com.portfolio.platform.data.model.diana.Complication;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ComplicationDao {
    @DexIgnore
    Object clearAll();  // void declaration

    @DexIgnore
    Object getAllComplications(qn7<? super List<Complication>> qn7);

    @DexIgnore
    Object getComplicationById(String str, qn7<? super Complication> qn7);

    @DexIgnore
    List<Complication> getComplicationByIds(List<String> list);

    @DexIgnore
    List<Complication> queryComplicationByName(String str);

    @DexIgnore
    void upsertComplicationList(List<Complication> list);
}
