package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.os.Build;
import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridCustomizeDatabase_Impl extends HybridCustomizeDatabase {
    @DexIgnore
    public volatile HybridPresetDao _hybridPresetDao;
    @DexIgnore
    public volatile MicroAppDao _microAppDao;
    @DexIgnore
    public volatile MicroAppLastSettingDao _microAppLastSettingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `microApp` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `nameKey` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `categories` TEXT NOT NULL, `description` TEXT NOT NULL, `descriptionKey` TEXT NOT NULL, `icon` TEXT, PRIMARY KEY(`id`, `serialNumber`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `microAppSetting` (`id` TEXT NOT NULL, `appId` TEXT NOT NULL, `setting` TEXT, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`appId`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `microAppVariant` (`id` TEXT NOT NULL, `appId` TEXT NOT NULL, `name` TEXT NOT NULL, `description` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `majorNumber` INTEGER NOT NULL, `minorNumber` INTEGER NOT NULL, `serialNumber` TEXT NOT NULL, PRIMARY KEY(`appId`, `serialNumber`, `name`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `microAppLastSetting` (`appId` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`appId`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `declarationFile` (`appId` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `variantName` TEXT NOT NULL, `id` TEXT NOT NULL, `description` TEXT, `content` TEXT, PRIMARY KEY(`appId`, `serialNumber`, `variantName`, `id`), FOREIGN KEY(`appId`, `serialNumber`, `variantName`) REFERENCES `microAppVariant`(`appId`, `serialNumber`, `name`) ON UPDATE CASCADE ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `hybridPreset` (`pinType` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT, `id` TEXT NOT NULL, `name` TEXT, `serialNumber` TEXT NOT NULL, `buttons` TEXT NOT NULL, `isActive` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `hybridRecommendPreset` (`id` TEXT NOT NULL, `name` TEXT, `serialNumber` TEXT NOT NULL, `buttons` TEXT NOT NULL, `isDefault` INTEGER NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '1cd16a2fbeeede74e77b1faa1351cfe8')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `microApp`");
            lx0.execSQL("DROP TABLE IF EXISTS `microAppSetting`");
            lx0.execSQL("DROP TABLE IF EXISTS `microAppVariant`");
            lx0.execSQL("DROP TABLE IF EXISTS `microAppLastSetting`");
            lx0.execSQL("DROP TABLE IF EXISTS `declarationFile`");
            lx0.execSQL("DROP TABLE IF EXISTS `hybridPreset`");
            lx0.execSQL("DROP TABLE IF EXISTS `hybridRecommendPreset`");
            if (HybridCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = HybridCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) HybridCustomizeDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (HybridCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = HybridCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) HybridCustomizeDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            HybridCustomizeDatabase_Impl.this.mDatabase = lx0;
            lx0.execSQL("PRAGMA foreign_keys = ON");
            HybridCustomizeDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (HybridCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = HybridCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) HybridCustomizeDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(8);
            hashMap.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap.put("name", new ix0.a("name", "TEXT", true, 0, null, 1));
            hashMap.put("nameKey", new ix0.a("nameKey", "TEXT", true, 0, null, 1));
            hashMap.put("serialNumber", new ix0.a("serialNumber", "TEXT", true, 2, null, 1));
            hashMap.put("categories", new ix0.a("categories", "TEXT", true, 0, null, 1));
            hashMap.put("description", new ix0.a("description", "TEXT", true, 0, null, 1));
            hashMap.put("descriptionKey", new ix0.a("descriptionKey", "TEXT", true, 0, null, 1));
            hashMap.put("icon", new ix0.a("icon", "TEXT", false, 0, null, 1));
            ix0 ix0 = new ix0("microApp", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "microApp");
            if (!ix0.equals(a2)) {
                return new sw0.b(false, "microApp(com.portfolio.platform.data.model.room.microapp.MicroApp).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(6);
            hashMap2.put("id", new ix0.a("id", "TEXT", true, 0, null, 1));
            hashMap2.put("appId", new ix0.a("appId", "TEXT", true, 1, null, 1));
            hashMap2.put(MicroAppSetting.SETTING, new ix0.a(MicroAppSetting.SETTING, "TEXT", false, 0, null, 1));
            hashMap2.put("createdAt", new ix0.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap2.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap2.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            ix0 ix02 = new ix0(MicroAppSetting.TABLE_NAME, hashMap2, new HashSet(0), new HashSet(0));
            ix0 a3 = ix0.a(lx0, MicroAppSetting.TABLE_NAME);
            if (!ix02.equals(a3)) {
                return new sw0.b(false, "microAppSetting(com.portfolio.platform.data.model.room.microapp.MicroAppSetting).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
            }
            HashMap hashMap3 = new HashMap(9);
            hashMap3.put("id", new ix0.a("id", "TEXT", true, 0, null, 1));
            hashMap3.put("appId", new ix0.a("appId", "TEXT", true, 1, null, 1));
            hashMap3.put("name", new ix0.a("name", "TEXT", true, 3, null, 1));
            hashMap3.put("description", new ix0.a("description", "TEXT", true, 0, null, 1));
            hashMap3.put("createdAt", new ix0.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap3.put("updatedAt", new ix0.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap3.put(MicroAppVariant.COLUMN_MAJOR_NUMBER, new ix0.a(MicroAppVariant.COLUMN_MAJOR_NUMBER, "INTEGER", true, 0, null, 1));
            hashMap3.put(MicroAppVariant.COLUMN_MINOR_NUMBER, new ix0.a(MicroAppVariant.COLUMN_MINOR_NUMBER, "INTEGER", true, 0, null, 1));
            hashMap3.put("serialNumber", new ix0.a("serialNumber", "TEXT", true, 2, null, 1));
            ix0 ix03 = new ix0("microAppVariant", hashMap3, new HashSet(0), new HashSet(0));
            ix0 a4 = ix0.a(lx0, "microAppVariant");
            if (!ix03.equals(a4)) {
                return new sw0.b(false, "microAppVariant(com.portfolio.platform.data.model.room.microapp.MicroAppVariant).\n Expected:\n" + ix03 + "\n Found:\n" + a4);
            }
            HashMap hashMap4 = new HashMap(3);
            hashMap4.put("appId", new ix0.a("appId", "TEXT", true, 1, null, 1));
            hashMap4.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap4.put(MicroAppSetting.SETTING, new ix0.a(MicroAppSetting.SETTING, "TEXT", true, 0, null, 1));
            ix0 ix04 = new ix0("microAppLastSetting", hashMap4, new HashSet(0), new HashSet(0));
            ix0 a5 = ix0.a(lx0, "microAppLastSetting");
            if (!ix04.equals(a5)) {
                return new sw0.b(false, "microAppLastSetting(com.portfolio.platform.data.model.microapp.MicroAppLastSetting).\n Expected:\n" + ix04 + "\n Found:\n" + a5);
            }
            HashMap hashMap5 = new HashMap(6);
            hashMap5.put("appId", new ix0.a("appId", "TEXT", true, 1, null, 1));
            hashMap5.put("serialNumber", new ix0.a("serialNumber", "TEXT", true, 2, null, 1));
            hashMap5.put("variantName", new ix0.a("variantName", "TEXT", true, 3, null, 1));
            hashMap5.put("id", new ix0.a("id", "TEXT", true, 4, null, 1));
            hashMap5.put("description", new ix0.a("description", "TEXT", false, 0, null, 1));
            hashMap5.put("content", new ix0.a("content", "TEXT", false, 0, null, 1));
            HashSet hashSet = new HashSet(1);
            hashSet.add(new ix0.b("microAppVariant", "NO ACTION", "CASCADE", Arrays.asList("appId", "serialNumber", "variantName"), Arrays.asList("appId", "serialNumber", "name")));
            ix0 ix05 = new ix0("declarationFile", hashMap5, hashSet, new HashSet(0));
            ix0 a6 = ix0.a(lx0, "declarationFile");
            if (!ix05.equals(a6)) {
                return new sw0.b(false, "declarationFile(com.portfolio.platform.data.model.room.microapp.DeclarationFile).\n Expected:\n" + ix05 + "\n Found:\n" + a6);
            }
            HashMap hashMap6 = new HashMap(8);
            hashMap6.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap6.put("createdAt", new ix0.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap6.put("updatedAt", new ix0.a("updatedAt", "TEXT", false, 0, null, 1));
            hashMap6.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap6.put("name", new ix0.a("name", "TEXT", false, 0, null, 1));
            hashMap6.put("serialNumber", new ix0.a("serialNumber", "TEXT", true, 0, null, 1));
            hashMap6.put("buttons", new ix0.a("buttons", "TEXT", true, 0, null, 1));
            hashMap6.put("isActive", new ix0.a("isActive", "INTEGER", true, 0, null, 1));
            ix0 ix06 = new ix0("hybridPreset", hashMap6, new HashSet(0), new HashSet(0));
            ix0 a7 = ix0.a(lx0, "hybridPreset");
            if (!ix06.equals(a7)) {
                return new sw0.b(false, "hybridPreset(com.portfolio.platform.data.model.room.microapp.HybridPreset).\n Expected:\n" + ix06 + "\n Found:\n" + a7);
            }
            HashMap hashMap7 = new HashMap(7);
            hashMap7.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap7.put("name", new ix0.a("name", "TEXT", false, 0, null, 1));
            hashMap7.put("serialNumber", new ix0.a("serialNumber", "TEXT", true, 0, null, 1));
            hashMap7.put("buttons", new ix0.a("buttons", "TEXT", true, 0, null, 1));
            hashMap7.put("isDefault", new ix0.a("isDefault", "INTEGER", true, 0, null, 1));
            hashMap7.put("createdAt", new ix0.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap7.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            ix0 ix07 = new ix0("hybridRecommendPreset", hashMap7, new HashSet(0), new HashSet(0));
            ix0 a8 = ix0.a(lx0, "hybridRecommendPreset");
            if (ix07.equals(a8)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "hybridRecommendPreset(com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset).\n Expected:\n" + ix07 + "\n Found:\n" + a8);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        boolean z = Build.VERSION.SDK_INT >= 21;
        if (!z) {
            try {
                writableDatabase.execSQL("PRAGMA foreign_keys = FALSE");
            } catch (Throwable th) {
                super.endTransaction();
                if (!z) {
                    writableDatabase.execSQL("PRAGMA foreign_keys = TRUE");
                }
                writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
                if (!writableDatabase.inTransaction()) {
                    writableDatabase.execSQL("VACUUM");
                }
                throw th;
            }
        }
        super.beginTransaction();
        if (z) {
            writableDatabase.execSQL("PRAGMA defer_foreign_keys = TRUE");
        }
        writableDatabase.execSQL("DELETE FROM `microApp`");
        writableDatabase.execSQL("DELETE FROM `microAppSetting`");
        writableDatabase.execSQL("DELETE FROM `microAppVariant`");
        writableDatabase.execSQL("DELETE FROM `microAppLastSetting`");
        writableDatabase.execSQL("DELETE FROM `declarationFile`");
        writableDatabase.execSQL("DELETE FROM `hybridPreset`");
        writableDatabase.execSQL("DELETE FROM `hybridRecommendPreset`");
        super.setTransactionSuccessful();
        super.endTransaction();
        if (!z) {
            writableDatabase.execSQL("PRAGMA foreign_keys = TRUE");
        }
        writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
        if (!writableDatabase.inTransaction()) {
            writableDatabase.execSQL("VACUUM");
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "microApp", MicroAppSetting.TABLE_NAME, "microAppVariant", "microAppLastSetting", "declarationFile", "hybridPreset", "hybridRecommendPreset");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(10), "1cd16a2fbeeede74e77b1faa1351cfe8", "c7fc8c005bafc0b2f864a0c6cb79cc7b");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase
    public MicroAppDao microAppDao() {
        MicroAppDao microAppDao;
        if (this._microAppDao != null) {
            return this._microAppDao;
        }
        synchronized (this) {
            if (this._microAppDao == null) {
                this._microAppDao = new MicroAppDao_Impl(this);
            }
            microAppDao = this._microAppDao;
        }
        return microAppDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase
    public MicroAppLastSettingDao microAppLastSettingDao() {
        MicroAppLastSettingDao microAppLastSettingDao;
        if (this._microAppLastSettingDao != null) {
            return this._microAppLastSettingDao;
        }
        synchronized (this) {
            if (this._microAppLastSettingDao == null) {
                this._microAppLastSettingDao = new MicroAppLastSettingDao_Impl(this);
            }
            microAppLastSettingDao = this._microAppLastSettingDao;
        }
        return microAppLastSettingDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase
    public HybridPresetDao presetDao() {
        HybridPresetDao hybridPresetDao;
        if (this._hybridPresetDao != null) {
            return this._hybridPresetDao;
        }
        synchronized (this) {
            if (this._hybridPresetDao == null) {
                this._hybridPresetDao = new HybridPresetDao_Impl(this);
            }
            hybridPresetDao = this._hybridPresetDao;
        }
        return hybridPresetDao;
    }
}
