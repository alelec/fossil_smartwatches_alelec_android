package com.portfolio.platform.data.source;

import com.fossil.sk5;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitiesRepository_Factory implements Factory<ActivitiesRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;
    @DexIgnore
    public /* final */ Provider<sk5> mFitnessHelperProvider;
    @DexIgnore
    public /* final */ Provider<UserRepository> mUserRepositoryProvider;

    @DexIgnore
    public ActivitiesRepository_Factory(Provider<ApiServiceV2> provider, Provider<UserRepository> provider2, Provider<sk5> provider3) {
        this.mApiServiceProvider = provider;
        this.mUserRepositoryProvider = provider2;
        this.mFitnessHelperProvider = provider3;
    }

    @DexIgnore
    public static ActivitiesRepository_Factory create(Provider<ApiServiceV2> provider, Provider<UserRepository> provider2, Provider<sk5> provider3) {
        return new ActivitiesRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static ActivitiesRepository newInstance(ApiServiceV2 apiServiceV2, UserRepository userRepository, sk5 sk5) {
        return new ActivitiesRepository(apiServiceV2, userRepository, sk5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ActivitiesRepository get() {
        return newInstance(this.mApiServiceProvider.get(), this.mUserRepositoryProvider.get(), this.mFitnessHelperProvider.get());
    }
}
