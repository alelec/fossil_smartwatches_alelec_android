package com.portfolio.platform.data.source.remote;

import com.fossil.iq5;
import com.fossil.jq5;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.Device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ ApiService2Dot1 mApiServiceV21;
    @DexIgnore
    public /* final */ SecureApiService2Dot1 mSecureApiService2Dot1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    /*
    static {
        String simpleName = DeviceRemoteDataSource.class.getSimpleName();
        pq7.b(simpleName, "DeviceRemoteDataSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public DeviceRemoteDataSource(ApiServiceV2 apiServiceV2, ApiService2Dot1 apiService2Dot1, SecureApiService2Dot1 secureApiService2Dot1) {
        pq7.c(apiServiceV2, "mApiService");
        pq7.c(apiService2Dot1, "mApiServiceV21");
        pq7.c(secureApiService2Dot1, "mSecureApiService2Dot1");
        this.mApiService = apiServiceV2;
        this.mApiServiceV21 = apiService2Dot1;
        this.mSecureApiService2Dot1 = secureApiService2Dot1;
    }

    @DexIgnore
    public final Object forceLinkDevice(Device device, qn7<? super iq5<Void>> qn7) {
        return jq5.d(new DeviceRemoteDataSource$forceLinkDevice$Anon2(this, device, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object generatePairingKey(java.lang.String r9, com.fossil.qn7<? super com.fossil.iq5<java.lang.String>> r10) {
        /*
        // Method dump skipped, instructions count: 250
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.generatePairingKey(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object getAllDevice(qn7<? super iq5<ApiResponse<Device>>> qn7) {
        FLogger.INSTANCE.getLocal().d(TAG, "getAllDevice");
        return jq5.d(new DeviceRemoteDataSource$getAllDevice$Anon2(this, null), qn7);
    }

    @DexIgnore
    public final Object getDeviceBySerial(String str, qn7<? super iq5<Device>> qn7) {
        FLogger.INSTANCE.getLocal().d(TAG, "getDeviceBySerial");
        return jq5.d(new DeviceRemoteDataSource$getDeviceBySerial$Anon2(this, str, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getDeviceSecretKey(java.lang.String r12, com.fossil.qn7<? super com.fossil.iq5<java.lang.String>> r13) {
        /*
        // Method dump skipped, instructions count: 241
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.getDeviceSecretKey(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getLatestWatchParamFromServer(java.lang.String r8, int r9, com.fossil.qn7<? super com.portfolio.platform.data.model.WatchParameterResponse> r10) {
        /*
        // Method dump skipped, instructions count: 247
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.getLatestWatchParamFromServer(java.lang.String, int, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSupportedSku(int r9, com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.SKUModel>>> r10) {
        /*
            r8 = this;
            r6 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getSupportedSku$Anon1
            if (r0 == 0) goto L_0x0040
            r0 = r10
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getSupportedSku$Anon1 r0 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getSupportedSku$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x0040
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x004e
            if (r4 != r6) goto L_0x0046
            int r2 = r0.I$0
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x002a:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0075
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r1 = r0.a()
            if (r1 == 0) goto L_0x0071
            com.fossil.kq5 r0 = new com.fossil.kq5
            r2 = 0
            r4 = 2
            r0.<init>(r1, r2, r4, r3)
        L_0x003f:
            return r0
        L_0x0040:
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getSupportedSku$Anon1 r0 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getSupportedSku$Anon1
            r0.<init>(r8, r10)
            goto L_0x0014
        L_0x0046:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004e:
            com.fossil.el7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.TAG
            java.lang.String r5 = "fetchSupportedSkus"
            r1.d(r4, r5)
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getSupportedSku$response$Anon1 r1 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getSupportedSku$response$Anon1
            r1.<init>(r8, r9, r3)
            r0.L$0 = r8
            r0.I$0 = r9
            r0.label = r6
            java.lang.Object r0 = com.fossil.jq5.d(r1, r0)
            if (r0 != r2) goto L_0x002a
            r0 = r2
            goto L_0x003f
        L_0x0071:
            com.fossil.pq7.i()
            throw r3
        L_0x0075:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x008f
            r2 = r0
            com.fossil.hq5 r2 = (com.fossil.hq5) r2
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r2.a()
            com.portfolio.platform.data.model.ServerError r2 = r2.c()
            r6 = 16
            r4 = r3
            r5 = r3
            r7 = r3
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x003f
        L_0x008f:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.getSupportedSku(int, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object removeDevice(Device device, qn7<? super iq5<Void>> qn7) {
        FLogger.INSTANCE.getLocal().d(TAG, "removeDevice");
        return jq5.d(new DeviceRemoteDataSource$removeDevice$Anon2(this, device, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object swapPairingKey(java.lang.String r9, java.lang.String r10, com.fossil.qn7<? super com.fossil.iq5<java.lang.String>> r11) {
        /*
        // Method dump skipped, instructions count: 292
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.swapPairingKey(java.lang.String, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object updateDevice(Device device, qn7<? super iq5<Void>> qn7) {
        FLogger.INSTANCE.getLocal().d(TAG, "updateDevice");
        return jq5.d(new DeviceRemoteDataSource$updateDevice$Anon2(this, device, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object updateDeviceSecretKey(java.lang.String r7, java.lang.String r8, com.fossil.qn7<? super com.fossil.iq5<com.fossil.gj4>> r9) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$updateDeviceSecretKey$Anon1
            if (r0 == 0) goto L_0x0065
            r0 = r9
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$updateDeviceSecretKey$Anon1 r0 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$updateDeviceSecretKey$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0065
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r1 = r2.label
            if (r1 == 0) goto L_0x0074
            if (r1 != r5) goto L_0x006c
            java.lang.Object r0 = r2.L$3
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            java.lang.Object r0 = r2.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r2.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r2 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource) r2
            com.fossil.el7.b(r3)
            r2 = r3
            r8 = r0
        L_0x0035:
            r0 = r2
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "updateDeviceSecretKey "
            r4.append(r5)
            r4.append(r8)
            java.lang.String r5 = " of "
            r4.append(r5)
            r4.append(r1)
            java.lang.String r1 = " response "
            r4.append(r1)
            r4.append(r0)
            java.lang.String r1 = r4.toString()
            r2.d(r3, r1)
        L_0x0064:
            return r0
        L_0x0065:
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$updateDeviceSecretKey$Anon1 r0 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$updateDeviceSecretKey$Anon1
            r0.<init>(r6, r9)
            r2 = r0
            goto L_0x0014
        L_0x006c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0074:
            com.fossil.el7.b(r3)
            com.fossil.gj4 r1 = new com.fossil.gj4
            r1.<init>()
            java.lang.String r3 = "secretKey"
            r1.n(r3, r8)
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$updateDeviceSecretKey$response$Anon1 r3 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$updateDeviceSecretKey$response$Anon1
            r4 = 0
            r3.<init>(r6, r7, r1, r4)
            r2.L$0 = r6
            r2.L$1 = r7
            r2.L$2 = r8
            r2.L$3 = r1
            r2.label = r5
            java.lang.Object r2 = com.fossil.jq5.d(r3, r2)
            if (r2 == r0) goto L_0x0064
            r1 = r7
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.updateDeviceSecretKey(java.lang.String, java.lang.String, com.fossil.qn7):java.lang.Object");
    }
}
