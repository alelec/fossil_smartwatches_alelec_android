package com.portfolio.platform.data.source;

import com.fossil.ao7;
import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getCurrentLastSleepGoal$2", f = "SleepSummariesRepository.kt", l = {62}, m = "invokeSuspend")
public final class SleepSummariesRepository$getCurrentLastSleepGoal$Anon2 extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    public SleepSummariesRepository$getCurrentLastSleepGoal$Anon2(qn7 qn7) {
        super(2, qn7);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SleepSummariesRepository$getCurrentLastSleepGoal$Anon2 sleepSummariesRepository$getCurrentLastSleepGoal$Anon2 = new SleepSummariesRepository$getCurrentLastSleepGoal$Anon2(qn7);
        sleepSummariesRepository$getCurrentLastSleepGoal$Anon2.p$ = (iv7) obj;
        return sleepSummariesRepository$getCurrentLastSleepGoal$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
        return ((SleepSummariesRepository$getCurrentLastSleepGoal$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object D;
        Integer e;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.label = 1;
            D = bn5.D(this);
            if (D == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            D = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFSleepSettings sleepSettings = ((SleepDatabase) D).sleepDao().getSleepSettings();
        return ao7.e((sleepSettings == null || (e = ao7.e(sleepSettings.getSleepGoal())) == null) ? 480 : e.intValue());
    }
}
