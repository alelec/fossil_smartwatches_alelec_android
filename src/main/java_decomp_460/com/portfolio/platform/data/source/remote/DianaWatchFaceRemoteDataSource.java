package com.portfolio.platform.data.source.remote;

import com.fossil.bj4;
import com.fossil.gj4;
import com.fossil.iq5;
import com.fossil.jq5;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "DianaWatchFaceRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;
    @DexIgnore
    public /* final */ SecureApiService mSecureApiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public DianaWatchFaceRemoteDataSource(ApiServiceV2 apiServiceV2, SecureApiService secureApiService) {
        pq7.c(apiServiceV2, "mApiServiceV2");
        pq7.c(secureApiService, "mSecureApiService");
        this.mApiServiceV2 = apiServiceV2;
        this.mSecureApiService = secureApiService;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object createWatchFace(com.fossil.gj4 r9, com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.model.watchface.DianaWatchFaceUser>> r10) {
        /*
            r8 = this;
            r6 = 28
            r7 = 1
            r5 = 0
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource$createWatchFace$Anon1
            if (r0 == 0) goto L_0x005a
            r0 = r10
            com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource$createWatchFace$Anon1 r0 = (com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource$createWatchFace$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x005a
            int r1 = r1 + r4
            r0.label = r1
            r1 = r0
        L_0x0018:
            java.lang.Object r2 = r1.result
            java.lang.Object r4 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0069
            if (r0 != r7) goto L_0x0061
            java.lang.Object r0 = r1.L$1
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x0030:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0090
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r0 = (com.portfolio.platform.data.source.remote.ApiResponse) r0
            if (r0 == 0) goto L_0x007f
            java.util.List r0 = r0.get_items()
            r1 = r0
        L_0x0045:
            if (r1 == 0) goto L_0x0081
            boolean r0 = r1.isEmpty()
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0081
            com.fossil.kq5 r0 = new com.fossil.kq5
            java.lang.Object r1 = r1.get(r5)
            r2 = 2
            r0.<init>(r1, r5, r2, r3)
        L_0x0059:
            return r0
        L_0x005a:
            com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource$createWatchFace$Anon1 r0 = new com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource$createWatchFace$Anon1
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0018
        L_0x0061:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0069:
            com.fossil.el7.b(r2)
            com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource$createWatchFace$response$Anon1 r0 = new com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource$createWatchFace$response$Anon1
            r0.<init>(r8, r9, r3)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.label = r7
            java.lang.Object r0 = com.fossil.jq5.d(r0, r1)
            if (r0 != r4) goto L_0x0030
            r0 = r4
            goto L_0x0059
        L_0x007f:
            r1 = r3
            goto L_0x0045
        L_0x0081:
            com.fossil.hq5 r0 = new com.fossil.hq5
            r1 = -1
            com.portfolio.platform.data.model.ServerError r2 = new com.portfolio.platform.data.model.ServerError
            r2.<init>()
            r4 = r3
            r5 = r3
            r7 = r3
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0059
        L_0x0090:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00a8
            r2 = r0
            com.fossil.hq5 r2 = (com.fossil.hq5) r2
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r2.a()
            com.portfolio.platform.data.model.ServerError r2 = r2.c()
            r4 = r3
            r5 = r3
            r7 = r3
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0059
        L_0x00a8:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource.createWatchFace(com.fossil.gj4, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object deleteDianaWatchFaceUser(String str, qn7<? super iq5<Object>> qn7) {
        gj4 gj4 = new gj4();
        bj4 bj4 = new bj4();
        bj4.l(str);
        gj4.k("_ids", bj4);
        return jq5.d(new DianaWatchFaceRemoteDataSource$deleteDianaWatchFaceUser$Anon2(this, gj4, null), qn7);
    }

    @DexIgnore
    public final Object getAllDianaWatchFaceUser(int i, int i2, qn7<? super iq5<ApiResponse<DianaWatchFaceUser>>> qn7) {
        return jq5.d(new DianaWatchFaceRemoteDataSource$getAllDianaWatchFaceUser$Anon2(this, i2, i, null), qn7);
    }

    @DexIgnore
    public final Object getDianaWatchFaceUserById(String str, qn7<? super iq5<DianaWatchFaceUser>> qn7) {
        return jq5.d(new DianaWatchFaceRemoteDataSource$getDianaWatchFaceUserById$Anon2(this, str, null), qn7);
    }

    @DexIgnore
    public final Object getDianaWatchFaceUserWithOrderId(String str, qn7<? super iq5<ApiResponse<DianaWatchFaceUser>>> qn7) {
        return jq5.d(new DianaWatchFaceRemoteDataSource$getDianaWatchFaceUserWithOrderId$Anon2(this, str, null), qn7);
    }
}
