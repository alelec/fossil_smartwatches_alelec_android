package com.portfolio.platform.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.co7;
import com.fossil.eo7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.ActivityStatistic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$1", f = "SummariesRepository.kt", l = {MFNetworkReturnCode.CONTENT_TYPE_ERROR}, m = "saveCallResult")
public final class SummariesRepository$getActivityStatistic$Anon1$saveCallResult$Anon1_Level2 extends co7 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getActivityStatistic$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getActivityStatistic$Anon1$saveCallResult$Anon1_Level2(SummariesRepository$getActivityStatistic$Anon1 summariesRepository$getActivityStatistic$Anon1, qn7 qn7) {
        super(qn7);
        this.this$0 = summariesRepository$getActivityStatistic$Anon1;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.saveCallResult((ActivityStatistic) null, (qn7<? super tl7>) this);
    }
}
