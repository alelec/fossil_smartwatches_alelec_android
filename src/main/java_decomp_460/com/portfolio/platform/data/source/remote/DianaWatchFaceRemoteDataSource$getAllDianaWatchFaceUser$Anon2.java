package com.portfolio.platform.data.source.remote;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource$getAllDianaWatchFaceUser$2", f = "DianaWatchFaceRemoteDataSource.kt", l = {20}, m = "invokeSuspend")
public final class DianaWatchFaceRemoteDataSource$getAllDianaWatchFaceUser$Anon2 extends ko7 implements rp7<qn7<? super q88<ApiResponse<DianaWatchFaceUser>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $limits;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ DianaWatchFaceRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaWatchFaceRemoteDataSource$getAllDianaWatchFaceUser$Anon2(DianaWatchFaceRemoteDataSource dianaWatchFaceRemoteDataSource, int i, int i2, qn7 qn7) {
        super(1, qn7);
        this.this$0 = dianaWatchFaceRemoteDataSource;
        this.$limits = i;
        this.$offset = i2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(qn7<?> qn7) {
        pq7.c(qn7, "completion");
        return new DianaWatchFaceRemoteDataSource$getAllDianaWatchFaceUser$Anon2(this.this$0, this.$limits, this.$offset, qn7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public final Object invoke(qn7<? super q88<ApiResponse<DianaWatchFaceUser>>> qn7) {
        return ((DianaWatchFaceRemoteDataSource$getAllDianaWatchFaceUser$Anon2) create(qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiServiceV2;
            int i2 = this.$limits;
            int i3 = this.$offset;
            this.label = 1;
            Object allUserDianaWatchFace = apiServiceV2.getAllUserDianaWatchFace(i2, i3, this);
            return allUserDianaWatchFace == d ? d : allUserDianaWatchFace;
        } else if (i == 1) {
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
