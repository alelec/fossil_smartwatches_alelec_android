package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.c47;
import com.fossil.eo7;
import com.fossil.gi0;
import com.fossil.gj4;
import com.fossil.h47;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$2", f = "SleepSummariesRepository.kt", l = {ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 141}, m = "invokeSuspend")
public final class SleepSummariesRepository$getSleepSummary$Anon2 extends ko7 implements vp7<iv7, qn7<? super LiveData<h47<? extends MFSleepDay>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements gi0<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepDatabase $sleepDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository$getSleepSummary$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends c47<MFSleepDay, gj4> {
            @DexIgnore
            public /* final */ /* synthetic */ List $fitnessDataList;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, List list) {
                this.this$0 = anon1_Level2;
                this.$fitnessDataList = list;
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object createCall(qn7<? super q88<gj4>> qn7) {
                Date V = lk5.V(this.this$0.this$0.$date);
                Date E = lk5.E(this.this$0.this$0.$date);
                Calendar instance = Calendar.getInstance();
                pq7.b(instance, "calendar");
                instance.setTimeInMillis(0);
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiService;
                String k = lk5.k(V);
                pq7.b(k, "DateHelper.formatShortDate(startDate)");
                String k2 = lk5.k(E);
                pq7.b(k2, "DateHelper.formatShortDate(endDate)");
                return apiServiceV2.getSleepSummaries(k, k2, 0, 100, qn7);
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object loadFromDb(qn7<? super LiveData<MFSleepDay>> qn7) {
                Calendar instance = Calendar.getInstance();
                pq7.b(instance, "calendar");
                instance.setTime(this.this$0.this$0.$date);
                SleepDao sleepDao = this.this$0.$sleepDatabase.sleepDao();
                String k = lk5.k(this.this$0.this$0.$date);
                pq7.b(k, "DateHelper.formatShortDate(date)");
                return sleepDao.getSleepDayLiveData(k);
            }

            @DexIgnore
            @Override // com.fossil.c47
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.TAG, "getActivityList onFetchFailed");
            }

            @DexIgnore
            public Object saveCallResult(gj4 gj4, qn7<? super tl7> qn7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = SleepSummariesRepository.TAG;
                local.d(str, "getSleepSummary saveCallResult onResponse: response = " + gj4);
                SleepSummariesRepository$getSleepSummary$Anon2 sleepSummariesRepository$getSleepSummary$Anon2 = this.this$0.this$0;
                Object saveSleepSummary = sleepSummariesRepository$getSleepSummary$Anon2.this$0.saveSleepSummary(gj4, sleepSummariesRepository$getSleepSummary$Anon2.$date, qn7);
                return saveSleepSummary == yn7.d() ? saveSleepSummary : tl7.f3441a;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
            @Override // com.fossil.c47
            public /* bridge */ /* synthetic */ Object saveCallResult(gj4 gj4, qn7 qn7) {
                return saveCallResult(gj4, (qn7<? super tl7>) qn7);
            }

            @DexIgnore
            public boolean shouldFetch(MFSleepDay mFSleepDay) {
                return this.$fitnessDataList.isEmpty();
            }
        }

        @DexIgnore
        public Anon1_Level2(SleepSummariesRepository$getSleepSummary$Anon2 sleepSummariesRepository$getSleepSummary$Anon2, SleepDatabase sleepDatabase) {
            this.this$0 = sleepSummariesRepository$getSleepSummary$Anon2;
            this.$sleepDatabase = sleepDatabase;
        }

        @DexIgnore
        public final LiveData<h47<MFSleepDay>> apply(List<FitnessDataWrapper> list) {
            return new Anon1_Level3(this, list).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepSummary$Anon2(SleepSummariesRepository sleepSummariesRepository, Date date, qn7 qn7) {
        super(2, qn7);
        this.this$0 = sleepSummariesRepository;
        this.$date = date;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SleepSummariesRepository$getSleepSummary$Anon2 sleepSummariesRepository$getSleepSummary$Anon2 = new SleepSummariesRepository$getSleepSummary$Anon2(this.this$0, this.$date, qn7);
        sleepSummariesRepository$getSleepSummary$Anon2.p$ = (iv7) obj;
        return sleepSummariesRepository$getSleepSummary$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super LiveData<h47<? extends MFSleepDay>>> qn7) {
        return ((SleepSummariesRepository$getSleepSummary$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0098  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
        /*
            r9 = this;
            r8 = 0
            r7 = 2
            r6 = 1
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r9.label
            if (r0 == 0) goto L_0x005e
            if (r0 == r6) goto L_0x003b
            if (r0 != r7) goto L_0x0033
            java.lang.Object r0 = r9.L$1
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            java.lang.Object r1 = r9.L$0
            com.fossil.iv7 r1 = (com.fossil.iv7) r1
            com.fossil.el7.b(r10)
            r1 = r10
            r2 = r0
        L_0x001c:
            r0 = r1
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r0
            com.portfolio.platform.data.source.local.FitnessDataDao r1 = r2.getFitnessDataDao()
            java.util.Date r2 = r9.$date
            androidx.lifecycle.LiveData r1 = r1.getFitnessDataLiveData(r2, r2)
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon2$Anon1_Level2 r2 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon2$Anon1_Level2
            r2.<init>(r9, r0)
            androidx.lifecycle.LiveData r0 = com.fossil.ss0.c(r1, r2)
        L_0x0032:
            return r0
        L_0x0033:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003b:
            java.lang.Object r0 = r9.L$0
            com.fossil.iv7 r0 = (com.fossil.iv7) r0
            com.fossil.el7.b(r10)
            r2 = r0
            r1 = r10
        L_0x0044:
            r0 = r1
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            com.fossil.dv7 r1 = com.fossil.bw7.b()
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon2$sleepDatabase$Anon1_Level2 r4 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon2$sleepDatabase$Anon1_Level2
            r4.<init>(r8)
            r9.L$0 = r2
            r9.L$1 = r0
            r9.label = r7
            java.lang.Object r1 = com.fossil.eu7.g(r1, r4, r9)
            if (r1 != r3) goto L_0x0098
            r0 = r3
            goto L_0x0032
        L_0x005e:
            com.fossil.el7.b(r10)
            com.fossil.iv7 r0 = r9.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.SleepSummariesRepository.access$getTAG$cp()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "getSleepSummary date="
            r4.append(r5)
            java.util.Date r5 = r9.$date
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.d(r2, r4)
            com.fossil.dv7 r1 = com.fossil.bw7.b()
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon2$fitnessDatabase$Anon1_Level2 r2 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon2$fitnessDatabase$Anon1_Level2
            r2.<init>(r8)
            r9.L$0 = r0
            r9.label = r6
            java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r9)
            if (r1 != r3) goto L_0x009a
            r0 = r3
            goto L_0x0032
        L_0x0098:
            r2 = r0
            goto L_0x001c
        L_0x009a:
            r2 = r0
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
