package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideDeviceDatabaseFactory implements Factory<DeviceDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideDeviceDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideDeviceDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideDeviceDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static DeviceDatabase provideDeviceDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        DeviceDatabase provideDeviceDatabase = portfolioDatabaseModule.provideDeviceDatabase(portfolioApp);
        lk7.c(provideDeviceDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideDeviceDatabase;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public DeviceDatabase get() {
        return provideDeviceDatabase(this.module, this.appProvider.get());
    }
}
