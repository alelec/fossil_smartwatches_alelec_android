package com.portfolio.platform.data.source;

import android.content.Context;
import com.portfolio.platform.data.source.remote.LabelRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LabelRepository_Factory implements Factory<LabelRepository> {
    @DexIgnore
    public /* final */ Provider<Context> contextProvider;
    @DexIgnore
    public /* final */ Provider<LabelRemoteDataSource> mRemoteSourceProvider;

    @DexIgnore
    public LabelRepository_Factory(Provider<Context> provider, Provider<LabelRemoteDataSource> provider2) {
        this.contextProvider = provider;
        this.mRemoteSourceProvider = provider2;
    }

    @DexIgnore
    public static LabelRepository_Factory create(Provider<Context> provider, Provider<LabelRemoteDataSource> provider2) {
        return new LabelRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static LabelRepository newInstance(Context context, LabelRemoteDataSource labelRemoteDataSource) {
        return new LabelRepository(context, labelRemoteDataSource);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public LabelRepository get() {
        return newInstance(this.contextProvider.get(), this.mRemoteSourceProvider.get());
    }
}
