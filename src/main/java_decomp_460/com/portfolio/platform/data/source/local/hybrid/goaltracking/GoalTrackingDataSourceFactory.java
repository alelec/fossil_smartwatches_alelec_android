package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.MutableLiveData;
import com.fossil.fl5;
import com.fossil.no4;
import com.fossil.pq7;
import com.fossil.xt0;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataSourceFactory extends xt0.b<Long, GoalTrackingData> {
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public GoalTrackingDataLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ no4 mAppExecutors;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public /* final */ fl5.a mListener;
    @DexIgnore
    public /* final */ MutableLiveData<GoalTrackingDataLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public GoalTrackingDataSourceFactory(GoalTrackingRepository goalTrackingRepository, GoalTrackingDatabase goalTrackingDatabase, Date date, no4 no4, fl5.a aVar) {
        pq7.c(goalTrackingRepository, "mGoalTrackingRepository");
        pq7.c(goalTrackingDatabase, "mGoalTrackingDatabase");
        pq7.c(date, "currentDate");
        pq7.c(no4, "mAppExecutors");
        pq7.c(aVar, "mListener");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.currentDate = date;
        this.mAppExecutors = no4;
        this.mListener = aVar;
    }

    @DexIgnore
    @Override // com.fossil.xt0.b
    public xt0<Long, GoalTrackingData> create() {
        GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = new GoalTrackingDataLocalDataSource(this.mGoalTrackingRepository, this.mGoalTrackingDatabase, this.currentDate, this.mAppExecutors, this.mListener);
        this.localDataSource = goalTrackingDataLocalDataSource;
        this.sourceLiveData.l(goalTrackingDataLocalDataSource);
        GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource2 = this.localDataSource;
        if (goalTrackingDataLocalDataSource2 != null) {
            return goalTrackingDataLocalDataSource2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final GoalTrackingDataLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<GoalTrackingDataLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
        this.localDataSource = goalTrackingDataLocalDataSource;
    }
}
