package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory implements Factory<NotificationSettingsDao> {
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<NotificationSettingsDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<NotificationSettingsDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static NotificationSettingsDao provideNotificationSettingsDao(PortfolioDatabaseModule portfolioDatabaseModule, NotificationSettingsDatabase notificationSettingsDatabase) {
        NotificationSettingsDao provideNotificationSettingsDao = portfolioDatabaseModule.provideNotificationSettingsDao(notificationSettingsDatabase);
        lk7.c(provideNotificationSettingsDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideNotificationSettingsDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public NotificationSettingsDao get() {
        return provideNotificationSettingsDao(this.module, this.dbProvider.get());
    }
}
