package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bw7;
import com.fossil.cu0;
import com.fossil.dv7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.eu7;
import com.fossil.fl5;
import com.fossil.gi0;
import com.fossil.gp7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.no4;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.qq7;
import com.fossil.ss0;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.fossil.zt0;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataPaging$2", f = "GoalTrackingRepository.kt", l = {319}, m = "invokeSuspend")
public final class GoalTrackingRepository$getGoalTrackingDataPaging$Anon2 extends ko7 implements vp7<iv7, qn7<? super Listing<GoalTrackingData>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ no4 $appExecutors;
    @DexIgnore
    public /* final */ /* synthetic */ Date $currentDate;
    @DexIgnore
    public /* final */ /* synthetic */ fl5.a $listener;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements gi0<X, LiveData<Y>> {
        @DexIgnore
        public static /* final */ Anon1_Level2 INSTANCE; // = new Anon1_Level2();

        @DexIgnore
        public final LiveData<NetworkState> apply(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
            return goalTrackingDataLocalDataSource.getMNetworkState();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2_Level2 extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(GoalTrackingDataSourceFactory goalTrackingDataSourceFactory) {
            super(0);
            this.$sourceFactory = goalTrackingDataSourceFactory;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            GoalTrackingDataLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
            if (e != null) {
                e.invalidate();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3_Level2 extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(GoalTrackingDataSourceFactory goalTrackingDataSourceFactory) {
            super(0);
            this.$sourceFactory = goalTrackingDataSourceFactory;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            fl5 mHelper;
            GoalTrackingDataLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
            if (e != null && (mHelper = e.getMHelper()) != null) {
                mHelper.g();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getGoalTrackingDataPaging$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, no4 no4, fl5.a aVar, qn7 qn7) {
        super(2, qn7);
        this.this$0 = goalTrackingRepository;
        this.$currentDate = date;
        this.$appExecutors = no4;
        this.$listener = aVar;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        GoalTrackingRepository$getGoalTrackingDataPaging$Anon2 goalTrackingRepository$getGoalTrackingDataPaging$Anon2 = new GoalTrackingRepository$getGoalTrackingDataPaging$Anon2(this.this$0, this.$currentDate, this.$appExecutors, this.$listener, qn7);
        goalTrackingRepository$getGoalTrackingDataPaging$Anon2.p$ = (iv7) obj;
        return goalTrackingRepository$getGoalTrackingDataPaging$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super Listing<GoalTrackingData>> qn7) {
        return ((GoalTrackingRepository$getGoalTrackingDataPaging$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object g;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "getGoalTrackingDataPaging");
            dv7 b = bw7.b();
            GoalTrackingRepository$getGoalTrackingDataPaging$Anon2$goalTrackingDatabase$Anon1_Level2 goalTrackingRepository$getGoalTrackingDataPaging$Anon2$goalTrackingDatabase$Anon1_Level2 = new GoalTrackingRepository$getGoalTrackingDataPaging$Anon2$goalTrackingDatabase$Anon1_Level2(null);
            this.L$0 = iv7;
            this.label = 1;
            g = eu7.g(b, goalTrackingRepository$getGoalTrackingDataPaging$Anon2$goalTrackingDatabase$Anon1_Level2, this);
            if (g == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            g = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        GoalTrackingDataSourceFactory goalTrackingDataSourceFactory = new GoalTrackingDataSourceFactory(this.this$0, (GoalTrackingDatabase) g, this.$currentDate, this.$appExecutors, this.$listener);
        this.this$0.mSourceDataFactoryList.add(goalTrackingDataSourceFactory);
        cu0.f.a aVar = new cu0.f.a();
        aVar.c(100);
        aVar.b(false);
        aVar.d(100);
        aVar.e(5);
        cu0.f a2 = aVar.a();
        pq7.b(a2, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a3 = new zt0(goalTrackingDataSourceFactory, a2).a();
        pq7.b(a3, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData c = ss0.c(goalTrackingDataSourceFactory.getSourceLiveData(), Anon1_Level2.INSTANCE);
        pq7.b(c, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing(a3, c, new Anon2_Level2(goalTrackingDataSourceFactory), new Anon3_Level2(goalTrackingDataSourceFactory));
    }
}
