package com.portfolio.platform.data.source.remote;

import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ErrorItems {
    @DexIgnore
    public int _error;
    @DexIgnore
    public String _errorMessage;

    @DexIgnore
    public ErrorItems(int i, String str) {
        pq7.c(str, "_errorMessage");
        this._error = i;
        this._errorMessage = str;
    }

    @DexIgnore
    public static /* synthetic */ ErrorItems copy$default(ErrorItems errorItems, int i, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = errorItems._error;
        }
        if ((i2 & 2) != 0) {
            str = errorItems._errorMessage;
        }
        return errorItems.copy(i, str);
    }

    @DexIgnore
    public final int component1() {
        return this._error;
    }

    @DexIgnore
    public final String component2() {
        return this._errorMessage;
    }

    @DexIgnore
    public final ErrorItems copy(int i, String str) {
        pq7.c(str, "_errorMessage");
        return new ErrorItems(i, str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ErrorItems) {
                ErrorItems errorItems = (ErrorItems) obj;
                if (this._error != errorItems._error || !pq7.a(this._errorMessage, errorItems._errorMessage)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int get_error() {
        return this._error;
    }

    @DexIgnore
    public final String get_errorMessage() {
        return this._errorMessage;
    }

    @DexIgnore
    public int hashCode() {
        int i = this._error;
        String str = this._errorMessage;
        return (str != null ? str.hashCode() : 0) + (i * 31);
    }

    @DexIgnore
    public final void set_error(int i) {
        this._error = i;
    }

    @DexIgnore
    public final void set_errorMessage(String str) {
        pq7.c(str, "<set-?>");
        this._errorMessage = str;
    }

    @DexIgnore
    public String toString() {
        return "ErrorItems(_error=" + this._error + ", _errorMessage=" + this._errorMessage + ")";
    }
}
