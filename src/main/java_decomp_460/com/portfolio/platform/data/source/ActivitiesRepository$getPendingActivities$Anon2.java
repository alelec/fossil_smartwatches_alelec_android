package com.portfolio.platform.data.source;

import com.facebook.share.internal.VideoUploader;
import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.fitness.SampleRawDao;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.ActivitiesRepository$getPendingActivities$2", f = "ActivitiesRepository.kt", l = {Action.Selfie.TAKE_BURST}, m = "invokeSuspend")
public final class ActivitiesRepository$getPendingActivities$Anon2 extends ko7 implements vp7<iv7, qn7<? super List<SampleRaw>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$getPendingActivities$Anon2(Date date, Date date2, qn7 qn7) {
        super(2, qn7);
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ActivitiesRepository$getPendingActivities$Anon2 activitiesRepository$getPendingActivities$Anon2 = new ActivitiesRepository$getPendingActivities$Anon2(this.$startDate, this.$endDate, qn7);
        activitiesRepository$getPendingActivities$Anon2.p$ = (iv7) obj;
        return activitiesRepository$getPendingActivities$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super List<SampleRaw>> qn7) {
        return ((ActivitiesRepository$getPendingActivities$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Calendar instance;
        Object y;
        Calendar calendar;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            instance = Calendar.getInstance();
            pq7.b(instance, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
            instance.setTime(this.$startDate);
            Calendar instance2 = Calendar.getInstance();
            pq7.b(instance2, "end");
            instance2.setTime(this.$endDate);
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.L$1 = instance;
            this.L$2 = instance2;
            this.label = 1;
            y = bn5.y(this);
            if (y == d) {
                return d;
            }
            calendar = instance2;
        } else if (i == 1) {
            instance = (Calendar) this.L$1;
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            calendar = (Calendar) this.L$2;
            y = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        SampleRawDao sampleRawDao = ((FitnessDatabase) y).sampleRawDao();
        pq7.b(instance, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date V = lk5.V(instance.getTime());
        pq7.b(V, "DateHelper.getStartOfDay(start.time)");
        pq7.b(calendar, "end");
        Date E = lk5.E(calendar.getTime());
        pq7.b(E, "DateHelper.getEndOfDay(end.time)");
        return sampleRawDao.getPendingActivitySamples(V, E);
    }
}
