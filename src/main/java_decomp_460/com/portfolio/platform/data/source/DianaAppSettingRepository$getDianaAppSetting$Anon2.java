package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.DianaAppSettingRepository$getDianaAppSetting$2", f = "DianaAppSettingRepository.kt", l = {107}, m = "invokeSuspend")
public final class DianaAppSettingRepository$getDianaAppSetting$Anon2 extends ko7 implements vp7<iv7, qn7<? super DianaAppSetting>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $appId;
    @DexIgnore
    public /* final */ /* synthetic */ String $category;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaAppSettingRepository$getDianaAppSetting$Anon2(String str, String str2, qn7 qn7) {
        super(2, qn7);
        this.$category = str;
        this.$appId = str2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        DianaAppSettingRepository$getDianaAppSetting$Anon2 dianaAppSettingRepository$getDianaAppSetting$Anon2 = new DianaAppSettingRepository$getDianaAppSetting$Anon2(this.$category, this.$appId, qn7);
        dianaAppSettingRepository$getDianaAppSetting$Anon2.p$ = (iv7) obj;
        return dianaAppSettingRepository$getDianaAppSetting$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super DianaAppSetting> qn7) {
        return ((DianaAppSettingRepository$getDianaAppSetting$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object v;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.label = 1;
            v = bn5.v(this);
            if (v == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            v = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return ((DianaCustomizeDatabase) v).getDianaAppSettingDao().getDianaAppSetting(this.$category, this.$appId);
    }
}
