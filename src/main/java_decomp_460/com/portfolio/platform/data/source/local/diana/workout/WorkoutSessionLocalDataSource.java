package com.portfolio.platform.data.source.local.diana.workout;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.bw7;
import com.fossil.fl5;
import com.fossil.gl5;
import com.fossil.gu7;
import com.fossil.jv7;
import com.fossil.kq7;
import com.fossil.no4;
import com.fossil.nw0;
import com.fossil.pq7;
import com.fossil.xw7;
import com.fossil.yt0;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionLocalDataSource extends yt0<Long, WorkoutSession> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public /* final */ fl5.a listener;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public fl5 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ nw0.c mObserver; // = new Anon1(this, "workout_session", new String[0]);
    @DexIgnore
    public int mOffset;
    @DexIgnore
    public /* final */ WorkoutSessionRepository workoutSessionRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends nw0.c {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WorkoutSessionLocalDataSource workoutSessionLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = workoutSessionLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.nw0.c
        public void onInvalidated(Set<String> set) {
            pq7.c(set, "tables");
            FLogger.INSTANCE.getLocal().d(WorkoutSessionLocalDataSource.Companion.getTAG(), "XXX- invalidate workout session table");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return WorkoutSessionLocalDataSource.TAG;
        }
    }

    /*
    static {
        String simpleName = WorkoutSessionLocalDataSource.class.getSimpleName();
        pq7.b(simpleName, "WorkoutSessionLocalDataS\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public WorkoutSessionLocalDataSource(WorkoutSessionRepository workoutSessionRepository2, FitnessDatabase fitnessDatabase, Date date, no4 no4, fl5.a aVar) {
        pq7.c(workoutSessionRepository2, "workoutSessionRepository");
        pq7.c(fitnessDatabase, "mFitnessDatabase");
        pq7.c(date, "currentDate");
        pq7.c(no4, "appExecutors");
        pq7.c(aVar, "listener");
        this.workoutSessionRepository = workoutSessionRepository2;
        this.mFitnessDatabase = fitnessDatabase;
        this.currentDate = date;
        this.listener = aVar;
        fl5 fl5 = new fl5(no4.a());
        this.mHelper = fl5;
        this.mNetworkState = gl5.b(fl5);
        this.mHelper.a(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final List<WorkoutSession> getDataInDatabase(int i) {
        return this.mFitnessDatabase.getWorkoutDao().getWorkoutSessionsInDateDesc(this.currentDate, i);
    }

    @DexIgnore
    private final xw7 loadData(fl5.b.a aVar, int i) {
        return gu7.d(jv7.a(bw7.b()), null, null, new WorkoutSessionLocalDataSource$loadData$Anon1(this, i, aVar, null), 3, null);
    }

    @DexIgnore
    public static /* synthetic */ xw7 loadData$default(WorkoutSessionLocalDataSource workoutSessionLocalDataSource, fl5.b.a aVar, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return workoutSessionLocalDataSource.loadData(aVar, i);
    }

    @DexIgnore
    public Long getKey(WorkoutSession workoutSession) {
        pq7.c(workoutSession, "item");
        return Long.valueOf(workoutSession.getCreatedAt());
    }

    @DexIgnore
    public final fl5 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public boolean isInvalid() {
        this.mFitnessDatabase.getInvalidationTracker().h();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.yt0
    public void loadAfter(yt0.f<Long> fVar, yt0.a<WorkoutSession> aVar) {
        pq7.c(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - params = " + ((Object) fVar.f4367a) + " & currentDate = " + this.currentDate);
        WorkoutDao workoutDao = this.mFitnessDatabase.getWorkoutDao();
        Date date = this.currentDate;
        Key key = fVar.f4367a;
        pq7.b(key, "params.key");
        aVar.a(workoutDao.getWorkoutSessionsInDateAfterDesc(date, key.longValue(), fVar.b));
        this.mHelper.h(fl5.d.AFTER, new WorkoutSessionLocalDataSource$loadAfter$Anon1(this));
    }

    @DexIgnore
    @Override // com.fossil.yt0
    public void loadBefore(yt0.f<Long> fVar, yt0.a<WorkoutSession> aVar) {
        pq7.c(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.fossil.yt0
    public void loadInitial(yt0.e<Long> eVar, yt0.c<WorkoutSession> cVar) {
        pq7.c(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - currentDate = " + this.currentDate);
        cVar.a(getDataInDatabase(eVar.b));
        this.mHelper.h(fl5.d.INITIAL, new WorkoutSessionLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.f(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().j(this.mObserver);
    }

    @DexIgnore
    public final void setMHelper(fl5 fl5) {
        pq7.c(fl5, "<set-?>");
        this.mHelper = fl5;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        pq7.c(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }
}
