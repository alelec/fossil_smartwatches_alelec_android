package com.portfolio.platform.data.source.local.diana;

import android.util.Log;
import com.fossil.ax0;
import com.fossil.ci5;
import com.fossil.lx0;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeDatabase$Companion$MIGRATION_FROM_13_TO_14$Anon1 extends ax0 {
    @DexIgnore
    public DianaCustomizeDatabase$Companion$MIGRATION_FROM_13_TO_14$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.ax0
    public void migrate(lx0 lx0) {
        pq7.c(lx0, "database");
        Log.d(DianaCustomizeDatabase.TAG, "MIGRATION_FROM_13_TO_14 - start");
        lx0.beginTransaction();
        try {
            Log.d(DianaCustomizeDatabase.TAG, "Migrate WatchFace - start");
            lx0.execSQL("DROP TABLE IF EXISTS `DianaComplicationRingStyle`");
            lx0.execSQL("CREATE TABLE `DianaComplicationRingStyle` (`id` TEXT NOT NULL, `category` TEXT NOT NULL, `name` TEXT NOT NULL, `data` TEXT NOT NULL, `metaData` TEXT NOT NULL, `serial` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("DROP TABLE IF EXISTS `watch_face_temp`");
            lx0.execSQL("CREATE TABLE `watch_face_temp` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `ringStyleItems` TEXT, `background` TEXT NOT NULL, `previewUrl` TEXT NOT NULL, `serial` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("INSERT INTO watch_face_temp(id,name, ringStyleItems, background, previewUrl, serial) SELECT id, name, ringStyleItems, background, previewUrl, serial FROM watch_face");
            int value = ci5.BACKGROUND.getValue();
            lx0.execSQL("ALTER TABLE `watch_face_temp` ADD `watchFaceType` INTEGER NOT NULL CONSTRAINT DefaultTypeValue DEFAULT(" + value + ')');
            lx0.execSQL("DROP TABLE `watch_face`");
            lx0.execSQL("ALTER TABLE watch_face_temp RENAME TO watch_face");
            Log.d(DianaCustomizeDatabase.TAG, "Migrate WatchFace - end");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e(DianaCustomizeDatabase.TAG, "MIGRATION_FROM_13_TO_14 - end with exception -- e=" + e);
            lx0.execSQL("DROP TABLE IF EXISTS `watch_face`");
            lx0.execSQL("DROP TABLE IF EXISTS `watch_face_temp`");
            lx0.execSQL("CREATE TABLE `watch_face` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `ringStyleItems` TEXT, `background` TEXT NOT NULL, `previewUrl` TEXT NOT NULL, `serial` TEXT NOT NULL, `watchFaceType` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        }
        lx0.setTransactionSuccessful();
        lx0.endTransaction();
    }
}
