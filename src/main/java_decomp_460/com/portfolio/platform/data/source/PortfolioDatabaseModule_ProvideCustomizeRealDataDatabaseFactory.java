package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory implements Factory<CustomizeRealDataDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static CustomizeRealDataDatabase provideCustomizeRealDataDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        CustomizeRealDataDatabase provideCustomizeRealDataDatabase = portfolioDatabaseModule.provideCustomizeRealDataDatabase(portfolioApp);
        lk7.c(provideCustomizeRealDataDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideCustomizeRealDataDatabase;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public CustomizeRealDataDatabase get() {
        return provideCustomizeRealDataDatabase(this.module, this.appProvider.get());
    }
}
