package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.UserRepository$updateUser$2", f = "UserRepository.kt", l = {44, 49, 59}, m = "invokeSuspend")
public final class UserRepository$updateUser$Anon2 extends ko7 implements vp7<iv7, qn7<? super iq5<? extends MFUser>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isRemote;
    @DexIgnore
    public /* final */ /* synthetic */ MFUser $user;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRepository$updateUser$Anon2(UserRepository userRepository, boolean z, MFUser mFUser, qn7 qn7) {
        super(2, qn7);
        this.this$0 = userRepository;
        this.$isRemote = z;
        this.$user = mFUser;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        UserRepository$updateUser$Anon2 userRepository$updateUser$Anon2 = new UserRepository$updateUser$Anon2(this.this$0, this.$isRemote, this.$user, qn7);
        userRepository$updateUser$Anon2.p$ = (iv7) obj;
        return userRepository$updateUser$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<? extends MFUser>> qn7) {
        return ((UserRepository$updateUser$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00dc  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
        // Method dump skipped, instructions count: 270
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.UserRepository$updateUser$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
