package com.portfolio.platform.data.source.local.diana;

import com.fossil.ax0;
import com.fossil.h97;
import com.fossil.kq7;
import com.fossil.qo5;
import com.fossil.qw0;
import com.fossil.u77;
import com.fossil.wo5;
import com.portfolio.platform.data.source.local.RingStyleDao;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class DianaCustomizeDatabase extends qw0 {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ ax0 MIGRATION_FROM_13_TO_14; // = new DianaCustomizeDatabase$Companion$MIGRATION_FROM_13_TO_14$Anon1(13, 14);
    @DexIgnore
    public static /* final */ ax0 MIGRATION_FROM_14_TO_15; // = new DianaCustomizeDatabase$Companion$MIGRATION_FROM_14_TO_15$Anon1(14, 15);
    @DexIgnore
    public static /* final */ ax0 MIGRATION_FROM_15_TO_16; // = new DianaCustomizeDatabase$Companion$MIGRATION_FROM_15_TO_16$Anon1(15, 16);
    @DexIgnore
    public static /* final */ String TAG; // = "DianaCustomizeDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ax0 getMIGRATION_FROM_13_TO_14() {
            return DianaCustomizeDatabase.MIGRATION_FROM_13_TO_14;
        }

        @DexIgnore
        public final ax0 getMIGRATION_FROM_14_TO_15() {
            return DianaCustomizeDatabase.MIGRATION_FROM_14_TO_15;
        }

        @DexIgnore
        public final ax0 getMIGRATION_FROM_15_TO_16() {
            return DianaCustomizeDatabase.MIGRATION_FROM_15_TO_16;
        }
    }

    @DexIgnore
    public abstract ComplicationDao getComplicationDao();

    @DexIgnore
    public abstract ComplicationLastSettingDao getComplicationLastSettingDao();

    @DexIgnore
    public abstract DianaAppSettingDao getDianaAppSettingDao();

    @DexIgnore
    public abstract qo5 getDianaPresetDao();

    @DexIgnore
    public abstract DianaWatchFaceRingDao getDianaWatchFaceRingDao();

    @DexIgnore
    public abstract DianaWatchFaceTemplateDao getDianaWatchFaceTemplateDao();

    @DexIgnore
    public abstract DianaWatchFaceUserDao getDianaWatchFaceUserDao();

    @DexIgnore
    public abstract DianaPresetDao getPresetDao();

    @DexIgnore
    public abstract wo5 getRecommendedPresetDao();

    @DexIgnore
    public abstract RingStyleDao getRingStyleDao();

    @DexIgnore
    public abstract h97 getWFBackgroundDao();

    @DexIgnore
    public abstract u77 getWFTemplateDao();

    @DexIgnore
    public abstract WatchAppDao getWatchAppDao();

    @DexIgnore
    public abstract WatchAppDataDao getWatchAppDataDao();

    @DexIgnore
    public abstract WatchAppLastSettingDao getWatchAppSettingDao();

    @DexIgnore
    public abstract WatchFaceDao getWatchFaceDao();
}
