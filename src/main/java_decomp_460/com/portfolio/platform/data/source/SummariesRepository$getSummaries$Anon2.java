package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bw7;
import com.fossil.c47;
import com.fossil.cl7;
import com.fossil.dv7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.eu7;
import com.fossil.gi0;
import com.fossil.gj4;
import com.fossil.h47;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.ss0;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SummariesRepository$getSummaries$2", f = "SummariesRepository.kt", l = {231}, m = "invokeSuspend")
public final class SummariesRepository$getSummaries$Anon2 extends ko7 implements vp7<iv7, qn7<? super LiveData<h47<? extends List<ActivitySummary>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements gi0<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ FitnessDatabase $fitnessDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends c47<List<ActivitySummary>, gj4> {
            @DexIgnore
            public /* final */ /* synthetic */ cl7 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, cl7 cl7) {
                this.this$0 = anon1_Level2;
                this.$downloadingDate = cl7;
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object createCall(qn7<? super q88<gj4>> qn7) {
                Date date;
                Date date2;
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiServiceV2;
                cl7 cl7 = this.$downloadingDate;
                if (cl7 == null || (date = (Date) cl7.getFirst()) == null) {
                    date = this.this$0.this$0.$startDate;
                }
                String k = lk5.k(date);
                pq7.b(k, "DateHelper.formatShortDa\u2026            ?: startDate)");
                cl7 cl72 = this.$downloadingDate;
                if (cl72 == null || (date2 = (Date) cl72.getSecond()) == null) {
                    date2 = this.this$0.this$0.$endDate;
                }
                String k2 = lk5.k(date2);
                pq7.b(k2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return apiServiceV2.getSummaries(k, k2, 0, 100, qn7);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:16:0x0078  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
            @Override // com.fossil.c47
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object loadFromDb(com.fossil.qn7<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>>> r9) {
                /*
                // Method dump skipped, instructions count: 269
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.getSummaries.Anon2.Anon1_Level2.Anon1_Level3.loadFromDb(com.fossil.qn7):java.lang.Object");
            }

            @DexIgnore
            @Override // com.fossil.c47
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getSummaries - onFetchFailed");
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:19:0x0083  */
            /* JADX WARNING: Removed duplicated region for block: B:22:0x00bd  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object saveCallResult(com.fossil.gj4 r10, com.fossil.qn7<? super com.fossil.tl7> r11) {
                /*
                // Method dump skipped, instructions count: 378
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.getSummaries.Anon2.Anon1_Level2.Anon1_Level3.saveCallResult(com.fossil.gj4, com.fossil.qn7):java.lang.Object");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
            @Override // com.fossil.c47
            public /* bridge */ /* synthetic */ Object saveCallResult(gj4 gj4, qn7 qn7) {
                return saveCallResult(gj4, (qn7<? super tl7>) qn7);
            }

            @DexIgnore
            public boolean shouldFetch(List<ActivitySummary> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(SummariesRepository$getSummaries$Anon2 summariesRepository$getSummaries$Anon2, FitnessDatabase fitnessDatabase) {
            this.this$0 = summariesRepository$getSummaries$Anon2;
            this.$fitnessDatabase = fitnessDatabase;
        }

        @DexIgnore
        public final LiveData<h47<List<ActivitySummary>>> apply(List<FitnessDataWrapper> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "getSummaries - startDate=" + this.this$0.$startDate + ", endDate=" + this.this$0.$endDate + " fitnessDataList=" + list.size());
            pq7.b(list, "fitnessDataList");
            SummariesRepository$getSummaries$Anon2 summariesRepository$getSummaries$Anon2 = this.this$0;
            return new Anon1_Level3(this, FitnessDataWrapperKt.calculateRangeDownload(list, summariesRepository$getSummaries$Anon2.$startDate, summariesRepository$getSummaries$Anon2.$endDate)).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummaries$Anon2(SummariesRepository summariesRepository, Date date, Date date2, boolean z, qn7 qn7) {
        super(2, qn7);
        this.this$0 = summariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SummariesRepository$getSummaries$Anon2 summariesRepository$getSummaries$Anon2 = new SummariesRepository$getSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, this.$shouldFetch, qn7);
        summariesRepository$getSummaries$Anon2.p$ = (iv7) obj;
        return summariesRepository$getSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super LiveData<h47<? extends List<ActivitySummary>>>> qn7) {
        return ((SummariesRepository$getSummaries$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object g;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "getSummaries - startDate=" + this.$startDate + ", endDate=" + this.$endDate);
            dv7 b = bw7.b();
            SummariesRepository$getSummaries$Anon2$fitnessDatabase$Anon1_Level2 summariesRepository$getSummaries$Anon2$fitnessDatabase$Anon1_Level2 = new SummariesRepository$getSummaries$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = iv7;
            this.label = 1;
            g = eu7.g(b, summariesRepository$getSummaries$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (g == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            g = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) g;
        return ss0.c(fitnessDatabase.getFitnessDataDao().getFitnessDataLiveData(this.$startDate, this.$endDate), new Anon1_Level2(this, fitnessDatabase));
    }
}
