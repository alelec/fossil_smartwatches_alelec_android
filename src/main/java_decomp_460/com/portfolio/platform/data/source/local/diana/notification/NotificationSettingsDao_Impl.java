package com.portfolio.platform.data.source.local.diana.notification;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationSettingsDao_Impl implements NotificationSettingsDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<NotificationSettingsModel> __insertionAdapterOfNotificationSettingsModel;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<NotificationSettingsModel> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, NotificationSettingsModel notificationSettingsModel) {
            if (notificationSettingsModel.getSettingsName() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, notificationSettingsModel.getSettingsName());
            }
            px0.bindLong(2, (long) notificationSettingsModel.getSettingsType());
            px0.bindLong(3, notificationSettingsModel.isCall() ? 1 : 0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `notificationSettings` (`settingsName`,`settingsType`,`isCall`) VALUES (?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM notificationSettings";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<NotificationSettingsModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon3(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<NotificationSettingsModel> call() throws Exception {
            Cursor b = ex0.b(NotificationSettingsDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "settingsName");
                int c2 = dx0.c(b, "settingsType");
                int c3 = dx0.c(b, "isCall");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new NotificationSettingsModel(b.getString(c), b.getInt(c2), b.getInt(c3) != 0));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<NotificationSettingsModel> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon4(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public NotificationSettingsModel call() throws Exception {
            NotificationSettingsModel notificationSettingsModel = null;
            boolean z = false;
            Cursor b = ex0.b(NotificationSettingsDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "settingsName");
                int c2 = dx0.c(b, "settingsType");
                int c3 = dx0.c(b, "isCall");
                if (b.moveToFirst()) {
                    String string = b.getString(c);
                    int i = b.getInt(c2);
                    if (b.getInt(c3) != 0) {
                        z = true;
                    }
                    notificationSettingsModel = new NotificationSettingsModel(string, i, z);
                }
                return notificationSettingsModel;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public NotificationSettingsDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfNotificationSettingsModel = new Anon1(qw0);
        this.__preparedStmtOfDelete = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public LiveData<List<NotificationSettingsModel>> getListNotificationSettings() {
        tw0 f = tw0.f("SELECT * FROM notificationSettings", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"notificationSettings"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public List<NotificationSettingsModel> getListNotificationSettingsNoLiveData() {
        tw0 f = tw0.f("SELECT * FROM notificationSettings", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "settingsName");
            int c2 = dx0.c(b, "settingsType");
            int c3 = dx0.c(b, "isCall");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new NotificationSettingsModel(b.getString(c), b.getInt(c2), b.getInt(c3) != 0));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public LiveData<NotificationSettingsModel> getNotificationSettingsWithFieldIsCall(boolean z) {
        tw0 f = tw0.f("SELECT * FROM notificationSettings WHERE isCall = ?", 1);
        f.bindLong(1, z ? 1 : 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"notificationSettings"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public NotificationSettingsModel getNotificationSettingsWithIsCallNoLiveData(boolean z) {
        NotificationSettingsModel notificationSettingsModel = null;
        boolean z2 = true;
        tw0 f = tw0.f("SELECT * FROM notificationSettings WHERE isCall = ?", 1);
        f.bindLong(1, z ? 1 : 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "settingsName");
            int c2 = dx0.c(b, "settingsType");
            int c3 = dx0.c(b, "isCall");
            if (b.moveToFirst()) {
                String string = b.getString(c);
                int i = b.getInt(c2);
                if (b.getInt(c3) == 0) {
                    z2 = false;
                }
                notificationSettingsModel = new NotificationSettingsModel(string, i, z2);
            }
            return notificationSettingsModel;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public void insertListNotificationSettings(List<NotificationSettingsModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfNotificationSettingsModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public void insertNotificationSettings(NotificationSettingsModel notificationSettingsModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfNotificationSettingsModel.insert((jw0<NotificationSettingsModel>) notificationSettingsModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
