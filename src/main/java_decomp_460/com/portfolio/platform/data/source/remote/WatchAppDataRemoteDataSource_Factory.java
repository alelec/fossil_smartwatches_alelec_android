package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppDataRemoteDataSource_Factory implements Factory<WatchAppDataRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;

    @DexIgnore
    public WatchAppDataRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceV2Provider = provider;
    }

    @DexIgnore
    public static WatchAppDataRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new WatchAppDataRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static WatchAppDataRemoteDataSource newInstance(ApiServiceV2 apiServiceV2) {
        return new WatchAppDataRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WatchAppDataRemoteDataSource get() {
        return newInstance(this.mApiServiceV2Provider.get());
    }
}
