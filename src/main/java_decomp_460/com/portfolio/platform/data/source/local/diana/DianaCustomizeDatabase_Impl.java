package com.portfolio.platform.data.source.local.diana;

import com.fossil.ex0;
import com.fossil.h97;
import com.fossil.hw0;
import com.fossil.i97;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qo5;
import com.fossil.qw0;
import com.fossil.ro5;
import com.fossil.sw0;
import com.fossil.u77;
import com.fossil.v77;
import com.fossil.wo5;
import com.fossil.xo5;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.local.RingStyleDao;
import com.portfolio.platform.data.source.local.RingStyleDao_Impl;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeDatabase_Impl extends DianaCustomizeDatabase {
    @DexIgnore
    public volatile ComplicationDao _complicationDao;
    @DexIgnore
    public volatile ComplicationLastSettingDao _complicationLastSettingDao;
    @DexIgnore
    public volatile DianaAppSettingDao _dianaAppSettingDao;
    @DexIgnore
    public volatile DianaPresetDao _dianaPresetDao;
    @DexIgnore
    public volatile qo5 _dianaPresetDao_1;
    @DexIgnore
    public volatile wo5 _dianaRecommendedPresetDao;
    @DexIgnore
    public volatile DianaWatchFaceRingDao _dianaWatchFaceRingDao;
    @DexIgnore
    public volatile DianaWatchFaceTemplateDao _dianaWatchFaceTemplateDao;
    @DexIgnore
    public volatile DianaWatchFaceUserDao _dianaWatchFaceUserDao;
    @DexIgnore
    public volatile RingStyleDao _ringStyleDao;
    @DexIgnore
    public volatile u77 _wFAssetsDao;
    @DexIgnore
    public volatile h97 _wFBackgroundPhotoDao;
    @DexIgnore
    public volatile WatchAppDao _watchAppDao;
    @DexIgnore
    public volatile WatchAppDataDao _watchAppDataDao;
    @DexIgnore
    public volatile WatchAppLastSettingDao _watchAppLastSettingDao;
    @DexIgnore
    public volatile WatchFaceDao _watchFaceDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `complicationLastSetting` (`complicationId` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`complicationId`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `watchAppLastSetting` (`watchAppId` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`watchAppId`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `dianaPreset` (`createdAt` TEXT, `updatedAt` TEXT, `pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `name` TEXT NOT NULL, `isActive` INTEGER NOT NULL, `complications` TEXT NOT NULL, `watchapps` TEXT NOT NULL, `watchFaceId` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `dianaRecommendPreset` (`serialNumber` TEXT NOT NULL, `id` TEXT NOT NULL, `name` TEXT NOT NULL, `isDefault` INTEGER NOT NULL, `complications` TEXT NOT NULL, `watchapps` TEXT NOT NULL, `watchFaceId` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `watch_face` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `ringStyleItems` TEXT, `background` TEXT NOT NULL, `previewUrl` TEXT NOT NULL, `serial` TEXT NOT NULL, `watchFaceType` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `DianaComplicationRingStyle` (`id` TEXT NOT NULL, `category` TEXT NOT NULL, `name` TEXT NOT NULL, `data` TEXT NOT NULL, `metaData` TEXT NOT NULL, `serial` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `complication` (`complicationId` TEXT NOT NULL, `name` TEXT NOT NULL, `nameKey` TEXT NOT NULL, `categories` TEXT NOT NULL, `description` TEXT NOT NULL, `descriptionKey` TEXT NOT NULL, `icon` TEXT, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`complicationId`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `watchApp` (`watchappId` TEXT NOT NULL, `name` TEXT NOT NULL, `nameKey` TEXT NOT NULL, `description` TEXT NOT NULL, `descriptionKey` TEXT NOT NULL, `categories` TEXT NOT NULL, `icon` TEXT, `updatedAt` TEXT NOT NULL, `createdAt` TEXT NOT NULL, PRIMARY KEY(`watchappId`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `watchAppData` (`id` TEXT NOT NULL, `type` TEXT NOT NULL, `maxAppVersion` TEXT NOT NULL, `maxFirmwareOSVersion` TEXT NOT NULL, `minAppVersion` TEXT NOT NULL, `minFirmwareOSVersion` TEXT NOT NULL, `version` TEXT NOT NULL, `executableBinaryDataUrl` TEXT NOT NULL, `updatedAt` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `DianaAppSetting` (`pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `appId` TEXT NOT NULL, `category` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`appId`, `category`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `dianaWatchfaceTemplate` (`createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `id` TEXT NOT NULL, `downloadURL` TEXT NOT NULL, `checksum` TEXT NOT NULL, `firmwareOSVersion` TEXT NOT NULL, `packageVersion` TEXT NOT NULL, `themeClass` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `diana_watchface_preset` (`createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `buttons` TEXT, `checksumFace` TEXT NOT NULL, `faceUrl` TEXT NOT NULL, `previewFaceUrl` TEXT, `isActive` INTEGER NOT NULL, `name` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `uid` TEXT NOT NULL, `originalItemIdInStore` TEXT, `isNew` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `diana_recommended_preset` (`serial` TEXT NOT NULL, `id` TEXT NOT NULL, `name` TEXT NOT NULL, `button` TEXT NOT NULL, `isDefault` INTEGER NOT NULL, `faceUrl` TEXT, `checkSum` TEXT, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `dianaWatchfaceUser` (`createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `downloadURL` TEXT NOT NULL, `name` TEXT NOT NULL, `checksum` TEXT NOT NULL, `previewURL` TEXT NOT NULL, `uid` TEXT NOT NULL, `orderId` TEXT, `watchFaceId` TEXT, `packageVersion` TEXT, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `dianaWatchFaceRing` (`createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `id` TEXT NOT NULL, `category` TEXT NOT NULL, `name` TEXT NOT NULL, `data` TEXT NOT NULL, `metaData` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `wf_asset` (`id` TEXT NOT NULL, `category` TEXT NOT NULL, `name` TEXT NOT NULL, `data` TEXT NOT NULL, `isNew` INTEGER NOT NULL, `meta` TEXT, `assetType` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `wf_background_photo` (`id` TEXT NOT NULL, `downloadUrl` TEXT NOT NULL, `checkSum` TEXT NOT NULL, `uid` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `localPath` TEXT, `pinType` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'a21b3e763a4890918d43c35c227a4ff1')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `complicationLastSetting`");
            lx0.execSQL("DROP TABLE IF EXISTS `watchAppLastSetting`");
            lx0.execSQL("DROP TABLE IF EXISTS `dianaPreset`");
            lx0.execSQL("DROP TABLE IF EXISTS `dianaRecommendPreset`");
            lx0.execSQL("DROP TABLE IF EXISTS `watch_face`");
            lx0.execSQL("DROP TABLE IF EXISTS `DianaComplicationRingStyle`");
            lx0.execSQL("DROP TABLE IF EXISTS `complication`");
            lx0.execSQL("DROP TABLE IF EXISTS `watchApp`");
            lx0.execSQL("DROP TABLE IF EXISTS `watchAppData`");
            lx0.execSQL("DROP TABLE IF EXISTS `DianaAppSetting`");
            lx0.execSQL("DROP TABLE IF EXISTS `dianaWatchfaceTemplate`");
            lx0.execSQL("DROP TABLE IF EXISTS `diana_watchface_preset`");
            lx0.execSQL("DROP TABLE IF EXISTS `diana_recommended_preset`");
            lx0.execSQL("DROP TABLE IF EXISTS `dianaWatchfaceUser`");
            lx0.execSQL("DROP TABLE IF EXISTS `dianaWatchFaceRing`");
            lx0.execSQL("DROP TABLE IF EXISTS `wf_asset`");
            lx0.execSQL("DROP TABLE IF EXISTS `wf_background_photo`");
            if (DianaCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = DianaCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) DianaCustomizeDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (DianaCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = DianaCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) DianaCustomizeDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            DianaCustomizeDatabase_Impl.this.mDatabase = lx0;
            DianaCustomizeDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (DianaCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = DianaCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) DianaCustomizeDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("complicationId", new ix0.a("complicationId", "TEXT", true, 1, null, 1));
            hashMap.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap.put(MicroAppSetting.SETTING, new ix0.a(MicroAppSetting.SETTING, "TEXT", true, 0, null, 1));
            ix0 ix0 = new ix0("complicationLastSetting", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "complicationLastSetting");
            if (!ix0.equals(a2)) {
                return new sw0.b(false, "complicationLastSetting(com.portfolio.platform.data.model.diana.ComplicationLastSetting).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(3);
            hashMap2.put("watchAppId", new ix0.a("watchAppId", "TEXT", true, 1, null, 1));
            hashMap2.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap2.put(MicroAppSetting.SETTING, new ix0.a(MicroAppSetting.SETTING, "TEXT", true, 0, null, 1));
            ix0 ix02 = new ix0("watchAppLastSetting", hashMap2, new HashSet(0), new HashSet(0));
            ix0 a3 = ix0.a(lx0, "watchAppLastSetting");
            if (!ix02.equals(a3)) {
                return new sw0.b(false, "watchAppLastSetting(com.portfolio.platform.data.model.diana.WatchAppLastSetting).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
            }
            HashMap hashMap3 = new HashMap(10);
            hashMap3.put("createdAt", new ix0.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap3.put("updatedAt", new ix0.a("updatedAt", "TEXT", false, 0, null, 1));
            hashMap3.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap3.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap3.put("serialNumber", new ix0.a("serialNumber", "TEXT", true, 0, null, 1));
            hashMap3.put("name", new ix0.a("name", "TEXT", true, 0, null, 1));
            hashMap3.put("isActive", new ix0.a("isActive", "INTEGER", true, 0, null, 1));
            hashMap3.put("complications", new ix0.a("complications", "TEXT", true, 0, null, 1));
            hashMap3.put("watchapps", new ix0.a("watchapps", "TEXT", true, 0, null, 1));
            hashMap3.put("watchFaceId", new ix0.a("watchFaceId", "TEXT", true, 0, null, 1));
            ix0 ix03 = new ix0("dianaPreset", hashMap3, new HashSet(0), new HashSet(0));
            ix0 a4 = ix0.a(lx0, "dianaPreset");
            if (!ix03.equals(a4)) {
                return new sw0.b(false, "dianaPreset(com.portfolio.platform.data.model.diana.preset.DianaPreset).\n Expected:\n" + ix03 + "\n Found:\n" + a4);
            }
            HashMap hashMap4 = new HashMap(9);
            hashMap4.put("serialNumber", new ix0.a("serialNumber", "TEXT", true, 0, null, 1));
            hashMap4.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap4.put("name", new ix0.a("name", "TEXT", true, 0, null, 1));
            hashMap4.put("isDefault", new ix0.a("isDefault", "INTEGER", true, 0, null, 1));
            hashMap4.put("complications", new ix0.a("complications", "TEXT", true, 0, null, 1));
            hashMap4.put("watchapps", new ix0.a("watchapps", "TEXT", true, 0, null, 1));
            hashMap4.put("watchFaceId", new ix0.a("watchFaceId", "TEXT", true, 0, null, 1));
            hashMap4.put("createdAt", new ix0.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap4.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            ix0 ix04 = new ix0("dianaRecommendPreset", hashMap4, new HashSet(0), new HashSet(0));
            ix0 a5 = ix0.a(lx0, "dianaRecommendPreset");
            if (!ix04.equals(a5)) {
                return new sw0.b(false, "dianaRecommendPreset(com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset).\n Expected:\n" + ix04 + "\n Found:\n" + a5);
            }
            HashMap hashMap5 = new HashMap(7);
            hashMap5.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap5.put("name", new ix0.a("name", "TEXT", true, 0, null, 1));
            hashMap5.put("ringStyleItems", new ix0.a("ringStyleItems", "TEXT", false, 0, null, 1));
            hashMap5.put(Explore.COLUMN_BACKGROUND, new ix0.a(Explore.COLUMN_BACKGROUND, "TEXT", true, 0, null, 1));
            hashMap5.put("previewUrl", new ix0.a("previewUrl", "TEXT", true, 0, null, 1));
            hashMap5.put("serial", new ix0.a("serial", "TEXT", true, 0, null, 1));
            hashMap5.put("watchFaceType", new ix0.a("watchFaceType", "INTEGER", true, 0, null, 1));
            ix0 ix05 = new ix0("watch_face", hashMap5, new HashSet(0), new HashSet(0));
            ix0 a6 = ix0.a(lx0, "watch_face");
            if (!ix05.equals(a6)) {
                return new sw0.b(false, "watch_face(com.portfolio.platform.data.model.diana.preset.WatchFace).\n Expected:\n" + ix05 + "\n Found:\n" + a6);
            }
            HashMap hashMap6 = new HashMap(6);
            hashMap6.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap6.put("category", new ix0.a("category", "TEXT", true, 0, null, 1));
            hashMap6.put("name", new ix0.a("name", "TEXT", true, 0, null, 1));
            hashMap6.put("data", new ix0.a("data", "TEXT", true, 0, null, 1));
            hashMap6.put("metaData", new ix0.a("metaData", "TEXT", true, 0, null, 1));
            hashMap6.put("serial", new ix0.a("serial", "TEXT", true, 0, null, 1));
            ix0 ix06 = new ix0("DianaComplicationRingStyle", hashMap6, new HashSet(0), new HashSet(0));
            ix0 a7 = ix0.a(lx0, "DianaComplicationRingStyle");
            if (!ix06.equals(a7)) {
                return new sw0.b(false, "DianaComplicationRingStyle(com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle).\n Expected:\n" + ix06 + "\n Found:\n" + a7);
            }
            HashMap hashMap7 = new HashMap(9);
            hashMap7.put("complicationId", new ix0.a("complicationId", "TEXT", true, 1, null, 1));
            hashMap7.put("name", new ix0.a("name", "TEXT", true, 0, null, 1));
            hashMap7.put("nameKey", new ix0.a("nameKey", "TEXT", true, 0, null, 1));
            hashMap7.put("categories", new ix0.a("categories", "TEXT", true, 0, null, 1));
            hashMap7.put("description", new ix0.a("description", "TEXT", true, 0, null, 1));
            hashMap7.put("descriptionKey", new ix0.a("descriptionKey", "TEXT", true, 0, null, 1));
            hashMap7.put("icon", new ix0.a("icon", "TEXT", false, 0, null, 1));
            hashMap7.put("createdAt", new ix0.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap7.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            ix0 ix07 = new ix0("complication", hashMap7, new HashSet(0), new HashSet(0));
            ix0 a8 = ix0.a(lx0, "complication");
            if (!ix07.equals(a8)) {
                return new sw0.b(false, "complication(com.portfolio.platform.data.model.diana.Complication).\n Expected:\n" + ix07 + "\n Found:\n" + a8);
            }
            HashMap hashMap8 = new HashMap(9);
            hashMap8.put("watchappId", new ix0.a("watchappId", "TEXT", true, 1, null, 1));
            hashMap8.put("name", new ix0.a("name", "TEXT", true, 0, null, 1));
            hashMap8.put("nameKey", new ix0.a("nameKey", "TEXT", true, 0, null, 1));
            hashMap8.put("description", new ix0.a("description", "TEXT", true, 0, null, 1));
            hashMap8.put("descriptionKey", new ix0.a("descriptionKey", "TEXT", true, 0, null, 1));
            hashMap8.put("categories", new ix0.a("categories", "TEXT", true, 0, null, 1));
            hashMap8.put("icon", new ix0.a("icon", "TEXT", false, 0, null, 1));
            hashMap8.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap8.put("createdAt", new ix0.a("createdAt", "TEXT", true, 0, null, 1));
            ix0 ix08 = new ix0("watchApp", hashMap8, new HashSet(0), new HashSet(0));
            ix0 a9 = ix0.a(lx0, "watchApp");
            if (!ix08.equals(a9)) {
                return new sw0.b(false, "watchApp(com.portfolio.platform.data.model.diana.WatchApp).\n Expected:\n" + ix08 + "\n Found:\n" + a9);
            }
            HashMap hashMap9 = new HashMap(10);
            hashMap9.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap9.put("type", new ix0.a("type", "TEXT", true, 0, null, 1));
            hashMap9.put("maxAppVersion", new ix0.a("maxAppVersion", "TEXT", true, 0, null, 1));
            hashMap9.put("maxFirmwareOSVersion", new ix0.a("maxFirmwareOSVersion", "TEXT", true, 0, null, 1));
            hashMap9.put("minAppVersion", new ix0.a("minAppVersion", "TEXT", true, 0, null, 1));
            hashMap9.put("minFirmwareOSVersion", new ix0.a("minFirmwareOSVersion", "TEXT", true, 0, null, 1));
            hashMap9.put("version", new ix0.a("version", "TEXT", true, 0, null, 1));
            hashMap9.put("executableBinaryDataUrl", new ix0.a("executableBinaryDataUrl", "TEXT", true, 0, null, 1));
            hashMap9.put("updatedAt", new ix0.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap9.put("createdAt", new ix0.a("createdAt", "INTEGER", true, 0, null, 1));
            ix0 ix09 = new ix0("watchAppData", hashMap9, new HashSet(0), new HashSet(0));
            ix0 a10 = ix0.a(lx0, "watchAppData");
            if (!ix09.equals(a10)) {
                return new sw0.b(false, "watchAppData(com.portfolio.platform.data.model.diana.WatchAppData).\n Expected:\n" + ix09 + "\n Found:\n" + a10);
            }
            HashMap hashMap10 = new HashMap(5);
            hashMap10.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap10.put("id", new ix0.a("id", "TEXT", true, 0, null, 1));
            hashMap10.put("appId", new ix0.a("appId", "TEXT", true, 1, null, 1));
            hashMap10.put("category", new ix0.a("category", "TEXT", true, 2, null, 1));
            hashMap10.put(MicroAppSetting.SETTING, new ix0.a(MicroAppSetting.SETTING, "TEXT", true, 0, null, 1));
            ix0 ix010 = new ix0("DianaAppSetting", hashMap10, new HashSet(0), new HashSet(0));
            ix0 a11 = ix0.a(lx0, "DianaAppSetting");
            if (!ix010.equals(a11)) {
                return new sw0.b(false, "DianaAppSetting(com.portfolio.platform.data.model.diana.DianaAppSetting).\n Expected:\n" + ix010 + "\n Found:\n" + a11);
            }
            HashMap hashMap11 = new HashMap(8);
            hashMap11.put("createdAt", new ix0.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap11.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap11.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap11.put("downloadURL", new ix0.a("downloadURL", "TEXT", true, 0, null, 1));
            hashMap11.put("checksum", new ix0.a("checksum", "TEXT", true, 0, null, 1));
            hashMap11.put("firmwareOSVersion", new ix0.a("firmwareOSVersion", "TEXT", true, 0, null, 1));
            hashMap11.put("packageVersion", new ix0.a("packageVersion", "TEXT", true, 0, null, 1));
            hashMap11.put("themeClass", new ix0.a("themeClass", "TEXT", true, 0, null, 1));
            ix0 ix011 = new ix0("dianaWatchfaceTemplate", hashMap11, new HashSet(0), new HashSet(0));
            ix0 a12 = ix0.a(lx0, "dianaWatchfaceTemplate");
            if (!ix011.equals(a12)) {
                return new sw0.b(false, "dianaWatchfaceTemplate(com.portfolio.platform.data.model.watchface.DianaWatchFaceTemplate).\n Expected:\n" + ix011 + "\n Found:\n" + a12);
            }
            HashMap hashMap12 = new HashMap(14);
            hashMap12.put("createdAt", new ix0.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap12.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap12.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap12.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap12.put("buttons", new ix0.a("buttons", "TEXT", false, 0, null, 1));
            hashMap12.put("checksumFace", new ix0.a("checksumFace", "TEXT", true, 0, null, 1));
            hashMap12.put("faceUrl", new ix0.a("faceUrl", "TEXT", true, 0, null, 1));
            hashMap12.put("previewFaceUrl", new ix0.a("previewFaceUrl", "TEXT", false, 0, null, 1));
            hashMap12.put("isActive", new ix0.a("isActive", "INTEGER", true, 0, null, 1));
            hashMap12.put("name", new ix0.a("name", "TEXT", true, 0, null, 1));
            hashMap12.put("serialNumber", new ix0.a("serialNumber", "TEXT", true, 0, null, 1));
            hashMap12.put("uid", new ix0.a("uid", "TEXT", true, 0, null, 1));
            hashMap12.put("originalItemIdInStore", new ix0.a("originalItemIdInStore", "TEXT", false, 0, null, 1));
            hashMap12.put("isNew", new ix0.a("isNew", "INTEGER", true, 0, null, 1));
            ix0 ix012 = new ix0("diana_watchface_preset", hashMap12, new HashSet(0), new HashSet(0));
            ix0 a13 = ix0.a(lx0, "diana_watchface_preset");
            if (!ix012.equals(a13)) {
                return new sw0.b(false, "diana_watchface_preset(com.portfolio.platform.preset.data.entity.DianaPreset).\n Expected:\n" + ix012 + "\n Found:\n" + a13);
            }
            HashMap hashMap13 = new HashMap(9);
            hashMap13.put("serial", new ix0.a("serial", "TEXT", true, 0, null, 1));
            hashMap13.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap13.put("name", new ix0.a("name", "TEXT", true, 0, null, 1));
            hashMap13.put("button", new ix0.a("button", "TEXT", true, 0, null, 1));
            hashMap13.put("isDefault", new ix0.a("isDefault", "INTEGER", true, 0, null, 1));
            hashMap13.put("faceUrl", new ix0.a("faceUrl", "TEXT", false, 0, null, 1));
            hashMap13.put("checkSum", new ix0.a("checkSum", "TEXT", false, 0, null, 1));
            hashMap13.put("createdAt", new ix0.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap13.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            ix0 ix013 = new ix0("diana_recommended_preset", hashMap13, new HashSet(0), new HashSet(0));
            ix0 a14 = ix0.a(lx0, "diana_recommended_preset");
            if (!ix013.equals(a14)) {
                return new sw0.b(false, "diana_recommended_preset(com.portfolio.platform.preset.data.entity.DianaRecommendedPreset).\n Expected:\n" + ix013 + "\n Found:\n" + a14);
            }
            HashMap hashMap14 = new HashMap(12);
            hashMap14.put("createdAt", new ix0.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap14.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap14.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap14.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap14.put("downloadURL", new ix0.a("downloadURL", "TEXT", true, 0, null, 1));
            hashMap14.put("name", new ix0.a("name", "TEXT", true, 0, null, 1));
            hashMap14.put("checksum", new ix0.a("checksum", "TEXT", true, 0, null, 1));
            hashMap14.put("previewURL", new ix0.a("previewURL", "TEXT", true, 0, null, 1));
            hashMap14.put("uid", new ix0.a("uid", "TEXT", true, 0, null, 1));
            hashMap14.put("orderId", new ix0.a("orderId", "TEXT", false, 0, null, 1));
            hashMap14.put("watchFaceId", new ix0.a("watchFaceId", "TEXT", false, 0, null, 1));
            hashMap14.put("packageVersion", new ix0.a("packageVersion", "TEXT", false, 0, null, 1));
            ix0 ix014 = new ix0("dianaWatchfaceUser", hashMap14, new HashSet(0), new HashSet(0));
            ix0 a15 = ix0.a(lx0, "dianaWatchfaceUser");
            if (!ix014.equals(a15)) {
                return new sw0.b(false, "dianaWatchfaceUser(com.portfolio.platform.data.model.watchface.DianaWatchFaceUser).\n Expected:\n" + ix014 + "\n Found:\n" + a15);
            }
            HashMap hashMap15 = new HashMap(7);
            hashMap15.put("createdAt", new ix0.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap15.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap15.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap15.put("category", new ix0.a("category", "TEXT", true, 0, null, 1));
            hashMap15.put("name", new ix0.a("name", "TEXT", true, 0, null, 1));
            hashMap15.put("data", new ix0.a("data", "TEXT", true, 0, null, 1));
            hashMap15.put("metaData", new ix0.a("metaData", "TEXT", true, 0, null, 1));
            ix0 ix015 = new ix0("dianaWatchFaceRing", hashMap15, new HashSet(0), new HashSet(0));
            ix0 a16 = ix0.a(lx0, "dianaWatchFaceRing");
            if (!ix015.equals(a16)) {
                return new sw0.b(false, "dianaWatchFaceRing(com.portfolio.platform.data.model.watchface.DianaWatchFaceRing).\n Expected:\n" + ix015 + "\n Found:\n" + a16);
            }
            HashMap hashMap16 = new HashMap(7);
            hashMap16.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap16.put("category", new ix0.a("category", "TEXT", true, 0, null, 1));
            hashMap16.put("name", new ix0.a("name", "TEXT", true, 0, null, 1));
            hashMap16.put("data", new ix0.a("data", "TEXT", true, 0, null, 1));
            hashMap16.put("isNew", new ix0.a("isNew", "INTEGER", true, 0, null, 1));
            hashMap16.put(Constants.META, new ix0.a(Constants.META, "TEXT", false, 0, null, 1));
            hashMap16.put("assetType", new ix0.a("assetType", "INTEGER", true, 0, null, 1));
            ix0 ix016 = new ix0("wf_asset", hashMap16, new HashSet(0), new HashSet(0));
            ix0 a17 = ix0.a(lx0, "wf_asset");
            if (!ix016.equals(a17)) {
                return new sw0.b(false, "wf_asset(com.portfolio.platform.watchface.data.entity.WFAsset).\n Expected:\n" + ix016 + "\n Found:\n" + a17);
            }
            HashMap hashMap17 = new HashMap(8);
            hashMap17.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap17.put(Firmware.COLUMN_DOWNLOAD_URL, new ix0.a(Firmware.COLUMN_DOWNLOAD_URL, "TEXT", true, 0, null, 1));
            hashMap17.put("checkSum", new ix0.a("checkSum", "TEXT", true, 0, null, 1));
            hashMap17.put("uid", new ix0.a("uid", "TEXT", true, 0, null, 1));
            hashMap17.put("createdAt", new ix0.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap17.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap17.put("localPath", new ix0.a("localPath", "TEXT", false, 0, null, 1));
            hashMap17.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            ix0 ix017 = new ix0("wf_background_photo", hashMap17, new HashSet(0), new HashSet(0));
            ix0 a18 = ix0.a(lx0, "wf_background_photo");
            if (ix017.equals(a18)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "wf_background_photo(com.portfolio.platform.watchface.edit.photobackground.data.enity.WFBackgroundPhoto).\n Expected:\n" + ix017 + "\n Found:\n" + a18);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `complicationLastSetting`");
            writableDatabase.execSQL("DELETE FROM `watchAppLastSetting`");
            writableDatabase.execSQL("DELETE FROM `dianaPreset`");
            writableDatabase.execSQL("DELETE FROM `dianaRecommendPreset`");
            writableDatabase.execSQL("DELETE FROM `watch_face`");
            writableDatabase.execSQL("DELETE FROM `DianaComplicationRingStyle`");
            writableDatabase.execSQL("DELETE FROM `complication`");
            writableDatabase.execSQL("DELETE FROM `watchApp`");
            writableDatabase.execSQL("DELETE FROM `watchAppData`");
            writableDatabase.execSQL("DELETE FROM `DianaAppSetting`");
            writableDatabase.execSQL("DELETE FROM `dianaWatchfaceTemplate`");
            writableDatabase.execSQL("DELETE FROM `diana_watchface_preset`");
            writableDatabase.execSQL("DELETE FROM `diana_recommended_preset`");
            writableDatabase.execSQL("DELETE FROM `dianaWatchfaceUser`");
            writableDatabase.execSQL("DELETE FROM `dianaWatchFaceRing`");
            writableDatabase.execSQL("DELETE FROM `wf_asset`");
            writableDatabase.execSQL("DELETE FROM `wf_background_photo`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "complicationLastSetting", "watchAppLastSetting", "dianaPreset", "dianaRecommendPreset", "watch_face", "DianaComplicationRingStyle", "complication", "watchApp", "watchAppData", "DianaAppSetting", "dianaWatchfaceTemplate", "diana_watchface_preset", "diana_recommended_preset", "dianaWatchfaceUser", "dianaWatchFaceRing", "wf_asset", "wf_background_photo");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(16), "a21b3e763a4890918d43c35c227a4ff1", "988b4243d8c06cf5f9b451e6b05bb089");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public ComplicationDao getComplicationDao() {
        ComplicationDao complicationDao;
        if (this._complicationDao != null) {
            return this._complicationDao;
        }
        synchronized (this) {
            if (this._complicationDao == null) {
                this._complicationDao = new ComplicationDao_Impl(this);
            }
            complicationDao = this._complicationDao;
        }
        return complicationDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public ComplicationLastSettingDao getComplicationLastSettingDao() {
        ComplicationLastSettingDao complicationLastSettingDao;
        if (this._complicationLastSettingDao != null) {
            return this._complicationLastSettingDao;
        }
        synchronized (this) {
            if (this._complicationLastSettingDao == null) {
                this._complicationLastSettingDao = new ComplicationLastSettingDao_Impl(this);
            }
            complicationLastSettingDao = this._complicationLastSettingDao;
        }
        return complicationLastSettingDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public DianaAppSettingDao getDianaAppSettingDao() {
        DianaAppSettingDao dianaAppSettingDao;
        if (this._dianaAppSettingDao != null) {
            return this._dianaAppSettingDao;
        }
        synchronized (this) {
            if (this._dianaAppSettingDao == null) {
                this._dianaAppSettingDao = new DianaAppSettingDao_Impl(this);
            }
            dianaAppSettingDao = this._dianaAppSettingDao;
        }
        return dianaAppSettingDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public qo5 getDianaPresetDao() {
        qo5 qo5;
        if (this._dianaPresetDao_1 != null) {
            return this._dianaPresetDao_1;
        }
        synchronized (this) {
            if (this._dianaPresetDao_1 == null) {
                this._dianaPresetDao_1 = new ro5(this);
            }
            qo5 = this._dianaPresetDao_1;
        }
        return qo5;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public DianaWatchFaceRingDao getDianaWatchFaceRingDao() {
        DianaWatchFaceRingDao dianaWatchFaceRingDao;
        if (this._dianaWatchFaceRingDao != null) {
            return this._dianaWatchFaceRingDao;
        }
        synchronized (this) {
            if (this._dianaWatchFaceRingDao == null) {
                this._dianaWatchFaceRingDao = new DianaWatchFaceRingDao_Impl(this);
            }
            dianaWatchFaceRingDao = this._dianaWatchFaceRingDao;
        }
        return dianaWatchFaceRingDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public DianaWatchFaceTemplateDao getDianaWatchFaceTemplateDao() {
        DianaWatchFaceTemplateDao dianaWatchFaceTemplateDao;
        if (this._dianaWatchFaceTemplateDao != null) {
            return this._dianaWatchFaceTemplateDao;
        }
        synchronized (this) {
            if (this._dianaWatchFaceTemplateDao == null) {
                this._dianaWatchFaceTemplateDao = new DianaWatchFaceTemplateDao_Impl(this);
            }
            dianaWatchFaceTemplateDao = this._dianaWatchFaceTemplateDao;
        }
        return dianaWatchFaceTemplateDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public DianaWatchFaceUserDao getDianaWatchFaceUserDao() {
        DianaWatchFaceUserDao dianaWatchFaceUserDao;
        if (this._dianaWatchFaceUserDao != null) {
            return this._dianaWatchFaceUserDao;
        }
        synchronized (this) {
            if (this._dianaWatchFaceUserDao == null) {
                this._dianaWatchFaceUserDao = new DianaWatchFaceUserDao_Impl(this);
            }
            dianaWatchFaceUserDao = this._dianaWatchFaceUserDao;
        }
        return dianaWatchFaceUserDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public DianaPresetDao getPresetDao() {
        DianaPresetDao dianaPresetDao;
        if (this._dianaPresetDao != null) {
            return this._dianaPresetDao;
        }
        synchronized (this) {
            if (this._dianaPresetDao == null) {
                this._dianaPresetDao = new DianaPresetDao_Impl(this);
            }
            dianaPresetDao = this._dianaPresetDao;
        }
        return dianaPresetDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public wo5 getRecommendedPresetDao() {
        wo5 wo5;
        if (this._dianaRecommendedPresetDao != null) {
            return this._dianaRecommendedPresetDao;
        }
        synchronized (this) {
            if (this._dianaRecommendedPresetDao == null) {
                this._dianaRecommendedPresetDao = new xo5(this);
            }
            wo5 = this._dianaRecommendedPresetDao;
        }
        return wo5;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public RingStyleDao getRingStyleDao() {
        RingStyleDao ringStyleDao;
        if (this._ringStyleDao != null) {
            return this._ringStyleDao;
        }
        synchronized (this) {
            if (this._ringStyleDao == null) {
                this._ringStyleDao = new RingStyleDao_Impl(this);
            }
            ringStyleDao = this._ringStyleDao;
        }
        return ringStyleDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public h97 getWFBackgroundDao() {
        h97 h97;
        if (this._wFBackgroundPhotoDao != null) {
            return this._wFBackgroundPhotoDao;
        }
        synchronized (this) {
            if (this._wFBackgroundPhotoDao == null) {
                this._wFBackgroundPhotoDao = new i97(this);
            }
            h97 = this._wFBackgroundPhotoDao;
        }
        return h97;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public u77 getWFTemplateDao() {
        u77 u77;
        if (this._wFAssetsDao != null) {
            return this._wFAssetsDao;
        }
        synchronized (this) {
            if (this._wFAssetsDao == null) {
                this._wFAssetsDao = new v77(this);
            }
            u77 = this._wFAssetsDao;
        }
        return u77;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public WatchAppDao getWatchAppDao() {
        WatchAppDao watchAppDao;
        if (this._watchAppDao != null) {
            return this._watchAppDao;
        }
        synchronized (this) {
            if (this._watchAppDao == null) {
                this._watchAppDao = new WatchAppDao_Impl(this);
            }
            watchAppDao = this._watchAppDao;
        }
        return watchAppDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public WatchAppDataDao getWatchAppDataDao() {
        WatchAppDataDao watchAppDataDao;
        if (this._watchAppDataDao != null) {
            return this._watchAppDataDao;
        }
        synchronized (this) {
            if (this._watchAppDataDao == null) {
                this._watchAppDataDao = new WatchAppDataDao_Impl(this);
            }
            watchAppDataDao = this._watchAppDataDao;
        }
        return watchAppDataDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public WatchAppLastSettingDao getWatchAppSettingDao() {
        WatchAppLastSettingDao watchAppLastSettingDao;
        if (this._watchAppLastSettingDao != null) {
            return this._watchAppLastSettingDao;
        }
        synchronized (this) {
            if (this._watchAppLastSettingDao == null) {
                this._watchAppLastSettingDao = new WatchAppLastSettingDao_Impl(this);
            }
            watchAppLastSettingDao = this._watchAppLastSettingDao;
        }
        return watchAppLastSettingDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public WatchFaceDao getWatchFaceDao() {
        WatchFaceDao watchFaceDao;
        if (this._watchFaceDao != null) {
            return this._watchFaceDao;
        }
        synchronized (this) {
            if (this._watchFaceDao == null) {
                this._watchFaceDao = new WatchFaceDao_Impl(this);
            }
            watchFaceDao = this._watchFaceDao;
        }
        return watchFaceDao;
    }
}
