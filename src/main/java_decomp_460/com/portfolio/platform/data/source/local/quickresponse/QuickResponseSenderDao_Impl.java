package com.portfolio.platform.data.source.local.quickresponse;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.QuickResponseSender;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseSenderDao_Impl extends QuickResponseSenderDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<QuickResponseSender> __insertionAdapterOfQuickResponseSender;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfRemoveById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<QuickResponseSender> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, QuickResponseSender quickResponseSender) {
            px0.bindLong(1, (long) quickResponseSender.getId());
            if (quickResponseSender.getContent() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, quickResponseSender.getContent());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `quickResponseSender` (`id`,`content`) VALUES (nullif(?, 0),?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM quickResponseSender WHERE id = ?";
        }
    }

    @DexIgnore
    public QuickResponseSenderDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfQuickResponseSender = new Anon1(qw0);
        this.__preparedStmtOfRemoveById = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao
    public List<QuickResponseSender> getAllQuickResponseSender() {
        tw0 f = tw0.f("SELECT * FROM quickResponseSender", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "content");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                QuickResponseSender quickResponseSender = new QuickResponseSender(b.getString(c2));
                quickResponseSender.setId(b.getInt(c));
                arrayList.add(quickResponseSender);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao
    public QuickResponseSender getQuickResponseSenderById(int i) {
        QuickResponseSender quickResponseSender = null;
        tw0 f = tw0.f("SELECT * FROM quickResponseSender WHERE id = ?", 1);
        f.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "content");
            if (b.moveToFirst()) {
                quickResponseSender = new QuickResponseSender(b.getString(c2));
                quickResponseSender.setId(b.getInt(c));
            }
            return quickResponseSender;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao
    public long insertQuickResponseSender(QuickResponseSender quickResponseSender) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfQuickResponseSender.insertAndReturnId(quickResponseSender);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao
    public void removeById(int i) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfRemoveById.acquire();
        acquire.bindLong(1, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveById.release(acquire);
        }
    }
}
