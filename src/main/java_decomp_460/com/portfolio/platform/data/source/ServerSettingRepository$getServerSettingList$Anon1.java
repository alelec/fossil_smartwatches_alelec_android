package com.portfolio.platform.data.source;

import com.fossil.bw7;
import com.fossil.gu7;
import com.fossil.jv7;
import com.fossil.xw7;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerSettingRepository$getServerSettingList$Anon1 implements ServerSettingDataSource.OnGetServerSettingList {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingDataSource.OnGetServerSettingList $callback;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRepository this$0;

    @DexIgnore
    public ServerSettingRepository$getServerSettingList$Anon1(ServerSettingRepository serverSettingRepository, ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        this.this$0 = serverSettingRepository;
        this.$callback = onGetServerSettingList;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
    public void onFailed(int i) {
        this.$callback.onFailed(i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
    public void onSuccess(ServerSettingList serverSettingList) {
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2(this, serverSettingList, null), 3, null);
        this.$callback.onSuccess(serverSettingList);
    }
}
