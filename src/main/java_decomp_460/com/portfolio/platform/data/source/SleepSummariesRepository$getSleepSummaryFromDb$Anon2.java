package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaryFromDb$2", f = "SleepSummariesRepository.kt", l = {58}, m = "invokeSuspend")
public final class SleepSummariesRepository$getSleepSummaryFromDb$Anon2 extends ko7 implements vp7<iv7, qn7<? super MFSleepDay>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepSummaryFromDb$Anon2(Date date, qn7 qn7) {
        super(2, qn7);
        this.$date = date;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SleepSummariesRepository$getSleepSummaryFromDb$Anon2 sleepSummariesRepository$getSleepSummaryFromDb$Anon2 = new SleepSummariesRepository$getSleepSummaryFromDb$Anon2(this.$date, qn7);
        sleepSummariesRepository$getSleepSummaryFromDb$Anon2.p$ = (iv7) obj;
        return sleepSummariesRepository$getSleepSummaryFromDb$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super MFSleepDay> qn7) {
        return ((SleepSummariesRepository$getSleepSummaryFromDb$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object D;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            try {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = SleepSummariesRepository.TAG;
                local.d(str, "getSummary: calendar = " + this.$date);
            } catch (Exception e) {
                e.printStackTrace();
            }
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.label = 1;
            D = bn5.D(this);
            if (D == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            D = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return ((SleepDatabase) D).sleepDao().getSleepDay(this.$date);
    }
}
