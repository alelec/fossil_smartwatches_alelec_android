package com.portfolio.platform.data.source.local.reminders;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InactivityNudgeTimeDao_Impl implements InactivityNudgeTimeDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<InactivityNudgeTimeModel> __insertionAdapterOfInactivityNudgeTimeModel;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<InactivityNudgeTimeModel> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, InactivityNudgeTimeModel inactivityNudgeTimeModel) {
            if (inactivityNudgeTimeModel.getNudgeTimeName() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, inactivityNudgeTimeModel.getNudgeTimeName());
            }
            px0.bindLong(2, (long) inactivityNudgeTimeModel.getMinutes());
            px0.bindLong(3, (long) inactivityNudgeTimeModel.getNudgeTimeType());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `inactivityNudgeTimeModel` (`nudgeTimeName`,`minutes`,`nudgeTimeType`) VALUES (?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM inactivityNudgeTimeModel";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<InactivityNudgeTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon3(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<InactivityNudgeTimeModel> call() throws Exception {
            Cursor b = ex0.b(InactivityNudgeTimeDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "nudgeTimeName");
                int c2 = dx0.c(b, "minutes");
                int c3 = dx0.c(b, "nudgeTimeType");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new InactivityNudgeTimeModel(b.getString(c), b.getInt(c2), b.getInt(c3)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<InactivityNudgeTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon4(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public InactivityNudgeTimeModel call() throws Exception {
            InactivityNudgeTimeModel inactivityNudgeTimeModel = null;
            Cursor b = ex0.b(InactivityNudgeTimeDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "nudgeTimeName");
                int c2 = dx0.c(b, "minutes");
                int c3 = dx0.c(b, "nudgeTimeType");
                if (b.moveToFirst()) {
                    inactivityNudgeTimeModel = new InactivityNudgeTimeModel(b.getString(c), b.getInt(c2), b.getInt(c3));
                }
                return inactivityNudgeTimeModel;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public InactivityNudgeTimeDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfInactivityNudgeTimeModel = new Anon1(qw0);
        this.__preparedStmtOfDelete = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao
    public InactivityNudgeTimeModel getInactivityNudgeTimeModelWithFieldNudgeTimeType(int i) {
        InactivityNudgeTimeModel inactivityNudgeTimeModel = null;
        tw0 f = tw0.f("SELECT * FROM inactivityNudgeTimeModel WHERE nudgeTimeType = ?", 1);
        f.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "nudgeTimeName");
            int c2 = dx0.c(b, "minutes");
            int c3 = dx0.c(b, "nudgeTimeType");
            if (b.moveToFirst()) {
                inactivityNudgeTimeModel = new InactivityNudgeTimeModel(b.getString(c), b.getInt(c2), b.getInt(c3));
            }
            return inactivityNudgeTimeModel;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao
    public LiveData<InactivityNudgeTimeModel> getInactivityNudgeTimeWithFieldNudgeTimeType(int i) {
        tw0 f = tw0.f("SELECT * FROM inactivityNudgeTimeModel WHERE nudgeTimeType = ?", 1);
        f.bindLong(1, (long) i);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"inactivityNudgeTimeModel"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao
    public LiveData<List<InactivityNudgeTimeModel>> getListInactivityNudgeTime() {
        tw0 f = tw0.f("SELECT * FROM inactivityNudgeTimeModel", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"inactivityNudgeTimeModel"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao
    public List<InactivityNudgeTimeModel> getListInactivityNudgeTimeModel() {
        tw0 f = tw0.f("SELECT * FROM inactivityNudgeTimeModel", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "nudgeTimeName");
            int c2 = dx0.c(b, "minutes");
            int c3 = dx0.c(b, "nudgeTimeType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new InactivityNudgeTimeModel(b.getString(c), b.getInt(c2), b.getInt(c3)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao
    public void upsertInactivityNudgeTime(InactivityNudgeTimeModel inactivityNudgeTimeModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfInactivityNudgeTimeModel.insert((jw0<InactivityNudgeTimeModel>) inactivityNudgeTimeModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao
    public void upsertListInactivityNudgeTime(List<InactivityNudgeTimeModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfInactivityNudgeTimeModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
