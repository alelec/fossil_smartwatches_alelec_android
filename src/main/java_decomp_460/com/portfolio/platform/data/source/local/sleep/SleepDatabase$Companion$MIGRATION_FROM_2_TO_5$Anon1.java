package com.portfolio.platform.data.source.local.sleep;

import com.fossil.ax0;
import com.fossil.lx0;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDatabase$Companion$MIGRATION_FROM_2_TO_5$Anon1 extends ax0 {
    @DexIgnore
    public SleepDatabase$Companion$MIGRATION_FROM_2_TO_5$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.ax0
    public void migrate(lx0 lx0) {
        pq7.c(lx0, "database");
        FLogger.INSTANCE.getLocal().d(SleepDatabase.TAG, "Migration 2 to 5 start");
        lx0.beginTransaction();
        try {
            FLogger.INSTANCE.getLocal().d(SleepDatabase.TAG, "Migrate sleep session table");
            lx0.execSQL("CREATE TABLE sleep_session_new (date INTEGER NOT NULL, day TEXT NOT NULL, deviceSerialNumber TEXT, syncTime INTEGER, bookmarkTime INTEGER, normalizedSleepQuality REAL NOT NULL DEFAULT 0, source INTEGER NOT NULL DEFAULT 0, realStartTime INTEGER NOT NULL DEFAULT 0, realEndTime INTEGER PRIMARY KEY NOT NULL DEFAULT 0, realSleepMinutes INTEGER NOT NULL DEFAULT 0, realSleepStateDistInMinute TEXT NOT NULL DEFAULT '{\"awake\":0,\"deep\":0,\"light\":0}', editedStartTime INTEGER, editedEndTime INTEGER, editedSleepMinutes INTEGER, editedSleepStateDistInMinute TEXT, sleepStates TEXT NOT NULL DEFAULT '', createdAt INTEGER NOT NULL DEFAULT 0, updatedAt INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 0, timezoneOffset INTEGER NOT NULL DEFAULT 0)");
            lx0.execSQL("INSERT INTO sleep_session_new (date, day, deviceSerialNumber, syncTime, bookmarkTime, normalizedSleepQuality, source, realStartTime, realEndTime, realSleepMinutes, realSleepMinutes, realSleepStateDistInMinute, editedStartTime, editedEndTime, editedSleepMinutes, editedSleepStateDistInMinute, sleepStates, createdAt, updatedAt, timezoneOffset) SELECT date, day, deviceSerialNumber, syncTime, bookmarkTime, normalizedSleepQuality, source, realStartTime, realEndTime, realSleepMinutes, realSleepMinutes, realSleepStateDistInMinute, editedStartTime, editedEndTime, editedSleepMinutes, editedSleepStateDistInMinute, sleepStates, createdAt, updatedAt, timezoneOffset  FROM sleep_session");
            lx0.execSQL("DROP TABLE sleep_session");
            lx0.execSQL("ALTER TABLE sleep_session_new RENAME TO sleep_session");
            FLogger.INSTANCE.getLocal().d(SleepDatabase.TAG, "Migrate sleep session table success");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SleepDatabase.TAG, "Migrate sleep session table fail " + e);
            lx0.execSQL("DROP TABLE IF EXISTS sleep_session_new");
            lx0.execSQL("DROP TABLE IF EXISTS sleep_session");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS sleep_session (date INTEGER NOT NULL, day TEXT NOT NULL, deviceSerialNumber TEXT, syncTime INTEGER, bookmarkTime INTEGER, normalizedSleepQuality REAL NOT NULL DEFAULT 0, source INTEGER NOT NULL DEFAULT 0, realStartTime INTEGER NOT NULL DEFAULT 0, realEndTime INTEGER PRIMARY KEY NOT NULL DEFAULT 0, realSleepMinutes INTEGER NOT NULL DEFAULT 0, realSleepStateDistInMinute TEXT NOT NULL DEFAULT '{\"awake\":0,\"deep\":0,\"light\":0}', editedStartTime INTEGER, editedEndTime INTEGER, editedSleepMinutes INTEGER, editedSleepStateDistInMinute TEXT, sleepStates TEXT NOT NULL DEFAULT '', createdAt INTEGER NOT NULL DEFAULT 0, updatedAt INTEGER, pinType INTEGER NOT NULL DEFAULT 1, timezoneOffset INTEGER NOT NULL DEFAULT 0)");
        }
        try {
            FLogger.INSTANCE.getLocal().d(SleepDatabase.TAG, "Migrate sleep date table");
            lx0.execSQL("CREATE TABLE sleep_date_new (date TEXT PRIMARY KEY NOT NULL, goalMinutes INTEGER NOT NULL DEFAULT 0, sleepMinutes INTEGER NOT NULL DEFAULT 0, sleepStateDistInMinute TEXT, createdAt INTEGER, updatedAt INTEGER, pinType INTEGER NOT NULL DEFAULT 0, timezoneOffset INTEGER NOT NULL DEFAULT 0)");
            lx0.execSQL("INSERT INTO sleep_date_new (date, goalMinutes, sleepMinutes, sleepStateDistInMinute, createdAt, updatedAt, timezoneOffset) SELECT date, goalMinutes, sleepMinutes, sleepStateDistInMinute, createdAt, updatedAt, timezoneOffset  FROM sleep_date");
            lx0.execSQL("DROP TABLE sleep_date");
            lx0.execSQL("ALTER TABLE sleep_date_new RENAME TO sleep_date");
            FLogger.INSTANCE.getLocal().d(SleepDatabase.TAG, "Migrate sleep date table success");
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(SleepDatabase.TAG, "Migrate sleep date table fail " + e2);
            lx0.execSQL("DROP TABLE IF EXISTS sleep_date_new");
            lx0.execSQL("DROP TABLE IF EXISTS sleep_date");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS sleep_date (date TEXT PRIMARY KEY NOT NULL, goalMinutes INTEGER NOT NULL DEFAULT 0, sleepMinutes INTEGER NOT NULL DEFAULT 0, sleepStateDistInMinute TEXT, createdAt INTEGER, updatedAt INTEGER, pinType INTEGER NOT NULL DEFAULT 0, timezoneOffset INTEGER NOT NULL DEFAULT 0)");
        }
        try {
            FLogger.INSTANCE.getLocal().d(SleepDatabase.TAG, "Migrate sleep setting");
            lx0.execSQL("CREATE TABLE sleep_settings (sleepGoal INTEGER NOT NULL DEFAULT 480, id INTEGER PRIMARY KEY ASC NOT NULL)");
            lx0.execSQL("INSERT INTO sleep_settings (sleepGoal) SELECT minute FROM sleep_goal ORDER BY date DESC LIMIT 1");
            lx0.execSQL("DROP TABLE sleep_goal");
            FLogger.INSTANCE.getLocal().d(SleepDatabase.TAG, "Migrate sleep setting success");
        } catch (Exception e3) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS sleep_settings (sleepGoal INTEGER NOT NULL DEFAULT 480, id INTEGER PRIMARY KEY ASC NOT NULL)");
            lx0.execSQL("DROP TABLE IF EXISTS sleep_goal");
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d(SleepDatabase.TAG, "Migrate sleep setting fail " + e3);
        }
        lx0.execSQL("CREATE TABLE sleepRecommendedGoals (recommendedSleepGoal INTEGER NOT NULL, id INTEGER PRIMARY KEY ASC NOT NULL)");
        try {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS sleep_statistic (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, sleepMinutesBestDay TEXT, sleepTimeBestStreak TEXT, totalDays INTEGER NOT NULL, totalSleeps REAL NOT NULL, totalSleepMinutes INTEGER NOT NULL, totalSleepStateDistInMinute TEXT NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
        } catch (Exception e4) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.e(SleepDatabase.TAG, "MIGRATION_FROM_2_TO_5 - SleepStatistic -- e=" + e4);
            e4.printStackTrace();
        }
        lx0.setTransactionSuccessful();
        lx0.endTransaction();
    }
}
