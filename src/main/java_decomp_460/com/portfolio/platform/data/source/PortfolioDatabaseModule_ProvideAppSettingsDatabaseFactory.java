package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.AppSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideAppSettingsDatabaseFactory implements Factory<AppSettingsDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideAppSettingsDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideAppSettingsDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideAppSettingsDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static AppSettingsDatabase provideAppSettingsDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        AppSettingsDatabase provideAppSettingsDatabase = portfolioDatabaseModule.provideAppSettingsDatabase(portfolioApp);
        lk7.c(provideAppSettingsDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideAppSettingsDatabase;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public AppSettingsDatabase get() {
        return provideAppSettingsDatabase(this.module, this.appProvider.get());
    }
}
