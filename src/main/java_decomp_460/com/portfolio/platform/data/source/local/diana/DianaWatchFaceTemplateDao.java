package com.portfolio.platform.data.source.local.diana;

import com.portfolio.platform.data.model.watchface.DianaWatchFaceTemplate;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface DianaWatchFaceTemplateDao {
    @DexIgnore
    void deleteDianaWatchFaceTemplate(DianaWatchFaceTemplate dianaWatchFaceTemplate);

    @DexIgnore
    List<DianaWatchFaceTemplate> getAllDianaWatchFaceTemplate();

    @DexIgnore
    DianaWatchFaceTemplate getDianaWatchFaceTemplateById(String str);

    @DexIgnore
    void upsertDianaWatchFaceTemplate(DianaWatchFaceTemplate dianaWatchFaceTemplate);
}
