package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.fossil.lr4;
import com.fossil.nr4;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory implements Factory<nr4> {
    @DexIgnore
    public /* final */ Provider<lr4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<lr4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<lr4> provider) {
        return new PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static nr4 providesFlagLocalSource(PortfolioDatabaseModule portfolioDatabaseModule, lr4 lr4) {
        nr4 providesFlagLocalSource = portfolioDatabaseModule.providesFlagLocalSource(lr4);
        lk7.c(providesFlagLocalSource, "Cannot return null from a non-@Nullable @Provides method");
        return providesFlagLocalSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public nr4 get() {
        return providesFlagLocalSource(this.module, this.daoProvider.get());
    }
}
