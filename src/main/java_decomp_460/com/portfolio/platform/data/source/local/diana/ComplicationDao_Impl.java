package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.fw0;
import com.fossil.g78;
import com.fossil.hx0;
import com.fossil.jw0;
import com.fossil.m05;
import com.fossil.px0;
import com.fossil.qn7;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.diana.Complication;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationDao_Impl implements ComplicationDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<Complication> __insertionAdapterOfComplication;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAll;
    @DexIgnore
    public /* final */ m05 __stringArrayConverter; // = new m05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<Complication> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, Complication complication) {
            if (complication.getComplicationId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, complication.getComplicationId());
            }
            if (complication.getName() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, complication.getName());
            }
            if (complication.getNameKey() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, complication.getNameKey());
            }
            String b = ComplicationDao_Impl.this.__stringArrayConverter.b(complication.getCategories());
            if (b == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, b);
            }
            if (complication.getDescription() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, complication.getDescription());
            }
            if (complication.getDescriptionKey() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, complication.getDescriptionKey());
            }
            if (complication.getIcon() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, complication.getIcon());
            }
            if (complication.getCreatedAt() == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, complication.getCreatedAt());
            }
            if (complication.getUpdatedAt() == null) {
                px0.bindNull(9);
            } else {
                px0.bindString(9, complication.getUpdatedAt());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `complication` (`complicationId`,`name`,`nameKey`,`categories`,`description`,`descriptionKey`,`icon`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM complication";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<Complication>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon3(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<Complication> call() throws Exception {
            Cursor b = ex0.b(ComplicationDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "complicationId");
                int c2 = dx0.c(b, "name");
                int c3 = dx0.c(b, "nameKey");
                int c4 = dx0.c(b, "categories");
                int c5 = dx0.c(b, "description");
                int c6 = dx0.c(b, "descriptionKey");
                int c7 = dx0.c(b, "icon");
                int c8 = dx0.c(b, "createdAt");
                int c9 = dx0.c(b, "updatedAt");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new Complication(b.getString(c), b.getString(c2), b.getString(c3), ComplicationDao_Impl.this.__stringArrayConverter.a(b.getString(c4)), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8), b.getString(c9)));
                }
                return arrayList;
            } finally {
                b.close();
                this.val$_statement.m();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<Complication> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon4(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Complication call() throws Exception {
            Complication complication = null;
            Cursor b = ex0.b(ComplicationDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "complicationId");
                int c2 = dx0.c(b, "name");
                int c3 = dx0.c(b, "nameKey");
                int c4 = dx0.c(b, "categories");
                int c5 = dx0.c(b, "description");
                int c6 = dx0.c(b, "descriptionKey");
                int c7 = dx0.c(b, "icon");
                int c8 = dx0.c(b, "createdAt");
                int c9 = dx0.c(b, "updatedAt");
                if (b.moveToFirst()) {
                    complication = new Complication(b.getString(c), b.getString(c2), b.getString(c3), ComplicationDao_Impl.this.__stringArrayConverter.a(b.getString(c4)), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8), b.getString(c9));
                }
                return complication;
            } finally {
                b.close();
                this.val$_statement.m();
            }
        }
    }

    @DexIgnore
    public ComplicationDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfComplication = new Anon1(qw0);
        this.__preparedStmtOfClearAll = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationDao
    public Object getAllComplications(qn7<? super List<Complication>> qn7) {
        return fw0.a(this.__db, false, new Anon3(tw0.f("SELECT * FROM complication WHERE complicationId != 'empty'", 0)), qn7);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationDao
    public Object getComplicationById(String str, qn7<? super Complication> qn7) {
        tw0 f = tw0.f("SELECT * FROM complication WHERE complicationId=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        return fw0.a(this.__db, false, new Anon4(f), qn7);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationDao
    public List<Complication> getComplicationByIds(List<String> list) {
        StringBuilder b = hx0.b();
        b.append("SELECT ");
        b.append(g78.ANY_MARKER);
        b.append(" FROM complication WHERE complicationId IN (");
        int size = list.size();
        hx0.a(b, size);
        b.append(")");
        tw0 f = tw0.f(b.toString(), size + 0);
        int i = 1;
        for (String str : list) {
            if (str == null) {
                f.bindNull(i);
            } else {
                f.bindString(i, str);
            }
            i++;
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b2, "complicationId");
            int c2 = dx0.c(b2, "name");
            int c3 = dx0.c(b2, "nameKey");
            int c4 = dx0.c(b2, "categories");
            int c5 = dx0.c(b2, "description");
            int c6 = dx0.c(b2, "descriptionKey");
            int c7 = dx0.c(b2, "icon");
            int c8 = dx0.c(b2, "createdAt");
            int c9 = dx0.c(b2, "updatedAt");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new Complication(b2.getString(c), b2.getString(c2), b2.getString(c3), this.__stringArrayConverter.a(b2.getString(c4)), b2.getString(c5), b2.getString(c6), b2.getString(c7), b2.getString(c8), b2.getString(c9)));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationDao
    public List<Complication> queryComplicationByName(String str) {
        tw0 f = tw0.f("SELECT * FROM complication WHERE name LIKE '%' || ? || '%'", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "complicationId");
            int c2 = dx0.c(b, "name");
            int c3 = dx0.c(b, "nameKey");
            int c4 = dx0.c(b, "categories");
            int c5 = dx0.c(b, "description");
            int c6 = dx0.c(b, "descriptionKey");
            int c7 = dx0.c(b, "icon");
            int c8 = dx0.c(b, "createdAt");
            int c9 = dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new Complication(b.getString(c), b.getString(c2), b.getString(c3), this.__stringArrayConverter.a(b.getString(c4)), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8), b.getString(c9)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationDao
    public void upsertComplicationList(List<Complication> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfComplication.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
