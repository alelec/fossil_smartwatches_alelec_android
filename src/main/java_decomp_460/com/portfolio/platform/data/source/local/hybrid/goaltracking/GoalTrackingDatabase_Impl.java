package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDatabase_Impl extends GoalTrackingDatabase {
    @DexIgnore
    public volatile GoalTrackingDao _goalTrackingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `goalTrackingRaw` (`pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `trackedAt` TEXT NOT NULL, `timezoneOffsetInSecond` INTEGER NOT NULL, `date` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `goalTrackingDay` (`pinType` INTEGER NOT NULL, `date` TEXT NOT NULL, `totalTracked` INTEGER NOT NULL, `goalTarget` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`date`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `goalSetting` (`id` INTEGER NOT NULL, `currentTarget` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'ac5e072da94cafa67a74f62806ddb95f')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `goalTrackingRaw`");
            lx0.execSQL("DROP TABLE IF EXISTS `goalTrackingDay`");
            lx0.execSQL("DROP TABLE IF EXISTS `goalSetting`");
            if (GoalTrackingDatabase_Impl.this.mCallbacks != null) {
                int size = GoalTrackingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) GoalTrackingDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (GoalTrackingDatabase_Impl.this.mCallbacks != null) {
                int size = GoalTrackingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) GoalTrackingDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            GoalTrackingDatabase_Impl.this.mDatabase = lx0;
            GoalTrackingDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (GoalTrackingDatabase_Impl.this.mCallbacks != null) {
                int size = GoalTrackingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) GoalTrackingDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(7);
            hashMap.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap.put(GoalTrackingEvent.COLUMN_TRACKED_AT, new ix0.a(GoalTrackingEvent.COLUMN_TRACKED_AT, "TEXT", true, 0, null, 1));
            hashMap.put("timezoneOffsetInSecond", new ix0.a("timezoneOffsetInSecond", "INTEGER", true, 0, null, 1));
            hashMap.put("date", new ix0.a("date", "TEXT", true, 0, null, 1));
            hashMap.put("createdAt", new ix0.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap.put("updatedAt", new ix0.a("updatedAt", "INTEGER", true, 0, null, 1));
            ix0 ix0 = new ix0("goalTrackingRaw", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "goalTrackingRaw");
            if (!ix0.equals(a2)) {
                return new sw0.b(false, "goalTrackingRaw(com.portfolio.platform.data.model.goaltracking.GoalTrackingData).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(6);
            hashMap2.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap2.put("date", new ix0.a("date", "TEXT", true, 1, null, 1));
            hashMap2.put("totalTracked", new ix0.a("totalTracked", "INTEGER", true, 0, null, 1));
            hashMap2.put("goalTarget", new ix0.a("goalTarget", "INTEGER", true, 0, null, 1));
            hashMap2.put("createdAt", new ix0.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap2.put("updatedAt", new ix0.a("updatedAt", "INTEGER", true, 0, null, 1));
            ix0 ix02 = new ix0("goalTrackingDay", hashMap2, new HashSet(0), new HashSet(0));
            ix0 a3 = ix0.a(lx0, "goalTrackingDay");
            if (!ix02.equals(a3)) {
                return new sw0.b(false, "goalTrackingDay(com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
            }
            HashMap hashMap3 = new HashMap(2);
            hashMap3.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap3.put("currentTarget", new ix0.a("currentTarget", "INTEGER", true, 0, null, 1));
            ix0 ix03 = new ix0("goalSetting", hashMap3, new HashSet(0), new HashSet(0));
            ix0 a4 = ix0.a(lx0, "goalSetting");
            if (ix03.equals(a4)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "goalSetting(com.portfolio.platform.data.model.GoalSetting).\n Expected:\n" + ix03 + "\n Found:\n" + a4);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `goalTrackingRaw`");
            writableDatabase.execSQL("DELETE FROM `goalTrackingDay`");
            writableDatabase.execSQL("DELETE FROM `goalSetting`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "goalTrackingRaw", "goalTrackingDay", "goalSetting");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(2), "ac5e072da94cafa67a74f62806ddb95f", "56ec29011feb0e074f1d2ba7ecbcbb17");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase
    public GoalTrackingDao getGoalTrackingDao() {
        GoalTrackingDao goalTrackingDao;
        if (this._goalTrackingDao != null) {
            return this._goalTrackingDao;
        }
        synchronized (this) {
            if (this._goalTrackingDao == null) {
                this._goalTrackingDao = new GoalTrackingDao_Impl(this);
            }
            goalTrackingDao = this._goalTrackingDao;
        }
        return goalTrackingDao;
    }
}
