package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.DeviceRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceRepository_Factory implements Factory<DeviceRepository> {
    @DexIgnore
    public /* final */ Provider<DeviceDao> mDeviceDaoProvider;
    @DexIgnore
    public /* final */ Provider<DeviceRemoteDataSource> mDeviceRemoteDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<SkuDao> mSkuDaoProvider;

    @DexIgnore
    public DeviceRepository_Factory(Provider<DeviceDao> provider, Provider<SkuDao> provider2, Provider<DeviceRemoteDataSource> provider3) {
        this.mDeviceDaoProvider = provider;
        this.mSkuDaoProvider = provider2;
        this.mDeviceRemoteDataSourceProvider = provider3;
    }

    @DexIgnore
    public static DeviceRepository_Factory create(Provider<DeviceDao> provider, Provider<SkuDao> provider2, Provider<DeviceRemoteDataSource> provider3) {
        return new DeviceRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static DeviceRepository newInstance(DeviceDao deviceDao, SkuDao skuDao, DeviceRemoteDataSource deviceRemoteDataSource) {
        return new DeviceRepository(deviceDao, skuDao, deviceRemoteDataSource);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public DeviceRepository get() {
        return newInstance(this.mDeviceDaoProvider.get(), this.mSkuDaoProvider.get(), this.mDeviceRemoteDataSourceProvider.get());
    }
}
