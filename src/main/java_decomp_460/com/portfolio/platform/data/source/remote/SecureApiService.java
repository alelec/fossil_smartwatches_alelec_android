package com.portfolio.platform.data.source.remote;

import com.fossil.gj4;
import com.fossil.i98;
import com.fossil.mo5;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.s98;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface SecureApiService {
    @DexIgnore
    @s98("users/me/diana-faces")
    Object createWatchFace(@i98 gj4 gj4, qn7<? super q88<ApiResponse<DianaWatchFaceUser>>> qn7);

    @DexIgnore
    @s98("users/me/diana-watch-face-presets")
    Object upsertPresets(@i98 gj4 gj4, qn7<? super q88<ApiResponse<mo5>>> qn7);
}
