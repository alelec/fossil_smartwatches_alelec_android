package com.portfolio.platform.data.source.local.diana.workout;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.facebook.places.PlaceManager;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.q05;
import com.fossil.qw0;
import com.fossil.qz4;
import com.fossil.sz4;
import com.fossil.tw0;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.fossil.xw0;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDao_Impl implements WorkoutDao {
    @DexIgnore
    public /* final */ qz4 __dateShortStringConverter; // = new qz4();
    @DexIgnore
    public /* final */ sz4 __dateTimeISOStringConverter; // = new sz4();
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<WorkoutSession> __insertionAdapterOfWorkoutSession;
    @DexIgnore
    public /* final */ jw0<WorkoutSession> __insertionAdapterOfWorkoutSession_1;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteAllWorkoutSession;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteWorkoutSessionById;
    @DexIgnore
    public /* final */ q05 __workoutTypeConverter; // = new q05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<WorkoutSession> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, WorkoutSession workoutSession) {
            String i = WorkoutDao_Impl.this.__workoutTypeConverter.i(workoutSession.getSpeed());
            if (i == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, i);
            }
            if (workoutSession.getScreenShotUri() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, workoutSession.getScreenShotUri());
            }
            String j = WorkoutDao_Impl.this.__workoutTypeConverter.j(workoutSession.getStates());
            if (j == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, j);
            }
            if (workoutSession.getId() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, workoutSession.getId());
            }
            String a2 = WorkoutDao_Impl.this.__dateShortStringConverter.a(workoutSession.getDate());
            if (a2 == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, a2);
            }
            String a3 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getStartTime());
            if (a3 == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, a3);
            }
            String a4 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getEndTime());
            if (a4 == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, a4);
            }
            if (workoutSession.getDeviceSerialNumber() == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, workoutSession.getDeviceSerialNumber());
            }
            String k = WorkoutDao_Impl.this.__workoutTypeConverter.k(workoutSession.getStep());
            if (k == null) {
                px0.bindNull(9);
            } else {
                px0.bindString(9, k);
            }
            String c = WorkoutDao_Impl.this.__workoutTypeConverter.c(workoutSession.getCalorie());
            if (c == null) {
                px0.bindNull(10);
            } else {
                px0.bindString(10, c);
            }
            String d = WorkoutDao_Impl.this.__workoutTypeConverter.d(workoutSession.getDistance());
            if (d == null) {
                px0.bindNull(11);
            } else {
                px0.bindString(11, d);
            }
            String e = WorkoutDao_Impl.this.__workoutTypeConverter.e(workoutSession.getHeartRate());
            if (e == null) {
                px0.bindNull(12);
            } else {
                px0.bindString(12, e);
            }
            String h = WorkoutDao_Impl.this.__workoutTypeConverter.h(workoutSession.getSourceType());
            if (h == null) {
                px0.bindNull(13);
            } else {
                px0.bindString(13, h);
            }
            String l = WorkoutDao_Impl.this.__workoutTypeConverter.l(workoutSession.getWorkoutType());
            if (l == null) {
                px0.bindNull(14);
            } else {
                px0.bindString(14, l);
            }
            px0.bindLong(15, (long) workoutSession.getTimezoneOffsetInSecond());
            px0.bindLong(16, (long) workoutSession.getDuration());
            px0.bindLong(17, workoutSession.getCreatedAt());
            px0.bindLong(18, workoutSession.getUpdatedAt());
            String a5 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getWorkoutGpsPoints());
            if (a5 == null) {
                px0.bindNull(19);
            } else {
                px0.bindString(19, a5);
            }
            if (workoutSession.getGpsDataPoints() == null) {
                px0.bindNull(20);
            } else {
                px0.bindString(20, workoutSession.getGpsDataPoints());
            }
            String f = WorkoutDao_Impl.this.__workoutTypeConverter.f(workoutSession.getMode());
            if (f == null) {
                px0.bindNull(21);
            } else {
                px0.bindString(21, f);
            }
            String g = WorkoutDao_Impl.this.__workoutTypeConverter.g(workoutSession.getPace());
            if (g == null) {
                px0.bindNull(22);
            } else {
                px0.bindString(22, g);
            }
            String b = WorkoutDao_Impl.this.__workoutTypeConverter.b(workoutSession.getCadence());
            if (b == null) {
                px0.bindNull(23);
            } else {
                px0.bindString(23, b);
            }
            String a6 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getEditedStartTime());
            if (a6 == null) {
                px0.bindNull(24);
            } else {
                px0.bindString(24, a6);
            }
            String a7 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getEditedEndTime());
            if (a7 == null) {
                px0.bindNull(25);
            } else {
                px0.bindString(25, a7);
            }
            String l2 = WorkoutDao_Impl.this.__workoutTypeConverter.l(workoutSession.getEditedType());
            if (l2 == null) {
                px0.bindNull(26);
            } else {
                px0.bindString(26, l2);
            }
            String f2 = WorkoutDao_Impl.this.__workoutTypeConverter.f(workoutSession.getEditedMode());
            if (f2 == null) {
                px0.bindNull(27);
            } else {
                px0.bindString(27, f2);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR IGNORE INTO `workout_session` (`speed`,`screenShotUri`,`states`,`id`,`date`,`startTime`,`endTime`,`deviceSerialNumber`,`step`,`calorie`,`distance`,`heartRate`,`sourceType`,`workoutType`,`timezoneOffset`,`duration`,`createdAt`,`updatedAt`,`workoutGpsPoints`,`gpsDataPoints`,`mode`,`pace`,`cadence`,`editedStartTime`,`editedEndTime`,`editedType`,`editedMode`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends jw0<WorkoutSession> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, WorkoutSession workoutSession) {
            String i = WorkoutDao_Impl.this.__workoutTypeConverter.i(workoutSession.getSpeed());
            if (i == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, i);
            }
            if (workoutSession.getScreenShotUri() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, workoutSession.getScreenShotUri());
            }
            String j = WorkoutDao_Impl.this.__workoutTypeConverter.j(workoutSession.getStates());
            if (j == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, j);
            }
            if (workoutSession.getId() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, workoutSession.getId());
            }
            String a2 = WorkoutDao_Impl.this.__dateShortStringConverter.a(workoutSession.getDate());
            if (a2 == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, a2);
            }
            String a3 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getStartTime());
            if (a3 == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, a3);
            }
            String a4 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getEndTime());
            if (a4 == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, a4);
            }
            if (workoutSession.getDeviceSerialNumber() == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, workoutSession.getDeviceSerialNumber());
            }
            String k = WorkoutDao_Impl.this.__workoutTypeConverter.k(workoutSession.getStep());
            if (k == null) {
                px0.bindNull(9);
            } else {
                px0.bindString(9, k);
            }
            String c = WorkoutDao_Impl.this.__workoutTypeConverter.c(workoutSession.getCalorie());
            if (c == null) {
                px0.bindNull(10);
            } else {
                px0.bindString(10, c);
            }
            String d = WorkoutDao_Impl.this.__workoutTypeConverter.d(workoutSession.getDistance());
            if (d == null) {
                px0.bindNull(11);
            } else {
                px0.bindString(11, d);
            }
            String e = WorkoutDao_Impl.this.__workoutTypeConverter.e(workoutSession.getHeartRate());
            if (e == null) {
                px0.bindNull(12);
            } else {
                px0.bindString(12, e);
            }
            String h = WorkoutDao_Impl.this.__workoutTypeConverter.h(workoutSession.getSourceType());
            if (h == null) {
                px0.bindNull(13);
            } else {
                px0.bindString(13, h);
            }
            String l = WorkoutDao_Impl.this.__workoutTypeConverter.l(workoutSession.getWorkoutType());
            if (l == null) {
                px0.bindNull(14);
            } else {
                px0.bindString(14, l);
            }
            px0.bindLong(15, (long) workoutSession.getTimezoneOffsetInSecond());
            px0.bindLong(16, (long) workoutSession.getDuration());
            px0.bindLong(17, workoutSession.getCreatedAt());
            px0.bindLong(18, workoutSession.getUpdatedAt());
            String a5 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getWorkoutGpsPoints());
            if (a5 == null) {
                px0.bindNull(19);
            } else {
                px0.bindString(19, a5);
            }
            if (workoutSession.getGpsDataPoints() == null) {
                px0.bindNull(20);
            } else {
                px0.bindString(20, workoutSession.getGpsDataPoints());
            }
            String f = WorkoutDao_Impl.this.__workoutTypeConverter.f(workoutSession.getMode());
            if (f == null) {
                px0.bindNull(21);
            } else {
                px0.bindString(21, f);
            }
            String g = WorkoutDao_Impl.this.__workoutTypeConverter.g(workoutSession.getPace());
            if (g == null) {
                px0.bindNull(22);
            } else {
                px0.bindString(22, g);
            }
            String b = WorkoutDao_Impl.this.__workoutTypeConverter.b(workoutSession.getCadence());
            if (b == null) {
                px0.bindNull(23);
            } else {
                px0.bindString(23, b);
            }
            String a6 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getEditedStartTime());
            if (a6 == null) {
                px0.bindNull(24);
            } else {
                px0.bindString(24, a6);
            }
            String a7 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getEditedEndTime());
            if (a7 == null) {
                px0.bindNull(25);
            } else {
                px0.bindString(25, a7);
            }
            String l2 = WorkoutDao_Impl.this.__workoutTypeConverter.l(workoutSession.getEditedType());
            if (l2 == null) {
                px0.bindNull(26);
            } else {
                px0.bindString(26, l2);
            }
            String f2 = WorkoutDao_Impl.this.__workoutTypeConverter.f(workoutSession.getEditedMode());
            if (f2 == null) {
                px0.bindNull(27);
            } else {
                px0.bindString(27, f2);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `workout_session` (`speed`,`screenShotUri`,`states`,`id`,`date`,`startTime`,`endTime`,`deviceSerialNumber`,`step`,`calorie`,`distance`,`heartRate`,`sourceType`,`workoutType`,`timezoneOffset`,`duration`,`createdAt`,`updatedAt`,`workoutGpsPoints`,`gpsDataPoints`,`mode`,`pace`,`cadence`,`editedStartTime`,`editedEndTime`,`editedType`,`editedMode`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM workout_session WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends xw0 {
        @DexIgnore
        public Anon4(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM workout_session";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 implements Callable<List<WorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon5(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WorkoutSession> call() throws Exception {
            Cursor b = ex0.b(WorkoutDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, PlaceManager.PARAM_SPEED);
                int c2 = dx0.c(b, "screenShotUri");
                int c3 = dx0.c(b, "states");
                int c4 = dx0.c(b, "id");
                int c5 = dx0.c(b, "date");
                int c6 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
                int c7 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
                int c8 = dx0.c(b, MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
                int c9 = dx0.c(b, "step");
                int c10 = dx0.c(b, "calorie");
                int c11 = dx0.c(b, "distance");
                int c12 = dx0.c(b, "heartRate");
                int c13 = dx0.c(b, "sourceType");
                int c14 = dx0.c(b, "workoutType");
                int c15 = dx0.c(b, "timezoneOffset");
                int c16 = dx0.c(b, "duration");
                int c17 = dx0.c(b, "createdAt");
                int c18 = dx0.c(b, "updatedAt");
                int c19 = dx0.c(b, "workoutGpsPoints");
                int c20 = dx0.c(b, "gpsDataPoints");
                int c21 = dx0.c(b, "mode");
                int c22 = dx0.c(b, "pace");
                int c23 = dx0.c(b, "cadence");
                int c24 = dx0.c(b, MFSleepSession.COLUMN_EDITED_START_TIME);
                int c25 = dx0.c(b, MFSleepSession.COLUMN_EDITED_END_TIME);
                int c26 = dx0.c(b, "editedType");
                int c27 = dx0.c(b, "editedMode");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    WorkoutSession workoutSession = new WorkoutSession(b.getString(c4), WorkoutDao_Impl.this.__dateShortStringConverter.b(b.getString(c5)), WorkoutDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c6)), WorkoutDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c7)), b.getString(c8), WorkoutDao_Impl.this.__workoutTypeConverter.w(b.getString(c9)), WorkoutDao_Impl.this.__workoutTypeConverter.o(b.getString(c10)), WorkoutDao_Impl.this.__workoutTypeConverter.p(b.getString(c11)), WorkoutDao_Impl.this.__workoutTypeConverter.q(b.getString(c12)), WorkoutDao_Impl.this.__workoutTypeConverter.t(b.getString(c13)), WorkoutDao_Impl.this.__workoutTypeConverter.x(b.getString(c14)), b.getInt(c15), b.getInt(c16), b.getLong(c17), b.getLong(c18), WorkoutDao_Impl.this.__workoutTypeConverter.m(b.getString(c19)), b.getString(c20), WorkoutDao_Impl.this.__workoutTypeConverter.r(b.getString(c21)), WorkoutDao_Impl.this.__workoutTypeConverter.s(b.getString(c22)), WorkoutDao_Impl.this.__workoutTypeConverter.n(b.getString(c23)), WorkoutDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c24)), WorkoutDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c25)), WorkoutDao_Impl.this.__workoutTypeConverter.x(b.getString(c26)), WorkoutDao_Impl.this.__workoutTypeConverter.r(b.getString(c27)));
                    workoutSession.setSpeed(WorkoutDao_Impl.this.__workoutTypeConverter.u(b.getString(c)));
                    workoutSession.setScreenShotUri(b.getString(c2));
                    workoutSession.setStates(WorkoutDao_Impl.this.__workoutTypeConverter.v(b.getString(c3)));
                    arrayList.add(workoutSession);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 implements Callable<List<WorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon6(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WorkoutSession> call() throws Exception {
            Cursor b = ex0.b(WorkoutDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, PlaceManager.PARAM_SPEED);
                int c2 = dx0.c(b, "screenShotUri");
                int c3 = dx0.c(b, "states");
                int c4 = dx0.c(b, "id");
                int c5 = dx0.c(b, "date");
                int c6 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
                int c7 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
                int c8 = dx0.c(b, MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
                int c9 = dx0.c(b, "step");
                int c10 = dx0.c(b, "calorie");
                int c11 = dx0.c(b, "distance");
                int c12 = dx0.c(b, "heartRate");
                int c13 = dx0.c(b, "sourceType");
                int c14 = dx0.c(b, "workoutType");
                int c15 = dx0.c(b, "timezoneOffset");
                int c16 = dx0.c(b, "duration");
                int c17 = dx0.c(b, "createdAt");
                int c18 = dx0.c(b, "updatedAt");
                int c19 = dx0.c(b, "workoutGpsPoints");
                int c20 = dx0.c(b, "gpsDataPoints");
                int c21 = dx0.c(b, "mode");
                int c22 = dx0.c(b, "pace");
                int c23 = dx0.c(b, "cadence");
                int c24 = dx0.c(b, MFSleepSession.COLUMN_EDITED_START_TIME);
                int c25 = dx0.c(b, MFSleepSession.COLUMN_EDITED_END_TIME);
                int c26 = dx0.c(b, "editedType");
                int c27 = dx0.c(b, "editedMode");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    WorkoutSession workoutSession = new WorkoutSession(b.getString(c4), WorkoutDao_Impl.this.__dateShortStringConverter.b(b.getString(c5)), WorkoutDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c6)), WorkoutDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c7)), b.getString(c8), WorkoutDao_Impl.this.__workoutTypeConverter.w(b.getString(c9)), WorkoutDao_Impl.this.__workoutTypeConverter.o(b.getString(c10)), WorkoutDao_Impl.this.__workoutTypeConverter.p(b.getString(c11)), WorkoutDao_Impl.this.__workoutTypeConverter.q(b.getString(c12)), WorkoutDao_Impl.this.__workoutTypeConverter.t(b.getString(c13)), WorkoutDao_Impl.this.__workoutTypeConverter.x(b.getString(c14)), b.getInt(c15), b.getInt(c16), b.getLong(c17), b.getLong(c18), WorkoutDao_Impl.this.__workoutTypeConverter.m(b.getString(c19)), b.getString(c20), WorkoutDao_Impl.this.__workoutTypeConverter.r(b.getString(c21)), WorkoutDao_Impl.this.__workoutTypeConverter.s(b.getString(c22)), WorkoutDao_Impl.this.__workoutTypeConverter.n(b.getString(c23)), WorkoutDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c24)), WorkoutDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c25)), WorkoutDao_Impl.this.__workoutTypeConverter.x(b.getString(c26)), WorkoutDao_Impl.this.__workoutTypeConverter.r(b.getString(c27)));
                    workoutSession.setSpeed(WorkoutDao_Impl.this.__workoutTypeConverter.u(b.getString(c)));
                    workoutSession.setScreenShotUri(b.getString(c2));
                    workoutSession.setStates(WorkoutDao_Impl.this.__workoutTypeConverter.v(b.getString(c3)));
                    arrayList.add(workoutSession);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public WorkoutDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfWorkoutSession = new Anon1(qw0);
        this.__insertionAdapterOfWorkoutSession_1 = new Anon2(qw0);
        this.__preparedStmtOfDeleteWorkoutSessionById = new Anon3(qw0);
        this.__preparedStmtOfDeleteAllWorkoutSession = new Anon4(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public void deleteAllWorkoutSession() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteAllWorkoutSession.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllWorkoutSession.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public void deleteWorkoutSessionById(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteWorkoutSessionById.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWorkoutSessionById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public WorkoutSession getWorkoutSessionById(String str) {
        Throwable th;
        WorkoutSession workoutSession;
        tw0 f = tw0.f("SELECT * FROM workout_session WHERE id = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, PlaceManager.PARAM_SPEED);
            int c2 = dx0.c(b, "screenShotUri");
            int c3 = dx0.c(b, "states");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "date");
            int c6 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c7 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
            int c8 = dx0.c(b, MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int c9 = dx0.c(b, "step");
            int c10 = dx0.c(b, "calorie");
            int c11 = dx0.c(b, "distance");
            int c12 = dx0.c(b, "heartRate");
            int c13 = dx0.c(b, "sourceType");
            try {
                int c14 = dx0.c(b, "workoutType");
                int c15 = dx0.c(b, "timezoneOffset");
                int c16 = dx0.c(b, "duration");
                int c17 = dx0.c(b, "createdAt");
                int c18 = dx0.c(b, "updatedAt");
                int c19 = dx0.c(b, "workoutGpsPoints");
                int c20 = dx0.c(b, "gpsDataPoints");
                int c21 = dx0.c(b, "mode");
                int c22 = dx0.c(b, "pace");
                int c23 = dx0.c(b, "cadence");
                int c24 = dx0.c(b, MFSleepSession.COLUMN_EDITED_START_TIME);
                int c25 = dx0.c(b, MFSleepSession.COLUMN_EDITED_END_TIME);
                int c26 = dx0.c(b, "editedType");
                int c27 = dx0.c(b, "editedMode");
                if (b.moveToFirst()) {
                    workoutSession = new WorkoutSession(b.getString(c4), this.__dateShortStringConverter.b(b.getString(c5)), this.__dateTimeISOStringConverter.b(b.getString(c6)), this.__dateTimeISOStringConverter.b(b.getString(c7)), b.getString(c8), this.__workoutTypeConverter.w(b.getString(c9)), this.__workoutTypeConverter.o(b.getString(c10)), this.__workoutTypeConverter.p(b.getString(c11)), this.__workoutTypeConverter.q(b.getString(c12)), this.__workoutTypeConverter.t(b.getString(c13)), this.__workoutTypeConverter.x(b.getString(c14)), b.getInt(c15), b.getInt(c16), b.getLong(c17), b.getLong(c18), this.__workoutTypeConverter.m(b.getString(c19)), b.getString(c20), this.__workoutTypeConverter.r(b.getString(c21)), this.__workoutTypeConverter.s(b.getString(c22)), this.__workoutTypeConverter.n(b.getString(c23)), this.__dateTimeISOStringConverter.b(b.getString(c24)), this.__dateTimeISOStringConverter.b(b.getString(c25)), this.__workoutTypeConverter.x(b.getString(c26)), this.__workoutTypeConverter.r(b.getString(c27)));
                    workoutSession.setSpeed(this.__workoutTypeConverter.u(b.getString(c)));
                    workoutSession.setScreenShotUri(b.getString(c2));
                    workoutSession.setStates(this.__workoutTypeConverter.v(b.getString(c3)));
                } else {
                    workoutSession = null;
                }
                b.close();
                f.m();
                return workoutSession;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public LiveData<List<WorkoutSession>> getWorkoutSessions(Date date, Date date2) {
        tw0 f = tw0.f("SELECT * FROM workout_session WHERE date >= ? AND date <= ? ORDER BY startTime ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon5 anon5 = new Anon5(f);
        return invalidationTracker.d(new String[]{"workout_session"}, false, anon5);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public LiveData<List<WorkoutSession>> getWorkoutSessionsDesc(Date date, Date date2) {
        tw0 f = tw0.f("SELECT * FROM workout_session WHERE date >= ? AND date <= ? ORDER BY startTime DESC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon6 anon6 = new Anon6(f);
        return invalidationTracker.d(new String[]{"workout_session"}, false, anon6);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public List<WorkoutSession> getWorkoutSessionsInDate(Date date) {
        tw0 f = tw0.f("SELECT * FROM workout_session WHERE date = ? ORDER BY startTime ASC", 1);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, PlaceManager.PARAM_SPEED);
            int c2 = dx0.c(b, "screenShotUri");
            int c3 = dx0.c(b, "states");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "date");
            int c6 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c7 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
            int c8 = dx0.c(b, MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int c9 = dx0.c(b, "step");
            int c10 = dx0.c(b, "calorie");
            int c11 = dx0.c(b, "distance");
            int c12 = dx0.c(b, "heartRate");
            int c13 = dx0.c(b, "sourceType");
            try {
                int c14 = dx0.c(b, "workoutType");
                int c15 = dx0.c(b, "timezoneOffset");
                int c16 = dx0.c(b, "duration");
                int c17 = dx0.c(b, "createdAt");
                int c18 = dx0.c(b, "updatedAt");
                int c19 = dx0.c(b, "workoutGpsPoints");
                int c20 = dx0.c(b, "gpsDataPoints");
                int c21 = dx0.c(b, "mode");
                int c22 = dx0.c(b, "pace");
                int c23 = dx0.c(b, "cadence");
                int c24 = dx0.c(b, MFSleepSession.COLUMN_EDITED_START_TIME);
                int c25 = dx0.c(b, MFSleepSession.COLUMN_EDITED_END_TIME);
                int c26 = dx0.c(b, "editedType");
                int c27 = dx0.c(b, "editedMode");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    WorkoutSession workoutSession = new WorkoutSession(b.getString(c4), this.__dateShortStringConverter.b(b.getString(c5)), this.__dateTimeISOStringConverter.b(b.getString(c6)), this.__dateTimeISOStringConverter.b(b.getString(c7)), b.getString(c8), this.__workoutTypeConverter.w(b.getString(c9)), this.__workoutTypeConverter.o(b.getString(c10)), this.__workoutTypeConverter.p(b.getString(c11)), this.__workoutTypeConverter.q(b.getString(c12)), this.__workoutTypeConverter.t(b.getString(c13)), this.__workoutTypeConverter.x(b.getString(c14)), b.getInt(c15), b.getInt(c16), b.getLong(c17), b.getLong(c18), this.__workoutTypeConverter.m(b.getString(c19)), b.getString(c20), this.__workoutTypeConverter.r(b.getString(c21)), this.__workoutTypeConverter.s(b.getString(c22)), this.__workoutTypeConverter.n(b.getString(c23)), this.__dateTimeISOStringConverter.b(b.getString(c24)), this.__dateTimeISOStringConverter.b(b.getString(c25)), this.__workoutTypeConverter.x(b.getString(c26)), this.__workoutTypeConverter.r(b.getString(c27)));
                    workoutSession.setSpeed(this.__workoutTypeConverter.u(b.getString(c)));
                    workoutSession.setScreenShotUri(b.getString(c2));
                    workoutSession.setStates(this.__workoutTypeConverter.v(b.getString(c3)));
                    arrayList.add(workoutSession);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public List<WorkoutSession> getWorkoutSessionsInDateAfterDesc(Date date, long j, int i) {
        tw0 f = tw0.f("SELECT * FROM workout_session WHERE date = ? AND createdAt < ? ORDER BY startTime DESC limit ?", 3);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        f.bindLong(2, j);
        f.bindLong(3, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, PlaceManager.PARAM_SPEED);
            int c2 = dx0.c(b, "screenShotUri");
            int c3 = dx0.c(b, "states");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "date");
            int c6 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c7 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
            int c8 = dx0.c(b, MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int c9 = dx0.c(b, "step");
            int c10 = dx0.c(b, "calorie");
            int c11 = dx0.c(b, "distance");
            int c12 = dx0.c(b, "heartRate");
            int c13 = dx0.c(b, "sourceType");
            try {
                int c14 = dx0.c(b, "workoutType");
                int c15 = dx0.c(b, "timezoneOffset");
                int c16 = dx0.c(b, "duration");
                int c17 = dx0.c(b, "createdAt");
                int c18 = dx0.c(b, "updatedAt");
                int c19 = dx0.c(b, "workoutGpsPoints");
                int c20 = dx0.c(b, "gpsDataPoints");
                int c21 = dx0.c(b, "mode");
                int c22 = dx0.c(b, "pace");
                int c23 = dx0.c(b, "cadence");
                int c24 = dx0.c(b, MFSleepSession.COLUMN_EDITED_START_TIME);
                int c25 = dx0.c(b, MFSleepSession.COLUMN_EDITED_END_TIME);
                int c26 = dx0.c(b, "editedType");
                int c27 = dx0.c(b, "editedMode");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    WorkoutSession workoutSession = new WorkoutSession(b.getString(c4), this.__dateShortStringConverter.b(b.getString(c5)), this.__dateTimeISOStringConverter.b(b.getString(c6)), this.__dateTimeISOStringConverter.b(b.getString(c7)), b.getString(c8), this.__workoutTypeConverter.w(b.getString(c9)), this.__workoutTypeConverter.o(b.getString(c10)), this.__workoutTypeConverter.p(b.getString(c11)), this.__workoutTypeConverter.q(b.getString(c12)), this.__workoutTypeConverter.t(b.getString(c13)), this.__workoutTypeConverter.x(b.getString(c14)), b.getInt(c15), b.getInt(c16), b.getLong(c17), b.getLong(c18), this.__workoutTypeConverter.m(b.getString(c19)), b.getString(c20), this.__workoutTypeConverter.r(b.getString(c21)), this.__workoutTypeConverter.s(b.getString(c22)), this.__workoutTypeConverter.n(b.getString(c23)), this.__dateTimeISOStringConverter.b(b.getString(c24)), this.__dateTimeISOStringConverter.b(b.getString(c25)), this.__workoutTypeConverter.x(b.getString(c26)), this.__workoutTypeConverter.r(b.getString(c27)));
                    workoutSession.setSpeed(this.__workoutTypeConverter.u(b.getString(c)));
                    workoutSession.setScreenShotUri(b.getString(c2));
                    workoutSession.setStates(this.__workoutTypeConverter.v(b.getString(c3)));
                    arrayList.add(workoutSession);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public List<WorkoutSession> getWorkoutSessionsInDateDesc(Date date, int i) {
        tw0 f = tw0.f("SELECT * FROM workout_session WHERE date = ? ORDER BY startTime DESC limit ?", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        f.bindLong(2, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, PlaceManager.PARAM_SPEED);
            int c2 = dx0.c(b, "screenShotUri");
            int c3 = dx0.c(b, "states");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "date");
            int c6 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c7 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
            int c8 = dx0.c(b, MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int c9 = dx0.c(b, "step");
            int c10 = dx0.c(b, "calorie");
            int c11 = dx0.c(b, "distance");
            int c12 = dx0.c(b, "heartRate");
            int c13 = dx0.c(b, "sourceType");
            try {
                int c14 = dx0.c(b, "workoutType");
                int c15 = dx0.c(b, "timezoneOffset");
                int c16 = dx0.c(b, "duration");
                int c17 = dx0.c(b, "createdAt");
                int c18 = dx0.c(b, "updatedAt");
                int c19 = dx0.c(b, "workoutGpsPoints");
                int c20 = dx0.c(b, "gpsDataPoints");
                int c21 = dx0.c(b, "mode");
                int c22 = dx0.c(b, "pace");
                int c23 = dx0.c(b, "cadence");
                int c24 = dx0.c(b, MFSleepSession.COLUMN_EDITED_START_TIME);
                int c25 = dx0.c(b, MFSleepSession.COLUMN_EDITED_END_TIME);
                int c26 = dx0.c(b, "editedType");
                int c27 = dx0.c(b, "editedMode");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    WorkoutSession workoutSession = new WorkoutSession(b.getString(c4), this.__dateShortStringConverter.b(b.getString(c5)), this.__dateTimeISOStringConverter.b(b.getString(c6)), this.__dateTimeISOStringConverter.b(b.getString(c7)), b.getString(c8), this.__workoutTypeConverter.w(b.getString(c9)), this.__workoutTypeConverter.o(b.getString(c10)), this.__workoutTypeConverter.p(b.getString(c11)), this.__workoutTypeConverter.q(b.getString(c12)), this.__workoutTypeConverter.t(b.getString(c13)), this.__workoutTypeConverter.x(b.getString(c14)), b.getInt(c15), b.getInt(c16), b.getLong(c17), b.getLong(c18), this.__workoutTypeConverter.m(b.getString(c19)), b.getString(c20), this.__workoutTypeConverter.r(b.getString(c21)), this.__workoutTypeConverter.s(b.getString(c22)), this.__workoutTypeConverter.n(b.getString(c23)), this.__dateTimeISOStringConverter.b(b.getString(c24)), this.__dateTimeISOStringConverter.b(b.getString(c25)), this.__workoutTypeConverter.x(b.getString(c26)), this.__workoutTypeConverter.r(b.getString(c27)));
                    workoutSession.setSpeed(this.__workoutTypeConverter.u(b.getString(c)));
                    workoutSession.setScreenShotUri(b.getString(c2));
                    workoutSession.setStates(this.__workoutTypeConverter.v(b.getString(c3)));
                    arrayList.add(workoutSession);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public List<WorkoutSession> getWorkoutSessionsRaw(Date date, Date date2) {
        tw0 f = tw0.f("SELECT * FROM workout_session WHERE date >= ? AND date <= ? ORDER BY startTime ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, PlaceManager.PARAM_SPEED);
            int c2 = dx0.c(b, "screenShotUri");
            int c3 = dx0.c(b, "states");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "date");
            int c6 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c7 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
            int c8 = dx0.c(b, MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int c9 = dx0.c(b, "step");
            int c10 = dx0.c(b, "calorie");
            int c11 = dx0.c(b, "distance");
            int c12 = dx0.c(b, "heartRate");
            int c13 = dx0.c(b, "sourceType");
            try {
                int c14 = dx0.c(b, "workoutType");
                int c15 = dx0.c(b, "timezoneOffset");
                int c16 = dx0.c(b, "duration");
                int c17 = dx0.c(b, "createdAt");
                int c18 = dx0.c(b, "updatedAt");
                int c19 = dx0.c(b, "workoutGpsPoints");
                int c20 = dx0.c(b, "gpsDataPoints");
                int c21 = dx0.c(b, "mode");
                int c22 = dx0.c(b, "pace");
                int c23 = dx0.c(b, "cadence");
                int c24 = dx0.c(b, MFSleepSession.COLUMN_EDITED_START_TIME);
                int c25 = dx0.c(b, MFSleepSession.COLUMN_EDITED_END_TIME);
                int c26 = dx0.c(b, "editedType");
                int c27 = dx0.c(b, "editedMode");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    WorkoutSession workoutSession = new WorkoutSession(b.getString(c4), this.__dateShortStringConverter.b(b.getString(c5)), this.__dateTimeISOStringConverter.b(b.getString(c6)), this.__dateTimeISOStringConverter.b(b.getString(c7)), b.getString(c8), this.__workoutTypeConverter.w(b.getString(c9)), this.__workoutTypeConverter.o(b.getString(c10)), this.__workoutTypeConverter.p(b.getString(c11)), this.__workoutTypeConverter.q(b.getString(c12)), this.__workoutTypeConverter.t(b.getString(c13)), this.__workoutTypeConverter.x(b.getString(c14)), b.getInt(c15), b.getInt(c16), b.getLong(c17), b.getLong(c18), this.__workoutTypeConverter.m(b.getString(c19)), b.getString(c20), this.__workoutTypeConverter.r(b.getString(c21)), this.__workoutTypeConverter.s(b.getString(c22)), this.__workoutTypeConverter.n(b.getString(c23)), this.__dateTimeISOStringConverter.b(b.getString(c24)), this.__dateTimeISOStringConverter.b(b.getString(c25)), this.__workoutTypeConverter.x(b.getString(c26)), this.__workoutTypeConverter.r(b.getString(c27)));
                    workoutSession.setSpeed(this.__workoutTypeConverter.u(b.getString(c)));
                    workoutSession.setScreenShotUri(b.getString(c2));
                    workoutSession.setStates(this.__workoutTypeConverter.v(b.getString(c3)));
                    arrayList.add(workoutSession);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public void insertWorkoutSession(WorkoutSession workoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkoutSession.insert((jw0<WorkoutSession>) workoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public void upsertListWorkoutSession(List<WorkoutSession> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkoutSession_1.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public void upsertWorkoutSession(WorkoutSession workoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkoutSession_1.insert((jw0<WorkoutSession>) workoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
