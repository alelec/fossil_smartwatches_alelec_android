package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.fossil.st4;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesChallengeRemoteDataSourceFactory implements Factory<st4> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesChallengeRemoteDataSourceFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        this.module = portfolioDatabaseModule;
        this.apiProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesChallengeRemoteDataSourceFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        return new PortfolioDatabaseModule_ProvidesChallengeRemoteDataSourceFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static st4 providesChallengeRemoteDataSource(PortfolioDatabaseModule portfolioDatabaseModule, ApiServiceV2 apiServiceV2) {
        st4 providesChallengeRemoteDataSource = portfolioDatabaseModule.providesChallengeRemoteDataSource(apiServiceV2);
        lk7.c(providesChallengeRemoteDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return providesChallengeRemoteDataSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public st4 get() {
        return providesChallengeRemoteDataSource(this.module, this.apiProvider.get());
    }
}
