package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.ThemeRepository$initializeLocalTheme$2", f = "ThemeRepository.kt", l = {44, 45}, m = "invokeSuspend")
public final class ThemeRepository$initializeLocalTheme$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThemeRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThemeRepository$initializeLocalTheme$Anon2(ThemeRepository themeRepository, qn7 qn7) {
        super(2, qn7);
        this.this$0 = themeRepository;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ThemeRepository$initializeLocalTheme$Anon2 themeRepository$initializeLocalTheme$Anon2 = new ThemeRepository$initializeLocalTheme$Anon2(this.this$0, qn7);
        themeRepository$initializeLocalTheme$Anon2.p$ = (iv7) obj;
        return themeRepository$initializeLocalTheme$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((ThemeRepository$initializeLocalTheme$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r8) {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            java.lang.Object r1 = com.fossil.yn7.d()
            int r0 = r7.label
            if (r0 == 0) goto L_0x0039
            if (r0 == r5) goto L_0x0020
            if (r0 != r6) goto L_0x0018
            java.lang.Object r0 = r7.L$0
            com.fossil.iv7 r0 = (com.fossil.iv7) r0
            com.fossil.el7.b(r8)
        L_0x0015:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0017:
            return r0
        L_0x0018:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0020:
            java.lang.Object r0 = r7.L$0
            com.fossil.iv7 r0 = (com.fossil.iv7) r0
            com.fossil.el7.b(r8)
        L_0x0027:
            com.fossil.qn5$a r2 = com.fossil.qn5.l
            com.fossil.qn5 r2 = r2.a()
            r7.L$0 = r0
            r7.label = r6
            java.lang.Object r0 = r2.i(r7)
            if (r0 != r1) goto L_0x0015
            r0 = r1
            goto L_0x0017
        L_0x0039:
            com.fossil.el7.b(r8)
            com.fossil.iv7 r0 = r7.p$
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            com.portfolio.platform.data.source.ThemeRepository$Companion r3 = com.portfolio.platform.data.source.ThemeRepository.Companion
            java.lang.String r3 = r3.getTAG()
            java.lang.String r4 = "initializeLocalTheme"
            r2.d(r3, r4)
            com.portfolio.platform.data.source.ThemeRepository r2 = r7.this$0
            java.lang.String r3 = "theme/default/config"
            com.portfolio.platform.data.source.ThemeRepository.access$loadTheme(r2, r3)
            com.portfolio.platform.data.source.ThemeRepository r2 = r7.this$0
            com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r3 = r3.c()
            java.lang.String r3 = r3.U()
            r7.L$0 = r0
            r7.label = r5
            java.lang.Object r2 = r2.setCurrentThemeId(r3, r7)
            if (r2 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ThemeRepository$initializeLocalTheme$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
