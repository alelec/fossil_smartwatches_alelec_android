package com.portfolio.platform.data.source.local.alarm;

import com.fossil.ax0;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qw0;
import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class AlarmDatabase extends qw0 {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ ax0 MIGRATION_FROM_5_TO_6; // = new AlarmDatabase$Companion$MIGRATION_FROM_5_TO_6$Anon1(5, 6);
    @DexIgnore
    public static /* final */ String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ax0 getMIGRATION_FROM_5_TO_6() {
            return AlarmDatabase.MIGRATION_FROM_5_TO_6;
        }

        @DexIgnore
        public final ax0 migrating3Or4To5(String str, int i) {
            pq7.c(str, ButtonService.USER_ID);
            return new AlarmDatabase$Companion$migrating3Or4To5$Anon1(i, str, i, 5);
        }
    }

    /*
    static {
        String simpleName = AlarmDatabase.class.getSimpleName();
        pq7.b(simpleName, "AlarmDatabase::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public abstract AlarmDao alarmDao();
}
