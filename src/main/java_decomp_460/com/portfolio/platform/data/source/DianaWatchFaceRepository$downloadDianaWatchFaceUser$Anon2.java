package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.DianaWatchFaceRepository$downloadDianaWatchFaceUser$2", f = "DianaWatchFaceRepository.kt", l = {134, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 141}, m = "invokeSuspend")
public final class DianaWatchFaceRepository$downloadDianaWatchFaceUser$Anon2 extends ko7 implements vp7<iv7, qn7<? super DianaWatchFaceUser>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $id;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaWatchFaceRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaWatchFaceRepository$downloadDianaWatchFaceUser$Anon2(DianaWatchFaceRepository dianaWatchFaceRepository, String str, qn7 qn7) {
        super(2, qn7);
        this.this$0 = dianaWatchFaceRepository;
        this.$id = str;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        DianaWatchFaceRepository$downloadDianaWatchFaceUser$Anon2 dianaWatchFaceRepository$downloadDianaWatchFaceUser$Anon2 = new DianaWatchFaceRepository$downloadDianaWatchFaceUser$Anon2(this.this$0, this.$id, qn7);
        dianaWatchFaceRepository$downloadDianaWatchFaceUser$Anon2.p$ = (iv7) obj;
        return dianaWatchFaceRepository$downloadDianaWatchFaceUser$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super DianaWatchFaceUser> qn7) {
        return ((DianaWatchFaceRepository$downloadDianaWatchFaceUser$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c1 A[RETURN, SYNTHETIC] */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
        // Method dump skipped, instructions count: 205
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaWatchFaceRepository$downloadDianaWatchFaceUser$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
