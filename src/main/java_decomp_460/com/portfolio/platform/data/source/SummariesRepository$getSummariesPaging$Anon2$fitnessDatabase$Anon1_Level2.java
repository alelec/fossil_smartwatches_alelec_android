package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SummariesRepository$getSummariesPaging$2$fitnessDatabase$1", f = "SummariesRepository.kt", l = {356}, m = "invokeSuspend")
public final class SummariesRepository$getSummariesPaging$Anon2$fitnessDatabase$Anon1_Level2 extends ko7 implements vp7<iv7, qn7<? super FitnessDatabase>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    public SummariesRepository$getSummariesPaging$Anon2$fitnessDatabase$Anon1_Level2(qn7 qn7) {
        super(2, qn7);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SummariesRepository$getSummariesPaging$Anon2$fitnessDatabase$Anon1_Level2 summariesRepository$getSummariesPaging$Anon2$fitnessDatabase$Anon1_Level2 = new SummariesRepository$getSummariesPaging$Anon2$fitnessDatabase$Anon1_Level2(qn7);
        summariesRepository$getSummariesPaging$Anon2$fitnessDatabase$Anon1_Level2.p$ = (iv7) obj;
        return summariesRepository$getSummariesPaging$Anon2$fitnessDatabase$Anon1_Level2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super FitnessDatabase> qn7) {
        return ((SummariesRepository$getSummariesPaging$Anon2$fitnessDatabase$Anon1_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.label = 1;
            Object y = bn5.y(this);
            return y == d ? d : y;
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
