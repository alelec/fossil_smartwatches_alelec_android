package com.portfolio.platform.data.source;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDatabase_Impl extends DeviceDatabase {
    @DexIgnore
    public volatile DeviceDao _deviceDao;
    @DexIgnore
    public volatile SkuDao _skuDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `device` (`major` INTEGER NOT NULL, `minor` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT, `owner` TEXT, `productDisplayName` TEXT, `manufacturer` TEXT, `softwareRevision` TEXT, `hardwareRevision` TEXT, `activationDate` TEXT, `deviceId` TEXT NOT NULL, `macAddress` TEXT, `sku` TEXT, `firmwareRevision` TEXT, `batteryLevel` INTEGER NOT NULL, `vibrationStrength` INTEGER, `isActive` INTEGER NOT NULL, PRIMARY KEY(`deviceId`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `SKU` (`createdAt` TEXT, `updatedAt` TEXT, `serialNumberPrefix` TEXT NOT NULL, `sku` TEXT, `deviceName` TEXT, `groupName` TEXT, `gender` TEXT, `deviceType` TEXT, `fastPairId` TEXT, PRIMARY KEY(`serialNumberPrefix`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `watchParam` (`prefixSerial` TEXT NOT NULL, `versionMajor` TEXT, `versionMinor` TEXT, `data` TEXT, PRIMARY KEY(`prefixSerial`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '33a7c7e78a298d1d8772b73c27f82083')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `device`");
            lx0.execSQL("DROP TABLE IF EXISTS `SKU`");
            lx0.execSQL("DROP TABLE IF EXISTS `watchParam`");
            if (DeviceDatabase_Impl.this.mCallbacks != null) {
                int size = DeviceDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) DeviceDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (DeviceDatabase_Impl.this.mCallbacks != null) {
                int size = DeviceDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) DeviceDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            DeviceDatabase_Impl.this.mDatabase = lx0;
            DeviceDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (DeviceDatabase_Impl.this.mCallbacks != null) {
                int size = DeviceDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) DeviceDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(17);
            hashMap.put("major", new ix0.a("major", "INTEGER", true, 0, null, 1));
            hashMap.put("minor", new ix0.a("minor", "INTEGER", true, 0, null, 1));
            hashMap.put("createdAt", new ix0.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap.put("updatedAt", new ix0.a("updatedAt", "TEXT", false, 0, null, 1));
            hashMap.put("owner", new ix0.a("owner", "TEXT", false, 0, null, 1));
            hashMap.put("productDisplayName", new ix0.a("productDisplayName", "TEXT", false, 0, null, 1));
            hashMap.put("manufacturer", new ix0.a("manufacturer", "TEXT", false, 0, null, 1));
            hashMap.put("softwareRevision", new ix0.a("softwareRevision", "TEXT", false, 0, null, 1));
            hashMap.put("hardwareRevision", new ix0.a("hardwareRevision", "TEXT", false, 0, null, 1));
            hashMap.put("activationDate", new ix0.a("activationDate", "TEXT", false, 0, null, 1));
            hashMap.put("deviceId", new ix0.a("deviceId", "TEXT", true, 1, null, 1));
            hashMap.put("macAddress", new ix0.a("macAddress", "TEXT", false, 0, null, 1));
            hashMap.put(LegacyDeviceModel.COLUMN_DEVICE_MODEL, new ix0.a(LegacyDeviceModel.COLUMN_DEVICE_MODEL, "TEXT", false, 0, null, 1));
            hashMap.put(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION, new ix0.a(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION, "TEXT", false, 0, null, 1));
            hashMap.put(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, new ix0.a(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, "INTEGER", true, 0, null, 1));
            hashMap.put("vibrationStrength", new ix0.a("vibrationStrength", "INTEGER", false, 0, null, 1));
            hashMap.put("isActive", new ix0.a("isActive", "INTEGER", true, 0, null, 1));
            ix0 ix0 = new ix0("device", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "device");
            if (!ix0.equals(a2)) {
                return new sw0.b(false, "device(com.portfolio.platform.data.model.Device).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(9);
            hashMap2.put("createdAt", new ix0.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap2.put("updatedAt", new ix0.a("updatedAt", "TEXT", false, 0, null, 1));
            hashMap2.put("serialNumberPrefix", new ix0.a("serialNumberPrefix", "TEXT", true, 1, null, 1));
            hashMap2.put(LegacyDeviceModel.COLUMN_DEVICE_MODEL, new ix0.a(LegacyDeviceModel.COLUMN_DEVICE_MODEL, "TEXT", false, 0, null, 1));
            hashMap2.put("deviceName", new ix0.a("deviceName", "TEXT", false, 0, null, 1));
            hashMap2.put("groupName", new ix0.a("groupName", "TEXT", false, 0, null, 1));
            hashMap2.put("gender", new ix0.a("gender", "TEXT", false, 0, null, 1));
            hashMap2.put("deviceType", new ix0.a("deviceType", "TEXT", false, 0, null, 1));
            hashMap2.put("fastPairId", new ix0.a("fastPairId", "TEXT", false, 0, null, 1));
            ix0 ix02 = new ix0("SKU", hashMap2, new HashSet(0), new HashSet(0));
            ix0 a3 = ix0.a(lx0, "SKU");
            if (!ix02.equals(a3)) {
                return new sw0.b(false, "SKU(com.portfolio.platform.data.model.SKUModel).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
            }
            HashMap hashMap3 = new HashMap(4);
            hashMap3.put("prefixSerial", new ix0.a("prefixSerial", "TEXT", true, 1, null, 1));
            hashMap3.put("versionMajor", new ix0.a("versionMajor", "TEXT", false, 0, null, 1));
            hashMap3.put("versionMinor", new ix0.a("versionMinor", "TEXT", false, 0, null, 1));
            hashMap3.put("data", new ix0.a("data", "TEXT", false, 0, null, 1));
            ix0 ix03 = new ix0("watchParam", hashMap3, new HashSet(0), new HashSet(0));
            ix0 a4 = ix0.a(lx0, "watchParam");
            if (ix03.equals(a4)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "watchParam(com.portfolio.platform.data.model.WatchParam).\n Expected:\n" + ix03 + "\n Found:\n" + a4);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `device`");
            writableDatabase.execSQL("DELETE FROM `SKU`");
            writableDatabase.execSQL("DELETE FROM `watchParam`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "device", "SKU", "watchParam");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(4), "33a7c7e78a298d1d8772b73c27f82083", "ccea49e2e20194ca42a76fa8a936d66a");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDatabase
    public DeviceDao deviceDao() {
        DeviceDao deviceDao;
        if (this._deviceDao != null) {
            return this._deviceDao;
        }
        synchronized (this) {
            if (this._deviceDao == null) {
                this._deviceDao = new DeviceDao_Impl(this);
            }
            deviceDao = this._deviceDao;
        }
        return deviceDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDatabase
    public SkuDao skuDao() {
        SkuDao skuDao;
        if (this._skuDao != null) {
            return this._skuDao;
        }
        synchronized (this) {
            if (this._skuDao == null) {
                this._skuDao = new SkuDao_Impl(this);
            }
            skuDao = this._skuDao;
        }
        return skuDao;
    }
}
