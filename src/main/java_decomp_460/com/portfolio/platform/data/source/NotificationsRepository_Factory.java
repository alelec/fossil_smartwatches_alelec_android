package com.portfolio.platform.data.source;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationsRepository_Factory implements Factory<NotificationsRepository> {
    @DexIgnore
    public /* final */ Provider<NotificationsDataSource> mappingLocalSetDataSourceProvider;

    @DexIgnore
    public NotificationsRepository_Factory(Provider<NotificationsDataSource> provider) {
        this.mappingLocalSetDataSourceProvider = provider;
    }

    @DexIgnore
    public static NotificationsRepository_Factory create(Provider<NotificationsDataSource> provider) {
        return new NotificationsRepository_Factory(provider);
    }

    @DexIgnore
    public static NotificationsRepository newInstance(NotificationsDataSource notificationsDataSource) {
        return new NotificationsRepository(notificationsDataSource);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public NotificationsRepository get() {
        return newInstance(this.mappingLocalSetDataSourceProvider.get());
    }
}
