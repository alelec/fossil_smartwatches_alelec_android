package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.gi0;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryRepository$getSummariesPaging$Anon2<I, O> implements gi0<X, LiveData<Y>> {
    @DexIgnore
    public static /* final */ HeartRateSummaryRepository$getSummariesPaging$Anon2 INSTANCE; // = new HeartRateSummaryRepository$getSummariesPaging$Anon2();

    @DexIgnore
    public final LiveData<NetworkState> apply(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        return heartRateSummaryLocalDataSource.getMNetworkState();
    }
}
