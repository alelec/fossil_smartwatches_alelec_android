package com.portfolio.platform.data.source;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UAppSystemVersionRepository_Factory implements Factory<UAppSystemVersionRepository> {
    @DexIgnore
    public /* final */ Provider<UAppSystemVersionDataSource> uAppSystemVersionDataSourceProvider;

    @DexIgnore
    public UAppSystemVersionRepository_Factory(Provider<UAppSystemVersionDataSource> provider) {
        this.uAppSystemVersionDataSourceProvider = provider;
    }

    @DexIgnore
    public static UAppSystemVersionRepository_Factory create(Provider<UAppSystemVersionDataSource> provider) {
        return new UAppSystemVersionRepository_Factory(provider);
    }

    @DexIgnore
    public static UAppSystemVersionRepository newInstance(UAppSystemVersionDataSource uAppSystemVersionDataSource) {
        return new UAppSystemVersionRepository(uAppSystemVersionDataSource);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public UAppSystemVersionRepository get() {
        return newInstance(this.uAppSystemVersionDataSourceProvider.get());
    }
}
