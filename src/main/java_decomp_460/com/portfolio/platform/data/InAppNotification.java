package com.portfolio.platform.data;

import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InAppNotification {
    @DexIgnore
    public String content;
    @DexIgnore
    public String id;
    @DexIgnore
    public String title;

    @DexIgnore
    public InAppNotification(String str, String str2, String str3) {
        pq7.c(str, "id");
        pq7.c(str2, "title");
        pq7.c(str3, "content");
        this.id = str;
        this.title = str2;
        this.content = str3;
    }

    @DexIgnore
    public static /* synthetic */ InAppNotification copy$default(InAppNotification inAppNotification, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = inAppNotification.id;
        }
        if ((i & 2) != 0) {
            str2 = inAppNotification.title;
        }
        if ((i & 4) != 0) {
            str3 = inAppNotification.content;
        }
        return inAppNotification.copy(str, str2, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.title;
    }

    @DexIgnore
    public final String component3() {
        return this.content;
    }

    @DexIgnore
    public final InAppNotification copy(String str, String str2, String str3) {
        pq7.c(str, "id");
        pq7.c(str2, "title");
        pq7.c(str3, "content");
        return new InAppNotification(str, str2, str3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof InAppNotification) {
                InAppNotification inAppNotification = (InAppNotification) obj;
                if (!pq7.a(this.id, inAppNotification.id) || !pq7.a(this.title, inAppNotification.title) || !pq7.a(this.content, inAppNotification.content)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getContent() {
        return this.content;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getTitle() {
        return this.title;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.title;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.content;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public final void setContent(String str) {
        pq7.c(str, "<set-?>");
        this.content = str;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setTitle(String str) {
        pq7.c(str, "<set-?>");
        this.title = str;
    }

    @DexIgnore
    public String toString() {
        return "InAppNotification(id=" + this.id + ", title=" + this.title + ", content=" + this.content + ")";
    }
}
