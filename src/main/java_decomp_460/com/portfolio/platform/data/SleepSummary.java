package com.portfolio.platform.data;

import com.fossil.kq7;
import com.fossil.pq7;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummary {
    @DexIgnore
    public MFSleepDay sleepDay;
    @DexIgnore
    public List<MFSleepSession> sleepSessions;

    @DexIgnore
    public SleepSummary() {
        this(null, null, 3, null);
    }

    @DexIgnore
    public SleepSummary(MFSleepDay mFSleepDay, List<MFSleepSession> list) {
        this.sleepDay = mFSleepDay;
        this.sleepSessions = list;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ SleepSummary(MFSleepDay mFSleepDay, List list, int i, kq7 kq7) {
        this((i & 1) != 0 ? null : mFSleepDay, (i & 2) != 0 ? null : list);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.SleepSummary */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ SleepSummary copy$default(SleepSummary sleepSummary, MFSleepDay mFSleepDay, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            mFSleepDay = sleepSummary.sleepDay;
        }
        if ((i & 2) != 0) {
            list = sleepSummary.sleepSessions;
        }
        return sleepSummary.copy(mFSleepDay, list);
    }

    @DexIgnore
    public final MFSleepDay component1() {
        return this.sleepDay;
    }

    @DexIgnore
    public final List<MFSleepSession> component2() {
        return this.sleepSessions;
    }

    @DexIgnore
    public final SleepSummary copy(MFSleepDay mFSleepDay, List<MFSleepSession> list) {
        return new SleepSummary(mFSleepDay, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SleepSummary) {
                SleepSummary sleepSummary = (SleepSummary) obj;
                if (!pq7.a(this.sleepDay, sleepSummary.sleepDay) || !pq7.a(this.sleepSessions, sleepSummary.sleepSessions)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Date getDate() {
        MFSleepDay mFSleepDay = this.sleepDay;
        if (mFSleepDay == null) {
            List<MFSleepSession> list = this.sleepSessions;
            if (list == null) {
                return null;
            }
            if (list == null) {
                pq7.i();
                throw null;
            } else if (!(!list.isEmpty())) {
                return null;
            } else {
                List<MFSleepSession> list2 = this.sleepSessions;
                if (list2 != null) {
                    return list2.get(0).getDay();
                }
                pq7.i();
                throw null;
            }
        } else if (mFSleepDay != null) {
            return mFSleepDay.getDate();
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final MFSleepDay getSleepDay() {
        return this.sleepDay;
    }

    @DexIgnore
    public final List<MFSleepSession> getSleepSessions() {
        return this.sleepSessions;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        MFSleepDay mFSleepDay = this.sleepDay;
        int hashCode = mFSleepDay != null ? mFSleepDay.hashCode() : 0;
        List<MFSleepSession> list = this.sleepSessions;
        if (list != null) {
            i = list.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public final void setSleepDay(MFSleepDay mFSleepDay) {
        this.sleepDay = mFSleepDay;
    }

    @DexIgnore
    public final void setSleepSessions(List<MFSleepSession> list) {
        this.sleepSessions = list;
    }

    @DexIgnore
    public String toString() {
        return "SleepSummary(sleepDay=" + this.sleepDay + ", sleepSessions=" + this.sleepSessions + ")";
    }
}
