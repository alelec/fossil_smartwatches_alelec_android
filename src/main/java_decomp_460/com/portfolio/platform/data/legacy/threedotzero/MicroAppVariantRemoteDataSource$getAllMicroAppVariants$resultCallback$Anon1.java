package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.c88;
import com.fossil.gj4;
import com.fossil.il7;
import com.fossil.nq5;
import com.fossil.pq7;
import com.fossil.q88;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import com.portfolio.platform.data.model.Range;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRemoteDataSource$getAllMicroAppVariants$resultCallback$Anon1 implements c88<gj4> {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantDataSource.GetVariantListCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ int $major;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $microAppVariants;
    @DexIgnore
    public /* final */ /* synthetic */ int $minor;
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantRemoteDataSource this$0;

    @DexIgnore
    public MicroAppVariantRemoteDataSource$getAllMicroAppVariants$resultCallback$Anon1(MicroAppVariantRemoteDataSource microAppVariantRemoteDataSource, String str, ArrayList arrayList, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        this.this$0 = microAppVariantRemoteDataSource;
        this.$serialNumber = str;
        this.$microAppVariants = arrayList;
        this.$major = i;
        this.$minor = i2;
        this.$callback = getVariantListCallback;
    }

    @DexIgnore
    @Override // com.fossil.c88
    public void onFailure(Call<gj4> call, Throwable th) {
        pq7.c(call, "call");
        pq7.c(th, "throwable");
        String tag = MicroAppVariantRemoteDataSource.Companion.getTAG();
        MFLogger.d(tag, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onFailure");
        if (!this.$microAppVariants.isEmpty()) {
            String tag2 = MicroAppVariantRemoteDataSource.Companion.getTAG();
            MFLogger.d(tag2, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onFailure microAppVariants not null");
            MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback = this.$callback;
            if (getVariantListCallback != null) {
                ((MicroAppVariantDataSource.GetVariantListRemoteCallback) getVariantListCallback).onSuccess(this.$microAppVariants);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback");
        }
        String tag3 = MicroAppVariantRemoteDataSource.Companion.getTAG();
        MFLogger.d(tag3, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onFailure microAppVariants is null");
        if (th instanceof SocketTimeoutException) {
            MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback2 = this.$callback;
            if (getVariantListCallback2 != null) {
                getVariantListCallback2.onFail(MFNetworkReturnCode.CLIENT_TIMEOUT);
                return;
            }
            return;
        }
        MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback3 = this.$callback;
        if (getVariantListCallback3 != null) {
            getVariantListCallback3.onFail(601);
        }
    }

    @DexIgnore
    @Override // com.fossil.c88
    public void onResponse(Call<gj4> call, q88<gj4> q88) {
        pq7.c(call, "call");
        pq7.c(q88, "response");
        if (q88.e()) {
            gj4 a2 = q88.a();
            String tag = MicroAppVariantRemoteDataSource.Companion.getTAG();
            MFLogger.d(tag, "getMicroAppGallery deviceSerial=" + this.$serialNumber + " onSuccess response=" + a2);
            nq5 nq5 = new nq5();
            nq5.c(a2);
            this.$microAppVariants.addAll(nq5.a());
            Range b = nq5.b();
            if (b == null || !b.isHasNext()) {
                String tag2 = MicroAppVariantRemoteDataSource.Companion.getTAG();
                MFLogger.d(tag2, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onSuccess hasNext=false");
                if (this.$microAppVariants.isEmpty()) {
                    MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback = this.$callback;
                    if (getVariantListCallback != null) {
                        getVariantListCallback.onFail(q88.b());
                        return;
                    }
                    return;
                }
                MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback2 = this.$callback;
                if (getVariantListCallback2 != null) {
                    ((MicroAppVariantDataSource.GetVariantListRemoteCallback) getVariantListCallback2).onSuccess(this.$microAppVariants);
                    return;
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback");
            }
            String tag3 = MicroAppVariantRemoteDataSource.Companion.getTAG();
            MFLogger.d(tag3, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onSuccess hasNext=true");
            this.this$0.getAllMicroAppVariants$app_fossilRelease(this.$serialNumber, this.$major, this.$minor, b.getOffset() + b.getLimit(), b.getLimit(), this);
            return;
        }
        String tag4 = MicroAppVariantRemoteDataSource.Companion.getTAG();
        MFLogger.d(tag4, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " !isSuccessful");
        if (!this.$microAppVariants.isEmpty()) {
            String tag5 = MicroAppVariantRemoteDataSource.Companion.getTAG();
            MFLogger.d(tag5, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " !isSuccessful microAppVariants not null");
            MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback3 = this.$callback;
            if (getVariantListCallback3 != null) {
                ((MicroAppVariantDataSource.GetVariantListRemoteCallback) getVariantListCallback3).onSuccess(this.$microAppVariants);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback");
        }
        String tag6 = MicroAppVariantRemoteDataSource.Companion.getTAG();
        MFLogger.d(tag6, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " !isSuccessful microAppVariants is null");
        MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback4 = this.$callback;
        if (getVariantListCallback4 != null) {
            getVariantListCallback4.onFail(404);
        }
    }
}
