package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.no4;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MicroAppSettingRepositoryModule {
    @DexIgnore
    @Local
    public MicroAppSettingDataSource provideFavoriteMappingSetLocalDataSource() {
        return new MicroAppSettingLocalDataSource();
    }

    @DexIgnore
    @Remote
    public MicroAppSettingDataSource provideFavoriteMappingSetRemoteDataSource(ShortcutApiService shortcutApiService, no4 no4) {
        return new MicroAppSettingRemoteDataSource(shortcutApiService, no4);
    }
}
