package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.pq7;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRepository$getAllMicroAppVariants$Anon1 implements MicroAppVariantDataSource.GetVariantListCallback {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantDataSource.GetVariantListCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ int $major;
    @DexIgnore
    public /* final */ /* synthetic */ int $minor;
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantRepository this$0;

    @DexIgnore
    public MicroAppVariantRepository$getAllMicroAppVariants$Anon1(MicroAppVariantRepository microAppVariantRepository, String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        this.this$0 = microAppVariantRepository;
        this.$serialNumber = str;
        this.$major = i;
        this.$minor = i2;
        this.$callback = getVariantListCallback;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback
    public void onFail(int i) {
        String tag = MicroAppVariantRepository.Companion.getTAG();
        MFLogger.d(tag, "getAllMicroAppVariants local serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onFail");
        MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback = this.$callback;
        if (getVariantListCallback != null) {
            getVariantListCallback.onFail(i);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback
    public void onSuccess(List<MicroAppVariant> list) {
        pq7.c(list, "variantList");
        String tag = MicroAppVariantRepository.Companion.getTAG();
        MFLogger.d(tag, "getAllMicroAppVariants local serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onSuccess");
        this.this$0.notifyStatusChanged("DECLARATION_FILES_DOWNLOADED", this.$serialNumber);
        MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback = this.$callback;
        if (getVariantListCallback != null) {
            getVariantListCallback.onSuccess(list);
        }
    }
}
