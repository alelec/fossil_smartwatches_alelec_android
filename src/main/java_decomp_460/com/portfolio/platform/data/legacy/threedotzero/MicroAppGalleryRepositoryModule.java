package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.no4;
import com.fossil.pq7;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRepositoryModule {
    @DexIgnore
    @Local
    public final MicroAppGalleryDataSource provideGalleryLocalDataSource$app_fossilRelease() {
        return new MicroAppGalleryLocalDataSource();
    }

    @DexIgnore
    @Remote
    public final MicroAppGalleryDataSource provideGalleryRemoteDataSource$app_fossilRelease(ShortcutApiService shortcutApiService, no4 no4) {
        pq7.c(shortcutApiService, "shortcutApiService");
        pq7.c(no4, "appExecutors");
        return new MicroAppGalleryRemoteDataSource(shortcutApiService, no4);
    }
}
