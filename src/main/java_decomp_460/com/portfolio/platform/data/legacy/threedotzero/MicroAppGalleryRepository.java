package com.portfolio.platform.data.legacy.threedotzero;

import android.text.TextUtils;
import com.fossil.kq7;
import com.fossil.no4;
import com.fossil.pq7;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRepository extends MicroAppGalleryDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ no4 mAppExecutors;
    @DexIgnore
    public /* final */ MicroAppGalleryDataSource mMicroAppSettingLocalDataSource;
    @DexIgnore
    public /* final */ MicroAppGalleryDataSource mMicroAppSettingRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppGalleryRepository.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            pq7.c(str, "<set-?>");
            MicroAppGalleryRepository.TAG = str;
        }
    }

    /*
    static {
        String simpleName = MicroAppGalleryRepository.class.getSimpleName();
        pq7.b(simpleName, "MicroAppGalleryRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppGalleryRepository(@Remote MicroAppGalleryDataSource microAppGalleryDataSource, @Local MicroAppGalleryDataSource microAppGalleryDataSource2, no4 no4) {
        pq7.c(microAppGalleryDataSource, "mMicroAppSettingRemoteDataSource");
        pq7.c(microAppGalleryDataSource2, "mMicroAppSettingLocalDataSource");
        pq7.c(no4, "mAppExecutors");
        this.mMicroAppSettingRemoteDataSource = microAppGalleryDataSource;
        this.mMicroAppSettingLocalDataSource = microAppGalleryDataSource2;
        this.mAppExecutors = no4;
    }

    @DexIgnore
    public final void downloadMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        pq7.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        String str2 = TAG;
        MFLogger.d(str2, "downloadMicroAppGallery deviceSerial=" + str);
        this.mMicroAppSettingRemoteDataSource.getMicroAppGallery(str, new MicroAppGalleryRepository$downloadMicroAppGallery$Anon1(this, str, getMicroAppGalleryCallback));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void getMicroApp(String str, String str2, MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback) {
        pq7.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        pq7.c(str2, "microAppId");
        if (TextUtils.isEmpty(str)) {
            MFLogger.d(TAG, "getMicroApp deviceSerial=empty");
            if (getMicroAppCallback != null) {
                getMicroAppCallback.onFail();
                return;
            }
            return;
        }
        this.mMicroAppSettingLocalDataSource.getMicroApp(str, str2, new MicroAppGalleryRepository$getMicroApp$Anon1(this, str2, getMicroAppCallback, str));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void getMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        pq7.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        String str2 = TAG;
        MFLogger.d(str2, "getMicroAppGallery deviceSerial=" + str);
        if (TextUtils.isEmpty(str)) {
            MFLogger.d(TAG, "getMicroAppGallery deviceSerial=empty");
            if (getMicroAppGalleryCallback != null) {
                getMicroAppGalleryCallback.onFail();
                return;
            }
            return;
        }
        this.mMicroAppSettingLocalDataSource.getMicroAppGallery(str, new MicroAppGalleryRepository$getMicroAppGallery$Anon1(str, getMicroAppGalleryCallback));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void updateMicroApp(MicroApp microApp, MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback) {
        pq7.c(microApp, "microApp");
        String str = TAG;
        MFLogger.d(str, "updateMicroApp microApp=" + microApp.getAppId());
        this.mMicroAppSettingLocalDataSource.updateMicroApp(microApp, getMicroAppCallback);
    }
}
