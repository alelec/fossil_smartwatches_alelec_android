package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.kq7;
import com.fossil.no4;
import com.fossil.pq7;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import com.portfolio.platform.data.source.remote.ShortcutApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRemoteDataSource extends MicroAppGalleryDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ no4 mAppExecutors;
    @DexIgnore
    public /* final */ ShortcutApiService mShortcutApiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppGalleryRemoteDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            pq7.c(str, "<set-?>");
            MicroAppGalleryRemoteDataSource.TAG = str;
        }
    }

    /*
    static {
        String simpleName = MicroAppGalleryRemoteDataSource.class.getSimpleName();
        pq7.b(simpleName, "MicroAppGalleryRemoteDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppGalleryRemoteDataSource(ShortcutApiService shortcutApiService, no4 no4) {
        pq7.c(shortcutApiService, "mShortcutApiService");
        pq7.c(no4, "mAppExecutors");
        this.mShortcutApiService = shortcutApiService;
        this.mAppExecutors = no4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void getMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        pq7.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "getMicroAppGallery deviceSerial=" + str);
        this.mShortcutApiService.getMicroAppGallery(0, 100, str).D(new MicroAppGalleryRemoteDataSource$getMicroAppGallery$Anon1(str, getMicroAppGalleryCallback));
    }
}
