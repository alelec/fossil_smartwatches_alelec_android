package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.c88;
import com.fossil.gj4;
import com.fossil.kq7;
import com.fossil.no4;
import com.fossil.pq7;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRemoteDataSource extends MicroAppVariantDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ no4 mAppExecutors;
    @DexIgnore
    public /* final */ ShortcutApiService mShortcutApiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppVariantRemoteDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            pq7.c(str, "<set-?>");
            MicroAppVariantRemoteDataSource.TAG = str;
        }
    }

    /*
    static {
        String simpleName = MicroAppVariantRemoteDataSource.class.getSimpleName();
        pq7.b(simpleName, "MicroAppVariantRemoteDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppVariantRemoteDataSource(ShortcutApiService shortcutApiService, no4 no4) {
        pq7.c(shortcutApiService, "mShortcutApiService");
        pq7.c(no4, "mAppExecutors");
        this.mShortcutApiService = shortcutApiService;
        this.mAppExecutors = no4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource
    public void getAllMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        pq7.c(str, "serialNumber");
        String str2 = TAG;
        MFLogger.d(str2, "getAllMicroAppVariants serialNumber=" + str);
        getAllMicroAppVariants$app_fossilRelease(str, i, i2, 0, 100, new MicroAppVariantRemoteDataSource$getAllMicroAppVariants$resultCallback$Anon1(this, str, new ArrayList(), i, i2, getVariantListCallback));
    }

    @DexIgnore
    public final void getAllMicroAppVariants$app_fossilRelease(String str, int i, int i2, int i3, int i4, c88<gj4> c88) {
        pq7.c(str, "serialNumber");
        pq7.c(c88, Constants.CALLBACK);
    }
}
