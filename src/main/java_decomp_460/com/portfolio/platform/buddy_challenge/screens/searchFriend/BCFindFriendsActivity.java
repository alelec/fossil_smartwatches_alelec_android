package com.portfolio.platform.buddy_challenge.screens.searchFriend;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.sw4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCFindFriendsActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            pq7.c(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), BCFindFriendsActivity.class);
            intent.putExtra("challenge_history_id_extra", str);
            fragment.startActivityForResult(intent, 16);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((sw4) getSupportFragmentManager().Y(2131362158)) == null) {
            k(sw4.t.b(getIntent().getStringExtra("challenge_history_id_extra")), sw4.t.a(), 2131362158);
        }
    }
}
