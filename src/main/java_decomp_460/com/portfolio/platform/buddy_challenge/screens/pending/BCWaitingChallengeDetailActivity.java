package com.portfolio.platform.buddy_challenge.screens.pending;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.lw4;
import com.fossil.pq7;
import com.fossil.ps4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCWaitingChallengeDetailActivity extends ls5 {
    @DexIgnore
    public static /* final */ String A;
    @DexIgnore
    public static /* final */ a B; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, ps4 ps4, String str, int i, boolean z) {
            pq7.c(fragment, "fragment");
            pq7.c(str, "category");
            Intent intent = new Intent(fragment.getContext(), BCWaitingChallengeDetailActivity.class);
            intent.putExtra("challenge_extra", ps4);
            intent.putExtra("category_extra", str);
            intent.putExtra("index_extra", i);
            intent.putExtra("about_extra", z);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = BCWaitingChallengeDetailActivity.A;
            local.e(str2, "startActivityForResult - challenge: " + ps4 + " - category: " + str + " - index: " + i);
            fragment.startActivityForResult(intent, 13);
        }
    }

    /*
    static {
        String simpleName = BCWaitingChallengeDetailActivity.class.getSimpleName();
        pq7.b(simpleName, "BCWaitingChallengeDetail\u2026ty::class.java.simpleName");
        A = simpleName;
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        Fragment Y = getSupportFragmentManager().Y(2131362158);
        if (!(Y instanceof lw4)) {
            Y = null;
        }
        lw4 lw4 = (lw4) Y;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String r = r();
        local.e(r, "onCreate - fragment: " + lw4);
        if (lw4 == null) {
            ps4 ps4 = (ps4) getIntent().getParcelableExtra("challenge_extra");
            String stringExtra = getIntent().getStringExtra("category_extra");
            int intExtra = getIntent().getIntExtra("index_extra", -1);
            String stringExtra2 = getIntent().getStringExtra("challenge_id_extra");
            boolean booleanExtra = getIntent().getBooleanExtra("about_extra", false);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str = A;
            local2.e(str, "onCreate - challenge: " + ps4 + " - category: " + stringExtra + " - index: " + intExtra + " - visitId: " + stringExtra2 + " - about: " + booleanExtra);
            k(lw4.D.b(ps4, stringExtra, intExtra, stringExtra2, booleanExtra), lw4.D.a(), 2131362158);
        }
    }
}
