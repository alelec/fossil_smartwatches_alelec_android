package com.portfolio.platform.buddy_challenge.screens.memeber;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.sv4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCMemberInChallengeActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, boolean z) {
            pq7.c(fragment, "fragment");
            pq7.c(str, "challengeId");
            Intent intent = new Intent(fragment.getContext(), BCMemberInChallengeActivity.class);
            intent.putExtra("challenge_id_extra", str);
            intent.putExtra("challenge_status_extra", z);
            fragment.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((sv4) getSupportFragmentManager().Y(2131362158)) == null) {
            k(sv4.t.b(getIntent().getStringExtra("challenge_id_extra"), getIntent().getBooleanExtra("challenge_status_extra", true)), sv4.t.a(), 2131362158);
        }
    }
}
