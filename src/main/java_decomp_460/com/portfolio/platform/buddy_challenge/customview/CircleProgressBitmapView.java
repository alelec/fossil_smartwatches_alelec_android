package com.portfolio.platform.buddy_challenge.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import androidx.annotation.Keep;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.bz4;
import com.fossil.gl0;
import com.fossil.il7;
import com.fossil.pq7;
import com.fossil.sq4;
import com.fossil.xy4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CircleProgressBitmapView extends View implements bz4 {
    @DexIgnore
    public static /* final */ String A;
    @DexIgnore
    public /* final */ short b; // = ((short) -90);
    @DexIgnore
    public /* final */ PorterDuffXfermode c; // = new PorterDuffXfermode(PorterDuff.Mode.DST_OUT);
    @DexIgnore
    public /* final */ PorterDuffXfermode d; // = new PorterDuffXfermode(PorterDuff.Mode.DST_OVER);
    @DexIgnore
    public /* final */ SparseArray<WeakReference<Bitmap>> e; // = new SparseArray<>();
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public Bitmap h;
    @DexIgnore
    public Bitmap i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public /* final */ Rect l; // = new Rect();
    @DexIgnore
    @Keep
    public float lackingAngle; // = -360.0f;
    @DexIgnore
    public /* final */ Rect m; // = new Rect();
    @DexIgnore
    public /* final */ RectF s; // = new RectF();
    @DexIgnore
    public Paint t;
    @DexIgnore
    public int u;
    @DexIgnore
    public long v;
    @DexIgnore
    public long w;
    @DexIgnore
    public long x;
    @DexIgnore
    public CountDownTimer y;
    @DexIgnore
    public TimerViewObserver z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends CountDownTimer {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ CircleProgressBitmapView f4688a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(CircleProgressBitmapView circleProgressBitmapView, long j, long j2, long j3, long j4) {
            super(j3, j4);
            this.f4688a = circleProgressBitmapView;
            this.b = j;
        }

        @DexIgnore
        public void onFinish() {
            this.f4688a.h(-1, 0);
        }

        @DexIgnore
        public void onTick(long j) {
            this.f4688a.h(j, this.b);
        }
    }

    /*
    static {
        String simpleName = CircleProgressBitmapView.class.getSimpleName();
        pq7.b(simpleName, "CircleProgressBitmapView::class.java.simpleName");
        A = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleProgressBitmapView(Context context) {
        super(context);
        pq7.c(context, "context");
        new PorterDuffColorFilter(0, PorterDuff.Mode.SRC_IN);
        f(context, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleProgressBitmapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        pq7.c(context, "context");
        new PorterDuffColorFilter(0, PorterDuff.Mode.SRC_IN);
        f(context, attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleProgressBitmapView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        pq7.c(context, "context");
        new PorterDuffColorFilter(0, PorterDuff.Mode.SRC_IN);
        f(context, attributeSet);
    }

    @DexIgnore
    public final Bitmap a(Context context, int i2) {
        WeakReference<Bitmap> weakReference = this.e.get(i2);
        if ((weakReference != null ? weakReference.get() : null) == null) {
            Drawable f2 = gl0.f(context, i2);
            if (f2 != null) {
                WeakReference<Bitmap> weakReference2 = new WeakReference<>(((BitmapDrawable) f2).getBitmap());
                this.e.put(i2, weakReference2);
                weakReference = weakReference2;
            } else {
                throw new il7("null cannot be cast to non-null type android.graphics.drawable.BitmapDrawable");
            }
        }
        return weakReference.get();
    }

    @DexIgnore
    public final float c(long j2, long j3) {
        return (((float) 1) - ((j2 <= 0 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : (j3 <= 0 || j2 >= j3) ? 100.0f : (((float) 100) * ((float) j2)) / ((float) j3)) / 100.0f)) * ((float) -360);
    }

    @DexIgnore
    public final void d(long j2, long j3) {
        CountDownTimer countDownTimer = this.y;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        a aVar = new a(this, j2, j3, j3, this.x);
        this.y = aVar;
        if (aVar != null) {
            aVar.start();
        }
    }

    @DexIgnore
    public final int e() {
        return System.identityHashCode(this);
    }

    @DexIgnore
    public final void f(Context context, AttributeSet attributeSet) {
        Paint paint = new Paint(1);
        this.t = paint;
        if (paint != null) {
            paint.setFilterBitmap(true);
            Paint paint2 = this.t;
            if (paint2 != null) {
                paint2.setColor(-16777216);
                TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.CircleProgressView);
                int resourceId = obtainStyledAttributes.getResourceId(10, 2131231035);
                this.f = resourceId;
                this.h = a(context, resourceId);
                int resourceId2 = obtainStyledAttributes.getResourceId(1, 2131231035);
                this.g = resourceId2;
                this.i = a(context, resourceId2);
                obtainStyledAttributes.recycle();
                return;
            }
            pq7.n("paint");
            throw null;
        }
        pq7.n("paint");
        throw null;
    }

    @DexIgnore
    public final void g(long j2, long j3) {
        this.v = j2;
        this.w = j3;
        this.x = j2 <= 3600000 ? ButtonService.CONNECT_TIMEOUT : 30000;
        d(j2, this.w - xy4.f4212a.b());
    }

    @DexIgnore
    public final void h(long j2, long j3) {
        if (j2 < 0) {
            j2 = 0;
        }
        if (j3 < 0) {
            j3 = 1000;
        }
        this.lackingAngle = c(j2, j3);
        invalidate();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        pq7.c(canvas, "canvas");
        super.onDraw(canvas);
        if (this.k > 0) {
            int saveLayer = canvas.saveLayer(this.s, null, 31);
            Rect rect = this.l;
            Bitmap bitmap = this.h;
            if (bitmap != null) {
                int width = bitmap.getWidth();
                Bitmap bitmap2 = this.h;
                if (bitmap2 != null) {
                    rect.set(0, 0, width, bitmap2.getHeight());
                    Bitmap bitmap3 = this.h;
                    if (bitmap3 != null) {
                        Rect rect2 = this.l;
                        Rect rect3 = this.m;
                        Paint paint = this.t;
                        if (paint != null) {
                            canvas.drawBitmap(bitmap3, rect2, rect3, paint);
                            Paint paint2 = this.t;
                            if (paint2 != null) {
                                paint2.setColorFilter(null);
                                Paint paint3 = this.t;
                                if (paint3 != null) {
                                    paint3.setXfermode(this.c);
                                    RectF rectF = this.s;
                                    float f2 = (float) this.b;
                                    float f3 = this.lackingAngle;
                                    Paint paint4 = this.t;
                                    if (paint4 != null) {
                                        canvas.drawArc(rectF, f2, f3, true, paint4);
                                        Paint paint5 = this.t;
                                        if (paint5 != null) {
                                            paint5.setXfermode(this.d);
                                            Rect rect4 = this.l;
                                            Bitmap bitmap4 = this.i;
                                            if (bitmap4 != null) {
                                                int width2 = bitmap4.getWidth();
                                                Bitmap bitmap5 = this.i;
                                                if (bitmap5 != null) {
                                                    rect4.set(0, 0, width2, bitmap5.getHeight());
                                                    Bitmap bitmap6 = this.i;
                                                    if (bitmap6 != null) {
                                                        Rect rect5 = this.l;
                                                        Rect rect6 = this.m;
                                                        Paint paint6 = this.t;
                                                        if (paint6 != null) {
                                                            canvas.drawBitmap(bitmap6, rect5, rect6, paint6);
                                                            Paint paint7 = this.t;
                                                            if (paint7 != null) {
                                                                paint7.setXfermode(null);
                                                                canvas.restoreToCount(saveLayer);
                                                                return;
                                                            }
                                                            pq7.n("paint");
                                                            throw null;
                                                        }
                                                        pq7.n("paint");
                                                        throw null;
                                                    }
                                                    pq7.i();
                                                    throw null;
                                                }
                                                pq7.i();
                                                throw null;
                                            }
                                            pq7.i();
                                            throw null;
                                        }
                                        pq7.n("paint");
                                        throw null;
                                    }
                                    pq7.n("paint");
                                    throw null;
                                }
                                pq7.n("paint");
                                throw null;
                            }
                            pq7.n("paint");
                            throw null;
                        }
                        pq7.n("paint");
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.bz4
    public void onPause() {
        FLogger.INSTANCE.getLocal().e(A, "onPause");
        CountDownTimer countDownTimer = this.y;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @DexIgnore
    @Override // com.fossil.bz4
    public void onResume() {
        FLogger.INSTANCE.getLocal().e(A, "onResume");
        d(this.v, this.w - xy4.f4212a.b());
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        int i6 = this.j * 2;
        if (i2 <= i6 || i3 <= i6) {
            this.k = 0;
            return;
        }
        this.k = i3 - i6;
        int width = getWidth();
        int i7 = this.k;
        int i8 = (width - i7) / 2;
        Rect rect = this.m;
        int i9 = this.j;
        rect.set(i8, i9, i8 + i7, i7 + i9);
        int i10 = this.j;
        int i11 = this.k;
        this.s.set((float) i8, (float) i10, (float) (i8 + i11), (float) (i10 + i11));
    }

    @DexIgnore
    public final void setBackgroundDrawable(int i2) {
        this.g = i2;
        Context context = getContext();
        pq7.b(context, "context");
        this.i = a(context, this.g);
        invalidate();
    }

    @DexIgnore
    public final void setColour(int i2) {
        this.u = i2;
        new PorterDuffColorFilter(this.u, PorterDuff.Mode.SRC_IN);
        invalidate();
    }

    @DexIgnore
    public final void setObserver(TimerViewObserver timerViewObserver) {
        if (this.z == null) {
            this.z = timerViewObserver;
            if (timerViewObserver != null) {
                timerViewObserver.c(this, e());
            }
        }
    }

    @DexIgnore
    public final void setProgressDrawable(int i2) {
        this.f = i2;
        Context context = getContext();
        pq7.b(context, "context");
        this.h = a(context, this.f);
        invalidate();
    }
}
