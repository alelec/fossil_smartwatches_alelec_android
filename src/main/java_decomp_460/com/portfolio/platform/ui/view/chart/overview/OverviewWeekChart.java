package com.portfolio.platform.ui.view.chart.overview;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.al7;
import com.fossil.ar7;
import com.fossil.cl7;
import com.fossil.dl5;
import com.fossil.hm7;
import com.fossil.il7;
import com.fossil.mn7;
import com.fossil.mv5;
import com.fossil.ov5;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.qn5;
import com.fossil.um5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class OverviewWeekChart extends BarChart {
    @DexIgnore
    public mv5 A0;
    @DexIgnore
    public PointF x0;
    @DexIgnore
    public ObjectAnimator y0;
    @DexIgnore
    public ObjectAnimator z0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Animator.AnimatorListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ OverviewWeekChart f4728a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(OverviewWeekChart overviewWeekChart) {
            this.f4728a = overviewWeekChart;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.f4728a.getTAG(), "changeModel - onAnimationCancel");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = this.f4728a.getTAG();
            StringBuilder sb = new StringBuilder();
            sb.append("changeModel - onAnimationEnd -- isRunning=");
            ObjectAnimator objectAnimator = this.f4728a.y0;
            sb.append(objectAnimator != null ? Boolean.valueOf(objectAnimator.isRunning()) : null);
            local.d(tag, sb.toString());
            OverviewWeekChart overviewWeekChart = this.f4728a;
            mv5 mv5 = overviewWeekChart.A0;
            if (mv5 != null) {
                overviewWeekChart.I(mv5);
                ObjectAnimator objectAnimator2 = this.f4728a.z0;
                if (objectAnimator2 != null) {
                    objectAnimator2.start();
                    return;
                }
                return;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.f4728a.getTAG(), "changeModel - onAnimationRepeat");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.f4728a.getTAG(), "changeModel - onAnimationStart");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return mn7.c(t.c(), t2.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return mn7.c(t.c(), t2.c());
        }
    }

    @DexIgnore
    public OverviewWeekChart(Context context) {
        this(context, null);
    }

    @DexIgnore
    public OverviewWeekChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewWeekChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewWeekChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.x0 = new PointF();
    }

    @DexIgnore
    public static /* synthetic */ ObjectAnimator O(OverviewWeekChart overviewWeekChart, int i, int i2, int i3, int i4, int i5, Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewWeekChart.N(i, i2, i3, i4);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createInAnim");
    }

    @DexIgnore
    public static /* synthetic */ ObjectAnimator Q(OverviewWeekChart overviewWeekChart, int i, int i2, int i3, int i4, int i5, Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewWeekChart.P(i, i2, i3, i4);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createOutAnim");
    }

    @DexIgnore
    public String M(int i) {
        float f = (float) i;
        String valueOf = String.valueOf((int) f);
        float f2 = (float) 1000;
        if (f < f2) {
            return valueOf;
        }
        float f3 = f / f2;
        return dl5.b(f3, 1) + um5.c(getContext(), 2131886674);
    }

    @DexIgnore
    public final ObjectAnimator N(int i, int i2, int i3, int i4) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, PropertyValuesHolder.ofInt("maxValue", i4 * i, i), PropertyValuesHolder.ofInt("barAlpha", i2, i3));
        pq7.b(ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026his, inMaxValue, inAlpha)");
        ofPropertyValuesHolder.setDuration(200L);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    public final ObjectAnimator P(int i, int i2, int i3, int i4) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, PropertyValuesHolder.ofInt("maxValue", i, i4 * i), PropertyValuesHolder.ofInt("barAlpha", i2, i3));
        pq7.b(ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026s, outMaxValue, outAlpha)");
        ofPropertyValuesHolder.setDuration(200L);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart, com.portfolio.platform.ui.view.chart.base.BaseChart
    public void a() {
        super.a();
        setMNumberBar(7);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void c(Canvas canvas) {
        Bitmap t;
        pq7.c(canvas, "canvas");
        super.c(canvas);
        Iterator<BarChart.a> it = getMChartModel().b().iterator();
        while (it.hasNext()) {
            BarChart.a next = it.next();
            ArrayList<BarChart.b> arrayList = next.d().get(0);
            pq7.b(arrayList, "item.mListOfBarPoints[0]");
            List b0 = pm7.b0(arrayList, new c());
            if ((!b0.isEmpty()) && next.e() && (t = BarChart.t(this, getMLegendIconRes(), 0, 2, null)) != null) {
                RectF a2 = ((BarChart.b) b0.get(0)).a();
                canvas.drawBitmap(t, a2.left + ((a2.width() - ((float) t.getWidth())) * 0.5f), a2.bottom + ((float) getMTextMargin()), new Paint(1));
                t.recycle();
            }
        }
        v(canvas);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart, com.portfolio.platform.ui.view.chart.base.BaseChart
    public void f(Canvas canvas) {
        pq7.c(canvas, "canvas");
        m();
        n();
        x(canvas);
        u(canvas);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void n() {
        int i;
        T t;
        ArrayList<BarChart.a> b2 = getMChartModel().b();
        RectF rectF = new RectF(getMBarMargin(), getMSafeAreaHeight(), ((float) getMGraphWidth()) - getMBarMarginEnd(), (float) getMGraphHeight());
        float height = rectF.height();
        float f = rectF.left;
        ar7 ar7 = new ar7();
        ar7 ar72 = new ar7();
        ar7 ar73 = new ar7();
        Iterator<T> it = b2.iterator();
        if (!it.hasNext()) {
            t = null;
        } else {
            T next = it.next();
            if (!it.hasNext()) {
                t = next;
            } else {
                ArrayList<BarChart.b> arrayList = next.d().get(0);
                pq7.b(arrayList, "it.mListOfBarPoints[0]");
                Iterator<T> it2 = arrayList.iterator();
                int i2 = 0;
                while (true) {
                    i = i2;
                    if (!it2.hasNext()) {
                        break;
                    }
                    i2 = it2.next().e() + i;
                }
                do {
                    next = it.next();
                    ArrayList<BarChart.b> arrayList2 = next.d().get(0);
                    pq7.b(arrayList2, "it.mListOfBarPoints[0]");
                    Iterator<T> it3 = arrayList2.iterator();
                    int i3 = 0;
                    while (true) {
                        i = i3;
                        if (!it3.hasNext()) {
                            break;
                        }
                        i3 = it3.next().e() + i;
                    }
                    if (i >= i) {
                        i = i;
                        next = next;
                    }
                } while (it.hasNext());
                t = next;
            }
        }
        T t2 = t;
        int size = b2.size();
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f3 = 0.0f;
        boolean z = false;
        int i4 = 0;
        for (T t3 : b2) {
            if (i4 >= 0) {
                T t4 = t3;
                ArrayList<BarChart.b> arrayList3 = t4.d().get(0);
                pq7.b(arrayList3, "item.mListOfBarPoints[0]");
                float b3 = (float) t4.b();
                ar72.element = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                ar7.element = getMBarWidth() + f;
                Iterator<BarChart.b> it4 = arrayList3.iterator();
                while (it4.hasNext()) {
                    float e = ((float) it4.next().e()) + ar72.element;
                    ar72.element = e;
                    float mMaxValue = (e * height) / ((float) getMMaxValue());
                    if (mMaxValue != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && mMaxValue < getMBarRadius()) {
                        mMaxValue = getMBarRadius();
                    }
                    f2 = ((float) getMGraphHeight()) - mMaxValue;
                }
                setMGoalIconShow(true);
                float mGraphHeight = ((float) getMGraphHeight()) - ((b3 * height) / ((float) getMMaxValue()));
                float mBarSpace = ar7.element + (getMBarSpace() * 0.5f);
                ar73.element = mBarSpace;
                float f4 = rectF.right;
                if (mBarSpace > f4) {
                    ar73.element = (getMBarMargin() * 0.5f) + f4;
                }
                if (!z) {
                    getMGoalLinePath().moveTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, mGraphHeight);
                    getMGoalLinePath().lineTo(ar73.element, mGraphHeight);
                    z = true;
                } else {
                    getMGoalLinePath().lineTo(f3, mGraphHeight);
                    getMGoalLinePath().lineTo(ar73.element, mGraphHeight);
                    getMGoalIconPoint().set(ar73.element, mGraphHeight - (((float) getMGoalIconSize()) * 0.5f));
                }
                if (i4 == size - 1) {
                    getMGoalLinePath().lineTo((float) getMGraphWidth(), mGraphHeight);
                    this.x0 = new PointF((float) getMGraphWidth(), mGraphHeight);
                }
                float f5 = ar73.element;
                if (pq7.a(t2, t4)) {
                    ArrayList<BarChart.b> arrayList4 = t4.d().get(0);
                    pq7.b(arrayList4, "item.mListOfBarPoints[0]");
                    Iterator<T> it5 = arrayList4.iterator();
                    int i5 = 0;
                    while (it5.hasNext()) {
                        i5 = it5.next().e() + i5;
                    }
                    if (i5 >= t4.b()) {
                        getMStarIconPoint().add(new PointF(f - ((float) getMStarIconSize()), f2 - (((float) getMStarIconSize()) * 1.25f)));
                        float mGraphHeight2 = ((((float) getMGraphHeight()) - f2) * 0.5f) + f2;
                        getMStarIconPoint().add(new PointF(ar7.element + (((float) getMStarIconSize()) * 0.5f), mGraphHeight2 - (((float) getMStarIconSize()) * 2.0f)));
                        getMStarIconPoint().add(new PointF(f - ((float) getMStarIconSize()), mGraphHeight2));
                    }
                }
                f3 = f5;
                i4++;
                f = getMBarSpace() + ar7.element;
            } else {
                hm7.l();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void p(float f, float f2) {
        float f3;
        Rect rect = new Rect();
        getMLegendPaint().getTextBounds("gh", 0, 2, rect);
        float mLegendHeight = (float) (getMLegendHeight() + rect.height());
        int size = getMLegendTexts().size();
        int i = 0;
        float f4 = f;
        while (i < 7) {
            if (i < size) {
                String str = getMLegendTexts().get(i);
                pq7.b(str, "mLegendTexts[i]");
                String str2 = str;
                getMLegendPaint().getTextBounds(str2, 0, str2.length(), rect);
                float mBarWidth = getMBarWidth() + f4;
                getMTextPoint().add(new cl7<>(str2, new PointF(((f4 + mBarWidth) - ((float) rect.width())) * 0.5f, mLegendHeight * 0.5f)));
                f3 = getMBarSpace() + mBarWidth;
            } else {
                f3 = f4;
            }
            i++;
            f4 = f3;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void r(mv5 mv5) {
        synchronized (this) {
            pq7.c(mv5, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            StringBuilder sb = new StringBuilder();
            sb.append("changeModel - model=");
            sb.append(mv5);
            sb.append(", mOutAnim.isRunning=");
            ObjectAnimator objectAnimator = this.y0;
            sb.append(objectAnimator != null ? Boolean.valueOf(objectAnimator.isRunning()) : null);
            sb.append(", mInAnim.isRunning=");
            ObjectAnimator objectAnimator2 = this.z0;
            sb.append(objectAnimator2 != null ? Boolean.valueOf(objectAnimator2.isRunning()) : null);
            local.d(tag, sb.toString());
            ObjectAnimator objectAnimator3 = this.y0;
            Boolean valueOf = objectAnimator3 != null ? Boolean.valueOf(objectAnimator3.isRunning()) : null;
            ObjectAnimator objectAnimator4 = this.z0;
            Boolean valueOf2 = objectAnimator4 != null ? Boolean.valueOf(objectAnimator4.isRunning()) : null;
            if (pq7.a(valueOf, Boolean.TRUE) || pq7.a(valueOf2, Boolean.TRUE)) {
                if (pq7.a(mv5, this.A0)) {
                    FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - model == mTempModel");
                    return;
                }
                this.A0 = mv5;
                if (pq7.a(valueOf, Boolean.TRUE)) {
                    FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - outRunning == true");
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String tag2 = getTAG();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("changeModel - outRunning == true - mMaxValue=");
                    mv5 mv52 = this.A0;
                    if (mv52 != null) {
                        sb2.append(((BarChart.c) mv52).d());
                        local2.d(tag2, sb2.toString());
                        mv5 mv53 = this.A0;
                        if (mv53 != null) {
                            this.z0 = O(this, ((BarChart.c) mv53).d(), 0, 255, 0, 8, null);
                        } else {
                            throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                        }
                    } else {
                        throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - inRunning == true");
                    ObjectAnimator objectAnimator5 = this.z0;
                    if (objectAnimator5 != null) {
                        objectAnimator5.cancel();
                    }
                    int mMaxValue = getMMaxValue();
                    int d = getMChartModel().d();
                    int mBarAlpha = getMBarAlpha();
                    if (d <= 0) {
                        d = 1;
                    }
                    int i = mMaxValue / d;
                    mv5 mv54 = this.A0;
                    if (mv54 != null) {
                        I(mv54);
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String tag3 = getTAG();
                        local3.d(tag3, "changeModel - inRunning == true -- tempStartMaxValue=" + mMaxValue + ", tempEndMaxValue=" + mMaxValue + ", tempAlpha=" + mMaxValue + ", tempMaxRate=" + mMaxValue + ", newMaxValue=" + getMChartModel().d());
                        ObjectAnimator N = N(getMChartModel().d(), mBarAlpha, 255, i);
                        this.z0 = N;
                        if (N != null) {
                            N.start();
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            } else if (pq7.a(getMChartModel(), mv5)) {
                FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - mChartModel == model");
            } else {
                this.A0 = mv5;
                this.y0 = Q(this, getMMaxValue(), 255, 0, 0, 8, null);
                mv5 mv55 = this.A0;
                if (mv55 != null) {
                    this.z0 = O(this, ((BarChart.c) mv55).d(), 0, 255, 0, 8, null);
                    ObjectAnimator objectAnimator6 = this.y0;
                    if (objectAnimator6 != null) {
                        objectAnimator6.addListener(new a(this));
                    }
                    ObjectAnimator objectAnimator7 = this.y0;
                    if (objectAnimator7 != null) {
                        objectAnimator7.start();
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                }
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void u(Canvas canvas) {
        int i;
        pq7.c(canvas, "canvas");
        Iterator<BarChart.a> it = getMChartModel().b().iterator();
        while (it.hasNext()) {
            BarChart.a next = it.next();
            ArrayList<BarChart.b> arrayList = next.d().get(0);
            pq7.b(arrayList, "item.mListOfBarPoints[0]");
            Iterator it2 = pm7.b0(arrayList, new b()).iterator();
            while (true) {
                if (it2.hasNext()) {
                    BarChart.b bVar = (BarChart.b) it2.next();
                    if (bVar.e() != 0) {
                        Paint mGraphPaint = getMGraphPaint();
                        if (next.b() <= 0 || bVar.e() < next.b()) {
                            i = getMInActiveColor();
                        } else {
                            int i2 = ov5.f2728a[bVar.c().ordinal()];
                            if (i2 == 1) {
                                i = getMLowestColor();
                            } else if (i2 == 2) {
                                i = getMDefaultColor();
                            } else if (i2 == 3) {
                                i = getMHighestColor();
                            } else {
                                throw new al7();
                            }
                        }
                        mGraphPaint.setColor(i);
                        canvas.drawRoundRect(bVar.a(), getMBarRadius(), getMBarRadius(), getMGraphPaint());
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void w(Canvas canvas) {
        String d;
        pq7.c(canvas, "canvas");
        float width = (float) canvas.getWidth();
        if (getMGoalIconShow()) {
            canvas.drawPath(getMGoalLinePath(), getMGraphGoalLinePaint());
            int size = getMChartModel().b().size();
            if (size > 0) {
                Rect rect = new Rect();
                String M = M(getMChartModel().b().get(size - 1).b());
                getMLegendPaint().getTextBounds(M, 0, M.length(), rect);
                getMLegendPaint().setColor(getMActiveColor());
                canvas.drawText(M, (width - getMGraphLegendMargin()) - ((float) rect.width()), ((float) rect.height()) + this.x0.y + getMGraphLegendMargin(), getMLegendPaint());
                if (!TextUtils.isEmpty(getMTextColor()) && (d = qn5.l.a().d(getMTextColor())) != null) {
                    getMLegendPaint().setColor(Color.parseColor(d));
                }
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void x(Canvas canvas) {
        pq7.c(canvas, "canvas");
        float width = (float) canvas.getWidth();
        Iterator<cl7<Integer, PointF>> it = getMGraphLegendPoint().iterator();
        while (it.hasNext()) {
            cl7<Integer, PointF> next = it.next();
            Rect rect = new Rect();
            String M = M(next.getFirst().intValue());
            getMLegendPaint().getTextBounds(M, 0, M.length(), rect);
            float f = next.getSecond().y;
            canvas.drawLine(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f, width, f, getMLegendLinePaint());
            canvas.drawText(M, (width - getMGraphLegendMargin()) - ((float) rect.width()), getMGraphLegendMargin() + f + ((float) rect.height()), getMLegendPaint());
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void y(Canvas canvas) {
        pq7.c(canvas, "canvas");
    }
}
