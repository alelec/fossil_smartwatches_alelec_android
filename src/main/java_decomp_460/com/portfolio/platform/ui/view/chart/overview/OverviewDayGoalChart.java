package com.portfolio.platform.ui.view.chart.overview;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import com.fossil.mn7;
import com.fossil.pm7;
import com.fossil.pq7;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OverviewDayGoalChart extends OverviewDayChart {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return mn7.c(t.c(), t2.c());
        }
    }

    @DexIgnore
    public OverviewDayGoalChart(Context context) {
        this(context, null);
    }

    @DexIgnore
    public OverviewDayGoalChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewDayGoalChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewDayGoalChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart, com.portfolio.platform.ui.view.chart.overview.OverviewDayChart
    public void u(Canvas canvas) {
        Object obj;
        pq7.c(canvas, "canvas");
        getMGraphPaint().setColor(getMDefaultColor());
        Iterator<BarChart.a> it = getMChartModel().b().iterator();
        while (it.hasNext()) {
            ArrayList<BarChart.b> arrayList = it.next().d().get(0);
            pq7.b(arrayList, "item.mListOfBarPoints[0]");
            Iterator it2 = pm7.b0(arrayList, new a()).iterator();
            if (!it2.hasNext()) {
                obj = null;
            } else {
                Object next = it2.next();
                if (it2.hasNext()) {
                    int e = ((BarChart.b) next).e();
                    while (true) {
                        next = it2.next();
                        e = ((BarChart.b) next).e();
                        if (e >= e) {
                            e = e;
                            next = next;
                        }
                        if (!it2.hasNext()) {
                            break;
                        }
                    }
                }
                obj = next;
            }
            BarChart.b bVar = (BarChart.b) obj;
            if (bVar != null) {
                canvas.drawRoundRect(bVar.a(), getMBarRadius(), getMBarRadius(), getMGraphPaint());
            }
        }
    }
}
