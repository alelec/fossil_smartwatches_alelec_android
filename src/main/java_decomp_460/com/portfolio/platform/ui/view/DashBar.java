package com.portfolio.platform.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.Keep;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.qn5;
import com.fossil.sq4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashBar extends View {
    @DexIgnore
    public Paint b; // = new Paint(1);
    @DexIgnore
    public Paint c; // = new Paint(1);
    @DexIgnore
    public float d; // = 10.0f;
    @DexIgnore
    public float e; // = 6.0f;
    @DexIgnore
    public int f; // = 4;
    @DexIgnore
    public String g; // = "primaryText";
    @DexIgnore
    public String h; // = "secondaryText";
    @DexIgnore
    public String i; // = "primaryText";
    @DexIgnore
    public int j; // = 50;

    @DexIgnore
    public DashBar(Context context) {
        super(context, null);
    }

    @DexIgnore
    public DashBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context != null ? context.obtainStyledAttributes(attributeSet, sq4.DashBar) : null;
        if (obtainStyledAttributes != null) {
            this.d = obtainStyledAttributes.getDimension(6, 10.0f);
            this.e = obtainStyledAttributes.getDimension(3, 6.0f);
            this.f = obtainStyledAttributes.getInteger(4, 4);
            String string = obtainStyledAttributes.getString(2);
            this.g = string == null ? "" : string;
            String string2 = obtainStyledAttributes.getString(1);
            this.h = string2 == null ? "" : string2;
            String string3 = obtainStyledAttributes.getString(0);
            this.i = string3 == null ? "" : string3;
            this.j = obtainStyledAttributes.getInt(5, 0);
        }
        a(this.d);
        setLayerType(1, null);
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public final void a(float f2) {
        this.b.setDither(true);
        this.b.setStyle(Paint.Style.STROKE);
        this.b.setStrokeWidth(f2);
        this.b.setAntiAlias(true);
        this.b.setStrokeCap(Paint.Cap.ROUND);
        this.b.setStrokeJoin(Paint.Join.ROUND);
    }

    @DexIgnore
    public final void b(float f2) {
        if (!TextUtils.isEmpty(this.g) && !TextUtils.isEmpty(this.h) && !TextUtils.isEmpty(this.i)) {
            String d2 = qn5.l.a().d(this.g);
            String d3 = qn5.l.a().d(this.h);
            String d4 = qn5.l.a().d(this.i);
            int parseColor = d2 != null ? Color.parseColor(d2) : -16777216;
            int parseColor2 = d3 != null ? Color.parseColor(d3) : -16777216;
            int parseColor3 = d4 != null ? Color.parseColor(d4) : -16777216;
            this.b.setShader(new LinearGradient((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, parseColor, parseColor2, Shader.TileMode.CLAMP));
            Paint paint = this.b;
            int i2 = this.f;
            float f3 = this.e;
            paint.setPathEffect(new DashPathEffect(new float[]{((f2 - 20.0f) - (((float) (i2 - 1)) * f3)) / ((float) i2), f3}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            Paint paint2 = new Paint(this.b);
            this.c = paint2;
            paint2.setShader(new LinearGradient((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, parseColor3, parseColor3, Shader.TileMode.CLAMP));
        }
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        b((float) getWidth());
        if (canvas != null) {
            float height = ((float) canvas.getHeight()) / 2.0f;
            float width = (float) canvas.getWidth();
            canvas.drawLine(10.0f, height, width - 10.0f, height, this.c);
            canvas.drawLine(10.0f, height, ((((float) this.j) * width) / ((float) 100)) - 10.0f, height, this.b);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0020, code lost:
        if (r4 != 1073741824) goto L_0x0022;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r8, int r9) {
        /*
            r7 = this;
            r6 = 1073741824(0x40000000, float:2.0)
            r0 = 0
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            int r3 = android.view.View.MeasureSpec.getMode(r8)
            int r2 = android.view.View.MeasureSpec.getSize(r8)
            int r4 = android.view.View.MeasureSpec.getMode(r9)
            int r1 = android.view.View.MeasureSpec.getSize(r9)
            if (r3 == r5) goto L_0x001c
            if (r3 == 0) goto L_0x001c
            if (r3 == r6) goto L_0x001c
            r2 = r0
        L_0x001c:
            if (r4 == r5) goto L_0x002c
            if (r4 == 0) goto L_0x0026
            if (r4 == r6) goto L_0x002c
        L_0x0022:
            r7.setMeasuredDimension(r2, r0)
            return
        L_0x0026:
            float r0 = r7.d
            int r0 = (int) r0
            int r0 = r0 * 2
            goto L_0x0022
        L_0x002c:
            r0 = r1
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.view.DashBar.onMeasure(int, int):void");
    }

    @DexIgnore
    @Keep
    public final void setLength(int i2) {
        this.f = i2;
        invalidate();
    }

    @DexIgnore
    @Keep
    public final void setProgress(int i2) {
        this.j = i2;
        invalidate();
    }
}
