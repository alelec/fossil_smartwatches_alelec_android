package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityOverviewFragment$initUI$$inlined$let$lambda$Anon1 extends LinearLayoutManager {
    @DexIgnore
    public ActivityOverviewFragment$initUI$$inlined$let$lambda$Anon1(Context context, int i, boolean z, ActivityOverviewFragment activityOverviewFragment, ArrayList arrayList) {
        super(context, i, z);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.m, androidx.recyclerview.widget.LinearLayoutManager
    public boolean l() {
        return false;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.m, androidx.recyclerview.widget.LinearLayoutManager
    public boolean m() {
        return false;
    }
}
