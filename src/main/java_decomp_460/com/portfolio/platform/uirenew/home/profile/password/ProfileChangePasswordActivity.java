package com.portfolio.platform.uirenew.home.profile.password;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.sq6;
import com.fossil.tq6;
import com.fossil.vq6;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileChangePasswordActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public vq6 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            pq7.c(context, "context");
            Intent intent = new Intent(context, ProfileChangePasswordActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        sq6 sq6 = (sq6) getSupportFragmentManager().Y(2131362158);
        if (sq6 == null) {
            sq6 = sq6.A.a();
            i(sq6, 2131362158);
        }
        PortfolioApp.h0.c().M().y(new tq6(sq6)).a(this);
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onResume() {
        super.onResume();
        l(false);
    }
}
