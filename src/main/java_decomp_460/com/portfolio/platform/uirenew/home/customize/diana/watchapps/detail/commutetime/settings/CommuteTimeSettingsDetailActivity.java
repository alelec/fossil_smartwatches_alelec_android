package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.da6;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsDetailActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, Bundle bundle, int i) {
            pq7.c(fragment, "fragment");
            pq7.c(bundle, "bundle");
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsDetailActivity.class);
            intent.putExtra("KEY_BUNDLE_SETTING_ADDRESS", bundle);
            fragment.startActivityForResult(intent, i);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((da6) getSupportFragmentManager().Y(2131362158)) == null) {
            Bundle bundleExtra = getIntent().getBundleExtra("KEY_BUNDLE_SETTING_ADDRESS");
            k(da6.s.b((AddressWrapper) bundleExtra.getParcelable("KEY_SELECTED_ADDRESS"), bundleExtra.getStringArrayList("KEY_LIST_ADDRESS"), bundleExtra.getBoolean("KEY_HAVING_MAP_RESULT")), da6.s.a(), 2131362158);
        }
    }
}
