package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.aq0;
import com.fossil.c25;
import com.fossil.df6;
import com.fossil.ef6;
import com.fossil.g37;
import com.fossil.g67;
import com.fossil.if6;
import com.fossil.mo0;
import com.fossil.mv0;
import com.fossil.nk5;
import com.fossil.of6;
import com.fossil.pf6;
import com.fossil.pq7;
import com.fossil.pv5;
import com.fossil.qn5;
import com.fossil.ro4;
import com.fossil.uf6;
import com.fossil.vf6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityOverviewFragment extends pv5 {
    @DexIgnore
    public g37<c25> g;
    @DexIgnore
    public ef6 h;
    @DexIgnore
    public vf6 i;
    @DexIgnore
    public pf6 j;
    @DexIgnore
    public df6 k;
    @DexIgnore
    public uf6 l;
    @DexIgnore
    public of6 m;
    @DexIgnore
    public int s; // = 7;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewFragment b;

        @DexIgnore
        public a(ActivityOverviewFragment activityOverviewFragment) {
            this.b = activityOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActivityOverviewFragment activityOverviewFragment = this.b;
            g37 g37 = activityOverviewFragment.g;
            activityOverviewFragment.O6(7, g37 != null ? (c25) g37.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewFragment b;

        @DexIgnore
        public b(ActivityOverviewFragment activityOverviewFragment) {
            this.b = activityOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActivityOverviewFragment activityOverviewFragment = this.b;
            g37 g37 = activityOverviewFragment.g;
            activityOverviewFragment.O6(4, g37 != null ? (c25) g37.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewFragment b;

        @DexIgnore
        public c(ActivityOverviewFragment activityOverviewFragment) {
            this.b = activityOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActivityOverviewFragment activityOverviewFragment = this.b;
            g37 g37 = activityOverviewFragment.g;
            activityOverviewFragment.O6(2, g37 != null ? (c25) g37.a() : null);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "ActivityOverviewFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void M6(c25 c25) {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewFragment", "initUI");
        this.k = (df6) getChildFragmentManager().Z("ActivityOverviewDayFragment");
        this.l = (uf6) getChildFragmentManager().Z("ActivityOverviewWeekFragment");
        this.m = (of6) getChildFragmentManager().Z("ActivityOverviewMonthFragment");
        if (this.k == null) {
            this.k = new df6();
        }
        if (this.l == null) {
            this.l = new uf6();
        }
        if (this.m == null) {
            this.m = new of6();
        }
        ArrayList arrayList = new ArrayList();
        df6 df6 = this.k;
        if (df6 != null) {
            arrayList.add(df6);
            uf6 uf6 = this.l;
            if (uf6 != null) {
                arrayList.add(uf6);
                of6 of6 = this.m;
                if (of6 != null) {
                    arrayList.add(of6);
                    RecyclerView recyclerView = c25.w;
                    pq7.b(recyclerView, "it");
                    recyclerView.setAdapter(new g67(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new ActivityOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new mv0().b(recyclerView);
                    O6(this.s, c25);
                    ro4 M = PortfolioApp.h0.c().M();
                    df6 df62 = this.k;
                    if (df62 != null) {
                        uf6 uf62 = this.l;
                        if (uf62 != null) {
                            of6 of62 = this.m;
                            if (of62 != null) {
                                M.h0(new if6(df62, uf62, of62)).a(this);
                                c25.u.setOnClickListener(new a(this));
                                c25.r.setOnClickListener(new b(this));
                                c25.s.setOnClickListener(new c(this));
                                return;
                            }
                            pq7.i();
                            throw null;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void N6() {
        g37<c25> g37;
        c25 a2;
        g37<c25> g372;
        c25 a3;
        ConstraintLayout constraintLayout;
        g37<c25> g373;
        c25 a4;
        FlexibleTextView flexibleTextView;
        g37<c25> g374;
        c25 a5;
        nk5.a aVar = nk5.o;
        ef6 ef6 = this.h;
        if (ef6 != null) {
            String d = aVar.w(ef6.n()) ? qn5.l.a().d("dianaStepsTab") : qn5.l.a().d("hybridStepsTab");
            nk5.a aVar2 = nk5.o;
            ef6 ef62 = this.h;
            if (ef62 != null) {
                String d2 = aVar2.w(ef62.n()) ? qn5.l.a().d("onDianaStepsTab") : qn5.l.a().d("onHybridStepsTab");
                if (!(d == null || (g374 = this.g) == null || (a5 = g374.a()) == null)) {
                    a5.x.setBackgroundColor(Color.parseColor(d));
                    a5.y.setBackgroundColor(Color.parseColor(d));
                }
                if (!(d2 == null || (g373 = this.g) == null || (a4 = g373.a()) == null || (flexibleTextView = a4.t) == null)) {
                    flexibleTextView.setTextColor(Color.parseColor(d2));
                }
                nk5.a aVar3 = nk5.o;
                ef6 ef63 = this.h;
                if (ef63 != null) {
                    this.t = aVar3.w(ef63.n()) ? qn5.l.a().d("onDianaInactiveTab") : qn5.l.a().d("onHybridInactiveTab");
                    String d3 = qn5.l.a().d("nonBrandSurface");
                    this.u = qn5.l.a().d("primaryText");
                    if (!(d3 == null || (g372 = this.g) == null || (a3 = g372.a()) == null || (constraintLayout = a3.q) == null)) {
                        constraintLayout.setBackgroundColor(Color.parseColor(d3));
                    }
                    if (!TextUtils.isEmpty(this.t) && !TextUtils.isEmpty(this.u) && (g37 = this.g) != null && (a2 = g37.a()) != null) {
                        FlexibleTextView flexibleTextView2 = a2.u;
                        pq7.b(flexibleTextView2, "it.ftvToday");
                        if (flexibleTextView2.isSelected()) {
                            a2.u.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.u.setTextColor(Color.parseColor(this.t));
                        }
                        FlexibleTextView flexibleTextView3 = a2.r;
                        pq7.b(flexibleTextView3, "it.ftv7Days");
                        if (flexibleTextView3.isSelected()) {
                            a2.r.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.r.setTextColor(Color.parseColor(this.t));
                        }
                        FlexibleTextView flexibleTextView4 = a2.s;
                        pq7.b(flexibleTextView4, "it.ftvMonth");
                        if (flexibleTextView4.isSelected()) {
                            a2.s.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.s.setTextColor(Color.parseColor(this.t));
                        }
                    }
                } else {
                    pq7.n("mActivityOverviewDayPresenter");
                    throw null;
                }
            } else {
                pq7.n("mActivityOverviewDayPresenter");
                throw null;
            }
        } else {
            pq7.n("mActivityOverviewDayPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void O6(int i2, c25 c25) {
        c25 a2;
        RecyclerView recyclerView;
        g37<c25> g37;
        c25 a3;
        c25 a4;
        RecyclerView recyclerView2;
        c25 a5;
        RecyclerView recyclerView3;
        c25 a6;
        RecyclerView recyclerView4;
        if (c25 != null) {
            FlexibleTextView flexibleTextView = c25.u;
            pq7.b(flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = c25.r;
            pq7.b(flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = c25.s;
            pq7.b(flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = c25.u;
            pq7.b(flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = c25.r;
            pq7.b(flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = c25.s;
            pq7.b(flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i2 == 2) {
                FlexibleTextView flexibleTextView7 = c25.s;
                pq7.b(flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = c25.s;
                pq7.b(flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = c25.r;
                pq7.b(flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                g37<c25> g372 = this.g;
                if (!(g372 == null || (a2 = g372.a()) == null || (recyclerView = a2.w) == null)) {
                    recyclerView.scrollToPosition(2);
                }
            } else if (i2 == 4) {
                FlexibleTextView flexibleTextView10 = c25.r;
                pq7.b(flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = c25.r;
                pq7.b(flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = c25.r;
                pq7.b(flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                g37<c25> g373 = this.g;
                if (!(g373 == null || (a4 = g373.a()) == null || (recyclerView2 = a4.w) == null)) {
                    recyclerView2.scrollToPosition(1);
                }
            } else if (i2 != 7) {
                FlexibleTextView flexibleTextView13 = c25.u;
                pq7.b(flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = c25.u;
                pq7.b(flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = c25.r;
                pq7.b(flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                g37<c25> g374 = this.g;
                if (!(g374 == null || (a6 = g374.a()) == null || (recyclerView4 = a6.w) == null)) {
                    recyclerView4.scrollToPosition(0);
                }
            } else {
                FlexibleTextView flexibleTextView16 = c25.u;
                pq7.b(flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = c25.u;
                pq7.b(flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = c25.r;
                pq7.b(flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                g37<c25> g375 = this.g;
                if (!(g375 == null || (a5 = g375.a()) == null || (recyclerView3 = a5.w) == null)) {
                    recyclerView3.scrollToPosition(0);
                }
            }
            if (!TextUtils.isEmpty(this.t) && !TextUtils.isEmpty(this.u) && (g37 = this.g) != null && (a3 = g37.a()) != null) {
                FlexibleTextView flexibleTextView19 = a3.u;
                pq7.b(flexibleTextView19, "it.ftvToday");
                if (flexibleTextView19.isSelected()) {
                    a3.u.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.u.setTextColor(Color.parseColor(this.t));
                }
                FlexibleTextView flexibleTextView20 = a3.r;
                pq7.b(flexibleTextView20, "it.ftv7Days");
                if (flexibleTextView20.isSelected()) {
                    a3.r.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.r.setTextColor(Color.parseColor(this.t));
                }
                FlexibleTextView flexibleTextView21 = a3.s;
                pq7.b(flexibleTextView21, "it.ftvMonth");
                if (flexibleTextView21.isSelected()) {
                    a3.s.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.s.setTextColor(Color.parseColor(this.t));
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        c25 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewFragment", "onCreateView");
        c25 c25 = (c25) aq0.f(layoutInflater, 2131558497, viewGroup, false, A6());
        mo0.y0(c25.w, false);
        if (bundle != null) {
            this.s = bundle.getInt("CURRENT_TAB", 7);
        }
        pq7.b(c25, "binding");
        M6(c25);
        this.g = new g37<>(this, c25);
        N6();
        g37<c25> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.s);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
