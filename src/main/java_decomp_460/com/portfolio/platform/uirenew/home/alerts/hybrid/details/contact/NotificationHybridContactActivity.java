package com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import com.fossil.il7;
import com.fossil.j06;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.q56;
import com.fossil.r56;
import com.fossil.ro4;
import com.fossil.v56;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridContactActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public v56 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, int i, ArrayList<j06> arrayList) {
            pq7.c(fragment, "fragment");
            pq7.c(arrayList, "contactWrappersSelected");
            Intent intent = new Intent(fragment.getContext(), NotificationHybridContactActivity.class);
            intent.putExtra("HAND_NUMBER", i);
            intent.putExtra("LIST_CONTACTS_SELECTED", arrayList);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 5678);
        }
    }

    /*
    static {
        pq7.b(NotificationHybridContactActivity.class.getSimpleName(), "NotificationHybridContac\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        q56 q56 = (q56) getSupportFragmentManager().Y(2131362158);
        if (q56 == null) {
            q56 = q56.l.b();
            k(q56, q56.l.a(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (q56 != null) {
            LoaderManager supportLoaderManager = getSupportLoaderManager();
            pq7.b(supportLoaderManager, "supportLoaderManager");
            M.n0(new r56(q56, getIntent().getIntExtra("HAND_NUMBER", 0), (ArrayList) getIntent().getSerializableExtra("LIST_CONTACTS_SELECTED"), supportLoaderManager)).a(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactContract.View");
    }
}
