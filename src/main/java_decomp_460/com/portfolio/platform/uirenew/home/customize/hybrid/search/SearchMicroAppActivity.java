package com.portfolio.platform.uirenew.home.customize.hybrid.search;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import androidx.fragment.app.Fragment;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.ro4;
import com.fossil.sc6;
import com.fossil.tc6;
import com.fossil.vc6;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchMicroAppActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public vc6 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, String str2, String str3) {
            pq7.c(fragment, "fragment");
            pq7.c(str, "watchAppTop");
            pq7.c(str2, "watchAppMiddle");
            pq7.c(str3, "watchAppBottom");
            Intent intent = new Intent(fragment.getContext(), SearchMicroAppActivity.class);
            intent.putExtra(ViewHierarchy.DIMENSION_TOP_KEY, str);
            intent.putExtra("middle", str2);
            intent.putExtra("bottom", str3);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 102, ActivityOptions.makeSceneTransitionAnimation(fragment.getActivity(), new Pair[0]).toBundle());
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        sc6 sc6 = (sc6) getSupportFragmentManager().Y(2131362158);
        if (sc6 == null) {
            sc6 = sc6.l.b();
            k(sc6, sc6.l.a(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (sc6 != null) {
            M.G(new tc6(sc6)).a(this);
            Intent intent = getIntent();
            pq7.b(intent, "intent");
            Bundle extras = intent.getExtras();
            if (extras != null) {
                vc6 vc6 = this.A;
                if (vc6 != null) {
                    String string = extras.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string == null) {
                        string = "empty";
                    }
                    String string2 = extras.getString("middle");
                    if (string2 == null) {
                        string2 = "empty";
                    }
                    String string3 = extras.getString("bottom");
                    if (string3 == null) {
                        string3 = "empty";
                    }
                    vc6.E(string, string2, string3);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
            if (bundle != null) {
                vc6 vc62 = this.A;
                if (vc62 != null) {
                    String string4 = bundle.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string4 == null) {
                        string4 = "empty";
                    }
                    String string5 = bundle.getString("middle");
                    if (string5 == null) {
                        string5 = "empty";
                    }
                    String string6 = bundle.getString("bottom");
                    if (string6 == null) {
                        string6 = "empty";
                    }
                    vc62.E(string4, string5, string6);
                    return;
                }
                pq7.n("mPresenter");
                throw null;
            }
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppContract.View");
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        vc6 vc6 = this.A;
        if (vc6 != null) {
            vc6.B(bundle);
            if (bundle != null) {
                super.onSaveInstanceState(bundle);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }
}
