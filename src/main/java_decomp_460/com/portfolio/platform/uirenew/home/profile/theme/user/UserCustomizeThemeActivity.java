package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.pt6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserCustomizeThemeActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str, boolean z) {
            pq7.c(context, "context");
            pq7.c(str, "id");
            Intent intent = new Intent(context, UserCustomizeThemeActivity.class);
            intent.setFlags(536870912);
            Bundle bundle = new Bundle();
            bundle.putString("THEME_ID", str);
            bundle.putBoolean("THEME_MODE_EDIT", z);
            intent.addFlags(32768);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        pt6 pt6 = (pt6) getSupportFragmentManager().Y(2131362158);
        if (pt6 == null) {
            pt6 = pt6.m.c();
            i(pt6, 2131362158);
        }
        Intent intent = getIntent();
        pq7.b(intent, "intent");
        Bundle extras = intent.getExtras();
        if (extras != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("THEME_ID", extras.getString("THEME_ID"));
            bundle2.putBoolean("THEME_MODE_EDIT", extras.getBoolean("THEME_MODE_EDIT"));
            pt6.setArguments(bundle2);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onResume() {
        super.onResume();
        l(false);
    }
}
