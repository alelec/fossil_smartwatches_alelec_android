package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.ro4;
import com.fossil.t46;
import com.fossil.u46;
import com.fossil.x46;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationDialLandingActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public x46 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment) {
            pq7.c(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationDialLandingActivity.class);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }
    }

    /*
    static {
        pq7.b(NotificationDialLandingActivity.class.getSimpleName(), "NotificationDialLandingA\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        t46 t46 = (t46) getSupportFragmentManager().Y(2131362158);
        if (t46 == null) {
            t46 = t46.k.b();
            k(t46, t46.k.a(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (t46 != null) {
            LoaderManager supportLoaderManager = getSupportLoaderManager();
            pq7.b(supportLoaderManager, "supportLoaderManager");
            M.P1(new u46(t46, supportLoaderManager)).a(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingContract.View");
    }
}
