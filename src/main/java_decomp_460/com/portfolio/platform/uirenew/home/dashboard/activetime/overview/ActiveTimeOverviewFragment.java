package com.portfolio.platform.uirenew.home.dashboard.activetime.overview;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.aq0;
import com.fossil.de6;
import com.fossil.g37;
import com.fossil.g67;
import com.fossil.je6;
import com.fossil.ke6;
import com.fossil.mo0;
import com.fossil.mv0;
import com.fossil.nk5;
import com.fossil.pe6;
import com.fossil.pq7;
import com.fossil.pv5;
import com.fossil.qe6;
import com.fossil.qn5;
import com.fossil.ro4;
import com.fossil.s15;
import com.fossil.yd6;
import com.fossil.zd6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActiveTimeOverviewFragment extends pv5 {
    @DexIgnore
    public g37<s15> g;
    @DexIgnore
    public zd6 h;
    @DexIgnore
    public qe6 i;
    @DexIgnore
    public ke6 j;
    @DexIgnore
    public yd6 k;
    @DexIgnore
    public pe6 l;
    @DexIgnore
    public je6 m;
    @DexIgnore
    public int s; // = 7;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment b;

        @DexIgnore
        public a(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.b = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.b;
            g37 g37 = activeTimeOverviewFragment.g;
            activeTimeOverviewFragment.O6(7, g37 != null ? (s15) g37.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment b;

        @DexIgnore
        public b(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.b = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.b;
            g37 g37 = activeTimeOverviewFragment.g;
            activeTimeOverviewFragment.O6(4, g37 != null ? (s15) g37.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment b;

        @DexIgnore
        public c(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.b = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.b;
            g37 g37 = activeTimeOverviewFragment.g;
            activeTimeOverviewFragment.O6(2, g37 != null ? (s15) g37.a() : null);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "ActiveTimeOverviewFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void M6(s15 s15) {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "initUI");
        this.k = (yd6) getChildFragmentManager().Z("ActiveTimeOverviewDayFragment");
        this.l = (pe6) getChildFragmentManager().Z("ActiveTimeOverviewWeekFragment");
        this.m = (je6) getChildFragmentManager().Z("ActiveTimeOverviewMonthFragment");
        if (this.k == null) {
            this.k = new yd6();
        }
        if (this.l == null) {
            this.l = new pe6();
        }
        if (this.m == null) {
            this.m = new je6();
        }
        ArrayList arrayList = new ArrayList();
        yd6 yd6 = this.k;
        if (yd6 != null) {
            arrayList.add(yd6);
            pe6 pe6 = this.l;
            if (pe6 != null) {
                arrayList.add(pe6);
                je6 je6 = this.m;
                if (je6 != null) {
                    arrayList.add(je6);
                    RecyclerView recyclerView = s15.w;
                    pq7.b(recyclerView, "it");
                    recyclerView.setAdapter(new g67(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new ActiveTimeOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new mv0().b(recyclerView);
                    O6(this.s, s15);
                    ro4 M = PortfolioApp.h0.c().M();
                    yd6 yd62 = this.k;
                    if (yd62 != null) {
                        pe6 pe62 = this.l;
                        if (pe62 != null) {
                            je6 je62 = this.m;
                            if (je62 != null) {
                                M.E(new de6(yd62, pe62, je62)).a(this);
                                s15.u.setOnClickListener(new a(this));
                                s15.r.setOnClickListener(new b(this));
                                s15.s.setOnClickListener(new c(this));
                                return;
                            }
                            pq7.i();
                            throw null;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void N6() {
        g37<s15> g37;
        s15 a2;
        g37<s15> g372;
        s15 a3;
        ConstraintLayout constraintLayout;
        g37<s15> g373;
        s15 a4;
        FlexibleTextView flexibleTextView;
        g37<s15> g374;
        s15 a5;
        nk5.a aVar = nk5.o;
        zd6 zd6 = this.h;
        if (zd6 != null) {
            String d = aVar.w(zd6.n()) ? qn5.l.a().d("dianaActiveMinutesTab") : qn5.l.a().d("hybridActiveMinutesTab");
            nk5.a aVar2 = nk5.o;
            zd6 zd62 = this.h;
            if (zd62 != null) {
                String d2 = aVar2.w(zd62.n()) ? qn5.l.a().d("onDianaActiveMinutesTab") : qn5.l.a().d("onHybridActiveMinutesTab");
                if (!(d == null || (g374 = this.g) == null || (a5 = g374.a()) == null)) {
                    a5.x.setBackgroundColor(Color.parseColor(d));
                    a5.y.setBackgroundColor(Color.parseColor(d));
                }
                if (!(d2 == null || (g373 = this.g) == null || (a4 = g373.a()) == null || (flexibleTextView = a4.t) == null)) {
                    flexibleTextView.setTextColor(Color.parseColor(d2));
                }
                nk5.a aVar3 = nk5.o;
                zd6 zd63 = this.h;
                if (zd63 != null) {
                    this.t = aVar3.w(zd63.n()) ? qn5.l.a().d("onDianaInactiveTab") : qn5.l.a().d("onHybridInactiveTab");
                    String d3 = qn5.l.a().d("nonBrandSurface");
                    this.u = qn5.l.a().d("primaryText");
                    if (!(d3 == null || (g372 = this.g) == null || (a3 = g372.a()) == null || (constraintLayout = a3.q) == null)) {
                        constraintLayout.setBackgroundColor(Color.parseColor(d3));
                    }
                    if (!TextUtils.isEmpty(this.t) && !TextUtils.isEmpty(this.u) && (g37 = this.g) != null && (a2 = g37.a()) != null) {
                        FlexibleTextView flexibleTextView2 = a2.u;
                        pq7.b(flexibleTextView2, "it.ftvToday");
                        if (flexibleTextView2.isSelected()) {
                            a2.u.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.u.setTextColor(Color.parseColor(this.t));
                        }
                        FlexibleTextView flexibleTextView3 = a2.r;
                        pq7.b(flexibleTextView3, "it.ftv7Days");
                        if (flexibleTextView3.isSelected()) {
                            a2.r.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.r.setTextColor(Color.parseColor(this.t));
                        }
                        FlexibleTextView flexibleTextView4 = a2.s;
                        pq7.b(flexibleTextView4, "it.ftvMonth");
                        if (flexibleTextView4.isSelected()) {
                            a2.s.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.s.setTextColor(Color.parseColor(this.t));
                        }
                    }
                } else {
                    pq7.n("mActiveTimeOverviewDayPresenter");
                    throw null;
                }
            } else {
                pq7.n("mActiveTimeOverviewDayPresenter");
                throw null;
            }
        } else {
            pq7.n("mActiveTimeOverviewDayPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void O6(int i2, s15 s15) {
        s15 a2;
        RecyclerView recyclerView;
        g37<s15> g37;
        s15 a3;
        s15 a4;
        RecyclerView recyclerView2;
        s15 a5;
        RecyclerView recyclerView3;
        s15 a6;
        RecyclerView recyclerView4;
        if (s15 != null) {
            FlexibleTextView flexibleTextView = s15.u;
            pq7.b(flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = s15.r;
            pq7.b(flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = s15.s;
            pq7.b(flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = s15.u;
            pq7.b(flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = s15.r;
            pq7.b(flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = s15.s;
            pq7.b(flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i2 == 2) {
                FlexibleTextView flexibleTextView7 = s15.s;
                pq7.b(flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = s15.s;
                pq7.b(flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = s15.r;
                pq7.b(flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                g37<s15> g372 = this.g;
                if (!(g372 == null || (a2 = g372.a()) == null || (recyclerView = a2.w) == null)) {
                    recyclerView.scrollToPosition(2);
                }
            } else if (i2 == 4) {
                FlexibleTextView flexibleTextView10 = s15.r;
                pq7.b(flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = s15.r;
                pq7.b(flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = s15.r;
                pq7.b(flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                g37<s15> g373 = this.g;
                if (!(g373 == null || (a4 = g373.a()) == null || (recyclerView2 = a4.w) == null)) {
                    recyclerView2.scrollToPosition(1);
                }
            } else if (i2 != 7) {
                FlexibleTextView flexibleTextView13 = s15.u;
                pq7.b(flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = s15.u;
                pq7.b(flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = s15.r;
                pq7.b(flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                g37<s15> g374 = this.g;
                if (!(g374 == null || (a6 = g374.a()) == null || (recyclerView4 = a6.w) == null)) {
                    recyclerView4.scrollToPosition(0);
                }
            } else {
                FlexibleTextView flexibleTextView16 = s15.u;
                pq7.b(flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = s15.u;
                pq7.b(flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = s15.r;
                pq7.b(flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                g37<s15> g375 = this.g;
                if (!(g375 == null || (a5 = g375.a()) == null || (recyclerView3 = a5.w) == null)) {
                    recyclerView3.scrollToPosition(0);
                }
            }
            if (!TextUtils.isEmpty(this.t) && !TextUtils.isEmpty(this.u) && (g37 = this.g) != null && (a3 = g37.a()) != null) {
                FlexibleTextView flexibleTextView19 = a3.u;
                pq7.b(flexibleTextView19, "it.ftvToday");
                if (flexibleTextView19.isSelected()) {
                    a3.u.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.u.setTextColor(Color.parseColor(this.t));
                }
                FlexibleTextView flexibleTextView20 = a3.r;
                pq7.b(flexibleTextView20, "it.ftv7Days");
                if (flexibleTextView20.isSelected()) {
                    a3.r.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.r.setTextColor(Color.parseColor(this.t));
                }
                FlexibleTextView flexibleTextView21 = a3.s;
                pq7.b(flexibleTextView21, "it.ftvMonth");
                if (flexibleTextView21.isSelected()) {
                    a3.s.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.s.setTextColor(Color.parseColor(this.t));
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        s15 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "onCreateView");
        s15 s15 = (s15) aq0.f(layoutInflater, 2131558492, viewGroup, false, A6());
        mo0.y0(s15.w, false);
        if (bundle != null) {
            this.s = bundle.getInt("CURRENT_TAB", 7);
        }
        pq7.b(s15, "binding");
        M6(s15);
        this.g = new g37<>(this, s15);
        N6();
        g37<s15> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.s);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
