package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.c86;
import com.fossil.e86;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.n76;
import com.fossil.pq7;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public e86 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            pq7.c(fragment, "fragment");
            pq7.c(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 106);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView(2131558428);
        n76 n76 = (n76) getSupportFragmentManager().Y(2131362158);
        if (n76 == null) {
            n76 = n76.s.a();
            k(n76, n76.s.b(), 2131362158);
        }
        PortfolioApp.h0.c().M().T0(new c86(n76)).a(this);
        if (getIntent() == null || (str = getIntent().getStringExtra(Constants.USER_SETTING)) == null) {
            str = "";
        }
        e86 e86 = this.A;
        if (e86 != null) {
            e86.E(str);
        } else {
            pq7.n("mCommuteTimeSettingsPresenter");
            throw null;
        }
    }
}
