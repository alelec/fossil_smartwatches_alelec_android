package com.portfolio.platform.uirenew.home.customize.tutorial;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.s66;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeTutorialActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            pq7.c(context, "context");
            pq7.c(str, "watchAppId");
            Intent intent = new Intent(context, CustomizeTutorialActivity.class);
            intent.putExtra("EXTRA_WATCHAPP_ID", str);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        if (((s66) getSupportFragmentManager().Y(2131362158)) == null) {
            String stringExtra = getIntent().getStringExtra("EXTRA_WATCHAPP_ID");
            s66.a aVar = s66.k;
            pq7.b(stringExtra, "watchAppId");
            k(aVar.a(stringExtra), "CustomizeTutorialFragment", 2131362158);
        }
    }
}
