package com.portfolio.platform.uirenew.home.customize.hybrid.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ec6;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ks5;
import com.fossil.ln0;
import com.fossil.ls5;
import com.fossil.po4;
import com.fossil.pq7;
import com.fossil.rk0;
import com.fossil.sb6;
import com.fossil.sk0;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridCustomizeEditActivity extends ls5 {
    @DexIgnore
    public static /* final */ a D; // = new a(null);
    @DexIgnore
    public ec6 A;
    @DexIgnore
    public po4 B;
    @DexIgnore
    public sb6 C;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            pq7.c(context, "context");
            pq7.c(str, "presetId");
            pq7.c(str2, "microAppPos");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditActivity", "start - presetId=" + str + ", microAppPos=" + str2);
            Intent intent = new Intent(context, HybridCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str2);
            context.startActivity(intent);
        }

        @DexIgnore
        public final void b(FragmentActivity fragmentActivity, String str, ArrayList<ln0<View, String>> arrayList, List<? extends ln0<CustomizeWidget, String>> list, String str2) {
            pq7.c(fragmentActivity, "context");
            pq7.c(str, "presetId");
            pq7.c(arrayList, "views");
            pq7.c(list, "customizeWidgetViews");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditActivity", "startForResultAnimation() - presetId=" + str + ", microAppPos=" + str2);
            Intent intent = new Intent(fragmentActivity, HybridCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str2);
            Iterator<? extends ln0<CustomizeWidget, String>> it = list.iterator();
            while (it.hasNext()) {
                ln0 ln0 = (ln0) it.next();
                arrayList.add(new ln0<>(ln0.f2221a, ln0.b));
                Bundle bundle = new Bundle();
                ks5.a aVar = ks5.c;
                F f = ln0.f2221a;
                if (f != null) {
                    pq7.b(f, "wcPair.first!!");
                    aVar.a(f, bundle);
                    F f2 = ln0.f2221a;
                    if (f2 != null) {
                        pq7.b(f2, "wcPair.first!!");
                        intent.putExtra(f2.getTransitionName(), bundle);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            Object[] array = arrayList.toArray(new ln0[0]);
            if (array != null) {
                ln0[] ln0Arr = (ln0[]) array;
                sk0 a2 = sk0.a(fragmentActivity, (ln0[]) Arrays.copyOf(ln0Arr, ln0Arr.length));
                pq7.b(a2, "ActivityOptionsCompat.ma\u2026context, *viewsTypeArray)");
                rk0.y(fragmentActivity, intent, 100, a2.b());
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Fragment Y = getSupportFragmentManager().Y(2131362158);
        if (Y != null) {
            Y.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.ls5
    public void onBackPressed() {
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x013b  */
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
        // Method dump skipped, instructions count: 329
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity.onCreate(android.os.Bundle):void");
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        ec6 ec6 = this.A;
        if (ec6 != null) {
            ec6.A(bundle);
            if (bundle != null) {
                super.onSaveInstanceState(bundle);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }
}
