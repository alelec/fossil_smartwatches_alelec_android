package com.portfolio.platform.uirenew.home.profile.goal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.ep6;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileGoalEditActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public ep6 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            pq7.c(context, "context");
            Intent intent = new Intent(context, ProfileGoalEditActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        ep6 ep6 = this.A;
        if (ep6 != null) {
            ep6.onActivityResult(i, i2, intent);
        }
        super.onActivityResult(i, i2, intent);
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.ls5
    public void onBackPressed() {
        ep6 ep6 = this.A;
        if (ep6 != null) {
            ep6.F6();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        ep6 ep6 = (ep6) getSupportFragmentManager().Y(2131362158);
        this.A = ep6;
        if (ep6 == null) {
            ep6 b = ep6.v.b();
            this.A = b;
            if (b != null) {
                k(b, ep6.v.a(), 2131362158);
            } else {
                pq7.i();
                throw null;
            }
        }
    }
}
