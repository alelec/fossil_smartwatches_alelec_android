package com.portfolio.platform.uirenew.migration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.jv6;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MigrationActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            pq7.c(context, "context");
            context.startActivity(new Intent(context, MigrationActivity.class));
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.ls5
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        if (((jv6) getSupportFragmentManager().Y(2131362158)) == null) {
            i(jv6.k.a(), 2131362158);
        }
    }
}
