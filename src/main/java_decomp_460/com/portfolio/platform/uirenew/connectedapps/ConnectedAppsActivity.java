package com.portfolio.platform.uirenew.connectedapps;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.dy5;
import com.fossil.fy5;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.ro4;
import com.fossil.xn6;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ConnectedAppsActivity extends ls5 {
    @DexIgnore
    public static /* final */ a C; // = new a(null);
    @DexIgnore
    public xn6 A;
    @DexIgnore
    public fy5 B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            pq7.c(context, "context");
            Intent intent = new Intent(context, ConnectedAppsActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        xn6 xn6 = this.A;
        if (xn6 != null) {
            xn6.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        xn6 xn6 = (xn6) getSupportFragmentManager().Y(2131362158);
        this.A = xn6;
        if (xn6 == null) {
            xn6 b = xn6.k.b();
            this.A = b;
            if (b != null) {
                k(b, xn6.k.a(), 2131362158);
            } else {
                pq7.i();
                throw null;
            }
        }
        ro4 M = PortfolioApp.h0.c().M();
        xn6 xn62 = this.A;
        if (xn62 != null) {
            M.h(new dy5(xn62)).a(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.connectedapps.ConnectedAppsContract.View");
    }
}
