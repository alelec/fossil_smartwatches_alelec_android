package com.portfolio.platform.uirenew.signup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.qz6;
import com.fossil.ro4;
import com.fossil.rz6;
import com.fossil.un5;
import com.fossil.uz6;
import com.fossil.vn5;
import com.fossil.wn5;
import com.fossil.xn5;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SignUpActivity extends ls5 {
    @DexIgnore
    public static /* final */ a F; // = new a(null);
    @DexIgnore
    public uz6 A;
    @DexIgnore
    public un5 B;
    @DexIgnore
    public vn5 C;
    @DexIgnore
    public xn5 D;
    @DexIgnore
    public wn5 E;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            pq7.c(context, "context");
            context.startActivity(new Intent(context, SignUpActivity.class));
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null) {
            un5 un5 = this.B;
            if (un5 != null) {
                un5.d(i, i2, intent);
                vn5 vn5 = this.C;
                if (vn5 != null) {
                    vn5.h(i, i2, intent);
                    xn5 xn5 = this.D;
                    if (xn5 != null) {
                        xn5.b(i, i2, intent);
                        Fragment Y = getSupportFragmentManager().Y(2131362158);
                        if (Y != null) {
                            Y.onActivityResult(i, i2, intent);
                            return;
                        }
                        return;
                    }
                    pq7.n("mLoginWeiboManager");
                    throw null;
                }
                pq7.n("mLoginGoogleManager");
                throw null;
            }
            pq7.n("mLoginFacebookManager");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        qz6 qz6 = (qz6) getSupportFragmentManager().Y(2131362158);
        if (qz6 == null) {
            qz6 = qz6.j.a();
            k(qz6, r(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (qz6 != null) {
            M.R1(new rz6(this, qz6)).a(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.signup.SignUpContract.View");
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onNewIntent(Intent intent) {
        pq7.c(intent, "intent");
        super.onNewIntent(intent);
        wn5 wn5 = this.E;
        if (wn5 != null) {
            Intent intent2 = getIntent();
            pq7.b(intent2, "getIntent()");
            wn5.g(intent2);
            return;
        }
        pq7.n("mLoginWechatManager");
        throw null;
    }
}
