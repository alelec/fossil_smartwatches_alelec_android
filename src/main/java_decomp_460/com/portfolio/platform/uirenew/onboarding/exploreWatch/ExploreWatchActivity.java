package com.portfolio.platform.uirenew.onboarding.exploreWatch;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.ro4;
import com.fossil.rv6;
import com.fossil.tv5;
import com.fossil.tv6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ExploreWatchActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public tv6 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, boolean z) {
            pq7.c(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ExploreWatchActivity", "start - isOnboarding=" + z);
            Intent intent = new Intent(context, ExploreWatchActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.ls5
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        boolean z = false;
        super.onCreate(bundle);
        setContentView(2131558439);
        tv5 tv5 = (tv5) getSupportFragmentManager().Y(2131362158);
        if (getIntent() != null) {
            z = getIntent().getBooleanExtra("IS_ONBOARDING_FLOW", false);
        }
        if (tv5 == null) {
            tv5 = tv5.t.a(z);
            k(tv5, "ExploreWatchFragment", 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (tv5 != null) {
            M.n1(new rv6(tv5)).a(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchContract.View");
    }
}
