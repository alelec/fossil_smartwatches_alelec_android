package com.portfolio.platform.uirenew.onboarding.heightweight;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.il7;
import com.fossil.jw6;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.lw6;
import com.fossil.pq7;
import com.fossil.ro4;
import com.fossil.vv5;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OnboardingHeightWeightActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public lw6 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            pq7.c(context, "context");
            context.startActivity(new Intent(context, OnboardingHeightWeightActivity.class));
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.ls5
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        vv5 vv5 = (vv5) getSupportFragmentManager().Y(2131362158);
        if (vv5 == null) {
            vv5 = vv5.k.b();
            k(vv5, vv5.k.a(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (vv5 != null) {
            M.i0(new jw6(vv5)).a(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightContract.View");
    }
}
