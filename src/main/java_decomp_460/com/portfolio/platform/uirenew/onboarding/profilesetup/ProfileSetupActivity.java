package com.portfolio.platform.uirenew.onboarding.profilesetup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import com.fossil.ax6;
import com.fossil.cx6;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.ro4;
import com.fossil.xv5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileSetupActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public cx6 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, SignUpEmailAuth signUpEmailAuth) {
            pq7.c(context, "context");
            pq7.c(signUpEmailAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ProfileSetupActivity", "start with auth=" + signUpEmailAuth);
            Intent intent = new Intent(context, ProfileSetupActivity.class);
            intent.putExtra("EMAIL_AUTH", signUpEmailAuth);
            context.startActivity(intent);
        }

        @DexIgnore
        public final void b(Context context, SignUpSocialAuth signUpSocialAuth) {
            pq7.c(context, "context");
            pq7.c(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ProfileSetupActivity", "startSetUpSocial with " + signUpSocialAuth);
            Intent intent = new Intent(context, ProfileSetupActivity.class);
            intent.putExtra("SOCIAL_AUTH", signUpSocialAuth);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.ls5
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        xv5 xv5 = (xv5) getSupportFragmentManager().Y(2131362158);
        if (xv5 == null) {
            xv5 = xv5.t.a();
            k(xv5, "ProfileSetupFragment", 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (xv5 != null) {
            M.F0(new ax6(xv5)).a(this);
            Intent intent = getIntent();
            if (intent != null) {
                if (intent.hasExtra("SOCIAL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(r(), "Retrieve from intent socialAuth");
                    cx6 cx6 = this.A;
                    if (cx6 != null) {
                        Parcelable parcelableExtra = intent.getParcelableExtra("SOCIAL_AUTH");
                        pq7.b(parcelableExtra, "it.getParcelableExtra(co\u2026ms.Constants.SOCIAL_AUTH)");
                        cx6.j0((SignUpSocialAuth) parcelableExtra);
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                }
                if (intent.hasExtra("EMAIL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(r(), "Retrieve from intent emailAuth");
                    cx6 cx62 = this.A;
                    if (cx62 != null) {
                        Parcelable parcelableExtra2 = intent.getParcelableExtra("EMAIL_AUTH");
                        pq7.b(parcelableExtra2, "it.getParcelableExtra(co\u2026ums.Constants.EMAIL_AUTH)");
                        cx62.h0((SignUpEmailAuth) parcelableExtra2);
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                }
                if (intent.hasExtra("IS_UPDATE_PROFILE_PROCESS")) {
                    boolean booleanExtra = intent.getBooleanExtra("IS_UPDATE_PROFILE_PROCESS", false);
                    cx6 cx63 = this.A;
                    if (cx63 != null) {
                        cx63.i0(booleanExtra);
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                }
            }
            if (bundle != null) {
                if (bundle.containsKey("SOCIAL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(r(), "Retrieve from savedInstanceState socialAuth");
                    SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) bundle.getParcelable("SOCIAL_AUTH");
                    if (signUpSocialAuth != null) {
                        cx6 cx64 = this.A;
                        if (cx64 != null) {
                            cx64.j0(signUpSocialAuth);
                        } else {
                            pq7.n("mPresenter");
                            throw null;
                        }
                    }
                }
                if (bundle.containsKey("EMAIL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(r(), "Retrieve from savedInstanceState emailAuth");
                    SignUpEmailAuth signUpEmailAuth = (SignUpEmailAuth) bundle.getParcelable("EMAIL_AUTH");
                    if (signUpEmailAuth != null) {
                        cx6 cx65 = this.A;
                        if (cx65 != null) {
                            cx65.h0(signUpEmailAuth);
                        } else {
                            pq7.n("mPresenter");
                            throw null;
                        }
                    }
                }
                if (bundle.containsKey("IS_UPDATE_PROFILE_PROCESS")) {
                    boolean z = bundle.getBoolean("IS_UPDATE_PROFILE_PROCESS");
                    cx6 cx66 = this.A;
                    if (cx66 != null) {
                        cx66.i0(z);
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                }
            }
        } else {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupContract.View");
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        cx6 cx6 = this.A;
        if (cx6 != null) {
            SignUpEmailAuth R = cx6.R();
            if (R != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String r = r();
                local.d(r, "onSaveInstanceState put emailAuth " + R);
                bundle.putParcelable("EMAIL_AUTH", R);
            }
            cx6 cx62 = this.A;
            if (cx62 != null) {
                SignUpSocialAuth Y = cx62.Y();
                if (Y != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String r2 = r();
                    local2.d(r2, "onSaveInstanceState put socialAuth " + Y);
                    bundle.putParcelable("SOCIAL_AUTH", Y);
                }
                cx6 cx63 = this.A;
                if (cx63 != null) {
                    bundle.putBoolean("IS_UPDATE_PROFILE_PROCESS", cx63.f0());
                    super.onSaveInstanceState(bundle);
                    return;
                }
                pq7.n("mPresenter");
                throw null;
            }
            pq7.n("mPresenter");
            throw null;
        }
        pq7.n("mPresenter");
        throw null;
    }
}
