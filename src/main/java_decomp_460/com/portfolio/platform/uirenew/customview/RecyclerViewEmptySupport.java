package com.portfolio.platform.uirenew.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecyclerViewEmptySupport extends RecyclerView {
    @DexIgnore
    public View b;
    @DexIgnore
    public /* final */ a c; // = new a(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.AdapterDataObserver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ RecyclerViewEmptySupport f4731a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(RecyclerViewEmptySupport recyclerViewEmptySupport) {
            this.f4731a = recyclerViewEmptySupport;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void a() {
            if (this.f4731a.getAdapter() != null && this.f4731a.getEmptyView$app_fossilRelease() != null) {
                RecyclerView.g adapter = this.f4731a.getAdapter();
                if (adapter != null) {
                    pq7.b(adapter, "adapter!!");
                    if (adapter.getItemCount() == 0) {
                        View emptyView$app_fossilRelease = this.f4731a.getEmptyView$app_fossilRelease();
                        if (emptyView$app_fossilRelease != null) {
                            emptyView$app_fossilRelease.setVisibility(0);
                            this.f4731a.setVisibility(8);
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                    View emptyView$app_fossilRelease2 = this.f4731a.getEmptyView$app_fossilRelease();
                    if (emptyView$app_fossilRelease2 != null) {
                        emptyView$app_fossilRelease2.setVisibility(8);
                        this.f4731a.setVisibility(0);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewEmptySupport(Context context) {
        super(context);
        pq7.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewEmptySupport(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewEmptySupport(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
    }

    @DexIgnore
    public final View getEmptyView$app_fossilRelease() {
        return this.b;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView
    public void setAdapter(RecyclerView.g<?> gVar) {
        super.setAdapter(gVar);
        if (gVar != null) {
            gVar.registerAdapterDataObserver(this.c);
        }
        this.c.a();
    }

    @DexIgnore
    public final void setEmptyView(View view) {
        pq7.c(view, "emptyView");
        this.b = view;
    }

    @DexIgnore
    public final void setEmptyView$app_fossilRelease(View view) {
        this.b = view;
    }
}
