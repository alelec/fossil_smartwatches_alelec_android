package com.portfolio.platform.uirenew.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.pq7;
import com.fossil.sq4;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TitleValueCell extends ConstraintLayout {
    @DexIgnore
    public FlexibleTextView w;
    @DexIgnore
    public FlexibleTextView x;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TitleValueCell(Context context) {
        super(context);
        pq7.c(context, "context");
        G(context, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TitleValueCell(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attributeSet");
        G(context, attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TitleValueCell(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attributeSet");
        G(context, attributeSet);
    }

    @DexIgnore
    public final void G(Context context, AttributeSet attributeSet) {
        View inflate = ViewGroup.inflate(context, 2131558833, this);
        View findViewById = inflate.findViewById(2131362546);
        pq7.b(findViewById, "view.findViewById(R.id.ftv_title)");
        this.w = (FlexibleTextView) findViewById;
        View findViewById2 = inflate.findViewById(2131362554);
        pq7.b(findViewById2, "view.findViewById(R.id.ftv_value)");
        this.x = (FlexibleTextView) findViewById2;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.TitleValueCell);
        String string = obtainStyledAttributes.getString(0);
        String string2 = obtainStyledAttributes.getString(1);
        obtainStyledAttributes.recycle();
        FlexibleTextView flexibleTextView = this.w;
        if (flexibleTextView != null) {
            if (string == null) {
                string = "";
            }
            flexibleTextView.setText(string);
            FlexibleTextView flexibleTextView2 = this.x;
            if (flexibleTextView2 != null) {
                flexibleTextView2.setText(string2 != null ? string2 : "");
            } else {
                pq7.n("ftvValue");
                throw null;
            }
        } else {
            pq7.n("ftvTitle");
            throw null;
        }
    }

    @DexIgnore
    public final void setTitle(String str) {
        pq7.c(str, "name");
        FlexibleTextView flexibleTextView = this.w;
        if (flexibleTextView != null) {
            flexibleTextView.setText(str);
        } else {
            pq7.n("ftvTitle");
            throw null;
        }
    }

    @DexIgnore
    public final void setValue(String str) {
        pq7.c(str, "value");
        FlexibleTextView flexibleTextView = this.x;
        if (flexibleTextView != null) {
            flexibleTextView.setText(str);
        } else {
            pq7.n("ftvValue");
            throw null;
        }
    }
}
