package com.portfolio.platform.receiver;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Process;
import android.text.TextUtils;
import com.fossil.bw7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gu7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.xw7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.workers.PushPendingDataWorker;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NetworkChangedReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String b;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public UserRepository f4701a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.receiver.NetworkChangedReceiver$onReceive$2", f = "NetworkChangedReceiver.kt", l = {52}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isInternetAvailable;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NetworkChangedReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(NetworkChangedReceiver networkChangedReceiver, boolean z, qn7 qn7) {
            super(2, qn7);
            this.this$0 = networkChangedReceiver;
            this.$isInternetAvailable = z;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$isInternetAvailable, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object currentUser;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                UserRepository b = this.this$0.b();
                this.L$0 = iv7;
                this.label = 1;
                currentUser = b.getCurrentUser(this);
                if (currentUser == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                currentUser = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((MFUser) currentUser) != null && !this.$isInternetAvailable) {
                PushPendingDataWorker.H.a();
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = NetworkChangedReceiver.class.getSimpleName();
        pq7.b(simpleName, "NetworkChangedReceiver::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public NetworkChangedReceiver() {
        PortfolioApp.h0.c().M().A0(this);
    }

    @DexIgnore
    public final boolean a(Context context) {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        return (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isConnected()) ? false : true;
    }

    @DexIgnore
    public final UserRepository b() {
        UserRepository userRepository = this.f4701a;
        if (userRepository != null) {
            return userRepository;
        }
        pq7.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        ActivityManager.RunningAppProcessInfo runningAppProcessInfo;
        pq7.c(context, "context");
        pq7.c(intent, "intent");
        FLogger.INSTANCE.getLocal().d(b, "onReceive");
        int myPid = Process.myPid();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Constants.ACTIVITY);
        if (activityManager != null) {
            if (activityManager.getRunningAppProcesses() != null) {
                Iterator<ActivityManager.RunningAppProcessInfo> it = activityManager.getRunningAppProcesses().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    ActivityManager.RunningAppProcessInfo next = it.next();
                    FLogger.INSTANCE.getLocal().d(b, "onReceive processId=" + next.pid);
                    if (next.pid == myPid) {
                        runningAppProcessInfo = next;
                        break;
                    }
                }
            } else {
                return;
            }
        }
        runningAppProcessInfo = null;
        if (runningAppProcessInfo != null) {
            if (!TextUtils.isEmpty(runningAppProcessInfo != null ? runningAppProcessInfo.processName : null)) {
                if (pq7.a(runningAppProcessInfo != null ? runningAppProcessInfo.processName : null, PortfolioApp.h0.c().getPackageName())) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = b;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onReceive processName=");
                    sb.append(runningAppProcessInfo != null ? runningAppProcessInfo.processName : null);
                    local.d(str, sb.toString());
                    xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new a(this, a(context), null), 3, null);
                }
            }
        }
    }
}
