package com.portfolio.platform.service.microapp;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import com.facebook.applinks.AppLinkData;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.VideoUploader;
import com.fossil.bw7;
import com.fossil.ea3;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.fa3;
import com.fossil.ga3;
import com.fossil.gl0;
import com.fossil.gu7;
import com.fossil.ha3;
import com.fossil.ht3;
import com.fossil.ia3;
import com.fossil.il7;
import com.fossil.it3;
import com.fossil.iv7;
import com.fossil.ja3;
import com.fossil.jt3;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.kq7;
import com.fossil.kr5;
import com.fossil.mr5;
import com.fossil.n62;
import com.fossil.na3;
import com.fossil.nt3;
import com.fossil.pq7;
import com.fossil.pu5;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.xw7;
import com.fossil.yn7;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.gson.Gson;
import com.google.maps.model.TravelMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.Mapping;
import com.misfit.frameworks.buttonservice.model.microapp.CommuteTimeETAMicroAppResponse;
import com.misfit.frameworks.buttonservice.model.microapp.CommuteTimeTravelMicroAppResponse;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeService extends kr5 implements ga3, LocationListener {
    @DexIgnore
    public static /* final */ int A; // = A;
    @DexIgnore
    public static /* final */ float B; // = 50.0f;
    @DexIgnore
    public static /* final */ a C; // = new a(null);
    @DexIgnore
    public static /* final */ String y;
    @DexIgnore
    public static /* final */ int z; // = 120000;
    @DexIgnore
    public Location e;
    @DexIgnore
    public Location f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public String h;
    @DexIgnore
    public long i;
    @DexIgnore
    public CommuteTimeSetting j;
    @DexIgnore
    public LocationManager k;
    @DexIgnore
    public String l;
    @DexIgnore
    public Handler m;
    @DexIgnore
    public ea3 s;
    @DexIgnore
    public na3 t;
    @DexIgnore
    public LocationRequest u;
    @DexIgnore
    public ia3 v;
    @DexIgnore
    public fa3 w;
    @DexIgnore
    public pu5 x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return CommuteTimeService.y;
        }

        @DexIgnore
        public final void b(Context context, Bundle bundle, mr5 mr5) {
            pq7.c(context, "context");
            pq7.c(bundle, Mapping.COLUMN_EXTRA_INFO);
            pq7.c(mr5, "listener");
            Intent intent = new Intent(context, CommuteTimeService.class);
            intent.putExtras(bundle);
            context.startService(intent);
            kr5.e(mr5);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fa3 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ CommuteTimeService f4712a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(CommuteTimeService commuteTimeService) {
            this.f4712a = commuteTimeService;
        }

        @DexIgnore
        @Override // com.fossil.fa3
        public void onLocationResult(LocationResult locationResult) {
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "onLocationResult");
            super.onLocationResult(locationResult);
            CommuteTimeService commuteTimeService = this.f4712a;
            if (locationResult != null) {
                commuteTimeService.D(locationResult.c());
                if (this.f4712a.w() != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = CommuteTimeService.C.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("onLocationResult lastLocation=");
                    Location w = this.f4712a.w();
                    if (w != null) {
                        sb.append(w);
                        local.d(a2, sb.toString());
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "onLocationResult lastLocation is null");
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.microapp.CommuteTimeService$getDurationTime$1", f = "CommuteTimeService.kt", l = {}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Location $location;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(CommuteTimeService commuteTimeService, Location location, qn7 qn7) {
            super(2, qn7);
            this.this$0 = commuteTimeService;
            this.$location = location;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$location, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                if (this.this$0.j != null) {
                    CommuteTimeSetting commuteTimeSetting = this.this$0.j;
                    if (commuteTimeSetting != null) {
                        if (!(commuteTimeSetting.getAddress().length() == 0)) {
                            pu5 v = this.this$0.v();
                            if (v != null) {
                                CommuteTimeSetting commuteTimeSetting2 = this.this$0.j;
                                if (commuteTimeSetting2 != null) {
                                    String address = commuteTimeSetting2.getAddress();
                                    TravelMode travelMode = TravelMode.DRIVING;
                                    CommuteTimeSetting commuteTimeSetting3 = this.this$0.j;
                                    if (commuteTimeSetting3 != null) {
                                        long a2 = v.a(address, travelMode, commuteTimeSetting3.getAvoidTolls(), this.$location.getLatitude(), this.$location.getLongitude());
                                        if (a2 != -1) {
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String a3 = CommuteTimeService.C.a();
                                            local.d(a3, "getDurationTime duration " + a2);
                                            this.this$0.i = a2;
                                            if (this.this$0.i < ((long) 60)) {
                                                this.this$0.i = 60;
                                            }
                                            this.this$0.A();
                                        } else {
                                            this.this$0.a();
                                        }
                                    } else {
                                        pq7.i();
                                        throw null;
                                    }
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<TResult> implements ht3<Location> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ d f4713a;

            @DexIgnore
            public a(d dVar) {
                this.f4713a = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ht3
            public final void onComplete(nt3<Location> nt3) {
                pq7.c(nt3, "task");
                if (!nt3.q() || nt3.m() == null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = CommuteTimeService.C.a();
                    local.d(a2, "getLastLocation:exception" + nt3.l());
                    this.f4713a.b.a();
                    return;
                }
                CommuteTimeService commuteTimeService = this.f4713a.b;
                Location w = commuteTimeService.w();
                if (w != null) {
                    commuteTimeService.D(commuteTimeService.y(w, nt3.m()));
                    long currentTimeMillis = System.currentTimeMillis();
                    Location w2 = this.f4713a.b.w();
                    if (w2 == null) {
                        pq7.i();
                        throw null;
                    } else if (currentTimeMillis - w2.getTime() > ((long) CommuteTimeService.A)) {
                        FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "Runnable after 5s over 5 mins not trust");
                        this.f4713a.b.a();
                    } else {
                        CommuteTimeService commuteTimeService2 = this.f4713a.b;
                        Location w3 = commuteTimeService2.w();
                        if (w3 != null) {
                            commuteTimeService2.u(w3);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        public d(CommuteTimeService commuteTimeService) {
            this.b = commuteTimeService;
        }

        @DexIgnore
        public final void run() {
            nt3<Location> s;
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "Runnable after 5s");
            if (this.b.e == null && this.b.k != null) {
                LocationManager locationManager = this.b.k;
                if (locationManager != null) {
                    locationManager.removeUpdates(this.b);
                    if (gl0.a(PortfolioApp.h0.c(), "android.permission.ACCESS_FINE_LOCATION") == 0 && gl0.a(PortfolioApp.h0.c(), "android.permission.ACCESS_COARSE_LOCATION") == 0 && LocationUtils.isLocationEnable(PortfolioApp.h0.c())) {
                        CommuteTimeService commuteTimeService = this.b;
                        LocationManager locationManager2 = commuteTimeService.k;
                        if (locationManager2 != null) {
                            commuteTimeService.D(locationManager2.getLastKnownLocation(this.b.l));
                            if (this.b.w() != null) {
                                ea3 ea3 = this.b.s;
                                if (ea3 != null && (s = ea3.s()) != null) {
                                    s.b(new a(this));
                                    return;
                                }
                                return;
                            }
                            this.b.a();
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                    FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "Runnable after 5s permission not granted");
                    this.b.a();
                    return;
                }
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<TResult> implements jt3<ja3> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ CommuteTimeService f4714a;

        @DexIgnore
        public e(CommuteTimeService commuteTimeService) {
            this.f4714a = commuteTimeService;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onSuccess(ja3 ja3) {
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "All location settings are satisfied");
            if (gl0.a(this.f4714a, "android.permission.ACCESS_FINE_LOCATION") == 0 || gl0.a(this.f4714a, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
                ea3 ea3 = this.f4714a.s;
                if (ea3 != null) {
                    ea3.u(this.f4714a.u, this.f4714a.w, Looper.myLooper());
                    return;
                }
                return;
            }
            this.f4714a.a();
            this.f4714a.E(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements it3 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ CommuteTimeService f4715a;

        @DexIgnore
        public f(CommuteTimeService commuteTimeService) {
            this.f4715a = commuteTimeService;
        }

        @DexIgnore
        @Override // com.fossil.it3
        public final void onFailure(Exception exc) {
            pq7.c(exc, "exception");
            int statusCode = ((n62) exc).getStatusCode();
            if (statusCode == 6) {
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "Location settings are not satisfied. Attempting to upgrade location settings ");
                this.f4715a.a();
            } else if (statusCode == 8502) {
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "Location settings are inadequate, and cannot be fixed here. Fix in Settings.");
                this.f4715a.E(false);
                this.f4715a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<TResult> implements ht3<Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ CommuteTimeService f4716a;

        @DexIgnore
        public g(CommuteTimeService commuteTimeService) {
            this.f4716a = commuteTimeService;
        }

        @DexIgnore
        @Override // com.fossil.ht3
        public final void onComplete(nt3<Void> nt3) {
            pq7.c(nt3, "it");
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "stopLocationUpdates success");
            this.f4716a.E(false);
        }
    }

    /*
    static {
        String simpleName = CommuteTimeService.class.getSimpleName();
        pq7.b(simpleName, "CommuteTimeService::class.java.simpleName");
        y = simpleName;
    }
    */

    @DexIgnore
    public final void A() {
        FLogger.INSTANCE.getLocal().d(y, "playHands");
        CommuteTimeSetting commuteTimeSetting = this.j;
        if (commuteTimeSetting == null) {
            pq7.i();
            throw null;
        } else if (pq7.a(commuteTimeSetting.getFormat(), "travel")) {
            C();
        } else {
            B();
        }
    }

    @DexIgnore
    public final void B() {
        FLogger.INSTANCE.getLocal().d(y, "playHandsETA");
        long currentTimeMillis = System.currentTimeMillis();
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "calendar");
        instance.setTimeInMillis(currentTimeMillis + (this.i * ((long) 1000)));
        int i2 = instance.get(11) % 12;
        int i3 = instance.get(12);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.d(str, "playHandsETA - duration=" + this.i + ", hour=" + i2 + ", minute=" + i3);
        try {
            PortfolioApp c2 = PortfolioApp.h0.c();
            String str2 = this.h;
            if (str2 != null) {
                c2.i1(str2, new CommuteTimeETAMicroAppResponse(i2, i3));
                a();
                return;
            }
            pq7.i();
            throw null;
        } catch (IllegalArgumentException e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = y;
            local2.d(str3, "playHandsETA exception exception=" + e2.getMessage());
        }
    }

    @DexIgnore
    public final void C() {
        FLogger.INSTANCE.getLocal().d(y, "playHandsMinute");
        int round = Math.round(((float) this.i) / 60.0f);
        try {
            PortfolioApp c2 = PortfolioApp.h0.c();
            String str = this.h;
            if (str != null) {
                c2.i1(str, new CommuteTimeTravelMicroAppResponse(round));
                a();
                return;
            }
            pq7.i();
            throw null;
        } catch (IllegalArgumentException e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = y;
            local.d(str2, "playHandsMinute exception exception=" + e2.getMessage());
        }
    }

    @DexIgnore
    public final void D(Location location) {
        this.f = location;
    }

    @DexIgnore
    public final void E(boolean z2) {
        this.g = z2;
    }

    @DexIgnore
    public final void F() {
        FLogger.INSTANCE.getLocal().d(y, "startLocationUpdates");
        if (gl0.a(PortfolioApp.h0.c(), "android.permission.ACCESS_FINE_LOCATION") == 0 && gl0.a(PortfolioApp.h0.c(), "android.permission.ACCESS_COARSE_LOCATION") == 0 && LocationUtils.isLocationEnable(PortfolioApp.h0.c())) {
            this.g = true;
            na3 na3 = this.t;
            if (na3 != null) {
                nt3<ja3> s2 = na3.s(this.v);
                s2.f(new e(this));
                s2.d(new f(this));
                return;
            }
            pq7.i();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(y, "startLocationUpdates permission not granted");
        a();
        this.g = false;
    }

    @DexIgnore
    public final void G() {
        nt3<Void> t2;
        if (!this.g) {
            FLogger.INSTANCE.getLocal().d(y, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }
        ea3 ea3 = this.s;
        if (ea3 != null && (t2 = ea3.t(this.w)) != null) {
            t2.b(new g(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.kr5
    public void a() {
        FLogger.INSTANCE.getLocal().d(y, VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        super.a();
        stopSelf();
    }

    @DexIgnore
    @Override // com.fossil.kr5
    public void b() {
        FLogger.INSTANCE.getLocal().d(y, "forceStop");
        a();
    }

    @DexIgnore
    @Override // com.fossil.kr5
    public IBinder onBind(Intent intent) {
        pq7.c(intent, "intent");
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        FLogger.INSTANCE.getLocal().d(y, "onCreate");
        PortfolioApp.h0.c().M().t1(this);
        this.m = new Handler();
        this.h = PortfolioApp.h0.c().J();
        this.s = ha3.a(this);
        this.t = ha3.b(this);
        s();
        t();
        r();
        x();
        F();
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(y, "onDestroy");
        super.onDestroy();
        G();
        Handler handler = this.m;
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            LocationManager locationManager = this.k;
            if (locationManager == null) {
                return;
            }
            if (locationManager != null) {
                locationManager.removeUpdates(this);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ga3
    public void onLocationChanged(Location location) {
        FLogger.INSTANCE.getLocal().d(y, "onLocationChanged");
        if (!(this.j == null || location == null || this.e != null)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = y;
            local.d(str, "onLocationChanged location=lat: " + location.getLatitude() + " long: " + location.getLongitude());
            this.e = location;
            if (location != null) {
                u(location);
            } else {
                pq7.i();
                throw null;
            }
        }
        LocationManager locationManager = this.k;
        if (locationManager == null) {
            return;
        }
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public void onProviderDisabled(String str) {
        pq7.c(str, "provider");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = y;
        local.d(str2, "onProviderDisabled provider=" + str);
    }

    @DexIgnore
    public void onProviderEnabled(String str) {
        pq7.c(str, "provider");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = y;
        local.d(str2, "onProviderEnabled provider=" + str);
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        Bundle extras;
        pq7.c(intent, "intent");
        FLogger.INSTANCE.getLocal().d(y, "onStartCommand");
        this.b = Action.MicroAppAction.SHOW_COMMUTE;
        super.d();
        if (this.j != null || (extras = intent.getExtras()) == null) {
            return 2;
        }
        String string = extras.getString(Constants.EXTRA_INFO);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        StringBuilder sb = new StringBuilder();
        sb.append("onStartCommand json=");
        if (string != null) {
            sb.append(string);
            local.d(str, sb.toString());
            this.j = (CommuteTimeSetting) new Gson().k(string, CommuteTimeSetting.class);
            Handler handler = this.m;
            if (handler != null) {
                handler.postDelayed(new d(this), 5000);
                return 2;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public void onStatusChanged(String str, int i2, Bundle bundle) {
        pq7.c(str, "provider");
        pq7.c(bundle, AppLinkData.ARGUMENTS_EXTRAS_KEY);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = y;
        local.d(str2, "onStatusChanged status=" + i2);
    }

    @DexIgnore
    public final void r() {
        ia3.a aVar = new ia3.a();
        LocationRequest locationRequest = this.u;
        if (locationRequest != null) {
            if (locationRequest != null) {
                aVar.a(locationRequest);
            } else {
                pq7.i();
                throw null;
            }
        }
        this.v = aVar.b();
    }

    @DexIgnore
    public final void s() {
        this.w = new b(this);
    }

    @DexIgnore
    public final void t() {
        FLogger.INSTANCE.getLocal().d(y, "createLocationRequest");
        LocationRequest locationRequest = new LocationRequest();
        this.u = locationRequest;
        if (locationRequest != null) {
            locationRequest.A(1000);
            LocationRequest locationRequest2 = this.u;
            if (locationRequest2 != null) {
                locationRequest2.k(1000);
                LocationRequest locationRequest3 = this.u;
                if (locationRequest3 != null) {
                    locationRequest3.F(100);
                    LocationRequest locationRequest4 = this.u;
                    if (locationRequest4 != null) {
                        locationRequest4.L(B);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void u(Location location) {
        pq7.c(location, PlaceFields.LOCATION);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.d(str, "getDurationTime location long=" + location.getLongitude() + " lat=" + location.getLatitude());
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new c(this, location, null), 3, null);
    }

    @DexIgnore
    public final pu5 v() {
        pu5 pu5 = this.x;
        if (pu5 != null) {
            return pu5;
        }
        pq7.n("mDurationUtils");
        throw null;
    }

    @DexIgnore
    public final Location w() {
        return this.f;
    }

    @DexIgnore
    public final void x() {
        boolean z2;
        FLogger.INSTANCE.getLocal().d(y, "initLocationManager");
        if (gl0.a(PortfolioApp.h0.c(), "android.permission.ACCESS_FINE_LOCATION") == 0 || gl0.a(PortfolioApp.h0.c(), "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            Criteria criteria = new Criteria();
            criteria.setPowerRequirement(1);
            Object systemService = getSystemService(PlaceFields.LOCATION);
            if (systemService != null) {
                LocationManager locationManager = (LocationManager) systemService;
                this.k = locationManager;
                if (locationManager == null) {
                    z2 = false;
                } else if (locationManager != null) {
                    z2 = locationManager.isProviderEnabled("network");
                } else {
                    pq7.i();
                    throw null;
                }
                if (z2) {
                    criteria.setAccuracy(2);
                } else {
                    criteria.setAccuracy(1);
                }
                LocationManager locationManager2 = this.k;
                if (locationManager2 != null) {
                    String bestProvider = locationManager2.getBestProvider(criteria, true);
                    this.l = bestProvider;
                    if (this.k != null && bestProvider != null) {
                        FLogger.INSTANCE.getLocal().d(y, "mLocationManager");
                        LocationManager locationManager3 = this.k;
                        if (locationManager3 != null) {
                            locationManager3.requestLocationUpdates(this.l, 0, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                throw new il7("null cannot be cast to non-null type android.location.LocationManager");
            }
        } else {
            a();
        }
    }

    @DexIgnore
    public final Location y(Location location, Location location2) {
        boolean z2 = true;
        pq7.c(location, PlaceFields.LOCATION);
        FLogger.INSTANCE.getLocal().d(y, "isBetterLocation location long=" + location.getLongitude() + " lat=" + location.getLatitude() + " time=" + location.getTime());
        if (location2 == null) {
            FLogger.INSTANCE.getLocal().d(y, "isBetterLocation currentBestLocation null");
            return location;
        }
        FLogger.INSTANCE.getLocal().d(y, "isBetterLocation currentBestLocation long=" + location2.getLongitude() + " lat=" + location2.getLatitude() + " time=" + location2.getTime());
        long time = location.getTime() - location2.getTime();
        boolean z3 = time > ((long) z);
        boolean z4 = time < ((long) (-z));
        boolean z5 = time > 0;
        if (z3) {
            FLogger.INSTANCE.getLocal().d(y, "isBetterLocation isSignificantlyNewer");
            return location;
        } else if (z4) {
            FLogger.INSTANCE.getLocal().d(y, "isBetterLocation isSignificantlyOlder");
            return location2;
        } else {
            FLogger.INSTANCE.getLocal().d(y, "isBetterLocation accuracy location=" + location.getAccuracy() + " currentBestLocation=" + location2.getAccuracy());
            int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
            boolean z6 = accuracy > 0;
            boolean z7 = accuracy < 0;
            if (accuracy <= 200) {
                z2 = false;
            }
            boolean z8 = z(location.getProvider(), location2.getProvider());
            if (z7) {
                FLogger.INSTANCE.getLocal().d(y, "isBetterLocation isMoreAccurate");
                return location;
            } else if (z5 && !z6) {
                FLogger.INSTANCE.getLocal().d(y, "isBetterLocation isNewer && isLessAccurate=false");
                return location;
            } else if (!z5 || z2 || !z8) {
                return location2;
            } else {
                FLogger.INSTANCE.getLocal().d(y, "isBetterLocation isNewer && isSignificantlyLessAccurate=false && isFromSameProvider");
                return location;
            }
        }
    }

    @DexIgnore
    public final boolean z(String str, String str2) {
        FLogger.INSTANCE.getLocal().d(y, "isSameProvider");
        return str == null ? str2 == null : pq7.a(str, str2);
    }
}
