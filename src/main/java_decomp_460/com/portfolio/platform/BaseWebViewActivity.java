package com.portfolio.platform;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.databinding.ViewDataBinding;
import com.fossil.aq0;
import com.fossil.el0;
import com.fossil.g15;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.vt7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"Registered"})
public class BaseWebViewActivity extends ls5 {
    @DexIgnore
    public static /* final */ String C;
    @DexIgnore
    public static /* final */ a D; // = new a(null);
    @DexIgnore
    public g15 A;
    @DexIgnore
    public String B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return BaseWebViewActivity.C;
        }

        @DexIgnore
        public final void b(Context context, String str, String str2) {
            pq7.c(context, "context");
            pq7.c(str, "title");
            pq7.c(str2, "url");
            Intent intent = new Intent(context, BaseWebViewActivity.class);
            intent.putExtra("urlToLoad", str2);
            intent.putExtra("title", str);
            context.startActivity(intent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends WebViewClient {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ BaseWebViewActivity f4682a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(BaseWebViewActivity baseWebViewActivity) {
            this.f4682a = baseWebViewActivity;
        }

        @DexIgnore
        @Override // android.webkit.WebViewClient
        @TargetApi(21)
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            Uri uri = null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = BaseWebViewActivity.D.a();
            StringBuilder sb = new StringBuilder();
            sb.append("Should override ");
            sb.append(webResourceRequest != null ? webResourceRequest.getUrl() : null);
            local.d(a2, sb.toString());
            if (!vt7.s(String.valueOf(webResourceRequest != null ? webResourceRequest.getUrl() : null), "mailto", false, 2, null)) {
                return super.shouldOverrideUrlLoading(webView, webResourceRequest);
            }
            FLogger.INSTANCE.getLocal().d(BaseWebViewActivity.D.a(), "We are overriding the mailto urlToLoad");
            if (webResourceRequest != null) {
                uri = webResourceRequest.getUrl();
            }
            String valueOf = String.valueOf(uri);
            if (valueOf != null) {
                String substring = valueOf.substring(7);
                pq7.b(substring, "(this as java.lang.String).substring(startIndex)");
                el0 d = el0.d(this.f4682a);
                pq7.b(d, "ShareCompat.IntentBuilde\u2026this@BaseWebViewActivity)");
                d.g("message/rfc822");
                d.a(substring);
                d.h();
                return true;
            }
            throw new il7("null cannot be cast to non-null type java.lang.String");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ BaseWebViewActivity b;

        @DexIgnore
        public c(BaseWebViewActivity baseWebViewActivity) {
            this.b = baseWebViewActivity;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.finish();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends WebChromeClient {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ BaseWebViewActivity f4683a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(BaseWebViewActivity baseWebViewActivity) {
            this.f4683a = baseWebViewActivity;
        }

        @DexIgnore
        public void onProgressChanged(WebView webView, int i) {
            pq7.c(webView, "view");
            if (i == 100) {
                this.f4683a.t();
            }
        }
    }

    /*
    static {
        String simpleName = BaseWebViewActivity.class.getSimpleName();
        pq7.b(simpleName, "BaseWebViewActivity::class.java.simpleName");
        C = simpleName;
    }
    */

    @DexIgnore
    public WebViewClient M() {
        FLogger.INSTANCE.getLocal().d(C, "Building default web client");
        return new b(this);
    }

    @DexIgnore
    public final void N() {
        if (Build.VERSION.SDK_INT >= 22) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
            return;
        }
        CookieSyncManager createInstance = CookieSyncManager.createInstance(getBaseContext());
        createInstance.startSync();
        CookieManager instance = CookieManager.getInstance();
        instance.removeAllCookie();
        instance.removeSessionCookie();
        createInstance.stopSync();
        createInstance.sync();
    }

    @DexIgnore
    public g15 O() {
        g15 g15 = this.A;
        if (g15 != null) {
            return g15;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public String P() {
        String str = this.B;
        if (str != null) {
            return str;
        }
        pq7.n("urlToLoad");
        throw null;
    }

    @DexIgnore
    public void Q(g15 g15) {
        pq7.c(g15, "<set-?>");
        this.A = g15;
    }

    @DexIgnore
    @SuppressLint({"SetJavaScriptEnabled"})
    public void R() {
        WebView webView = O().s;
        pq7.b(webView, "binding.webView");
        WebSettings settings = webView.getSettings();
        pq7.b(settings, "ws");
        settings.setSaveFormData(false);
        settings.setSavePassword(false);
        O().s.clearCache(true);
        WebView webView2 = O().s;
        pq7.b(webView2, "binding.webView");
        webView2.setWebViewClient(M());
        WebView webView3 = O().s;
        pq7.b(webView3, "binding.webView");
        WebSettings settings2 = webView3.getSettings();
        pq7.b(settings2, Constants.USER_SETTING);
        settings2.setJavaScriptEnabled(true);
        settings2.setDomStorageEnabled(true);
        settings2.setAllowFileAccess(true);
        settings2.setAllowFileAccessFromFileURLs(true);
        settings2.setAllowUniversalAccessFromFileURLs(true);
        settings2.setUserAgentString("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36");
        WebView webView4 = O().s;
        pq7.b(webView4, "binding.webView");
        webView4.setWebChromeClient(new d(this));
        if (PortfolioApp.h0.e() && TextUtils.isEmpty(P())) {
            FLogger.INSTANCE.getLocal().e(C, "You must create a urlToLoad to load before the webview is created");
        }
        O().s.loadUrl(P());
    }

    @DexIgnore
    public void S(String str) {
        pq7.c(str, "<set-?>");
        this.B = str;
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        String stringExtra;
        super.onCreate(bundle);
        if (getIntent().getStringExtra("urlToLoad") == null) {
            stringExtra = "";
        } else {
            stringExtra = getIntent().getStringExtra("urlToLoad");
            pq7.b(stringExtra, "intent.getStringExtra(KEY_URL)");
        }
        S(stringExtra);
        ViewDataBinding g = aq0.g(this, 2131558438);
        pq7.b(g, "DataBindingUtil.setConte\u2026.layout.activity_webview)");
        Q((g15) g);
        FlexibleTextView flexibleTextView = O().r;
        pq7.b(flexibleTextView, "binding.ftvTitle");
        flexibleTextView.setText(getIntent().getStringExtra("title"));
        O().q.setOnClickListener(new c(this));
        ls5.I(this, true, null, 2, null);
        N();
        R();
    }
}
