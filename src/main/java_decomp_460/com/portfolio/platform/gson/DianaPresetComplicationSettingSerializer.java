package com.portfolio.platform.gson;

import com.fossil.bj4;
import com.fossil.fj4;
import com.fossil.fj5;
import com.fossil.gj4;
import com.fossil.ij4;
import com.fossil.il7;
import com.fossil.kj4;
import com.fossil.lj4;
import com.fossil.pq7;
import com.google.gson.JsonElement;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import java.lang.reflect.Type;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetComplicationSettingSerializer implements lj4<List<? extends DianaPresetComplicationSetting>> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(List<DianaPresetComplicationSetting> list, Type type, kj4 kj4) {
        pq7.c(list, "src");
        bj4 bj4 = new bj4();
        ij4 ij4 = new ij4();
        for (DianaPresetComplicationSetting dianaPresetComplicationSetting : list) {
            String component1 = dianaPresetComplicationSetting.component1();
            String component2 = dianaPresetComplicationSetting.component2();
            String component3 = dianaPresetComplicationSetting.component3();
            String component4 = dianaPresetComplicationSetting.component4();
            gj4 gj4 = new gj4();
            gj4.n("complicationPosition", component1);
            gj4.n("appId", component2);
            gj4.n("localUpdatedAt", component3);
            if (!fj5.d(component4)) {
                try {
                    JsonElement c = ij4.c(component4);
                    if (c != null) {
                        gj4.k(Constants.USER_SETTING, (gj4) c);
                    } else {
                        throw new il7("null cannot be cast to non-null type com.google.gson.JsonObject");
                    }
                } catch (Exception e) {
                    gj4.k(Constants.USER_SETTING, new fj4());
                }
            } else {
                gj4.k(Constants.USER_SETTING, new fj4());
            }
            bj4.k(gj4);
        }
        return bj4;
    }
}
