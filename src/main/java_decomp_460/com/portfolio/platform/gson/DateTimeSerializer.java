package com.portfolio.platform.gson;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.hj4;
import com.fossil.jj4;
import com.fossil.kj4;
import com.fossil.lj4;
import com.fossil.lk5;
import com.fossil.pq7;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DateTimeSerializer implements dj4<DateTime>, lj4<DateTime> {
    @DexIgnore
    /* renamed from: a */
    public DateTime deserialize(JsonElement jsonElement, Type type, cj4 cj4) throws hj4 {
        pq7.c(jsonElement, "je");
        String f = jsonElement.f();
        pq7.b(f, "je.asString");
        if (f.length() == 0) {
            return null;
        }
        return lk5.S(jsonElement.f());
    }

    @DexIgnore
    /* renamed from: b */
    public JsonElement serialize(DateTime dateTime, Type type, kj4 kj4) {
        String str;
        if (dateTime == null || (str = lk5.x0(dateTime)) == null) {
            str = "";
        }
        return new jj4(str);
    }
}
