package com.portfolio.platform.gson;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.gj4;
import com.fossil.jj5;
import com.fossil.pq7;
import com.google.gson.JsonElement;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaAppSettingDeserializer implements dj4<DianaAppSetting> {
    @DexIgnore
    /* renamed from: a */
    public DianaAppSetting deserialize(JsonElement jsonElement, Type type, cj4 cj4) {
        String str;
        if (jsonElement != null) {
            gj4 d = jsonElement.d();
            JsonElement p = d.p("id");
            pq7.b(p, "jsonObject.get(\"id\")");
            String f = p.f();
            JsonElement p2 = d.p("category");
            pq7.b(p2, "jsonObject.get(\"category\")");
            String f2 = p2.f();
            JsonElement p3 = d.p("appId");
            pq7.b(p3, "jsonObject.get(\"appId\")");
            String f3 = p3.f();
            if (d.p(Constants.USER_SETTING) != null) {
                JsonElement p4 = d.p(Constants.USER_SETTING);
                pq7.b(p4, "jsonObject.get(\"settings\")");
                str = jj5.a(p4.d());
            } else {
                str = "";
            }
            pq7.b(f, "id");
            pq7.b(f3, "appId");
            pq7.b(f2, "category");
            DianaAppSetting dianaAppSetting = new DianaAppSetting(f, f3, f2, str);
            dianaAppSetting.getPinType();
            return dianaAppSetting;
        }
        pq7.i();
        throw null;
    }
}
