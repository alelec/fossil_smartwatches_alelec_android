package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.model.PlaceFields;
import com.fossil.gp1;
import com.fossil.hp1;
import com.fossil.il7;
import com.fossil.im7;
import com.fossil.ip1;
import com.fossil.jp1;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rn1;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherWatchAppInfo implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ CurrentWeatherInfo currentWeatherInfo;
    @DexIgnore
    public /* final */ List<WeatherDayForecast> dayForecast; // = new ArrayList();
    @DexIgnore
    public /* final */ long expiredAt;
    @DexIgnore
    public /* final */ List<WeatherHourForecast> hourForecast; // = new ArrayList();
    @DexIgnore
    public /* final */ String location;
    @DexIgnore
    public /* final */ UserDisplayUnit.TemperatureUnit temperatureUnit;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherWatchAppInfo> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherWatchAppInfo createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new WeatherWatchAppInfo(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherWatchAppInfo[] newArray(int i) {
            return new WeatherWatchAppInfo[i];
        }
    }

    @DexIgnore
    public WeatherWatchAppInfo(Parcel parcel) {
        pq7.c(parcel, "parcel");
        String readString = parcel.readString();
        this.location = readString == null ? "" : readString;
        UserDisplayUnit.TemperatureUnit.Companion companion = UserDisplayUnit.TemperatureUnit.Companion;
        String readString2 = parcel.readString();
        this.temperatureUnit = companion.fromValue(readString2 == null ? "C" : readString2);
        CurrentWeatherInfo currentWeatherInfo2 = (CurrentWeatherInfo) parcel.readParcelable(CurrentWeatherInfo.class.getClassLoader());
        this.currentWeatherInfo = currentWeatherInfo2 == null ? new CurrentWeatherInfo() : currentWeatherInfo2;
        parcel.readTypedList(this.dayForecast, WeatherDayForecast.CREATOR);
        parcel.readTypedList(this.hourForecast, WeatherHourForecast.CREATOR);
        this.expiredAt = parcel.readLong();
    }

    @DexIgnore
    public WeatherWatchAppInfo(String str, UserDisplayUnit.TemperatureUnit temperatureUnit2, CurrentWeatherInfo currentWeatherInfo2, List<WeatherDayForecast> list, List<WeatherHourForecast> list2, long j) {
        pq7.c(str, PlaceFields.LOCATION);
        pq7.c(temperatureUnit2, "temperatureUnit");
        pq7.c(currentWeatherInfo2, "currentWeatherInfo");
        pq7.c(list, "dayForecast");
        pq7.c(list2, "hourForecast");
        this.location = str;
        this.temperatureUnit = temperatureUnit2;
        this.currentWeatherInfo = currentWeatherInfo2;
        this.dayForecast.addAll(list);
        this.hourForecast.addAll(list2);
        this.expiredAt = j;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final jp1 toSDKWeatherAppInfo() {
        List<WeatherDayForecast> list = this.dayForecast;
        ArrayList arrayList = new ArrayList(im7.m(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().toSDKWeatherDayForecast());
        }
        List<WeatherHourForecast> list2 = this.hourForecast;
        ArrayList arrayList2 = new ArrayList(im7.m(list2, 10));
        Iterator<T> it2 = list2.iterator();
        while (it2.hasNext()) {
            arrayList2.add(it2.next().toSDKWeatherHourForecast());
        }
        long j = this.expiredAt;
        String str = this.location;
        rn1 sDKTemperatureUnit = this.temperatureUnit.toSDKTemperatureUnit();
        gp1 sDKCurrentWeatherInfo = this.currentWeatherInfo.toSDKCurrentWeatherInfo();
        Object[] array = arrayList2.toArray(new ip1[0]);
        if (array != null) {
            ip1[] ip1Arr = (ip1[]) array;
            Object[] array2 = arrayList.toArray(new hp1[0]);
            if (array2 != null) {
                return new jp1(j, str, sDKTemperatureUnit, sDKCurrentWeatherInfo, ip1Arr, (hp1[]) array2);
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.location);
        parcel.writeString(this.temperatureUnit.getValue());
        parcel.writeParcelable(this.currentWeatherInfo, i);
        parcel.writeTypedList(this.dayForecast);
        parcel.writeTypedList(this.hourForecast);
        parcel.writeLong(this.expiredAt);
    }
}
