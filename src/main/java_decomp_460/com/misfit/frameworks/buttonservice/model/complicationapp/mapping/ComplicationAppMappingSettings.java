package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.em1;
import com.fossil.gv1;
import com.fossil.hv1;
import com.fossil.kq7;
import com.fossil.pq7;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationAppMappingSettings implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<ComplicationAppMappingSettings> CREATOR; // = new ComplicationAppMappingSettings$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public ComplicationAppMapping bottomAppMapping;
    @DexIgnore
    public ComplicationAppMapping leftAppMapping;
    @DexIgnore
    public ComplicationAppMapping rightAppMapping;
    @DexIgnore
    public /* final */ long timeStamp;
    @DexIgnore
    public ComplicationAppMapping topAppMapping;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final long compareTimeStamp(ComplicationAppMappingSettings complicationAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings2) {
            long j = 0;
            long timeStamp = complicationAppMappingSettings != null ? complicationAppMappingSettings.getTimeStamp() : 0;
            if (complicationAppMappingSettings2 != null) {
                j = complicationAppMappingSettings2.getTimeStamp();
            }
            return timeStamp - j;
        }

        @DexIgnore
        public final boolean isSettingsSame(ComplicationAppMappingSettings complicationAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings2) {
            if ((complicationAppMappingSettings != null || complicationAppMappingSettings2 == null) && (complicationAppMappingSettings == null || complicationAppMappingSettings2 != null)) {
                return pq7.a(complicationAppMappingSettings, complicationAppMappingSettings2);
            }
            return false;
        }
    }

    @DexIgnore
    public ComplicationAppMappingSettings(Parcel parcel) {
        this.timeStamp = parcel.readLong();
        ComplicationAppMapping complicationAppMapping = (ComplicationAppMapping) parcel.readParcelable(ComplicationAppMapping.class.getClassLoader());
        this.topAppMapping = complicationAppMapping == null ? new CaloriesComplicationAppMapping() : complicationAppMapping;
        ComplicationAppMapping complicationAppMapping2 = (ComplicationAppMapping) parcel.readParcelable(ComplicationAppMapping.class.getClassLoader());
        this.bottomAppMapping = complicationAppMapping2 == null ? new ActiveMinutesComplicationAppMapping() : complicationAppMapping2;
        ComplicationAppMapping complicationAppMapping3 = (ComplicationAppMapping) parcel.readParcelable(ComplicationAppMapping.class.getClassLoader());
        this.leftAppMapping = complicationAppMapping3 == null ? new DateComplicationAppMapping() : complicationAppMapping3;
        ComplicationAppMapping complicationAppMapping4 = (ComplicationAppMapping) parcel.readParcelable(ComplicationAppMapping.class.getClassLoader());
        this.rightAppMapping = complicationAppMapping4 == null ? new ChanceOfRainComplicationAppMapping() : complicationAppMapping4;
    }

    @DexIgnore
    public /* synthetic */ ComplicationAppMappingSettings(Parcel parcel, kq7 kq7) {
        this(parcel);
    }

    @DexIgnore
    public ComplicationAppMappingSettings(ComplicationAppMapping complicationAppMapping, ComplicationAppMapping complicationAppMapping2, ComplicationAppMapping complicationAppMapping3, ComplicationAppMapping complicationAppMapping4, long j) {
        pq7.c(complicationAppMapping, "topAppMapping");
        pq7.c(complicationAppMapping2, "bottomAppMapping");
        pq7.c(complicationAppMapping3, "leftAppMapping");
        pq7.c(complicationAppMapping4, "rightAppMapping");
        this.topAppMapping = complicationAppMapping;
        this.bottomAppMapping = complicationAppMapping2;
        this.leftAppMapping = complicationAppMapping3;
        this.rightAppMapping = complicationAppMapping4;
        this.timeStamp = j;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ComplicationAppMappingSettings)) {
            return false;
        }
        return pq7.a(getHash(), ((ComplicationAppMappingSettings) obj).getHash());
    }

    @DexIgnore
    public final String getHash() {
        String str = this.topAppMapping.getHash() + ":" + this.bottomAppMapping.getHash() + ":" + this.leftAppMapping + ":" + this.rightAppMapping;
        pq7.b(str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public final long getTimeStamp() {
        return this.timeStamp;
    }

    @DexIgnore
    public final gv1 toSDKSetting(boolean z) {
        return new gv1(new hv1[]{new em1(this.topAppMapping.toSDKSetting(z), this.rightAppMapping.toSDKSetting(z), this.bottomAppMapping.toSDKSetting(z), this.leftAppMapping.toSDKSetting(z))});
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        pq7.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeLong(this.timeStamp);
        parcel.writeParcelable(this.topAppMapping, i);
        parcel.writeParcelable(this.bottomAppMapping, i);
        parcel.writeParcelable(this.leftAppMapping, i);
        parcel.writeParcelable(this.rightAppMapping, i);
    }
}
