package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ReplyMessageMappingGroup implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public String iconFwPath;
    @DexIgnore
    public List<ReplyMessageMapping> replyMessageList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ReplyMessageMappingGroup> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ReplyMessageMappingGroup createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new ReplyMessageMappingGroup(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ReplyMessageMappingGroup[] newArray(int i) {
            return new ReplyMessageMappingGroup[i];
        }
    }

    @DexIgnore
    public ReplyMessageMappingGroup(Parcel parcel) {
        pq7.c(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        this.replyMessageList = arrayList;
        parcel.readTypedList(arrayList, ReplyMessageMapping.CREATOR);
        String readString = parcel.readString();
        this.iconFwPath = readString == null ? "" : readString;
    }

    @DexIgnore
    public ReplyMessageMappingGroup(List<ReplyMessageMapping> list, String str) {
        pq7.c(list, "replyMessageList");
        pq7.c(str, "iconFwPath");
        this.replyMessageList = list;
        this.iconFwPath = str;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getIconFwPath() {
        return this.iconFwPath;
    }

    @DexIgnore
    public final List<ReplyMessageMapping> getReplyMessageList() {
        return this.replyMessageList;
    }

    @DexIgnore
    public final void setIconFwPath(String str) {
        pq7.c(str, "<set-?>");
        this.iconFwPath = str;
    }

    @DexIgnore
    public final void setReplyMessageList(List<ReplyMessageMapping> list) {
        pq7.c(list, "<set-?>");
        this.replyMessageList = list;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeTypedList(this.replyMessageList);
            parcel.writeString(this.iconFwPath);
        }
    }
}
