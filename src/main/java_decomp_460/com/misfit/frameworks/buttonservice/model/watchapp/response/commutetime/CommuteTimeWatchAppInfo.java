package com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime;

import android.os.Parcel;
import com.facebook.share.internal.ShareConstants;
import com.fossil.iq1;
import com.fossil.jq1;
import com.fossil.lt1;
import com.fossil.mt1;
import com.fossil.np1;
import com.fossil.pq7;
import com.fossil.ry1;
import com.fossil.yl1;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppInfo extends DeviceAppResponse {
    @DexIgnore
    public int commuteTimeInMinute;
    @DexIgnore
    public String destination; // = "";
    @DexIgnore
    public String traffic; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppInfo(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
        String readString = parcel.readString();
        this.destination = readString == null ? "" : readString;
        this.commuteTimeInMinute = parcel.readInt();
        String readString2 = parcel.readString();
        this.traffic = readString2 == null ? "" : readString2;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppInfo(String str, int i, String str2) {
        super(np1.COMMUTE_TIME_WATCH_APP);
        pq7.c(str, ShareConstants.DESTINATION);
        pq7.c(str2, "traffic");
        this.destination = str;
        this.commuteTimeInMinute = i;
        this.traffic = str2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceData() {
        return new lt1(new yl1(this.destination, this.commuteTimeInMinute, this.traffic));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceResponse(jq1 jq1, ry1 ry1) {
        pq7.c(jq1, "deviceRequest");
        if (jq1 instanceof iq1) {
            return new lt1((iq1) jq1, new yl1(this.destination, this.commuteTimeInMinute, this.traffic));
        }
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.destination);
        parcel.writeInt(this.commuteTimeInMinute);
        parcel.writeString(this.traffic);
    }
}
