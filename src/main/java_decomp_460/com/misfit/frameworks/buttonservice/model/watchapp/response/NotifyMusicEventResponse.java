package com.misfit.frameworks.buttonservice.model.watchapp.response;

import android.os.Parcel;
import com.fossil.al7;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.vn1;
import com.fossil.wn1;
import com.fossil.xn1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotifyMusicEventResponse extends MusicResponse {
    @DexIgnore
    public /* final */ MusicMediaAction musicAction;
    @DexIgnore
    public /* final */ MusicMediaStatus status;

    @DexIgnore
    public enum MusicMediaAction {
        PLAY,
        PAUSE,
        TOGGLE_PLAY_PAUSE,
        NEXT,
        PREVIOUS,
        VOLUME_UP,
        VOLUME_DOWN,
        NONE;
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public final /* synthetic */ class WhenMappings {
                @DexIgnore
                public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

                /*
                static {
                    int[] iArr = new int[vn1.values().length];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[vn1.PLAY.ordinal()] = 1;
                    $EnumSwitchMapping$0[vn1.PAUSE.ordinal()] = 2;
                    $EnumSwitchMapping$0[vn1.TOGGLE_PLAY_PAUSE.ordinal()] = 3;
                    $EnumSwitchMapping$0[vn1.NEXT.ordinal()] = 4;
                    $EnumSwitchMapping$0[vn1.PREVIOUS.ordinal()] = 5;
                    $EnumSwitchMapping$0[vn1.VOLUME_UP.ordinal()] = 6;
                    $EnumSwitchMapping$0[vn1.VOLUME_DOWN.ordinal()] = 7;
                }
                */
            }

            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(kq7 kq7) {
                this();
            }

            @DexIgnore
            public final MusicMediaAction fromSDKMusicAction(vn1 vn1) {
                pq7.c(vn1, "sdkMusicAction");
                switch (WhenMappings.$EnumSwitchMapping$0[vn1.ordinal()]) {
                    case 1:
                        return MusicMediaAction.PLAY;
                    case 2:
                        return MusicMediaAction.PAUSE;
                    case 3:
                        return MusicMediaAction.TOGGLE_PLAY_PAUSE;
                    case 4:
                        return MusicMediaAction.NEXT;
                    case 5:
                        return MusicMediaAction.PREVIOUS;
                    case 6:
                        return MusicMediaAction.VOLUME_UP;
                    case 7:
                        return MusicMediaAction.VOLUME_DOWN;
                    default:
                        return MusicMediaAction.NONE;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[MusicMediaAction.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[MusicMediaAction.PLAY.ordinal()] = 1;
                $EnumSwitchMapping$0[MusicMediaAction.PAUSE.ordinal()] = 2;
                $EnumSwitchMapping$0[MusicMediaAction.TOGGLE_PLAY_PAUSE.ordinal()] = 3;
                $EnumSwitchMapping$0[MusicMediaAction.NEXT.ordinal()] = 4;
                $EnumSwitchMapping$0[MusicMediaAction.PREVIOUS.ordinal()] = 5;
                $EnumSwitchMapping$0[MusicMediaAction.VOLUME_UP.ordinal()] = 6;
                $EnumSwitchMapping$0[MusicMediaAction.VOLUME_DOWN.ordinal()] = 7;
                $EnumSwitchMapping$0[MusicMediaAction.NONE.ordinal()] = 8;
            }
            */
        }

        @DexIgnore
        public final vn1 toSDKMusicAction() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return vn1.PLAY;
                case 2:
                    return vn1.PAUSE;
                case 3:
                    return vn1.TOGGLE_PLAY_PAUSE;
                case 4:
                    return vn1.NEXT;
                case 5:
                    return vn1.PREVIOUS;
                case 6:
                    return vn1.VOLUME_UP;
                case 7:
                    return vn1.VOLUME_DOWN;
                case 8:
                    return null;
                default:
                    throw new al7();
            }
        }
    }

    @DexIgnore
    public enum MusicMediaStatus {
        SUCCESS,
        NO_MUSIC_PLAYER,
        FAIL_TO_TRIGGER;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[MusicMediaStatus.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[MusicMediaStatus.SUCCESS.ordinal()] = 1;
                $EnumSwitchMapping$0[MusicMediaStatus.NO_MUSIC_PLAYER.ordinal()] = 2;
                $EnumSwitchMapping$0[MusicMediaStatus.FAIL_TO_TRIGGER.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public final wn1 toSDKMusicActionStatus() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return wn1.SUCCESS;
            }
            if (i == 2) {
                return wn1.NO_MUSIC_PLAYER;
            }
            if (i == 3) {
                return wn1.FAIL_TO_TRIGGER;
            }
            throw new al7();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotifyMusicEventResponse(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
        this.musicAction = MusicMediaAction.values()[parcel.readInt()];
        this.status = MusicMediaStatus.values()[parcel.readInt()];
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotifyMusicEventResponse(MusicMediaAction musicMediaAction, MusicMediaStatus musicMediaStatus) {
        super("Music Event_" + musicMediaAction);
        pq7.c(musicMediaAction, "musicAction");
        pq7.c(musicMediaStatus, "status");
        this.musicAction = musicMediaAction;
        this.status = musicMediaStatus;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public String getHash() {
        String str = this.musicAction + ":" + this.status;
        pq7.b(str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public final MusicMediaAction getMusicAction() {
        return this.musicAction;
    }

    @DexIgnore
    public final MusicMediaStatus getStatus() {
        return this.status;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public String toRemoteLogString() {
        return super.toRemoteLogString() + ", musicAction=" + this.musicAction + ", status=" + this.status;
    }

    @DexIgnore
    public final xn1 toSDKMusicEvent() {
        vn1 sDKMusicAction = this.musicAction.toSDKMusicAction();
        if (sDKMusicAction != null) {
            return new xn1(sDKMusicAction, this.status.toSDKMusicActionStatus());
        }
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.musicAction.ordinal());
        parcel.writeInt(this.status.ordinal());
    }
}
