package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import com.fossil.jq1;
import com.fossil.mt1;
import com.fossil.np1;
import com.fossil.nq1;
import com.fossil.pq7;
import com.fossil.ry1;
import com.fossil.st1;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingMyPhoneMicroAppResponse extends DeviceAppResponse {
    @DexIgnore
    public RingMyPhoneMicroAppResponse() {
        super(np1.RING_MY_PHONE_MICRO_APP);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceResponse(jq1 jq1, ry1 ry1) {
        pq7.c(jq1, "deviceRequest");
        if (!(jq1 instanceof nq1) || ry1 == null) {
            return null;
        }
        return new st1((nq1) jq1, ry1);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
    }
}
