package com.misfit.frameworks.buttonservice.model.watchparams;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.a78;
import com.fossil.et7;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.su1;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchParamsFileMapping implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public byte[] data;
    @DexIgnore
    public /* final */ String dataContent;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WatchParamsFileMapping> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WatchParamsFileMapping createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            String readString = parcel.readString();
            if (readString != null) {
                try {
                    Class<?> cls = Class.forName(readString);
                    pq7.b(cls, "Class.forName(dynamicClassName!!)");
                    Constructor<?> declaredConstructor = cls.getDeclaredConstructor(Parcel.class);
                    pq7.b(declaredConstructor, "dynamicClass.getDeclared\u2026uctor(Parcel::class.java)");
                    declaredConstructor.setAccessible(true);
                    Object newInstance = declaredConstructor.newInstance(parcel);
                    if (newInstance != null) {
                        return (WatchParamsFileMapping) newInstance;
                    }
                    throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    return null;
                } catch (NoSuchMethodException e2) {
                    e2.printStackTrace();
                    return null;
                } catch (IllegalAccessException e3) {
                    e3.printStackTrace();
                    return null;
                } catch (InstantiationException e4) {
                    e4.printStackTrace();
                    return null;
                } catch (InvocationTargetException e5) {
                    e5.printStackTrace();
                    return null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WatchParamsFileMapping[] newArray(int i) {
            return new WatchParamsFileMapping[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public WatchParamsFileMapping(android.os.Parcel r2) {
        /*
            r1 = this;
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r2, r0)
            java.lang.String r0 = r2.readString()
            if (r0 == 0) goto L_0x0012
        L_0x000b:
            r1.<init>(r0)
            r1.initialize()
            return
        L_0x0012:
            java.lang.String r0 = ""
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public WatchParamsFileMapping(String str) {
        pq7.c(str, "dataContent");
        this.dataContent = str;
        initialize();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final void initialize() {
        String str = this.dataContent;
        Charset charset = et7.f986a;
        if (str != null) {
            byte[] bytes = str.getBytes(charset);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            this.data = a78.a(bytes);
            return;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final su1 toSDKSetting() {
        byte[] bArr = this.data;
        if (bArr != null) {
            return new su1(bArr);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(WatchParamsFileMapping.class.getName());
        parcel.writeString(this.dataContent);
    }
}
