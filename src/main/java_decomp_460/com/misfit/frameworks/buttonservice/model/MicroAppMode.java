package com.misfit.frameworks.buttonservice.model;

import com.misfit.frameworks.common.enums.Action;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum MicroAppMode {
    UNKNOWN,
    HID,
    STREAMING,
    GOAL_TRACKING,
    DISPLAY_ACTION;

    @DexIgnore
    public static MicroAppMode fromAction(int i) {
        return Action.HIDAction.isActionBelongToThisType(i) ? HID : Action.StreamingAction.isActionBelongToThisType(i) ? STREAMING : Action.GoalTracking.isActionBelongToThisType(i) ? GOAL_TRACKING : Action.DisplayAction.isActionBelongToThisType(i) ? DISPLAY_ACTION : UNKNOWN;
    }

    @DexIgnore
    public static MicroAppMode fromMappings(List<Mapping> list) {
        return (list == null || list.size() == 0) ? UNKNOWN : fromAction(list.get(0).getAction());
    }
}
