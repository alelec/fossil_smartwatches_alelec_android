package com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime;

import android.os.Parcel;
import com.fossil.eu1;
import com.fossil.iq1;
import com.fossil.jq1;
import com.fossil.lt1;
import com.fossil.mt1;
import com.fossil.np1;
import com.fossil.nt1;
import com.fossil.pq7;
import com.fossil.ry1;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppMessage extends DeviceAppResponse {
    @DexIgnore
    public eu1 deviceMessageType; // = eu1.END;
    @DexIgnore
    public String message; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[eu1.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[eu1.END.ordinal()] = 1;
            $EnumSwitchMapping$0[eu1.IN_PROGRESS.ordinal()] = 2;
        }
        */
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMessage(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
        String readString = parcel.readString();
        this.message = readString == null ? "" : readString;
        this.deviceMessageType = eu1.values()[parcel.readInt()];
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMessage(String str, eu1 eu1) {
        super(np1.COMMUTE_TIME_WATCH_APP);
        pq7.c(str, "message");
        pq7.c(eu1, "deviceMessageType");
        this.message = str;
        this.deviceMessageType = eu1;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceData() {
        return new lt1(new nt1(this.message, this.deviceMessageType));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceResponse(jq1 jq1, ry1 ry1) {
        pq7.c(jq1, "deviceRequest");
        if (!(jq1 instanceof iq1)) {
            return null;
        }
        int i = WhenMappings.$EnumSwitchMapping$0[this.deviceMessageType.ordinal()];
        return i != 1 ? i != 2 ? new lt1(new nt1(this.message, this.deviceMessageType)) : new lt1(new nt1(this.message, this.deviceMessageType)) : new lt1((iq1) jq1, new nt1(this.message, this.deviceMessageType));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.message);
        parcel.writeInt(this.deviceMessageType.ordinal());
    }
}
