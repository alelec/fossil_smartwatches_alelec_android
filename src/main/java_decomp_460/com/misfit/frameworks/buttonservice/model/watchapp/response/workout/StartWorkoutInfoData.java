package com.misfit.frameworks.buttonservice.model.watchapp.response.workout;

import android.os.Parcel;
import com.fossil.eu1;
import com.fossil.jq1;
import com.fossil.mt1;
import com.fossil.np1;
import com.fossil.nt1;
import com.fossil.pq1;
import com.fossil.pq7;
import com.fossil.ry1;
import com.fossil.ut1;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class StartWorkoutInfoData extends DeviceAppResponse {
    @DexIgnore
    public eu1 deviceMessageType; // = eu1.SUCCESS;
    @DexIgnore
    public String message; // = "";
    @DexIgnore
    public pq1 startRequest;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StartWorkoutInfoData(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
        String readString = parcel.readString();
        this.message = readString == null ? "" : readString;
        this.deviceMessageType = eu1.values()[parcel.readInt()];
        this.startRequest = (pq1) parcel.readParcelable(pq1.class.getClassLoader());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StartWorkoutInfoData(pq1 pq1, String str, eu1 eu1) {
        super(np1.WORKOUT_START);
        pq7.c(pq1, "startRequest");
        pq7.c(str, "message");
        pq7.c(eu1, "deviceMessageType");
        this.message = str;
        this.deviceMessageType = eu1;
        this.startRequest = pq1;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceResponse(jq1 jq1, ry1 ry1) {
        pq7.c(jq1, "deviceRequest");
        if (jq1 instanceof pq1) {
            return new ut1((pq1) jq1, new nt1(this.message, this.deviceMessageType));
        }
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.message);
        parcel.writeInt(this.deviceMessageType.ordinal());
        parcel.writeParcelable(this.startRequest, i);
    }
}
