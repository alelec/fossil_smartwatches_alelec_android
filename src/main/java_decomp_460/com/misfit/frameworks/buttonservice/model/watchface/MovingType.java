package com.misfit.frameworks.buttonservice.model.watchface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum MovingType {
    STEP,
    ACTIVE,
    BATTERY,
    CALORIES,
    RAIN,
    DATE,
    SECOND_TIME,
    WEATHER,
    HEART,
    TICKER,
    TEXT
}
