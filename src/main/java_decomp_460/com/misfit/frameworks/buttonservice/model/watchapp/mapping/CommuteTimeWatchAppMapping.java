package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import com.fossil.il7;
import com.fossil.pq7;
import com.fossil.ro1;
import com.fossil.ww1;
import com.fossil.yo1;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppMapping extends WatchAppMapping {
    @DexIgnore
    public List<String> destinations;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMapping(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        parcel.readStringList(arrayList);
        this.destinations = arrayList;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMapping(List<String> list) {
        super(WatchAppMapping.WatchAppMappingType.INSTANCE.getCOMMUTE_TIME());
        pq7.c(list, "destinations");
        this.destinations = list;
    }

    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0005: INVOKE  (r1v0 int) = 
      (r2v0 'this' com.misfit.frameworks.buttonservice.model.watchapp.mapping.CommuteTimeWatchAppMapping A[IMMUTABLE_TYPE, THIS])
     type: VIRTUAL call: com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping.getMType():int)] */
    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        pq7.b(sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping
    public yo1 toSDKSetting() {
        List<String> list = this.destinations;
        if (list != null) {
            Object[] array = list.toArray(new String[0]);
            if (array != null) {
                return new ro1(new ww1((String[]) array));
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
        pq7.n("destinations");
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        List<String> list = this.destinations;
        if (list != null) {
            parcel.writeStringList(list);
        } else {
            pq7.n("destinations");
            throw null;
        }
    }
}
