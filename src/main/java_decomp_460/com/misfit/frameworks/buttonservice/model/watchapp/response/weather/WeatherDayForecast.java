package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.al7;
import com.fossil.hp1;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.un1;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherDayForecast implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ float highTemperature;
    @DexIgnore
    public /* final */ float lowTemperature;
    @DexIgnore
    public /* final */ WeatherComplicationAppInfo.WeatherCondition weatherCondition;
    @DexIgnore
    public /* final */ WeatherWeekDay weekDay;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherDayForecast> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherDayForecast createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new WeatherDayForecast(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherDayForecast[] newArray(int i) {
            return new WeatherDayForecast[i];
        }
    }

    @DexIgnore
    public enum WeatherWeekDay {
        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[WeatherWeekDay.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[WeatherWeekDay.SUNDAY.ordinal()] = 1;
                $EnumSwitchMapping$0[WeatherWeekDay.MONDAY.ordinal()] = 2;
                $EnumSwitchMapping$0[WeatherWeekDay.TUESDAY.ordinal()] = 3;
                $EnumSwitchMapping$0[WeatherWeekDay.WEDNESDAY.ordinal()] = 4;
                $EnumSwitchMapping$0[WeatherWeekDay.THURSDAY.ordinal()] = 5;
                $EnumSwitchMapping$0[WeatherWeekDay.FRIDAY.ordinal()] = 6;
                $EnumSwitchMapping$0[WeatherWeekDay.SATURDAY.ordinal()] = 7;
            }
            */
        }

        @DexIgnore
        public final un1 toSDKWeekDay() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return un1.SUNDAY;
                case 2:
                    return un1.MONDAY;
                case 3:
                    return un1.TUESDAY;
                case 4:
                    return un1.WEDNESDAY;
                case 5:
                    return un1.THURSDAY;
                case 6:
                    return un1.FRIDAY;
                case 7:
                    return un1.SATURDAY;
                default:
                    throw new al7();
            }
        }
    }

    @DexIgnore
    public WeatherDayForecast(float f, float f2, WeatherComplicationAppInfo.WeatherCondition weatherCondition2, WeatherWeekDay weatherWeekDay) {
        pq7.c(weatherCondition2, "weatherCondition");
        pq7.c(weatherWeekDay, "weekDay");
        this.highTemperature = f;
        this.lowTemperature = f2;
        this.weatherCondition = weatherCondition2;
        this.weekDay = weatherWeekDay;
    }

    @DexIgnore
    public WeatherDayForecast(Parcel parcel) {
        pq7.c(parcel, "parcel");
        this.highTemperature = parcel.readFloat();
        this.lowTemperature = parcel.readFloat();
        this.weatherCondition = WeatherComplicationAppInfo.WeatherCondition.values()[parcel.readInt()];
        this.weekDay = WeatherWeekDay.values()[parcel.readInt()];
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final hp1 toSDKWeatherDayForecast() {
        return new hp1(this.weekDay.toSDKWeekDay(), this.highTemperature, this.lowTemperature, this.weatherCondition.toSdkWeatherCondition());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeFloat(this.highTemperature);
        parcel.writeFloat(this.lowTemperature);
        parcel.writeInt(this.weatherCondition.ordinal());
        parcel.writeInt(this.weekDay.ordinal());
    }
}
