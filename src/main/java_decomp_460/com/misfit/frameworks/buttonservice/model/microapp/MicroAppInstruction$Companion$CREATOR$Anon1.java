package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.il7;
import com.fossil.pq7;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppInstruction$Companion$CREATOR$Anon1 implements Parcelable.Creator<MicroAppInstruction> {
    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public MicroAppInstruction createFromParcel(Parcel parcel) {
        pq7.c(parcel, "in");
        String readString = parcel.readString();
        if (readString != null) {
            try {
                Class<?> cls = Class.forName(readString);
                pq7.b(cls, "Class.forName(dynamicClassName!!)");
                Constructor<?> declaredConstructor = cls.getDeclaredConstructor(Parcel.class);
                pq7.b(declaredConstructor, "dynamicClass.getDeclared\u2026uctor(Parcel::class.java)");
                declaredConstructor.setAccessible(true);
                Object newInstance = declaredConstructor.newInstance(parcel);
                if (newInstance != null) {
                    return (MicroAppInstruction) newInstance;
                }
                throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (NoSuchMethodException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
                return null;
            } catch (InstantiationException e4) {
                e4.printStackTrace();
                return null;
            } catch (InvocationTargetException e5) {
                e5.printStackTrace();
                return null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public MicroAppInstruction[] newArray(int i) {
        return new MicroAppInstruction[i];
    }
}
