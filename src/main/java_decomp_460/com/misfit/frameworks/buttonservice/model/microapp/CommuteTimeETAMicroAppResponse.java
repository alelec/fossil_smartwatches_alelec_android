package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import com.fossil.gq1;
import com.fossil.jq1;
import com.fossil.jt1;
import com.fossil.mt1;
import com.fossil.np1;
import com.fossil.pq7;
import com.fossil.ry1;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeETAMicroAppResponse extends DeviceAppResponse {
    @DexIgnore
    public int mHour;
    @DexIgnore
    public int mMinute;

    @DexIgnore
    public CommuteTimeETAMicroAppResponse(int i, int i2) {
        super(np1.COMMUTE_TIME_ETA_MICRO_APP);
        this.mHour = i;
        this.mMinute = i2;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeETAMicroAppResponse(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
        this.mHour = parcel.readInt();
        this.mMinute = parcel.readInt();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceResponse(jq1 jq1, ry1 ry1) {
        pq7.c(jq1, "deviceRequest");
        if (!(jq1 instanceof gq1) || ry1 == null) {
            return null;
        }
        return new jt1((gq1) jq1, ry1, this.mHour, this.mMinute);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.mHour);
        parcel.writeInt(this.mMinute);
    }
}
