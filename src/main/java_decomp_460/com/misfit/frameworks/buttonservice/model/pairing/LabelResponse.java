package com.misfit.frameworks.buttonservice.model.pairing;

import android.os.Parcel;
import com.fossil.kq7;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LabelResponse extends PairingResponse {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String DEFAULT_VERSION; // = "0.0";
    @DexIgnore
    public int code;
    @DexIgnore
    public String version; // = "0.0";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LabelResponse(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
        String readString = parcel.readString();
        this.version = readString == null ? "0.0" : readString;
        this.code = parcel.readInt();
    }

    @DexIgnore
    public LabelResponse(String str, int i) {
        pq7.c(str, "version");
        this.version = str;
        this.code = i;
    }

    @DexIgnore
    public final int getCode() {
        return this.code;
    }

    @DexIgnore
    public final String getVersion() {
        return this.version;
    }

    @DexIgnore
    public final void setCode(int i) {
        this.code = i;
    }

    @DexIgnore
    public final void setVersion(String str) {
        pq7.c(str, "<set-?>");
        this.version = str;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.pairing.PairingResponse
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.version);
        parcel.writeInt(this.code);
    }
}
