package com.misfit.frameworks.buttonservice.model.watchapp.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.model.LifeCountDownObject;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class MusicResponse implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<MusicResponse> CREATOR; // = new MusicResponse$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ int LIFE_COUNT_DOWN; // = 7;
    @DexIgnore
    public static /* final */ String TYPE_MUSIC_EVENT; // = "Music Event";
    @DexIgnore
    public static /* final */ String TYPE_MUSIC_TRACK_INFO; // = "Music Track Info";
    @DexIgnore
    public /* final */ long createdTime;
    @DexIgnore
    public /* final */ LifeCountDownObject lifeCountDownObject; // = new LifeCountDownObject(7);
    @DexIgnore
    public /* final */ String type;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public MusicResponse(Parcel parcel) {
        pq7.c(parcel, "parcel");
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "Calendar.getInstance()");
        this.createdTime = instance.getTimeInMillis();
        String readString = parcel.readString();
        this.type = readString == null ? "" : readString;
    }

    @DexIgnore
    public MusicResponse(String str) {
        pq7.c(str, "type");
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "Calendar.getInstance()");
        this.createdTime = instance.getTimeInMillis();
        this.type = str;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof MusicResponse)) {
            return false;
        }
        return pq7.a(getHash(), ((MusicResponse) obj).getHash());
    }

    @DexIgnore
    public final long getCreatedTime() {
        return this.createdTime;
    }

    @DexIgnore
    public abstract String getHash();

    @DexIgnore
    public final LifeCountDownObject getLifeCountDownObject() {
        return this.lifeCountDownObject;
    }

    @DexIgnore
    public final String getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public String toRemoteLogString() {
        return "Type: " + this.type + ", createdTime: " + this.createdTime;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        pq7.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(getClass().getName());
        parcel.writeString(this.type);
    }
}
