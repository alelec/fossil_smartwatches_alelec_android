package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.hn1;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.ry1;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.communite.ble.BleAdapter;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MisfitDeviceProfile implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<MisfitDeviceProfile> CREATOR; // = new MisfitDeviceProfile$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public String address;
    @DexIgnore
    public int batteryLevel;
    @DexIgnore
    public String deviceModel;
    @DexIgnore
    public String deviceSerial;
    @DexIgnore
    public String firmwareVersion;
    @DexIgnore
    public int gattState;
    @DexIgnore
    public HeartRateMode heartRateMode;
    @DexIgnore
    public int hidState;
    @DexIgnore
    public String locale;
    @DexIgnore
    public String localeVersion;
    @DexIgnore
    public short microAppMajorVersion;
    @DexIgnore
    public short microAppMinorVersion;
    @DexIgnore
    public String productName;
    @DexIgnore
    public ry1 uiPackageOSVersion;
    @DexIgnore
    public VibrationStrengthObj vibrationStrength;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final MisfitDeviceProfile cloneFrom(BleAdapter bleAdapter) {
            pq7.c(bleAdapter, "bleAdapter");
            String nameBySerial = DeviceIdentityUtils.getNameBySerial(bleAdapter.getSerial());
            String firmwareVersion = bleAdapter.getFirmwareVersion();
            String deviceModel = bleAdapter.getDeviceModel();
            short microAppMajorVersion = bleAdapter.getMicroAppMajorVersion();
            short microAppMinorVersion = bleAdapter.getMicroAppMinorVersion();
            ry1 uiPackageOSVersion = bleAdapter.getUiPackageOSVersion();
            String macAddress = bleAdapter.getMacAddress();
            pq7.b(nameBySerial, "productName");
            return new MisfitDeviceProfile(macAddress, nameBySerial, bleAdapter.getSerial(), deviceModel, firmwareVersion, bleAdapter.getBatteryLevel(), bleAdapter.getLocale(), bleAdapter.getGattState(), bleAdapter.getHidState(), microAppMajorVersion, microAppMinorVersion, bleAdapter.getHeartRateMode(), bleAdapter.getVibrationStrength(), bleAdapter.getLocaleVersion(), uiPackageOSVersion);
        }
    }

    @DexIgnore
    public MisfitDeviceProfile(Parcel parcel) {
        pq7.c(parcel, "parcel");
        String readString = parcel.readString();
        this.address = readString == null ? "" : readString;
        String readString2 = parcel.readString();
        this.productName = readString2 == null ? "" : readString2;
        String readString3 = parcel.readString();
        this.deviceSerial = readString3 == null ? "" : readString3;
        String readString4 = parcel.readString();
        this.deviceModel = readString4 == null ? "" : readString4;
        String readString5 = parcel.readString();
        this.firmwareVersion = readString5 == null ? "" : readString5;
        this.batteryLevel = parcel.readInt();
        String readString6 = parcel.readString();
        this.locale = readString6 == null ? "" : readString6;
        this.gattState = parcel.readInt();
        this.hidState = parcel.readInt();
        this.microAppMajorVersion = (short) ((short) parcel.readInt());
        this.microAppMinorVersion = (short) ((short) parcel.readInt());
        this.heartRateMode = HeartRateMode.Companion.fromValue(parcel.readInt());
        VibrationStrengthObj vibrationStrengthObj = (VibrationStrengthObj) parcel.readParcelable(VibrationStrengthObj.class.getClassLoader());
        this.vibrationStrength = vibrationStrengthObj == null ? VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel(hn1.a.MEDIUM, true) : vibrationStrengthObj;
        String readString7 = parcel.readString();
        this.localeVersion = readString7 == null ? "" : readString7;
        this.uiPackageOSVersion = (ry1) parcel.readParcelable(ry1.class.getClassLoader());
    }

    @DexIgnore
    public MisfitDeviceProfile(String str, String str2, String str3, String str4, String str5, int i, String str6, int i2, int i3, short s, short s2, HeartRateMode heartRateMode2, VibrationStrengthObj vibrationStrengthObj, String str7, ry1 ry1) {
        pq7.c(str, "address");
        pq7.c(str2, "productName");
        pq7.c(str3, DeviceLocation.COLUMN_DEVICE_SERIAL);
        pq7.c(str4, "deviceModel");
        pq7.c(str5, "firmwareVersion");
        pq7.c(str6, "locale");
        pq7.c(heartRateMode2, "heartRateMode");
        pq7.c(vibrationStrengthObj, "vibrationStrength");
        pq7.c(str7, "localeVersion");
        this.address = str;
        this.productName = str2;
        this.deviceSerial = str3;
        this.deviceModel = str4;
        this.firmwareVersion = str5;
        this.batteryLevel = i;
        this.locale = str6;
        this.gattState = i2;
        this.hidState = i3;
        this.microAppMajorVersion = (short) s;
        this.microAppMinorVersion = (short) s2;
        this.heartRateMode = heartRateMode2;
        this.vibrationStrength = vibrationStrengthObj;
        this.localeVersion = str7;
        this.uiPackageOSVersion = ry1;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MisfitDeviceProfile(String str, String str2, String str3, String str4, String str5, int i, String str6, int i2, int i3, short s, short s2, HeartRateMode heartRateMode2, VibrationStrengthObj vibrationStrengthObj, String str7, ry1 ry1, int i4, kq7 kq7) {
        this(str, str2, str3, str4, str5, i, str6, i2, i3, s, s2, (i4 & 2048) != 0 ? HeartRateMode.NONE : heartRateMode2, (i4 & 4096) != 0 ? new VibrationStrengthObj(2, false, 2, null) : vibrationStrengthObj, str7, ry1);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public MisfitDeviceProfile(String str, String str2, String str3, String str4, String str5, int i, String str6, int i2, int i3, short s, short s2, String str7, ry1 ry1) {
        this(str, str2, str3, str4, str5, i, str6, i2, i3, s, s2, HeartRateMode.NONE, new VibrationStrengthObj(2, false, 2, null), str7, ry1);
        pq7.c(str, "address");
        pq7.c(str2, "productName");
        pq7.c(str3, DeviceLocation.COLUMN_DEVICE_SERIAL);
        pq7.c(str4, "deviceModel");
        pq7.c(str5, "firmwareVersion");
        pq7.c(str6, "locale");
        pq7.c(str7, "localeVersion");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getAddress() {
        return this.address;
    }

    @DexIgnore
    public final int getBatteryLevel() {
        return this.batteryLevel;
    }

    @DexIgnore
    public final String getDeviceModel() {
        return this.deviceModel;
    }

    @DexIgnore
    public final String getDeviceSerial() {
        return this.deviceSerial;
    }

    @DexIgnore
    public final String getFirmwareVersion() {
        return this.firmwareVersion;
    }

    @DexIgnore
    public final int getGattState() {
        return this.gattState;
    }

    @DexIgnore
    public final HeartRateMode getHeartRateMode() {
        return this.heartRateMode;
    }

    @DexIgnore
    public final int getHidState() {
        return this.hidState;
    }

    @DexIgnore
    public final String getLocale() {
        return this.locale;
    }

    @DexIgnore
    public final String getLocaleVersion() {
        return this.localeVersion;
    }

    @DexIgnore
    public final short getMicroAppMajorVersion() {
        return this.microAppMajorVersion;
    }

    @DexIgnore
    public final short getMicroAppMinorVersion() {
        return this.microAppMinorVersion;
    }

    @DexIgnore
    public final String getProductName() {
        return this.productName;
    }

    @DexIgnore
    public final ry1 getUiPackageOSVersion() {
        return this.uiPackageOSVersion;
    }

    @DexIgnore
    public final VibrationStrengthObj getVibrationStrength() {
        return this.vibrationStrength;
    }

    @DexIgnore
    public String toString() {
        return "[MisfitDeviceProfile: address=" + this.address + ", serial=" + this.deviceSerial + ", name=" + this.productName + ", deviceModel=" + this.deviceModel + ", firmware=" + this.firmwareVersion + ", microAppMajorVersion=" + ((int) this.microAppMajorVersion) + ", microAppMinorVersion=" + ((int) this.microAppMinorVersion) + ", heartRateMode=" + this.heartRateMode + ", batteryLevel=" + this.batteryLevel + ", locale=" + this.locale + ", localeVersion=" + this.localeVersion + ", uiPackageOSVersion=" + this.uiPackageOSVersion + ", vibrationStrength=" + VibrationStrengthObj.Companion.toString() + "]";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "dest");
        parcel.writeString(this.address);
        parcel.writeString(this.productName);
        parcel.writeString(this.deviceSerial);
        parcel.writeString(this.deviceModel);
        parcel.writeString(this.firmwareVersion);
        parcel.writeInt(this.batteryLevel);
        parcel.writeString(this.locale);
        parcel.writeInt(this.gattState);
        parcel.writeInt(this.hidState);
        parcel.writeInt(this.microAppMajorVersion);
        parcel.writeInt(this.microAppMinorVersion);
        parcel.writeInt(this.heartRateMode.getValue());
        parcel.writeParcelable(this.vibrationStrength, i);
        parcel.writeString(this.localeVersion);
        parcel.writeParcelable(this.uiPackageOSVersion, i);
    }
}
