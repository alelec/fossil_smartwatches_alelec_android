package com.misfit.frameworks.buttonservice.model.animation;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class HandAnimationEnum {

    @DexIgnore
    public enum Direction {
        CLOCKWISE(1),
        COUNTER_CLOCKWISE(2),
        SHORTEST_PATH(3);
        
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public Direction(int i) {
            this.value = (byte) i;
        }

        @DexIgnore
        public static Direction fromValue(int i) {
            Direction[] values = values();
            for (Direction direction : values) {
                if (direction.getValue() == i) {
                    return direction;
                }
            }
            return SHORTEST_PATH;
        }

        @DexIgnore
        public int getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public enum HandId {
        HOUR(1),
        MINUTE(2),
        SUB_EYE(3),
        COMPLETE(4);
        
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public HandId(int i) {
            this.value = (byte) i;
        }

        @DexIgnore
        public static HandId fromValue(int i) {
            HandId[] values = values();
            for (HandId handId : values) {
                if (handId.getValue() == i) {
                    return handId;
                }
            }
            return HOUR;
        }

        @DexIgnore
        public int getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public enum MovingType {
        DISTANCE(1),
        POSITION(2);
        
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public MovingType(int i) {
            this.value = (byte) i;
        }

        @DexIgnore
        public static MovingType fromValue(int i) {
            MovingType[] values = values();
            for (MovingType movingType : values) {
                if (movingType.getValue() == i) {
                    return movingType;
                }
            }
            return DISTANCE;
        }

        @DexIgnore
        public int getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public enum Speed {
        FULL(1),
        HALF(2),
        QUARTER(3),
        EIGHTH(4),
        SIXTEENTH(5);
        
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public Speed(int i) {
            this.value = (byte) i;
        }

        @DexIgnore
        public static Speed fromValue(int i) {
            Speed[] values = values();
            for (Speed speed : values) {
                if (speed.getValue() == i) {
                    return speed;
                }
            }
            return FULL;
        }

        @DexIgnore
        public int getValue() {
            return this.value;
        }
    }
}
