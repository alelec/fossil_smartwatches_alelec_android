package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import com.facebook.places.model.PlaceFields;
import com.fossil.ct1;
import com.fossil.dm1;
import com.fossil.km1;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SecondTimezoneComplicationAppMapping extends ComplicationAppMapping {
    @DexIgnore
    public String location;
    @DexIgnore
    public int utcOffsetInMinutes;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SecondTimezoneComplicationAppMapping(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
        String readString = parcel.readString();
        this.location = readString == null ? "" : readString;
        this.utcOffsetInMinutes = parcel.readInt();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SecondTimezoneComplicationAppMapping(String str, int i) {
        super(ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getTIMEZONE_2_TYPE());
        pq7.c(str, PlaceFields.LOCATION);
        this.location = str;
        this.utcOffsetInMinutes = i;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping
    public String getHash() {
        String str = getMType() + ":" + this.location + ":" + this.utcOffsetInMinutes;
        pq7.b(str, "builder.toString()");
        return str;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping
    public dm1 toSDKSetting(boolean z) {
        return new km1(new ct1(this.location, this.utcOffsetInMinutes));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.location);
        parcel.writeInt(this.utcOffsetInMinutes);
    }
}
