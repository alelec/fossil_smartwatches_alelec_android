package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EmptyFirmwareData extends FirmwareData {
    @DexIgnore
    public EmptyFirmwareData() {
        super("", "", "");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public EmptyFirmwareData(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
    }
}
