package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppMappingSettings$Companion$CREATOR$Anon1 implements Parcelable.Creator<WatchAppMappingSettings> {
    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public WatchAppMappingSettings createFromParcel(Parcel parcel) {
        pq7.c(parcel, "parcel");
        return new WatchAppMappingSettings(parcel, null);
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public WatchAppMappingSettings[] newArray(int i) {
        return new WatchAppMappingSettings[i];
    }
}
