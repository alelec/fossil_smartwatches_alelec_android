package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fossil.rj4;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.BaseEnumType;
import com.j256.ormlite.support.DatabaseResults;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.common.enums.Gesture;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "mapping")
public class Mapping implements Parcelable {
    @DexIgnore
    public static /* final */ String COLUMN_ACTION; // = "action";
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_FAMILY; // = "deviceFamily";
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_ID; // = "deviceId";
    @DexIgnore
    public static /* final */ String COLUMN_EXTRA_INFO; // = "extraInfo";
    @DexIgnore
    public static /* final */ String COLUMN_GESTURE; // = "gesture";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_OBJECT_ID; // = "objectId";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ Parcelable.Creator<Mapping> CREATOR; // = new Anon1();
    @DexIgnore
    public boolean isServiceCommand;
    @DexIgnore
    @DatabaseField(columnName = "action")
    @rj4("action")
    public int mAction;
    @DexIgnore
    @DatabaseField(columnName = "deviceFamily")
    public String mDeviceFamily;
    @DexIgnore
    @DatabaseField(columnName = "deviceId")
    @rj4("deviceId")
    public String mDeviceId;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_EXTRA_INFO)
    @rj4(COLUMN_EXTRA_INFO)
    public String mExtraInfo;
    @DexIgnore
    @DatabaseField(columnName = "gesture", persisterClass = GestureDataType.class)
    @rj4("gesture")
    public Gesture mGesture;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String mId;
    @DexIgnore
    @DatabaseField(columnName = "objectId")
    @rj4("objectId")
    public String mObjectId;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    @rj4("updatedAt")
    public String mUpdatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<Mapping> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Mapping createFromParcel(Parcel parcel) {
            return new Mapping(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Mapping[] newArray(int i) {
            return new Mapping[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class GestureDataType extends BaseEnumType {
        @DexIgnore
        public static /* final */ GestureDataType singleTon; // = new GestureDataType();

        @DexIgnore
        public GestureDataType() {
            super(SqlType.INTEGER);
        }

        @DexIgnore
        public static GestureDataType getSingleton() {
            return singleTon;
        }

        @DexIgnore
        @Override // com.j256.ormlite.field.types.BaseDataType, com.j256.ormlite.field.DataPersister
        public Class<?> getPrimaryClass() {
            return Integer.TYPE;
        }

        @DexIgnore
        @Override // com.j256.ormlite.field.BaseFieldConverter, com.j256.ormlite.field.FieldConverter
        public Object javaToSqlArg(FieldType fieldType, Object obj) {
            return Integer.valueOf(((Gesture) obj).getValue());
        }

        @DexIgnore
        @Override // com.j256.ormlite.field.types.BaseDataType, com.j256.ormlite.field.DataPersister
        public Object makeConfigObject(FieldType fieldType) throws SQLException {
            HashMap hashMap = new HashMap();
            Gesture[] gestureArr = (Gesture[]) fieldType.getType().getEnumConstants();
            if (gestureArr != null) {
                for (Gesture gesture : gestureArr) {
                    hashMap.put(Integer.valueOf(gesture.getValue()), gesture);
                }
                return hashMap;
            }
            throw new SQLException("Field " + fieldType + " improperly configured as type " + this);
        }

        @DexIgnore
        @Override // com.j256.ormlite.field.types.BaseEnumType, com.j256.ormlite.field.types.BaseDataType, com.j256.ormlite.field.BaseFieldConverter, com.j256.ormlite.field.FieldConverter
        public Object parseDefaultString(FieldType fieldType, String str) {
            return Integer.valueOf(Integer.parseInt(str));
        }

        @DexIgnore
        @Override // com.j256.ormlite.field.types.BaseEnumType, com.j256.ormlite.field.types.BaseDataType, com.j256.ormlite.field.BaseFieldConverter
        public Object resultToSqlArg(FieldType fieldType, DatabaseResults databaseResults, int i) throws SQLException {
            return Integer.valueOf(databaseResults.getInt(i));
        }

        @DexIgnore
        @Override // com.j256.ormlite.field.BaseFieldConverter
        public Object sqlArgToJava(FieldType fieldType, Object obj, int i) throws SQLException {
            if (fieldType == null) {
                return obj;
            }
            Integer num = (Integer) obj;
            Map map = (Map) fieldType.getDataTypeConfigObj();
            return BaseEnumType.enumVal(fieldType, num, map == null ? null : (Enum) map.get(num), fieldType.getUnknownEnumVal());
        }
    }

    @DexIgnore
    public Mapping() {
        this.mExtraInfo = "";
    }

    @DexIgnore
    public Mapping(Parcel parcel) {
        this.mDeviceId = parcel.readString();
        this.mGesture = Gesture.fromInt(parcel.readInt());
        this.mAction = parcel.readInt();
        this.mExtraInfo = parcel.readString();
        this.mUpdatedAt = parcel.readString();
        this.mObjectId = parcel.readString();
        this.mId = parcel.readString();
        this.mDeviceFamily = parcel.readString();
        this.isServiceCommand = parcel.readByte() != 0;
    }

    @DexIgnore
    public Mapping(Gesture gesture, int i) {
        this.mGesture = gesture;
        this.mAction = i;
    }

    @DexIgnore
    public Mapping(Gesture gesture, int i, String str) {
        this(gesture, i);
        this.mExtraInfo = str;
    }

    @DexIgnore
    @Override // java.lang.Object
    public Mapping clone() {
        Mapping mapping = new Mapping();
        mapping.mDeviceId = this.mDeviceId;
        mapping.mGesture = this.mGesture;
        mapping.mAction = this.mAction;
        mapping.mExtraInfo = this.mExtraInfo;
        mapping.mUpdatedAt = this.mUpdatedAt;
        mapping.mObjectId = this.mObjectId;
        mapping.mDeviceFamily = this.mDeviceFamily;
        mapping.isServiceCommand = this.isServiceCommand;
        mapping.mId = this.mId;
        return mapping;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == null || !(obj instanceof Mapping)) {
            return false;
        }
        Mapping mapping = (Mapping) obj;
        String deviceFamily = mapping.getDeviceFamily();
        String deviceFamily2 = getDeviceFamily();
        boolean z2 = (TextUtils.isEmpty(deviceFamily) && TextUtils.isEmpty(deviceFamily2)) || (deviceFamily != null && deviceFamily.equalsIgnoreCase(deviceFamily2));
        if (mapping.getAction() != getAction() || mapping.getGesture() != getGesture() || !z2) {
            return false;
        }
        if (mapping.getAction() != 505) {
            return true;
        }
        if (mapping.getExtraInfo() != null) {
            return mapping.getExtraInfo().equalsIgnoreCase(getExtraInfo());
        }
        if (getExtraInfo() != null) {
            z = false;
        }
        return z;
    }

    @DexIgnore
    public int getAction() {
        return this.mAction;
    }

    @DexIgnore
    public String getDeviceFamily() {
        return this.mDeviceFamily;
    }

    @DexIgnore
    public String getDeviceId() {
        return this.mDeviceId;
    }

    @DexIgnore
    public String getExtraInfo() {
        return this.mExtraInfo;
    }

    @DexIgnore
    public Gesture getGesture() {
        return this.mGesture;
    }

    @DexIgnore
    public String getId() {
        return this.mDeviceId.concat(this.mGesture.toString());
    }

    @DexIgnore
    public String getObjectId() {
        return this.mObjectId;
    }

    @DexIgnore
    public String getUpdatedAt() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public boolean isServiceCommand() {
        return this.isServiceCommand;
    }

    @DexIgnore
    public void setAction(int i) {
        this.mAction = i;
    }

    @DexIgnore
    public void setDeviceFamily(String str) {
        this.mDeviceFamily = str;
    }

    @DexIgnore
    public void setDeviceId(String str) {
        this.mDeviceId = str;
        this.mDeviceFamily = DeviceIdentityUtils.getDeviceFamily(str).name();
    }

    @DexIgnore
    public void setExtraInfo(String str) {
        this.mExtraInfo = str;
    }

    @DexIgnore
    public void setGesture(Gesture gesture) {
        this.mGesture = gesture;
    }

    @DexIgnore
    public void setId(String str) {
        this.mId = str;
    }

    @DexIgnore
    public void setIsServiceCommand(boolean z) {
        this.isServiceCommand = z;
    }

    @DexIgnore
    public void setObjectId(String str) {
        this.mObjectId = str;
    }

    @DexIgnore
    public void setUpdatedAt(String str) {
        this.mUpdatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "Serial=" + getDeviceId() + ", deviceFamily=" + getDeviceFamily() + ", action=" + this.mAction + ", gesture=" + this.mGesture + ", extraInfo=" + this.mExtraInfo + "\n";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mDeviceId);
        parcel.writeInt(this.mGesture.getValue());
        parcel.writeInt(this.mAction);
        parcel.writeString(this.mExtraInfo);
        parcel.writeString(this.mUpdatedAt);
        parcel.writeString(this.mObjectId);
        parcel.writeString(this.mId);
        parcel.writeString(this.mDeviceFamily);
        parcel.writeByte(this.isServiceCommand ? (byte) 1 : 0);
    }
}
