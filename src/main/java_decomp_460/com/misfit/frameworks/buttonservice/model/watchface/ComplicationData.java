package com.misfit.frameworks.buttonservice.model.watchface;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public ThemeColour colour;
    @DexIgnore
    public byte[] data;
    @DexIgnore
    public boolean enableRingGoal;
    @DexIgnore
    public String name;
    @DexIgnore
    public TimeZoneData timeZoneData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ComplicationData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ComplicationData createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new ComplicationData(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ComplicationData[] newArray(int i) {
            return new ComplicationData[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ComplicationData(android.os.Parcel r7) {
        /*
            r6 = this;
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r7, r0)
            byte[] r1 = r7.createByteArray()
            if (r1 == 0) goto L_0x0043
        L_0x000b:
            java.lang.String r2 = r7.readString()
            if (r2 == 0) goto L_0x0052
            java.lang.String r0 = "parcel.readString()!!"
            com.fossil.pq7.b(r2, r0)
            java.lang.Class r0 = java.lang.Boolean.TYPE
            java.lang.ClassLoader r0 = r0.getClassLoader()
            java.lang.Object r0 = r7.readValue(r0)
            if (r0 == 0) goto L_0x004a
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r3 = r0.booleanValue()
            java.lang.Class<com.misfit.frameworks.buttonservice.model.watchface.TimeZoneData> r0 = com.misfit.frameworks.buttonservice.model.watchface.TimeZoneData.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r4 = r7.readParcelable(r0)
            com.misfit.frameworks.buttonservice.model.watchface.TimeZoneData r4 = (com.misfit.frameworks.buttonservice.model.watchface.TimeZoneData) r4
            java.lang.String r0 = r7.readString()
            if (r0 == 0) goto L_0x0047
        L_0x003a:
            com.misfit.frameworks.buttonservice.model.watchface.ThemeColour r5 = com.misfit.frameworks.buttonservice.model.watchface.ThemeColour.valueOf(r0)
            r0 = r6
            r0.<init>(r1, r2, r3, r4, r5)
            return
        L_0x0043:
            r0 = 0
            byte[] r1 = new byte[r0]
            goto L_0x000b
        L_0x0047:
            java.lang.String r0 = ""
            goto L_0x003a
        L_0x004a:
            com.fossil.il7 r0 = new com.fossil.il7
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Boolean"
            r0.<init>(r1)
            throw r0
        L_0x0052:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.watchface.ComplicationData.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public ComplicationData(byte[] bArr, String str, boolean z, TimeZoneData timeZoneData2, ThemeColour themeColour) {
        pq7.c(bArr, "data");
        pq7.c(str, "name");
        pq7.c(themeColour, "colour");
        this.data = bArr;
        this.name = str;
        this.enableRingGoal = z;
        this.timeZoneData = timeZoneData2;
        this.colour = themeColour;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ComplicationData(byte[] bArr, String str, boolean z, TimeZoneData timeZoneData2, ThemeColour themeColour, int i, kq7 kq7) {
        this((i & 1) != 0 ? new byte[0] : bArr, str, (i & 4) == 0 ? z : false, (i & 8) != 0 ? null : timeZoneData2, (i & 16) != 0 ? ThemeColour.DEFAULT : themeColour);
    }

    @DexIgnore
    public static /* synthetic */ ComplicationData copy$default(ComplicationData complicationData, byte[] bArr, String str, boolean z, TimeZoneData timeZoneData2, ThemeColour themeColour, int i, Object obj) {
        return complicationData.copy((i & 1) != 0 ? complicationData.data : bArr, (i & 2) != 0 ? complicationData.name : str, (i & 4) != 0 ? complicationData.enableRingGoal : z, (i & 8) != 0 ? complicationData.timeZoneData : timeZoneData2, (i & 16) != 0 ? complicationData.colour : themeColour);
    }

    @DexIgnore
    public final byte[] component1() {
        return this.data;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final boolean component3() {
        return this.enableRingGoal;
    }

    @DexIgnore
    public final TimeZoneData component4() {
        return this.timeZoneData;
    }

    @DexIgnore
    public final ThemeColour component5() {
        return this.colour;
    }

    @DexIgnore
    public final ComplicationData copy(byte[] bArr, String str, boolean z, TimeZoneData timeZoneData2, ThemeColour themeColour) {
        pq7.c(bArr, "data");
        pq7.c(str, "name");
        pq7.c(themeColour, "colour");
        return new ComplicationData(bArr, str, z, timeZoneData2, themeColour);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ComplicationData) {
                ComplicationData complicationData = (ComplicationData) obj;
                if (!pq7.a(this.data, complicationData.data) || !pq7.a(this.name, complicationData.name) || this.enableRingGoal != complicationData.enableRingGoal || !pq7.a(this.timeZoneData, complicationData.timeZoneData) || !pq7.a(this.colour, complicationData.colour)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ThemeColour getColour() {
        return this.colour;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.data;
    }

    @DexIgnore
    public final boolean getEnableRingGoal() {
        return this.enableRingGoal;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final TimeZoneData getTimeZoneData() {
        return this.timeZoneData;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        byte[] bArr = this.data;
        int hashCode = bArr != null ? Arrays.hashCode(bArr) : 0;
        String str = this.name;
        int hashCode2 = str != null ? str.hashCode() : 0;
        boolean z = this.enableRingGoal;
        if (z) {
            z = true;
        }
        TimeZoneData timeZoneData2 = this.timeZoneData;
        int hashCode3 = timeZoneData2 != null ? timeZoneData2.hashCode() : 0;
        ThemeColour themeColour = this.colour;
        if (themeColour != null) {
            i = themeColour.hashCode();
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((hashCode * 31) + hashCode2) * 31) + i2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public final void setColour(ThemeColour themeColour) {
        pq7.c(themeColour, "<set-?>");
        this.colour = themeColour;
    }

    @DexIgnore
    public final void setData(byte[] bArr) {
        pq7.c(bArr, "<set-?>");
        this.data = bArr;
    }

    @DexIgnore
    public final void setEnableRingGoal(boolean z) {
        this.enableRingGoal = z;
    }

    @DexIgnore
    public final void setName(String str) {
        pq7.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setTimeZoneData(TimeZoneData timeZoneData2) {
        this.timeZoneData = timeZoneData2;
    }

    @DexIgnore
    public String toString() {
        return "ComplicationData(data=" + Arrays.toString(this.data) + ", name=" + this.name + ", enableRingGoal=" + this.enableRingGoal + ", timeZoneData=" + this.timeZoneData + ", colour=" + this.colour + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeByteArray(this.data);
        parcel.writeString(this.name);
        parcel.writeValue(Boolean.valueOf(this.enableRingGoal));
        parcel.writeParcelable(this.timeZoneData, i);
        parcel.writeString(this.colour.name());
    }
}
