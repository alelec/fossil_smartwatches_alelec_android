package com.misfit.frameworks.buttonservice.model.watchface;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TickerData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public ThemeColour colour;
    @DexIgnore
    public byte[] data;
    @DexIgnore
    public String name;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<TickerData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public TickerData createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new TickerData(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public TickerData[] newArray(int i) {
            return new TickerData[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TickerData(android.os.Parcel r4) {
        /*
            r3 = this;
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r4, r0)
            byte[] r0 = r4.createByteArray()
            if (r0 == 0) goto L_0x0024
        L_0x000b:
            java.lang.String r2 = r4.readString()
            if (r2 == 0) goto L_0x002b
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.pq7.b(r2, r1)
            java.lang.String r1 = r4.readString()
            if (r1 == 0) goto L_0x0028
        L_0x001c:
            com.misfit.frameworks.buttonservice.model.watchface.ThemeColour r1 = com.misfit.frameworks.buttonservice.model.watchface.ThemeColour.valueOf(r1)
            r3.<init>(r0, r2, r1)
            return
        L_0x0024:
            r0 = 0
            byte[] r0 = new byte[r0]
            goto L_0x000b
        L_0x0028:
            java.lang.String r1 = ""
            goto L_0x001c
        L_0x002b:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.watchface.TickerData.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public TickerData(byte[] bArr, String str, ThemeColour themeColour) {
        pq7.c(bArr, "data");
        pq7.c(str, "name");
        pq7.c(themeColour, "colour");
        this.data = bArr;
        this.name = str;
        this.colour = themeColour;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ TickerData(byte[] bArr, String str, ThemeColour themeColour, int i, kq7 kq7) {
        this((i & 1) != 0 ? new byte[0] : bArr, str, (i & 4) != 0 ? ThemeColour.DEFAULT : themeColour);
    }

    @DexIgnore
    public static /* synthetic */ TickerData copy$default(TickerData tickerData, byte[] bArr, String str, ThemeColour themeColour, int i, Object obj) {
        if ((i & 1) != 0) {
            bArr = tickerData.data;
        }
        if ((i & 2) != 0) {
            str = tickerData.name;
        }
        if ((i & 4) != 0) {
            themeColour = tickerData.colour;
        }
        return tickerData.copy(bArr, str, themeColour);
    }

    @DexIgnore
    public final byte[] component1() {
        return this.data;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final ThemeColour component3() {
        return this.colour;
    }

    @DexIgnore
    public final TickerData copy(byte[] bArr, String str, ThemeColour themeColour) {
        pq7.c(bArr, "data");
        pq7.c(str, "name");
        pq7.c(themeColour, "colour");
        return new TickerData(bArr, str, themeColour);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof TickerData) {
                TickerData tickerData = (TickerData) obj;
                if (!pq7.a(this.data, tickerData.data) || !pq7.a(this.name, tickerData.name) || !pq7.a(this.colour, tickerData.colour)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ThemeColour getColour() {
        return this.colour;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.data;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        byte[] bArr = this.data;
        int hashCode = bArr != null ? Arrays.hashCode(bArr) : 0;
        String str = this.name;
        int hashCode2 = str != null ? str.hashCode() : 0;
        ThemeColour themeColour = this.colour;
        if (themeColour != null) {
            i = themeColour.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public final void setColour(ThemeColour themeColour) {
        pq7.c(themeColour, "<set-?>");
        this.colour = themeColour;
    }

    @DexIgnore
    public final void setData(byte[] bArr) {
        pq7.c(bArr, "<set-?>");
        this.data = bArr;
    }

    @DexIgnore
    public final void setName(String str) {
        pq7.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public String toString() {
        return "TickerData(data=" + Arrays.toString(this.data) + ", name=" + this.name + ", colour=" + this.colour + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeByteArray(this.data);
        parcel.writeString(this.name);
        parcel.writeString(this.colour.name());
    }
}
