package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.yo1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class WatchAppMapping implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<WatchAppMapping> CREATOR; // = new WatchAppMapping$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String FIELD_TYPE; // = "mType";
    @DexIgnore
    public int mType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String getFIELD_TYPE() {
            return WatchAppMapping.FIELD_TYPE;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class WatchAppMappingType {
        @DexIgnore
        public static /* final */ int BUDDY_CHALLENGE; // = 13;
        @DexIgnore
        public static /* final */ int COMMUTE_TIME; // = 12;
        @DexIgnore
        public static /* final */ int DIAGNOTICS; // = 2;
        @DexIgnore
        public static /* final */ int EMPTY; // = 5;
        @DexIgnore
        public static /* final */ WatchAppMappingType INSTANCE; // = new WatchAppMappingType();
        @DexIgnore
        public static /* final */ int MUSIC; // = 6;
        @DexIgnore
        public static /* final */ int NOTIFICATION_PANEL; // = 7;
        @DexIgnore
        public static /* final */ int STOP_WATCH; // = 8;
        @DexIgnore
        public static /* final */ int TIMER; // = 10;
        @DexIgnore
        public static /* final */ int WEATHER; // = 11;
        @DexIgnore
        public static /* final */ int WELLNESS_DASHBOARD; // = 3;
        @DexIgnore
        public static /* final */ int WORK_OUT; // = 4;

        @DexIgnore
        public final int getBUDDY_CHALLENGE() {
            return BUDDY_CHALLENGE;
        }

        @DexIgnore
        public final int getCOMMUTE_TIME() {
            return COMMUTE_TIME;
        }

        @DexIgnore
        public final int getDIAGNOTICS() {
            return DIAGNOTICS;
        }

        @DexIgnore
        public final int getEMPTY() {
            return EMPTY;
        }

        @DexIgnore
        public final int getMUSIC() {
            return MUSIC;
        }

        @DexIgnore
        public final int getNOTIFICATION_PANEL() {
            return NOTIFICATION_PANEL;
        }

        @DexIgnore
        public final int getSTOP_WATCH() {
            return STOP_WATCH;
        }

        @DexIgnore
        public final int getTIMER() {
            return TIMER;
        }

        @DexIgnore
        public final int getWEATHER() {
            return WEATHER;
        }

        @DexIgnore
        public final int getWELLNESS_DASHBOARD() {
            return WELLNESS_DASHBOARD;
        }

        @DexIgnore
        public final int getWORK_OUT() {
            return WORK_OUT;
        }
    }

    @DexIgnore
    public WatchAppMapping(int i) {
        this.mType = i;
    }

    @DexIgnore
    public WatchAppMapping(Parcel parcel) {
        pq7.c(parcel, "parcel");
        this.mType = parcel.readInt();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof WatchAppMapping)) {
            return false;
        }
        return pq7.a(getHash(), ((WatchAppMapping) obj).getHash());
    }

    @DexIgnore
    public abstract String getHash();

    @DexIgnore
    public final int getMType() {
        return this.mType;
    }

    @DexIgnore
    public final void setMType(int i) {
        this.mType = i;
    }

    @DexIgnore
    public abstract yo1 toSDKSetting();

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(getClass().getName());
        parcel.writeInt(this.mType);
    }
}
