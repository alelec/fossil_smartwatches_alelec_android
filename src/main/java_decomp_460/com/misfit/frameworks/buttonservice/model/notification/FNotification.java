package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FNotification implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ String appName;
    @DexIgnore
    public /* final */ String iconFwPath;
    @DexIgnore
    public /* final */ NotificationBaseObj.ANotificationType notificationType;
    @DexIgnore
    public /* final */ String packageName;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<FNotification> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public FNotification createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new FNotification(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public FNotification[] newArray(int i) {
            return new FNotification[i];
        }
    }

    @DexIgnore
    public FNotification() {
        this("", "", "", NotificationBaseObj.ANotificationType.values()[1]);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FNotification(android.os.Parcel r6) {
        /*
            r5 = this;
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r6, r0)
            java.lang.String r0 = r6.readString()
            java.lang.String r3 = ""
            if (r0 == 0) goto L_0x0027
        L_0x000d:
            java.lang.String r1 = r6.readString()
            if (r1 == 0) goto L_0x002a
        L_0x0013:
            java.lang.String r2 = r6.readString()
            if (r2 == 0) goto L_0x002d
        L_0x0019:
            com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType[] r3 = com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationType.values()
            int r4 = r6.readInt()
            r3 = r3[r4]
            r5.<init>(r0, r1, r2, r3)
            return
        L_0x0027:
            java.lang.String r0 = ""
            goto L_0x000d
        L_0x002a:
            java.lang.String r1 = ""
            goto L_0x0013
        L_0x002d:
            r2 = r3
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.notification.FNotification.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public FNotification(String str, String str2, String str3, NotificationBaseObj.ANotificationType aNotificationType) {
        pq7.c(str, "appName");
        pq7.c(str2, "packageName");
        pq7.c(str3, "iconFwPath");
        pq7.c(aNotificationType, "notificationType");
        this.appName = str;
        this.packageName = str2;
        this.iconFwPath = str3;
        this.notificationType = aNotificationType;
    }

    @DexIgnore
    public static /* synthetic */ FNotification copy$default(FNotification fNotification, String str, String str2, String str3, NotificationBaseObj.ANotificationType aNotificationType, int i, Object obj) {
        if ((i & 1) != 0) {
            str = fNotification.appName;
        }
        if ((i & 2) != 0) {
            str2 = fNotification.packageName;
        }
        if ((i & 4) != 0) {
            str3 = fNotification.iconFwPath;
        }
        if ((i & 8) != 0) {
            aNotificationType = fNotification.notificationType;
        }
        return fNotification.copy(str, str2, str3, aNotificationType);
    }

    @DexIgnore
    public final String component1() {
        return this.appName;
    }

    @DexIgnore
    public final String component2() {
        return this.packageName;
    }

    @DexIgnore
    public final String component3() {
        return this.iconFwPath;
    }

    @DexIgnore
    public final NotificationBaseObj.ANotificationType component4() {
        return this.notificationType;
    }

    @DexIgnore
    public final FNotification copy(String str, String str2, String str3, NotificationBaseObj.ANotificationType aNotificationType) {
        pq7.c(str, "appName");
        pq7.c(str2, "packageName");
        pq7.c(str3, "iconFwPath");
        pq7.c(aNotificationType, "notificationType");
        return new FNotification(str, str2, str3, aNotificationType);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof FNotification) {
                FNotification fNotification = (FNotification) obj;
                if (!pq7.a(this.appName, fNotification.appName) || !pq7.a(this.packageName, fNotification.packageName) || !pq7.a(this.iconFwPath, fNotification.iconFwPath) || !pq7.a(this.notificationType, fNotification.notificationType)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getAppName() {
        return this.appName;
    }

    @DexIgnore
    public final String getIconFwPath() {
        return this.iconFwPath;
    }

    @DexIgnore
    public final NotificationBaseObj.ANotificationType getNotificationType() {
        return this.notificationType;
    }

    @DexIgnore
    public final String getPackageName() {
        return this.packageName;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.appName;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.packageName;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.iconFwPath;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        NotificationBaseObj.ANotificationType aNotificationType = this.notificationType;
        if (aNotificationType != null) {
            i = aNotificationType.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "FNotification(appName=" + this.appName + ", packageName=" + this.packageName + ", iconFwPath=" + this.iconFwPath + ", notificationType=" + this.notificationType + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.appName);
        parcel.writeString(this.packageName);
        parcel.writeString(this.iconFwPath);
        parcel.writeInt(this.notificationType.ordinal());
    }
}
