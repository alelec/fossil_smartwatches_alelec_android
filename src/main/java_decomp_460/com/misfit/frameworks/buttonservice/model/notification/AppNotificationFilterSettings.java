package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppNotificationFilterSettings implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public List<AppNotificationFilter> mNotificationFilters;
    @DexIgnore
    public /* final */ long timeStamp;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<AppNotificationFilterSettings> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final long compareTimeStamp(AppNotificationFilterSettings appNotificationFilterSettings, AppNotificationFilterSettings appNotificationFilterSettings2) {
            long j = 0;
            long timeStamp = appNotificationFilterSettings != null ? appNotificationFilterSettings.getTimeStamp() : 0;
            if (appNotificationFilterSettings2 != null) {
                j = appNotificationFilterSettings2.getTimeStamp();
            }
            return timeStamp - j;
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public AppNotificationFilterSettings createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new AppNotificationFilterSettings(parcel);
        }

        @DexIgnore
        public final boolean isSettingsSame(AppNotificationFilterSettings appNotificationFilterSettings, AppNotificationFilterSettings appNotificationFilterSettings2) {
            List<AppNotificationFilter> arrayList;
            List<AppNotificationFilter> notificationFilters;
            ArrayList arrayList2 = (appNotificationFilterSettings == null || (notificationFilters = appNotificationFilterSettings.getNotificationFilters()) == null) ? new ArrayList() : notificationFilters;
            if (appNotificationFilterSettings2 == null || (arrayList = appNotificationFilterSettings2.getNotificationFilters()) == null) {
                arrayList = new ArrayList<>();
            }
            return arrayList2.size() == arrayList.size() && arrayList2.containsAll(arrayList);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public AppNotificationFilterSettings[] newArray(int i) {
            return new AppNotificationFilterSettings[i];
        }
    }

    @DexIgnore
    public AppNotificationFilterSettings(Parcel parcel) {
        pq7.c(parcel, "parcel");
        this.timeStamp = parcel.readLong();
        ArrayList arrayList = new ArrayList();
        this.mNotificationFilters = arrayList;
        parcel.readTypedList(arrayList, AppNotificationFilter.CREATOR);
    }

    @DexIgnore
    public AppNotificationFilterSettings(List<AppNotificationFilter> list, long j) {
        pq7.c(list, "notificationFilters");
        this.mNotificationFilters = list;
        this.timeStamp = j;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final List<AppNotificationFilter> getNotificationFilters() {
        return this.mNotificationFilters;
    }

    @DexIgnore
    public final long getTimeStamp() {
        return this.timeStamp;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeLong(this.timeStamp);
        parcel.writeTypedList(this.mNotificationFilters);
    }
}
