package com.misfit.frameworks.buttonservice.model.watchapp.response.buddychallenge;

import android.os.Parcel;
import com.fossil.dq1;
import com.fossil.ht1;
import com.fossil.il7;
import com.fossil.jq1;
import com.fossil.mt1;
import com.fossil.np1;
import com.fossil.pq7;
import com.fossil.ry1;
import com.fossil.xs1;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCListChallengeWatchAppInfo extends DeviceAppResponse {
    @DexIgnore
    public List<xs1> challenges;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BCListChallengeWatchAppInfo(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        this.challenges = arrayList;
        parcel.readList(arrayList, xs1.class.getClassLoader());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BCListChallengeWatchAppInfo(List<xs1> list) {
        super(np1.BUDDY_CHALLENGE_LIST_CHALLENGES);
        pq7.c(list, "challenges");
        this.challenges = new ArrayList();
        this.challenges = list;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceResponse(jq1 jq1, ry1 ry1) {
        pq7.c(jq1, "deviceRequest");
        if (!(jq1 instanceof dq1)) {
            return null;
        }
        dq1 dq1 = (dq1) jq1;
        Object[] array = this.challenges.toArray(new xs1[0]);
        if (array != null) {
            return new ht1(dq1, (xs1[]) array);
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeList(this.challenges);
    }
}
