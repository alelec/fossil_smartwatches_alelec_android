package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import com.fossil.pq7;
import com.fossil.so1;
import com.fossil.yo1;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DiagnosticsWatchAppMapping extends WatchAppMapping {
    @DexIgnore
    public DiagnosticsWatchAppMapping() {
        super(WatchAppMapping.WatchAppMappingType.INSTANCE.getDIAGNOTICS());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DiagnosticsWatchAppMapping(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
    }

    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0005: INVOKE  (r1v0 int) = 
      (r2v0 'this' com.misfit.frameworks.buttonservice.model.watchapp.mapping.DiagnosticsWatchAppMapping A[IMMUTABLE_TYPE, THIS])
     type: VIRTUAL call: com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping.getMType():int)] */
    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        pq7.b(sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping
    public yo1 toSDKSetting() {
        return new so1();
    }
}
