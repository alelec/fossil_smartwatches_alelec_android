package com.misfit.frameworks.buttonservice.source;

import android.content.Context;
import com.fossil.a68;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.net.URLConnection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FirmwareFileRepository implements FirmwareFileSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static FirmwareFileRepository INSTANCE;
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public String applicationFileDir;
    @DexIgnore
    public /* final */ FirmwareFileLocalSource mLocalFirmwareFileSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final FirmwareFileRepository getInstance(Context context, FirmwareFileLocalSource firmwareFileLocalSource) {
            pq7.c(context, "context");
            pq7.c(firmwareFileLocalSource, "firmwareFileLocalSource");
            FirmwareFileRepository firmwareFileRepository = FirmwareFileRepository.INSTANCE;
            return firmwareFileRepository != null ? firmwareFileRepository : new FirmwareFileRepository(context, firmwareFileLocalSource);
        }
    }

    @DexIgnore
    public FirmwareFileRepository(Context context, FirmwareFileLocalSource firmwareFileLocalSource) {
        pq7.c(context, "applicationContext");
        pq7.c(firmwareFileLocalSource, "mLocalFirmwareFileSource");
        this.mLocalFirmwareFileSource = firmwareFileLocalSource;
        String name = FirmwareFileRepository.class.getName();
        pq7.b(name, "FirmwareFileRepository::class.java.name");
        this.TAG = name;
        String file = context.getFilesDir().toString();
        pq7.b(file, "applicationContext.filesDir.toString()");
        this.applicationFileDir = file;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x012d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ java.lang.Object downloadFirmware$suspendImpl(com.misfit.frameworks.buttonservice.source.FirmwareFileRepository r10, java.lang.String r11, java.lang.String r12, java.lang.String r13, com.fossil.qn7 r14) {
        /*
        // Method dump skipped, instructions count: 308
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.source.FirmwareFileRepository.downloadFirmware$suspendImpl(com.misfit.frameworks.buttonservice.source.FirmwareFileRepository, java.lang.String, java.lang.String, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.source.FirmwareFileSource
    public Object downloadFirmware(String str, String str2, String str3, qn7<? super File> qn7) {
        return downloadFirmware$suspendImpl(this, str, str2, str3, qn7);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.source.FirmwareFileSource
    public String getFirmwareFilePath(String str) {
        pq7.c(str, "firmwareVersion");
        return this.applicationFileDir + "/" + a68.a(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.source.FirmwareFileSource
    public boolean isDownloaded(String str, String str2) {
        pq7.c(str, "firmwareVersion");
        pq7.c(str2, "checkSum");
        String firmwareFilePath = getFirmwareFilePath(str);
        return !(str2.length() == 0) ? this.mLocalFirmwareFileSource.verify(firmwareFilePath, str2) : this.mLocalFirmwareFileSource.getFile(firmwareFilePath) != null;
    }

    @DexIgnore
    public BufferedInputStream openConnectURL$buttonservice_release(String str) {
        pq7.c(str, "fileUrl");
        try {
            URL url = new URL(str);
            URLConnection openConnection = url.openConnection();
            openConnection.connect();
            pq7.b(openConnection, "connection");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local.d(str2, "openConnectURL(), filePath=" + url + ", size=" + ((long) openConnection.getContentLength()));
            return new BufferedInputStream(openConnection.getInputStream());
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local2.e(str3, "openConnectURL(), ex=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.source.FirmwareFileSource
    public byte[] readFirmware(String str) {
        byte[] bArr = null;
        pq7.c(str, "firmwareVersion");
        FileInputStream readFile = this.mLocalFirmwareFileSource.readFile(getFirmwareFilePath(str));
        if (readFile != null) {
            try {
                int size = (int) readFile.getChannel().size();
                byte[] bArr2 = new byte[size];
                int read = readFile.read(bArr2);
                if (read != size) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = this.TAG;
                    local.e(str2, "getOtaData() - expectedSize=" + size + ", readSize=" + read);
                } else {
                    bArr = bArr2;
                }
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = this.TAG;
                local2.e(str3, "getOtaData() - e=" + e);
            } catch (Throwable th) {
                readFile.close();
                throw th;
            }
            readFile.close();
        }
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str4 = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("readFirmware() - firmwareVersion=");
        sb.append(str);
        sb.append(", dataExist=");
        sb.append(bArr != null);
        local3.d(str4, sb.toString());
        return bArr;
    }
}
