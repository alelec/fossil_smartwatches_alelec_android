package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import com.fossil.b68;
import com.fossil.il7;
import com.fossil.pq7;
import com.fossil.ru1;
import com.fossil.wt7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppUtils {
    @DexIgnore
    public static /* final */ WatchAppUtils INSTANCE; // = new WatchAppUtils();
    @DexIgnore
    public static /* final */ String TAG;

    /*
    static {
        String name = WatchAppUtils.class.getName();
        pq7.b(name, "WatchAppUtils::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    public final ru1[] getWatchAppFiles(Context context) {
        FileInputStream fileInputStream;
        Throwable th;
        Throwable th2;
        pq7.c(context, "context");
        ArrayList arrayList = new ArrayList();
        File[] listFiles = new File(FileUtils.getDirectory(context, FileType.WATCH_APP)).listFiles();
        Boolean bool = SharePreferencesUtils.getInstance(context).getBoolean(SharePreferencesUtils.BC_STATUS, true);
        if (listFiles != null) {
            for (File file : listFiles) {
                try {
                    fileInputStream = new FileInputStream(file);
                    try {
                        byte[] g = b68.g(fileInputStream);
                        ru1.a aVar = ru1.CREATOR;
                        pq7.b(g, "byteArray");
                        ru1 b = aVar.b(g);
                        if (b != null) {
                            pq7.b(bool, "isBCOn");
                            if (bool.booleanValue()) {
                                arrayList.add(b);
                            } else if (!wt7.u(b.getBundleId(), "buddyChallengeApp", true)) {
                                arrayList.add(b);
                            }
                        }
                        b68.b(fileInputStream);
                    } catch (Exception e) {
                        e = e;
                        try {
                            FLogger.INSTANCE.getLocal().e(TAG, "getWatchAppFiles, exception=" + e);
                            b68.b(fileInputStream);
                        } catch (Throwable th3) {
                            th2 = th3;
                            th = th2;
                            b68.b(fileInputStream);
                            throw th;
                        }
                    } catch (Throwable th4) {
                        th = th4;
                        b68.b(fileInputStream);
                        throw th;
                    }
                } catch (Exception e2) {
                    e = e2;
                    fileInputStream = null;
                    FLogger.INSTANCE.getLocal().e(TAG, "getWatchAppFiles, exception=" + e);
                    b68.b(fileInputStream);
                } catch (Throwable th5) {
                    th2 = th5;
                    fileInputStream = null;
                    th = th2;
                    b68.b(fileInputStream);
                    throw th;
                }
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "file list=" + arrayList);
        Object[] array = arrayList.toArray(new ru1[0]);
        if (array != null) {
            return (ru1[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
