package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import com.facebook.internal.Utility;
import com.fossil.i68;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.db.DBConstants;
import com.misfit.frameworks.buttonservice.model.FileType;
import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FileUtils {
    @DexIgnore
    public static /* final */ String TAG; // = "FileUtils";

    @DexIgnore
    public static String bytesToString(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            sb.append(Integer.toString((b & 255) + 256, 16).substring(1));
        }
        return sb.toString().toLowerCase();
    }

    @DexIgnore
    public static String getDirectory(Context context, FileType fileType) {
        String downloadedPath = getDownloadedPath(context);
        File file = new File(downloadedPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return downloadedPath + fileType.getMValue();
    }

    @DexIgnore
    public static String getDownloadedPath(Context context) {
        return context.getFilesDir() + File.separator + DBConstants.BASE_DOWNLOADED_FILE_DIR_NAME + File.separator;
    }

    @DexIgnore
    public static boolean verifyDownloadFile(String str, String str2) {
        if (i68.b(str2)) {
            return false;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
            FileInputStream fileInputStream = new FileInputStream(str);
            byte[] bArr = new byte[2014];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read == -1) {
                    return str2.toLowerCase().equals(bytesToString(instance.digest()));
                } else if (read > 0) {
                    instance.update(bArr, 0, read);
                }
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local.e(str3, "Error inside " + TAG + ".verifyDownloadFile - e=" + e);
            return false;
        }
    }
}
