package com.misfit.frameworks.buttonservice.utils;

import com.fossil.gp7;
import com.fossil.qq7;
import com.misfit.frameworks.buttonservice.utils.NotificationUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationUtils$Companion$instance$Anon2 extends qq7 implements gp7<NotificationUtils> {
    @DexIgnore
    public static /* final */ NotificationUtils$Companion$instance$Anon2 INSTANCE; // = new NotificationUtils$Companion$instance$Anon2();

    @DexIgnore
    public NotificationUtils$Companion$instance$Anon2() {
        super(0);
    }

    @DexIgnore
    @Override // com.fossil.gp7
    public final NotificationUtils invoke() {
        return NotificationUtils.Singleton.INSTANCE.getINSTANCE();
    }
}
