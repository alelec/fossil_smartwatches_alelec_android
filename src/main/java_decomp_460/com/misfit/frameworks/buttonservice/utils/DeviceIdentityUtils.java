package com.misfit.frameworks.buttonservice.utils;

import android.text.TextUtils;
import com.fossil.i68;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DeviceIdentityUtils {
    @DexIgnore
    public static /* final */ String FAKE_SAM_SERIAL_NUMBER_PREFIX; // = "SAM-FSL";
    @DexIgnore
    public static /* final */ String FLASH_SERIAL_NUMBER_PREFIX; // = "F";
    @DexIgnore
    public static /* final */ String[] Q_MOTION_PREFIX; // = {"BF", "BM", "BK", "BS"};
    @DexIgnore
    public static /* final */ String RAY_SERIAL_NUMBER_PREFIX; // = "B0";
    @DexIgnore
    public static /* final */ String RMM_SERIAL_NUMBER_PREFIX; // = "C0";
    @DexIgnore
    public static /* final */ String SAM_DIANA_SERIAL_NUMBER_PREFIX; // = "D0";
    @DexIgnore
    public static /* final */ String SAM_MINI_SERIAL_NUMBER_PREFIX; // = "M0";
    @DexIgnore
    public static /* final */ String SAM_SE0_SERIAL_NUMBER_PREFIX; // = "Z0";
    @DexIgnore
    public static /* final */ String SAM_SERIAL_NUMBER_PREFIX; // = "W0";
    @DexIgnore
    public static /* final */ String SAM_SLIM_SERIAL_NUMBER_PREFIX; // = "L0";
    @DexIgnore
    public static /* final */ String SHINE2_SERIAL_NUMBER_PREFIX; // = "S2";
    @DexIgnore
    public static /* final */ String SHINE_SERIAL_NUMBER_PREFIX; // = "S";
    @DexIgnore
    public static /* final */ String SPEEDO_SERIAL_NUMBER_PREFIX; // = "SV0EZ";
    @DexIgnore
    public static /* final */ String SWAROVSKI_SERIAL_NUMBER_PREFIX; // = "SC";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Anon1 {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE;

        /*
        static {
            int[] iArr = new int[FossilDeviceSerialPatternUtil.DEVICE.values().length];
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE = iArr;
            try {
                iArr[FossilDeviceSerialPatternUtil.DEVICE.RMM.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.FAKE_SAM.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SAM.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SE0.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.DIANA.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
        }
        */
    }

    @DexIgnore
    public enum QMotionType {
        BLACK("DZ"),
        STAINLESS_SILVER("SZ"),
        GOLD("GZ"),
        ROSE_GOLD("R1"),
        BLUE("BZ"),
        COPPER("PZ");
        
        @DexIgnore
        public String value;

        @DexIgnore
        public QMotionType(String str) {
            this.value = str;
        }

        @DexIgnore
        public static QMotionType fromColorCode(String str) {
            QMotionType[] values = values();
            for (QMotionType qMotionType : values) {
                if (qMotionType.getValue().equalsIgnoreCase(str)) {
                    return qMotionType;
                }
            }
            return ROSE_GOLD;
        }

        @DexIgnore
        public String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public static MFDeviceFamily getDeviceFamily(String str) {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.getDeviceBySerial(str).ordinal()]) {
            case 1:
                return MFDeviceFamily.DEVICE_FAMILY_RMM;
            case 2:
                return MFDeviceFamily.DEVICE_FAMILY_Q_MOTION;
            case 3:
            case 4:
                return MFDeviceFamily.DEVICE_FAMILY_SAM;
            case 5:
                return MFDeviceFamily.DEVICE_FAMILY_SAM_SLIM;
            case 6:
                return MFDeviceFamily.DEVICE_FAMILY_SAM_MINI;
            case 7:
                return MFDeviceFamily.DEVICE_FAMILY_SE0;
            case 8:
                return MFDeviceFamily.DEVICE_FAMILY_DIANA;
            default:
                return MFDeviceFamily.UNKNOWN;
        }
    }

    @DexIgnore
    public static String getNameBySerial(String str) {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.getDeviceBySerial(str).ordinal()]) {
            case 1:
                return "RMM";
            case 2:
                return "Q Motion";
            case 3:
            case 4:
                return "Hybrid Smartwatch";
            case 5:
                return "SAM Slim";
            case 6:
                return "SAM Mini";
            default:
                return "UNKNOWN";
        }
    }

    @DexIgnore
    public static QMotionType getQMotionTypeBySerial(String str) {
        return (TextUtils.isEmpty(str) || str.length() < 4) ? QMotionType.ROSE_GOLD : QMotionType.fromColorCode(i68.i(str, 3, 5));
    }

    @DexIgnore
    public static boolean isDianaDevice(String str) {
        return str != null && str.startsWith(SAM_DIANA_SERIAL_NUMBER_PREFIX);
    }

    @DexIgnore
    public static boolean isFlash(String str) {
        return str != null && str.startsWith(FLASH_SERIAL_NUMBER_PREFIX);
    }

    @DexIgnore
    public static boolean isFlashButton(String str) {
        return str != null && str.equals(Constants.BUTTON_MODEL);
    }

    @DexIgnore
    public static boolean isMisfitDevice(String str) {
        return FossilDeviceSerialPatternUtil.isRmmDevice(str) || isFlash(str) || isFlashButton(str) || isRay(str) || isShine(str) || isShine2(str) || isSpeedoShine(str) || isSwarovskiShine(str) || isQMotion(str) || FossilDeviceSerialPatternUtil.isHybridSmartWatchDevice(str);
    }

    @DexIgnore
    public static boolean isQMotion(String str) {
        if (TextUtils.isEmpty(str) || str.length() <= 2) {
            return false;
        }
        String substring = str.substring(0, 2);
        for (String str2 : Q_MOTION_PREFIX) {
            if (substring.equalsIgnoreCase(str2)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static boolean isRay(String str) {
        return str != null && str.startsWith(RAY_SERIAL_NUMBER_PREFIX);
    }

    @DexIgnore
    public static boolean isShine(String str) {
        return str != null && str.startsWith(SHINE_SERIAL_NUMBER_PREFIX) && !str.startsWith("S2");
    }

    @DexIgnore
    public static boolean isShine2(String str) {
        return str != null && str.startsWith("S2");
    }

    @DexIgnore
    public static boolean isSpeedoShine(String str) {
        return str != null && str.startsWith(SPEEDO_SERIAL_NUMBER_PREFIX);
    }

    @DexIgnore
    public static boolean isSwarovskiShine(String str) {
        return str != null && str.startsWith(SWAROVSKI_SERIAL_NUMBER_PREFIX);
    }

    @DexIgnore
    public static boolean isWearOSDevice(String str) {
        return str == null || str.isEmpty();
    }
}
