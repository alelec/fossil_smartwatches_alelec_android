package com.misfit.frameworks.buttonservice.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BitmapUtils {
    @DexIgnore
    public static /* final */ BitmapUtils INSTANCE; // = new BitmapUtils();
    @DexIgnore
    public static /* final */ String TAG; // = "BitmapUtils";

    @DexIgnore
    public final Bitmap decodeBitmapFromDirectory(String str, int i, int i2) {
        pq7.c(str, "directory");
        try {
            return Bitmap.createScaledBitmap(BitmapFactory.decodeFile(str), i, i2, false);
        } catch (Exception e) {
            e.printStackTrace();
            FLogger.INSTANCE.getLocal().e(TAG, e.getMessage());
            return null;
        }
    }
}
