package com.misfit.frameworks.buttonservice.ble;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.al1;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.tk1;
import com.fossil.uk1;
import com.fossil.xk1;
import com.fossil.yk1;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ScanService implements tk1.b {
    @DexIgnore
    public static /* final */ long BLE_SCAN_TIMEOUT; // = 120000;
    @DexIgnore
    public static /* final */ int CONNECT_TIMEOUT; // = 10000;
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ int RETRIEVE_DEVICE_BOND_RSSI_MARK; // = -999999;
    @DexIgnore
    public static /* final */ int RETRIEVE_DEVICE_RSSI_MARK; // = 0;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public Handler autoStopHandler;
    @DexIgnore
    public /* final */ Runnable autoStopTask; // = new ScanService$autoStopTask$Anon1(this);
    @DexIgnore
    public Callback callback;
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public boolean isScanning; // = false;
    @DexIgnore
    public MFLog mfLog;
    @DexIgnore
    public long startScanTimestamp;
    @DexIgnore
    public /* final */ long tagTime;

    @DexIgnore
    public interface Callback {
        @DexIgnore
        void onDeviceFound(yk1 yk1, int i);

        @DexIgnore
        void onScanFail(uk1 uk1);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    /*
    static {
        String simpleName = ScanService.class.getSimpleName();
        pq7.b(simpleName, "ScanService::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ScanService(Context context2, Callback callback2, long j) {
        pq7.c(context2, "context");
        this.callback = callback2;
        this.tagTime = j;
        Context applicationContext = context2.getApplicationContext();
        pq7.b(applicationContext, "context.applicationContext");
        this.context = applicationContext;
    }

    @DexIgnore
    private final String getCollectionTagByActiveLog() {
        MFLog mFLog = this.mfLog;
        if (mFLog == null) {
            String l = Long.toString(this.tagTime);
            pq7.b(l, "java.lang.Long.toString(tagTime)");
            return l;
        } else if (mFLog != null) {
            return String.valueOf(mFLog.getStartTimeEpoch());
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    private final void log(String str) {
        MFLog mFLog = this.mfLog;
        if (mFLog == null) {
            return;
        }
        if (mFLog != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            MFLog mFLog2 = this.mfLog;
            if (mFLog2 != null) {
                sb.append(mFLog2.getSerial());
                sb.append(" - Scanning] ");
                sb.append(str);
                mFLog.log(sb.toString());
                return;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    private final void startAutoStopTimer() {
        stopAutoStopTimer();
        Handler handler = new Handler(Looper.getMainLooper());
        this.autoStopHandler = handler;
        if (handler != null) {
            handler.postDelayed(this.autoStopTask, 120000);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    private final void stopAutoStopTimer() {
        Handler handler = this.autoStopHandler;
        if (handler != null) {
            handler.removeCallbacks(this.autoStopTask);
        }
    }

    @DexIgnore
    public final yk1 buildDeviceBySerial(String str, String str2) {
        pq7.c(str, "serial");
        pq7.c(str2, "macAddress");
        try {
            return tk1.k.x(str, str2);
        } catch (Exception e) {
            log("BuildDeviceBySerrial, error:" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.tk1.b
    public void onDeviceFound(yk1 yk1, int i) {
        pq7.c(yk1, "device");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, ".onDeviceFound, serialNumber=" + yk1.M().getSerialNumber() + ", fastPairIdHex=" + yk1.M().getFastPairIdInHexString() + ", hashcode=" + hashCode());
        Callback callback2 = this.callback;
        if (callback2 == null) {
            FLogger.INSTANCE.getLocal().e(TAG, ".onDeviceFound, callback is null");
        } else if (callback2 != null) {
            callback2.onDeviceFound(yk1, i);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.tk1.b
    public void onStartScanFailed(uk1 uk1) {
        pq7.c(uk1, "scanError");
        stopScan();
        Callback callback2 = this.callback;
        if (callback2 == null) {
            FLogger.INSTANCE.getLocal().e(TAG, ".onStartScanFail, callback is null");
        } else if (callback2 != null) {
            callback2.onScanFail(uk1);
        } else {
            pq7.i();
            throw null;
        }
        this.callback = null;
    }

    @DexIgnore
    public final void setActiveDeviceLog(String str) {
        pq7.c(str, "serial");
        this.mfLog = MFLogManager.getInstance(this.context).getActiveLog(str);
    }

    @DexIgnore
    public final void startScan() {
        synchronized (this) {
            FLogger.INSTANCE.getLocal().d(TAG, ".startScan");
            this.isScanning = true;
            tk1.k.y(new xk1().setDeviceTypes(new al1[]{al1.DIANA, al1.MINI, al1.SLIM, al1.SE1, al1.WEAR_OS, al1.IVY}), this);
            log("Called start scan api v2.");
        }
    }

    @DexIgnore
    public final void startScanWithAutoStopTimer() {
        synchronized (this) {
            FLogger.INSTANCE.getLocal().d(TAG, ".startScanWithAutoStopTimer()");
            startAutoStopTimer();
            this.isScanning = true;
            tk1.k.y(new xk1().setDeviceTypes(new al1[]{al1.DIANA, al1.MINI, al1.SLIM, al1.SE1, al1.WEAR_OS, al1.IVY}), this);
            this.startScanTimestamp = System.currentTimeMillis();
        }
    }

    @DexIgnore
    public final void stopScan() {
        synchronized (this) {
            FLogger.INSTANCE.getLocal().d(TAG, ".stopScan");
            this.startScanTimestamp = -1;
            this.isScanning = false;
            stopAutoStopTimer();
            tk1.k.z(this);
            log("Called stop scan api v2.");
        }
    }
}
