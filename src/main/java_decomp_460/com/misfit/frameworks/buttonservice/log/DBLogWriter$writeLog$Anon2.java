package com.misfit.frameworks.buttonservice.log;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.DBLogWriter;
import com.misfit.frameworks.buttonservice.log.db.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$writeLog$2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$writeLog$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$writeLog$Anon2(DBLogWriter dBLogWriter, qn7 qn7) {
        super(2, qn7);
        this.this$0 = dBLogWriter;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        DBLogWriter$writeLog$Anon2 dBLogWriter$writeLog$Anon2 = new DBLogWriter$writeLog$Anon2(this.this$0, qn7);
        dBLogWriter$writeLog$Anon2.p$ = (iv7) obj;
        return dBLogWriter$writeLog$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((DBLogWriter$writeLog$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        DBLogWriter.IDBLogWriterCallback iDBLogWriterCallback;
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            int countExcept = this.this$0.logDao.countExcept(Log.Flag.SYNCING);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = DBLogWriter.TAG;
            local.d(str, ".writeLog(), dbCount=" + countExcept);
            if (countExcept >= this.this$0.thresholdValue && (iDBLogWriterCallback = this.this$0.callback) != null) {
                iDBLogWriterCallback.onReachDBThreshold();
            }
            return tl7.f3441a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
