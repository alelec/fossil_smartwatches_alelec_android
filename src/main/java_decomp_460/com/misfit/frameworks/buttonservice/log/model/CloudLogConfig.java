package com.misfit.frameworks.buttonservice.log.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CloudLogConfig implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ String accessKey;
    @DexIgnore
    public /* final */ String endPointBaseUrl;
    @DexIgnore
    public /* final */ String logBrandName;
    @DexIgnore
    public /* final */ String secretKey;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<CloudLogConfig> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CloudLogConfig createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new CloudLogConfig(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CloudLogConfig[] newArray(int i) {
            return new CloudLogConfig[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CloudLogConfig(android.os.Parcel r6) {
        /*
            r5 = this;
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r6, r0)
            java.lang.String r0 = r6.readString()
            java.lang.String r4 = ""
            if (r0 == 0) goto L_0x0023
        L_0x000d:
            java.lang.String r1 = r6.readString()
            if (r1 == 0) goto L_0x0026
        L_0x0013:
            java.lang.String r2 = r6.readString()
            if (r2 == 0) goto L_0x0029
        L_0x0019:
            java.lang.String r3 = r6.readString()
            if (r3 == 0) goto L_0x002c
        L_0x001f:
            r5.<init>(r0, r1, r2, r3)
            return
        L_0x0023:
            java.lang.String r0 = ""
            goto L_0x000d
        L_0x0026:
            java.lang.String r1 = ""
            goto L_0x0013
        L_0x0029:
            java.lang.String r2 = ""
            goto L_0x0019
        L_0x002c:
            r3 = r4
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.model.CloudLogConfig.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public CloudLogConfig(String str, String str2, String str3, String str4) {
        pq7.c(str, "logBrandName");
        pq7.c(str2, "endPointBaseUrl");
        pq7.c(str3, "accessKey");
        pq7.c(str4, "secretKey");
        this.logBrandName = str;
        this.endPointBaseUrl = str2;
        this.accessKey = str3;
        this.secretKey = str4;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getAccessKey() {
        return this.accessKey;
    }

    @DexIgnore
    public final String getEndPointBaseUrl() {
        return this.endPointBaseUrl;
    }

    @DexIgnore
    public final String getLogBrandName() {
        return this.logBrandName;
    }

    @DexIgnore
    public final String getSecretKey() {
        return this.secretKey;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.logBrandName);
        parcel.writeString(this.endPointBaseUrl);
        parcel.writeString(this.accessKey);
        parcel.writeString(this.secretKey);
    }
}
