package com.misfit.frameworks.buttonservice.log;

import com.fossil.ht7;
import com.fossil.jt7;
import com.fossil.mn7;
import com.fossil.pq7;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferLogWriter$renameToFullBufferFile$$inlined$sortBy$Anon1<T> implements Comparator<T> {
    @DexIgnore
    @Override // java.util.Comparator
    public final int compare(T t, T t2) {
        String str = null;
        T t3 = t;
        jt7 jt7 = new jt7("\\d+");
        pq7.b(t3, "it");
        String name = t3.getName();
        pq7.b(name, "it.name");
        ht7 find$default = jt7.find$default(jt7, name, 0, 2, null);
        String value = find$default != null ? find$default.getValue() : null;
        T t4 = t2;
        jt7 jt72 = new jt7("\\d+");
        pq7.b(t4, "it");
        String name2 = t4.getName();
        pq7.b(name2, "it.name");
        ht7 find$default2 = jt7.find$default(jt72, name2, 0, 2, null);
        if (find$default2 != null) {
            str = find$default2.getValue();
        }
        return mn7.c(value, str);
    }
}
