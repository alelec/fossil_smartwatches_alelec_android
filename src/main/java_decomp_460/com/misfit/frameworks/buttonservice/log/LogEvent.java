package com.misfit.frameworks.buttonservice.log;

import com.fossil.pq7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LogEvent {
    @DexIgnore
    public String appVersion;
    @DexIgnore
    public FLogger.Component component;
    @DexIgnore
    public String deviceModel;
    @DexIgnore
    public String fwVersion;
    @DexIgnore
    public /* final */ String id;
    @DexIgnore
    public FLogger.LogLevel logLevel;
    @DexIgnore
    public String logMessage;
    @DexIgnore
    public String phoneId;
    @DexIgnore
    public String phoneModel;
    @DexIgnore
    public String platform;
    @DexIgnore
    public String platformVersion;
    @DexIgnore
    public String sdkVersion;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public FLogger.Session session;
    @DexIgnore
    public transient Object tag;
    @DexIgnore
    public long timestamp;
    @DexIgnore
    public String userId;

    @DexIgnore
    public LogEvent(FLogger.LogLevel logLevel2, long j, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, FLogger.Component component2, FLogger.Session session2, String str10, String str11) {
        pq7.c(logLevel2, "logLevel");
        pq7.c(str, ButtonService.USER_ID);
        pq7.c(str2, "phoneId");
        pq7.c(str3, "appVersion");
        pq7.c(str4, "platform");
        pq7.c(str5, "platformVersion");
        pq7.c(str6, "phoneModel");
        pq7.c(str7, "fwVersion");
        pq7.c(str8, "sdkVersion");
        pq7.c(str9, "deviceModel");
        pq7.c(component2, "component");
        pq7.c(session2, Constants.SESSION);
        pq7.c(str10, "serialNumber");
        pq7.c(str11, "logMessage");
        this.logLevel = logLevel2;
        this.timestamp = j;
        this.userId = str;
        this.phoneId = str2;
        this.appVersion = str3;
        this.platform = str4;
        this.platformVersion = str5;
        this.phoneModel = str6;
        this.fwVersion = str7;
        this.sdkVersion = str8;
        this.deviceModel = str9;
        this.component = component2;
        this.session = session2;
        this.serialNumber = str10;
        this.logMessage = str11;
        String uuid = UUID.randomUUID().toString();
        pq7.b(uuid, "UUID.randomUUID().toString()");
        this.id = uuid;
    }

    @DexIgnore
    public final String getAppVersion() {
        return this.appVersion;
    }

    @DexIgnore
    public final FLogger.Component getComponent() {
        return this.component;
    }

    @DexIgnore
    public final String getDeviceModel() {
        return this.deviceModel;
    }

    @DexIgnore
    public final String getFwVersion() {
        return this.fwVersion;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final FLogger.LogLevel getLogLevel() {
        return this.logLevel;
    }

    @DexIgnore
    public final String getLogMessage() {
        return this.logMessage;
    }

    @DexIgnore
    public final String getPhoneId() {
        return this.phoneId;
    }

    @DexIgnore
    public final String getPhoneModel() {
        return this.phoneModel;
    }

    @DexIgnore
    public final String getPlatform() {
        return this.platform;
    }

    @DexIgnore
    public final String getPlatformVersion() {
        return this.platformVersion;
    }

    @DexIgnore
    public final String getSdkVersion() {
        return this.sdkVersion;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final FLogger.Session getSession() {
        return this.session;
    }

    @DexIgnore
    public final Object getTag() {
        return this.tag;
    }

    @DexIgnore
    public final long getTimestamp() {
        return this.timestamp;
    }

    @DexIgnore
    public final String getUserId() {
        return this.userId;
    }

    @DexIgnore
    public final void setAppVersion(String str) {
        pq7.c(str, "<set-?>");
        this.appVersion = str;
    }

    @DexIgnore
    public final void setComponent(FLogger.Component component2) {
        pq7.c(component2, "<set-?>");
        this.component = component2;
    }

    @DexIgnore
    public final void setDeviceModel(String str) {
        pq7.c(str, "<set-?>");
        this.deviceModel = str;
    }

    @DexIgnore
    public final void setFwVersion(String str) {
        pq7.c(str, "<set-?>");
        this.fwVersion = str;
    }

    @DexIgnore
    public final void setLogLevel(FLogger.LogLevel logLevel2) {
        pq7.c(logLevel2, "<set-?>");
        this.logLevel = logLevel2;
    }

    @DexIgnore
    public final void setLogMessage(String str) {
        pq7.c(str, "value");
        String substring = str.substring(0, Math.min(str.length(), 2000));
        pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        this.logMessage = substring;
    }

    @DexIgnore
    public final void setPhoneId(String str) {
        pq7.c(str, "<set-?>");
        this.phoneId = str;
    }

    @DexIgnore
    public final void setPhoneModel(String str) {
        pq7.c(str, "<set-?>");
        this.phoneModel = str;
    }

    @DexIgnore
    public final void setPlatform(String str) {
        pq7.c(str, "<set-?>");
        this.platform = str;
    }

    @DexIgnore
    public final void setPlatformVersion(String str) {
        pq7.c(str, "<set-?>");
        this.platformVersion = str;
    }

    @DexIgnore
    public final void setSdkVersion(String str) {
        pq7.c(str, "<set-?>");
        this.sdkVersion = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        pq7.c(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setSession(FLogger.Session session2) {
        pq7.c(session2, "<set-?>");
        this.session = session2;
    }

    @DexIgnore
    public final void setTag(Object obj) {
        this.tag = obj;
    }

    @DexIgnore
    public final void setTimestamp(long j) {
        this.timestamp = j;
    }

    @DexIgnore
    public final void setUserId(String str) {
        pq7.c(str, "<set-?>");
        this.userId = str;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        pq7.b(t, "Gson().toJson(this)");
        return t;
    }
}
