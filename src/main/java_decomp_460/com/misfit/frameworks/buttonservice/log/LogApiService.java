package com.misfit.frameworks.buttonservice.log;

import com.fossil.gj4;
import com.fossil.i98;
import com.fossil.t98;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface LogApiService {
    @DexIgnore
    @t98("app_log/event")
    Call<gj4> sendLogs(@i98 gj4 gj4);
}
