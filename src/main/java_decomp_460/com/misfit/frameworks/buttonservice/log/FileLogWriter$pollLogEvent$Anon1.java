package com.misfit.frameworks.buttonservice.log;

import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.misfit.frameworks.buttonservice.log.FileLogWriter$pollLogEvent$1", f = "FileLogWriter.kt", l = {133}, m = "invokeSuspend")
public final class FileLogWriter$pollLogEvent$Anon1 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ FileLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FileLogWriter$pollLogEvent$Anon1(FileLogWriter fileLogWriter, qn7 qn7) {
        super(2, qn7);
        this.this$0 = fileLogWriter;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        FileLogWriter$pollLogEvent$Anon1 fileLogWriter$pollLogEvent$Anon1 = new FileLogWriter$pollLogEvent$Anon1(this.this$0, qn7);
        fileLogWriter$pollLogEvent$Anon1.p$ = (iv7) obj;
        return fileLogWriter$pollLogEvent$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((FileLogWriter$pollLogEvent$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0131, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0132, code lost:
        com.fossil.so7.a(r4, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0135, code lost:
        throw r3;
     */
    @DexIgnore
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
        /*
        // Method dump skipped, instructions count: 314
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.FileLogWriter$pollLogEvent$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
