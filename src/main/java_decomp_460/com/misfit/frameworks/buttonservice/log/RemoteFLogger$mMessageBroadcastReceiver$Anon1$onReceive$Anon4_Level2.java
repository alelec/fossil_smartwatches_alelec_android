package com.misfit.frameworks.buttonservice.log;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$mMessageBroadcastReceiver$1$onReceive$4", f = "RemoteFLogger.kt", l = {99}, m = "invokeSuspend")
public final class RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger$mMessageBroadcastReceiver$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2(RemoteFLogger$mMessageBroadcastReceiver$Anon1 remoteFLogger$mMessageBroadcastReceiver$Anon1, qn7 qn7) {
        super(2, qn7);
        this.this$0 = remoteFLogger$mMessageBroadcastReceiver$Anon1;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2 remoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2 = new RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2(this.this$0, qn7);
        remoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2.p$ = (iv7) obj;
        return remoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            RemoteFLogger remoteFLogger = this.this$0.this$0;
            this.L$0 = iv7;
            this.label = 1;
            if (remoteFLogger.flushBuffer(this) == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return tl7.f3441a;
    }
}
