package com.misfit.frameworks.buttonservice.log;

import com.fossil.kq7;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Failure<T> extends RepoResponse<T> {
    @DexIgnore
    public /* final */ int code;
    @DexIgnore
    public /* final */ String errorItems;
    @DexIgnore
    public /* final */ ServerError serverError;
    @DexIgnore
    public /* final */ Throwable throwable;

    @DexIgnore
    public Failure(int i, ServerError serverError2, Throwable th, String str) {
        super(null);
        this.code = i;
        this.serverError = serverError2;
        this.throwable = th;
        this.errorItems = str;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Failure(int i, ServerError serverError2, Throwable th, String str, int i2, kq7 kq7) {
        this(i, serverError2, (i2 & 4) != 0 ? null : th, (i2 & 8) != 0 ? null : str);
    }

    @DexIgnore
    public static /* synthetic */ Failure copy$default(Failure failure, int i, ServerError serverError2, Throwable th, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = failure.code;
        }
        if ((i2 & 2) != 0) {
            serverError2 = failure.serverError;
        }
        if ((i2 & 4) != 0) {
            th = failure.throwable;
        }
        if ((i2 & 8) != 0) {
            str = failure.errorItems;
        }
        return failure.copy(i, serverError2, th, str);
    }

    @DexIgnore
    public final int component1() {
        return this.code;
    }

    @DexIgnore
    public final ServerError component2() {
        return this.serverError;
    }

    @DexIgnore
    public final Throwable component3() {
        return this.throwable;
    }

    @DexIgnore
    public final String component4() {
        return this.errorItems;
    }

    @DexIgnore
    public final Failure<T> copy(int i, ServerError serverError2, Throwable th, String str) {
        return new Failure<>(i, serverError2, th, str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Failure) {
                Failure failure = (Failure) obj;
                if (this.code != failure.code || !pq7.a(this.serverError, failure.serverError) || !pq7.a(this.throwable, failure.throwable) || !pq7.a(this.errorItems, failure.errorItems)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getCode() {
        return this.code;
    }

    @DexIgnore
    public final String getErrorItems() {
        return this.errorItems;
    }

    @DexIgnore
    public final ServerError getServerError() {
        return this.serverError;
    }

    @DexIgnore
    public final Throwable getThrowable() {
        return this.throwable;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int i2 = this.code;
        ServerError serverError2 = this.serverError;
        int hashCode = serverError2 != null ? serverError2.hashCode() : 0;
        Throwable th = this.throwable;
        int hashCode2 = th != null ? th.hashCode() : 0;
        String str = this.errorItems;
        if (str != null) {
            i = str.hashCode();
        }
        return ((((hashCode + (i2 * 31)) * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Failure(code=" + this.code + ", serverError=" + this.serverError + ", throwable=" + this.throwable + ", errorItems=" + this.errorItems + ")";
    }
}
