package com.misfit.frameworks.buttonservice.log;

import com.fossil.qn7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface IRemoteLogWriter {
    @DexIgnore
    Object sendLog(List<LogEvent> list, qn7<? super List<LogEvent>> qn7);
}
