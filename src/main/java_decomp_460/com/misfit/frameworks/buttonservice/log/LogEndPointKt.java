package com.misfit.frameworks.buttonservice.log;

import com.fossil.go7;
import com.fossil.qn7;
import com.fossil.vn7;
import com.fossil.xn7;
import com.fossil.yn7;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogEndPointKt {
    @DexIgnore
    public static final <T> Object await(Call<T> call, qn7<? super RepoResponse<T>> qn7) {
        vn7 vn7 = new vn7(xn7.c(qn7));
        call.D(new LogEndPointKt$await$Anon2$Anon1_Level2(vn7));
        Object a2 = vn7.a();
        if (a2 == yn7.d()) {
            go7.c(qn7);
        }
        return a2;
    }
}
