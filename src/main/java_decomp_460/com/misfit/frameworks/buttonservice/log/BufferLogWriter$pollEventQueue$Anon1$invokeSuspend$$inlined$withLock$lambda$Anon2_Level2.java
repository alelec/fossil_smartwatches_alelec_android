package com.misfit.frameworks.buttonservice.log;

import com.fossil.el7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BufferLogWriter$pollEventQueue$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2(qn7 qn7, BufferLogWriter$pollEventQueue$Anon1 bufferLogWriter$pollEventQueue$Anon1) {
        super(2, qn7);
        this.this$0 = bufferLogWriter$pollEventQueue$Anon1;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2 bufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2 = new BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2(qn7, this.this$0);
        bufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2.p$ = (iv7) obj;
        return bufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        IFinishCallback callback;
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            BufferDebugOption bufferDebugOption = this.this$0.this$0.debugOption;
            if (bufferDebugOption == null || (callback = bufferDebugOption.getCallback()) == null) {
                return null;
            }
            callback.onFinish();
            return tl7.f3441a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
