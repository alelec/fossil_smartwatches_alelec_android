package com.misfit.frameworks.buttonservice.log;

import com.fossil.bw7;
import com.fossil.gu7;
import com.fossil.hr7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.u08;
import com.fossil.ux7;
import com.fossil.w08;
import com.fossil.xw7;
import com.misfit.frameworks.buttonservice.extensions.SynchronizeQueue;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileLogWriter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ int FILE_LOG_SIZE_THRESHOLD; // = 5242880;
    @DexIgnore
    public static /* final */ String FILE_NAME_PATTERN; // = "app_log_%s.txt";
    @DexIgnore
    public static /* final */ String LOG_FOLDER; // = "logs";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public FileDebugOption debugOption;
    @DexIgnore
    public String directoryPath; // = "";
    @DexIgnore
    public /* final */ SynchronizeQueue<String> logEventQueue; // = new SynchronizeQueue<>();
    @DexIgnore
    public int mCount;
    @DexIgnore
    public /* final */ u08 mFileLogWriterMutex; // = w08.b(false, 1, null);
    @DexIgnore
    public /* final */ iv7 mFileLogWriterScope; // = jv7.a(bw7.b().plus(ux7.b(null, 1, null)));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    /*
    static {
        String name = FileLogWriter.class.getName();
        pq7.b(name, "FileLogWriter::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    private final String getFilePath(String str) {
        return this.directoryPath + File.separatorChar + LOG_FOLDER + File.separatorChar + str;
    }

    @DexIgnore
    private final xw7 pollLogEvent() {
        return gu7.d(this.mFileLogWriterScope, null, null, new FileLogWriter$pollLogEvent$Anon1(this, null), 3, null);
    }

    @DexIgnore
    private final void rotateFiles() throws Exception {
        synchronized (this) {
            for (int i = 2; i >= 0; i--) {
                hr7 hr7 = hr7.f1520a;
                String format = String.format(FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                File file = new File(getFilePath(format));
                if (file.exists()) {
                    FileChannel channel = new RandomAccessFile(file, "rw").getChannel();
                    FileLock lock = channel.lock();
                    if (i >= 2) {
                        file.delete();
                    } else {
                        hr7 hr72 = hr7.f1520a;
                        String format2 = String.format(FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{Integer.valueOf(i + 1)}, 1));
                        pq7.b(format2, "java.lang.String.format(format, *args)");
                        file.renameTo(new File(getFilePath(format2)));
                    }
                    lock.release();
                    channel.close();
                }
            }
            hr7 hr73 = hr7.f1520a;
            String format3 = String.format(FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{0}, 1));
            pq7.b(format3, "java.lang.String.format(format, *args)");
            new File(getFilePath(format3)).createNewFile();
        }
    }

    @DexIgnore
    public final List<File> exportLogs() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i <= 2; i++) {
            hr7 hr7 = hr7.f1520a;
            String format = String.format(FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            File file = new File(getFilePath(format));
            if (file.exists()) {
                arrayList.add(file);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final void startWriter(String str) {
        pq7.c(str, "directoryPath");
        this.directoryPath = str;
        pollLogEvent();
    }

    @DexIgnore
    public final void startWriter(String str, FileDebugOption fileDebugOption) {
        pq7.c(str, "directoryPath");
        this.debugOption = fileDebugOption;
        this.mCount = 0;
        startWriter(str);
    }

    @DexIgnore
    public final void writeLog(String str) {
        pq7.c(str, "logMessage");
        this.logEventQueue.add(str);
        pollLogEvent();
    }
}
