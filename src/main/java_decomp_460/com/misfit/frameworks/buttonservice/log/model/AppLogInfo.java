package com.misfit.frameworks.buttonservice.log.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppLogInfo implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ String appVersion;
    @DexIgnore
    public /* final */ String phoneID;
    @DexIgnore
    public /* final */ String phoneModel;
    @DexIgnore
    public /* final */ String platform;
    @DexIgnore
    public /* final */ String platformVersion;
    @DexIgnore
    public /* final */ String sdkVersion;
    @DexIgnore
    public /* final */ String userId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<AppLogInfo> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public AppLogInfo createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new AppLogInfo(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public AppLogInfo[] newArray(int i) {
            return new AppLogInfo[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppLogInfo(android.os.Parcel r9) {
        /*
            r8 = this;
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r9, r0)
            java.lang.String r1 = r9.readString()
            if (r1 == 0) goto L_0x0034
        L_0x000b:
            java.lang.String r2 = r9.readString()
            if (r2 == 0) goto L_0x0037
        L_0x0011:
            java.lang.String r3 = r9.readString()
            if (r3 == 0) goto L_0x003a
        L_0x0017:
            java.lang.String r4 = r9.readString()
            if (r4 == 0) goto L_0x003d
        L_0x001d:
            java.lang.String r5 = r9.readString()
            if (r5 == 0) goto L_0x0040
        L_0x0023:
            java.lang.String r6 = r9.readString()
            if (r6 == 0) goto L_0x0043
        L_0x0029:
            java.lang.String r7 = r9.readString()
            if (r7 == 0) goto L_0x0046
        L_0x002f:
            r0 = r8
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            return
        L_0x0034:
            java.lang.String r1 = ""
            goto L_0x000b
        L_0x0037:
            java.lang.String r2 = ""
            goto L_0x0011
        L_0x003a:
            java.lang.String r3 = ""
            goto L_0x0017
        L_0x003d:
            java.lang.String r4 = ""
            goto L_0x001d
        L_0x0040:
            java.lang.String r5 = ""
            goto L_0x0023
        L_0x0043:
            java.lang.String r6 = ""
            goto L_0x0029
        L_0x0046:
            java.lang.String r7 = ""
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.model.AppLogInfo.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public AppLogInfo(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        pq7.c(str, ButtonService.USER_ID);
        pq7.c(str2, "appVersion");
        pq7.c(str3, "platform");
        pq7.c(str4, "platformVersion");
        pq7.c(str5, "phoneID");
        pq7.c(str6, "sdkVersion");
        pq7.c(str7, "phoneModel");
        this.userId = str;
        this.appVersion = str2;
        this.platform = str3;
        this.platformVersion = str4;
        this.phoneID = str5;
        this.sdkVersion = str6;
        this.phoneModel = str7;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getAppVersion() {
        return this.appVersion;
    }

    @DexIgnore
    public final String getPhoneID() {
        return this.phoneID;
    }

    @DexIgnore
    public final String getPhoneModel() {
        return this.phoneModel;
    }

    @DexIgnore
    public final String getPlatform() {
        return this.platform;
    }

    @DexIgnore
    public final String getPlatformVersion() {
        return this.platformVersion;
    }

    @DexIgnore
    public final String getSdkVersion() {
        return this.sdkVersion;
    }

    @DexIgnore
    public final String getUserId() {
        return this.userId;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.userId);
        parcel.writeString(this.appVersion);
        parcel.writeString(this.platform);
        parcel.writeString(this.platformVersion);
        parcel.writeString(this.phoneID);
        parcel.writeString(this.sdkVersion);
        parcel.writeString(this.phoneModel);
    }
}
