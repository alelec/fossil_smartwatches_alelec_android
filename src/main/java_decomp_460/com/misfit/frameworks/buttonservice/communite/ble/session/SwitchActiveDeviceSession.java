package com.misfit.frameworks.buttonservice.communite.ble.session;

import android.os.Bundle;
import com.fossil.cl1;
import com.fossil.il7;
import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.tl7;
import com.fossil.us1;
import com.fossil.yx1;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.ISwitchDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BaseSwitchActiveDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseExchangeSecretKeySubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeConfig;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SwitchActiveDeviceSession extends BaseSwitchActiveDeviceSession implements IExchangeKeySession, ISwitchDeviceSession {
    @DexIgnore
    public BackgroundConfig backgroundConfig;
    @DexIgnore
    public ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public LocalizationData localizationData;
    @DexIgnore
    public List<? extends MicroAppMapping> microAppMappings;
    @DexIgnore
    public List<AlarmSetting> multiAlarmSettings;
    @DexIgnore
    public AppNotificationFilterSettings notificationFilterSettings;
    @DexIgnore
    public int secondTimezoneOffset;
    @DexIgnore
    public ThemeConfig themeConfig;
    @DexIgnore
    public WatchAppMappingSettings watchAppMappingSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class CloseConnectionState extends BleStateAbs {
        @DexIgnore
        public int failureCode;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public CloseConnectionState() {
            super(SwitchActiveDeviceSession.this.getTAG());
        }

        @DexIgnore
        public final int getFailureCode() {
            return this.failureCode;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            SwitchActiveDeviceSession.this.getBleAdapter().closeConnection(SwitchActiveDeviceSession.this.getLogSession(), true);
            SwitchActiveDeviceSession.this.stop(this.failureCode);
            return true;
        }

        @DexIgnore
        public final void setFailureCode(int i) {
            this.failureCode = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DianaExchangeSecretKeySubFlow extends BaseExchangeSecretKeySubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DianaExchangeSecretKeySubFlow() {
            super(CommunicateMode.LINK, SwitchActiveDeviceSession.this.getTAG(), SwitchActiveDeviceSession.this, SwitchActiveDeviceSession.this.getMfLog(), SwitchActiveDeviceSession.this.getLogSession(), SwitchActiveDeviceSession.this.getSerial(), SwitchActiveDeviceSession.this.getBleAdapter(), SwitchActiveDeviceSession.this.getBleSessionCallback());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            if (i == cl1.REQUEST_UNSUPPORTED.getCode() || i == cl1.UNSUPPORTED_FORMAT.getCode() || i == 0) {
                SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
                switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState((SwitchActiveDeviceSession) BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE));
                return;
            }
            SwitchActiveDeviceSession.this.stop(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class LinkServerState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public LinkServerState() {
            super(SwitchActiveDeviceSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            BleSession.BleSessionCallback bleSessionCallback = SwitchActiveDeviceSession.this.getBleSessionCallback();
            if (bleSessionCallback == null) {
                return true;
            }
            CommunicateMode communicateMode = CommunicateMode.SWITCH_DEVICE;
            Bundle bundle = new Bundle();
            bundle.putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(SwitchActiveDeviceSession.this.getBleAdapter()));
            bleSessionCallback.onAskForLinkServer(communicateMode, bundle);
            return true;
        }

        @DexIgnore
        public final void onLinkServerCompleted(boolean z, int i) {
            if (z) {
                SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
                switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState((SwitchActiveDeviceSession) BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
                return;
            }
            BleState createConcreteState = SwitchActiveDeviceSession.this.createConcreteState((SwitchActiveDeviceSession) BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE);
            if (createConcreteState instanceof CloseConnectionState) {
                ((CloseConnectionState) createConcreteState).setFailureCode(FailureCode.FAILED_TO_CLEAR_LINK_MAPPING);
            }
            SwitchActiveDeviceSession.this.enterStateAsync(createConcreteState);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class StopCurrentWorkoutState extends BleStateAbs {
        @DexIgnore
        public qy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public StopCurrentWorkoutState() {
            super(SwitchActiveDeviceSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            qy1<tl7> stopCurrentWorkoutSession = SwitchActiveDeviceSession.this.getBleAdapter().stopCurrentWorkoutSession(SwitchActiveDeviceSession.this.getLogSession(), this);
            this.task = stopCurrentWorkoutSession;
            if (stopCurrentWorkoutSession == null) {
                SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
                switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState((SwitchActiveDeviceSession) BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW));
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onStopCurrentWorkoutSessionFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            if (!(yx1 instanceof cl1) || !(yx1 == cl1.REQUEST_UNSUPPORTED || yx1 == cl1.UNSUPPORTED_FORMAT)) {
                SwitchActiveDeviceSession.this.stop(FailureCode.FAILED_TO_STOP_CURRENT_WORKOUT_SESSION);
                return;
            }
            SwitchActiveDeviceSession.this.log("Stop current workout request is unsupported. Skip it.");
            SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
            switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState((SwitchActiveDeviceSession) BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onStopCurrentWorkoutSessionSuccess() {
            stopTimeout();
            SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
            switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState((SwitchActiveDeviceSession) BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            qy1<tl7> qy1 = this.task;
            if (qy1 != null) {
                us1.a(qy1);
            }
            SwitchActiveDeviceSession.this.stop(FailureCode.FAILED_TO_STOP_CURRENT_WORKOUT_SESSION);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferDataSubFlow extends BaseTransferDataSubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public TransferDataSubFlow() {
            super(SwitchActiveDeviceSession.this.getTAG(), SwitchActiveDeviceSession.this, SwitchActiveDeviceSession.this.getMfLog(), SwitchActiveDeviceSession.this.getLogSession(), SwitchActiveDeviceSession.this.getSerial(), SwitchActiveDeviceSession.this.getBleAdapter(), SwitchActiveDeviceSession.this.getUserProfile(), SwitchActiveDeviceSession.this.getBleSessionCallback(), false);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            if (i != 0) {
                BleState createConcreteState = SwitchActiveDeviceSession.this.createConcreteState((SwitchActiveDeviceSession) BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE);
                if (createConcreteState != null) {
                    CloseConnectionState closeConnectionState = (CloseConnectionState) createConcreteState;
                    closeConnectionState.setFailureCode(FailureCode.FAILED_TO_CLEAR_DATA);
                    SwitchActiveDeviceSession.this.enterStateAsync(closeConnectionState);
                    return;
                }
                throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SwitchActiveDeviceSession.CloseConnectionState");
            }
            SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
            switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState((SwitchActiveDeviceSession) BleSessionAbs.SessionState.LINK_SERVER));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferSettingsSubFlow extends BaseTransferSettingsSubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public TransferSettingsSubFlow() {
            super(SwitchActiveDeviceSession.this.getTAG(), SwitchActiveDeviceSession.this, SwitchActiveDeviceSession.this.getMfLog(), SwitchActiveDeviceSession.this.getLogSession(), true, SwitchActiveDeviceSession.this.getSerial(), SwitchActiveDeviceSession.this.getBleAdapter(), SwitchActiveDeviceSession.this.getUserProfile(), SwitchActiveDeviceSession.this.multiAlarmSettings, SwitchActiveDeviceSession.this.complicationAppMappingSettings, SwitchActiveDeviceSession.this.watchAppMappingSettings, SwitchActiveDeviceSession.this.backgroundConfig, SwitchActiveDeviceSession.this.themeConfig, SwitchActiveDeviceSession.this.notificationFilterSettings, SwitchActiveDeviceSession.this.localizationData, SwitchActiveDeviceSession.this.microAppMappings, SwitchActiveDeviceSession.this.secondTimezoneOffset, SwitchActiveDeviceSession.this.getBleSessionCallback());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            SwitchActiveDeviceSession.this.stop(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class VerifySecretKeySessionState extends BleStateAbs {
        @DexIgnore
        public qy1<Boolean> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public VerifySecretKeySessionState() {
            super(SwitchActiveDeviceSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            byte[] tSecretKey = SwitchActiveDeviceSession.this.getBleAdapter().getTSecretKey();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "enter VerifySecretKeySessionState currentSecretKey " + tSecretKey);
            if (tSecretKey == null) {
                SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
                switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState((SwitchActiveDeviceSession) BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW));
                return true;
            }
            qy1<Boolean> verifySecretKey = SwitchActiveDeviceSession.this.getBleAdapter().verifySecretKey(SwitchActiveDeviceSession.this.getLogSession(), tSecretKey, this);
            this.task = verifySecretKey;
            if (verifySecretKey == null) {
                SwitchActiveDeviceSession switchActiveDeviceSession2 = SwitchActiveDeviceSession.this;
                switchActiveDeviceSession2.enterStateAsync(switchActiveDeviceSession2.createConcreteState((SwitchActiveDeviceSession) BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW));
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            qy1<Boolean> qy1 = this.task;
            if (qy1 != null) {
                us1.a(qy1);
            }
            SwitchActiveDeviceSession.this.stop(FailureCode.FAILED_TO_CONNECT_TIMEOUT);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onVerifySecretKeyFail(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "Error code " + yx1.getErrorCode().getCode());
            int code = yx1.getErrorCode().getCode();
            if (code == cl1.REQUEST_UNSUPPORTED.getCode()) {
                SwitchActiveDeviceSession.this.stop(cl1.REQUEST_UNSUPPORTED.getCode());
            } else if (code == cl1.UNSUPPORTED_FORMAT.getCode()) {
                SwitchActiveDeviceSession.this.stop(cl1.UNSUPPORTED_FORMAT.getCode());
            } else {
                SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
                switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState((SwitchActiveDeviceSession) BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW));
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onVerifySecretKeySuccess(boolean z) {
            stopTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "onVerifySecretKeySuccess isValid " + z);
            if (z) {
                SwitchActiveDeviceSession switchActiveDeviceSession = SwitchActiveDeviceSession.this;
                switchActiveDeviceSession.enterStateAsync(switchActiveDeviceSession.createConcreteState((SwitchActiveDeviceSession) BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE));
                return;
            }
            SwitchActiveDeviceSession switchActiveDeviceSession2 = SwitchActiveDeviceSession.this;
            switchActiveDeviceSession2.enterStateAsync(switchActiveDeviceSession2.createConcreteState((SwitchActiveDeviceSession) BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceSession(UserProfile userProfile, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        super(userProfile, bleAdapterImpl, bleSessionCallback, communicationResultCallback);
        pq7.c(userProfile, "userProfile");
        pq7.c(bleAdapterImpl, "bleAdapter");
        setLogSession(FLogger.Session.SWITCH_DEVICE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SwitchActiveDeviceSession switchActiveDeviceSession = new SwitchActiveDeviceSession(getUserProfile(), getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback());
        switchActiveDeviceSession.setDevice(getDevice());
        return switchActiveDeviceSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void doNextState() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "doNextState, serial=" + getSerial());
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SwitchActiveDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        super.initSettings();
        this.multiAlarmSettings = DevicePreferenceUtils.getAutoListAlarm(getContext());
        this.complicationAppMappingSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(getContext(), getSerial());
        this.watchAppMappingSettings = DevicePreferenceUtils.getAutoWatchAppSettings(getContext(), getSerial());
        this.backgroundConfig = DevicePreferenceUtils.getAutoBackgroundImageConfig(getContext(), getSerial());
        this.notificationFilterSettings = DevicePreferenceUtils.getAutoNotificationFiltersConfig(getContext(), getSerial());
        this.localizationData = DevicePreferenceUtils.getAutoLocalizationDataSettings(getContext(), getSerial());
        this.microAppMappings = MicroAppMapping.convertToMicroAppMapping(DevicePreferenceUtils.getAutoMapping(getContext(), getSerial()));
        this.secondTimezoneOffset = DevicePreferenceUtils.getAutoSecondTimezone(getContext());
        this.themeConfig = DevicePreferenceUtils.getThemeConfig(getContext(), getSerial());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BaseSwitchActiveDeviceSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.VERIFY_SECRET_KEY;
        String name = VerifySecretKeySessionState.class.getName();
        pq7.b(name, "VerifySecretKeySessionState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW;
        String name2 = DianaExchangeSecretKeySubFlow.class.getName();
        pq7.b(name2, "DianaExchangeSecretKeySubFlow::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap3 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState3 = BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE;
        String name3 = StopCurrentWorkoutState.class.getName();
        pq7.b(name3, "StopCurrentWorkoutState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap4 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState4 = BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW;
        String name4 = TransferDataSubFlow.class.getName();
        pq7.b(name4, "TransferDataSubFlow::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap5 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState5 = BleSessionAbs.SessionState.LINK_SERVER;
        String name5 = LinkServerState.class.getName();
        pq7.b(name5, "LinkServerState::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap6 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState6 = BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW;
        String name6 = TransferSettingsSubFlow.class.getName();
        pq7.b(name6, "TransferSettingsSubFlow::class.java.name");
        sessionStateMap6.put(sessionState6, name6);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap7 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState7 = BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE;
        String name7 = CloseConnectionState.class.getName();
        pq7.b(name7, "CloseConnectionState::class.java.name");
        sessionStateMap7.put(sessionState7, name7);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onGetWatchParamFailed() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed");
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).onGetWatchParamFailed();
                return;
            }
            throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SwitchActiveDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISwitchDeviceSession
    public void onLinkServerSuccess(boolean z, int i) {
        BleState currentState = getCurrentState();
        if (currentState instanceof LinkServerState) {
            ((LinkServerState) currentState).onLinkServerCompleted(z, i);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onReceiveRandomKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveRandomKey randomKey " + bArr + " state " + currentState);
        if (currentState instanceof DianaExchangeSecretKeySubFlow) {
            ((DianaExchangeSecretKeySubFlow) currentState).onReceiveRandomKey(bArr, i);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onReceiveServerSecretKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerSecretKey secretKey " + bArr + " state " + currentState);
        if (currentState instanceof DianaExchangeSecretKeySubFlow) {
            ((DianaExchangeSecretKeySubFlow) currentState).onReceiveServerSecretKey(bArr, i);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchAppFileSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onWatchAppFilesReady(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onWatchAppFilesReady, isSuccess = " + z);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setWatchAppFiles(z);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SwitchActiveDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setWatchAppFiles FAILED. It is not in SetWatchAppFilesSession");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        pq7.c(str, "serial");
        pq7.c(watchParamsFileMapping, "watchParamsData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setLatestWatchParam, serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String tag2 = getTAG();
        remote.d(component, session, str, tag2, "setLatestWatchParam(), serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setLatestWatchParam(str, watchParamsFileMapping);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SwitchActiveDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, can't set WatchParams because currentState is not an instance of TransferSettingSubFlow");
    }
}
