package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.dr7;
import com.fossil.fv1;
import com.fossil.ks1;
import com.fossil.pq7;
import com.fossil.qq7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.yx1;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon2 extends qq7 implements rp7<yx1, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ks1 $replyMessageFeature$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ReplyMessageMappingGroup $replyMessageGroup$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ dr7 $replyMessageIcon$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ fv1[] $replyMessageMapping$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon2(fv1[] fv1Arr, BleAdapterImpl bleAdapterImpl, dr7 dr7, ReplyMessageMappingGroup replyMessageMappingGroup, ks1 ks1, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.$replyMessageMapping$inlined = fv1Arr;
        this.this$0 = bleAdapterImpl;
        this.$replyMessageIcon$inlined = dr7;
        this.$replyMessageGroup$inlined = replyMessageMappingGroup;
        this.$replyMessageFeature$inlined = ks1;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(yx1 yx1) {
        invoke(yx1);
        return tl7.f3441a;
    }

    @DexIgnore
    public final void invoke(yx1 yx1) {
        pq7.c(yx1, "error");
        this.this$0.logSdkError(this.$logSession$inlined, "setReplyMessageMapping", ErrorCodeBuilder.Step.SET_REPLY_MESSAGE_MAPPING, yx1);
        this.$callback$inlined.onSetReplyMessageMappingError(yx1);
    }
}
