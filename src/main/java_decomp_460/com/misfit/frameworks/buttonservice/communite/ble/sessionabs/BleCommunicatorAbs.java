package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import com.fossil.bw7;
import com.fossil.gu7;
import com.fossil.jv7;
import com.fossil.kq7;
import com.fossil.mp1;
import com.fossil.pq7;
import com.fossil.xw7;
import com.fossil.yk1;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.GetBatteryLevelSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.GetVibrationStrengthSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.PlayAnimationSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ReadRealTimeStepsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ReadRssiSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetActivityGoalSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetVibrationStrengthSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetWorkoutDetectionConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.UnlinkSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.UpdateCurrentTimeSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.QuickCommandQueue;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BleCommunicatorAbs extends BleCommunicator {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ HandlerThread handlerThread;
    @DexIgnore
    public BleSession currentSession;
    @DexIgnore
    public /* final */ BleAdapterImpl mBleAdapter;
    @DexIgnore
    public /* final */ Handler mHandler; // = new Handler(getHandlerThread().getLooper());
    @DexIgnore
    public QuickCommandQueue mQuickCommandQueue; // = new QuickCommandQueue();
    @DexIgnore
    public /* final */ yk1.b mStateCallback;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final HandlerThread getHandlerThread() {
            return BleCommunicatorAbs.handlerThread;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[yk1.c.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[yk1.c.CONNECTED.ordinal()] = 1;
        }
        */
    }

    /*
    static {
        String simpleName = BleCommunicatorAbs.class.getSimpleName();
        pq7.b(simpleName, "BleCommunicatorAbs::class.java.simpleName");
        TAG = simpleName;
        HandlerThread handlerThread2 = new HandlerThread(TAG);
        handlerThread = handlerThread2;
        handlerThread2.start();
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleCommunicatorAbs(BleAdapterImpl bleAdapterImpl, Context context, String str, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        super(str, communicationResultCallback);
        pq7.c(bleAdapterImpl, "mBleAdapter");
        pq7.c(context, "context");
        pq7.c(str, "serial");
        pq7.c(communicationResultCallback, "communicationResultCallback");
        this.mBleAdapter = bleAdapterImpl;
        this.currentSession = BleSessionAbs.Companion.createNullSession(context);
        BleCommunicatorAbs$mStateCallback$Anon1 bleCommunicatorAbs$mStateCallback$Anon1 = new BleCommunicatorAbs$mStateCallback$Anon1(this);
        this.mStateCallback = bleCommunicatorAbs$mStateCallback$Anon1;
        this.mBleAdapter.registerBluetoothStateCallback(bleCommunicatorAbs$mStateCallback$Anon1);
    }

    @DexIgnore
    private final void processQuickCommandQueue() {
        synchronized (this) {
            Object poll = this.mQuickCommandQueue.poll();
            if (poll != null) {
                if (getBleAdapter().isDeviceReady()) {
                    onQuickCommandAction(poll);
                }
                xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new BleCommunicatorAbs$processQuickCommandQueue$Anon1(this, null), 3, null);
            }
        }
    }

    @DexIgnore
    public final void addToQuickCommandQueue(Object obj) {
        synchronized (this) {
            pq7.c(obj, "obj");
            this.mQuickCommandQueue.add(obj);
            processQuickCommandQueue();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void cleanUp() {
        super.cleanUp();
        this.mQuickCommandQueue.clear();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void clearQuickCommandQueue() {
        this.mQuickCommandQueue.clear();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public BleAdapterImpl getBleAdapter() {
        return this.mBleAdapter;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public BleSession getCurrentSession() {
        return this.currentSession;
    }

    @DexIgnore
    public final BleAdapterImpl getMBleAdapter() {
        return this.mBleAdapter;
    }

    @DexIgnore
    public final Handler getMHandler() {
        return this.mHandler;
    }

    @DexIgnore
    public void handleDeviceStateChanged(yk1 yk1, yk1.c cVar, yk1.c cVar2) {
        pq7.c(yk1, "device");
        pq7.c(cVar, "previousState");
        pq7.c(cVar2, "newState");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onDeviceStateChanged, device=" + yk1.M().getSerialNumber() + ", previousState=" + cVar + ", newState=" + cVar2);
        if (WhenMappings.$EnumSwitchMapping$0[cVar2.ordinal()] != 1) {
            getCommunicationResultCallback().onGattConnectionStateChanged(getBleAdapter().getSerial(), 0);
            return;
        }
        getCommunicationResultCallback().onGattConnectionStateChanged(getBleAdapter().getSerial(), 2);
        processQuickCommandQueue();
    }

    @DexIgnore
    public void handleEventReceived(yk1 yk1, mp1 mp1) {
        pq7.c(yk1, "device");
        pq7.c(mp1, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onEventReceived(), device=" + yk1.M().getSerialNumber() + ", event=" + mp1);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean isDeviceReady() {
        return this.mBleAdapter.isDeviceReady();
    }

    @DexIgnore
    public abstract void onQuickCommandAction(Object obj);

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void sendCustomCommand(CustomRequest customRequest) {
        pq7.c(customRequest, Constants.COMMAND);
        this.mBleAdapter.sendCustomCommand(customRequest);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void setCurrentSession(BleSession bleSession) {
        pq7.c(bleSession, "<set-?>");
        this.currentSession = bleSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void setNullCurrentSession() {
        setCurrentSession(BleSessionAbs.Companion.createNullSession(getBleAdapter().getContext()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startCalibrationSession() {
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startGetBatteryLevelSession() {
        queueSessionAndStart(new GetBatteryLevelSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startGetRssiSession() {
        queueSessionAndStart(new ReadRssiSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startGetVibrationStrengthSession() {
        queueSessionAndStart(new GetVibrationStrengthSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startPlayAnimationSession() {
        queueSessionAndStart(new PlayAnimationSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startReadRealTimeStepSession() {
        queueSessionAndStart(new ReadRealTimeStepsSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSendNotification(NotificationBaseObj notificationBaseObj) {
        pq7.c(notificationBaseObj, "newNotification");
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void startSessionInQueueProcess() {
        if (BleSession.Companion.isNull(getCurrentSession())) {
            BleSession poll = getHighSessionQueue().poll();
            if (poll == null) {
                poll = getLowSessionQueue().poll();
            }
            if (poll == null || !(poll instanceof BleSessionAbs)) {
                FLogger.INSTANCE.getLocal().d(getTAG(), ".startSessionInQueueProcess() - queue is empty. Be idle now.");
                return;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, ".startSessionInQueueProcess() - next session is " + poll);
            setCurrentSession(poll);
            getCurrentSession().start(new Object[0]);
            return;
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), ".startSessionInQueueProcess() - a session is exist, session will be start later.");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetActivityGoals(int i, int i2, int i3) {
        queueSessionAndStart(new SetActivityGoalSession(i, i2, i3, this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoMultiAlarms(List<AlarmSetting> list) {
        pq7.c(list, "multipleAlarmList");
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetLocalizationData(LocalizationData localizationData) {
        pq7.c(localizationData, "localizationData");
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetMultipleAlarmsSession(List<AlarmSetting> list) {
        pq7.c(list, "multipleAlarmList");
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetVibrationStrengthSession(VibrationStrengthObj vibrationStrengthObj) {
        pq7.c(vibrationStrengthObj, "vibrationStrengthLevelObj");
        queueSessionAndStart(new SetVibrationStrengthSession(vibrationStrengthObj, this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetWorkoutDetectionSetting(WorkoutDetectionSetting workoutDetectionSetting) {
        pq7.c(workoutDetectionSetting, "workoutDetectionSetting");
        queueSessionAndStart(new SetWorkoutDetectionConfigSession(workoutDetectionSetting, this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startUnlinkSession() {
        queueSessionAndStart(new UnlinkSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startUpdateCurrentTime() {
        queueSessionAndStart(new UpdateCurrentTimeSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }
}
