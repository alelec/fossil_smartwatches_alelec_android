package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.qq7;
import com.fossil.rp7;
import com.fossil.ru1;
import com.fossil.tl7;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$setWatchAppFiles$$inlined$let$lambda$Anon3 extends qq7 implements rp7<Float, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ru1[] $watchAppFiles$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setWatchAppFiles$$inlined$let$lambda$Anon3(BleAdapterImpl bleAdapterImpl, ru1[] ru1Arr, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$watchAppFiles$inlined = ru1Arr;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(Float f) {
        invoke(f.floatValue());
        return tl7.f3441a;
    }

    @DexIgnore
    public final void invoke(float f) {
        BleAdapterImpl bleAdapterImpl = this.this$0;
        FLogger.Session session = this.$logSession$inlined;
        bleAdapterImpl.log(session, "Set Watch App Files: progress->" + f);
        this.$callback$inlined.onSetWatchAppFileProgressChanged(f);
    }
}
