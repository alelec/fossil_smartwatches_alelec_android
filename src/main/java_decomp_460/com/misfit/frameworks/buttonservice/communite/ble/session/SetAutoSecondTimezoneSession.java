package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.ln1;
import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.us1;
import com.fossil.ym1;
import com.fossil.yx1;
import com.fossil.zm1;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.SettingsUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoSecondTimezoneSession extends SetAutoSettingsSession {
    @DexIgnore
    public String mOldSecondTimezoneId;
    @DexIgnore
    public /* final */ String mSecondTimezoneId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneSetAutoSecondTimezoneState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneSetAutoSecondTimezoneState() {
            super(SetAutoSecondTimezoneSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoSecondTimezoneSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetSecondTimezoneState extends BleStateAbs {
        @DexIgnore
        public qy1<zm1[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetSecondTimezoneState() {
            super(SetAutoSecondTimezoneSession.this.getTAG());
        }

        @DexIgnore
        private final ym1[] prepareConfigData() {
            int timezoneRawOffsetById = ConversionUtils.INSTANCE.getTimezoneRawOffsetById(SetAutoSecondTimezoneSession.this.mSecondTimezoneId);
            ln1 ln1 = new ln1();
            short s = (short) timezoneRawOffsetById;
            if (SettingsUtils.INSTANCE.isSecondTimezoneInRange(s)) {
                ln1.q(s);
            } else {
                SetAutoSecondTimezoneSession setAutoSecondTimezoneSession = SetAutoSecondTimezoneSession.this;
                setAutoSecondTimezoneSession.log("Set Device Config: Timezone is out of range: " + timezoneRawOffsetById);
            }
            return ln1.b();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            qy1<zm1[]> deviceConfig = SetAutoSecondTimezoneSession.this.getBleAdapter().setDeviceConfig(SetAutoSecondTimezoneSession.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetAutoSecondTimezoneSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            SetAutoSecondTimezoneSession.this.stop(FailureCode.FAILED_TO_SET_SECOND_TIMEZONE);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoSecondTimezone(SetAutoSecondTimezoneSession.this.getBleAdapter().getContext(), SetAutoSecondTimezoneSession.this.mSecondTimezoneId);
            DevicePreferenceUtils.removeSettingFlag(SetAutoSecondTimezoneSession.this.getBleAdapter().getContext(), DeviceSettings.SECOND_TIMEZONE);
            SetAutoSecondTimezoneSession setAutoSecondTimezoneSession = SetAutoSecondTimezoneSession.this;
            setAutoSecondTimezoneSession.enterStateAsync(setAutoSecondTimezoneSession.createConcreteState((SetAutoSecondTimezoneSession) BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            qy1<zm1[]> qy1 = this.task;
            if (qy1 != null) {
                us1.a(qy1);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoSecondTimezoneSession(String str, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_SECOND_TIMEZONE, bleAdapterImpl, bleSessionCallback);
        pq7.c(str, "mSecondTimezoneId");
        pq7.c(bleAdapterImpl, "bleAdapter");
        this.mSecondTimezoneId = str;
    }

    @DexIgnore
    private final void storeSettings(String str, boolean z) {
        DevicePreferenceUtils.setAutoSecondTimezone(getBleAdapter().getContext(), str);
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.SECOND_TIMEZONE);
        } else {
            DevicePreferenceUtils.removeSettingFlag(getBleAdapter().getContext(), DeviceSettings.SECOND_TIMEZONE);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetAutoSecondTimezoneSession setAutoSecondTimezoneSession = new SetAutoSecondTimezoneSession(this.mSecondTimezoneId, getBleAdapter(), getBleSessionCallback());
        setAutoSecondTimezoneSession.setDevice(getDevice());
        return setAutoSecondTimezoneSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public BleState getStartState() {
        if (getBleAdapter().isDeviceReady()) {
            String str = this.mSecondTimezoneId;
            String str2 = this.mOldSecondTimezoneId;
            if (str2 == null) {
                pq7.n("mOldSecondTimezoneId");
                throw null;
            } else if (pq7.a(str, str2)) {
                log("The second timezones are the same, no need to store again.");
                return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
            } else {
                storeSettings(this.mSecondTimezoneId, true);
                return createConcreteState(BleSessionAbs.SessionState.SET_SECOND_TIMEZONE_STATE);
            }
        } else {
            storeSettings(this.mSecondTimezoneId, true);
            log("Device is not ready. Cannot set second timezone.");
            return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        String autoSecondTimezoneId = DevicePreferenceUtils.getAutoSecondTimezoneId(getContext());
        pq7.b(autoSecondTimezoneId, "DevicePreferenceUtils.ge\u2026SecondTimezoneId(context)");
        this.mOldSecondTimezoneId = autoSecondTimezoneId;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_SECOND_TIMEZONE_STATE;
        String name = SetSecondTimezoneState.class.getName();
        pq7.b(name, "SetSecondTimezoneState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneSetAutoSecondTimezoneState.class.getName();
        pq7.b(name2, "DoneSetAutoSecondTimezoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }
}
