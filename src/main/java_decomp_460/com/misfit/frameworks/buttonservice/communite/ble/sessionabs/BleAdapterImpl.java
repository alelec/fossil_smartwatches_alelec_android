package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import com.fossil.as1;
import com.fossil.blesdk.device.feature.CustomCommandFeature;
import com.fossil.br1;
import com.fossil.cn1;
import com.fossil.cr1;
import com.fossil.cs1;
import com.fossil.dl1;
import com.fossil.dr1;
import com.fossil.er1;
import com.fossil.es1;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.GpsDataPoint;
import com.fossil.fitness.GpsDataProvider;
import com.fossil.fs1;
import com.fossil.gs1;
import com.fossil.hn1;
import com.fossil.hr1;
import com.fossil.hs1;
import com.fossil.il7;
import com.fossil.im7;
import com.fossil.is1;
import com.fossil.jo1;
import com.fossil.js1;
import com.fossil.kq7;
import com.fossil.kw1;
import com.fossil.lp1;
import com.fossil.lr1;
import com.fossil.ls1;
import com.fossil.lw1;
import com.fossil.mm1;
import com.fossil.mr1;
import com.fossil.ms1;
import com.fossil.mt1;
import com.fossil.nm1;
import com.fossil.nn1;
import com.fossil.nr1;
import com.fossil.nw1;
import com.fossil.or1;
import com.fossil.os1;
import com.fossil.ou1;
import com.fossil.oy1;
import com.fossil.pq7;
import com.fossil.pr1;
import com.fossil.ps1;
import com.fossil.qr1;
import com.fossil.qs1;
import com.fossil.qy1;
import com.fossil.rr1;
import com.fossil.rs1;
import com.fossil.ru1;
import com.fossil.ry1;
import com.fossil.sr1;
import com.fossil.ss1;
import com.fossil.tl1;
import com.fossil.tl7;
import com.fossil.tr1;
import com.fossil.ts1;
import com.fossil.tu1;
import com.fossil.uk1;
import com.fossil.ul1;
import com.fossil.ur1;
import com.fossil.vl1;
import com.fossil.vq1;
import com.fossil.vr1;
import com.fossil.wl1;
import com.fossil.wq1;
import com.fossil.wr1;
import com.fossil.xl1;
import com.fossil.xn1;
import com.fossil.xq1;
import com.fossil.xr1;
import com.fossil.yk1;
import com.fossil.ym1;
import com.fossil.yn1;
import com.fossil.yq1;
import com.fossil.yr1;
import com.fossil.yx1;
import com.fossil.zk1;
import com.fossil.zm1;
import com.fossil.zn1;
import com.fossil.zq1;
import com.fossil.zr1;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import com.misfit.frameworks.buttonservice.communite.ble.BleAdapter;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.extensions.AlarmExtensionKt;
import com.misfit.frameworks.buttonservice.extensions.LocationExtensionKt;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.calibration.DianaCalibrationObj;
import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.misfit.frameworks.buttonservice.utils.ThemeExtentionKt;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl extends BleAdapter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public yk1 deviceObj;
    @DexIgnore
    public boolean isScanning;
    @DexIgnore
    public HashMap<zm1, ym1> mDeviceConfiguration;
    @DexIgnore
    public yk1.b mDeviceStateListener;
    @DexIgnore
    public ScanService scanService;
    @DexIgnore
    public byte[] tSecretKey;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class BleScanServiceCallback implements ScanService.Callback {
        @DexIgnore
        public /* final */ ISessionSdkCallback callback;

        @DexIgnore
        public BleScanServiceCallback(ISessionSdkCallback iSessionSdkCallback) {
            this.callback = iSessionSdkCallback;
        }

        @DexIgnore
        public final ISessionSdkCallback getCallback() {
            return this.callback;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.ble.ScanService.Callback
        public void onDeviceFound(yk1 yk1, int i) {
            pq7.c(yk1, "device");
            if (yk1.getState() == yk1.c.CONNECTED || yk1.getState() == yk1.c.UPGRADING_FIRMWARE) {
                i = 0;
            } else if (yk1.P() == yk1.a.BONDED) {
                i = ScanService.RETRIEVE_DEVICE_BOND_RSSI_MARK;
            }
            ISessionSdkCallback iSessionSdkCallback = this.callback;
            if (iSessionSdkCallback != null) {
                iSessionSdkCallback.onDeviceFound(yk1, i);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.ble.ScanService.Callback
        public void onScanFail(uk1 uk1) {
            pq7.c(uk1, "scanError");
            ISessionSdkCallback iSessionSdkCallback = this.callback;
            if (iSessionSdkCallback != null) {
                iSessionSdkCallback.onScanFail(uk1);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[yk1.c.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[yk1.c.DISCONNECTED.ordinal()] = 1;
            $EnumSwitchMapping$0[yk1.c.CONNECTED.ordinal()] = 2;
            $EnumSwitchMapping$0[yk1.c.UPGRADING_FIRMWARE.ordinal()] = 3;
            $EnumSwitchMapping$0[yk1.c.CONNECTING.ordinal()] = 4;
            $EnumSwitchMapping$0[yk1.c.DISCONNECTING.ordinal()] = 5;
        }
        */
    }

    /*
    static {
        String simpleName = BleAdapterImpl.class.getSimpleName();
        pq7.b(simpleName, "BleAdapterImpl::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl(Context context, String str, String str2) {
        super(context, str, str2);
        pq7.c(context, "context");
        pq7.c(str, "serial");
        pq7.c(str2, "macAddress");
    }

    @DexIgnore
    private final int getGattState(yk1 yk1) {
        if (yk1 != null) {
            int i = WhenMappings.$EnumSwitchMapping$0[yk1.getState().ordinal()];
            if (i != 1) {
                if (i == 2 || i == 3) {
                    return 2;
                }
                if (i != 4) {
                    return i != 5 ? 0 : 3;
                }
                return 1;
            }
        } else {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, "Inside " + TAG + ".getGattState - serial=" + getSerial() + ", shineDevice=NULL");
        }
    }

    @DexIgnore
    private final void requestPairing(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "requestPairing(), serial=" + getSerial());
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            ur1 ur1 = (ur1) yk1.g0(ur1.class);
            if (ur1 == null || ur1.u().m(new BleAdapterImpl$requestPairing$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).l(new BleAdapterImpl$requestPairing$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback)) == null) {
                log(session, "Device[" + getSerial() + "] does not support RequestPairingFeature");
                iSessionSdkCallback.onAuthorizeDeviceFailed();
                tl7 tl7 = tl7.f3441a;
                return;
            }
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<lw1> applyTheme(FLogger.Session session, lw1 lw1, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(lw1, "themePackage");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            ss1 ss1 = (ss1) yk1.g0(ss1.class);
            if (ss1 != null) {
                oy1<lw1> x = ss1.k(lw1).s(new BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon1(this, lw1, iSessionSdkCallback, session));
                x.w(new BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon2(this, lw1, iSessionSdkCallback, session));
                return x.r(new BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon3(this, lw1, iSessionSdkCallback, session));
            }
            log(session, "Device[" + getSerial() + "] does not support applyTheme");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<lw1> applyTheme(FLogger.Session session, ThemeData themeData, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(themeData, "themeData");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            ss1 ss1 = (ss1) yk1.g0(ss1.class);
            if (ss1 != null) {
                oy1<lw1> x = ss1.r(ThemeExtentionKt.toThemeEditor(themeData)).s(new BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon4(this, themeData, iSessionSdkCallback, session));
                x.w(new BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon5(this, themeData, iSessionSdkCallback, session));
                return x.r(new BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon6(this, themeData, iSessionSdkCallback, session));
            }
            log(session, "Device[" + getSerial() + "] does not support applyTheme");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final yk1 buildDeviceBySerial(String str, String str2, long j) {
        pq7.c(str, "serial");
        pq7.c(str2, "macAddress");
        if (this.scanService == null) {
            this.scanService = new ScanService(getContext(), null, j);
        }
        ScanService scanService2 = this.scanService;
        if (scanService2 != null) {
            return scanService2.buildDeviceBySerial(str, str2);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> calibrationApplyHandPosition(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Apply Hands Positions");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            cs1 cs1 = (cs1) yk1.g0(cs1.class);
            if (cs1 != null) {
                return cs1.W().m(new BleAdapterImpl$calibrationApplyHandPosition$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).l(new BleAdapterImpl$calibrationApplyHandPosition$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingCalibrationPositionFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> calibrationMoveHand(FLogger.Session session, HandCalibrationObj handCalibrationObj, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(handCalibrationObj, "handCalibrationObj");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        DianaCalibrationObj consume = DianaCalibrationObj.Companion.consume(handCalibrationObj);
        ul1[] ul1Arr = {new ul1(consume.getHandId(), consume.getDegree(), consume.getHandMovingDirection(), consume.getHandMovingSpeed())};
        log(session, "Move Hand: handMovingType=" + consume.getHandMovingType() + ", handMovingConfigs=" + ul1Arr + ' ');
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            or1 or1 = (or1) yk1.g0(or1.class);
            if (or1 != null) {
                return or1.p(consume.getHandMovingType(), ul1Arr).m(new BleAdapterImpl$calibrationMoveHand$$inlined$let$lambda$Anon1(this, consume, ul1Arr, session, iSessionSdkCallback)).l(new BleAdapterImpl$calibrationMoveHand$$inlined$let$lambda$Anon2(this, consume, ul1Arr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support MovingHandsFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> calibrationReleaseHandControl(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Release Hand Control");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            tr1 tr1 = (tr1) yk1.g0(tr1.class);
            if (tr1 != null) {
                return tr1.L().m(new BleAdapterImpl$calibrationReleaseHandControl$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).l(new BleAdapterImpl$calibrationReleaseHandControl$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support ReleasingHandsFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> calibrationRequestHandControl(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Request Hand Control");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            vr1 vr1 = (vr1) yk1.g0(vr1.class);
            if (vr1 != null) {
                return vr1.O().m(new BleAdapterImpl$calibrationRequestHandControl$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).l(new BleAdapterImpl$calibrationRequestHandControl$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support RequestingHandsFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> calibrationResetHandToZeroDegree(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Reset Hands to 0 degrees.");
        ul1[] ul1Arr = {new ul1(tl1.HOUR, 0, vl1.SHORTEST_PATH, wl1.FULL), new ul1(tl1.MINUTE, 0, vl1.SHORTEST_PATH, wl1.FULL), new ul1(tl1.SUB_EYE, 0, vl1.SHORTEST_PATH, wl1.FULL)};
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            or1 or1 = (or1) yk1.g0(or1.class);
            if (or1 != null) {
                return or1.p(xl1.POSITION, ul1Arr).m(new BleAdapterImpl$calibrationResetHandToZeroDegree$$inlined$let$lambda$Anon1(this, ul1Arr, session, iSessionSdkCallback)).l(new BleAdapterImpl$calibrationResetHandToZeroDegree$$inlined$let$lambda$Anon2(this, ul1Arr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support MovingHandsFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void clearCache(FLogger.Session session) {
        pq7.c(session, "logSession");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Clear cache, serial=" + getSerial());
        yk1 yk1 = this.deviceObj;
        if (yk1 == null || yk1.l() == null) {
            log(session, "clearCache() failed, deviceObj is null.");
            tl7 tl7 = tl7.f3441a;
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public void closeConnection(FLogger.Session session, boolean z) {
        pq7.c(session, "logSession");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Close Connection, serial=" + getSerial());
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            br1 br1 = (br1) yk1.g0(br1.class);
            if (br1 != null) {
                br1.a();
                log(session, "Close Connection, serial=" + getSerial() + ", OK");
                return;
            }
            log(session, "Device[" + getSerial() + "] does not support DisconnectingFeature");
            return;
        }
        logAppError(session, "closeConnection failed. DeviceObject is null. Connect has not been established.", ErrorCodeBuilder.Step.DISCONNECT, ErrorCodeBuilder.AppError.UNKNOWN);
    }

    @DexIgnore
    public final oy1<tl7> configureMicroApp(FLogger.Session session, List<tu1> list, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(list, "mappings");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Configure MicroApp, mapping=" + list);
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            yq1 yq1 = (yq1) yk1.g0(yq1.class);
            if (yq1 != null) {
                Object[] array = list.toArray(new tu1[0]);
                if (array != null) {
                    return yq1.C((tu1[]) array).s(new BleAdapterImpl$configureMicroApp$$inlined$let$lambda$Anon1(this, list, session, iSessionSdkCallback)).r(new BleAdapterImpl$configureMicroApp$$inlined$let$lambda$Anon2(this, list, session, iSessionSdkCallback));
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
            log(session, "Device[" + getSerial() + "] does not support ConfiguringMicroAppFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> confirmAuthorization(FLogger.Session session, long j, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "confirmAuthorization(), timeout=" + j);
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            vq1 vq1 = (vq1) yk1.g0(vq1.class);
            if (vq1 != null) {
                return vq1.d(j).m(new BleAdapterImpl$confirmAuthorization$$inlined$let$lambda$Anon1(this, j, session, iSessionSdkCallback)).l(new BleAdapterImpl$confirmAuthorization$$inlined$let$lambda$Anon2(this, j, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support AuthorizationFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final oy1<String> doOTA(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(bArr, "firmwareData");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Do OTA");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            rr1 rr1 = (rr1) yk1.g0(rr1.class);
            if (rr1 != null) {
                oy1<String> E = rr1.E(bArr, null);
                E.w(new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback));
                return E.s(new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback)).r(new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon3(this, bArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support OTAFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final Boolean enableMaintainConnection(FLogger.Session session) {
        pq7.c(session, "logSession");
        log(session, "Enable Maintain Connection");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            cr1 cr1 = (cr1) yk1.g0(cr1.class);
            if (cr1 != null) {
                return Boolean.valueOf(cr1.F());
            }
            log(session, "Device[" + getSerial() + "] does not support EnablingMaintainConnectionFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> eraseDataFile(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Erase DataFiles");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            xq1 xq1 = (xq1) yk1.g0(xq1.class);
            if (xq1 != null) {
                return xq1.cleanUp().s(new BleAdapterImpl$eraseDataFile$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).r(new BleAdapterImpl$eraseDataFile$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support CleanUpDeviceFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void error(FLogger.Session session, String str, ErrorCodeBuilder.Step step, String str2) {
        pq7.c(session, "logSession");
        pq7.c(str, "errorCode");
        pq7.c(step, "step");
        pq7.c(str2, "errorMessage");
        MFLog activeLog = MFLogManager.getInstance(getContext()).getActiveLog(getSerial());
        if (activeLog != null) {
            activeLog.error('[' + getSerial() + "] " + str2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, '[' + getSerial() + "] error=" + str + ' ' + str2);
        FLogger.INSTANCE.getRemote().e(FLogger.Component.BLE, session, getSerial(), TAG, str, step, str2);
    }

    @DexIgnore
    public final qy1<byte[]> exchangeSecretKey(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        qy1<byte[]> r;
        pq7.c(session, "logSession");
        pq7.c(bArr, "data");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Exchange Secret Key");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            dr1 dr1 = (dr1) yk1.g0(dr1.class);
            if (dr1 != null && (r = dr1.I(bArr).m(new BleAdapterImpl$exchangeSecretKey$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).l(new BleAdapterImpl$exchangeSecretKey$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback))) != null) {
                return r;
            }
            log(session, "Device[" + getSerial() + "] does not support ExchangingSecretKeyFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<zk1> fetchDeviceInfo(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Fetch Device Information");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            er1 er1 = (er1) yk1.g0(er1.class);
            if (er1 != null) {
                return er1.m().s(new BleAdapterImpl$fetchDeviceInfo$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).r(new BleAdapterImpl$fetchDeviceInfo$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support FetchingDeviceInformationFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public int getBatteryLevel() {
        HashMap<zm1, ym1> hashMap = this.mDeviceConfiguration;
        if (hashMap == null || !hashMap.containsKey(zm1.BATTERY)) {
            return -1;
        }
        ym1 ym1 = hashMap.get(zm1.BATTERY);
        if (ym1 != null) {
            return ((mm1) ym1).getPercentage();
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BatteryConfig");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getCurrentLabelVersion() {
        zk1 M;
        dl1 eLabelVersions;
        ry1 currentVersion;
        String shortDescription;
        yk1 yk1 = this.deviceObj;
        return (yk1 == null || (M = yk1.M()) == null || (eLabelVersions = M.getELabelVersions()) == null || (currentVersion = eLabelVersions.getCurrentVersion()) == null || (shortDescription = currentVersion.getShortDescription()) == null) ? "0.0" : shortDescription;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getCurrentWatchParamVersion() {
        zk1 M;
        dl1 watchParameterVersions;
        ry1 currentVersion;
        String shortDescription;
        yk1 yk1 = this.deviceObj;
        return (yk1 == null || (M = yk1.M()) == null || (watchParameterVersions = M.getWatchParameterVersions()) == null || (currentVersion = watchParameterVersions.getCurrentVersion()) == null || (shortDescription = currentVersion.getShortDescription()) == null) ? "0.0" : shortDescription;
    }

    @DexIgnore
    public final qy1<HashMap<zm1, ym1>> getDeviceConfig(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Get Device Configuration");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            hr1 hr1 = (hr1) yk1.g0(hr1.class);
            if (hr1 != null) {
                return hr1.R().s(new BleAdapterImpl$getDeviceConfig$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).r(new BleAdapterImpl$getDeviceConfig$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support GettingConfigurationsFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getDeviceModel() {
        zk1 M;
        String modelNumber;
        yk1 yk1 = this.deviceObj;
        return (yk1 == null || (M = yk1.M()) == null || (modelNumber = M.getModelNumber()) == null) ? "" : modelNumber;
    }

    @DexIgnore
    public final yk1 getDeviceObj() {
        return this.deviceObj;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getFirmwareVersion() {
        zk1 M;
        String firmwareVersion;
        yk1 yk1 = this.deviceObj;
        return (yk1 == null || (M = yk1.M()) == null || (firmwareVersion = M.getFirmwareVersion()) == null) ? "" : firmwareVersion;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public int getGattState() {
        return getGattState(this.deviceObj);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public HeartRateMode getHeartRateMode() {
        HeartRateMode heartRateMode;
        HashMap<zm1, ym1> hashMap = this.mDeviceConfiguration;
        if (hashMap != null) {
            if (hashMap.containsKey(zm1.HEART_RATE_MODE)) {
                HeartRateMode.Companion companion = HeartRateMode.Companion;
                ym1 ym1 = hashMap.get(zm1.HEART_RATE_MODE);
                if (ym1 != null) {
                    heartRateMode = companion.consume(((cn1) ym1).getHeartRateMode());
                } else {
                    throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.HeartRateModeConfig");
                }
            } else {
                heartRateMode = HeartRateMode.NONE;
            }
            if (heartRateMode != null) {
                return heartRateMode;
            }
        }
        return HeartRateMode.NONE;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public int getHidState() {
        return 0;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getLocale() {
        zk1 M;
        String localeString;
        yk1 yk1 = this.deviceObj;
        return (yk1 == null || (M = yk1.M()) == null || (localeString = M.getLocaleString()) == null) ? "" : localeString;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getLocaleVersion() {
        zk1 M;
        dl1 localeVersions;
        yk1 yk1 = this.deviceObj;
        return String.valueOf((yk1 == null || (M = yk1.M()) == null || (localeVersions = M.getLocaleVersions()) == null) ? null : localeVersions.getCurrentVersion());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public short getMicroAppMajorVersion() {
        zk1 M;
        ry1 microAppVersion;
        yk1 yk1 = this.deviceObj;
        if (yk1 == null || (M = yk1.M()) == null || (microAppVersion = M.getMicroAppVersion()) == null) {
            return 255;
        }
        return (short) microAppVersion.getMajor();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public short getMicroAppMinorVersion() {
        zk1 M;
        ry1 microAppVersion;
        yk1 yk1 = this.deviceObj;
        if (yk1 == null || (M = yk1.M()) == null || (microAppVersion = M.getMicroAppVersion()) == null) {
            return 255;
        }
        return (short) microAppVersion.getMinor();
    }

    @DexIgnore
    public final byte[] getSecretKeyThroughSDK(FLogger.Session session) {
        pq7.c(session, "logSession");
        log(session, "Get Secret Key Through SDK");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            lr1 lr1 = (lr1) yk1.g0(lr1.class);
            if (lr1 != null) {
                setTSecretKey(lr1.A());
                log(session, "Secret key from SDK " + getTSecretKey());
                return getTSecretKey();
            }
            log(session, "Device[" + getSerial() + "] does not support GettingSecretKeyFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public int getSupportedWatchParamMajor() {
        zk1 M;
        dl1 watchParameterVersions;
        ry1 supportedVersion;
        yk1 yk1 = this.deviceObj;
        if (yk1 == null || (M = yk1.M()) == null || (watchParameterVersions = M.getWatchParameterVersions()) == null || (supportedVersion = watchParameterVersions.getSupportedVersion()) == null) {
            return 0;
        }
        return supportedVersion.getMajor();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public int getSupportedWatchParamMinor() {
        zk1 M;
        dl1 watchParameterVersions;
        ry1 supportedVersion;
        yk1 yk1 = this.deviceObj;
        if (yk1 == null || (M = yk1.M()) == null || (watchParameterVersions = M.getWatchParameterVersions()) == null || (supportedVersion = watchParameterVersions.getSupportedVersion()) == null) {
            return 0;
        }
        return supportedVersion.getMinor();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getSupportedWatchParamVersion() {
        zk1 M;
        dl1 watchParameterVersions;
        ry1 supportedVersion;
        String shortDescription;
        yk1 yk1 = this.deviceObj;
        return (yk1 == null || (M = yk1.M()) == null || (watchParameterVersions = M.getWatchParameterVersions()) == null || (supportedVersion = watchParameterVersions.getSupportedVersion()) == null || (shortDescription = supportedVersion.getShortDescription()) == null) ? "0.0" : shortDescription;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public byte[] getTSecretKey() {
        return this.tSecretKey;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public ry1 getUiPackageOSVersion() {
        zk1 M;
        yk1 yk1 = this.deviceObj;
        if (yk1 == null || (M = yk1.M()) == null) {
            return null;
        }
        return M.getUiPackageOSVersion();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public VibrationStrengthObj getVibrationStrength() {
        VibrationStrengthObj consumeSDKVibrationStrengthLevel;
        HashMap<zm1, ym1> hashMap = this.mDeviceConfiguration;
        if (hashMap != null) {
            if (hashMap.containsKey(zm1.VIBE_STRENGTH)) {
                ym1 ym1 = hashMap.get(zm1.VIBE_STRENGTH);
                if (ym1 != null) {
                    hn1.a vibeStrengthLevel = ((hn1) ym1).getVibeStrengthLevel();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.e(str, "Device config not null, vibeStrengthConfig not null, return vibrationStrength: value = " + vibeStrengthLevel);
                    consumeSDKVibrationStrengthLevel = VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel$default(VibrationStrengthObj.Companion, vibeStrengthLevel, false, 2, null);
                } else {
                    throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.VibeStrengthConfig");
                }
            } else {
                FLogger.INSTANCE.getLocal().e(TAG, "Device config not null but vibeStrengthConfig is null, vibrationStrength return default value MEDIUM");
                consumeSDKVibrationStrengthLevel = VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel(hn1.a.MEDIUM, true);
            }
            if (consumeSDKVibrationStrengthLevel != null) {
                return consumeSDKVibrationStrengthLevel;
            }
        }
        FLogger.INSTANCE.getLocal().e(TAG, "Device config null, vibrationStrength return default value MEDIUM");
        return VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel(hn1.a.MEDIUM, true);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public boolean isDeviceReady() {
        boolean z = false;
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            if (yk1.getState() == yk1.c.CONNECTED) {
                z = true;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append(".isDeviceReady() - serial=");
            sb.append(getSerial());
            sb.append(", state=");
            sb.append(yk1.getState());
            sb.append(", ready=");
            sb.append(!z ? "NO" : "YES");
            local.d(str, sb.toString());
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.e(str2, "Is device ready " + getSerial() + ". FAILED: deviceObject is NULL");
        }
        return z;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public <T> T isSupportedFeature(Class<T> cls) {
        pq7.c(cls, "feature");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            return (T) yk1.g0(cls);
        }
        return null;
    }

    @DexIgnore
    public final void logAppError(FLogger.Session session, String str, ErrorCodeBuilder.Step step, ErrorCodeBuilder.AppError appError) {
        pq7.c(session, "logSession");
        pq7.c(str, "message");
        pq7.c(step, "step");
        pq7.c(appError, "error");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, '[' + getSerial() + "] " + str + ", appError=" + appError);
        error(session, ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, appError), step, str);
    }

    @DexIgnore
    public final void logSdkError(FLogger.Session session, String str, ErrorCodeBuilder.Step step, yx1 yx1) {
        pq7.c(session, "logSession");
        pq7.c(str, "message");
        pq7.c(step, "step");
        pq7.c(yx1, "sdkError");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, '[' + getSerial() + "] " + str + ", sdkError=" + yx1.getErrorCode().getLogName());
        error(session, ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, yx1), step, str);
    }

    @DexIgnore
    public final qy1<tl7> notifyMusicEvent(FLogger.Session session, xn1 xn1) {
        pq7.c(session, "logSession");
        pq7.c(xn1, "musicEvent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Notify Music Event, musicEvent=" + xn1);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Notify Music Event");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            qr1 qr1 = (qr1) yk1.g0(qr1.class);
            if (qr1 != null) {
                return qr1.i(xn1);
            }
            log(session, "Device[" + getSerial() + "] does not support NotifyingMusicEventFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> notifyNotificationEvent(FLogger.Session session, NotificationBaseObj notificationBaseObj, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(notificationBaseObj, "notifyNotificationEvent");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Do notifyNotificationEvent");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            pr1 pr1 = (pr1) yk1.g0(pr1.class);
            DianaNotificationObj dianaNotificationObj = (DianaNotificationObj) notificationBaseObj;
            if (pr1 != null) {
                return pr1.z(dianaNotificationObj.toSDKNotifyNotificationEvent()).m(new BleAdapterImpl$notifyNotificationEvent$$inlined$let$lambda$Anon1(this, dianaNotificationObj, session, iSessionSdkCallback)).l(new BleAdapterImpl$notifyNotificationEvent$$inlined$let$lambda$Anon2(this, dianaNotificationObj, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support notifyNotificationEvent");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final ThemeData parseBinaryToThemeData(byte[] bArr) {
        ThemeData themeData;
        pq7.c(bArr, "byteArray");
        lw1 a2 = lw1.CREATOR.a(bArr);
        kw1 edit = a2 != null ? a2.edit() : null;
        if (!(edit instanceof nw1)) {
            edit = null;
        }
        nw1 nw1 = (nw1) edit;
        return (nw1 == null || (themeData = ThemeExtentionKt.toThemeData(nw1)) == null) ? new ThemeData(null, null, 3, null) : themeData;
    }

    @DexIgnore
    public final qy1<tl7> playDeviceAnimation(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Play Device Animation");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            nr1 nr1 = (nr1) yk1.g0(nr1.class);
            if (nr1 != null) {
                return nr1.c().m(new BleAdapterImpl$playDeviceAnimation$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).l(new BleAdapterImpl$playDeviceAnimation$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support HandAnimationFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final oy1<tl7> previewTheme(FLogger.Session session, kw1 kw1, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(kw1, "themeEditor");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            ss1 ss1 = (ss1) yk1.g0(ss1.class);
            if (ss1 != null) {
                oy1<tl7> x = ss1.U(kw1).s(new BleAdapterImpl$previewTheme$$inlined$let$lambda$Anon4(this, kw1, iSessionSdkCallback, session));
                x.w(new BleAdapterImpl$previewTheme$$inlined$let$lambda$Anon5(this, kw1, iSessionSdkCallback, session));
                return x.r(new BleAdapterImpl$previewTheme$$inlined$let$lambda$Anon6(this, kw1, iSessionSdkCallback, session));
            }
            log(session, "Device[" + getSerial() + "] does not support applyTheme");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final oy1<tl7> previewTheme(FLogger.Session session, lw1 lw1, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(lw1, "themePackage");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            ss1 ss1 = (ss1) yk1.g0(ss1.class);
            if (ss1 != null) {
                oy1<tl7> x = ss1.N(lw1).s(new BleAdapterImpl$previewTheme$$inlined$let$lambda$Anon1(this, lw1, iSessionSdkCallback, session));
                x.w(new BleAdapterImpl$previewTheme$$inlined$let$lambda$Anon2(this, lw1, iSessionSdkCallback, session));
                return x.r(new BleAdapterImpl$previewTheme$$inlined$let$lambda$Anon3(this, lw1, iSessionSdkCallback, session));
            }
            log(session, "Device[" + getSerial() + "] does not support applyTheme");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<lp1> readCurrentWorkoutSession(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        qy1<lp1> r;
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Get Workout Session");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            mr1 mr1 = (mr1) yk1.g0(mr1.class);
            if (mr1 != null && (r = mr1.V().m(new BleAdapterImpl$readCurrentWorkoutSession$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).l(new BleAdapterImpl$readCurrentWorkoutSession$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback))) != null) {
                return r;
            }
            log(session, "Device[" + getSerial() + "] does not support GettingWorkoutSessionFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final oy1<FitnessData[]> readDataFile(FLogger.Session session, nm1 nm1, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(nm1, "biometricProfile");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Read DataFiles");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            rs1 rs1 = (rs1) yk1.g0(rs1.class);
            if (rs1 != null) {
                oy1<FitnessData[]> g = rs1.g(nm1);
                g.w(new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon1(this, nm1, session, iSessionSdkCallback));
                return g.s(new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon2(this, nm1, session, iSessionSdkCallback)).r(new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon3(this, nm1, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SynchronizationFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<Integer> readRssi(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Read Rssi");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            sr1 sr1 = (sr1) yk1.g0(sr1.class);
            if (sr1 != null) {
                return sr1.H().m(new BleAdapterImpl$readRssi$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).l(new BleAdapterImpl$readRssi$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support ReadingRSSIFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void registerBluetoothStateCallback(yk1.b bVar) {
        pq7.c(bVar, Constants.CALLBACK);
        this.mDeviceStateListener = bVar;
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            yk1.B(bVar);
        }
    }

    @DexIgnore
    public final void sendCustomCommand(CustomRequest customRequest) {
        qy1 sendCustomCommand$default;
        qy1 s;
        pq7.c(customRequest, Constants.COMMAND);
        log(FLogger.Session.OTHER, "Send Custom Command: " + customRequest);
        yk1 yk1 = this.deviceObj;
        if (!(yk1 instanceof zq1)) {
            yk1 = null;
        }
        zq1 zq1 = (zq1) yk1;
        if (zq1 == null || (sendCustomCommand$default = CustomCommandFeature.DefaultImpls.sendCustomCommand$default(zq1, customRequest.getCustomCommand(), nn1.DC, 0, 0, 12, (Object) null)) == null || (s = sendCustomCommand$default.m(new BleAdapterImpl$sendCustomCommand$Anon1(this, customRequest))) == null || s.l(new BleAdapterImpl$sendCustomCommand$Anon2(this)) == null) {
            log(FLogger.Session.OTHER, "Send Custom Command not support");
            tl7 tl7 = tl7.f3441a;
        }
    }

    @DexIgnore
    public final qy1<tl7> sendNotification(FLogger.Session session, NotificationBaseObj notificationBaseObj) {
        pq7.c(session, "logSession");
        pq7.c(notificationBaseObj, "notification");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Notification, notification=" + notificationBaseObj);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Notification");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            wr1 wr1 = (wr1) yk1.g0(wr1.class);
            if (wr1 != null) {
                zn1 sDKNotification = notificationBaseObj.toSDKNotification();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "send sdk notification " + sDKNotification.toString());
                return wr1.s(sDKNotification);
            }
            log(session, "Device[" + getSerial() + "] does not support SendingAppNotificationFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> sendRespond(FLogger.Session session, mt1 mt1) {
        pq7.c(session, "logSession");
        pq7.c(mt1, "deviceData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Respond, deviceData=" + mt1);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Respond");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            xr1 xr1 = (xr1) yk1.g0(xr1.class);
            if (xr1 != null) {
                return xr1.y(mt1);
            }
            log(session, "Device[" + getSerial() + "] does not support SendingDataFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> sendTrackInfo(FLogger.Session session, yn1 yn1) {
        pq7.c(session, "logSession");
        pq7.c(yn1, "trackInfo");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Track Info, trackInfo=" + yn1);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Track Info");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            zr1 zr1 = (zr1) yk1.g0(zr1.class);
            if (zr1 != null) {
                return zr1.Y(yn1);
            }
            log(session, "Device[" + getSerial() + "] does not support SendingTrackInfoFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> sendingEncryptedData(byte[] bArr, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(bArr, "encryptedData");
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "sendingEncryptedData");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            yr1 yr1 = (yr1) yk1.g0(yr1.class);
            if (yr1 != null) {
                return yr1.x(bArr).s(new BleAdapterImpl$sendingEncryptedData$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).r(new BleAdapterImpl$sendingEncryptedData$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SendingEncryptedDataFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> setAlarms(FLogger.Session session, List<AlarmSetting> list, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(list, "alarms");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set List Alarms: " + list);
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            as1 as1 = (as1) yk1.g0(as1.class);
            if (as1 != null) {
                return as1.n(AlarmExtensionKt.toSDKV2Setting(list)).s(new BleAdapterImpl$setAlarms$$inlined$let$lambda$Anon1(this, list, session, iSessionSdkCallback)).r(new BleAdapterImpl$setAlarms$$inlined$let$lambda$Anon2(this, list, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingAlarmFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> setBackgroundImage(FLogger.Session session, BackgroundConfig backgroundConfig, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(backgroundConfig, "backgroundConfig");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Background Image: " + backgroundConfig);
        String directory = FileUtils.getDirectory(getContext(), FileType.WATCH_FACE);
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            js1 js1 = (js1) yk1.g0(js1.class);
            if (js1 != null) {
                pq7.b(directory, "directory");
                return js1.h(backgroundConfig.toSDKBackgroundImageConfig(directory)).s(new BleAdapterImpl$setBackgroundImage$$inlined$let$lambda$Anon1(this, backgroundConfig, directory, session, iSessionSdkCallback)).r(new BleAdapterImpl$setBackgroundImage$$inlined$let$lambda$Anon2(this, backgroundConfig, directory, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingPresetFeature - set BackgroundImageFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final boolean setDevice(yk1 yk1) {
        pq7.c(yk1, "deviceObj");
        setDeviceObj(yk1);
        DevicePreferenceUtils.setUiMinorVersion(getContext(), yk1.M().getSerialNumber(), yk1.M().getUiPackageOSVersion().getMinor());
        DevicePreferenceUtils.setUiMajorVersion(getContext(), yk1.M().getSerialNumber(), yk1.M().getUiPackageOSVersion().getMajor());
        return true;
    }

    @DexIgnore
    public final qy1<zm1[]> setDeviceConfig(FLogger.Session session, ym1[] ym1Arr, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(ym1Arr, "deviceConfigItems");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Device Config: " + new Gson().t(ym1Arr));
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            es1 es1 = (es1) yk1.g0(es1.class);
            if (es1 != null) {
                return es1.J(ym1Arr).s(new BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon1(this, ym1Arr, session, iSessionSdkCallback)).r(new BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon2(this, ym1Arr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingConfigurationsFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void setDeviceObj(yk1 yk1) {
        this.deviceObj = yk1;
        if (yk1 != null) {
            if (!TextUtils.isEmpty(yk1.M().getMacAddress())) {
                setMacAddress(yk1.M().getMacAddress());
            }
            yk1.B(this.mDeviceStateListener);
        }
    }

    @DexIgnore
    public final qy1<tl7> setFrontLightEnable(FLogger.Session session, boolean z, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Front Light Enable: " + z);
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            gs1 gs1 = (gs1) yk1.g0(gs1.class);
            if (gs1 != null) {
                return gs1.f(z).s(new BleAdapterImpl$setFrontLightEnable$$inlined$let$lambda$Anon1(this, z, session, iSessionSdkCallback)).r(new BleAdapterImpl$setFrontLightEnable$$inlined$let$lambda$Anon2(this, z, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingFrontLightFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final oy1<ry1> setLabelFile(FLogger.Session session, ou1 ou1, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(ou1, "labelFile");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "setLabelFile File: " + ou1);
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            fs1 fs1 = (fs1) yk1.g0(fs1.class);
            if (fs1 != null) {
                return fs1.q(ou1).s(new BleAdapterImpl$setLabelFile$$inlined$let$lambda$Anon1(this, ou1, session, iSessionSdkCallback)).r(new BleAdapterImpl$setLabelFile$$inlined$let$lambda$Anon2(this, ou1, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support setLabelFiles");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final oy1<String> setLocalizationData(LocalizationData localizationData, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(localizationData, "localizationData");
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Localization");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            hs1 hs1 = (hs1) yk1.g0(hs1.class);
            if (hs1 != null) {
                return hs1.e(localizationData.toLocalizationFile()).s(new BleAdapterImpl$setLocalizationData$$inlined$let$lambda$Anon1(this, localizationData, session, iSessionSdkCallback)).r(new BleAdapterImpl$setLocalizationData$$inlined$let$lambda$Anon2(this, localizationData, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingLocalizationFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> setMinimumStepThreshold(long j, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "setMinimumStepThreshold");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            wq1 wq1 = (wq1) yk1.g0(wq1.class);
            if (wq1 != null) {
                return wq1.K(j).s(new BleAdapterImpl$setMinimumStepThreshold$$inlined$let$lambda$Anon1(this, j, session, iSessionSdkCallback)).r(new BleAdapterImpl$setMinimumStepThreshold$$inlined$let$lambda$Anon2(this, j, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support BuddyChallengeFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final oy1<tl7> setNotificationFilters(FLogger.Session session, List<AppNotificationFilter> list, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(list, "notificationFilters");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        ArrayList arrayList = new ArrayList(im7.m(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().toSDKNotificationFilter(getContext()));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Set Notification Filter: " + list + " bleNotifications " + arrayList);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Set Notification Filter");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            is1 is1 = (is1) yk1.g0(is1.class);
            if (is1 != null) {
                Object[] array = arrayList.toArray(new jo1[0]);
                if (array != null) {
                    oy1<tl7> D = is1.D((jo1[]) array);
                    D.w(new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon1(this, arrayList, session, iSessionSdkCallback));
                    return D.s(new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon2(this, arrayList, session, iSessionSdkCallback)).r(new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon3(this, arrayList, session, iSessionSdkCallback));
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
            log(session, "Device[" + getSerial() + "] does not support SettingLocalizationFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0086, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0087, code lost:
        com.fossil.so7.a(r9, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x008a, code lost:
        throw r1;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.oy1<com.fossil.tl7> setReplyMessageMapping(com.misfit.frameworks.buttonservice.log.FLogger.Session r12, com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup r13, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback r14) {
        /*
        // Method dump skipped, instructions count: 273
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl.setReplyMessageMapping(com.misfit.frameworks.buttonservice.log.FLogger$Session, com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback):com.fossil.oy1");
    }

    @DexIgnore
    public final void setSecretKey(FLogger.Session session, byte[] bArr) {
        pq7.c(session, "logSession");
        pq7.c(bArr, "secretKey");
        log(session, "Set Secret Key");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            ls1 ls1 = (ls1) yk1.g0(ls1.class);
            if (ls1 != null) {
                ls1.Z(bArr);
                setTSecretKey(bArr);
                return;
            }
            log(session, "Device[" + getSerial() + "] does not support GettingSecretKeyFeature");
            return;
        }
        FLogger.INSTANCE.getLocal().e(TAG, "Set Secret Key. FAILED: deviceObject is NULL");
    }

    @DexIgnore
    public void setTSecretKey(byte[] bArr) {
        this.tSecretKey = bArr;
    }

    @DexIgnore
    public final oy1<ru1[]> setWatchAppFiles(FLogger.Session session, ru1[] ru1Arr, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(ru1Arr, "watchAppFiles");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Watch App Files: " + ru1Arr);
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            ms1 ms1 = (ms1) yk1.g0(ms1.class);
            if (ms1 != null) {
                oy1<ru1[]> v = ms1.v(ru1Arr).s(new BleAdapterImpl$setWatchAppFiles$$inlined$let$lambda$Anon1(this, ru1Arr, session, iSessionSdkCallback)).r(new BleAdapterImpl$setWatchAppFiles$$inlined$let$lambda$Anon2(this, ru1Arr, session, iSessionSdkCallback));
                v.w(new BleAdapterImpl$setWatchAppFiles$$inlined$let$lambda$Anon3(this, ru1Arr, session, iSessionSdkCallback));
                return v;
            }
            log(session, "Device[" + getSerial() + "] does not support SettingUpWatchAppFeature - setWatchAppFiles");
            iSessionSdkCallback.onSetWatchAppFileFailed();
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> setWatchApps(FLogger.Session session, WatchAppMappingSettings watchAppMappingSettings, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(watchAppMappingSettings, "watchAppMappingSettings");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Watch Apps: " + watchAppMappingSettings);
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            js1 js1 = (js1) yk1.g0(js1.class);
            if (js1 != null) {
                return js1.h(watchAppMappingSettings.toSDKSetting()).s(new BleAdapterImpl$setWatchApps$$inlined$let$lambda$Anon1(this, watchAppMappingSettings, session, iSessionSdkCallback)).r(new BleAdapterImpl$setWatchApps$$inlined$let$lambda$Anon2(this, watchAppMappingSettings, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingPresetFeature - setWatchApps");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qy1<tl7> setWatchParams(FLogger.Session session, WatchParamsFileMapping watchParamsFileMapping, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(watchParamsFileMapping, "watchParamFileMapping");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set WatchParams: " + watchParamsFileMapping);
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            os1 os1 = (os1) yk1.g0(os1.class);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "Setting watchParam with file value = " + watchParamsFileMapping.toSDKSetting());
            if (os1 != null) {
                return os1.a0(watchParamsFileMapping.toSDKSetting()).s(new BleAdapterImpl$setWatchParams$$inlined$let$lambda$Anon1(this, watchParamsFileMapping, session, iSessionSdkCallback)).r(new BleAdapterImpl$setWatchParams$$inlined$let$lambda$Anon2(this, watchParamsFileMapping, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingWatchParameterFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void setWorkoutGPSData(Location location, FLogger.Session session) {
        pq7.c(location, "data");
        pq7.c(session, "logSession");
        GpsDataPoint gpsDataPoint = LocationExtensionKt.toGpsDataPoint(location);
        GpsDataProvider.defaultProvider().onLocationChanged(gpsDataPoint);
        log(session, "setWorkoutGPSData dataPoint " + gpsDataPoint);
    }

    @DexIgnore
    public final qy1<byte[]> startAuthenticate(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(bArr, "phoneRandomNumber");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Start Authenticate phoneRandomNumber " + ConversionUtils.INSTANCE.byteArrayToString(bArr));
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Start Authenticate phoneRandomNumber");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            ps1 ps1 = (ps1) yk1.g0(ps1.class);
            if (ps1 != null) {
                return ps1.b(bArr).m(new BleAdapterImpl$startAuthenticate$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).l(new BleAdapterImpl$startAuthenticate$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support StartingAuthenticateFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void startScanning(FLogger.Session session, long j, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Start scanning...");
        this.isScanning = true;
        ScanService scanService2 = new ScanService(getContext(), new BleScanServiceCallback(iSessionSdkCallback), j);
        this.scanService = scanService2;
        if (scanService2 != null) {
            scanService2.setActiveDeviceLog(getSerial());
            ScanService scanService3 = this.scanService;
            if (scanService3 != null) {
                scanService3.startScanWithAutoStopTimer();
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final qy1<tl7> stopCurrentWorkoutSession(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        pq7.c(session, "logSession");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Stop Current Workout Session");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            qs1 qs1 = (qs1) yk1.g0(qs1.class);
            if (qs1 != null) {
                return qs1.t().m(new BleAdapterImpl$stopCurrentWorkoutSession$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).l(new BleAdapterImpl$stopCurrentWorkoutSession$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support StoppingWorkoutSessionFeature");
            return null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void stopScanning(FLogger.Session session) {
        pq7.c(session, "logSession");
        ScanService scanService2 = this.scanService;
        if (scanService2 != null) {
            scanService2.stopScan();
        }
        if (this.isScanning) {
            log(session, "Stop scanning for " + getSerial());
        }
        this.isScanning = false;
    }

    @DexIgnore
    public final qy1<Boolean> verifySecretKey(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        qy1<Boolean> r;
        pq7.c(session, "logSession");
        pq7.c(bArr, "secretKey");
        pq7.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Verify Secret Key");
        yk1 yk1 = this.deviceObj;
        if (yk1 != null) {
            ts1 ts1 = (ts1) yk1.g0(ts1.class);
            if (ts1 != null && (r = ts1.w(bArr).m(new BleAdapterImpl$verifySecretKey$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).l(new BleAdapterImpl$verifySecretKey$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback))) != null) {
                return r;
            }
            log(session, "Device[" + getSerial() + "] does not support VerifyingSecretKeyFeature");
            return null;
        }
        pq7.i();
        throw null;
    }
}
