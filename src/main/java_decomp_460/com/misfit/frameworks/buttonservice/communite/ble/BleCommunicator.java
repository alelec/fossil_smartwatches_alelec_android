package com.misfit.frameworks.buttonservice.communite.ble;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.HandlerThread;
import android.text.TextUtils;
import com.fossil.as1;
import com.fossil.es1;
import com.fossil.fitness.FitnessData;
import com.fossil.hs1;
import com.fossil.il7;
import com.fossil.js1;
import com.fossil.kw1;
import com.fossil.lw1;
import com.fossil.pq7;
import com.fossil.tl7;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.buttonservice.model.pairing.LabelResponse;
import com.misfit.frameworks.buttonservice.model.pairing.PairingAuthorizeResponse;
import com.misfit.frameworks.buttonservice.model.pairing.PairingLinkServerResponse;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.buttonservice.model.pairing.PairingUpdateFWResponse;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeConfig;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;
import com.misfit.frameworks.buttonservice.model.watchparams.Version;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BleCommunicator {
    @DexIgnore
    public /* final */ int NOTIFICATION_THRESHOLD; // = 20;
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public BleSession.BleSessionCallback bleSessionCallback; // = new BleCommunicator$bleSessionCallback$Anon1(this);
    @DexIgnore
    public /* final */ CommunicationResultCallback communicationResultCallback;
    @DexIgnore
    public /* final */ HandlerThread handlerThread;
    @DexIgnore
    public PriorityBlockingQueue<BleSession> highSessionQueue;
    @DexIgnore
    public PriorityBlockingQueue<BleSession> lowSessionQueue;
    @DexIgnore
    public /* final */ ArrayList<DianaNotificationObj> mNotificationQueue; // = new ArrayList<>();
    @DexIgnore
    public /* final */ String serial;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1<T> implements Comparator<BleSession> {
        @DexIgnore
        public static /* final */ Anon1 INSTANCE; // = new Anon1();

        @DexIgnore
        public final int compare(BleSession bleSession, BleSession bleSession2) {
            return bleSession2.getSessionType().compareTo(bleSession.getSessionType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2<T> implements Comparator<BleSession> {
        @DexIgnore
        public static /* final */ Anon2 INSTANCE; // = new Anon2();

        @DexIgnore
        public final int compare(BleSession bleSession, BleSession bleSession2) {
            return bleSession2.getSessionType().compareTo(bleSession.getSessionType());
        }
    }

    @DexIgnore
    public interface CommunicationResultCallback {
        @DexIgnore
        void onAskForCurrentSecretKey(String str);

        @DexIgnore
        void onAskForLabelFile(String str, CommunicateMode communicateMode);

        @DexIgnore
        void onAskForLinkServer(String str, CommunicateMode communicateMode, Bundle bundle);

        @DexIgnore
        void onAskForRandomKey(String str);

        @DexIgnore
        void onAskForServerSecretKey(String str, Bundle bundle);

        @DexIgnore
        void onAskForStopWorkout(String str);

        @DexIgnore
        void onAskForWatchAppFiles(String str, Bundle bundle);

        @DexIgnore
        void onAuthorizeDeviceSuccess(String str);

        @DexIgnore
        void onCommunicatorResult(CommunicateMode communicateMode, String str, int i, List<Integer> list, Bundle bundle);

        @DexIgnore
        void onDeviceAppsRequest(int i, Bundle bundle, String str);

        @DexIgnore
        void onExchangeSecretKeySuccess(String str, String str2);

        @DexIgnore
        void onFirmwareLatest(String str);

        @DexIgnore
        void onGattConnectionStateChanged(String str, int i);

        @DexIgnore
        void onHeartBeatDataReceived(int i, int i2, String str);

        @DexIgnore
        void onHeartRateNotification(short s, String str);

        @DexIgnore
        void onHidConnectionStateChanged(String str, int i);

        @DexIgnore
        void onNeedStartTimer(String str);

        @DexIgnore
        void onNotificationSent(int i, boolean z);

        @DexIgnore
        void onOtaProgressUpdated(String str, float f);

        @DexIgnore
        void onPreparationCompleted(boolean z, String str);

        @DexIgnore
        void onReadCurrentWorkoutSuccess(String str, Bundle bundle);

        @DexIgnore
        void onReceivedSyncData(String str, Bundle bundle);

        @DexIgnore
        void onRequestLatestFirmware(String str, Bundle bundle);

        @DexIgnore
        void onRequestLatestWatchParams(String str, Bundle bundle);

        @DexIgnore
        void onRequestPushSecretKeyToServer(String str, String str2);

        @DexIgnore
        void onUpdateFirmwareFailed(String str);

        @DexIgnore
        void onUpdateFirmwareSuccess(String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[SessionType.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[SessionType.SPECIAL.ordinal()] = 1;
            $EnumSwitchMapping$0[SessionType.SYNC.ordinal()] = 2;
            $EnumSwitchMapping$0[SessionType.CONNECT.ordinal()] = 3;
            $EnumSwitchMapping$0[SessionType.UI.ordinal()] = 4;
            $EnumSwitchMapping$0[SessionType.DEVICE_SETTING.ordinal()] = 5;
            $EnumSwitchMapping$0[SessionType.URGENT.ordinal()] = 6;
        }
        */
    }

    @DexIgnore
    public BleCommunicator(String str, CommunicationResultCallback communicationResultCallback2) {
        pq7.c(str, "serial");
        pq7.c(communicationResultCallback2, "communicationResultCallback");
        this.serial = str;
        this.communicationResultCallback = communicationResultCallback2;
        String simpleName = BleCommunicator.class.getSimpleName();
        pq7.b(simpleName, "BleCommunicator::class.java.simpleName");
        this.TAG = simpleName;
        HandlerThread handlerThread2 = new HandlerThread(this.TAG);
        this.handlerThread = handlerThread2;
        handlerThread2.start();
        this.highSessionQueue = new PriorityBlockingQueue<>(11, Anon1.INSTANCE);
        this.lowSessionQueue = new PriorityBlockingQueue<>(11, Anon2.INSTANCE);
    }

    @DexIgnore
    public static /* synthetic */ boolean previewTheme$default(BleCommunicator bleCommunicator, lw1 lw1, kw1 kw1, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                lw1 = null;
            }
            if ((i & 2) != 0) {
                kw1 = null;
            }
            return bleCommunicator.previewTheme(lw1, kw1);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: previewTheme");
    }

    @DexIgnore
    public static /* synthetic */ boolean startConnectionDeviceSession$default(BleCommunicator bleCommunicator, boolean z, UserProfile userProfile, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                userProfile = null;
            }
            return bleCommunicator.startConnectionDeviceSession(z, userProfile);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: startConnectionDeviceSession");
    }

    @DexIgnore
    public static /* synthetic */ void startSendDeviceAppResponse$default(BleCommunicator bleCommunicator, DeviceAppResponse deviceAppResponse, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                z = false;
            }
            bleCommunicator.startSendDeviceAppResponse(deviceAppResponse, z);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: startSendDeviceAppResponse");
    }

    @DexIgnore
    public abstract boolean applyTheme(ThemeConfig themeConfig);

    @DexIgnore
    public final void cancelCalibrationSession() {
        synchronized (this) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.d(str, ".cancelCalibrationSession() - current session=" + getCurrentSession());
            if (!BleSession.Companion.isNull(getCurrentSession()) && (getCurrentSession() instanceof ICalibrationSession)) {
                BleSession currentSession = getCurrentSession();
                if (currentSession != null) {
                    ((ICalibrationSession) currentSession).handleReleaseHandControl();
                } else {
                    throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.ICalibrationSession");
                }
            }
            ArrayList arrayList = new ArrayList();
            printQueue();
            Iterator<BleSession> it = this.highSessionQueue.iterator();
            while (it.hasNext()) {
                BleSession next = it.next();
                if (next instanceof ICalibrationSession) {
                    arrayList.add(next);
                    next.stop(0);
                }
            }
            this.highSessionQueue.removeAll(arrayList);
        }
    }

    @DexIgnore
    public final void cancelPairDevice() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, ".cancelPairDevice() - current session=" + getCurrentSession());
        if (!BleSession.Companion.isNull(getCurrentSession()) && (getCurrentSession() instanceof IPairDeviceSession)) {
            FLogger.INSTANCE.getRemote().d(FLogger.Component.BLE, FLogger.Session.PAIR, this.serial, this.TAG, "App called cancel Pair Device.");
            getCurrentSession().stop(FailureCode.SESSION_INTERRUPTED);
        }
        ArrayList arrayList = new ArrayList();
        printQueue();
        Iterator<BleSession> it = this.highSessionQueue.iterator();
        while (it.hasNext()) {
            BleSession next = it.next();
            if (next instanceof IPairDeviceSession) {
                arrayList.add(next);
                next.stop(0);
            }
        }
        this.highSessionQueue.removeAll(arrayList);
    }

    @DexIgnore
    public void cleanUp() {
        DevicePreferenceUtils.clearAutoListAlarm(getBleAdapter().getContext());
        DevicePreferenceUtils.clearAutoSetMapping(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAutoComplicationAppSettings(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAutoWatchAppSettings(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAutoBackgroundImageConfig(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAutoNotificationFiltersConfig(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAllSettingFlag(getBleAdapter().getContext());
    }

    @DexIgnore
    public abstract void clearQuickCommandQueue();

    @DexIgnore
    public final void clearSessionQueue() {
        synchronized (this) {
            synchronized (this.lowSessionQueue) {
                synchronized (this.highSessionQueue) {
                    Iterator<BleSession> it = this.highSessionQueue.iterator();
                    while (it.hasNext()) {
                        it.next().stop(FailureCode.SESSION_INTERRUPTED);
                    }
                    this.highSessionQueue.clear();
                    Iterator<BleSession> it2 = this.lowSessionQueue.iterator();
                    while (it2.hasNext()) {
                        it2.next().stop(FailureCode.SESSION_INTERRUPTED);
                    }
                    this.lowSessionQueue.clear();
                    if (getCurrentSession().getCommunicateMode() != CommunicateMode.UNLINK) {
                        interruptCurrentSession();
                    }
                    tl7 tl7 = tl7.f3441a;
                }
                tl7 tl72 = tl7.f3441a;
            }
        }
    }

    @DexIgnore
    public final void closeConnection() {
        clearSessionQueue();
        if (getCurrentSession().getCommunicateMode() != CommunicateMode.UNLINK) {
            getBleAdapter().closeConnection(FLogger.Session.OTHER, true);
        }
    }

    @DexIgnore
    public abstract void confirmStopWorkout(String str, boolean z);

    @DexIgnore
    public final boolean containSyncMode() {
        CommunicateMode communicateMode;
        CommunicateMode communicateMode2 = getCommunicateMode();
        if (communicateMode2 != CommunicateMode.SYNC && !this.highSessionQueue.isEmpty()) {
            Iterator<BleSession> it = this.highSessionQueue.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                CommunicateMode communicateMode3 = it.next().getCommunicateMode();
                communicateMode = CommunicateMode.SYNC;
                if (communicateMode3 == communicateMode) {
                    break;
                }
            }
        }
        communicateMode = communicateMode2;
        return communicateMode == CommunicateMode.SYNC;
    }

    @DexIgnore
    public abstract boolean disableHeartRateNotification();

    @DexIgnore
    public abstract boolean enableHeartRateNotification();

    @DexIgnore
    public abstract BleAdapter getBleAdapter();

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final CommunicateMode getCommunicateMode() {
        return BleSession.Companion.isNull(getCurrentSession()) ? CommunicateMode.IDLE : getCurrentSession().getCommunicateMode();
    }

    @DexIgnore
    public final CommunicationResultCallback getCommunicationResultCallback() {
        return this.communicationResultCallback;
    }

    @DexIgnore
    public abstract BleSession getCurrentSession();

    @DexIgnore
    public final String getCurrentSessionName() {
        if (BleSession.Companion.isNull(getCurrentSession())) {
            return "NULL";
        }
        String simpleName = getCurrentSession().getClass().getSimpleName();
        pq7.b(simpleName, "currentSession.javaClass.simpleName");
        return simpleName;
    }

    @DexIgnore
    public final String getCurrentStateName() {
        if (BleSession.Companion.isNull(getCurrentSession())) {
            return "NULL Session";
        }
        if (BleState.Companion.isNull(getCurrentSession().getCurrentState())) {
            return "NULL State";
        }
        String name = getCurrentSession().getCurrentState().getClass().getName();
        pq7.b(name, "currentSession.currentState.javaClass.name");
        return name;
    }

    @DexIgnore
    public final int getGattState() {
        return getBleAdapter().getGattState();
    }

    @DexIgnore
    public final HandlerThread getHandlerThread() {
        return this.handlerThread;
    }

    @DexIgnore
    public final int getHidState() {
        return getBleAdapter().getHidState();
    }

    @DexIgnore
    public final PriorityBlockingQueue<BleSession> getHighSessionQueue() {
        return this.highSessionQueue;
    }

    @DexIgnore
    public final PriorityBlockingQueue<BleSession> getLowSessionQueue() {
        return this.lowSessionQueue;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public abstract List<FitnessData> getSyncData();

    @DexIgnore
    public final String getTAG() {
        return this.TAG;
    }

    @DexIgnore
    public abstract Version getUiOSVersion();

    @DexIgnore
    public final void interruptCurrentSession() {
        synchronized (this) {
            if (!BleSession.Companion.isNull(getCurrentSession()) && getCurrentSession().getSessionType() != SessionType.CONNECT_WITHOUT_TIMEOUT) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.TAG;
                local.d(str, "Inside " + this.TAG + ".interruptCurrentSession - currentSession=" + getCurrentSession());
                getCurrentSession().stop(FailureCode.SESSION_INTERRUPTED);
                setNullCurrentSession();
            }
        }
    }

    @DexIgnore
    public abstract boolean isDeviceReady();

    @DexIgnore
    public final boolean isQueueEmpty() {
        boolean z;
        synchronized (this) {
            z = this.highSessionQueue.isEmpty() && this.lowSessionQueue.isEmpty();
        }
        return z;
    }

    @DexIgnore
    public final boolean isRunning() {
        return !BleSession.Companion.isNull(getCurrentSession()) || !isQueueEmpty();
    }

    @DexIgnore
    public final void log(String str) {
        MFLog mfLog;
        pq7.c(str, "message");
        if (!BleSession.Companion.isNull(getCurrentSession()) && (mfLog = getCurrentSession().getMfLog()) != null) {
            mfLog.log('[' + this.serial + "] " + str);
        }
    }

    @DexIgnore
    public final void logNoCurrentSession(String str) {
        pq7.c(str, "handle");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.TAG;
        local.i(str2, "No running session to handle " + str);
        log("No running session to handle " + str);
    }

    @DexIgnore
    public final void logNoCurrentState(String str) {
        pq7.c(str, "handle");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.TAG;
        local.i(str2, "No current state to handle " + str);
        log("No current state to handle " + str);
    }

    @DexIgnore
    public abstract boolean onPing();

    @DexIgnore
    public abstract boolean onReceiveCurrentSecretKey(byte[] bArr);

    @DexIgnore
    public abstract boolean onReceivePushSecretKeyResponse(boolean z);

    @DexIgnore
    public abstract boolean onReceiveServerRandomKey(byte[] bArr, int i);

    @DexIgnore
    public abstract boolean onReceiveServerSecretKey(byte[] bArr, int i);

    @DexIgnore
    public abstract void onSetWatchParamResponse(String str, boolean z, WatchParamsFileMapping watchParamsFileMapping);

    @DexIgnore
    public final boolean pairDeviceResponse(PairingResponse pairingResponse) {
        pq7.c(pairingResponse, "response");
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IPairDeviceSession) {
            if (pairingResponse instanceof PairingUpdateFWResponse) {
                ((IPairDeviceSession) currentSession).updateFirmware(((PairingUpdateFWResponse) pairingResponse).getFirmwareData());
            } else if (pairingResponse instanceof PairingLinkServerResponse) {
                PairingLinkServerResponse pairingLinkServerResponse = (PairingLinkServerResponse) pairingResponse;
                ((IPairDeviceSession) currentSession).onLinkServerSuccess(pairingLinkServerResponse.isSuccess(), pairingLinkServerResponse.getFailureCode());
            } else if (pairingResponse instanceof PairingAuthorizeResponse) {
                ((IPairDeviceSession) currentSession).onAuthorizeDevice(((PairingAuthorizeResponse) pairingResponse).getTimeOutDuration());
            } else if (pairingResponse instanceof LabelResponse) {
                ((IPairDeviceSession) currentSession).onDownloadLabelResponse((LabelResponse) pairingResponse);
            } else {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.TAG;
                local.d(str, ".pairDeviceResponse() FAILED, response=" + pairingResponse);
                IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                FLogger.Component component = FLogger.Component.BLE;
                FLogger.Session session = FLogger.Session.PAIR;
                String str2 = this.serial;
                String str3 = this.TAG;
                remote.i(component, session, str2, str3, "pairDeviceResponse FAILED, response=" + pairingResponse);
                return false;
            }
            return true;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str4 = this.TAG;
        local2.d(str4, ".pairDeviceResponse() FAILED, current session=" + currentSession);
        IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
        FLogger.Component component2 = FLogger.Component.BLE;
        FLogger.Session session2 = FLogger.Session.OTHER;
        String str5 = this.serial;
        String str6 = this.TAG;
        remote2.i(component2, session2, str5, str6, "pairDeviceResponse FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    public abstract ThemeData parseBinaryToThemeData(byte[] bArr);

    @DexIgnore
    public abstract boolean previewTheme(lw1 lw1, kw1 kw1);

    @DexIgnore
    public final void printQueue() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, "Inside " + this.TAG + ".printQueue - current session=" + getCurrentSession().getClass().getSimpleName());
        Iterator<BleSession> it = this.highSessionQueue.iterator();
        while (it.hasNext()) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local2.d(str2, "Inside " + this.TAG + ".printQueue - high session=" + it.next().getClass().getSimpleName());
        }
        Iterator<BleSession> it2 = this.lowSessionQueue.iterator();
        while (it2.hasNext()) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local3.d(str3, "Inside " + this.TAG + ".printQueue - low session=" + it2.next().getClass().getSimpleName());
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean queueSessionAndStart(com.misfit.frameworks.buttonservice.communite.ble.BleSession r12) {
        /*
        // Method dump skipped, instructions count: 1704
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator.queueSessionAndStart(com.misfit.frameworks.buttonservice.communite.ble.BleSession):boolean");
    }

    @DexIgnore
    public final void resetSettingFlagsToDefault() {
        Context context = getBleAdapter().getContext();
        List<AlarmSetting> autoListAlarm = DevicePreferenceUtils.getAutoListAlarm(context);
        String autoSecondTimezoneId = DevicePreferenceUtils.getAutoSecondTimezoneId(context);
        List<BLEMapping> autoMapping = DevicePreferenceUtils.getAutoMapping(context, this.serial);
        pq7.b(autoMapping, "DevicePreferenceUtils.ge\u2026oMapping(context, serial)");
        ComplicationAppMappingSettings autoComplicationAppSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(context, this.serial);
        WatchAppMappingSettings autoWatchAppSettings = DevicePreferenceUtils.getAutoWatchAppSettings(context, this.serial);
        LocalizationData autoLocalizationDataSettings = DevicePreferenceUtils.getAutoLocalizationDataSettings(context, this.serial);
        DevicePreferenceUtils.clearAllSettingFlag(context);
        if (!(autoListAlarm == null || !(!autoListAlarm.isEmpty()) || getBleAdapter().isSupportedFeature(as1.class) == null)) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.MULTI_ALARM);
        }
        if (!TextUtils.isEmpty(autoSecondTimezoneId) && getBleAdapter().isSupportedFeature(es1.class) != null) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.SECOND_TIMEZONE);
        }
        if (!autoMapping.isEmpty()) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.MAPPINGS);
        }
        if (!(autoComplicationAppSettings == null || getBleAdapter().isSupportedFeature(js1.class) == null)) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.COMPLICATION_APPS);
        }
        if (!(autoWatchAppSettings == null || getBleAdapter().isSupportedFeature(js1.class) == null)) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.WATCH_APPS);
        }
        if (autoLocalizationDataSettings != null && getBleAdapter().isSupportedFeature(hs1.class) != null) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.LOCALIZATION_DATA);
        }
    }

    @DexIgnore
    public abstract void sendCustomCommand(CustomRequest customRequest);

    @DexIgnore
    public abstract boolean sendingEncryptedDataSession(byte[] bArr, boolean z);

    @DexIgnore
    public final void setBleSessionCallback(BleSession.BleSessionCallback bleSessionCallback2) {
        pq7.c(bleSessionCallback2, "<set-?>");
        this.bleSessionCallback = bleSessionCallback2;
    }

    @DexIgnore
    public abstract void setCurrentSession(BleSession bleSession);

    @DexIgnore
    public final void setHighSessionQueue(PriorityBlockingQueue<BleSession> priorityBlockingQueue) {
        pq7.c(priorityBlockingQueue, "<set-?>");
        this.highSessionQueue = priorityBlockingQueue;
    }

    @DexIgnore
    public final void setLowSessionQueue(PriorityBlockingQueue<BleSession> priorityBlockingQueue) {
        pq7.c(priorityBlockingQueue, "<set-?>");
        this.lowSessionQueue = priorityBlockingQueue;
    }

    @DexIgnore
    public abstract boolean setMinimumStepThresholdSession(long j);

    @DexIgnore
    public abstract void setNullCurrentSession();

    @DexIgnore
    public abstract void setSecretKey(byte[] bArr);

    @DexIgnore
    public abstract boolean setWorkoutConfigSession(WorkoutConfigData workoutConfigData);

    @DexIgnore
    public abstract boolean setWorkoutGPSData(Location location);

    @DexIgnore
    public abstract boolean startCalibrationSession();

    @DexIgnore
    public abstract boolean startCleanLinkMappingSession(List<? extends BLEMapping> list);

    @DexIgnore
    public abstract boolean startConnectionDeviceSession(boolean z, UserProfile userProfile);

    @DexIgnore
    public abstract boolean startGetBatteryLevelSession();

    @DexIgnore
    public abstract boolean startGetRssiSession();

    @DexIgnore
    public abstract boolean startGetVibrationStrengthSession();

    @DexIgnore
    public abstract boolean startInstallWatchApps(boolean z);

    @DexIgnore
    public abstract boolean startNotifyNotificationEvent(NotificationBaseObj notificationBaseObj);

    @DexIgnore
    public abstract boolean startOtaSession(FirmwareData firmwareData, UserProfile userProfile);

    @DexIgnore
    public abstract boolean startPairingSession(UserProfile userProfile);

    @DexIgnore
    public abstract boolean startPlayAnimationSession();

    @DexIgnore
    public abstract boolean startReadCurrentWorkoutSession();

    @DexIgnore
    public abstract boolean startReadRealTimeStepSession();

    @DexIgnore
    public abstract void startSendDeviceAppResponse(DeviceAppResponse deviceAppResponse, boolean z);

    @DexIgnore
    public abstract void startSendMusicAppResponse(MusicResponse musicResponse);

    @DexIgnore
    public abstract boolean startSendNotification(NotificationBaseObj notificationBaseObj);

    @DexIgnore
    public final void startSessionInQueue() {
        synchronized (this) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.d(str, "Inside " + this.TAG + ".startSessionInQueue - Starting currentSession=" + getCurrentSession());
            startSessionInQueueProcess();
        }
    }

    @DexIgnore
    public abstract void startSessionInQueueProcess();

    @DexIgnore
    public abstract boolean startSetActivityGoals(int i, int i2, int i3);

    @DexIgnore
    public abstract boolean startSetAutoBackgroundImageConfig(BackgroundConfig backgroundConfig);

    @DexIgnore
    public abstract boolean startSetAutoBiometricData(UserBiometricData userBiometricData);

    @DexIgnore
    public abstract boolean startSetAutoMapping(List<? extends BLEMapping> list);

    @DexIgnore
    public abstract boolean startSetAutoMultiAlarms(List<AlarmSetting> list);

    @DexIgnore
    public abstract boolean startSetAutoNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings);

    @DexIgnore
    public abstract boolean startSetAutoSecondTimezone(String str);

    @DexIgnore
    public abstract boolean startSetAutoWatchApps(WatchAppMappingSettings watchAppMappingSettings);

    @DexIgnore
    public abstract boolean startSetBackgroundImageConfig(BackgroundConfig backgroundConfig);

    @DexIgnore
    public abstract boolean startSetFrontLightEnable(boolean z);

    @DexIgnore
    public abstract boolean startSetHeartRateMode(HeartRateMode heartRateMode);

    @DexIgnore
    public abstract boolean startSetImplicitDeviceConfig(UserProfile userProfile);

    @DexIgnore
    public abstract boolean startSetImplicitDisplayUnitSettings(UserDisplayUnit userDisplayUnit);

    @DexIgnore
    public abstract boolean startSetInactiveNudgeConfigSession(InactiveNudgeData inactiveNudgeData);

    @DexIgnore
    public abstract boolean startSetLinkMappingSession(List<? extends BLEMapping> list);

    @DexIgnore
    public abstract boolean startSetLocalizationData(LocalizationData localizationData);

    @DexIgnore
    public abstract boolean startSetMultipleAlarmsSession(List<AlarmSetting> list);

    @DexIgnore
    public abstract boolean startSetNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings);

    @DexIgnore
    public abstract boolean startSetReplyMessageMappingSettings(ReplyMessageMappingGroup replyMessageMappingGroup);

    @DexIgnore
    public abstract boolean startSetSecondTimezoneSession(String str);

    @DexIgnore
    public abstract boolean startSetVibrationStrengthSession(VibrationStrengthObj vibrationStrengthObj);

    @DexIgnore
    public abstract boolean startSetWatchApps(WatchAppMappingSettings watchAppMappingSettings);

    @DexIgnore
    public abstract boolean startSetWorkoutDetectionSetting(WorkoutDetectionSetting workoutDetectionSetting);

    @DexIgnore
    public abstract boolean startStopCurrentWorkoutSession();

    @DexIgnore
    public abstract boolean startSwitchDeviceSession(UserProfile userProfile);

    @DexIgnore
    public abstract boolean startSyncingSession(UserProfile userProfile);

    @DexIgnore
    public abstract boolean startUnlinkSession();

    @DexIgnore
    public abstract boolean startUpdateCurrentTime();

    @DexIgnore
    public abstract boolean startVerifySecretKeySession();

    @DexIgnore
    public final boolean switchDeviceResponse(boolean z, int i) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof ISwitchDeviceSession) {
            ((ISwitchDeviceSession) currentSession).onLinkServerSuccess(z, i);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, ".switchDeviceResponse() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String str2 = this.serial;
        String str3 = this.TAG;
        remote.i(component, session, str2, str3, "switchDeviceResponse FAILED, currentSession=" + currentSession);
        return false;
    }
}
