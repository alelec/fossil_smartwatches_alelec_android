package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.fitness.FitnessData;
import com.fossil.mp1;
import com.fossil.pq7;
import com.fossil.yk1;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleCommunicatorAbs$mStateCallback$Anon1 implements yk1.b {
    @DexIgnore
    public /* final */ /* synthetic */ BleCommunicatorAbs this$0;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public BleCommunicatorAbs$mStateCallback$Anon1(BleCommunicatorAbs bleCommunicatorAbs) {
        this.this$0 = bleCommunicatorAbs;
    }

    @DexIgnore
    @Override // com.fossil.yk1.b
    public void onAutoSyncFitnessDataReceived(FitnessData[] fitnessDataArr) {
        pq7.c(fitnessDataArr, "data");
    }

    @DexIgnore
    @Override // com.fossil.yk1.b
    public void onDeviceStateChanged(yk1 yk1, yk1.c cVar, yk1.c cVar2) {
        pq7.c(yk1, "device");
        pq7.c(cVar, "previousState");
        pq7.c(cVar2, "newState");
        this.this$0.handleDeviceStateChanged(yk1, cVar, cVar2);
    }

    @DexIgnore
    @Override // com.fossil.yk1.b
    public void onEventReceived(yk1 yk1, mp1 mp1) {
        pq7.c(yk1, "device");
        pq7.c(mp1, Constants.EVENT);
        this.this$0.handleEventReceived(yk1, mp1);
    }
}
