package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import android.os.Bundle;
import com.fossil.il7;
import com.fossil.lw1;
import com.fossil.oy1;
import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.ru1;
import com.fossil.ry1;
import com.fossil.tl7;
import com.fossil.tu1;
import com.fossil.us1;
import com.fossil.yx1;
import com.fossil.zm1;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeConfig;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.SharePreferencesUtils;
import com.misfit.frameworks.buttonservice.utils.ThemeExtentionKt;
import com.misfit.frameworks.buttonservice.utils.WatchAppUtils;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseTransferSettingsSubFlow extends SubFlow {
    @DexIgnore
    public /* final */ BackgroundConfig backgroundConfig;
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public /* final */ ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public /* final */ boolean isFullSync;
    @DexIgnore
    public /* final */ LocalizationData localizationData;
    @DexIgnore
    public /* final */ List<MicroAppMapping> microAppMappings;
    @DexIgnore
    public /* final */ List<AlarmSetting> multiAlarmSettings;
    @DexIgnore
    public /* final */ AppNotificationFilterSettings notificationFilterSettings;
    @DexIgnore
    public /* final */ int secondTimezoneOffset;
    @DexIgnore
    public /* final */ ThemeConfig themeConfig;
    @DexIgnore
    public /* final */ UserProfile userProfile;
    @DexIgnore
    public /* final */ WatchAppMappingSettings watchAppMappingSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetBackgroundImageConfig extends BleStateAbs {
        @DexIgnore
        public qy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetBackgroundImageConfig() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                BackgroundConfig backgroundConfig = BaseTransferSettingsSubFlow.this.getBackgroundConfig();
                if (backgroundConfig != null) {
                    qy1<tl7> backgroundImage = BaseTransferSettingsSubFlow.this.getBleAdapter().setBackgroundImage(BaseTransferSettingsSubFlow.this.getLogSession(), backgroundConfig, this);
                    this.task = backgroundImage;
                    if (backgroundImage == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
                    } else {
                        startTimeout();
                        obj = tl7.f3441a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip this step. No background image config");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Background Image Config step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetBackgroundConfig=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetBackgroundImageFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_BACKGROUND_IMAGE_CONFIG);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetBackgroundImageSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Background Image timeout. Cancel.");
            qy1<tl7> qy1 = this.task;
            if (qy1 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_BACKGROUND_IMAGE_CONFIG);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
            } else if (qy1 != null) {
                us1.a(qy1);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class SetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public qy1<zm1[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetDeviceConfigState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0174, code lost:
            if (r1 == null) goto L_0x0225;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x0190, code lost:
            if (r1 == null) goto L_0x024f;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private final com.fossil.ym1[] prepareConfigData() {
            /*
            // Method dump skipped, instructions count: 602
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetDeviceConfigState.prepareConfigData():com.fossil.ym1[]");
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            qy1<zm1[]> deviceConfig = BaseTransferSettingsSubFlow.this.getBleAdapter().setDeviceConfig(BaseTransferSettingsSubFlow.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                BaseTransferSettingsSubFlow.this.stopSubFlow(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_CONFIG);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LIST_ALARMS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LIST_ALARMS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Device Config timeout. Cancel.");
            qy1<zm1[]> qy1 = this.task;
            if (qy1 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_CONFIG);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LIST_ALARMS_STATE));
            } else if (qy1 != null) {
                us1.a(qy1);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetListAlarmsState extends BleStateAbs {
        @DexIgnore
        public qy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetListAlarmsState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MULTI_ALARM);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                List<AlarmSetting> multiAlarmSettings = BaseTransferSettingsSubFlow.this.getMultiAlarmSettings();
                if (multiAlarmSettings != null) {
                    qy1<tl7> alarms = BaseTransferSettingsSubFlow.this.getBleAdapter().setAlarms(BaseTransferSettingsSubFlow.this.getLogSession(), multiAlarmSettings, this);
                    this.task = alarms;
                    if (alarms == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APP_FILES));
                    } else {
                        startTimeout();
                        obj = tl7.f3441a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Alarm step. Alarm settings null");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MULTI_ALARM);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_WATCH_APP_FILES));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Alarm step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetMultiAlarmSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_WATCH_APP_FILES));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetAlarmFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_ALARM);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APP_FILES));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetAlarmSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MULTI_ALARM);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APP_FILES));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set List Alarms timeout. Cancel.");
            qy1<tl7> qy1 = this.task;
            if (qy1 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_ALARM);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APP_FILES));
            } else if (qy1 != null) {
                us1.a(qy1);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetLocalization extends BleStateAbs {
        @DexIgnore
        public oy1<String> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetLocalization() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            Object enterSubStateAsync;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.LOCALIZATION_DATA);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                LocalizationData localizationData = BaseTransferSettingsSubFlow.this.getLocalizationData();
                if (localizationData != null) {
                    if (localizationData.isDataValid()) {
                        oy1<String> localizationData2 = BaseTransferSettingsSubFlow.this.getBleAdapter().setLocalizationData(localizationData, BaseTransferSettingsSubFlow.this.getLogSession(), this);
                        this.task = localizationData2;
                        if (localizationData2 == null) {
                            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                            enterSubStateAsync = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
                        } else {
                            startTimeout();
                            enterSubStateAsync = tl7.f3441a;
                        }
                    } else {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                        baseTransferSettingsSubFlow2.log("Skip Set Localization step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetLocalizationSettings=" + isNeedToSetSetting);
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
                        enterSubStateAsync = baseTransferSettingsSubFlow3.enterSubStateAsync(baseTransferSettingsSubFlow3.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
                    }
                    if (enterSubStateAsync != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Localization step. No localization settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.LOCALIZATION_DATA);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow5 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow5.log("Skip Set Localization step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetLocalizationSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow6 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow6.enterSubStateAsync(baseTransferSettingsSubFlow6.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetLocalizationDataFail(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_LOCALIZATION_DATA);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetLocalizationDataSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.LOCALIZATION_DATA);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Localization timeout. Cancel.");
            oy1<String> oy1 = this.task;
            if (oy1 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_LOCALIZATION_DATA);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
            } else if (oy1 != null) {
                us1.a(oy1);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetMicroAppMappingState extends BleStateAbs {
        @DexIgnore
        public oy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetMicroAppMappingState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onConfigureMicroAppFail(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_MICRO_APP_MAPPING);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onConfigureMicroAppSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MAPPINGS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (BaseTransferSettingsSubFlow.this.getMicroAppMappings() == null || !(!BaseTransferSettingsSubFlow.this.getMicroAppMappings().isEmpty())) {
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE));
                return true;
            }
            BleAdapterImpl bleAdapter = BaseTransferSettingsSubFlow.this.getBleAdapter();
            FLogger.Session logSession = BaseTransferSettingsSubFlow.this.getLogSession();
            List<tu1> convertToSDKMapping = MicroAppMapping.convertToSDKMapping(BaseTransferSettingsSubFlow.this.getMicroAppMappings());
            pq7.b(convertToSDKMapping, "MicroAppMapping.convertT\u2026Mapping(microAppMappings)");
            oy1<tl7> configureMicroApp = bleAdapter.configureMicroApp(logSession, convertToSDKMapping, this);
            this.task = configureMicroApp;
            if (configureMicroApp == null) {
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE));
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Micro Apps timeout. Cancel.");
            oy1<tl7> oy1 = this.task;
            if (oy1 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_MICRO_APP_MAPPING);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE));
            } else if (oy1 != null) {
                us1.a(oy1);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetNotificationFilterSettings extends BleStateAbs {
        @DexIgnore
        public oy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetNotificationFilterSettings() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.NOTIFICATION_FILTERS);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                AppNotificationFilterSettings notificationFilterSettings = BaseTransferSettingsSubFlow.this.getNotificationFilterSettings();
                if (notificationFilterSettings != null) {
                    oy1<tl7> notificationFilters = BaseTransferSettingsSubFlow.this.getBleAdapter().setNotificationFilters(BaseTransferSettingsSubFlow.this.getLogSession(), notificationFilterSettings.getNotificationFilters(), this);
                    this.task = notificationFilters;
                    if (notificationFilters == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
                    } else {
                        startTimeout();
                        obj = tl7.f3441a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip this step. No notification filter settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.NOTIFICATION_FILTERS);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Notification Filter Settings step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetNotificationFilterSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetNotificationFilterFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetNotificationFilterProgressChanged(float f) {
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetNotificationFilterSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.NOTIFICATION_FILTERS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Notification Filter Settings timeout. Cancel.");
            oy1<tl7> oy1 = this.task;
            if (oy1 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
            } else if (oy1 != null) {
                us1.a(oy1);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetReplyMessageMappingState extends BleStateAbs {
        @DexIgnore
        public oy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetReplyMessageMappingState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (!FossilDeviceSerialPatternUtil.isDianaDevice(BaseTransferSettingsSubFlow.this.getSerial()) || BaseTransferSettingsSubFlow.this.getUserProfile() == null) {
                BaseTransferSettingsSubFlow.this.stopSubFlow(0);
                return true;
            }
            oy1<tl7> replyMessageMapping = BaseTransferSettingsSubFlow.this.getBleAdapter().setReplyMessageMapping(BaseTransferSettingsSubFlow.this.getLogSession(), BaseTransferSettingsSubFlow.this.getUserProfile().getReplyMessageMapping(), this);
            this.task = replyMessageMapping;
            if (replyMessageMapping == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(10000);
                BaseTransferSettingsSubFlow.this.stopSubFlow(0);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetReplyMessageMappingError(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.log("setReplyMessageMappingError");
            BaseTransferSettingsSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetReplyMessageMappingSuccess() {
            stopTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Reply Message Mapping success");
            BaseTransferSettingsSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            oy1<tl7> oy1 = this.task;
            if (oy1 == null) {
                BaseTransferSettingsSubFlow.this.stopSubFlow(0);
            } else if (oy1 != null) {
                us1.a(oy1);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetTheme extends BleStateAbs {
        @DexIgnore
        public qy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetTheme() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onApplyThemeError(yx1 yx1) {
            pq7.c(yx1, "error");
            super.onApplyThemeError(yx1);
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_BACKGROUND_IMAGE_CONFIG);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onApplyThemeSuccess(byte[] bArr) {
            pq7.c(bArr, "themeBinary");
            super.onApplyThemeSuccess(bArr);
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            Object obj;
            qy1<tl7> qy1 = null;
            super.onEnter();
            boolean needToSetTheme = DevicePreferenceUtils.needToSetTheme(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.THEME);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || needToSetTheme) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = getTAG();
                local.e(tag, "SetTheme - themeConfig: " + BaseTransferSettingsSubFlow.this.getThemeConfig());
                ThemeConfig themeConfig = BaseTransferSettingsSubFlow.this.getThemeConfig();
                if (themeConfig != null) {
                    if (themeConfig.getPathThemeData() != null) {
                        lw1 themePackage = ThemeExtentionKt.toThemePackage(themeConfig.getPathThemeData());
                        if (themePackage != null) {
                            qy1<lw1> applyTheme = BaseTransferSettingsSubFlow.this.getBleAdapter().applyTheme(BaseTransferSettingsSubFlow.this.getLogSession(), themePackage, this);
                            qy1 = applyTheme;
                            if (applyTheme == null) {
                                throw new il7("null cannot be cast to non-null type com.fossil.common.task.Task<kotlin.Unit>");
                            }
                        }
                    } else {
                        BleAdapterImpl bleAdapter = BaseTransferSettingsSubFlow.this.getBleAdapter();
                        FLogger.Session logSession = BaseTransferSettingsSubFlow.this.getLogSession();
                        ThemeData themeData = themeConfig.getThemeData();
                        if (themeData != null) {
                            qy1<lw1> applyTheme2 = bleAdapter.applyTheme(logSession, themeData, this);
                            qy1 = applyTheme2;
                            if (applyTheme2 == null) {
                                throw new il7("null cannot be cast to non-null type com.fossil.common.task.Task<kotlin.Unit>");
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                    this.task = qy1;
                    if (qy1 == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
                    } else {
                        startTimeout();
                        obj = tl7.f3441a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip this step. No theme data");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Theme. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetBackgroundConfig=" + needToSetTheme);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Theme timeout. Cancel.");
            qy1<tl7> qy1 = this.task;
            if (qy1 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_APPLY_THEME);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
            } else if (qy1 != null) {
                us1.a(qy1);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchAppFiles extends BleStateAbs {
        @DexIgnore
        public oy1<ru1[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWatchAppFiles() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        private final Bundle bundleUiVersion(ry1 ry1) {
            Bundle bundle = new Bundle();
            bundle.putInt(ButtonService.Companion.getWATCH_APP_MAJOR_VERSION(), ry1.getMajor());
            bundle.putInt(ButtonService.Companion.getWATCH_APP_MINOR_VERSION(), ry1.getMinor());
            return bundle;
        }

        @DexIgnore
        private final ru1[] prepareWatchAppFiles() {
            return WatchAppUtils.INSTANCE.getWatchAppFiles(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext());
        }

        /* JADX WARNING: Code restructure failed: missing block: B:7:0x0022, code lost:
            if ((r3.length == 0) != false) goto L_0x0024;
         */
        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onEnter() {
            /*
                r5 = this;
                r0 = 0
                r1 = 1
                super.onEnter()
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.String r3 = "Enter Set Watch App Files"
                r2.log(r3)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.String r2 = r2.getSerial()
                boolean r2 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(r2)
                if (r2 == 0) goto L_0x00b7
                com.fossil.ru1[] r3 = r5.prepareWatchAppFiles()
                if (r3 == 0) goto L_0x0024
                int r2 = r3.length
                if (r2 != 0) goto L_0x0094
                r2 = r1
            L_0x0022:
                if (r2 == 0) goto L_0x0025
            L_0x0024:
                r0 = r1
            L_0x0025:
                if (r0 == 0) goto L_0x00b3
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r2 = r5.getTAG()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "Start fetch watchAppData from network, version "
                r3.append(r4)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r4 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl r4 = r4.getBleAdapter()
                com.fossil.ry1 r4 = r4.getUiPackageOSVersion()
                r3.append(r4)
                java.lang.String r4 = " callback "
                r3.append(r4)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r4 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.BleSession$BleSessionCallback r4 = r4.getBleSessionCallback()
                r3.append(r4)
                java.lang.String r3 = r3.toString()
                r0.d(r2, r3)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl r0 = r0.getBleAdapter()
                com.fossil.ry1 r0 = r0.getUiPackageOSVersion()
                if (r0 == 0) goto L_0x00a7
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl r0 = r0.getBleAdapter()
                com.fossil.ry1 r0 = r0.getUiPackageOSVersion()
                if (r0 == 0) goto L_0x00a2
                android.os.Bundle r0 = r5.bundleUiVersion(r0)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.BleSession$BleSessionCallback r2 = r2.getBleSessionCallback()
                if (r2 == 0) goto L_0x0096
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.BleSession$BleSessionCallback r2 = r2.getBleSessionCallback()
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r3 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.String r3 = r3.getSerial()
                r2.onAskForWatchAppFiles(r3, r0)
                r5.startTimeout()
            L_0x0093:
                return r1
            L_0x0094:
                r2 = r0
                goto L_0x0022
            L_0x0096:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$SessionState r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow.SessionState.SET_WATCH_APPS_STATE
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs r2 = r0.createConcreteState(r2)
                r0.enterSubStateAsync(r2)
                goto L_0x0093
            L_0x00a2:
                com.fossil.pq7.i()
                r0 = 0
                throw r0
            L_0x00a7:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$SessionState r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow.SessionState.SET_WATCH_APPS_STATE
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs r2 = r0.createConcreteState(r2)
                r0.enterSubStateAsync(r2)
                goto L_0x0093
            L_0x00b3:
                r5.setWatchAppFilesToDevice(r3)
                goto L_0x0093
            L_0x00b7:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$SessionState r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow.SessionState.SET_WATCH_APPS_STATE
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs r2 = r0.createConcreteState(r2)
                r0.enterSubStateAsync(r2)
                goto L_0x0093
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetWatchAppFiles.onEnter():boolean");
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFileFailed() {
            stopTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch App Files Failed");
            SharePreferencesUtils instance = SharePreferencesUtils.getInstance(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext());
            instance.setBoolean(SharePreferencesUtils.BC_APP_READY + BaseTransferSettingsSubFlow.this.getSerial(), false);
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_APP_FILES);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFileProgressChanged(float f) {
            setTimeout(30000);
            startTimeout();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFileSuccess() {
            stopTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch App Files success");
            SharePreferencesUtils instance = SharePreferencesUtils.getInstance(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext());
            Boolean bool = instance.getBoolean(SharePreferencesUtils.BC_STATUS, false);
            pq7.b(bool, "isBCOn");
            if (bool.booleanValue()) {
                instance.setBoolean(SharePreferencesUtils.BC_APP_READY + BaseTransferSettingsSubFlow.this.getSerial(), true);
            } else {
                instance.setBoolean(SharePreferencesUtils.BC_APP_READY + BaseTransferSettingsSubFlow.this.getSerial(), false);
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch App Files Timeout");
            oy1<ru1[]> oy1 = this.task;
            if (oy1 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_APP_FILES);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
            } else if (oy1 != null) {
                us1.a(oy1);
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0049  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0051  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void resumeSetWatchAppFilesState(boolean r7) {
            /*
                r6 = this;
                r2 = 1
                r1 = 0
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "resumeSetWatchAppFilesState, isSuccess = "
                r3.append(r4)
                r3.append(r7)
                java.lang.String r3 = r3.toString()
                r0.log(r3)
                r6.stopTimeout()
                if (r7 == 0) goto L_0x005d
                com.fossil.ru1[] r3 = r6.prepareWatchAppFiles()
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r4 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r0 = "resumeSetWatchAppFilesState, watchAppFile size after downloaded "
                r5.append(r0)
                if (r3 == 0) goto L_0x004d
                int r0 = r3.length
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            L_0x0034:
                r5.append(r0)
                java.lang.String r0 = r5.toString()
                r4.log(r0)
                if (r3 == 0) goto L_0x0046
                int r0 = r3.length
                if (r0 != 0) goto L_0x004f
                r0 = r2
            L_0x0044:
                if (r0 == 0) goto L_0x0069
            L_0x0046:
                r0 = r2
            L_0x0047:
                if (r0 != 0) goto L_0x0051
                r6.setWatchAppFilesToDevice(r3)
            L_0x004c:
                return
            L_0x004d:
                r0 = 0
                goto L_0x0034
            L_0x004f:
                r0 = r1
                goto L_0x0044
            L_0x0051:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$SessionState r1 = com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow.SessionState.SET_WATCH_APPS_STATE
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs r1 = r0.createConcreteState(r1)
                r0.enterSubStateAsync(r1)
                goto L_0x004c
            L_0x005d:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$SessionState r1 = com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow.SessionState.SET_WATCH_APPS_STATE
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs r1 = r0.createConcreteState(r1)
                r0.enterSubStateAsync(r1)
                goto L_0x004c
            L_0x0069:
                r0 = r1
                goto L_0x0047
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetWatchAppFiles.resumeSetWatchAppFilesState(boolean):void");
        }

        @DexIgnore
        public final void setWatchAppFilesToDevice(ru1[] ru1Arr) {
            pq7.c(ru1Arr, "watchAppFiles");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "setWatchAppFilesToDevice, size = " + ru1Arr.length);
            oy1<ru1[]> watchAppFiles = BaseTransferSettingsSubFlow.this.getBleAdapter().setWatchAppFiles(BaseTransferSettingsSubFlow.this.getLogSession(), ru1Arr, this);
            this.task = watchAppFiles;
            if (watchAppFiles == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(10000);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
                return;
            }
            startTimeout();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchApps extends BleStateAbs {
        @DexIgnore
        public qy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWatchApps() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.WATCH_APPS);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                WatchAppMappingSettings watchAppMappingSettings = BaseTransferSettingsSubFlow.this.getWatchAppMappingSettings();
                if (watchAppMappingSettings != null) {
                    qy1<tl7> watchApps = BaseTransferSettingsSubFlow.this.getBleAdapter().setWatchApps(BaseTransferSettingsSubFlow.this.getLogSession(), watchAppMappingSettings, this);
                    this.task = watchApps;
                    if (watchApps == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_THEME_STATE));
                    } else {
                        startTimeout();
                        obj = tl7.f3441a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Watch App step. No watch apps settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.WATCH_APPS);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_THEME_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Watch App step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetComplicationAppSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_THEME_STATE));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_APPS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_THEME_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.WATCH_APPS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_THEME_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch Apps timeout. Cancel.");
            qy1<tl7> qy1 = this.task;
            if (qy1 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_APPS);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_THEME_STATE));
            } else if (qy1 != null) {
                us1.a(qy1);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchParamsState extends BleStateAbs {
        @DexIgnore
        public qy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWatchParamsState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        private final Bundle exportFirmwareVersion() {
            Bundle bundle = new Bundle();
            bundle.putInt(ButtonService.WATCH_PARAMS_MAJOR, BaseTransferSettingsSubFlow.this.getBleAdapter().getSupportedWatchParamMajor());
            bundle.putInt(ButtonService.WATCH_PARAMS_MINOR, BaseTransferSettingsSubFlow.this.getBleAdapter().getSupportedWatchParamMinor());
            bundle.putFloat(ButtonService.CURRENT_WATCH_PARAMS_VERSION, Float.parseFloat(BaseTransferSettingsSubFlow.this.getBleAdapter().getCurrentWatchParamVersion()));
            return bundle;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            startTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "supportedWatchParamVersion = " + BaseTransferSettingsSubFlow.this.getBleAdapter().getSupportedWatchParamVersion() + ", currentWatchParamVersion=" + BaseTransferSettingsSubFlow.this.getBleAdapter().getCurrentWatchParamVersion());
            BleSession.BleSessionCallback bleSessionCallback = BaseTransferSettingsSubFlow.this.getBleSessionCallback();
            if (bleSessionCallback == null) {
                return true;
            }
            bleSessionCallback.onRequestLatestWatchParams(BaseTransferSettingsSubFlow.this.getSerial(), exportFirmwareVersion());
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetWatchParamsFail() {
            stopTimeout();
            if (!retry(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), BaseTransferSettingsSubFlow.this.getSerial())) {
                BaseTransferSettingsSubFlow.this.log("Reach the limit retry. Stop.");
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
                BaseTransferSettingsSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchParamsFail(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch Param failed");
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
            BaseTransferSettingsSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchParamsSuccess() {
            BaseTransferSettingsSubFlow.this.log("Set Watch Param success");
            stopTimeout();
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_DEVICE_CONFIG_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch Params timeout. Cancel.");
            qy1<tl7> qy1 = this.task;
            if (qy1 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
                BaseTransferSettingsSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
            } else if (qy1 != null) {
                us1.a(qy1);
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        public final void setWatchParamToDevice(String str, WatchParamsFileMapping watchParamsFileMapping) {
            pq7.c(str, "serial");
            pq7.c(watchParamsFileMapping, "watchParamsFileMapping");
            BaseTransferSettingsSubFlow.this.log("Set WatchParam to device");
            qy1<tl7> watchParams = BaseTransferSettingsSubFlow.this.getBleAdapter().setWatchParams(BaseTransferSettingsSubFlow.this.getLogSession(), watchParamsFileMapping, this);
            this.task = watchParams;
            if (watchParams == null) {
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_DEVICE_CONFIG_STATE));
                return;
            }
            startTimeout();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r24v0, resolved type: java.util.List<? extends com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseTransferSettingsSubFlow(String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, boolean z, String str2, BleAdapterImpl bleAdapterImpl, UserProfile userProfile2, List<AlarmSetting> list, ComplicationAppMappingSettings complicationAppMappingSettings2, WatchAppMappingSettings watchAppMappingSettings2, BackgroundConfig backgroundConfig2, ThemeConfig themeConfig2, AppNotificationFilterSettings appNotificationFilterSettings, LocalizationData localizationData2, List<? extends MicroAppMapping> list2, int i, BleSession.BleSessionCallback bleSessionCallback2) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        pq7.c(str, "tagName");
        pq7.c(bleSession, "bleSession");
        pq7.c(session, "logSession");
        pq7.c(str2, "serial");
        pq7.c(bleAdapterImpl, "mBleAdapterV2");
        this.isFullSync = z;
        this.userProfile = userProfile2;
        this.multiAlarmSettings = list;
        this.complicationAppMappingSettings = complicationAppMappingSettings2;
        this.watchAppMappingSettings = watchAppMappingSettings2;
        this.backgroundConfig = backgroundConfig2;
        this.themeConfig = themeConfig2;
        this.notificationFilterSettings = appNotificationFilterSettings;
        this.localizationData = localizationData2;
        this.microAppMappings = list2;
        this.secondTimezoneOffset = i;
        this.bleSessionCallback = bleSessionCallback2;
    }

    @DexIgnore
    public final void doNextState() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState");
        if (getMCurrentState() instanceof SetWatchParamsState) {
            enterSubStateAsync(createConcreteState(SubFlow.SessionState.SET_DEVICE_CONFIG_STATE));
        } else {
            FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState, failed because the current sub state is not an instance of SetWatchParamsState");
        }
    }

    @DexIgnore
    public final BackgroundConfig getBackgroundConfig() {
        return this.backgroundConfig;
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final ComplicationAppMappingSettings getComplicationAppMappingSettings() {
        return this.complicationAppMappingSettings;
    }

    @DexIgnore
    public final LocalizationData getLocalizationData() {
        return this.localizationData;
    }

    @DexIgnore
    public final List<MicroAppMapping> getMicroAppMappings() {
        return this.microAppMappings;
    }

    @DexIgnore
    public final List<AlarmSetting> getMultiAlarmSettings() {
        return this.multiAlarmSettings;
    }

    @DexIgnore
    public final AppNotificationFilterSettings getNotificationFilterSettings() {
        return this.notificationFilterSettings;
    }

    @DexIgnore
    public final int getSecondTimezoneOffset() {
        return this.secondTimezoneOffset;
    }

    @DexIgnore
    public final ThemeConfig getThemeConfig() {
        return this.themeConfig;
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    public final WatchAppMappingSettings getWatchAppMappingSettings() {
        return this.watchAppMappingSettings;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.SET_WATCH_PARAMS;
        String name = SetWatchParamsState.class.getName();
        pq7.b(name, "SetWatchParamsState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.SET_DEVICE_CONFIG_STATE;
        String name2 = SetDeviceConfigState.class.getName();
        pq7.b(name2, "SetDeviceConfigState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<SubFlow.SessionState, String> sessionStateMap3 = getSessionStateMap();
        SubFlow.SessionState sessionState3 = SubFlow.SessionState.SET_LIST_ALARMS_STATE;
        String name3 = SetListAlarmsState.class.getName();
        pq7.b(name3, "SetListAlarmsState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<SubFlow.SessionState, String> sessionStateMap4 = getSessionStateMap();
        SubFlow.SessionState sessionState4 = SubFlow.SessionState.SET_WATCH_APP_FILES;
        String name4 = SetWatchAppFiles.class.getName();
        pq7.b(name4, "SetWatchAppFiles::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<SubFlow.SessionState, String> sessionStateMap5 = getSessionStateMap();
        SubFlow.SessionState sessionState5 = SubFlow.SessionState.SET_WATCH_APPS_STATE;
        String name5 = SetWatchApps.class.getName();
        pq7.b(name5, "SetWatchApps::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
        HashMap<SubFlow.SessionState, String> sessionStateMap6 = getSessionStateMap();
        SubFlow.SessionState sessionState6 = SubFlow.SessionState.SET_THEME_STATE;
        String name6 = SetTheme.class.getName();
        pq7.b(name6, "SetTheme::class.java.name");
        sessionStateMap6.put(sessionState6, name6);
        HashMap<SubFlow.SessionState, String> sessionStateMap7 = getSessionStateMap();
        SubFlow.SessionState sessionState7 = SubFlow.SessionState.SET_LOCALIZATION_STATE;
        String name7 = SetLocalization.class.getName();
        pq7.b(name7, "SetLocalization::class.java.name");
        sessionStateMap7.put(sessionState7, name7);
        HashMap<SubFlow.SessionState, String> sessionStateMap8 = getSessionStateMap();
        SubFlow.SessionState sessionState8 = SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE;
        String name8 = SetNotificationFilterSettings.class.getName();
        pq7.b(name8, "SetNotificationFilterSettings::class.java.name");
        sessionStateMap8.put(sessionState8, name8);
        HashMap<SubFlow.SessionState, String> sessionStateMap9 = getSessionStateMap();
        SubFlow.SessionState sessionState9 = SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE;
        String name9 = SetMicroAppMappingState.class.getName();
        pq7.b(name9, "SetMicroAppMappingState::class.java.name");
        sessionStateMap9.put(sessionState9, name9);
        HashMap<SubFlow.SessionState, String> sessionStateMap10 = getSessionStateMap();
        SubFlow.SessionState sessionState10 = SubFlow.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE;
        String name10 = SetReplyMessageMappingState.class.getName();
        pq7.b(name10, "SetReplyMessageMappingState::class.java.name");
        sessionStateMap10.put(sessionState10, name10);
    }

    @DexIgnore
    public final boolean isFullSync() {
        return this.isFullSync;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.SET_WATCH_PARAMS));
        return true;
    }

    @DexIgnore
    public final void onGetWatchParamFailed() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "getWatchParamFailed");
        if (getMCurrentState() instanceof SetWatchParamsState) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((SetWatchParamsState) mCurrentState).onGetWatchParamsFail();
                return;
            }
            throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetWatchParamsState");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, failed because the current sub state is not an instance of SetWatchParamsState");
    }

    @DexIgnore
    public final void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        pq7.c(str, "serial");
        pq7.c(watchParamsFileMapping, "watchParamsData");
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam...");
        if (getMCurrentState() instanceof SetWatchParamsState) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((SetWatchParamsState) mCurrentState).setWatchParamToDevice(str, watchParamsFileMapping);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetWatchParamsState");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, failed because the current sub state is not an instance of SetWatchParamsState");
    }

    @DexIgnore
    public final void setWatchAppFiles(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setWatchAppFiles, isSuccess = " + z);
        if (getMCurrentState() instanceof SetWatchAppFiles) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((SetWatchAppFiles) mCurrentState).resumeSetWatchAppFilesState(z);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetWatchAppFiles");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setWatchAppFiles FAILED. It is not in SetWatchAppFilesSession");
    }
}
