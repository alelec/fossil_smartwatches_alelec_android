package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.ln1;
import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.us1;
import com.fossil.ym1;
import com.fossil.yx1;
import com.fossil.zm1;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetActivityGoalSession extends EnableMaintainingSession {
    @DexIgnore
    public /* final */ int activeTimeGoal;
    @DexIgnore
    public /* final */ int caloriesGoal;
    @DexIgnore
    public /* final */ int stepGoal;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetActivityGoalsState extends BleStateAbs {
        @DexIgnore
        public qy1<zm1[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetActivityGoalsState() {
            super(SetActivityGoalSession.this.getTAG());
        }

        @DexIgnore
        private final ym1[] prepareConfigData() {
            ln1 ln1 = new ln1();
            ln1.l((long) SetActivityGoalSession.this.stepGoal);
            ln1.h((long) SetActivityGoalSession.this.caloriesGoal);
            ln1.f(SetActivityGoalSession.this.activeTimeGoal);
            return ln1.b();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            SetActivityGoalSession setActivityGoalSession = SetActivityGoalSession.this;
            setActivityGoalSession.log("Set Activity Goal: stepGoal=" + SetActivityGoalSession.this.stepGoal + ", caloriesGoal=" + SetActivityGoalSession.this.caloriesGoal + ", activeTimeGoal=" + SetActivityGoalSession.this.activeTimeGoal);
            qy1<zm1[]> deviceConfig = SetActivityGoalSession.this.getBleAdapter().setDeviceConfig(SetActivityGoalSession.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetActivityGoalSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            if (!retry(SetActivityGoalSession.this.getContext(), SetActivityGoalSession.this.getSerial())) {
                SetActivityGoalSession.this.log("Reach the limit retry. Stop.");
                SetActivityGoalSession.this.stop(FailureCode.FAILED_TO_SET_STEP_GOAL);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetActivityGoalSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            qy1<zm1[]> qy1 = this.task;
            if (qy1 != null) {
                us1.a(qy1);
            }
            SetActivityGoalSession.this.stop(FailureCode.FAILED_TO_SET_STEP_GOAL);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetActivityGoalSession(int i, int i2, int i3, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.URGENT, CommunicateMode.SET_STEP_GOAL, bleAdapterImpl, bleSessionCallback);
        pq7.c(bleAdapterImpl, "bleAdapter");
        this.stepGoal = i;
        this.caloriesGoal = i2;
        this.activeTimeGoal = i3;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetActivityGoalSession setActivityGoalSession = new SetActivityGoalSession(this.stepGoal, this.caloriesGoal, this.activeTimeGoal, getBleAdapter(), getBleSessionCallback());
        setActivityGoalSession.setDevice(getDevice());
        return setActivityGoalSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.SET_ACTIVITY_GOALS_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        super.initSettings();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_ACTIVITY_GOALS_STATE;
        String name = SetActivityGoalsState.class.getName();
        pq7.b(name, "SetActivityGoalsState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
