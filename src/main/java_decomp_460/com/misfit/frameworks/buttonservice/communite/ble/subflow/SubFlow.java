package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import com.fossil.bw7;
import com.fossil.fitness.FitnessData;
import com.fossil.gu7;
import com.fossil.il7;
import com.fossil.jv7;
import com.fossil.lp1;
import com.fossil.pq7;
import com.fossil.uk1;
import com.fossil.vt7;
import com.fossil.xw7;
import com.fossil.yk1;
import com.fossil.ym1;
import com.fossil.yx1;
import com.fossil.zk1;
import com.fossil.zm1;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.NullBleState;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import java.lang.reflect.Constructor;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class SubFlow extends BleStateAbs implements ISessionSdkCallback {
    @DexIgnore
    public /* final */ BleAdapterImpl bleAdapter;
    @DexIgnore
    public /* final */ BleSession bleSession;
    @DexIgnore
    public /* final */ FLogger.Session logSession;
    @DexIgnore
    public BleStateAbs mCurrentState; // = new NullBleState(getTAG());
    @DexIgnore
    public /* final */ MFLog mfLog;
    @DexIgnore
    public /* final */ String serial;
    @DexIgnore
    public HashMap<SessionState, String> sessionStateMap;

    @DexIgnore
    public enum SessionState {
        SCANNING_STATE,
        ENABLE_MAINTAINING_CONNECTION_STATE,
        FETCH_DEVICE_INFO_STATE,
        GET_DEVICE_CONFIG_STATE,
        PLAY_DEVICE_ANIMATION_STATE,
        ERASE_DATA_FILE_STATE,
        DONE_STATE,
        OTA_STATE,
        SET_DEVICE_CONFIG_STATE,
        SET_WATCH_PARAMS,
        READ_OR_ERASE_STATE,
        READ_DATA_FILE_STATE,
        PROCESS_AND_STORE_DATA_STATE,
        SET_COMPLICATIONS_STATE,
        SET_WATCH_APPS_STATE,
        SET_WATCH_APP_FILES,
        SET_BACKGROUND_IMAGE_CONFIG_STATE,
        SET_LOCALIZATION_STATE,
        SET_THEME_STATE,
        SET_MICRO_APP_MAPPING,
        CLOSE_CONNECTION_STATE,
        REQUEST_HAND_CONTROL_STATE,
        RESET_HANDS_STATE,
        MOVE_HAND_STATE,
        APPLY_HAND_STATE,
        RELEASE_HAND_CONTROL_STATE,
        SET_STEP_GOAL_STATE,
        READ_RSSI_STATE,
        READ_REAL_TIME_STEPS_STATE,
        UPDATE_CURRENT_TIME_STATE,
        GET_BATTERY_LEVEL_STATE,
        SET_VIBRATION_STRENGTH_STATE,
        GET_VIBRATION_STRENGTH_STATE,
        SET_LIST_ALARMS_STATE,
        SET_BIOMETRIC_DATA_STATE,
        SET_SETTING_DONE_STATE,
        SET_HEART_RATE_MODE_STATE,
        SET_FRONT_LIGHT_ENABLE_STATE,
        READ_CURRENT_WORKOUT_STATE,
        STOP_CURRENT_WORKOUT_STATE,
        SET_NOTIFICATION_FILTERS_STATE,
        VERIFY_SECRET_KEY,
        GENERATE_RANDOM_KEY,
        GET_SECRET_KEY_THROUGH_SDK,
        AUTHENTICATE_DEVICE,
        EXCHANGE_SECRET_KEY,
        PUSH_SECRET_KEY_TO_CLOUD,
        SET_SECRET_KEY_TO_DEVICE,
        PROCESS_MAPPING,
        SET_MICRO_APP_MAPPING_AFTER_OTA_STATE,
        SET_REPLY_MESSAGE_MAPPING_STATE,
        PROCESS_HID
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SubFlow(String str, BleSession bleSession2, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl) {
        super(str);
        pq7.c(str, "tagName");
        pq7.c(bleSession2, "bleSession");
        pq7.c(session, "logSession");
        pq7.c(str2, "serial");
        pq7.c(bleAdapterImpl, "bleAdapter");
        this.bleSession = bleSession2;
        this.mfLog = mFLog;
        this.logSession = session;
        this.serial = str2;
        this.bleAdapter = bleAdapterImpl;
        initMap();
    }

    @DexIgnore
    private final boolean enterSubState(BleStateAbs bleStateAbs) {
        if (!isExist()) {
            return false;
        }
        if (!BleState.Companion.isNull(this.mCurrentState) && !BleState.Companion.isNull(bleStateAbs) && vt7.j(this.mCurrentState.getClass().getName(), bleStateAbs.getClass().getName(), true)) {
            return true;
        }
        if (!BleState.Companion.isNull(this.mCurrentState)) {
            this.mCurrentState.onExit();
        }
        this.mCurrentState = bleStateAbs;
        if (BleState.Companion.isNull(bleStateAbs)) {
            return false;
        }
        boolean onEnter = bleStateAbs.onEnter();
        if (onEnter) {
            return onEnter;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.e(tag, "Failed to enter state: " + bleStateAbs);
        this.mCurrentState = new NullBleState(getTAG());
        return onEnter;
    }

    @DexIgnore
    private final void initMap() {
        this.sessionStateMap = new HashMap<>();
        initStateMap();
    }

    @DexIgnore
    public final void addFailureCode(int i) {
        this.bleSession.addFailureCode(i);
    }

    @DexIgnore
    public final BleStateAbs createConcreteState(SessionState sessionState) {
        BleStateAbs bleStateAbs;
        pq7.c(sessionState, "state");
        HashMap<SessionState, String> hashMap = this.sessionStateMap;
        if (hashMap != null) {
            String str = hashMap.get(sessionState);
            if (str != null) {
                try {
                    Class<?> cls = Class.forName(str);
                    pq7.b(cls, "Class.forName(stateClassName!!)");
                    Constructor<?> declaredConstructor = cls.getDeclaredConstructor(cls.getDeclaringClass());
                    pq7.b(declaredConstructor, "innerClass.getDeclaredConstructor(parentClass)");
                    declaredConstructor.setAccessible(true);
                    Object newInstance = declaredConstructor.newInstance(this);
                    if (newInstance != null) {
                        bleStateAbs = (BleStateAbs) newInstance;
                        return bleStateAbs != null ? bleStateAbs : new NullBleState(getTAG());
                    }
                    throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs");
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String tag = getTAG();
                    local.e(tag, "Inside getState method, cannot instance state " + str + ", e = " + e);
                    bleStateAbs = null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("sessionStateMap");
            throw null;
        }
    }

    @DexIgnore
    public final xw7 enterSubStateAsync(BleStateAbs bleStateAbs) {
        pq7.c(bleStateAbs, "newState");
        return gu7.d(jv7.a(bw7.a()), null, null, new SubFlow$enterSubStateAsync$Anon1(this, bleStateAbs, null), 3, null);
    }

    @DexIgnore
    public final void errorLog(String str, ErrorCodeBuilder.Step step, yx1 yx1) {
        pq7.c(str, "message");
        pq7.c(step, "step");
        pq7.c(yx1, "sdkError");
        String build = ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, yx1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.e(tag, str + ", error=" + build);
        MFLog mFLog = this.mfLog;
        if (mFLog != null) {
            mFLog.log('[' + this.serial + "] " + str + " , error=" + build);
        }
        FLogger.INSTANCE.getRemote().e(FLogger.Component.BLE, this.logSession, this.serial, getTAG(), build, step, str);
    }

    @DexIgnore
    public final void errorLog(String str, FLogger.Component component, ErrorCodeBuilder.Step step, ErrorCodeBuilder.AppError appError) {
        pq7.c(str, "message");
        pq7.c(component, "component");
        pq7.c(step, "step");
        pq7.c(appError, "error");
        String build = ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.APP, appError);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.e(tag, str + ", error=" + build);
        MFLog mFLog = this.mfLog;
        if (mFLog != null) {
            mFLog.log('[' + this.serial + "] " + str + " , error=" + build);
        }
        FLogger.INSTANCE.getRemote().e(component, this.logSession, this.serial, getTAG(), build, step, str);
    }

    @DexIgnore
    public final BleAdapterImpl getBleAdapter() {
        return this.bleAdapter;
    }

    @DexIgnore
    public final BleSession getBleSession() {
        return this.bleSession;
    }

    @DexIgnore
    public final FLogger.Session getLogSession() {
        return this.logSession;
    }

    @DexIgnore
    public final BleStateAbs getMCurrentState() {
        return this.mCurrentState;
    }

    @DexIgnore
    public final MFLog getMfLog() {
        return this.mfLog;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public final HashMap<SessionState, String> getSessionStateMap() {
        HashMap<SessionState, String> hashMap = this.sessionStateMap;
        if (hashMap != null) {
            return hashMap;
        }
        pq7.n("sessionStateMap");
        throw null;
    }

    @DexIgnore
    public abstract void initStateMap();

    @DexIgnore
    public void log(String str) {
        pq7.c(str, "message");
        FLogger.INSTANCE.getLocal().d(getTAG(), str);
        MFLog mFLog = this.mfLog;
        if (mFLog != null) {
            mFLog.log('[' + this.serial + "] " + str);
        }
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, this.logSession, this.serial, getTAG(), str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onApplyHandPositionFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onApplyHandPositionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onDataTransferCompleted() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onDataTransferFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onDataTransferProgressChange(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onDeviceFound(yk1 yk1, int i) {
        pq7.c(yk1, "device");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onEraseDataFilesFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onEraseDataFilesSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onEraseHWLogFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onEraseHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
    public void onExit() {
        if (!BleState.Companion.isNull(this.mCurrentState)) {
            this.mCurrentState.stopTimeout();
            enterSubState(new NullBleState(getTAG()));
        }
        super.onExit();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onFetchDeviceInfoFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onFetchDeviceInfoSuccess(zk1 zk1) {
        pq7.c(zk1, "deviceInformation");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onGetDeviceConfigFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onGetDeviceConfigSuccess(HashMap<zm1, ym1> hashMap) {
        pq7.c(hashMap, "deviceConfiguration");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onMoveHandFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onMoveHandSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onNotifyNotificationEventFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onNotifyNotificationEventSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onPlayDeviceAnimation(boolean z, yx1 yx1) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadCurrentWorkoutSessionFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadCurrentWorkoutSessionSuccess(lp1 lp1) {
        pq7.c(lp1, "workoutSession");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadDataFilesFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadDataFilesProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadDataFilesSuccess(FitnessData[] fitnessDataArr) {
        pq7.c(fitnessDataArr, "data");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadHWLogFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadHWLogProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadRssiFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadRssiSuccess(int i) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReleaseHandControlFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReleaseHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onRequestHandControlFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onRequestHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onResetHandsFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onResetHandsSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onScanFail(uk1 uk1) {
        pq7.c(uk1, "scanError");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetAlarmFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetAlarmSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetBackgroundImageFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetBackgroundImageSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetComplicationFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetComplicationSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetDeviceConfigFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetDeviceConfigSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetFrontLightFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetFrontLightSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetWatchAppFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetWatchAppFileProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetWatchAppSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public abstract void onStop(int i);

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onStopCurrentWorkoutSessionFailed(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onStopCurrentWorkoutSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onVerifySecretKeyFail(yx1 yx1) {
        pq7.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onVerifySecretKeySuccess(boolean z) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public final void setMCurrentState(BleStateAbs bleStateAbs) {
        pq7.c(bleStateAbs, "<set-?>");
        this.mCurrentState = bleStateAbs;
    }

    @DexIgnore
    public final void setSessionStateMap(HashMap<SessionState, String> hashMap) {
        pq7.c(hashMap, "<set-?>");
        this.sessionStateMap = hashMap;
    }

    @DexIgnore
    public final void stopSubFlow(int i) {
        xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new SubFlow$stopSubFlow$Anon1(this, i, null), 3, null);
    }
}
