package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.ln1;
import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.us1;
import com.fossil.ym1;
import com.fossil.yx1;
import com.fossil.zm1;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetWorkoutConfigSession extends EnableMaintainingSession {
    @DexIgnore
    public /* final */ WorkoutConfigData mWorkoutConfigData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWorkoutConfigState extends BleStateAbs {
        @DexIgnore
        public qy1<zm1[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWorkoutConfigState() {
            super(SetWorkoutConfigSession.this.getTAG());
        }

        @DexIgnore
        private final ym1[] prepareConfigData() {
            ln1 ln1 = new ln1();
            ln1.e(SetWorkoutConfigSession.this.mWorkoutConfigData.diameterInMillimeter, SetWorkoutConfigSession.this.mWorkoutConfigData.tireSizeInMillimeter, SetWorkoutConfigSession.this.mWorkoutConfigData.chainring, SetWorkoutConfigSession.this.mWorkoutConfigData.cog);
            return ln1.b();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            SetWorkoutConfigSession setWorkoutConfigSession = SetWorkoutConfigSession.this;
            setWorkoutConfigSession.log("SetWorkoutConfigSession: WorkoutConfigData=" + SetWorkoutConfigSession.this.mWorkoutConfigData);
            qy1<zm1[]> deviceConfig = SetWorkoutConfigSession.this.getBleAdapter().setDeviceConfig(SetWorkoutConfigSession.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetWorkoutConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            if (!retry(SetWorkoutConfigSession.this.getContext(), SetWorkoutConfigSession.this.getSerial())) {
                SetWorkoutConfigSession.this.log("Reach the limit retry. Stop.");
                SetWorkoutConfigSession.this.stop(FailureCode.FAILED_TO_SET_WORKOUT_CONFIG_SESSION);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetWorkoutConfigSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            qy1<zm1[]> qy1 = this.task;
            if (qy1 != null) {
                us1.a(qy1);
            }
            SetWorkoutConfigSession.this.stop(FailureCode.FAILED_TO_SET_WORKOUT_CONFIG_SESSION);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetWorkoutConfigSession(WorkoutConfigData workoutConfigData, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.DEVICE_SETTING, CommunicateMode.SET_WORKOUT_CONFIG_SESSION, bleAdapterImpl, bleSessionCallback);
        pq7.c(workoutConfigData, "mWorkoutConfigData");
        pq7.c(bleAdapterImpl, "bleAdapter");
        this.mWorkoutConfigData = workoutConfigData;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetWorkoutConfigSession setWorkoutConfigSession = new SetWorkoutConfigSession(this.mWorkoutConfigData, getBleAdapter(), getBleSessionCallback());
        setWorkoutConfigSession.setDevice(getDevice());
        return setWorkoutConfigSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.SET_WORKOUT_CONFIG_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_WORKOUT_CONFIG_STATE;
        String name = SetWorkoutConfigState.class.getName();
        pq7.b(name, "SetWorkoutConfigState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
