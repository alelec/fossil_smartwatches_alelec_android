package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.js1;
import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.tl7;
import com.fossil.us1;
import com.fossil.yx1;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.common.log.MFLogger;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoBackgroundImageConfigSession extends SetAutoSettingsSession {
    @DexIgnore
    public /* final */ BackgroundConfig mNewBackgroundImageConfig;
    @DexIgnore
    public BackgroundConfig mOldBackgroundImageConfig;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneState() {
            super(SetAutoBackgroundImageConfigSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            String tag = getTAG();
            MFLogger.d(tag, "All done of " + getTAG());
            SetAutoBackgroundImageConfigSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetBackgroundImageConfigState extends BleStateAbs {
        @DexIgnore
        public qy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetBackgroundImageConfigState() {
            super(SetAutoBackgroundImageConfigSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            qy1<tl7> backgroundImage = SetAutoBackgroundImageConfigSession.this.getBleAdapter().setBackgroundImage(SetAutoBackgroundImageConfigSession.this.getLogSession(), SetAutoBackgroundImageConfigSession.this.mNewBackgroundImageConfig, this);
            this.task = backgroundImage;
            if (backgroundImage == null) {
                SetAutoBackgroundImageConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetBackgroundImageFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            if (!retry(SetAutoBackgroundImageConfigSession.this.getContext(), SetAutoBackgroundImageConfigSession.this.getSerial())) {
                SetAutoBackgroundImageConfigSession.this.log("Reach the limit retry. Stop.");
                SetAutoBackgroundImageConfigSession setAutoBackgroundImageConfigSession = SetAutoBackgroundImageConfigSession.this;
                setAutoBackgroundImageConfigSession.storeMappings(setAutoBackgroundImageConfigSession.mNewBackgroundImageConfig, true);
                SetAutoBackgroundImageConfigSession.this.stop(FailureCode.FAILED_TO_SET_BACKGROUND_IMAGE_CONFIG);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetBackgroundImageSuccess() {
            stopTimeout();
            SetAutoBackgroundImageConfigSession setAutoBackgroundImageConfigSession = SetAutoBackgroundImageConfigSession.this;
            setAutoBackgroundImageConfigSession.storeMappings(setAutoBackgroundImageConfigSession.mNewBackgroundImageConfig, false);
            SetAutoBackgroundImageConfigSession setAutoBackgroundImageConfigSession2 = SetAutoBackgroundImageConfigSession.this;
            setAutoBackgroundImageConfigSession2.enterStateAsync(setAutoBackgroundImageConfigSession2.createConcreteState((SetAutoBackgroundImageConfigSession) BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            qy1<tl7> qy1 = this.task;
            if (qy1 != null) {
                us1.a(qy1);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoBackgroundImageConfigSession(BackgroundConfig backgroundConfig, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_BACKGROUND_IMAGE_CONFIG, bleAdapterImpl, bleSessionCallback);
        pq7.c(backgroundConfig, "mNewBackgroundImageConfig");
        pq7.c(bleAdapterImpl, "bleAdapter");
        this.mNewBackgroundImageConfig = backgroundConfig;
    }

    @DexIgnore
    private final void storeMappings(BackgroundConfig backgroundConfig, boolean z) {
        DevicePreferenceUtils.setAutoBackgroundImageConfig(getBleAdapter().getContext(), getBleAdapter().getSerial(), new Gson().t(backgroundConfig));
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetAutoBackgroundImageConfigSession setAutoBackgroundImageConfigSession = new SetAutoBackgroundImageConfigSession(this.mNewBackgroundImageConfig, getBleAdapter(), getBleSessionCallback());
        setAutoBackgroundImageConfigSession.setDevice(getDevice());
        return setAutoBackgroundImageConfigSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        BleState createConcreteState;
        super.initSettings();
        this.mOldBackgroundImageConfig = DevicePreferenceUtils.getAutoBackgroundImageConfig(getContext(), getSerial());
        if (getBleAdapter().isSupportedFeature(js1.class) == null) {
            log("This device does not support set background image.");
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (pq7.a(this.mNewBackgroundImageConfig, this.mOldBackgroundImageConfig)) {
            log("New Background image config and the old one are the same, no need to store again.");
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else {
            long timestamp = this.mNewBackgroundImageConfig.getTimestamp();
            BackgroundConfig backgroundConfig = this.mOldBackgroundImageConfig;
            if (timestamp > (backgroundConfig != null ? backgroundConfig.getTimestamp() : 0)) {
                storeMappings(this.mNewBackgroundImageConfig, true);
                createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE);
            } else {
                log("Old Background image config timestamp is greater than the new one, no need to store again.");
                createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
            }
        }
        setStartState(createConcreteState);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE;
        String name = SetBackgroundImageConfigState.class.getName();
        pq7.b(name, "SetBackgroundImageConfigState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        pq7.b(name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        pq7.c(bleState, "<set-?>");
        this.startState = bleState;
    }
}
