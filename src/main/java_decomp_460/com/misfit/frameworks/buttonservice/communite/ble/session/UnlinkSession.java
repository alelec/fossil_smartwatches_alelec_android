package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.facebook.internal.NativeProtocol;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UnlinkSession extends BleSessionAbs {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ClearCacheDevice extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ClearCacheDevice() {
            super(UnlinkSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            UnlinkSession.this.getBleAdapter().clearCache(UnlinkSession.this.getLogSession());
            UnlinkSession unlinkSession = UnlinkSession.this;
            unlinkSession.enterStateAsync(unlinkSession.createConcreteState((UnlinkSession) BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE));
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class CloseConnectionState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public CloseConnectionState() {
            super(UnlinkSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            UnlinkSession.this.getBleAdapter().closeConnection(UnlinkSession.this.getLogSession(), true);
            UnlinkSession unlinkSession = UnlinkSession.this;
            unlinkSession.log("Unlink Device " + UnlinkSession.this.getSerial() + ": Close connection is called.");
            UnlinkSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UnlinkSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.SPECIAL, CommunicateMode.UNLINK, bleAdapterImpl, bleSessionCallback);
        pq7.c(bleAdapterImpl, "bleAdapter");
        setSerial(bleAdapterImpl.getSerial());
        setContext(bleAdapterImpl.getContext());
        setLogSession(FLogger.Session.REMOVE_DEVICE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        UnlinkSession unlinkSession = new UnlinkSession(getBleAdapter(), getBleSessionCallback());
        unlinkSession.setDevice(getDevice());
        return unlinkSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs
    public void initStateMap() {
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.CLEAR_CACHE_DEVICE;
        String name = ClearCacheDevice.class.getName();
        pq7.b(name, "ClearCacheDevice::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE;
        String name2 = CloseConnectionState.class.getName();
        pq7.b(name2, "CloseConnectionState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean onStart(Object... objArr) {
        pq7.c(objArr, NativeProtocol.WEB_DIALOG_PARAMS);
        super.onStart(Arrays.copyOf(objArr, objArr.length));
        enterStateAsync(createConcreteState(BleSessionAbs.SessionState.CLEAR_CACHE_DEVICE));
        return true;
    }
}
