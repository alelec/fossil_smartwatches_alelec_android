package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import android.os.Bundle;
import com.fossil.fitness.FitnessData;
import com.fossil.hm7;
import com.fossil.hr7;
import com.fossil.il7;
import com.fossil.mm7;
import com.fossil.oy1;
import com.fossil.pq7;
import com.fossil.qm1;
import com.fossil.qy1;
import com.fossil.rm1;
import com.fossil.sm1;
import com.fossil.tl7;
import com.fossil.tm1;
import com.fossil.um1;
import com.fossil.us1;
import com.fossil.vm1;
import com.fossil.wm1;
import com.fossil.xm1;
import com.fossil.ym1;
import com.fossil.yx1;
import com.fossil.zm1;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.db.DataCollectorHelper;
import com.misfit.frameworks.buttonservice.db.DataFile;
import com.misfit.frameworks.buttonservice.db.DataFileProvider;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFSyncLog;
import com.misfit.frameworks.buttonservice.log.model.SessionDetailInfo;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseTransferDataSubFlow extends SubFlow {
    @DexIgnore
    public int activeMinute;
    @DexIgnore
    public int activeMinuteGoal;
    @DexIgnore
    public int awakeMin;
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public long calorie;
    @DexIgnore
    public long calorieGoal;
    @DexIgnore
    public long dailyStepInfo;
    @DexIgnore
    public int deepMin;
    @DexIgnore
    public long distanceInCentimeter;
    @DexIgnore
    public /* final */ boolean isClearDataOptional;
    @DexIgnore
    public boolean isNewDevice;
    @DexIgnore
    public int lightMin;
    @DexIgnore
    public List<FitnessData> mSyncData;
    @DexIgnore
    public /* final */ MFLog mflog;
    @DexIgnore
    public long realTimeSteps;
    @DexIgnore
    public long realTimeStepsInfo;
    @DexIgnore
    public long stepGoal;
    @DexIgnore
    public MFSyncLog syncLog;
    @DexIgnore
    public long syncTime;
    @DexIgnore
    public int totalSleepInMin;
    @DexIgnore
    public /* final */ UserProfile userProfile;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class EraseDataFileState extends BleStateAbs {
        @DexIgnore
        public qy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public EraseDataFileState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            qy1<tl7> eraseDataFile = BaseTransferDataSubFlow.this.getBleAdapter().eraseDataFile(BaseTransferDataSubFlow.this.getLogSession(), this);
            this.task = eraseDataFile;
            if (eraseDataFile == null) {
                BaseTransferDataSubFlow.this.stopSubFlow(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onEraseDataFilesFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            if (BaseTransferDataSubFlow.this.isClearDataOptional()) {
                BaseTransferDataSubFlow.this.addFailureCode(FailureCode.FAILED_TO_CLEAR_DATA);
                BaseTransferDataSubFlow.this.stopSubFlow(0);
                return;
            }
            BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_CLEAR_DATA);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onEraseDataFilesSuccess() {
            stopTimeout();
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Erase DataFiles timeout. Cancel.");
            qy1<tl7> qy1 = this.task;
            if (qy1 != null) {
                if (qy1 != null) {
                    us1.a(qy1);
                } else {
                    pq7.i();
                    throw null;
                }
            } else if (BaseTransferDataSubFlow.this.isClearDataOptional()) {
                BaseTransferDataSubFlow.this.addFailureCode(FailureCode.FAILED_TO_CLEAR_DATA);
                BaseTransferDataSubFlow.this.stopSubFlow(0);
            } else {
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_CLEAR_DATA);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public qy1<HashMap<zm1, ym1>> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public GetDeviceConfigState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        private final void logConfiguration(HashMap<zm1, ym1> hashMap) {
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            hr7 hr7 = hr7.f1520a;
            String format = String.format("Get configuration  " + BaseTransferDataSubFlow.this.getSerial() + "\n\t[timeConfiguration: \n\ttime = %s,\n\tbattery = %s,\n\tbiometric = %s,\n\tdaily steps = %s,\n\tdaily step goal = %s, \n\tdaily calorie: %s, \n\tdaily calorie goal: %s, \n\tdaily total active minute: %s, \n\tdaily active minute goal: %s, \n\tdaily distance: %s, \n\tdaily sleep: %s, \n\tinactive nudge: %s, \n\tvibration strength: %s, \n\tdo not disturb schedule: %s, \n\t]", Arrays.copyOf(new Object[]{hashMap.get(zm1.TIME), hashMap.get(zm1.BATTERY), hashMap.get(zm1.BIOMETRIC_PROFILE), hashMap.get(zm1.DAILY_STEP), hashMap.get(zm1.DAILY_STEP_GOAL), hashMap.get(zm1.DAILY_CALORIE), hashMap.get(zm1.DAILY_CALORIE_GOAL), hashMap.get(zm1.DAILY_TOTAL_ACTIVE_MINUTE), hashMap.get(zm1.DAILY_ACTIVE_MINUTE_GOAL), hashMap.get(zm1.DAILY_DISTANCE), hashMap.get(zm1.DAILY_SLEEP), hashMap.get(zm1.DAILY_DISTANCE), hashMap.get(zm1.DAILY_DISTANCE), hashMap.get(zm1.INACTIVE_NUDGE), hashMap.get(zm1.VIBE_STRENGTH), hashMap.get(zm1.DO_NOT_DISTURB_SCHEDULE)}, 16));
            pq7.b(format, "java.lang.String.format(format, *args)");
            baseTransferDataSubFlow.log(format);
        }

        @DexIgnore
        private final void readConfig(HashMap<zm1, ym1> hashMap) {
            if (!BaseTransferDataSubFlow.this.getUserProfile().isNewDevice()) {
                ym1 ym1 = hashMap.get(zm1.DAILY_STEP_GOAL);
                if (ym1 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                    if (ym1 != null) {
                        baseTransferDataSubFlow.stepGoal = ((wm1) ym1).getStep();
                    } else {
                        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepGoalConfig");
                    }
                }
                ym1 ym12 = hashMap.get(zm1.DAILY_STEP);
                if (ym12 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                    if (ym12 != null) {
                        baseTransferDataSubFlow2.realTimeSteps = ((vm1) ym12).getStep();
                        BaseTransferDataSubFlow baseTransferDataSubFlow3 = BaseTransferDataSubFlow.this;
                        baseTransferDataSubFlow3.realTimeStepsInfo = baseTransferDataSubFlow3.realTimeSteps;
                    } else {
                        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepConfig");
                    }
                }
                ym1 ym13 = hashMap.get(zm1.DAILY_ACTIVE_MINUTE_GOAL);
                if (ym13 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow4 = BaseTransferDataSubFlow.this;
                    if (ym13 != null) {
                        baseTransferDataSubFlow4.activeMinuteGoal = ((qm1) ym13).getMinute();
                    } else {
                        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyActiveMinuteGoalConfig");
                    }
                }
                ym1 ym14 = hashMap.get(zm1.DAILY_TOTAL_ACTIVE_MINUTE);
                if (ym14 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow5 = BaseTransferDataSubFlow.this;
                    if (ym14 != null) {
                        baseTransferDataSubFlow5.activeMinute = ((xm1) ym14).getMinute();
                    } else {
                        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyTotalActiveMinuteConfig");
                    }
                }
                ym1 ym15 = hashMap.get(zm1.DAILY_CALORIE_GOAL);
                if (ym15 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow6 = BaseTransferDataSubFlow.this;
                    if (ym15 != null) {
                        baseTransferDataSubFlow6.calorieGoal = ((sm1) ym15).getCalorie();
                    } else {
                        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyCalorieGoalConfig");
                    }
                }
                ym1 ym16 = hashMap.get(zm1.DAILY_CALORIE);
                if (ym16 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow7 = BaseTransferDataSubFlow.this;
                    if (ym16 != null) {
                        baseTransferDataSubFlow7.calorie = ((rm1) ym16).getCalorie();
                    } else {
                        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyCalorieConfig");
                    }
                }
                ym1 ym17 = hashMap.get(zm1.DAILY_DISTANCE);
                if (ym17 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow8 = BaseTransferDataSubFlow.this;
                    if (ym17 != null) {
                        baseTransferDataSubFlow8.distanceInCentimeter = ((tm1) ym17).getCentimeter();
                    } else {
                        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyDistanceConfig");
                    }
                }
                ym1 ym18 = hashMap.get(zm1.DAILY_SLEEP);
                if (ym18 != null) {
                    if (!(ym18 instanceof um1)) {
                        ym18 = null;
                    }
                    um1 um1 = (um1) ym18;
                    if (um1 != null) {
                        BaseTransferDataSubFlow.this.totalSleepInMin = um1.getTotalSleepInMinute();
                        BaseTransferDataSubFlow.this.awakeMin = um1.getAwakeInMinute();
                        BaseTransferDataSubFlow.this.lightMin = um1.getLightSleepInMinute();
                        BaseTransferDataSubFlow.this.deepMin = um1.getDeepSleepInMinute();
                        return;
                    }
                    return;
                }
                return;
            }
            BaseTransferDataSubFlow.this.log("We skip read real time steps cause by full sync");
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            qy1<HashMap<zm1, ym1>> deviceConfig = BaseTransferDataSubFlow.this.getBleAdapter().getDeviceConfig(BaseTransferDataSubFlow.this.getLogSession(), this);
            this.task = deviceConfig;
            if (deviceConfig != null) {
                startTimeout();
                return true;
            } else if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return true;
            } else {
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
                return true;
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            BaseTransferDataSubFlow.this.addFailureCode(FailureCode.FAILED_TO_GET_CONFIG);
            if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return;
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigSuccess(HashMap<zm1, ym1> hashMap) {
            pq7.c(hashMap, "deviceConfiguration");
            stopTimeout();
            logConfiguration(hashMap);
            readConfig(hashMap);
            if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return;
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Get device configuration timeout. Cancel.");
            qy1<HashMap<zm1, ym1>> qy1 = this.task;
            if (qy1 != null) {
                if (qy1 != null) {
                    us1.a(qy1);
                } else {
                    pq7.i();
                    throw null;
                }
            } else if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
            } else {
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class PlayDeviceAnimationState extends BleStateAbs {
        @DexIgnore
        public qy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public PlayDeviceAnimationState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            qy1<tl7> playDeviceAnimation = BaseTransferDataSubFlow.this.getBleAdapter().playDeviceAnimation(BaseTransferDataSubFlow.this.getLogSession(), this);
            this.task = playDeviceAnimation;
            if (playDeviceAnimation == null) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.GET_DEVICE_CONFIG_STATE));
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onPlayDeviceAnimation(boolean z, yx1 yx1) {
            stopTimeout();
            if (!z) {
                BaseTransferDataSubFlow.this.addFailureCode(201);
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.GET_DEVICE_CONFIG_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Play device animation timeout. Cancel.");
            qy1<tl7> qy1 = this.task;
            if (qy1 == null) {
                BaseTransferDataSubFlow.this.addFailureCode(201);
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.GET_DEVICE_CONFIG_STATE));
            } else if (qy1 != null) {
                us1.a(qy1);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ProcessAndStoreDataState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ProcessAndStoreDataState() {
            super(BaseTransferDataSubFlow.this.getTAG());
            setTimeout(CommuteTimeService.A);
            BaseTransferDataSubFlow.this.log("Process and store data of device with serial " + BaseTransferDataSubFlow.this.getSerial());
        }

        @DexIgnore
        private final Bundle bundleData() {
            Bundle bundle = new Bundle();
            bundle.putLong(Constants.SYNC_RESULT, BaseTransferDataSubFlow.this.getSyncTime());
            bundle.putLong(ButtonService.Companion.getREALTIME_STEPS(), BaseTransferDataSubFlow.this.realTimeSteps);
            return bundle;
        }

        @DexIgnore
        private final List<FitnessData> readDataFromCache() {
            int i;
            ArrayList arrayList = new ArrayList();
            List<DataFile> allDataFiles = DataFileProvider.getInstance(BaseTransferDataSubFlow.this.getBleAdapter().getContext()).getAllDataFiles(BaseTransferDataSubFlow.this.getSyncTime(), BaseTransferDataSubFlow.this.getSerial());
            Gson gson = new Gson();
            if (allDataFiles.size() > 0) {
                pq7.b(allDataFiles, "cacheData");
                long j = -1;
                int i2 = 0;
                for (T t : allDataFiles) {
                    try {
                        pq7.b(t, "it");
                        FitnessData fitnessData = (FitnessData) gson.k(t.getDataFile(), FitnessData.class);
                        pq7.b(fitnessData, "fitnessData");
                        arrayList.add(fitnessData);
                        if (t.getSyncTime() != j) {
                            if (j != -1) {
                                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                                baseTransferDataSubFlow.log("Got cache data of " + j + ", fitness data size=" + i2);
                            }
                            i = 0;
                            j = t.getSyncTime();
                        } else {
                            i = i2;
                        }
                        i2 = i + 1;
                    } catch (Exception e) {
                        BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Got cache data of ");
                        pq7.b(t, "it");
                        sb.append(t.getSyncTime());
                        sb.append(" exception=");
                        sb.append(e);
                        baseTransferDataSubFlow2.log(sb.toString());
                    }
                }
                if (j != -1 && i2 > 0) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow3 = BaseTransferDataSubFlow.this;
                    baseTransferDataSubFlow3.log("Got cache data of " + j + ", fitness data size=" + i2);
                }
            } else {
                BaseTransferDataSubFlow.this.log("No cache data.");
            }
            return arrayList;
        }

        /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: long : 0x0043: INVOKE  (r2v6 long) = 
          (wrap: com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow : 0x0041: IGET  (r2v5 com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow) = 
          (r9v0 'this' com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow$ProcessAndStoreDataState A[IMMUTABLE_TYPE, THIS])
         com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow.ProcessAndStoreDataState.this$0 com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow)
         type: VIRTUAL call: com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow.getSyncTime():long), ('_' char), (r0v2 int)] */
        @DexIgnore
        private final void saveAsCache(List<FitnessData> list) {
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("Save latest sync data as cache, sync time=" + BaseTransferDataSubFlow.this.getSyncTime() + ", fitness data size=" + list.size());
            int i = 0;
            for (T t : list) {
                if (i >= 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(BaseTransferDataSubFlow.this.getSyncTime());
                    sb.append('_');
                    sb.append(i);
                    DataCollectorHelper.saveDataFile(BaseTransferDataSubFlow.this.getBleAdapter().getContext(), new DataFile(sb.toString(), new Gson().t(t), BaseTransferDataSubFlow.this.getSerial(), BaseTransferDataSubFlow.this.getSyncTime()));
                    i++;
                } else {
                    hm7.l();
                    throw null;
                }
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            FLogger.INSTANCE.getLocal().d(getTAG(), "onEnter ProcessAndStoreDataState");
            List<FitnessData> readDataFromCache = readDataFromCache();
            saveAsCache(BaseTransferDataSubFlow.this.mSyncData);
            readDataFromCache.addAll(BaseTransferDataSubFlow.this.mSyncData);
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("set syncdata from cache data with size " + readDataFromCache.size());
            BaseTransferDataSubFlow.this.mSyncData = readDataFromCache;
            startTimeout();
            BleSession.BleSessionCallback bleSessionCallback = BaseTransferDataSubFlow.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                bleSessionCallback.onReceivedSyncData(bundleData());
                return true;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Process and Store data timeout.");
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        public final void updateCurrentStepsAndStepGoal(boolean z, UserProfile userProfile) {
            pq7.c(userProfile, "newUserProfile");
            if (z) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.log("Set percentage goal progress session " + BaseTransferDataSubFlow.this.getSerial() + ", keep setting in device");
            } else {
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.log("Set percentage goal progress session " + BaseTransferDataSubFlow.this.getSerial() + ", {currentSteps=" + userProfile.getCurrentSteps() + ", stepGoal=" + userProfile.getGoalSteps() + ",activeMinute=" + userProfile.getActiveMinute() + ", activeMinuteGoal=" + userProfile.getActiveMinuteGoal() + ",calories=" + userProfile.getCalories() + ", caloriesGoal=" + userProfile.getCaloriesGoal() + ",distanceInCentimeter=" + userProfile.getDistanceInCentimeter() + "totalSleepMin=" + userProfile.getTotalSleepInMinute() + "awakeMin=" + userProfile.getAwakeInMinute() + "lightMin=" + userProfile.getLightSleepInMinute() + "deepMin=" + userProfile.getDeepSleepInMinute() + "}");
            }
            stopTimeout();
            if (!z) {
                BaseTransferDataSubFlow.this.getUserProfile().setCurrentSteps(userProfile.getCurrentSteps());
                BaseTransferDataSubFlow.this.getUserProfile().setGoalSteps(userProfile.getGoalSteps());
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinute(userProfile.getActiveMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinuteGoal(userProfile.getActiveMinuteGoal());
                BaseTransferDataSubFlow.this.getUserProfile().setCalories(userProfile.getCalories());
                BaseTransferDataSubFlow.this.getUserProfile().setCaloriesGoal(userProfile.getCaloriesGoal());
                BaseTransferDataSubFlow.this.getUserProfile().setDistanceInCentimeter(userProfile.getDistanceInCentimeter());
                BaseTransferDataSubFlow.this.getUserProfile().setTotalSleepInMinute(userProfile.getTotalSleepInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setAwakeInMinute(userProfile.getAwakeInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setLightSleepInMinute(userProfile.getLightSleepInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setDeepSleepInMinute(userProfile.getDeepSleepInMinute());
            } else {
                BaseTransferDataSubFlow.this.getUserProfile().setCurrentSteps(BaseTransferDataSubFlow.this.realTimeSteps);
                BaseTransferDataSubFlow.this.getUserProfile().setGoalSteps(BaseTransferDataSubFlow.this.stepGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinute(BaseTransferDataSubFlow.this.activeMinute);
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinuteGoal(BaseTransferDataSubFlow.this.activeMinuteGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setCalories(BaseTransferDataSubFlow.this.calorie);
                BaseTransferDataSubFlow.this.getUserProfile().setCaloriesGoal(BaseTransferDataSubFlow.this.calorieGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setDistanceInCentimeter(BaseTransferDataSubFlow.this.distanceInCentimeter);
                BaseTransferDataSubFlow.this.getUserProfile().setTotalSleepInMinute(BaseTransferDataSubFlow.this.totalSleepInMin);
                BaseTransferDataSubFlow.this.getUserProfile().setAwakeInMinute(BaseTransferDataSubFlow.this.awakeMin);
                BaseTransferDataSubFlow.this.getUserProfile().setLightSleepInMinute(BaseTransferDataSubFlow.this.lightMin);
                BaseTransferDataSubFlow.this.getUserProfile().setDeepSleepInMinute(BaseTransferDataSubFlow.this.deepMin);
            }
            FLogger.INSTANCE.updateSessionDetailInfo(new SessionDetailInfo(BaseTransferDataSubFlow.this.getBleAdapter().getBatteryLevel(), (int) BaseTransferDataSubFlow.this.realTimeStepsInfo, (int) BaseTransferDataSubFlow.this.dailyStepInfo));
            MFSyncLog syncLog = BaseTransferDataSubFlow.this.getSyncLog();
            if (syncLog != null) {
                syncLog.setRealTimeStep(BaseTransferDataSubFlow.this.getUserProfile().getCurrentSteps());
            }
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadDataFileState extends BleStateAbs {
        @DexIgnore
        public oy1<FitnessData[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ReadDataFileState() {
            super(BaseTransferDataSubFlow.this.getTAG());
            setMaxRetries(3);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void cancelCurrentBleTask() {
            super.cancelCurrentBleTask();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "cancelCurrentBleTask is " + this.task);
            oy1<FitnessData[]> oy1 = this.task;
            if (oy1 != null) {
                us1.a(oy1);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            try {
                oy1<FitnessData[]> readDataFile = BaseTransferDataSubFlow.this.getBleAdapter().readDataFile(BaseTransferDataSubFlow.this.getLogSession(), BaseTransferDataSubFlow.this.getUserProfile().getUserBiometricData().toSDKBiometricProfile(), this);
                this.task = readDataFile;
                if (readDataFile == null) {
                    BaseTransferDataSubFlow.this.stopSubFlow(10000);
                    return true;
                }
                startTimeout();
                return true;
            } catch (Exception e) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.log("Read Data Files:  ex=" + e.getMessage());
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.errorLog("ReadDataFileState: catchException=" + e.getMessage(), FLogger.Component.BLE, ErrorCodeBuilder.Step.UNKNOWN, ErrorCodeBuilder.AppError.UNKNOWN);
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SYNC);
                return true;
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadDataFilesFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            if (!retry(BaseTransferDataSubFlow.this.getBleAdapter().getContext(), BaseTransferDataSubFlow.this.getSerial())) {
                BaseTransferDataSubFlow.this.log("Reach the limit retry. Stop.");
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SYNC);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadDataFilesProgressChanged(float f) {
            setTimeout(30000);
            startTimeout();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadDataFilesSuccess(FitnessData[] fitnessDataArr) {
            pq7.c(fitnessDataArr, "data");
            stopTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, ".onReadDataFilesSuccess(), sdk = " + new Gson().t(fitnessDataArr));
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("data size from SDK " + fitnessDataArr.length);
            mm7.t(BaseTransferDataSubFlow.this.mSyncData, fitnessDataArr);
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.PROCESS_AND_STORE_DATA_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Read DataFiles timeout. Cancel.");
            oy1<FitnessData[]> oy1 = this.task;
            if (oy1 == null) {
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SYNC);
            } else if (oy1 != null) {
                us1.a(oy1);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseTransferDataSubFlow(String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl, UserProfile userProfile2, BleSession.BleSessionCallback bleSessionCallback2, boolean z) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        pq7.c(str, "tagName");
        pq7.c(bleSession, "bleSession");
        pq7.c(session, "logSession");
        pq7.c(str2, "serial");
        pq7.c(bleAdapterImpl, "bleAdapter");
        pq7.c(userProfile2, "userProfile");
        this.mflog = mFLog;
        this.userProfile = userProfile2;
        this.bleSessionCallback = bleSessionCallback2;
        this.isClearDataOptional = z;
        this.syncLog = (MFSyncLog) (!(mFLog instanceof MFSyncLog) ? null : mFLog);
        this.realTimeSteps = this.userProfile.getCurrentSteps();
        this.stepGoal = this.userProfile.getGoalSteps();
        this.activeMinute = this.userProfile.getActiveMinute();
        this.activeMinuteGoal = this.userProfile.getActiveMinuteGoal();
        this.calorie = this.userProfile.getCalories();
        this.calorieGoal = this.userProfile.getCaloriesGoal();
        this.distanceInCentimeter = this.userProfile.getDistanceInCentimeter();
        this.totalSleepInMin = this.userProfile.getTotalSleepInMinute();
        this.awakeMin = this.userProfile.getAwakeInMinute();
        this.lightMin = this.userProfile.getLightSleepInMinute();
        this.deepMin = this.userProfile.getDeepSleepInMinute();
        this.isNewDevice = this.userProfile.isNewDevice();
        this.realTimeStepsInfo = this.userProfile.getCurrentSteps();
        this.dailyStepInfo = this.userProfile.getCurrentSteps();
        this.mSyncData = new ArrayList();
        this.syncTime = System.currentTimeMillis() / ((long) 1000);
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final MFLog getMflog() {
        return this.mflog;
    }

    @DexIgnore
    public final List<FitnessData> getSyncData() {
        log("getSyncData dataSize " + this.mSyncData.size());
        return this.mSyncData;
    }

    @DexIgnore
    public final MFSyncLog getSyncLog() {
        return this.syncLog;
    }

    @DexIgnore
    public final long getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.PLAY_DEVICE_ANIMATION_STATE;
        String name = PlayDeviceAnimationState.class.getName();
        pq7.b(name, "PlayDeviceAnimationState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.GET_DEVICE_CONFIG_STATE;
        String name2 = GetDeviceConfigState.class.getName();
        pq7.b(name2, "GetDeviceConfigState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<SubFlow.SessionState, String> sessionStateMap3 = getSessionStateMap();
        SubFlow.SessionState sessionState3 = SubFlow.SessionState.ERASE_DATA_FILE_STATE;
        String name3 = EraseDataFileState.class.getName();
        pq7.b(name3, "EraseDataFileState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<SubFlow.SessionState, String> sessionStateMap4 = getSessionStateMap();
        SubFlow.SessionState sessionState4 = SubFlow.SessionState.READ_DATA_FILE_STATE;
        String name4 = ReadDataFileState.class.getName();
        pq7.b(name4, "ReadDataFileState::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<SubFlow.SessionState, String> sessionStateMap5 = getSessionStateMap();
        SubFlow.SessionState sessionState5 = SubFlow.SessionState.PROCESS_AND_STORE_DATA_STATE;
        String name5 = ProcessAndStoreDataState.class.getName();
        pq7.b(name5, "ProcessAndStoreDataState::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
    }

    @DexIgnore
    public final boolean isClearDataOptional() {
        return this.isClearDataOptional;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.PLAY_DEVICE_ANIMATION_STATE));
        return true;
    }

    @DexIgnore
    public final void setSyncLog(MFSyncLog mFSyncLog) {
        this.syncLog = mFSyncLog;
    }

    @DexIgnore
    public final void setSyncTime(long j) {
        this.syncTime = j;
    }

    @DexIgnore
    public final void updateCurrentStepAndStepGoalFromApp(boolean z, UserProfile userProfile2) {
        pq7.c(userProfile2, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".updateCurrentStepAndStepGoalFromApp() - currentState=" + getMCurrentState());
        if (getMCurrentState() instanceof ProcessAndStoreDataState) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((ProcessAndStoreDataState) mCurrentState).updateCurrentStepsAndStepGoal(z, userProfile2);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow.ProcessAndStoreDataState");
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String tag2 = getTAG();
        local2.d(tag2, "Inside " + getTAG() + ".updateCurrentStepAndStepGoalFromApp - cannot update current steps and step goal to device caused by current state null or not an instance of ProcessAndStoreDataState.");
    }
}
