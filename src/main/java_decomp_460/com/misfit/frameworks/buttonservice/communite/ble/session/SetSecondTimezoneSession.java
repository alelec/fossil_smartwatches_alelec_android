package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.facebook.internal.NativeProtocol;
import com.fossil.ln1;
import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.us1;
import com.fossil.ym1;
import com.fossil.yx1;
import com.fossil.zm1;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.SettingsUtils;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetSecondTimezoneSession extends QuickResponseSession {
    @DexIgnore
    public String mOldSecondTimezoneId;
    @DexIgnore
    public String mSecondTimezoneId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneSetAutoSecondTimezoneState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneSetAutoSecondTimezoneState() {
            super(SetSecondTimezoneSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetSecondTimezoneSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetSecondTimezoneState extends BleStateAbs {
        @DexIgnore
        public qy1<zm1[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetSecondTimezoneState() {
            super(SetSecondTimezoneSession.this.getTAG());
        }

        @DexIgnore
        private final ym1[] prepareConfigData() {
            int timezoneRawOffsetById = ConversionUtils.INSTANCE.getTimezoneRawOffsetById(SetSecondTimezoneSession.this.mSecondTimezoneId);
            ln1 ln1 = new ln1();
            short s = (short) timezoneRawOffsetById;
            if (SettingsUtils.INSTANCE.isSecondTimezoneInRange(s)) {
                ln1.q(s);
            } else {
                SetSecondTimezoneSession setSecondTimezoneSession = SetSecondTimezoneSession.this;
                setSecondTimezoneSession.log("Set Device Config: Timezone is out of range: " + timezoneRawOffsetById);
            }
            return ln1.b();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            qy1<zm1[]> deviceConfig = SetSecondTimezoneSession.this.getBleAdapter().setDeviceConfig(SetSecondTimezoneSession.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetSecondTimezoneSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            SetSecondTimezoneSession.this.stop(FailureCode.FAILED_TO_SET_SECOND_TIMEZONE);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoSecondTimezone(SetSecondTimezoneSession.this.getBleAdapter().getContext(), SetSecondTimezoneSession.this.mSecondTimezoneId);
            DevicePreferenceUtils.removeSettingFlag(SetSecondTimezoneSession.this.getBleAdapter().getContext(), DeviceSettings.SECOND_TIMEZONE);
            SetSecondTimezoneSession setSecondTimezoneSession = SetSecondTimezoneSession.this;
            setSecondTimezoneSession.enterStateAsync(setSecondTimezoneSession.createConcreteState((SetSecondTimezoneSession) BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            qy1<zm1[]> qy1 = this.task;
            if (qy1 != null) {
                us1.a(qy1);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetSecondTimezoneSession(String str, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_SECOND_TIMEZONE, bleAdapterImpl, bleSessionCallback);
        pq7.c(str, "mSecondTimezoneId");
        pq7.c(bleAdapterImpl, "bleAdapter");
        this.mSecondTimezoneId = str;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean accept(BleSession bleSession) {
        pq7.c(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_SECOND_TIMEZONE) ? false : true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetSecondTimezoneSession setSecondTimezoneSession = new SetSecondTimezoneSession(this.mSecondTimezoneId, getBleAdapter(), getBleSessionCallback());
        setSecondTimezoneSession.setDevice(getDevice());
        return setSecondTimezoneSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_SECOND_TIMEZONE_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initSettings() {
        String autoSecondTimezoneId = DevicePreferenceUtils.getAutoSecondTimezoneId(getContext());
        pq7.b(autoSecondTimezoneId, "DevicePreferenceUtils.ge\u2026SecondTimezoneId(context)");
        this.mOldSecondTimezoneId = autoSecondTimezoneId;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_SECOND_TIMEZONE_STATE;
        String name = SetSecondTimezoneState.class.getName();
        pq7.b(name, "SetSecondTimezoneState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneSetAutoSecondTimezoneState.class.getName();
        pq7.b(name2, "DoneSetAutoSecondTimezoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession, com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean onStart(Object... objArr) {
        pq7.c(objArr, NativeProtocol.WEB_DIALOG_PARAMS);
        initSettings();
        String str = this.mOldSecondTimezoneId;
        if (str == null) {
            pq7.n("mOldSecondTimezoneId");
            throw null;
        } else if (!pq7.a(str, this.mSecondTimezoneId)) {
            return super.onStart(Arrays.copyOf(objArr, objArr.length));
        } else {
            log("The second timezone ids are the same. No need to set again");
            enterStateAsync(createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
            return true;
        }
    }
}
