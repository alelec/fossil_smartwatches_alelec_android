package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.http.AndroidHttpClient;
import android.os.Build;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class la1 {
    @DexIgnore
    public static n91 a(Context context) {
        return c(context, null);
    }

    @DexIgnore
    public static n91 b(Context context, g91 g91) {
        n91 n91 = new n91(new z91(new File(context.getCacheDir(), "volley")), g91);
        n91.d();
        return n91;
    }

    @DexIgnore
    public static n91 c(Context context, w91 w91) {
        x91 x91;
        String str;
        if (w91 != null) {
            x91 = new x91(w91);
        } else if (Build.VERSION.SDK_INT >= 9) {
            x91 = new x91((w91) new ea1());
        } else {
            try {
                String packageName = context.getPackageName();
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
                str = packageName + "/" + packageInfo.versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                str = "volley/0";
            }
            x91 = new x91(new aa1(AndroidHttpClient.newInstance(str)));
        }
        return b(context, x91);
    }
}
