package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ur2 implements Parcelable.Creator<tr2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ tr2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        za3 za3 = tr2.f;
        List<zb2> list = tr2.e;
        String str = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                za3 = (za3) ad2.e(parcel, t, za3.CREATOR);
            } else if (l == 2) {
                list = ad2.j(parcel, t, zb2.CREATOR);
            } else if (l != 3) {
                ad2.B(parcel, t);
            } else {
                str = ad2.f(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new tr2(za3, list, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ tr2[] newArray(int i) {
        return new tr2[i];
    }
}
