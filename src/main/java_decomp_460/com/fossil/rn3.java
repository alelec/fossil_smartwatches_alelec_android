package com.fossil;

import android.content.Context;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rn3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3132a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public Boolean e;
    @DexIgnore
    public long f;
    @DexIgnore
    public xs2 g;
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public Long i;

    @DexIgnore
    public rn3(Context context, xs2 xs2, Long l) {
        rc2.k(context);
        Context applicationContext = context.getApplicationContext();
        rc2.k(applicationContext);
        this.f3132a = applicationContext;
        this.i = l;
        if (xs2 != null) {
            this.g = xs2;
            this.b = xs2.g;
            this.c = xs2.f;
            this.d = xs2.e;
            this.h = xs2.d;
            this.f = xs2.c;
            Bundle bundle = xs2.h;
            if (bundle != null) {
                this.e = Boolean.valueOf(bundle.getBoolean("dataCollectionDefaultEnabled", true));
            }
        }
    }
}
