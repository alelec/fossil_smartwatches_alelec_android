package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dm3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f806a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public String c;
    @DexIgnore
    public /* final */ /* synthetic */ xl3 d;

    @DexIgnore
    public dm3(xl3 xl3, String str, String str2) {
        this.d = xl3;
        rc2.g(str);
        this.f806a = str;
    }

    @DexIgnore
    public final String a() {
        if (!this.b) {
            this.b = true;
            this.c = this.d.B().getString(this.f806a, null);
        }
        return this.c;
    }

    @DexIgnore
    public final void b(String str) {
        if (this.d.m().s(xg3.x0) || !kr3.z0(str, this.c)) {
            SharedPreferences.Editor edit = this.d.B().edit();
            edit.putString(this.f806a, str);
            edit.apply();
            this.c = str;
        }
    }
}
