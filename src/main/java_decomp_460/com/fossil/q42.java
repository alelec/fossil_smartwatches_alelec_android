package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q42 extends f52 {
    @DexIgnore
    @Override // com.fossil.e52
    public void A(Status status) throws RemoteException {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.e52
    public void W(Status status) throws RemoteException {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.e52
    public void y0(GoogleSignInAccount googleSignInAccount, Status status) throws RemoteException {
        throw new UnsupportedOperationException();
    }
}
