package com.fossil;

import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wh0 implements View.OnLongClickListener, View.OnHoverListener, View.OnAttachStateChangeListener {
    @DexIgnore
    public static wh0 k;
    @DexIgnore
    public static wh0 l;
    @DexIgnore
    public /* final */ View b;
    @DexIgnore
    public /* final */ CharSequence c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ Runnable e; // = new a();
    @DexIgnore
    public /* final */ Runnable f; // = new b();
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public xh0 i;
    @DexIgnore
    public boolean j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            wh0.this.g(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            wh0.this.c();
        }
    }

    @DexIgnore
    public wh0(View view, CharSequence charSequence) {
        this.b = view;
        this.c = charSequence;
        this.d = no0.c(ViewConfiguration.get(view.getContext()));
        b();
        this.b.setOnLongClickListener(this);
        this.b.setOnHoverListener(this);
    }

    @DexIgnore
    public static void e(wh0 wh0) {
        wh0 wh02 = k;
        if (wh02 != null) {
            wh02.a();
        }
        k = wh0;
        if (wh0 != null) {
            wh0.d();
        }
    }

    @DexIgnore
    public static void f(View view, CharSequence charSequence) {
        wh0 wh0 = k;
        if (wh0 != null && wh0.b == view) {
            e(null);
        }
        if (TextUtils.isEmpty(charSequence)) {
            wh0 wh02 = l;
            if (wh02 != null && wh02.b == view) {
                wh02.c();
            }
            view.setOnLongClickListener(null);
            view.setLongClickable(false);
            view.setOnHoverListener(null);
            return;
        }
        new wh0(view, charSequence);
    }

    @DexIgnore
    public final void a() {
        this.b.removeCallbacks(this.e);
    }

    @DexIgnore
    public final void b() {
        this.g = Integer.MAX_VALUE;
        this.h = Integer.MAX_VALUE;
    }

    @DexIgnore
    public void c() {
        if (l == this) {
            l = null;
            xh0 xh0 = this.i;
            if (xh0 != null) {
                xh0.c();
                this.i = null;
                b();
                this.b.removeOnAttachStateChangeListener(this);
            } else {
                Log.e("TooltipCompatHandler", "sActiveHandler.mPopup == null");
            }
        }
        if (k == this) {
            e(null);
        }
        this.b.removeCallbacks(this.f);
    }

    @DexIgnore
    public final void d() {
        this.b.postDelayed(this.e, (long) ViewConfiguration.getLongPressTimeout());
    }

    @DexIgnore
    public void g(boolean z) {
        long j2;
        int longPressTimeout;
        long j3;
        if (mo0.P(this.b)) {
            e(null);
            wh0 wh0 = l;
            if (wh0 != null) {
                wh0.c();
            }
            l = this;
            this.j = z;
            xh0 xh0 = new xh0(this.b.getContext());
            this.i = xh0;
            xh0.e(this.b, this.g, this.h, this.j, this.c);
            this.b.addOnAttachStateChangeListener(this);
            if (this.j) {
                j3 = 2500;
            } else {
                if ((mo0.J(this.b) & 1) == 1) {
                    j2 = 3000;
                    longPressTimeout = ViewConfiguration.getLongPressTimeout();
                } else {
                    j2 = 15000;
                    longPressTimeout = ViewConfiguration.getLongPressTimeout();
                }
                j3 = j2 - ((long) longPressTimeout);
            }
            this.b.removeCallbacks(this.f);
            this.b.postDelayed(this.f, j3);
        }
    }

    @DexIgnore
    public final boolean h(MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (Math.abs(x - this.g) <= this.d && Math.abs(y - this.h) <= this.d) {
            return false;
        }
        this.g = x;
        this.h = y;
        return true;
    }

    @DexIgnore
    public boolean onHover(View view, MotionEvent motionEvent) {
        if (this.i == null || !this.j) {
            AccessibilityManager accessibilityManager = (AccessibilityManager) this.b.getContext().getSystemService("accessibility");
            if (!accessibilityManager.isEnabled() || !accessibilityManager.isTouchExplorationEnabled()) {
                int action = motionEvent.getAction();
                if (action != 7) {
                    if (action == 10) {
                        b();
                        c();
                    }
                } else if (this.b.isEnabled() && this.i == null && h(motionEvent)) {
                    e(this);
                }
            }
        }
        return false;
    }

    @DexIgnore
    public boolean onLongClick(View view) {
        this.g = view.getWidth() / 2;
        this.h = view.getHeight() / 2;
        g(true);
        return true;
    }

    @DexIgnore
    public void onViewAttachedToWindow(View view) {
    }

    @DexIgnore
    public void onViewDetachedFromWindow(View view) {
        c();
    }
}
