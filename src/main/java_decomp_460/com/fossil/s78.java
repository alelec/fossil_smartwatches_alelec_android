package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s78 {
    @DexIgnore
    public static /* final */ s78 b; // = new s78();
    @DexIgnore
    public static String c; // = "1.6.99";
    @DexIgnore
    public static /* final */ String d; // = n78.class.getName();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ d78 f3215a; // = new n78();

    @DexIgnore
    public static final s78 c() {
        return b;
    }

    @DexIgnore
    public d78 a() {
        return this.f3215a;
    }

    @DexIgnore
    public String b() {
        return d;
    }
}
