package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.view.MenuItem;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface hm0 extends MenuItem {
    @DexIgnore
    hm0 a(sn0 sn0);

    @DexIgnore
    sn0 b();

    @DexIgnore
    boolean collapseActionView();

    @DexIgnore
    boolean expandActionView();

    @DexIgnore
    View getActionView();

    @DexIgnore
    int getAlphabeticModifiers();

    @DexIgnore
    CharSequence getContentDescription();

    @DexIgnore
    ColorStateList getIconTintList();

    @DexIgnore
    PorterDuff.Mode getIconTintMode();

    @DexIgnore
    int getNumericModifiers();

    @DexIgnore
    CharSequence getTooltipText();

    @DexIgnore
    boolean isActionViewExpanded();

    @DexIgnore
    @Override // android.view.MenuItem
    MenuItem setActionView(int i);

    @DexIgnore
    @Override // android.view.MenuItem
    MenuItem setActionView(View view);

    @DexIgnore
    MenuItem setAlphabeticShortcut(char c, int i);

    @DexIgnore
    hm0 setContentDescription(CharSequence charSequence);

    @DexIgnore
    MenuItem setIconTintList(ColorStateList colorStateList);

    @DexIgnore
    MenuItem setIconTintMode(PorterDuff.Mode mode);

    @DexIgnore
    MenuItem setNumericShortcut(char c, int i);

    @DexIgnore
    MenuItem setShortcut(char c, char c2, int i, int i2);

    @DexIgnore
    void setShowAsAction(int i);

    @DexIgnore
    MenuItem setShowAsActionFlags(int i);

    @DexIgnore
    hm0 setTooltipText(CharSequence charSequence);
}
