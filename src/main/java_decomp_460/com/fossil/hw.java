package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hw extends ox1 {
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public n6 c;
    @DexIgnore
    public byte[] d;
    @DexIgnore
    public /* final */ JSONObject e;

    @DexIgnore
    public /* synthetic */ hw(long j, n6 n6Var, byte[] bArr, JSONObject jSONObject, int i) {
        j = (i & 1) != 0 ? System.currentTimeMillis() : j;
        n6Var = (i & 2) != 0 ? null : n6Var;
        bArr = (i & 4) != 0 ? new byte[0] : bArr;
        jSONObject = (i & 8) != 0 ? new JSONObject() : jSONObject;
        this.b = j;
        this.c = n6Var;
        this.d = bArr;
        this.e = jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject k = g80.k(new JSONObject(), jd0.Q0, Double.valueOf(hy1.f(this.b)));
        jd0 jd0 = jd0.R0;
        n6 n6Var = this.c;
        return gy1.c(g80.k(g80.k(k, jd0, n6Var != null ? n6Var.b : null), jd0.S0, dy1.e(this.d, null, 1, null)), this.e);
    }
}
