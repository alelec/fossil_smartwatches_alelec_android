package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s13<K> implements Map.Entry<K, Object> {
    @DexIgnore
    public Map.Entry<K, q13> b;

    @DexIgnore
    public s13(Map.Entry<K, q13> entry) {
        this.b = entry;
    }

    @DexIgnore
    public final q13 a() {
        return this.b.getValue();
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final K getKey() {
        return this.b.getKey();
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final Object getValue() {
        if (this.b.getValue() == null) {
            return null;
        }
        q13.e();
        throw null;
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final Object setValue(Object obj) {
        if (obj instanceof m23) {
            return this.b.getValue().a((m23) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
