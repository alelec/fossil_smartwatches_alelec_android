package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b53 implements xw2<e53> {
    @DexIgnore
    public static b53 c; // = new b53();
    @DexIgnore
    public /* final */ xw2<e53> b;

    @DexIgnore
    public b53() {
        this(ww2.b(new d53()));
    }

    @DexIgnore
    public b53(xw2<e53> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((e53) c.zza()).zza();
    }

    @DexIgnore
    public static long b() {
        return ((e53) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ e53 zza() {
        return this.b.zza();
    }
}
