package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewOverlay;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fz0 implements gz0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ViewOverlay f1238a;

    @DexIgnore
    public fz0(View view) {
        this.f1238a = view.getOverlay();
    }

    @DexIgnore
    @Override // com.fossil.gz0
    public void a(Drawable drawable) {
        this.f1238a.add(drawable);
    }

    @DexIgnore
    @Override // com.fossil.gz0
    public void b(Drawable drawable) {
        this.f1238a.remove(drawable);
    }
}
