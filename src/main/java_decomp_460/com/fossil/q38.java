package com.fossil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface q38 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static final q38 f2921a = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements q38 {
        @DexIgnore
        @Override // com.fossil.q38
        public c58 a(File file) throws FileNotFoundException {
            return s48.k(file);
        }

        @DexIgnore
        @Override // com.fossil.q38
        public a58 b(File file) throws FileNotFoundException {
            try {
                return s48.f(file);
            } catch (FileNotFoundException e) {
                file.getParentFile().mkdirs();
                return s48.f(file);
            }
        }

        @DexIgnore
        @Override // com.fossil.q38
        public void c(File file) throws IOException {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                for (File file2 : listFiles) {
                    if (file2.isDirectory()) {
                        c(file2);
                    }
                    if (!file2.delete()) {
                        throw new IOException("failed to delete " + file2);
                    }
                }
                return;
            }
            throw new IOException("not a readable directory: " + file);
        }

        @DexIgnore
        @Override // com.fossil.q38
        public boolean d(File file) {
            return file.exists();
        }

        @DexIgnore
        @Override // com.fossil.q38
        public void e(File file, File file2) throws IOException {
            f(file2);
            if (!file.renameTo(file2)) {
                throw new IOException("failed to rename " + file + " to " + file2);
            }
        }

        @DexIgnore
        @Override // com.fossil.q38
        public void f(File file) throws IOException {
            if (!file.delete() && file.exists()) {
                throw new IOException("failed to delete " + file);
            }
        }

        @DexIgnore
        @Override // com.fossil.q38
        public a58 g(File file) throws FileNotFoundException {
            try {
                return s48.a(file);
            } catch (FileNotFoundException e) {
                file.getParentFile().mkdirs();
                return s48.a(file);
            }
        }

        @DexIgnore
        @Override // com.fossil.q38
        public long h(File file) {
            return file.length();
        }
    }

    @DexIgnore
    c58 a(File file) throws FileNotFoundException;

    @DexIgnore
    a58 b(File file) throws FileNotFoundException;

    @DexIgnore
    void c(File file) throws IOException;

    @DexIgnore
    boolean d(File file);

    @DexIgnore
    void e(File file, File file2) throws IOException;

    @DexIgnore
    void f(File file) throws IOException;

    @DexIgnore
    a58 g(File file) throws FileNotFoundException;

    @DexIgnore
    long h(File file);
}
