package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ff2 {
    @DexIgnore
    public static boolean a(Collection<?> collection) {
        if (collection == null) {
            return true;
        }
        return collection.isEmpty();
    }

    @DexIgnore
    @Deprecated
    public static <T> List<T> b() {
        return Collections.emptyList();
    }

    @DexIgnore
    @Deprecated
    public static <T> List<T> c(T t) {
        return Collections.singletonList(t);
    }

    @DexIgnore
    @Deprecated
    public static <T> List<T> d(T... tArr) {
        int length = tArr.length;
        return length != 0 ? length != 1 ? Collections.unmodifiableList(Arrays.asList(tArr)) : c(tArr[0]) : b();
    }
}
