package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ks extends ps {
    @DexIgnore
    public /* final */ n6 G;
    @DexIgnore
    public /* final */ boolean H;
    @DexIgnore
    public long I;
    @DexIgnore
    public /* final */ ArrayList<gs> J;
    @DexIgnore
    public /* final */ byte[] K;
    @DexIgnore
    public /* final */ n6 L;
    @DexIgnore
    public /* final */ int M;

    @DexIgnore
    @Override // com.fossil.ps
    public mt E(byte b) {
        return new is();
    }

    @DexIgnore
    @Override // com.fossil.ps
    public void I(o7 o7Var) {
        super.I(o7Var);
        this.J.add(new gs(o7Var.f2638a, System.currentTimeMillis(), o7Var.b));
        if (this.J.size() >= this.M) {
            this.E = true;
        }
    }

    @DexIgnore
    @Override // com.fossil.ps
    public n6 K() {
        return this.L;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] L() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public n6 N() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public boolean O() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void f(long j) {
        this.I = j;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public long x() {
        return this.I;
    }
}
