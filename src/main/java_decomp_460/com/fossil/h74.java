package com.fossil;

import java.util.Collections;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class h74 implements mg4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ h74 f1437a; // = new h74();

    @DexIgnore
    public static mg4 a() {
        return f1437a;
    }

    @DexIgnore
    @Override // com.fossil.mg4
    public Object get() {
        return Collections.emptySet();
    }
}
