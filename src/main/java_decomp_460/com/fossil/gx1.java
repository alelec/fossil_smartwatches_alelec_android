package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gx1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ gx1 f1387a; // = new gx1();

    @DexIgnore
    public final boolean a(Context context, String[] strArr) {
        pq7.c(context, "context");
        pq7.c(strArr, "permissions");
        boolean z = true;
        for (String str : strArr) {
            z = z && gl0.a(context, str) == 0;
        }
        return z;
    }
}
