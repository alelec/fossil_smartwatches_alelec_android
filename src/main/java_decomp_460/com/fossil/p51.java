package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import com.facebook.places.internal.LocationScannerImpl;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p51 implements u51 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Paint f2779a; // = new Paint(3);
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends InputStream {
        @DexIgnore
        public /* final */ InputStream b;

        @DexIgnore
        public a(InputStream inputStream) {
            pq7.c(inputStream, "delegate");
            this.b = inputStream;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int available() {
            return Integer.MAX_VALUE;
        }

        @DexIgnore
        @Override // java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
        public void close() {
            this.b.close();
        }

        @DexIgnore
        public void mark(int i) {
            this.b.mark(i);
        }

        @DexIgnore
        public boolean markSupported() {
            return this.b.markSupported();
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read() {
            return this.b.read();
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read(byte[] bArr) {
            pq7.c(bArr, "b");
            return this.b.read(bArr);
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) {
            pq7.c(bArr, "b");
            return this.b.read(bArr, i, i2);
        }

        @DexIgnore
        @Override // java.io.InputStream
        public void reset() {
            this.b.reset();
        }

        @DexIgnore
        @Override // java.io.InputStream
        public long skip(long j) {
            return this.b.skip(j);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends n48 {
        @DexIgnore
        public Exception c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(c58 c58) {
            super(c58);
            pq7.c(c58, "delegate");
        }

        @DexIgnore
        public final Exception b() {
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.n48, com.fossil.c58
        public long d0(i48 i48, long j) {
            pq7.c(i48, "sink");
            try {
                return super.d0(i48, j);
            } catch (Exception e) {
                this.c = e;
                throw e;
            }
        }
    }

    @DexIgnore
    public p51(Context context) {
        pq7.c(context, "context");
        this.b = context;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0211, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0212, code lost:
        com.fossil.so7.a(r13, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0215, code lost:
        throw r3;
     */
    @DexIgnore
    @Override // com.fossil.u51
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.g51 r19, com.fossil.k48 r20, com.fossil.f81 r21, com.fossil.x51 r22, com.fossil.qn7<? super com.fossil.r51> r23) {
        /*
        // Method dump skipped, instructions count: 538
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.p51.a(com.fossil.g51, com.fossil.k48, com.fossil.f81, com.fossil.x51, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.u51
    public boolean b(k48 k48, String str) {
        pq7.c(k48, "source");
        return true;
    }

    @DexIgnore
    public final boolean c(boolean z, Bitmap.Config config, String str) {
        return z && (Build.VERSION.SDK_INT < 26 || config == Bitmap.Config.ARGB_8888) && pq7.a(str, "image/jpeg");
    }

    @DexIgnore
    public final Bitmap d(g51 g51, Bitmap bitmap, Bitmap.Config config, boolean z, int i) {
        boolean z2 = i > 0;
        if (!z && !z2) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        float width = ((float) bitmap.getWidth()) / 2.0f;
        float height = ((float) bitmap.getHeight()) / 2.0f;
        if (z) {
            matrix.postScale(-1.0f, 1.0f, width, height);
        }
        if (z2) {
            matrix.postRotate((float) i, width, height);
        }
        RectF rectF = new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) bitmap.getWidth(), (float) bitmap.getHeight());
        matrix.mapRect(rectF);
        if (!(rectF.left == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && rectF.top == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
            matrix.postTranslate(-rectF.left, -rectF.top);
        }
        Bitmap c = (i == 90 || i == 270) ? g51.c(bitmap.getHeight(), bitmap.getWidth(), config) : g51.c(bitmap.getWidth(), bitmap.getHeight(), config);
        new Canvas(c).drawBitmap(bitmap, matrix, this.f2779a);
        g51.b(bitmap);
        return c;
    }
}
