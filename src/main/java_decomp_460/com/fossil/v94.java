package com.fossil;

import android.content.Context;
import java.io.File;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v94 {
    @DexIgnore
    public static /* final */ c d; // = new c();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3737a;
    @DexIgnore
    public /* final */ b b;
    @DexIgnore
    public u94 c;

    @DexIgnore
    public interface b {
        @DexIgnore
        File a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements u94 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.u94
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.u94
        public String b() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.u94
        public byte[] c() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.u94
        public void d() {
        }

        @DexIgnore
        @Override // com.fossil.u94
        public void e(long j, String str) {
        }
    }

    @DexIgnore
    public v94(Context context, b bVar) {
        this(context, bVar, null);
    }

    @DexIgnore
    public v94(Context context, b bVar, String str) {
        this.f3737a = context;
        this.b = bVar;
        this.c = d;
        g(str);
    }

    @DexIgnore
    public void a() {
        this.c.d();
    }

    @DexIgnore
    public void b(Set<String> set) {
        File[] listFiles = this.b.a().listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (!set.contains(e(file))) {
                    file.delete();
                }
            }
        }
    }

    @DexIgnore
    public byte[] c() {
        return this.c.c();
    }

    @DexIgnore
    public String d() {
        return this.c.b();
    }

    @DexIgnore
    public final String e(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".temp");
        return lastIndexOf == -1 ? name : name.substring(20, lastIndexOf);
    }

    @DexIgnore
    public final File f(String str) {
        return new File(this.b.a(), "crashlytics-userlog-" + str + ".temp");
    }

    @DexIgnore
    public final void g(String str) {
        this.c.a();
        this.c = d;
        if (str != null) {
            if (!r84.l(this.f3737a, "com.crashlytics.CollectCustomLogs", true)) {
                x74.f().b("Preferences requested no custom logs. Aborting log file creation.");
            } else {
                h(f(str), 65536);
            }
        }
    }

    @DexIgnore
    public void h(File file, int i) {
        this.c = new x94(file, i);
    }

    @DexIgnore
    public void i(long j, String str) {
        this.c.e(j, str);
    }
}
