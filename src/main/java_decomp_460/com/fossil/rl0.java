package com.fossil;

import android.graphics.Path;
import android.util.Log;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rl0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f3124a;
        @DexIgnore
        public boolean b;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public char f3125a;
        @DexIgnore
        public float[] b;

        @DexIgnore
        public b(char c, float[] fArr) {
            this.f3125a = (char) c;
            this.b = fArr;
        }

        @DexIgnore
        public b(b bVar) {
            this.f3125a = (char) bVar.f3125a;
            float[] fArr = bVar.b;
            this.b = rl0.c(fArr, 0, fArr.length);
        }

        @DexIgnore
        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public static void a(Path path, float[] fArr, char c, char c2, float[] fArr2) {
            int i;
            float f;
            float f2;
            float f3;
            float f4;
            float f5;
            float f6;
            float f7;
            float f8;
            float f9;
            float f10;
            float f11;
            float f12;
            float f13;
            float f14 = fArr[0];
            float f15 = fArr[1];
            float f16 = fArr[2];
            float f17 = fArr[3];
            float f18 = fArr[4];
            float f19 = fArr[5];
            switch (c2) {
                case 'A':
                case 'a':
                    i = 7;
                    break;
                case 'C':
                case 'c':
                    i = 6;
                    break;
                case 'H':
                case 'V':
                case 'h':
                case 'v':
                    i = 1;
                    break;
                case 'L':
                case 'M':
                case 'T':
                case 'l':
                case 'm':
                case 't':
                default:
                    i = 2;
                    break;
                case 'Q':
                case 'S':
                case 'q':
                case 's':
                    i = 4;
                    break;
                case 'Z':
                case 'z':
                    path.close();
                    path.moveTo(f18, f19);
                    f17 = f19;
                    f16 = f18;
                    f15 = f19;
                    f14 = f18;
                    i = 2;
                    break;
            }
            int i2 = 0;
            while (true) {
                float f20 = f15;
                float f21 = f14;
                if (i2 < fArr2.length) {
                    if (c2 == 'A') {
                        int i3 = i2 + 5;
                        int i4 = i2 + 6;
                        c(path, f21, f20, fArr2[i3], fArr2[i4], fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3] != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, fArr2[i2 + 4] != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        float f22 = fArr2[i3];
                        f = fArr2[i4];
                        f2 = f22;
                        f17 = f;
                        f16 = f2;
                        f15 = f;
                        f14 = f2;
                        f19 = f19;
                        f18 = f18;
                    } else if (c2 == 'C') {
                        int i5 = i2 + 2;
                        int i6 = i2 + 3;
                        int i7 = i2 + 4;
                        int i8 = i2 + 5;
                        path.cubicTo(fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i5], fArr2[i6], fArr2[i7], fArr2[i8]);
                        float f23 = fArr2[i7];
                        float f24 = fArr2[i8];
                        f16 = fArr2[i5];
                        f17 = fArr2[i6];
                        f15 = f24;
                        f14 = f23;
                        f19 = f19;
                        f18 = f18;
                    } else if (c2 == 'H') {
                        int i9 = i2 + 0;
                        path.lineTo(fArr2[i9], f20);
                        f15 = f20;
                        f14 = fArr2[i9];
                        f19 = f19;
                        f18 = f18;
                    } else if (c2 == 'Q') {
                        int i10 = i2 + 0;
                        int i11 = i2 + 1;
                        int i12 = i2 + 2;
                        int i13 = i2 + 3;
                        path.quadTo(fArr2[i10], fArr2[i11], fArr2[i12], fArr2[i13]);
                        f16 = fArr2[i10];
                        f17 = fArr2[i11];
                        float f25 = fArr2[i12];
                        f15 = fArr2[i13];
                        f14 = f25;
                        f19 = f19;
                        f18 = f18;
                    } else if (c2 == 'V') {
                        int i14 = i2 + 0;
                        path.lineTo(f21, fArr2[i14]);
                        f15 = fArr2[i14];
                        f14 = f21;
                        f19 = f19;
                        f18 = f18;
                    } else if (c2 != 'a') {
                        if (c2 == 'c') {
                            int i15 = i2 + 2;
                            int i16 = i2 + 3;
                            int i17 = i2 + 4;
                            int i18 = i2 + 5;
                            path.rCubicTo(fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i15], fArr2[i16], fArr2[i17], fArr2[i18]);
                            f3 = fArr2[i15] + f21;
                            float f26 = fArr2[i16] + f20;
                            f4 = fArr2[i18];
                            f5 = fArr2[i17] + f21;
                            f6 = f26;
                            f20 += f4;
                            f17 = f6;
                            f16 = f3;
                            f21 = f5;
                        } else if (c2 != 'h') {
                            if (c2 != 'q') {
                                if (c2 == 'v') {
                                    int i19 = i2 + 0;
                                    path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, fArr2[i19]);
                                    f7 = fArr2[i19];
                                } else if (c2 != 'L') {
                                    if (c2 == 'M') {
                                        int i20 = i2 + 0;
                                        f21 = fArr2[i20];
                                        int i21 = i2 + 1;
                                        f20 = fArr2[i21];
                                        if (i2 > 0) {
                                            path.lineTo(fArr2[i20], fArr2[i21]);
                                        } else {
                                            path.moveTo(fArr2[i20], fArr2[i21]);
                                        }
                                    } else if (c2 == 'S') {
                                        if (c == 'c' || c == 's' || c == 'C' || c == 'S') {
                                            f9 = (2.0f * f21) - f16;
                                            f8 = (2.0f * f20) - f17;
                                        } else {
                                            f9 = f21;
                                            f8 = f20;
                                        }
                                        int i22 = i2 + 0;
                                        int i23 = i2 + 1;
                                        int i24 = i2 + 2;
                                        int i25 = i2 + 3;
                                        path.cubicTo(f9, f8, fArr2[i22], fArr2[i23], fArr2[i24], fArr2[i25]);
                                        f16 = fArr2[i22];
                                        f17 = fArr2[i23];
                                        f21 = fArr2[i24];
                                        f20 = fArr2[i25];
                                    } else if (c2 == 'T') {
                                        if (c == 'q' || c == 't' || c == 'Q' || c == 'T') {
                                            f21 = (2.0f * f21) - f16;
                                            f20 = (2.0f * f20) - f17;
                                        }
                                        int i26 = i2 + 0;
                                        int i27 = i2 + 1;
                                        path.quadTo(f21, f20, fArr2[i26], fArr2[i27]);
                                        f14 = fArr2[i26];
                                        f15 = fArr2[i27];
                                        f17 = f20;
                                        f16 = f21;
                                        f19 = f19;
                                        f18 = f18;
                                    } else if (c2 == 'l') {
                                        int i28 = i2 + 0;
                                        int i29 = i2 + 1;
                                        path.rLineTo(fArr2[i28], fArr2[i29]);
                                        f21 += fArr2[i28];
                                        f7 = fArr2[i29];
                                    } else if (c2 == 'm') {
                                        int i30 = i2 + 0;
                                        f21 += fArr2[i30];
                                        int i31 = i2 + 1;
                                        f20 += fArr2[i31];
                                        if (i2 > 0) {
                                            path.rLineTo(fArr2[i30], fArr2[i31]);
                                        } else {
                                            path.rMoveTo(fArr2[i30], fArr2[i31]);
                                        }
                                    } else if (c2 == 's') {
                                        if (c == 'c' || c == 's' || c == 'C' || c == 'S') {
                                            f11 = f21 - f16;
                                            f10 = f20 - f17;
                                        } else {
                                            f11 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                                            f10 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                                        }
                                        int i32 = i2 + 0;
                                        int i33 = i2 + 1;
                                        int i34 = i2 + 2;
                                        int i35 = i2 + 3;
                                        path.rCubicTo(f11, f10, fArr2[i32], fArr2[i33], fArr2[i34], fArr2[i35]);
                                        f3 = fArr2[i32] + f21;
                                        float f27 = fArr2[i33] + f20;
                                        f4 = fArr2[i35];
                                        f5 = fArr2[i34] + f21;
                                        f6 = f27;
                                    } else if (c2 == 't') {
                                        if (c == 'q' || c == 't' || c == 'Q' || c == 'T') {
                                            f13 = f20 - f17;
                                            f12 = f21 - f16;
                                        } else {
                                            f13 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                                            f12 = 0.0f;
                                        }
                                        int i36 = i2 + 0;
                                        int i37 = i2 + 1;
                                        path.rQuadTo(f12, f13, fArr2[i36], fArr2[i37]);
                                        float f28 = f21 + fArr2[i36];
                                        f17 = f13 + f20;
                                        f16 = f12 + f21;
                                        f20 = fArr2[i37] + f20;
                                        f21 = f28;
                                    }
                                    f15 = f20;
                                    f14 = f21;
                                    f19 = f20;
                                    f18 = f21;
                                } else {
                                    int i38 = i2 + 0;
                                    int i39 = i2 + 1;
                                    path.lineTo(fArr2[i38], fArr2[i39]);
                                    f21 = fArr2[i38];
                                    f20 = fArr2[i39];
                                }
                                f20 += f7;
                            } else {
                                int i40 = i2 + 0;
                                int i41 = i2 + 1;
                                int i42 = i2 + 2;
                                int i43 = i2 + 3;
                                path.rQuadTo(fArr2[i40], fArr2[i41], fArr2[i42], fArr2[i43]);
                                f3 = fArr2[i40] + f21;
                                float f29 = fArr2[i41] + f20;
                                f4 = fArr2[i43];
                                f5 = fArr2[i42] + f21;
                                f6 = f29;
                            }
                            f20 += f4;
                            f17 = f6;
                            f16 = f3;
                            f21 = f5;
                        } else {
                            int i44 = i2 + 0;
                            path.rLineTo(fArr2[i44], LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            f21 += fArr2[i44];
                        }
                        f15 = f20;
                        f14 = f21;
                        f19 = f19;
                        f18 = f18;
                    } else {
                        int i45 = i2 + 5;
                        int i46 = i2 + 6;
                        c(path, f21, f20, fArr2[i45] + f21, fArr2[i46] + f20, fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3] != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, fArr2[i2 + 4] != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        float f30 = f21 + fArr2[i45];
                        f = fArr2[i46] + f20;
                        f2 = f30;
                        f17 = f;
                        f16 = f2;
                        f15 = f;
                        f14 = f2;
                        f19 = f19;
                        f18 = f18;
                    }
                    i2 += i;
                    c = c2;
                } else {
                    fArr[0] = f21;
                    fArr[1] = f20;
                    fArr[2] = f16;
                    fArr[3] = f17;
                    fArr[4] = f18;
                    fArr[5] = f19;
                    return;
                }
            }
        }

        @DexIgnore
        public static void b(Path path, double d, double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9) {
            int ceil = (int) Math.ceil(Math.abs((4.0d * d9) / 3.141592653589793d));
            double cos = Math.cos(d7);
            double sin = Math.sin(d7);
            double cos2 = Math.cos(d8);
            double sin2 = Math.sin(d8);
            double d10 = -d3;
            double d11 = d10 * cos;
            double d12 = d4 * sin;
            double d13 = d10 * sin;
            double d14 = d4 * cos;
            double d15 = d9 / ((double) ceil);
            double d16 = (sin2 * d13) + (cos2 * d14);
            double d17 = (sin2 * d11) - (cos2 * d12);
            int i = 0;
            while (i < ceil) {
                double d18 = d8 + d15;
                double sin3 = Math.sin(d18);
                double cos3 = Math.cos(d18);
                double d19 = (((d3 * cos) * cos3) + d) - (d12 * sin3);
                double d20 = (d3 * sin * cos3) + d2 + (d14 * sin3);
                double d21 = (d11 * sin3) - (d12 * cos3);
                double d22 = (cos3 * d14) + (sin3 * d13);
                double d23 = d18 - d8;
                double tan = Math.tan(d23 / 2.0d);
                double sin4 = (Math.sin(d23) * (Math.sqrt((tan * (3.0d * tan)) + 4.0d) - 1.0d)) / 3.0d;
                path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                path.cubicTo((float) ((d17 * sin4) + d5), (float) ((d16 * sin4) + d6), (float) (d19 - (sin4 * d21)), (float) (d20 - (sin4 * d22)), (float) d19, (float) d20);
                i++;
                d8 = d18;
                d6 = d20;
                d5 = d19;
                d16 = d22;
                d17 = d21;
            }
        }

        @DexIgnore
        public static void c(Path path, float f, float f2, float f3, float f4, float f5, float f6, float f7, boolean z, boolean z2) {
            double d;
            double d2;
            double radians = Math.toRadians((double) f7);
            double cos = Math.cos(radians);
            double sin = Math.sin(radians);
            double d3 = (double) f;
            double d4 = (double) f2;
            double d5 = (double) f5;
            double d6 = ((d3 * cos) + (d4 * sin)) / d5;
            double d7 = (double) f6;
            double d8 = ((((double) (-f)) * sin) + (d4 * cos)) / d7;
            double d9 = (double) f4;
            double d10 = ((((double) f3) * cos) + (d9 * sin)) / d5;
            double d11 = ((((double) (-f3)) * sin) + (d9 * cos)) / d7;
            double d12 = d6 - d10;
            double d13 = d8 - d11;
            double d14 = (d6 + d10) / 2.0d;
            double d15 = (d8 + d11) / 2.0d;
            double d16 = (d12 * d12) + (d13 * d13);
            if (d16 == 0.0d) {
                Log.w("PathParser", " Points are coincident");
                return;
            }
            double d17 = (1.0d / d16) - 0.25d;
            if (d17 < 0.0d) {
                Log.w("PathParser", "Points are too far apart " + d16);
                float sqrt = (float) (Math.sqrt(d16) / 1.99999d);
                c(path, f, f2, f3, f4, f5 * sqrt, f6 * sqrt, f7, z, z2);
                return;
            }
            double sqrt2 = Math.sqrt(d17);
            double d18 = d12 * sqrt2;
            double d19 = sqrt2 * d13;
            if (z == z2) {
                d = d14 - d19;
                d2 = d15 + d18;
            } else {
                d = d19 + d14;
                d2 = d15 - d18;
            }
            double atan2 = Math.atan2(d8 - d2, d6 - d);
            double atan22 = Math.atan2(d11 - d2, d10 - d) - atan2;
            int i = (atan22 > 0.0d ? 1 : (atan22 == 0.0d ? 0 : -1));
            if (z2 != (i >= 0)) {
                atan22 = i > 0 ? atan22 - 6.283185307179586d : atan22 + 6.283185307179586d;
            }
            double d20 = d * d5;
            double d21 = d2 * d7;
            b(path, (d20 * cos) - (d21 * sin), (d21 * cos) + (d20 * sin), d5, d7, d3, d4, radians, atan2, atan22);
        }

        @DexIgnore
        public static void e(b[] bVarArr, Path path) {
            float[] fArr = new float[6];
            char c = 'm';
            for (int i = 0; i < bVarArr.length; i++) {
                a(path, fArr, c, bVarArr[i].f3125a, bVarArr[i].b);
                c = bVarArr[i].f3125a;
            }
        }

        @DexIgnore
        public void d(b bVar, b bVar2, float f) {
            this.f3125a = (char) bVar.f3125a;
            int i = 0;
            while (true) {
                float[] fArr = bVar.b;
                if (i < fArr.length) {
                    this.b[i] = (fArr[i] * (1.0f - f)) + (bVar2.b[i] * f);
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public static void a(ArrayList<b> arrayList, char c, float[] fArr) {
        arrayList.add(new b(c, fArr));
    }

    @DexIgnore
    public static boolean b(b[] bVarArr, b[] bVarArr2) {
        if (bVarArr == null || bVarArr2 == null || bVarArr.length != bVarArr2.length) {
            return false;
        }
        for (int i = 0; i < bVarArr.length; i++) {
            if (!(bVarArr[i].f3125a == bVarArr2[i].f3125a && bVarArr[i].b.length == bVarArr2[i].b.length)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static float[] c(float[] fArr, int i, int i2) {
        if (i <= i2) {
            int length = fArr.length;
            if (i < 0 || i > length) {
                throw new ArrayIndexOutOfBoundsException();
            }
            int i3 = i2 - i;
            int min = Math.min(i3, length - i);
            float[] fArr2 = new float[i3];
            System.arraycopy(fArr, i, fArr2, 0, min);
            return fArr2;
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public static b[] d(String str) {
        if (str == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int i = 0;
        int i2 = 1;
        while (i2 < str.length()) {
            int i3 = i(str, i2);
            String trim = str.substring(i, i3).trim();
            if (trim.length() > 0) {
                a(arrayList, trim.charAt(0), h(trim));
            }
            i2 = i3 + 1;
            i = i3;
        }
        if (i2 - i == 1 && i < str.length()) {
            a(arrayList, str.charAt(i), new float[0]);
        }
        return (b[]) arrayList.toArray(new b[arrayList.size()]);
    }

    @DexIgnore
    public static Path e(String str) {
        Path path = new Path();
        b[] d = d(str);
        if (d == null) {
            return null;
        }
        try {
            b.e(d, path);
            return path;
        } catch (RuntimeException e) {
            throw new RuntimeException("Error in parsing " + str, e);
        }
    }

    @DexIgnore
    public static b[] f(b[] bVarArr) {
        if (bVarArr == null) {
            return null;
        }
        b[] bVarArr2 = new b[bVarArr.length];
        for (int i = 0; i < bVarArr.length; i++) {
            bVarArr2[i] = new b(bVarArr[i]);
        }
        return bVarArr2;
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003a A[LOOP:0: B:1:0x0008->B:22:0x003a, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0024 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void g(java.lang.String r8, int r9, com.fossil.rl0.a r10) {
        /*
            r5 = 1
            r1 = 0
            r10.b = r1
            r0 = r1
            r2 = r1
            r3 = r1
            r4 = r9
        L_0x0008:
            int r6 = r8.length()
            if (r4 >= r6) goto L_0x0024
            char r6 = r8.charAt(r4)
            r7 = 32
            if (r6 == r7) goto L_0x002e
            r7 = 69
            if (r6 == r7) goto L_0x0038
            r7 = 101(0x65, float:1.42E-43)
            if (r6 == r7) goto L_0x0038
            switch(r6) {
                case 44: goto L_0x002e;
                case 45: goto L_0x0031;
                case 46: goto L_0x0027;
                default: goto L_0x0021;
            }
        L_0x0021:
            r3 = r1
        L_0x0022:
            if (r0 == 0) goto L_0x003a
        L_0x0024:
            r10.f3124a = r4
            return
        L_0x0027:
            if (r2 != 0) goto L_0x002c
            r2 = r5
            r3 = r1
            goto L_0x0022
        L_0x002c:
            r10.b = r5
        L_0x002e:
            r0 = r5
            r3 = r1
            goto L_0x0022
        L_0x0031:
            if (r4 == r9) goto L_0x0021
            if (r3 != 0) goto L_0x0021
            r10.b = r5
            goto L_0x002e
        L_0x0038:
            r3 = r5
            goto L_0x0022
        L_0x003a:
            int r4 = r4 + 1
            goto L_0x0008
            switch-data {44->0x002e, 45->0x0031, 46->0x0027, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rl0.g(java.lang.String, int, com.fossil.rl0$a):void");
    }

    @DexIgnore
    public static float[] h(String str) {
        int i = 0;
        if (str.charAt(0) == 'z' || str.charAt(0) == 'Z') {
            return new float[0];
        }
        try {
            float[] fArr = new float[str.length()];
            a aVar = new a();
            int length = str.length();
            int i2 = 1;
            while (i2 < length) {
                g(str, i2, aVar);
                int i3 = aVar.f3124a;
                if (i2 < i3) {
                    fArr[i] = Float.parseFloat(str.substring(i2, i3));
                    i++;
                }
                i2 = aVar.b ? i3 : i3 + 1;
            }
            return c(fArr, 0, i);
        } catch (NumberFormatException e) {
            throw new RuntimeException("error in parsing \"" + str + "\"", e);
        }
    }

    @DexIgnore
    public static int i(String str, int i) {
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (((charAt - 'A') * (charAt - 'Z') <= 0 || (charAt - 'a') * (charAt - 'z') <= 0) && charAt != 'e' && charAt != 'E') {
                break;
            }
            i++;
        }
        return i;
    }

    @DexIgnore
    public static void j(b[] bVarArr, b[] bVarArr2) {
        for (int i = 0; i < bVarArr2.length; i++) {
            bVarArr[i].f3125a = (char) bVarArr2[i].f3125a;
            for (int i2 = 0; i2 < bVarArr2[i].b.length; i2++) {
                bVarArr[i].b[i2] = bVarArr2[i].b[i2];
            }
        }
    }
}
