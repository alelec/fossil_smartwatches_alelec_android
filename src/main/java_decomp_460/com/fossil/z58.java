package com.fossil;

import com.zendesk.sdk.network.impl.DeviceInfo;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z58 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ BigInteger f4420a;
    @DexIgnore
    public static /* final */ BigInteger b;
    @DexIgnore
    public static /* final */ BigInteger c;
    @DexIgnore
    public static /* final */ BigInteger d;
    @DexIgnore
    public static /* final */ BigInteger e;
    @DexIgnore
    public static /* final */ BigInteger f;

    /*
    static {
        BigInteger valueOf = BigInteger.valueOf(DeviceInfo.BYTES_MULTIPLIER);
        f4420a = valueOf;
        BigInteger multiply = valueOf.multiply(valueOf);
        b = multiply;
        BigInteger multiply2 = f4420a.multiply(multiply);
        c = multiply2;
        BigInteger multiply3 = f4420a.multiply(multiply2);
        d = multiply3;
        BigInteger multiply4 = f4420a.multiply(multiply3);
        e = multiply4;
        f4420a.multiply(multiply4);
        BigInteger multiply5 = BigInteger.valueOf(DeviceInfo.BYTES_MULTIPLIER).multiply(BigInteger.valueOf(1152921504606846976L));
        f = multiply5;
        f4420a.multiply(multiply5);
        Charset.forName("UTF-8");
    }
    */

    @DexIgnore
    public static FileInputStream a(File file) throws IOException {
        if (!file.exists()) {
            throw new FileNotFoundException("File '" + file + "' does not exist");
        } else if (file.isDirectory()) {
            throw new IOException("File '" + file + "' exists but is a directory");
        } else if (file.canRead()) {
            return new FileInputStream(file);
        } else {
            throw new IOException("File '" + file + "' cannot be read");
        }
    }

    @DexIgnore
    public static FileOutputStream b(File file, boolean z) throws IOException {
        if (!file.exists()) {
            File parentFile = file.getParentFile();
            if (parentFile != null && !parentFile.mkdirs() && !parentFile.isDirectory()) {
                throw new IOException("Directory '" + parentFile + "' could not be created");
            }
        } else if (file.isDirectory()) {
            throw new IOException("File '" + file + "' exists but is a directory");
        } else if (!file.canWrite()) {
            throw new IOException("File '" + file + "' cannot be written to");
        }
        return new FileOutputStream(file, z);
    }

    @DexIgnore
    public static byte[] c(File file) throws IOException {
        Throwable th;
        FileInputStream fileInputStream;
        try {
            fileInputStream = a(file);
            try {
                byte[] i = b68.i(fileInputStream, file.length());
                b68.b(fileInputStream);
                return i;
            } catch (Throwable th2) {
                th = th2;
                b68.b(fileInputStream);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            fileInputStream = null;
            b68.b(fileInputStream);
            throw th;
        }
    }

    @DexIgnore
    public static void d(File file, byte[] bArr) throws IOException {
        e(file, bArr, false);
    }

    @DexIgnore
    public static void e(File file, byte[] bArr, boolean z) throws IOException {
        Throwable th;
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = b(file, z);
            try {
                fileOutputStream.write(bArr);
                fileOutputStream.close();
                b68.c(fileOutputStream);
            } catch (Throwable th2) {
                th = th2;
                b68.c(fileOutputStream);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            fileOutputStream = null;
            b68.c(fileOutputStream);
            throw th;
        }
    }
}
