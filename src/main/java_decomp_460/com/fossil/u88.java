package com.fossil;

import com.facebook.appevents.codeless.CodelessMatcher;
import com.j256.ormlite.stmt.query.SimpleComparison;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u88 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Type[] f3551a; // = new Type[0];

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements GenericArrayType {
        @DexIgnore
        public /* final */ Type b;

        @DexIgnore
        public a(Type type) {
            this.b = type;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return (obj instanceof GenericArrayType) && u88.e(this, (GenericArrayType) obj);
        }

        @DexIgnore
        public Type getGenericComponentType() {
            return this.b;
        }

        @DexIgnore
        public int hashCode() {
            return this.b.hashCode();
        }

        @DexIgnore
        public String toString() {
            return u88.u(this.b) + "[]";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ParameterizedType {
        @DexIgnore
        public /* final */ Type b;
        @DexIgnore
        public /* final */ Type c;
        @DexIgnore
        public /* final */ Type[] d;

        @DexIgnore
        public b(Type type, Type type2, Type... typeArr) {
            boolean z = true;
            if (type2 instanceof Class) {
                if ((type == null) != (((Class) type2).getEnclosingClass() != null ? false : z)) {
                    throw new IllegalArgumentException();
                }
            }
            for (Type type3 : typeArr) {
                u88.b(type3, "typeArgument == null");
                u88.c(type3);
            }
            this.b = type;
            this.c = type2;
            this.d = (Type[]) typeArr.clone();
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return (obj instanceof ParameterizedType) && u88.e(this, (ParameterizedType) obj);
        }

        @DexIgnore
        public Type[] getActualTypeArguments() {
            return (Type[]) this.d.clone();
        }

        @DexIgnore
        public Type getOwnerType() {
            return this.b;
        }

        @DexIgnore
        public Type getRawType() {
            return this.c;
        }

        @DexIgnore
        public int hashCode() {
            int hashCode = Arrays.hashCode(this.d);
            int hashCode2 = this.c.hashCode();
            Type type = this.b;
            return (type != null ? type.hashCode() : 0) ^ (hashCode ^ hashCode2);
        }

        @DexIgnore
        public String toString() {
            Type[] typeArr = this.d;
            if (typeArr.length == 0) {
                return u88.u(this.c);
            }
            int length = typeArr.length;
            StringBuilder sb = new StringBuilder((length + 1) * 30);
            sb.append(u88.u(this.c));
            sb.append(SimpleComparison.LESS_THAN_OPERATION);
            sb.append(u88.u(this.d[0]));
            for (int i = 1; i < this.d.length; i++) {
                sb.append(", ");
                sb.append(u88.u(this.d[i]));
            }
            sb.append(SimpleComparison.GREATER_THAN_OPERATION);
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements WildcardType {
        @DexIgnore
        public /* final */ Type b;
        @DexIgnore
        public /* final */ Type c;

        @DexIgnore
        public c(Type[] typeArr, Type[] typeArr2) {
            if (typeArr2.length > 1) {
                throw new IllegalArgumentException();
            } else if (typeArr.length != 1) {
                throw new IllegalArgumentException();
            } else if (typeArr2.length == 1) {
                if (typeArr2[0] != null) {
                    u88.c(typeArr2[0]);
                    if (typeArr[0] == Object.class) {
                        this.c = typeArr2[0];
                        this.b = Object.class;
                        return;
                    }
                    throw new IllegalArgumentException();
                }
                throw null;
            } else if (typeArr[0] != null) {
                u88.c(typeArr[0]);
                this.c = null;
                this.b = typeArr[0];
            } else {
                throw null;
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return (obj instanceof WildcardType) && u88.e(this, (WildcardType) obj);
        }

        @DexIgnore
        public Type[] getLowerBounds() {
            Type type = this.c;
            if (type == null) {
                return u88.f3551a;
            }
            return new Type[]{type};
        }

        @DexIgnore
        public Type[] getUpperBounds() {
            return new Type[]{this.b};
        }

        @DexIgnore
        public int hashCode() {
            Type type = this.c;
            return (type != null ? type.hashCode() + 31 : 1) ^ (this.b.hashCode() + 31);
        }

        @DexIgnore
        public String toString() {
            if (this.c != null) {
                return "? super " + u88.u(this.c);
            } else if (this.b == Object.class) {
                return "?";
            } else {
                return "? extends " + u88.u(this.b);
            }
        }
    }

    @DexIgnore
    public static w18 a(w18 w18) throws IOException {
        i48 i48 = new i48();
        w18.source().e0(i48);
        return w18.create(w18.contentType(), w18.contentLength(), i48);
    }

    @DexIgnore
    public static <T> T b(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    @DexIgnore
    public static void c(Type type) {
        if ((type instanceof Class) && ((Class) type).isPrimitive()) {
            throw new IllegalArgumentException();
        }
    }

    @DexIgnore
    public static Class<?> d(TypeVariable<?> typeVariable) {
        GenericDeclaration genericDeclaration = typeVariable.getGenericDeclaration();
        if (genericDeclaration instanceof Class) {
            return (Class) genericDeclaration;
        }
        return null;
    }

    @DexIgnore
    public static boolean e(Type type, Type type2) {
        boolean z = true;
        if (type == type2) {
            return true;
        }
        if (type instanceof Class) {
            return type.equals(type2);
        }
        if (type instanceof ParameterizedType) {
            if (!(type2 instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType parameterizedType = (ParameterizedType) type;
            ParameterizedType parameterizedType2 = (ParameterizedType) type2;
            Type ownerType = parameterizedType.getOwnerType();
            Type ownerType2 = parameterizedType2.getOwnerType();
            if ((ownerType != ownerType2 && (ownerType == null || !ownerType.equals(ownerType2))) || !parameterizedType.getRawType().equals(parameterizedType2.getRawType()) || !Arrays.equals(parameterizedType.getActualTypeArguments(), parameterizedType2.getActualTypeArguments())) {
                z = false;
            }
            return z;
        } else if (type instanceof GenericArrayType) {
            if (type2 instanceof GenericArrayType) {
                return e(((GenericArrayType) type).getGenericComponentType(), ((GenericArrayType) type2).getGenericComponentType());
            }
            return false;
        } else if (type instanceof WildcardType) {
            if (!(type2 instanceof WildcardType)) {
                return false;
            }
            WildcardType wildcardType = (WildcardType) type;
            WildcardType wildcardType2 = (WildcardType) type2;
            if (!Arrays.equals(wildcardType.getUpperBounds(), wildcardType2.getUpperBounds()) || !Arrays.equals(wildcardType.getLowerBounds(), wildcardType2.getLowerBounds())) {
                z = false;
            }
            return z;
        } else if (!(type instanceof TypeVariable) || !(type2 instanceof TypeVariable)) {
            return false;
        } else {
            TypeVariable typeVariable = (TypeVariable) type;
            TypeVariable typeVariable2 = (TypeVariable) type2;
            if (typeVariable.getGenericDeclaration() != typeVariable2.getGenericDeclaration() || !typeVariable.getName().equals(typeVariable2.getName())) {
                z = false;
            }
            return z;
        }
    }

    @DexIgnore
    public static Type f(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2 == cls) {
            return type;
        }
        if (cls2.isInterface()) {
            Class<?>[] interfaces = cls.getInterfaces();
            int length = interfaces.length;
            for (int i = 0; i < length; i++) {
                if (interfaces[i] == cls2) {
                    return cls.getGenericInterfaces()[i];
                }
                if (cls2.isAssignableFrom(interfaces[i])) {
                    return f(cls.getGenericInterfaces()[i], interfaces[i], cls2);
                }
            }
        }
        if (!cls.isInterface()) {
            while (cls != Object.class) {
                Class<? super Object> superclass = cls.getSuperclass();
                if (superclass == cls2) {
                    return cls.getGenericSuperclass();
                }
                if (cls2.isAssignableFrom(superclass)) {
                    return f(cls.getGenericSuperclass(), superclass, cls2);
                }
                cls = superclass;
            }
        }
        return cls2;
    }

    @DexIgnore
    public static Type g(int i, ParameterizedType parameterizedType) {
        Type type = parameterizedType.getActualTypeArguments()[i];
        return type instanceof WildcardType ? ((WildcardType) type).getLowerBounds()[0] : type;
    }

    @DexIgnore
    public static Type h(int i, ParameterizedType parameterizedType) {
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        if (i < 0 || i >= actualTypeArguments.length) {
            throw new IllegalArgumentException("Index " + i + " not in range [0," + actualTypeArguments.length + ") for " + parameterizedType);
        }
        Type type = actualTypeArguments[i];
        return type instanceof WildcardType ? ((WildcardType) type).getUpperBounds()[0] : type;
    }

    @DexIgnore
    public static Class<?> i(Type type) {
        b(type, "type == null");
        if (type instanceof Class) {
            return (Class) type;
        }
        if (type instanceof ParameterizedType) {
            Type rawType = ((ParameterizedType) type).getRawType();
            if (rawType instanceof Class) {
                return (Class) rawType;
            }
            throw new IllegalArgumentException();
        } else if (type instanceof GenericArrayType) {
            return Array.newInstance(i(((GenericArrayType) type).getGenericComponentType()), 0).getClass();
        } else {
            if (type instanceof TypeVariable) {
                return Object.class;
            }
            if (type instanceof WildcardType) {
                return i(((WildcardType) type).getUpperBounds()[0]);
            }
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + type.getClass().getName());
        }
    }

    @DexIgnore
    public static Type j(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2.isAssignableFrom(cls)) {
            return r(type, cls, f(type, cls, cls2));
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public static boolean k(Type type) {
        if (type instanceof Class) {
            return false;
        }
        if (type instanceof ParameterizedType) {
            for (Type type2 : ((ParameterizedType) type).getActualTypeArguments()) {
                if (k(type2)) {
                    return true;
                }
            }
            return false;
        } else if (type instanceof GenericArrayType) {
            return k(((GenericArrayType) type).getGenericComponentType());
        } else {
            if (type instanceof TypeVariable) {
                return true;
            }
            if (type instanceof WildcardType) {
                return true;
            }
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + (type == null ? "null" : type.getClass().getName()));
        }
    }

    @DexIgnore
    public static int l(Object[] objArr, Object obj) {
        for (int i = 0; i < objArr.length; i++) {
            if (obj.equals(objArr[i])) {
                return i;
            }
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public static boolean m(Annotation[] annotationArr, Class<? extends Annotation> cls) {
        for (Annotation annotation : annotationArr) {
            if (cls.isInstance(annotation)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static RuntimeException n(Method method, String str, Object... objArr) {
        return o(method, null, str, objArr);
    }

    @DexIgnore
    public static RuntimeException o(Method method, Throwable th, String str, Object... objArr) {
        String format = String.format(str, objArr);
        return new IllegalArgumentException(format + "\n    for method " + method.getDeclaringClass().getSimpleName() + CodelessMatcher.CURRENT_CLASS_NAME + method.getName(), th);
    }

    @DexIgnore
    public static RuntimeException p(Method method, int i, String str, Object... objArr) {
        return n(method, str + " (parameter #" + (i + 1) + ")", objArr);
    }

    @DexIgnore
    public static RuntimeException q(Method method, Throwable th, int i, String str, Object... objArr) {
        return o(method, th, str + " (parameter #" + (i + 1) + ")", objArr);
    }

    @DexIgnore
    public static Type r(Type type, Class<?> cls, Type type2) {
        boolean z;
        while (type2 instanceof TypeVariable) {
            TypeVariable typeVariable = (TypeVariable) type2;
            type2 = s(type, cls, typeVariable);
            if (type2 == typeVariable) {
                return type2;
            }
        }
        if (type2 instanceof Class) {
            Class cls2 = (Class) type2;
            if (cls2.isArray()) {
                Class<?> componentType = cls2.getComponentType();
                Type r = r(type, cls, componentType);
                Type type3 = cls2;
                if (componentType != r) {
                    type3 = new a(r);
                }
                return type3;
            }
        }
        if (type2 instanceof GenericArrayType) {
            GenericArrayType genericArrayType = (GenericArrayType) type2;
            Type genericComponentType = genericArrayType.getGenericComponentType();
            Type r2 = r(type, cls, genericComponentType);
            return genericComponentType != r2 ? new a(r2) : genericArrayType;
        } else if (type2 instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type2;
            Type ownerType = parameterizedType.getOwnerType();
            Type r3 = r(type, cls, ownerType);
            boolean z2 = r3 != ownerType;
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            int length = actualTypeArguments.length;
            int i = 0;
            while (i < length) {
                Type r4 = r(type, cls, actualTypeArguments[i]);
                if (r4 != actualTypeArguments[i]) {
                    if (!z2) {
                        z = true;
                        actualTypeArguments = (Type[]) actualTypeArguments.clone();
                    } else {
                        z = z2;
                    }
                    actualTypeArguments[i] = r4;
                } else {
                    z = z2;
                }
                i++;
                z2 = z;
            }
            return z2 ? new b(r3, parameterizedType.getRawType(), actualTypeArguments) : parameterizedType;
        } else if (!(type2 instanceof WildcardType)) {
            return type2;
        } else {
            WildcardType wildcardType = (WildcardType) type2;
            Type[] lowerBounds = wildcardType.getLowerBounds();
            Type[] upperBounds = wildcardType.getUpperBounds();
            if (lowerBounds.length == 1) {
                Type r5 = r(type, cls, lowerBounds[0]);
                if (r5 == lowerBounds[0]) {
                    return wildcardType;
                }
                return new c(new Type[]{Object.class}, new Type[]{r5});
            } else if (upperBounds.length != 1) {
                return wildcardType;
            } else {
                Type r6 = r(type, cls, upperBounds[0]);
                if (r6 == upperBounds[0]) {
                    return wildcardType;
                }
                return new c(new Type[]{r6}, f3551a);
            }
        }
    }

    @DexIgnore
    public static Type s(Type type, Class<?> cls, TypeVariable<?> typeVariable) {
        Class<?> d = d(typeVariable);
        if (d == null) {
            return typeVariable;
        }
        Type f = f(type, cls, d);
        if (!(f instanceof ParameterizedType)) {
            return typeVariable;
        }
        return ((ParameterizedType) f).getActualTypeArguments()[l(d.getTypeParameters(), typeVariable)];
    }

    @DexIgnore
    public static void t(Throwable th) {
        if (th instanceof VirtualMachineError) {
            throw ((VirtualMachineError) th);
        } else if (th instanceof ThreadDeath) {
            throw ((ThreadDeath) th);
        } else if (th instanceof LinkageError) {
            throw ((LinkageError) th);
        }
    }

    @DexIgnore
    public static String u(Type type) {
        return type instanceof Class ? ((Class) type).getName() : type.toString();
    }

    @DexIgnore
    public static <T> void v(Class<T> cls) {
        if (!cls.isInterface()) {
            throw new IllegalArgumentException("API declarations must be interfaces.");
        } else if (cls.getInterfaces().length > 0) {
            throw new IllegalArgumentException("API interfaces must not extend other interfaces.");
        }
    }
}
