package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wx7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ThreadLocal<hw7> f4016a; // = new ThreadLocal<>();
    @DexIgnore
    public static /* final */ wx7 b; // = new wx7();

    @DexIgnore
    public final hw7 a() {
        return f4016a.get();
    }

    @DexIgnore
    public final hw7 b() {
        hw7 hw7 = f4016a.get();
        if (hw7 != null) {
            return hw7;
        }
        hw7 a2 = kw7.a();
        f4016a.set(a2);
        return a2;
    }

    @DexIgnore
    public final void c() {
        f4016a.set(null);
    }

    @DexIgnore
    public final void d(hw7 hw7) {
        f4016a.set(hw7);
    }
}
