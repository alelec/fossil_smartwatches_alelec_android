package com.fossil;

import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wb6 implements MembersInjector<HybridCustomizeEditActivity> {
    @DexIgnore
    public static void a(HybridCustomizeEditActivity hybridCustomizeEditActivity, ec6 ec6) {
        hybridCustomizeEditActivity.A = ec6;
    }

    @DexIgnore
    public static void b(HybridCustomizeEditActivity hybridCustomizeEditActivity, po4 po4) {
        hybridCustomizeEditActivity.B = po4;
    }
}
