package com.fossil;

import com.fossil.jn5;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l66 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2148a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<n66> c;
    @DexIgnore
    public List<? extends jn5.a> d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public l66(String str, String str2, ArrayList<n66> arrayList, List<? extends jn5.a> list, boolean z) {
        pq7.c(str, "mPresetId");
        pq7.c(str2, "mPresetName");
        pq7.c(arrayList, "mWatchApps");
        pq7.c(list, "mPermissionFeatures");
        this.f2148a = str;
        this.b = str2;
        this.c = arrayList;
        this.d = list;
        this.e = z;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final ArrayList<n66> b() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof l66) {
                l66 l66 = (l66) obj;
                if (!pq7.a(this.f2148a, l66.f2148a) || !pq7.a(this.b, l66.b) || !pq7.a(this.c, l66.c) || !pq7.a(this.d, l66.d) || this.e != l66.e) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f2148a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        ArrayList<n66> arrayList = this.c;
        int hashCode3 = arrayList != null ? arrayList.hashCode() : 0;
        List<? extends jn5.a> list = this.d;
        if (list != null) {
            i = list.hashCode();
        }
        boolean z = this.e;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "DianaPresetConfigWrapper(mPresetId=" + this.f2148a + ", mPresetName=" + this.b + ", mWatchApps=" + this.c + ", mPermissionFeatures=" + this.d + ", mIsActive=" + this.e + ")";
    }
}
