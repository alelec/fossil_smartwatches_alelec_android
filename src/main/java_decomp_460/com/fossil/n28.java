package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n28 extends RuntimeException {
    @DexIgnore
    public IOException firstException;
    @DexIgnore
    public IOException lastException;

    @DexIgnore
    public n28(IOException iOException) {
        super(iOException);
        this.firstException = iOException;
        this.lastException = iOException;
    }

    @DexIgnore
    public void addConnectException(IOException iOException) {
        b28.a(this.firstException, iOException);
        this.lastException = iOException;
    }

    @DexIgnore
    public IOException getFirstConnectException() {
        return this.firstException;
    }

    @DexIgnore
    public IOException getLastConnectException() {
        return this.lastException;
    }
}
