package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.x37;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xv5 extends pv5 implements zw6 {
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public yw6 g;
    @DexIgnore
    public g37<pa5> h;
    @DexIgnore
    public sv5 i;
    @DexIgnore
    public kx6 j;
    @DexIgnore
    public z67 k;
    @DexIgnore
    public Integer l;
    @DexIgnore
    public /* final */ String m; // = qn5.l.a().d("primaryColor");
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final xv5 a() {
            return new xv5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xv5 f4186a;

        @DexIgnore
        public b(xv5 xv5) {
            this.f4186a = xv5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            xv5.K6(this.f4186a).y(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xv5 f4187a;

        @DexIgnore
        public c(xv5 xv5) {
            this.f4187a = xv5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            xv5.K6(this.f4187a).x(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xv5 f4188a;

        @DexIgnore
        public d(xv5 xv5) {
            this.f4188a = xv5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            xv5.K6(this.f4188a).w(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xv5 f4189a;

        @DexIgnore
        public e(xv5 xv5) {
            this.f4189a = xv5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            xv5.K6(this.f4189a).v(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xv5 b;

        @DexIgnore
        public f(xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            xv5.K6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ xv5 b;

        @DexIgnore
        public g(xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            xv5.K6(this.b).r(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ xv5 b;

        @DexIgnore
        public h(xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            xv5.K6(this.b).s(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ xv5 b;

        @DexIgnore
        public i(xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            xv5.K6(this.b).u(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xv5 b;

        @DexIgnore
        public j(xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pa5 b;
        @DexIgnore
        public /* final */ /* synthetic */ xv5 c;

        @DexIgnore
        public k(pa5 pa5, xv5 xv5) {
            this.b = pa5;
            this.c = xv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FlexibleTextInputEditText flexibleTextInputEditText = this.b.v;
            pq7.b(flexibleTextInputEditText, "binding.etBirthday");
            Editable text = flexibleTextInputEditText.getText();
            if (text == null || text.length() == 0) {
                xv5.K6(this.c).A();
            } else {
                xv5.K6(this.c).z();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xv5 b;

        @DexIgnore
        public l(xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            xv5.K6(this.b).t(qh5.MALE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xv5 b;

        @DexIgnore
        public m(xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            xv5.K6(this.b).t(qh5.FEMALE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xv5 b;

        @DexIgnore
        public n(xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            xv5.K6(this.b).t(qh5.OTHER);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xv5 f4190a;

        @DexIgnore
        public o(xv5 xv5) {
            this.f4190a = xv5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            xv5.K6(this.f4190a).q(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p<T> implements ls0<Date> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xv5 f4191a;

        @DexIgnore
        public p(xv5 xv5) {
            this.f4191a = xv5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Date date) {
            if (date != null) {
                Calendar o = xv5.K6(this.f4191a).o();
                o.setTime(date);
                xv5.K6(this.f4191a).p(date, o);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ yw6 K6(xv5 xv5) {
        yw6 yw6 = xv5.g;
        if (yw6 != null) {
            return yw6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "ProfileSetupFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void H4(Spanned spanned) {
        pq7.c(spanned, "message");
        if (isActive()) {
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.H;
                    pq7.b(flexibleTextView, "binding.ftvThree");
                    flexibleTextView.setText(spanned);
                    Integer num = this.l;
                    if (num != null) {
                        a2.H.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void I3(SignUpEmailAuth signUpEmailAuth) {
        pq7.c(signUpEmailAuth, "auth");
        if (isActive()) {
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    pq7.b(flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(um5.c(PortfolioApp.h0.c(), 2131886968));
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.K;
                    pq7.b(flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setVisibility(8);
                    RTLImageView rTLImageView = a2.O;
                    pq7.b(rTLImageView, "it.ivCheckedEmail");
                    rTLImageView.setVisibility(8);
                    if (!TextUtils.isEmpty(signUpEmailAuth.getEmail())) {
                        a2.w.setText(signUpEmailAuth.getEmail());
                        FlexibleTextInputEditText flexibleTextInputEditText = a2.w;
                        pq7.b(flexibleTextInputEditText, "it.etEmail");
                        flexibleTextInputEditText.setKeyListener(null);
                        FlexibleTextInputLayout flexibleTextInputLayout2 = a2.K;
                        pq7.b(flexibleTextInputLayout2, "it.inputEmail");
                        flexibleTextInputLayout2.setFocusable(false);
                        FlexibleTextInputLayout flexibleTextInputLayout3 = a2.K;
                        pq7.b(flexibleTextInputLayout3, "it.inputEmail");
                        flexibleTextInputLayout3.setEnabled(true);
                        return;
                    }
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void K4(boolean z, String str) {
        pq7.c(str, "message");
        if (isActive()) {
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.J;
                    pq7.b(flexibleTextInputLayout, "it.inputBirthday");
                    flexibleTextInputLayout.setErrorEnabled(z);
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.J;
                    pq7.b(flexibleTextInputLayout2, "it.inputBirthday");
                    flexibleTextInputLayout2.setError(str);
                    RTLImageView rTLImageView = a2.N;
                    pq7.b(rTLImageView, "it.ivCheckedBirthday");
                    rTLImageView.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void L3(boolean z) {
        RTLImageView rTLImageView;
        if (isActive()) {
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null && (rTLImageView = a2.P) != null) {
                    pq7.b(rTLImageView, "it");
                    rTLImageView.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void L6(TextView textView) {
        Drawable f2 = gl0.f(PortfolioApp.h0.c(), 2131230836);
        textView.setTextColor(gl0.d(PortfolioApp.h0.c(), 2131099707));
        textView.setEnabled(false);
        textView.setClickable(false);
        textView.setBackground(f2);
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(yw6 yw6) {
        pq7.c(yw6, "presenter");
        this.g = yw6;
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void Q1() {
        FragmentActivity activity;
        if (isActive() && (activity = getActivity()) != null) {
            OnboardingHeightWeightActivity.a aVar = OnboardingHeightWeightActivity.B;
            pq7.b(activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void Q3(Spanned spanned) {
        pq7.c(spanned, "message");
        if (isActive()) {
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.D;
                    pq7.b(flexibleTextView, "binding.ftvFive");
                    flexibleTextView.setText(spanned);
                    Integer num = this.l;
                    if (num != null) {
                        a2.D.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void T5(int i2, String str) {
        pq7.c(str, "errorMessage");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void Y2(Spanned spanned) {
        pq7.c(spanned, "message");
        if (isActive()) {
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.E;
                    pq7.b(flexibleTextView, "binding.ftvFour");
                    flexibleTextView.setText(spanned);
                    Integer num = this.l;
                    if (num != null) {
                        a2.E.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void Z5(String str) {
        FlexibleTextInputEditText flexibleTextInputEditText;
        pq7.c(str, "birthdate");
        if (isActive()) {
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null && (flexibleTextInputEditText = a2.v) != null) {
                    flexibleTextInputEditText.setText(str);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void c6() {
        if (isActive()) {
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    pq7.b(flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setEnabled(false);
                    FlexibleButton flexibleButton2 = a2.z;
                    pq7.b(flexibleButton2, "it.fbCreateAccount");
                    flexibleButton2.setClickable(false);
                    FlexibleButton flexibleButton3 = a2.z;
                    pq7.b(flexibleButton3, "it.fbCreateAccount");
                    flexibleButton3.setFocusable(false);
                    a2.z.d("flexible_button_disabled");
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void e3(SignUpSocialAuth signUpSocialAuth) {
        pq7.c(signUpSocialAuth, "auth");
        if (isActive()) {
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    pq7.b(flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(um5.c(PortfolioApp.h0.c(), 2131886968));
                    a2.x.setText(signUpSocialAuth.getFirstName());
                    a2.y.setText(signUpSocialAuth.getLastName());
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.K;
                    pq7.b(flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setVisibility(0);
                    if (!TextUtils.isEmpty(signUpSocialAuth.getEmail())) {
                        a2.w.setText(signUpSocialAuth.getEmail());
                        FlexibleTextInputEditText flexibleTextInputEditText = a2.w;
                        pq7.b(flexibleTextInputEditText, "it.etEmail");
                        flexibleTextInputEditText.setKeyListener(null);
                        FlexibleTextInputLayout flexibleTextInputLayout2 = a2.K;
                        pq7.b(flexibleTextInputLayout2, "it.inputEmail");
                        flexibleTextInputLayout2.setFocusable(false);
                        FlexibleTextInputLayout flexibleTextInputLayout3 = a2.K;
                        pq7.b(flexibleTextInputLayout3, "it.inputEmail");
                        flexibleTextInputLayout3.setEnabled(true);
                        return;
                    }
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void f() {
        DashBar dashBar;
        g37<pa5> g37 = this.h;
        if (g37 != null) {
            pa5 a2 = g37.a();
            if (a2 != null && (dashBar = a2.S) != null) {
                x37.a aVar = x37.f4036a;
                pq7.b(dashBar, "this");
                aVar.f(dashBar, 500);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void h() {
        if (isActive()) {
            a();
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void i() {
        if (isActive()) {
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886961);
            pq7.b(c2, "LanguageHelper.getString\u2026edTerms_Text__PleaseWait)");
            H6(c2);
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void k3() {
        if (isActive()) {
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    pq7.b(flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setEnabled(true);
                    FlexibleButton flexibleButton2 = a2.z;
                    pq7.b(flexibleButton2, "it.fbCreateAccount");
                    flexibleButton2.setClickable(true);
                    FlexibleButton flexibleButton3 = a2.z;
                    pq7.b(flexibleButton3, "it.fbCreateAccount");
                    flexibleButton3.setFocusable(true);
                    a2.z.d("flexible_button_primary");
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void n0(boolean z, boolean z2, String str) {
        int i2;
        pq7.c(str, "errorMessage");
        if (isActive()) {
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.K;
                    pq7.b(flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setErrorEnabled(z2);
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.K;
                    pq7.b(flexibleTextInputLayout2, "it.inputEmail");
                    flexibleTextInputLayout2.setError(str);
                    RTLImageView rTLImageView = a2.O;
                    pq7.b(rTLImageView, "it.ivCheckedEmail");
                    if (z) {
                        FlexibleTextInputLayout flexibleTextInputLayout3 = a2.K;
                        pq7.b(flexibleTextInputLayout3, "it.inputEmail");
                        if (flexibleTextInputLayout3.getVisibility() == 0) {
                            i2 = 0;
                            rTLImageView.setVisibility(i2);
                            return;
                        }
                    }
                    i2 = 8;
                    rTLImageView.setVisibility(i2);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void n2(MFUser mFUser) {
        Date date;
        pq7.c(mFUser, "user");
        if (isActive()) {
            c6();
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    pq7.b(flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(um5.c(PortfolioApp.h0.c(), 2131886951));
                    p4(qh5.Companion.a(mFUser.getGender()));
                    if (!vt7.l(mFUser.getEmail())) {
                        a2.w.setText(mFUser.getEmail());
                        FlexibleTextInputEditText flexibleTextInputEditText = a2.w;
                        pq7.b(flexibleTextInputEditText, "it.etEmail");
                        L6(flexibleTextInputEditText);
                    }
                    if (!vt7.l(mFUser.getFirstName())) {
                        a2.x.setText(mFUser.getFirstName());
                        FlexibleTextInputEditText flexibleTextInputEditText2 = a2.x;
                        pq7.b(flexibleTextInputEditText2, "it.etFirstName");
                        L6(flexibleTextInputEditText2);
                    }
                    if (!vt7.l(mFUser.getLastName())) {
                        a2.y.setText(mFUser.getLastName());
                        FlexibleTextInputEditText flexibleTextInputEditText3 = a2.y;
                        pq7.b(flexibleTextInputEditText3, "it.etLastName");
                        L6(flexibleTextInputEditText3);
                    }
                    if (!vt7.l(mFUser.getBirthday())) {
                        FlexibleTextInputEditText flexibleTextInputEditText4 = a2.v;
                        pq7.b(flexibleTextInputEditText4, "it.etBirthday");
                        L6(flexibleTextInputEditText4);
                        try {
                            SimpleDateFormat simpleDateFormat = lk5.f2210a.get();
                            if (simpleDateFormat != null) {
                                date = simpleDateFormat.parse(mFUser.getBirthday());
                                yw6 yw6 = this.g;
                                if (yw6 != null) {
                                    Calendar o2 = yw6.o();
                                    o2.setTime(date);
                                    yw6 yw62 = this.g;
                                    if (yw62 == null) {
                                        pq7.n("mPresenter");
                                        throw null;
                                    } else if (date != null) {
                                        yw62.p(date, o2);
                                    } else {
                                        pq7.i();
                                        throw null;
                                    }
                                } else {
                                    pq7.n("mPresenter");
                                    throw null;
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } catch (Exception e2) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            local.e("ProfileSetupFragment", "toOffsetDateTime - e=" + e2);
                            date = new Date();
                        }
                    }
                }
            } else {
                pq7.n("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        g37<pa5> g37 = new g37<>(this, (pa5) aq0.f(layoutInflater, 2131558612, viewGroup, false, A6()));
        this.h = g37;
        if (g37 != null) {
            pa5 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        yw6 yw6 = this.g;
        if (yw6 != null) {
            yw6.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        yw6 yw6 = this.g;
        if (yw6 != null) {
            yw6.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        ts0 a2 = vs0.e(requireActivity()).a(z67.class);
        pq7.b(a2, "ViewModelProviders.of(re\u2026DayViewModel::class.java)");
        this.k = (z67) a2;
        sv5 sv5 = (sv5) getChildFragmentManager().Z(sv5.u.a());
        this.i = sv5;
        if (sv5 == null) {
            this.i = sv5.u.b();
        }
        ro4 M = PortfolioApp.h0.c().M();
        sv5 sv52 = this.i;
        if (sv52 != null) {
            M.C1(new ix6(sv52)).b(this);
            z67 z67 = this.k;
            if (z67 != null) {
                z67.a().h(getViewLifecycleOwner(), new p(this));
                g37<pa5> g37 = this.h;
                if (g37 != null) {
                    pa5 a3 = g37.a();
                    if (a3 != null) {
                        if (!TextUtils.isEmpty(this.m)) {
                            Integer valueOf = Integer.valueOf(Color.parseColor(this.m));
                            this.l = valueOf;
                            if (valueOf != null) {
                                int intValue = valueOf.intValue();
                                FlexibleCheckBox flexibleCheckBox = a3.u;
                                pq7.b(flexibleCheckBox, "binding.cbTwo");
                                flexibleCheckBox.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox2 = a3.u;
                                pq7.b(flexibleCheckBox2, "binding.cbTwo");
                                flexibleCheckBox2.setButtonTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox3 = a3.s;
                                pq7.b(flexibleCheckBox3, "binding.cbOne");
                                flexibleCheckBox3.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox4 = a3.s;
                                pq7.b(flexibleCheckBox4, "binding.cbOne");
                                flexibleCheckBox4.setButtonTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox5 = a3.t;
                                pq7.b(flexibleCheckBox5, "binding.cbThree");
                                flexibleCheckBox5.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox6 = a3.t;
                                pq7.b(flexibleCheckBox6, "binding.cbThree");
                                flexibleCheckBox6.setButtonTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox7 = a3.q;
                                pq7.b(flexibleCheckBox7, "binding.cbFive");
                                flexibleCheckBox7.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox8 = a3.q;
                                pq7.b(flexibleCheckBox8, "binding.cbFive");
                                flexibleCheckBox8.setButtonTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox9 = a3.r;
                                pq7.b(flexibleCheckBox9, "binding.cbFour");
                                flexibleCheckBox9.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox10 = a3.r;
                                pq7.b(flexibleCheckBox10, "binding.cbFour");
                                flexibleCheckBox10.setButtonTintList(ColorStateList.valueOf(intValue));
                            }
                        }
                        a3.w.addTextChangedListener(new g(this));
                        a3.x.addTextChangedListener(new h(this));
                        a3.y.addTextChangedListener(new i(this));
                        a3.R.setOnClickListener(new j(this));
                        a3.v.setOnClickListener(new k(a3, this));
                        a3.B.setOnClickListener(new l(this));
                        a3.A.setOnClickListener(new m(this));
                        a3.C.setOnClickListener(new n(this));
                        a3.u.setOnCheckedChangeListener(new o(this));
                        a3.s.setOnCheckedChangeListener(new b(this));
                        a3.t.setOnCheckedChangeListener(new c(this));
                        a3.r.setOnCheckedChangeListener(new d(this));
                        a3.q.setOnCheckedChangeListener(new e(this));
                        a3.z.setOnClickListener(new f(this));
                        FlexibleTextView flexibleTextView = a3.G;
                        pq7.b(flexibleTextView, "binding.ftvOne");
                        flexibleTextView.setMovementMethod(new LinkMovementMethod());
                        FlexibleTextView flexibleTextView2 = a3.H;
                        pq7.b(flexibleTextView2, "binding.ftvThree");
                        flexibleTextView2.setMovementMethod(new LinkMovementMethod());
                        FlexibleTextView flexibleTextView3 = a3.E;
                        pq7.b(flexibleTextView3, "binding.ftvFour");
                        flexibleTextView3.setMovementMethod(new LinkMovementMethod());
                        FlexibleTextView flexibleTextView4 = a3.D;
                        pq7.b(flexibleTextView4, "binding.ftvFive");
                        flexibleTextView4.setMovementMethod(new LinkMovementMethod());
                        if (wr4.f3989a.a().i()) {
                            FlexibleCheckBox flexibleCheckBox11 = a3.s;
                            pq7.b(flexibleCheckBox11, "binding.cbOne");
                            flexibleCheckBox11.setVisibility(8);
                            FlexibleTextView flexibleTextView5 = a3.G;
                            pq7.b(flexibleTextView5, "binding.ftvOne");
                            flexibleTextView5.setVisibility(8);
                            FlexibleCheckBox flexibleCheckBox12 = a3.u;
                            pq7.b(flexibleCheckBox12, "binding.cbTwo");
                            flexibleCheckBox12.setVisibility(8);
                            FlexibleTextView flexibleTextView6 = a3.I;
                            pq7.b(flexibleTextView6, "binding.ftvTwo");
                            flexibleTextView6.setVisibility(8);
                            FlexibleCheckBox flexibleCheckBox13 = a3.t;
                            pq7.b(flexibleCheckBox13, "binding.cbThree");
                            flexibleCheckBox13.setVisibility(0);
                            FlexibleTextView flexibleTextView7 = a3.H;
                            pq7.b(flexibleTextView7, "binding.ftvThree");
                            flexibleTextView7.setVisibility(0);
                            FlexibleCheckBox flexibleCheckBox14 = a3.r;
                            pq7.b(flexibleCheckBox14, "binding.cbFour");
                            flexibleCheckBox14.setVisibility(0);
                            FlexibleTextView flexibleTextView8 = a3.E;
                            pq7.b(flexibleTextView8, "binding.ftvFour");
                            flexibleTextView8.setVisibility(0);
                            FlexibleCheckBox flexibleCheckBox15 = a3.q;
                            pq7.b(flexibleCheckBox15, "binding.cbFive");
                            flexibleCheckBox15.setVisibility(0);
                            FlexibleTextView flexibleTextView9 = a3.D;
                            pq7.b(flexibleTextView9, "binding.ftvFive");
                            flexibleTextView9.setVisibility(0);
                        }
                    }
                    E6("profile_setup_view");
                    return;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("mUserBirthDayViewModel");
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void p3(boolean z) {
        RTLImageView rTLImageView;
        if (isActive()) {
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null && (rTLImageView = a2.Q) != null) {
                    pq7.b(rTLImageView, "it");
                    rTLImageView.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void p4(qh5 qh5) {
        pq7.c(qh5, "gender");
        if (isActive()) {
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null) {
                    PortfolioApp c2 = PortfolioApp.h0.c();
                    int d2 = gl0.d(c2, 2131099968);
                    int d3 = gl0.d(c2, 2131100360);
                    Drawable f2 = gl0.f(c2, 2131230836);
                    Drawable f3 = gl0.f(c2, 2131230830);
                    a2.C.setTextColor(d2);
                    a2.A.setTextColor(d2);
                    a2.B.setTextColor(d2);
                    FlexibleButton flexibleButton = a2.C;
                    pq7.b(flexibleButton, "it.fbOther");
                    flexibleButton.setBackground(f2);
                    FlexibleButton flexibleButton2 = a2.A;
                    pq7.b(flexibleButton2, "it.fbFemale");
                    flexibleButton2.setBackground(f2);
                    FlexibleButton flexibleButton3 = a2.B;
                    pq7.b(flexibleButton3, "it.fbMale");
                    flexibleButton3.setBackground(f2);
                    a2.B.d("flexible_button_secondary");
                    a2.A.d("flexible_button_secondary");
                    a2.C.d("flexible_button_secondary");
                    int i2 = yv5.f4382a[qh5.ordinal()];
                    if (i2 == 1) {
                        a2.B.setTextColor(d3);
                        FlexibleButton flexibleButton4 = a2.B;
                        pq7.b(flexibleButton4, "it.fbMale");
                        flexibleButton4.setBackground(f3);
                        a2.B.d("flexible_button_primary");
                    } else if (i2 == 2) {
                        a2.A.setTextColor(d3);
                        FlexibleButton flexibleButton5 = a2.A;
                        pq7.b(flexibleButton5, "it.fbFemale");
                        flexibleButton5.setBackground(f3);
                        a2.A.d("flexible_button_primary");
                    } else if (i2 == 3) {
                        a2.C.setTextColor(d3);
                        FlexibleButton flexibleButton6 = a2.C;
                        pq7.b(flexibleButton6, "it.fbOther");
                        flexibleButton6.setBackground(f3);
                        a2.C.d("flexible_button_primary");
                    }
                }
            } else {
                pq7.n("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void w0(Spanned spanned) {
        pq7.c(spanned, "message");
        if (isActive()) {
            g37<pa5> g37 = this.h;
            if (g37 != null) {
                pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.G;
                    pq7.b(flexibleTextView, "binding.ftvOne");
                    flexibleTextView.setText(spanned);
                    Integer num = this.l;
                    if (num != null) {
                        a2.G.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw6
    public void z0(Bundle bundle) {
        pq7.c(bundle, "data");
        sv5 sv5 = this.i;
        if (sv5 != null) {
            sv5.setArguments(bundle);
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            sv5.show(childFragmentManager, sv5.u.a());
        }
    }
}
