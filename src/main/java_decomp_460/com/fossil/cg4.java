package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Parcelable;
import android.util.Log;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.google.firebase.iid.FirebaseInstanceIdReceiver;
import com.misfit.frameworks.common.enums.Action;
import java.util.ArrayDeque;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cg4 {
    @DexIgnore
    public static cg4 e;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f611a; // = null;
    @DexIgnore
    public Boolean b; // = null;
    @DexIgnore
    public Boolean c; // = null;
    @DexIgnore
    public /* final */ Queue<Intent> d; // = new ArrayDeque();

    @DexIgnore
    public static cg4 b() {
        cg4 cg4;
        synchronized (cg4.class) {
            try {
                if (e == null) {
                    e = new cg4();
                }
                cg4 = e;
            } catch (Throwable th) {
                throw th;
            }
        }
        return cg4;
    }

    @DexIgnore
    public static void h(Context context, Intent intent) {
        context.sendBroadcast(j(context, "com.google.firebase.MESSAGING_EVENT", intent));
    }

    @DexIgnore
    public static Intent i(Intent intent) {
        Parcelable parcelableExtra = intent.getParcelableExtra("wrapped_intent");
        if (parcelableExtra instanceof Intent) {
            return (Intent) parcelableExtra;
        }
        return null;
    }

    @DexIgnore
    public static Intent j(Context context, String str, Intent intent) {
        Intent intent2 = new Intent(context, FirebaseInstanceIdReceiver.class);
        intent2.setAction(str);
        intent2.putExtra("wrapped_intent", intent);
        return intent2;
    }

    @DexIgnore
    public final int a(Context context, Intent intent) {
        ComponentName startService;
        String f = f(context, intent);
        if (f != null) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(f);
                Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Restricting intent to a specific service: ".concat(valueOf) : new String("Restricting intent to a specific service: "));
            }
            intent.setClassName(context.getPackageName(), f);
        }
        try {
            if (e(context)) {
                startService = fg4.e(context, intent);
            } else {
                startService = context.startService(intent);
                Log.d("FirebaseInstanceId", "Missing wake lock permission, service start may be delayed");
            }
            if (startService != null) {
                return -1;
            }
            Log.e("FirebaseInstanceId", "Error while delivering the message: ServiceIntent not found.");
            return 404;
        } catch (SecurityException e2) {
            Log.e("FirebaseInstanceId", "Error while delivering the message to the serviceIntent", e2);
            return 401;
        } catch (IllegalStateException e3) {
            String valueOf2 = String.valueOf(e3);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf2).length() + 45);
            sb.append("Failed to start service while in background: ");
            sb.append(valueOf2);
            Log.e("FirebaseInstanceId", sb.toString());
            return Action.ActivityTracker.TAG_ACTIVITY;
        }
    }

    @DexIgnore
    public Intent c() {
        return this.d.poll();
    }

    @DexIgnore
    public boolean d(Context context) {
        if (this.c == null) {
            this.c = Boolean.valueOf(context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0);
        }
        if (!this.b.booleanValue() && Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Missing Permission: android.permission.ACCESS_NETWORK_STATE this should normally be included by the manifest merger, but may needed to be manually added to your manifest");
        }
        return this.c.booleanValue();
    }

    @DexIgnore
    public boolean e(Context context) {
        if (this.b == null) {
            this.b = Boolean.valueOf(context.checkCallingOrSelfPermission("android.permission.WAKE_LOCK") == 0);
        }
        if (!this.b.booleanValue() && Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Missing Permission: android.permission.WAKE_LOCK this should normally be included by the manifest merger, but may needed to be manually added to your manifest");
        }
        return this.b.booleanValue();
    }

    @DexIgnore
    public final String f(Context context, Intent intent) {
        String str = null;
        synchronized (this) {
            if (this.f611a != null) {
                str = this.f611a;
            } else {
                ResolveInfo resolveService = context.getPackageManager().resolveService(intent, 0);
                if (resolveService == null || resolveService.serviceInfo == null) {
                    Log.e("FirebaseInstanceId", "Failed to resolve target intent service, skipping classname enforcement");
                } else {
                    ServiceInfo serviceInfo = resolveService.serviceInfo;
                    if (!context.getPackageName().equals(serviceInfo.packageName) || serviceInfo.name == null) {
                        String str2 = serviceInfo.packageName;
                        String str3 = serviceInfo.name;
                        StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 94 + String.valueOf(str3).length());
                        sb.append("Error resolving target intent service, skipping classname enforcement. Resolved service was: ");
                        sb.append(str2);
                        sb.append("/");
                        sb.append(str3);
                        Log.e("FirebaseInstanceId", sb.toString());
                    } else {
                        if (serviceInfo.name.startsWith(CodelessMatcher.CURRENT_CLASS_NAME)) {
                            String valueOf = String.valueOf(context.getPackageName());
                            String valueOf2 = String.valueOf(serviceInfo.name);
                            this.f611a = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                        } else {
                            this.f611a = serviceInfo.name;
                        }
                        str = this.f611a;
                    }
                }
            }
        }
        return str;
    }

    @DexIgnore
    public int g(Context context, Intent intent) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Starting service");
        }
        this.d.offer(intent);
        Intent intent2 = new Intent("com.google.firebase.MESSAGING_EVENT");
        intent2.setPackage(context.getPackageName());
        return a(context, intent2);
    }
}
