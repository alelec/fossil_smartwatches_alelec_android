package com.fossil;

import android.graphics.Color;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ft6 extends ts0 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e; // = "#bdbdbd";

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public MutableLiveData<a> f1201a; // = new MutableLiveData<>();
    @DexIgnore
    public a b; // = new a(null, null, null, null, null, null, 63, null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Integer f1202a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public Integer e;
        @DexIgnore
        public Integer f;

        @DexIgnore
        public a() {
            this(null, null, null, null, null, null, 63, null);
        }

        @DexIgnore
        public a(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6) {
            this.f1202a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
            this.e = num5;
            this.f = num6;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, int i, kq7 kq7) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2, (i & 4) != 0 ? null : num3, (i & 8) != 0 ? null : num4, (i & 16) != 0 ? null : num5, (i & 32) == 0 ? num6 : null);
        }

        @DexIgnore
        public static /* synthetic */ void h(a aVar, Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, int i, Object obj) {
            Integer num7 = null;
            Integer num8 = (i & 1) != 0 ? null : num;
            Integer num9 = (i & 2) != 0 ? null : num2;
            Integer num10 = (i & 4) != 0 ? null : num3;
            Integer num11 = (i & 8) != 0 ? null : num4;
            Integer num12 = (i & 16) != 0 ? null : num5;
            if ((i & 32) == 0) {
                num7 = num6;
            }
            aVar.g(num8, num9, num10, num11, num12, num7);
        }

        @DexIgnore
        public final Integer a() {
            return this.b;
        }

        @DexIgnore
        public final Integer b() {
            return this.f1202a;
        }

        @DexIgnore
        public final Integer c() {
            return this.f;
        }

        @DexIgnore
        public final Integer d() {
            return this.e;
        }

        @DexIgnore
        public final Integer e() {
            return this.d;
        }

        @DexIgnore
        public final Integer f() {
            return this.c;
        }

        @DexIgnore
        public final void g(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6) {
            this.f1202a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
            this.e = num5;
            this.f = num6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeSleepChartViewModel$saveColor$1", f = "CustomizeSleepChartViewModel.kt", l = {66, 67, 100}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $awakeColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $deepColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $lightColor;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ft6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ft6 ft6, String str, String str2, String str3, String str4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ft6;
            this.$id = str;
            this.$awakeColor = str2;
            this.$lightColor = str3;
            this.$deepColor = str4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$id, this.$awakeColor, this.$lightColor, this.$deepColor, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x004c  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x006c  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00b8  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00e1  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00fc  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0136  */
        /* JADX WARNING: Removed duplicated region for block: B:49:0x01a6  */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x01a9  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 429
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ft6.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = ft6.class.getSimpleName();
        pq7.b(simpleName, "CustomizeSleepChartViewM\u2026el::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public ft6(ThemeRepository themeRepository) {
        pq7.c(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void d() {
        this.f1201a.l(this.b);
    }

    @DexIgnore
    public final MutableLiveData<a> e() {
        return this.f1201a;
    }

    @DexIgnore
    public final void f(String str, String str2, String str3, String str4) {
        pq7.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str5 = d;
        local.d(str5, "saveColor awakeColor=" + str2 + " lightColor=" + str3 + " deepColor=" + str4);
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new b(this, str, str2, str3, str4, null), 3, null);
    }

    @DexIgnore
    public final void g() {
        String a2 = ct6.t.a();
        int parseColor = a2 != null ? Color.parseColor(a2) : Color.parseColor(e);
        String c2 = ct6.t.c();
        int parseColor2 = c2 != null ? Color.parseColor(c2) : Color.parseColor(e);
        String b2 = ct6.t.b();
        int parseColor3 = b2 != null ? Color.parseColor(b2) : Color.parseColor(e);
        this.b.g(Integer.valueOf(parseColor), Integer.valueOf(parseColor), Integer.valueOf(parseColor2), Integer.valueOf(parseColor2), Integer.valueOf(parseColor3), Integer.valueOf(parseColor3));
        d();
    }

    @DexIgnore
    public final void h(int i, int i2) {
        switch (i) {
            case 701:
                a.h(this.b, Integer.valueOf(i2), Integer.valueOf(i2), null, null, null, null, 60, null);
                d();
                return;
            case 702:
                a.h(this.b, null, null, Integer.valueOf(i2), Integer.valueOf(i2), null, null, 51, null);
                d();
                return;
            case 703:
                a.h(this.b, null, null, null, null, Integer.valueOf(i2), Integer.valueOf(i2), 15, null);
                d();
                return;
            default:
                return;
        }
    }
}
