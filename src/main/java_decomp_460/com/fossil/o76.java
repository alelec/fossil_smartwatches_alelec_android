package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.pw5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o76 extends pv5 implements u86 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public g37<za5> g;
    @DexIgnore
    public t86 h;
    @DexIgnore
    public pw5 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final o76 a() {
            return new o76();
        }

        @DexIgnore
        public final String b() {
            return o76.k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements pw5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ o76 f2643a;

        @DexIgnore
        public b(o76 o76) {
            this.f2643a = o76;
        }

        @DexIgnore
        @Override // com.fossil.pw5.b
        public void a(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b = o76.l.b();
            local.d(b, "onItemClick position=" + i);
            pw5 pw5 = this.f2643a.i;
            if (pw5 != null) {
                pw5.c cVar = pw5.h().get(i);
                if (cVar.b() == pw5.c.a.TYPE_VALUE) {
                    SecondTimezoneSetting c = cVar.c();
                    if (c != null) {
                        c.setTimezoneOffset(ConversionUtils.INSTANCE.getTimezoneRawOffsetById(c.getTimeZoneId()));
                        Intent intent = new Intent();
                        intent.putExtra("SECOND_TIMEZONE", c);
                        FragmentActivity activity = this.f2643a.getActivity();
                        if (activity != null) {
                            activity.setResult(-1, intent);
                        }
                        FragmentActivity activity2 = this.f2643a.getActivity();
                        if (activity2 != null) {
                            activity2.finish();
                            return;
                        }
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewAlphabetIndex.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ o76 f2644a;
        @DexIgnore
        public /* final */ /* synthetic */ za5 b;

        @DexIgnore
        public c(o76 o76, za5 za5) {
            this.f2644a = o76;
            this.b = za5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex.b
        public void a(View view, int i, String str) {
            pq7.c(view, "view");
            pq7.c(str, "character");
            pw5 pw5 = this.f2644a.i;
            Integer valueOf = pw5 != null ? Integer.valueOf(pw5.i(str)) : null;
            if (valueOf != null && valueOf.intValue() != -1) {
                RecyclerView recyclerView = this.b.z;
                pq7.b(recyclerView, "binding.timezoneRecyclerView");
                RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                if (layoutManager != null) {
                    ((LinearLayoutManager) layoutManager).D2(valueOf.intValue(), 0);
                    return;
                }
                throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ o76 b;
        @DexIgnore
        public /* final */ /* synthetic */ za5 c;

        @DexIgnore
        public d(o76 o76, za5 za5) {
            this.b = o76;
            this.c = za5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FLogger.INSTANCE.getLocal().d(o76.l.b(), "afterTextChanged s=" + ((Object) editable));
            pw5 pw5 = this.b.i;
            if (pw5 != null) {
                String valueOf = String.valueOf(editable);
                int length = valueOf.length() - 1;
                boolean z = false;
                int i = 0;
                while (i <= length) {
                    boolean z2 = valueOf.charAt(!z ? i : length) <= ' ';
                    if (!z) {
                        if (!z2) {
                            z = true;
                        } else {
                            i++;
                        }
                    } else if (!z2) {
                        break;
                    } else {
                        length--;
                    }
                }
                pw5.g(valueOf.subSequence(i, length + 1).toString());
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b2 = o76.l.b();
            local.d(b2, "beforeTextChanged s=" + charSequence + " start=" + i + " count=" + i2 + " after=" + i3);
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b2 = o76.l.b();
            local.d(b2, "onTextChanged s=" + charSequence + " start=" + i + " before=" + i2 + " count=" + i3);
            ImageView imageView = this.c.r;
            pq7.b(imageView, "binding.clearIv");
            imageView.setVisibility(!TextUtils.isEmpty(charSequence) ? 0 : 4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ o76 b;

        @DexIgnore
        public e(o76 o76) {
            this.b = o76;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ za5 b;

        @DexIgnore
        public f(za5 za5) {
            this.b = za5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.y.setText("");
        }
    }

    /*
    static {
        String simpleName = o76.class.getSimpleName();
        pq7.b(simpleName, "SearchSecondTimezoneFrag\u2026nt::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.u86
    public void G2(List<SecondTimezoneSetting> list) {
        pq7.c(list, "secondTimezones");
        pw5 pw5 = this.i;
        if (pw5 != null) {
            pw5.k(list);
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(t86 t86) {
        pq7.c(t86, "presenter");
        this.h = t86;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        za5 za5 = (za5) aq0.f(layoutInflater, 2131558620, viewGroup, false, A6());
        pw5 pw5 = new pw5();
        pw5.j(new b(this));
        this.i = pw5;
        RecyclerView recyclerView = za5.z;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.i);
        RecyclerViewAlphabetIndex recyclerViewAlphabetIndex = za5.x;
        recyclerViewAlphabetIndex.a();
        recyclerViewAlphabetIndex.setOnSectionIndexClickListener(new c(this, za5));
        za5.y.addTextChangedListener(new d(this, za5));
        za5.q.setOnClickListener(new e(this));
        za5.r.setOnClickListener(new f(za5));
        this.g = new g37<>(this, za5);
        pq7.b(za5, "binding");
        return za5.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        t86 t86 = this.h;
        if (t86 != null) {
            t86.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        t86 t86 = this.h;
        if (t86 != null) {
            t86.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.u86
    public void v5(String str) {
        FlexibleTextView flexibleTextView;
        pq7.c(str, "cityName");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = k;
        local.d(str2, "cityName=" + str);
        g37<za5> g37 = this.g;
        if (g37 != null) {
            za5 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.t) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
