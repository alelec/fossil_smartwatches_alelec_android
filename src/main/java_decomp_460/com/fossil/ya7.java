package com.fossil;

import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ya7 implements Factory<WatchFaceGalleryViewModel> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<UserRepository> f4288a;
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> b;
    @DexIgnore
    public /* final */ Provider<wb7> c;

    @DexIgnore
    public ya7(Provider<UserRepository> provider, Provider<DianaWatchFaceRepository> provider2, Provider<wb7> provider3) {
        this.f4288a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static ya7 a(Provider<UserRepository> provider, Provider<DianaWatchFaceRepository> provider2, Provider<wb7> provider3) {
        return new ya7(provider, provider2, provider3);
    }

    @DexIgnore
    public static WatchFaceGalleryViewModel c(UserRepository userRepository, DianaWatchFaceRepository dianaWatchFaceRepository, wb7 wb7) {
        return new WatchFaceGalleryViewModel(userRepository, dianaWatchFaceRepository, wb7);
    }

    @DexIgnore
    /* renamed from: b */
    public WatchFaceGalleryViewModel get() {
        return c(this.f4288a.get(), this.b.get(), this.c.get());
    }
}
