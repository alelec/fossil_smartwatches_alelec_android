package com.fossil;

import com.fossil.u24;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h34<E> extends u24<E> implements Set<E> {
    @DexIgnore
    public static /* final */ int MAX_TABLE_SIZE; // = 1073741824;
    @DexIgnore
    @LazyInit
    public transient y24<E> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<E> extends u24.a<E> {
        @DexIgnore
        public a() {
            this(4);
        }

        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        @CanIgnoreReturnValue
        /* renamed from: g */
        public a<E> a(E e) {
            super.e(e);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public a<E> h(E... eArr) {
            super.b(eArr);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public a<E> i(Iterator<? extends E> it) {
            super.c(it);
            return this;
        }

        @DexIgnore
        public h34<E> j() {
            h34<E> a2 = h34.a(this.b, this.f3507a);
            this.b = a2.size();
            return a2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<E> extends h34<E> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends s24<E> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.s24
            public b<E> delegateCollection() {
                return b.this;
            }

            @DexIgnore
            @Override // java.util.List
            public E get(int i) {
                return (E) b.this.get(i);
            }
        }

        @DexIgnore
        @Override // com.fossil.h34
        public y24<E> createAsList() {
            return new a();
        }

        @DexIgnore
        public abstract E get(int i);

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.u24, com.fossil.u24, com.fossil.h34, com.fossil.h34, java.lang.Iterable
        public h54<E> iterator() {
            return asList().iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Object[] elements;

        @DexIgnore
        public c(Object[] objArr) {
            this.elements = objArr;
        }

        @DexIgnore
        public Object readResolve() {
            return h34.copyOf(this.elements);
        }
    }

    @DexIgnore
    public static <E> h34<E> a(int i, Object... objArr) {
        int i2;
        if (i == 0) {
            return of();
        }
        if (i == 1) {
            return of(objArr[0]);
        }
        int chooseTableSize = chooseTableSize(i);
        Object[] objArr2 = new Object[chooseTableSize];
        int i3 = chooseTableSize - 1;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        while (i6 < i) {
            Object obj = objArr[i6];
            h44.b(obj, i6);
            int hashCode = obj.hashCode();
            int b2 = r24.b(hashCode);
            while (true) {
                int i7 = b2 & i3;
                Object obj2 = objArr2[i7];
                if (obj2 == null) {
                    objArr[i5] = obj;
                    objArr2[i7] = obj;
                    i2 = i4 + hashCode;
                    i5++;
                    break;
                } else if (obj2.equals(obj)) {
                    i2 = i4;
                    break;
                } else {
                    b2++;
                }
            }
            i6++;
            i4 = i2;
        }
        Arrays.fill(objArr, i5, i, (Object) null);
        if (i5 == 1) {
            return new a54(objArr[0], i4);
        }
        if (chooseTableSize != chooseTableSize(i5)) {
            return a(i5, objArr);
        }
        if (i5 < objArr.length) {
            objArr = h44.a(objArr, i5);
        }
        return new r44(objArr, i4, objArr2, i3);
    }

    @DexIgnore
    public static h34 b(EnumSet enumSet) {
        return x24.asImmutable(EnumSet.copyOf(enumSet));
    }

    @DexIgnore
    public static <E> a<E> builder() {
        return new a<>();
    }

    @DexIgnore
    public static int chooseTableSize(int i) {
        boolean z = true;
        if (i < 751619276) {
            int highestOneBit = Integer.highestOneBit(i - 1);
            while (true) {
                highestOneBit <<= 1;
                if (((double) highestOneBit) * 0.7d >= ((double) i)) {
                    return highestOneBit;
                }
            }
        } else {
            if (i >= 1073741824) {
                z = false;
            }
            i14.e(z, "collection too large");
            return 1073741824;
        }
    }

    @DexIgnore
    public static <E> h34<E> copyOf(Iterable<? extends E> iterable) {
        return iterable instanceof Collection ? copyOf((Collection) iterable) : copyOf(iterable.iterator());
    }

    @DexIgnore
    public static <E> h34<E> copyOf(Collection<? extends E> collection) {
        if ((collection instanceof h34) && !(collection instanceof m34)) {
            h34<E> h34 = (h34) collection;
            if (!h34.isPartialView()) {
                return h34;
            }
        } else if (collection instanceof EnumSet) {
            return b((EnumSet) collection);
        }
        Object[] array = collection.toArray();
        return a(array.length, array);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: com.fossil.h34$a */
    /* JADX WARN: Multi-variable type inference failed */
    public static <E> h34<E> copyOf(Iterator<? extends E> it) {
        if (!it.hasNext()) {
            return of();
        }
        Object next = it.next();
        if (!it.hasNext()) {
            return of(next);
        }
        a aVar = new a();
        aVar.a(next);
        aVar.i(it);
        return aVar.j();
    }

    @DexIgnore
    public static <E> h34<E> copyOf(E[] eArr) {
        int length = eArr.length;
        return length != 0 ? length != 1 ? a(eArr.length, (Object[]) eArr.clone()) : of((Object) eArr[0]) : of();
    }

    @DexIgnore
    public static <E> h34<E> of() {
        return r44.EMPTY;
    }

    @DexIgnore
    public static <E> h34<E> of(E e) {
        return new a54(e);
    }

    @DexIgnore
    public static <E> h34<E> of(E e, E e2) {
        return a(2, e, e2);
    }

    @DexIgnore
    public static <E> h34<E> of(E e, E e2, E e3) {
        return a(3, e, e2, e3);
    }

    @DexIgnore
    public static <E> h34<E> of(E e, E e2, E e3, E e4) {
        return a(4, e, e2, e3, e4);
    }

    @DexIgnore
    public static <E> h34<E> of(E e, E e2, E e3, E e4, E e5) {
        return a(5, e, e2, e3, e4, e5);
    }

    @DexIgnore
    @SafeVarargs
    public static <E> h34<E> of(E e, E e2, E e3, E e4, E e5, E e6, E... eArr) {
        int length = eArr.length + 6;
        Object[] objArr = new Object[length];
        objArr[0] = e;
        objArr[1] = e2;
        objArr[2] = e3;
        objArr[3] = e4;
        objArr[4] = e5;
        objArr[5] = e6;
        System.arraycopy(eArr, 0, objArr, 6, eArr.length);
        return a(length, objArr);
    }

    @DexIgnore
    @Override // com.fossil.u24
    public y24<E> asList() {
        y24<E> y24 = this.b;
        if (y24 != null) {
            return y24;
        }
        y24<E> createAsList = createAsList();
        this.b = createAsList;
        return createAsList;
    }

    @DexIgnore
    public y24<E> createAsList() {
        return new m44(this, toArray());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof h34) || !isHashCodeFast() || !((h34) obj).isHashCodeFast() || hashCode() == obj.hashCode()) {
            return x44.a(this, obj);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return x44.b(this);
    }

    @DexIgnore
    public boolean isHashCodeFast() {
        return false;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.u24, com.fossil.u24, java.lang.Iterable
    public abstract h54<E> iterator();

    @DexIgnore
    @Override // com.fossil.u24
    public Object writeReplace() {
        return new c(toArray());
    }
}
