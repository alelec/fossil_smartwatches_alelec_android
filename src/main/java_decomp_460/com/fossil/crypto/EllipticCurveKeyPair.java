package com.fossil.crypto;

import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class EllipticCurveKeyPair {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CppProxy extends EllipticCurveKeyPair {
        @DexIgnore
        public static /* final */ /* synthetic */ boolean $assertionsDisabled; // = false;
        @DexIgnore
        public /* final */ AtomicBoolean destroyed; // = new AtomicBoolean(false);
        @DexIgnore
        public /* final */ long nativeRef;

        @DexIgnore
        public CppProxy(long j) {
            if (j != 0) {
                this.nativeRef = j;
                return;
            }
            throw new RuntimeException("nativeRef is zero");
        }

        @DexIgnore
        public static native EllipticCurveKeyPair create();

        @DexIgnore
        private native void nativeDestroy(long j);

        @DexIgnore
        private native byte[] native_calculateSecretKey(long j, byte[] bArr);

        @DexIgnore
        private native byte[] native_privateKey(long j);

        @DexIgnore
        private native byte[] native_publicKey(long j);

        @DexIgnore
        public void _djinni_private_destroy() {
            if (!this.destroyed.getAndSet(true)) {
                nativeDestroy(this.nativeRef);
            }
        }

        @DexIgnore
        @Override // com.fossil.crypto.EllipticCurveKeyPair
        public byte[] calculateSecretKey(byte[] bArr) {
            return native_calculateSecretKey(this.nativeRef, bArr);
        }

        @DexIgnore
        public void finalize() throws Throwable {
            _djinni_private_destroy();
            super.finalize();
        }

        @DexIgnore
        @Override // com.fossil.crypto.EllipticCurveKeyPair
        public byte[] privateKey() {
            return native_privateKey(this.nativeRef);
        }

        @DexIgnore
        @Override // com.fossil.crypto.EllipticCurveKeyPair
        public byte[] publicKey() {
            return native_publicKey(this.nativeRef);
        }
    }

    @DexIgnore
    public static EllipticCurveKeyPair create() {
        return CppProxy.create();
    }

    @DexIgnore
    public abstract byte[] calculateSecretKey(byte[] bArr);

    @DexIgnore
    public abstract byte[] privateKey();

    @DexIgnore
    public abstract byte[] publicKey();
}
