package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lj7 {
    @DexIgnore
    public static /* final */ int abc_action_bar_home_description; // = 2131887235;
    @DexIgnore
    public static /* final */ int abc_action_bar_up_description; // = 2131887236;
    @DexIgnore
    public static /* final */ int abc_action_menu_overflow_description; // = 2131887237;
    @DexIgnore
    public static /* final */ int abc_action_mode_done; // = 2131887238;
    @DexIgnore
    public static /* final */ int abc_activity_chooser_view_see_all; // = 2131887239;
    @DexIgnore
    public static /* final */ int abc_activitychooserview_choose_application; // = 2131887240;
    @DexIgnore
    public static /* final */ int abc_capital_off; // = 2131887241;
    @DexIgnore
    public static /* final */ int abc_capital_on; // = 2131887242;
    @DexIgnore
    public static /* final */ int abc_search_hint; // = 2131887253;
    @DexIgnore
    public static /* final */ int abc_searchview_description_clear; // = 2131887254;
    @DexIgnore
    public static /* final */ int abc_searchview_description_query; // = 2131887255;
    @DexIgnore
    public static /* final */ int abc_searchview_description_search; // = 2131887256;
    @DexIgnore
    public static /* final */ int abc_searchview_description_submit; // = 2131887257;
    @DexIgnore
    public static /* final */ int abc_searchview_description_voice; // = 2131887258;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with; // = 2131887259;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with_application; // = 2131887260;
    @DexIgnore
    public static /* final */ int abc_toolbar_collapse_description; // = 2131887261;
    @DexIgnore
    public static /* final */ int belvedere_dialog_camera; // = 2131887296;
    @DexIgnore
    public static /* final */ int belvedere_dialog_gallery; // = 2131887297;
    @DexIgnore
    public static /* final */ int belvedere_dialog_unknown; // = 2131887298;
    @DexIgnore
    public static /* final */ int belvedere_sdk_fpa_suffix; // = 2131887301;
    @DexIgnore
    public static /* final */ int status_bar_notification_info_overflow; // = 2131887575;
}
