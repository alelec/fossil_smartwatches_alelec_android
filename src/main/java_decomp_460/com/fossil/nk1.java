package com.fossil;

import android.util.Log;
import com.crashlytics.android.answers.Answers;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nk1 implements qk1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Answers f2536a;

    @DexIgnore
    public nk1(Answers answers) {
        this.f2536a = answers;
    }

    @DexIgnore
    public static nk1 b() throws NoClassDefFoundError, IllegalStateException {
        return c(Answers.getInstance());
    }

    @DexIgnore
    public static nk1 c(Answers answers) throws IllegalStateException {
        if (answers != null) {
            return new nk1(answers);
        }
        throw new IllegalStateException("Answers must be initialized before logging kit events");
    }

    @DexIgnore
    @Override // com.fossil.qk1
    public void a(pk1 pk1) {
        try {
            this.f2536a.logCustom(pk1.b());
        } catch (Throwable th) {
            Log.w("AnswersKitEventLogger", "Unexpected error sending Answers event", th);
        }
    }
}
