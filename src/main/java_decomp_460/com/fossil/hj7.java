package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hj7 implements dj7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public boolean f1490a; // = false;

    @DexIgnore
    @Override // com.fossil.dj7
    public void d(String str, String str2) {
        if (this.f1490a) {
            Log.d(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.dj7
    public void e(String str, String str2) {
        if (this.f1490a) {
            Log.e(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.dj7
    public void e(String str, String str2, Throwable th) {
        if (this.f1490a) {
            Log.e(str, str2, th);
        }
    }

    @DexIgnore
    @Override // com.fossil.dj7
    public void setLoggable(boolean z) {
        this.f1490a = z;
    }

    @DexIgnore
    @Override // com.fossil.dj7
    public void w(String str, String str2) {
        if (this.f1490a) {
            Log.w(str, str2);
        }
    }
}
