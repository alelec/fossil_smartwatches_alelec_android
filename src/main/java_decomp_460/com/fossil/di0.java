package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class di0 {
    @DexIgnore
    public abstract void a(Runnable runnable);

    @DexIgnore
    public void b(Runnable runnable) {
        if (c()) {
            runnable.run();
        } else {
            d(runnable);
        }
    }

    @DexIgnore
    public abstract boolean c();

    @DexIgnore
    public abstract void d(Runnable runnable);
}
