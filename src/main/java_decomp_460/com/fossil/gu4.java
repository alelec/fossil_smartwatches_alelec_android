package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gu4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ApiServiceV2 f1365a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource", f = "ProfileRemoteDataSource.kt", l = {31}, m = "changeSocialId")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ gu4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(gu4 gu4, qn7 qn7) {
            super(qn7);
            this.this$0 = gu4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$changeSocialId$response$1", f = "ProfileRemoteDataSource.kt", l = {31}, m = "invokeSuspend")
    public static final class b extends ko7 implements rp7<qn7<? super q88<it4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ gu4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(gu4 gu4, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = gu4;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new b(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<it4>> qn7) {
            return ((b) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f1365a;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object socialId = apiServiceV2.socialId(gj4, this);
                return socialId == d ? d : socialId;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource", f = "ProfileRemoteDataSource.kt", l = {16}, m = "fetchSocialProfile")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ gu4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(gu4 gu4, qn7 qn7) {
            super(qn7);
            this.this$0 = gu4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$fetchSocialProfile$response$1", f = "ProfileRemoteDataSource.kt", l = {16}, m = "invokeSuspend")
    public static final class d extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ gu4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(gu4 gu4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = gu4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new d(this.this$0, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((d) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f1365a;
                this.label = 1;
                Object mySocialProfile = apiServiceV2.getMySocialProfile(this);
                return mySocialProfile == d ? d : mySocialProfile;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public gu4(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        this.f1365a = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(java.lang.String r9, com.fossil.qn7<? super com.fossil.iq5<com.fossil.it4>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.fossil.gu4.a
            if (r0 == 0) goto L_0x0046
            r0 = r10
            com.fossil.gu4$a r0 = (com.fossil.gu4.a) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0046
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0055
            if (r0 != r5) goto L_0x004d
            java.lang.Object r0 = r1.L$2
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.gu4 r0 = (com.fossil.gu4) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x0031:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0077
            com.fossil.kq5 r1 = new com.fossil.kq5
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r0 = r0.a()
            r2 = 0
            r3 = 2
            r1.<init>(r0, r2, r3, r4)
            r0 = r1
        L_0x0045:
            return r0
        L_0x0046:
            com.fossil.gu4$a r0 = new com.fossil.gu4$a
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x004d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0055:
            com.fossil.el7.b(r2)
            com.fossil.gj4 r0 = new com.fossil.gj4
            r0.<init>()
            java.lang.String r2 = "socialId"
            r0.n(r2, r9)
            com.fossil.gu4$b r2 = new com.fossil.gu4$b
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r0
            r1.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r2, r1)
            if (r0 != r3) goto L_0x0031
            r0 = r3
            goto L_0x0045
        L_0x0077:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x0094
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0045
        L_0x0094:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gu4.b(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.fossil.qn7<? super com.fossil.iq5<com.fossil.it4>> r9) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.fossil.gu4.c
            if (r0 == 0) goto L_0x0045
            r0 = r9
            com.fossil.gu4$c r0 = (com.fossil.gu4.c) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0045
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0053
            if (r3 != r5) goto L_0x004b
            java.lang.Object r0 = r0.L$0
            com.fossil.gu4 r0 = (com.fossil.gu4) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0028:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0078
            com.fossil.hz4 r1 = com.fossil.hz4.f1561a
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r1 = r0.a()
            com.fossil.gj4 r1 = (com.fossil.gj4) r1
            if (r1 != 0) goto L_0x0067
        L_0x003a:
            com.fossil.kq5 r1 = new com.fossil.kq5
            boolean r0 = r0.b()
            r1.<init>(r4, r0)
            r0 = r1
        L_0x0044:
            return r0
        L_0x0045:
            com.fossil.gu4$c r0 = new com.fossil.gu4$c
            r0.<init>(r8, r9)
            goto L_0x0014
        L_0x004b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0053:
            com.fossil.el7.b(r1)
            com.fossil.gu4$d r1 = new com.fossil.gu4$d
            r1.<init>(r8, r4)
            r0.L$0 = r8
            r0.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r1, r0)
            if (r0 != r2) goto L_0x0028
            r0 = r2
            goto L_0x0044
        L_0x0067:
            com.google.gson.Gson r2 = new com.google.gson.Gson     // Catch:{ mj4 -> 0x0073 }
            r2.<init>()     // Catch:{ mj4 -> 0x0073 }
            java.lang.Class<com.fossil.it4> r3 = com.fossil.it4.class
            java.lang.Object r4 = r2.g(r1, r3)     // Catch:{ mj4 -> 0x0073 }
            goto L_0x003a
        L_0x0073:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003a
        L_0x0078:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x0095
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0044
        L_0x0095:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gu4.c(com.fossil.qn7):java.lang.Object");
    }
}
