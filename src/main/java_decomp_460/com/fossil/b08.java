package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface b08 {
    @DexIgnore
    int a();

    @DexIgnore
    void b(a08<?> a08);

    @DexIgnore
    a08<?> e();

    @DexIgnore
    void f(int i);
}
