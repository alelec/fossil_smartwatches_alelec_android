package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nf5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;

    @DexIgnore
    public nf5(Object obj, View view, int i, FlexibleTextView flexibleTextView) {
        super(obj, view, i);
        this.q = flexibleTextView;
    }

    @DexIgnore
    @Deprecated
    public static nf5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (nf5) ViewDataBinding.p(layoutInflater, 2131558709, viewGroup, z, obj);
    }

    @DexIgnore
    public static nf5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, aq0.d());
    }
}
