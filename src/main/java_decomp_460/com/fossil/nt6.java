package com.fossil;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nt6 extends kq0 implements View.OnClickListener {
    @DexIgnore
    public static /* final */ a h; // = new a(null);
    @DexIgnore
    public b b;
    @DexIgnore
    public String c;
    @DexIgnore
    public FlexibleTextView d;
    @DexIgnore
    public FlexibleEditText e;
    @DexIgnore
    public ImageView f;
    @DexIgnore
    public HashMap g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final nt6 a(String str, b bVar) {
            pq7.c(bVar, "listener");
            nt6 nt6 = new nt6();
            nt6.b = bVar;
            nt6.c = str;
            return nt6;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str);

        @DexIgnore
        Object onCancel();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ nt6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(nt6 nt6) {
            this.b = nt6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            pq7.c(editable, "text");
            nt6 nt6 = this.b;
            String obj = editable.toString();
            int length = obj.length() - 1;
            boolean z = false;
            int i = 0;
            while (i <= length) {
                boolean z2 = obj.charAt(!z ? i : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            nt6.c = obj.subSequence(i, length + 1).toString();
            boolean z3 = !TextUtils.isEmpty(this.b.c);
            nt6.w6(this.b).setEnabled(z3);
            if (z3) {
                nt6.x6(this.b).setVisibility(0);
            } else {
                nt6.x6(this.b).setVisibility(8);
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            pq7.c(charSequence, "text");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            pq7.c(charSequence, "text");
        }
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView w6(nt6 nt6) {
        FlexibleTextView flexibleTextView = nt6.d;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        pq7.n("ftvRename");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ImageView x6(nt6 nt6) {
        ImageView imageView = nt6.f;
        if (imageView != null) {
            return imageView;
        }
        pq7.n("ivClearName");
        throw null;
    }

    @DexIgnore
    public final void B6(boolean z) {
        b bVar = this.b;
        if (bVar != null) {
            if (z) {
                bVar.onCancel();
            } else {
                String str = this.c;
                if (str != null) {
                    bVar.a(str);
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }
        dismiss();
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131362378) {
                B6(true);
            } else if (id != 2131362513) {
                if (id == 2131362684) {
                    FlexibleEditText flexibleEditText = this.e;
                    if (flexibleEditText != null) {
                        flexibleEditText.setText("");
                    } else {
                        pq7.n("fetRename");
                        throw null;
                    }
                }
            } else if (!TextUtils.isEmpty(this.c)) {
                B6(false);
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(1, 16973830);
    }

    @DexIgnore
    @Override // com.fossil.kq0
    public Dialog onCreateDialog(Bundle bundle) {
        Dialog onCreateDialog = super.onCreateDialog(bundle);
        pq7.b(onCreateDialog, "super.onCreateDialog(savedInstanceState)");
        onCreateDialog.requestWindowFeature(1);
        Window window = onCreateDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.setLayout(-1, -1);
        }
        return onCreateDialog;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(2131558617, viewGroup);
        View findViewById = inflate.findViewById(2131362301);
        pq7.b(findViewById, "view.findViewById(R.id.fet_rename)");
        this.e = (FlexibleEditText) findViewById;
        View findViewById2 = inflate.findViewById(2131362513);
        pq7.b(findViewById2, "view.findViewById(R.id.ftv_rename)");
        this.d = (FlexibleTextView) findViewById2;
        View findViewById3 = inflate.findViewById(2131362684);
        pq7.b(findViewById3, "view.findViewById(R.id.iv_clear_name)");
        this.f = (ImageView) findViewById3;
        String str = this.c;
        if (str != null) {
            FlexibleEditText flexibleEditText = this.e;
            if (flexibleEditText != null) {
                flexibleEditText.setText(str);
                FlexibleEditText flexibleEditText2 = this.e;
                if (flexibleEditText2 != null) {
                    flexibleEditText2.setSelection(str.length());
                    if (str.length() == 0) {
                        ImageView imageView = this.f;
                        if (imageView != null) {
                            imageView.setVisibility(8);
                        } else {
                            pq7.n("ivClearName");
                            throw null;
                        }
                    } else {
                        ImageView imageView2 = this.f;
                        if (imageView2 != null) {
                            imageView2.setVisibility(0);
                        } else {
                            pq7.n("ivClearName");
                            throw null;
                        }
                    }
                } else {
                    pq7.n("fetRename");
                    throw null;
                }
            } else {
                pq7.n("fetRename");
                throw null;
            }
        }
        FlexibleEditText flexibleEditText3 = this.e;
        if (flexibleEditText3 != null) {
            flexibleEditText3.addTextChangedListener(new c(this));
            ImageView imageView3 = this.f;
            if (imageView3 != null) {
                imageView3.setOnClickListener(this);
                FlexibleTextView flexibleTextView = this.d;
                if (flexibleTextView != null) {
                    flexibleTextView.setOnClickListener(this);
                    inflate.findViewById(2131362378).setOnClickListener(this);
                    return inflate;
                }
                pq7.n("ftvRename");
                throw null;
            }
            pq7.n("ivClearName");
            throw null;
        }
        pq7.n("fetRename");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    public void v6() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
