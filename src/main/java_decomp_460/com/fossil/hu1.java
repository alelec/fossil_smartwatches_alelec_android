package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum hu1 {
    SKIP_LIST,
    SKIP_ERASE,
    SKIP_ERASE_CACHE_AFTER_SUCCESS,
    NUMBER_OF_FILE_REQUIRED,
    ERASE_CACHE_FILE_BEFORE_GET
}
