package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class az4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f365a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ nf5 f366a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(nf5 nf5, View view) {
            super(view);
            pq7.c(nf5, "binding");
            pq7.c(view, "root");
            this.f366a = nf5;
        }

        @DexIgnore
        public final void a(String str) {
            pq7.c(str, "name");
            FlexibleTextView flexibleTextView = this.f366a.q;
            pq7.b(flexibleTextView, "binding.ftvTitle");
            flexibleTextView.setText(str);
        }
    }

    @DexIgnore
    public az4(int i) {
        this.f365a = i;
    }

    @DexIgnore
    public final int a() {
        return this.f365a;
    }

    @DexIgnore
    public boolean b(List<? extends Object> list, int i) {
        pq7.c(list, "items");
        return list.get(i) instanceof String;
    }

    @DexIgnore
    public void c(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        Object obj = null;
        pq7.c(list, "items");
        pq7.c(viewHolder, "holder");
        a aVar = (a) (!(viewHolder instanceof a) ? null : viewHolder);
        Object obj2 = list.get(i);
        if (obj2 instanceof String) {
            obj = obj2;
        }
        String str = (String) obj;
        if (aVar != null && str != null) {
            aVar.a(str);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder d(ViewGroup viewGroup) {
        pq7.c(viewGroup, "parent");
        nf5 z = nf5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemRecommendedChallenge\u2026(inflater, parent, false)");
        View n = z.n();
        pq7.b(n, "binding.root");
        return new a(z, n);
    }
}
