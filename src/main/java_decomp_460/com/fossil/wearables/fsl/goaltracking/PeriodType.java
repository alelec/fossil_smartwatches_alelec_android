package com.fossil.wearables.fsl.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum PeriodType {
    UNKNOWN(-1),
    DAY(0),
    WEEK(1),
    MONTH(2);
    
    @DexIgnore
    public int value;

    @DexIgnore
    public PeriodType(int i) {
        this.value = i;
    }

    @DexIgnore
    public static PeriodType fromInt(int i) {
        PeriodType[] values = values();
        for (PeriodType periodType : values) {
            if (periodType.getValue() == i) {
                return periodType;
            }
        }
        return UNKNOWN;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
