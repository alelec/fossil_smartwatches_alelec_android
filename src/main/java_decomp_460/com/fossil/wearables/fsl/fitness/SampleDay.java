package com.fossil.wearables.fsl.fitness;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.field.DatabaseField;
import com.misfit.frameworks.common.log.MFLogger;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SampleDay implements Serializable {
    @DexIgnore
    public static /* final */ String COLUMN_ACTIVE_TIME; // = "activeTime";
    @DexIgnore
    public static /* final */ String COLUMN_ACTIVE_TIME_GOAL; // = "activeTimeGoal";
    @DexIgnore
    public static /* final */ String COLUMN_CALORIES; // = "calories";
    @DexIgnore
    public static /* final */ String COLUMN_CALORIES_GOAL; // = "caloriesGoal";
    @DexIgnore
    public static /* final */ String COLUMN_CREATED_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String COLUMN_DAY; // = "day";
    @DexIgnore
    public static /* final */ String COLUMN_DISTANCE; // = "distance";
    @DexIgnore
    public static /* final */ String COLUMN_DST_OFFSET; // = "dstOffset";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_INTENSITIES; // = "intensities";
    @DexIgnore
    public static /* final */ String COLUMN_MONTH; // = "month";
    @DexIgnore
    public static /* final */ String COLUMN_PIN_TYPE; // = "pinType";
    @DexIgnore
    public static /* final */ String COLUMN_STEPS; // = "steps";
    @DexIgnore
    public static /* final */ String COLUMN_STEP_GOAL; // = "stepGoal";
    @DexIgnore
    public static /* final */ String COLUMN_TIMEZONE_NAME; // = "timezoneName";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATED_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ String COLUMN_YEAR; // = "year";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "sampleday";
    @DexIgnore
    public static /* final */ String TAG; // = SampleDay.class.getSimpleName();
    @DexIgnore
    @DatabaseField(columnName = COLUMN_ACTIVE_TIME)
    public int activeTime;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_ACTIVE_TIME_GOAL)
    public int activeTimeGoal;
    @DexIgnore
    @DatabaseField(columnName = "calories")
    public double calories;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_CALORIES_GOAL)
    public int caloriesGoal;
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    public long createdAt;
    @DexIgnore
    @DatabaseField(columnName = "day")
    public int day;
    @DexIgnore
    @DatabaseField(columnName = "distance")
    public double distance;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_DST_OFFSET)
    public int dstOffset;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String id;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_INTENSITIES)
    public String intensities;
    @DexIgnore
    @DatabaseField(columnName = "month")
    public int month;
    @DexIgnore
    @DatabaseField(columnName = "pinType")
    public int pinType;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_STEP_GOAL)
    public int stepGoal;
    @DexIgnore
    @DatabaseField(columnName = "steps")
    public double steps;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_TIMEZONE_NAME)
    public String timezoneName;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    public long updatedAt;
    @DexIgnore
    public URI uri;
    @DexIgnore
    @DatabaseField(columnName = "year")
    public int year;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends TypeToken<List<Integer>> {
        @DexIgnore
        public Anon1() {
        }
    }

    @DexIgnore
    public SampleDay() {
    }

    @DexIgnore
    public SampleDay(int i, int i2, int i3, String str, int i4, double d, double d2, double d3, List<Integer> list) {
        this.year = i;
        this.month = i2;
        this.day = i3;
        this.timezoneName = str;
        this.dstOffset = i4;
        this.steps = d;
        this.calories = d2;
        this.distance = d3;
        URI generateURI = FitnessURI.generateURI(this);
        this.uri = generateURI;
        this.id = generateURI.toASCIIString();
        this.pinType = 0;
        setIntensities(list);
    }

    @DexIgnore
    public int getActiveTime() {
        return this.activeTime;
    }

    @DexIgnore
    public int getActiveTimeGoal() {
        return this.activeTimeGoal;
    }

    @DexIgnore
    public double getCalories() {
        return this.calories;
    }

    @DexIgnore
    public int getCaloriesGoal() {
        return this.caloriesGoal;
    }

    @DexIgnore
    public long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public int getDay() {
        return this.day;
    }

    @DexIgnore
    public double getDistance() {
        return this.distance;
    }

    @DexIgnore
    public int getDstOffset() {
        return this.dstOffset;
    }

    @DexIgnore
    public List<Integer> getIntensities() {
        ArrayList arrayList = new ArrayList();
        String str = this.intensities;
        if (str == null) {
            return arrayList;
        }
        if (str.isEmpty()) {
            return arrayList;
        }
        try {
            return (List) new Gson().l(this.intensities, new Anon1().getType());
        } catch (Exception e) {
            String str2 = TAG;
            MFLogger.d(str2, "exception=" + e);
            return arrayList;
        }
    }

    @DexIgnore
    public int getMonth() {
        return this.month;
    }

    @DexIgnore
    public int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public int getStepGoal() {
        return this.stepGoal;
    }

    @DexIgnore
    public double getSteps() {
        return this.steps;
    }

    @DexIgnore
    public String getTimezoneName() {
        return this.timezoneName;
    }

    @DexIgnore
    public long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public URI getUri() {
        String str;
        if (this.uri == null && (str = this.id) != null) {
            this.uri = URI.create(str);
        }
        return this.uri;
    }

    @DexIgnore
    public int getYear() {
        return this.year;
    }

    @DexIgnore
    public void setActiveTime(int i) {
        this.activeTime = i;
    }

    @DexIgnore
    public void setActiveTimeGoal(int i) {
        this.activeTimeGoal = i;
    }

    @DexIgnore
    public void setCalories(double d) {
        this.calories = d;
    }

    @DexIgnore
    public void setCaloriesGoal(int i) {
        this.caloriesGoal = i;
    }

    @DexIgnore
    public void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public void setDistance(double d) {
        this.distance = d;
    }

    @DexIgnore
    public void setIntensities(List<Integer> list) {
        this.intensities = new Gson().t(list);
    }

    @DexIgnore
    public void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public void setStepGoal(int i) {
        this.stepGoal = i;
    }

    @DexIgnore
    public void setSteps(double d) {
        this.steps = d;
    }

    @DexIgnore
    public void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "[SampleDay: date=" + this.year + "/" + this.month + "/" + this.day + ", timezone=" + this.timezoneName + ", steps=" + this.steps + ", calories=" + this.calories + ", distance=" + this.distance + ", stepGoal=" + this.stepGoal + ", caloriesGoal=" + this.caloriesGoal + ", activeTimeGoal=" + this.activeTimeGoal + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", intensities=" + this.intensities + ", pinType=" + this.pinType + "]";
    }
}
