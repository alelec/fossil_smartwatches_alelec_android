package com.fossil.wearables.fsl.contact;

import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import com.fossil.wearables.fsl.shared.BaseModel;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Contact extends BaseModel {
    @DexIgnore
    @DatabaseField
    public String companyName;
    @DexIgnore
    @DatabaseField(columnName = "contact_group_id", foreign = true, foreignAutoRefresh = true)
    public ContactGroup contactGroup;
    @DexIgnore
    @DatabaseField
    public int contactId;
    @DexIgnore
    public String displayName;
    @DexIgnore
    @ForeignCollectionField(eager = true)
    public ForeignCollection<EmailAddress> emailAddresses;
    @DexIgnore
    @DatabaseField
    public String firstName;
    @DexIgnore
    @DatabaseField
    public String lastName;
    @DexIgnore
    @ForeignCollectionField(eager = true)
    public ForeignCollection<PhoneNumber> phoneNumbers;
    @DexIgnore
    @DatabaseField
    public String photoThumbUri;
    @DexIgnore
    @DatabaseField
    public boolean useCall;
    @DexIgnore
    @DatabaseField
    public boolean useEmail;
    @DexIgnore
    @DatabaseField
    public boolean useSms;

    @DexIgnore
    private boolean arePhoneNumbersEqual(String str, String str2) {
        return PhoneNumberUtils.compare(str, str2);
    }

    @DexIgnore
    public boolean containsEmail(String str) {
        if (!TextUtils.isEmpty(str) && getEmailAddresses() != null && getEmailAddresses().size() > 0) {
            for (EmailAddress emailAddress : getEmailAddresses()) {
                if (!(emailAddress == null || TextUtils.isEmpty(emailAddress.getAddress()) || !emailAddress.getAddress().equals(str))) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public boolean containsPhoneNumber(String str) {
        if (!TextUtils.isEmpty(str) && getPhoneNumbers() != null && getPhoneNumbers().size() > 0) {
            for (PhoneNumber phoneNumber : getPhoneNumbers()) {
                if (!(phoneNumber == null || TextUtils.isEmpty(phoneNumber.getNumber()) || !arePhoneNumbersEqual(phoneNumber.getNumber(), str))) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public String getCompanyName() {
        return this.companyName;
    }

    @DexIgnore
    public ContactGroup getContactGroup() {
        return this.contactGroup;
    }

    @DexIgnore
    public int getContactId() {
        return this.contactId;
    }

    @DexIgnore
    public String getDisplayName() {
        if (!TextUtils.isEmpty(this.displayName)) {
            return this.displayName;
        }
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(this.firstName)) {
            sb.append(this.firstName);
        }
        if (!TextUtils.isEmpty(this.lastName)) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(this.lastName);
        }
        String sb2 = sb.toString();
        this.displayName = sb2;
        return sb2;
    }

    @DexIgnore
    public List<EmailAddress> getEmailAddresses() {
        ArrayList arrayList = new ArrayList();
        ForeignCollection<EmailAddress> foreignCollection = this.emailAddresses;
        return (foreignCollection == null || foreignCollection.size() <= 0) ? arrayList : new ArrayList(this.emailAddresses);
    }

    @DexIgnore
    public String getFirstName() {
        return this.firstName;
    }

    @DexIgnore
    public String getLastName() {
        return this.lastName;
    }

    @DexIgnore
    public List<PhoneNumber> getPhoneNumbers() {
        ArrayList arrayList = new ArrayList();
        ForeignCollection<PhoneNumber> foreignCollection = this.phoneNumbers;
        return (foreignCollection == null || foreignCollection.size() <= 0) ? arrayList : new ArrayList(this.phoneNumbers);
    }

    @DexIgnore
    public String getPhotoThumbUri() {
        return this.photoThumbUri;
    }

    @DexIgnore
    public boolean isUseCall() {
        return this.useCall;
    }

    @DexIgnore
    public boolean isUseEmail() {
        return this.useEmail;
    }

    @DexIgnore
    public boolean isUseSms() {
        return this.useSms;
    }

    @DexIgnore
    public void setCompanyName(String str) {
        this.companyName = str;
    }

    @DexIgnore
    public void setContactGroup(ContactGroup contactGroup2) {
        this.contactGroup = contactGroup2;
    }

    @DexIgnore
    public void setContactId(int i) {
        this.contactId = i;
    }

    @DexIgnore
    public void setFirstName(String str) {
        this.firstName = str;
    }

    @DexIgnore
    public void setLastName(String str) {
        this.lastName = str;
    }

    @DexIgnore
    public void setPhotoThumbUri(String str) {
        this.photoThumbUri = str;
    }

    @DexIgnore
    public void setUseCall(boolean z) {
        this.useCall = z;
    }

    @DexIgnore
    public void setUseEmail(boolean z) {
        this.useEmail = z;
    }

    @DexIgnore
    public void setUseSms(boolean z) {
        this.useSms = z;
    }
}
