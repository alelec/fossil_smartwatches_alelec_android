package com.fossil.wearables.fsl.contact;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ContactProviderImpl extends BaseDbProvider implements ContactProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "entourage.db";

    @DexIgnore
    public ContactProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private List<Contact> getAllContacts() {
        try {
            return getContactDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    private boolean isContactExisted(Contact contact) {
        ContentResolver contentResolver = this.context.getContentResolver();
        int contactId = contact.getContactId();
        Cursor query = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, "name_raw_contact_id=?", new String[]{String.valueOf(contactId)}, null);
        if (query == null) {
            return false;
        }
        boolean moveToFirst = query.moveToFirst();
        query.close();
        return moveToFirst;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00c5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00c6, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00c9, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0078, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0079, code lost:
        r0 = r6;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00c5 A[ExcHandler: all (r0v1 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void updateContactToDatabase(com.fossil.wearables.fsl.contact.Contact r11) {
        /*
        // Method dump skipped, instructions count: 333
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wearables.fsl.contact.ContactProviderImpl.updateContactToDatabase(com.fossil.wearables.fsl.contact.Contact):void");
    }

    @DexIgnore
    public void clearAllTables() {
        try {
            TableUtils.clearTable(getContactGroupDao().getConnectionSource(), ContactGroup.class);
            TableUtils.clearTable(getContactDao().getConnectionSource(), Contact.class);
            TableUtils.clearTable(getPhoneNumberDao().getConnectionSource(), PhoneNumber.class);
            TableUtils.clearTable(getEmailAddressDao().getConnectionSource(), EmailAddress.class);
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "clearAllTables Exception=" + e);
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.contact.ContactProvider
    public List<ContactGroup> getAllContactGroups() {
        ArrayList arrayList = new ArrayList();
        try {
            return getContactGroupDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.contact.ContactProvider
    public List<ContactGroup> getAllContactGroups(int i) {
        ArrayList arrayList = new ArrayList();
        try {
            return getContactGroupDao().queryForEq("deviceFamily", Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.contact.ContactProvider
    public Contact getContact(int i) {
        try {
            return getContactDao().queryForId(Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public Dao<Contact, Integer> getContactDao() throws SQLException {
        return this.databaseHelper.getDao(Contact.class);
    }

    @DexIgnore
    public ContactGroup getContactGroup(int i) {
        try {
            return getContactGroupDao().queryForId(Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public Dao<ContactGroup, Integer> getContactGroupDao() throws SQLException {
        return this.databaseHelper.getDao(ContactGroup.class);
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingEmail(String str) {
        Contact contactWithEmail;
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = getAllContactGroups();
        if (allContactGroups != null && allContactGroups.size() > 0) {
            for (ContactGroup contactGroup : allContactGroups) {
                if (contactGroup != null && contactGroup.isEnabled() && (contactWithEmail = contactGroup.getContactWithEmail(str)) != null && contactWithEmail.isUseEmail()) {
                    arrayList.add(contactGroup);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.contact.ContactProvider
    public List<ContactGroup> getContactGroupsMatchingEmail(String str, int i) {
        Contact contactWithEmail;
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = getAllContactGroups(i);
        if (allContactGroups != null && allContactGroups.size() > 0) {
            for (ContactGroup contactGroup : allContactGroups) {
                if (contactGroup != null && contactGroup.isEnabled() && (contactWithEmail = contactGroup.getContactWithEmail(str)) != null && contactWithEmail.isUseEmail()) {
                    arrayList.add(contactGroup);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingIncomingCall(String str) {
        Contact contactWithPhoneNumber;
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = getAllContactGroups();
        if (allContactGroups != null && allContactGroups.size() > 0) {
            for (ContactGroup contactGroup : allContactGroups) {
                if (contactGroup != null && contactGroup.isEnabled() && (contactWithPhoneNumber = contactGroup.getContactWithPhoneNumber(str)) != null && contactWithPhoneNumber.isUseCall()) {
                    arrayList.add(contactGroup);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.contact.ContactProvider
    public List<ContactGroup> getContactGroupsMatchingIncomingCall(String str, int i) {
        Contact contactWithPhoneNumber;
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = getAllContactGroups(i);
        if (allContactGroups != null && allContactGroups.size() > 0) {
            for (ContactGroup contactGroup : allContactGroups) {
                if (contactGroup != null && contactGroup.isEnabled() && (contactWithPhoneNumber = contactGroup.getContactWithPhoneNumber(str)) != null && contactWithPhoneNumber.isUseCall()) {
                    arrayList.add(contactGroup);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingSms(String str) {
        Contact contactWithPhoneNumber;
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = getAllContactGroups();
        if (allContactGroups != null && allContactGroups.size() > 0) {
            for (ContactGroup contactGroup : allContactGroups) {
                if (contactGroup != null && contactGroup.isEnabled() && (contactWithPhoneNumber = contactGroup.getContactWithPhoneNumber(str)) != null && contactWithPhoneNumber.isUseSms()) {
                    arrayList.add(contactGroup);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.contact.ContactProvider
    public List<ContactGroup> getContactGroupsMatchingSms(String str, int i) {
        Contact contactWithPhoneNumber;
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = getAllContactGroups(i);
        if (allContactGroups != null && allContactGroups.size() > 0) {
            for (ContactGroup contactGroup : allContactGroups) {
                if (contactGroup != null && contactGroup.isEnabled() && (contactWithPhoneNumber = contactGroup.getContactWithPhoneNumber(str)) != null && contactWithPhoneNumber.isUseSms()) {
                    arrayList.add(contactGroup);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{Contact.class, ContactGroup.class, EmailAddress.class, PhoneNumber.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider, com.fossil.wearables.fsl.contact.ContactProvider
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 5;
    }

    @DexIgnore
    public Dao<EmailAddress, Integer> getEmailAddressDao() throws SQLException {
        return this.databaseHelper.getDao(EmailAddress.class);
    }

    @DexIgnore
    public Dao<PhoneNumber, Integer> getPhoneNumberDao() throws SQLException {
        return this.databaseHelper.getDao(PhoneNumber.class);
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.contact.ContactProvider
    public void removeAllContactGroups() {
        for (ContactGroup contactGroup : getAllContactGroups()) {
            removeContactGroup(contactGroup);
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.contact.ContactProvider
    public void removeContact(Contact contact) {
        if (contact != null) {
            try {
                for (PhoneNumber phoneNumber : contact.getPhoneNumbers()) {
                    removePhoneNumber(phoneNumber);
                }
                for (EmailAddress emailAddress : contact.getEmailAddresses()) {
                    removeEmailAddress(emailAddress);
                }
                getContactDao().delete((Dao<Contact, Integer>) contact);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.contact.ContactProvider
    public void removeContactGroup(ContactGroup contactGroup) {
        try {
            for (Contact contact : contactGroup.getContacts()) {
                removeContact(contact);
            }
            getContactGroupDao().delete((Dao<ContactGroup, Integer>) contactGroup);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    public void removeEmailAddress(EmailAddress emailAddress) {
        try {
            getEmailAddressDao().delete((Dao<EmailAddress, Integer>) emailAddress);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    public void removePhoneNumber(PhoneNumber phoneNumber) {
        try {
            getPhoneNumberDao().delete((Dao<PhoneNumber, Integer>) phoneNumber);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.contact.ContactProvider
    public void saveContact(Contact contact) {
        if (contact != null) {
            try {
                Contact queryForSameId = getContactDao().queryForSameId(contact);
                if (queryForSameId != null) {
                    contact.setDbRowId(queryForSameId.getDbRowId());
                }
                getContactDao().createOrUpdate(contact);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.contact.ContactProvider
    public void saveContactGroup(ContactGroup contactGroup) {
        if (contactGroup != null) {
            try {
                getContactGroupDao().createOrUpdate(contactGroup);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.contact.ContactProvider
    public void saveEmailAddress(EmailAddress emailAddress) {
        EmailAddress emailAddress2;
        if (emailAddress != null && emailAddress.getContact() != null) {
            try {
                QueryBuilder<EmailAddress, Integer> queryBuilder = getEmailAddressDao().queryBuilder();
                queryBuilder.where().idEq(Integer.valueOf(emailAddress.getContact().getContactId()));
                List<EmailAddress> query = getEmailAddressDao().query(queryBuilder.prepare());
                if (!(query == null || query.size() <= 0 || (emailAddress2 = query.get(0)) == null)) {
                    emailAddress.setDbRowId(emailAddress2.getDbRowId());
                }
                getEmailAddressDao().createOrUpdate(emailAddress);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.contact.ContactProvider
    public void savePhoneNumber(PhoneNumber phoneNumber) {
        PhoneNumber phoneNumber2;
        if (phoneNumber != null && phoneNumber.getContact() != null) {
            try {
                QueryBuilder<PhoneNumber, Integer> queryBuilder = getPhoneNumberDao().queryBuilder();
                queryBuilder.where().idEq(Integer.valueOf(phoneNumber.getContact().getContactId()));
                List<PhoneNumber> query = getPhoneNumberDao().query(queryBuilder.prepare());
                if (!(query == null || query.size() <= 0 || (phoneNumber2 = query.get(0)) == null)) {
                    phoneNumber.setDbRowId(phoneNumber2.getDbRowId());
                }
                getPhoneNumberDao().createOrUpdate(phoneNumber);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void syncContacts() {
        List<ContactGroup> allContactGroups = getAllContactGroups();
        if (allContactGroups != null && allContactGroups.size() > 0) {
            for (ContactGroup contactGroup : allContactGroups) {
                List<Contact> contacts = contactGroup.getContacts();
                if (contacts != null && contacts.size() > 0) {
                    for (Contact contact : contacts) {
                        if (isContactExisted(contact)) {
                            updateContactToDatabase(contact);
                        } else {
                            removeContact(contact);
                        }
                    }
                }
            }
        }
        for (ContactGroup contactGroup2 : getAllContactGroups()) {
            if (contactGroup2.getContacts().size() == 0) {
                removeContactGroup(contactGroup2);
            }
        }
    }
}
