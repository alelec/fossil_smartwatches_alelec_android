package com.fossil.wearables.fsl.codeword;

import com.fossil.wearables.fsl.shared.BaseModel;
import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Word extends BaseModel {
    @DexIgnore
    @DatabaseField(columnName = "word_group_id", foreign = true, foreignAutoRefresh = true)
    public WordGroup group;
    @DexIgnore
    @DatabaseField
    public String value;

    @DexIgnore
    public WordGroup getGroup() {
        return this.group;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }

    @DexIgnore
    public void setGroup(WordGroup wordGroup) {
        this.group = wordGroup;
    }

    @DexIgnore
    public void setValue(String str) {
        this.value = str;
    }
}
