package com.fossil;

import com.fossil.vf4;
import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ye4 implements vf4.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ FirebaseInstanceId f4307a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public ye4(FirebaseInstanceId firebaseInstanceId, String str, String str2, String str3) {
        this.f4307a = firebaseInstanceId;
        this.b = str;
        this.c = str2;
        this.d = str3;
    }

    @DexIgnore
    @Override // com.fossil.vf4.a
    public final nt3 start() {
        return this.f4307a.B(this.b, this.c, this.d);
    }
}
