package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y88 implements e88<w18, Boolean> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ y88 f4260a; // = new y88();

    @DexIgnore
    /* renamed from: b */
    public Boolean a(w18 w18) throws IOException {
        return Boolean.valueOf(w18.string());
    }
}
