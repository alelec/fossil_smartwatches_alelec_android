package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gj extends qq7 implements rp7<lp, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ ko b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gj(ko koVar) {
        super(1);
        this.b = koVar;
    }

    @DexIgnore
    public final void a(lp lpVar) {
        this.b.U = System.currentTimeMillis();
        ko koVar = this.b;
        zk1 zk1 = ((fh) lpVar).C;
        koVar.X = zk1;
        koVar.W = zk1.getFirmwareVersion();
        ko koVar2 = this.b;
        String str = koVar2.a0;
        if (str == null || vt7.j(koVar2.W, str, true)) {
            ko koVar3 = this.b;
            koVar3.Z = false;
            koVar3.l(nr.a(koVar3.v, null, zq.SUCCESS, null, null, 13));
            return;
        }
        ko koVar4 = this.b;
        koVar4.Z = false;
        koVar4.l(nr.a(koVar4.v, null, zq.TARGET_FIRMWARE_NOT_MATCHED, null, null, 13));
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(lp lpVar) {
        a(lpVar);
        return tl7.f3441a;
    }
}
