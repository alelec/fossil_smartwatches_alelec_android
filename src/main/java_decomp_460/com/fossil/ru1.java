package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ru1 extends iw1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ru1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* renamed from: a */
        public ru1 createFromParcel(Parcel parcel) {
            return new ru1(parcel, null);
        }

        @DexIgnore
        public final ru1 b(byte[] bArr) {
            try {
                iw1 iw1 = (iw1) ga.d.f(bArr);
                if (iw1 instanceof ru1) {
                    return (ru1) iw1;
                }
                throw new IllegalArgumentException("Failed requirement.".toString());
            } catch (IllegalArgumentException e) {
                return null;
            }
        }

        @DexIgnore
        /* renamed from: c */
        public ru1[] newArray(int i) {
            return new ru1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ru1(Parcel parcel, kq7 kq7) {
        super(parcel);
        j();
    }

    @DexIgnore
    public ru1(ry1 ry1, yb0 yb0, cc0[] cc0Arr, cc0[] cc0Arr2, cc0[] cc0Arr3, cc0[] cc0Arr4, cc0[] cc0Arr5, cc0[] cc0Arr6, cc0[] cc0Arr7) throws IllegalArgumentException {
        super(ry1, yb0, cc0Arr, cc0Arr2, cc0Arr3, cc0Arr4, cc0Arr5, cc0Arr6, cc0Arr7);
        j();
    }

    @DexIgnore
    @Override // com.fossil.iw1, com.fossil.iw1, java.lang.Object
    public ru1 clone() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.iw1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ru1)) {
            return false;
        }
        ru1 ru1 = (ru1) obj;
        if (!pq7.a(getBundleId(), ru1.getBundleId())) {
            return false;
        }
        return getPackageCrc() == ru1.getPackageCrc();
    }

    @DexIgnore
    @Override // com.fossil.iw1
    public int hashCode() {
        return (getBundleId().hashCode() * 31) + Long.valueOf(getPackageCrc()).hashCode();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0016, code lost:
        if ((!(f().length == 0)) != false) goto L_0x0018;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void j() {
        /*
            r4 = this;
            r0 = 1
            r1 = 0
            com.fossil.yb0 r2 = r4.g()
            com.fossil.jw1 r2 = r2.b
            com.fossil.jw1 r3 = com.fossil.jw1.WATCH_APP
            if (r2 != r3) goto L_0x0029
            com.fossil.cc0[] r2 = r4.f()
            int r2 = r2.length
            if (r2 != 0) goto L_0x001b
            r2 = r0
        L_0x0014:
            r2 = r2 ^ 1
            if (r2 == 0) goto L_0x0029
        L_0x0018:
            if (r0 == 0) goto L_0x001d
            return
        L_0x001b:
            r2 = r1
            goto L_0x0014
        L_0x001d:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Incorrect package type."
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0029:
            r0 = r1
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ru1.j():void");
    }

    @DexIgnore
    @Override // com.fossil.iw1, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(new JSONObject(), jd0.U0, hy1.k((int) getPackageCrc(), null, 1, null)), jd0.M4, getBundleId());
    }
}
