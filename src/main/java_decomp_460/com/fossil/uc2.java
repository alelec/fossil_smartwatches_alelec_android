package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uc2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<uc2> CREATOR; // = new ce2();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    @Deprecated
    public /* final */ Scope[] e;

    @DexIgnore
    public uc2(int i, int i2, int i3, Scope[] scopeArr) {
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = scopeArr;
    }

    @DexIgnore
    public uc2(int i, int i2, Scope[] scopeArr) {
        this(1, i, i2, null);
    }

    @DexIgnore
    public int c() {
        return this.c;
    }

    @DexIgnore
    public int f() {
        return this.d;
    }

    @DexIgnore
    @Deprecated
    public Scope[] h() {
        return this.e;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.n(parcel, 1, this.b);
        bd2.n(parcel, 2, c());
        bd2.n(parcel, 3, f());
        bd2.x(parcel, 4, h(), i, false);
        bd2.b(parcel, a2);
    }
}
