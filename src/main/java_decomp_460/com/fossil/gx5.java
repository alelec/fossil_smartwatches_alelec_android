package com.fossil;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gx5 extends RecyclerView.g<b> implements Filterable {
    @DexIgnore
    public List<i06> b; // = new ArrayList();
    @DexIgnore
    public int c;
    @DexIgnore
    public List<i06> d;
    @DexIgnore
    public a e;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(i06 i06, boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ld5 f1390a;
        @DexIgnore
        public /* final */ /* synthetic */ gx5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List list = this.b.b.d;
                    if (list != null) {
                        InstalledApp installedApp = ((i06) list.get(adapterPosition)).getInstalledApp();
                        Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                        if (isSelected != null) {
                            boolean booleanValue = isSelected.booleanValue();
                            if (booleanValue) {
                                List list2 = this.b.b.d;
                                if (list2 == null) {
                                    pq7.i();
                                    throw null;
                                } else if (((i06) list2.get(adapterPosition)).getCurrentHandGroup() != this.b.b.c) {
                                    a aVar = this.b.b.e;
                                    if (aVar != null) {
                                        List list3 = this.b.b.d;
                                        if (list3 != null) {
                                            aVar.a((i06) list3.get(adapterPosition), booleanValue);
                                            return;
                                        } else {
                                            pq7.i();
                                            throw null;
                                        }
                                    } else {
                                        return;
                                    }
                                }
                            }
                            a aVar2 = this.b.b.e;
                            if (aVar2 != null) {
                                List list4 = this.b.b.d;
                                if (list4 != null) {
                                    aVar2.a((i06) list4.get(adapterPosition), !booleanValue);
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(gx5 gx5, ld5 ld5) {
            super(ld5.n());
            pq7.c(ld5, "binding");
            this.b = gx5;
            this.f1390a = ld5;
            ld5.w.setOnClickListener(new a(this));
            String d = qn5.l.a().d("nonBrandSeparatorLine");
            String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d2)) {
                this.f1390a.q.setBackgroundColor(Color.parseColor(d2));
            }
            if (!TextUtils.isEmpty(d)) {
                this.f1390a.u.setBackgroundColor(Color.parseColor(d));
            }
        }

        @DexIgnore
        public final void a(i06 i06) {
            pq7.c(i06, "appWrapper");
            ImageView imageView = this.f1390a.t;
            pq7.b(imageView, "binding.ivAppIcon");
            tj5.a(imageView.getContext()).G(i06.getUri()).u0(((fj1) ((fj1) new fj1().n()).l(wc1.f3916a)).m0(true)).n().F0(this.f1390a.t);
            FlexibleTextView flexibleTextView = this.f1390a.r;
            pq7.b(flexibleTextView, "binding.ftvAppName");
            InstalledApp installedApp = i06.getInstalledApp();
            flexibleTextView.setText(installedApp != null ? installedApp.getTitle() : null);
            FlexibleSwitchCompat flexibleSwitchCompat = this.f1390a.w;
            pq7.b(flexibleSwitchCompat, "binding.swEnabled");
            InstalledApp installedApp2 = i06.getInstalledApp();
            Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
            if (isSelected != null) {
                flexibleSwitchCompat.setChecked(isSelected.booleanValue());
                if (i06.getCurrentHandGroup() == 0 || i06.getCurrentHandGroup() == this.b.c) {
                    FlexibleTextView flexibleTextView2 = this.f1390a.s;
                    pq7.b(flexibleTextView2, "binding.ftvAssigned");
                    flexibleTextView2.setText("");
                    FlexibleTextView flexibleTextView3 = this.f1390a.s;
                    pq7.b(flexibleTextView3, "binding.ftvAssigned");
                    flexibleTextView3.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView4 = this.f1390a.s;
                pq7.b(flexibleTextView4, "binding.ftvAssigned");
                hr7 hr7 = hr7.f1520a;
                String c = um5.c(PortfolioApp.h0.c(), 2131886157);
                pq7.b(c, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i06.getCurrentHandGroup())}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView4.setText(format);
                FlexibleTextView flexibleTextView5 = this.f1390a.s;
                pq7.b(flexibleTextView5, "binding.ftvAssigned");
                flexibleTextView5.setVisibility(0);
                FlexibleSwitchCompat flexibleSwitchCompat2 = this.f1390a.w;
                pq7.b(flexibleSwitchCompat2, "binding.swEnabled");
                flexibleSwitchCompat2.setChecked(false);
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Filter {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gx5 f1391a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(gx5 gx5) {
            this.f1391a = gx5;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            String str;
            String title;
            pq7.c(charSequence, "constraint");
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (TextUtils.isEmpty(charSequence)) {
                filterResults.values = this.f1391a.b;
            } else {
                ArrayList arrayList = new ArrayList();
                String obj = charSequence.toString();
                if (obj != null) {
                    String lowerCase = obj.toLowerCase();
                    pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    for (i06 i06 : this.f1391a.b) {
                        InstalledApp installedApp = i06.getInstalledApp();
                        if (installedApp == null || (title = installedApp.getTitle()) == null) {
                            str = null;
                        } else if (title != null) {
                            str = title.toLowerCase();
                            pq7.b(str, "(this as java.lang.String).toLowerCase()");
                        } else {
                            throw new il7("null cannot be cast to non-null type java.lang.String");
                        }
                        if (str != null && wt7.v(str, lowerCase, false, 2, null)) {
                            arrayList.add(i06);
                        }
                    }
                    filterResults.values = arrayList;
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            pq7.c(charSequence, "charSequence");
            pq7.c(filterResults, "results");
            this.f1391a.d = (List) filterResults.values;
            this.f1391a.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public Filter getFilter() {
        return new c(this);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<i06> list = this.d;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: l */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        List<i06> list = this.d;
        if (list != null) {
            bVar.a(list.get(i));
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: m */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        ld5 z = ld5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemAppHybridNotificatio\u2026.context), parent, false)");
        return new b(this, z);
    }

    @DexIgnore
    public final void n(List<i06> list, int i) {
        pq7.c(list, "listAppWrapper");
        this.c = i;
        this.b = list;
        this.d = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void o(a aVar) {
        pq7.c(aVar, "listener");
        this.e = aVar;
    }
}
