package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v8 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ u8 CREATOR; // = new u8(null);
    @DexIgnore
    public /* final */ t8 b;
    @DexIgnore
    public /* final */ short c;

    @DexIgnore
    public v8(t8 t8Var, short s) {
        this.b = t8Var;
        this.c = (short) s;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(v8.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            v8 v8Var = (v8) obj;
            if (this.b != v8Var.b) {
                return false;
            }
            return this.c == v8Var.c;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncFrame");
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(new JSONObject(), jd0.v0, ey1.a(this.b)), jd0.A0, hy1.l(this.c, null, 1, null));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
    }
}
