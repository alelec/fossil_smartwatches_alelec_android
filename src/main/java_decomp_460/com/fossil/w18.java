package com.fossil;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class w18 implements Closeable {
    @DexIgnore
    public Reader reader;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends w18 {
        @DexIgnore
        public /* final */ /* synthetic */ r18 b;
        @DexIgnore
        public /* final */ /* synthetic */ long c;
        @DexIgnore
        public /* final */ /* synthetic */ k48 d;

        @DexIgnore
        public a(r18 r18, long j, k48 k48) {
            this.b = r18;
            this.c = j;
            this.d = k48;
        }

        @DexIgnore
        @Override // com.fossil.w18
        public long contentLength() {
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.w18
        public r18 contentType() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.w18
        public k48 source() {
            return this.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Reader {
        @DexIgnore
        public /* final */ k48 b;
        @DexIgnore
        public /* final */ Charset c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public Reader e;

        @DexIgnore
        public b(k48 k48, Charset charset) {
            this.b = k48;
            this.c = charset;
        }

        @DexIgnore
        @Override // java.io.Closeable, java.io.Reader, java.lang.AutoCloseable
        public void close() throws IOException {
            this.d = true;
            Reader reader = this.e;
            if (reader != null) {
                reader.close();
            } else {
                this.b.close();
            }
        }

        @DexIgnore
        @Override // java.io.Reader
        public int read(char[] cArr, int i, int i2) throws IOException {
            if (!this.d) {
                Reader reader = this.e;
                if (reader == null) {
                    reader = new InputStreamReader(this.b.n0(), b28.c(this.b, this.c));
                    this.e = reader;
                }
                return reader.read(cArr, i, i2);
            }
            throw new IOException("Stream closed");
        }
    }

    @DexIgnore
    private Charset charset() {
        r18 contentType = contentType();
        return contentType != null ? contentType.b(b28.i) : b28.i;
    }

    @DexIgnore
    public static w18 create(r18 r18, long j, k48 k48) {
        if (k48 != null) {
            return new a(r18, j, k48);
        }
        throw new NullPointerException("source == null");
    }

    @DexIgnore
    public static w18 create(r18 r18, l48 l48) {
        i48 i48 = new i48();
        i48.t0(l48);
        return create(r18, (long) l48.size(), i48);
    }

    @DexIgnore
    public static w18 create(r18 r18, String str) {
        Charset charset = b28.i;
        if (r18 != null && (charset = r18.a()) == null) {
            charset = b28.i;
            r18 = r18.d(r18 + "; charset=utf-8");
        }
        i48 i48 = new i48();
        i48.C0(str, charset);
        return create(r18, i48.p0(), i48);
    }

    @DexIgnore
    public static w18 create(r18 r18, byte[] bArr) {
        i48 i48 = new i48();
        i48.u0(bArr);
        return create(r18, (long) bArr.length, i48);
    }

    @DexIgnore
    public final InputStream byteStream() {
        return source().n0();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final byte[] bytes() throws IOException {
        long contentLength = contentLength();
        if (contentLength <= 2147483647L) {
            k48 source = source();
            try {
                byte[] r = source.r();
                b28.g(source);
                if (contentLength == -1 || contentLength == ((long) r.length)) {
                    return r;
                }
                throw new IOException("Content-Length (" + contentLength + ") and stream length (" + r.length + ") disagree");
            } catch (Throwable th) {
                b28.g(source);
                throw th;
            }
        } else {
            throw new IOException("Cannot buffer entire body for content length: " + contentLength);
        }
    }

    @DexIgnore
    public final Reader charStream() {
        Reader reader2 = this.reader;
        if (reader2 != null) {
            return reader2;
        }
        b bVar = new b(source(), charset());
        this.reader = bVar;
        return bVar;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        b28.g(source());
    }

    @DexIgnore
    public abstract long contentLength();

    @DexIgnore
    public abstract r18 contentType();

    @DexIgnore
    public abstract k48 source();

    @DexIgnore
    public final String string() throws IOException {
        k48 source = source();
        try {
            return source.I(b28.c(source, charset()));
        } finally {
            b28.g(source);
        }
    }
}
