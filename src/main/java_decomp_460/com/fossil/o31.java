package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o31 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2626a;
    @DexIgnore
    public f11 b; // = f11.ENQUEUED;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public r01 e;
    @DexIgnore
    public r01 f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public long i;
    @DexIgnore
    public p01 j;
    @DexIgnore
    public int k;
    @DexIgnore
    public n01 l;
    @DexIgnore
    public long m;
    @DexIgnore
    public long n;
    @DexIgnore
    public long o;
    @DexIgnore
    public long p;
    @DexIgnore
    public boolean q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f2627a;
        @DexIgnore
        public f11 b;

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.b == aVar.b) {
                return this.f2627a.equals(aVar.f2627a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (this.f2627a.hashCode() * 31) + this.b.hashCode();
        }
    }

    /*
    static {
        x01.f("WorkSpec");
    }
    */

    @DexIgnore
    public o31(o31 o31) {
        r01 r01 = r01.c;
        this.e = r01;
        this.f = r01;
        this.j = p01.i;
        this.l = n01.EXPONENTIAL;
        this.m = 30000;
        this.p = -1;
        this.f2626a = o31.f2626a;
        this.c = o31.c;
        this.b = o31.b;
        this.d = o31.d;
        this.e = new r01(o31.e);
        this.f = new r01(o31.f);
        this.g = o31.g;
        this.h = o31.h;
        this.i = o31.i;
        this.j = new p01(o31.j);
        this.k = o31.k;
        this.l = o31.l;
        this.m = o31.m;
        this.n = o31.n;
        this.o = o31.o;
        this.p = o31.p;
        this.q = o31.q;
    }

    @DexIgnore
    public o31(String str, String str2) {
        r01 r01 = r01.c;
        this.e = r01;
        this.f = r01;
        this.j = p01.i;
        this.l = n01.EXPONENTIAL;
        this.m = 30000;
        this.p = -1;
        this.f2626a = str;
        this.c = str2;
    }

    @DexIgnore
    public long a() {
        boolean z = true;
        long j2 = 0;
        if (c()) {
            if (this.l != n01.LINEAR) {
                z = false;
            }
            return Math.min(18000000L, z ? this.m * ((long) this.k) : (long) Math.scalb((float) this.m, this.k - 1)) + this.n;
        } else if (d()) {
            long currentTimeMillis = System.currentTimeMillis();
            long j3 = this.n;
            if (j3 == 0) {
                j3 = this.g + currentTimeMillis;
            }
            if (this.i == this.h) {
                z = false;
            }
            if (z) {
                if (this.n == 0) {
                    j2 = this.i * -1;
                }
                return j3 + this.h + j2;
            }
            if (this.n != 0) {
                j2 = this.h;
            }
            return j3 + j2;
        } else {
            long j4 = this.n;
            if (j4 == 0) {
                j4 = System.currentTimeMillis();
            }
            return j4 + this.g;
        }
    }

    @DexIgnore
    public boolean b() {
        return !p01.i.equals(this.j);
    }

    @DexIgnore
    public boolean c() {
        return this.b == f11.ENQUEUED && this.k > 0;
    }

    @DexIgnore
    public boolean d() {
        return this.h != 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof o31)) {
            return false;
        }
        o31 o31 = (o31) obj;
        if (this.g != o31.g || this.h != o31.h || this.i != o31.i || this.k != o31.k || this.m != o31.m || this.n != o31.n || this.o != o31.o || this.p != o31.p || this.q != o31.q || !this.f2626a.equals(o31.f2626a) || this.b != o31.b || !this.c.equals(o31.c)) {
            return false;
        }
        String str = this.d;
        if (str != null) {
            if (!str.equals(o31.d)) {
                return false;
            }
        } else if (o31.d != null) {
            return false;
        }
        if (!this.e.equals(o31.e) || !this.f.equals(o31.f) || !this.j.equals(o31.j)) {
            return false;
        }
        if (this.l != o31.l) {
            z = false;
        }
        return z;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.f2626a.hashCode();
        int hashCode2 = this.b.hashCode();
        int hashCode3 = this.c.hashCode();
        String str = this.d;
        int hashCode4 = str != null ? str.hashCode() : 0;
        int hashCode5 = this.e.hashCode();
        int hashCode6 = this.f.hashCode();
        long j2 = this.g;
        long j3 = this.h;
        int i2 = (int) (j3 ^ (j3 >>> 32));
        long j4 = this.i;
        int hashCode7 = this.j.hashCode();
        int i3 = this.k;
        int hashCode8 = this.l.hashCode();
        long j5 = this.m;
        long j6 = this.n;
        int i4 = (int) (j6 ^ (j6 >>> 32));
        long j7 = this.o;
        long j8 = this.p;
        return ((((((((((((((((((((((((((hashCode4 + (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31)) * 31) + hashCode5) * 31) + hashCode6) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + i2) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + hashCode7) * 31) + i3) * 31) + hashCode8) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + i4) * 31) + ((int) (j7 ^ (j7 >>> 32)))) * 31) + ((int) ((j8 >>> 32) ^ j8))) * 31) + (this.q ? 1 : 0);
    }

    @DexIgnore
    public String toString() {
        return "{WorkSpec: " + this.f2626a + "}";
    }
}
