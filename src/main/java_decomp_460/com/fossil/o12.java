package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o12 implements Factory<h22> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<Context> f2611a;
    @DexIgnore
    public /* final */ Provider<k22> b;
    @DexIgnore
    public /* final */ Provider<v12> c;
    @DexIgnore
    public /* final */ Provider<t32> d;

    @DexIgnore
    public o12(Provider<Context> provider, Provider<k22> provider2, Provider<v12> provider3, Provider<t32> provider4) {
        this.f2611a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static o12 a(Provider<Context> provider, Provider<k22> provider2, Provider<v12> provider3, Provider<t32> provider4) {
        return new o12(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static h22 c(Context context, k22 k22, v12 v12, t32 t32) {
        h22 a2 = n12.a(context, k22, v12, t32);
        lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    /* renamed from: b */
    public h22 get() {
        return c(this.f2611a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
