package com.fossil;

import android.database.sqlite.SQLiteStatement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ux0 extends tx0 implements px0 {
    @DexIgnore
    public /* final */ SQLiteStatement c;

    @DexIgnore
    public ux0(SQLiteStatement sQLiteStatement) {
        super(sQLiteStatement);
        this.c = sQLiteStatement;
    }

    @DexIgnore
    @Override // com.fossil.px0
    public long executeInsert() {
        return this.c.executeInsert();
    }

    @DexIgnore
    @Override // com.fossil.px0
    public int executeUpdateDelete() {
        return this.c.executeUpdateDelete();
    }
}
