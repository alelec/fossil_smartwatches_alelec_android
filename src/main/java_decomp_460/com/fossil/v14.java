package com.fossil;

import com.fossil.c44;
import com.fossil.d44;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v14<E> extends AbstractCollection<E> implements c44<E> {
    @DexIgnore
    public transient Set<E> b;
    @DexIgnore
    public transient Set<c44.a<E>> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends d44.c<E> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.d44.c
        public c44<E> a() {
            return v14.this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends d44.d<E> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.d44.d
        public c44<E> a() {
            return v14.this;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<c44.a<E>> iterator() {
            return v14.this.entryIterator();
        }

        @DexIgnore
        public int size() {
            return v14.this.distinctElements();
        }
    }

    @DexIgnore
    @Override // com.fossil.c44
    @CanIgnoreReturnValue
    public int add(E e, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, com.fossil.c44, java.util.Collection
    @CanIgnoreReturnValue
    public boolean add(E e) {
        add(e, 1);
        return true;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    public boolean addAll(Collection<? extends E> collection) {
        return d44.a(this, collection);
    }

    @DexIgnore
    public abstract void clear();

    @DexIgnore
    @Override // com.fossil.c44
    public boolean contains(Object obj) {
        return count(obj) > 0;
    }

    @DexIgnore
    @Override // com.fossil.c44
    public abstract int count(Object obj);

    @DexIgnore
    public Set<E> createElementSet() {
        return new a();
    }

    @DexIgnore
    public Set<c44.a<E>> createEntrySet() {
        return new b();
    }

    @DexIgnore
    public abstract int distinctElements();

    @DexIgnore
    @Override // com.fossil.c44
    public Set<E> elementSet() {
        Set<E> set = this.b;
        if (set != null) {
            return set;
        }
        Set<E> createElementSet = createElementSet();
        this.b = createElementSet;
        return createElementSet;
    }

    @DexIgnore
    public abstract Iterator<c44.a<E>> entryIterator();

    @DexIgnore
    @Override // com.fossil.c44
    public Set<c44.a<E>> entrySet() {
        Set<c44.a<E>> set = this.c;
        if (set != null) {
            return set;
        }
        Set<c44.a<E>> createEntrySet = createEntrySet();
        this.c = createEntrySet;
        return createEntrySet;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return d44.c(this, obj);
    }

    @DexIgnore
    public int hashCode() {
        return entrySet().hashCode();
    }

    @DexIgnore
    public boolean isEmpty() {
        return entrySet().isEmpty();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public abstract Iterator<E> iterator();

    @DexIgnore
    @Override // com.fossil.c44
    @CanIgnoreReturnValue
    public abstract int remove(Object obj, int i);

    @DexIgnore
    @CanIgnoreReturnValue
    public boolean remove(Object obj) {
        return remove(obj, 1) > 0;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    public boolean removeAll(Collection<?> collection) {
        return d44.f(this, collection);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    public boolean retainAll(Collection<?> collection) {
        return d44.g(this, collection);
    }

    @DexIgnore
    @Override // com.fossil.c44
    @CanIgnoreReturnValue
    public int setCount(E e, int i) {
        return d44.h(this, e, i);
    }

    @DexIgnore
    @Override // com.fossil.c44
    @CanIgnoreReturnValue
    public boolean setCount(E e, int i, int i2) {
        return d44.i(this, e, i, i2);
    }

    @DexIgnore
    public int size() {
        return d44.j(this);
    }

    @DexIgnore
    public String toString() {
        return entrySet().toString();
    }
}
