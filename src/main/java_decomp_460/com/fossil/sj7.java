package com.fossil;

import android.os.Build;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sj7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f3271a; // = "sj7";
    @DexIgnore
    public static /* final */ List<String> b; // = Arrays.asList("he", "yi", "id");

    @DexIgnore
    public static Locale a(String str, String str2) {
        try {
            if (Build.VERSION.SDK_INT >= 14) {
                Constructor declaredConstructor = Locale.class.getDeclaredConstructor(Boolean.TYPE, String.class, String.class);
                declaredConstructor.setAccessible(true);
                return (Locale) declaredConstructor.newInstance(Boolean.TRUE, str, str2);
            }
            Constructor declaredConstructor2 = Locale.class.getDeclaredConstructor(new Class[0]);
            declaredConstructor2.setAccessible(true);
            Locale locale = (Locale) declaredConstructor2.newInstance(new Object[0]);
            Class<?> cls = locale.getClass();
            Field declaredField = cls.getDeclaredField("languageCode");
            declaredField.setAccessible(true);
            declaredField.set(locale, str);
            Field declaredField2 = cls.getDeclaredField("countryCode");
            declaredField2.setAccessible(true);
            declaredField2.set(locale, str2);
            return locale;
        } catch (Exception e) {
            mj7.e(f3271a, "Unable to create ISO-6390-Alpha3 per reflection", e, new Object[0]);
            return null;
        }
    }

    @DexIgnore
    public static Locale b(String str, String str2) {
        try {
            Method declaredMethod = Locale.class.getDeclaredMethod("createConstant", String.class, String.class);
            declaredMethod.setAccessible(true);
            return (Locale) declaredMethod.invoke(null, str, str2);
        } catch (Exception e) {
            mj7.e(f3271a, "Unable to create ISO-6390-Alpha3 per reflection", e, new Object[0]);
            return null;
        }
    }

    @DexIgnore
    public static Locale c(String str) {
        int i = 2;
        mj7.c(f3271a, "Assuming Locale.getDefault()", new Object[0]);
        Locale locale = Locale.getDefault();
        if (!tj7.b(str)) {
            return locale;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(str, ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
        int countTokens = stringTokenizer.countTokens();
        if (countTokens == 1 || countTokens == 2) {
            if (countTokens != 1) {
                i = 5;
            }
            if (i != str.length()) {
                mj7.c(f3271a, "number of tokens is correct but the length of the locale string does not match the expected length", new Object[0]);
                return locale;
            }
            String nextToken = stringTokenizer.nextToken();
            String upperCase = (stringTokenizer.hasMoreTokens() ? stringTokenizer.nextToken() : "").toUpperCase(Locale.US);
            if (!b.contains(nextToken)) {
                return new Locale(nextToken, upperCase);
            }
            mj7.c(f3271a, "New ISO-6390-Alpha3 locale detected trying to create new locale per reflection", new Object[0]);
            Locale b2 = b(nextToken, upperCase);
            if (b2 == null) {
                b2 = a(nextToken, upperCase);
            }
            return b2 == null ? new Locale(nextToken, upperCase) : b2;
        }
        mj7.l(f3271a, "Unexpected number of tokens, must be at least one and at most two", new Object[0]);
        return locale;
    }

    @DexIgnore
    public static String d(Locale locale) {
        if (locale == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(locale.getLanguage());
        if (tj7.b(locale.getCountry())) {
            sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
            sb.append(locale.getCountry().toLowerCase(Locale.US));
        }
        return sb.toString();
    }
}
