package com.fossil;

import android.content.ComponentCallbacks2;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.View;
import androidx.fragment.app.Fragment;
import com.bumptech.glide.GeneratedAppGlideModule;
import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.dc1;
import com.fossil.fc1;
import com.fossil.ff1;
import com.fossil.hf1;
import com.fossil.if1;
import com.fossil.jf1;
import com.fossil.kf1;
import com.fossil.lf1;
import com.fossil.mf1;
import com.fossil.ne1;
import com.fossil.nf1;
import com.fossil.oe1;
import com.fossil.of1;
import com.fossil.pf1;
import com.fossil.qe1;
import com.fossil.qf1;
import com.fossil.re1;
import com.fossil.se1;
import com.fossil.wg1;
import com.fossil.xe1;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oa1 implements ComponentCallbacks2 {
    @DexIgnore
    public static volatile oa1 j;
    @DexIgnore
    public static volatile boolean k;
    @DexIgnore
    public /* final */ rd1 b;
    @DexIgnore
    public /* final */ ie1 c;
    @DexIgnore
    public /* final */ qa1 d;
    @DexIgnore
    public /* final */ ua1 e;
    @DexIgnore
    public /* final */ od1 f;
    @DexIgnore
    public /* final */ hi1 g;
    @DexIgnore
    public /* final */ zh1 h;
    @DexIgnore
    public /* final */ List<wa1> i; // = new ArrayList();

    @DexIgnore
    public interface a {
        @DexIgnore
        fj1 build();
    }

    @DexIgnore
    public oa1(Context context, xc1 xc1, ie1 ie1, rd1 rd1, od1 od1, hi1 hi1, zh1 zh1, int i2, a aVar, Map<Class<?>, xa1<?, ?>> map, List<ej1<Object>> list, boolean z, boolean z2) {
        qb1 ag1;
        qb1 sg1;
        ra1 ra1 = ra1.NORMAL;
        this.b = rd1;
        this.f = od1;
        this.c = ie1;
        this.g = hi1;
        this.h = zh1;
        Resources resources = context.getResources();
        ua1 ua1 = new ua1();
        this.e = ua1;
        ua1.o(new eg1());
        if (Build.VERSION.SDK_INT >= 27) {
            this.e.o(new jg1());
        }
        List<ImageHeaderParser> g2 = this.e.g();
        fh1 fh1 = new fh1(context, g2, rd1, od1);
        qb1<ParcelFileDescriptor, Bitmap> h2 = vg1.h(rd1);
        gg1 gg1 = new gg1(this.e.g(), resources.getDisplayMetrics(), rd1, od1);
        if (!z2 || Build.VERSION.SDK_INT < 28) {
            ag1 = new ag1(gg1);
            sg1 = new sg1(gg1, od1);
        } else {
            sg1 = new ng1();
            ag1 = new bg1();
        }
        bh1 bh1 = new bh1(context);
        ff1.c cVar = new ff1.c(resources);
        ff1.d dVar = new ff1.d(resources);
        ff1.b bVar = new ff1.b(resources);
        ff1.a aVar2 = new ff1.a(resources);
        wf1 wf1 = new wf1(od1);
        ph1 ph1 = new ph1();
        sh1 sh1 = new sh1();
        ContentResolver contentResolver = context.getContentResolver();
        ua1 ua12 = this.e;
        ua12.a(ByteBuffer.class, new pe1());
        ua12.a(InputStream.class, new gf1(od1));
        ua12.e("Bitmap", ByteBuffer.class, Bitmap.class, ag1);
        ua12.e("Bitmap", InputStream.class, Bitmap.class, sg1);
        if (fc1.c()) {
            this.e.e("Bitmap", ParcelFileDescriptor.class, Bitmap.class, new pg1(gg1));
        }
        ua1 ua13 = this.e;
        ua13.e("Bitmap", ParcelFileDescriptor.class, Bitmap.class, h2);
        ua13.e("Bitmap", AssetFileDescriptor.class, Bitmap.class, vg1.c(rd1));
        ua13.d(Bitmap.class, Bitmap.class, if1.a.a());
        ua13.e("Bitmap", Bitmap.class, Bitmap.class, new ug1());
        ua13.b(Bitmap.class, wf1);
        ua13.e("BitmapDrawable", ByteBuffer.class, BitmapDrawable.class, new uf1(resources, ag1));
        ua13.e("BitmapDrawable", InputStream.class, BitmapDrawable.class, new uf1(resources, sg1));
        ua13.e("BitmapDrawable", ParcelFileDescriptor.class, BitmapDrawable.class, new uf1(resources, h2));
        ua13.b(BitmapDrawable.class, new vf1(rd1, wf1));
        ua13.e("Gif", InputStream.class, hh1.class, new oh1(g2, fh1, od1));
        ua13.e("Gif", ByteBuffer.class, hh1.class, fh1);
        ua13.b(hh1.class, new ih1());
        ua13.d(bb1.class, bb1.class, if1.a.a());
        ua13.e("Bitmap", bb1.class, Bitmap.class, new mh1(rd1));
        ua13.c(Uri.class, Drawable.class, bh1);
        ua13.c(Uri.class, Bitmap.class, new rg1(bh1, rd1));
        ua13.p(new wg1.a());
        ua13.d(File.class, ByteBuffer.class, new qe1.b());
        ua13.d(File.class, InputStream.class, new se1.e());
        ua13.c(File.class, File.class, new dh1());
        ua13.d(File.class, ParcelFileDescriptor.class, new se1.b());
        ua13.d(File.class, File.class, if1.a.a());
        ua13.p(new dc1.a(od1));
        if (fc1.c()) {
            this.e.p(new fc1.a());
        }
        ua1 ua14 = this.e;
        ua14.d(Integer.TYPE, InputStream.class, cVar);
        ua14.d(Integer.TYPE, ParcelFileDescriptor.class, bVar);
        ua14.d(Integer.class, InputStream.class, cVar);
        ua14.d(Integer.class, ParcelFileDescriptor.class, bVar);
        ua14.d(Integer.class, Uri.class, dVar);
        ua14.d(Integer.TYPE, AssetFileDescriptor.class, aVar2);
        ua14.d(Integer.class, AssetFileDescriptor.class, aVar2);
        ua14.d(Integer.TYPE, Uri.class, dVar);
        ua14.d(String.class, InputStream.class, new re1.c());
        ua14.d(Uri.class, InputStream.class, new re1.c());
        ua14.d(String.class, InputStream.class, new hf1.c());
        ua14.d(String.class, ParcelFileDescriptor.class, new hf1.b());
        ua14.d(String.class, AssetFileDescriptor.class, new hf1.a());
        ua14.d(Uri.class, InputStream.class, new mf1.a());
        ua14.d(Uri.class, InputStream.class, new ne1.c(context.getAssets()));
        ua14.d(Uri.class, ParcelFileDescriptor.class, new ne1.b(context.getAssets()));
        ua14.d(Uri.class, InputStream.class, new nf1.a(context));
        ua14.d(Uri.class, InputStream.class, new of1.a(context));
        if (Build.VERSION.SDK_INT >= 29) {
            this.e.d(Uri.class, InputStream.class, new pf1.c(context));
            this.e.d(Uri.class, ParcelFileDescriptor.class, new pf1.b(context));
        }
        ua1 ua15 = this.e;
        ua15.d(Uri.class, InputStream.class, new jf1.d(contentResolver));
        ua15.d(Uri.class, ParcelFileDescriptor.class, new jf1.b(contentResolver));
        ua15.d(Uri.class, AssetFileDescriptor.class, new jf1.a(contentResolver));
        ua15.d(Uri.class, InputStream.class, new kf1.a());
        ua15.d(URL.class, InputStream.class, new qf1.a());
        ua15.d(Uri.class, File.class, new xe1.a(context));
        ua15.d(te1.class, InputStream.class, new lf1.a());
        ua15.d(byte[].class, ByteBuffer.class, new oe1.a());
        ua15.d(byte[].class, InputStream.class, new oe1.d());
        ua15.d(Uri.class, Uri.class, if1.a.a());
        ua15.d(Drawable.class, Drawable.class, if1.a.a());
        ua15.c(Drawable.class, Drawable.class, new ch1());
        ua15.q(Bitmap.class, BitmapDrawable.class, new qh1(resources));
        ua15.q(Bitmap.class, byte[].class, ph1);
        ua15.q(Drawable.class, byte[].class, new rh1(rd1, ph1, sh1));
        ua15.q(hh1.class, byte[].class, sh1);
        if (Build.VERSION.SDK_INT >= 23) {
            qb1<ByteBuffer, Bitmap> d2 = vg1.d(rd1);
            this.e.c(ByteBuffer.class, Bitmap.class, d2);
            this.e.c(ByteBuffer.class, BitmapDrawable.class, new uf1(resources, d2));
        }
        this.d = new qa1(context, od1, this.e, new oj1(), aVar, map, list, xc1, z, i2);
    }

    @DexIgnore
    public static void a(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        if (!k) {
            k = true;
            m(context, generatedAppGlideModule);
            k = false;
            return;
        }
        throw new IllegalStateException("You cannot call Glide.get() in registerComponents(), use the provided Glide instance instead");
    }

    @DexIgnore
    public static oa1 c(Context context) {
        if (j == null) {
            GeneratedAppGlideModule d2 = d(context.getApplicationContext());
            synchronized (oa1.class) {
                try {
                    if (j == null) {
                        a(context, d2);
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return j;
    }

    @DexIgnore
    public static GeneratedAppGlideModule d(Context context) {
        try {
            return (GeneratedAppGlideModule) Class.forName("com.bumptech.glide.GeneratedAppGlideModuleImpl").getDeclaredConstructor(Context.class).newInstance(context.getApplicationContext());
        } catch (ClassNotFoundException e2) {
            if (!Log.isLoggable("Glide", 5)) {
                return null;
            }
            Log.w("Glide", "Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored");
            return null;
        } catch (InstantiationException e3) {
            q(e3);
            throw null;
        } catch (IllegalAccessException e4) {
            q(e4);
            throw null;
        } catch (NoSuchMethodException e5) {
            q(e5);
            throw null;
        } catch (InvocationTargetException e6) {
            q(e6);
            throw null;
        }
    }

    @DexIgnore
    public static hi1 l(Context context) {
        ik1.e(context, "You cannot start a load on a not yet attached View or a Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).");
        return c(context).k();
    }

    @DexIgnore
    public static void m(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        n(context, new pa1(), generatedAppGlideModule);
    }

    @DexIgnore
    public static void n(Context context, pa1 pa1, GeneratedAppGlideModule generatedAppGlideModule) {
        Context applicationContext = context.getApplicationContext();
        List<oi1> a2 = (generatedAppGlideModule == null || generatedAppGlideModule.c()) ? new qi1(applicationContext).a() : Collections.emptyList();
        if (generatedAppGlideModule != null && !generatedAppGlideModule.d().isEmpty()) {
            Set<Class<?>> d2 = generatedAppGlideModule.d();
            Iterator<oi1> it = a2.iterator();
            while (it.hasNext()) {
                oi1 next = it.next();
                if (d2.contains(next.getClass())) {
                    if (Log.isLoggable("Glide", 3)) {
                        Log.d("Glide", "AppGlideModule excludes manifest GlideModule: " + next);
                    }
                    it.remove();
                }
            }
        }
        if (Log.isLoggable("Glide", 3)) {
            Iterator<oi1> it2 = a2.iterator();
            while (it2.hasNext()) {
                Log.d("Glide", "Discovered GlideModule from manifest: " + it2.next().getClass());
            }
        }
        pa1.b(generatedAppGlideModule != null ? generatedAppGlideModule.e() : null);
        for (oi1 oi1 : a2) {
            oi1.a(applicationContext, pa1);
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.a(applicationContext, pa1);
        }
        oa1 a3 = pa1.a(applicationContext);
        for (oi1 oi12 : a2) {
            try {
                oi12.b(applicationContext, a3, a3.e);
            } catch (AbstractMethodError e2) {
                throw new IllegalStateException("Attempting to register a Glide v3 module. If you see this, you or one of your dependencies may be including Glide v3 even though you're using Glide v4. You'll need to find and remove (or update) the offending dependency. The v3 module name is: " + oi12.getClass().getName(), e2);
            }
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.b(applicationContext, a3, a3.e);
        }
        applicationContext.registerComponentCallbacks(a3);
        j = a3;
    }

    @DexIgnore
    public static void q(Exception exc) {
        throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", exc);
    }

    @DexIgnore
    public static wa1 t(Context context) {
        return l(context).k(context);
    }

    @DexIgnore
    public static wa1 u(View view) {
        return l(view.getContext()).l(view);
    }

    @DexIgnore
    public static wa1 v(Fragment fragment) {
        return l(fragment.getContext()).m(fragment);
    }

    @DexIgnore
    public void b() {
        jk1.b();
        this.c.d();
        this.b.d();
        this.f.d();
    }

    @DexIgnore
    public od1 e() {
        return this.f;
    }

    @DexIgnore
    public rd1 f() {
        return this.b;
    }

    @DexIgnore
    public zh1 g() {
        return this.h;
    }

    @DexIgnore
    public Context h() {
        return this.d.getBaseContext();
    }

    @DexIgnore
    public qa1 i() {
        return this.d;
    }

    @DexIgnore
    public ua1 j() {
        return this.e;
    }

    @DexIgnore
    public hi1 k() {
        return this.g;
    }

    @DexIgnore
    public void o(wa1 wa1) {
        synchronized (this.i) {
            if (!this.i.contains(wa1)) {
                this.i.add(wa1);
            } else {
                throw new IllegalStateException("Cannot register already registered manager");
            }
        }
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    public void onLowMemory() {
        b();
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        r(i2);
    }

    @DexIgnore
    public boolean p(qj1<?> qj1) {
        synchronized (this.i) {
            for (wa1 wa1 : this.i) {
                if (wa1.A(qj1)) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    public void r(int i2) {
        jk1.b();
        for (wa1 wa1 : this.i) {
            wa1.onTrimMemory(i2);
        }
        this.c.a(i2);
        this.b.a(i2);
        this.f.a(i2);
    }

    @DexIgnore
    public void s(wa1 wa1) {
        synchronized (this.i) {
            if (this.i.contains(wa1)) {
                this.i.remove(wa1);
            } else {
                throw new IllegalStateException("Cannot unregister not yet registered manager");
            }
        }
    }
}
