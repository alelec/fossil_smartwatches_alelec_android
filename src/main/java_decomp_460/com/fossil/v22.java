package com.fossil;

import android.database.Cursor;
import com.fossil.j32;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class v22 implements j32.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ j32 f3699a;
    @DexIgnore
    public /* final */ List b;
    @DexIgnore
    public /* final */ h02 c;

    @DexIgnore
    public v22(j32 j32, List list, h02 h02) {
        this.f3699a = j32;
        this.b = list;
        this.c = h02;
    }

    @DexIgnore
    public static j32.b a(j32 j32, List list, h02 h02) {
        return new v22(j32, list, h02);
    }

    @DexIgnore
    @Override // com.fossil.j32.b
    public Object apply(Object obj) {
        return j32.S(this.f3699a, this.b, this.c, (Cursor) obj);
    }
}
