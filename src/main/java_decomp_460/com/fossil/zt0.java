package com.fossil;

import android.annotation.SuppressLint;
import androidx.lifecycle.LiveData;
import com.fossil.cu0;
import com.fossil.xt0;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zt0<Key, Value> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Key f4526a;
    @DexIgnore
    public cu0.f b;
    @DexIgnore
    public xt0.b<Key, Value> c;
    @DexIgnore
    public cu0.c d;
    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public Executor e; // = bi0.e();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends mr0<cu0<Value>> {
        @DexIgnore
        public cu0<Value> g;
        @DexIgnore
        public xt0<Key, Value> h;
        @DexIgnore
        public /* final */ xt0.c i; // = new C0309a();
        @DexIgnore
        public /* final */ /* synthetic */ Object j;
        @DexIgnore
        public /* final */ /* synthetic */ xt0.b k;
        @DexIgnore
        public /* final */ /* synthetic */ cu0.f l;
        @DexIgnore
        public /* final */ /* synthetic */ Executor m;
        @DexIgnore
        public /* final */ /* synthetic */ Executor n;
        @DexIgnore
        public /* final */ /* synthetic */ cu0.c o;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.zt0$a$a")
        /* renamed from: com.fossil.zt0$a$a  reason: collision with other inner class name */
        public class C0309a implements xt0.c {
            @DexIgnore
            public C0309a() {
            }

            @DexIgnore
            @Override // com.fossil.xt0.c
            public void a() {
                a.this.c();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Executor executor, Object obj, xt0.b bVar, cu0.f fVar, Executor executor2, Executor executor3, cu0.c cVar) {
            super(executor);
            this.j = obj;
            this.k = bVar;
            this.l = fVar;
            this.m = executor2;
            this.n = executor3;
            this.o = cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v4, resolved type: com.fossil.cu0$d */
        /* JADX WARN: Multi-variable type inference failed */
        /* renamed from: d */
        public cu0<Value> a() {
            cu0<Value> a2;
            Object obj = this.j;
            cu0<Value> cu0 = this.g;
            if (cu0 != null) {
                obj = cu0.q();
            }
            do {
                xt0<Key, Value> xt0 = this.h;
                if (xt0 != null) {
                    xt0.removeInvalidatedCallback(this.i);
                }
                xt0<Key, Value> create = this.k.create();
                this.h = create;
                create.addInvalidatedCallback(this.i);
                cu0.d dVar = new cu0.d(this.h, this.l);
                dVar.e(this.m);
                dVar.c(this.n);
                dVar.b(this.o);
                dVar.d(obj);
                a2 = dVar.a();
                this.g = a2;
            } while (a2.t());
            return this.g;
        }
    }

    @DexIgnore
    public zt0(xt0.b<Key, Value> bVar, cu0.f fVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("PagedList.Config must be provided");
        } else if (bVar != null) {
            this.c = bVar;
            this.b = fVar;
        } else {
            throw new IllegalArgumentException("DataSource.Factory must be provided");
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public static <Key, Value> LiveData<cu0<Value>> b(Key key, cu0.f fVar, cu0.c cVar, xt0.b<Key, Value> bVar, Executor executor, Executor executor2) {
        return new a(executor2, key, bVar, fVar, executor, executor2, cVar).b();
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public LiveData<cu0<Value>> a() {
        return b(this.f4526a, this.b, this.d, this.c, bi0.g(), this.e);
    }
}
