package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ap2 implements Parcelable.Creator<yo2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ yo2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        ArrayList arrayList = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            if (ad2.l(t) != 1) {
                ad2.B(parcel, t);
            } else {
                arrayList = ad2.j(parcel, t, DataType.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new yo2(arrayList);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ yo2[] newArray(int i) {
        return new yo2[i];
    }
}
