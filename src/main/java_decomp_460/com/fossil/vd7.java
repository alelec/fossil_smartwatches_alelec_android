package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vd7 extends vc7<Target> {
    @DexIgnore
    public vd7(Picasso picasso, Target target, pd7 pd7, int i, int i2, Drawable drawable, String str, Object obj, int i3) {
        super(picasso, target, pd7, i, i2, i3, drawable, str, obj, false);
    }

    @DexIgnore
    @Override // com.fossil.vc7
    public void b(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
        if (bitmap != null) {
            Target target = (Target) k();
            if (target != null) {
                target.onBitmapLoaded(bitmap, loadedFrom);
                if (bitmap.isRecycled()) {
                    throw new IllegalStateException("Target callback must not recycle bitmap!");
                }
                return;
            }
            return;
        }
        throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", this));
    }

    @DexIgnore
    @Override // com.fossil.vc7
    public void c() {
        Target target = (Target) k();
        if (target == null) {
            return;
        }
        if (this.g != 0) {
            target.onBitmapFailed(this.f3749a.e.getResources().getDrawable(this.g));
        } else {
            target.onBitmapFailed(this.h);
        }
    }
}
