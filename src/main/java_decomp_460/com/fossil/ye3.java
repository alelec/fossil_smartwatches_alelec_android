package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ye3 implements Parcelable.Creator<ce3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ce3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        Float f = null;
        IBinder iBinder = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 2) {
                i = ad2.v(parcel, t);
            } else if (l == 3) {
                iBinder = ad2.u(parcel, t);
            } else if (l != 4) {
                ad2.B(parcel, t);
            } else {
                f = ad2.s(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new ce3(i, iBinder, f);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ce3[] newArray(int i) {
        return new ce3[i];
    }
}
