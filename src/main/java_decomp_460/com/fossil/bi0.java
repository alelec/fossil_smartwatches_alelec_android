package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bi0 extends di0 {
    @DexIgnore
    public static volatile bi0 c;
    @DexIgnore
    public static /* final */ Executor d; // = new a();
    @DexIgnore
    public static /* final */ Executor e; // = new b();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public di0 f431a;
    @DexIgnore
    public di0 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Executor {
        @DexIgnore
        public void execute(Runnable runnable) {
            bi0.f().d(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Executor {
        @DexIgnore
        public void execute(Runnable runnable) {
            bi0.f().a(runnable);
        }
    }

    @DexIgnore
    public bi0() {
        ci0 ci0 = new ci0();
        this.b = ci0;
        this.f431a = ci0;
    }

    @DexIgnore
    public static Executor e() {
        return e;
    }

    @DexIgnore
    public static bi0 f() {
        if (c != null) {
            return c;
        }
        synchronized (bi0.class) {
            try {
                if (c == null) {
                    c = new bi0();
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return c;
    }

    @DexIgnore
    public static Executor g() {
        return d;
    }

    @DexIgnore
    @Override // com.fossil.di0
    public void a(Runnable runnable) {
        this.f431a.a(runnable);
    }

    @DexIgnore
    @Override // com.fossil.di0
    public boolean c() {
        return this.f431a.c();
    }

    @DexIgnore
    @Override // com.fossil.di0
    public void d(Runnable runnable) {
        this.f431a.d(runnable);
    }
}
