package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zr4 implements Factory<yr4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ck5> f4518a;
    @DexIgnore
    public /* final */ Provider<tt4> b;

    @DexIgnore
    public zr4(Provider<ck5> provider, Provider<tt4> provider2) {
        this.f4518a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static zr4 a(Provider<ck5> provider, Provider<tt4> provider2) {
        return new zr4(provider, provider2);
    }

    @DexIgnore
    public static yr4 c(ck5 ck5, tt4 tt4) {
        return new yr4(ck5, tt4);
    }

    @DexIgnore
    /* renamed from: b */
    public yr4 get() {
        return c(this.f4518a.get(), this.b.get());
    }
}
