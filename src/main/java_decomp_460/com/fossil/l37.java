package com.fossil;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.text.TextUtils;
import com.fossil.lu5;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.j256.ormlite.field.FieldType;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l37 extends ContentObserver implements lu5.a {
    @DexIgnore
    public static /* final */ String l; // = l37.class.getSimpleName();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ HashMap<String, Integer> f2143a; // = new HashMap<>();
    @DexIgnore
    public /* final */ NotificationsRepository b;
    @DexIgnore
    public /* final */ ContentResolver c;
    @DexIgnore
    public /* final */ no4 d;
    @DexIgnore
    public /* final */ PortfolioApp e;
    @DexIgnore
    public /* final */ lu5 f;
    @DexIgnore
    public /* final */ on5 g;
    @DexIgnore
    public DeviceRepository h;
    @DexIgnore
    public NotificationSettingsDatabase i;
    @DexIgnore
    public long j;
    @DexIgnore
    public long k;

    @DexIgnore
    public l37(PortfolioApp portfolioApp, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, NotificationSettingsDatabase notificationSettingsDatabase, no4 no4, ContentResolver contentResolver, lu5 lu5, on5 on5) {
        super(null);
        new HashMap();
        this.j = System.currentTimeMillis();
        this.k = -1;
        this.e = portfolioApp;
        this.f = lu5;
        i14.m(no4, "appExecutors cannot be NULL");
        this.d = no4;
        i14.m(notificationsRepository, "repository cannot be nulL!");
        this.b = notificationsRepository;
        i14.m(deviceRepository, "deviceRepository cannot be nulL!");
        this.h = deviceRepository;
        i14.m(notificationSettingsDatabase, "notificationSettingsDatabase cannot be null!");
        this.i = notificationSettingsDatabase;
        i14.m(contentResolver, "contentResolver cannot be null!");
        this.c = contentResolver;
        this.g = on5;
    }

    @DexIgnore
    @Override // com.fossil.lu5.a
    public void a() {
        l();
    }

    @DexIgnore
    public void b() {
        this.f.c(this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002a, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002c, code lost:
        r6 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002d, code lost:
        if (r2 == null) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0033, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0034, code lost:
        r6 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0015, code lost:
        if (r2 != null) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0017, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001c, code lost:
        if (r2.moveToNext() == false) goto L_0x002c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0028, code lost:
        if (r2.getInt(r2.getColumnIndex("starred")) != 1) goto L_0x0017;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0045  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean c(java.lang.String r10) {
        /*
            r9 = this;
            r7 = 1
            r8 = 0
            r6 = 0
            android.content.ContentResolver r0 = r9.c     // Catch:{ Exception -> 0x003e, all -> 0x0042 }
            android.net.Uri r1 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x003e, all -> 0x0042 }
            r2 = 0
            java.lang.String r3 = "contact_id = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x003e, all -> 0x0042 }
            r5 = 0
            r4[r5] = r10     // Catch:{ Exception -> 0x003e, all -> 0x0042 }
            r5 = 0
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x003e, all -> 0x0042 }
            if (r2 == 0) goto L_0x002d
        L_0x0017:
            r0 = r6
        L_0x0018:
            boolean r1 = r2.moveToNext()     // Catch:{ Exception -> 0x0033 }
            if (r1 == 0) goto L_0x002c
            java.lang.String r1 = "starred"
            int r1 = r2.getColumnIndex(r1)     // Catch:{ Exception -> 0x0033 }
            int r0 = r2.getInt(r1)     // Catch:{ Exception -> 0x0033 }
            if (r0 != r7) goto L_0x0017
            r0 = r7
            goto L_0x0018
        L_0x002c:
            r6 = r0
        L_0x002d:
            if (r2 == 0) goto L_0x0032
            r2.close()
        L_0x0032:
            return r6
        L_0x0033:
            r1 = move-exception
            r6 = r0
        L_0x0035:
            r1.printStackTrace()     // Catch:{ all -> 0x0049 }
            if (r2 == 0) goto L_0x0032
            r2.close()
            goto L_0x0032
        L_0x003e:
            r0 = move-exception
            r1 = r0
            r2 = r8
            goto L_0x0035
        L_0x0042:
            r0 = move-exception
        L_0x0043:
            if (r8 == 0) goto L_0x0048
            r8.close()
        L_0x0048:
            throw r0
        L_0x0049:
            r0 = move-exception
            r8 = r2
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.l37.c(java.lang.String):boolean");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String d(java.lang.String r9) {
        /*
            r8 = this;
            r7 = 0
            java.lang.String r6 = ""
            android.content.ContentResolver r0 = r8.c     // Catch:{ Exception -> 0x0038, all -> 0x0043 }
            android.net.Uri r1 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x0038, all -> 0x0043 }
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0038, all -> 0x0043 }
            r3 = 0
            java.lang.String r4 = "photo_thumb_uri"
            r2[r3] = r4     // Catch:{ Exception -> 0x0038, all -> 0x0043 }
            java.lang.String r3 = "contact_id = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0038, all -> 0x0043 }
            r5 = 0
            r4[r5] = r9     // Catch:{ Exception -> 0x0038, all -> 0x0043 }
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0038, all -> 0x0043 }
            if (r1 == 0) goto L_0x0050
            r0 = r6
        L_0x001f:
            boolean r2 = r1.moveToNext()     // Catch:{ Exception -> 0x004b }
            if (r2 == 0) goto L_0x0030
            java.lang.String r2 = "photo_thumb_uri"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x004b }
            java.lang.String r0 = r1.getString(r2)     // Catch:{ Exception -> 0x004b }
            goto L_0x001f
        L_0x0030:
            r2 = r0
        L_0x0031:
            if (r1 == 0) goto L_0x0037
            r0 = r1
        L_0x0034:
            r0.close()
        L_0x0037:
            return r2
        L_0x0038:
            r0 = move-exception
            r3 = r0
            r2 = r6
            r1 = r7
        L_0x003c:
            r3.printStackTrace()     // Catch:{ all -> 0x004e }
            if (r1 == 0) goto L_0x0037
            r0 = r1
            goto L_0x0034
        L_0x0043:
            r0 = move-exception
            r1 = r7
        L_0x0045:
            if (r1 == 0) goto L_0x004a
            r1.close()
        L_0x004a:
            throw r0
        L_0x004b:
            r3 = move-exception
            r2 = r0
            goto L_0x003c
        L_0x004e:
            r0 = move-exception
            goto L_0x0045
        L_0x0050:
            r2 = r6
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.l37.d(java.lang.String):java.lang.String");
    }

    @DexIgnore
    public boolean deliverSelfNotifications() {
        return true;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String e(java.lang.String r9) {
        /*
            r8 = this;
            r7 = 0
            java.lang.String r6 = ""
            android.content.ContentResolver r0 = r8.c     // Catch:{ Exception -> 0x0031, all -> 0x003c }
            android.net.Uri r1 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x0031, all -> 0x003c }
            r2 = 0
            java.lang.String r3 = "contact_id = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0031, all -> 0x003c }
            r5 = 0
            r4[r5] = r9     // Catch:{ Exception -> 0x0031, all -> 0x003c }
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0031, all -> 0x003c }
            if (r1 == 0) goto L_0x0049
            r0 = r6
        L_0x0018:
            boolean r2 = r1.moveToNext()     // Catch:{ Exception -> 0x0044 }
            if (r2 == 0) goto L_0x0029
            java.lang.String r2 = "display_name"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r0 = r1.getString(r2)     // Catch:{ Exception -> 0x0044 }
            goto L_0x0018
        L_0x0029:
            r2 = r0
        L_0x002a:
            if (r1 == 0) goto L_0x0030
            r0 = r1
        L_0x002d:
            r0.close()
        L_0x0030:
            return r2
        L_0x0031:
            r0 = move-exception
            r3 = r0
            r2 = r6
            r1 = r7
        L_0x0035:
            r3.printStackTrace()     // Catch:{ all -> 0x0047 }
            if (r1 == 0) goto L_0x0030
            r0 = r1
            goto L_0x002d
        L_0x003c:
            r0 = move-exception
            r1 = r7
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.close()
        L_0x0043:
            throw r0
        L_0x0044:
            r3 = move-exception
            r2 = r0
            goto L_0x0035
        L_0x0047:
            r0 = move-exception
            goto L_0x003e
        L_0x0049:
            r2 = r6
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.l37.e(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0049, code lost:
        if (r0 != null) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004b, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004e, code lost:
        return r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004f, code lost:
        if (r0 != null) goto L_0x004b;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0057  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> f(java.lang.String r9) {
        /*
            r8 = this;
            r6 = 0
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            android.content.ContentResolver r0 = r8.c     // Catch:{ Exception -> 0x005b, all -> 0x0052 }
            android.net.Uri r1 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x005b, all -> 0x0052 }
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x005b, all -> 0x0052 }
            r3 = 0
            java.lang.String r4 = "data1"
            r2[r3] = r4     // Catch:{ Exception -> 0x005b, all -> 0x0052 }
            java.lang.String r3 = "contact_id = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x005b, all -> 0x0052 }
            r5 = 0
            r4[r5] = r9     // Catch:{ Exception -> 0x005b, all -> 0x0052 }
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x005b, all -> 0x0052 }
            if (r0 == 0) goto L_0x004f
        L_0x0021:
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0045 }
            if (r1 == 0) goto L_0x004f
            java.lang.String r1 = "data1"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x0045 }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0045 }
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0045 }
            if (r2 != 0) goto L_0x0021
            java.lang.Boolean r2 = com.fossil.p47.j(r7, r1)     // Catch:{ Exception -> 0x0045 }
            boolean r2 = r2.booleanValue()     // Catch:{ Exception -> 0x0045 }
            if (r2 != 0) goto L_0x0021
            r7.add(r1)     // Catch:{ Exception -> 0x0045 }
            goto L_0x0021
        L_0x0045:
            r1 = move-exception
        L_0x0046:
            r1.printStackTrace()     // Catch:{ all -> 0x005e }
            if (r0 == 0) goto L_0x004e
        L_0x004b:
            r0.close()
        L_0x004e:
            return r7
        L_0x004f:
            if (r0 == 0) goto L_0x004e
            goto L_0x004b
        L_0x0052:
            r0 = move-exception
            r1 = r0
            r2 = r6
        L_0x0055:
            if (r2 == 0) goto L_0x005a
            r2.close()
        L_0x005a:
            throw r1
        L_0x005b:
            r1 = move-exception
            r0 = r6
            goto L_0x0046
        L_0x005e:
            r1 = move-exception
            r2 = r0
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.l37.f(java.lang.String):java.util.List");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x006b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean g(java.lang.String r9) {
        /*
            r8 = this;
            r7 = 0
            r6 = 0
            android.content.ContentResolver r0 = r8.c     // Catch:{ Exception -> 0x0043, all -> 0x0067 }
            android.net.Uri r1 = android.provider.ContactsContract.RawContacts.CONTENT_URI     // Catch:{ Exception -> 0x0043, all -> 0x0067 }
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0043, all -> 0x0067 }
            r3 = 0
            java.lang.String r4 = "contact_id"
            r2[r3] = r4     // Catch:{ Exception -> 0x0043, all -> 0x0067 }
            r3 = 1
            java.lang.String r4 = "version"
            r2[r3] = r4     // Catch:{ Exception -> 0x0043, all -> 0x0067 }
            java.lang.String r3 = "contact_id = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0043, all -> 0x0067 }
            r5 = 0
            r4[r5] = r9     // Catch:{ Exception -> 0x0043, all -> 0x0067 }
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0043, all -> 0x0067 }
            if (r1 == 0) goto L_0x0074
            r0 = r6
        L_0x0023:
            boolean r2 = r1.moveToNext()     // Catch:{ Exception -> 0x006f }
            if (r2 == 0) goto L_0x0035
            java.lang.String r2 = "version"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x006f }
            int r2 = r1.getInt(r2)     // Catch:{ Exception -> 0x006f }
            int r0 = r0 + r2
            goto L_0x0023
        L_0x0035:
            r2 = r0
        L_0x0036:
            if (r1 == 0) goto L_0x003c
            r0 = r1
        L_0x0039:
            r0.close()
        L_0x003c:
            boolean r0 = r8.h(r9, r2)
            r0 = r0 ^ 1
            return r0
        L_0x0043:
            r0 = move-exception
            r3 = r0
            r1 = r7
            r2 = r6
        L_0x0047:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0072 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ all -> 0x0072 }
            java.lang.String r4 = com.fossil.l37.l     // Catch:{ all -> 0x0072 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0072 }
            r5.<init>()     // Catch:{ all -> 0x0072 }
            java.lang.String r6 = "isHasNewVersion e="
            r5.append(r6)     // Catch:{ all -> 0x0072 }
            r5.append(r3)     // Catch:{ all -> 0x0072 }
            java.lang.String r3 = r5.toString()     // Catch:{ all -> 0x0072 }
            r0.d(r4, r3)     // Catch:{ all -> 0x0072 }
            if (r1 == 0) goto L_0x003c
            r0 = r1
            goto L_0x0039
        L_0x0067:
            r0 = move-exception
            r1 = r7
        L_0x0069:
            if (r1 == 0) goto L_0x006e
            r1.close()
        L_0x006e:
            throw r0
        L_0x006f:
            r3 = move-exception
            r2 = r0
            goto L_0x0047
        L_0x0072:
            r0 = move-exception
            goto L_0x0069
        L_0x0074:
            r2 = r6
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.l37.g(java.lang.String):boolean");
    }

    @DexIgnore
    public final boolean h(String str, int i2) {
        synchronized (this.f2143a) {
            Integer num = this.f2143a.get(str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = l;
            local.d(str2, " Enter isVersionLasted contactId= " + str + " savedVersion=" + num + " newVersion:" + i2);
            if (num == null) {
                FLogger.INSTANCE.getLocal().d(l, "Contact not found, let's update");
                this.f2143a.put(str, Integer.valueOf(i2));
                return false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = l;
            local2.d(str3, "Found contact " + str);
            if (i2 > num.intValue()) {
                FLogger.INSTANCE.getLocal().d(l, "Saved version of this contact is old, let's update");
                this.f2143a.put(str, Integer.valueOf(i2));
                return false;
            }
            FLogger.INSTANCE.getLocal().d(l, "Saved version of this contact is lasted, skip!");
            k(str);
            return true;
        }
    }

    @DexIgnore
    public void j() {
        this.f.c(null);
    }

    @DexIgnore
    public final void k(String str) {
        synchronized (this.f2143a) {
            int size = this.f2143a.size();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = l;
            local.d(str2, "removeOtherContactVersionHashMap. Size = " + size);
            LinkedList<String> linkedList = new LinkedList();
            for (String str3 : this.f2143a.keySet()) {
                if (!str3.equals(str)) {
                    linkedList.add(str3);
                }
            }
            for (String str4 : linkedList) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str5 = l;
                local2.d(str5, "remove from contact versions, contactId= " + str4);
                this.f2143a.remove(str4);
            }
        }
    }

    @DexIgnore
    public final void l() {
        AppNotificationFilterSettings appNotificationFilterSettings;
        FLogger.INSTANCE.getLocal().d(l, "updateNotificationSettings()");
        String J = PortfolioApp.d0.J();
        if (!TextUtils.isEmpty(J)) {
            if (!FossilDeviceSerialPatternUtil.isDianaDevice(J)) {
                appNotificationFilterSettings = e47.b.b(this.b.getAllNotificationsByHour(J, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()), e47.b.j(this.h.getSkuModelBySerialPrefix(nk5.o.m(J)), J));
            } else {
                appNotificationFilterSettings = new AppNotificationFilterSettings(e47.b.d(this.e, this.i, this.g), System.currentTimeMillis());
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = l;
            local.d(str, "updateNotificationSettings - data = " + appNotificationFilterSettings);
            this.d.b().execute(new f37(appNotificationFilterSettings, J));
        }
    }

    @DexIgnore
    public void onChange(boolean z, Uri uri) {
        String str;
        FLogger.INSTANCE.getLocal().d(l, "onChange selftChange=" + z + ", uri=" + uri + "update = " + this.k);
        if (System.currentTimeMillis() - this.j >= 1000) {
            this.j = System.currentTimeMillis();
            if (g47.f1261a.j(this.e, "android.permission.READ_PHONE_STATE", "android.permission.READ_CALL_LOG", "android.permission.READ_CONTACTS", "android.permission.READ_SMS")) {
                Cursor query = this.c.query(ContactsContract.DeletedContacts.CONTENT_URI, new String[]{"contact_id", "contact_deleted_timestamp"}, null, null, "contact_deleted_timestamp Desc");
                if (query != null && query.moveToFirst()) {
                    String string = query.getString(query.getColumnIndex("contact_id"));
                    Long valueOf = Long.valueOf(query.getLong(query.getColumnIndex("contact_deleted_timestamp")));
                    if (!(string == null || this.k == valueOf.longValue())) {
                        boolean z2 = this.k == -1;
                        this.k = valueOf.longValue();
                        int intValue = Integer.valueOf(string).intValue();
                        FLogger.INSTANCE.getLocal().d(l, "Last deleted happen for contactId=" + intValue);
                        ArrayList<ContactGroup> arrayList = new ArrayList();
                        List<ContactGroup> allContactGroups = this.b.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
                        List<ContactGroup> allContactGroups2 = this.b.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                        if (allContactGroups != null) {
                            arrayList.addAll(allContactGroups);
                        }
                        if (allContactGroups2 != null) {
                            arrayList.addAll(allContactGroups2);
                        }
                        ArrayList<PhoneFavoritesContact> arrayList2 = new ArrayList();
                        if (!arrayList.isEmpty()) {
                            boolean z3 = false;
                            for (ContactGroup contactGroup : arrayList) {
                                arrayList2.clear();
                                if (contactGroup.getContacts().get(0).getContactId() == intValue) {
                                    Contact contact = contactGroup.getContacts().get(0);
                                    FLogger.INSTANCE.getLocal().d(l, "Found deleted contact in app with contactName = " + contact.getDisplayName());
                                    for (PhoneNumber phoneNumber : contact.getPhoneNumbers()) {
                                        arrayList2.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
                                    }
                                    this.b.removeContact(contact);
                                    this.b.removeContactGroup(contactGroup);
                                    for (PhoneFavoritesContact phoneFavoritesContact : arrayList2) {
                                        this.b.removePhoneFavoritesContact(phoneFavoritesContact);
                                    }
                                    z3 = true;
                                }
                            }
                            if (z3) {
                                l();
                            }
                        }
                        if (!z2) {
                            query.close();
                            return;
                        }
                    }
                    query.close();
                }
                Cursor query2 = this.c.query(ContactsContract.Contacts.CONTENT_URI, new String[]{FieldType.FOREIGN_ID_FIELD_SUFFIX}, null, null, "contact_last_updated_timestamp Desc");
                if (query2 == null || !query2.moveToFirst()) {
                    str = "";
                } else {
                    str = query2.getString(query2.getColumnIndex(FieldType.FOREIGN_ID_FIELD_SUFFIX));
                    FLogger.INSTANCE.getLocal().d(l, "Last updated happen for contactId=" + str);
                    query2.close();
                }
                if (TextUtils.isEmpty(str)) {
                    FLogger.INSTANCE.getLocal().d(l, "No change for contact, return");
                    return;
                }
                String e2 = e(str);
                boolean c2 = c(str);
                String d2 = d(str);
                List<String> f2 = f(str);
                this.g.o1(true);
                if (g(str)) {
                    FLogger.INSTANCE.getLocal().d(l, "Update Contact: " + str + " - contactName: " + e2 + " - contactImageUri: " + d2 + " - contactPhone: " + f2 + " - isFavorites:" + c2);
                    this.f.d(str, e2, f2, d2);
                }
            }
        }
    }
}
