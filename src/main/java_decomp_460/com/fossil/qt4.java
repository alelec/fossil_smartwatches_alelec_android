package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qt4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ps4 f3023a;
    @DexIgnore
    public ms4 b;
    @DexIgnore
    public ms4 c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public qt4(ps4 ps4, ms4 ms4, ms4 ms42, boolean z) {
        pq7.c(ps4, "challenge");
        this.f3023a = ps4;
        this.b = ms4;
        this.c = ms42;
        this.d = z;
    }

    @DexIgnore
    public final ps4 a() {
        return this.f3023a;
    }

    @DexIgnore
    public final ms4 b() {
        return this.c;
    }

    @DexIgnore
    public final ms4 c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof qt4) {
                qt4 qt4 = (qt4) obj;
                if (!pq7.a(this.f3023a, qt4.f3023a) || !pq7.a(this.b, qt4.b) || !pq7.a(this.c, qt4.c) || this.d != qt4.d) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        ps4 ps4 = this.f3023a;
        int hashCode = ps4 != null ? ps4.hashCode() : 0;
        ms4 ms4 = this.b;
        int hashCode2 = ms4 != null ? ms4.hashCode() : 0;
        ms4 ms42 = this.c;
        if (ms42 != null) {
            i = ms42.hashCode();
        }
        boolean z = this.d;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((hashCode * 31) + hashCode2) * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "UIHistory(challenge=" + this.f3023a + ", topPlayer=" + this.b + ", currentPlayer=" + this.c + ", isFirst=" + this.d + ")";
    }
}
