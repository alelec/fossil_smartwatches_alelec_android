package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o05 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<ArrayList<String>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<ArrayList<String>> {
    }

    @DexIgnore
    public final String a(ArrayList<String> arrayList) {
        if (ff2.a(arrayList)) {
            return "";
        }
        String u = new Gson().u(arrayList, new a().getType());
        pq7.b(u, "Gson().toJson(list, type)");
        return u;
    }

    @DexIgnore
    public final ArrayList<String> b(String str) {
        pq7.c(str, "value");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList<>();
        }
        Object l = new Gson().l(str, new b().getType());
        pq7.b(l, "Gson().fromJson(value, type)");
        return (ArrayList) l;
    }
}
