package com.fossil;

import android.graphics.Rect;
import android.view.ViewGroup;
import androidx.transition.Transition;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qy0 extends oz0 {
    @DexIgnore
    public float b; // = 3.0f;
    @DexIgnore
    public int c; // = 80;

    @DexIgnore
    @Override // com.fossil.uy0
    public long c(ViewGroup viewGroup, Transition transition, wy0 wy0, wy0 wy02) {
        int i;
        int i2;
        int i3;
        if (wy0 == null && wy02 == null) {
            return 0;
        }
        Rect y = transition.y();
        if (wy02 == null || e(wy0) == 0) {
            i = -1;
            wy02 = wy0;
        } else {
            i = 1;
        }
        int f = f(wy02);
        int g = g(wy02);
        int[] iArr = new int[2];
        viewGroup.getLocationOnScreen(iArr);
        int round = iArr[0] + Math.round(viewGroup.getTranslationX());
        int round2 = iArr[1] + Math.round(viewGroup.getTranslationY());
        int width = round + viewGroup.getWidth();
        int height = round2 + viewGroup.getHeight();
        if (y != null) {
            i2 = y.centerX();
            i3 = y.centerY();
        } else {
            i2 = (round + width) / 2;
            i3 = (round2 + height) / 2;
        }
        float h = ((float) h(viewGroup, f, g, i2, i3, round, round2, width, height)) / ((float) i(viewGroup));
        long x = transition.x();
        if (x < 0) {
            x = 300;
        }
        return (long) Math.round((((float) (x * ((long) i))) / this.b) * h);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002f, code lost:
        if ((com.fossil.mo0.z(r7) == 1) != false) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0031, code lost:
        r1 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0011, code lost:
        if (r4 != false) goto L_0x0013;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0016  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int h(android.view.View r7, int r8, int r9, int r10, int r11, int r12, int r13, int r14, int r15) {
        /*
            r6 = this;
            r2 = 5
            r3 = 3
            r0 = 0
            r4 = 1
            int r1 = r6.c
            r5 = 8388611(0x800003, float:1.1754948E-38)
            if (r1 != r5) goto L_0x0023
            int r1 = com.fossil.mo0.z(r7)
            if (r1 != r4) goto L_0x0021
        L_0x0011:
            if (r4 == 0) goto L_0x0031
        L_0x0013:
            r1 = r2
        L_0x0014:
            if (r1 == r3) goto L_0x0053
            if (r1 == r2) goto L_0x0049
            r2 = 48
            if (r1 == r2) goto L_0x003f
            r2 = 80
            if (r1 == r2) goto L_0x0035
        L_0x0020:
            return r0
        L_0x0021:
            r4 = r0
            goto L_0x0011
        L_0x0023:
            r5 = 8388613(0x800005, float:1.175495E-38)
            if (r1 != r5) goto L_0x0014
            int r1 = com.fossil.mo0.z(r7)
            if (r1 != r4) goto L_0x0033
            r1 = r4
        L_0x002f:
            if (r1 == 0) goto L_0x0013
        L_0x0031:
            r1 = r3
            goto L_0x0014
        L_0x0033:
            r1 = r0
            goto L_0x002f
        L_0x0035:
            int r0 = r9 - r13
            int r1 = r10 - r8
            int r1 = java.lang.Math.abs(r1)
            int r0 = r0 + r1
            goto L_0x0020
        L_0x003f:
            int r0 = r15 - r9
            int r1 = r10 - r8
            int r1 = java.lang.Math.abs(r1)
            int r0 = r0 + r1
            goto L_0x0020
        L_0x0049:
            int r0 = r8 - r12
            int r1 = r11 - r9
            int r1 = java.lang.Math.abs(r1)
            int r0 = r0 + r1
            goto L_0x0020
        L_0x0053:
            int r0 = r14 - r8
            int r1 = r11 - r9
            int r1 = java.lang.Math.abs(r1)
            int r0 = r0 + r1
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qy0.h(android.view.View, int, int, int, int, int, int, int, int):int");
    }

    @DexIgnore
    public final int i(ViewGroup viewGroup) {
        int i = this.c;
        return (i == 3 || i == 5 || i == 8388611 || i == 8388613) ? viewGroup.getWidth() : viewGroup.getHeight();
    }

    @DexIgnore
    public void j(int i) {
        this.c = i;
    }
}
