package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import com.fossil.jh0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ug0 {
    @DexIgnore
    public static /* final */ PorterDuff.Mode b; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public static ug0 c;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public jh0 f3582a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements jh0.e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int[] f3583a; // = {pe0.abc_textfield_search_default_mtrl_alpha, pe0.abc_textfield_default_mtrl_alpha, pe0.abc_ab_share_pack_mtrl_alpha};
        @DexIgnore
        public /* final */ int[] b; // = {pe0.abc_ic_commit_search_api_mtrl_alpha, pe0.abc_seekbar_tick_mark_material, pe0.abc_ic_menu_share_mtrl_alpha, pe0.abc_ic_menu_copy_mtrl_am_alpha, pe0.abc_ic_menu_cut_mtrl_alpha, pe0.abc_ic_menu_selectall_mtrl_alpha, pe0.abc_ic_menu_paste_mtrl_am_alpha};
        @DexIgnore
        public /* final */ int[] c; // = {pe0.abc_textfield_activated_mtrl_alpha, pe0.abc_textfield_search_activated_mtrl_alpha, pe0.abc_cab_background_top_mtrl_alpha, pe0.abc_text_cursor_material, pe0.abc_text_select_handle_left_mtrl_dark, pe0.abc_text_select_handle_middle_mtrl_dark, pe0.abc_text_select_handle_right_mtrl_dark, pe0.abc_text_select_handle_left_mtrl_light, pe0.abc_text_select_handle_middle_mtrl_light, pe0.abc_text_select_handle_right_mtrl_light};
        @DexIgnore
        public /* final */ int[] d; // = {pe0.abc_popup_background_mtrl_mult, pe0.abc_cab_background_internal_bg, pe0.abc_menu_hardkey_panel_mtrl_mult};
        @DexIgnore
        public /* final */ int[] e; // = {pe0.abc_tab_indicator_material, pe0.abc_textfield_search_material};
        @DexIgnore
        public /* final */ int[] f; // = {pe0.abc_btn_check_material, pe0.abc_btn_radio_material, pe0.abc_btn_check_material_anim, pe0.abc_btn_radio_material_anim};

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0063  */
        /* JADX WARNING: Removed duplicated region for block: B:6:0x0019  */
        @Override // com.fossil.jh0.e
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(android.content.Context r8, int r9, android.graphics.drawable.Drawable r10) {
            /*
                r7 = this;
                r5 = 1
                r6 = 0
                r3 = -1
                android.graphics.PorterDuff$Mode r0 = com.fossil.ug0.a()
                int[] r1 = r7.f3583a
                boolean r2 = r7.f(r1, r9)
                r1 = 16842801(0x1010031, float:2.3693695E-38)
                if (r2 == 0) goto L_0x0035
                int r1 = com.fossil.le0.colorControlNormal
            L_0x0014:
                r2 = r3
                r4 = r1
            L_0x0016:
                r1 = r5
            L_0x0017:
                if (r1 == 0) goto L_0x0063
                boolean r1 = com.fossil.dh0.a(r10)
                if (r1 == 0) goto L_0x0023
                android.graphics.drawable.Drawable r10 = r10.mutate()
            L_0x0023:
                int r1 = com.fossil.oh0.c(r8, r4)
                android.graphics.PorterDuffColorFilter r0 = com.fossil.ug0.e(r1, r0)
                r10.setColorFilter(r0)
                if (r2 == r3) goto L_0x0033
                r10.setAlpha(r2)
            L_0x0033:
                r0 = r5
            L_0x0034:
                return r0
            L_0x0035:
                int[] r2 = r7.c
                boolean r2 = r7.f(r2, r9)
                if (r2 == 0) goto L_0x0040
                int r1 = com.fossil.le0.colorControlActivated
                goto L_0x0014
            L_0x0040:
                int[] r2 = r7.d
                boolean r2 = r7.f(r2, r9)
                if (r2 == 0) goto L_0x004b
                android.graphics.PorterDuff$Mode r0 = android.graphics.PorterDuff.Mode.MULTIPLY
                goto L_0x0014
            L_0x004b:
                int r2 = com.fossil.pe0.abc_list_divider_mtrl_alpha
                if (r9 != r2) goto L_0x005b
                r4 = 16842800(0x1010030, float:2.3693693E-38)
                r1 = 1109603123(0x42233333, float:40.8)
                int r1 = java.lang.Math.round(r1)
                r2 = r1
                goto L_0x0016
            L_0x005b:
                int r2 = com.fossil.pe0.abc_dialog_material_background
                if (r9 == r2) goto L_0x0014
                r1 = r6
                r2 = r3
                r4 = r6
                goto L_0x0017
            L_0x0063:
                r0 = r6
                goto L_0x0034
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ug0.a.a(android.content.Context, int, android.graphics.drawable.Drawable):boolean");
        }

        @DexIgnore
        @Override // com.fossil.jh0.e
        public PorterDuff.Mode b(int i) {
            if (i == pe0.abc_switch_thumb_material) {
                return PorterDuff.Mode.MULTIPLY;
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.jh0.e
        public Drawable c(jh0 jh0, Context context, int i) {
            if (i != pe0.abc_cab_background_top_material) {
                return null;
            }
            return new LayerDrawable(new Drawable[]{jh0.j(context, pe0.abc_cab_background_internal_bg), jh0.j(context, pe0.abc_cab_background_top_mtrl_alpha)});
        }

        @DexIgnore
        @Override // com.fossil.jh0.e
        public ColorStateList d(Context context, int i) {
            if (i == pe0.abc_edit_text_material) {
                return gf0.c(context, ne0.abc_tint_edittext);
            }
            if (i == pe0.abc_switch_track_mtrl_alpha) {
                return gf0.c(context, ne0.abc_tint_switch_track);
            }
            if (i == pe0.abc_switch_thumb_material) {
                return k(context);
            }
            if (i == pe0.abc_btn_default_mtrl_shape) {
                return j(context);
            }
            if (i == pe0.abc_btn_borderless_material) {
                return g(context);
            }
            if (i == pe0.abc_btn_colored_material) {
                return i(context);
            }
            if (i == pe0.abc_spinner_mtrl_am_alpha || i == pe0.abc_spinner_textfield_background_material) {
                return gf0.c(context, ne0.abc_tint_spinner);
            }
            if (f(this.b, i)) {
                return oh0.e(context, le0.colorControlNormal);
            }
            if (f(this.e, i)) {
                return gf0.c(context, ne0.abc_tint_default);
            }
            if (f(this.f, i)) {
                return gf0.c(context, ne0.abc_tint_btn_checkable);
            }
            if (i == pe0.abc_seekbar_thumb_material) {
                return gf0.c(context, ne0.abc_tint_seek_thumb);
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.jh0.e
        public boolean e(Context context, int i, Drawable drawable) {
            if (i == pe0.abc_seekbar_track_material) {
                LayerDrawable layerDrawable = (LayerDrawable) drawable;
                l(layerDrawable.findDrawableByLayerId(16908288), oh0.c(context, le0.colorControlNormal), ug0.b);
                l(layerDrawable.findDrawableByLayerId(16908303), oh0.c(context, le0.colorControlNormal), ug0.b);
                l(layerDrawable.findDrawableByLayerId(16908301), oh0.c(context, le0.colorControlActivated), ug0.b);
                return true;
            } else if (i != pe0.abc_ratingbar_material && i != pe0.abc_ratingbar_indicator_material && i != pe0.abc_ratingbar_small_material) {
                return false;
            } else {
                LayerDrawable layerDrawable2 = (LayerDrawable) drawable;
                l(layerDrawable2.findDrawableByLayerId(16908288), oh0.b(context, le0.colorControlNormal), ug0.b);
                l(layerDrawable2.findDrawableByLayerId(16908303), oh0.c(context, le0.colorControlActivated), ug0.b);
                l(layerDrawable2.findDrawableByLayerId(16908301), oh0.c(context, le0.colorControlActivated), ug0.b);
                return true;
            }
        }

        @DexIgnore
        public final boolean f(int[] iArr, int i) {
            for (int i2 : iArr) {
                if (i2 == i) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public final ColorStateList g(Context context) {
            return h(context, 0);
        }

        @DexIgnore
        public final ColorStateList h(Context context, int i) {
            int c2 = oh0.c(context, le0.colorControlHighlight);
            int b2 = oh0.b(context, le0.colorButtonNormal);
            int[] iArr = oh0.b;
            int[] iArr2 = oh0.d;
            int e2 = pl0.e(c2, i);
            int[] iArr3 = oh0.c;
            int e3 = pl0.e(c2, i);
            return new ColorStateList(new int[][]{iArr, iArr2, iArr3, oh0.f}, new int[]{b2, e2, e3, i});
        }

        @DexIgnore
        public final ColorStateList i(Context context) {
            return h(context, oh0.c(context, le0.colorAccent));
        }

        @DexIgnore
        public final ColorStateList j(Context context) {
            return h(context, oh0.c(context, le0.colorButtonNormal));
        }

        @DexIgnore
        public final ColorStateList k(Context context) {
            int[][] iArr = new int[3][];
            int[] iArr2 = new int[3];
            ColorStateList e2 = oh0.e(context, le0.colorSwitchThumbNormal);
            if (e2 == null || !e2.isStateful()) {
                iArr[0] = oh0.b;
                iArr2[0] = oh0.b(context, le0.colorSwitchThumbNormal);
                iArr[1] = oh0.e;
                iArr2[1] = oh0.c(context, le0.colorControlActivated);
                iArr[2] = oh0.f;
                iArr2[2] = oh0.c(context, le0.colorSwitchThumbNormal);
            } else {
                iArr[0] = oh0.b;
                iArr2[0] = e2.getColorForState(iArr[0], 0);
                iArr[1] = oh0.e;
                iArr2[1] = oh0.c(context, le0.colorControlActivated);
                iArr[2] = oh0.f;
                iArr2[2] = e2.getDefaultColor();
            }
            return new ColorStateList(iArr, iArr2);
        }

        @DexIgnore
        public final void l(Drawable drawable, int i, PorterDuff.Mode mode) {
            if (dh0.a(drawable)) {
                drawable = drawable.mutate();
            }
            if (mode == null) {
                mode = ug0.b;
            }
            drawable.setColorFilter(ug0.e(i, mode));
        }
    }

    @DexIgnore
    public static ug0 b() {
        ug0 ug0;
        synchronized (ug0.class) {
            try {
                if (c == null) {
                    h();
                }
                ug0 = c;
            } catch (Throwable th) {
                throw th;
            }
        }
        return ug0;
    }

    @DexIgnore
    public static PorterDuffColorFilter e(int i, PorterDuff.Mode mode) {
        PorterDuffColorFilter l;
        synchronized (ug0.class) {
            try {
                l = jh0.l(i, mode);
            } catch (Throwable th) {
                throw th;
            }
        }
        return l;
    }

    @DexIgnore
    public static void h() {
        synchronized (ug0.class) {
            try {
                if (c == null) {
                    ug0 ug0 = new ug0();
                    c = ug0;
                    ug0.f3582a = jh0.h();
                    c.f3582a.u(new a());
                }
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    public static void i(Drawable drawable, rh0 rh0, int[] iArr) {
        jh0.w(drawable, rh0, iArr);
    }

    @DexIgnore
    public Drawable c(Context context, int i) {
        Drawable j;
        synchronized (this) {
            j = this.f3582a.j(context, i);
        }
        return j;
    }

    @DexIgnore
    public Drawable d(Context context, int i, boolean z) {
        Drawable k;
        synchronized (this) {
            k = this.f3582a.k(context, i, z);
        }
        return k;
    }

    @DexIgnore
    public ColorStateList f(Context context, int i) {
        ColorStateList m;
        synchronized (this) {
            m = this.f3582a.m(context, i);
        }
        return m;
    }

    @DexIgnore
    public void g(Context context) {
        synchronized (this) {
            this.f3582a.s(context);
        }
    }
}
