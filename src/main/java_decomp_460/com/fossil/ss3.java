package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ss3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ss3> CREATOR; // = new rs3();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ sc2 c;

    @DexIgnore
    public ss3(int i, sc2 sc2) {
        this.b = i;
        this.c = sc2;
    }

    @DexIgnore
    public ss3(sc2 sc2) {
        this(1, sc2);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.n(parcel, 1, this.b);
        bd2.t(parcel, 2, this.c, i, false);
        bd2.b(parcel, a2);
    }
}
