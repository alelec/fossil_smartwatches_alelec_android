package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class um4 extends jm4 {
    @DexIgnore
    @Override // com.fossil.jm4, com.fossil.nm4
    public void a(om4 om4) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!om4.i()) {
                break;
            }
            char c = om4.c();
            om4.f++;
            c(c, sb);
            if (sb.length() % 3 == 0) {
                jm4.g(om4, sb);
                int n = qm4.n(om4.d(), om4.f, e());
                if (n != e()) {
                    om4.o(n);
                    break;
                }
            }
        }
        f(om4, sb);
    }

    @DexIgnore
    @Override // com.fossil.jm4
    public int c(char c, StringBuilder sb) {
        if (c == '\r') {
            sb.append((char) 0);
        } else if (c == '*') {
            sb.append((char) 1);
        } else if (c == '>') {
            sb.append((char) 2);
        } else if (c == ' ') {
            sb.append((char) 3);
        } else if (c >= '0' && c <= '9') {
            sb.append((char) ((c - '0') + 4));
        } else if (c < 'A' || c > 'Z') {
            qm4.e(c);
            throw null;
        } else {
            sb.append((char) ((c - 'A') + 14));
        }
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.jm4
    public int e() {
        return 3;
    }

    @DexIgnore
    @Override // com.fossil.jm4
    public void f(om4 om4, StringBuilder sb) {
        om4.p();
        int a2 = om4.g().a() - om4.a();
        om4.f -= sb.length();
        if (om4.f() > 1 || a2 > 1 || om4.f() != a2) {
            om4.r('\u00fe');
        }
        if (om4.e() < 0) {
            om4.o(0);
        }
    }
}
