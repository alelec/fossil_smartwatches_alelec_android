package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p90 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ HashMap<Integer, cl7<zu1, av1>> f2804a; // = zm7.i(hl7.a(3073, new cl7(zu1.RING_PHONE, av1.STANDARD)), hl7.a(6657, new cl7(zu1.ALARM, av1.STANDARD)), hl7.a(6658, new cl7(zu1.ALARM, av1.SEQUENCED)), hl7.a(7169, new cl7(zu1.PROGRESS, av1.STANDARD)), hl7.a(7170, new cl7(zu1.PROGRESS, av1.SWEEP)), hl7.a(7681, new cl7(zu1.TWENTY_FOUR_HOUR, av1.STANDARD)), hl7.a(7682, new cl7(zu1.TWENTY_FOUR_HOUR, av1.SEQUENCED)), hl7.a(1025, new cl7(zu1.GOAL_TRACKING, av1.STANDARD)), hl7.a(4097, new cl7(zu1.SELFIE, av1.STANDARD)), hl7.a(4609, new cl7(zu1.MUSIC_CONTROL, av1.PLAY_PAUSE)), hl7.a(4610, new cl7(zu1.MUSIC_CONTROL, av1.NEXT)), hl7.a(4611, new cl7(zu1.MUSIC_CONTROL, av1.PREVIOUS)), hl7.a(4612, new cl7(zu1.MUSIC_CONTROL, av1.VOLUME_UP)), hl7.a(4613, new cl7(zu1.MUSIC_CONTROL, av1.VOLUME_DOWN)), hl7.a(4614, new cl7(zu1.MUSIC_CONTROL, av1.STANDARD)), hl7.a(5121, new cl7(zu1.DATE, av1.STANDARD)), hl7.a(5122, new cl7(zu1.DATE, av1.SEQUENCED)), hl7.a(5633, new cl7(zu1.TIME2, av1.STANDARD)), hl7.a(5634, new cl7(zu1.TIME2, av1.SEQUENCED)), hl7.a(6145, new cl7(zu1.ALERT, av1.STANDARD)), hl7.a(6146, new cl7(zu1.ALERT, av1.SEQUENCED)), hl7.a(8193, new cl7(zu1.STOPWATCH, av1.STANDARD)), hl7.a(9217, new cl7(zu1.COMMUTE_TIME, av1.TRAVEL)), hl7.a(9218, new cl7(zu1.COMMUTE_TIME, av1.ETA)));
    @DexIgnore
    public static /* final */ p90 b; // = new p90();

    @DexIgnore
    public final zu1 a(short s) {
        cl7<zu1, av1> cl7 = f2804a.get(Integer.valueOf(s));
        return cl7 != null ? cl7.getFirst() : zu1.UNDEFINED;
    }

    @DexIgnore
    public final av1 b(short s) {
        cl7<zu1, av1> cl7 = f2804a.get(Integer.valueOf(s));
        return cl7 != null ? cl7.getSecond() : av1.UNDEFINED;
    }
}
