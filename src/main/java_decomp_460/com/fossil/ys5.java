package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ys5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f4362a;
    @DexIgnore
    public ArrayList<us5> b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public ys5(String str, String str2, ArrayList<us5> arrayList, boolean z) {
        pq7.c(str, "tagName");
        pq7.c(str2, "title");
        pq7.c(arrayList, "listItem");
        this.f4362a = str2;
        this.b = arrayList;
        this.c = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ys5(String str, String str2, ArrayList arrayList, boolean z, int i, kq7 kq7) {
        this(str, str2, arrayList, (i & 8) != 0 ? false : z);
    }

    @DexIgnore
    public final ArrayList<us5> a() {
        return this.b;
    }

    @DexIgnore
    public final String b() {
        return this.f4362a;
    }

    @DexIgnore
    public final boolean c() {
        return this.c;
    }

    @DexIgnore
    public final void d(boolean z) {
        this.c = z;
    }
}
