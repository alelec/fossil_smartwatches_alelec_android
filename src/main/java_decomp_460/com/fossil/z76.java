package com.fossil;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z76 extends v76 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public PlacesClient e;
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public String g;
    @DexIgnore
    public /* final */ String h; // = um5.c(PortfolioApp.h0.c(), 2131886354);
    @DexIgnore
    public /* final */ String i; // = um5.c(PortfolioApp.h0.c(), 2131886355);
    @DexIgnore
    public /* final */ ArrayList<String> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ w76 k;
    @DexIgnore
    public /* final */ on5 l;
    @DexIgnore
    public /* final */ UserRepository m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $address$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ zq7 $isAddressSaved$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ zq7 $isChanged$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ String $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z76 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.z76$a$a")
        /* renamed from: com.fossil.z76$a$a  reason: collision with other inner class name */
        public static final class C0303a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0303a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0303a aVar = new C0303a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((C0303a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.x().I0(this.this$0.this$0.j);
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository y = this.this$0.this$0.y();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = y.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c extends ko7 implements vp7<iv7, qn7<? super iq5<? extends MFUser>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MFUser $user;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(MFUser mFUser, qn7 qn7, a aVar) {
                super(2, qn7);
                this.$user = mFUser;
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.$user, qn7, this.this$0);
                cVar.p$ = (iv7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super iq5<? extends MFUser>> qn7) {
                return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository y = this.this$0.this$0.y();
                    MFUser mFUser = this.$user;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object updateUser = y.updateUser(mFUser, true, this);
                    return updateUser == d ? d : updateUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class d extends ko7 implements vp7<iv7, qn7<? super iq5<? extends MFUser>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MFUser $user;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(MFUser mFUser, qn7 qn7, a aVar) {
                super(2, qn7);
                this.$user = mFUser;
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                d dVar = new d(this.$user, qn7, this.this$0);
                dVar.p$ = (iv7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super iq5<? extends MFUser>> qn7) {
                return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository y = this.this$0.this$0.y();
                    MFUser mFUser = this.$user;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object updateUser = y.updateUser(mFUser, false, this);
                    return updateUser == d ? d : updateUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(String str, qn7 qn7, z76 z76, String str2, zq7 zq7, zq7 zq72) {
            super(2, qn7);
            this.$it = str;
            this.this$0 = z76;
            this.$address$inlined = str2;
            this.$isChanged$inlined = zq7;
            this.$isAddressSaved$inlined = zq72;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.$it, qn7, this.this$0, this.$address$inlined, this.$isChanged$inlined, this.$isAddressSaved$inlined);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:116:0x02ff  */
        /* JADX WARNING: Removed duplicated region for block: B:117:0x030a  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0081  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00af  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0101  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0150  */
        /* JADX WARNING: Removed duplicated region for block: B:86:0x023b  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 781
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.z76.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$start$1", f = "CommuteTimeSettingsDefaultAddressPresenter.kt", l = {40}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z76 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$start$1$recentSearchedAddress$1", f = "CommuteTimeSettingsDefaultAddressPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<String>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<String>> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.x().h();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(z76 z76, qn7 qn7) {
            super(2, qn7);
            this.this$0 = z76;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(i2, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            pq7.b(g, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
            this.this$0.j.clear();
            this.this$0.j.addAll((List) g);
            this.this$0.e = Places.createClient(PortfolioApp.h0.c());
            this.this$0.z().H(this.this$0.e);
            if (pq7.a(this.this$0.g, "Home")) {
                w76 z = this.this$0.z();
                String str = this.this$0.h;
                pq7.b(str, "mHomeTitle");
                z.setTitle(str);
            } else {
                w76 z2 = this.this$0.z();
                String str2 = this.this$0.i;
                pq7.b(str2, "mWorkTitle");
                z2.setTitle(str2);
            }
            this.this$0.z().V1(this.this$0.f);
            this.this$0.z().h0(this.this$0.j);
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = z76.class.getSimpleName();
        pq7.b(simpleName, "CommuteTimeSettingsDefau\u2026er::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public z76(w76 w76, on5 on5, UserRepository userRepository) {
        pq7.c(w76, "mView");
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(userRepository, "mUserRepository");
        this.k = w76;
        this.l = on5;
        this.m = userRepository;
    }

    @DexIgnore
    public void A(String str, String str2) {
        pq7.c(str, "addressType");
        pq7.c(str2, "defaultPlace");
        this.f = str2;
        this.g = str;
    }

    @DexIgnore
    public void B() {
        this.k.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        xw7 unused = gu7.d(k(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        this.e = null;
    }

    @DexIgnore
    @Override // com.fossil.v76
    public void n(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = n;
        local.d(str2, "onUserExit with address " + str);
        zq7 zq7 = new zq7();
        zq7.element = false;
        zq7 zq72 = new zq7();
        zq72.element = false;
        if (str == null || gu7.d(k(), null, null, new a(str, null, this, str, zq7, zq72), 3, null) == null) {
            this.k.S3(null);
            tl7 tl7 = tl7.f3441a;
        }
    }

    @DexIgnore
    public final on5 x() {
        return this.l;
    }

    @DexIgnore
    public final UserRepository y() {
        return this.m;
    }

    @DexIgnore
    public final w76 z() {
        return this.k;
    }
}
