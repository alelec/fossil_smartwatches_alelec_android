package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ka8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Double f1895a;
    @DexIgnore
    public Double b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ String j;
    @DexIgnore
    public /* final */ long k;

    @DexIgnore
    public ka8(String str, String str2, long j2, long j3, int i2, int i3, int i4, String str3, long j4) {
        pq7.c(str, "id");
        pq7.c(str2, "path");
        pq7.c(str3, "displayName");
        this.c = str;
        this.d = str2;
        this.e = j2;
        this.f = j3;
        this.g = i2;
        this.h = i3;
        this.i = i4;
        this.j = str3;
        this.k = j4;
    }

    @DexIgnore
    public final long a() {
        return this.f;
    }

    @DexIgnore
    public final String b() {
        return this.j;
    }

    @DexIgnore
    public final long c() {
        return this.e;
    }

    @DexIgnore
    public final int d() {
        return this.h;
    }

    @DexIgnore
    public final String e() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof ka8)) {
                return false;
            }
            ka8 ka8 = (ka8) obj;
            if (!pq7.a(this.c, ka8.c) || !pq7.a(this.d, ka8.d)) {
                return false;
            }
            if (!(this.e == ka8.e)) {
                return false;
            }
            if (!(this.f == ka8.f)) {
                return false;
            }
            if (!(this.g == ka8.g)) {
                return false;
            }
            if (!(this.h == ka8.h)) {
                return false;
            }
            if (!(this.i == ka8.i) || !pq7.a(this.j, ka8.j)) {
                return false;
            }
            if (!(this.k == ka8.k)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final Double f() {
        return this.f1895a;
    }

    @DexIgnore
    public final Double g() {
        return this.b;
    }

    @DexIgnore
    public final long h() {
        return this.k;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        String str = this.c;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.d;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        long j2 = this.e;
        int i3 = (int) (j2 ^ (j2 >>> 32));
        long j3 = this.f;
        int i4 = (int) (j3 ^ (j3 >>> 32));
        int i5 = this.g;
        int i6 = this.h;
        int i7 = this.i;
        String str3 = this.j;
        if (str3 != null) {
            i2 = str3.hashCode();
        }
        long j4 = this.k;
        return (((((((((((((((hashCode * 31) + hashCode2) * 31) + i3) * 31) + i4) * 31) + i5) * 31) + i6) * 31) + i7) * 31) + i2) * 31) + ((int) ((j4 >>> 32) ^ j4));
    }

    @DexIgnore
    public final String i() {
        return this.d;
    }

    @DexIgnore
    public final int j() {
        return this.i;
    }

    @DexIgnore
    public final int k() {
        return this.g;
    }

    @DexIgnore
    public final void l(Double d2) {
        this.f1895a = d2;
    }

    @DexIgnore
    public final void m(Double d2) {
        this.b = d2;
    }

    @DexIgnore
    public String toString() {
        return "AssetEntity(id=" + this.c + ", path=" + this.d + ", duration=" + this.e + ", createDt=" + this.f + ", width=" + this.g + ", height=" + this.h + ", type=" + this.i + ", displayName=" + this.j + ", modifiedDate=" + this.k + ")";
    }
}
