package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class wt extends Enum<wt> implements mt {
    @DexIgnore
    public static /* final */ wt d;
    @DexIgnore
    public static /* final */ wt e;
    @DexIgnore
    public static /* final */ /* synthetic */ wt[] f;
    @DexIgnore
    public static /* final */ vt g; // = new vt(null);
    @DexIgnore
    public /* final */ String b; // = ey1.a(this);
    @DexIgnore
    public /* final */ byte c;

    /*
    static {
        wt wtVar = new wt("SUCCESS", 0, (byte) 0);
        d = wtVar;
        wt wtVar2 = new wt("WRONG_LENGTH", 1, (byte) 1);
        wt wtVar3 = new wt("FAIL_TO_GENERATE_RANDOM_NUMBER", 2, (byte) 2);
        wt wtVar4 = new wt("FAIL_TO_GET_KEY", 3, (byte) 3);
        wt wtVar5 = new wt("FAIL_TO_ENCRYPT_FRAME", 4, (byte) 4);
        wt wtVar6 = new wt("FAIL_TO_DECRYPT_FRAME", 5, (byte) 5);
        wt wtVar7 = new wt("INVALID_KEY_TYPE", 6, (byte) 6);
        wt wtVar8 = new wt("WRONG_CONFIRM_RANDOM_NUMBER", 7, (byte) 7);
        wt wtVar9 = new wt("WRONG_STATE", 8, (byte) 8);
        wt wtVar10 = new wt("FAIL_TO_STORE_KEY", 9, (byte) 9);
        wt wtVar11 = new wt("WRONG_KEY_OPERATION", 10, (byte) 10);
        wt wtVar12 = new wt("NOT_SUPPORTED", 11, (byte) 11);
        wt wtVar13 = new wt("FAIL_TO_GENERATE_PUBLIC_KEY", 12, (byte) 12);
        wt wtVar14 = new wt("FAIL_TO_GENERATE_SECRET_KEY", 13, (byte) 13);
        wt wtVar15 = new wt("FAIL_BECAUSE_OF_INVALID_INPUT", 14, DateTimeFieldType.HOUR_OF_HALFDAY);
        wt wtVar16 = new wt("FAIL_BECAUSE_OF_NOT_SET_KEY", 15, DateTimeFieldType.CLOCKHOUR_OF_HALFDAY);
        wt wtVar17 = new wt("FAIL_TO_SEND_REQUEST_AUTHENTICATE_THROUGH_ASYNC", 16, DateTimeFieldType.CLOCKHOUR_OF_DAY);
        wt wtVar18 = new wt("UNKNOWN", 17, (byte) 255);
        e = wtVar18;
        f = new wt[]{wtVar, wtVar2, wtVar3, wtVar4, wtVar5, wtVar6, wtVar7, wtVar8, wtVar9, wtVar10, wtVar11, wtVar12, wtVar13, wtVar14, wtVar15, wtVar16, wtVar17, wtVar18};
    }
    */

    @DexIgnore
    public wt(String str, int i, byte b2) {
        this.c = (byte) b2;
    }

    @DexIgnore
    public static wt valueOf(String str) {
        return (wt) Enum.valueOf(wt.class, str);
    }

    @DexIgnore
    public static wt[] values() {
        return (wt[]) f.clone();
    }

    @DexIgnore
    @Override // com.fossil.mt
    public boolean a() {
        return this == d;
    }

    @DexIgnore
    @Override // com.fossil.mt
    public String getLogName() {
        return this.b;
    }
}
