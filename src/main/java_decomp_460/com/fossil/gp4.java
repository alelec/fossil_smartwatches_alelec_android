package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gp4 implements Factory<cn5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f1341a;

    @DexIgnore
    public gp4(uo4 uo4) {
        this.f1341a = uo4;
    }

    @DexIgnore
    public static gp4 a(uo4 uo4) {
        return new gp4(uo4);
    }

    @DexIgnore
    public static cn5 c(uo4 uo4) {
        cn5 n = uo4.n();
        lk7.c(n, "Cannot return null from a non-@Nullable @Provides method");
        return n;
    }

    @DexIgnore
    /* renamed from: b */
    public cn5 get() {
        return c(this.f1341a);
    }
}
