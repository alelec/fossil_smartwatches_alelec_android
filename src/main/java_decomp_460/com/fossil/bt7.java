package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bt7<T, R> implements ts7<R> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ts7<T> f507a;
    @DexIgnore
    public /* final */ rp7<T, R> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<R>, jr7 {
        @DexIgnore
        public /* final */ Iterator<T> b;
        @DexIgnore
        public /* final */ /* synthetic */ bt7 c;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(bt7 bt7) {
            this.c = bt7;
            this.b = bt7.f507a.iterator();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public R next() {
            return (R) this.c.b.invoke(this.b.next());
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.ts7<? extends T> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.rp7<? super T, ? extends R> */
    /* JADX WARN: Multi-variable type inference failed */
    public bt7(ts7<? extends T> ts7, rp7<? super T, ? extends R> rp7) {
        pq7.c(ts7, "sequence");
        pq7.c(rp7, "transformer");
        this.f507a = ts7;
        this.b = rp7;
    }

    @DexIgnore
    @Override // com.fossil.ts7
    public Iterator<R> iterator() {
        return new a(this);
    }
}
