package com.fossil;

import android.net.Uri;
import android.util.Log;
import com.google.android.gms.common.data.DataHolder;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kv3 extends sb2 implements vu3 {
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public kv3(DataHolder dataHolder, int i, int i2) {
        super(dataHolder, i);
        this.d = i2;
    }

    @DexIgnore
    public final Map<String, wu3> e() {
        HashMap hashMap = new HashMap(this.d);
        for (int i = 0; i < this.d; i++) {
            jv3 jv3 = new jv3(this.f3230a, this.b + i);
            if (jv3.e() != null) {
                hashMap.put(jv3.e(), jv3);
            }
        }
        return hashMap;
    }

    @DexIgnore
    public final byte[] f() {
        return a("data");
    }

    @DexIgnore
    public final Uri g() {
        return Uri.parse(c("path"));
    }

    @DexIgnore
    public final String toString() {
        boolean isLoggable = Log.isLoggable("DataItem", 3);
        byte[] f = f();
        Map<String, wu3> e = e();
        StringBuilder sb = new StringBuilder("DataItemRef{ ");
        String valueOf = String.valueOf(g());
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 4);
        sb2.append("uri=");
        sb2.append(valueOf);
        sb.append(sb2.toString());
        String valueOf2 = String.valueOf(f == null ? "null" : Integer.valueOf(f.length));
        StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf2).length() + 9);
        sb3.append(", dataSz=");
        sb3.append(valueOf2);
        sb.append(sb3.toString());
        int size = e.size();
        StringBuilder sb4 = new StringBuilder(23);
        sb4.append(", numAssets=");
        sb4.append(size);
        sb.append(sb4.toString());
        if (isLoggable && !e.isEmpty()) {
            sb.append(", assets=[");
            String str = "";
            for (Map.Entry<String, wu3> entry : e.entrySet()) {
                String key = entry.getKey();
                String id = entry.getValue().getId();
                StringBuilder sb5 = new StringBuilder(str.length() + 2 + String.valueOf(key).length() + String.valueOf(id).length());
                sb5.append(str);
                sb5.append(key);
                sb5.append(": ");
                sb5.append(id);
                sb.append(sb5.toString());
                str = ", ";
            }
            sb.append("]");
        }
        sb.append(" }");
        return sb.toString();
    }
}
