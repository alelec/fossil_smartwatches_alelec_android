package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lo1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ short b;
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ short d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<lo1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public lo1 createFromParcel(Parcel parcel) {
            return new lo1((short) parcel.readInt(), (short) parcel.readInt(), (short) parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public lo1[] newArray(int i) {
            return new lo1[i];
        }
    }

    @DexIgnore
    public lo1(short s, short s2, short s3, int i) throws IllegalArgumentException {
        boolean z = false;
        this.b = (short) s;
        this.c = (short) s2;
        this.d = (short) s3;
        this.e = i;
        if (s == -2 || s == -1 || (s >= 0 && 359 >= s)) {
            short s4 = this.c;
            if (s4 == -2 || s4 == -1 || (s4 >= 0 && 359 >= s4)) {
                short s5 = this.d;
                if (s5 == -2 || s5 == -1 || (s5 >= 0 && 359 >= s5)) {
                    int i2 = this.e;
                    if (1000 <= i2 && 60000 >= i2) {
                        z = true;
                    }
                    if (!z) {
                        throw new IllegalArgumentException(e.c(e.e("durationInMs ("), this.e, ") is out of range ", "[1000, 60000]."));
                    }
                    return;
                }
                StringBuilder e2 = e.e("subeyeDegree (");
                e2.append((int) this.d);
                e2.append(") must be equal to ");
                e2.append("-2 or -1 or in range");
                e2.append("[0, 359].");
                throw new IllegalArgumentException(e2.toString());
            }
            StringBuilder e3 = e.e("minuteDegree (");
            e3.append((int) this.c);
            e3.append(") must be equal to ");
            e3.append("-2 or -1 or in range");
            e3.append("[0, 359].");
            throw new IllegalArgumentException(e3.toString());
        }
        StringBuilder e4 = e.e("hourDegree (");
        e4.append((int) this.b);
        e4.append(") must be equal to ");
        e4.append("-2 or -1 or in range");
        e4.append("[0, 359].");
        throw new IllegalArgumentException(e4.toString());
    }

    @DexIgnore
    public final byte[] a() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putShort(this.b).putShort(this.c).putShort(this.d).putShort((short) this.e).array();
        pq7.b(array, "ByteBuffer.allocate(DATA\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(lo1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            lo1 lo1 = (lo1) obj;
            return this.b == lo1.b && this.c == lo1.c && this.d == lo1.d && this.e == lo1.e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.NotificationHandMovingConfig");
    }

    @DexIgnore
    public final int getDurationInMs() {
        return this.e;
    }

    @DexIgnore
    public final short getHourDegree() {
        return this.b;
    }

    @DexIgnore
    public final short getMinuteDegree() {
        return this.c;
    }

    @DexIgnore
    public final short getSubeyeDegree() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.b * 31) + this.c) * 31) + this.d) * 31) + this.e;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.x2, Short.valueOf(this.b)), jd0.y2, Short.valueOf(this.c)), jd0.z2, Short.valueOf(this.d)), jd0.A2, Integer.valueOf(this.e));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }
}
