package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nu7 {
    @DexIgnore
    public static final void a(ku7<?> ku7, dw7 dw7) {
        ku7.e(new ew7(dw7));
    }

    @DexIgnore
    public static final <T> lu7<T> b(qn7<? super T> qn7) {
        if (!(qn7 instanceof vv7)) {
            return new lu7<>(qn7, 0);
        }
        lu7<T> l = ((vv7) qn7).l();
        if (l != null) {
            if (!l.B()) {
                l = null;
            }
            if (l != null) {
                return l;
            }
        }
        return new lu7<>(qn7, 0);
    }

    @DexIgnore
    public static final void c(ku7<?> ku7, lz7 lz7) {
        ku7.e(new ox7(lz7));
    }
}
