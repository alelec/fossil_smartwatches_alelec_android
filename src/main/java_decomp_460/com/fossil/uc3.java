package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uc3 extends ds2 implements tc3 {
    @DexIgnore
    public uc3() {
        super("com.google.android.gms.maps.internal.IOnMarkerDragListener");
    }

    @DexIgnore
    @Override // com.fossil.ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            U1(ms2.e(parcel.readStrongBinder()));
        } else if (i == 2) {
            t2(ms2.e(parcel.readStrongBinder()));
        } else if (i != 3) {
            return false;
        } else {
            q2(ms2.e(parcel.readStrongBinder()));
        }
        parcel2.writeNoException();
        return true;
    }
}
