package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tl extends lp {
    @DexIgnore
    public se C;
    @DexIgnore
    public /* final */ ArrayList<ow> D; // = by1.a(this.i, hm7.c(ow.DEVICE_CONFIG));
    @DexIgnore
    public /* final */ ve E;

    @DexIgnore
    public tl(k5 k5Var, i60 i60, ve veVar, String str) {
        super(k5Var, i60, yp.k, str, false, 16);
        this.E = veVar;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        lp.i(this, new su(this.w), new vj(this), new hk(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(super.C(), jd0.m4, this.E.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        JSONObject E2 = super.E();
        jd0 jd0 = jd0.n4;
        se seVar = this.C;
        return g80.k(E2, jd0, seVar != null ? seVar.toJSONObject() : null);
    }

    @DexIgnore
    public final se I() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        se seVar = this.C;
        return seVar != null ? seVar : new se(0, 0, 0);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.D;
    }
}
