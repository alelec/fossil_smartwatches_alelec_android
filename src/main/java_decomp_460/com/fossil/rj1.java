package com.fossil;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public abstract class rj1<T extends View, Z> extends jj1<Z> {
    @DexIgnore
    public static int g; // = ta1.glide_custom_view_target_tag;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ a c;
    @DexIgnore
    public View.OnAttachStateChangeListener d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static Integer e;

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ View f3116a;
        @DexIgnore
        public /* final */ List<pj1> b; // = new ArrayList();
        @DexIgnore
        public boolean c;
        @DexIgnore
        public ViewTreeObserver$OnPreDrawListenerC0208a d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.rj1$a$a")
        /* renamed from: com.fossil.rj1$a$a  reason: collision with other inner class name */
        public static final class ViewTreeObserver$OnPreDrawListenerC0208a implements ViewTreeObserver.OnPreDrawListener {
            @DexIgnore
            public /* final */ WeakReference<a> b;

            @DexIgnore
            public ViewTreeObserver$OnPreDrawListenerC0208a(a aVar) {
                this.b = new WeakReference<>(aVar);
            }

            @DexIgnore
            public boolean onPreDraw() {
                if (Log.isLoggable("ViewTarget", 2)) {
                    Log.v("ViewTarget", "OnGlobalLayoutListener called attachStateListener=" + this);
                }
                a aVar = this.b.get();
                if (aVar == null) {
                    return true;
                }
                aVar.a();
                return true;
            }
        }

        @DexIgnore
        public a(View view) {
            this.f3116a = view;
        }

        @DexIgnore
        public static int c(Context context) {
            if (e == null) {
                WindowManager windowManager = (WindowManager) context.getSystemService("window");
                ik1.d(windowManager);
                Display defaultDisplay = windowManager.getDefaultDisplay();
                Point point = new Point();
                defaultDisplay.getSize(point);
                e = Integer.valueOf(Math.max(point.x, point.y));
            }
            return e.intValue();
        }

        @DexIgnore
        public void a() {
            if (!this.b.isEmpty()) {
                int g = g();
                int f = f();
                if (i(g, f)) {
                    j(g, f);
                    b();
                }
            }
        }

        @DexIgnore
        public void b() {
            ViewTreeObserver viewTreeObserver = this.f3116a.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this.d);
            }
            this.d = null;
            this.b.clear();
        }

        @DexIgnore
        public void d(pj1 pj1) {
            int g = g();
            int f = f();
            if (i(g, f)) {
                pj1.d(g, f);
                return;
            }
            if (!this.b.contains(pj1)) {
                this.b.add(pj1);
            }
            if (this.d == null) {
                ViewTreeObserver viewTreeObserver = this.f3116a.getViewTreeObserver();
                ViewTreeObserver$OnPreDrawListenerC0208a aVar = new ViewTreeObserver$OnPreDrawListenerC0208a(this);
                this.d = aVar;
                viewTreeObserver.addOnPreDrawListener(aVar);
            }
        }

        @DexIgnore
        public final int e(int i, int i2, int i3) {
            int i4 = i2 - i3;
            if (i4 > 0) {
                return i4;
            }
            if (this.c && this.f3116a.isLayoutRequested()) {
                return 0;
            }
            int i5 = i - i3;
            if (i5 > 0) {
                return i5;
            }
            if (this.f3116a.isLayoutRequested() || i2 != -2) {
                return 0;
            }
            if (Log.isLoggable("ViewTarget", 4)) {
                Log.i("ViewTarget", "Glide treats LayoutParams.WRAP_CONTENT as a request for an image the size of this device's screen dimensions. If you want to load the original image and are ok with the corresponding memory cost and OOMs (depending on the input size), use override(Target.SIZE_ORIGINAL). Otherwise, use LayoutParams.MATCH_PARENT, set layout_width and layout_height to fixed dimension, or use .override() with fixed dimensions.");
            }
            return c(this.f3116a.getContext());
        }

        @DexIgnore
        public final int f() {
            int paddingTop = this.f3116a.getPaddingTop();
            int paddingBottom = this.f3116a.getPaddingBottom();
            ViewGroup.LayoutParams layoutParams = this.f3116a.getLayoutParams();
            return e(this.f3116a.getHeight(), layoutParams != null ? layoutParams.height : 0, paddingTop + paddingBottom);
        }

        @DexIgnore
        public final int g() {
            int paddingLeft = this.f3116a.getPaddingLeft();
            int paddingRight = this.f3116a.getPaddingRight();
            ViewGroup.LayoutParams layoutParams = this.f3116a.getLayoutParams();
            return e(this.f3116a.getWidth(), layoutParams != null ? layoutParams.width : 0, paddingLeft + paddingRight);
        }

        @DexIgnore
        public final boolean h(int i) {
            return i > 0 || i == Integer.MIN_VALUE;
        }

        @DexIgnore
        public final boolean i(int i, int i2) {
            return h(i) && h(i2);
        }

        @DexIgnore
        public final void j(int i, int i2) {
            Iterator it = new ArrayList(this.b).iterator();
            while (it.hasNext()) {
                ((pj1) it.next()).d(i, i2);
            }
        }

        @DexIgnore
        public void k(pj1 pj1) {
            this.b.remove(pj1);
        }
    }

    @DexIgnore
    public rj1(T t) {
        ik1.d(t);
        this.b = t;
        this.c = new a(t);
    }

    @DexIgnore
    @Override // com.fossil.qj1
    public void a(pj1 pj1) {
        this.c.k(pj1);
    }

    @DexIgnore
    public final Object c() {
        return this.b.getTag(g);
    }

    @DexIgnore
    @Override // com.fossil.qj1
    public void d(bj1 bj1) {
        l(bj1);
    }

    @DexIgnore
    public final void e() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.d;
        if (onAttachStateChangeListener != null && !this.f) {
            this.b.addOnAttachStateChangeListener(onAttachStateChangeListener);
            this.f = true;
        }
    }

    @DexIgnore
    public final void g() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.d;
        if (onAttachStateChangeListener != null && this.f) {
            this.b.removeOnAttachStateChangeListener(onAttachStateChangeListener);
            this.f = false;
        }
    }

    @DexIgnore
    @Override // com.fossil.jj1, com.fossil.qj1
    public void h(Drawable drawable) {
        super.h(drawable);
        e();
    }

    @DexIgnore
    @Override // com.fossil.qj1
    public bj1 i() {
        Object c2 = c();
        if (c2 == null) {
            return null;
        }
        if (c2 instanceof bj1) {
            return (bj1) c2;
        }
        throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
    }

    @DexIgnore
    @Override // com.fossil.jj1, com.fossil.qj1
    public void j(Drawable drawable) {
        super.j(drawable);
        this.c.b();
        if (!this.e) {
            g();
        }
    }

    @DexIgnore
    @Override // com.fossil.qj1
    public void k(pj1 pj1) {
        this.c.d(pj1);
    }

    @DexIgnore
    public final void l(Object obj) {
        this.b.setTag(g, obj);
    }

    @DexIgnore
    public String toString() {
        return "Target for: " + this.b;
    }
}
