package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p27 extends iq4<a, c, b> {
    @DexIgnore
    public /* final */ DianaPresetRepository d;
    @DexIgnore
    public /* final */ CustomizeRealDataRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.usecase.DSTChangeUseCase", f = "DSTChangeUseCase.kt", l = {29}, m = "run")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ p27 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(p27 p27, qn7 qn7) {
            super(qn7);
            this.this$0 = p27;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore
    public p27(DianaPresetRepository dianaPresetRepository, CustomizeRealDataRepository customizeRealDataRepository) {
        pq7.c(dianaPresetRepository, "mDianaPresetRepository");
        pq7.c(customizeRealDataRepository, "mRealDataRepository");
        this.d = dianaPresetRepository;
        this.e = customizeRealDataRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "DSTChangeUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [('+' char), (r2v15 double), ('h' char)] */
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r2v15 double), ('h' char)] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.p27.a r7, com.fossil.qn7<java.lang.Object> r8) {
        /*
        // Method dump skipped, instructions count: 321
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.p27.k(com.fossil.p27$a, com.fossil.qn7):java.lang.Object");
    }
}
