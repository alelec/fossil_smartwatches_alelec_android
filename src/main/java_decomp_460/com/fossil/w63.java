package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w63 implements xw2<v63> {
    @DexIgnore
    public static w63 c; // = new w63();
    @DexIgnore
    public /* final */ xw2<v63> b;

    @DexIgnore
    public w63() {
        this(ww2.b(new y63()));
    }

    @DexIgnore
    public w63(xw2<v63> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((v63) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((v63) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ v63 zza() {
        return this.b.zza();
    }
}
