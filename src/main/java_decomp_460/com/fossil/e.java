package com.fossil;

import com.fossil.bl1;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class e {
    @DexIgnore
    public static String a(String str) {
        String uuid = UUID.randomUUID().toString();
        pq7.b(uuid, str);
        return uuid;
    }

    @DexIgnore
    public static String b(StringBuilder sb, int i, String str) {
        sb.append(i);
        sb.append(str);
        return sb.toString();
    }

    @DexIgnore
    public static String c(StringBuilder sb, int i, String str, String str2) {
        sb.append(i);
        sb.append(str);
        sb.append(str2);
        return sb.toString();
    }

    @DexIgnore
    public static cl7 d(e60 e60, bl1.b bVar) {
        return hl7.a(bVar, Boolean.valueOf(e60.B0()));
    }

    @DexIgnore
    public static StringBuilder e(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        return sb;
    }
}
