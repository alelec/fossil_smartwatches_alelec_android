package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zs extends fs {
    @DexIgnore
    public long A;
    @DexIgnore
    public long B;
    @DexIgnore
    public long C; // = 15000;
    @DexIgnore
    public float D;
    @DexIgnore
    public /* final */ gp7<tl7> E;
    @DexIgnore
    public /* final */ short F;
    @DexIgnore
    public /* final */ vr G;

    @DexIgnore
    public zs(short s, vr vrVar, k5 k5Var) {
        super(hs.r, k5Var, 0, 4);
        this.F = (short) s;
        this.G = vrVar;
        this.E = new xs(this, k5Var);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(g80.k(super.A(), jd0.I0, Long.valueOf(this.A)), jd0.J0, Long.valueOf(this.B));
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void f(long j) {
        this.C = j;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void s(o7 o7Var) {
        if (o7Var.f2638a == n6.FTC) {
            byte[] bArr = o7Var.b;
            if (bArr.length >= 12) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                byte b = order.get(0);
                if (this.F != order.getShort(1)) {
                    return;
                }
                if (iu.i.a() == b) {
                    B();
                    mw b2 = mw.g.b(ku.i.a(order.get(3)));
                    this.v = mw.a(this.v, null, null, b2.d, null, b2.f, 11);
                    this.A = hy1.o(order.getInt(4));
                    this.B = hy1.o(order.getInt(8));
                    this.g.add(new hw(0, o7Var.f2638a, o7Var.b, g80.k(g80.k(new JSONObject(), jd0.I0, Long.valueOf(this.A)), jd0.J0, Long.valueOf(this.B)), 1));
                    m(this.v);
                    return;
                }
                this.g.add(new hw(0, o7Var.f2638a, o7Var.b, null, 9));
                m(mw.a(this.v, null, null, lw.e, null, null, 27));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void v(u5 u5Var) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        this.v = mw.a(this.v, null, null, mw.g.a(u5Var.e).d, u5Var.e, null, 19);
        a90 a90 = this.f;
        if (a90 != null) {
            a90.j = true;
        }
        a90 a902 = this.f;
        if (!(a902 == null || (jSONObject2 = a902.n) == null)) {
            g80.k(jSONObject2, jd0.k, ey1.a(lw.b));
        }
        lw lwVar = this.v.d;
        lw lwVar2 = lw.b;
        n(this.p);
        a90 a903 = this.f;
        if (!(a903 == null || (jSONObject = a903.n) == null)) {
            g80.k(jSONObject, jd0.q3, Integer.valueOf(this.G.c()));
        }
        float min = Math.min((((float) this.G.c()) * 1.0f) / ((float) this.G.c), 1.0f);
        if (Math.abs(this.D - min) > 0.001f || this.G.c() >= this.G.c) {
            this.D = min;
            e(min);
        }
        q();
    }

    @DexIgnore
    @Override // com.fossil.fs
    public u5 w() {
        if (!(this.G.b.remaining() > 0)) {
            return null;
        }
        byte[] a2 = this.G.a();
        if (this.s) {
            a2 = jx.b.c(this.y.x, this.G.f, a2);
        }
        return new j6(this.G.f, a2, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public long x() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void y() {
        this.C = 30000;
        n(this.E);
        C();
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(super.z(), jd0.A0, hy1.l(this.F, null, 1, null));
    }
}
