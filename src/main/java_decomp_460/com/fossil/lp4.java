package com.fossil;

import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lp4 implements Factory<pu6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f2228a;
    @DexIgnore
    public /* final */ Provider<InAppNotificationRepository> b;

    @DexIgnore
    public lp4(uo4 uo4, Provider<InAppNotificationRepository> provider) {
        this.f2228a = uo4;
        this.b = provider;
    }

    @DexIgnore
    public static lp4 a(uo4 uo4, Provider<InAppNotificationRepository> provider) {
        return new lp4(uo4, provider);
    }

    @DexIgnore
    public static pu6 c(uo4 uo4, InAppNotificationRepository inAppNotificationRepository) {
        pu6 s = uo4.s(inAppNotificationRepository);
        lk7.c(s, "Cannot return null from a non-@Nullable @Provides method");
        return s;
    }

    @DexIgnore
    /* renamed from: b */
    public pu6 get() {
        return c(this.f2228a, this.b.get());
    }
}
