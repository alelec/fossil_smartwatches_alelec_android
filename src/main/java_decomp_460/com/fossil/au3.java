package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class au3<TResult> implements hu3<TResult> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f327a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public ht3<TResult> c;

    @DexIgnore
    public au3(Executor executor, ht3<TResult> ht3) {
        this.f327a = executor;
        this.c = ht3;
    }

    @DexIgnore
    @Override // com.fossil.hu3
    public final void a(nt3<TResult> nt3) {
        synchronized (this.b) {
            if (this.c != null) {
                this.f327a.execute(new zt3(this, nt3));
            }
        }
    }
}
