package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum mo1 {
    UNSUPPORTED((byte) 0, 1),
    INCOMING_CALL((byte) 1, 2),
    TEXT((byte) 2, 4),
    NOTIFICATION((byte) 3, 8),
    EMAIL((byte) 4, 16),
    CALENDAR((byte) 5, 32),
    MISSED_CALL((byte) 6, 64),
    REMOVED((byte) 7, 128);
    
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public mo1(byte b2, int i) {
        this.b = (byte) b2;
        this.c = i;
    }

    @DexIgnore
    public final int a() {
        return this.c;
    }

    @DexIgnore
    public final byte b() {
        return this.b;
    }
}
