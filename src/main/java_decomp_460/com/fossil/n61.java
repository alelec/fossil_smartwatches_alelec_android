package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface n61<T, V> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <T, V> boolean a(n61<T, V> n61, T t) {
            pq7.c(t, "data");
            return true;
        }
    }

    @DexIgnore
    boolean a(T t);

    @DexIgnore
    V b(T t);
}
