package com.fossil;

import android.annotation.SuppressLint;
import android.os.Build;
import android.text.PrecomputedText;
import android.text.Spannable;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.MetricAffectingSpan;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class en0 implements Spannable {
    @DexIgnore
    public /* final */ Spannable b;
    @DexIgnore
    public /* final */ a c;
    @DexIgnore
    public /* final */ PrecomputedText d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ TextPaint f960a;
        @DexIgnore
        public /* final */ TextDirectionHeuristic b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ int d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.en0$a$a")
        /* renamed from: com.fossil.en0$a$a  reason: collision with other inner class name */
        public static class C0067a {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ TextPaint f961a;
            @DexIgnore
            public TextDirectionHeuristic b;
            @DexIgnore
            public int c;
            @DexIgnore
            public int d;

            @DexIgnore
            public C0067a(TextPaint textPaint) {
                this.f961a = textPaint;
                if (Build.VERSION.SDK_INT >= 23) {
                    this.c = 1;
                    this.d = 1;
                } else {
                    this.d = 0;
                    this.c = 0;
                }
                if (Build.VERSION.SDK_INT >= 18) {
                    this.b = TextDirectionHeuristics.FIRSTSTRONG_LTR;
                } else {
                    this.b = null;
                }
            }

            @DexIgnore
            public a a() {
                return new a(this.f961a, this.b, this.c, this.d);
            }

            @DexIgnore
            public C0067a b(int i) {
                this.c = i;
                return this;
            }

            @DexIgnore
            public C0067a c(int i) {
                this.d = i;
                return this;
            }

            @DexIgnore
            public C0067a d(TextDirectionHeuristic textDirectionHeuristic) {
                this.b = textDirectionHeuristic;
                return this;
            }
        }

        @DexIgnore
        public a(PrecomputedText.Params params) {
            this.f960a = params.getTextPaint();
            this.b = params.getTextDirection();
            this.c = params.getBreakStrategy();
            this.d = params.getHyphenationFrequency();
            int i = Build.VERSION.SDK_INT;
        }

        @DexIgnore
        @SuppressLint({"NewApi"})
        public a(TextPaint textPaint, TextDirectionHeuristic textDirectionHeuristic, int i, int i2) {
            if (Build.VERSION.SDK_INT >= 29) {
                new PrecomputedText.Params.Builder(textPaint).setBreakStrategy(i).setHyphenationFrequency(i2).setTextDirection(textDirectionHeuristic).build();
            }
            this.f960a = textPaint;
            this.b = textDirectionHeuristic;
            this.c = i;
            this.d = i2;
        }

        @DexIgnore
        public boolean a(a aVar) {
            if ((Build.VERSION.SDK_INT >= 23 && (this.c != aVar.b() || this.d != aVar.c())) || this.f960a.getTextSize() != aVar.e().getTextSize() || this.f960a.getTextScaleX() != aVar.e().getTextScaleX() || this.f960a.getTextSkewX() != aVar.e().getTextSkewX()) {
                return false;
            }
            if ((Build.VERSION.SDK_INT >= 21 && (this.f960a.getLetterSpacing() != aVar.e().getLetterSpacing() || !TextUtils.equals(this.f960a.getFontFeatureSettings(), aVar.e().getFontFeatureSettings()))) || this.f960a.getFlags() != aVar.e().getFlags()) {
                return false;
            }
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                if (!this.f960a.getTextLocales().equals(aVar.e().getTextLocales())) {
                    return false;
                }
            } else if (i >= 17 && !this.f960a.getTextLocale().equals(aVar.e().getTextLocale())) {
                return false;
            }
            if (this.f960a.getTypeface() == null) {
                return aVar.e().getTypeface() == null;
            }
            if (!this.f960a.getTypeface().equals(aVar.e().getTypeface())) {
                return false;
            }
        }

        @DexIgnore
        public int b() {
            return this.c;
        }

        @DexIgnore
        public int c() {
            return this.d;
        }

        @DexIgnore
        public TextDirectionHeuristic d() {
            return this.b;
        }

        @DexIgnore
        public TextPaint e() {
            return this.f960a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (!a(aVar)) {
                return false;
            }
            return Build.VERSION.SDK_INT < 18 || this.b == aVar.d();
        }

        @DexIgnore
        public int hashCode() {
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                return kn0.b(Float.valueOf(this.f960a.getTextSize()), Float.valueOf(this.f960a.getTextScaleX()), Float.valueOf(this.f960a.getTextSkewX()), Float.valueOf(this.f960a.getLetterSpacing()), Integer.valueOf(this.f960a.getFlags()), this.f960a.getTextLocales(), this.f960a.getTypeface(), Boolean.valueOf(this.f960a.isElegantTextHeight()), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else if (i >= 21) {
                return kn0.b(Float.valueOf(this.f960a.getTextSize()), Float.valueOf(this.f960a.getTextScaleX()), Float.valueOf(this.f960a.getTextSkewX()), Float.valueOf(this.f960a.getLetterSpacing()), Integer.valueOf(this.f960a.getFlags()), this.f960a.getTextLocale(), this.f960a.getTypeface(), Boolean.valueOf(this.f960a.isElegantTextHeight()), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else if (i >= 18) {
                return kn0.b(Float.valueOf(this.f960a.getTextSize()), Float.valueOf(this.f960a.getTextScaleX()), Float.valueOf(this.f960a.getTextSkewX()), Integer.valueOf(this.f960a.getFlags()), this.f960a.getTextLocale(), this.f960a.getTypeface(), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else if (i >= 17) {
                return kn0.b(Float.valueOf(this.f960a.getTextSize()), Float.valueOf(this.f960a.getTextScaleX()), Float.valueOf(this.f960a.getTextSkewX()), Integer.valueOf(this.f960a.getFlags()), this.f960a.getTextLocale(), this.f960a.getTypeface(), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else {
                return kn0.b(Float.valueOf(this.f960a.getTextSize()), Float.valueOf(this.f960a.getTextScaleX()), Float.valueOf(this.f960a.getTextSkewX()), Integer.valueOf(this.f960a.getFlags()), this.f960a.getTypeface(), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            }
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder("{");
            sb.append("textSize=" + this.f960a.getTextSize());
            sb.append(", textScaleX=" + this.f960a.getTextScaleX());
            sb.append(", textSkewX=" + this.f960a.getTextSkewX());
            if (Build.VERSION.SDK_INT >= 21) {
                sb.append(", letterSpacing=" + this.f960a.getLetterSpacing());
                sb.append(", elegantTextHeight=" + this.f960a.isElegantTextHeight());
            }
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                sb.append(", textLocale=" + this.f960a.getTextLocales());
            } else if (i >= 17) {
                sb.append(", textLocale=" + this.f960a.getTextLocale());
            }
            sb.append(", typeface=" + this.f960a.getTypeface());
            if (Build.VERSION.SDK_INT >= 26) {
                sb.append(", variationSettings=" + this.f960a.getFontVariationSettings());
            }
            sb.append(", textDir=" + this.b);
            sb.append(", breakStrategy=" + this.c);
            sb.append(", hyphenationFrequency=" + this.d);
            sb.append("}");
            return sb.toString();
        }
    }

    @DexIgnore
    public a a() {
        return this.c;
    }

    @DexIgnore
    public PrecomputedText b() {
        Spannable spannable = this.b;
        if (spannable instanceof PrecomputedText) {
            return (PrecomputedText) spannable;
        }
        return null;
    }

    @DexIgnore
    public char charAt(int i) {
        return this.b.charAt(i);
    }

    @DexIgnore
    public int getSpanEnd(Object obj) {
        return this.b.getSpanEnd(obj);
    }

    @DexIgnore
    public int getSpanFlags(Object obj) {
        return this.b.getSpanFlags(obj);
    }

    @DexIgnore
    public int getSpanStart(Object obj) {
        return this.b.getSpanStart(obj);
    }

    @DexIgnore
    @Override // android.text.Spanned
    @SuppressLint({"NewApi"})
    public <T> T[] getSpans(int i, int i2, Class<T> cls) {
        return Build.VERSION.SDK_INT >= 29 ? (T[]) this.d.getSpans(i, i2, cls) : (T[]) this.b.getSpans(i, i2, cls);
    }

    @DexIgnore
    public int length() {
        return this.b.length();
    }

    @DexIgnore
    public int nextSpanTransition(int i, int i2, Class cls) {
        return this.b.nextSpanTransition(i, i2, cls);
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public void removeSpan(Object obj) {
        if (obj instanceof MetricAffectingSpan) {
            throw new IllegalArgumentException("MetricAffectingSpan can not be removed from PrecomputedText.");
        } else if (Build.VERSION.SDK_INT >= 29) {
            this.d.removeSpan(obj);
        } else {
            this.b.removeSpan(obj);
        }
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public void setSpan(Object obj, int i, int i2, int i3) {
        if (obj instanceof MetricAffectingSpan) {
            throw new IllegalArgumentException("MetricAffectingSpan can not be set to PrecomputedText.");
        } else if (Build.VERSION.SDK_INT >= 29) {
            this.d.setSpan(obj, i, i2, i3);
        } else {
            this.b.setSpan(obj, i, i2, i3);
        }
    }

    @DexIgnore
    public CharSequence subSequence(int i, int i2) {
        return this.b.subSequence(i, i2);
    }

    @DexIgnore
    public String toString() {
        return this.b.toString();
    }
}
