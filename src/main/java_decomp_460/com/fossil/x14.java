package com.fossil;

import java.io.Serializable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x14 extends i44<Object> implements Serializable {
    @DexIgnore
    public static /* final */ x14 INSTANCE; // = new x14();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    @Override // com.fossil.i44, java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <E> y24<E> immutableSortedCopy(Iterable<E> iterable) {
        return y24.copyOf(iterable);
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <S> i44<S> reverse() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <E> List<E> sortedCopy(Iterable<E> iterable) {
        return t34.h(iterable);
    }

    @DexIgnore
    public String toString() {
        return "Ordering.allEqual()";
    }
}
