package com.fossil;

import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r48 implements c58 {
    @DexIgnore
    public /* final */ InputStream b;
    @DexIgnore
    public /* final */ d58 c;

    @DexIgnore
    public r48(InputStream inputStream, d58 d58) {
        pq7.c(inputStream, "input");
        pq7.c(d58, "timeout");
        this.b = inputStream;
        this.c = d58;
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.c58, java.lang.AutoCloseable
    public void close() {
        this.b.close();
    }

    @DexIgnore
    @Override // com.fossil.c58
    public long d0(i48 i48, long j) {
        pq7.c(i48, "sink");
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i == 0) {
            return 0;
        }
        if (i >= 0) {
            try {
                this.c.f();
                x48 s0 = i48.s0(1);
                int read = this.b.read(s0.f4040a, s0.c, (int) Math.min(j, (long) (8192 - s0.c)));
                if (read == -1) {
                    if (s0.b == s0.c) {
                        i48.b = s0.b();
                        y48.c.a(s0);
                    }
                    return -1;
                }
                s0.c += read;
                long j2 = (long) read;
                i48.o0(i48.p0() + j2);
                return j2;
            } catch (AssertionError e) {
                if (s48.e(e)) {
                    throw new IOException(e);
                }
                throw e;
            }
        } else {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.c58
    public d58 e() {
        return this.c;
    }

    @DexIgnore
    public String toString() {
        return "source(" + this.b + ')';
    }
}
