package com.fossil;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cv0;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class i67 extends cv0.i {
    @DexIgnore
    public i67(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.cv0.f
    public void u(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
        pq7.c(canvas, "c");
        pq7.c(recyclerView, "recyclerView");
        pq7.c(viewHolder, "viewHolder");
        super.u(canvas, recyclerView, viewHolder, f, f2, i, z);
        View view = viewHolder.itemView;
        pq7.b(view, "viewHolder.itemView");
        ColorDrawable colorDrawable = new ColorDrawable(-65536);
        float f3 = (float) 0;
        if (f < f3) {
            colorDrawable.setBounds(view.getRight() + Math.round(f) + 0, view.getTop(), view.getRight(), view.getBottom());
        } else {
            colorDrawable.setBounds(0, 0, 0, 0);
        }
        colorDrawable.draw(canvas);
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize((float) PortfolioApp.h0.c().getResources().getDimensionPixelSize(2131165722));
        float f4 = (float) 2;
        float bottom = ((float) (view.getBottom() + view.getTop())) / f4;
        String c = um5.c(PortfolioApp.h0.c(), 2131887379);
        pq7.b(c, "LanguageHelper.getString\u2026ute_time_app_CTA__Delete)");
        if (c != null) {
            String upperCase = c.toUpperCase();
            pq7.b(upperCase, "(this as java.lang.String).toUpperCase()");
            canvas.drawText(upperCase, ((((float) view.getRight()) + f) - f3) + ((float) PortfolioApp.h0.c().getResources().getDimensionPixelSize(2131165400)), bottom - ((paint.descent() + paint.ascent()) / f4), paint);
            return;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.cv0.f
    public boolean y(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
        pq7.c(recyclerView, "recyclerView");
        pq7.c(viewHolder, "viewHolder");
        pq7.c(viewHolder2, "target");
        return false;
    }
}
