package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lh1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ bb1 f2193a;
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public /* final */ List<b> c;
    @DexIgnore
    public /* final */ wa1 d;
    @DexIgnore
    public /* final */ rd1 e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public va1<Bitmap> i;
    @DexIgnore
    public a j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public a l;
    @DexIgnore
    public Bitmap m;
    @DexIgnore
    public a n;
    @DexIgnore
    public d o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends lj1<Bitmap> {
        @DexIgnore
        public /* final */ Handler e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ long g;
        @DexIgnore
        public Bitmap h;

        @DexIgnore
        public a(Handler handler, int i, long j) {
            this.e = handler;
            this.f = i;
            this.g = j;
        }

        @DexIgnore
        public Bitmap c() {
            return this.h;
        }

        @DexIgnore
        /* renamed from: e */
        public void b(Bitmap bitmap, tj1<? super Bitmap> tj1) {
            this.h = bitmap;
            this.e.sendMessageAtTime(this.e.obtainMessage(1, this), this.g);
        }

        @DexIgnore
        @Override // com.fossil.qj1
        public void j(Drawable drawable) {
            this.h = null;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Handler.Callback {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                lh1.this.m((a) message.obj);
                return true;
            }
            if (i == 2) {
                lh1.this.d.l((a) message.obj);
            }
            return false;
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore
    public lh1(oa1 oa1, bb1 bb1, int i2, int i3, sb1<Bitmap> sb1, Bitmap bitmap) {
        this(oa1.f(), oa1.t(oa1.h()), bb1, null, i(oa1.t(oa1.h()), i2, i3), sb1, bitmap);
    }

    @DexIgnore
    public lh1(rd1 rd1, wa1 wa1, bb1 bb1, Handler handler, va1<Bitmap> va1, sb1<Bitmap> sb1, Bitmap bitmap) {
        this.c = new ArrayList();
        this.d = wa1;
        handler = handler == null ? new Handler(Looper.getMainLooper(), new c()) : handler;
        this.e = rd1;
        this.b = handler;
        this.i = va1;
        this.f2193a = bb1;
        o(sb1, bitmap);
    }

    @DexIgnore
    public static mb1 g() {
        return new yj1(Double.valueOf(Math.random()));
    }

    @DexIgnore
    public static va1<Bitmap> i(wa1 wa1, int i2, int i3) {
        return wa1.e().d(((fj1) ((fj1) fj1.u0(wc1.f3916a).s0(true)).m0(true)).b0(i2, i3));
    }

    @DexIgnore
    public void a() {
        this.c.clear();
        n();
        q();
        a aVar = this.j;
        if (aVar != null) {
            this.d.l(aVar);
            this.j = null;
        }
        a aVar2 = this.l;
        if (aVar2 != null) {
            this.d.l(aVar2);
            this.l = null;
        }
        a aVar3 = this.n;
        if (aVar3 != null) {
            this.d.l(aVar3);
            this.n = null;
        }
        this.f2193a.clear();
        this.k = true;
    }

    @DexIgnore
    public ByteBuffer b() {
        return this.f2193a.f().asReadOnlyBuffer();
    }

    @DexIgnore
    public Bitmap c() {
        a aVar = this.j;
        return aVar != null ? aVar.c() : this.m;
    }

    @DexIgnore
    public int d() {
        a aVar = this.j;
        if (aVar != null) {
            return aVar.f;
        }
        return -1;
    }

    @DexIgnore
    public Bitmap e() {
        return this.m;
    }

    @DexIgnore
    public int f() {
        return this.f2193a.c();
    }

    @DexIgnore
    public int h() {
        return this.r;
    }

    @DexIgnore
    public int j() {
        return this.f2193a.i() + this.p;
    }

    @DexIgnore
    public int k() {
        return this.q;
    }

    @DexIgnore
    public final void l() {
        if (this.f && !this.g) {
            if (this.h) {
                ik1.a(this.n == null, "Pending target must be null when starting from the first frame");
                this.f2193a.g();
                this.h = false;
            }
            a aVar = this.n;
            if (aVar != null) {
                this.n = null;
                m(aVar);
                return;
            }
            this.g = true;
            int d2 = this.f2193a.d();
            long uptimeMillis = SystemClock.uptimeMillis();
            this.f2193a.b();
            this.l = new a(this.b, this.f2193a.h(), ((long) d2) + uptimeMillis);
            this.i.d(fj1.v0(g())).M0(this.f2193a).C0(this.l);
        }
    }

    @DexIgnore
    public void m(a aVar) {
        d dVar = this.o;
        if (dVar != null) {
            dVar.a();
        }
        this.g = false;
        if (this.k) {
            this.b.obtainMessage(2, aVar).sendToTarget();
        } else if (!this.f) {
            this.n = aVar;
        } else {
            if (aVar.c() != null) {
                n();
                a aVar2 = this.j;
                this.j = aVar;
                for (int size = this.c.size() - 1; size >= 0; size--) {
                    this.c.get(size).a();
                }
                if (aVar2 != null) {
                    this.b.obtainMessage(2, aVar2).sendToTarget();
                }
            }
            l();
        }
    }

    @DexIgnore
    public final void n() {
        Bitmap bitmap = this.m;
        if (bitmap != null) {
            this.e.b(bitmap);
            this.m = null;
        }
    }

    @DexIgnore
    public void o(sb1<Bitmap> sb1, Bitmap bitmap) {
        ik1.d(sb1);
        ik1.d(bitmap);
        this.m = bitmap;
        this.i = this.i.d(new fj1().o0(sb1));
        this.p = jk1.h(bitmap);
        this.q = bitmap.getWidth();
        this.r = bitmap.getHeight();
    }

    @DexIgnore
    public final void p() {
        if (!this.f) {
            this.f = true;
            this.k = false;
            l();
        }
    }

    @DexIgnore
    public final void q() {
        this.f = false;
    }

    @DexIgnore
    public void r(b bVar) {
        if (this.k) {
            throw new IllegalStateException("Cannot subscribe to a cleared frame loader");
        } else if (!this.c.contains(bVar)) {
            boolean isEmpty = this.c.isEmpty();
            this.c.add(bVar);
            if (isEmpty) {
                p();
            }
        } else {
            throw new IllegalStateException("Cannot subscribe twice in a row");
        }
    }

    @DexIgnore
    public void s(b bVar) {
        this.c.remove(bVar);
        if (this.c.isEmpty()) {
            q();
        }
    }
}
