package com.fossil;

import android.bluetooth.BluetoothGattCharacteristic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j5 extends qq7 implements rp7<BluetoothGattCharacteristic, String> {
    @DexIgnore
    public static /* final */ j5 b; // = new j5();

    @DexIgnore
    public j5() {
        super(1);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public String invoke(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        BluetoothGattCharacteristic bluetoothGattCharacteristic2 = bluetoothGattCharacteristic;
        StringBuilder sb = new StringBuilder();
        sb.append("    ");
        pq7.b(bluetoothGattCharacteristic2, "characteristic");
        sb.append(bluetoothGattCharacteristic2.getUuid().toString());
        return sb.toString();
    }
}
