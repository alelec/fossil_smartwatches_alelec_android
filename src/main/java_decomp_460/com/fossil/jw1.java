package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum jw1 {
    SYSTEM((byte) 0),
    THEME((byte) 1),
    WATCH_APP((byte) 2);
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final jw1 a(byte b) {
            jw1[] values = jw1.values();
            for (jw1 jw1 : values) {
                if (jw1.a() == b) {
                    return jw1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public jw1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
