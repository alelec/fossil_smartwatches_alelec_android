package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.f57;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xw4 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<xs4> f4193a; // = new ArrayList();
    @DexIgnore
    public b b;
    @DexIgnore
    public /* final */ f57.b c;
    @DexIgnore
    public /* final */ uy4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ pe5 f4194a;
        @DexIgnore
        public /* final */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ xw4 c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xw4$a$a")
        /* renamed from: com.fossil.xw4$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0291a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;
            @DexIgnore
            public /* final */ /* synthetic */ xs4 c;

            @DexIgnore
            public View$OnClickListenerC0291a(a aVar, xs4 xs4, f57.b bVar, uy4 uy4) {
                this.b = aVar;
                this.c = xs4;
            }

            @DexIgnore
            public final void onClick(View view) {
                b bVar;
                if (this.b.getAdapterPosition() != -1 && (bVar = this.b.c.b) != null) {
                    bVar.a(this.c, this.b.getAdapterPosition());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(xw4 xw4, pe5 pe5, View view) {
            super(view);
            pq7.c(pe5, "binding");
            pq7.c(view, "root");
            this.c = xw4;
            this.f4194a = pe5;
            this.b = view;
        }

        @DexIgnore
        public void a(xs4 xs4, f57.b bVar, uy4 uy4) {
            pq7.c(xs4, "item");
            pq7.c(bVar, "drawableBuilder");
            pq7.c(uy4, "colorGenerator");
            pe5 pe5 = this.f4194a;
            String b2 = hz4.f1561a.b(xs4.b(), xs4.e(), xs4.i());
            FlexibleTextView flexibleTextView = pe5.u;
            pq7.b(flexibleTextView, "tvFullName");
            flexibleTextView.setText(b2);
            FlexibleTextView flexibleTextView2 = pe5.v;
            pq7.b(flexibleTextView2, "tvSocial");
            flexibleTextView2.setText(xs4.i());
            ImageView imageView = pe5.s;
            pq7.b(imageView, "ivAvatar");
            ty4.b(imageView, xs4.h(), b2, bVar, uy4);
            pe5.r.setOnClickListener(new View$OnClickListenerC0291a(this, xs4, bVar, uy4));
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(xs4 xs4, int i);
    }

    @DexIgnore
    public xw4() {
        f57.b f = f57.a().f();
        pq7.b(f, "TextDrawable.builder().round()");
        this.c = f;
        this.d = uy4.d.b();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f4193a.size();
    }

    @DexIgnore
    public final void h() {
        if (this.f4193a.size() != 0) {
            this.f4193a.clear();
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    /* renamed from: i */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        aVar.a(this.f4193a.get(i), this.c, this.d);
    }

    @DexIgnore
    /* renamed from: j */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        pe5 z = pe5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemFriendSearchBinding.\u2026tInflater, parent, false)");
        View n = z.n();
        pq7.b(n, "itemFriendSearchBinding.root");
        return new a(this, z, n);
    }

    @DexIgnore
    public final void k(b bVar) {
        pq7.c(bVar, "listener");
        this.b = bVar;
    }

    @DexIgnore
    public final void l(List<xs4> list) {
        pq7.c(list, "friendList");
        if (!pq7.a(this.f4193a, list)) {
            this.f4193a.clear();
            this.f4193a.addAll(list);
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    public final void m(int i) {
        if (i != -1) {
            this.f4193a.remove(i);
            notifyItemRemoved(i);
        }
    }
}
