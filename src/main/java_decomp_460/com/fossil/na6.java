package com.fossil;

import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class na6 extends ts0 {
    @DexIgnore
    public static /* final */ String f;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Gson f2493a; // = new Gson();
    @DexIgnore
    public CommuteTimeWatchAppSetting b;
    @DexIgnore
    public MFUser c;
    @DexIgnore
    public MutableLiveData<a> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ UserRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public List<AddressWrapper> f2494a;

        @DexIgnore
        public a(List<AddressWrapper> list) {
            this.f2494a = list;
        }

        @DexIgnore
        public final List<AddressWrapper> a() {
            return this.f2494a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || ((obj instanceof a) && pq7.a(this.f2494a, ((a) obj).f2494a));
        }

        @DexIgnore
        public int hashCode() {
            List<AddressWrapper> list = this.f2494a;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(addresses=" + this.f2494a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel$start$1", f = "CommuteTimeWatchAppSettingsViewModel.kt", l = {63}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ na6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel$start$1$1", f = "CommuteTimeWatchAppSettingsViewModel.kt", l = {63}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.e;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(na6 na6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = na6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            na6 na6;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                na6 na62 = this.this$0;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.L$1 = na62;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
                na6 = na62;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
                na6 = (na6) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            na6.c = (MFUser) g;
            na6 na63 = this.this$0;
            CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = na63.b;
            na63.e(commuteTimeWatchAppSetting != null ? commuteTimeWatchAppSetting.getAddresses() : null);
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = na6.class.getSimpleName();
        pq7.b(simpleName, "CommuteTimeWatchAppSetti\u2026el::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public na6(on5 on5, UserRepository userRepository) {
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(userRepository, "mUserRepository");
        this.e = userRepository;
    }

    @DexIgnore
    public final void e(List<AddressWrapper> list) {
        this.d.l(new a(list));
    }

    @DexIgnore
    public final CommuteTimeWatchAppSetting f() {
        return this.b;
    }

    @DexIgnore
    public final CommuteTimeWatchAppSetting g() {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting(new ArrayList());
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.b;
        if (commuteTimeWatchAppSetting2 != null) {
            List<AddressWrapper> addresses = commuteTimeWatchAppSetting2.getAddresses();
            ArrayList arrayList = new ArrayList();
            for (T t : addresses) {
                if (!TextUtils.isEmpty(t.getAddress())) {
                    arrayList.add(t);
                }
            }
            commuteTimeWatchAppSetting.setAddresses(pm7.j0(arrayList));
        }
        return commuteTimeWatchAppSetting;
    }

    @DexIgnore
    public final MutableLiveData<a> h() {
        return this.d;
    }

    @DexIgnore
    public final void i(String str) {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = f;
        local.d(str2, "init with setting " + str);
        try {
            commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) this.f2493a.k(str, CommuteTimeWatchAppSetting.class);
            List<AddressWrapper> addresses = commuteTimeWatchAppSetting.getAddresses();
            ArrayList arrayList = new ArrayList();
            for (T t : addresses) {
                if (t.getType() == AddressWrapper.AddressType.HOME) {
                    arrayList.add(t);
                }
            }
            List<AddressWrapper> addresses2 = commuteTimeWatchAppSetting.getAddresses();
            ArrayList arrayList2 = new ArrayList();
            for (T t2 : addresses2) {
                if (t2.getType() == AddressWrapper.AddressType.WORK) {
                    arrayList2.add(t2);
                }
            }
            if (arrayList.isEmpty()) {
                commuteTimeWatchAppSetting.getAddresses().add(0, new AddressWrapper(AddressWrapper.AddressType.HOME.getValue(), AddressWrapper.AddressType.HOME));
            }
            if (arrayList2.isEmpty()) {
                commuteTimeWatchAppSetting.getAddresses().add(1, new AddressWrapper(AddressWrapper.AddressType.WORK.getValue(), AddressWrapper.AddressType.WORK));
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = f;
            local2.d(str3, "exception when parse commute time setting " + e2);
            commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting(null, 1, null);
        }
        this.b = commuteTimeWatchAppSetting;
        if (commuteTimeWatchAppSetting == null) {
            this.b = new CommuteTimeWatchAppSetting(null, 1, null);
        }
    }

    @DexIgnore
    public final boolean j() {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.b;
        return commuteTimeWatchAppSetting != null && commuteTimeWatchAppSetting.getAddresses().size() < 10;
    }

    @DexIgnore
    public final void k(AddressWrapper addressWrapper) {
        FLogger.INSTANCE.getLocal().d(f, "onUserChooseAddress " + addressWrapper);
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.b;
        if (!(commuteTimeWatchAppSetting == null || addressWrapper == null)) {
            if (commuteTimeWatchAppSetting != null) {
                int i = 0;
                int i2 = -1;
                for (T t : commuteTimeWatchAppSetting.getAddresses()) {
                    if (i >= 0) {
                        int i3 = pq7.a(addressWrapper.getId(), t.getId()) ? i : i2;
                        i++;
                        i2 = i3;
                    } else {
                        hm7.l();
                        throw null;
                    }
                }
                if (i2 != -1) {
                    CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.b;
                    if (commuteTimeWatchAppSetting2 != null) {
                        commuteTimeWatchAppSetting2.getAddresses().remove(i2);
                        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting3 = this.b;
                        if (commuteTimeWatchAppSetting3 != null) {
                            commuteTimeWatchAppSetting3.getAddresses().add(i2, addressWrapper);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    CommuteTimeWatchAppSetting commuteTimeWatchAppSetting4 = this.b;
                    if (commuteTimeWatchAppSetting4 != null) {
                        commuteTimeWatchAppSetting4.getAddresses().add(addressWrapper);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting5 = this.b;
                e(commuteTimeWatchAppSetting5 != null ? commuteTimeWatchAppSetting5.getAddresses() : null);
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void l(AddressWrapper addressWrapper) {
        List<AddressWrapper> list = null;
        if (addressWrapper != null) {
            if (addressWrapper.getType() == AddressWrapper.AddressType.OTHER) {
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.b;
                if (commuteTimeWatchAppSetting != null) {
                    commuteTimeWatchAppSetting.getAddresses().remove(addressWrapper);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.b;
                if (commuteTimeWatchAppSetting2 != null) {
                    commuteTimeWatchAppSetting2.getAddresses();
                    if (pq7.a(addressWrapper.getId(), addressWrapper.getId())) {
                        addressWrapper.setAddress("");
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            CommuteTimeWatchAppSetting commuteTimeWatchAppSetting3 = this.b;
            if (commuteTimeWatchAppSetting3 != null) {
                list = commuteTimeWatchAppSetting3.getAddresses();
            }
            e(list);
        }
    }

    @DexIgnore
    public final void m() {
        xw7 unused = gu7.d(us0.a(this), null, null, new b(this, null), 3, null);
    }
}
