package com.fossil;

import android.os.Build;
import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zo0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f4503a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends AccessibilityNodeProvider {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ zo0 f4504a;

        @DexIgnore
        public a(zo0 zo0) {
            this.f4504a = zo0;
        }

        @DexIgnore
        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            yo0 a2 = this.f4504a.a(i);
            if (a2 == null) {
                return null;
            }
            return a2.D0();
        }

        @DexIgnore
        @Override // android.view.accessibility.AccessibilityNodeProvider
        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            List<yo0> b = this.f4504a.b(str, i);
            if (b == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            int size = b.size();
            for (int i2 = 0; i2 < size; i2++) {
                arrayList.add(b.get(i2).D0());
            }
            return arrayList;
        }

        @DexIgnore
        public boolean performAction(int i, int i2, Bundle bundle) {
            return this.f4504a.e(i, i2, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends a {
        @DexIgnore
        public b(zo0 zo0) {
            super(zo0);
        }

        @DexIgnore
        public AccessibilityNodeInfo findFocus(int i) {
            yo0 c = this.f4504a.c(i);
            if (c == null) {
                return null;
            }
            return c.D0();
        }
    }

    @DexIgnore
    public zo0() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            this.f4503a = new b(this);
        } else if (i >= 16) {
            this.f4503a = new a(this);
        } else {
            this.f4503a = null;
        }
    }

    @DexIgnore
    public zo0(Object obj) {
        this.f4503a = obj;
    }

    @DexIgnore
    public yo0 a(int i) {
        return null;
    }

    @DexIgnore
    public List<yo0> b(String str, int i) {
        return null;
    }

    @DexIgnore
    public yo0 c(int i) {
        return null;
    }

    @DexIgnore
    public Object d() {
        return this.f4503a;
    }

    @DexIgnore
    public boolean e(int i, int i2, Bundle bundle) {
        return false;
    }
}
