package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hq1 extends lq1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ bv1 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<hq1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public hq1 createFromParcel(Parcel parcel) {
            return new hq1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public hq1[] newArray(int i) {
            return new hq1[i];
        }
    }

    @DexIgnore
    public hq1(byte b, bv1 bv1) {
        super(np1.COMMUTE_TIME_TRAVEL_MICRO_APP, b, bv1);
        this.f = bv1;
    }

    @DexIgnore
    public /* synthetic */ hq1(Parcel parcel, kq7 kq7) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(bv1.class.getClassLoader());
        if (readParcelable != null) {
            this.f = (bv1) readParcelable;
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1, com.fossil.lq1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(hq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.f, ((hq1) obj).f) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.CommuteTimeTravelMicroAppRequest");
    }

    @DexIgnore
    public final bv1 getCommuteTimeTravelMicroAppEvent() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1, com.fossil.lq1
    public int hashCode() {
        return (super.hashCode() * 31) + this.f.hashCode();
    }
}
