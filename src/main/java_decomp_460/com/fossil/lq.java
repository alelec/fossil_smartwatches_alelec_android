package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lq {
    @DexIgnore
    public /* synthetic */ lq(kq7 kq7) {
    }

    @DexIgnore
    public final zq a(mw mwVar) {
        switch (xp.f4154a[mwVar.d.ordinal()]) {
            case 1:
                return zq.SUCCESS;
            case 2:
                return zq.INTERRUPTED;
            case 3:
                return zq.CONNECTION_DROPPED;
            case 4:
                return zq.REQUEST_UNSUPPORTED;
            case 5:
                return zq.UNSUPPORTED_FILE_HANDLE;
            case 6:
                return zq.BLUETOOTH_OFF;
            case 7:
                return zq.REQUEST_UNSUPPORTED;
            case 8:
                return zq.HID_INPUT_DEVICE_DISABLED;
            case 9:
                return mwVar.f == ku.g ? zq.UNSUPPORTED_FILE_HANDLE : zq.REQUEST_ERROR;
            default:
                return zq.REQUEST_ERROR;
        }
    }
}
