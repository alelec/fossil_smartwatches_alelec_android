package com.fossil;

import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r9 extends vx1<jo1[], jo1[]> {
    @DexIgnore
    public static /* final */ qx1<jo1[]>[] b; // = {new l9(), new o9()};
    @DexIgnore
    public static /* final */ rx1<jo1[]>[] c; // = new rx1[0];
    @DexIgnore
    public static /* final */ r9 d; // = new r9();

    @DexIgnore
    @Override // com.fossil.vx1
    public qx1<jo1[]>[] b() {
        return b;
    }

    @DexIgnore
    @Override // com.fossil.vx1
    public rx1<jo1[]>[] c() {
        return c;
    }

    @DexIgnore
    public final byte[] h(jo1[] jo1Arr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (jo1 jo1 : jo1Arr) {
            byteArrayOutputStream.write(jo1.a());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        pq7.b(byteArray, "entriesData.toByteArray()");
        return byteArray;
    }
}
