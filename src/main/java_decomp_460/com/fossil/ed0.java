package com.fossil;

import a.b.a.h.c.b;
import a.b.a.h.c.c;
import java.io.File;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ed0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Retrofit.b f920a;
    @DexIgnore
    public Cache b;
    @DexIgnore
    public /* final */ OkHttpClient.b c; // = new OkHttpClient.b();

    @DexIgnore
    public ed0() {
        Retrofit.b bVar = new Retrofit.b();
        zi4 zi4 = new zi4();
        zi4.g("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        zi4.f(ry1.class, new c());
        zi4.f(ec0.class, new b());
        bVar.a(GsonConverterFactory.g(zi4.d()));
        this.f920a = bVar;
    }

    @DexIgnore
    public final ed0 a(File file) {
        if (this.b == null) {
            File file2 = new File(file.getAbsolutePath(), "cacheResponse");
            if (!file2.exists()) {
                file2.mkdir();
            }
            this.b = new Cache(file2, 10485760);
        }
        this.c.e(this.b);
        return this;
    }

    @DexIgnore
    public final ed0 b(String str) {
        this.f920a.b(str);
        return this;
    }

    @DexIgnore
    public final ed0 c(Interceptor interceptor) {
        if (interceptor != null && !this.c.j().contains(interceptor)) {
            this.c.a(interceptor);
        }
        return this;
    }

    @DexIgnore
    public final <S> S d(Class<S> cls) {
        this.c.m(10, TimeUnit.SECONDS);
        this.c.g(10, TimeUnit.SECONDS);
        this.c.n(10, TimeUnit.SECONDS);
        Retrofit.b bVar = this.f920a;
        bVar.f(this.c.d());
        return (S) bVar.d().b(cls);
    }
}
