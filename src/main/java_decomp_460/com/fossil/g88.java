package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g88 extends RuntimeException {
    @DexIgnore
    public /* final */ transient q88<?> b;
    @DexIgnore
    public /* final */ int code;
    @DexIgnore
    public /* final */ String message;

    @DexIgnore
    public g88(q88<?> q88) {
        super(a(q88));
        this.code = q88.b();
        this.message = q88.f();
        this.b = q88;
    }

    @DexIgnore
    public static String a(q88<?> q88) {
        u88.b(q88, "response == null");
        return "HTTP " + q88.b() + " " + q88.f();
    }

    @DexIgnore
    public int code() {
        return this.code;
    }

    @DexIgnore
    public String message() {
        return this.message;
    }

    @DexIgnore
    public q88<?> response() {
        return this.b;
    }
}
