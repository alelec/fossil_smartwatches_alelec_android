package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j94 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f1730a;

    @DexIgnore
    public static String b(Context context) {
        String installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
        return installerPackageName == null ? "" : installerPackageName;
    }

    @DexIgnore
    public String a(Context context) {
        String str;
        synchronized (this) {
            if (this.f1730a == null) {
                this.f1730a = b(context);
            }
            str = "".equals(this.f1730a) ? null : this.f1730a;
        }
        return str;
    }
}
