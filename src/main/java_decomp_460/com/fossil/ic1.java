package com.fossil;

import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ic1 {
    @DexIgnore
    public boolean a(File file) {
        return file.exists();
    }

    @DexIgnore
    public File b(String str) {
        return new File(str);
    }

    @DexIgnore
    public long c(File file) {
        return file.length();
    }
}
