package com.fossil;

import android.util.SparseArray;
import androidx.renderscript.RenderScript;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bw0 extends uv0 {
    @DexIgnore
    public boolean d; // = false;

    @DexIgnore
    public bw0(long j, RenderScript renderScript) {
        super(j, renderScript);
        new SparseArray();
        new SparseArray();
        new SparseArray();
    }

    @DexIgnore
    public void f(int i, tv0 tv0, tv0 tv02, wv0 wv0) {
        long j = 0;
        if (tv0 == null && tv02 == null) {
            throw new yv0("At least one of ain or aout is required to be non-null.");
        }
        long c = tv0 != null ? tv0.c(this.c) : 0;
        if (tv02 != null) {
            j = tv02.c(this.c);
        }
        if (wv0 != null) {
            wv0.a();
            throw null;
        } else if (this.d) {
            long g = g(tv0);
            long g2 = g(tv02);
            RenderScript renderScript = this.c;
            renderScript.B(c(renderScript), i, g, g2, null, this.d);
        } else {
            RenderScript renderScript2 = this.c;
            renderScript2.B(c(renderScript2), i, c, j, null, this.d);
        }
    }

    @DexIgnore
    public long g(tv0 tv0) {
        if (tv0 == null) {
            return 0;
        }
        ew0 l = tv0.l();
        long h = l.h(this.c, l.i().p(this.c));
        int j = l.j();
        int o = l.i().o();
        RenderScript renderScript = this.c;
        long t = renderScript.t(tv0.c(renderScript), h, j * o);
        tv0.n(t);
        return t;
    }

    @DexIgnore
    public void h(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public void i(int i, float f) {
        RenderScript renderScript = this.c;
        renderScript.D(c(renderScript), i, f, this.d);
    }

    @DexIgnore
    public void j(int i, uv0 uv0) {
        long j = 0;
        if (this.d) {
            long g = g((tv0) uv0);
            RenderScript renderScript = this.c;
            long c = c(renderScript);
            if (uv0 != null) {
                j = g;
            }
            renderScript.E(c, i, j, this.d);
            return;
        }
        RenderScript renderScript2 = this.c;
        long c2 = c(renderScript2);
        if (uv0 != null) {
            j = uv0.c(this.c);
        }
        renderScript2.E(c2, i, j, this.d);
    }
}
