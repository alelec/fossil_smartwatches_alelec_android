package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class za2 implements r92 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ ya2 f4443a;

    @DexIgnore
    public za2(ya2 ya2) {
        this.f4443a = ya2;
    }

    @DexIgnore
    public /* synthetic */ za2(ya2 ya2, xa2 xa2) {
        this(ya2);
    }

    @DexIgnore
    @Override // com.fossil.r92
    public final void a(z52 z52) {
        this.f4443a.s.lock();
        try {
            this.f4443a.l = z52;
            this.f4443a.C();
        } finally {
            this.f4443a.s.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.r92
    public final void b(Bundle bundle) {
        this.f4443a.s.lock();
        try {
            this.f4443a.l = z52.f;
            this.f4443a.C();
        } finally {
            this.f4443a.s.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.r92
    public final void c(int i, boolean z) {
        this.f4443a.s.lock();
        try {
            if (this.f4443a.m) {
                this.f4443a.m = false;
                this.f4443a.o(i, z);
                return;
            }
            this.f4443a.m = true;
            this.f4443a.e.d(i);
            this.f4443a.s.unlock();
        } finally {
            this.f4443a.s.unlock();
        }
    }
}
