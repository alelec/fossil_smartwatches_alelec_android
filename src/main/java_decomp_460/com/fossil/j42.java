package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j42 extends q62<GoogleSignInOptions> {
    @DexIgnore
    public static int j; // = a.f1708a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ int f1708a; // = 1;
        @DexIgnore
        public static /* final */ int b; // = 2;
        @DexIgnore
        public static /* final */ int c; // = 3;
        @DexIgnore
        public static /* final */ int d; // = 4;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] e; // = {1, 2, 3, 4};

        @DexIgnore
        public static int[] a() {
            return (int[]) e.clone();
        }
    }

    @DexIgnore
    public j42(Activity activity, GoogleSignInOptions googleSignInOptions) {
        super(activity, d42.e, googleSignInOptions, (u72) new f72());
    }

    @DexIgnore
    public j42(Context context, GoogleSignInOptions googleSignInOptions) {
        super(context, d42.e, googleSignInOptions, new f72());
    }

    @DexIgnore
    public Intent s() {
        Context k = k();
        int i = p52.f2780a[u() - 1];
        return i != 1 ? i != 2 ? v42.g(k, (GoogleSignInOptions) j()) : v42.b(k, (GoogleSignInOptions) j()) : v42.e(k, (GoogleSignInOptions) j());
    }

    @DexIgnore
    public nt3<Void> t() {
        return qc2.c(v42.c(b(), k(), u() == a.c));
    }

    @DexIgnore
    public final int u() {
        int i;
        synchronized (this) {
            if (j == a.f1708a) {
                Context k = k();
                c62 q = c62.q();
                int j2 = q.j(k, h62.f1430a);
                if (j2 == 0) {
                    j = a.d;
                } else if (q.d(k, j2, null) != null || DynamiteModule.a(k, "com.google.android.gms.auth.api.fallback") == 0) {
                    j = a.b;
                } else {
                    j = a.c;
                }
            }
            i = j;
        }
        return i;
    }
}
