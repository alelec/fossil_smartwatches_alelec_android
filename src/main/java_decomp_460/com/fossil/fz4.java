package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fz4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<View, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ rp7 $onSafeClick;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(rp7 rp7) {
            super(1);
            this.$onSafeClick = rp7;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(View view) {
            invoke(view);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(View view) {
            this.$onSafeClick.invoke(view);
        }
    }

    @DexIgnore
    public static final void a(View view, rp7<? super View, tl7> rp7) {
        pq7.c(view, "$this$setOnSafeClickListener");
        pq7.c(rp7, "onSafeClick");
        view.setOnClickListener(new ez4(0, new a(rp7), 1, null));
    }
}
