package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class f95 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleEditText q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ View u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ RecyclerView w;

    @DexIgnore
    public f95(Object obj, View view, int i, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, ImageView imageView, ImageView imageView2, View view2, ConstraintLayout constraintLayout, RecyclerView recyclerView) {
        super(obj, view, i);
        this.q = flexibleEditText;
        this.r = flexibleTextView;
        this.s = imageView;
        this.t = imageView2;
        this.u = view2;
        this.v = constraintLayout;
        this.w = recyclerView;
    }
}
