package com.fossil;

import com.facebook.internal.Utility;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kx1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ kx1 f2108a; // = new kx1();

    @DexIgnore
    public final String a(byte[] bArr) {
        pq7.c(bArr, "data");
        try {
            MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
            pq7.b(instance, "MessageDigest.getInstance(\"MD5\")");
            instance.update(bArr, 0, bArr.length);
            String bigInteger = new BigInteger(1, instance.digest()).toString(16);
            hr7 hr7 = hr7.f1520a;
            String format = String.format("%32s", Arrays.copyOf(new Object[]{bigInteger}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            return vt7.p(format, ' ', '0', false, 4, null);
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }
}
