package com.fossil;

import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX WARN: Init of enum d can be incorrect */
/* JADX WARN: Init of enum e can be incorrect */
/* JADX WARN: Init of enum f can be incorrect */
/* JADX WARN: Init of enum g can be incorrect */
/* JADX WARN: Init of enum h can be incorrect */
/* JADX WARN: Init of enum i can be incorrect */
/* JADX WARN: Init of enum j can be incorrect */
/* JADX WARN: Init of enum l can be incorrect */
/* JADX WARN: Init of enum n can be incorrect */
/* JADX WARN: Init of enum o can be incorrect */
public enum n6 {
    UNKNOWN("unknown", r0),
    MODEL_NUMBER("model_number", r0),
    SERIAL_NUMBER(Constants.SERIAL_NUMBER, r0),
    FIRMWARE_VERSION(Constants.FIRMWARE_VERSION, r0),
    SOFTWARE_REVISION("software_revision", r0),
    DC("device_config", r0),
    FTC("file_transfer_control", r0),
    FTD("file_transfer_data", (byte) 0),
    ASYNC("async", r0),
    FTD_1("file_transfer_data_1", (byte) 1),
    AUTHENTICATION("authentication", r0),
    HEART_RATE("heart_rate", r0);
    
    @DexIgnore
    public static /* final */ l6 q; // = new l6(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ byte c;

    /*
    static {
        byte b2 = (byte) 255;
    }
    */

    @DexIgnore
    public n6(String str, byte b2) {
        this.b = str;
        this.c = (byte) b2;
    }

    @DexIgnore
    public final ow a() {
        switch (m6.f2306a[ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
                return ow.DEVICE_INFORMATION;
            case 5:
                return ow.DEVICE_CONFIG;
            case 6:
                return ow.FILE_CONFIG;
            case 7:
            case 8:
                return ow.TRANSFER_DATA;
            case 9:
                return ow.AUTHENTICATION;
            case 10:
                return ow.ASYNC;
            default:
                return ow.UNKNOWN;
        }
    }
}
