package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface mg1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements mg1 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ dc1 f2375a;
        @DexIgnore
        public /* final */ od1 b;
        @DexIgnore
        public /* final */ List<ImageHeaderParser> c;

        @DexIgnore
        public a(InputStream inputStream, List<ImageHeaderParser> list, od1 od1) {
            ik1.d(od1);
            this.b = od1;
            ik1.d(list);
            this.c = list;
            this.f2375a = new dc1(inputStream, od1);
        }

        @DexIgnore
        @Override // com.fossil.mg1
        public int a() throws IOException {
            return lb1.b(this.c, this.f2375a.b(), this.b);
        }

        @DexIgnore
        @Override // com.fossil.mg1
        public Bitmap b(BitmapFactory.Options options) throws IOException {
            return BitmapFactory.decodeStream(this.f2375a.b(), null, options);
        }

        @DexIgnore
        @Override // com.fossil.mg1
        public void c() {
            this.f2375a.c();
        }

        @DexIgnore
        @Override // com.fossil.mg1
        public ImageHeaderParser.ImageType d() throws IOException {
            return lb1.e(this.c, this.f2375a.b(), this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements mg1 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ od1 f2376a;
        @DexIgnore
        public /* final */ List<ImageHeaderParser> b;
        @DexIgnore
        public /* final */ fc1 c;

        @DexIgnore
        public b(ParcelFileDescriptor parcelFileDescriptor, List<ImageHeaderParser> list, od1 od1) {
            ik1.d(od1);
            this.f2376a = od1;
            ik1.d(list);
            this.b = list;
            this.c = new fc1(parcelFileDescriptor);
        }

        @DexIgnore
        @Override // com.fossil.mg1
        public int a() throws IOException {
            return lb1.a(this.b, this.c, this.f2376a);
        }

        @DexIgnore
        @Override // com.fossil.mg1
        public Bitmap b(BitmapFactory.Options options) throws IOException {
            return BitmapFactory.decodeFileDescriptor(this.c.b().getFileDescriptor(), null, options);
        }

        @DexIgnore
        @Override // com.fossil.mg1
        public void c() {
        }

        @DexIgnore
        @Override // com.fossil.mg1
        public ImageHeaderParser.ImageType d() throws IOException {
            return lb1.d(this.b, this.c, this.f2376a);
        }
    }

    @DexIgnore
    int a() throws IOException;

    @DexIgnore
    Bitmap b(BitmapFactory.Options options) throws IOException;

    @DexIgnore
    Object c();  // void declaration

    @DexIgnore
    ImageHeaderParser.ImageType d() throws IOException;
}
