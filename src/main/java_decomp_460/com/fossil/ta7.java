package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ta7 implements Factory<sa7> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<PortfolioApp> f3388a;
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> b;
    @DexIgnore
    public /* final */ Provider<FileRepository> c;
    @DexIgnore
    public /* final */ Provider<s77> d;
    @DexIgnore
    public /* final */ Provider<UserRepository> e;
    @DexIgnore
    public /* final */ Provider<zm5> f;
    @DexIgnore
    public /* final */ Provider<CustomizeRealDataRepository> g;

    @DexIgnore
    public ta7(Provider<PortfolioApp> provider, Provider<DianaWatchFaceRepository> provider2, Provider<FileRepository> provider3, Provider<s77> provider4, Provider<UserRepository> provider5, Provider<zm5> provider6, Provider<CustomizeRealDataRepository> provider7) {
        this.f3388a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
    }

    @DexIgnore
    public static ta7 a(Provider<PortfolioApp> provider, Provider<DianaWatchFaceRepository> provider2, Provider<FileRepository> provider3, Provider<s77> provider4, Provider<UserRepository> provider5, Provider<zm5> provider6, Provider<CustomizeRealDataRepository> provider7) {
        return new ta7(provider, provider2, provider3, provider4, provider5, provider6, provider7);
    }

    @DexIgnore
    public static sa7 c(PortfolioApp portfolioApp, DianaWatchFaceRepository dianaWatchFaceRepository, FileRepository fileRepository, s77 s77, UserRepository userRepository, zm5 zm5, CustomizeRealDataRepository customizeRealDataRepository) {
        return new sa7(portfolioApp, dianaWatchFaceRepository, fileRepository, s77, userRepository, zm5, customizeRealDataRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public sa7 get() {
        return c(this.f3388a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get());
    }
}
