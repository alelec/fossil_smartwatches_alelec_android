package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jb7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public float f1740a;
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;

    @DexIgnore
    public jb7(float f, float f2, float f3, float f4) {
        this.f1740a = f;
        this.b = f2;
        this.c = f3;
        this.d = f4;
    }

    @DexIgnore
    public final float a() {
        return this.d;
    }

    @DexIgnore
    public final float b() {
        return this.c;
    }

    @DexIgnore
    public final float c() {
        return this.f1740a;
    }

    @DexIgnore
    public final float d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof jb7) {
                jb7 jb7 = (jb7) obj;
                if (!(Float.compare(this.f1740a, jb7.f1740a) == 0 && Float.compare(this.b, jb7.b) == 0 && Float.compare(this.c, jb7.c) == 0 && Float.compare(this.d, jb7.d) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (((((Float.floatToIntBits(this.f1740a) * 31) + Float.floatToIntBits(this.b)) * 31) + Float.floatToIntBits(this.c)) * 31) + Float.floatToIntBits(this.d);
    }

    @DexIgnore
    public String toString() {
        return "WFMetric(scaledX=" + this.f1740a + ", scaledY=" + this.b + ", scaledWidth=" + this.c + ", scaledHeight=" + this.d + ")";
    }
}
