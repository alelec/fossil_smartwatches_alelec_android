package com.fossil;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import androidx.core.app.TaskStackBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.NotificationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import com.portfolio.platform.uirenew.home.HomeActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eo5 {
    @DexIgnore
    public static /* final */ a c; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f963a;
    @DexIgnore
    public String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.eo5$a$a")
        @eo7(c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1", f = "FossilNotificationBar.kt", l = {42}, m = "invokeSuspend")
        /* renamed from: com.fossil.eo5$a$a  reason: collision with other inner class name */
        public static final class C0069a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Context $context;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.eo5$a$a$a")
            @eo7(c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1$1", f = "FossilNotificationBar.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.eo5$a$a$a  reason: collision with other inner class name */
            public static final class C0070a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ eo5 $fossilNotificationBar;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0069a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0070a(C0069a aVar, eo5 eo5, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$fossilNotificationBar = eo5;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0070a aVar = new C0070a(this.this$0, this.$fossilNotificationBar, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0070a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        a.g(eo5.c, this.this$0.$context, this.$fossilNotificationBar, false, 4, null);
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0069a(Context context, qn7 qn7) {
                super(2, qn7);
                this.$context = context;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0069a aVar = new C0069a(this.$context, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((C0069a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    String I = PortfolioApp.h0.c().I();
                    eo5 eo5 = new eo5(I, null, 2, null);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("FossilNotificationBar", "content " + I);
                    jx7 c = bw7.c();
                    C0070a aVar = new C0070a(this, eo5, null);
                    this.L$0 = iv7;
                    this.L$1 = I;
                    this.L$2 = eo5;
                    this.label = 1;
                    if (eu7.g(c, aVar, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    eo5 eo52 = (eo5) this.L$2;
                    String str = (String) this.L$1;
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void g(a aVar, Context context, eo5 eo5, boolean z, int i, Object obj) {
            if ((i & 4) != 0) {
                z = false;
            }
            aVar.f(context, eo5, z);
        }

        @DexIgnore
        public static /* synthetic */ void i(a aVar, Context context, eo5 eo5, boolean z, int i, Object obj) {
            if ((i & 4) != 0) {
                z = false;
            }
            aVar.h(context, eo5, z);
        }

        @DexIgnore
        public final PendingIntent a(Context context, String str, int i) {
            Intent intent = new Intent(context, NotificationReceiver.class);
            intent.setAction(str);
            intent.putExtra("ACTION_EVENT", i);
            return PendingIntent.getBroadcast(context, Action.DisplayMode.DATE, intent, 134217728);
        }

        @DexIgnore
        public final PendingIntent b(Context context, int i, int i2) {
            Intent intent = new Intent(context, HomeActivity.class);
            intent.putExtra("OUT_STATE_DASHBOARD_CURRENT_TAB", i2);
            TaskStackBuilder f = TaskStackBuilder.f(context);
            f.b(intent);
            pq7.b(f, "TaskStackBuilder.create(\u2026ntWithParentStack(intent)");
            return f.g(i, 134217728);
        }

        @DexIgnore
        public final void c(Context context, Service service, boolean z) {
            pq7.c(context, "context");
            pq7.c(service, Constants.SERVICE);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "Service Trackinng - startForegroundNotification() - context=" + context + ", service=" + service + ", isStopForeground = " + z);
            try {
                NotificationUtils.Companion.getInstance().startForegroundNotification(context, service, "", "", z);
                d(context);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("FossilNotificationBar", "startForegroundNotification() - ex=" + e);
            }
        }

        @DexIgnore
        public final void d(Context context) {
            pq7.c(context, "context");
            try {
                boolean q0 = PortfolioApp.h0.c().q0();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FossilNotificationBar", "updateData() - context=" + context + " hasWorkout " + q0);
                if (q0) {
                    cl7<String, String> k = PortfolioApp.h0.c().g0().k();
                    i(this, context, new eo5(k.getSecond(), k.getFirst()), false, 4, null);
                    return;
                }
                xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new C0069a(context, null), 3, null);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("FossilNotificationBar", "updateData - ex=" + e);
            }
        }

        @DexIgnore
        public final void e(Context context, String str, String str2) {
            pq7.c(context, "context");
            pq7.c(str, "title");
            pq7.c(str2, "content");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateData() - title=" + str + " - content=" + str2);
            try {
                i(this, context, vt7.l(str) ^ true ? new eo5(str2, str) : new eo5(str2, null, 2, null), false, 4, null);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("FossilNotificationBar", "updateData - ex=" + e);
                e.printStackTrace();
            }
        }

        @DexIgnore
        public final void f(Context context, eo5 eo5, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateNotification content " + eo5.f963a);
            if (!TextUtils.isEmpty(PortfolioApp.h0.c().J())) {
                NotificationUtils.Companion.getInstance().updateNotification(context, 1, eo5.f963a, b(context, Action.DisplayMode.ACTIVITY, 0), a(context, ".news.notifications.NotificationReceiver", 1), z);
            } else {
                NotificationUtils.Companion.getInstance().updateNotification(context, 1, eo5.f963a, null, null, z);
            }
        }

        @DexIgnore
        public final void h(Context context, eo5 eo5, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateNotificationWithRichContent " + eo5.f963a);
            if (!TextUtils.isEmpty(PortfolioApp.h0.c().J())) {
                NotificationUtils.Companion.getInstance().updateRichTextNotification(context, 1, eo5.b, eo5.f963a, z);
            } else {
                NotificationUtils.Companion.getInstance().updateRichTextNotification(context, 1, eo5.b, eo5.f963a, z);
            }
        }
    }

    @DexIgnore
    public eo5() {
        this(null, null, 3, null);
    }

    @DexIgnore
    public eo5(String str, String str2) {
        pq7.c(str, "mContent");
        pq7.c(str2, "mTitle");
        this.f963a = str;
        this.b = str2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ eo5(java.lang.String r3, java.lang.String r4, int r5, com.fossil.kq7 r6) {
        /*
            r2 = this;
            r0 = r5 & 1
            if (r0 == 0) goto L_0x0016
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            r1 = 2131887059(0x7f1203d3, float:1.9408714E38)
            java.lang.String r3 = com.fossil.um5.c(r0, r1)
            java.lang.String r0 = "LanguageHelper.getString\u2026Dashboard_CTA__PairWatch)"
            com.fossil.pq7.b(r3, r0)
        L_0x0016:
            r0 = r5 & 2
            if (r0 == 0) goto L_0x001c
            java.lang.String r4 = ""
        L_0x001c:
            r2.<init>(r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.eo5.<init>(java.lang.String, java.lang.String, int, com.fossil.kq7):void");
    }
}
