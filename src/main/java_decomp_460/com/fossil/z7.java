package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z7 extends qq7 implements rp7<lp, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ oy1 b;
    @DexIgnore
    public /* final */ /* synthetic */ e60 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z7(oy1 oy1, e60 e60, lp lpVar) {
        super(1);
        this.b = oy1;
        this.c = e60;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(lp lpVar) {
        lp lpVar2 = lpVar;
        if (lpVar2.v.c == zq.SUCCESS) {
            Object x = lpVar2.x();
            if (x instanceof tl7) {
                this.c.c.post(new f1(this, x));
            } else {
                this.c.c.post(new a3(this));
            }
        } else {
            this.c.c.post(new s3(this, lpVar2));
            this.c.n0(lpVar2.v);
        }
        return tl7.f3441a;
    }
}
