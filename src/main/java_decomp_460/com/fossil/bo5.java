package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bo5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ DianaPresetRepository f463a;
    @DexIgnore
    public /* final */ on5 b;
    @DexIgnore
    public /* final */ WatchFaceRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.validation.DataValidationManager", f = "DataValidationManager.kt", l = {19, 20, 26, 37}, m = "validateDianaPreset")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(bo5 bo5, qn7 qn7) {
            super(qn7);
            this.this$0 = bo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, this);
        }
    }

    @DexIgnore
    public bo5(DianaPresetRepository dianaPresetRepository, on5 on5, WatchFaceRepository watchFaceRepository) {
        pq7.c(dianaPresetRepository, "mDianaPresetRepository");
        pq7.c(on5, "mSharePreferencesManager");
        pq7.c(watchFaceRepository, "mWatchFaceRepository");
        this.f463a = dianaPresetRepository;
        this.b = on5;
        this.c = watchFaceRepository;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x01a1  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01c4  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01e7  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r19, com.fossil.qn7<? super com.fossil.tl7> r20) {
        /*
        // Method dump skipped, instructions count: 534
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bo5.a(java.lang.String, com.fossil.qn7):java.lang.Object");
    }
}
