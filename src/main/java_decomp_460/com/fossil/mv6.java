package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mv6 implements Factory<lv6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<do5> f2427a;
    @DexIgnore
    public /* final */ Provider<mj5> b;
    @DexIgnore
    public /* final */ Provider<on5> c;

    @DexIgnore
    public mv6(Provider<do5> provider, Provider<mj5> provider2, Provider<on5> provider3) {
        this.f2427a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static mv6 a(Provider<do5> provider, Provider<mj5> provider2, Provider<on5> provider3) {
        return new mv6(provider, provider2, provider3);
    }

    @DexIgnore
    public static lv6 c(do5 do5, mj5 mj5, on5 on5) {
        return new lv6(do5, mj5, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public lv6 get() {
        return c(this.f2427a.get(), this.b.get(), this.c.get());
    }
}
