package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class va5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ LinearLayout t;
    @DexIgnore
    public /* final */ NumberPicker u;

    @DexIgnore
    public va5(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, LinearLayout linearLayout, NumberPicker numberPicker) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleButton;
        this.s = flexibleTextView;
        this.t = linearLayout;
        this.u = numberPicker;
    }
}
