package com.fossil;

import android.os.Bundle;
import android.util.SparseArray;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x46 extends r46 implements LoaderManager.a<SparseArray<List<? extends BaseFeatureModel>>> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ s46 e;
    @DexIgnore
    public /* final */ NotificationsLoader f;
    @DexIgnore
    public /* final */ LoaderManager g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingPresenter$start$1", f = "NotificationDialLandingPresenter.kt", l = {33}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ x46 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.x46$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingPresenter$start$1$1", f = "NotificationDialLandingPresenter.kt", l = {34}, m = "invokeSuspend")
        /* renamed from: com.fossil.x46$a$a  reason: collision with other inner class name */
        public static final class C0277a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public C0277a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0277a aVar = new C0277a(qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((C0277a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    PortfolioApp c = PortfolioApp.h0.c();
                    this.L$0 = iv7;
                    this.label = 1;
                    if (c.V0(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(x46 x46, qn7 qn7) {
            super(2, qn7);
            this.this$0 = x46;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 h = this.this$0.h();
                C0277a aVar = new C0277a(null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(h, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = x46.class.getSimpleName();
        pq7.b(simpleName, "NotificationDialLandingP\u2026er::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public x46(s46 s46, NotificationsLoader notificationsLoader, LoaderManager loaderManager) {
        pq7.c(s46, "mView");
        pq7.c(notificationsLoader, "mNotificationLoader");
        pq7.c(loaderManager, "mLoaderManager");
        this.e = s46;
        this.f = notificationsLoader;
        this.g = loaderManager;
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.at0<android.util.SparseArray<java.util.List<com.fossil.wearables.fsl.shared.BaseFeatureModel>>>' to match base method */
    @Override // androidx.loader.app.LoaderManager.a
    public at0<SparseArray<List<? extends BaseFeatureModel>>> d(int i, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "onCreateLoader id=" + i);
        return this.f;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.at0<android.util.SparseArray<java.util.List<com.fossil.wearables.fsl.shared.BaseFeatureModel>>>] */
    @Override // androidx.loader.app.LoaderManager.a
    public void g(at0<SparseArray<List<? extends BaseFeatureModel>>> at0) {
        pq7.c(at0, "loader");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(h, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!PortfolioApp.h0.c().k0().s0()) {
            xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
        }
        this.g.d(3, null, this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        this.g.a(3);
        FLogger.INSTANCE.getLocal().d(h, "stop");
    }

    @DexIgnore
    /* renamed from: o */
    public void a(at0<SparseArray<List<BaseFeatureModel>>> at0, SparseArray<List<BaseFeatureModel>> sparseArray) {
        pq7.c(at0, "loader");
        if (sparseArray != null) {
            this.e.k5(sparseArray);
        } else {
            FLogger.INSTANCE.getLocal().d(h, "onLoadFinished, data=null");
        }
    }

    @DexIgnore
    public void p() {
        this.e.M5(this);
    }
}
