package com.fossil;

import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ym4 extends jn4 {

    @DexIgnore
    public enum a {
        UNCODABLE,
        ONE_DIGIT,
        TWO_DIGITS,
        FNC_1
    }

    @DexIgnore
    public static int f(CharSequence charSequence, int i, int i2) {
        a g;
        a g2;
        a g3 = g(charSequence, i);
        if (!(g3 == a.UNCODABLE || g3 == a.ONE_DIGIT)) {
            if (i2 == 99) {
                return i2;
            }
            if (i2 != 100) {
                if (g3 == a.FNC_1) {
                    g3 = g(charSequence, i + 1);
                }
                if (g3 == a.TWO_DIGITS) {
                    return 99;
                }
            } else if (g3 == a.FNC_1 || (g = g(charSequence, i + 2)) == a.UNCODABLE || g == a.ONE_DIGIT) {
                return i2;
            } else {
                if (g == a.FNC_1) {
                    return g(charSequence, i + 3) == a.TWO_DIGITS ? 99 : 100;
                }
                int i3 = i + 4;
                while (true) {
                    g2 = g(charSequence, i3);
                    if (g2 != a.TWO_DIGITS) {
                        break;
                    }
                    i3 += 2;
                }
                return g2 == a.ONE_DIGIT ? 100 : 99;
            }
        }
        return 100;
    }

    @DexIgnore
    public static a g(CharSequence charSequence, int i) {
        int length = charSequence.length();
        if (i >= length) {
            return a.UNCODABLE;
        }
        char charAt = charSequence.charAt(i);
        if (charAt == '\u00f1') {
            return a.FNC_1;
        }
        if (charAt < '0' || charAt > '9') {
            return a.UNCODABLE;
        }
        int i2 = i + 1;
        if (i2 >= length) {
            return a.ONE_DIGIT;
        }
        char charAt2 = charSequence.charAt(i2);
        return (charAt2 < '0' || charAt2 > '9') ? a.ONE_DIGIT : a.TWO_DIGITS;
    }

    @DexIgnore
    @Override // com.fossil.jn4, com.fossil.ql4
    public bm4 a(String str, kl4 kl4, int i, int i2, Map<ml4, ?> map) throws rl4 {
        if (kl4 == kl4.CODE_128) {
            return super.a(str, kl4, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode CODE_128, but got " + kl4);
    }

    @DexIgnore
    @Override // com.fossil.jn4
    public boolean[] c(String str) {
        int i;
        int i2 = 0;
        int length = str.length();
        if (length <= 0 || length > 80) {
            throw new IllegalArgumentException("Contents length should be between 1 and 80 characters, but got " + length);
        }
        for (int i3 = 0; i3 < length; i3++) {
            char charAt = str.charAt(i3);
            if (charAt < ' ' || charAt > '~') {
                switch (charAt) {
                    default:
                        throw new IllegalArgumentException("Bad character in input: " + charAt);
                    case '\u00f1':
                    case '\u00f2':
                    case '\u00f3':
                    case '\u00f4':
                        break;
                }
            }
        }
        ArrayList<int[]> arrayList = new ArrayList();
        int i4 = 1;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (i7 < length) {
            int f = f(str, i7, i5);
            if (f == i5) {
                switch (str.charAt(i7)) {
                    case '\u00f1':
                        i = 102;
                        break;
                    case '\u00f2':
                        i = 97;
                        break;
                    case '\u00f3':
                        i = 96;
                        break;
                    case '\u00f4':
                        i = 100;
                        break;
                    default:
                        if (i5 == 100) {
                            i = str.charAt(i7) - ' ';
                            break;
                        } else {
                            i = Integer.parseInt(str.substring(i7, i7 + 2));
                            i7++;
                            break;
                        }
                }
                i7++;
                f = i5;
            } else {
                i = i5 == 0 ? f == 100 ? 104 : 105 : f;
            }
            arrayList.add(xm4.f4139a[i]);
            i6 += i * i4;
            if (i7 != 0) {
                i4++;
                i5 = f;
            } else {
                i5 = f;
            }
        }
        arrayList.add(xm4.f4139a[i6 % 103]);
        arrayList.add(xm4.f4139a[106]);
        int i8 = 0;
        for (int[] iArr : arrayList) {
            for (int i9 : iArr) {
                i8 += i9;
            }
        }
        boolean[] zArr = new boolean[i8];
        for (int[] iArr2 : arrayList) {
            i2 += jn4.b(zArr, i2, iArr2, true);
        }
        return zArr;
    }
}
