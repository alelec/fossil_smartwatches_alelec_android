package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class us0 {
    @DexIgnore
    public static final iv7 a(ts0 ts0) {
        pq7.c(ts0, "$this$viewModelScope");
        iv7 iv7 = (iv7) ts0.getTag("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY");
        if (iv7 != null) {
            return iv7;
        }
        Object tagIfAbsent = ts0.setTagIfAbsent("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY", new lr0(ux7.b(null, 1, null).plus(bw7.c().S())));
        pq7.b(tagIfAbsent, "setTagIfAbsent(JOB_KEY,\n\u2026patchers.Main.immediate))");
        return (iv7) tagIfAbsent;
    }
}
