package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tr3 extends wr3 {
    @DexIgnore
    public hu2 g;
    @DexIgnore
    public /* final */ /* synthetic */ pr3 h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public tr3(pr3 pr3, String str, int i, hu2 hu2) {
        super(str, i);
        this.h = pr3;
        this.g = hu2;
    }

    @DexIgnore
    @Override // com.fossil.wr3
    public final int a() {
        return this.g.J();
    }

    @DexIgnore
    @Override // com.fossil.wr3
    public final boolean i() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.wr3
    public final boolean j() {
        return this.g.N();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:137:0x040c  */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x040f  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x014d  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0156 A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean k(java.lang.Long r15, java.lang.Long r16, com.fossil.wu2 r17, long r18, com.fossil.rg3 r20, boolean r21) {
        /*
        // Method dump skipped, instructions count: 1121
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tr3.k(java.lang.Long, java.lang.Long, com.fossil.wu2, long, com.fossil.rg3, boolean):boolean");
    }
}
