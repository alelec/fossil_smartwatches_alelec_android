package com.fossil;

import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gh0 implements View.OnTouchListener, View.OnAttachStateChangeListener {
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ View e;
    @DexIgnore
    public Runnable f;
    @DexIgnore
    public Runnable g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public int i;
    @DexIgnore
    public /* final */ int[] j; // = new int[2];

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            ViewParent parent = gh0.this.e.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            gh0.this.e();
        }
    }

    @DexIgnore
    public gh0(View view) {
        this.e = view;
        view.setLongClickable(true);
        view.addOnAttachStateChangeListener(this);
        this.b = (float) ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
        int tapTimeout = ViewConfiguration.getTapTimeout();
        this.c = tapTimeout;
        this.d = (tapTimeout + ViewConfiguration.getLongPressTimeout()) / 2;
    }

    @DexIgnore
    public static boolean h(View view, float f2, float f3, float f4) {
        float f5 = -f4;
        return f2 >= f5 && f3 >= f5 && f2 < ((float) (view.getRight() - view.getLeft())) + f4 && f3 < ((float) (view.getBottom() - view.getTop())) + f4;
    }

    @DexIgnore
    public final void a() {
        Runnable runnable = this.g;
        if (runnable != null) {
            this.e.removeCallbacks(runnable);
        }
        Runnable runnable2 = this.f;
        if (runnable2 != null) {
            this.e.removeCallbacks(runnable2);
        }
    }

    @DexIgnore
    public abstract lg0 b();

    @DexIgnore
    public abstract boolean c();

    @DexIgnore
    public boolean d() {
        lg0 b2 = b();
        if (b2 == null || !b2.a()) {
            return true;
        }
        b2.dismiss();
        return true;
    }

    @DexIgnore
    public void e() {
        a();
        View view = this.e;
        if (view.isEnabled() && !view.isLongClickable() && c()) {
            view.getParent().requestDisallowInterceptTouchEvent(true);
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
            view.onTouchEvent(obtain);
            obtain.recycle();
            this.h = true;
        }
    }

    @DexIgnore
    public final boolean f(MotionEvent motionEvent) {
        View view = this.e;
        lg0 b2 = b();
        if (b2 != null) {
            if (!b2.a()) {
                return false;
            }
            eh0 eh0 = (eh0) b2.j();
            if (eh0 != null) {
                if (!eh0.isShown()) {
                    return false;
                }
                MotionEvent obtainNoHistory = MotionEvent.obtainNoHistory(motionEvent);
                i(view, obtainNoHistory);
                j(eh0, obtainNoHistory);
                boolean e2 = eh0.e(obtainNoHistory, this.i);
                obtainNoHistory.recycle();
                int actionMasked = motionEvent.getActionMasked();
                boolean z = (actionMasked == 1 || actionMasked == 3) ? false : true;
                if (e2 && z) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean g(MotionEvent motionEvent) {
        View view = this.e;
        if (!view.isEnabled()) {
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked == 2) {
                    int findPointerIndex = motionEvent.findPointerIndex(this.i);
                    if (findPointerIndex < 0 || h(view, motionEvent.getX(findPointerIndex), motionEvent.getY(findPointerIndex), this.b)) {
                        return false;
                    }
                    a();
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    return true;
                } else if (actionMasked != 3) {
                    return false;
                }
            }
            a();
            return false;
        }
        this.i = motionEvent.getPointerId(0);
        if (this.f == null) {
            this.f = new a();
        }
        view.postDelayed(this.f, (long) this.c);
        if (this.g == null) {
            this.g = new b();
        }
        view.postDelayed(this.g, (long) this.d);
        return false;
    }

    @DexIgnore
    public final boolean i(View view, MotionEvent motionEvent) {
        int[] iArr = this.j;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) iArr[0], (float) iArr[1]);
        return true;
    }

    @DexIgnore
    public final boolean j(View view, MotionEvent motionEvent) {
        int[] iArr = this.j;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) (-iArr[0]), (float) (-iArr[1]));
        return true;
    }

    @DexIgnore
    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z;
        boolean z2 = this.h;
        if (z2) {
            z = f(motionEvent) || !d();
        } else {
            boolean z3 = g(motionEvent) && c();
            if (z3) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
                this.e.onTouchEvent(obtain);
                obtain.recycle();
                z = z3;
            } else {
                z = z3;
            }
        }
        this.h = z;
        return z || z2;
    }

    @DexIgnore
    public void onViewAttachedToWindow(View view) {
    }

    @DexIgnore
    public void onViewDetachedFromWindow(View view) {
        this.h = false;
        this.i = -1;
        Runnable runnable = this.f;
        if (runnable != null) {
            this.e.removeCallbacks(runnable);
        }
    }
}
