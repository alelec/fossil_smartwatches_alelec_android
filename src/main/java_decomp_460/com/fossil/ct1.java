package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ct1 extends at1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ct1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public ct1 a(Parcel parcel) {
            return new ct1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ct1 createFromParcel(Parcel parcel) {
            return new ct1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ct1[] newArray(int i) {
            return new ct1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ct1(Parcel parcel, kq7 kq7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.d = readString;
            this.e = parcel.readInt();
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public ct1(String str, int i) {
        super(g90.SECOND_TIMEZONE, false, 2);
        this.d = str;
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.at1
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.at1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(ct1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            ct1 ct1 = (ct1) obj;
            if (!pq7.a(this.d, ct1.d)) {
                return false;
            }
            return this.e == ct1.e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.TimeZoneTwoComplicationDataConfig");
    }

    @DexIgnore
    public final String getLocation() {
        return this.d;
    }

    @DexIgnore
    public final int getUtcOffsetInMinutes() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.at1
    public int hashCode() {
        return (((super.hashCode() * 31) + this.d.hashCode()) * 31) + this.e;
    }

    @DexIgnore
    @Override // com.fossil.at1, com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject put = super.toJSONObject().put("loc", this.d).put("utc", this.e);
        pq7.b(put, "super.toJSONObject()\n   \u2026.UTC, utcOffsetInMinutes)");
        return put;
    }

    @DexIgnore
    @Override // com.fossil.at1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }
}
