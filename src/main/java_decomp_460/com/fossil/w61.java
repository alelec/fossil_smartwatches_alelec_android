package com.fossil;

public final class w61 {

    /* renamed from: a  reason: collision with root package name */
    public static final boolean f3888a;
    public static final w61 b = new w61();

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0054, code lost:
        if (com.fossil.vt7.r(r2, "moto g(6)", true) == false) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0072, code lost:
        if (com.fossil.vt7.s(r2, "LM-X410", false, 2, null) != false) goto L_0x0023;
     */
    /*
    static {
        /*
            r6 = 0
            r5 = 2
            r0 = 1
            r1 = 0
            com.fossil.w61 r2 = new com.fossil.w61
            r2.<init>()
            com.fossil.w61.b = r2
            java.lang.String r2 = android.os.Build.MODEL
            if (r2 == 0) goto L_0x0074
            int r3 = android.os.Build.VERSION.SDK_INT
            r4 = 26
            if (r3 != r4) goto L_0x0056
            java.lang.String r3 = "SAMSUNG-"
            java.lang.String r3 = com.fossil.wt7.U(r2, r3)
            java.lang.String r4 = "SM-"
            boolean r3 = com.fossil.vt7.s(r3, r4, r1, r5, r6)
            if (r3 == 0) goto L_0x0026
        L_0x0023:
            com.fossil.w61.f3888a = r0
            return
        L_0x0026:
            java.lang.String[] r3 = new java.lang.String[r5]
            java.lang.String r4 = "Moto E"
            r3[r1] = r4
            java.lang.String r4 = "XT1924-9"
            r3[r0] = r4
            boolean r3 = com.fossil.em7.B(r3, r2)
            if (r3 != 0) goto L_0x0023
            java.lang.String r3 = "moto e5"
            boolean r3 = com.fossil.vt7.r(r2, r3, r0)
            if (r3 != 0) goto L_0x0023
            java.lang.String[] r3 = new java.lang.String[r5]
            java.lang.String r4 = "Moto G Play"
            r3[r1] = r4
            java.lang.String r4 = "XT1925-10"
            r3[r0] = r4
            boolean r3 = com.fossil.em7.B(r3, r2)
            if (r3 != 0) goto L_0x0023
            java.lang.String r3 = "moto g(6)"
            boolean r3 = com.fossil.vt7.r(r2, r3, r0)
            if (r3 != 0) goto L_0x0023
        L_0x0056:
            int r3 = android.os.Build.VERSION.SDK_INT
            r4 = 27
            if (r3 != r4) goto L_0x0074
            java.lang.String r3 = "LG-Q710"
            boolean r3 = com.fossil.vt7.s(r2, r3, r1, r5, r6)
            if (r3 != 0) goto L_0x0023
            java.lang.String r3 = "LM-X220PM"
            boolean r3 = com.fossil.pq7.a(r2, r3)
            if (r3 != 0) goto L_0x0023
            java.lang.String r3 = "LM-X410"
            boolean r2 = com.fossil.vt7.s(r2, r3, r1, r5, r6)
            if (r2 != 0) goto L_0x0023
        L_0x0074:
            r0 = r1
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.w61.<clinit>():void");
    }
    */

    public final boolean a() {
        return f3888a;
    }
}
