package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Build;
import android.util.Log;
import android.view.MenuItem;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ao0 {
    @DexIgnore
    public static MenuItem a(MenuItem menuItem, sn0 sn0) {
        if (menuItem instanceof hm0) {
            return ((hm0) menuItem).a(sn0);
        }
        Log.w("MenuItemCompat", "setActionProvider: item does not implement SupportMenuItem; ignoring");
        return menuItem;
    }

    @DexIgnore
    public static void b(MenuItem menuItem, char c, int i) {
        if (menuItem instanceof hm0) {
            ((hm0) menuItem).setAlphabeticShortcut(c, i);
        } else if (Build.VERSION.SDK_INT >= 26) {
            menuItem.setAlphabeticShortcut(c, i);
        }
    }

    @DexIgnore
    public static void c(MenuItem menuItem, CharSequence charSequence) {
        if (menuItem instanceof hm0) {
            ((hm0) menuItem).setContentDescription(charSequence);
        } else if (Build.VERSION.SDK_INT >= 26) {
            menuItem.setContentDescription(charSequence);
        }
    }

    @DexIgnore
    public static void d(MenuItem menuItem, ColorStateList colorStateList) {
        if (menuItem instanceof hm0) {
            ((hm0) menuItem).setIconTintList(colorStateList);
        } else if (Build.VERSION.SDK_INT >= 26) {
            menuItem.setIconTintList(colorStateList);
        }
    }

    @DexIgnore
    public static void e(MenuItem menuItem, PorterDuff.Mode mode) {
        if (menuItem instanceof hm0) {
            ((hm0) menuItem).setIconTintMode(mode);
        } else if (Build.VERSION.SDK_INT >= 26) {
            menuItem.setIconTintMode(mode);
        }
    }

    @DexIgnore
    public static void f(MenuItem menuItem, char c, int i) {
        if (menuItem instanceof hm0) {
            ((hm0) menuItem).setNumericShortcut(c, i);
        } else if (Build.VERSION.SDK_INT >= 26) {
            menuItem.setNumericShortcut(c, i);
        }
    }

    @DexIgnore
    public static void g(MenuItem menuItem, CharSequence charSequence) {
        if (menuItem instanceof hm0) {
            ((hm0) menuItem).setTooltipText(charSequence);
        } else if (Build.VERSION.SDK_INT >= 26) {
            menuItem.setTooltipText(charSequence);
        }
    }
}
