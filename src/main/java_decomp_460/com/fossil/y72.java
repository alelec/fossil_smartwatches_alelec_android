package com.fossil;

import android.os.RemoteException;
import com.fossil.m62;
import com.fossil.m62.b;
import com.fossil.p72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y72<A extends m62.b, L> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ p72.a<L> f4252a;

    @DexIgnore
    public y72(p72.a<L> aVar) {
        this.f4252a = aVar;
    }

    @DexIgnore
    public p72.a<L> a() {
        return this.f4252a;
    }

    @DexIgnore
    public abstract void b(A a2, ot3<Boolean> ot3) throws RemoteException;
}
