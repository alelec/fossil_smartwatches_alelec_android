package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y35 extends x35 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362056, 1);
        E.put(2131363030, 2);
        E.put(2131363031, 3);
        E.put(2131363390, 4);
        E.put(2131362053, 5);
        E.put(2131362687, 6);
        E.put(2131363296, 7);
        E.put(2131363171, 8);
        E.put(2131363374, 9);
        E.put(2131363295, 10);
        E.put(2131363293, 11);
    }
    */

    @DexIgnore
    public y35(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 12, D, E));
    }

    @DexIgnore
    public y35(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[5], (ConstraintLayout) objArr[1], (ImageView) objArr[6], (ScrollView) objArr[0], (RecyclerView) objArr[2], (RecyclerView) objArr[3], (FlexibleSwitchCompat) objArr[8], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[4]);
        this.C = -1;
        this.t.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.C != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.C = 1;
        }
        w();
    }
}
