package com.fossil;

import android.database.Cursor;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hx5 extends jx5<b> implements SectionIndexer {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public /* final */ ArrayList<String> i; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Integer> j; // = new ArrayList<>();
    @DexIgnore
    public int k; // = -1;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public a m;
    @DexIgnore
    public AlphabetIndexer s;
    @DexIgnore
    public /* final */ List<j06> t;

    @DexIgnore
    public interface a {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f1552a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public /* final */ ae5 h;
        @DexIgnore
        public /* final */ /* synthetic */ hx5 i;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.c();
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hx5$b$b")
        /* renamed from: com.fossil.hx5$b$b  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0118b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public View$OnClickListenerC0118b(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.c();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(hx5 hx5, ae5 ae5) {
            super(ae5.n());
            pq7.c(ae5, "binding");
            this.i = hx5;
            this.h = ae5;
            String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            String d3 = qn5.l.a().d("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(d2)) {
                this.h.s.setBackgroundColor(Color.parseColor(d2));
            }
            if (!TextUtils.isEmpty(d3)) {
                this.h.x.setBackgroundColor(Color.parseColor(d3));
            }
            this.h.r.setOnClickListener(new a(this));
            this.h.q.setOnClickListener(new View$OnClickListenerC0118b(this));
        }

        @DexIgnore
        public final j06 b() {
            Contact contact = new Contact();
            contact.setContactId(this.e);
            contact.setFirstName(this.f1552a);
            contact.setPhotoThumbUri(this.b);
            j06 j06 = new j06(contact, null, 2, null);
            if (this.f == 1) {
                j06.setHasPhoneNumber(true);
                j06.setPhoneNumber(this.c);
            } else {
                j06.setHasPhoneNumber(false);
            }
            Contact contact2 = j06.getContact();
            if (contact2 != null) {
                contact2.setUseSms(true);
                Contact contact3 = j06.getContact();
                if (contact3 != null) {
                    contact3.setUseCall(true);
                    j06.setFavorites(this.g);
                    j06.setAdded(true);
                    return j06;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        public final void c() {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                Iterator it = this.i.t.iterator();
                int i2 = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i2 = -1;
                        break;
                    }
                    Contact contact = ((j06) it.next()).getContact();
                    if (contact != null && contact.getContactId() == this.e) {
                        break;
                    }
                    i2++;
                }
                if (i2 != -1) {
                    this.i.t.remove(i2);
                } else {
                    this.i.t.add(b());
                }
                a aVar = this.i.m;
                if (aVar != null) {
                    aVar.a();
                }
                this.i.notifyItemChanged(adapterPosition);
            }
        }

        @DexIgnore
        public final void d(Cursor cursor, int i2) {
            Object obj;
            boolean z;
            boolean z2 = true;
            pq7.c(cursor, "cursor");
            cursor.moveToPosition(i2);
            FLogger.INSTANCE.getLocal().d(hx5.u, ".Inside renderData, cursor move position=" + i2);
            this.f1552a = cursor.getString(cursor.getColumnIndex("display_name"));
            this.b = cursor.getString(cursor.getColumnIndex("photo_thumb_uri"));
            this.e = cursor.getInt(cursor.getColumnIndex("contact_id"));
            this.f = cursor.getInt(cursor.getColumnIndex("has_phone_number"));
            this.c = cursor.getString(cursor.getColumnIndex("data1"));
            this.d = cursor.getString(cursor.getColumnIndex("sort_key"));
            this.g = cursor.getInt(cursor.getColumnIndex("starred")) == 1;
            if (this.i.l < i2) {
                this.i.l = i2;
            }
            String d2 = p47.d(this.c);
            if (this.i.j.contains(Integer.valueOf(i2)) || (this.i.l < i2 && this.i.k == this.e && this.i.i.contains(d2))) {
                if (!this.i.j.contains(Integer.valueOf(i2))) {
                    this.i.j.add(Integer.valueOf(i2));
                }
                e(8);
            } else {
                if (i2 > this.i.l) {
                    this.i.l = i2;
                }
                if (this.i.k != this.e) {
                    this.i.i.clear();
                    this.i.k = this.e;
                }
                this.i.i.add(d2);
                e(0);
                if (cursor.moveToPrevious() && cursor.getInt(cursor.getColumnIndex("contact_id")) == this.e) {
                    FlexibleCheckBox flexibleCheckBox = this.h.q;
                    pq7.b(flexibleCheckBox, "binding.accbSelect");
                    flexibleCheckBox.setVisibility(4);
                    FlexibleTextView flexibleTextView = this.h.w;
                    pq7.b(flexibleTextView, "binding.pickContactTitle");
                    flexibleTextView.setVisibility(8);
                }
                cursor.moveToNext();
            }
            FlexibleTextView flexibleTextView2 = this.h.w;
            pq7.b(flexibleTextView2, "binding.pickContactTitle");
            flexibleTextView2.setText(this.f1552a);
            if (this.f == 1) {
                FlexibleTextView flexibleTextView3 = this.h.v;
                pq7.b(flexibleTextView3, "binding.pickContactPhone");
                flexibleTextView3.setText(this.c);
                FlexibleTextView flexibleTextView4 = this.h.v;
                pq7.b(flexibleTextView4, "binding.pickContactPhone");
                flexibleTextView4.setVisibility(0);
            } else {
                FlexibleTextView flexibleTextView5 = this.h.v;
                pq7.b(flexibleTextView5, "binding.pickContactPhone");
                flexibleTextView5.setVisibility(8);
            }
            Iterator it = this.i.t.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                Object next = it.next();
                Contact contact = ((j06) next).getContact();
                if (contact == null || contact.getContactId() != this.e) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    obj = next;
                    break;
                }
            }
            j06 j06 = (j06) obj;
            FlexibleCheckBox flexibleCheckBox2 = this.h.q;
            pq7.b(flexibleCheckBox2, "binding.accbSelect");
            if (j06 == null) {
                z2 = false;
            }
            flexibleCheckBox2.setChecked(z2);
            FLogger.INSTANCE.getLocal().d(hx5.u, "Inside renderData, contactId = " + this.e + ", displayName = " + this.f1552a + ", hasPhoneNumber = " + this.f + ", phoneNumber = " + this.c + ", newSortKey = " + this.d);
            if (i2 == this.i.getPositionForSection(this.i.getSectionForPosition(i2))) {
                FlexibleTextView flexibleTextView6 = this.h.t;
                pq7.b(flexibleTextView6, "binding.ftvAlphabet");
                flexibleTextView6.setVisibility(0);
                FlexibleTextView flexibleTextView7 = this.h.t;
                pq7.b(flexibleTextView7, "binding.ftvAlphabet");
                flexibleTextView7.setText(Character.toString(jl5.b.j(this.d)));
                return;
            }
            FlexibleTextView flexibleTextView8 = this.h.t;
            pq7.b(flexibleTextView8, "binding.ftvAlphabet");
            flexibleTextView8.setVisibility(8);
        }

        @DexIgnore
        public final void e(int i2) {
            FlexibleCheckBox flexibleCheckBox = this.h.q;
            pq7.b(flexibleCheckBox, "binding.accbSelect");
            flexibleCheckBox.setVisibility(i2);
            ConstraintLayout constraintLayout = this.h.r;
            pq7.b(constraintLayout, "binding.clMainContainer");
            constraintLayout.setVisibility(i2);
            FlexibleTextView flexibleTextView = this.h.t;
            pq7.b(flexibleTextView, "binding.ftvAlphabet");
            flexibleTextView.setVisibility(i2);
            LinearLayout linearLayout = this.h.u;
            pq7.b(linearLayout, "binding.llTextContainer");
            linearLayout.setVisibility(i2);
            FlexibleTextView flexibleTextView2 = this.h.v;
            pq7.b(flexibleTextView2, "binding.pickContactPhone");
            flexibleTextView2.setVisibility(i2);
            FlexibleTextView flexibleTextView3 = this.h.w;
            pq7.b(flexibleTextView3, "binding.pickContactTitle");
            flexibleTextView3.setVisibility(i2);
            View view = this.h.x;
            pq7.b(view, "binding.vLineSeparation");
            view.setVisibility(i2);
            ConstraintLayout constraintLayout2 = this.h.s;
            pq7.b(constraintLayout2, "binding.clRoot");
            constraintLayout2.setVisibility(i2);
        }
    }

    /*
    static {
        String simpleName = hx5.class.getSimpleName();
        pq7.b(simpleName, "CursorContactSearchAdapter::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hx5(Cursor cursor, List<j06> list) {
        super(cursor);
        pq7.c(list, "mContactWrapperList");
        this.t = list;
    }

    @DexIgnore
    public int getPositionForSection(int i2) {
        try {
            AlphabetIndexer alphabetIndexer = this.s;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getPositionForSection(i2);
            }
            pq7.i();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public int getSectionForPosition(int i2) {
        try {
            AlphabetIndexer alphabetIndexer = this.s;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getSectionForPosition(i2);
            }
            pq7.i();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public Object[] getSections() {
        AlphabetIndexer alphabetIndexer = this.s;
        if (alphabetIndexer != null) {
            return alphabetIndexer.getSections();
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.jx5
    public Cursor l(Cursor cursor) {
        if (cursor == null || cursor.isClosed()) {
            return null;
        }
        AlphabetIndexer alphabetIndexer = new AlphabetIndexer(cursor, cursor.getColumnIndex("display_name"), "#ABCDEFGHIJKLMNOPQRTSUVWXYZ");
        this.s = alphabetIndexer;
        if (alphabetIndexer != null) {
            alphabetIndexer.setCursor(cursor);
            this.i.clear();
            this.j.clear();
            this.k = -1;
            this.l = -1;
            return super.l(cursor);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void v(String str) {
        pq7.c(str, "constraint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = u;
        local.d(str2, "filter: constraint = " + str);
        getFilter().filter(str);
    }

    @DexIgnore
    /* renamed from: w */
    public void i(b bVar, Cursor cursor, int i2) {
        pq7.c(bVar, "holder");
        if (cursor != null) {
            bVar.d(cursor, i2);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: x */
    public b onCreateViewHolder(ViewGroup viewGroup, int i2) {
        pq7.c(viewGroup, "parent");
        ae5 z = ae5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemContactBinding.infla\u2026.context), parent, false)");
        return new b(this, z);
    }

    @DexIgnore
    public final void y(a aVar) {
        pq7.c(aVar, "itemClickListener");
        this.m = aVar;
    }
}
