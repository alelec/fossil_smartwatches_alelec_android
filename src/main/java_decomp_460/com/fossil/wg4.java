package com.fossil;

import com.google.firebase.installations.FirebaseInstallationsRegistrar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class wg4 implements d74 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ wg4 f3934a; // = new wg4();

    @DexIgnore
    public static d74 b() {
        return f3934a;
    }

    @DexIgnore
    @Override // com.fossil.d74
    public Object a(b74 b74) {
        return FirebaseInstallationsRegistrar.lambda$getComponents$0(b74);
    }
}
