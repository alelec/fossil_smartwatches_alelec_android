package com.fossil;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l33 extends r33 {
    @DexIgnore
    public /* final */ /* synthetic */ g33 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l33(g33 g33) {
        super(g33, null);
        this.c = g33;
    }

    @DexIgnore
    public /* synthetic */ l33(g33 g33, j33 j33) {
        this(g33);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, com.fossil.r33, java.util.Collection, java.util.Set, java.lang.Iterable
    public final Iterator<Map.Entry<K, V>> iterator() {
        return new i33(this.c, null);
    }
}
