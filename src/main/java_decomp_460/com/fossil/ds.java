package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ds {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ fs f827a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public ds(fs fsVar) {
        this.f827a = fsVar;
    }

    @DexIgnore
    public void a(n7 n7Var) {
        if (n7Var instanceof o7) {
            o7 o7Var = (o7) n7Var;
            if (this.f827a.a(o7Var) > 0) {
                fs fsVar = this.f827a;
                fsVar.f(fsVar.a(o7Var));
                fs fsVar2 = this.f827a;
                fsVar2.n(fsVar2.p);
                fs fsVar3 = this.f827a;
                fsVar3.g.add(new hw(0, o7Var.f2638a, o7Var.b, g80.k(new JSONObject(), jd0.i1, Long.valueOf(this.f827a.x())), 1));
                return;
            }
        }
        this.f827a.h(n7Var);
    }
}
