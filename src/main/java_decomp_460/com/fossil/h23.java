package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h23<K, V> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ l43 f1414a;
    @DexIgnore
    public /* final */ K b;
    @DexIgnore
    public /* final */ l43 c;
    @DexIgnore
    public /* final */ V d;
}
