package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.service.syncmodel.WrapperTapEventSummary;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bs5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f497a;
    @DexIgnore
    public static /* final */ bs5 b; // = new bs5();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ long f498a;
        @DexIgnore
        public /* final */ List<WrapperTapEventSummary> b;
        @DexIgnore
        public /* final */ List<ActivitySample> c;
        @DexIgnore
        public /* final */ List<GFitSample> d;
        @DexIgnore
        public /* final */ List<ActivitySummary> e;
        @DexIgnore
        public /* final */ List<MFSleepSession> f;

        @DexIgnore
        public a(long j, List<WrapperTapEventSummary> list, List<ActivitySample> list2, List<GFitSample> list3, List<ActivitySummary> list4, List<MFSleepSession> list5) {
            pq7.c(list, "tapEventSummaryList");
            pq7.c(list2, "sampleRawList");
            pq7.c(list3, "gFitSampleList");
            pq7.c(list4, "summaryList");
            pq7.c(list5, "sleepSessionList");
            this.f498a = j;
            this.b = list;
            this.c = list2;
            this.d = list3;
            this.e = list4;
            this.f = list5;
        }

        @DexIgnore
        public final List<GFitSample> a() {
            return this.d;
        }

        @DexIgnore
        public final long b() {
            return this.f498a;
        }

        @DexIgnore
        public final List<ActivitySample> c() {
            return this.c;
        }

        @DexIgnore
        public final List<MFSleepSession> d() {
            return this.f;
        }

        @DexIgnore
        public final List<ActivitySummary> e() {
            return this.e;
        }

        @DexIgnore
        public final List<WrapperTapEventSummary> f() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing", f = "HybridSyncDataProcessing.kt", l = {308}, m = "processGoalTrackingData")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bs5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(bs5 bs5, qn7 qn7) {
            super(qn7);
            this.this$0 = bs5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(null, null, null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing", f = "HybridSyncDataProcessing.kt", l = {122, 133, 148, 156, 166, 176, 183}, m = "saveSyncResult")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$13;
        @DexIgnore
        public Object L$14;
        @DexIgnore
        public Object L$15;
        @DexIgnore
        public Object L$16;
        @DexIgnore
        public Object L$17;
        @DexIgnore
        public Object L$18;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bs5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(bs5 bs5, qn7 qn7) {
            super(qn7);
            this.this$0 = bs5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g(null, null, null, null, null, null, null, null, null, null, null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2", f = "HybridSyncDataProcessing.kt", l = {137}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super xw7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ a $finalResult;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository $thirdPartyRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2$1", f = "HybridSyncDataProcessing.kt", l = {138, 139}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super xw7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $listGFitSample;
            @DexIgnore
            public /* final */ /* synthetic */ List $listSleepSession;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, List list, List list2, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
                this.$listGFitSample = list;
                this.$listSleepSession = list2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$listGFitSample, this.$listSleepSession, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super xw7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                iv7 iv7;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv72 = this.p$;
                    ThirdPartyRepository thirdPartyRepository = this.this$0.$thirdPartyRepository;
                    List<GFitSample> list = this.$listGFitSample;
                    List<GFitHeartRate> e = hm7.e();
                    List<GFitWorkoutSession> e2 = hm7.e();
                    List<MFSleepSession> list2 = this.$listSleepSession;
                    this.L$0 = iv72;
                    this.label = 1;
                    if (thirdPartyRepository.saveData(list, null, e, e2, list2, this) == d) {
                        return d;
                    }
                    iv7 = iv72;
                } else if (i == 1) {
                    iv7 = (iv7) this.L$0;
                    el7.b(obj);
                } else if (i == 2) {
                    iv7 iv73 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ThirdPartyRepository thirdPartyRepository2 = this.this$0.$thirdPartyRepository;
                this.L$0 = iv7;
                this.label = 2;
                Object uploadData$default = ThirdPartyRepository.uploadData$default(thirdPartyRepository2, null, this, 1, null);
                return uploadData$default == d ? d : uploadData$default;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(a aVar, ThirdPartyRepository thirdPartyRepository, qn7 qn7) {
            super(2, qn7);
            this.$finalResult = aVar;
            this.$thirdPartyRepository = thirdPartyRepository;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.$finalResult, this.$thirdPartyRepository, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super xw7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                List<GFitSample> a2 = this.$finalResult.a();
                List<MFSleepSession> d2 = this.$finalResult.d();
                dv7 b = bw7.b();
                a aVar = new a(this, a2, d2, null);
                this.L$0 = iv7;
                this.L$1 = a2;
                this.L$2 = d2;
                this.label = 1;
                Object g = eu7.g(b, aVar, this);
                return g == d ? d : g;
            } else if (i == 1) {
                List list = (List) this.L$2;
                List list2 = (List) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3", f = "HybridSyncDataProcessing.kt", l = {192}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super ActivityStatistic>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivitiesRepository $activityRepository;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSessionsRepository $sleepSessionsRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository $sleepSummariesRepository;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $startDate;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository $summaryRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3$1", f = "HybridSyncDataProcessing.kt", l = {193, 194, 196, 197, 198}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super ActivityStatistic>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ActivityStatistic> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:18:0x0076  */
            /* JADX WARNING: Removed duplicated region for block: B:22:0x00b3  */
            /* JADX WARNING: Removed duplicated region for block: B:26:0x00ec  */
            /* JADX WARNING: Removed duplicated region for block: B:30:0x012a  */
            /* JADX WARNING: Removed duplicated region for block: B:31:0x012d  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r14) {
                /*
                // Method dump skipped, instructions count: 306
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.bs5.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(List list, dr7 dr7, dr7 dr72, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, SleepSessionsRepository sleepSessionsRepository, SleepSummariesRepository sleepSummariesRepository, qn7 qn7) {
            super(2, qn7);
            this.$fitnessDataList = list;
            this.$startDate = dr7;
            this.$endDate = dr72;
            this.$activityRepository = activitiesRepository;
            this.$summaryRepository = summariesRepository;
            this.$sleepSessionsRepository = sleepSessionsRepository;
            this.$sleepSummariesRepository = sleepSummariesRepository;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.$fitnessDataList, this.$startDate, this.$endDate, this.$activityRepository, this.$summaryRepository, this.$sleepSessionsRepository, this.$sleepSummariesRepository, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super ActivityStatistic> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                for (FitnessDataWrapper fitnessDataWrapper : this.$fitnessDataList) {
                    if (fitnessDataWrapper.getStartLongTime() < this.$startDate.element.getMillis()) {
                        this.$startDate.element = (T) fitnessDataWrapper.getStartTimeTZ();
                    }
                    if (fitnessDataWrapper.getEndLongTime() > this.$endDate.element.getMillis()) {
                        this.$endDate.element = (T) fitnessDataWrapper.getEndTimeTZ();
                    }
                }
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                Object g = eu7.g(b, aVar, this);
                return g == d ? d : g;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    /*
    static {
        String simpleName = bs5.class.getSimpleName();
        pq7.b(simpleName, "HybridSyncDataProcessing::class.java.simpleName");
        f497a = simpleName;
    }
    */

    @DexIgnore
    public final String a(List<ActivitySample> list) {
        if (list.isEmpty()) {
            return "empty\n";
        }
        StringBuilder sb = new StringBuilder();
        for (ActivitySample activitySample : list) {
            sb.append("[");
            sb.append("startTime:");
            sb.append(activitySample.getStartTime());
            sb.append(", step:");
            sb.append(activitySample.getSteps());
            sb.append("]");
        }
        String sb2 = sb.toString();
        pq7.b(sb2, "buffer.toString()");
        return sb2;
    }

    @DexIgnore
    public final String b(List<MFSleepSession> list) {
        if (list.isEmpty()) {
            return "empty\n";
        }
        StringBuilder sb = new StringBuilder();
        for (MFSleepSession mFSleepSession : list) {
            sb.append(mFSleepSession.toString());
            sb.append("\n");
            sb.append("State: ");
            String sleepStates = mFSleepSession.getSleepStates();
            int length = sleepStates.length();
            for (int i = 0; i < length; i++) {
                sb.append(String.valueOf(sleepStates.charAt(i)));
                sb.append(" -- ");
            }
            sb.append("\n");
        }
        sb.append("\n");
        String sb2 = sb.toString();
        pq7.b(sb2, "buffer.toString()");
        return sb2;
    }

    @DexIgnore
    public final a c(String str, List<FitnessDataWrapper> list, MFUser mFUser, UserProfile userProfile, long j, long j2, PortfolioApp portfolioApp) {
        pq7.c(str, "serial");
        pq7.c(list, "syncData");
        pq7.c(mFUser, "user");
        pq7.c(userProfile, "userProfile");
        pq7.c(portfolioApp, "portfolioApp");
        FLogger.INSTANCE.getLocal().d(f497a, ".buildSyncResult(), get all data files, synctime=" + j + ", data=" + list);
        a aVar = new a(j2, new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList());
        if (!list.isEmpty()) {
            portfolioApp.p(CommunicateMode.SYNC, str, "Calculating sleep and activity...");
            gl7<List<ActivitySample>, List<ActivitySummary>, List<GFitSample>> d2 = k47.d(list, str, mFUser.getUserId(), 1000 * j);
            List<ActivitySample> first = d2.getFirst();
            List<ActivitySummary> second = d2.getSecond();
            List<GFitSample> third = d2.getThird();
            List<MFSleepSession> e2 = k47.e(list, str);
            List<WrapperTapEventSummary> c2 = k47.c(list);
            int size = e2.size();
            double d3 = 0.0d;
            double d4 = 0.0d;
            double d5 = 0.0d;
            double d6 = 0.0d;
            for (T t : first) {
                d5 += t.getCalories();
                d4 += t.getDistance();
                d3 += t.getSteps();
                t.getActiveTime();
                Boolean p0 = lk5.p0(t.getDate());
                pq7.b(p0, "DateHelper.isToday(it.date)");
                if (p0.booleanValue()) {
                    d6 += t.getSteps();
                    t.getActiveTime();
                }
            }
            hr7 hr7 = hr7.f1520a;
            String format = String.format("Done calculating sleep: total %s sleep session(s)", Arrays.copyOf(new Object[]{String.valueOf(size)}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            f(str, format);
            hr7 hr72 = hr7.f1520a;
            String format2 = String.format("After calculation: steps=%s, todayStep=%s, realTimeSteps=%s, lastRealtimeSteps=%s. Calories=%s. DistanceWrapper=%s. Taps=%s", Arrays.copyOf(new Object[]{Double.valueOf(d3), Double.valueOf(d6), Long.valueOf(j2), Long.valueOf(userProfile.getCurrentSteps()), Double.valueOf(d5), Double.valueOf(d4), d(c2)}, 7));
            pq7.b(format2, "java.lang.String.format(format, *args)");
            f(str, format2);
            FLogger.INSTANCE.getLocal().d(f497a, "Release=" + tk5.d());
            if (!tk5.d()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = f497a;
                StringBuilder sb = new StringBuilder();
                sb.append("onSyncCompleted - Sleep session details: ");
                sb.append(e2.isEmpty() ? b(e2) : ", no sleep data");
                local.d(str2, sb.toString());
                FLogger.INSTANCE.getLocal().d(f497a, "onSyncCompleted - Minute data details: " + a(first));
                FLogger.INSTANCE.getLocal().d(f497a, "onSyncCompleted - Tap event details: " + d(c2));
            }
            return new a(j2, c2, first, third, second, e2);
        }
        f(str, "Sync data is empty");
        return aVar;
    }

    @DexIgnore
    public final String d(List<? extends WrapperTapEventSummary> list) {
        if (list == null || list.isEmpty()) {
            return "null\n";
        }
        StringBuilder sb = new StringBuilder();
        for (WrapperTapEventSummary wrapperTapEventSummary : list) {
            sb.append("[startTime:");
            sb.append(wrapperTapEventSummary.startTime);
            sb.append(", timezoneOffsetInSecond=");
            sb.append(wrapperTapEventSummary.timezoneOffsetInSecond);
            sb.append(", goalTrackingIds=");
            sb.append(wrapperTapEventSummary.goalId);
            sb.append("]");
            sb.append("\n");
        }
        String sb2 = sb.toString();
        pq7.b(sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object e(java.util.List<? extends com.portfolio.platform.service.syncmodel.WrapperTapEventSummary> r19, java.lang.String r20, com.portfolio.platform.data.source.HybridPresetRepository r21, com.portfolio.platform.data.source.GoalTrackingRepository r22, com.portfolio.platform.data.source.UserRepository r23, com.fossil.qn7<? super com.fossil.tl7> r24) {
        /*
        // Method dump skipped, instructions count: 367
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bs5.e(java.util.List, java.lang.String, com.portfolio.platform.data.source.HybridPresetRepository, com.portfolio.platform.data.source.GoalTrackingRepository, com.portfolio.platform.data.source.UserRepository, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void f(String str, String str2) {
        PortfolioApp.h0.c().p(CommunicateMode.SYNC, str, str2);
        FLogger.INSTANCE.getLocal().d(f497a, str2);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str, f497a, str2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:103:0x04e6  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x04ea  */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0597  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x05c0  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x06eb  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x070f  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x072c  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:192:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:195:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:196:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x019f  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0245  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x028b  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0301 A[Catch:{ Exception -> 0x0742 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0379  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0024  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x041c  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0446  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g(com.fossil.bs5.a r37, java.lang.String r38, com.portfolio.platform.data.source.SleepSessionsRepository r39, com.portfolio.platform.data.source.SummariesRepository r40, com.portfolio.platform.data.source.SleepSummariesRepository r41, com.portfolio.platform.data.source.FitnessDataRepository r42, com.portfolio.platform.data.source.ActivitiesRepository r43, com.portfolio.platform.data.source.HybridPresetRepository r44, com.portfolio.platform.data.source.GoalTrackingRepository r45, com.portfolio.platform.data.source.ThirdPartyRepository r46, com.portfolio.platform.data.source.UserRepository r47, com.portfolio.platform.PortfolioApp r48, com.fossil.sk5 r49, com.fossil.qn7<? super com.fossil.tl7> r50) {
        /*
        // Method dump skipped, instructions count: 1910
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bs5.g(com.fossil.bs5$a, java.lang.String, com.portfolio.platform.data.source.SleepSessionsRepository, com.portfolio.platform.data.source.SummariesRepository, com.portfolio.platform.data.source.SleepSummariesRepository, com.portfolio.platform.data.source.FitnessDataRepository, com.portfolio.platform.data.source.ActivitiesRepository, com.portfolio.platform.data.source.HybridPresetRepository, com.portfolio.platform.data.source.GoalTrackingRepository, com.portfolio.platform.data.source.ThirdPartyRepository, com.portfolio.platform.data.source.UserRepository, com.portfolio.platform.PortfolioApp, com.fossil.sk5, com.fossil.qn7):java.lang.Object");
    }
}
