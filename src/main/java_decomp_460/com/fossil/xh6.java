package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xh6 extends uh6 {
    @DexIgnore
    public /* final */ MutableLiveData<Date> e; // = new MutableLiveData<>();
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g;
    @DexIgnore
    public List<GoalTrackingSummary> h; // = new ArrayList();
    @DexIgnore
    public /* final */ LiveData<h47<List<GoalTrackingSummary>>> i;
    @DexIgnore
    public TreeMap<Long, Float> j;
    @DexIgnore
    public /* final */ vh6 k;
    @DexIgnore
    public /* final */ UserRepository l;
    @DexIgnore
    public /* final */ on5 m;
    @DexIgnore
    public /* final */ GoalTrackingRepository n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter$loadData$1", f = "GoalTrackingOverviewMonthPresenter.kt", l = {95}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xh6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xh6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter$loadData$1$currentUser$1", f = "GoalTrackingOverviewMonthPresenter.kt", l = {95}, m = "invokeSuspend")
        /* renamed from: com.fossil.xh6$a$a  reason: collision with other inner class name */
        public static final class C0282a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0282a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0282a aVar = new C0282a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                return ((C0282a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.l;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(xh6 xh6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = xh6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 h = this.this$0.h();
                C0282a aVar = new C0282a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(h, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) g;
            if (mFUser != null) {
                this.this$0.g = lk5.q0(mFUser.getCreatedAt());
                vh6 vh6 = this.this$0.k;
                Date date = this.this$0.f;
                if (date != null) {
                    Date date2 = this.this$0.g;
                    if (date2 == null) {
                        date2 = new Date();
                    }
                    vh6.g(date, date2);
                    this.this$0.e.l(this.this$0.f);
                } else {
                    pq7.i();
                    throw null;
                }
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xh6 f4125a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter$mGoalTrackingSummaries$1$1", f = "GoalTrackingOverviewMonthPresenter.kt", l = {44, 44}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<GoalTrackingSummary>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<GoalTrackingSummary>>> hs0, qn7<? super tl7> qn7) {
                return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x004e  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r14) {
                /*
                // Method dump skipped, instructions count: 289
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.xh6.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public b(xh6 xh6) {
            this.f4125a = xh6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<GoalTrackingSummary>>> apply(Date date) {
            return or0.c(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<h47<? extends List<GoalTrackingSummary>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xh6 f4126a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter$start$1$1", f = "GoalTrackingOverviewMonthPresenter.kt", l = {66}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xh6$c$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter$start$1$1$1", f = "GoalTrackingOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.xh6$c$a$a  reason: collision with other inner class name */
            public static final class C0283a extends ko7 implements vp7<iv7, qn7<? super TreeMap<Long, Float>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0283a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0283a aVar = new C0283a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super TreeMap<Long, Float>> qn7) {
                    return ((C0283a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        xh6 xh6 = this.this$0.this$0.f4126a;
                        Object e = xh6.e.e();
                        if (e != null) {
                            pq7.b(e, "mDateLiveData.value!!");
                            return xh6.F((Date) e, this.this$0.$data);
                        }
                        pq7.i();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, List list, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$data = list;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$data, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                xh6 xh6;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    xh6 xh62 = this.this$0.f4126a;
                    dv7 h = xh62.h();
                    C0283a aVar = new C0283a(this, null);
                    this.L$0 = iv7;
                    this.L$1 = xh62;
                    this.label = 1;
                    g = eu7.g(h, aVar, this);
                    if (g == d) {
                        return d;
                    }
                    xh6 = xh62;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    xh6 = (xh6) this.L$1;
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                xh6.j = (TreeMap) g;
                vh6 vh6 = this.this$0.f4126a.k;
                TreeMap<Long, Float> treeMap = this.this$0.f4126a.j;
                if (treeMap == null) {
                    treeMap = new TreeMap<>();
                }
                vh6.e(treeMap);
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public c(xh6 xh6) {
            this.f4126a = xh6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<GoalTrackingSummary>> h47) {
            xh5 a2 = h47.a();
            List list = (List) h47.b();
            FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthPresenter", "mDateTransformations observer");
            if (a2 != xh5.DATABASE_LOADING) {
                if (list != null && (!pq7.a(this.f4126a.h, list))) {
                    this.f4126a.h = list;
                    xw7 unused = gu7.d(this.f4126a.k(), null, null, new a(this, list, null), 3, null);
                }
                this.f4126a.k.x(!this.f4126a.m.g0());
            }
        }
    }

    @DexIgnore
    public xh6(vh6 vh6, UserRepository userRepository, on5 on5, GoalTrackingRepository goalTrackingRepository) {
        pq7.c(vh6, "mView");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(goalTrackingRepository, "mGoalTrackingRepository");
        this.k = vh6;
        this.l = userRepository;
        this.m = on5;
        this.n = goalTrackingRepository;
        LiveData<h47<List<GoalTrackingSummary>>> c2 = ss0.c(this.e, new b(this));
        pq7.b(c2, "Transformations.switchMa\u2026        }\n        }\n    }");
        this.i = c2;
    }

    @DexIgnore
    public void D() {
        Date date = this.f;
        if (date == null || !lk5.p0(date).booleanValue()) {
            this.f = new Date();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewMonthPresenter", "loadData - mDateLiveData=" + this.f);
            xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
            return;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("GoalTrackingOverviewMonthPresenter", "loadData - mDateLiveData=" + this.f);
    }

    @DexIgnore
    public void E() {
        this.k.M5(this);
    }

    @DexIgnore
    public final TreeMap<Long, Float> F(Date date, List<GoalTrackingSummary> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("GoalTrackingOverviewMonthPresenter", sb.toString());
        TreeMap<Long, Float> treeMap = new TreeMap<>();
        if (list != null) {
            for (GoalTrackingSummary goalTrackingSummary : list) {
                Date component1 = goalTrackingSummary.component1();
                int component2 = goalTrackingSummary.component2();
                int component3 = goalTrackingSummary.component3();
                if (component3 > 0) {
                    Date V = lk5.V(component1);
                    pq7.b(V, "DateHelper.getStartOfDay(date1)");
                    treeMap.put(Long.valueOf(V.getTime()), Float.valueOf(((float) component2) / ((float) component3)));
                } else {
                    Date V2 = lk5.V(component1);
                    pq7.b(V2, "DateHelper.getStartOfDay(date1)");
                    treeMap.put(Long.valueOf(V2.getTime()), Float.valueOf((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                }
            }
        }
        return treeMap;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        D();
        LiveData<h47<List<GoalTrackingSummary>>> liveData = this.i;
        vh6 vh6 = this.k;
        if (vh6 != null) {
            liveData.h((wh6) vh6, new c(this));
            this.k.x(!this.m.g0());
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthPresenter", "stop");
        try {
            LiveData<h47<List<GoalTrackingSummary>>> liveData = this.i;
            vh6 vh6 = this.k;
            if (vh6 != null) {
                liveData.n((wh6) vh6);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewMonthPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.uh6
    public void n(Date date) {
        pq7.c(date, "date");
        if (this.e.e() == null || !lk5.m0(this.e.e(), date)) {
            this.e.l(date);
        }
    }
}
