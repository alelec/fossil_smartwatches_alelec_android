package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.source.ThemeRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cr6 extends ts0 {
    @DexIgnore
    public static /* final */ String h;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ArrayList<Theme> f643a; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<Style> b; // = new ArrayList<>();
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public MutableLiveData<a> e; // = new MutableLiveData<>();
    @DexIgnore
    public a f; // = new a(null, null, null, null, false, 31, null);
    @DexIgnore
    public /* final */ ThemeRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public ArrayList<Theme> f644a;
        @DexIgnore
        public List<Style> b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public boolean e;

        @DexIgnore
        public a() {
            this(null, null, null, null, false, 31, null);
        }

        @DexIgnore
        public a(ArrayList<Theme> arrayList, List<Style> list, String str, String str2, boolean z) {
            this.f644a = arrayList;
            this.b = list;
            this.c = str;
            this.d = str2;
            this.e = z;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(ArrayList arrayList, List list, String str, String str2, boolean z, int i, kq7 kq7) {
            this((i & 1) != 0 ? null : arrayList, (i & 2) != 0 ? null : list, (i & 4) != 0 ? null : str, (i & 8) == 0 ? str2 : null, (i & 16) != 0 ? false : z);
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final boolean b() {
            return this.e;
        }

        @DexIgnore
        public final ArrayList<Theme> c() {
            return this.f644a;
        }

        @DexIgnore
        public final List<Style> d() {
            return this.b;
        }

        @DexIgnore
        public final String e() {
            return this.d;
        }

        @DexIgnore
        public final void f(boolean z) {
            this.e = z;
        }

        @DexIgnore
        public final void g(ArrayList<Theme> arrayList, String str, String str2, List<Style> list) {
            this.f644a = arrayList;
            this.d = str2;
            this.c = str;
            this.e = false;
            this.b = list;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$applyTheme$1", f = "ThemesViewModel.kt", l = {66}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ cr6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$applyTheme$1$1", f = "ThemesViewModel.kt", l = {68, 69}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x0049  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r1 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x004b
                    if (r0 == r6) goto L_0x0032
                    if (r0 != r7) goto L_0x002a
                    java.lang.Object r0 = r8.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r9)
                L_0x0015:
                    com.fossil.cr6$b r0 = r8.this$0
                    com.fossil.cr6 r0 = r0.this$0
                    com.fossil.cr6$a r0 = com.fossil.cr6.f(r0)
                    r0.f(r6)
                    com.fossil.cr6$b r0 = r8.this$0
                    com.fossil.cr6 r0 = r0.this$0
                    com.fossil.cr6.a(r0)
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0029:
                    return r0
                L_0x002a:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0032:
                    java.lang.Object r0 = r8.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r9)
                L_0x0039:
                    com.fossil.qn5$a r2 = com.fossil.qn5.l
                    com.fossil.qn5 r2 = r2.a()
                    r8.L$0 = r0
                    r8.label = r7
                    java.lang.Object r0 = r2.i(r8)
                    if (r0 != r1) goto L_0x0015
                    r0 = r1
                    goto L_0x0029
                L_0x004b:
                    com.fossil.el7.b(r9)
                    com.fossil.iv7 r0 = r8.p$
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    java.lang.String r3 = com.fossil.cr6.h()
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "apply theme "
                    r4.append(r5)
                    com.fossil.cr6$b r5 = r8.this$0
                    com.fossil.cr6 r5 = r5.this$0
                    java.lang.String r5 = com.fossil.cr6.g(r5)
                    r4.append(r5)
                    java.lang.String r4 = r4.toString()
                    r2.d(r3, r4)
                    com.fossil.cr6$b r2 = r8.this$0
                    com.fossil.cr6 r2 = r2.this$0
                    com.portfolio.platform.data.source.ThemeRepository r2 = com.fossil.cr6.e(r2)
                    com.fossil.cr6$b r3 = r8.this$0
                    com.fossil.cr6 r3 = r3.this$0
                    java.lang.String r3 = com.fossil.cr6.g(r3)
                    r8.L$0 = r0
                    r8.label = r6
                    java.lang.Object r2 = r2.setCurrentThemeId(r3, r8)
                    if (r2 != r1) goto L_0x0039
                    r0 = r1
                    goto L_0x0029
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.cr6.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(cr6 cr6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cr6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(b, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$deleteTheme$1", f = "ThemesViewModel.kt", l = {79, 80, 82, 83, 85}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ cr6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(cr6 cr6, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cr6;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$id, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x00d3  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00e9  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0124  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x013b  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0164  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0167  */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x016b  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0175  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x017d  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 394
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.cr6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$onThemeChange$1", f = "ThemesViewModel.kt", l = {50}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $themeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ cr6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$onThemeChange$1$1", f = "ThemesViewModel.kt", l = {51}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super ArrayList<Style>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ArrayList<Style>> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object listStyleById;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    ThemeRepository themeRepository = this.this$0.this$0.g;
                    String str = this.this$0.$themeId;
                    this.L$0 = iv7;
                    this.label = 1;
                    listStyleById = themeRepository.getListStyleById(str, this);
                    if (listStyleById == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    listStyleById = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ArrayList arrayList = (ArrayList) listStyleById;
                return arrayList != null ? arrayList : new ArrayList();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(cr6 cr6, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cr6;
            this.$themeId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$themeId, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            cr6 cr6;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.d = this.$themeId;
                cr6 cr62 = this.this$0;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.L$1 = cr62;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
                cr6 = cr62;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                cr6 = (cr6) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            cr6.b = (ArrayList) g;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = cr6.h;
            local.d(str, "userSelectedId " + this.this$0.d + " currentThemeId " + this.this$0.c);
            this.this$0.f.g(this.this$0.f643a, this.this$0.c, this.this$0.d, this.this$0.b);
            this.this$0.o();
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$start$1", f = "ThemesViewModel.kt", l = {27}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ cr6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$start$1$1", f = "ThemesViewModel.kt", l = {28, 29, 31}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:16:0x006d  */
            /* JADX WARNING: Removed duplicated region for block: B:19:0x007e  */
            /* JADX WARNING: Removed duplicated region for block: B:25:0x00b6  */
            /* JADX WARNING: Removed duplicated region for block: B:26:0x00b9  */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x00c3  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                // Method dump skipped, instructions count: 208
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.cr6.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(cr6 cr6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cr6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(b, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = cr6.h;
            local.d(str, "mCurrentThemeId=" + this.this$0.c);
            this.this$0.f.g(this.this$0.f643a, this.this$0.c, this.this$0.d, this.this$0.b);
            this.this$0.o();
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = cr6.class.getSimpleName();
        pq7.b(simpleName, "ThemesViewModel::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public cr6(ThemeRepository themeRepository) {
        pq7.c(themeRepository, "mThemesRepository");
        this.g = themeRepository;
    }

    @DexIgnore
    public final void m() {
        xw7 unused = gu7.d(us0.a(this), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    public final void n(String str) {
        pq7.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = h;
        local.d(str2, "deleteTheme id=" + str);
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new c(this, str, null), 3, null);
    }

    @DexIgnore
    public final void o() {
        this.e.l(this.f);
    }

    @DexIgnore
    public final MutableLiveData<a> p() {
        return this.e;
    }

    @DexIgnore
    public final void q(String str) {
        pq7.c(str, "themeId");
        xw7 unused = gu7.d(us0.a(this), null, null, new d(this, str, null), 3, null);
    }

    @DexIgnore
    public final void r() {
        xw7 unused = gu7.d(us0.a(this), null, null, new e(this, null), 3, null);
    }
}
