package com.fossil;

import android.graphics.Bitmap;
import java.util.LinkedHashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a97 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Map<String, Bitmap> f229a; // = new LinkedHashMap();
    @DexIgnore
    public static /* final */ a97 b; // = new a97();

    @DexIgnore
    public final void a(String str, Bitmap bitmap) {
        pq7.c(str, "id");
        pq7.c(bitmap, "bitmap");
        f229a.put(str, bitmap);
    }

    @DexIgnore
    public final void b() {
        f229a.clear();
    }

    @DexIgnore
    public final Bitmap c(String str) {
        pq7.c(str, "id");
        return f229a.get(str);
    }

    @DexIgnore
    public final boolean d() {
        return !f229a.isEmpty();
    }
}
