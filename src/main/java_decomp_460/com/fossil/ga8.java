package com.fossil;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.common.constants.Constants;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ga8 implements MethodChannel.MethodCallHandler {
    @DexIgnore
    public static /* final */ ThreadPoolExecutor f; // = new ThreadPoolExecutor(11, 1000, 200, TimeUnit.MINUTES, new ArrayBlockingQueue(11));
    @DexIgnore
    public static boolean g; // = true;
    @DexIgnore
    public static /* final */ c h; // = new c(null);
    @DexIgnore
    public /* final */ sa8 b; // = new sa8();
    @DexIgnore
    public /* final */ fa8 c; // = new fa8(this.e, new Handler());
    @DexIgnore
    public /* final */ ea8 d;
    @DexIgnore
    public /* final */ PluginRegistry.Registrar e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements PluginRegistry.RequestPermissionsResultListener {
        @DexIgnore
        public /* final */ /* synthetic */ ga8 b;

        @DexIgnore
        public a(ga8 ga8) {
            this.b = ga8;
        }

        @DexIgnore
        @Override // io.flutter.plugin.common.PluginRegistry.RequestPermissionsResultListener
        public final boolean onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
            this.b.b.c(i, strArr, iArr);
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ra8 {
        @DexIgnore
        @Override // com.fossil.ra8
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.ra8
        public void b(List<String> list, List<String> list2) {
            pq7.c(list, "deniedPermissions");
            pq7.c(list2, "grantedPermissions");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public /* synthetic */ c(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final boolean a() {
            return ga8.g;
        }

        @DexIgnore
        public final void b(gp7<tl7> gp7) {
            pq7.c(gp7, "runnable");
            ga8.f.execute(new ha8(gp7));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ga8 ga8, MethodCall methodCall, ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            Object argument = this.$call.argument("ids");
            if (argument != null) {
                pq7.b(argument, "call.argument<List<String>>(\"ids\")!!");
                this.$resultHandler.c(this.this$0.d.c((List) argument));
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ga8 ga8, MethodCall methodCall, ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            try {
                Object argument = this.$call.argument("image");
                if (argument != null) {
                    pq7.b(argument, "call.argument<ByteArray>(\"image\")!!");
                    byte[] bArr = (byte[]) argument;
                    String str = (String) this.$call.argument("title");
                    String str2 = str != null ? str : "";
                    pq7.b(str2, "call.argument<String>(\"title\") ?: \"\"");
                    String str3 = (String) this.$call.argument(Constants.DESC);
                    if (str3 == null) {
                        str3 = "";
                    }
                    pq7.b(str3, "call.argument<String>(\"desc\") ?: \"\"");
                    ka8 n = this.this$0.d.n(bArr, str2, str3);
                    if (n == null) {
                        this.$resultHandler.c(null);
                    } else {
                        this.$resultHandler.c(oa8.f2663a.c(n));
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } catch (Exception e) {
                xa8.a("save image error", e);
                this.$resultHandler.c(null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ga8 ga8, MethodCall methodCall, ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            try {
                Object argument = this.$call.argument("path");
                if (argument != null) {
                    pq7.b(argument, "call.argument<String>(\"path\")!!");
                    String str = (String) argument;
                    Object argument2 = this.$call.argument("title");
                    if (argument2 != null) {
                        pq7.b(argument2, "call.argument<String>(\"title\")!!");
                        String str2 = (String) argument2;
                        String str3 = (String) this.$call.argument(Constants.DESC);
                        if (str3 == null) {
                            str3 = "";
                        }
                        pq7.b(str3, "call.argument<String>(\"desc\") ?: \"\"");
                        ka8 o = this.this$0.d.o(str, str2, str3);
                        if (o == null) {
                            this.$resultHandler.c(null);
                            return;
                        }
                        this.$resultHandler.c(oa8.f2663a.c(o));
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            } catch (Exception e) {
                xa8.a("save video error", e);
                this.$resultHandler.c(null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(ga8 ga8, MethodCall methodCall, ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            Object argument = this.$call.argument("type");
            if (argument != null) {
                pq7.b(argument, "call.argument<Int>(\"type\")!!");
                int intValue = ((Number) argument).intValue();
                long k = this.this$0.k(this.$call);
                Object argument2 = this.$call.argument("hasAll");
                if (argument2 != null) {
                    pq7.b(argument2, "call.argument<Boolean>(\"hasAll\")!!");
                    this.$resultHandler.c(oa8.f2663a.d(this.this$0.d.i(intValue, k, ((Boolean) argument2).booleanValue(), this.this$0.i(this.$call))));
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(ga8 ga8, MethodCall methodCall, ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                pq7.b(argument, "call.argument<String>(\"id\")!!");
                String str = (String) argument;
                Object argument2 = this.$call.argument(PlaceFields.PAGE);
                if (argument2 != null) {
                    pq7.b(argument2, "call.argument<Int>(\"page\")!!");
                    int intValue = ((Number) argument2).intValue();
                    Object argument3 = this.$call.argument("pageCount");
                    if (argument3 != null) {
                        pq7.b(argument3, "call.argument<Int>(\"pageCount\")!!");
                        int intValue2 = ((Number) argument3).intValue();
                        Object argument4 = this.$call.argument("type");
                        if (argument4 != null) {
                            pq7.b(argument4, "call.argument<Int>(\"type\")!!");
                            this.$resultHandler.c(oa8.f2663a.b(this.this$0.d.e(str, intValue, intValue2, ((Number) argument4).intValue(), this.this$0.k(this.$call), this.this$0.i(this.$call))));
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(ga8 ga8, MethodCall methodCall, ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            this.$resultHandler.c(oa8.f2663a.b(this.this$0.d.f(this.this$0.j(this.$call, "galleryId"), this.this$0.h(this.$call, "type"), this.this$0.h(this.$call, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE), this.this$0.h(this.$call, "end"), this.this$0.k(this.$call), this.this$0.i(this.$call))));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(ga8 ga8, MethodCall methodCall, ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                pq7.b(argument, "call.argument<String>(\"id\")!!");
                this.this$0.d.a((String) argument, this.$resultHandler);
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $haveLocationPermission;
        @DexIgnore
        public /* final */ /* synthetic */ ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(ga8 ga8, MethodCall methodCall, boolean z, ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$haveLocationPermission = z;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            boolean booleanValue;
            Object argument = this.$call.argument("id");
            if (argument != null) {
                pq7.b(argument, "call.argument<String>(\"id\")!!");
                String str = (String) argument;
                if (!this.$haveLocationPermission) {
                    booleanValue = false;
                } else {
                    Object argument2 = this.$call.argument("isOrigin");
                    if (argument2 != null) {
                        pq7.b(argument2, "call.argument<Boolean>(\"isOrigin\")!!");
                        booleanValue = ((Boolean) argument2).booleanValue();
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                this.this$0.d.h(str, booleanValue, this.$resultHandler);
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $haveLocationPermission;
        @DexIgnore
        public /* final */ /* synthetic */ ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(ga8 ga8, MethodCall methodCall, boolean z, ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$haveLocationPermission = z;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                pq7.b(argument, "call.argument<String>(\"id\")!!");
                this.this$0.d.k((String) argument, ga8.h.a(), this.$haveLocationPermission, this.$resultHandler);
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(ga8 ga8, MethodCall methodCall, ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                pq7.b(argument, "call.argument<String>(\"id\")!!");
                String str = (String) argument;
                Object argument2 = this.$call.argument("type");
                if (argument2 != null) {
                    pq7.b(argument2, "call.argument<Int>(\"type\")!!");
                    ma8 l = this.this$0.d.l(str, ((Number) argument2).intValue(), this.this$0.k(this.$call), this.this$0.i(this.$call));
                    if (l != null) {
                        this.$resultHandler.c(oa8.f2663a.d(gm7.b(l)));
                        return;
                    }
                    this.$resultHandler.c(null);
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(ga8 ga8, MethodCall methodCall, ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                pq7.b(argument, "call.argument<String>(\"id\")!!");
                this.$resultHandler.c(this.this$0.d.j((String) argument));
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(ga8 ga8, MethodCall methodCall) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            if (pq7.a((Boolean) this.$call.argument("notify"), Boolean.TRUE)) {
                this.this$0.c.d();
            } else {
                this.this$0.c.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements ra8 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ga8 f1285a;
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall b;
        @DexIgnore
        public /* final */ /* synthetic */ ya8 c;

        @DexIgnore
        public p(ga8 ga8, MethodCall methodCall, ya8 ya8) {
            this.f1285a = ga8;
            this.b = methodCall;
            this.c = ya8;
        }

        @DexIgnore
        @Override // com.fossil.ra8
        public void a() {
            this.f1285a.l(this.b, this.c, true);
        }

        @DexIgnore
        @Override // com.fossil.ra8
        public void b(List<String> list, List<String> list2) {
            pq7.c(list, "deniedPermissions");
            pq7.c(list2, "grantedPermissions");
            xa8.b("onDenied call.method = " + this.b.method);
            if (pq7.a(this.b.method, "requestPermission")) {
                this.c.c(0);
                return;
            }
            if (list2.containsAll(hm7.c("android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"))) {
                this.f1285a.l(this.b, this.c, false);
            } else {
                this.f1285a.m(this.c);
            }
        }
    }

    @DexIgnore
    public ga8(PluginRegistry.Registrar registrar) {
        pq7.c(registrar, "registrar");
        this.e = registrar;
        this.e.addRequestPermissionsResultListener(new a(this));
        this.b.i(new b());
        Context context = this.e.context();
        pq7.b(context, "registrar.context()");
        Context applicationContext = context.getApplicationContext();
        pq7.b(applicationContext, "registrar.context().applicationContext");
        this.d = new ea8(applicationContext);
    }

    @DexIgnore
    public final int h(MethodCall methodCall, String str) {
        pq7.c(methodCall, "$this$getInt");
        pq7.c(str, "key");
        Object argument = methodCall.argument(str);
        if (argument != null) {
            return ((Number) argument).intValue();
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final la8 i(MethodCall methodCall) {
        pq7.c(methodCall, "$this$getOption");
        Object argument = methodCall.argument("option");
        if (argument != null) {
            pq7.b(argument, "argument<Map<*, *>>(\"option\")!!");
            return oa8.f2663a.a((Map) argument);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final String j(MethodCall methodCall, String str) {
        pq7.c(methodCall, "$this$getString");
        pq7.c(str, "key");
        Object argument = methodCall.argument(str);
        if (argument != null) {
            return (String) argument;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final long k(MethodCall methodCall) {
        pq7.c(methodCall, "$this$getTimeStamp");
        Object argument = methodCall.argument("timestamp");
        if (argument != null) {
            return ((Number) argument).longValue();
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void l(MethodCall methodCall, ya8 ya8, boolean z) {
        xa8.b("onGranted call.method = " + methodCall.method);
        String str = methodCall.method;
        if (str != null) {
            switch (str.hashCode()) {
                case -1283288098:
                    if (str.equals("getLatLngAndroidQ")) {
                        h.b(new n(this, methodCall, ya8));
                        return;
                    }
                    break;
                case -1039689911:
                    if (str.equals("notify")) {
                        h.b(new o(this, methodCall));
                        return;
                    }
                    break;
                case -886445535:
                    if (str.equals("getFullFile")) {
                        h.b(new k(this, methodCall, z, ya8));
                        return;
                    }
                    break;
                case -151967598:
                    if (str.equals("fetchPathProperties")) {
                        h.b(new m(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 163601886:
                    if (str.equals("saveImage")) {
                        h.b(new e(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 175491326:
                    if (str.equals("saveVideo")) {
                        h.b(new f(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 594039295:
                    if (str.equals("getAssetListWithRange")) {
                        h.b(new i(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 746581438:
                    if (str.equals("requestPermission")) {
                        ya8.c(1);
                        return;
                    }
                    break;
                case 857200492:
                    if (str.equals("assetExists")) {
                        h.b(new j(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 1063055279:
                    if (str.equals("getOriginBytes")) {
                        h.b(new l(this, methodCall, z, ya8));
                        return;
                    }
                    break;
                case 1150344167:
                    if (str.equals("deleteWithIds")) {
                        h.b(new d(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 1505159642:
                    if (str.equals("getGalleryList")) {
                        if (Build.VERSION.SDK_INT >= 29) {
                            this.c.c(true);
                        }
                        h.b(new g(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 1642188493:
                    if (str.equals("getAssetWithGalleryId")) {
                        h.b(new h(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 1966168096:
                    if (str.equals("getThumb")) {
                        Object argument = methodCall.argument("id");
                        if (argument != null) {
                            pq7.b(argument, "call.argument<String>(\"id\")!!");
                            String str2 = (String) argument;
                            Object argument2 = methodCall.argument("width");
                            if (argument2 != null) {
                                pq7.b(argument2, "call.argument<Int>(\"width\")!!");
                                int intValue = ((Number) argument2).intValue();
                                Object argument3 = methodCall.argument("height");
                                if (argument3 != null) {
                                    pq7.b(argument3, "call.argument<Int>(\"height\")!!");
                                    int intValue2 = ((Number) argument3).intValue();
                                    Object argument4 = methodCall.argument("format");
                                    if (argument4 != null) {
                                        pq7.b(argument4, "call.argument<Int>(\"format\")!!");
                                        this.d.m(str2, intValue, intValue2, ((Number) argument4).intValue(), ya8);
                                        return;
                                    }
                                    pq7.i();
                                    throw null;
                                }
                                pq7.i();
                                throw null;
                            }
                            pq7.i();
                            throw null;
                        }
                        pq7.i();
                        throw null;
                    }
                    break;
            }
        }
        ya8.b();
    }

    @DexIgnore
    public final void m(ya8 ya8) {
        ya8.d("Request for permission failed.", "User denied permission.", null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    @Override // io.flutter.plugin.common.MethodChannel.MethodCallHandler
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMethodCall(io.flutter.plugin.common.MethodCall r8, io.flutter.plugin.common.MethodChannel.Result r9) {
        /*
        // Method dump skipped, instructions count: 332
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ga8.onMethodCall(io.flutter.plugin.common.MethodCall, io.flutter.plugin.common.MethodChannel$Result):void");
    }
}
