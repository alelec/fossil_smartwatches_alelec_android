package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class df7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f782a;
    @DexIgnore
    public String b;

    @DexIgnore
    public abstract boolean a();

    @DexIgnore
    public void b(Bundle bundle) {
        this.f782a = xe7.a(bundle, "_wxapi_basereq_transaction");
        this.b = xe7.a(bundle, "_wxapi_basereq_openid");
    }

    @DexIgnore
    public abstract int c();

    @DexIgnore
    public void d(Bundle bundle) {
        bundle.putInt("_wxapi_command_type", c());
        bundle.putString("_wxapi_basereq_transaction", this.f782a);
        bundle.putString("_wxapi_basereq_openid", this.b);
    }
}
