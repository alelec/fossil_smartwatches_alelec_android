package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j41<V> extends h41<V> {
    @DexIgnore
    public static <V> j41<V> t() {
        return new j41<>();
    }

    @DexIgnore
    @Override // com.fossil.h41
    public boolean p(V v) {
        return super.p(v);
    }

    @DexIgnore
    @Override // com.fossil.h41
    public boolean q(Throwable th) {
        return super.q(th);
    }

    @DexIgnore
    @Override // com.fossil.h41
    public boolean r(g64<? extends V> g64) {
        return super.r(g64);
    }
}
