package com.fossil;

import com.fossil.bc4;
import java.io.File;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ac4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ bc4.c f246a;

    @DexIgnore
    public ac4(bc4.c cVar) {
        this.f246a = cVar;
    }

    @DexIgnore
    public boolean a() {
        File[] b = this.f246a.b();
        File[] a2 = this.f246a.a();
        if (b == null || b.length <= 0) {
            return a2 != null && a2.length > 0;
        }
        return true;
    }

    @DexIgnore
    public void b(ec4 ec4) {
        ec4.remove();
    }

    @DexIgnore
    public void c(List<ec4> list) {
        for (ec4 ec4 : list) {
            b(ec4);
        }
    }

    @DexIgnore
    public List<ec4> d() {
        x74.f().b("Checking for crash reports...");
        File[] b = this.f246a.b();
        File[] a2 = this.f246a.a();
        LinkedList linkedList = new LinkedList();
        if (b != null) {
            for (File file : b) {
                x74.f().b("Found crash report " + file.getPath());
                linkedList.add(new fc4(file));
            }
        }
        if (a2 != null) {
            for (File file2 : a2) {
                linkedList.add(new dc4(file2));
            }
        }
        if (linkedList.isEmpty()) {
            x74.f().b("No reports found.");
        }
        return linkedList;
    }
}
