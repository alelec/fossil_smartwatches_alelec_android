package com.fossil;

import java.lang.annotation.Annotation;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t88 implements s88 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ s88 f3378a; // = new t88();

    @DexIgnore
    public static Annotation[] a(Annotation[] annotationArr) {
        if (u88.m(annotationArr, s88.class)) {
            return annotationArr;
        }
        Annotation[] annotationArr2 = new Annotation[(annotationArr.length + 1)];
        annotationArr2[0] = f3378a;
        System.arraycopy(annotationArr, 0, annotationArr2, 1, annotationArr.length);
        return annotationArr2;
    }

    @DexIgnore
    @Override // java.lang.annotation.Annotation
    public Class<? extends Annotation> annotationType() {
        return s88.class;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj instanceof s88;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "@" + s88.class.getName() + "()";
    }
}
