package com.fossil;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nw7 extends mw7 implements tv7 {
    @DexIgnore
    public boolean c;

    @DexIgnore
    @Override // com.fossil.tv7
    public dw7 G(long j, Runnable runnable) {
        ScheduledFuture<?> V = this.c ? V(runnable, j, TimeUnit.MILLISECONDS) : null;
        return V != null ? new cw7(V) : pv7.i.G(j, runnable);
    }

    @DexIgnore
    @Override // com.fossil.dv7
    public void M(tn7 tn7, Runnable runnable) {
        Runnable runnable2;
        try {
            Executor S = S();
            xx7 a2 = yx7.a();
            if (a2 == null || (runnable2 = a2.b(runnable)) == null) {
                runnable2 = runnable;
            }
            S.execute(runnable2);
        } catch (RejectedExecutionException e) {
            xx7 a3 = yx7.a();
            if (a3 != null) {
                a3.d();
            }
            pv7.i.z0(runnable);
        }
    }

    @DexIgnore
    public final void T() {
        this.c = dz7.a(S());
    }

    @DexIgnore
    public final ScheduledFuture<?> V(Runnable runnable, long j, TimeUnit timeUnit) {
        try {
            Executor S = S();
            if (!(S instanceof ScheduledExecutorService)) {
                S = null;
            }
            ScheduledExecutorService scheduledExecutorService = (ScheduledExecutorService) S;
            if (scheduledExecutorService != null) {
                return scheduledExecutorService.schedule(runnable, j, timeUnit);
            }
            return null;
        } catch (RejectedExecutionException e) {
            return null;
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        Executor S = S();
        if (!(S instanceof ExecutorService)) {
            S = null;
        }
        ExecutorService executorService = (ExecutorService) S;
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof nw7) && ((nw7) obj).S() == S();
    }

    @DexIgnore
    @Override // com.fossil.tv7
    public void f(long j, ku7<? super tl7> ku7) {
        ScheduledFuture<?> V = this.c ? V(new qx7(this, ku7), j, TimeUnit.MILLISECONDS) : null;
        if (V != null) {
            bx7.e(ku7, V);
        } else {
            pv7.i.f(j, ku7);
        }
    }

    @DexIgnore
    public int hashCode() {
        return System.identityHashCode(S());
    }

    @DexIgnore
    @Override // com.fossil.dv7
    public String toString() {
        return S().toString();
    }
}
