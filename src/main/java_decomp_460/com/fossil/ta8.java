package com.fossil;

import android.database.ContentObserver;
import android.os.Handler;
import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ta8 extends ContentObserver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public MethodChannel f3389a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ta8(Handler handler) {
        super(handler);
        pq7.c(handler, "handler");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ta8(Handler handler, int i, kq7 kq7) {
        this((i & 1) != 0 ? new Handler() : handler);
    }

    @DexIgnore
    public void onChange(boolean z) {
        super.onChange(z);
        MethodChannel methodChannel = this.f3389a;
        if (methodChannel != null) {
            methodChannel.invokeMethod("change", 1);
        }
    }
}
