package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Xml;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t81 {
    @DexIgnore
    public static final Drawable a(Context context, int i) {
        pq7.c(context, "$this$getDrawableCompat");
        Drawable d = gf0.d(context, i);
        if (d != null) {
            return d;
        }
        throw new IllegalStateException(("Invalid resource ID: " + i).toString());
    }

    @DexIgnore
    public static final Lifecycle b(Context context) {
        pq7.c(context, "$this$getLifecycle");
        Context context2 = context;
        while (!(context2 instanceof LifecycleOwner)) {
            if (!(context2 instanceof ContextWrapper)) {
                return null;
            }
            context2 = ((ContextWrapper) context2).getBaseContext();
            pq7.b(context2, "context.baseContext");
        }
        return ((LifecycleOwner) context2).getLifecycle();
    }

    @DexIgnore
    @SuppressLint({"ResourceType"})
    public static final Drawable c(Context context, Resources resources, int i) {
        String name;
        pq7.c(context, "$this$getXmlDrawableCompat");
        pq7.c(resources, "resources");
        XmlResourceParser xml = resources.getXml(i);
        pq7.b(xml, "resources.getXml(resId)");
        int next = xml.next();
        while (next != 2 && next != 1) {
            next = xml.next();
        }
        if (next == 2) {
            if (Build.VERSION.SDK_INT < 24 && (name = xml.getName()) != null) {
                int hashCode = name.hashCode();
                if (hashCode != -820387517) {
                    if (hashCode == 2118620333 && name.equals("animated-vector")) {
                        uz0 a2 = uz0.a(context, resources, xml, Xml.asAttributeSet(xml), context.getTheme());
                        pq7.b(a2, "AnimatedVectorDrawableCo\u2026es, parser, attrs, theme)");
                        return a2;
                    }
                } else if (name.equals("vector")) {
                    a01 c = a01.c(resources, xml, Xml.asAttributeSet(xml), context.getTheme());
                    pq7.b(c, "VectorDrawableCompat.cre\u2026es, parser, attrs, theme)");
                    return c;
                }
            }
            return w81.d(resources, i, context.getTheme());
        }
        throw new XmlPullParserException("No start tag found.");
    }
}
