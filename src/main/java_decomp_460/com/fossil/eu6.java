package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface eu6 extends gq4<du6> {
    @DexIgnore
    void A2(ai5 ai5);

    @DexIgnore
    void F3(ai5 ai5);

    @DexIgnore
    void R2(ai5 ai5);

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    void m1(ai5 ai5);

    @DexIgnore
    void o(int i, String str);
}
