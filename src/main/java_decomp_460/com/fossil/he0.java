package com.fossil;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"BanParcelableUsage"})
public class he0 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<he0> CREATOR; // = new a();
    @DexIgnore
    public /* final */ boolean b; // = false;
    @DexIgnore
    public /* final */ Handler c; // = null;
    @DexIgnore
    public ge0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Parcelable.Creator<he0> {
        @DexIgnore
        /* renamed from: a */
        public he0 createFromParcel(Parcel parcel) {
            return new he0(parcel);
        }

        @DexIgnore
        /* renamed from: b */
        public he0[] newArray(int i) {
            return new he0[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ge0.a {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.ge0
        public void T2(int i, Bundle bundle) {
            he0 he0 = he0.this;
            Handler handler = he0.c;
            if (handler != null) {
                handler.post(new c(i, bundle));
            } else {
                he0.a(i, bundle);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ Bundle c;

        @DexIgnore
        public c(int i, Bundle bundle) {
            this.b = i;
            this.c = bundle;
        }

        @DexIgnore
        public void run() {
            he0.this.a(this.b, this.c);
        }
    }

    @DexIgnore
    public he0(Parcel parcel) {
        this.d = ge0.a.d(parcel.readStrongBinder());
    }

    @DexIgnore
    public void a(int i, Bundle bundle) {
    }

    @DexIgnore
    public void b(int i, Bundle bundle) {
        if (this.b) {
            Handler handler = this.c;
            if (handler != null) {
                handler.post(new c(i, bundle));
            } else {
                a(i, bundle);
            }
        } else {
            ge0 ge0 = this.d;
            if (ge0 != null) {
                try {
                    ge0.T2(i, bundle);
                } catch (RemoteException e) {
                }
            }
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        synchronized (this) {
            if (this.d == null) {
                this.d = new b();
            }
            parcel.writeStrongBinder(this.d.asBinder());
        }
    }
}
