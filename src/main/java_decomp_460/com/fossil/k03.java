package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k03 extends j03 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f1851a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;

    @DexIgnore
    public k03(byte[] bArr, int i, int i2, boolean z) {
        super();
        this.e = Integer.MAX_VALUE;
        this.f1851a = i2 + i;
        this.c = i;
        this.d = i;
    }

    @DexIgnore
    public final int d(int i) throws l13 {
        if (i >= 0) {
            int e2 = e() + i;
            int i2 = this.e;
            if (e2 <= i2) {
                this.e = e2;
                f();
                return i2;
            }
            throw l13.zza();
        }
        throw l13.zzb();
    }

    @DexIgnore
    public final int e() {
        return this.c - this.d;
    }

    @DexIgnore
    public final void f() {
        int i = this.f1851a + this.b;
        this.f1851a = i;
        int i2 = i - this.d;
        int i3 = this.e;
        if (i2 > i3) {
            int i4 = i2 - i3;
            this.b = i4;
            this.f1851a = i - i4;
            return;
        }
        this.b = 0;
    }
}
