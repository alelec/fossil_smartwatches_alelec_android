package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class z84 {
    @DexIgnore
    public static z84 a(ta4 ta4, String str) {
        return new m84(ta4, str);
    }

    @DexIgnore
    public abstract ta4 b();

    @DexIgnore
    public abstract String c();
}
