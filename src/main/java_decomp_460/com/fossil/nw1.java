package com.fossil;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.widget.TextView;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.facebook.appevents.codeless.CodelessMatcher;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nw1 extends kw1 {
    @DexIgnore
    public static /* final */ iv1 j; // = new iv1(120, 120);
    @DexIgnore
    public /* final */ ec0 g;
    @DexIgnore
    public nv1 h;
    @DexIgnore
    public /* final */ ArrayList<mv1> i;

    @DexIgnore
    public nw1() {
        super(new ry1(3, 0), null, null, 6);
        this.g = ec0.COMPLICATIONS;
        this.i = new ArrayList<>();
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public nw1(com.fossil.ow1 r12, com.fossil.ry1 r13) {
        /*
        // Method dump skipped, instructions count: 491
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nw1.<init>(com.fossil.ow1, com.fossil.ry1):void");
    }

    @DexIgnore
    @Override // com.fossil.kw1
    public lw1 a(hc0 hc0) {
        cc0 cc0;
        cc0 a2;
        String h2;
        cc0 a3;
        if (!(hc0 instanceof pw1)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        ArrayList arrayList5 = new ArrayList();
        ArrayList arrayList6 = new ArrayList();
        ArrayList arrayList7 = new ArrayList();
        pw1 pw1 = (pw1) hc0;
        arrayList.add(new cc0(d(), pw1.f()[0].c));
        JSONArray jSONArray = new JSONArray();
        pc0 pc0 = new pc0();
        nv1 nv1 = this.h;
        if (!(nv1 == null || (a3 = nv1.a((h2 = h(pc0, nv1.getName(), nv1.e().a())))) == null)) {
            arrayList2.add(a3);
            jSONArray.put(nv1.b(h2));
        }
        mw1 f = f();
        if (!(f == null || (a2 = fw1.a(f, new lv1(192, 192), null, 2, null)) == null)) {
            arrayList2.add(a2);
        }
        cc0[] b = pw1.b();
        ArrayList arrayList8 = new ArrayList();
        for (cc0 cc02 : b) {
            if (!pq7.a(cc02.b, "!preview.rle")) {
                arrayList8.add(cc02);
            }
        }
        arrayList2.addAll(arrayList8);
        mm7.t(arrayList3, pw1.d());
        mm7.t(arrayList4, pw1.e());
        cc0[] c = pw1.c();
        ArrayList arrayList9 = new ArrayList();
        for (cc0 cc03 : c) {
            if (!pq7.a(cc03.b, "display_name")) {
                arrayList9.add(cc03);
            }
        }
        arrayList5.addAll(arrayList9);
        String a4 = iy1.a(e());
        Charset charset = et7.f986a;
        if (a4 != null) {
            byte[] bytes = a4.getBytes(charset);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            arrayList5.add(new cc0("display_name", bytes));
            String str = "null cannot be cast to non-null type java.lang.String";
            for (T t : this.i) {
                if (t instanceof dw1) {
                    String name = t.getName();
                    T t2 = t;
                    String h3 = h(pc0, name, t2.e().a());
                    cc0 a5 = t2.a(h3);
                    if (a5 != null) {
                        jSONArray.put(t2.b(h3));
                        arrayList2.add(a5);
                    }
                } else if (t instanceof sv1) {
                    cc0[] f2 = pw1.f();
                    int length = f2.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            cc0 = null;
                            break;
                        }
                        cc0 cc04 = f2[i2];
                        if (pq7.a(cc04.b, t.getComplicationType().a())) {
                            cc0 = cc04;
                            break;
                        }
                        i2++;
                    }
                    if (cc0 != null) {
                        arrayList.add(cc0);
                        T t3 = t;
                        tv1 backgroundImage = t3.getBackgroundImage();
                        String h4 = backgroundImage != null ? h(pc0, backgroundImage.getName(), backgroundImage.a()) : null;
                        jSONArray.put(t3.b(h4));
                        cc0 a6 = t3.a(h4);
                        if (a6 != null) {
                            arrayList2.add(a6);
                        }
                    }
                } else if (t instanceof ew1) {
                    lv1 actualSize = t.getScaledSize().toActualSize(240, 240);
                    Bitmap createBitmap = Bitmap.createBitmap(actualSize.getWidth(), actualSize.getHeight(), Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(createBitmap);
                    TextView textView = new TextView(id0.i.a());
                    textView.layout(0, 0, actualSize.getWidth(), actualSize.getHeight());
                    T t4 = t;
                    textView.setText(t4.getText());
                    textView.setTextSize(0, t4.getFontScaledSize() * ((float) 240));
                    textView.setTypeface(qc0.f2957a.a(cx1.f.e(), t4.getFontName()));
                    textView.setTextColor(t4.getTextColor().getColorCode());
                    textView.draw(canvas);
                    String name2 = t.getName();
                    jv1 scaledPosition = t.getScaledPosition();
                    kv1 scaledSize = t.getScaledSize();
                    pq7.b(createBitmap, "bitmap");
                    dw1 dw1 = new dw1(name2, scaledPosition, scaledSize, cy1.b(createBitmap, null, 1, null));
                    String h5 = h(pc0, t.getName(), dw1.e().a());
                    cc0 a7 = dw1.a(h5);
                    if (a7 != null) {
                        jSONArray.put(dw1.b(h5));
                        arrayList2.add(a7);
                    }
                    arrayList7.add(t4.a(wt7.t0(h5, CodelessMatcher.CURRENT_CLASS_NAME, null, 2, null)));
                }
                str = str;
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("layout", jSONArray);
            String jSONObject2 = jSONObject.toString();
            pq7.b(jSONObject2, "configNodeJSON.toString()");
            String a8 = iy1.a(jSONObject2);
            Charset c2 = hd0.y.c();
            if (a8 != null) {
                byte[] bytes2 = a8.getBytes(c2);
                pq7.b(bytes2, "(this as java.lang.String).getBytes(charset)");
                arrayList6.add(new cc0("customWatchFace", bytes2));
                ry1 h6 = pw1.h();
                jw1 jw1 = pw1.g().b;
                ry1 ry1 = new ry1(pw1.g().c.getMajor(), b().getMinor());
                boolean z = pw1.g().d;
                byte[] bArr = pw1.g().e;
                byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
                pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
                yb0 yb0 = new yb0(jw1, ry1, z, copyOf);
                Object[] array = arrayList.toArray(new cc0[0]);
                if (array != null) {
                    cc0[] cc0Arr = (cc0[]) array;
                    Object[] array2 = arrayList2.toArray(new cc0[0]);
                    if (array2 != null) {
                        cc0[] cc0Arr2 = (cc0[]) array2;
                        Object[] array3 = arrayList3.toArray(new cc0[0]);
                        if (array3 != null) {
                            cc0[] cc0Arr3 = (cc0[]) array3;
                            Object[] array4 = arrayList4.toArray(new cc0[0]);
                            if (array4 != null) {
                                cc0[] cc0Arr4 = (cc0[]) array4;
                                Object[] array5 = arrayList5.toArray(new cc0[0]);
                                if (array5 != null) {
                                    cc0[] cc0Arr5 = (cc0[]) array5;
                                    Object[] array6 = arrayList6.toArray(new cc0[0]);
                                    if (array6 != null) {
                                        cc0[] cc0Arr6 = (cc0[]) array6;
                                        Object[] array7 = arrayList7.toArray(new cc0[0]);
                                        if (array7 != null) {
                                            return new ow1(h6, yb0, cc0Arr, cc0Arr2, cc0Arr3, cc0Arr4, cc0Arr5, cc0Arr6, (cc0[]) array7);
                                        }
                                        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                                    }
                                    throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                                }
                                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                            }
                            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                    throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
            throw new il7(str);
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.kw1
    public ec0 g() {
        return this.g;
    }

    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: char : 0x0051: INVOKE  (r4v5 char) = 
      ("0123456789abcdefghijklmnopqrstuvwxyz")
      (wrap: int : 0x004f: ARITH  (r5v1 int) = (wrap: int : 0x004d: ARITH  (r5v0 int) = (r0v6 int) / (36 int)) % (36 int))
     type: VIRTUAL call: java.lang.String.charAt(int):char), (wrap: char : 0x005c: INVOKE  (r0v8 char) = ("0123456789abcdefghijklmnopqrstuvwxyz"), (wrap: int : 0x005a: ARITH  (r0v7 int) = (r0v6 int) % (36 int)) type: VIRTUAL call: java.lang.String.charAt(int):char)] */
    @DexIgnore
    public final String h(pc0 pc0, String str, String str2) {
        String str3 = '.' + str2;
        Integer num = pc0.f2813a.get(str);
        if (num == null) {
            num = 0;
        }
        pq7.b(num, "suffixMappings[name] ?: 0");
        int intValue = num.intValue();
        pc0.f2813a.put(str, Integer.valueOf((intValue + 1) % (((int) Math.pow(10.0d, (double) 2)) - 1)));
        hr7 hr7 = hr7.f1520a;
        Locale locale = Locale.ROOT;
        pq7.b(locale, "Locale.ROOT");
        StringBuilder sb = new StringBuilder();
        sb.append("0123456789abcdefghijklmnopqrstuvwxyz".charAt((intValue / 36) % 36));
        sb.append("0123456789abcdefghijklmnopqrstuvwxyz".charAt(intValue % 36));
        String format = String.format(locale, "%s%s%s%s", Arrays.copyOf(new Object[]{str, LocaleConverter.LOCALE_DELIMITER, sb.toString(), str3}, 4));
        pq7.b(format, "java.lang.String.format(locale, format, *args)");
        return format;
    }

    @DexIgnore
    public final nv1 i() {
        return this.h;
    }

    @DexIgnore
    public final ArrayList<mv1> j() {
        return this.i;
    }

    @DexIgnore
    public final nw1 k(nv1 nv1) {
        this.h = nv1;
        return this;
    }
}
