package com.fossil;

import com.fossil.sx1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vx1<K, V> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ a f3851a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final byte[] a(byte[] bArr) {
            pq7.c(bArr, "fileData");
            return bArr.length > 16 ? dm7.k(bArr, 12, bArr.length - 4) : new byte[0];
        }
    }

    @DexIgnore
    public final byte[] a(short s, ry1 ry1, K k) throws sx1 {
        pq7.c(ry1, "version");
        pq7.c(k, "entries");
        qx1<K> d = d(ry1);
        if (d != null) {
            return d.a(s, k);
        }
        sx1.a aVar = sx1.a.UNSUPPORTED_VERSION;
        throw new sx1(aVar, "Not support version " + ry1 + '.', null, 4, null);
    }

    @DexIgnore
    public abstract qx1<K>[] b();

    @DexIgnore
    public abstract rx1<V>[] c();

    @DexIgnore
    public final qx1<K> d(ry1 ry1) {
        ry1 c;
        qx1<K>[] b = b();
        int length = b.length;
        qx1<K> qx1 = null;
        int i = 0;
        while (i < length) {
            qx1<K> qx12 = b[i];
            if (pq7.a(qx12.c(), ry1)) {
                return qx12;
            }
            if (qx12.c().getMajor() == ry1.getMajor()) {
                if (qx12.c().getMinor() >= ((qx1 == null || (c = qx1.c()) == null) ? 0 : c.getMinor())) {
                    i++;
                    qx1 = qx12;
                }
            }
            qx12 = qx1;
            i++;
            qx1 = qx12;
        }
        return qx1;
    }

    @DexIgnore
    public final rx1<V> e(ry1 ry1) {
        ry1 a2;
        rx1<V>[] c = c();
        int length = c.length;
        rx1<V> rx1 = null;
        int i = 0;
        while (i < length) {
            rx1<V> rx12 = c[i];
            if (pq7.a(rx12.a(), ry1)) {
                return rx12;
            }
            if (rx12.a().getMajor() == ry1.getMajor()) {
                if (rx12.a().getMinor() >= ((rx1 == null || (a2 = rx1.a()) == null) ? 0 : a2.getMinor())) {
                    i++;
                    rx1 = rx12;
                }
            }
            rx12 = rx1;
            i++;
            rx1 = rx12;
        }
        return rx1;
    }

    @DexIgnore
    public final V f(byte[] bArr) throws sx1 {
        pq7.c(bArr, "data");
        try {
            ry1 ry1 = new ry1(bArr[2], bArr[3]);
            rx1<V> e = e(ry1);
            if (e != null) {
                return e.b(bArr);
            }
            sx1.a aVar = sx1.a.UNSUPPORTED_VERSION;
            throw new sx1(aVar, "Not support version " + ry1 + '.', null, 4, null);
        } catch (Exception e2) {
            throw new sx1(sx1.a.INVALID_FILE_DATA, "Invalid file data.", e2);
        }
    }
}
