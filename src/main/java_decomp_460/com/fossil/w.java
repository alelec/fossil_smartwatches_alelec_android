package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w extends xw0 {
    @DexIgnore
    public w(z zVar, qw0 qw0) {
        super(qw0);
    }

    @DexIgnore
    @Override // com.fossil.xw0
    public String createQuery() {
        return "delete from DeviceFile where deviceMacAddress = ? and fileType = ? and fileIndex = ? and isCompleted = 0";
    }
}
