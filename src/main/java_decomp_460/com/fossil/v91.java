package com.fossil;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.conn.ConnectTimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v91 extends w91 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ da1 f3736a;

    @DexIgnore
    public v91(da1 da1) {
        this.f3736a = da1;
    }

    @DexIgnore
    @Override // com.fossil.w91
    public ca1 b(m91<?> m91, Map<String, String> map) throws IOException, z81 {
        try {
            HttpResponse a2 = this.f3736a.a(m91, map);
            int statusCode = a2.getStatusLine().getStatusCode();
            Header[] allHeaders = a2.getAllHeaders();
            ArrayList arrayList = new ArrayList(allHeaders.length);
            for (Header header : allHeaders) {
                arrayList.add(new f91(header.getName(), header.getValue()));
            }
            if (a2.getEntity() == null) {
                return new ca1(statusCode, arrayList);
            }
            long contentLength = a2.getEntity().getContentLength();
            if (((long) ((int) contentLength)) == contentLength) {
                return new ca1(statusCode, arrayList, (int) a2.getEntity().getContentLength(), a2.getEntity().getContent());
            }
            throw new IOException("Response too large: " + contentLength);
        } catch (ConnectTimeoutException e) {
            throw new SocketTimeoutException(e.getMessage());
        }
    }
}
