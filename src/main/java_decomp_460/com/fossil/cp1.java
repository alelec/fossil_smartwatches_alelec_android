package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cp1 extends yo1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<cp1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public cp1 a(Parcel parcel) {
            return new cp1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public cp1 createFromParcel(Parcel parcel) {
            return new cp1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public cp1[] newArray(int i) {
            return new cp1[i];
        }
    }

    @DexIgnore
    public cp1() {
        super(ap1.WELLNESS, null, null, 6);
    }

    @DexIgnore
    public /* synthetic */ cp1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    public cp1(vw1 vw1) {
        super(ap1.WELLNESS, vw1, null, 4);
    }
}
