package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cb5 extends bb5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d P; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Q;
    @DexIgnore
    public long O;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        Q = sparseIntArray;
        sparseIntArray.put(2131362810, 1);
        Q.put(2131362686, 2);
        Q.put(2131363333, 3);
        Q.put(2131363410, 4);
        Q.put(2131363157, 5);
        Q.put(2131362638, 6);
        Q.put(2131362233, 7);
        Q.put(2131362679, 8);
        Q.put(2131362644, 9);
        Q.put(2131362241, 10);
        Q.put(2131363329, 11);
        Q.put(2131363328, 12);
        Q.put(2131361924, 13);
        Q.put(2131361926, 14);
        Q.put(2131363398, 15);
        Q.put(2131362714, 16);
        Q.put(2131362719, 17);
        Q.put(2131362661, 18);
        Q.put(2131362772, 19);
        Q.put(2131362771, 20);
        Q.put(2131363340, 21);
        Q.put(2131363397, 22);
        Q.put(2131361941, 23);
    }
    */

    @DexIgnore
    public cb5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 24, P, Q));
    }

    @DexIgnore
    public cb5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (Barrier) objArr[13], (Barrier) objArr[14], (FlexibleButton) objArr[23], (FlexibleTextInputEditText) objArr[7], (FlexibleTextInputEditText) objArr[10], (FlexibleTextInputLayout) objArr[6], (FlexibleTextInputLayout) objArr[9], (FloatingActionButton) objArr[18], (RTLImageView) objArr[8], (ImageView) objArr[2], (ImageView) objArr[16], (ImageView) objArr[17], (ConstraintLayout) objArr[20], (ConstraintLayout) objArr[19], (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[0], (ScrollView) objArr[5], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[21], (FlexibleButton) objArr[22], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[4]);
        this.O = -1;
        this.F.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.O = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.O != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.O = 1;
        }
        w();
    }
}
