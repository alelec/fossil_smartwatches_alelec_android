package com.fossil;

import com.fossil.m62;
import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m82 extends r82 {
    @DexIgnore
    public /* final */ Map<m62.f, j82> c;
    @DexIgnore
    public /* final */ /* synthetic */ h82 d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m82(h82 h82, Map<m62.f, j82> map) {
        super(h82, null);
        this.d = h82;
        this.c = map;
    }

    @DexIgnore
    @Override // com.fossil.r82
    public final void a() {
        int i = 0;
        ic2 ic2 = new ic2(this.d.d);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (m62.f fVar : this.c.keySet()) {
            if (!fVar.r() || (this.c.get(fVar).c)) {
                arrayList2.add(fVar);
            } else {
                arrayList.add(fVar);
            }
        }
        int i2 = -1;
        if (arrayList.isEmpty()) {
            int size = arrayList2.size();
            while (i < size) {
                Object obj = arrayList2.get(i);
                i++;
                i2 = ic2.b(this.d.c, (m62.f) obj);
                if (i2 == 0) {
                    break;
                }
            }
        } else {
            int size2 = arrayList.size();
            int i3 = 0;
            while (i3 < size2) {
                Object obj2 = arrayList.get(i3);
                i3++;
                i2 = ic2.b(this.d.c, (m62.f) obj2);
                if (i2 != 0) {
                    break;
                }
            }
        }
        if (i2 != 0) {
            this.d.f1448a.p(new l82(this, this.d, new z52(i2, null)));
            return;
        }
        if ((this.d.m) && this.d.k != null) {
            this.d.k.b();
        }
        for (m62.f fVar2 : this.c.keySet()) {
            j82 j82 = this.c.get(fVar2);
            if (!fVar2.r() || ic2.b(this.d.c, fVar2) == 0) {
                fVar2.l(j82);
            } else {
                this.d.f1448a.p(new o82(this, this.d, j82));
            }
        }
    }
}
