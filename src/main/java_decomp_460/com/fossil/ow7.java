package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ow7 extends nw7 {
    @DexIgnore
    public /* final */ Executor d;

    @DexIgnore
    public ow7(Executor executor) {
        this.d = executor;
        T();
    }

    @DexIgnore
    @Override // com.fossil.mw7
    public Executor S() {
        return this.d;
    }
}
