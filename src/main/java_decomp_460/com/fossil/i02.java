package com.fossil;

import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i02 implements yy1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set<ty1> f1566a;
    @DexIgnore
    public /* final */ h02 b;
    @DexIgnore
    public /* final */ l02 c;

    @DexIgnore
    public i02(Set<ty1> set, h02 h02, l02 l02) {
        this.f1566a = set;
        this.b = h02;
        this.c = l02;
    }

    @DexIgnore
    @Override // com.fossil.yy1
    public <T> xy1<T> a(String str, Class<T> cls, wy1<T, byte[]> wy1) {
        return b(str, cls, ty1.b("proto"), wy1);
    }

    @DexIgnore
    @Override // com.fossil.yy1
    public <T> xy1<T> b(String str, Class<T> cls, ty1 ty1, wy1<T, byte[]> wy1) {
        if (this.f1566a.contains(ty1)) {
            return new k02(this.b, str, ty1, wy1, this.c);
        }
        throw new IllegalArgumentException(String.format("%s is not supported byt this factory. Supported encodings are: %s.", ty1, this.f1566a));
    }
}
