package com.fossil;

import com.fossil.ta4;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ca4 extends ta4.c.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f588a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.c.b.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f589a;
        @DexIgnore
        public byte[] b;

        @DexIgnore
        @Override // com.fossil.ta4.c.b.a
        public ta4.c.b a() {
            String str = "";
            if (this.f589a == null) {
                str = " filename";
            }
            if (this.b == null) {
                str = str + " contents";
            }
            if (str.isEmpty()) {
                return new ca4(this.f589a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.c.b.a
        public ta4.c.b.a b(byte[] bArr) {
            if (bArr != null) {
                this.b = bArr;
                return this;
            }
            throw new NullPointerException("Null contents");
        }

        @DexIgnore
        @Override // com.fossil.ta4.c.b.a
        public ta4.c.b.a c(String str) {
            if (str != null) {
                this.f589a = str;
                return this;
            }
            throw new NullPointerException("Null filename");
        }
    }

    @DexIgnore
    public ca4(String str, byte[] bArr) {
        this.f588a = str;
        this.b = bArr;
    }

    @DexIgnore
    @Override // com.fossil.ta4.c.b
    public byte[] b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ta4.c.b
    public String c() {
        return this.f588a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.c.b)) {
            return false;
        }
        ta4.c.b bVar = (ta4.c.b) obj;
        if (this.f588a.equals(bVar.c())) {
            if (Arrays.equals(this.b, bVar instanceof ca4 ? ((ca4) bVar).b : bVar.b())) {
                z = true;
                return z;
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.f588a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b);
    }

    @DexIgnore
    public String toString() {
        return "File{filename=" + this.f588a + ", contents=" + Arrays.toString(this.b) + "}";
    }
}
