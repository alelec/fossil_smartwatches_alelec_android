package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class w01 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends w01 {
        @DexIgnore
        @Override // com.fossil.w01
        public v01 a(String str) {
            return null;
        }
    }

    @DexIgnore
    public static w01 c() {
        return new a();
    }

    @DexIgnore
    public abstract v01 a(String str);

    @DexIgnore
    public final v01 b(String str) {
        v01 a2 = a(str);
        return a2 == null ? v01.a(str) : a2;
    }
}
