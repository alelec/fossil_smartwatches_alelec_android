package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pq0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ CopyOnWriteArrayList<a> f2855a; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ FragmentManager b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ FragmentManager.f f2856a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public a(FragmentManager.f fVar, boolean z) {
            this.f2856a = fVar;
            this.b = z;
        }
    }

    @DexIgnore
    public pq0(FragmentManager fragmentManager) {
        this.b = fragmentManager;
    }

    @DexIgnore
    public void a(Fragment fragment, Bundle bundle, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().a(fragment, bundle, true);
        }
        Iterator<a> it = this.f2855a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.f2856a.a(this.b, fragment, bundle);
            }
        }
    }

    @DexIgnore
    public void b(Fragment fragment, Context context, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().b(fragment, context, true);
        }
        Iterator<a> it = this.f2855a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.f2856a.b(this.b, fragment, context);
            }
        }
    }

    @DexIgnore
    public void c(Fragment fragment, Bundle bundle, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().c(fragment, bundle, true);
        }
        Iterator<a> it = this.f2855a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.f2856a.c(this.b, fragment, bundle);
            }
        }
    }

    @DexIgnore
    public void d(Fragment fragment, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().d(fragment, true);
        }
        Iterator<a> it = this.f2855a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.f2856a.d(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void e(Fragment fragment, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().e(fragment, true);
        }
        Iterator<a> it = this.f2855a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.f2856a.e(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void f(Fragment fragment, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().f(fragment, true);
        }
        Iterator<a> it = this.f2855a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.f2856a.f(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void g(Fragment fragment, Context context, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().g(fragment, context, true);
        }
        Iterator<a> it = this.f2855a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.f2856a.g(this.b, fragment, context);
            }
        }
    }

    @DexIgnore
    public void h(Fragment fragment, Bundle bundle, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().h(fragment, bundle, true);
        }
        Iterator<a> it = this.f2855a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.f2856a.h(this.b, fragment, bundle);
            }
        }
    }

    @DexIgnore
    public void i(Fragment fragment, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().i(fragment, true);
        }
        Iterator<a> it = this.f2855a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.f2856a.i(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void j(Fragment fragment, Bundle bundle, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().j(fragment, bundle, true);
        }
        Iterator<a> it = this.f2855a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.f2856a.j(this.b, fragment, bundle);
            }
        }
    }

    @DexIgnore
    public void k(Fragment fragment, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().k(fragment, true);
        }
        Iterator<a> it = this.f2855a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.f2856a.k(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void l(Fragment fragment, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().l(fragment, true);
        }
        Iterator<a> it = this.f2855a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.f2856a.l(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void m(Fragment fragment, View view, Bundle bundle, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().m(fragment, view, bundle, true);
        }
        Iterator<a> it = this.f2855a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.f2856a.m(this.b, fragment, view, bundle);
            }
        }
    }

    @DexIgnore
    public void n(Fragment fragment, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().n(fragment, true);
        }
        Iterator<a> it = this.f2855a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.f2856a.n(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void o(FragmentManager.f fVar, boolean z) {
        this.f2855a.add(new a(fVar, z));
    }

    @DexIgnore
    public void p(FragmentManager.f fVar) {
        synchronized (this.f2855a) {
            int size = this.f2855a.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    break;
                } else if (this.f2855a.get(i).f2856a == fVar) {
                    this.f2855a.remove(i);
                    break;
                } else {
                    i++;
                }
            }
        }
    }
}
