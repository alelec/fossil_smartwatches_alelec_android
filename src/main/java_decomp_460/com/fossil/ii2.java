package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ii2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ii2> CREATOR; // = new hi2();
    @DexIgnore
    public static /* final */ ii2 c; // = new ii2("com.google.android.gms");
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public ii2(String str) {
        rc2.k(str);
        this.b = str;
    }

    @DexIgnore
    public static ii2 f(String str) {
        return "com.google.android.gms".equals(str) ? c : new ii2(str);
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ii2)) {
            return false;
        }
        return this.b.equals(((ii2) obj).b);
    }

    @DexIgnore
    public final int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final String toString() {
        return String.format("Application{%s}", this.b);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.u(parcel, 1, this.b, false);
        bd2.b(parcel, a2);
    }
}
