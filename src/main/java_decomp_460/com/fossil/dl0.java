package com.fossil;

import android.app.RemoteInput;
import android.os.Build;
import android.os.Bundle;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dl0 {
    @DexIgnore
    public static RemoteInput a(dl0 dl0) {
        RemoteInput.Builder addExtras = new RemoteInput.Builder(dl0.i()).setLabel(dl0.h()).setChoices(dl0.e()).setAllowFreeFormInput(dl0.c()).addExtras(dl0.g());
        if (Build.VERSION.SDK_INT >= 29) {
            addExtras.setEditChoicesBeforeSending(dl0.f());
        }
        return addExtras.build();
    }

    @DexIgnore
    public static RemoteInput[] b(dl0[] dl0Arr) {
        if (dl0Arr == null) {
            return null;
        }
        RemoteInput[] remoteInputArr = new RemoteInput[dl0Arr.length];
        for (int i = 0; i < dl0Arr.length; i++) {
            remoteInputArr[i] = a(dl0Arr[i]);
        }
        return remoteInputArr;
    }

    @DexIgnore
    public abstract boolean c();

    @DexIgnore
    public abstract Set<String> d();

    @DexIgnore
    public abstract CharSequence[] e();

    @DexIgnore
    public abstract int f();

    @DexIgnore
    public abstract Bundle g();

    @DexIgnore
    public abstract CharSequence h();

    @DexIgnore
    public abstract String i();
}
