package com.fossil;

import java.util.Comparator;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zy2 {
    @DexIgnore
    public static boolean a(Comparator<?> comparator, Iterable<?> iterable) {
        Object comparator2;
        sw2.b(comparator);
        sw2.b(iterable);
        if (iterable instanceof SortedSet) {
            comparator2 = ((SortedSet) iterable).comparator();
            if (comparator2 == null) {
                comparator2 = ly2.zza;
            }
        } else if (!(iterable instanceof az2)) {
            return false;
        } else {
            comparator2 = ((az2) iterable).comparator();
        }
        return comparator.equals(comparator2);
    }
}
