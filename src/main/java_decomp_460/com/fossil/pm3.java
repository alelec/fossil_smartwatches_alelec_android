package com.fossil;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pm3 implements ln3 {
    @DexIgnore
    public static volatile pm3 G;
    @DexIgnore
    public volatile Boolean A;
    @DexIgnore
    public Boolean B;
    @DexIgnore
    public Boolean C;
    @DexIgnore
    public int D;
    @DexIgnore
    public AtomicInteger E; // = new AtomicInteger(0);
    @DexIgnore
    public /* final */ long F;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2846a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ yr3 f;
    @DexIgnore
    public /* final */ zr3 g;
    @DexIgnore
    public /* final */ xl3 h;
    @DexIgnore
    public /* final */ kl3 i;
    @DexIgnore
    public /* final */ im3 j;
    @DexIgnore
    public /* final */ jq3 k;
    @DexIgnore
    public /* final */ kr3 l;
    @DexIgnore
    public /* final */ il3 m;
    @DexIgnore
    public /* final */ ef2 n;
    @DexIgnore
    public /* final */ ap3 o;
    @DexIgnore
    public /* final */ un3 p;
    @DexIgnore
    public /* final */ gg3 q;
    @DexIgnore
    public /* final */ ro3 r;
    @DexIgnore
    public gl3 s;
    @DexIgnore
    public fp3 t;
    @DexIgnore
    public pg3 u;
    @DexIgnore
    public dl3 v;
    @DexIgnore
    public cm3 w;
    @DexIgnore
    public boolean x; // = false;
    @DexIgnore
    public Boolean y;
    @DexIgnore
    public long z;

    @DexIgnore
    public pm3(rn3 rn3) {
        Bundle bundle;
        rc2.k(rn3);
        yr3 yr3 = new yr3(rn3.f3132a);
        this.f = yr3;
        al3.f285a = yr3;
        this.f2846a = rn3.f3132a;
        this.b = rn3.b;
        this.c = rn3.c;
        this.d = rn3.d;
        this.e = rn3.h;
        this.A = rn3.e;
        xs2 xs2 = rn3.g;
        if (!(xs2 == null || (bundle = xs2.h) == null)) {
            Object obj = bundle.get("measurementEnabled");
            if (obj instanceof Boolean) {
                this.B = (Boolean) obj;
            }
            Object obj2 = xs2.h.get("measurementDeactivated");
            if (obj2 instanceof Boolean) {
                this.C = (Boolean) obj2;
            }
        }
        xv2.h(this.f2846a);
        ef2 d2 = hf2.d();
        this.n = d2;
        Long l2 = rn3.i;
        this.F = l2 != null ? l2.longValue() : d2.b();
        this.g = new zr3(this);
        xl3 xl3 = new xl3(this);
        xl3.p();
        this.h = xl3;
        kl3 kl3 = new kl3(this);
        kl3.p();
        this.i = kl3;
        kr3 kr3 = new kr3(this);
        kr3.p();
        this.l = kr3;
        il3 il3 = new il3(this);
        il3.p();
        this.m = il3;
        this.q = new gg3(this);
        ap3 ap3 = new ap3(this);
        ap3.y();
        this.o = ap3;
        un3 un3 = new un3(this);
        un3.y();
        this.p = un3;
        jq3 jq3 = new jq3(this);
        jq3.y();
        this.k = jq3;
        ro3 ro3 = new ro3(this);
        ro3.p();
        this.r = ro3;
        im3 im3 = new im3(this);
        im3.p();
        this.j = im3;
        xs2 xs22 = rn3.g;
        boolean z2 = (xs22 == null || xs22.c == 0) ? false : true;
        if (this.f2846a.getApplicationContext() instanceof Application) {
            un3 E2 = E();
            if (E2.e().getApplicationContext() instanceof Application) {
                Application application = (Application) E2.e().getApplicationContext();
                if (E2.c == null) {
                    E2.c = new qo3(E2, null);
                }
                if (!z2) {
                    application.unregisterActivityLifecycleCallbacks(E2.c);
                    application.registerActivityLifecycleCallbacks(E2.c);
                    E2.d().N().a("Registered activity lifecycle callback");
                }
            }
        } else {
            d().I().a("Application context is not an Application");
        }
        this.j.y(new rm3(this, rn3));
    }

    @DexIgnore
    public static pm3 a(Context context, xs2 xs2, Long l2) {
        Bundle bundle;
        if (xs2 != null && (xs2.f == null || xs2.g == null)) {
            xs2 = new xs2(xs2.b, xs2.c, xs2.d, xs2.e, null, null, xs2.h);
        }
        rc2.k(context);
        rc2.k(context.getApplicationContext());
        if (G == null) {
            synchronized (pm3.class) {
                try {
                    if (G == null) {
                        G = new pm3(new rn3(context, xs2, l2));
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        } else if (!(xs2 == null || (bundle = xs2.h) == null || !bundle.containsKey("dataCollectionDefaultEnabled"))) {
            G.m(xs2.h.getBoolean("dataCollectionDefaultEnabled"));
        }
        return G;
    }

    @DexIgnore
    public static void j(jn3 jn3) {
        if (jn3 == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    @DexIgnore
    public static void x(lm3 lm3) {
        if (lm3 == null) {
            throw new IllegalStateException("Component not created");
        } else if (!lm3.w()) {
            String valueOf = String.valueOf(lm3.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    public static void y(in3 in3) {
        if (in3 == null) {
            throw new IllegalStateException("Component not created");
        } else if (!in3.s()) {
            String valueOf = String.valueOf(in3.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    public final kl3 A() {
        kl3 kl3 = this.i;
        if (kl3 == null || !kl3.s()) {
            return null;
        }
        return this.i;
    }

    @DexIgnore
    public final jq3 B() {
        x(this.k);
        return this.k;
    }

    @DexIgnore
    public final cm3 C() {
        return this.w;
    }

    @DexIgnore
    public final im3 D() {
        return this.j;
    }

    @DexIgnore
    public final un3 E() {
        x(this.p);
        return this.p;
    }

    @DexIgnore
    public final kr3 F() {
        j(this.l);
        return this.l;
    }

    @DexIgnore
    public final il3 G() {
        j(this.m);
        return this.m;
    }

    @DexIgnore
    public final gl3 H() {
        x(this.s);
        return this.s;
    }

    @DexIgnore
    public final boolean I() {
        return TextUtils.isEmpty(this.b);
    }

    @DexIgnore
    public final String J() {
        return this.b;
    }

    @DexIgnore
    public final String K() {
        return this.c;
    }

    @DexIgnore
    public final String L() {
        return this.d;
    }

    @DexIgnore
    public final boolean M() {
        return this.e;
    }

    @DexIgnore
    public final ap3 N() {
        x(this.o);
        return this.o;
    }

    @DexIgnore
    public final fp3 O() {
        x(this.t);
        return this.t;
    }

    @DexIgnore
    public final pg3 P() {
        y(this.u);
        return this.u;
    }

    @DexIgnore
    public final dl3 Q() {
        x(this.v);
        return this.v;
    }

    @DexIgnore
    public final gg3 R() {
        gg3 gg3 = this.q;
        if (gg3 != null) {
            return gg3;
        }
        throw new IllegalStateException("Component not created");
    }

    @DexIgnore
    @Override // com.fossil.ln3
    public final yr3 b() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.ln3
    public final im3 c() {
        y(this.j);
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.ln3
    public final kl3 d() {
        y(this.i);
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.ln3
    public final Context e() {
        return this.f2846a;
    }

    @DexIgnore
    public final void f() {
        c().h();
        if (z().e.a() == 0) {
            z().e.b(this.n.b());
        }
        if (Long.valueOf(z().j.a()).longValue() == 0) {
            d().N().b("Persisting first open", Long.valueOf(this.F));
            z().j.b(this.F);
        }
        if (this.g.s(xg3.R0)) {
            E().h.c();
        }
        if (t()) {
            if (!TextUtils.isEmpty(Q().D()) || !TextUtils.isEmpty(Q().E())) {
                F();
                if (kr3.i0(Q().D(), z().C(), Q().E(), z().D())) {
                    d().L().a("Rechecking which service to use due to a GMP App Id change");
                    z().F();
                    H().H();
                    this.t.b0();
                    this.t.Z();
                    z().j.b(this.F);
                    z().l.b(null);
                }
                z().y(Q().D());
                z().A(Q().E());
            }
            E().M(z().l.a());
            if (z53.a() && this.g.s(xg3.v0) && !F().N0() && !TextUtils.isEmpty(z().z.a())) {
                d().I().a("Remote config removed with active feature rollouts");
                z().z.b(null);
            }
            if (!TextUtils.isEmpty(Q().D()) || !TextUtils.isEmpty(Q().E())) {
                boolean o2 = o();
                if (!z().I() && !this.g.G()) {
                    z().z(!o2);
                }
                if (o2) {
                    E().e0();
                }
                B().d.a();
                O().S(new AtomicReference<>());
                if (j73.a() && this.g.s(xg3.N0)) {
                    O().F(z().C.a());
                }
            }
        } else if (o()) {
            if (!F().A0("android.permission.INTERNET")) {
                d().F().a("App is missing INTERNET permission");
            }
            if (!F().A0("android.permission.ACCESS_NETWORK_STATE")) {
                d().F().a("App is missing ACCESS_NETWORK_STATE permission");
            }
            if (!ag2.a(this.f2846a).g() && !this.g.Q()) {
                if (!hm3.b(this.f2846a)) {
                    d().F().a("AppMeasurementReceiver not registered/enabled");
                }
                if (!kr3.X(this.f2846a, false)) {
                    d().F().a("AppMeasurementService not registered/enabled");
                }
            }
            d().F().a("Uploading is not possible. App measurement disabled");
        }
        z().t.a(this.g.s(xg3.a0));
    }

    @DexIgnore
    public final void g(lm3 lm3) {
        this.D++;
    }

    @DexIgnore
    public final void i(in3 in3) {
        this.D++;
    }

    @DexIgnore
    public final void k(rn3 rn3) {
        nl3 L;
        String concat;
        c().h();
        pg3 pg3 = new pg3(this);
        pg3.p();
        this.u = pg3;
        dl3 dl3 = new dl3(this, rn3.f);
        dl3.y();
        this.v = dl3;
        gl3 gl3 = new gl3(this);
        gl3.y();
        this.s = gl3;
        fp3 fp3 = new fp3(this);
        fp3.y();
        this.t = fp3;
        this.l.q();
        this.h.q();
        this.w = new cm3(this);
        this.v.z();
        d().L().b("App measurement initialized, version", Long.valueOf(this.g.C()));
        d().L().a("To enable debug logging run: adb shell setprop log.tag.FA VERBOSE");
        String C2 = dl3.C();
        if (TextUtils.isEmpty(this.b)) {
            if (F().C0(C2)) {
                L = d().L();
                concat = "Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none.";
            } else {
                L = d().L();
                String valueOf = String.valueOf(C2);
                concat = valueOf.length() != 0 ? "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ".concat(valueOf) : new String("To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ");
            }
            L.a(concat);
        }
        d().M().a("Debug-level message logging enabled");
        if (this.D != this.E.get()) {
            d().F().c("Not all components initialized", Integer.valueOf(this.D), Integer.valueOf(this.E.get()));
        }
        this.x = true;
    }

    @DexIgnore
    public final /* synthetic */ void l(String str, int i2, Throwable th, byte[] bArr, Map map) {
        List<ResolveInfo> queryIntentActivities;
        boolean z2 = true;
        if (!((i2 == 200 || i2 == 204 || i2 == 304) && th == null)) {
            d().I().c("Network Request for Deferred Deep Link failed. response, exception", Integer.valueOf(i2), th);
            return;
        }
        z().x.a(true);
        if (bArr.length == 0) {
            d().M().a("Deferred Deep Link response empty.");
            return;
        }
        try {
            JSONObject jSONObject = new JSONObject(new String(bArr));
            String optString = jSONObject.optString("deeplink", "");
            String optString2 = jSONObject.optString("gclid", "");
            double optDouble = jSONObject.optDouble("timestamp", 0.0d);
            if (TextUtils.isEmpty(optString)) {
                d().M().a("Deferred Deep Link is empty.");
                return;
            }
            kr3 F2 = F();
            F2.f();
            if (TextUtils.isEmpty(optString) || (queryIntentActivities = F2.e().getPackageManager().queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse(optString)), 0)) == null || queryIntentActivities.isEmpty()) {
                z2 = false;
            }
            if (!z2) {
                d().I().c("Deferred Deep Link validation failed. gclid, deep link", optString2, optString);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString("gclid", optString2);
            bundle.putString("_cis", "ddp");
            this.p.Q("auto", "_cmp", bundle);
            kr3 F3 = F();
            if (!TextUtils.isEmpty(optString) && F3.d0(optString, optDouble)) {
                F3.e().sendBroadcast(new Intent("android.google.analytics.action.DEEPLINK_ACTION"));
            }
        } catch (JSONException e2) {
            d().F().b("Failed to parse the Deferred Deep Link response. exception", e2);
        }
    }

    @DexIgnore
    public final void m(boolean z2) {
        this.A = Boolean.valueOf(z2);
    }

    @DexIgnore
    public final boolean n() {
        return this.A != null && this.A.booleanValue();
    }

    @DexIgnore
    public final boolean o() {
        return p() == 0;
    }

    @DexIgnore
    public final int p() {
        c().h();
        if (this.g.G()) {
            return 1;
        }
        Boolean bool = this.C;
        if (bool != null && bool.booleanValue()) {
            return 2;
        }
        Boolean G2 = z().G();
        if (G2 != null) {
            return !G2.booleanValue() ? 3 : 0;
        }
        zr3 zr3 = this.g;
        zr3.b();
        Boolean A2 = zr3.A("firebase_analytics_collection_enabled");
        if (A2 != null) {
            return !A2.booleanValue() ? 4 : 0;
        }
        Boolean bool2 = this.B;
        if (bool2 != null) {
            return !bool2.booleanValue() ? 5 : 0;
        }
        if (m72.d()) {
            return 6;
        }
        return (!this.g.s(xg3.T) || this.A == null || this.A.booleanValue()) ? 0 : 7;
    }

    @DexIgnore
    public final void q() {
    }

    @DexIgnore
    public final void r() {
        throw new IllegalStateException("Unexpected call on client side");
    }

    @DexIgnore
    public final void s() {
        this.E.incrementAndGet();
    }

    @DexIgnore
    public final boolean t() {
        boolean z2 = true;
        if (this.x) {
            c().h();
            Boolean bool = this.y;
            if (bool == null || this.z == 0 || (bool != null && !bool.booleanValue() && Math.abs(this.n.c() - this.z) > 1000)) {
                this.z = this.n.c();
                Boolean valueOf = Boolean.valueOf(F().A0("android.permission.INTERNET") && F().A0("android.permission.ACCESS_NETWORK_STATE") && (ag2.a(this.f2846a).g() || this.g.Q() || (hm3.b(this.f2846a) && kr3.X(this.f2846a, false))));
                this.y = valueOf;
                if (valueOf.booleanValue()) {
                    if (!F().h0(Q().D(), Q().E(), Q().F()) && TextUtils.isEmpty(Q().E())) {
                        z2 = false;
                    }
                    this.y = Boolean.valueOf(z2);
                }
            }
            return this.y.booleanValue();
        }
        throw new IllegalStateException("AppMeasurement is not initialized");
    }

    @DexIgnore
    public final void u() {
        c().h();
        y(v());
        String C2 = Q().C();
        Pair<String, Boolean> t2 = z().t(C2);
        if (!this.g.I().booleanValue() || ((Boolean) t2.second).booleanValue() || TextUtils.isEmpty((CharSequence) t2.first)) {
            d().M().a("ADID unavailable to retrieve Deferred Deep Link. Skipping");
        } else if (!v().w()) {
            d().I().a("Network is not available for Deferred Deep Link request. Skipping");
        } else {
            URL H = F().H(Q().m().C(), C2, (String) t2.first, z().y.a() - 1);
            ro3 v2 = v();
            om3 om3 = new om3(this);
            v2.h();
            v2.o();
            rc2.k(H);
            rc2.k(om3);
            v2.c().B(new to3(v2, C2, H, null, null, om3));
        }
    }

    @DexIgnore
    public final ro3 v() {
        y(this.r);
        return this.r;
    }

    @DexIgnore
    public final zr3 w() {
        return this.g;
    }

    @DexIgnore
    public final xl3 z() {
        j(this.h);
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.ln3
    public final ef2 zzm() {
        return this.n;
    }
}
