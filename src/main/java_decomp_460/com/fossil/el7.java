package com.fossil;

import com.fossil.dl7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class el7 {
    @DexIgnore
    public static final Object a(Throwable th) {
        pq7.c(th, "exception");
        return new dl7.b(th);
    }

    @DexIgnore
    public static final void b(Object obj) {
        if (obj instanceof dl7.b) {
            throw ((dl7.b) obj).exception;
        }
    }
}
