package com.fossil;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u46 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ s46 f3514a;
    @DexIgnore
    public /* final */ LoaderManager b;

    @DexIgnore
    public u46(s46 s46, LoaderManager loaderManager) {
        pq7.c(s46, "mView");
        pq7.c(loaderManager, "mLoaderManager");
        this.f3514a = s46;
        this.b = loaderManager;
    }

    @DexIgnore
    public final LoaderManager a() {
        return this.b;
    }

    @DexIgnore
    public final s46 b() {
        return this.f3514a;
    }
}
