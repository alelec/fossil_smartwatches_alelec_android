package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j43 extends i43 {
    @DexIgnore
    public static int e(byte[] bArr, int i, long j, int i2) {
        if (i2 == 0) {
            return g43.a(i);
        }
        if (i2 == 1) {
            return g43.b(i, e43.a(bArr, j));
        }
        if (i2 == 2) {
            return g43.c(i, e43.a(bArr, j), e43.a(bArr, 1 + j));
        }
        throw new AssertionError();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0059, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00c6, code lost:
        return -1;
     */
    @DexIgnore
    @Override // com.fossil.i43
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(int r9, byte[] r10, int r11, int r12) {
        /*
        // Method dump skipped, instructions count: 241
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.j43.a(int, byte[], int, int):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036 A[LOOP:1: B:12:0x0036->B:18:0x004b, LOOP_START, PHI: r2 r4 r6 
      PHI: (r2v9 long) = (r2v8 long), (r2v34 long) binds: [B:10:0x0032, B:18:0x004b] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r4v4 long) = (r4v3 long), (r4v22 long) binds: [B:10:0x0032, B:18:0x004b] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r6v2 int) = (r6v1 int), (r6v9 int) binds: [B:10:0x0032, B:18:0x004b] A[DONT_GENERATE, DONT_INLINE]] */
    @DexIgnore
    @Override // com.fossil.i43
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int b(java.lang.CharSequence r15, byte[] r16, int r17, int r18) {
        /*
        // Method dump skipped, instructions count: 378
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.j43.b(java.lang.CharSequence, byte[], int, int):int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: CFG modification limit reached, blocks count: 143
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.processBlocksTree(BlockProcessor.java:72)
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.visit(BlockProcessor.java:46)
        */
    @DexIgnore
    @Override // com.fossil.i43
    public final java.lang.String c(byte[] r13, int r14, int r15) throws com.fossil.l13 {
        /*
        // Method dump skipped, instructions count: 222
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.j43.c(byte[], int, int):java.lang.String");
    }
}
