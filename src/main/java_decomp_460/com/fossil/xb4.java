package com.fossil;

import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xb4 implements Flushable {
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public int d; // = 0;
    @DexIgnore
    public /* final */ OutputStream e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends IOException {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = -6947486886997889499L;

        @DexIgnore
        public a() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }
    }

    @DexIgnore
    public xb4(OutputStream outputStream, byte[] bArr) {
        this.e = outputStream;
        this.b = bArr;
        this.c = bArr.length;
    }

    @DexIgnore
    public static xb4 A(OutputStream outputStream, int i) {
        return new xb4(outputStream, new byte[i]);
    }

    @DexIgnore
    public static int a(int i, boolean z) {
        return r(i) + b(z);
    }

    @DexIgnore
    public static int b(boolean z) {
        return 1;
    }

    @DexIgnore
    public static int c(int i, vb4 vb4) {
        return r(i) + f(vb4);
    }

    @DexIgnore
    public static int f(vb4 vb4) {
        return l(vb4.f()) + vb4.f();
    }

    @DexIgnore
    public static int g(int i, int i2) {
        return r(i) + h(i2);
    }

    @DexIgnore
    public static int h(int i) {
        return k(i);
    }

    @DexIgnore
    public static int i(int i, float f) {
        return r(i) + j(f);
    }

    @DexIgnore
    public static int j(float f) {
        return 4;
    }

    @DexIgnore
    public static int k(int i) {
        if (i >= 0) {
            return l(i);
        }
        return 10;
    }

    @DexIgnore
    public static int l(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (-268435456 & i) == 0 ? 4 : 5;
    }

    @DexIgnore
    public static int m(long j) {
        if ((-128 & j) == 0) {
            return 1;
        }
        if ((-16384 & j) == 0) {
            return 2;
        }
        if ((-2097152 & j) == 0) {
            return 3;
        }
        if ((-268435456 & j) == 0) {
            return 4;
        }
        if ((-34359738368L & j) == 0) {
            return 5;
        }
        if ((-4398046511104L & j) == 0) {
            return 6;
        }
        if ((-562949953421312L & j) == 0) {
            return 7;
        }
        if ((-72057594037927936L & j) == 0) {
            return 8;
        }
        return (Long.MIN_VALUE & j) == 0 ? 9 : 10;
    }

    @DexIgnore
    public static int o(int i, int i2) {
        return r(i) + q(i2);
    }

    @DexIgnore
    public static int q(int i) {
        return l(y(i));
    }

    @DexIgnore
    public static int r(int i) {
        return l(zb4.a(i, 0));
    }

    @DexIgnore
    public static int s(int i, int i2) {
        return r(i) + t(i2);
    }

    @DexIgnore
    public static int t(int i) {
        return l(i);
    }

    @DexIgnore
    public static int u(int i, long j) {
        return r(i) + w(j);
    }

    @DexIgnore
    public static int w(long j) {
        return m(j);
    }

    @DexIgnore
    public static int y(int i) {
        return (i >> 31) ^ (i << 1);
    }

    @DexIgnore
    public static xb4 z(OutputStream outputStream) {
        return A(outputStream, 4096);
    }

    @DexIgnore
    public final void B() throws IOException {
        OutputStream outputStream = this.e;
        if (outputStream != null) {
            outputStream.write(this.b, 0, this.d);
            this.d = 0;
            return;
        }
        throw new a();
    }

    @DexIgnore
    public void C(int i, boolean z) throws IOException {
        d0(i, 0);
        D(z);
    }

    @DexIgnore
    public void D(boolean z) throws IOException {
        R(z ? 1 : 0);
    }

    @DexIgnore
    public void F(int i, vb4 vb4) throws IOException {
        d0(i, 2);
        G(vb4);
    }

    @DexIgnore
    public void G(vb4 vb4) throws IOException {
        X(vb4.f());
        S(vb4);
    }

    @DexIgnore
    public void H(int i, int i2) throws IOException {
        d0(i, 0);
        I(i2);
    }

    @DexIgnore
    public void I(int i) throws IOException {
        P(i);
    }

    @DexIgnore
    public void L(int i, float f) throws IOException {
        d0(i, 5);
        M(f);
    }

    @DexIgnore
    public void M(float f) throws IOException {
        W(Float.floatToRawIntBits(f));
    }

    @DexIgnore
    public void P(int i) throws IOException {
        if (i >= 0) {
            X(i);
        } else {
            Y((long) i);
        }
    }

    @DexIgnore
    public void Q(byte b2) throws IOException {
        if (this.d == this.c) {
            B();
        }
        byte[] bArr = this.b;
        int i = this.d;
        this.d = i + 1;
        bArr[i] = (byte) b2;
    }

    @DexIgnore
    public void R(int i) throws IOException {
        Q((byte) i);
    }

    @DexIgnore
    public void S(vb4 vb4) throws IOException {
        T(vb4, 0, vb4.f());
    }

    @DexIgnore
    public void T(vb4 vb4, int i, int i2) throws IOException {
        int i3 = this.c;
        int i4 = this.d;
        if (i3 - i4 >= i2) {
            vb4.d(this.b, i, i4, i2);
            this.d += i2;
            return;
        }
        int i5 = i3 - i4;
        vb4.d(this.b, i, i4, i5);
        int i6 = i + i5;
        int i7 = i2 - i5;
        this.d = this.c;
        B();
        if (i7 <= this.c) {
            vb4.d(this.b, i6, 0, i7);
            this.d = i7;
            return;
        }
        InputStream e2 = vb4.e();
        long j = (long) i6;
        if (j == e2.skip(j)) {
            while (i7 > 0) {
                int min = Math.min(i7, this.c);
                int read = e2.read(this.b, 0, min);
                if (read == min) {
                    this.e.write(this.b, 0, read);
                    i7 -= read;
                } else {
                    throw new IllegalStateException("Read failed.");
                }
            }
            return;
        }
        throw new IllegalStateException("Skip failed.");
    }

    @DexIgnore
    public void U(byte[] bArr) throws IOException {
        V(bArr, 0, bArr.length);
    }

    @DexIgnore
    public void V(byte[] bArr, int i, int i2) throws IOException {
        int i3 = this.c;
        int i4 = this.d;
        if (i3 - i4 >= i2) {
            System.arraycopy(bArr, i, this.b, i4, i2);
            this.d += i2;
            return;
        }
        int i5 = i3 - i4;
        System.arraycopy(bArr, i, this.b, i4, i5);
        int i6 = i + i5;
        int i7 = i2 - i5;
        this.d = this.c;
        B();
        if (i7 <= this.c) {
            System.arraycopy(bArr, i6, this.b, 0, i7);
            this.d = i7;
            return;
        }
        this.e.write(bArr, i6, i7);
    }

    @DexIgnore
    public void W(int i) throws IOException {
        R(i & 255);
        R((i >> 8) & 255);
        R((i >> 16) & 255);
        R((i >> 24) & 255);
    }

    @DexIgnore
    public void X(int i) throws IOException {
        while ((i & -128) != 0) {
            R((i & 127) | 128);
            i >>>= 7;
        }
        R(i);
    }

    @DexIgnore
    public void Y(long j) throws IOException {
        while ((-128 & j) != 0) {
            R((((int) j) & 127) | 128);
            j >>>= 7;
        }
        R((int) j);
    }

    @DexIgnore
    public void b0(int i, int i2) throws IOException {
        d0(i, 0);
        c0(i2);
    }

    @DexIgnore
    public void c0(int i) throws IOException {
        X(y(i));
    }

    @DexIgnore
    public void d0(int i, int i2) throws IOException {
        X(zb4.a(i, i2));
    }

    @DexIgnore
    public void e0(int i, int i2) throws IOException {
        d0(i, 0);
        f0(i2);
    }

    @DexIgnore
    public void f0(int i) throws IOException {
        X(i);
    }

    @DexIgnore
    @Override // java.io.Flushable
    public void flush() throws IOException {
        if (this.e != null) {
            B();
        }
    }

    @DexIgnore
    public void g0(int i, long j) throws IOException {
        d0(i, 0);
        h0(j);
    }

    @DexIgnore
    public void h0(long j) throws IOException {
        Y(j);
    }
}
