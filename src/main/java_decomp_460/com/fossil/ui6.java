package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ui6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ oi6 f3599a;
    @DexIgnore
    public /* final */ fj6 b;
    @DexIgnore
    public /* final */ zi6 c;

    @DexIgnore
    public ui6(oi6 oi6, fj6 fj6, zi6 zi6) {
        pq7.c(oi6, "mHeartRateOverviewDayView");
        pq7.c(fj6, "mHeartRateOverviewWeekView");
        pq7.c(zi6, "mHeartRateOverviewMonthView");
        this.f3599a = oi6;
        this.b = fj6;
        this.c = zi6;
    }

    @DexIgnore
    public final oi6 a() {
        return this.f3599a;
    }

    @DexIgnore
    public final zi6 b() {
        return this.c;
    }

    @DexIgnore
    public final fj6 c() {
        return this.b;
    }
}
