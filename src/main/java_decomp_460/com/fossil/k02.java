package com.fossil;

import com.fossil.g02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k02<T> implements xy1<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ h02 f1850a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ty1 c;
    @DexIgnore
    public /* final */ wy1<T, byte[]> d;
    @DexIgnore
    public /* final */ l02 e;

    @DexIgnore
    public k02(h02 h02, String str, ty1 ty1, wy1<T, byte[]> wy1, l02 l02) {
        this.f1850a = h02;
        this.b = str;
        this.c = ty1;
        this.d = wy1;
        this.e = l02;
    }

    @DexIgnore
    public static /* synthetic */ void c(Exception exc) {
    }

    @DexIgnore
    @Override // com.fossil.xy1
    public void a(uy1<T> uy1) {
        b(uy1, j02.b());
    }

    @DexIgnore
    @Override // com.fossil.xy1
    public void b(uy1<T> uy1, zy1 zy1) {
        l02 l02 = this.e;
        g02.a a2 = g02.a();
        a2.e(this.f1850a);
        a2.c(uy1);
        a2.f(this.b);
        a2.d(this.d);
        a2.b(this.c);
        l02.a(a2.a(), zy1);
    }
}
