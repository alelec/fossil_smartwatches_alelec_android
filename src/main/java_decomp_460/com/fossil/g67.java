package com.fossil;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g67 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public static /* final */ String d; // = "g67";

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ FragmentManager f1267a;
    @DexIgnore
    public /* final */ List<Fragment> b;
    @DexIgnore
    public int[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public a(g67 g67, View view) {
            super(view);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder b;

        @DexIgnore
        public b(RecyclerView.ViewHolder viewHolder) {
            this.b = viewHolder;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            this.b.itemView.removeOnAttachStateChangeListener(this);
            pv5 pv5 = (pv5) g67.this.b.get(this.b.getAdapterPosition());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g67.d;
            local.d(str, "onBindViewHolder tag=" + pv5.D6() + " childFragmentManager=" + g67.this.f1267a + "this=" + this);
            Fragment Z = g67.this.f1267a.Z(pv5.D6());
            if (Z != null) {
                xq0 j = g67.this.f1267a.j();
                j.q(Z);
                j.k();
            }
            xq0 j2 = g67.this.f1267a.j();
            j2.b(view.getId(), pv5, pv5.D6());
            j2.k();
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
        }
    }

    @DexIgnore
    public g67(FragmentManager fragmentManager, List<Fragment> list) {
        this.f1267a = fragmentManager;
        this.b = list;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        int size = this.b.size();
        int[] iArr = this.c;
        if (iArr == null || size != iArr.length) {
            this.c = new int[size];
        }
        return size;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        this.c[i] = this.b.get(i).getId();
        int[] iArr = this.c;
        if (iArr[i] == 0) {
            iArr[i] = View.generateViewId();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = d;
            local.d(str, "getItemId: position = " + i + ", id = " + this.c[i]);
        }
        return (long) this.c[i];
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        local.d(str, "onBindViewHolder: position = " + i);
        Fragment Y = this.f1267a.Y(viewHolder.itemView.getId());
        if (Y != null) {
            xq0 j = this.f1267a.j();
            j.q(Y);
            j.k();
        }
        viewHolder.itemView.setId((int) getItemId(i));
        viewHolder.itemView.addOnAttachStateChangeListener(new b(viewHolder));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
        frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -1));
        return new a(this, frameLayout);
    }
}
