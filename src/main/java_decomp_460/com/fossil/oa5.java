package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomEditGoalView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oa5 extends na5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d P; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Q;
    @DexIgnore
    public long O;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        Q = sparseIntArray;
        sparseIntArray.put(2131363410, 1);
        Q.put(2131362666, 2);
        Q.put(2131363468, 3);
        Q.put(2131363158, 4);
        Q.put(2131361993, 5);
        Q.put(2131363473, 6);
        Q.put(2131362588, 7);
        Q.put(2131362014, 8);
        Q.put(2131362015, 9);
        Q.put(2131362012, 10);
        Q.put(2131362013, 11);
        Q.put(2131362443, 12);
        Q.put(2131362809, 13);
        Q.put(2131362299, 14);
        Q.put(2131362446, 15);
        Q.put(2131362827, 16);
        Q.put(2131362306, 17);
        Q.put(2131362531, 18);
        Q.put(2131362307, 19);
        Q.put(2131362532, 20);
        Q.put(2131361910, 21);
        Q.put(2131362405, 22);
        Q.put(2131363469, 23);
    }
    */

    @DexIgnore
    public oa5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 24, P, Q));
    }

    @DexIgnore
    public oa5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (Barrier) objArr[21], (ConstraintLayout) objArr[5], (CustomEditGoalView) objArr[10], (CustomEditGoalView) objArr[11], (CustomEditGoalView) objArr[8], (CustomEditGoalView) objArr[9], (FlexibleEditText) objArr[14], (FlexibleEditText) objArr[17], (FlexibleEditText) objArr[19], (FlexibleTextView) objArr[22], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[18], (FlexibleTextView) objArr[20], (Guideline) objArr[7], (RTLImageView) objArr[2], (LinearLayout) objArr[13], (LinearLayout) objArr[16], (ConstraintLayout) objArr[0], (ScrollView) objArr[4], (FlexibleTextView) objArr[1], (View) objArr[3], (View) objArr[23], (Guideline) objArr[6]);
        this.O = -1;
        this.I.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.O = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.O != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.O = 1;
        }
        w();
    }
}
