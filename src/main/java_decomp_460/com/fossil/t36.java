package com.fossil;

import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t36 implements Factory<s36> {
    @DexIgnore
    public static s36 a(r36 r36, DNDSettingsDatabase dNDSettingsDatabase) {
        return new s36(r36, dNDSettingsDatabase);
    }
}
