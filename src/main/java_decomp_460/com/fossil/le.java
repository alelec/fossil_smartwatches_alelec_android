package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class le<T> implements Comparator<lp> {
    @DexIgnore
    public static /* final */ le b; // = new le();

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // java.util.Comparator
    public int compare(lp lpVar, lp lpVar2) {
        return lpVar.y().ordinal() - lpVar2.y().ordinal();
    }
}
