package com.fossil;

import com.fossil.tn7;
import java.io.Closeable;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mw7 extends dv7 implements Closeable {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends on7<dv7, mw7> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mw7$a$a")
        /* renamed from: com.fossil.mw7$a$a  reason: collision with other inner class name */
        public static final class C0160a extends qq7 implements rp7<tn7.b, mw7> {
            @DexIgnore
            public static /* final */ C0160a INSTANCE; // = new C0160a();

            @DexIgnore
            public C0160a() {
                super(1);
            }

            @DexIgnore
            public final mw7 invoke(tn7.b bVar) {
                return (mw7) (!(bVar instanceof mw7) ? null : bVar);
            }
        }

        @DexIgnore
        public a() {
            super(dv7.b, C0160a.INSTANCE);
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public abstract Executor S();
}
