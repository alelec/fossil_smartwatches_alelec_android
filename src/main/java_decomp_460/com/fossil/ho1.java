package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ho1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ do1 c;
    @DexIgnore
    public /* final */ co1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ho1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ho1 createFromParcel(Parcel parcel) {
            int readInt = parcel.readInt();
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                do1 valueOf = do1.valueOf(readString);
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    pq7.b(readString2, "parcel.readString()!!");
                    return new ho1(readInt, valueOf, co1.valueOf(readString2));
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ho1[] newArray(int i) {
            return new ho1[i];
        }
    }

    @DexIgnore
    public ho1(int i, do1 do1, co1 co1) {
        this.b = i;
        this.c = do1;
        this.d = co1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(ho1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ho1 ho1 = (ho1) obj;
            if (this.b != ho1.b) {
                return false;
            }
            if (this.c != ho1.c) {
                return false;
            }
            return this.d == ho1.d;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.AppNotificationEvent");
    }

    @DexIgnore
    public final do1 getAction() {
        return this.c;
    }

    @DexIgnore
    public final co1 getActionStatus() {
        return this.d;
    }

    @DexIgnore
    public final int getNotificationUid() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Integer.valueOf(this.b).hashCode();
        return (((hashCode * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(new JSONObject(), jd0.d4, Integer.valueOf(this.b)), jd0.v0, ey1.a(this.c)), jd0.w0, ey1.a(this.d));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
    }
}
