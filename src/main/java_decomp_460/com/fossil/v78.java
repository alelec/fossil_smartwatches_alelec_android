package com.fossil;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import androidx.fragment.app.Fragment;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.rk0;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v78 {

    @DexIgnore
    public interface a extends rk0.b {
        @DexIgnore
        void C4(int i, List<String> list);

        @DexIgnore
        void k1(int i, List<String> list);
    }

    @DexIgnore
    public static boolean a(Context context, String... strArr) {
        if (Build.VERSION.SDK_INT < 23) {
            Log.w("EasyPermissions", "hasPermissions: API version < M, returning true by default");
            return true;
        } else if (context != null) {
            for (String str : strArr) {
                if (gl0.a(context, str) != 0) {
                    return false;
                }
            }
            return true;
        } else {
            throw new IllegalArgumentException("Can't check permissions for null context");
        }
    }

    @DexIgnore
    public static boolean b(Object obj) {
        if (!obj.getClass().getSimpleName().endsWith(LocaleConverter.LOCALE_DELIMITER)) {
            return false;
        }
        try {
            return Class.forName("org.androidannotations.api.view.HasViews").isInstance(obj);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    @DexIgnore
    public static void c(int i, String[] strArr, int[] iArr, Object... objArr) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i2 = 0; i2 < strArr.length; i2++) {
            String str = strArr[i2];
            if (iArr[i2] == 0) {
                arrayList.add(str);
            } else {
                arrayList2.add(str);
            }
        }
        for (Object obj : objArr) {
            if (!arrayList.isEmpty() && (obj instanceof a)) {
                ((a) obj).C4(i, arrayList);
            }
            if (!arrayList2.isEmpty() && (obj instanceof a)) {
                ((a) obj).k1(i, arrayList2);
            }
            if (!arrayList.isEmpty() && arrayList2.isEmpty()) {
                e(obj, i);
            }
        }
    }

    @DexIgnore
    public static boolean d(Fragment fragment, String str) {
        return y78.b(fragment).c(str);
    }

    @DexIgnore
    public static void e(Object obj, int i) {
        Class<?> cls = obj.getClass();
        for (Class<? super Object> superclass = b(obj) ? cls.getSuperclass() : cls; superclass != null; superclass = superclass.getSuperclass()) {
            Method[] declaredMethods = superclass.getDeclaredMethods();
            for (Method method : declaredMethods) {
                t78 t78 = (t78) method.getAnnotation(t78.class);
                if (t78 != null && t78.value() == i) {
                    if (method.getParameterTypes().length <= 0) {
                        try {
                            if (!method.isAccessible()) {
                                method.setAccessible(true);
                            }
                            method.invoke(obj, new Object[0]);
                        } catch (IllegalAccessException e) {
                            Log.e("EasyPermissions", "runDefaultMethod:IllegalAccessException", e);
                        } catch (InvocationTargetException e2) {
                            Log.e("EasyPermissions", "runDefaultMethod:InvocationTargetException", e2);
                        }
                    } else {
                        throw new RuntimeException("Cannot execute method " + method.getName() + " because it is non-void method and/or has input parameters.");
                    }
                }
            }
        }
    }

    @DexIgnore
    public static boolean f(Fragment fragment, List<String> list) {
        return y78.b(fragment).e(list);
    }
}
