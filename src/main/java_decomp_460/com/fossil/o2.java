package com.fossil;

import android.os.Parcel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o2 extends c2 {
    @DexIgnore
    public static /* final */ n2 CREATOR; // = new n2(null);

    @DexIgnore
    public o2(byte b) {
        super(lt.HEARTBEAT_EVENT, b, false, 4);
    }

    @DexIgnore
    public /* synthetic */ o2(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    @Override // com.fossil.c2
    public byte[] a() {
        return new byte[]{sb.c.b};
    }
}
