package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cr6;
import com.fossil.fr4;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zq6 extends pv5 implements fr4.a, t47.g {
    @DexIgnore
    public static String l; // = "";
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public g37<rb5> h;
    @DexIgnore
    public cr6 i;
    @DexIgnore
    public fr4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final zq6 a() {
            return new zq6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<cr6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ zq6 f4516a;

        @DexIgnore
        public b(zq6 zq6) {
            this.f4516a = zq6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cr6.a aVar) {
            FragmentActivity activity;
            ArrayList<Theme> c;
            if (!(aVar == null || (c = aVar.c()) == null)) {
                this.f4516a.M6(c);
                zq6 zq6 = this.f4516a;
                String e = aVar.e();
                if (e != null) {
                    String a2 = aVar.a();
                    if (a2 != null) {
                        zq6.L6(e, a2, aVar.d());
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            if (aVar.b() && (activity = this.f4516a.getActivity()) != null) {
                HomeActivity.a aVar2 = HomeActivity.B;
                pq7.b(activity, "it");
                HomeActivity.a.b(aVar2, activity, null, 2, null);
                this.f4516a.e0();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ zq6 b;

        @DexIgnore
        public c(zq6 zq6) {
            this.b = zq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ zq6 b;

        @DexIgnore
        public d(zq6 zq6) {
            this.b = zq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            zq6.K6(this.b).m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ zq6 b;

        @DexIgnore
        public e(zq6 zq6) {
            this.b = zq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                String valueOf = String.valueOf(System.currentTimeMillis());
                qn5.l.a().c(valueOf);
                UserCustomizeThemeActivity.a aVar = UserCustomizeThemeActivity.A;
                pq7.b(activity, "it");
                aVar.a(activity, valueOf, false);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ cr6 K6(zq6 zq6) {
        cr6 cr6 = zq6.i;
        if (cr6 != null) {
            return cr6;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.fr4.a
    public void D2(String str) {
        pq7.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "onUserChooseTheme id=" + str);
        cr6 cr6 = this.i;
        if (cr6 != null) {
            cr6.q(str);
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.fr4.a
    public void L4(String str) {
        pq7.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "onUserDeleteTheme id=" + str);
        s37 s37 = s37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        s37.n(childFragmentManager);
        l = str;
    }

    @DexIgnore
    public final void L6(String str, String str2, List<Style> list) {
        pq7.c(str, "userSelectedThemeId");
        pq7.c(str2, "currentThemeId");
        g37<rb5> g37 = this.h;
        if (g37 != null) {
            rb5 a2 = g37.a();
            if (a2 != null && list != null) {
                fr4 fr4 = this.j;
                if (fr4 != null) {
                    fr4.l(list, str);
                }
                String e2 = qn5.l.a().e(Explore.COLUMN_BACKGROUND, list);
                String e3 = qn5.l.a().e("primaryText", list);
                Typeface g2 = qn5.l.a().g("headline2", list);
                if (e2 != null) {
                    a2.r.setBackgroundColor(Color.parseColor(e2));
                }
                if (e3 != null) {
                    a2.v.setTextColor(Color.parseColor(e3));
                    a2.q.setColorFilter(Color.parseColor(e3));
                }
                if (g2 != null) {
                    FlexibleTextView flexibleTextView = a2.v;
                    pq7.b(flexibleTextView, "it.tvTitle");
                    flexibleTextView.setTypeface(g2);
                }
                String e4 = qn5.l.a().e("onPrimaryButton", list);
                Typeface g3 = qn5.l.a().g("textButtonPrimary", list);
                String e5 = qn5.l.a().e("primaryButton", list);
                if (e4 != null) {
                    a2.t.setTextColor(Color.parseColor(e4));
                    a2.u.setTextColor(Color.parseColor(e4));
                }
                if (g3 != null) {
                    FlexibleButton flexibleButton = a2.t;
                    pq7.b(flexibleButton, "it.tvApply");
                    flexibleButton.setTypeface(g3);
                    FlexibleButton flexibleButton2 = a2.u;
                    pq7.b(flexibleButton2, "it.tvCreateNewTheme");
                    flexibleButton2.setTypeface(g3);
                }
                if (e5 != null) {
                    a2.t.setBackgroundColor(Color.parseColor(e5));
                    a2.u.setBackgroundColor(Color.parseColor(e5));
                    return;
                }
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void M6(ArrayList<Theme> arrayList) {
        pq7.c(arrayList, "listTheme");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "showListTheme size=" + arrayList.size());
        fr4 fr4 = this.j;
        if (fr4 != null) {
            fr4.o(arrayList);
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        FLogger.INSTANCE.getLocal().d("ThemesFragment", "onDialogFragmentResult");
        if (str.hashCode() == -1478349291 && str.equals("DELETE_THEME") && i2 == 2131363373) {
            cr6 cr6 = this.i;
            if (cr6 != null) {
                cr6.n(l);
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.fr4.a
    public void g1(String str) {
        pq7.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "onUserEditTheme id=" + str);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            UserCustomizeThemeActivity.a aVar = UserCustomizeThemeActivity.A;
            pq7.b(activity, "it");
            aVar.a(activity, str, true);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        rb5 rb5 = (rb5) aq0.f(LayoutInflater.from(getContext()), 2131558629, null, false, A6());
        this.j = new fr4(new ArrayList(), this);
        PortfolioApp.h0.c().M().v(new br6()).a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(cr6.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026mesViewModel::class.java)");
            cr6 cr6 = (cr6) a2;
            this.i = cr6;
            if (cr6 != null) {
                cr6.p().h(getViewLifecycleOwner(), new b(this));
                cr6 cr62 = this.i;
                if (cr62 != null) {
                    cr62.r();
                    this.h = new g37<>(this, rb5);
                    pq7.b(rb5, "binding");
                    return rb5.n();
                }
                pq7.n("mViewModel");
                throw null;
            }
            pq7.n("mViewModel");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        cr6 cr6 = this.i;
        if (cr6 != null) {
            cr6.r();
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<rb5> g37 = this.h;
        if (g37 != null) {
            rb5 a2 = g37.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new c(this));
                a2.t.setOnClickListener(new d(this));
                a2.u.setOnClickListener(new e(this));
                RecyclerView recyclerView = a2.s;
                pq7.b(recyclerView, "binding.rvThemes");
                recyclerView.setAdapter(this.j);
                RecyclerView recyclerView2 = a2.s;
                pq7.b(recyclerView2, "binding.rvThemes");
                recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView3 = a2.s;
                pq7.b(recyclerView3, "binding.rvThemes");
                recyclerView3.setNestedScrollingEnabled(false);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
