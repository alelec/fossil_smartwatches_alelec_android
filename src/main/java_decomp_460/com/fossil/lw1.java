package com.fossil;

import android.graphics.Bitmap;
import android.os.Parcel;
import com.fossil.imagefilters.EInkImageFactory;
import com.fossil.imagefilters.Format;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lw1 extends iw1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public byte[] l;
    @DexIgnore
    public /* final */ ec0 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final lw1 a(byte[] bArr) {
            try {
                iw1 iw1 = (iw1) ga.d.f(bArr);
                if (iw1 instanceof lw1) {
                    return (lw1) iw1;
                }
            } catch (IllegalArgumentException e) {
            }
            return null;
        }
    }

    @DexIgnore
    public lw1(Parcel parcel) {
        super(parcel);
        cc0 cc0;
        byte[] bArr;
        cc0[] c = c();
        int length = c.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                cc0 = null;
                break;
            }
            cc0 = c[i];
            if (pq7.a(cc0.b, "theme_class")) {
                break;
            }
            i++;
        }
        if (!(cc0 == null || (bArr = cc0.c) == null)) {
            try {
                ec0 a2 = ec0.g.a(new String(dm7.k(bArr, 0, bArr.length - 1), hd0.y.c()));
                if (a2 != null) {
                    this.m = a2;
                    k();
                    return;
                }
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        }
        throw new IllegalArgumentException("Invalid Theme Classifier");
    }

    @DexIgnore
    public lw1(ry1 ry1, yb0 yb0, cc0[] cc0Arr, cc0[] cc0Arr2, cc0[] cc0Arr3, cc0[] cc0Arr4, cc0[] cc0Arr5, cc0[] cc0Arr6, cc0[] cc0Arr7) throws IllegalArgumentException {
        super(ry1, yb0, cc0Arr, cc0Arr2, cc0Arr3, cc0Arr4, cc0Arr5, cc0Arr6, cc0Arr7);
        cc0 cc0;
        byte[] bArr;
        cc0[] c = c();
        int length = c.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                cc0 = null;
                break;
            }
            cc0 = c[i];
            if (pq7.a(cc0.b, "theme_class")) {
                break;
            }
            i++;
        }
        if (!(cc0 == null || (bArr = cc0.c) == null)) {
            try {
                ec0 a2 = ec0.g.a(new String(dm7.k(bArr, 0, bArr.length - 1), hd0.y.c()));
                if (a2 != null) {
                    this.m = a2;
                    k();
                    return;
                }
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        }
        throw new IllegalArgumentException("Invalid Theme Classifier");
    }

    @DexIgnore
    @Override // com.fossil.iw1, com.fossil.iw1, java.lang.Object
    public abstract /* synthetic */ nx1 clone();

    @DexIgnore
    public kw1 edit() {
        return null;
    }

    @DexIgnore
    public final byte[] getPreviewImageData() {
        cc0 cc0;
        byte[] bArr = null;
        if (this.l == null) {
            cc0[] b = b();
            int length = b.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    cc0 = null;
                    break;
                }
                cc0 = b[i];
                if (pq7.a(cc0.b, "!preview.rle")) {
                    break;
                }
                i++;
            }
            if (cc0 != null) {
                Bitmap decode = EInkImageFactory.decode(cc0.c, Format.RLE);
                pq7.b(decode, "EInkImageFactory.decode(\u2026ageNode.data, Format.RLE)");
                bArr = cy1.b(decode, null, 1, null);
                decode.recycle();
            }
            this.l = bArr;
        }
        return this.l;
    }

    @DexIgnore
    public final ec0 getThemeClassifier() {
        return this.m;
    }

    @DexIgnore
    public final lw1 j() {
        cc0[] f = f();
        cc0 cc0 = (cc0) em7.L(f, 0);
        if (cc0 != null) {
            f[0] = new cc0("previewWatchFace", cc0.c);
        }
        int i = fc0.f1100a[this.m.ordinal()];
        if (i == 1) {
            return new qw1(h(), g(), f, b(), d(), e(), c(), a(), i());
        }
        if (i == 2) {
            return new ow1(h(), g(), f, b(), d(), e(), c(), a(), i());
        }
        if (i == 3) {
            return new tw1(h(), g(), f, b(), d(), e(), c(), a(), i());
        }
        throw new al7();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0016, code lost:
        if ((!(f().length == 0)) != false) goto L_0x0018;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void k() {
        /*
            r4 = this;
            r0 = 1
            r1 = 0
            com.fossil.yb0 r2 = r4.g()
            com.fossil.jw1 r2 = r2.b
            com.fossil.jw1 r3 = com.fossil.jw1.THEME
            if (r2 != r3) goto L_0x0029
            com.fossil.cc0[] r2 = r4.f()
            int r2 = r2.length
            if (r2 != 0) goto L_0x001b
            r2 = r0
        L_0x0014:
            r2 = r2 ^ 1
            if (r2 == 0) goto L_0x0029
        L_0x0018:
            if (r0 == 0) goto L_0x001d
            return
        L_0x001b:
            r2 = r1
            goto L_0x0014
        L_0x001d:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Incorrect package type."
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0029:
            r0 = r1
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.lw1.k():void");
    }

    @DexIgnore
    @Override // com.fossil.iw1, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(new JSONObject(), jd0.U0, hy1.k((int) getPackageCrc(), null, 1, null)), jd0.M4, getBundleId());
    }
}
