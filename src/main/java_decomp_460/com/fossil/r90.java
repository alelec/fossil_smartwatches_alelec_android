package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r90 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ q90 CREATOR; // = new q90(null);
    @DexIgnore
    public /* final */ ca0 b;
    @DexIgnore
    public /* final */ w90 c;
    @DexIgnore
    public /* final */ da0 d;
    @DexIgnore
    public /* final */ ea0 e;
    @DexIgnore
    public /* final */ short f;

    @DexIgnore
    public r90(ca0 ca0, w90 w90, da0 da0, ea0 ea0, short s) {
        this.b = ca0;
        this.c = w90;
        this.d = da0;
        this.e = ea0;
        this.f = (short) s;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(r90.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            r90 r90 = (r90) obj;
            return this.b == r90.b && this.c == r90.c && this.d == r90.d && this.e == r90.e && this.f == r90.f;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.animation.HandAnimation");
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = this.c.hashCode();
        return (((((((hashCode * 31) + hashCode2) * 31) + this.d.hashCode()) * 31) + this.e.hashCode()) * 31) + this.f;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.X0, ey1.a(this.b)), jd0.Z0, ey1.a(this.c)), jd0.W0, ey1.a(this.d)), jd0.a1, ey1.a(this.e)), jd0.R3, Short.valueOf(this.f));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.f);
        }
    }
}
