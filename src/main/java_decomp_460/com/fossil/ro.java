package com.fossil;

import com.fossil.ix1;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ro extends mj {
    @DexIgnore
    public byte[] E;
    @DexIgnore
    public long F;
    @DexIgnore
    public float G;
    @DexIgnore
    public long H;
    @DexIgnore
    public long I;
    @DexIgnore
    public boolean J;
    @DexIgnore
    public int K;
    @DexIgnore
    public n6 L;
    @DexIgnore
    public long M;
    @DexIgnore
    public long N;
    @DexIgnore
    public long O;
    @DexIgnore
    public long P;
    @DexIgnore
    public /* final */ boolean Q;
    @DexIgnore
    public /* final */ float R;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ro(k5 k5Var, i60 i60, yp ypVar, boolean z, short s, float f, String str, boolean z2, int i) {
        super(k5Var, i60, ypVar, s, (i & 64) != 0 ? e.a("UUID.randomUUID().toString()") : str, (i & 128) != 0 ? true : z2);
        f = (i & 32) != 0 ? 0.001f : f;
        this.Q = z;
        this.R = f;
        this.E = new byte[0];
        this.J = true;
        this.L = n6.UNKNOWN;
    }

    @DexIgnore
    public static final /* synthetic */ void J(ro roVar, long j, long j2) {
        long N2 = roVar.N();
        if (1 <= j && N2 >= j) {
            long b = ix1.f1688a.b(roVar.E, ix1.a.CRC32);
            roVar.G(j);
            if (j2 != b) {
                k5 k5Var = roVar.w;
                i60 i60 = roVar.x;
                String str = k5Var.x;
                short s = roVar.D;
                byte[] bArr = roVar.E;
                lp.h(roVar, new oi(k5Var, i60, new j0(str, j0.CREATOR.b(s), j0.CREATOR.a(s), bArr, hy1.o(bArr.length), ix1.f1688a.b(roVar.E, ix1.a.CRC32), 0, false), roVar.z), new tn(roVar), new fo(roVar), null, null, null, 56, null);
            } else if (j == roVar.N()) {
                lp.j(roVar, hs.n, null, 2, null);
            } else {
                roVar.Q();
            }
        } else {
            roVar.G(0);
            roVar.Q();
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        if (!(!(this.E.length == 0))) {
            l(nr.a(this.v, null, zq.SUCCESS, null, null, 13));
        } else if (this.Q) {
            lp.j(this, hs.m, null, 2, null);
        } else {
            lp.j(this, hs.o, null, 2, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.mj
    public JSONObject C() {
        return g80.k(g80.k(g80.k(super.C(), jd0.I, Integer.valueOf(this.E.length)), jd0.J, Long.valueOf(ix1.f1688a.b(this.E, ix1.a.CRC32))), jd0.x0, Boolean.valueOf(this.Q));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void D() {
        try {
            this.E = M();
            this.F = 0;
            this.H = 0;
            this.I = 0;
            this.J = true;
            this.K = 0;
            this.L = n6.UNKNOWN;
        } catch (sx1 e) {
            d90.i.i(e);
            l(nr.a(this.v, null, zq.UNSUPPORTED_FORMAT, null, null, 13));
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        return g80.k(g80.k(super.E(), jd0.o4, Long.valueOf(Math.max(this.N - this.M, 0L))), jd0.p4, Long.valueOf(Math.max(this.P - this.O, 0L)));
    }

    @DexIgnore
    public final void G(long j) {
        this.H = this.I;
        this.I = j;
        this.F = j;
    }

    @DexIgnore
    public final void H(oi oiVar) {
        G(oiVar.D);
        long j = this.I;
        this.F = j;
        float N2 = (((float) j) * 1.0f) / ((float) N());
        if (Math.abs(N2 - this.G) > this.R || N2 == 1.0f) {
            this.G = N2;
            d(N2);
        }
        lp.j(this, hs.m, null, 2, null);
    }

    @DexIgnore
    public final zs L() {
        if (this.N == 0) {
            this.N = System.currentTimeMillis();
        }
        if (this.O == 0) {
            this.O = System.currentTimeMillis();
        }
        long N2 = N();
        long j = this.I;
        short s = this.D;
        byte[] k = dm7.k(this.E, (int) j, (int) N());
        k5 k5Var = this.w;
        zs zsVar = new zs(s, new wr(k, Math.min(k5Var.m, k5Var.s), this.L), this.w);
        wl wlVar = new wl(this, N2 - j);
        if (!zsVar.t) {
            zsVar.o.add(wlVar);
        }
        zsVar.o(new jm(this));
        zsVar.c(new vm(this));
        zs zsVar2 = zsVar;
        zsVar2.s = w();
        return zsVar2;
    }

    @DexIgnore
    public abstract byte[] M();

    @DexIgnore
    public final long N() {
        return hy1.o(this.E.length);
    }

    @DexIgnore
    public mv O() {
        return new mv(this.D, hs.n, this.w, 0, 8);
    }

    @DexIgnore
    public void P() {
        l(nr.a(this.v, null, zq.SUCCESS, null, null, 13));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0032, code lost:
        if (r2 < 3) goto L_0x0008;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void Q() {
        /*
            r6 = this;
            r0 = 0
            r1 = 0
            boolean r2 = r6.J
            if (r2 == 0) goto L_0x0012
            r6.J = r0
        L_0x0008:
            r0 = 1
        L_0x0009:
            if (r0 == 0) goto L_0x0035
            com.fossil.hs r0 = com.fossil.hs.m
            r2 = 2
            com.fossil.lp.j(r6, r0, r1, r2, r1)
        L_0x0011:
            return
        L_0x0012:
            long r2 = r6.I
            long r4 = r6.H
            long r2 = r2 - r4
            float r2 = (float) r2
            r3 = 1065353216(0x3f800000, float:1.0)
            float r2 = r2 * r3
            long r4 = r6.N()
            float r3 = (float) r4
            float r2 = r2 / r3
            r3 = 1008981770(0x3c23d70a, float:0.01)
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x002b
            r6.K = r0
            goto L_0x0008
        L_0x002b:
            int r2 = r6.K
            int r2 = r2 + 1
            r6.K = r2
            r3 = 3
            if (r2 >= r3) goto L_0x0009
            goto L_0x0008
        L_0x0035:
            com.fossil.nr r0 = r6.v
            com.fossil.zq r2 = com.fossil.zq.DATA_TRANSFER_RETRY_REACH_THRESHOLD
            r5 = 13
            r3 = r1
            r4 = r1
            com.fossil.nr r0 = com.fossil.nr.a(r0, r1, r2, r3, r4, r5)
            r6.l(r0)
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro.Q():void");
    }

    @DexIgnore
    @Override // com.fossil.lp
    public fs b(hs hsVar) {
        int i = lk.f2206a[hsVar.ordinal()];
        if (i == 1) {
            hv hvVar = new hv(this.D, this.w, 0, 4);
            hvVar.o(new xk(this));
            return hvVar;
        } else if (i == 2) {
            if (this.M == 0) {
                this.M = System.currentTimeMillis();
            }
            kv kvVar = new kv(this.I, N() - this.I, N(), this.D, this.w, 0, 32);
            kvVar.o(new kl(this));
            return kvVar;
        } else if (i == 3) {
            mv O2 = O();
            O2.o(new hn(this));
            return O2;
        } else if (i != 4) {
            return null;
        } else {
            return L();
        }
    }
}
