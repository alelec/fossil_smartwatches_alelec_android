package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zq7 implements Serializable {
    @DexIgnore
    public boolean element;

    @DexIgnore
    public String toString() {
        return String.valueOf(this.element);
    }
}
