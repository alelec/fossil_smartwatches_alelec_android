package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cb0 implements Parcelable.Creator<db0> {
    @DexIgnore
    public /* synthetic */ cb0(kq7 kq7) {
    }

    @DexIgnore
    public db0 a(Parcel parcel) {
        return new db0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public db0 createFromParcel(Parcel parcel) {
        return new db0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public db0[] newArray(int i) {
        return new db0[i];
    }
}
