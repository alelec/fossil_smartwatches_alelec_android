package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iw4 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1678a;
    @DexIgnore
    public /* final */ MutableLiveData<ps4> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Object>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Boolean, Boolean>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Integer, ms4>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Boolean, ServerError>> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<gs4>> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<gl7<Boolean, Boolean, Boolean>> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Boolean, ServerError>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<gl7<String, String, Integer>> j; // = new MutableLiveData<>();
    @DexIgnore
    public LiveData<Object> k;
    @DexIgnore
    public String l;
    @DexIgnore
    public /* final */ tt4 m;
    @DexIgnore
    public /* final */ on5 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel", f = "BCOverviewLeaderBoardViewModel.kt", l = {267, 273}, m = "calculateTopNear")
    public static final class a extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ iw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(iw4 iw4, qn7 qn7) {
            super(qn7);
            this.this$0 = iw4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, null, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$calculateTopNear$2", f = "BCOverviewLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ms4 $current;
        @DexIgnore
        public /* final */ /* synthetic */ int $numberOfPlayers;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(iw4 iw4, int i, ms4 ms4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iw4;
            this.$numberOfPlayers = i;
            this.$current = ms4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$numberOfPlayers, this.$current, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.e.l(hl7.a(ao7.e(this.$numberOfPlayers), this.$current));
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$calculateTopNear$3", f = "BCOverviewLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $allPlayers;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(iw4 iw4, List list, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iw4;
            this.$allPlayers = list;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$allPlayers, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.c.l(this.$allPlayers);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel", f = "BCOverviewLeaderBoardViewModel.kt", l = {230}, m = "fetchChallengeWithId")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ iw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(iw4 iw4, qn7 qn7) {
            super(qn7);
            this.this$0 = iw4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.n(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchChallengeWithId$wrapper$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {230}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super kz4<ps4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(iw4 iw4, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iw4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, this.$challengeId, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super kz4<ps4>> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                tt4 tt4 = this.this$0.m;
                String str = this.$challengeId;
                this.L$0 = iv7;
                this.label = 1;
                Object n = tt4.n(tt4, str, null, this, 2, null);
                return n == d ? d : n;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ iw4 f1679a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchFocusedPlayersHistory$1$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {217, 219}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ bt4 $historyChallenge;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, bt4 bt4, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
                this.$historyChallenge = bt4;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$historyChallenge, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i != 0) {
                    if (i == 1) {
                        cl7 cl7 = (cl7) this.L$2;
                        List list = (List) this.L$1;
                    } else if (i != 2) {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    iv7 iv7 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    el7.b(obj);
                    iv7 iv72 = this.p$;
                    bt4 bt4 = this.$historyChallenge;
                    if (bt4 != null) {
                        List<ms4> k = bt4.k();
                        cl7 H = this.this$0.f1679a.H(k, 3, 3);
                        int size = k.size();
                        this.L$0 = iv72;
                        this.L$1 = k;
                        this.L$2 = H;
                        this.label = 1;
                        if (this.this$0.f1679a.m((List) H.getFirst(), (List) H.getSecond(), size, this) == d) {
                            return d;
                        }
                    } else {
                        f fVar = this.this$0;
                        iw4 iw4 = fVar.f1679a;
                        String str = fVar.b;
                        this.L$0 = iv72;
                        this.label = 2;
                        if (iw4.p(str, this) == d) {
                            return d;
                        }
                    }
                }
                this.this$0.f1679a.d.l(hl7.a(ao7.a(false), null));
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public f(iw4 iw4, String str) {
            this.f1679a = iw4;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<Object> apply(bt4 bt4) {
            xw7 unused = gu7.d(us0.a(this.f1679a), bw7.b(), null, new a(this, bt4, null), 2, null);
            return new MutableLiveData<>();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel", f = "BCOverviewLeaderBoardViewModel.kt", l = {242, 257}, m = "fetchFocusedPlayersOnServer")
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ iw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(iw4 iw4, qn7 qn7) {
            super(qn7);
            this.this$0 = iw4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.p(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchFocusedPlayersOnServer$playersWrapper$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {243}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends ks4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(iw4 iw4, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iw4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, this.$challengeId, qn7);
            hVar.p$ = (iv7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends ks4>>> qn7) {
            return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                tt4 tt4 = this.this$0.m;
                ArrayList c = hm7.c(this.$challengeId);
                this.L$0 = iv7;
                this.label = 1;
                Object q = tt4.q(tt4, c, 3, 3, false, this, 8, null);
                return q == d ? d : q;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchPlayersForHistory$2", f = "BCOverviewLeaderBoardViewModel.kt", l = {104, 108, 112}, m = "invokeSuspend")
    public static final class i extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $historyId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iw4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchPlayersForHistory$2$historyChallenge$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super bt4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(i iVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super bt4> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.m.z(this.this$0.$historyId);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(iw4 iw4, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iw4;
            this.$historyId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            i iVar = new i(this.this$0, this.$historyId, qn7);
            iVar.p$ = (iv7) obj;
            return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((i) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x008b  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00aa  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0107  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 268
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.iw4.i.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$init$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {73, 74, 76}, m = "invokeSuspend")
    public static final class j extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ps4 $challenge;
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ String $historyId;
        @DexIgnore
        public /* final */ /* synthetic */ int $numberOfPlayers;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(iw4 iw4, String str, String str2, int i, ps4 ps4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iw4;
            this.$historyId = str;
            this.$challengeId = str2;
            this.$numberOfPlayers = i;
            this.$challenge = ps4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            j jVar = new j(this.this$0, this.$historyId, this.$challengeId, this.$numberOfPlayers, this.$challenge, qn7);
            jVar.p$ = (iv7) obj;
            return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((j) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0053  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                r7 = 3
                r6 = 2
                r5 = 1
                java.lang.Object r1 = com.fossil.yn7.d()
                int r0 = r8.label
                if (r0 == 0) goto L_0x0055
                if (r0 == r5) goto L_0x003e
                if (r0 == r6) goto L_0x0023
                if (r0 != r7) goto L_0x001b
                java.lang.Object r0 = r8.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r9)
            L_0x0018:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x001a:
                return r0
            L_0x001b:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0023:
                java.lang.Object r0 = r8.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r9)
            L_0x002a:
                com.fossil.iw4 r2 = r8.this$0
                com.fossil.ps4 r3 = r8.$challenge
                java.lang.String r3 = r3.f()
                r8.L$0 = r0
                r8.label = r7
                java.lang.Object r0 = r2.n(r3, r8)
                if (r0 != r1) goto L_0x0018
                r0 = r1
                goto L_0x001a
            L_0x003e:
                java.lang.Object r0 = r8.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r9)
            L_0x0045:
                com.fossil.iw4 r2 = r8.this$0
                java.lang.String r3 = r8.$challengeId
                r8.L$0 = r0
                r8.label = r6
                java.lang.Object r2 = r2.p(r3, r8)
                if (r2 != r1) goto L_0x002a
                r0 = r1
                goto L_0x001a
            L_0x0055:
                com.fossil.el7.b(r9)
                com.fossil.iv7 r0 = r8.p$
                java.lang.String r2 = r8.$historyId
                if (r2 != 0) goto L_0x0070
                com.fossil.iw4 r2 = r8.this$0
                java.lang.String r3 = r8.$challengeId
                int r4 = r8.$numberOfPlayers
                r8.L$0 = r0
                r8.label = r5
                java.lang.Object r2 = r2.C(r3, r4, r8)
                if (r2 != r1) goto L_0x0045
                r0 = r1
                goto L_0x001a
            L_0x0070:
                com.fossil.iw4 r0 = r8.this$0
                com.fossil.iw4.a(r0, r2)
                goto L_0x0018
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.iw4.j.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$initDisplayPlayer$2", f = "BCOverviewLeaderBoardViewModel.kt", l = {195}, m = "invokeSuspend")
    public static final class k extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ int $numberOfPlayers;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(iw4 iw4, String str, int i, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iw4;
            this.$challengeId = str;
            this.$numberOfPlayers = i;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            k kVar = new k(this.this$0, this.$challengeId, this.$numberOfPlayers, qn7);
            kVar.p$ = (iv7) obj;
            return kVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((k) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ks4 j = this.this$0.m.j(this.$challengeId);
                if (j != null) {
                    List<ms4> d2 = j.d();
                    List<ms4> b = j.b();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = this.this$0.f1678a;
                    local.e(str, "initDisplayPlayer - top: " + d2 + " - near: " + b);
                    iw4 iw4 = this.this$0;
                    int i2 = this.$numberOfPlayers;
                    this.L$0 = iv7;
                    this.L$1 = j;
                    this.L$2 = d2;
                    this.L$3 = b;
                    this.label = 1;
                    if (iw4.m(d2, b, i2, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                List list = (List) this.L$3;
                List list2 = (List) this.L$2;
                ks4 ks4 = (ks4) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$leaveChallenge$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {126, 137}, m = "invokeSuspend")
    public static final class l extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iw4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$leaveChallenge$1$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Long>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $encryptedDataStr;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str, qn7 qn7) {
                super(2, qn7);
                this.$encryptedDataStr = str;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$encryptedDataStr, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Long> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return ao7.f(PortfolioApp.h0.c().m1(this.$encryptedDataStr));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$leaveChallenge$1$wrapper$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {126}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super kz4<ps4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ps4 $challenge;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ l this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(l lVar, ps4 ps4, qn7 qn7) {
                super(2, qn7);
                this.this$0 = lVar;
                this.$challenge = ps4;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, this.$challenge, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super kz4<ps4>> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    tt4 tt4 = this.this$0.this$0.m;
                    String f = this.$challenge.f();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object E = tt4.E(f, this);
                    return E == d ? d : E;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(iw4 iw4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iw4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            l lVar = new l(this.this$0, qn7);
            lVar.p$ = (iv7) obj;
            return lVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((l) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x006e  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0176  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
            // Method dump skipped, instructions count: 422
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.iw4.l.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$refresh$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {91, 92, 95}, m = "invokeSuspend")
    public static final class m extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(iw4 iw4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iw4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            m mVar = new m(this.this$0, qn7);
            mVar.p$ = (iv7) obj;
            return mVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((m) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0057  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                r11 = this;
                r10 = 0
                r9 = 3
                r8 = 2
                r7 = 1
                java.lang.Object r2 = com.fossil.yn7.d()
                int r0 = r11.label
                if (r0 == 0) goto L_0x0059
                if (r0 == r7) goto L_0x003a
                if (r0 == r8) goto L_0x0012
                if (r0 != r9) goto L_0x0032
            L_0x0012:
                java.lang.Object r0 = r11.L$1
                com.fossil.ps4 r0 = (com.fossil.ps4) r0
                java.lang.Object r0 = r11.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r12)
            L_0x001d:
                com.fossil.iw4 r0 = r11.this$0
                androidx.lifecycle.MutableLiveData r0 = com.fossil.iw4.k(r0)
                r1 = 0
                java.lang.Boolean r1 = com.fossil.ao7.a(r1)
                com.fossil.cl7 r1 = com.fossil.hl7.a(r1, r10)
                r0.l(r1)
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0031:
                return r0
            L_0x0032:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x003a:
                java.lang.Object r0 = r11.L$1
                com.fossil.ps4 r0 = (com.fossil.ps4) r0
                java.lang.Object r1 = r11.L$0
                com.fossil.iv7 r1 = (com.fossil.iv7) r1
                com.fossil.el7.b(r12)
            L_0x0045:
                com.fossil.iw4 r3 = r11.this$0
                java.lang.String r4 = r0.f()
                r11.L$0 = r1
                r11.L$1 = r0
                r11.label = r8
                java.lang.Object r0 = r3.n(r4, r11)
                if (r0 != r2) goto L_0x001d
                r0 = r2
                goto L_0x0031
            L_0x0059:
                com.fossil.el7.b(r12)
                com.fossil.iv7 r1 = r11.p$
                com.fossil.iw4 r0 = r11.this$0
                androidx.lifecycle.MutableLiveData r0 = com.fossil.iw4.f(r0)
                java.lang.Object r0 = r0.e()
                com.fossil.ps4 r0 = (com.fossil.ps4) r0
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                com.fossil.iw4 r4 = r11.this$0
                java.lang.String r4 = com.fossil.iw4.e(r4)
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "refresh - challenge: "
                r5.append(r6)
                r5.append(r0)
                java.lang.String r5 = r5.toString()
                r3.e(r4, r5)
                com.fossil.iw4 r3 = r11.this$0
                java.lang.String r3 = com.fossil.iw4.c(r3)
                if (r3 != 0) goto L_0x00a8
                if (r0 == 0) goto L_0x001d
                com.fossil.iw4 r3 = r11.this$0
                java.lang.String r4 = r0.f()
                r11.L$0 = r1
                r11.L$1 = r0
                r11.label = r7
                java.lang.Object r3 = r3.p(r4, r11)
                if (r3 != r2) goto L_0x0045
                r0 = r2
                goto L_0x0031
            L_0x00a8:
                com.fossil.iw4 r3 = r11.this$0
                java.lang.String r4 = com.fossil.iw4.c(r3)
                if (r4 == 0) goto L_0x00bf
                r11.L$0 = r1
                r11.L$1 = r0
                r11.label = r9
                java.lang.Object r0 = r3.q(r4, r11)
                if (r0 != r2) goto L_0x001d
                r0 = r2
                goto L_0x0031
            L_0x00bf:
                com.fossil.pq7.i()
                throw r10
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.iw4.m.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public iw4(tt4 tt4, on5 on5) {
        pq7.c(tt4, "challengeRepository");
        pq7.c(on5, "shared");
        this.m = tt4;
        this.n = on5;
        String simpleName = iw4.class.getSimpleName();
        pq7.b(simpleName, "BCOverviewLeaderBoardVie\u2026el::class.java.simpleName");
        this.f1678a = simpleName;
    }

    @DexIgnore
    public final LiveData<gl7<Boolean, Boolean, Boolean>> A() {
        return this.h;
    }

    @DexIgnore
    public final void B(ps4 ps4, String str) {
        this.l = str;
        if (ps4 != null) {
            Integer h2 = ps4.h();
            int intValue = h2 != null ? h2.intValue() : 0;
            String f2 = ps4.f();
            this.b.l(ps4);
            xw7 unused = gu7.d(us0.a(this), null, null, new j(this, str, f2, intValue, ps4, null), 3, null);
        }
    }

    @DexIgnore
    public final /* synthetic */ Object C(String str, int i2, qn7<? super tl7> qn7) {
        Object g2 = eu7.g(bw7.b(), new k(this, str, i2, null), qn7);
        return g2 == yn7.d() ? g2 : tl7.f3441a;
    }

    @DexIgnore
    public final void D() {
        FLogger.INSTANCE.getLocal().e(this.f1678a, "leave challenge");
        xw7 unused = gu7.d(us0.a(this), null, null, new l(this, null), 3, null);
    }

    @DexIgnore
    public final void E() {
        String n2;
        ps4 e2 = this.b.e();
        if (e2 != null && (n2 = e2.n()) != null) {
            int hashCode = n2.hashCode();
            if (hashCode != -1402931637) {
                if (hashCode == 1550783935 && n2.equals("running")) {
                    if (this.l == null) {
                        long b2 = xy4.f4212a.b();
                        Date e3 = e2.e();
                        if (b2 < (e3 != null ? e3.getTime() : 0)) {
                            this.g.l(qy4.f3046a.d());
                            return;
                        }
                    }
                    this.g.l(qy4.f3046a.c());
                }
            } else if (n2.equals("completed")) {
                this.g.l(qy4.f3046a.c());
            }
        }
    }

    @DexIgnore
    public final void F(vy4 vy4) {
        pq7.c(vy4, "option");
        int i2 = hw4.f1546a[vy4.ordinal()];
        if (i2 == 1) {
            MutableLiveData<gl7<Boolean, Boolean, Boolean>> mutableLiveData = this.h;
            Boolean bool = Boolean.TRUE;
            Boolean bool2 = Boolean.FALSE;
            mutableLiveData.l(new gl7<>(bool, bool2, bool2));
        } else if (i2 == 2) {
            this.h.l(new gl7<>(Boolean.FALSE, Boolean.TRUE, Boolean.FALSE));
        } else if (i2 == 3) {
            MutableLiveData<gl7<Boolean, Boolean, Boolean>> mutableLiveData2 = this.h;
            Boolean bool3 = Boolean.FALSE;
            mutableLiveData2.l(new gl7<>(bool3, bool3, Boolean.TRUE));
        }
    }

    @DexIgnore
    public final void G() {
        xw7 unused = gu7.d(us0.a(this), null, null, new m(this, null), 3, null);
    }

    @DexIgnore
    public final cl7<List<ms4>, List<ms4>> H(List<ms4> list, int i2, int i3) {
        FLogger.INSTANCE.getLocal().e(this.f1678a, "topAndNearFromPlayers - players: " + list);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        try {
            int size = list.size();
            int i4 = 0;
            T t = null;
            for (T t2 : list) {
                if (i4 >= 0) {
                    T t3 = t2;
                    if (i4 < i2) {
                        arrayList.add(t3);
                        if (pq7.a(t3.d(), PortfolioApp.h0.c().l0())) {
                            i4++;
                            t = t3;
                        }
                    }
                    t3 = t;
                    i4++;
                    t = t3;
                } else {
                    hm7.l();
                    throw null;
                }
            }
            if (t != null) {
                for (int i5 = i2; i5 < i3 + i2; i5++) {
                    if (i5 < size) {
                        arrayList2.add(list.get(i5));
                    }
                }
            } else if (size > i2) {
                for (int i6 = i2; i6 < size; i6++) {
                    if (pq7.a(list.get(i6).d(), PortfolioApp.h0.c().l0())) {
                        if (i6 == size - 1) {
                            int i7 = i6 - i3;
                            while (true) {
                                i7++;
                                if (i7 >= size) {
                                    break;
                                } else if (i7 > i2 - 1) {
                                    arrayList2.add(list.get(i7));
                                }
                            }
                        } else if (i6 == i2) {
                            for (int i8 = i6; i8 < i6 + i3; i8++) {
                                if (i8 < size) {
                                    arrayList2.add(list.get(i8));
                                }
                            }
                        } else {
                            for (int i9 = i6 - (i3 / 2); i9 < (i6 + i3) - 1; i9++) {
                                arrayList2.add(list.get(i9));
                            }
                        }
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return hl7.a(arrayList, arrayList2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00bf, code lost:
        if (r0 != null) goto L_0x00c1;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x014d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object m(java.util.List<com.fossil.ms4> r12, java.util.List<com.fossil.ms4> r13, int r14, com.fossil.qn7<? super com.fossil.tl7> r15) {
        /*
        // Method dump skipped, instructions count: 336
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.iw4.m(java.util.List, java.util.List, int, com.fossil.qn7):java.lang.Object");
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v11, resolved type: androidx.lifecycle.MutableLiveData<com.fossil.ps4> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0090, code lost:
        if (r1 == r0) goto L_0x0069;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object n(java.lang.String r7, com.fossil.qn7<? super com.fossil.ps4> r8) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.fossil.iw4.d
            if (r0 == 0) goto L_0x006a
            r0 = r8
            com.fossil.iw4$d r0 = (com.fossil.iw4.d) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x006a
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0079
            if (r3 != r5) goto L_0x0071
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.iw4 r0 = (com.fossil.iw4) r0
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.fossil.kz4 r0 = (com.fossil.kz4) r0
            java.lang.Object r0 = r0.c()
            com.fossil.ps4 r0 = (com.fossil.ps4) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r1.getLocal()
            java.lang.String r3 = r6.f1678a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r1 = "fetchChallengeWithId - challenge: "
            r4.append(r1)
            r4.append(r0)
            java.lang.String r1 = " - _challengeLive: "
            r4.append(r1)
            androidx.lifecycle.MutableLiveData<com.fossil.ps4> r1 = r6.b
            java.lang.Object r1 = r1.e()
            com.fossil.ps4 r1 = (com.fossil.ps4) r1
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            r2.e(r3, r1)
            if (r0 == 0) goto L_0x0069
            androidx.lifecycle.MutableLiveData<com.fossil.ps4> r1 = r6.b
            r1.l(r0)
        L_0x0069:
            return r0
        L_0x006a:
            com.fossil.iw4$d r0 = new com.fossil.iw4$d
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0014
        L_0x0071:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0079:
            com.fossil.el7.b(r2)
            com.fossil.dv7 r2 = com.fossil.bw7.b()
            com.fossil.iw4$e r3 = new com.fossil.iw4$e
            r4 = 0
            r3.<init>(r6, r7, r4)
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r5
            java.lang.Object r1 = com.fossil.eu7.g(r2, r3, r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.iw4.n(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void o(String str) {
        this.k = ss0.c(this.m.y(str), new f(this, str));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object p(java.lang.String r11, com.fossil.qn7<? super com.fossil.tl7> r12) {
        /*
        // Method dump skipped, instructions count: 247
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.iw4.p(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object q(String str, qn7<? super tl7> qn7) {
        Object g2 = eu7.g(bw7.b(), new i(this, str, null), qn7);
        return g2 == yn7.d() ? g2 : tl7.f3441a;
    }

    @DexIgnore
    public final LiveData<ps4> r() {
        return cz4.a(this.b);
    }

    @DexIgnore
    public final LiveData<cl7<Integer, ms4>> s() {
        return cz4.a(this.e);
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, ServerError>> t() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<List<Object>> u() {
        return cz4.a(this.c);
    }

    @DexIgnore
    public final LiveData<Object> v() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<gl7<String, String, Integer>> w() {
        return this.j;
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, ServerError>> x() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, Boolean>> y() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<List<gs4>> z() {
        return this.g;
    }
}
