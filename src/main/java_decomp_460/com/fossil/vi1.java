package com.fossil;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vi1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ AtomicReference<hk1> f3771a; // = new AtomicReference<>();
    @DexIgnore
    public /* final */ zi0<hk1, List<Class<?>>> b; // = new zi0<>();

    @DexIgnore
    public List<Class<?>> a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        hk1 hk1;
        List<Class<?>> list;
        hk1 andSet = this.f3771a.getAndSet(null);
        if (andSet == null) {
            hk1 = new hk1(cls, cls2, cls3);
        } else {
            andSet.a(cls, cls2, cls3);
            hk1 = andSet;
        }
        synchronized (this.b) {
            list = this.b.get(hk1);
        }
        this.f3771a.set(hk1);
        return list;
    }

    @DexIgnore
    public void b(Class<?> cls, Class<?> cls2, Class<?> cls3, List<Class<?>> list) {
        synchronized (this.b) {
            this.b.put(new hk1(cls, cls2, cls3), list);
        }
    }
}
