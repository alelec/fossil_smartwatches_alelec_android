package com.fossil;

import android.graphics.Bitmap;
import androidx.lifecycle.MutableLiveData;
import com.fossil.imagefilters.FilterType;
import com.portfolio.platform.data.RingStyleRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s96 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Bitmap f3222a;

    /*
    static {
        System.loadLibrary("EInkImageFilter");
    }
    */

    @DexIgnore
    public s96(k97 k97, RingStyleRepository ringStyleRepository) {
        pq7.c(k97, "wfBackgroundPhotoRepository");
        pq7.c(ringStyleRepository, "ringStyleRepository");
        new MutableLiveData();
        new MutableLiveData();
        FilterType filterType = FilterType.ORDERED_DITHERING;
    }

    @DexIgnore
    @Override // com.fossil.ts0
    public void onCleared() {
        jv7.d(us0.a(this), null, 1, null);
        Bitmap bitmap = this.f3222a;
        if (bitmap != null) {
            bitmap.recycle();
        }
        super.onCleared();
    }
}
