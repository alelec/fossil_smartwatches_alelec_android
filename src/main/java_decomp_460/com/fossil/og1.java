package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class og1 implements id1<BitmapDrawable>, ed1 {
    @DexIgnore
    public /* final */ Resources b;
    @DexIgnore
    public /* final */ id1<Bitmap> c;

    @DexIgnore
    public og1(Resources resources, id1<Bitmap> id1) {
        ik1.d(resources);
        this.b = resources;
        ik1.d(id1);
        this.c = id1;
    }

    @DexIgnore
    public static id1<BitmapDrawable> f(Resources resources, id1<Bitmap> id1) {
        if (id1 == null) {
            return null;
        }
        return new og1(resources, id1);
    }

    @DexIgnore
    @Override // com.fossil.ed1
    public void a() {
        id1<Bitmap> id1 = this.c;
        if (id1 instanceof ed1) {
            ((ed1) id1).a();
        }
    }

    @DexIgnore
    @Override // com.fossil.id1
    public void b() {
        this.c.b();
    }

    @DexIgnore
    @Override // com.fossil.id1
    public int c() {
        return this.c.c();
    }

    @DexIgnore
    @Override // com.fossil.id1
    public Class<BitmapDrawable> d() {
        return BitmapDrawable.class;
    }

    @DexIgnore
    /* renamed from: e */
    public BitmapDrawable get() {
        return new BitmapDrawable(this.b, this.c.get());
    }
}
