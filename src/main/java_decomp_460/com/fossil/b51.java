package com.fossil;

import android.content.Context;
import com.fossil.a18;
import com.fossil.y41;
import okhttp3.OkHttpClient;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b51 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public a18.a f395a;
    @DexIgnore
    public y41 b;
    @DexIgnore
    public double c;
    @DexIgnore
    public double d; // = y81.f4257a.f();
    @DexIgnore
    public z41 e; // = new z41(null, null, null, false, false, null, null, null, 255, null);
    @DexIgnore
    public /* final */ Context f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements gp7<OkHttpClient> {
        @DexIgnore
        public /* final */ /* synthetic */ b51 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(b51 b51) {
            super(0);
            this.this$0 = b51;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final OkHttpClient invoke() {
            OkHttpClient.b bVar = new OkHttpClient.b();
            bVar.e(r81.b(this.this$0.f));
            OkHttpClient d = bVar.d();
            pq7.b(d, "OkHttpClient.Builder()\n \u2026xt))\n            .build()");
            return d;
        }
    }

    @DexIgnore
    public b51(Context context) {
        pq7.c(context, "context");
        this.f = context;
        this.c = y81.f4257a.d(context);
    }

    @DexIgnore
    public final a51 b() {
        long b2 = y81.f4257a.b(this.f, this.c);
        long j = (long) (this.d * ((double) b2));
        g51 a2 = g51.f1262a.a(j);
        s61 s61 = new s61(a2);
        c71 a3 = c71.f572a.a(s61, (int) (b2 - j));
        Context context = this.f;
        z41 z41 = this.e;
        a18.a aVar = this.f395a;
        if (aVar == null) {
            aVar = c();
        }
        y41 y41 = this.b;
        if (y41 == null) {
            y41.b bVar = y41.e;
            y41 = new y41.a().d();
        }
        return new c51(context, z41, a2, s61, a3, aVar, y41);
    }

    @DexIgnore
    public final a18.a c() {
        return w81.p(new a(this));
    }
}
