package com.fossil;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h33 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Class<?> f1419a; // = F();
    @DexIgnore
    public static /* final */ x33<?, ?> b; // = g(false);
    @DexIgnore
    public static /* final */ x33<?, ?> c; // = g(true);
    @DexIgnore
    public static /* final */ x33<?, ?> d; // = new z33();

    @DexIgnore
    public static int A(List<Long> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (list instanceof z13) {
            z13 z13 = (z13) list;
            int i = 0;
            for (int i2 = 0; i2 < size; i2++) {
                i += l03.o0(z13.zzb(i2));
            }
            return i;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += l03.o0(list.get(i4).longValue());
        }
        return i3;
    }

    @DexIgnore
    public static x33<?, ?> B() {
        return d;
    }

    @DexIgnore
    public static void C(int i, List<Long> list, r43 r43, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zzc(i, list, z);
        }
    }

    @DexIgnore
    public static int D(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (size * l03.h0(i)) + E(list);
    }

    @DexIgnore
    public static int E(List<Integer> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (list instanceof f13) {
            f13 f13 = (f13) list;
            int i = 0;
            for (int i2 = 0; i2 < size; i2++) {
                i += l03.C0(f13.b(i2));
            }
            return i;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += l03.C0(list.get(i4).intValue());
        }
        return i3;
    }

    @DexIgnore
    public static Class<?> F() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static void G(int i, List<Long> list, r43 r43, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zzd(i, list, z);
        }
    }

    @DexIgnore
    public static int H(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (size * l03.h0(i)) + I(list);
    }

    @DexIgnore
    public static int I(List<Integer> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (list instanceof f13) {
            f13 f13 = (f13) list;
            int i = 0;
            for (int i2 = 0; i2 < size; i2++) {
                i += l03.l0(f13.b(i2));
            }
            return i;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += l03.l0(list.get(i4).intValue());
        }
        return i3;
    }

    @DexIgnore
    public static Class<?> J() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static void K(int i, List<Long> list, r43 r43, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zzn(i, list, z);
        }
    }

    @DexIgnore
    public static int L(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (size * l03.h0(i)) + M(list);
    }

    @DexIgnore
    public static int M(List<Integer> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (list instanceof f13) {
            f13 f13 = (f13) list;
            int i = 0;
            for (int i2 = 0; i2 < size; i2++) {
                i += l03.p0(f13.b(i2));
            }
            return i;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += l03.p0(list.get(i4).intValue());
        }
        return i3;
    }

    @DexIgnore
    public static void N(int i, List<Long> list, r43 r43, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zze(i, list, z);
        }
    }

    @DexIgnore
    public static int O(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (size * l03.h0(i)) + P(list);
    }

    @DexIgnore
    public static int P(List<Integer> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (list instanceof f13) {
            f13 f13 = (f13) list;
            int i = 0;
            for (int i2 = 0; i2 < size; i2++) {
                i += l03.t0(f13.b(i2));
            }
            return i;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += l03.t0(list.get(i4).intValue());
        }
        return i3;
    }

    @DexIgnore
    public static void Q(int i, List<Long> list, r43 r43, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zzl(i, list, z);
        }
    }

    @DexIgnore
    public static int R(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return l03.y0(i, 0) * size;
    }

    @DexIgnore
    public static int S(List<?> list) {
        return list.size() << 2;
    }

    @DexIgnore
    public static void T(int i, List<Integer> list, r43 r43, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zza(i, list, z);
        }
    }

    @DexIgnore
    public static int U(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * l03.r0(i, 0);
    }

    @DexIgnore
    public static int V(List<?> list) {
        return list.size() << 3;
    }

    @DexIgnore
    public static void W(int i, List<Integer> list, r43 r43, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zzj(i, list, z);
        }
    }

    @DexIgnore
    public static int X(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * l03.H(i, true);
    }

    @DexIgnore
    public static int Y(List<?> list) {
        return list.size();
    }

    @DexIgnore
    public static void Z(int i, List<Integer> list, r43 r43, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zzm(i, list, z);
        }
    }

    @DexIgnore
    public static int a(int i, Object obj, f33 f33) {
        return obj instanceof u13 ? l03.c(i, (u13) obj) : l03.F(i, (m23) obj, f33);
    }

    @DexIgnore
    public static void a0(int i, List<Integer> list, r43 r43, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zzb(i, list, z);
        }
    }

    @DexIgnore
    public static int b(int i, List<?> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int h0 = l03.h0(i) * size;
        if (list instanceof w13) {
            w13 w13 = (w13) list;
            for (int i2 = 0; i2 < size; i2++) {
                Object zzb = w13.zzb(i2);
                h0 += zzb instanceof xz2 ? l03.I((xz2) zzb) : l03.K((String) zzb);
            }
        } else {
            for (int i3 = 0; i3 < size; i3++) {
                Object obj = list.get(i3);
                h0 += obj instanceof xz2 ? l03.I((xz2) obj) : l03.K((String) obj);
            }
        }
        return h0;
    }

    @DexIgnore
    public static void b0(int i, List<Integer> list, r43 r43, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zzk(i, list, z);
        }
    }

    @DexIgnore
    public static int c(int i, List<?> list, f33 f33) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int h0 = l03.h0(i) * size;
        for (int i2 = 0; i2 < size; i2++) {
            Object obj = list.get(i2);
            h0 += obj instanceof u13 ? l03.d((u13) obj) : l03.e((m23) obj, f33);
        }
        return h0;
    }

    @DexIgnore
    public static void c0(int i, List<Integer> list, r43 r43, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zzh(i, list, z);
        }
    }

    @DexIgnore
    public static int d(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return e(list) + (list.size() * l03.h0(i));
    }

    @DexIgnore
    public static void d0(int i, List<Boolean> list, r43 r43, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zzi(i, list, z);
        }
    }

    @DexIgnore
    public static int e(List<Long> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (list instanceof z13) {
            z13 z13 = (z13) list;
            int i = 0;
            for (int i2 = 0; i2 < size; i2++) {
                i += l03.e0(z13.zzb(i2));
            }
            return i;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += l03.e0(list.get(i4).longValue());
        }
        return i3;
    }

    @DexIgnore
    public static x33<?, ?> f() {
        return b;
    }

    @DexIgnore
    public static x33<?, ?> g(boolean z) {
        try {
            Class<?> J = J();
            if (J == null) {
                return null;
            }
            return (x33) J.getConstructor(Boolean.TYPE).newInstance(Boolean.valueOf(z));
        } catch (Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static <UT, UB> UB h(int i, int i2, UB ub, x33<UT, UB> x33) {
        if (ub == null) {
            ub = x33.a();
        }
        x33.b(ub, i, (long) i2);
        return ub;
    }

    @DexIgnore
    public static <UT, UB> UB i(int i, List<Integer> list, i13 i13, UB ub, x33<UT, UB> x33) {
        UB ub2;
        int i2;
        if (i13 == null) {
            return ub;
        }
        if (list instanceof RandomAccess) {
            int size = list.size();
            int i3 = 0;
            int i4 = 0;
            ub2 = ub;
            while (i4 < size) {
                int intValue = list.get(i4).intValue();
                if (i13.zza(intValue)) {
                    if (i4 != i3) {
                        list.set(i3, Integer.valueOf(intValue));
                    }
                    i2 = i3 + 1;
                } else {
                    ub2 = (UB) h(i, intValue, ub2, x33);
                    i2 = i3;
                }
                i4++;
                i3 = i2;
            }
            if (i3 != size) {
                list.subList(i3, size).clear();
            }
        } else {
            Iterator<Integer> it = list.iterator();
            while (it.hasNext()) {
                int intValue2 = it.next().intValue();
                if (!i13.zza(intValue2)) {
                    ub = (UB) h(i, intValue2, ub, x33);
                    it.remove();
                }
            }
            ub2 = ub;
        }
        return ub2;
    }

    @DexIgnore
    public static void j(int i, List<String> list, r43 r43) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zza(i, list);
        }
    }

    @DexIgnore
    public static void k(int i, List<?> list, r43 r43, f33 f33) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.e(i, list, f33);
        }
    }

    @DexIgnore
    public static void l(int i, List<Double> list, r43 r43, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zzg(i, list, z);
        }
    }

    @DexIgnore
    public static <T, FT extends v03<FT>> void m(s03<FT> s03, T t, T t2) {
        t03<FT> b2 = s03.b(t2);
        if (!b2.f3344a.isEmpty()) {
            s03.f(t).g(b2);
        }
    }

    @DexIgnore
    public static <T> void n(j23 j23, T t, T t2, long j) {
        e43.j(t, j, j23.zza(e43.F(t, j), e43.F(t2, j)));
    }

    @DexIgnore
    public static <T, UT, UB> void o(x33<UT, UB> x33, T t, T t2) {
        x33.e(t, x33.i(x33.f(t), x33.f(t2)));
    }

    @DexIgnore
    public static void p(Class<?> cls) {
        Class<?> cls2;
        if (!e13.class.isAssignableFrom(cls) && (cls2 = f1419a) != null && !cls2.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    @DexIgnore
    public static boolean q(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    @DexIgnore
    public static int r(int i, List<xz2> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int h0 = size * l03.h0(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            h0 = l03.I(list.get(i2)) + h0;
        }
        return h0;
    }

    @DexIgnore
    public static int s(int i, List<m23> list, f33 f33) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += l03.V(i, list.get(i3), f33);
        }
        return i2;
    }

    @DexIgnore
    public static int t(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (size * l03.h0(i)) + u(list);
    }

    @DexIgnore
    public static int u(List<Long> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (list instanceof z13) {
            z13 z13 = (z13) list;
            int i = 0;
            for (int i2 = 0; i2 < size; i2++) {
                i += l03.j0(z13.zzb(i2));
            }
            return i;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += l03.j0(list.get(i4).longValue());
        }
        return i3;
    }

    @DexIgnore
    public static x33<?, ?> v() {
        return c;
    }

    @DexIgnore
    public static void w(int i, List<xz2> list, r43 r43) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zzb(i, list);
        }
    }

    @DexIgnore
    public static void x(int i, List<?> list, r43 r43, f33 f33) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.a(i, list, f33);
        }
    }

    @DexIgnore
    public static void y(int i, List<Float> list, r43 r43, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            r43.zzf(i, list, z);
        }
    }

    @DexIgnore
    public static int z(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (size * l03.h0(i)) + A(list);
    }
}
