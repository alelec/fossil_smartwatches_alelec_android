package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import java.io.File;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p80 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f2798a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public int c;
    @DexIgnore
    public long d;
    @DexIgnore
    public /* final */ Handler e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public p80(String str, int i, long j, Handler handler, String str2, String str3) {
        int i2 = 0;
        this.b = str;
        this.c = i;
        this.d = j;
        this.e = handler;
        this.f = str2;
        this.g = str3;
        Context a2 = id0.i.a();
        SharedPreferences sharedPreferences = a2 != null ? a2.getSharedPreferences(str, 0) : null;
        this.f2798a = sharedPreferences != null ? sharedPreferences.getInt("file_count", 0) : i2;
    }

    @DexIgnore
    public final String a(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.b);
        sb.append(File.separatorChar);
        hr7 hr7 = hr7.f1520a;
        String format = String.format("%s_%010d.%s", Arrays.copyOf(new Object[]{this.f, Integer.valueOf(i), this.g}, 3));
        pq7.b(format, "java.lang.String.format(format, *args)");
        sb.append(format);
        return sb.toString();
    }

    @DexIgnore
    public final void b() {
        List a0 = em7.a0(e(), new o80());
        long j = 0;
        int i = 0;
        while (i < a0.size() && j < this.d) {
            File file = (File) pm7.I(a0, i);
            i++;
            j = (file != null ? file.length() : 0) + j;
        }
        int size = a0.size();
        while (i < size) {
            File file2 = (File) pm7.I(a0, i);
            if (file2 != null) {
                file2.delete();
            }
            i++;
        }
    }

    @DexIgnore
    public final boolean c(String str) {
        pq7.b(str.getBytes(et7.f986a), "(this as java.lang.String).getBytes(charset)");
        File d2 = d(str.length());
        if (d2 != null) {
            return this.e.post(new n80(d2, this, str));
        }
        return false;
    }

    @DexIgnore
    public final File d(int i) {
        File g2 = g();
        if (g2 != null) {
            if (!g2.exists()) {
                g2.mkdirs();
            }
            File f2 = f();
            if (f2 != null) {
                if (f2.length() <= 0 || f2.length() + ((long) i) < ((long) this.c)) {
                    return f2;
                }
                i();
                b();
                return d(i);
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005f, code lost:
        if (com.fossil.pq7.a(com.fossil.cp7.f(r7), r12.g) != false) goto L_0x0061;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.io.File[] e() {
        /*
            r12 = this;
            r2 = 0
            java.io.File r0 = r12.g()
            if (r0 == 0) goto L_0x000d
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x001b
        L_0x000d:
            com.fossil.m80 r0 = com.fossil.m80.c
            java.lang.String r1 = "LogIO"
            java.lang.String r3 = "getCompletedLogFiles: Log Folder not exist."
            java.lang.Object[] r4 = new java.lang.Object[r2]
            r0.a(r1, r3, r4)
            java.io.File[] r0 = new java.io.File[r2]
        L_0x001a:
            return r0
        L_0x001b:
            r12.i()
            java.io.File[] r0 = r0.listFiles()
            if (r0 == 0) goto L_0x006a
        L_0x0024:
            java.io.File r4 = r12.f()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            int r6 = r0.length
            r3 = r2
        L_0x002f:
            if (r3 >= r6) goto L_0x006f
            r7 = r0[r3]
            boolean r8 = com.fossil.pq7.a(r7, r4)
            r1 = 1
            r8 = r8 ^ 1
            if (r8 == 0) goto L_0x006d
            boolean r8 = r7.isFile()
            if (r8 == 0) goto L_0x006d
            java.lang.String r8 = r7.getName()
            java.lang.String r9 = "it.name"
            com.fossil.pq7.b(r8, r9)
            java.lang.String r9 = r12.f
            r10 = 2
            r11 = 0
            boolean r8 = com.fossil.vt7.s(r8, r9, r2, r10, r11)
            if (r8 == 0) goto L_0x006d
            java.lang.String r8 = com.fossil.cp7.f(r7)
            java.lang.String r9 = r12.g
            boolean r8 = com.fossil.pq7.a(r8, r9)
            if (r8 == 0) goto L_0x006d
        L_0x0061:
            if (r1 == 0) goto L_0x0066
            r5.add(r7)
        L_0x0066:
            int r1 = r3 + 1
            r3 = r1
            goto L_0x002f
        L_0x006a:
            java.io.File[] r0 = new java.io.File[r2]
            goto L_0x0024
        L_0x006d:
            r1 = r2
            goto L_0x0061
        L_0x006f:
            java.io.File[] r0 = new java.io.File[r2]
            java.lang.Object[] r0 = r5.toArray(r0)
            if (r0 == 0) goto L_0x007a
            java.io.File[] r0 = (java.io.File[]) r0
            goto L_0x001a
        L_0x007a:
            com.fossil.il7 r0 = new com.fossil.il7
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Array<T>"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.p80.e():java.io.File[]");
    }

    @DexIgnore
    public final File f() {
        File a2;
        synchronized (Integer.valueOf(this.f2798a)) {
            String a3 = a(this.f2798a);
            Context a4 = id0.i.a();
            a2 = a4 != null ? ny1.f2594a.a(a4, a3) : null;
        }
        return a2;
    }

    @DexIgnore
    public final File g() {
        String str = this.b;
        Context a2 = id0.i.a();
        if (a2 != null) {
            return ny1.f2594a.a(a2, str);
        }
        return null;
    }

    @DexIgnore
    public final long h() {
        long j = 0;
        try {
            for (File file : e()) {
                j += file.length();
            }
        } catch (Exception e2) {
        }
        return j;
    }

    @DexIgnore
    public final void i() {
        SharedPreferences.Editor edit;
        SharedPreferences.Editor putInt;
        synchronized (Integer.valueOf(this.f2798a)) {
            long j = (long) Integer.MAX_VALUE;
            this.f2798a = (int) (((((long) this.f2798a) + 1) + j) % j);
            String str = this.b;
            Context a2 = id0.i.a();
            SharedPreferences sharedPreferences = a2 != null ? a2.getSharedPreferences(str, 0) : null;
            if (!(sharedPreferences == null || (edit = sharedPreferences.edit()) == null || (putInt = edit.putInt("file_count", this.f2798a)) == null)) {
                putInt.apply();
                tl7 tl7 = tl7.f3441a;
            }
        }
    }
}
