package com.fossil;

import android.content.Context;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.view.ActionMode;
import androidx.collection.SimpleArrayMap;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sf0 extends ActionMode {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3249a;
    @DexIgnore
    public /* final */ androidx.appcompat.view.ActionMode b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ActionMode.Callback {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ActionMode.Callback f3250a;
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public /* final */ ArrayList<sf0> c; // = new ArrayList<>();
        @DexIgnore
        public /* final */ SimpleArrayMap<Menu, Menu> d; // = new SimpleArrayMap<>();

        @DexIgnore
        public a(Context context, ActionMode.Callback callback) {
            this.b = context;
            this.f3250a = callback;
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public void a(androidx.appcompat.view.ActionMode actionMode) {
            this.f3250a.onDestroyActionMode(e(actionMode));
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean b(androidx.appcompat.view.ActionMode actionMode, Menu menu) {
            return this.f3250a.onCreateActionMode(e(actionMode), f(menu));
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean c(androidx.appcompat.view.ActionMode actionMode, Menu menu) {
            return this.f3250a.onPrepareActionMode(e(actionMode), f(menu));
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean d(androidx.appcompat.view.ActionMode actionMode, MenuItem menuItem) {
            return this.f3250a.onActionItemClicked(e(actionMode), new fg0(this.b, (hm0) menuItem));
        }

        @DexIgnore
        public android.view.ActionMode e(androidx.appcompat.view.ActionMode actionMode) {
            int size = this.c.size();
            for (int i = 0; i < size; i++) {
                sf0 sf0 = this.c.get(i);
                if (sf0 != null && sf0.b == actionMode) {
                    return sf0;
                }
            }
            sf0 sf02 = new sf0(this.b, actionMode);
            this.c.add(sf02);
            return sf02;
        }

        @DexIgnore
        public final Menu f(Menu menu) {
            Menu menu2 = this.d.get(menu);
            if (menu2 != null) {
                return menu2;
            }
            kg0 kg0 = new kg0(this.b, (gm0) menu);
            this.d.put(menu, kg0);
            return kg0;
        }
    }

    @DexIgnore
    public sf0(Context context, androidx.appcompat.view.ActionMode actionMode) {
        this.f3249a = context;
        this.b = actionMode;
    }

    @DexIgnore
    public void finish() {
        this.b.c();
    }

    @DexIgnore
    public View getCustomView() {
        return this.b.d();
    }

    @DexIgnore
    public Menu getMenu() {
        return new kg0(this.f3249a, (gm0) this.b.e());
    }

    @DexIgnore
    public MenuInflater getMenuInflater() {
        return this.b.f();
    }

    @DexIgnore
    public CharSequence getSubtitle() {
        return this.b.g();
    }

    @DexIgnore
    public Object getTag() {
        return this.b.h();
    }

    @DexIgnore
    public CharSequence getTitle() {
        return this.b.i();
    }

    @DexIgnore
    public boolean getTitleOptionalHint() {
        return this.b.j();
    }

    @DexIgnore
    public void invalidate() {
        this.b.k();
    }

    @DexIgnore
    public boolean isTitleOptional() {
        return this.b.l();
    }

    @DexIgnore
    public void setCustomView(View view) {
        this.b.m(view);
    }

    @DexIgnore
    @Override // android.view.ActionMode
    public void setSubtitle(int i) {
        this.b.n(i);
    }

    @DexIgnore
    @Override // android.view.ActionMode
    public void setSubtitle(CharSequence charSequence) {
        this.b.o(charSequence);
    }

    @DexIgnore
    public void setTag(Object obj) {
        this.b.p(obj);
    }

    @DexIgnore
    @Override // android.view.ActionMode
    public void setTitle(int i) {
        this.b.q(i);
    }

    @DexIgnore
    @Override // android.view.ActionMode
    public void setTitle(CharSequence charSequence) {
        this.b.r(charSequence);
    }

    @DexIgnore
    public void setTitleOptionalHint(boolean z) {
        this.b.s(z);
    }
}
