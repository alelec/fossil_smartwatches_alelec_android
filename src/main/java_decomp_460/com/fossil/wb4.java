package com.fossil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wb4 extends FileOutputStream {
    @DexIgnore
    public static /* final */ FilenameFilter e; // = new a();
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public File c;
    @DexIgnore
    public boolean d; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return str.endsWith(".cls_temp");
        }
    }

    @DexIgnore
    public wb4(File file, String str) throws FileNotFoundException {
        super(new File(file, str + ".cls_temp"));
        this.b = file + File.separator + str;
        StringBuilder sb = new StringBuilder();
        sb.append(this.b);
        sb.append(".cls_temp");
        this.c = new File(sb.toString());
    }

    @DexIgnore
    public void a() throws IOException {
        if (!this.d) {
            this.d = true;
            super.flush();
            super.close();
        }
    }

    @DexIgnore
    @Override // java.io.OutputStream, java.io.Closeable, java.io.FileOutputStream, java.lang.AutoCloseable
    public void close() throws IOException {
        synchronized (this) {
            if (!this.d) {
                this.d = true;
                super.flush();
                super.close();
                File file = new File(this.b + ".cls");
                if (this.c.renameTo(file)) {
                    this.c = null;
                    return;
                }
                String str = "";
                if (file.exists()) {
                    str = " (target already exists)";
                } else if (!this.c.exists()) {
                    str = " (source does not exist)";
                }
                throw new IOException("Could not rename temp file: " + this.c + " -> " + file + str);
            }
        }
    }
}
