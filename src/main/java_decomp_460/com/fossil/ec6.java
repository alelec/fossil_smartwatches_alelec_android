package com.fossil;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.ib6;
import com.fossil.iq4;
import com.fossil.jn5;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ec6 extends yb6 {
    @DexIgnore
    public sb6 e;
    @DexIgnore
    public MutableLiveData<HybridPreset> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<m66> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Gson h; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<m66> i;
    @DexIgnore
    public /* final */ zb6 j;
    @DexIgnore
    public /* final */ ib6 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $currentPreset$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $it;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $shouldShowSkippedPermissions$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ec6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ec6$a$a")
        /* renamed from: com.fossil.ec6$a$a  reason: collision with other inner class name */
        public static final class C0063a extends ko7 implements vp7<iv7, qn7<? super Parcelable>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ HybridPresetAppSetting $buttonMapping;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0063a(a aVar, HybridPresetAppSetting hybridPresetAppSetting, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
                this.$buttonMapping = hybridPresetAppSetting;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0063a aVar = new C0063a(this.this$0, this.$buttonMapping, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Parcelable> qn7) {
                return ((C0063a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return ec6.x(this.this$0.this$0).q(this.$buttonMapping.getAppId());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements iq4.e<ib6.c, ib6.a> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ a f913a;

            @DexIgnore
            public b(a aVar) {
                this.f913a = aVar;
            }

            @DexIgnore
            /* renamed from: b */
            public void a(ib6.a aVar) {
                pq7.c(aVar, "errorValue");
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setToWatch onError");
                this.f913a.this$0.j.w();
                int b = aVar.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HybridCustomizeEditPresenter", "setPresetToWatch() - mSetHybridPresetToWatchUseCase - onError - lastErrorCode = " + b);
                if (b != 1101) {
                    if (b == 8888) {
                        this.f913a.this$0.j.c();
                        return;
                    } else if (!(b == 1112 || b == 1113)) {
                        this.f913a.this$0.j.P();
                        return;
                    }
                }
                List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(aVar.a());
                pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                zb6 zb6 = this.f913a.this$0.j;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    zb6.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(ib6.c cVar) {
                pq7.c(cVar, "responseValue");
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setToWatch success");
                this.f913a.this$0.j.w();
                this.f913a.this$0.j.t0(true);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(HybridPreset hybridPreset, qn7 qn7, ec6 ec6, boolean z, HybridPreset hybridPreset2) {
            super(2, qn7);
            this.$it = hybridPreset;
            this.this$0 = ec6;
            this.$shouldShowSkippedPermissions$inlined = z;
            this.$currentPreset$inlined = hybridPreset2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.$it, qn7, this.this$0, this.$shouldShowSkippedPermissions$inlined, this.$currentPreset$inlined);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0089, code lost:
            r2 = r14.this$0.h();
            r5 = new com.fossil.ec6.a.C0063a(r14, r1, null);
            r14.L$0 = r6;
            r14.L$1 = r3;
            r14.L$2 = r9;
            r14.L$3 = r1;
            r14.L$4 = r10;
            r14.label = 1;
            r8 = com.fossil.eu7.g(r2, r5, r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x00a6, code lost:
            if (r8 != r11) goto L_0x011f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x011f, code lost:
            r5 = r3;
            r2 = r1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
            return r11;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x005d  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x0143  */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x0227  */
        /* JADX WARNING: Removed duplicated region for block: B:81:0x0124 A[EDGE_INSN: B:81:0x0124->B:37:0x0124 ?: BREAK  , SYNTHETIC] */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 667
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ec6.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<HybridPreset> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ec6 f914a;

        @DexIgnore
        public b(ec6 ec6) {
            this.f914a = ec6;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: androidx.lifecycle.MutableLiveData */
        /* JADX WARN: Multi-variable type inference failed */
        /* renamed from: a */
        public final void onChanged(HybridPreset hybridPreset) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "start - observe current preset value=" + hybridPreset);
            MutableLiveData mutableLiveData = this.f914a.f;
            if (hybridPreset != null) {
                mutableLiveData.l(hybridPreset.clone());
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ec6 f915a;

        @DexIgnore
        public c(ec6 ec6) {
            this.f915a = ec6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "start - observe selected microApp value=" + str);
            zb6 zb6 = this.f915a.j;
            if (str != null) {
                zb6.u4(str);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<m66> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ec6 f916a;

        @DexIgnore
        public d(ec6 ec6) {
            this.f916a = ec6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(m66 m66) {
            if (m66 != null) {
                this.f916a.j.e1(m66);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ec6 f917a;

        @DexIgnore
        public e(ec6 ec6) {
            this.f917a = ec6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "update preset status isChanged=" + bool);
            zb6 zb6 = this.f917a.j;
            if (bool != null) {
                zb6.s0(bool.booleanValue());
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ec6 f918a;

        @DexIgnore
        public f(ec6 ec6) {
            this.f918a = ec6;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<m66> apply(HybridPreset hybridPreset) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(hybridPreset.getButtons());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "wrapperPresetTransformations presetChanged microApps=" + arrayList);
            ArrayList arrayList2 = new ArrayList();
            hl5 hl5 = hl5.f1493a;
            pq7.b(hybridPreset, "preset");
            List<jn5.a> c = hl5.c(hybridPreset);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) it.next();
                String component1 = hybridPresetAppSetting.component1();
                MicroApp n = ec6.x(this.f918a).n(hybridPresetAppSetting.component2());
                if (n != null) {
                    String id = n.getId();
                    String icon = n.getIcon();
                    if (icon == null) {
                        icon = "";
                    }
                    arrayList2.add(new n66(id, icon, um5.d(PortfolioApp.h0.c(), n.getNameKey(), n.getName()), component1, null, 16, null));
                }
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("HybridCustomizeEditPresenter", "wrapperPresetTransformations presetChanged microAppsDetail=" + arrayList2);
            MutableLiveData mutableLiveData = this.f918a.g;
            String id2 = hybridPreset.getId();
            String name = hybridPreset.getName();
            if (name == null) {
                name = "";
            }
            mutableLiveData.l(new m66(id2, name, arrayList2, c, hybridPreset.isActive()));
            return this.f918a.g;
        }
    }

    @DexIgnore
    public ec6(zb6 zb6, ib6 ib6) {
        pq7.c(zb6, "mView");
        pq7.c(ib6, "mSetHybridPresetToWatchUseCase");
        this.j = zb6;
        this.k = ib6;
        LiveData<m66> c2 = ss0.c(this.f, new f(this));
        pq7.b(c2, "Transformations.switchMa\u2026urrentWrapperPreset\n    }");
        this.i = c2;
    }

    @DexIgnore
    public static final /* synthetic */ sb6 x(ec6 ec6) {
        sb6 sb6 = ec6.e;
        if (sb6 != null) {
            return sb6;
        }
        pq7.n("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public Bundle A(Bundle bundle) {
        sb6 sb6 = this.e;
        if (sb6 != null) {
            HybridPreset e2 = sb6.o().e();
            if (!(e2 == null || bundle == null)) {
                bundle.putString("KEY_CURRENT_PRESET", this.h.t(e2));
            }
            sb6 sb62 = this.e;
            if (sb62 != null) {
                HybridPreset r = sb62.r();
                if (!(r == null || bundle == null)) {
                    bundle.putString("KEY_ORIGINAL_PRESET", this.h.t(r));
                }
                sb6 sb63 = this.e;
                if (sb63 != null) {
                    String e3 = sb63.t().e();
                    if (!(e3 == null || bundle == null)) {
                        bundle.putString("KEY_PRESET_WATCH_APP_POS_SELECTED", e3);
                    }
                    return bundle;
                }
                pq7.n("mHybridCustomizeViewModel");
                throw null;
            }
            pq7.n("mHybridCustomizeViewModel");
            throw null;
        }
        pq7.n("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void B() {
        this.j.M5(this);
    }

    @DexIgnore
    public final void C(HybridPreset hybridPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HybridCustomizeEditPresenter", "updateCurrentPreset=" + hybridPreset);
        sb6 sb6 = this.e;
        if (sb6 != null) {
            sb6.B(hybridPreset);
        } else {
            pq7.n("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        if (FossilDeviceSerialPatternUtil.isSamDevice(PortfolioApp.h0.c().J())) {
            this.j.y1();
        }
        this.k.r();
        wq5.d.g(CommunicateMode.SET_LINK_MAPPING);
        sb6 sb6 = this.e;
        if (sb6 != null) {
            MutableLiveData<HybridPreset> o = sb6.o();
            zb6 zb6 = this.j;
            if (zb6 != null) {
                o.h((ac6) zb6, new b(this));
                sb6 sb62 = this.e;
                if (sb62 != null) {
                    sb62.t().h((LifecycleOwner) this.j, new c(this));
                    this.i.h((LifecycleOwner) this.j, new d(this));
                    sb6 sb63 = this.e;
                    if (sb63 != null) {
                        sb63.p().h((LifecycleOwner) this.j, new e(this));
                    } else {
                        pq7.n("mHybridCustomizeViewModel");
                        throw null;
                    }
                } else {
                    pq7.n("mHybridCustomizeViewModel");
                    throw null;
                }
            } else {
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment");
            }
        } else {
            pq7.n("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        this.k.w();
        sb6 sb6 = this.e;
        if (sb6 != null) {
            MutableLiveData<HybridPreset> o = sb6.o();
            zb6 zb6 = this.j;
            if (zb6 != null) {
                o.n((ac6) zb6);
                sb6 sb62 = this.e;
                if (sb62 != null) {
                    sb62.t().n((LifecycleOwner) this.j);
                    this.g.n((LifecycleOwner) this.j);
                    return;
                }
                pq7.n("mHybridCustomizeViewModel");
                throw null;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment");
        }
        pq7.n("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.yb6
    public void n(String str, String str2) {
        T t;
        pq7.c(str, "microAppId");
        pq7.c(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "dropMicroApp - microAppid=" + str + ", toPosition=" + str2);
        sb6 sb6 = this.e;
        if (sb6 == null) {
            pq7.n("mHybridCustomizeViewModel");
            throw null;
        } else if (!sb6.y(str)) {
            sb6 sb62 = this.e;
            if (sb62 != null) {
                HybridPreset e2 = sb62.o().e();
                if (e2 != null) {
                    Iterator<T> it = e2.getButtons().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        if (pq7.a(next.getPosition(), str2)) {
                            t = next;
                            break;
                        }
                    }
                    T t2 = t;
                    if (t2 != null) {
                        t2.setAppId(str);
                        t2.setSettings("");
                    }
                    FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "dropMicroApp - update preset");
                    pq7.b(e2, "currentPreset");
                    C(e2);
                    return;
                }
                return;
            }
            pq7.n("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yb6
    public void o() {
        sb6 sb6 = this.e;
        if (sb6 != null) {
            boolean z = sb6.z();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "onUserExit isPresetChanged " + z);
            if (z) {
                this.j.L();
            } else {
                this.j.t0(false);
            }
        } else {
            pq7.n("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yb6
    public void p(String str) {
        pq7.c(str, "microAppPos");
        sb6 sb6 = this.e;
        if (sb6 != null) {
            sb6.A(str);
        } else {
            pq7.n("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yb6
    public void q(boolean z) {
        sb6 sb6 = this.e;
        if (sb6 != null) {
            HybridPreset e2 = sb6.o().e();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "setPresetToWatch currentPreset=" + e2);
            if (e2 != null) {
                xw7 unused = gu7.d(k(), null, null, new a(e2, null, this, z, e2), 3, null);
                return;
            }
            return;
        }
        pq7.n("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.yb6
    public void r(sb6 sb6) {
        pq7.c(sb6, "viewModel");
        this.e = sb6;
    }

    @DexIgnore
    @Override // com.fossil.yb6
    public void s(String str, String str2) {
        T t;
        T t2;
        pq7.c(str, "fromPosition");
        pq7.c(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "swapMicroApp - fromPosition=" + str + ", toPosition=" + str2);
        if (!pq7.a(str, str2)) {
            sb6 sb6 = this.e;
            if (sb6 != null) {
                HybridPreset e2 = sb6.o().e();
                if (e2 != null) {
                    HybridPreset clone = e2.clone();
                    Iterator<T> it = clone.getButtons().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        if (pq7.a(next.getPosition(), str)) {
                            t = next;
                            break;
                        }
                    }
                    T t3 = t;
                    Iterator<T> it2 = clone.getButtons().iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            t2 = null;
                            break;
                        }
                        T next2 = it2.next();
                        if (pq7.a(next2.getPosition(), str2)) {
                            t2 = next2;
                            break;
                        }
                    }
                    T t4 = t2;
                    if (t3 != null) {
                        t3.setPosition(str2);
                    }
                    if (t4 != null) {
                        t4.setPosition(str);
                    }
                    FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "swapMicroApp - update preset");
                    C(clone);
                    return;
                }
                return;
            }
            pq7.n("mHybridCustomizeViewModel");
            throw null;
        }
    }
}
