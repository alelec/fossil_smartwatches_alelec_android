package com.fossil;

import com.fossil.c44;
import com.fossil.d44;
import com.google.j2objc.annotations.Weak;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b44 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K, V> extends q14<K, V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient m14<? extends List<V>> factory;

        @DexIgnore
        public a(Map<K, Collection<V>> map, m14<? extends List<V>> m14) {
            super(map);
            i14.l(m14);
            this.factory = m14;
        }

        @DexIgnore
        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.factory = (m14) objectInputStream.readObject();
            setMap((Map) objectInputStream.readObject());
        }

        @DexIgnore
        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeObject(this.factory);
            objectOutputStream.writeObject(backingMap());
        }

        @DexIgnore
        @Override // com.fossil.q14, com.fossil.q14, com.fossil.r14
        public List<V> createCollection() {
            return (List) this.factory.get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> extends w14<K, V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient m14<? extends Set<V>> factory;

        @DexIgnore
        public b(Map<K, Collection<V>> map, m14<? extends Set<V>> m14) {
            super(map);
            i14.l(m14);
            this.factory = m14;
        }

        @DexIgnore
        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.factory = (m14) objectInputStream.readObject();
            setMap((Map) objectInputStream.readObject());
        }

        @DexIgnore
        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeObject(this.factory);
            objectOutputStream.writeObject(backingMap());
        }

        @DexIgnore
        @Override // com.fossil.r14, com.fossil.w14, com.fossil.w14
        public Set<V> createCollection() {
            return (Set) this.factory.get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<K, V> extends AbstractCollection<Map.Entry<K, V>> {
        @DexIgnore
        public abstract y34<K, V> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return a().containsEntry(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return a().remove(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        public int size() {
            return a().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<K, V> extends v14<K> {
        @DexIgnore
        @Weak
        public /* final */ y34<K, V> d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends g54<Map.Entry<K, Collection<V>>, c44.a<K>> {

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.b44$d$a$a")
            /* renamed from: com.fossil.b44$d$a$a  reason: collision with other inner class name */
            public class C0013a extends d44.b<K> {
                @DexIgnore
                public /* final */ /* synthetic */ Map.Entry b;

                @DexIgnore
                public C0013a(a aVar, Map.Entry entry) {
                    this.b = entry;
                }

                @DexIgnore
                @Override // com.fossil.c44.a
                public int getCount() {
                    return ((Collection) this.b.getValue()).size();
                }

                @DexIgnore
                @Override // com.fossil.c44.a
                public K getElement() {
                    return (K) this.b.getKey();
                }
            }

            @DexIgnore
            public a(d dVar, Iterator it) {
                super(it);
            }

            @DexIgnore
            /* renamed from: b */
            public c44.a<K> a(Map.Entry<K, Collection<V>> entry) {
                return new C0013a(this, entry);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b extends d44.d<K> {
            @DexIgnore
            public b() {
            }

            @DexIgnore
            @Override // com.fossil.d44.d
            public c44<K> a() {
                return d.this;
            }

            @DexIgnore
            @Override // com.fossil.d44.d
            public boolean contains(Object obj) {
                if (obj instanceof c44.a) {
                    c44.a aVar = (c44.a) obj;
                    Collection<V> collection = d.this.d.asMap().get(aVar.getElement());
                    if (collection != null && collection.size() == aVar.getCount()) {
                        return true;
                    }
                }
                return false;
            }

            @DexIgnore
            public boolean isEmpty() {
                return d.this.d.isEmpty();
            }

            @DexIgnore
            @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
            public Iterator<c44.a<K>> iterator() {
                return d.this.entryIterator();
            }

            @DexIgnore
            @Override // com.fossil.d44.d
            public boolean remove(Object obj) {
                if (obj instanceof c44.a) {
                    c44.a aVar = (c44.a) obj;
                    Collection<V> collection = d.this.d.asMap().get(aVar.getElement());
                    if (collection != null && collection.size() == aVar.getCount()) {
                        collection.clear();
                        return true;
                    }
                }
                return false;
            }

            @DexIgnore
            public int size() {
                return d.this.distinctElements();
            }
        }

        @DexIgnore
        public d(y34<K, V> y34) {
            this.d = y34;
        }

        @DexIgnore
        @Override // com.fossil.v14
        public void clear() {
            this.d.clear();
        }

        @DexIgnore
        @Override // com.fossil.c44, com.fossil.v14
        public boolean contains(Object obj) {
            return this.d.containsKey(obj);
        }

        @DexIgnore
        @Override // com.fossil.c44, com.fossil.v14
        public int count(Object obj) {
            Collection collection = (Collection) x34.n(this.d.asMap(), obj);
            if (collection == null) {
                return 0;
            }
            return collection.size();
        }

        @DexIgnore
        @Override // com.fossil.v14
        public Set<c44.a<K>> createEntrySet() {
            return new b();
        }

        @DexIgnore
        @Override // com.fossil.v14
        public int distinctElements() {
            return this.d.asMap().size();
        }

        @DexIgnore
        @Override // com.fossil.c44, com.fossil.v14
        public Set<K> elementSet() {
            return this.d.keySet();
        }

        @DexIgnore
        @Override // com.fossil.v14
        public Iterator<c44.a<K>> entryIterator() {
            return new a(this, this.d.asMap().entrySet().iterator());
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.v14, java.lang.Iterable
        public Iterator<K> iterator() {
            return x34.h(this.d.entries().iterator());
        }

        @DexIgnore
        @Override // com.fossil.c44, com.fossil.v14
        public int remove(Object obj, int i) {
            a24.b(i, "occurrences");
            if (i == 0) {
                return count(obj);
            }
            Collection collection = (Collection) x34.n(this.d.asMap(), obj);
            if (collection == null) {
                return 0;
            }
            int size = collection.size();
            if (i >= size) {
                collection.clear();
            } else {
                Iterator it = collection.iterator();
                for (int i2 = 0; i2 < i; i2++) {
                    it.next();
                    it.remove();
                }
            }
            return size;
        }
    }

    @DexIgnore
    public static boolean a(y34<?, ?> y34, Object obj) {
        if (obj == y34) {
            return true;
        }
        if (obj instanceof y34) {
            return y34.asMap().equals(((y34) obj).asMap());
        }
        return false;
    }

    @DexIgnore
    public static <K, V> s34<K, V> b(Map<K, Collection<V>> map, m14<? extends List<V>> m14) {
        return new a(map, m14);
    }

    @DexIgnore
    public static <K, V> w44<K, V> c(Map<K, Collection<V>> map, m14<? extends Set<V>> m14) {
        return new b(map, m14);
    }
}
