package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yp4 implements Factory<pl5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f4348a;

    @DexIgnore
    public yp4(uo4 uo4) {
        this.f4348a = uo4;
    }

    @DexIgnore
    public static yp4 a(uo4 uo4) {
        return new yp4(uo4);
    }

    @DexIgnore
    public static pl5 c(uo4 uo4) {
        pl5 F = uo4.F();
        lk7.c(F, "Cannot return null from a non-@Nullable @Provides method");
        return F;
    }

    @DexIgnore
    /* renamed from: b */
    public pl5 get() {
        return c(this.f4348a);
    }
}
