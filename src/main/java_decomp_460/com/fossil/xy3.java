package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.FloatEvaluator;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xy3 {
    @DexIgnore
    public static /* final */ TimeInterpolator F; // = uw3.c;
    @DexIgnore
    public static /* final */ int[] G; // = {16842919, 16842910};
    @DexIgnore
    public static /* final */ int[] H; // = {16843623, 16842908, 16842910};
    @DexIgnore
    public static /* final */ int[] I; // = {16842908, 16842910};
    @DexIgnore
    public static /* final */ int[] J; // = {16843623, 16842910};
    @DexIgnore
    public static /* final */ int[] K; // = {16842910};
    @DexIgnore
    public static /* final */ int[] L; // = new int[0];
    @DexIgnore
    public /* final */ Rect A; // = new Rect();
    @DexIgnore
    public /* final */ RectF B; // = new RectF();
    @DexIgnore
    public /* final */ RectF C; // = new RectF();
    @DexIgnore
    public /* final */ Matrix D; // = new Matrix();
    @DexIgnore
    public ViewTreeObserver.OnPreDrawListener E;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public g04 f4207a;
    @DexIgnore
    public c04 b;
    @DexIgnore
    public Drawable c;
    @DexIgnore
    public wy3 d;
    @DexIgnore
    public Drawable e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g; // = true;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public int k;
    @DexIgnore
    public /* final */ gz3 l;
    @DexIgnore
    public bx3 m;
    @DexIgnore
    public bx3 n;
    @DexIgnore
    public Animator o;
    @DexIgnore
    public bx3 p;
    @DexIgnore
    public bx3 q;
    @DexIgnore
    public float r;
    @DexIgnore
    public float s; // = 1.0f;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u; // = 0;
    @DexIgnore
    public ArrayList<Animator.AnimatorListener> v;
    @DexIgnore
    public ArrayList<Animator.AnimatorListener> w;
    @DexIgnore
    public ArrayList<i> x;
    @DexIgnore
    public /* final */ FloatingActionButton y;
    @DexIgnore
    public /* final */ vz3 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f4208a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ j c;

        @DexIgnore
        public a(boolean z, j jVar) {
            this.b = z;
            this.c = jVar;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            this.f4208a = true;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            xy3.this.u = 0;
            xy3.this.o = null;
            if (!this.f4208a) {
                xy3.this.y.b(this.b ? 8 : 4, this.b);
                j jVar = this.c;
                if (jVar != null) {
                    jVar.b();
                }
            }
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            xy3.this.y.b(0, this.b);
            xy3.this.u = 1;
            xy3.this.o = animator;
            this.f4208a = false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ boolean f4209a;
        @DexIgnore
        public /* final */ /* synthetic */ j b;

        @DexIgnore
        public b(boolean z, j jVar) {
            this.f4209a = z;
            this.b = jVar;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            xy3.this.u = 0;
            xy3.this.o = null;
            j jVar = this.b;
            if (jVar != null) {
                jVar.a();
            }
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            xy3.this.y.b(0, this.f4209a);
            xy3.this.u = 2;
            xy3.this.o = animator;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ax3 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.ax3
        /* renamed from: a */
        public Matrix evaluate(float f, Matrix matrix, Matrix matrix2) {
            xy3.this.s = f;
            return super.a(f, matrix, matrix2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements TypeEvaluator<Float> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public FloatEvaluator f4210a; // = new FloatEvaluator();

        @DexIgnore
        public d(xy3 xy3) {
        }

        @DexIgnore
        /* renamed from: a */
        public Float evaluate(float f, Float f2, Float f3) {
            float floatValue = this.f4210a.evaluate(f, (Number) f2, (Number) f3).floatValue();
            if (floatValue < 0.1f) {
                floatValue = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
            return Float.valueOf(floatValue);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements ViewTreeObserver.OnPreDrawListener {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public boolean onPreDraw() {
            xy3.this.H();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends l {
        @DexIgnore
        public f(xy3 xy3) {
            super(xy3, null);
        }

        @DexIgnore
        @Override // com.fossil.xy3.l
        public float a() {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends l {
        @DexIgnore
        public g() {
            super(xy3.this, null);
        }

        @DexIgnore
        @Override // com.fossil.xy3.l
        public float a() {
            xy3 xy3 = xy3.this;
            return xy3.i + xy3.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends l {
        @DexIgnore
        public h() {
            super(xy3.this, null);
        }

        @DexIgnore
        @Override // com.fossil.xy3.l
        public float a() {
            xy3 xy3 = xy3.this;
            return xy3.j + xy3.h;
        }
    }

    @DexIgnore
    public interface i {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Object b();  // void declaration
    }

    @DexIgnore
    public interface j {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Object b();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends l {
        @DexIgnore
        public k() {
            super(xy3.this, null);
        }

        @DexIgnore
        @Override // com.fossil.xy3.l
        public float a() {
            return xy3.this.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class l extends AnimatorListenerAdapter implements ValueAnimator.AnimatorUpdateListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f4211a;
        @DexIgnore
        public float b;
        @DexIgnore
        public float c;

        @DexIgnore
        public l() {
        }

        @DexIgnore
        public /* synthetic */ l(xy3 xy3, a aVar) {
            this();
        }

        @DexIgnore
        public abstract float a();

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            xy3.this.g0((float) ((int) this.c));
            this.f4211a = false;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            if (!this.f4211a) {
                c04 c04 = xy3.this.b;
                this.b = c04 == null ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : c04.v();
                this.c = a();
                this.f4211a = true;
            }
            xy3 xy3 = xy3.this;
            float f = this.b;
            xy3.g0((float) ((int) (f + ((this.c - f) * valueAnimator.getAnimatedFraction()))));
        }
    }

    @DexIgnore
    public xy3(FloatingActionButton floatingActionButton, vz3 vz3) {
        this.y = floatingActionButton;
        this.z = vz3;
        gz3 gz3 = new gz3();
        this.l = gz3;
        gz3.a(G, i(new h()));
        this.l.a(H, i(new g()));
        this.l.a(I, i(new g()));
        this.l.a(J, i(new g()));
        this.l.a(K, i(new k()));
        this.l.a(L, i(new f(this)));
        this.r = this.y.getRotation();
    }

    @DexIgnore
    public void A() {
        this.l.c();
    }

    @DexIgnore
    public void B() {
        c04 c04 = this.b;
        if (c04 != null) {
            d04.f(this.y, c04);
        }
        if (K()) {
            this.y.getViewTreeObserver().addOnPreDrawListener(r());
        }
    }

    @DexIgnore
    public void C() {
    }

    @DexIgnore
    public void D() {
        ViewTreeObserver viewTreeObserver = this.y.getViewTreeObserver();
        ViewTreeObserver.OnPreDrawListener onPreDrawListener = this.E;
        if (onPreDrawListener != null) {
            viewTreeObserver.removeOnPreDrawListener(onPreDrawListener);
            this.E = null;
        }
    }

    @DexIgnore
    public void E(int[] iArr) {
        this.l.d(iArr);
    }

    @DexIgnore
    public void F(float f2, float f3, float f4) {
        f0();
        g0(f2);
    }

    @DexIgnore
    public void G(Rect rect) {
        pn0.e(this.e, "Didn't initialize content background");
        if (Z()) {
            this.z.b(new InsetDrawable(this.e, rect.left, rect.top, rect.right, rect.bottom));
            return;
        }
        this.z.b(this.e);
    }

    @DexIgnore
    public void H() {
        float rotation = this.y.getRotation();
        if (this.r != rotation) {
            this.r = rotation;
            d0();
        }
    }

    @DexIgnore
    public void I() {
        ArrayList<i> arrayList = this.x;
        if (arrayList != null) {
            Iterator<i> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().b();
            }
        }
    }

    @DexIgnore
    public void J() {
        ArrayList<i> arrayList = this.x;
        if (arrayList != null) {
            Iterator<i> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
        }
    }

    @DexIgnore
    public boolean K() {
        return true;
    }

    @DexIgnore
    public void L(ColorStateList colorStateList) {
        c04 c04 = this.b;
        if (c04 != null) {
            c04.setTintList(colorStateList);
        }
        wy3 wy3 = this.d;
        if (wy3 != null) {
            wy3.c(colorStateList);
        }
    }

    @DexIgnore
    public void M(PorterDuff.Mode mode) {
        c04 c04 = this.b;
        if (c04 != null) {
            c04.setTintMode(mode);
        }
    }

    @DexIgnore
    public final void N(float f2) {
        if (this.h != f2) {
            this.h = f2;
            F(f2, this.i, this.j);
        }
    }

    @DexIgnore
    public void O(boolean z2) {
        this.f = z2;
    }

    @DexIgnore
    public final void P(bx3 bx3) {
        this.q = bx3;
    }

    @DexIgnore
    public final void Q(float f2) {
        if (this.i != f2) {
            this.i = f2;
            F(this.h, f2, this.j);
        }
    }

    @DexIgnore
    public final void R(float f2) {
        this.s = f2;
        Matrix matrix = this.D;
        g(f2, matrix);
        this.y.setImageMatrix(matrix);
    }

    @DexIgnore
    public final void S(int i2) {
        if (this.t != i2) {
            this.t = i2;
            e0();
        }
    }

    @DexIgnore
    public void T(int i2) {
        this.k = i2;
    }

    @DexIgnore
    public final void U(float f2) {
        if (this.j != f2) {
            this.j = f2;
            F(this.h, this.i, f2);
        }
    }

    @DexIgnore
    public void V(ColorStateList colorStateList) {
        Drawable drawable = this.c;
        if (drawable != null) {
            am0.o(drawable, tz3.d(colorStateList));
        }
    }

    @DexIgnore
    public void W(boolean z2) {
        this.g = z2;
        f0();
    }

    @DexIgnore
    public final void X(g04 g04) {
        this.f4207a = g04;
        c04 c04 = this.b;
        if (c04 != null) {
            c04.setShapeAppearanceModel(g04);
        }
        Drawable drawable = this.c;
        if (drawable instanceof j04) {
            ((j04) drawable).setShapeAppearanceModel(g04);
        }
        wy3 wy3 = this.d;
        if (wy3 != null) {
            wy3.f(g04);
        }
    }

    @DexIgnore
    public final void Y(bx3 bx3) {
        this.p = bx3;
    }

    @DexIgnore
    public boolean Z() {
        return true;
    }

    @DexIgnore
    public final boolean a0() {
        return mo0.Q(this.y) && !this.y.isInEditMode();
    }

    @DexIgnore
    public final boolean b0() {
        return !this.f || this.y.getSizeDimension() >= this.k;
    }

    @DexIgnore
    public void c0(j jVar, boolean z2) {
        if (!z()) {
            Animator animator = this.o;
            if (animator != null) {
                animator.cancel();
            }
            if (a0()) {
                if (this.y.getVisibility() != 0) {
                    this.y.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    this.y.setScaleY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    this.y.setScaleX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    R(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                bx3 bx3 = this.p;
                if (bx3 == null) {
                    bx3 = m();
                }
                AnimatorSet h2 = h(bx3, 1.0f, 1.0f, 1.0f);
                h2.addListener(new b(z2, jVar));
                ArrayList<Animator.AnimatorListener> arrayList = this.v;
                if (arrayList != null) {
                    Iterator<Animator.AnimatorListener> it = arrayList.iterator();
                    while (it.hasNext()) {
                        h2.addListener(it.next());
                    }
                }
                h2.start();
                return;
            }
            this.y.b(0, z2);
            this.y.setAlpha(1.0f);
            this.y.setScaleY(1.0f);
            this.y.setScaleX(1.0f);
            R(1.0f);
            if (jVar != null) {
                jVar.a();
            }
        }
    }

    @DexIgnore
    public void d(Animator.AnimatorListener animatorListener) {
        if (this.w == null) {
            this.w = new ArrayList<>();
        }
        this.w.add(animatorListener);
    }

    @DexIgnore
    public void d0() {
        if (Build.VERSION.SDK_INT == 19) {
            if (this.r % 90.0f != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                if (this.y.getLayerType() != 1) {
                    this.y.setLayerType(1, null);
                }
            } else if (this.y.getLayerType() != 0) {
                this.y.setLayerType(0, null);
            }
        }
        c04 c04 = this.b;
        if (c04 != null) {
            c04.b0((int) this.r);
        }
    }

    @DexIgnore
    public void e(Animator.AnimatorListener animatorListener) {
        if (this.v == null) {
            this.v = new ArrayList<>();
        }
        this.v.add(animatorListener);
    }

    @DexIgnore
    public final void e0() {
        R(this.s);
    }

    @DexIgnore
    public void f(i iVar) {
        if (this.x == null) {
            this.x = new ArrayList<>();
        }
        this.x.add(iVar);
    }

    @DexIgnore
    public final void f0() {
        Rect rect = this.A;
        s(rect);
        G(rect);
        this.z.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    @DexIgnore
    public final void g(float f2, Matrix matrix) {
        matrix.reset();
        Drawable drawable = this.y.getDrawable();
        if (drawable != null && this.t != 0) {
            RectF rectF = this.B;
            RectF rectF2 = this.C;
            rectF.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight());
            int i2 = this.t;
            rectF2.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i2, (float) i2);
            matrix.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.CENTER);
            int i3 = this.t;
            matrix.postScale(f2, f2, ((float) i3) / 2.0f, ((float) i3) / 2.0f);
        }
    }

    @DexIgnore
    public void g0(float f2) {
        c04 c04 = this.b;
        if (c04 != null) {
            c04.U(f2);
        }
    }

    @DexIgnore
    public final AnimatorSet h(bx3 bx3, float f2, float f3, float f4) {
        ArrayList arrayList = new ArrayList();
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.y, View.ALPHA, f2);
        bx3.h("opacity").a(ofFloat);
        arrayList.add(ofFloat);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.y, View.SCALE_X, f3);
        bx3.h("scale").a(ofFloat2);
        h0(ofFloat2);
        arrayList.add(ofFloat2);
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(this.y, View.SCALE_Y, f3);
        bx3.h("scale").a(ofFloat3);
        h0(ofFloat3);
        arrayList.add(ofFloat3);
        g(f4, this.D);
        ObjectAnimator ofObject = ObjectAnimator.ofObject(this.y, new zw3(), new c(), new Matrix(this.D));
        bx3.h("iconScale").a(ofObject);
        arrayList.add(ofObject);
        AnimatorSet animatorSet = new AnimatorSet();
        vw3.a(animatorSet, arrayList);
        return animatorSet;
    }

    @DexIgnore
    public final void h0(ObjectAnimator objectAnimator) {
        if (Build.VERSION.SDK_INT == 26) {
            objectAnimator.setEvaluator(new d(this));
        }
    }

    @DexIgnore
    public final ValueAnimator i(l lVar) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setInterpolator(F);
        valueAnimator.setDuration(100L);
        valueAnimator.addListener(lVar);
        valueAnimator.addUpdateListener(lVar);
        valueAnimator.setFloatValues(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
        return valueAnimator;
    }

    @DexIgnore
    public c04 j() {
        g04 g04 = this.f4207a;
        pn0.d(g04);
        return new c04(g04);
    }

    @DexIgnore
    public final Drawable k() {
        return this.e;
    }

    @DexIgnore
    public final bx3 l() {
        if (this.n == null) {
            this.n = bx3.d(this.y.getContext(), iw3.design_fab_hide_motion_spec);
        }
        bx3 bx3 = this.n;
        pn0.d(bx3);
        return bx3;
    }

    @DexIgnore
    public final bx3 m() {
        if (this.m == null) {
            this.m = bx3.d(this.y.getContext(), iw3.design_fab_show_motion_spec);
        }
        bx3 bx3 = this.m;
        pn0.d(bx3);
        return bx3;
    }

    @DexIgnore
    public float n() {
        return this.h;
    }

    @DexIgnore
    public boolean o() {
        return this.f;
    }

    @DexIgnore
    public final bx3 p() {
        return this.q;
    }

    @DexIgnore
    public float q() {
        return this.i;
    }

    @DexIgnore
    public final ViewTreeObserver.OnPreDrawListener r() {
        if (this.E == null) {
            this.E = new e();
        }
        return this.E;
    }

    @DexIgnore
    public void s(Rect rect) {
        int sizeDimension = this.f ? (this.k - this.y.getSizeDimension()) / 2 : 0;
        float n2 = this.g ? n() + this.j : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int max = Math.max(sizeDimension, (int) Math.ceil((double) n2));
        int max2 = Math.max(sizeDimension, (int) Math.ceil((double) (n2 * 1.5f)));
        rect.set(max, max2, max, max2);
    }

    @DexIgnore
    public float t() {
        return this.j;
    }

    @DexIgnore
    public final g04 u() {
        return this.f4207a;
    }

    @DexIgnore
    public final bx3 v() {
        return this.p;
    }

    @DexIgnore
    public void w(j jVar, boolean z2) {
        if (!y()) {
            Animator animator = this.o;
            if (animator != null) {
                animator.cancel();
            }
            if (a0()) {
                bx3 bx3 = this.q;
                if (bx3 == null) {
                    bx3 = l();
                }
                AnimatorSet h2 = h(bx3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                h2.addListener(new a(z2, jVar));
                ArrayList<Animator.AnimatorListener> arrayList = this.w;
                if (arrayList != null) {
                    Iterator<Animator.AnimatorListener> it = arrayList.iterator();
                    while (it.hasNext()) {
                        h2.addListener(it.next());
                    }
                }
                h2.start();
                return;
            }
            this.y.b(z2 ? 8 : 4, z2);
            if (jVar != null) {
                jVar.b();
            }
        }
    }

    @DexIgnore
    public void x(ColorStateList colorStateList, PorterDuff.Mode mode, ColorStateList colorStateList2, int i2) {
        c04 j2 = j();
        this.b = j2;
        j2.setTintList(colorStateList);
        if (mode != null) {
            this.b.setTintMode(mode);
        }
        this.b.a0(-12303292);
        this.b.M(this.y.getContext());
        sz3 sz3 = new sz3(this.b.C());
        sz3.setTintList(tz3.d(colorStateList2));
        this.c = sz3;
        c04 c04 = this.b;
        pn0.d(c04);
        this.e = new LayerDrawable(new Drawable[]{c04, sz3});
    }

    @DexIgnore
    public boolean y() {
        if (this.y.getVisibility() == 0) {
            if (this.u == 1) {
                return true;
            }
        } else if (this.u != 2) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public boolean z() {
        if (this.y.getVisibility() != 0) {
            if (this.u == 2) {
                return true;
            }
        } else if (this.u != 1) {
            return true;
        }
        return false;
    }
}
