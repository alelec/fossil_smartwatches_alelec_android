package com.fossil;

import android.animation.Animator;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o67 implements View.OnTouchListener {
    @DexIgnore
    public static /* final */ DisplayMetrics j;
    @DexIgnore
    public static /* final */ int k; // = s.b(10.0f);
    @DexIgnore
    public static /* final */ int l; // = s.b(1.0f);
    @DexIgnore
    public static /* final */ yk7 m; // = zk7.a(a.INSTANCE);
    @DexIgnore
    public static /* final */ b s; // = new b(null);
    @DexIgnore
    public /* final */ c b; // = new c(this);
    @DexIgnore
    public /* final */ vn0 c; // = new vn0(this.i.getContext(), this.b);
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public int h; // = -1;
    @DexIgnore
    public /* final */ w67 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements gp7<Float> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(0);
        }

        @DexIgnore
        /* Return type fixed from 'float' to match base method */
        @Override // com.fossil.gp7
        public final Float invoke() {
            return TypedValue.applyDimension(1, 2.0f, o67.j);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final int b(float f) {
            return (int) TypedValue.applyDimension(1, f, o67.j);
        }

        @DexIgnore
        public final float c() {
            yk7 yk7 = o67.m;
            b bVar = o67.s;
            return ((Number) yk7.getValue()).floatValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public /* final */ /* synthetic */ o67 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(o67 o67) {
            this.b = o67;
        }

        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            this.b.c().o();
            return super.onSingleTapUp(motionEvent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends m67 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ o67 f2637a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(o67 o67) {
            this.f2637a = o67;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.f2637a.c().setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.f2637a.c().e();
            this.f2637a.c().setAnimating$app_fossilRelease(false);
        }
    }

    /*
    static {
        Resources system = Resources.getSystem();
        pq7.b(system, "Resources.getSystem()");
        j = system.getDisplayMetrics();
    }
    */

    @DexIgnore
    public o67(w67 w67) {
        pq7.c(w67, "element");
        this.i = w67;
    }

    @DexIgnore
    public final w67 c() {
        return this.i;
    }

    @DexIgnore
    public void d(View view, MotionEvent motionEvent) {
        boolean z = false;
        int i2 = 1;
        pq7.c(view, "_view");
        pq7.c(motionEvent, Constants.EVENT);
        if (!this.i.h()) {
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked == 0) {
                this.h = motionEvent.getPointerId(0);
                this.d = motionEvent.getX();
                this.e = motionEvent.getY();
                this.i.setEditing$app_fossilRelease(true);
                this.i.setTranslating$app_fossilRelease(false);
                this.i.setElevation(s.c());
                w67 w67 = this.i;
                w67.setTouchCount$app_fossilRelease(w67.getTouchCount$app_fossilRelease() + 1);
                this.i.bringToFront();
                this.i.q();
            } else if (actionMasked == 1) {
                this.h = -1;
                this.i.setEditing$app_fossilRelease(false);
                if (!this.i.a()) {
                    this.i.setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    this.i.e();
                    float f2 = this.f;
                    int i3 = l;
                    if (f2 > ((float) i3) || this.g > ((float) i3)) {
                        z = true;
                    }
                    if (z) {
                        this.i.m();
                    } else if (this.i.getTouchCount$app_fossilRelease() == 1) {
                        this.i.m();
                    }
                } else if (this.i.k(motionEvent)) {
                    this.i.b();
                    this.i.e();
                } else {
                    d dVar = new d(this);
                    this.i.setAnimating$app_fossilRelease(true);
                    this.i.animate().translationX(this.i.getMPrevTranslateX()).translationY(this.i.getMPrevTranslateY()).setListener(dVar);
                }
            } else if (actionMasked != 2) {
                if (actionMasked == 3) {
                    this.h = -1;
                } else if (actionMasked == 6) {
                    int actionIndex = motionEvent.getActionIndex();
                    if (motionEvent.getPointerId(actionIndex) == this.h) {
                        if (actionIndex != 0) {
                            i2 = 0;
                        }
                        this.d = motionEvent.getX(i2);
                        this.e = motionEvent.getY(i2);
                        this.h = motionEvent.getPointerId(i2);
                    }
                }
            } else if (motionEvent.getPointerId(motionEvent.getActionIndex()) == this.h) {
                float[] fArr = {motionEvent.getX() - this.d, motionEvent.getY() - this.e};
                this.i.getMatrix().mapVectors(fArr);
                w67 w672 = this.i;
                w672.setTranslationX(w672.getTranslationX() + fArr[0]);
                w67 w673 = this.i;
                w673.setTranslationY(w673.getTranslationY() + fArr[1]);
                this.f = Math.abs(this.i.getTranslationX() - this.i.getMPrevTranslateX());
                this.g = Math.abs(this.i.getTranslationY() - this.i.getMPrevTranslateY());
                if (!this.i.l()) {
                    w67 w674 = this.i;
                    float f3 = this.f;
                    int i4 = k;
                    if (f3 > ((float) i4) || this.g > ((float) i4)) {
                        z = true;
                    }
                    w674.setTranslating$app_fossilRelease(z);
                }
                if (this.i.l()) {
                    this.i.f();
                }
            }
            if (!this.i.l()) {
                this.c.a(motionEvent);
            }
        }
    }

    @DexIgnore
    public boolean onTouch(View view, MotionEvent motionEvent) {
        pq7.c(view, "view");
        pq7.c(motionEvent, Constants.EVENT);
        boolean z = this.i.i() || this.i.p();
        if (z) {
            d(view, motionEvent);
        }
        return z;
    }
}
