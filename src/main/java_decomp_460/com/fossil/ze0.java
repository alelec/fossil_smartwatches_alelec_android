package com.fossil;

import android.app.Dialog;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ze0 extends kq0 {
    @DexIgnore
    @Override // com.fossil.kq0
    public Dialog onCreateDialog(Bundle bundle) {
        return new ye0(getContext(), getTheme());
    }

    @DexIgnore
    @Override // com.fossil.kq0
    public void setupDialog(Dialog dialog, int i) {
        if (dialog instanceof ye0) {
            ye0 ye0 = (ye0) dialog;
            if (!(i == 1 || i == 2)) {
                if (i == 3) {
                    dialog.getWindow().addFlags(24);
                } else {
                    return;
                }
            }
            ye0.d(1);
            return;
        }
        super.setupDialog(dialog, i);
    }
}
