package com.fossil;

import com.fossil.dl7;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fz7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ int f1242a; // = d(Throwable.class, -1);
    @DexIgnore
    public static /* final */ ReentrantReadWriteLock b; // = new ReentrantReadWriteLock();
    @DexIgnore
    public static /* final */ WeakHashMap<Class<? extends Throwable>, rp7<Throwable, Throwable>> c; // = new WeakHashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object r0;
            try {
                dl7.a aVar = dl7.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(th.getMessage(), th);
                if (newInstance != null) {
                    r0 = dl7.m1constructorimpl((Throwable) newInstance);
                    if (dl7.m6isFailureimpl(r0)) {
                        r0 = null;
                    }
                    return (Throwable) r0;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th2) {
                dl7.a aVar2 = dl7.Companion;
                r0 = dl7.m1constructorimpl(el7.a(th2));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements rp7<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object r0;
            try {
                dl7.a aVar = dl7.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(th);
                if (newInstance != null) {
                    r0 = dl7.m1constructorimpl((Throwable) newInstance);
                    if (dl7.m6isFailureimpl(r0)) {
                        r0 = null;
                    }
                    return (Throwable) r0;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th2) {
                dl7.a aVar2 = dl7.Companion;
                r0 = dl7.m1constructorimpl(el7.a(th2));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qq7 implements rp7<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object r0;
            try {
                dl7.a aVar = dl7.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(th.getMessage());
                if (newInstance != null) {
                    Throwable th2 = (Throwable) newInstance;
                    th2.initCause(th);
                    r0 = dl7.m1constructorimpl(th2);
                    if (dl7.m6isFailureimpl(r0)) {
                        r0 = null;
                    }
                    return (Throwable) r0;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th3) {
                dl7.a aVar2 = dl7.Companion;
                r0 = dl7.m1constructorimpl(el7.a(th3));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends qq7 implements rp7<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object r0;
            try {
                dl7.a aVar = dl7.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(new Object[0]);
                if (newInstance != null) {
                    Throwable th2 = (Throwable) newInstance;
                    th2.initCause(th);
                    r0 = dl7.m1constructorimpl(th2);
                    if (dl7.m6isFailureimpl(r0)) {
                        r0 = null;
                    }
                    return (Throwable) r0;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th3) {
                dl7.a aVar2 = dl7.Companion;
                r0 = dl7.m1constructorimpl(el7.a(th3));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return mn7.c(Integer.valueOf(t2.getParameterTypes().length), Integer.valueOf(t.getParameterTypes().length));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends qq7 implements rp7 {
        @DexIgnore
        public static /* final */ f INSTANCE; // = new f();

        @DexIgnore
        public f() {
            super(1);
        }

        @DexIgnore
        public final Void invoke(Throwable th) {
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends qq7 implements rp7 {
        @DexIgnore
        public static /* final */ g INSTANCE; // = new g();

        @DexIgnore
        public g() {
            super(1);
        }

        @DexIgnore
        public final Void invoke(Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static final rp7<Throwable, Throwable> a(Constructor<?> constructor) {
        Class<?>[] parameterTypes = constructor.getParameterTypes();
        int length = parameterTypes.length;
        if (length == 0) {
            return new d(constructor);
        }
        if (length == 1) {
            Class<?> cls = parameterTypes[0];
            if (pq7.a(cls, Throwable.class)) {
                return new b(constructor);
            }
            if (pq7.a(cls, String.class)) {
                return new c(constructor);
            }
            return null;
        } else if (length == 2 && pq7.a(parameterTypes[0], String.class) && pq7.a(parameterTypes[1], Throwable.class)) {
            return new a(constructor);
        } else {
            return null;
        }
    }

    @DexIgnore
    public static final int b(Class<?> cls, int i) {
        do {
            Field[] declaredFields = cls.getDeclaredFields();
            int length = declaredFields.length;
            int i2 = 0;
            for (int i3 = 0; i3 < length; i3++) {
                if (!Modifier.isStatic(declaredFields[i3].getModifiers())) {
                    i2++;
                }
            }
            i += i2;
            cls = cls.getSuperclass();
        } while (cls != null);
        return i;
    }

    @DexIgnore
    public static /* synthetic */ int c(Class cls, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        return b(cls, i);
    }

    @DexIgnore
    public static final int d(Class<?> cls, int i) {
        Object r0;
        ep7.c(cls);
        try {
            dl7.a aVar = dl7.Companion;
            r0 = dl7.m1constructorimpl(Integer.valueOf(c(cls, 0, 1, null)));
        } catch (Throwable th) {
            dl7.a aVar2 = dl7.Companion;
            r0 = dl7.m1constructorimpl(el7.a(th));
        }
        if (dl7.m6isFailureimpl(r0)) {
            r0 = Integer.valueOf(i);
        }
        return ((Number) r0).intValue();
    }

    /*  JADX ERROR: StackOverflowError in pass: MarkFinallyVisitor
        java.lang.StackOverflowError
        	at jadx.core.dex.nodes.InsnNode.isSame(InsnNode.java:303)
        	at jadx.core.dex.instructions.InvokeNode.isSame(InvokeNode.java:77)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.sameInsns(MarkFinallyVisitor.java:451)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.compareBlocks(MarkFinallyVisitor.java:436)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.checkBlocksTree(MarkFinallyVisitor.java:408)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.checkBlocksTree(MarkFinallyVisitor.java:411)
        */
    @DexIgnore
    public static final <E extends java.lang.Throwable> E e(E r9) {
        /*
        // Method dump skipped, instructions count: 294
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fz7.e(java.lang.Throwable):java.lang.Throwable");
    }
}
