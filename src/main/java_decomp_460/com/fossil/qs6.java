package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.t47;
import com.fossil.ts6;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.chart.TodayHeartRateChart;
import com.sina.weibo.sdk.constant.WBConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import net.sqlcipher.database.SQLiteDatabase;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qs6 extends pv5 implements x47, t47.g {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static String l;
    @DexIgnore
    public static String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public ts6 h;
    @DexIgnore
    public g37<x45> i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return qs6.l;
        }

        @DexIgnore
        public final String b() {
            return qs6.m;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<ts6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ qs6 f3013a;

        @DexIgnore
        public b(qs6 qs6) {
            this.f3013a = qs6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ts6.a aVar) {
            if (aVar != null) {
                Integer a2 = aVar.a();
                if (a2 != null) {
                    this.f3013a.N6(a2.intValue());
                }
                Integer c = aVar.c();
                if (c != null) {
                    this.f3013a.P6(c.intValue());
                }
                Integer b = aVar.b();
                if (b != null) {
                    this.f3013a.O6(b.intValue());
                }
                Integer d = aVar.d();
                if (d != null) {
                    this.f3013a.Q6(d.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qs6 b;

        @DexIgnore
        public c(qs6 qs6) {
            this.b = qs6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, 601);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qs6 b;

        @DexIgnore
        public d(qs6 qs6) {
            this.b = qs6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, Action.Bolt.TURN_OFF);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qs6 b;

        @DexIgnore
        public e(qs6 qs6) {
            this.b = qs6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.p(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = qs6.class.getSimpleName();
        pq7.b(simpleName, "CustomizeHeartRateChartF\u2026nt::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.x47
    public void C3(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        hr7 hr7 = hr7.f1520a;
        String format = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(16777215 & i3)}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        ts6 ts6 = this.h;
        if (ts6 != null) {
            ts6.h(i2, Color.parseColor(format));
            if (i2 == 601) {
                l = format;
            } else if (i2 == 602) {
                m = format;
            }
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void M6() {
        TodayHeartRateChart todayHeartRateChart;
        TodayHeartRateChart todayHeartRateChart2;
        TodayHeartRateChart todayHeartRateChart3;
        g37<x45> g37 = this.i;
        if (g37 != null) {
            x45 a2 = g37.a();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new w57(84, 44, 198, 45, 50, 47));
            arrayList.add(new w57(93, 86, 108, 50, 55, 52));
            arrayList.add(new w57(90, 64, 100, 55, 60, 57));
            arrayList.add(new w57(70, 70, 197, 60, 65, 62));
            arrayList.add(new w57(92, 92, 100, 65, 70, 67));
            arrayList.add(new w57(86, 80, 120, 70, 75, 72));
            arrayList.add(new w57(87, 80, 90, 75, 80, 77));
            arrayList.add(new w57(78, 60, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 80, 85, 82));
            arrayList.add(new w57(90, 60, 180, 85, 90, 87));
            arrayList.add(new w57(70, 70, 187, 90, 95, 92));
            arrayList.add(new w57(70, 70, 187, 95, 100, 97));
            arrayList.add(new w57(69, 60, DateTimeConstants.HOURS_PER_WEEK, 100, 105, 102));
            arrayList.add(new w57(70, 70, 187, 105, 110, 107));
            arrayList.add(new w57(65, 60, 70, 110, 115, 112));
            arrayList.add(new w57(ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 14, DateTimeConstants.HOURS_PER_WEEK, 115, 120, 117));
            arrayList.add(new w57(90, 80, 100, 120, 125, 122));
            arrayList.add(new w57(92, 90, 184, 125, 130, 127));
            arrayList.add(new w57(110, 80, 124, 130, 135, 132));
            arrayList.add(new w57(65, 60, 70, 135, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 137));
            arrayList.add(new w57(ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 14, DateTimeConstants.HOURS_PER_WEEK, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 145, 142));
            arrayList.add(new w57(65, 60, 70, 145, 150, 147));
            arrayList.add(new w57(ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 14, DateTimeConstants.HOURS_PER_WEEK, 150, 155, 152));
            arrayList.add(new w57(108, 90, Action.Music.MUSIC_END_ACTION, 155, 160, 157));
            arrayList.add(new w57(60, 50, 70, 160, 165, 162));
            arrayList.add(new w57(60, 60, 80, 165, 170, 167));
            arrayList.add(new w57(96, 90, 169, 170, 175, 172));
            arrayList.add(new w57(182, 162, FacebookRequestErrorClassification.EC_INVALID_TOKEN, 175, 180, 177));
            arrayList.add(new w57(96, 90, 169, 180, 185, 182));
            arrayList.add(new w57(65, 60, 70, 185, FacebookRequestErrorClassification.EC_INVALID_TOKEN, 187));
            arrayList.add(new w57(96, 90, 169, FacebookRequestErrorClassification.EC_INVALID_TOKEN, 195, 192));
            arrayList.add(new w57(87, 80, 187, 195, 200, 197));
            arrayList.add(new w57(78, 78, 186, 200, 205, Action.Selfie.TAKE_BURST));
            arrayList.add(new w57(84, 44, 98, 205, 210, 207));
            arrayList.add(new w57(93, 86, 100, 210, 215, 212));
            arrayList.add(new w57(80, 70, 92, 215, 220, 217));
            arrayList.add(new w57(72, 72, 180, 220, 225, 222));
            arrayList.add(new w57(84, 44, 98, 225, 230, 227));
            arrayList.add(new w57(76, 70, 80, 230, 235, 232));
            arrayList.add(new w57(75, 80, 90, 235, 240, 237));
            arrayList.add(new w57(68, 60, 102, 240, 245, 242));
            arrayList.add(new w57(80, 50, 100, 245, 250, 247));
            arrayList.add(new w57(84, 44, 98, 250, 255, 252));
            arrayList.add(new w57(80, 66, 142, 255, 260, 257));
            arrayList.add(new w57(66, 60, 156, 260, 265, 262));
            arrayList.add(new w57(78, 70, 87, 265, 270, 267));
            arrayList.add(new w57(65, 60, 70, 270, 275, 272));
            arrayList.add(new w57(65, 60, 70, 275, 280, 277));
            arrayList.add(new w57(90, 90, 184, 280, 285, 282));
            arrayList.add(new w57(92, 80, 184, 285, 290, 287));
            arrayList.add(new w57(94, 60, 182, 290, 295, 292));
            arrayList.add(new w57(94, 66, 96, 295, 200, 297));
            arrayList.add(new w57(94, 64, 98, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS, 305, Action.Presenter.PREVIOUS));
            arrayList.add(new w57(84, 44, 98, 305, 310, 307));
            arrayList.add(new w57(65, 60, 70, 310, 315, 312));
            arrayList.add(new w57(80, 70, 100, 315, 320, 317));
            arrayList.add(new w57(92, 80, 184, 320, 325, 322));
            arrayList.add(new w57(84, 70, 87, 325, 330, 327));
            arrayList.add(new w57(80, 80, 155, 330, 335, 332));
            arrayList.add(new w57(78, 78, 186, 335, 340, 337));
            arrayList.add(new w57(52, 50, 60, 340, 345, 342));
            arrayList.add(new w57(69, 60, 80, 345, 350, 347));
            arrayList.add(new w57(88, 80, 90, 350, 355, 352));
            arrayList.add(new w57(88, 70, 187, 355, 360, 357));
            arrayList.add(new w57(92, 80, 92, 360, 365, 362));
            arrayList.add(new w57(66, 60, 76, 365, 370, 367));
            arrayList.add(new w57(93, 86, 108, 370, 375, 372));
            arrayList.add(new w57(87, 80, 90, 375, 380, 377));
            arrayList.add(new w57(80, 78, 82, 380, 385, 382));
            arrayList.add(new w57(66, 60, 70, 385, 390, 387));
            arrayList.add(new w57(70, 70, 70, 390, 395, 392));
            arrayList.add(new w57(70, 70, 70, 395, MFNetworkReturnCode.BAD_REQUEST, 397));
            arrayList.add(new w57(76, 70, 80, MFNetworkReturnCode.BAD_REQUEST, 405, Action.ActivityTracker.TAG_ACTIVITY));
            arrayList.add(new w57(66, 60, 70, 405, 410, 407));
            arrayList.add(new w57(90, 60, 90, 410, MFNetworkReturnCode.CONTENT_TYPE_ERROR, FacebookRequestErrorClassification.EC_APP_NOT_INSTALLED));
            arrayList.add(new w57(92, 80, 92, MFNetworkReturnCode.CONTENT_TYPE_ERROR, 420, 417));
            arrayList.add(new w57(76, 70, 80, 420, 425, 422));
            arrayList.add(new w57(92, 90, 95, 425, 430, 427));
            arrayList.add(new w57(92, 90, 92, 430, 435, 432));
            arrayList.add(new w57(65, 60, 70, 435, 440, 437));
            arrayList.add(new w57(92, 90, 92, 440, 445, 442));
            arrayList.add(new w57(92, 90, 100, 445, 450, 447));
            arrayList.add(new w57(88, 80, 90, 450, 455, 452));
            arrayList.add(new w57(94, 64, 98, 455, 460, 457));
            arrayList.add(new w57(60, 50, 70, 460, 465, 462));
            arrayList.add(new w57(60, 60, 80, 465, 470, 467));
            arrayList.add(new w57(92, 80, 92, 470, 475, 472));
            arrayList.add(new w57(84, 64, 90, 475, 480, 477));
            arrayList.add(new w57(78, 78, 80, 480, 485, 482));
            arrayList.add(new w57(90, 60, 90, 485, 490, 487));
            arrayList.add(new w57(96, 90, 96, 490, 495, 492));
            arrayList.add(new w57(92, 80, 92, 495, MFNetworkReturnCode.BAD_REQUEST, 497));
            arrayList.add(new w57(78, 78, 80, 500, Action.Apps.RING_MY_PHONE, Action.Apps.IF));
            arrayList.add(new w57(84, 44, 86, Action.Apps.RING_MY_PHONE, 510, 507));
            arrayList.add(new w57(93, 86, 93, 510, 515, 512));
            arrayList.add(new w57(80, 70, 92, 515, 520, 517));
            arrayList.add(new w57(94, 64, 98, 520, 525, 522));
            arrayList.add(new w57(84, 64, 90, 525, 530, 527));
            arrayList.add(new w57(76, 70, 80, 530, 535, 532));
            arrayList.add(new w57(75, 80, 90, 535, 540, 537));
            arrayList.add(new w57(94, 64, 98, 540, 545, 542));
            arrayList.add(new w57(80, 50, 80, 545, 550, 547));
            arrayList.add(new w57(84, 64, 98, 550, 555, 552));
            arrayList.add(new w57(80, 66, 80, 555, 560, 557));
            arrayList.add(new w57(94, 64, 98, 560, 565, 562));
            arrayList.add(new w57(94, 64, 98, 565, 570, 567));
            arrayList.add(new w57(84, 64, 98, 570, 575, 572));
            arrayList.add(new w57(84, 64, 90, 575, 580, 577));
            arrayList.add(new w57(90, 90, 92, 580, 585, 582));
            arrayList.add(new w57(52, 50, 60, 585, 590, 587));
            arrayList.add(new w57(94, 64, 98, 590, 595, 592));
            arrayList.add(new w57(94, 66, 96, 595, 600, 597));
            arrayList.add(new w57(78, 78, 80, 600, 605, Action.Bolt.TURN_OFF));
            arrayList.add(new w57(92, 90, 92, 605, 610, 607));
            arrayList.add(new w57(52, 50, 60, 610, 615, 612));
            arrayList.add(new w57(80, 70, 90, 615, 620, 617));
            arrayList.add(new w57(84, 44, 90, 620, 625, 622));
            arrayList.add(new w57(84, 70, 87, 625, 630, 627));
            arrayList.add(new w57(80, 80, 82, 630, 635, 632));
            arrayList.add(new w57(78, 78, 80, 635, 640, 637));
            arrayList.add(new w57(52, 50, 60, 640, 645, 642));
            arrayList.add(new w57(69, 60, 80, 645, 650, 647));
            arrayList.add(new w57(88, 80, 90, 650, 655, 652));
            arrayList.add(new w57(88, 70, 90, 655, 660, 657));
            arrayList.add(new w57(70, 60, 70, 660, 665, 662));
            arrayList.add(new w57(78, 70, 87, 665, 670, 667));
            arrayList.add(new w57(84, 64, 98, 670, 675, 672));
            arrayList.add(new w57(90, 60, 100, 675, 680, 677));
            arrayList.add(new w57(90, 90, 92, 680, 685, 682));
            arrayList.add(new w57(92, 80, 92, 685, 690, 687));
            arrayList.add(new w57(94, 60, 94, 690, 695, 692));
            arrayList.add(new w57(94, 64, 98, 695, 700, 697));
            arrayList.add(new w57(78, 78, 80, 700, 705, 702));
            arrayList.add(new w57(82, 80, 92, 705, 710, 707));
            arrayList.add(new w57(92, 80, 92, 710, 715, 712));
            arrayList.add(new w57(80, 70, 90, 715, 720, 717));
            arrayList.add(new w57(94, 64, 98, 720, 725, 722));
            arrayList.add(new w57(84, 70, 87, 725, 730, 727));
            arrayList.add(new w57(129, 110, 182, 730, 735, 732));
            arrayList.add(new w57(80, 78, 80, 735, 740, 737));
            arrayList.add(new w57(52, 50, 60, 740, 745, 742));
            arrayList.add(new w57(52, 50, 60, 745, 750, 747));
            arrayList.add(new w57(88, 80, 90, 750, 755, 752));
            arrayList.add(new w57(88, 70, 90, 755, 760, 757));
            arrayList.add(new w57(70, 60, 70, 760, WBConstants.SDK_ACTIVITY_FOR_RESULT_CODE, 762));
            arrayList.add(new w57(78, 70, 87, WBConstants.SDK_ACTIVITY_FOR_RESULT_CODE, 770, 767));
            arrayList.add(new w57(129, 110, 182, 770, 775, 772));
            arrayList.add(new w57(90, 60, 100, 775, 780, 777));
            arrayList.add(new w57(90, 90, 92, 780, 785, 782));
            arrayList.add(new w57(92, 80, 92, 785, 790, 787));
            arrayList.add(new w57(129, 110, 182, 790, 795, 792));
            arrayList.add(new w57(84, 66, 96, 795, 800, 797));
            arrayList.add(new w57(94, 64, 98, 800, 805, 802));
            arrayList.add(new w57(82, 80, 92, 805, 810, 807));
            arrayList.add(new w57(52, 50, 60, 810, 815, 812));
            arrayList.add(new w57(80, 70, 90, 815, 820, 817));
            arrayList.add(new w57(84, 44, 90, 820, 825, 822));
            arrayList.add(new w57(70, 70, 197, 825, 830, 827));
            arrayList.add(new w57(80, 80, 82, 830, 835, 832));
            arrayList.add(new w57(80, 78, 80, 835, Constants.MAXIMUM_SECOND_TIMEZONE_OFFSET, 837));
            arrayList.add(new w57(94, 64, 98, Constants.MAXIMUM_SECOND_TIMEZONE_OFFSET, 845, 842));
            arrayList.add(new w57(84, 44, 198, 845, 850, 847));
            arrayList.add(new w57(93, 86, 108, 850, 855, 852));
            arrayList.add(new w57(80, 44, 180, 855, 860, 857));
            arrayList.add(new w57(70, 70, 197, 860, 865, 862));
            arrayList.add(new w57(72, 72, 180, 865, 870, 867));
            arrayList.add(new w57(76, 70, 160, 870, 875, 872));
            arrayList.add(new w57(94, 64, 98, 875, 880, 877));
            arrayList.add(new w57(78, 60, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 880, 885, 882));
            arrayList.add(new w57(70, 70, 197, 885, 890, 887));
            arrayList.add(new w57(69, 60, DateTimeConstants.HOURS_PER_WEEK, 890, 895, 892));
            arrayList.add(new w57(94, 64, 98, 895, 900, 897));
            arrayList.add(new w57(129, 110, 182, 900, 905, 902));
            arrayList.add(new w57(72, 72, 180, 905, 910, 907));
            arrayList.add(new w57(72, 72, 180, 910, 915, 912));
            arrayList.add(new w57(129, 110, 182, 915, 920, 917));
            arrayList.add(new w57(132, 130, 184, 920, 925, 922));
            arrayList.add(new w57(72, 72, 180, 925, 930, 927));
            arrayList.add(new w57(129, 110, 182, 930, 935, 932));
            arrayList.add(new w57(72, 72, 180, 935, 940, 937));
            arrayList.add(new w57(94, 64, 98, 940, 945, 942));
            arrayList.add(new w57(100, 67, 166, 945, 950, 947));
            arrayList.add(new w57(105, 100, DateTimeConstants.HOURS_PER_WEEK, 950, 955, 952));
            arrayList.add(new w57(94, 64, 98, 955, 960, 957));
            arrayList.add(new w57(72, 72, 180, 960, 965, 962));
            arrayList.add(new w57(84, 84, 157, 965, 970, 967));
            arrayList.add(new w57(80, 78, 155, 970, 975, 972));
            arrayList.add(new w57(82, 62, 156, 975, 980, 977));
            arrayList.add(new w57(129, 110, 182, 980, 985, 982));
            arrayList.add(new w57(69, 60, DateTimeConstants.HOURS_PER_WEEK, 985, 990, 987));
            arrayList.add(new w57(96, 90, 169, 990, 995, 992));
            arrayList.add(new w57(87, 80, 187, 995, 1000, 997));
            arrayList.add(new w57(129, 110, 182, 1000, 1005, 1002));
            arrayList.add(new w57(80, 50, 100, 1005, 1010, 1007));
            arrayList.add(new w57(193, 186, 108, 1010, 1015, 1012));
            arrayList.add(new w57(80, 50, 100, 1015, 1020, 1017));
            arrayList.add(new w57(170, 170, 197, 1020, 1025, 1022));
            arrayList.add(new w57(69, 60, DateTimeConstants.HOURS_PER_WEEK, 1025, 1030, 1027));
            arrayList.add(new w57(176, 70, 180, 1030, 1035, 1032));
            arrayList.add(new w57(175, 80, 175, 1035, 1040, 1037));
            arrayList.add(new w57(68, 60, 102, 1040, 1045, 1042));
            arrayList.add(new w57(80, 50, 100, 1045, 1050, 1047));
            arrayList.add(new w57(69, 60, DateTimeConstants.HOURS_PER_WEEK, 1050, 1055, 1052));
            arrayList.add(new w57(80, 50, 100, 1055, 1060, 1057));
            arrayList.add(new w57(69, 60, DateTimeConstants.HOURS_PER_WEEK, 1060, 1065, 1062));
            arrayList.add(new w57(78, 70, 87, 1065, 1070, 1067));
            arrayList.add(new w57(88, 88, 90, 1070, 1075, 1072));
            arrayList.add(new w57(88, 80, DateTimeConstants.HOURS_PER_WEEK, 1075, 1080, 1077));
            arrayList.add(new w57(90, 90, 184, 1080, 1085, 1082));
            arrayList.add(new w57(92, 80, 184, 1085, 1090, 1087));
            arrayList.add(new w57(94, 60, 182, 1090, 1095, 1092));
            arrayList.add(new w57(94, 66, 96, 1095, 1100, 1097));
            arrayList.add(new w57(69, 60, DateTimeConstants.HOURS_PER_WEEK, 1100, 1105, 1102));
            arrayList.add(new w57(124, 105, 124, 1105, 1110, 1107));
            arrayList.add(new w57(115, 100, 120, 1110, 1115, FailureCode.LOCATION_SERVICE_DISABLED));
            arrayList.add(new w57(180, 80, Action.Music.MUSIC_END_ACTION, 1115, 1120, 1117));
            arrayList.add(new w57(120, 90, 123, 1120, 1125, FailureCode.DEVICE_NOT_FOUND));
            arrayList.add(new w57(184, 170, 187, 1125, 1130, 1127));
            arrayList.add(new w57(80, 80, 155, 1130, 1135, 1132));
            arrayList.add(new w57(182, 90, 189, 1135, 1140, 1137));
            arrayList.add(new w57(152, 89, 166, 1140, 1145, 1142));
            arrayList.add(new w57(169, 90, 180, 1145, 1150, 1147));
            arrayList.add(new w57(88, 80, 90, 1150, 1155, 1152));
            arrayList.add(new w57(88, 70, 187, 1155, 1160, 1157));
            arrayList.add(new w57(70, 60, 186, 1160, 1165, 1162));
            if (!(a2 == null || (todayHeartRateChart3 = a2.s) == null)) {
                todayHeartRateChart3.setDayInMinuteWithTimeZone(DateTimeConstants.MINUTES_PER_DAY);
            }
            ArrayList arrayList2 = new ArrayList();
            if (!(a2 == null || (todayHeartRateChart2 = a2.s) == null)) {
                todayHeartRateChart2.setListTimeZoneChange(arrayList2);
            }
            if (a2 != null && (todayHeartRateChart = a2.s) != null) {
                todayHeartRateChart.m(arrayList);
                tl7 tl7 = tl7.f3441a;
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void N6(int i2) {
        g37<x45> g37 = this.i;
        if (g37 != null) {
            x45 a2 = g37.a();
            if (a2 != null) {
                a2.s.t("maxHeartRate", i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void O6(int i2) {
        g37<x45> g37 = this.i;
        if (g37 != null) {
            x45 a2 = g37.a();
            if (a2 != null) {
                a2.s.t("lowestHeartRate", i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void P6(int i2) {
        g37<x45> g37 = this.i;
        if (g37 != null) {
            x45 a2 = g37.a();
            if (a2 != null) {
                a2.y.setBackgroundColor(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void Q6(int i2) {
        g37<x45> g37 = this.i;
        if (g37 != null) {
            x45 a2 = g37.a();
            if (a2 != null) {
                a2.z.setBackgroundColor(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        FLogger.INSTANCE.getLocal().d(k, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363373) {
            ts6 ts6 = this.h;
            if (ts6 != null) {
                ts6.f(pt6.m.a(), l, m);
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        x45 x45 = (x45) aq0.f(LayoutInflater.from(getContext()), 2131558536, null, false, A6());
        PortfolioApp.h0.c().M().f(new ss6()).a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(ts6.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026artViewModel::class.java)");
            ts6 ts6 = (ts6) a2;
            this.h = ts6;
            if (ts6 != null) {
                ts6.e().h(getViewLifecycleOwner(), new b(this));
                ts6 ts62 = this.h;
                if (ts62 != null) {
                    ts62.g();
                    this.i = new g37<>(this, x45);
                    M6();
                    pq7.b(x45, "binding");
                    return x45.n();
                }
                pq7.n("mViewModel");
                throw null;
            }
            pq7.n("mViewModel");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(k, "onDestroy");
        l = null;
        m = null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(k, "onResume");
        ts6 ts6 = this.h;
        if (ts6 != null) {
            ts6.g();
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<x45> g37 = this.i;
        if (g37 != null) {
            x45 a2 = g37.a();
            if (a2 != null) {
                a2.u.setOnClickListener(new c(this));
                a2.v.setOnClickListener(new d(this));
                a2.t.setOnClickListener(new e(this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.x47
    public void q3(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
