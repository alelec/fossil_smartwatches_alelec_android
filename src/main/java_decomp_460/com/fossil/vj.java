package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vj extends qq7 implements rp7<fs, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ tl b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vj(tl tlVar) {
        super(1);
        this.b = tlVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(fs fsVar) {
        tl tlVar = this.b;
        se seVar = ((su) fsVar).L;
        tlVar.C = seVar;
        if (seVar == null) {
            tlVar.l(nr.a(tlVar.v, null, zq.FLOW_BROKEN, null, null, 13));
        } else if (te.f3398a.a(seVar, tlVar.E)) {
            tl tlVar2 = this.b;
            tlVar2.l(nr.a(tlVar2.v, null, zq.SUCCESS, null, null, 13));
        } else {
            tl tlVar3 = this.b;
            lp.i(tlVar3, new xu(tlVar3.E, tlVar3.w), new tk(tlVar3), new gl(tlVar3), null, null, null, 56, null);
        }
        return tl7.f3441a;
    }
}
