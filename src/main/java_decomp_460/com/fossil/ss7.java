package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ss7<T1, T2, V> implements ts7<V> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ts7<T1> f3299a;
    @DexIgnore
    public /* final */ ts7<T2> b;
    @DexIgnore
    public /* final */ vp7<T1, T2, V> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<V>, jr7 {
        @DexIgnore
        public /* final */ Iterator<T1> b;
        @DexIgnore
        public /* final */ Iterator<T2> c;
        @DexIgnore
        public /* final */ /* synthetic */ ss7 d;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(ss7 ss7) {
            this.d = ss7;
            this.b = ss7.f3299a.iterator();
            this.c = ss7.b.iterator();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext() && this.c.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public V next() {
            return (V) this.d.c.invoke(this.b.next(), this.c.next());
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.ts7<? extends T1> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.ts7<? extends T2> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.vp7<? super T1, ? super T2, ? extends V> */
    /* JADX WARN: Multi-variable type inference failed */
    public ss7(ts7<? extends T1> ts7, ts7<? extends T2> ts72, vp7<? super T1, ? super T2, ? extends V> vp7) {
        pq7.c(ts7, "sequence1");
        pq7.c(ts72, "sequence2");
        pq7.c(vp7, "transform");
        this.f3299a = ts7;
        this.b = ts72;
        this.c = vp7;
    }

    @DexIgnore
    @Override // com.fossil.ts7
    public Iterator<V> iterator() {
        return new a(this);
    }
}
