package com.fossil;

import android.annotation.SuppressLint;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sm5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public View f3281a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnTouchListener {
        @DexIgnore
        public long b;
        @DexIgnore
        public float c;
        @DexIgnore
        public /* final */ /* synthetic */ b d;
        @DexIgnore
        public /* final */ /* synthetic */ View e;

        @DexIgnore
        public a(sm5 sm5, b bVar, View view) {
            this.d = bVar;
            this.e = view;
        }

        @DexIgnore
        @SuppressLint({"ClickableViewAccessibility"})
        public boolean onTouch(View view, MotionEvent motionEvent) {
            boolean z = false;
            int action = motionEvent.getAction();
            if (action != 0) {
                if (action != 1) {
                    if (action != 2) {
                        if (action != 3) {
                            return false;
                        }
                    } else if (System.currentTimeMillis() - this.b <= 300) {
                        return true;
                    } else {
                        this.d.A0(this.e);
                        return true;
                    }
                }
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - this.b > 300) {
                    this.d.V0(this.e);
                } else {
                    this.d.onClick(this.e);
                }
                this.d.h5(this.e);
                float x = motionEvent.getX() - this.c;
                if (x < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    z = true;
                }
                float b2 = (float) (al5.a().b() / 3);
                float f = x / ((float) (currentTimeMillis - this.b));
                float f2 = x / 200.0f;
                if ((Math.abs(x) > ((float) (al5.a().b() / 10)) && Math.abs(f) > f2) || x > b2) {
                    this.d.N5(this.e, Boolean.valueOf(z));
                }
                this.c = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                this.b = 0;
                return true;
            }
            this.b = System.currentTimeMillis();
            this.d.v4(this.e);
            this.c = motionEvent.getX();
            return true;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void A0(View view);

        @DexIgnore
        void N5(View view, Boolean bool);

        @DexIgnore
        void V0(View view);

        @DexIgnore
        void h5(View view);

        @DexIgnore
        void onClick(View view);

        @DexIgnore
        void v4(View view);
    }

    @DexIgnore
    public void a(View view, b bVar) {
        this.f3281a = view;
        view.setOnTouchListener(new a(this, bVar, view));
    }
}
