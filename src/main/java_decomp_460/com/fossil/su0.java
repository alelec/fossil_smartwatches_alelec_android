package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.kv0;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class su0 implements kv0.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public mn0<b> f3306a;
    @DexIgnore
    public /* final */ ArrayList<b> b;
    @DexIgnore
    public /* final */ ArrayList<b> c;
    @DexIgnore
    public /* final */ a d;
    @DexIgnore
    public Runnable e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ kv0 g;
    @DexIgnore
    public int h;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(int i, int i2);

        @DexIgnore
        void b(b bVar);

        @DexIgnore
        void c(int i, int i2, Object obj);

        @DexIgnore
        void d(b bVar);

        @DexIgnore
        RecyclerView.ViewHolder e(int i);

        @DexIgnore
        void f(int i, int i2);

        @DexIgnore
        void g(int i, int i2);

        @DexIgnore
        void h(int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f3307a;
        @DexIgnore
        public int b;
        @DexIgnore
        public Object c;
        @DexIgnore
        public int d;

        @DexIgnore
        public b(int i, int i2, int i3, Object obj) {
            this.f3307a = i;
            this.b = i2;
            this.d = i3;
            this.c = obj;
        }

        @DexIgnore
        public String a() {
            int i = this.f3307a;
            return i != 1 ? i != 2 ? i != 4 ? i != 8 ? "Object" : "mv" : "up" : "rm" : "add";
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            int i = this.f3307a;
            if (i != bVar.f3307a) {
                return false;
            }
            if (i == 8 && Math.abs(this.d - this.b) == 1 && this.d == bVar.b && this.b == bVar.d) {
                return true;
            }
            if (this.d != bVar.d) {
                return false;
            }
            if (this.b != bVar.b) {
                return false;
            }
            Object obj2 = this.c;
            return obj2 != null ? obj2.equals(bVar.c) : bVar.c == null;
        }

        @DexIgnore
        public int hashCode() {
            return (((this.f3307a * 31) + this.b) * 31) + this.d;
        }

        @DexIgnore
        public String toString() {
            return Integer.toHexString(System.identityHashCode(this)) + "[" + a() + ",s:" + this.b + "c:" + this.d + ",p:" + this.c + "]";
        }
    }

    @DexIgnore
    public su0(a aVar) {
        this(aVar, false);
    }

    @DexIgnore
    public su0(a aVar, boolean z) {
        this.f3306a = new nn0(30);
        this.b = new ArrayList<>();
        this.c = new ArrayList<>();
        this.h = 0;
        this.d = aVar;
        this.f = z;
        this.g = new kv0(this);
    }

    @DexIgnore
    @Override // com.fossil.kv0.a
    public void a(b bVar) {
        if (!this.f) {
            bVar.c = null;
            this.f3306a.a(bVar);
        }
    }

    @DexIgnore
    @Override // com.fossil.kv0.a
    public b b(int i, int i2, int i3, Object obj) {
        b b2 = this.f3306a.b();
        if (b2 == null) {
            return new b(i, i2, i3, obj);
        }
        b2.f3307a = i;
        b2.b = i2;
        b2.d = i3;
        b2.c = obj;
        return b2;
    }

    @DexIgnore
    public final void c(b bVar) {
        v(bVar);
    }

    @DexIgnore
    public final void d(b bVar) {
        v(bVar);
    }

    @DexIgnore
    public int e(int i) {
        int size = this.b.size();
        int i2 = i;
        for (int i3 = 0; i3 < size; i3++) {
            b bVar = this.b.get(i3);
            int i4 = bVar.f3307a;
            if (i4 != 1) {
                if (i4 == 2) {
                    int i5 = bVar.b;
                    if (i5 <= i2) {
                        int i6 = bVar.d;
                        if (i5 + i6 > i2) {
                            return -1;
                        }
                        i2 -= i6;
                    } else {
                        continue;
                    }
                } else if (i4 == 8) {
                    int i7 = bVar.b;
                    if (i7 == i2) {
                        i2 = bVar.d;
                    } else {
                        if (i7 < i2) {
                            i2--;
                        }
                        if (bVar.d <= i2) {
                            i2++;
                        }
                    }
                }
            } else if (bVar.b <= i2) {
                i2 += bVar.d;
            }
        }
        return i2;
    }

    @DexIgnore
    public final void f(b bVar) {
        boolean z;
        int i;
        int i2 = bVar.b;
        int i3 = bVar.d + i2;
        char c2 = '\uffff';
        int i4 = 0;
        int i5 = i2;
        while (i5 < i3) {
            if (this.d.e(i5) != null || h(i5)) {
                if (c2 == 0) {
                    k(b(2, i2, i4, null));
                    z = true;
                } else {
                    z = false;
                }
                c2 = 1;
            } else {
                if (c2 == 1) {
                    v(b(2, i2, i4, null));
                    z = true;
                } else {
                    z = false;
                }
                c2 = 0;
            }
            if (z) {
                i5 -= i4;
                i3 -= i4;
                i = 1;
            } else {
                i = i4 + 1;
            }
            i5++;
            i4 = i;
        }
        if (i4 != bVar.d) {
            a(bVar);
            bVar = b(2, i2, i4, null);
        }
        if (c2 == 0) {
            k(bVar);
        } else {
            v(bVar);
        }
    }

    @DexIgnore
    public final void g(b bVar) {
        int i = bVar.b;
        int i2 = bVar.d;
        char c2 = '\uffff';
        int i3 = 0;
        int i4 = i;
        for (int i5 = i; i5 < i2 + i; i5++) {
            if (this.d.e(i5) != null || h(i5)) {
                if (c2 == 0) {
                    k(b(4, i4, i3, bVar.c));
                    i4 = i5;
                    i3 = 0;
                }
                c2 = 1;
            } else {
                if (c2 == 1) {
                    v(b(4, i4, i3, bVar.c));
                    i3 = 0;
                    i4 = i5;
                }
                c2 = 0;
            }
            i3++;
        }
        if (i3 != bVar.d) {
            Object obj = bVar.c;
            a(bVar);
            bVar = b(4, i4, i3, obj);
        }
        if (c2 == 0) {
            k(bVar);
        } else {
            v(bVar);
        }
    }

    @DexIgnore
    public final boolean h(int i) {
        int size = this.c.size();
        for (int i2 = 0; i2 < size; i2++) {
            b bVar = this.c.get(i2);
            int i3 = bVar.f3307a;
            if (i3 == 8) {
                if (n(bVar.d, i2 + 1) == i) {
                    return true;
                }
            } else if (i3 == 1) {
                int i4 = bVar.b;
                int i5 = bVar.d;
                for (int i6 = i4; i6 < i5 + i4; i6++) {
                    if (n(i6, i2 + 1) == i) {
                        return true;
                    }
                }
                continue;
            } else {
                continue;
            }
        }
        return false;
    }

    @DexIgnore
    public void i() {
        int size = this.c.size();
        for (int i = 0; i < size; i++) {
            this.d.d(this.c.get(i));
        }
        x(this.c);
        this.h = 0;
    }

    @DexIgnore
    public void j() {
        i();
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            b bVar = this.b.get(i);
            int i2 = bVar.f3307a;
            if (i2 == 1) {
                this.d.d(bVar);
                this.d.g(bVar.b, bVar.d);
            } else if (i2 == 2) {
                this.d.d(bVar);
                this.d.h(bVar.b, bVar.d);
            } else if (i2 == 4) {
                this.d.d(bVar);
                this.d.c(bVar.b, bVar.d, bVar.c);
            } else if (i2 == 8) {
                this.d.d(bVar);
                this.d.a(bVar.b, bVar.d);
            }
            Runnable runnable = this.e;
            if (runnable != null) {
                runnable.run();
            }
        }
        x(this.b);
        this.h = 0;
    }

    @DexIgnore
    public final void k(b bVar) {
        int i;
        int i2 = bVar.f3307a;
        if (i2 == 1 || i2 == 8) {
            throw new IllegalArgumentException("should not dispatch add or move for pre layout");
        }
        int z = z(bVar.b, i2);
        int i3 = bVar.b;
        int i4 = bVar.f3307a;
        if (i4 == 2) {
            i = 0;
        } else if (i4 == 4) {
            i = 1;
        } else {
            throw new IllegalArgumentException("op should be remove or update." + bVar);
        }
        int i5 = 1;
        for (int i6 = 1; i6 < bVar.d; i6++) {
            int z2 = z(bVar.b + (i * i6), bVar.f3307a);
            int i7 = bVar.f3307a;
            if (i7 == 2 ? z2 == z : i7 == 4 && z2 == z + 1) {
                i5++;
            } else {
                b b2 = b(bVar.f3307a, z, i5, bVar.c);
                l(b2, i3);
                a(b2);
                if (bVar.f3307a == 4) {
                    i3 += i5;
                }
                i5 = 1;
                z = z2;
            }
        }
        Object obj = bVar.c;
        a(bVar);
        if (i5 > 0) {
            b b3 = b(bVar.f3307a, z, i5, obj);
            l(b3, i3);
            a(b3);
        }
    }

    @DexIgnore
    public void l(b bVar, int i) {
        this.d.b(bVar);
        int i2 = bVar.f3307a;
        if (i2 == 2) {
            this.d.h(i, bVar.d);
        } else if (i2 == 4) {
            this.d.c(i, bVar.d, bVar.c);
        } else {
            throw new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
        }
    }

    @DexIgnore
    public int m(int i) {
        return n(i, 0);
    }

    @DexIgnore
    public int n(int i, int i2) {
        int size = this.c.size();
        int i3 = i;
        while (i2 < size) {
            b bVar = this.c.get(i2);
            int i4 = bVar.f3307a;
            if (i4 == 8) {
                int i5 = bVar.b;
                if (i5 == i3) {
                    i3 = bVar.d;
                } else {
                    if (i5 < i3) {
                        i3--;
                    }
                    if (bVar.d <= i3) {
                        i3++;
                    }
                }
            } else {
                int i6 = bVar.b;
                if (i6 > i3) {
                    continue;
                } else if (i4 == 2) {
                    int i7 = bVar.d;
                    if (i3 < i6 + i7) {
                        return -1;
                    }
                    i3 -= i7;
                } else if (i4 == 1) {
                    i3 += bVar.d;
                }
            }
            i2++;
        }
        return i3;
    }

    @DexIgnore
    public boolean o(int i) {
        return (this.h & i) != 0;
    }

    @DexIgnore
    public boolean p() {
        return this.b.size() > 0;
    }

    @DexIgnore
    public boolean q() {
        return !this.c.isEmpty() && !this.b.isEmpty();
    }

    @DexIgnore
    public boolean r(int i, int i2, Object obj) {
        boolean z = true;
        if (i2 < 1) {
            return false;
        }
        this.b.add(b(4, i, i2, obj));
        this.h |= 4;
        if (this.b.size() != 1) {
            z = false;
        }
        return z;
    }

    @DexIgnore
    public boolean s(int i, int i2) {
        boolean z = true;
        if (i2 < 1) {
            return false;
        }
        this.b.add(b(1, i, i2, null));
        this.h |= 1;
        if (this.b.size() != 1) {
            z = false;
        }
        return z;
    }

    @DexIgnore
    public boolean t(int i, int i2, int i3) {
        boolean z = true;
        if (i == i2) {
            return false;
        }
        if (i3 == 1) {
            this.b.add(b(8, i, i2, null));
            this.h |= 8;
            if (this.b.size() != 1) {
                z = false;
            }
            return z;
        }
        throw new IllegalArgumentException("Moving more than 1 item is not supported yet");
    }

    @DexIgnore
    public boolean u(int i, int i2) {
        boolean z = true;
        if (i2 < 1) {
            return false;
        }
        this.b.add(b(2, i, i2, null));
        this.h |= 2;
        if (this.b.size() != 1) {
            z = false;
        }
        return z;
    }

    @DexIgnore
    public final void v(b bVar) {
        this.c.add(bVar);
        int i = bVar.f3307a;
        if (i == 1) {
            this.d.g(bVar.b, bVar.d);
        } else if (i == 2) {
            this.d.f(bVar.b, bVar.d);
        } else if (i == 4) {
            this.d.c(bVar.b, bVar.d, bVar.c);
        } else if (i == 8) {
            this.d.a(bVar.b, bVar.d);
        } else {
            throw new IllegalArgumentException("Unknown update op type for " + bVar);
        }
    }

    @DexIgnore
    public void w() {
        this.g.b(this.b);
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            b bVar = this.b.get(i);
            int i2 = bVar.f3307a;
            if (i2 == 1) {
                c(bVar);
            } else if (i2 == 2) {
                f(bVar);
            } else if (i2 == 4) {
                g(bVar);
            } else if (i2 == 8) {
                d(bVar);
            }
            Runnable runnable = this.e;
            if (runnable != null) {
                runnable.run();
            }
        }
        this.b.clear();
    }

    @DexIgnore
    public void x(List<b> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            a(list.get(i));
        }
        list.clear();
    }

    @DexIgnore
    public void y() {
        x(this.b);
        x(this.c);
        this.h = 0;
    }

    @DexIgnore
    public final int z(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6 = i;
        for (int size = this.c.size() - 1; size >= 0; size--) {
            b bVar = this.c.get(size);
            int i7 = bVar.f3307a;
            if (i7 == 8) {
                int i8 = bVar.b;
                int i9 = bVar.d;
                if (i8 < i9) {
                    i4 = i8;
                    i5 = i9;
                } else {
                    i4 = i9;
                    i5 = i8;
                }
                if (i6 < i4 || i6 > i5) {
                    int i10 = bVar.b;
                    if (i6 < i10) {
                        if (i2 == 1) {
                            bVar.b = i10 + 1;
                            bVar.d++;
                            i3 = i6;
                        } else if (i2 == 2) {
                            bVar.b = i10 - 1;
                            bVar.d--;
                            i3 = i6;
                        }
                    }
                    i3 = i6;
                } else {
                    int i11 = bVar.b;
                    if (i4 == i11) {
                        if (i2 == 1) {
                            bVar.d++;
                        } else if (i2 == 2) {
                            bVar.d--;
                        }
                        i3 = i6 + 1;
                    } else {
                        if (i2 == 1) {
                            bVar.b = i11 + 1;
                        } else if (i2 == 2) {
                            bVar.b = i11 - 1;
                        }
                        i3 = i6 - 1;
                    }
                }
            } else {
                int i12 = bVar.b;
                if (i12 <= i6) {
                    if (i7 == 1) {
                        i3 = i6 - bVar.d;
                    } else {
                        if (i7 == 2) {
                            i3 = bVar.d + i6;
                        }
                        i3 = i6;
                    }
                } else if (i2 == 1) {
                    bVar.b = i12 + 1;
                    i3 = i6;
                } else {
                    if (i2 == 2) {
                        bVar.b = i12 - 1;
                        i3 = i6;
                    }
                    i3 = i6;
                }
            }
            i6 = i3;
        }
        for (int size2 = this.c.size() - 1; size2 >= 0; size2--) {
            b bVar2 = this.c.get(size2);
            if (bVar2.f3307a == 8) {
                int i13 = bVar2.d;
                if (i13 == bVar2.b || i13 < 0) {
                    this.c.remove(size2);
                    a(bVar2);
                }
            } else if (bVar2.d <= 0) {
                this.c.remove(size2);
                a(bVar2);
            }
        }
        return i6;
    }
}
