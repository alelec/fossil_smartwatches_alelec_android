package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oe extends qq7 implements rp7<lp, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ yp[] b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public oe(yp[] ypVarArr) {
        super(1);
        this.b = ypVarArr;
    }

    @DexIgnore
    public final boolean a(lp lpVar) {
        return !em7.B(this.b, lpVar.y);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ Boolean invoke(lp lpVar) {
        return Boolean.valueOf(a(lpVar));
    }
}
