package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rs3 implements Parcelable.Creator<ss3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ss3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        sc2 sc2 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                i = ad2.v(parcel, t);
            } else if (l != 2) {
                ad2.B(parcel, t);
            } else {
                sc2 = (sc2) ad2.e(parcel, t, sc2.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new ss3(i, sc2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ss3[] newArray(int i) {
        return new ss3[i];
    }
}
