package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mf7 extends df7 {
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;

    @DexIgnore
    @Override // com.fossil.df7
    public boolean a() {
        String str;
        String str2 = this.c;
        if (str2 == null || str2.length() == 0 || this.c.length() > 1024) {
            str = "checkArgs fail, scope is invalid";
        } else {
            String str3 = this.d;
            if (str3 == null || str3.length() <= 1024) {
                return true;
            }
            str = "checkArgs fail, state is invalid";
        }
        ye7.b("MicroMsg.SDK.SendAuth.Req", str);
        return false;
    }

    @DexIgnore
    @Override // com.fossil.df7
    public int c() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.df7
    public void d(Bundle bundle) {
        super.d(bundle);
        bundle.putString("_wxapi_sendauth_req_scope", this.c);
        bundle.putString("_wxapi_sendauth_req_state", this.d);
    }
}
