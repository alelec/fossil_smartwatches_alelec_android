package com.fossil;

import android.view.View;
import android.webkit.WebView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class g15 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ WebView s;

    @DexIgnore
    public g15(Object obj, View view, int i, RTLImageView rTLImageView, FlexibleTextView flexibleTextView, WebView webView) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = flexibleTextView;
        this.s = webView;
    }
}
