package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zt1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public xb d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<zt1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public zt1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(sq1.class.getClassLoader());
            if (readParcelable != null) {
                pq7.b(readParcelable, "parcel.readParcelable<Wo\u2026class.java.classLoader)!!");
                return new zt1((sq1) readParcelable, (nt1) parcel.readParcelable(nt1.class.getClassLoader()), (xb) parcel.readParcelable(xb.class.getClassLoader()));
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public zt1[] newArray(int i) {
            return new zt1[i];
        }
    }

    @DexIgnore
    public zt1(sq1 sq1, nt1 nt1, xb xbVar) {
        super(sq1, nt1);
        this.d = xbVar;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public JSONObject a() {
        JSONObject jSONObject;
        JSONObject a2 = super.a();
        xb xbVar = this.d;
        if (xbVar == null || (jSONObject = xbVar.toJSONObject()) == null) {
            jSONObject = new JSONObject();
        }
        return gy1.c(a2, jSONObject);
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        jq1 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            xb xbVar = this.d;
            if (xbVar != null) {
                jSONObject.put("workoutApp._.config.gps", xbVar.toJSONObject());
            }
            if (getDeviceMessage() != null) {
                jSONObject.put("workoutApp._.config.response", getDeviceMessage().toJSONObject());
            }
        } catch (JSONException e) {
            d90.i.i(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        pq7.b(jSONObject4, "deviceResponseJSONObject.toString()");
        Charset c = hd0.y.c();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(c);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(zt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.d, ((zt1) obj).d) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.WorkoutGPSDistanceData");
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public int hashCode() {
        int hashCode = super.hashCode();
        xb xbVar = this.d;
        return (xbVar != null ? xbVar.hashCode() : 0) + (hashCode * 31);
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }
}
