package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.watchface.MetaData;
import com.portfolio.platform.data.source.FileRepository;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dc7 {
    @DexIgnore
    public static final fb7 a(p77 p77, FileRepository fileRepository) {
        BitmapDrawable bitmapDrawable;
        String str;
        File fileByRemoteUrl;
        byte[] b;
        Bitmap c;
        pq7.c(p77, "$this$toBackgroundTemplate");
        if (fileRepository == null || (fileByRemoteUrl = fileRepository.getFileByRemoteUrl(p77.c().a())) == null || (b = j37.b(fileByRemoteUrl)) == null || (c = j37.c(b)) == null) {
            bitmapDrawable = null;
        } else {
            Resources resources = PortfolioApp.h0.c().getResources();
            pq7.b(resources, "PortfolioApp.instance.resources");
            bitmapDrawable = new BitmapDrawable(resources, c);
        }
        String d = p77.d();
        String f = p77.f();
        String a2 = p77.c().a();
        String b2 = p77.c().b();
        l77 a3 = p77.a();
        MetaData e = p77.e();
        if (e == null || (str = e.getChecksum()) == null) {
            str = "";
        }
        return new fb7(d, f, a2, b2, a3, str, bitmapDrawable, null, null);
    }

    @DexIgnore
    public static final List<fb7> b(List<p77> list, FileRepository fileRepository) {
        pq7.c(list, "$this$toBackgroundTemplates");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(a(it.next(), fileRepository));
        }
        return arrayList;
    }

    @DexIgnore
    public static /* synthetic */ List c(List list, FileRepository fileRepository, int i, Object obj) {
        if ((i & 1) != 0) {
            fileRepository = null;
        }
        return b(list, fileRepository);
    }

    @DexIgnore
    public static final fb7 d(p77 p77) {
        pq7.c(p77, "$this$toRing");
        return new fb7(p77.d(), p77.f(), p77.c().a(), p77.c().b(), l77.RING_COMPLICATION, "", null, null, null);
    }

    @DexIgnore
    public static final List<fb7> e(List<p77> list) {
        pq7.c(list, "$this$toRings");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(d(it.next()));
        }
        return arrayList;
    }

    @DexIgnore
    public static final fb7 f(p77 p77) {
        String str;
        pq7.c(p77, "$this$toTicker");
        String d = p77.d();
        String f = p77.f();
        String a2 = p77.c().a();
        String b = p77.c().b();
        l77 a3 = p77.a();
        MetaData e = p77.e();
        if (e == null || (str = e.getChecksum()) == null) {
            str = "";
        }
        k77[] values = k77.values();
        for (k77 k77 : values) {
            int id = k77.getId();
            MetaData e2 = p77.e();
            Integer colourId = e2 != null ? e2.getColourId() : null;
            if (colourId != null && id == colourId.intValue()) {
                return new fb7(d, f, a2, b, a3, str, null, k77, null);
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }

    @DexIgnore
    public static final List<fb7> g(List<p77> list) {
        pq7.c(list, "$this$toTickers");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(f(it.next()));
        }
        return arrayList;
    }
}
