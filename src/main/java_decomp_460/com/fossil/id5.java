package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class id5 extends hd5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y;
    @DexIgnore
    public long w;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        y = sparseIntArray;
        sparseIntArray.put(2131362726, 1);
        y.put(2131362546, 2);
        y.put(2131362386, 3);
        y.put(2131362700, 4);
        y.put(2131362783, 5);
    }
    */

    @DexIgnore
    public id5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 6, x, y));
    }

    @DexIgnore
    public id5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[2], (ImageButton) objArr[4], (ImageView) objArr[1], (View) objArr[5]);
        this.w = -1;
        this.q.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.w != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.w = 1;
        }
        w();
    }
}
