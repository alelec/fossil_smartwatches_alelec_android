package com.fossil;

import android.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class be7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f421a;
    @DexIgnore
    public /* final */ List<Object> b;

    @DexIgnore
    public be7(String str, List<Object> list) {
        this.f421a = str;
        this.b = list == null ? new ArrayList<>() : list;
    }

    @DexIgnore
    public static Map<String, Object> a(Map<Object, Object> map) {
        HashMap hashMap = new HashMap();
        for (Map.Entry<Object, Object> entry : map.entrySet()) {
            Object value = entry.getValue();
            hashMap.put(j(entry.getKey()), value instanceof Map ? a((Map) value) : j(value));
        }
        return hashMap;
    }

    @DexIgnore
    public static String j(Object obj) {
        if (obj == null) {
            return null;
        }
        if (!(obj instanceof byte[])) {
            return obj instanceof Map ? a((Map) obj).toString() : obj.toString();
        }
        ArrayList arrayList = new ArrayList();
        for (byte b2 : (byte[]) obj) {
            arrayList.add(Integer.valueOf(b2));
        }
        return arrayList.toString();
    }

    @DexIgnore
    public static Object k(Object obj) {
        if (obj == null) {
            return null;
        }
        if (ce7.c) {
            Log.d("Sqflite", "arg " + obj.getClass().getCanonicalName() + " " + j(obj));
        }
        if (obj instanceof List) {
            List list = (List) obj;
            byte[] bArr = new byte[list.size()];
            for (int i = 0; i < list.size(); i++) {
                bArr[i] = (byte) ((byte) ((Integer) list.get(i)).intValue());
            }
            obj = bArr;
        }
        if (!ce7.c) {
            return obj;
        }
        Log.d("Sqflite", "arg " + obj.getClass().getCanonicalName() + " " + j(obj));
        return obj;
    }

    @DexIgnore
    public String[] b() {
        return c(this.b);
    }

    @DexIgnore
    public final String[] c(List<Object> list) {
        return (String[]) h(list).toArray(new String[0]);
    }

    @DexIgnore
    public List<Object> d() {
        return this.b;
    }

    @DexIgnore
    public String e() {
        return this.f421a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof be7)) {
            return false;
        }
        be7 be7 = (be7) obj;
        String str = this.f421a;
        if (str != null) {
            if (!str.equals(be7.f421a)) {
                return false;
            }
        } else if (be7.f421a != null) {
            return false;
        }
        if (this.b.size() != be7.b.size()) {
            return false;
        }
        for (int i = 0; i < this.b.size(); i++) {
            if (!(this.b.get(i) instanceof byte[]) || !(be7.b.get(i) instanceof byte[])) {
                if (!this.b.get(i).equals(be7.b.get(i))) {
                    return false;
                }
            } else if (!Arrays.equals((byte[]) this.b.get(i), (byte[]) be7.b.get(i))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public Object[] f() {
        return g(this.b);
    }

    @DexIgnore
    public final Object[] g(List<Object> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            for (Object obj : list) {
                arrayList.add(k(obj));
            }
        }
        return arrayList.toArray(new Object[0]);
    }

    @DexIgnore
    public final List<String> h(List<Object> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            for (Object obj : list) {
                arrayList.add(j(obj));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.f421a;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public be7 i() {
        int i;
        if (this.b.size() == 0) {
            return this;
        }
        StringBuilder sb = new StringBuilder();
        ArrayList arrayList = new ArrayList();
        int length = this.f421a.length();
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i4 < length) {
            char charAt = this.f421a.charAt(i4);
            if (charAt == '?') {
                int i5 = i4 + 1;
                if (i5 < length && Character.isDigit(this.f421a.charAt(i5))) {
                    return this;
                }
                i3++;
                if (i2 >= this.b.size()) {
                    return this;
                }
                i = i2 + 1;
                Object obj = this.b.get(i2);
                if ((obj instanceof Integer) || (obj instanceof Long)) {
                    sb.append(obj.toString());
                } else {
                    arrayList.add(obj);
                    sb.append(charAt);
                }
            } else {
                i = i2;
                sb.append(charAt);
            }
            i4++;
            i2 = i;
        }
        return i3 == this.b.size() ? new be7(sb.toString(), arrayList) : this;
    }

    @DexIgnore
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append(this.f421a);
        List<Object> list = this.b;
        if (list == null || list.isEmpty()) {
            str = "";
        } else {
            str = " " + h(this.b);
        }
        sb.append(str);
        return sb.toString();
    }
}
