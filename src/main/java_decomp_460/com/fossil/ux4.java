package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.dz4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.customview.InterceptSwipe;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ux4 extends pv5 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public g37<ta5> h;
    @DexIgnore
    public zx4 i;
    @DexIgnore
    public wx4 j;
    @DexIgnore
    public TimerViewObserver k; // = new TimerViewObserver();
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ux4.m;
        }

        @DexIgnore
        public final ux4 b() {
            return new ux4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements InterceptSwipe.j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ta5 f3660a;
        @DexIgnore
        public /* final */ /* synthetic */ ux4 b;

        @DexIgnore
        public b(ta5 ta5, ux4 ux4) {
            this.f3660a = ta5;
            this.b = ux4;
        }

        @DexIgnore
        @Override // com.portfolio.platform.uirenew.customview.InterceptSwipe.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.f3660a.r;
            pq7.b(flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            FlexibleTextView flexibleTextView2 = this.f3660a.q;
            pq7.b(flexibleTextView2, "ftvEmpty");
            flexibleTextView2.setVisibility(8);
            ux4.O6(this.b).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements dz4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ux4 f3661a;

        @DexIgnore
        public c(ux4 ux4) {
            this.f3661a = ux4;
        }

        @DexIgnore
        @Override // com.fossil.dz4.b
        public void a(View view, int i) {
            pq7.c(view, "view");
            ux4.L6(this.f3661a).g(i);
            Object h = ux4.L6(this.f3661a).h(i);
            if (!(h instanceof mt4)) {
                h = null;
            }
            mt4 mt4 = (mt4) h;
            if (mt4 != null) {
                ux4.O6(this.f3661a).l(mt4, i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ux4 f3662a;

        @DexIgnore
        public d(ux4 ux4) {
            this.f3662a = ux4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ta5 ta5 = (ta5) ux4.K6(this.f3662a).a();
            if (ta5 != null) {
                InterceptSwipe interceptSwipe = ta5.u;
                pq7.b(interceptSwipe, "swipeRefresh");
                pq7.b(bool, "it");
                interceptSwipe.setRefreshing(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ux4 f3663a;

        @DexIgnore
        public e(ux4 ux4) {
            this.f3663a = ux4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ux4 ux4 = this.f3663a;
            pq7.b(bool, "it");
            ux4.V6(bool.booleanValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<List<? extends Object>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ux4 f3664a;

        @DexIgnore
        public f(ux4 ux4) {
            this.f3664a = ux4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<? extends Object> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ux4.s.a();
            local.e(a2, "recommendedLive: " + list);
            this.f3664a.k.a();
            zx4 L6 = ux4.L6(this.f3664a);
            pq7.b(list, "it");
            L6.j(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<cl7<? extends mt4, ? extends Integer>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ux4 f3665a;

        @DexIgnore
        public g(ux4 ux4) {
            this.f3665a = ux4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<mt4, Integer> cl7) {
            this.f3665a.T6(cl7.getFirst().b(), cl7.getFirst().a(), cl7.getSecond().intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ls0<cl7<? extends Boolean, ? extends ServerError>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ux4 f3666a;

        @DexIgnore
        public h(ux4 ux4) {
            this.f3666a = ux4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, ? extends ServerError> cl7) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ux4.s.a();
            local.e(a2, "errorLive: - pair: " + cl7);
            ta5 ta5 = (ta5) ux4.K6(this.f3666a).a();
            if (ta5 != null) {
                boolean booleanValue = cl7.getFirst().booleanValue();
                ServerError serverError = (ServerError) cl7.getSecond();
                if (booleanValue) {
                    FlexibleTextView flexibleTextView = ta5.r;
                    pq7.b(flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView2 = ta5.r;
                pq7.b(flexibleTextView2, "ftvError");
                String c = um5.c(flexibleTextView2.getContext(), 2131886231);
                FragmentActivity activity = this.f3666a.getActivity();
                if (activity != null) {
                    Toast.makeText(activity, c, 1).show();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ls0<Integer> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ux4 f3667a;

        @DexIgnore
        public i(ux4 ux4) {
            this.f3667a = ux4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Integer num) {
            if (ux4.L6(this.f3667a).getItemCount() == 2) {
                this.f3667a.V6(true);
            }
            zx4 L6 = ux4.L6(this.f3667a);
            pq7.b(num, "it");
            L6.i(num.intValue());
        }
    }

    /*
    static {
        String name = ux4.class.getName();
        pq7.b(name, "BCRecommendationSubTabFragment::class.java.name");
        m = name;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 K6(ux4 ux4) {
        g37<ta5> g37 = ux4.h;
        if (g37 != null) {
            return g37;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ zx4 L6(ux4 ux4) {
        zx4 zx4 = ux4.i;
        if (zx4 != null) {
            return zx4;
        }
        pq7.n("recommendedAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ wx4 O6(ux4 ux4) {
        wx4 wx4 = ux4.j;
        if (wx4 != null) {
            return wx4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void R6() {
        zx4 zx4 = new zx4();
        this.i = zx4;
        if (zx4 != null) {
            zx4.k(this.k);
            g37<ta5> g37 = this.h;
            if (g37 != null) {
                ta5 a2 = g37.a();
                if (a2 != null) {
                    RecyclerView recyclerView = a2.s;
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                    zx4 zx42 = this.i;
                    if (zx42 != null) {
                        recyclerView.setAdapter(zx42);
                        a2.u.setOnRefreshListener(new b(a2, this));
                        RecyclerView recyclerView2 = a2.s;
                        Context requireContext = requireContext();
                        pq7.b(requireContext, "requireContext()");
                        recyclerView2.addOnItemTouchListener(new dz4(requireContext, new c(this)));
                        return;
                    }
                    pq7.n("recommendedAdapter");
                    throw null;
                }
                return;
            }
            pq7.n("binding");
            throw null;
        }
        pq7.n("recommendedAdapter");
        throw null;
    }

    @DexIgnore
    public final void S6() {
        wx4 wx4 = this.j;
        if (wx4 != null) {
            wx4.e().h(getViewLifecycleOwner(), new d(this));
            wx4 wx42 = this.j;
            if (wx42 != null) {
                wx42.c().h(getViewLifecycleOwner(), new e(this));
                wx4 wx43 = this.j;
                if (wx43 != null) {
                    wx43.f().h(getViewLifecycleOwner(), new f(this));
                    wx4 wx44 = this.j;
                    if (wx44 != null) {
                        wx44.g().h(getViewLifecycleOwner(), new g(this));
                        wx4 wx45 = this.j;
                        if (wx45 != null) {
                            wx45.d().h(getViewLifecycleOwner(), new h(this));
                            wx4 wx46 = this.j;
                            if (wx46 != null) {
                                wx46.h().h(getViewLifecycleOwner(), new i(this));
                            } else {
                                pq7.n("viewModel");
                                throw null;
                            }
                        } else {
                            pq7.n("viewModel");
                            throw null;
                        }
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void T6(ps4 ps4, String str, int i2) {
        BCWaitingChallengeDetailActivity.a aVar = BCWaitingChallengeDetailActivity.B;
        Fragment requireParentFragment = requireParentFragment();
        pq7.b(requireParentFragment, "requireParentFragment()");
        aVar.a(requireParentFragment, ps4, str, i2, false);
    }

    @DexIgnore
    public final void U6(int i2) {
        wx4 wx4 = this.j;
        if (wx4 != null) {
            wx4.m(i2);
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void V6(boolean z) {
        g37<ta5> g37 = this.h;
        if (g37 != null) {
            ta5 a2 = g37.a();
            if (a2 != null) {
                InterceptSwipe interceptSwipe = a2.u;
                pq7.b(interceptSwipe, "swipeRefresh");
                interceptSwipe.setRefreshing(false);
                if (z) {
                    FlexibleTextView flexibleTextView = a2.q;
                    pq7.b(flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(0);
                    RecyclerView recyclerView = a2.s;
                    pq7.b(recyclerView, "rcvRecommended");
                    recyclerView.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView2 = a2.q;
                pq7.b(flexibleTextView2, "ftvEmpty");
                flexibleTextView2.setVisibility(8);
                RecyclerView recyclerView2 = a2.s;
                pq7.b(recyclerView2, "rcvRecommended");
                recyclerView2.setVisibility(0);
                return;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().b1().a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(wx4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ionViewModel::class.java)");
            this.j = (wx4) a2;
            getLifecycle().c(this.k);
            getLifecycle().a(this.k);
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        ta5 ta5 = (ta5) aq0.f(layoutInflater, 2131558614, viewGroup, false, A6());
        this.h = new g37<>(this, ta5);
        pq7.b(ta5, "bd");
        return ta5.n();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        getLifecycle().c(this.k);
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        wx4 wx4 = this.j;
        if (wx4 != null) {
            wx4.i();
            R6();
            S6();
            return;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
