package com.fossil;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.ph2;
import com.fossil.r62;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uk5 {
    @DexIgnore
    public static /* final */ String c; // = "uk5";
    @DexIgnore
    public static r62 d;
    @DexIgnore
    public static d e;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public on5 f3609a;
    @DexIgnore
    public /* final */ Executor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements r62.c {
        @DexIgnore
        public a(uk5 uk5) {
        }

        @DexIgnore
        @Override // com.fossil.r72
        public void n(z52 z52) {
            d dVar = uk5.e;
            if (dVar != null) {
                dVar.c(z52);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements r62.b {
        @DexIgnore
        public b(uk5 uk5) {
        }

        @DexIgnore
        @Override // com.fossil.k72
        public void d(int i) {
            if (i == 2) {
                FLogger.INSTANCE.getLocal().d(uk5.c, "Connection lost.  Cause: Network Lost.");
            } else if (i == 1) {
                FLogger.INSTANCE.getLocal().d(uk5.c, "Connection lost.  Reason: Service Disconnected");
            }
        }

        @DexIgnore
        @Override // com.fossil.k72
        public void e(Bundle bundle) {
            d dVar = uk5.e;
            if (dVar != null) {
                dVar.b();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            t62<Status> a2 = oh2.d.a(uk5.d);
            d dVar = uk5.e;
            if (dVar != null) {
                dVar.f(a2);
            }
            uk5.this.f3609a.h1(false);
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        Object b();  // void declaration

        @DexIgnore
        void c(z52 z52);

        @DexIgnore
        void f(t62<Status> t62);
    }

    @DexIgnore
    public uk5(Context context, Executor executor, on5 on5) {
        if (d == null) {
            r62.a aVar = new r62.a(context);
            aVar.c(oh2.b, new Scope[0]);
            aVar.c(oh2.f2681a, new Scope[0]);
            aVar.c(oh2.c, new Scope[0]);
            aVar.f(new String[]{"https://www.googleapis.com/auth/fitness.activity.write", "https://www.googleapis.com/auth/fitness.body.write", "https://www.googleapis.com/auth/fitness.location.write"});
            aVar.d(new b(this));
            aVar.e(new a(this));
            d = aVar.g();
        }
        this.f3609a = on5;
        if (f()) {
            a();
        }
        this.b = executor;
    }

    @DexIgnore
    public void a() {
        d.f();
        this.f3609a.h1(true);
    }

    @DexIgnore
    public void b() {
        d.g();
        this.f3609a.h1(false);
    }

    @DexIgnore
    public final ph2 c() {
        ph2.a b2 = ph2.b();
        b2.a(DataType.k, 1);
        return b2.b();
    }

    @DexIgnore
    public boolean d() {
        return h42.c(h42.b(PortfolioApp.d0), c());
    }

    @DexIgnore
    public boolean e() {
        return d.n() && d();
    }

    @DexIgnore
    public boolean f() {
        return this.f3609a.q();
    }

    @DexIgnore
    public void g() {
        this.b.execute(new c());
    }

    @DexIgnore
    public void h(Fragment fragment) {
        h42.e(fragment, 1, h42.b(PortfolioApp.d0), c());
    }

    @DexIgnore
    public void i(d dVar) {
        e = dVar;
    }

    @DexIgnore
    public void j() {
        try {
            b();
            h42.a(PortfolioApp.d0, new GoogleSignInOptions.a(GoogleSignInOptions.v).a()).t();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public void k() {
        this.f3609a.h1(false);
    }
}
