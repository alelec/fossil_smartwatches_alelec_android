package com.fossil;

import com.fossil.a34;
import com.fossil.f34;
import com.fossil.h34;
import com.fossil.m34;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import com.google.j2objc.annotations.RetainedWith;
import com.google.j2objc.annotations.Weak;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i34<K, V> extends f34<K, V> implements w44<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient h34<V> g;
    @DexIgnore
    @RetainedWith
    @LazyInit
    public transient i34<V, K> h;
    @DexIgnore
    public transient h34<Map.Entry<K, V>> i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends f34.c<K, V> {
        @DexIgnore
        public a() {
            super(a44.a().d().c());
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.f34.c
        public /* bridge */ /* synthetic */ f34.c b(Object obj, Object obj2) {
            f(obj, obj2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.f34.c
        public /* bridge */ /* synthetic */ f34.c c(Map.Entry entry) {
            g(entry);
            return this;
        }

        @DexIgnore
        public i34<K, V> e() {
            if (this.b != null) {
                w44<K, V> c = a44.a().d().c();
                for (Map.Entry entry : i44.from(this.b).onKeys().immutableSortedCopy(this.f1046a.asMap().entrySet())) {
                    c.putAll((K) entry.getKey(), (Iterable) entry.getValue());
                }
                this.f1046a = c;
            }
            return i34.a(this.f1046a, this.c);
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public a<K, V> f(K k, V v) {
            y34<K, V> y34 = this.f1046a;
            i14.l(k);
            i14.l(v);
            y34.put(k, v);
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.y34<K, V> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public a<K, V> g(Map.Entry<? extends K, ? extends V> entry) {
            Object key = entry.getKey();
            i14.l(key);
            Object value = entry.getValue();
            i14.l(value);
            this.f1046a.put(key, value);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public a<K, V> h(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.d(iterable);
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<K, V> extends h34<Map.Entry<K, V>> {
        @DexIgnore
        @Weak
        public /* final */ transient i34<K, V> c;

        @DexIgnore
        public b(i34<K, V> i34) {
            this.c = i34;
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return this.c.containsEntry(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean isPartialView() {
            return false;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.u24, com.fossil.u24, com.fossil.h34, com.fossil.h34, java.lang.Iterable
        public h54<Map.Entry<K, V>> iterator() {
            return this.c.entryIterator();
        }

        @DexIgnore
        public int size() {
            return this.c.size();
        }
    }

    @DexIgnore
    public i34(a34<K, h34<V>> a34, int i2, Comparator<? super V> comparator) {
        super(a34, i2);
        this.g = b(comparator);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.a34$b */
    /* JADX WARN: Multi-variable type inference failed */
    public static <K, V> i34<K, V> a(y34<? extends K, ? extends V> y34, Comparator<? super V> comparator) {
        i14.l(y34);
        if (y34.isEmpty() && comparator == null) {
            return of();
        }
        if (y34 instanceof i34) {
            i34<K, V> i34 = (i34) y34;
            if (!i34.isPartialView()) {
                return i34;
            }
        }
        a34.b bVar = new a34.b(y34.asMap().size());
        int i2 = 0;
        for (Map.Entry<? extends K, Collection<? extends V>> entry : y34.asMap().entrySet()) {
            Object key = entry.getKey();
            h34 d = d(comparator, entry.getValue());
            if (!d.isEmpty()) {
                bVar.c(key, d);
                i2 = d.size() + i2;
            }
        }
        return new i34<>(bVar.a(), i2, comparator);
    }

    @DexIgnore
    public static <V> h34<V> b(Comparator<? super V> comparator) {
        return comparator == null ? h34.of() : m34.emptySet(comparator);
    }

    @DexIgnore
    public static <K, V> a<K, V> builder() {
        return new a<>();
    }

    @DexIgnore
    public static <K, V> i34<K, V> copyOf(y34<? extends K, ? extends V> y34) {
        return a(y34, null);
    }

    @DexIgnore
    public static <K, V> i34<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        a aVar = new a();
        aVar.h(iterable);
        return aVar.e();
    }

    @DexIgnore
    public static <V> h34<V> d(Comparator<? super V> comparator, Collection<? extends V> collection) {
        return comparator == null ? h34.copyOf((Collection) collection) : m34.copyOf((Comparator) comparator, (Collection) collection);
    }

    @DexIgnore
    public static <V> h34.a<V> e(Comparator<? super V> comparator) {
        return comparator == null ? new h34.a<>() : new m34.a(comparator);
    }

    @DexIgnore
    public static <K, V> i34<K, V> of() {
        return h24.INSTANCE;
    }

    @DexIgnore
    public static <K, V> i34<K, V> of(K k, V v) {
        a builder = builder();
        builder.f(k, v);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> i34<K, V> of(K k, V v, K k2, V v2) {
        a builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> i34<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        a builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        builder.f(k3, v3);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> i34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        a builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        builder.f(k3, v3);
        builder.f(k4, v4);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> i34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        a builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        builder.f(k3, v3);
        builder.f(k4, v4);
        builder.f(k5, v5);
        return builder.e();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: com.fossil.a34$b */
    /* JADX WARN: Multi-variable type inference failed */
    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        Comparator comparator = (Comparator) objectInputStream.readObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            a34.b builder = a34.builder();
            int i2 = 0;
            int i3 = 0;
            while (i3 < readInt) {
                Object readObject = objectInputStream.readObject();
                int readInt2 = objectInputStream.readInt();
                if (readInt2 > 0) {
                    h34.a e = e(comparator);
                    for (int i4 = 0; i4 < readInt2; i4++) {
                        e.a(objectInputStream.readObject());
                    }
                    h34 j = e.j();
                    if (j.size() == readInt2) {
                        builder.c(readObject, j);
                        i3++;
                        i2 += readInt2;
                    } else {
                        throw new InvalidObjectException("Duplicate key-value pairs exist for key " + readObject);
                    }
                } else {
                    throw new InvalidObjectException("Invalid value count " + readInt2);
                }
            }
            try {
                f34.e.f1047a.b(this, builder.a());
                f34.e.b.a(this, i2);
                f34.e.c.b(this, b(comparator));
            } catch (IllegalArgumentException e2) {
                throw ((InvalidObjectException) new InvalidObjectException(e2.getMessage()).initCause(e2));
            }
        } else {
            throw new InvalidObjectException("Invalid key count " + readInt);
        }
    }

    @DexIgnore
    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(valueComparator());
        v44.d(this, objectOutputStream);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.i34<K, V> */
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.i34$a */
    /* JADX WARN: Multi-variable type inference failed */
    public final i34<V, K> c() {
        a builder = builder();
        Iterator it = entries().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            builder.f(entry.getValue(), entry.getKey());
        }
        i34<V, K> e = builder.e();
        e.h = this;
        return e;
    }

    @DexIgnore
    @Override // com.fossil.f34, com.fossil.f34, com.fossil.u14, com.fossil.y34
    public h34<Map.Entry<K, V>> entries() {
        h34<Map.Entry<K, V>> h34 = this.i;
        if (h34 != null) {
            return h34;
        }
        b bVar = new b(this);
        this.i = bVar;
        return bVar;
    }

    @DexIgnore
    @Override // com.fossil.f34, com.fossil.f34, com.fossil.y34
    public h34<V> get(K k) {
        return (h34) e14.a((h34) this.map.get(k), this.g);
    }

    @DexIgnore
    @Override // com.fossil.f34
    public i34<V, K> inverse() {
        i34<V, K> i34 = this.h;
        if (i34 != null) {
            return i34;
        }
        i34<V, K> c = c();
        this.h = c;
        return c;
    }

    @DexIgnore
    @Override // com.fossil.f34, com.fossil.f34
    @CanIgnoreReturnValue
    @Deprecated
    public h34<V> removeAll(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.f34, com.fossil.f34, com.fossil.u14
    @CanIgnoreReturnValue
    @Deprecated
    public h34<V> replaceValues(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public Comparator<? super V> valueComparator() {
        h34<V> h34 = this.g;
        if (h34 instanceof m34) {
            return ((m34) h34).comparator();
        }
        return null;
    }
}
