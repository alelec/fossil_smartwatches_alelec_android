package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v83 implements w83 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f3732a;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f3732a = hw2.d("measurement.service.ssaid_removal", true);
        hw2.b("measurement.id.ssaid_removal", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.w83
    public final boolean zza() {
        return f3732a.o().booleanValue();
    }
}
