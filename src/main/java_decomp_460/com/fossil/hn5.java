package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.a37;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.RingPhoneComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.RingMyPhoneMicroAppResponse;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.model.QuickResponseSender;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hn5 implements mr5 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ CopyOnWriteArrayList<kr5> f1498a; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ iv7 b; // = jv7.a(ux7.b(null, 1, null).plus(bw7.b()));
    @DexIgnore
    public /* final */ HybridPresetRepository c;
    @DexIgnore
    public /* final */ AlarmsRepository d;
    @DexIgnore
    public /* final */ dr5 e;
    @DexIgnore
    public /* final */ fs5 f;
    @DexIgnore
    public /* final */ QuickResponseRepository g;
    @DexIgnore
    public /* final */ or5 h;
    @DexIgnore
    public /* final */ a37 i;
    @DexIgnore
    public /* final */ on5 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return hn5.k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Runnable {
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ hn5 d;

        @DexIgnore
        public b(hn5 hn5, String str, int i) {
            pq7.c(str, "serial");
            this.d = hn5;
            this.b = str;
            this.c = i;
        }

        @DexIgnore
        public void run() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = hn5.l.a();
            local.d(a2, "Inside SteamingAction .run - eventId=" + this.c + ", serial=" + this.b);
            if (TextUtils.isEmpty(this.b)) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = hn5.l.a();
                local2.e(a3, "Inside .onStreamingEvent of " + this.b + " no active device");
                return;
            }
            HybridPreset activePresetBySerial = this.d.c.getActivePresetBySerial(this.b);
            if (activePresetBySerial != null) {
                Iterator<HybridPresetAppSetting> it = activePresetBySerial.getButtons().iterator();
                while (it.hasNext()) {
                    HybridPresetAppSetting next = it.next();
                    if (pq7.a(next.getAppId(), MicroAppInstruction.MicroAppID.Companion.getMicroAppIdFromDeviceEventId(this.c).getValue())) {
                        int i = this.c;
                        if (i == np1.RING_MY_PHONE_MICRO_APP.ordinal()) {
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String a4 = hn5.l.a();
                            local3.d(a4, "MicroAppAction.run UAPP_RING_PHONE microAppSetting " + next.getSettings());
                            hn5 hn5 = this.d;
                            String str = this.b;
                            String settings = next.getSettings();
                            if (settings != null) {
                                hn5.y(str, settings);
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else if (i == np1.COMMUTE_TIME_ETA_MICRO_APP.ordinal() || i == np1.COMMUTE_TIME_TRAVEL_MICRO_APP.ordinal()) {
                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.EXTRA_INFO, next.getSettings());
                            CommuteTimeService.C.b(PortfolioApp.h0.c(), bundle, this.d);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.LinkStreamingManager$onDeviceAppEvent$1", f = "LinkStreamingManager.kt", l = {109, 116, 123, 219}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ vi5 $event;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ hn5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ EnumMap $map$inlined;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(qn7 qn7, EnumMap enumMap, c cVar) {
                super(2, qn7);
                this.$map$inlined = enumMap;
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7, this.$map$inlined, this.this$0);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    or5 or5 = this.this$0.this$0.h;
                    this.L$0 = iv7;
                    this.label = 1;
                    if (or5.n(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(hn5 hn5, vi5 vi5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = hn5;
            this.$event = vi5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$event, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:100:0x03df  */
        /* JADX WARNING: Removed duplicated region for block: B:102:0x03e2  */
        /* JADX WARNING: Removed duplicated region for block: B:106:0x040e  */
        /* JADX WARNING: Removed duplicated region for block: B:139:0x035f A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:141:0x035f A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:83:0x0399  */
        /* JADX WARNING: Removed duplicated region for block: B:85:0x039c  */
        /* JADX WARNING: Removed duplicated region for block: B:88:0x03bd  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 1473
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.hn5.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.LinkStreamingManager$processAlarmSynchronizeEvent$1", f = "LinkStreamingManager.kt", l = {419}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ hn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(hn5 hn5, int i, qn7 qn7) {
            super(2, qn7);
            this.this$0 = hn5;
            this.$action = i;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$action, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object activeAlarms;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (this.$action == op1.SET.ordinal()) {
                    AlarmsRepository alarmsRepository = this.this$0.d;
                    this.L$0 = iv7;
                    this.label = 1;
                    activeAlarms = alarmsRepository.getActiveAlarms(this);
                    if (activeAlarms == d) {
                        return d;
                    }
                }
                return tl7.f3441a;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                activeAlarms = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list = (List) activeAlarms;
            if (list == null) {
                list = new ArrayList();
            }
            PortfolioApp.h0.c().q1(dj5.a(list));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ vp1 $notificationEvent$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ bo1 $subEvent;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ hn5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends BroadcastReceiver {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ e f1499a;

            @DexIgnore
            public a(e eVar, byte b, QuickResponseMessage quickResponseMessage) {
                this.f1499a = eVar;
            }

            @DexIgnore
            public void onReceive(Context context, Intent intent) {
                NotificationBaseObj.NotificationControlActionStatus notificationControlActionStatus;
                boolean z = false;
                if (intent != null) {
                    z = intent.getBooleanExtra("SEND_SMS_PERMISSION_REQUIRED", false);
                }
                if (getResultCode() == -1) {
                    FLogger.INSTANCE.getLocal().d(hn5.l.a(), "SMS delivered");
                    notificationControlActionStatus = NotificationBaseObj.NotificationControlActionStatus.SUCCESS;
                } else if (z) {
                    FLogger.INSTANCE.getLocal().d(hn5.l.a(), "SMS permission required");
                    notificationControlActionStatus = NotificationBaseObj.NotificationControlActionStatus.NOT_FOUND;
                } else {
                    FLogger.INSTANCE.getLocal().d(hn5.l.a(), "SMS failed");
                    notificationControlActionStatus = NotificationBaseObj.NotificationControlActionStatus.FAILED;
                }
                PortfolioApp.h0.c().E0(PortfolioApp.h0.c().J(), new DianaNotificationObj(((qp1) this.f1499a.$notificationEvent$inlined).getNotificationUid(), NotificationBaseObj.ANotificationType.TEXT, DianaNotificationObj.AApplicationName.Companion.getMESSAGES(), "", "", 1, "", new ArrayList(), Long.valueOf(System.currentTimeMillis()), notificationControlActionStatus, NotificationBaseObj.NotificationControlActionType.REPLY_MESSAGE));
                PortfolioApp.h0.c().unregisterReceiver(this);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(bo1 bo1, qn7 qn7, hn5 hn5, vp1 vp1) {
            super(2, qn7);
            this.$subEvent = bo1;
            this.this$0 = hn5;
            this.$notificationEvent$inlined = vp1;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.$subEvent, qn7, this.this$0, this.$notificationEvent$inlined);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            T t;
            QuickResponseSender quickResponseSender;
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                byte messageId = ((go1) this.$subEvent).getMessageId();
                Iterator<T> it = this.this$0.g.getAllQuickResponse().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (ao7.a(next.getId() == messageId).booleanValue()) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (!(t2 == null || (quickResponseSender = this.this$0.g.getQuickResponseSender(((go1) this.$subEvent).getSenderId())) == null)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = hn5.l.a();
                    local.d(a2, "process to send sms to " + quickResponseSender.getContent() + " with id " + ((int) messageId) + " senderId " + ((go1) this.$subEvent).getSenderId());
                    ln5.c.a().e(quickResponseSender.getContent(), t2.getResponse(), new a(this, messageId, t2));
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.LinkStreamingManager$processDeviceConfigSynchronizeEvent$1", f = "LinkStreamingManager.kt", l = {431}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(int i, String str, qn7 qn7) {
            super(2, qn7);
            this.$action = i;
            this.$deviceId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.$action, this.$deviceId, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (this.$action == op1.SET.ordinal()) {
                    PortfolioApp c = PortfolioApp.h0.c();
                    String str = this.$deviceId;
                    this.L$0 = iv7;
                    this.label = 1;
                    if (c.x1(str, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.LinkStreamingManager$processNotificationFilterSynchronizeEvent$1", f = "LinkStreamingManager.kt", l = {}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceId;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ hn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(hn5 hn5, int i, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = hn5;
            this.$action = i;
            this.$deviceId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, this.$action, this.$deviceId, qn7);
            gVar.p$ = (iv7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                if (this.$action == op1.SET.ordinal()) {
                    this.this$0.i.e(new a37.a(this.$deviceId), null);
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.LinkStreamingManager$startRingMyPhone$1", f = "LinkStreamingManager.kt", l = {}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $ringtoneName;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(String str, qn7 qn7) {
            super(2, qn7);
            this.$ringtoneName = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.$ringtoneName, qn7);
            hVar.p$ = (iv7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                List<Ringtone> e = dk5.g.e();
                if (!e.isEmpty()) {
                    for (Ringtone ringtone : e) {
                        if (pq7.a(ringtone.getRingtoneName(), this.$ringtoneName)) {
                            pn5.o.a().k(ringtone);
                            return tl7.f3441a;
                        }
                    }
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String canonicalName = hn5.class.getCanonicalName();
        if (canonicalName != null) {
            pq7.b(canonicalName, "LinkStreamingManager::class.java.canonicalName!!");
            k = canonicalName;
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    public hn5(HybridPresetRepository hybridPresetRepository, AlarmsRepository alarmsRepository, dr5 dr5, fs5 fs5, QuickResponseRepository quickResponseRepository, or5 or5, a37 a37, on5 on5) {
        pq7.c(hybridPresetRepository, "mPresetRepository");
        pq7.c(alarmsRepository, "mAlarmsRepository");
        pq7.c(dr5, "mBuddyChallengeManager");
        pq7.c(fs5, "mWorkoutTetherGpsManager");
        pq7.c(quickResponseRepository, "mQuickResponseRepository");
        pq7.c(or5, "mMusicControlComponent");
        pq7.c(a37, "mSetNotificationUseCase");
        pq7.c(on5, "sharedPreferencesManager");
        this.c = hybridPresetRepository;
        this.d = alarmsRepository;
        this.e = dr5;
        this.f = fs5;
        this.g = quickResponseRepository;
        this.h = or5;
        this.i = a37;
        this.j = on5;
        PortfolioApp.h0.h(this);
    }

    @DexIgnore
    public final void A(List<? extends kr5> list) {
        if (!(list == null || this.f1498a.isEmpty())) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = k;
            local.d(str, "stopServices serviceListSize=" + this.f1498a.size());
            for (kr5 kr5 : list) {
                kr5.b();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.mr5
    public void a(kr5 kr5) {
        pq7.c(kr5, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(k, "addObject");
        if (!this.f1498a.contains(kr5)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = k;
            local.d(str, "addObject service actionId " + kr5.c());
            this.f1498a.add(kr5);
        }
    }

    @DexIgnore
    @Override // com.fossil.mr5
    public void b(int i2, vh5 vh5) {
        pq7.c(vh5, "status");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onStatusChanged action " + i2);
    }

    @DexIgnore
    @Override // com.fossil.mr5
    public void c(kr5 kr5) {
        pq7.c(kr5, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(k, "removeObject");
        if (!this.f1498a.isEmpty()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = k;
            local.d(str, "removeObject mServiceListSize=" + this.f1498a.size());
            Iterator<kr5> it = this.f1498a.iterator();
            while (it.hasNext()) {
                kr5 next = it.next();
                pq7.b(next, "autoStopBaseService");
                if (next.c() == kr5.c()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = k;
                    local2.d(str2, "removeObject actionId " + kr5.c());
                    this.f1498a.remove(next);
                }
            }
        }
    }

    @DexIgnore
    @tc7
    public final void onDeviceAppEvent(vi5 vi5) {
        pq7.c(vi5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Inside " + k + ".onDeviceAppEvent - appId=" + vi5.a() + ", serial=" + vi5.c());
        xw7 unused = gu7.d(this.b, null, null, new c(this, vi5, null), 3, null);
    }

    @DexIgnore
    @tc7
    public final void onMicroAppCancelEvent(wi5 wi5) {
        pq7.c(wi5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Inside " + k + ".onMicroAppCancelEvent - event=" + wi5.a() + ", serial=" + wi5.b());
        A(this.f1498a);
    }

    @DexIgnore
    @tc7
    public final void onMusicActionEvent(zi5 zi5) {
        pq7.c(zi5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Inside " + k + ".onMusicActionEvent - musicActionEvent=" + zi5.a() + ", serial=" + zi5.b());
        or5 or5 = this.h;
        NotifyMusicEventResponse.MusicMediaAction a2 = zi5.a();
        pq7.b(a2, "event.musicActionEvent");
        or5.l(a2);
    }

    @DexIgnore
    @tc7
    public final void onStreamingEvent(bj5 bj5) {
        pq7.c(bj5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Inside " + k + ".onStreamingEvent - event=" + bj5.a() + ", serial=" + bj5.b());
        String b2 = bj5.b();
        pq7.b(b2, "event.serial");
        new Thread(new b(this, b2, bj5.a())).start();
    }

    @DexIgnore
    public final String t(String str) {
        hr7 hr7 = hr7.f1520a;
        String format = String.format("update_%s", Arrays.copyOf(new Object[]{str}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final xw7 u(String str, int i2) {
        return gu7.d(jv7.a(bw7.b()), null, null, new d(this, i2, null), 3, null);
    }

    @DexIgnore
    public final void v(vp1 vp1) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        StringBuilder sb = new StringBuilder();
        sb.append("processCallMessageNotificationEvent event ");
        sb.append(vp1);
        sb.append(" subEvent ");
        qp1 qp1 = (qp1) vp1;
        sb.append(qp1 != null ? qp1.getAction() : null);
        local.d(str, sb.toString());
        if (vp1 != null) {
            bo1 action = qp1.getAction();
            if (action instanceof fo1) {
                if (nk5.o.z()) {
                    ln5.c.a().d();
                }
            } else if (action instanceof ao1) {
                if (nk5.o.z()) {
                    ln5.c.a().a();
                }
            } else if (action instanceof go1) {
                xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new e(action, null, this, vp1), 3, null);
            } else {
                if (action instanceof eo1) {
                }
            }
        }
    }

    @DexIgnore
    public final xw7 w(String str, int i2) {
        return gu7.d(jv7.a(bw7.a()), null, null, new f(i2, str, null), 3, null);
    }

    @DexIgnore
    public final xw7 x(String str, int i2) {
        return gu7.d(jv7.a(bw7.a()), null, null, new g(this, i2, str, null), 3, null);
    }

    @DexIgnore
    public final void y(String str, String str2) {
        pq7.c(str, "deviceId");
        pq7.c(str2, MicroAppSetting.SETTING);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = k;
        local.d(str3, "startRingMyPhone, setting=" + str2);
        try {
            if (TextUtils.isEmpty(str2)) {
                List<Ringtone> e2 = dk5.g.e();
                Ringtone ringtone = e2.get(0);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str4 = k;
                local2.d(str4, "ringtones=" + e2 + ", defaultRingtone=" + ringtone);
                pn5.o.a().k(ringtone);
                return;
            }
            String component1 = ((Ringtone) new Gson().k(str2, Ringtone.class)).component1();
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str5 = k;
            local3.d(str5, "MicroAppAction.run - ringtone " + str2);
            PortfolioApp.h0.c().g1(new RingMyPhoneMicroAppResponse(), str);
            xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new h(component1, null), 3, null);
        } catch (Exception e3) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("AppUtil", "Error when read asset file - ex=" + e3);
        }
    }

    @DexIgnore
    public final void z(String str, int i2) {
        ku1 ku1;
        FLogger.INSTANCE.getLocal().d(k, "startRingMyPhone");
        if (i2 == ku1.ON.ordinal()) {
            pn5.o.a().n(new Ringtone(Constants.RINGTONE_DEFAULT, ""), 60000);
            ku1 = ku1.ON;
        } else {
            pn5.o.a().p();
            ku1 = ku1.OFF;
        }
        PortfolioApp.h0.c().g1(new RingPhoneComplicationAppInfo(ku1), str);
    }
}
