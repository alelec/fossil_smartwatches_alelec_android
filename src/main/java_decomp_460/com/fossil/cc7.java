package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.misfit.frameworks.buttonservice.model.watchface.Background;
import com.misfit.frameworks.buttonservice.model.watchface.ComplicationData;
import com.misfit.frameworks.buttonservice.model.watchface.MetricObject;
import com.misfit.frameworks.buttonservice.model.watchface.MovingObject;
import com.misfit.frameworks.buttonservice.model.watchface.MovingType;
import com.misfit.frameworks.buttonservice.model.watchface.TextData;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeColour;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;
import com.misfit.frameworks.buttonservice.model.watchface.TickerData;
import com.misfit.frameworks.buttonservice.model.watchface.TimeZoneData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cc7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<cl7<? extends T, ? extends T>, Boolean> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ Boolean invoke(Object obj) {
            return Boolean.valueOf(invoke((cl7) obj));
        }

        @DexIgnore
        public final boolean invoke(cl7<? extends T, ? extends T> cl7) {
            pq7.c(cl7, "<name for destructuring parameter 0>");
            return pq7.a(cl7.component1(), cl7.component2());
        }
    }

    @DexIgnore
    public static final <T> boolean a(List<? extends T> list, List<? extends T> list2) {
        pq7.c(list, "$this$deepEqualTo");
        pq7.c(list2, FacebookRequestErrorClassification.KEY_OTHER);
        return list == list2 || (list.size() == list2.size() && !at7.f(at7.o(at7.v(pm7.z(list), pm7.z(list2)), a.INSTANCE), Boolean.FALSE));
    }

    @DexIgnore
    public static final Background b(hb7 hb7) {
        pq7.c(hb7, "$this$toButtonBackground");
        return new Background(hb7.a(), hb7.b());
    }

    @DexIgnore
    public static final ThemeColour c(nb7 nb7) {
        pq7.c(nb7, "$this$toButtonColour");
        return ThemeColour.valueOf(nb7.name());
    }

    @DexIgnore
    public static final ComplicationData d(ib7 ib7) {
        pq7.c(ib7, "$this$toButtonComplicationData");
        if (ib7.e() == null && ib7.g() == null) {
            return new ComplicationData(ib7.h(), ib7.f(), ib7.d(), null, c(ib7.c()), 8, null);
        }
        byte[] h = ib7.h();
        String f = ib7.f();
        boolean d = ib7.d();
        String e = ib7.e();
        String str = e != null ? e : "";
        Integer g = ib7.g();
        return new ComplicationData(h, f, d, new TimeZoneData(str, g != null ? g.intValue() : 1), c(ib7.c()));
    }

    @DexIgnore
    public static final MetricObject e(jb7 jb7) {
        pq7.c(jb7, "$this$toButtonMetric");
        return new MetricObject(jb7.c(), jb7.d(), jb7.b(), jb7.a());
    }

    @DexIgnore
    public static final MovingType f(lb7 lb7) {
        pq7.c(lb7, "$this$toButtonMovingType");
        return MovingType.valueOf(lb7.name());
    }

    @DexIgnore
    public static final TextData g(mb7 mb7) {
        pq7.c(mb7, "$this$toButtonTextData");
        return new TextData(mb7.f(), mb7.e(), mb7.d(), c(mb7.c()));
    }

    @DexIgnore
    public static final ThemeData h(ob7 ob7) {
        pq7.c(ob7, "$this$toButtonThemeData");
        hb7 b = ob7.b();
        Background b2 = b != null ? b(b) : null;
        ArrayList arrayList = new ArrayList();
        for (T t : ob7.a()) {
            MetricObject e = e(t.b());
            MovingType f = f(t.getType());
            if (t instanceof mb7) {
                arrayList.add(new MovingObject(e, null, null, g(t), f, 6, null));
            } else if (t instanceof pb7) {
                arrayList.add(new MovingObject(e, null, i(t), null, f, 10, null));
            } else if (t instanceof ib7) {
                arrayList.add(new MovingObject(e, d(t), null, null, f, 12, null));
            }
        }
        return new ThemeData(b2, arrayList);
    }

    @DexIgnore
    public static final TickerData i(pb7 pb7) {
        pq7.c(pb7, "$this$toButtonTicker");
        return new TickerData(pb7.e(), pb7.d(), c(pb7.c()));
    }

    @DexIgnore
    public static final ub7 j(ob7 ob7, List<fb7> list, List<fb7> list2, List<fb7> list3) {
        T t;
        T t2;
        T t3;
        byte[] a2;
        pq7.c(ob7, "$this$toUIThemeData");
        pq7.c(list, "backgroundsAsset");
        pq7.c(list2, "tickersAsset");
        pq7.c(list3, "ringsAsset");
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            String b = next.b();
            hb7 b2 = ob7.b();
            if (pq7.a(b, b2 != null ? b2.b() : null)) {
                t = next;
                break;
            }
        }
        T t4 = t;
        String f = t4 != null ? t4.f() : null;
        hb7 b3 = ob7.b();
        hb7 b4 = ob7.b();
        rb7 rb7 = new rb7(b3, (b4 == null || (a2 = b4.a()) == null) ? null : j37.c(a2), f, null, 8, null);
        ArrayList arrayList = new ArrayList();
        for (T t5 : ob7.a()) {
            if (t5 instanceof pb7) {
                Iterator<T> it2 = list2.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t3 = null;
                        break;
                    }
                    T next2 = it2.next();
                    if (pq7.a(next2.d(), t5.d())) {
                        t3 = next2;
                        break;
                    }
                }
                T t6 = t3;
                T t7 = t5;
                arrayList.add(new vb7(t7, t7.e().length == 0 ? null : j37.c(t7.e()), t6 != null ? t6.f() : null));
            } else if (t5 instanceof ib7) {
                Iterator<T> it3 = list3.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next3 = it3.next();
                    if (pq7.a(next3.b(), t5.f())) {
                        t2 = next3;
                        break;
                    }
                }
                T t8 = t2;
                T t9 = t5;
                arrayList.add(new sb7(t9, t9.h().length == 0 ? null : j37.c(t9.h()), t8 != null ? t8.f() : null));
            } else if (t5 instanceof mb7) {
                arrayList.add(new tb7(t5));
            }
        }
        return new ub7(rb7, arrayList);
    }

    @DexIgnore
    public static final hb7 k(Background background) {
        pq7.c(background, "$this$toWFBackground");
        return new hb7(background.getData(), background.getId());
    }

    @DexIgnore
    public static final ib7 l(ComplicationData complicationData, MetricObject metricObject, MovingType movingType, int i) {
        pq7.c(complicationData, "$this$toWFComplicationData");
        pq7.c(metricObject, "metric");
        pq7.c(movingType, "type");
        byte[] data = complicationData.getData();
        String name = complicationData.getName();
        boolean enableRingGoal = complicationData.getEnableRingGoal();
        TimeZoneData timeZoneData = complicationData.getTimeZoneData();
        String location = timeZoneData != null ? timeZoneData.getLocation() : null;
        TimeZoneData timeZoneData2 = complicationData.getTimeZoneData();
        return new ib7(data, name, enableRingGoal, location, timeZoneData2 != null ? Integer.valueOf(timeZoneData2.getOffsetMin()) : null, p(complicationData.getColour()), m(metricObject), n(movingType), i);
    }

    @DexIgnore
    public static final jb7 m(MetricObject metricObject) {
        pq7.c(metricObject, "$this$toWFMetric");
        return new jb7(metricObject.getScaledX(), metricObject.getScaledY(), metricObject.getScaledWidth(), metricObject.getScaledHeight());
    }

    @DexIgnore
    public static final lb7 n(MovingType movingType) {
        pq7.c(movingType, "$this$toWFMovingType");
        return lb7.valueOf(movingType.name());
    }

    @DexIgnore
    public static final mb7 o(TextData textData, MetricObject metricObject, int i) {
        pq7.c(textData, "$this$toWFTextData");
        pq7.c(metricObject, "metric");
        return new mb7(textData.getText(), textData.getFont(), textData.getFontScaled(), p(textData.getColour()), m(metricObject), null, i, 32, null);
    }

    @DexIgnore
    public static final nb7 p(ThemeColour themeColour) {
        pq7.c(themeColour, "$this$toWFThemeColour");
        return nb7.valueOf(themeColour.name());
    }

    @DexIgnore
    public static final ob7 q(ThemeData themeData) {
        pq7.c(themeData, "$this$toWFThemeData");
        Background background = themeData.getBackground();
        hb7 k = background != null ? k(background) : null;
        ArrayList arrayList = new ArrayList();
        int i = 0;
        for (T t : themeData.getMovingObjects()) {
            if (i >= 0) {
                T t2 = t;
                int i2 = bc7.f414a[t2.getType().ordinal()];
                if (i2 == 1) {
                    TextData textData = t2.getTextData();
                    if (textData != null) {
                        arrayList.add(o(textData, t2.getMetric(), i));
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else if (i2 != 2) {
                    ComplicationData complicationData = t2.getComplicationData();
                    if (complicationData != null) {
                        arrayList.add(l(complicationData, t2.getMetric(), t2.getType(), i));
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    TickerData tickerData = t2.getTickerData();
                    if (tickerData != null) {
                        arrayList.add(r(tickerData, t2.getMetric(), i));
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                i++;
            } else {
                hm7.l();
                throw null;
            }
        }
        return new ob7(k, arrayList);
    }

    @DexIgnore
    public static final pb7 r(TickerData tickerData, MetricObject metricObject, int i) {
        pq7.c(tickerData, "$this$toWFTicker");
        pq7.c(metricObject, "metric");
        return new pb7(tickerData.getData(), tickerData.getName(), p(tickerData.getColour()), m(metricObject), lb7.TICKER, i);
    }
}
