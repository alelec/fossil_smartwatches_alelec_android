package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n07 implements Factory<m07> {
    @DexIgnore
    public static m07 a(j07 j07, UserRepository userRepository, do5 do5, mj5 mj5, ThemeRepository themeRepository) {
        return new m07(j07, userRepository, do5, mj5, themeRepository);
    }
}
