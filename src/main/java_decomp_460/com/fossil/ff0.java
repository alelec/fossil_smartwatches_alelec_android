package com.fossil;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ActionBarContainer;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ActionBarOverlayLayout;
import androidx.appcompat.widget.Toolbar;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.cg0;
import java.lang.ref.WeakReference;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ff0 extends ActionBar implements ActionBarOverlayLayout.d {
    @DexIgnore
    public static /* final */ Interpolator C; // = new AccelerateInterpolator();
    @DexIgnore
    public static /* final */ Interpolator D; // = new DecelerateInterpolator();
    @DexIgnore
    public /* final */ so0 A; // = new b();
    @DexIgnore
    public /* final */ uo0 B; // = new c();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Context f1111a;
    @DexIgnore
    public Context b;
    @DexIgnore
    public Activity c;
    @DexIgnore
    public ActionBarOverlayLayout d;
    @DexIgnore
    public ActionBarContainer e;
    @DexIgnore
    public ch0 f;
    @DexIgnore
    public ActionBarContextView g;
    @DexIgnore
    public View h;
    @DexIgnore
    public mh0 i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public d k;
    @DexIgnore
    public ActionMode l;
    @DexIgnore
    public ActionMode.Callback m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public ArrayList<ActionBar.a> o; // = new ArrayList<>();
    @DexIgnore
    public boolean p;
    @DexIgnore
    public int q; // = 0;
    @DexIgnore
    public boolean r; // = true;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v; // = true;
    @DexIgnore
    public uf0 w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public /* final */ so0 z; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends to0 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.so0
        public void b(View view) {
            View view2;
            ff0 ff0 = ff0.this;
            if (ff0.r && (view2 = ff0.h) != null) {
                view2.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                ff0.this.e.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            ff0.this.e.setVisibility(8);
            ff0.this.e.setTransitioning(false);
            ff0 ff02 = ff0.this;
            ff02.w = null;
            ff02.A();
            ActionBarOverlayLayout actionBarOverlayLayout = ff0.this.d;
            if (actionBarOverlayLayout != null) {
                mo0.i0(actionBarOverlayLayout);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends to0 {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.so0
        public void b(View view) {
            ff0 ff0 = ff0.this;
            ff0.w = null;
            ff0.e.requestLayout();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements uo0 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.uo0
        public void a(View view) {
            ((View) ff0.this.e.getParent()).invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends ActionMode implements cg0.a {
        @DexIgnore
        public /* final */ Context d;
        @DexIgnore
        public /* final */ cg0 e;
        @DexIgnore
        public ActionMode.Callback f;
        @DexIgnore
        public WeakReference<View> g;

        @DexIgnore
        public d(Context context, ActionMode.Callback callback) {
            this.d = context;
            this.f = callback;
            cg0 cg0 = new cg0(context);
            cg0.W(1);
            this.e = cg0;
            cg0.V(this);
        }

        @DexIgnore
        @Override // com.fossil.cg0.a
        public boolean a(cg0 cg0, MenuItem menuItem) {
            ActionMode.Callback callback = this.f;
            if (callback != null) {
                return callback.d(this, menuItem);
            }
            return false;
        }

        @DexIgnore
        @Override // com.fossil.cg0.a
        public void b(cg0 cg0) {
            if (this.f != null) {
                k();
                ff0.this.g.l();
            }
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void c() {
            ff0 ff0 = ff0.this;
            if (ff0.k == this) {
                if (!ff0.z(ff0.s, ff0.t, false)) {
                    ff0 ff02 = ff0.this;
                    ff02.l = this;
                    ff02.m = this.f;
                } else {
                    this.f.a(this);
                }
                this.f = null;
                ff0.this.y(false);
                ff0.this.g.g();
                ff0.this.f.q().sendAccessibilityEvent(32);
                ff0 ff03 = ff0.this;
                ff03.d.setHideOnContentScrollEnabled(ff03.y);
                ff0.this.k = null;
            }
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public View d() {
            WeakReference<View> weakReference = this.g;
            if (weakReference != null) {
                return weakReference.get();
            }
            return null;
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public Menu e() {
            return this.e;
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public MenuInflater f() {
            return new tf0(this.d);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public CharSequence g() {
            return ff0.this.g.getSubtitle();
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public CharSequence i() {
            return ff0.this.g.getTitle();
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void k() {
            if (ff0.this.k == this) {
                this.e.h0();
                try {
                    this.f.c(this, this.e);
                } finally {
                    this.e.g0();
                }
            }
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public boolean l() {
            return ff0.this.g.j();
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void m(View view) {
            ff0.this.g.setCustomView(view);
            this.g = new WeakReference<>(view);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void n(int i) {
            o(ff0.this.f1111a.getResources().getString(i));
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void o(CharSequence charSequence) {
            ff0.this.g.setSubtitle(charSequence);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void q(int i) {
            r(ff0.this.f1111a.getResources().getString(i));
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void r(CharSequence charSequence) {
            ff0.this.g.setTitle(charSequence);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void s(boolean z) {
            super.s(z);
            ff0.this.g.setTitleOptional(z);
        }

        @DexIgnore
        public boolean t() {
            this.e.h0();
            try {
                return this.f.b(this, this.e);
            } finally {
                this.e.g0();
            }
        }
    }

    @DexIgnore
    public ff0(Activity activity, boolean z2) {
        new ArrayList();
        this.c = activity;
        View decorView = activity.getWindow().getDecorView();
        G(decorView);
        if (!z2) {
            this.h = decorView.findViewById(16908290);
        }
    }

    @DexIgnore
    public ff0(Dialog dialog) {
        new ArrayList();
        G(dialog.getWindow().getDecorView());
    }

    @DexIgnore
    public static boolean z(boolean z2, boolean z3, boolean z4) {
        if (z4) {
            return true;
        }
        return !z2 && !z3;
    }

    @DexIgnore
    public void A() {
        ActionMode.Callback callback = this.m;
        if (callback != null) {
            callback.a(this.l);
            this.l = null;
            this.m = null;
        }
    }

    @DexIgnore
    public void B(boolean z2) {
        View view;
        uf0 uf0 = this.w;
        if (uf0 != null) {
            uf0.a();
        }
        if (this.q != 0 || (!this.x && !z2)) {
            this.z.b(null);
            return;
        }
        this.e.setAlpha(1.0f);
        this.e.setTransitioning(true);
        uf0 uf02 = new uf0();
        float f2 = (float) (-this.e.getHeight());
        if (z2) {
            int[] iArr = {0, 0};
            this.e.getLocationInWindow(iArr);
            f2 -= (float) iArr[1];
        }
        ro0 c2 = mo0.c(this.e);
        c2.k(f2);
        c2.i(this.B);
        uf02.c(c2);
        if (this.r && (view = this.h) != null) {
            ro0 c3 = mo0.c(view);
            c3.k(f2);
            uf02.c(c3);
        }
        uf02.f(C);
        uf02.e(250);
        uf02.g(this.z);
        this.w = uf02;
        uf02.h();
    }

    @DexIgnore
    public void C(boolean z2) {
        View view;
        View view2;
        uf0 uf0 = this.w;
        if (uf0 != null) {
            uf0.a();
        }
        this.e.setVisibility(0);
        if (this.q != 0 || (!this.x && !z2)) {
            this.e.setAlpha(1.0f);
            this.e.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            if (this.r && (view = this.h) != null) {
                view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            this.A.b(null);
        } else {
            this.e.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float f2 = (float) (-this.e.getHeight());
            if (z2) {
                int[] iArr = {0, 0};
                this.e.getLocationInWindow(iArr);
                f2 -= (float) iArr[1];
            }
            this.e.setTranslationY(f2);
            uf0 uf02 = new uf0();
            ro0 c2 = mo0.c(this.e);
            c2.k(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            c2.i(this.B);
            uf02.c(c2);
            if (this.r && (view2 = this.h) != null) {
                view2.setTranslationY(f2);
                ro0 c3 = mo0.c(this.h);
                c3.k(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                uf02.c(c3);
            }
            uf02.f(D);
            uf02.e(250);
            uf02.g(this.A);
            this.w = uf02;
            uf02.h();
        }
        ActionBarOverlayLayout actionBarOverlayLayout = this.d;
        if (actionBarOverlayLayout != null) {
            mo0.i0(actionBarOverlayLayout);
        }
    }

    @DexIgnore
    public final ch0 D(View view) {
        if (view instanceof ch0) {
            return (ch0) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Can't make a decor toolbar out of ");
        sb.append(view != null ? view.getClass().getSimpleName() : "null");
        throw new IllegalStateException(sb.toString());
    }

    @DexIgnore
    public int E() {
        return this.f.n();
    }

    @DexIgnore
    public final void F() {
        if (this.u) {
            this.u = false;
            ActionBarOverlayLayout actionBarOverlayLayout = this.d;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(false);
            }
            N(false);
        }
    }

    @DexIgnore
    public final void G(View view) {
        ActionBarOverlayLayout actionBarOverlayLayout = (ActionBarOverlayLayout) view.findViewById(qe0.decor_content_parent);
        this.d = actionBarOverlayLayout;
        if (actionBarOverlayLayout != null) {
            actionBarOverlayLayout.setActionBarVisibilityCallback(this);
        }
        this.f = D(view.findViewById(qe0.action_bar));
        this.g = (ActionBarContextView) view.findViewById(qe0.action_context_bar);
        ActionBarContainer actionBarContainer = (ActionBarContainer) view.findViewById(qe0.action_bar_container);
        this.e = actionBarContainer;
        ch0 ch0 = this.f;
        if (ch0 == null || this.g == null || actionBarContainer == null) {
            throw new IllegalStateException(ff0.class.getSimpleName() + " can only be used with a compatible window decor layout");
        }
        this.f1111a = ch0.getContext();
        boolean z2 = (this.f.s() & 4) != 0;
        if (z2) {
            this.j = true;
        }
        of0 b2 = of0.b(this.f1111a);
        K(b2.a() || z2);
        I(b2.g());
        TypedArray obtainStyledAttributes = this.f1111a.obtainStyledAttributes(null, ue0.ActionBar, le0.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(ue0.ActionBar_hideOnContentScroll, false)) {
            J(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(ue0.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            t((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public void H(int i2, int i3) {
        int s2 = this.f.s();
        if ((i3 & 4) != 0) {
            this.j = true;
        }
        this.f.k((s2 & i3) | (i2 & i3));
    }

    @DexIgnore
    public final void I(boolean z2) {
        boolean z3 = true;
        this.p = z2;
        if (!z2) {
            this.f.i(null);
            this.e.setTabContainer(this.i);
        } else {
            this.e.setTabContainer(null);
            this.f.i(this.i);
        }
        boolean z4 = E() == 2;
        mh0 mh0 = this.i;
        if (mh0 != null) {
            if (z4) {
                mh0.setVisibility(0);
                ActionBarOverlayLayout actionBarOverlayLayout = this.d;
                if (actionBarOverlayLayout != null) {
                    mo0.i0(actionBarOverlayLayout);
                }
            } else {
                mh0.setVisibility(8);
            }
        }
        this.f.v(!this.p && z4);
        ActionBarOverlayLayout actionBarOverlayLayout2 = this.d;
        if (this.p || !z4) {
            z3 = false;
        }
        actionBarOverlayLayout2.setHasNonEmbeddedTabs(z3);
    }

    @DexIgnore
    public void J(boolean z2) {
        if (!z2 || this.d.w()) {
            this.y = z2;
            this.d.setHideOnContentScrollEnabled(z2);
            return;
        }
        throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }

    @DexIgnore
    public void K(boolean z2) {
        this.f.r(z2);
    }

    @DexIgnore
    public final boolean L() {
        return mo0.Q(this.e);
    }

    @DexIgnore
    public final void M() {
        if (!this.u) {
            this.u = true;
            ActionBarOverlayLayout actionBarOverlayLayout = this.d;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(true);
            }
            N(false);
        }
    }

    @DexIgnore
    public final void N(boolean z2) {
        if (z(this.s, this.t, this.u)) {
            if (!this.v) {
                this.v = true;
                C(z2);
            }
        } else if (this.v) {
            this.v = false;
            B(z2);
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void a() {
        if (this.t) {
            this.t = false;
            N(true);
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void b() {
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void c(boolean z2) {
        this.r = z2;
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void d() {
        if (!this.t) {
            this.t = true;
            N(true);
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void e() {
        uf0 uf0 = this.w;
        if (uf0 != null) {
            uf0.a();
            this.w = null;
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void f(int i2) {
        this.q = i2;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean h() {
        ch0 ch0 = this.f;
        if (ch0 == null || !ch0.j()) {
            return false;
        }
        this.f.collapseActionView();
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void i(boolean z2) {
        if (z2 != this.n) {
            this.n = z2;
            int size = this.o.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.o.get(i2).a(z2);
            }
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public int j() {
        return this.f.s();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public Context k() {
        if (this.b == null) {
            TypedValue typedValue = new TypedValue();
            this.f1111a.getTheme().resolveAttribute(le0.actionBarWidgetTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                this.b = new ContextThemeWrapper(this.f1111a, i2);
            } else {
                this.b = this.f1111a;
            }
        }
        return this.b;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void m(Configuration configuration) {
        I(of0.b(this.f1111a).g());
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean o(int i2, KeyEvent keyEvent) {
        Menu e2;
        d dVar = this.k;
        if (dVar == null || (e2 = dVar.e()) == null) {
            return false;
        }
        e2.setQwertyMode(KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1);
        return e2.performShortcut(i2, keyEvent, 0);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void r(boolean z2) {
        if (!this.j) {
            s(z2);
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void s(boolean z2) {
        H(z2 ? 4 : 0, 4);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void t(float f2) {
        mo0.s0(this.e, f2);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void u(boolean z2) {
        uf0 uf0;
        this.x = z2;
        if (!z2 && (uf0 = this.w) != null) {
            uf0.a();
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void v(CharSequence charSequence) {
        this.f.setTitle(charSequence);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void w(CharSequence charSequence) {
        this.f.setWindowTitle(charSequence);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public ActionMode x(ActionMode.Callback callback) {
        d dVar = this.k;
        if (dVar != null) {
            dVar.c();
        }
        this.d.setHideOnContentScrollEnabled(false);
        this.g.k();
        d dVar2 = new d(this.g.getContext(), callback);
        if (!dVar2.t()) {
            return null;
        }
        this.k = dVar2;
        dVar2.k();
        this.g.h(dVar2);
        y(true);
        this.g.sendAccessibilityEvent(32);
        return dVar2;
    }

    @DexIgnore
    public void y(boolean z2) {
        ro0 o2;
        ro0 f2;
        if (z2) {
            M();
        } else {
            F();
        }
        if (L()) {
            if (z2) {
                f2 = this.f.o(4, 100);
                o2 = this.g.f(0, 200);
            } else {
                o2 = this.f.o(0, 200);
                f2 = this.g.f(8, 100);
            }
            uf0 uf0 = new uf0();
            uf0.d(f2, o2);
            uf0.h();
        } else if (z2) {
            this.f.setVisibility(4);
            this.g.setVisibility(0);
        } else {
            this.f.setVisibility(0);
            this.g.setVisibility(8);
        }
    }
}
