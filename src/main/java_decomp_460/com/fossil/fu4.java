package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fu4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ jt4 f1208a;

    @DexIgnore
    public fu4(jt4 jt4) {
        pq7.c(jt4, "dao");
        this.f1208a = jt4;
    }

    @DexIgnore
    public final void a() {
        this.f1208a.a();
    }

    @DexIgnore
    public final long b(it4 it4) {
        pq7.c(it4, "profile");
        return this.f1208a.c(it4);
    }

    @DexIgnore
    public final it4 c() {
        return this.f1208a.d();
    }

    @DexIgnore
    public final LiveData<it4> d() {
        return this.f1208a.b();
    }
}
