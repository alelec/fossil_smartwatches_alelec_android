package com.fossil;

import android.content.Context;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ig0 {

    @DexIgnore
    public interface a {
        @DexIgnore
        void b(cg0 cg0, boolean z);

        @DexIgnore
        boolean c(cg0 cg0);
    }

    @DexIgnore
    void b(cg0 cg0, boolean z);

    @DexIgnore
    void c(boolean z);

    @DexIgnore
    boolean d();

    @DexIgnore
    boolean e(cg0 cg0, eg0 eg0);

    @DexIgnore
    boolean f(cg0 cg0, eg0 eg0);

    @DexIgnore
    void g(a aVar);

    @DexIgnore
    int getId();

    @DexIgnore
    void h(Context context, cg0 cg0);

    @DexIgnore
    void i(Parcelable parcelable);

    @DexIgnore
    boolean k(ng0 ng0);

    @DexIgnore
    Parcelable l();
}
