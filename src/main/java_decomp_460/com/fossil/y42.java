package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y42 extends a52<Status> {
    @DexIgnore
    public y42(r62 r62) {
        super(r62);
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final /* synthetic */ z62 f(Status status) {
        return status;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.m62$b] */
    @Override // com.fossil.i72
    public final /* synthetic */ void u(u42 u42) throws RemoteException {
        u42 u422 = u42;
        ((g52) u422.I()).d1(new z42(this), u422.t0());
    }
}
