package com.fossil;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lx1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ lx1 f2275a; // = new lx1();

    @DexIgnore
    public enum a {
        ENCRYPT(1),
        DECRYPT(2);
        
        @DexIgnore
        public /* final */ int cipherOptMode;

        @DexIgnore
        public a(int i) {
            this.cipherOptMode = i;
        }

        @DexIgnore
        public final int getCipherOptMode() {
            return this.cipherOptMode;
        }
    }

    @DexIgnore
    public enum b {
        CBC_PKCS5_PADDING("AES/CBC/PKCS5PADDING"),
        CBC_NO_PADDING("AES/CBC/NoPadding"),
        CTR_NO_PADDING("AES/CTR/NoPadding"),
        GCM_NO_PADDING("AES/GCM/NoPadding");
        
        @DexIgnore
        public /* final */ String value;

        @DexIgnore
        public b(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public final byte[] a(b bVar, byte[] bArr, byte[] bArr2, byte[] bArr3) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        pq7.c(bVar, "transformation");
        pq7.c(bArr, "rawKey");
        pq7.c(bArr2, "iv");
        pq7.c(bArr3, "data");
        return d(a.DECRYPT, bVar, bArr, bArr2, bArr3);
    }

    @DexIgnore
    public final byte[] b(b bVar, byte[] bArr, byte[] bArr2, byte[] bArr3) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        pq7.c(bVar, "transformation");
        pq7.c(bArr, "rawKey");
        pq7.c(bArr2, "iv");
        pq7.c(bArr3, "data");
        return d(a.ENCRYPT, bVar, bArr, bArr2, bArr3);
    }

    @DexIgnore
    public final SecretKey c() throws NoSuchAlgorithmException {
        SecureRandom secureRandom = new SecureRandom();
        KeyGenerator instance = KeyGenerator.getInstance("AES");
        instance.init(256, secureRandom);
        SecretKey generateKey = instance.generateKey();
        pq7.b(generateKey, "keyGenerator.generateKey()");
        return generateKey;
    }

    @DexIgnore
    public final byte[] d(a aVar, b bVar, byte[] bArr, byte[] bArr2, byte[] bArr3) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        pq7.c(aVar, "operation");
        pq7.c(bVar, "transformation");
        pq7.c(bArr, "rawKey");
        pq7.c(bArr2, "iv");
        pq7.c(bArr3, "data");
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
        Cipher instance = Cipher.getInstance(bVar.getValue());
        instance.init(aVar.getCipherOptMode(), secretKeySpec, new IvParameterSpec(bArr2));
        byte[] doFinal = instance.doFinal(bArr3);
        pq7.b(doFinal, "cipher.doFinal(data)");
        return doFinal;
    }
}
