package com.fossil;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hk4 extends JsonWriter {
    @DexIgnore
    public static /* final */ Writer u; // = new a();
    @DexIgnore
    public static /* final */ jj4 v; // = new jj4("closed");
    @DexIgnore
    public /* final */ List<JsonElement> m; // = new ArrayList();
    @DexIgnore
    public String s;
    @DexIgnore
    public JsonElement t; // = fj4.f1138a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Writer {
        @DexIgnore
        @Override // java.io.Closeable, java.io.Writer, java.lang.AutoCloseable
        public void close() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // java.io.Writer, java.io.Flushable
        public void flush() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // java.io.Writer
        public void write(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public hk4() {
        super(u);
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter A(String str) throws IOException {
        if (this.m.isEmpty() || this.s != null) {
            throw new IllegalStateException();
        } else if (q0() instanceof gj4) {
            this.s = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter C() throws IOException {
        r0(fj4.f1138a);
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter V(long j) throws IOException {
        r0(new jj4((Number) Long.valueOf(j)));
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter X(Boolean bool) throws IOException {
        if (bool == null) {
            C();
        } else {
            r0(new jj4(bool));
        }
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter b0(Number number) throws IOException {
        if (number == null) {
            C();
        } else {
            if (!o()) {
                double doubleValue = number.doubleValue();
                if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                    throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
                }
            }
            r0(new jj4(number));
        }
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter c() throws IOException {
        bj4 bj4 = new bj4();
        r0(bj4);
        this.m.add(bj4);
        return this;
    }

    @DexIgnore
    @Override // java.io.Closeable, com.google.gson.stream.JsonWriter, java.lang.AutoCloseable
    public void close() throws IOException {
        if (this.m.isEmpty()) {
            this.m.add(v);
            return;
        }
        throw new IOException("Incomplete document");
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter f() throws IOException {
        gj4 gj4 = new gj4();
        r0(gj4);
        this.m.add(gj4);
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter, java.io.Flushable
    public void flush() throws IOException {
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter g0(String str) throws IOException {
        if (str == null) {
            C();
        } else {
            r0(new jj4(str));
        }
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter i0(boolean z) throws IOException {
        r0(new jj4(Boolean.valueOf(z)));
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter j() throws IOException {
        if (this.m.isEmpty() || this.s != null) {
            throw new IllegalStateException();
        } else if (q0() instanceof bj4) {
            List<JsonElement> list = this.m;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter k() throws IOException {
        if (this.m.isEmpty() || this.s != null) {
            throw new IllegalStateException();
        } else if (q0() instanceof gj4) {
            List<JsonElement> list = this.m;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public JsonElement p0() {
        if (this.m.isEmpty()) {
            return this.t;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.m);
    }

    @DexIgnore
    public final JsonElement q0() {
        List<JsonElement> list = this.m;
        return list.get(list.size() - 1);
    }

    @DexIgnore
    public final void r0(JsonElement jsonElement) {
        if (this.s != null) {
            if (!jsonElement.h() || l()) {
                ((gj4) q0()).k(this.s, jsonElement);
            }
            this.s = null;
        } else if (this.m.isEmpty()) {
            this.t = jsonElement;
        } else {
            JsonElement q0 = q0();
            if (q0 instanceof bj4) {
                ((bj4) q0).k(jsonElement);
                return;
            }
            throw new IllegalStateException();
        }
    }
}
