package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fg5 extends eg5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d N; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray O;
    @DexIgnore
    public long M;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        O = sparseIntArray;
        sparseIntArray.put(2131362773, 1);
        O.put(2131362733, 2);
        O.put(2131362654, 3);
        O.put(2131363465, 4);
        O.put(2131363457, 5);
        O.put(2131363452, 6);
        O.put(2131362970, 7);
        O.put(2131362562, 8);
        O.put(2131362561, 9);
        O.put(2131362357, 10);
        O.put(2131362415, 11);
        O.put(2131362534, 12);
        O.put(2131362377, 13);
        O.put(2131362447, 14);
        O.put(2131362448, 15);
        O.put(2131362657, 16);
        O.put(2131362693, 17);
        O.put(2131362755, 18);
        O.put(2131362675, 19);
        O.put(2131362721, 20);
        O.put(2131362722, 21);
    }
    */

    @DexIgnore
    public fg5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 22, N, O));
    }

    @DexIgnore
    public fg5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[8], (RTLImageView) objArr[16], (RTLImageView) objArr[19], (RTLImageView) objArr[17], (RTLImageView) objArr[20], (RTLImageView) objArr[21], (ImageView) objArr[3], (ImageView) objArr[2], (RTLImageView) objArr[18], (ImageView) objArr[1], (FlexibleProgressBar) objArr[7], (View) objArr[6], (View) objArr[5], (View) objArr[4]);
        this.M = -1;
        this.q.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.M = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.M != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.M = 1;
        }
        w();
    }
}
