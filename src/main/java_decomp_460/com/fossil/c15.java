package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class c15 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ PresetRepository.Anon7.Anon1_Level2 b;
    @DexIgnore
    public /* final */ /* synthetic */ List c;
    @DexIgnore
    public /* final */ /* synthetic */ String d;
    @DexIgnore
    public /* final */ /* synthetic */ PresetDataSource.GetActivePresetCallback e;

    @DexIgnore
    public /* synthetic */ c15(PresetRepository.Anon7.Anon1_Level2 anon1_Level2, List list, String str, PresetDataSource.GetActivePresetCallback getActivePresetCallback) {
        this.b = anon1_Level2;
        this.c = list;
        this.d = str;
        this.e = getActivePresetCallback;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c, this.d, this.e);
    }
}
