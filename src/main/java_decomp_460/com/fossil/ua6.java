package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ua6 implements Factory<sa6> {
    @DexIgnore
    public static sa6 a(ta6 ta6) {
        sa6 a2 = ta6.a();
        lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
