package com.fossil;

import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ze1<A, B> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ fk1<b<A>, B> f4459a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends fk1<b<A>, B> {
        @DexIgnore
        public a(ze1 ze1, long j) {
            super(j);
        }

        @DexIgnore
        /* renamed from: n */
        public void j(b<A> bVar, B b) {
            bVar.c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<A> {
        @DexIgnore
        public static /* final */ Queue<b<?>> d; // = jk1.f(0);

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f4460a;
        @DexIgnore
        public int b;
        @DexIgnore
        public A c;

        @DexIgnore
        public static <A> b<A> a(A a2, int i, int i2) {
            b<A> bVar;
            synchronized (d) {
                bVar = (b<A>) d.poll();
            }
            if (bVar == null) {
                bVar = new b<>();
            }
            bVar.b(a2, i, i2);
            return bVar;
        }

        @DexIgnore
        public final void b(A a2, int i, int i2) {
            this.c = a2;
            this.b = i;
            this.f4460a = i2;
        }

        @DexIgnore
        public void c() {
            synchronized (d) {
                d.offer(this);
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return this.b == bVar.b && this.f4460a == bVar.f4460a && this.c.equals(bVar.c);
        }

        @DexIgnore
        public int hashCode() {
            return (((this.f4460a * 31) + this.b) * 31) + this.c.hashCode();
        }
    }

    @DexIgnore
    public ze1(long j) {
        this.f4459a = new a(this, j);
    }

    @DexIgnore
    public B a(A a2, int i, int i2) {
        b<A> a3 = b.a(a2, i, i2);
        B g = this.f4459a.g(a3);
        a3.c();
        return g;
    }

    @DexIgnore
    public void b(A a2, int i, int i2, B b2) {
        this.f4459a.k(b.a(a2, i, i2), b2);
    }
}
