package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y75 extends x75 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d V; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray W;
    @DexIgnore
    public long U;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        W = sparseIntArray;
        sparseIntArray.put(2131362093, 1);
        W.put(2131363049, 2);
        W.put(2131363173, 3);
        W.put(2131362122, 4);
        W.put(2131363410, 5);
        W.put(2131363316, 6);
        W.put(2131362926, 7);
        W.put(2131362087, 8);
        W.put(2131362396, 9);
        W.put(2131362665, 10);
        W.put(2131362501, 11);
        W.put(2131362406, 12);
        W.put(2131362252, 13);
        W.put(2131362028, 14);
        W.put(2131362816, 15);
        W.put(2131362256, 16);
        W.put(2131363369, 17);
        W.put(2131362808, 18);
        W.put(2131362255, 19);
        W.put(2131363338, 20);
        W.put(2131362806, 21);
        W.put(2131362253, 22);
        W.put(2131363324, 23);
        W.put(2131362826, 24);
        W.put(2131362258, 25);
        W.put(2131363396, 26);
        W.put(2131362822, 27);
        W.put(2131362257, 28);
        W.put(2131363387, 29);
    }
    */

    @DexIgnore
    public y75(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 30, V, W));
    }

    @DexIgnore
    public y75(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (View) objArr[14], (ConstraintLayout) objArr[8], (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[13], (CustomizeWidget) objArr[22], (CustomizeWidget) objArr[19], (CustomizeWidget) objArr[16], (CustomizeWidget) objArr[28], (CustomizeWidget) objArr[25], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[12], (FlexibleButton) objArr[11], (RTLImageView) objArr[10], (LinearLayout) objArr[21], (LinearLayout) objArr[18], (LinearLayout) objArr[15], (LinearLayout) objArr[27], (LinearLayout) objArr[24], (ProgressBar) objArr[7], (ConstraintLayout) objArr[0], (ViewPager2) objArr[2], (TabLayout) objArr[3], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[23], (FlexibleTextView) objArr[20], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[29], (FlexibleTextView) objArr[26], (FlexibleTextView) objArr[5]);
        this.U = -1;
        this.K.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.U = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.U != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.U = 1;
        }
        w();
    }
}
