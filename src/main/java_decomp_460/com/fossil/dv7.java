package com.fossil;

import com.fossil.rn7;
import com.fossil.tn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dv7 extends nn7 implements rn7 {
    @DexIgnore
    public static /* final */ a b; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends on7<rn7, dv7> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.dv7$a$a")
        /* renamed from: com.fossil.dv7$a$a  reason: collision with other inner class name */
        public static final class C0059a extends qq7 implements rp7<tn7.b, dv7> {
            @DexIgnore
            public static /* final */ C0059a INSTANCE; // = new C0059a();

            @DexIgnore
            public C0059a() {
                super(1);
            }

            @DexIgnore
            public final dv7 invoke(tn7.b bVar) {
                return (dv7) (!(bVar instanceof dv7) ? null : bVar);
            }
        }

        @DexIgnore
        public a() {
            super(rn7.p, C0059a.INSTANCE);
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public dv7() {
        super(rn7.p);
    }

    @DexIgnore
    public abstract void M(tn7 tn7, Runnable runnable);

    @DexIgnore
    public void P(tn7 tn7, Runnable runnable) {
        M(tn7, runnable);
    }

    @DexIgnore
    public boolean Q(tn7 tn7) {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.rn7
    public void a(qn7<?> qn7) {
        if (qn7 != null) {
            lu7<?> n = ((vv7) qn7).n();
            if (n != null) {
                n.o();
                return;
            }
            return;
        }
        throw new il7("null cannot be cast to non-null type kotlinx.coroutines.DispatchedContinuation<*>");
    }

    @DexIgnore
    @Override // com.fossil.rn7
    public final <T> qn7<T> c(qn7<? super T> qn7) {
        return new vv7(this, qn7);
    }

    @DexIgnore
    @Override // com.fossil.tn7, com.fossil.tn7.b, com.fossil.nn7
    public <E extends tn7.b> E get(tn7.c<E> cVar) {
        return (E) rn7.a.a(this, cVar);
    }

    @DexIgnore
    @Override // com.fossil.tn7, com.fossil.nn7
    public tn7 minusKey(tn7.c<?> cVar) {
        return rn7.a.b(this, cVar);
    }

    @DexIgnore
    public String toString() {
        return ov7.a(this) + '@' + ov7.b(this);
    }
}
