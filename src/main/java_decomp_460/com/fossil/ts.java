package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ts extends fs {
    @DexIgnore
    public rp7<? super c2, tl7> A;
    @DexIgnore
    public vp7<? super byte[], ? super n6, tl7> B;
    @DexIgnore
    public long C;

    @DexIgnore
    public ts(k5 k5Var) {
        super(hs.l, k5Var, 0, 4);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void f(long j) {
        this.C = j;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void i(p7 p7Var) {
        if (p7Var.f2790a == f5.DISCONNECTED) {
            wc.b.c(this.y.x);
        }
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void s(o7 o7Var) {
        rp7<? super c2, tl7> rp7;
        Object jSONObject;
        byte[] bArr = o7Var.b;
        n6 n6Var = o7Var.f2638a;
        vp7<? super byte[], ? super n6, tl7> vp7 = this.B;
        if (vp7 != null) {
            vp7.invoke(bArr, n6Var);
        }
        if (n6Var == n6.ASYNC) {
            c2 a2 = wc.b.a(this.y.x, bArr);
            if (a2 == null || a2.d) {
                d90.i.d(new a90("streaming", v80.f, this.y.x, this.c, this.d, true, null, null, null, g80.k(g80.k(new JSONObject(), jd0.S0, dy1.e(bArr, null, 1, null)), jd0.l1, (a2 == null || (jSONObject = a2.toJSONObject()) == null) ? JSONObject.NULL : jSONObject), 448));
            }
            if (a2 != null && (rp7 = this.A) != null) {
                rp7.invoke(a2);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.fs
    public u5 w() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public long x() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void y() {
    }
}
