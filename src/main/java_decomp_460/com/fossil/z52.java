package com.fossil;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.IntentSender;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.pc2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z52 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<z52> CREATOR; // = new bg2();
    @DexIgnore
    public static /* final */ z52 f; // = new z52(0);
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ PendingIntent d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public z52(int i) {
        this(i, null, null);
    }

    @DexIgnore
    public z52(int i, int i2, PendingIntent pendingIntent, String str) {
        this.b = i;
        this.c = i2;
        this.d = pendingIntent;
        this.e = str;
    }

    @DexIgnore
    public z52(int i, PendingIntent pendingIntent) {
        this(i, pendingIntent, null);
    }

    @DexIgnore
    public z52(int i, PendingIntent pendingIntent, String str) {
        this(1, i, pendingIntent, str);
    }

    @DexIgnore
    public static String F(int i) {
        if (i == 99) {
            return "UNFINISHED";
        }
        if (i == 1500) {
            return "DRIVE_EXTERNAL_STORAGE_REQUIRED";
        }
        switch (i) {
            case -1:
                return "UNKNOWN";
            case 0:
                return "SUCCESS";
            case 1:
                return "SERVICE_MISSING";
            case 2:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            case 3:
                return "SERVICE_DISABLED";
            case 4:
                return "SIGN_IN_REQUIRED";
            case 5:
                return "INVALID_ACCOUNT";
            case 6:
                return "RESOLUTION_REQUIRED";
            case 7:
                return "NETWORK_ERROR";
            case 8:
                return "INTERNAL_ERROR";
            case 9:
                return "SERVICE_INVALID";
            case 10:
                return "DEVELOPER_ERROR";
            case 11:
                return "LICENSE_CHECK_FAILED";
            default:
                switch (i) {
                    case 13:
                        return "CANCELED";
                    case 14:
                        return "TIMEOUT";
                    case 15:
                        return "INTERRUPTED";
                    case 16:
                        return "API_UNAVAILABLE";
                    case 17:
                        return "SIGN_IN_FAILED";
                    case 18:
                        return "SERVICE_UPDATING";
                    case 19:
                        return "SERVICE_MISSING_PERMISSION";
                    case 20:
                        return "RESTRICTED_PROFILE";
                    case 21:
                        return "API_VERSION_UPDATE_REQUIRED";
                    default:
                        StringBuilder sb = new StringBuilder(31);
                        sb.append("UNKNOWN_ERROR_CODE(");
                        sb.append(i);
                        sb.append(")");
                        return sb.toString();
                }
        }
    }

    @DexIgnore
    public final boolean A() {
        return this.c == 0;
    }

    @DexIgnore
    public final void D(Activity activity, int i) throws IntentSender.SendIntentException {
        if (k()) {
            activity.startIntentSenderForResult(this.d.getIntentSender(), i, null, 0, 0, 0);
        }
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof z52)) {
            return false;
        }
        z52 z52 = (z52) obj;
        return this.c == z52.c && pc2.a(this.d, z52.d) && pc2.a(this.e, z52.e);
    }

    @DexIgnore
    public final String f() {
        return this.e;
    }

    @DexIgnore
    public final PendingIntent h() {
        return this.d;
    }

    @DexIgnore
    public final int hashCode() {
        return pc2.b(Integer.valueOf(this.c), this.d, this.e);
    }

    @DexIgnore
    public final boolean k() {
        return (this.c == 0 || this.d == null) ? false : true;
    }

    @DexIgnore
    public final String toString() {
        pc2.a c2 = pc2.c(this);
        c2.a("statusCode", F(this.c));
        c2.a("resolution", this.d);
        c2.a("message", this.e);
        return c2.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.n(parcel, 1, this.b);
        bd2.n(parcel, 2, c());
        bd2.t(parcel, 3, h(), i, false);
        bd2.u(parcel, 4, f(), false);
        bd2.b(parcel, a2);
    }
}
