package com.fossil;

import com.fossil.a34;
import com.fossil.d14;
import com.fossil.x44;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.j2objc.annotations.Weak;
import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x34 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ d14.b f4035a; // = b24.f385a.l(SimpleComparison.EQUAL_TO_OPERATION);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends t14<K, V> {
        @DexIgnore
        public /* final */ /* synthetic */ Map.Entry b;

        @DexIgnore
        public a(Map.Entry entry) {
            this.b = entry;
        }

        @DexIgnore
        @Override // java.util.Map.Entry, com.fossil.t14
        public K getKey() {
            return (K) this.b.getKey();
        }

        @DexIgnore
        @Override // java.util.Map.Entry, com.fossil.t14
        public V getValue() {
            return (V) this.b.getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends h54<Map.Entry<K, V>> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator b;

        @DexIgnore
        public b(Iterator it) {
            this.b = it;
        }

        @DexIgnore
        /* renamed from: a */
        public Map.Entry<K, V> next() {
            return x34.q((Map.Entry) this.b.next());
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext();
        }
    }

    @DexIgnore
    public enum c implements b14<Map.Entry<?, ?>, Object> {
        KEY {
            @DexIgnore
            public Object apply(Map.Entry<?, ?> entry) {
                return entry.getKey();
            }
        },
        VALUE {
            @DexIgnore
            public Object apply(Map.Entry<?, ?> entry) {
                return entry.getValue();
            }
        };

        @DexIgnore
        public /* synthetic */ c(w34 w34) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.b14
        @CanIgnoreReturnValue
        public abstract /* synthetic */ T apply(F f);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<K, V> extends x44.a<Map.Entry<K, V>> {
        @DexIgnore
        public abstract Map<K, V> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public abstract boolean contains(Object obj);

        @DexIgnore
        public boolean isEmpty() {
            return a().isEmpty();
        }

        @DexIgnore
        @Override // com.fossil.x44.a, java.util.AbstractCollection, java.util.Collection, java.util.AbstractSet, java.util.Set
        public boolean removeAll(Collection<?> collection) {
            try {
                i14.l(collection);
                return super.removeAll(collection);
            } catch (UnsupportedOperationException e) {
                return x44.g(this, collection.iterator());
            }
        }

        @DexIgnore
        @Override // com.fossil.x44.a, java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean retainAll(Collection<?> collection) {
            try {
                i14.l(collection);
                return super.retainAll(collection);
            } catch (UnsupportedOperationException e) {
                HashSet d = x44.d(collection.size());
                for (Object obj : collection) {
                    if (contains(obj)) {
                        d.add(((Map.Entry) obj).getKey());
                    }
                }
                return a().keySet().retainAll(d);
            }
        }

        @DexIgnore
        public int size() {
            return a().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<K, V> extends x44.a<K> {
        @DexIgnore
        @Weak
        public /* final */ Map<K, V> b;

        @DexIgnore
        public e(Map<K, V> map) {
            i14.l(map);
            this.b = map;
        }

        @DexIgnore
        public Map<K, V> a() {
            return this.b;
        }

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return a().containsKey(obj);
        }

        @DexIgnore
        public boolean isEmpty() {
            return a().isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<K> iterator() {
            return x34.h(a().entrySet().iterator());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            if (!contains(obj)) {
                return false;
            }
            a().remove(obj);
            return true;
        }

        @DexIgnore
        public int size() {
            return a().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f<K, V> extends AbstractCollection<V> {
        @DexIgnore
        @Weak
        public /* final */ Map<K, V> b;

        @DexIgnore
        public f(Map<K, V> map) {
            i14.l(map);
            this.b = map;
        }

        @DexIgnore
        public final Map<K, V> a() {
            return this.b;
        }

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return a().containsValue(obj);
        }

        @DexIgnore
        public boolean isEmpty() {
            return a().isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            return x34.t(a().entrySet().iterator());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            try {
                return super.remove(obj);
            } catch (UnsupportedOperationException e) {
                for (Map.Entry<K, V> entry : a().entrySet()) {
                    if (f14.a(obj, entry.getValue())) {
                        a().remove(entry.getKey());
                        return true;
                    }
                }
                return false;
            }
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean removeAll(Collection<?> collection) {
            try {
                i14.l(collection);
                return super.removeAll(collection);
            } catch (UnsupportedOperationException e) {
                HashSet c = x44.c();
                for (Map.Entry<K, V> entry : a().entrySet()) {
                    if (collection.contains(entry.getValue())) {
                        c.add(entry.getKey());
                    }
                }
                return a().keySet().removeAll(c);
            }
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean retainAll(Collection<?> collection) {
            try {
                i14.l(collection);
                return super.retainAll(collection);
            } catch (UnsupportedOperationException e) {
                HashSet c = x44.c();
                for (Map.Entry<K, V> entry : a().entrySet()) {
                    if (collection.contains(entry.getValue())) {
                        c.add(entry.getKey());
                    }
                }
                return a().keySet().retainAll(c);
            }
        }

        @DexIgnore
        public int size() {
            return a().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class g<K, V> extends AbstractMap<K, V> {
        @DexIgnore
        public transient Set<Map.Entry<K, V>> b;
        @DexIgnore
        public transient Collection<V> c;

        @DexIgnore
        public abstract Set<Map.Entry<K, V>> a();

        @DexIgnore
        public Collection<V> b() {
            return new f(this);
        }

        @DexIgnore
        @Override // java.util.AbstractMap, java.util.Map
        public Set<Map.Entry<K, V>> entrySet() {
            Set<Map.Entry<K, V>> set = this.b;
            if (set != null) {
                return set;
            }
            Set<Map.Entry<K, V>> a2 = a();
            this.b = a2;
            return a2;
        }

        @DexIgnore
        @Override // java.util.AbstractMap, java.util.Map
        public Collection<V> values() {
            Collection<V> collection = this.c;
            if (collection != null) {
                return collection;
            }
            Collection<V> b2 = b();
            this.c = b2;
            return b2;
        }
    }

    @DexIgnore
    public static int a(int i) {
        if (i < 3) {
            a24.b(i, "expectedSize");
            return i + 1;
        } else if (i < 1073741824) {
            return (int) ((((float) i) / 0.75f) + 1.0f);
        } else {
            return Integer.MAX_VALUE;
        }
    }

    @DexIgnore
    public static boolean b(Map<?, ?> map, Object obj) {
        return p34.f(h(map.entrySet().iterator()), obj);
    }

    @DexIgnore
    public static boolean c(Map<?, ?> map, Object obj) {
        return p34.f(t(map.entrySet().iterator()), obj);
    }

    @DexIgnore
    public static boolean d(Map<?, ?> map, Object obj) {
        if (map == obj) {
            return true;
        }
        if (obj instanceof Map) {
            return map.entrySet().equals(((Map) obj).entrySet());
        }
        return false;
    }

    @DexIgnore
    public static <K, V> Map.Entry<K, V> e(K k, V v) {
        return new v24(k, v);
    }

    @DexIgnore
    public static <E> a34<E, Integer> f(Collection<E> collection) {
        a34.b bVar = new a34.b(collection.size());
        int i = 0;
        for (E e2 : collection) {
            bVar.c(e2, Integer.valueOf(i));
            i++;
        }
        return bVar.a();
    }

    @DexIgnore
    public static <K> b14<Map.Entry<K, ?>, K> g() {
        return c.KEY;
    }

    @DexIgnore
    public static <K, V> Iterator<K> h(Iterator<Map.Entry<K, V>> it) {
        return p34.w(it, g());
    }

    @DexIgnore
    public static <K> K i(Map.Entry<K, ?> entry) {
        if (entry == null) {
            return null;
        }
        return entry.getKey();
    }

    @DexIgnore
    public static <K, V> HashMap<K, V> j() {
        return new HashMap<>();
    }

    @DexIgnore
    public static <K, V> LinkedHashMap<K, V> k(int i) {
        return new LinkedHashMap<>(a(i));
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.Map<K, V> */
    /* JADX WARN: Multi-variable type inference failed */
    public static <K, V> void l(Map<K, V> map, Map<? extends K, ? extends V> map2) {
        for (Map.Entry<? extends K, ? extends V> entry : map2.entrySet()) {
            map.put(entry.getKey(), entry.getValue());
        }
    }

    @DexIgnore
    public static boolean m(Map<?, ?> map, Object obj) {
        i14.l(map);
        try {
            return map.containsKey(obj);
        } catch (ClassCastException | NullPointerException e2) {
            return false;
        }
    }

    @DexIgnore
    public static <V> V n(Map<?, V> map, Object obj) {
        i14.l(map);
        try {
            return map.get(obj);
        } catch (ClassCastException | NullPointerException e2) {
            return null;
        }
    }

    @DexIgnore
    public static <V> V o(Map<?, V> map, Object obj) {
        i14.l(map);
        try {
            return map.remove(obj);
        } catch (ClassCastException | NullPointerException e2) {
            return null;
        }
    }

    @DexIgnore
    public static String p(Map<?, ?> map) {
        StringBuilder c2 = b24.c(map.size());
        c2.append('{');
        f4035a.d(c2, map);
        c2.append('}');
        return c2.toString();
    }

    @DexIgnore
    public static <K, V> Map.Entry<K, V> q(Map.Entry<? extends K, ? extends V> entry) {
        i14.l(entry);
        return new a(entry);
    }

    @DexIgnore
    public static <K, V> h54<Map.Entry<K, V>> r(Iterator<Map.Entry<K, V>> it) {
        return new b(it);
    }

    @DexIgnore
    public static <V> b14<Map.Entry<?, V>, V> s() {
        return c.VALUE;
    }

    @DexIgnore
    public static <K, V> Iterator<V> t(Iterator<Map.Entry<K, V>> it) {
        return p34.w(it, s());
    }
}
