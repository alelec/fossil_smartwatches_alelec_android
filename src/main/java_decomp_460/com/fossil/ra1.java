package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ra1 {
    LOW(0.5f),
    NORMAL(1.0f),
    HIGH(1.5f);
    
    @DexIgnore
    public /* final */ float multiplier;

    @DexIgnore
    public ra1(float f) {
        this.multiplier = f;
    }

    @DexIgnore
    public float getMultiplier() {
        return this.multiplier;
    }
}
