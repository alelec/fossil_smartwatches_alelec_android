package com.fossil;

import com.fossil.v18;
import java.util.HashMap;
import java.util.Map;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yc0 implements Interceptor {
    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) {
        v18.a h = chain.c().h();
        rp7<v18, HashMap<String, String>> j = id0.i.j();
        if (j != null) {
            v18 c = chain.c();
            pq7.b(c, "chain.request()");
            for (Map.Entry<String, String> entry : j.invoke(c).entrySet()) {
                h.a(entry.getKey(), entry.getValue());
            }
        }
        Response d = chain.d(h.b());
        pq7.b(d, "chain.proceed(requestBuilder.build())");
        return d;
    }
}
