package com.fossil;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.jc2;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dc2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<dc2> CREATOR; // = new fe2();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public String e;
    @DexIgnore
    public IBinder f;
    @DexIgnore
    public Scope[] g;
    @DexIgnore
    public Bundle h;
    @DexIgnore
    public Account i;
    @DexIgnore
    public b62[] j;
    @DexIgnore
    public b62[] k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public int m;

    @DexIgnore
    public dc2(int i2) {
        this.b = 4;
        this.d = d62.f740a;
        this.c = i2;
        this.l = true;
    }

    @DexIgnore
    public dc2(int i2, int i3, int i4, String str, IBinder iBinder, Scope[] scopeArr, Bundle bundle, Account account, b62[] b62Arr, b62[] b62Arr2, boolean z, int i5) {
        this.b = i2;
        this.c = i3;
        this.d = i4;
        if ("com.google.android.gms".equals(str)) {
            this.e = "com.google.android.gms";
        } else {
            this.e = str;
        }
        if (i2 < 2) {
            this.i = iBinder != null ? wb2.i(jc2.a.e(iBinder)) : null;
        } else {
            this.f = iBinder;
            this.i = account;
        }
        this.g = scopeArr;
        this.h = bundle;
        this.j = b62Arr;
        this.k = b62Arr2;
        this.l = z;
        this.m = i5;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = bd2.a(parcel);
        bd2.n(parcel, 1, this.b);
        bd2.n(parcel, 2, this.c);
        bd2.n(parcel, 3, this.d);
        bd2.u(parcel, 4, this.e, false);
        bd2.m(parcel, 5, this.f, false);
        bd2.x(parcel, 6, this.g, i2, false);
        bd2.e(parcel, 7, this.h, false);
        bd2.t(parcel, 8, this.i, i2, false);
        bd2.x(parcel, 10, this.j, i2, false);
        bd2.x(parcel, 11, this.k, i2, false);
        bd2.c(parcel, 12, this.l);
        bd2.n(parcel, 13, this.m);
        bd2.b(parcel, a2);
    }
}
