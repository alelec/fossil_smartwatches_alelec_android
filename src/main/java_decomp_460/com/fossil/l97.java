package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l97 implements Factory<k97> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<j97> f2161a;

    @DexIgnore
    public l97(Provider<j97> provider) {
        this.f2161a = provider;
    }

    @DexIgnore
    public static l97 a(Provider<j97> provider) {
        return new l97(provider);
    }

    @DexIgnore
    public static k97 c(j97 j97) {
        return new k97(j97);
    }

    @DexIgnore
    /* renamed from: b */
    public k97 get() {
        return c(this.f2161a.get());
    }
}
