package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a86 implements Factory<z76> {
    @DexIgnore
    public static z76 a(w76 w76, on5 on5, UserRepository userRepository) {
        return new z76(w76, on5, userRepository);
    }
}
