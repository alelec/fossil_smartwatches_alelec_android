package com.fossil;

import androidx.lifecycle.MutableLiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r16 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public MutableLiveData<a> f3066a; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f3067a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public a(int i, boolean z) {
            this.f3067a = i;
            this.b = z;
        }

        @DexIgnore
        public final int a() {
            return this.f3067a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!(this.f3067a == aVar.f3067a && this.b == aVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.f3067a;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            return i2 + (i * 31);
        }

        @DexIgnore
        public String toString() {
            return "NotificationSetting(type=" + this.f3067a + ", isCall=" + this.b + ")";
        }
    }

    @DexIgnore
    public final MutableLiveData<a> a() {
        return this.f3066a;
    }
}
