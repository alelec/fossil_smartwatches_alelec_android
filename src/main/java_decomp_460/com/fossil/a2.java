package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class a2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f184a;

    /*
    static {
        int[] iArr = new int[lt.values().length];
        f184a = iArr;
        iArr[lt.JSON_FILE_EVENT.ordinal()] = 1;
        f184a[lt.HEARTBEAT_EVENT.ordinal()] = 2;
        f184a[lt.CONNECTION_PARAM_CHANGE_EVENT.ordinal()] = 3;
        f184a[lt.APP_NOTIFICATION_EVENT.ordinal()] = 4;
        f184a[lt.MUSIC_EVENT.ordinal()] = 5;
        f184a[lt.BACKGROUND_SYNC_EVENT.ordinal()] = 6;
        f184a[lt.SERVICE_CHANGE_EVENT.ordinal()] = 7;
        f184a[lt.MICRO_APP_EVENT.ordinal()] = 8;
        f184a[lt.TIME_SYNC_EVENT.ordinal()] = 9;
        f184a[lt.AUTHENTICATION_REQUEST_EVENT.ordinal()] = 10;
        f184a[lt.BATTERY_EVENT.ordinal()] = 11;
        f184a[lt.ENCRYPTED_DATA.ordinal()] = 12;
    }
    */
}
