package com.fossil;

public final class cv7 {

    /* renamed from: a  reason: collision with root package name */
    public static final boolean f672a;

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        if (r0.equals("on") == false) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0053, code lost:
        if (r0.equals("") != false) goto L_0x0008;
     */
    /*
    static {
        /*
            java.lang.String r0 = "kotlinx.coroutines.scheduler"
            java.lang.String r0 = com.fossil.wz7.d(r0)
            if (r0 != 0) goto L_0x000c
        L_0x0008:
            r0 = 1
        L_0x0009:
            com.fossil.cv7.f672a = r0
            return
        L_0x000c:
            int r1 = r0.hashCode()
            if (r1 == 0) goto L_0x004d
            r2 = 3551(0xddf, float:4.976E-42)
            if (r1 == r2) goto L_0x0025
            r2 = 109935(0x1ad6f, float:1.54052E-40)
            if (r1 != r2) goto L_0x002d
            java.lang.String r1 = "off"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x002d
            r0 = 0
            goto L_0x0009
        L_0x0025:
            java.lang.String r1 = "on"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x0008
        L_0x002d:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "System property 'kotlinx.coroutines.scheduler' has unrecognized value '"
            r1.append(r2)
            r1.append(r0)
            r0 = 39
            r1.append(r0)
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = r1.toString()
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x004d:
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x002d
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cv7.<clinit>():void");
    }
    */

    public static final dv7 a() {
        return f672a ? h08.i : tu7.e;
    }

    public static final String b(tn7 tn7) {
        String str;
        if (!nv7.c()) {
            return null;
        }
        gv7 gv7 = (gv7) tn7.get(gv7.c);
        if (gv7 == null) {
            return null;
        }
        hv7 hv7 = (hv7) tn7.get(hv7.c);
        if (hv7 == null || (str = hv7.M()) == null) {
            str = "coroutine";
        }
        return str + '#' + gv7.M();
    }

    public static final tn7 c(iv7 iv7, tn7 tn7) {
        tn7 plus = iv7.h().plus(tn7);
        tn7 plus2 = nv7.c() ? plus.plus(new gv7(nv7.b().incrementAndGet())) : plus;
        return (plus == bw7.a() || plus.get(rn7.p) != null) ? plus2 : plus2.plus(bw7.a());
    }
}
