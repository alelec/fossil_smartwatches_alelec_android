package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n2 implements Parcelable.Creator<o2> {
    @DexIgnore
    public /* synthetic */ n2(kq7 kq7) {
    }

    @DexIgnore
    public o2 a(Parcel parcel) {
        return new o2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public o2 createFromParcel(Parcel parcel) {
        return new o2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public o2[] newArray(int i) {
        return new o2[i];
    }
}
