package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ij2 implements Parcelable.Creator<gj2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ gj2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        IBinder iBinder = null;
        uh2 uh2 = null;
        long j = 0;
        long j2 = 0;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                uh2 = (uh2) ad2.e(parcel, t, uh2.CREATOR);
            } else if (l == 2) {
                iBinder = ad2.u(parcel, t);
            } else if (l == 3) {
                j2 = ad2.y(parcel, t);
            } else if (l != 4) {
                ad2.B(parcel, t);
            } else {
                j = ad2.y(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new gj2(uh2, iBinder, j2, j);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ gj2[] newArray(int i) {
        return new gj2[i];
    }
}
