package com.fossil;

import android.os.Handler;
import android.os.Looper;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class u5 implements gd0<h7> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3515a;
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public /* final */ n5 c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public s5 e;
    @DexIgnore
    public rp7<? super u5, tl7> f;
    @DexIgnore
    public rp7<? super u5, tl7> g;
    @DexIgnore
    public gp7<tl7> h;
    @DexIgnore
    public /* final */ v5 i;
    @DexIgnore
    public /* final */ n4 j;

    @DexIgnore
    public u5(v5 v5Var, n4 n4Var) {
        this.i = v5Var;
        this.j = n4Var;
        this.f3515a = ey1.a(v5Var);
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.b = new Handler(myLooper);
            this.c = n5.NORMAL;
            this.e = new s5(this.i, r5.c, null, 4);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final u5 a(gp7<tl7> gp7) {
        this.h = gp7;
        return this;
    }

    @DexIgnore
    public JSONObject b(boolean z) {
        return new JSONObject();
    }

    @DexIgnore
    public final void c() {
        if (!this.d) {
            this.d = true;
            fd0<h7> j2 = j();
            if (j2 != null) {
                j2.d(this);
            }
            r5 r5Var = r5.b;
            s5 s5Var = this.e;
            if (r5Var == s5Var.c) {
                m80.c.a(this.f3515a, "Command success: %s.", ox1.toJSONString$default(s5Var, 0, 1, null));
                rp7<? super u5, tl7> rp7 = this.f;
                if (rp7 != null) {
                    rp7.invoke(this);
                }
            } else {
                m80.c.b(this.f3515a, "Command error: %s.", ox1.toJSONString$default(s5Var, 0, 1, null));
                rp7<? super u5, tl7> rp72 = this.g;
                if (rp72 != null) {
                    rp72.invoke(this);
                }
            }
            gp7<tl7> gp7 = this.h;
            if (gp7 != null) {
                gp7.invoke();
            }
        }
    }

    @DexIgnore
    public abstract void d(k5 k5Var);

    @DexIgnore
    public final void e(r5 r5Var) {
        if (!this.d) {
            m80.c.a(this.f3515a, "command stop: %s.", r5Var);
            this.e = s5.a(this.e, null, r5Var, null, 5);
            if (l()) {
                this.b.postDelayed(new t5(this), 5000);
            } else {
                c();
            }
        }
    }

    @DexIgnore
    public void f(h7 h7Var) {
        k(h7Var);
        s5 a2 = s5.e.a(h7Var.f1435a);
        this.e = s5.a(this.e, null, a2.c, a2.d, 1);
    }

    @DexIgnore
    public void g(Object obj) {
        h7 h7Var = (h7) obj;
        if (i(h7Var)) {
            r5 r5Var = this.e.c;
            if (r5Var == r5.m || r5Var == r5.f) {
                this.e = s5.a(this.e, null, null, h7Var.f1435a, 3);
            } else {
                f(h7Var);
            }
            c();
        }
    }

    @DexIgnore
    public n5 h() {
        return this.c;
    }

    @DexIgnore
    public abstract boolean i(h7 h7Var);

    @DexIgnore
    public abstract fd0<h7> j();

    @DexIgnore
    public void k(h7 h7Var) {
    }

    @DexIgnore
    public boolean l() {
        return false;
    }
}
