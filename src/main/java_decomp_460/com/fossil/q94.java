package com.fossil;

import android.content.Context;
import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.ta4;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q94 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ y84 f2942a;
    @DexIgnore
    public /* final */ sb4 b;
    @DexIgnore
    public /* final */ mc4 c;
    @DexIgnore
    public /* final */ v94 d;
    @DexIgnore
    public /* final */ s94 e;
    @DexIgnore
    public String f;

    @DexIgnore
    public q94(y84 y84, sb4 sb4, mc4 mc4, v94 v94, s94 s94) {
        this.f2942a = y84;
        this.b = sb4;
        this.c = mc4;
        this.d = v94;
        this.e = s94;
    }

    @DexIgnore
    public static q94 b(Context context, h94 h94, tb4 tb4, l84 l84, v94 v94, s94 s94, kd4 kd4, rc4 rc4) {
        return new q94(new y84(context, h94, l84, kd4), new sb4(new File(tb4.a()), rc4), mc4.a(context), v94, s94);
    }

    @DexIgnore
    public static List<ta4.b> e(Map<String, String> map) {
        ArrayList arrayList = new ArrayList();
        arrayList.ensureCapacity(map.size());
        for (Map.Entry<String, String> entry : map.entrySet()) {
            ta4.b.a a2 = ta4.b.a();
            a2.b(entry.getKey());
            a2.c(entry.getValue());
            arrayList.add(a2.a());
        }
        Collections.sort(arrayList, p94.a());
        return arrayList;
    }

    @DexIgnore
    public void c(String str, List<l94> list) {
        ArrayList arrayList = new ArrayList();
        for (l94 l94 : list) {
            ta4.c.b c2 = l94.c();
            if (c2 != null) {
                arrayList.add(c2);
            }
        }
        sb4 sb4 = this.b;
        ta4.c.a a2 = ta4.c.a();
        a2.b(ua4.a(arrayList));
        sb4.j(str, a2.a());
    }

    @DexIgnore
    public void d(long j) {
        this.b.i(this.f, j);
    }

    @DexIgnore
    public void g(String str, long j) {
        this.f = str;
        this.b.B(this.f2942a.c(str, j));
    }

    @DexIgnore
    public void h() {
        this.f = null;
    }

    @DexIgnore
    public final boolean i(nt3<z84> nt3) {
        if (nt3.q()) {
            z84 m = nt3.m();
            x74 f2 = x74.f();
            f2.b("Crashlytics report successfully enqueued to DataTransport: " + m.c());
            this.b.h(m.c());
            return true;
        }
        x74.f().c("Crashlytics report could not be enqueued to DataTransport", nt3.l());
        return false;
    }

    @DexIgnore
    public final void j(Throwable th, Thread thread, String str, long j, boolean z) {
        String str2 = this.f;
        if (str2 == null) {
            x74.f().b("Cannot persist event, no currently open session");
            return;
        }
        boolean equals = str.equals(CrashDumperPlugin.NAME);
        ta4.d.AbstractC0224d b2 = this.f2942a.b(th, thread, str, j, 4, 8, z);
        ta4.d.AbstractC0224d.b g = b2.g();
        String d2 = this.d.d();
        if (d2 != null) {
            ta4.d.AbstractC0224d.AbstractC0235d.a a2 = ta4.d.AbstractC0224d.AbstractC0235d.a();
            a2.b(d2);
            g.d(a2.a());
        } else {
            x74.f().b("No log data to include with this event.");
        }
        List<ta4.b> e2 = e(this.e.a());
        if (!e2.isEmpty()) {
            ta4.d.AbstractC0224d.a.AbstractC0225a f2 = b2.b().f();
            f2.c(ua4.a(e2));
            g.b(f2.a());
        }
        this.b.A(g.a(), str2, equals);
    }

    @DexIgnore
    public void k(Throwable th, Thread thread, long j) {
        j(th, thread, CrashDumperPlugin.NAME, j, true);
    }

    @DexIgnore
    public void l() {
        String str = this.f;
        if (str == null) {
            x74.f().b("Could not persist user ID; no current session");
            return;
        }
        String b2 = this.e.b();
        if (b2 == null) {
            x74.f().b("Could not persist user ID; no user ID available");
        } else {
            this.b.C(b2, str);
        }
    }

    @DexIgnore
    public void m() {
        this.b.g();
    }

    @DexIgnore
    public nt3<Void> n(Executor executor, d94 d94) {
        if (d94 == d94.NONE) {
            x74.f().b("Send via DataTransport disabled. Removing DataTransport reports.");
            this.b.g();
            return qt3.f(null);
        }
        List<z84> x = this.b.x();
        ArrayList arrayList = new ArrayList();
        for (z84 z84 : x) {
            if (z84.b().k() != ta4.e.NATIVE || d94 == d94.ALL) {
                arrayList.add(this.c.e(z84).i(executor, o94.a(this)));
            } else {
                x74.f().b("Send native reports via DataTransport disabled. Removing DataTransport reports.");
                this.b.h(z84.c());
            }
        }
        return qt3.g(arrayList);
    }
}
