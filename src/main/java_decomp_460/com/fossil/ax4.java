package com.fossil;

import androidx.lifecycle.MutableLiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ax4 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ hu4 f357a;

    /*
    static {
        pq7.b(ax4.class.getSimpleName(), "BCTabsViewModel::class.java.simpleName");
    }
    */

    @DexIgnore
    public ax4(on5 on5, hu4 hu4) {
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(hu4, "mSocialProfileRepository");
        this.f357a = hu4;
        new MutableLiveData();
        this.f357a.e();
    }
}
