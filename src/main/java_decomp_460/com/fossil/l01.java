package com.fossil;

import android.view.View;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager2.widget.ViewPager2;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l01 extends ViewPager2.i {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ LinearLayoutManager f2125a;
    @DexIgnore
    public ViewPager2.k b;

    @DexIgnore
    public l01(LinearLayoutManager linearLayoutManager) {
        this.f2125a = linearLayoutManager;
    }

    @DexIgnore
    @Override // androidx.viewpager2.widget.ViewPager2.i
    public void a(int i) {
    }

    @DexIgnore
    @Override // androidx.viewpager2.widget.ViewPager2.i
    public void b(int i, float f, int i2) {
        if (this.b != null) {
            float f2 = -f;
            for (int i3 = 0; i3 < this.f2125a.K(); i3++) {
                View J = this.f2125a.J(i3);
                if (J != null) {
                    this.b.a(J, ((float) (this.f2125a.i0(J) - i)) + f2);
                } else {
                    throw new IllegalStateException(String.format(Locale.US, "LayoutManager returned a null child at pos %d/%d while transforming pages", Integer.valueOf(i3), Integer.valueOf(this.f2125a.K())));
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.viewpager2.widget.ViewPager2.i
    public void c(int i) {
    }

    @DexIgnore
    public ViewPager2.k d() {
        return this.b;
    }

    @DexIgnore
    public void e(ViewPager2.k kVar) {
        this.b = kVar;
    }
}
