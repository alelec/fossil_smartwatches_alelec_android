package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.view.FlexibleButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pg5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ConstraintLayout f2829a;
    @DexIgnore
    public /* final */ FlexibleButton b;
    @DexIgnore
    public /* final */ FlexibleButton c;
    @DexIgnore
    public /* final */ ConstraintLayout d;
    @DexIgnore
    public /* final */ ViewPager2 e;

    @DexIgnore
    public pg5(ConstraintLayout constraintLayout, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, ConstraintLayout constraintLayout2, ViewPager2 viewPager2) {
        this.f2829a = constraintLayout;
        this.b = flexibleButton;
        this.c = flexibleButton2;
        this.d = constraintLayout2;
        this.e = viewPager2;
    }

    @DexIgnore
    public static pg5 a(View view) {
        int i = 2131363527;
        FlexibleButton flexibleButton = (FlexibleButton) view.findViewById(2131362265);
        if (flexibleButton != null) {
            FlexibleButton flexibleButton2 = (FlexibleButton) view.findViewById(2131362284);
            if (flexibleButton2 != null) {
                ConstraintLayout constraintLayout = (ConstraintLayout) view;
                ViewPager2 viewPager2 = (ViewPager2) view.findViewById(2131363527);
                if (viewPager2 != null) {
                    return new pg5(constraintLayout, flexibleButton, flexibleButton2, constraintLayout, viewPager2);
                }
            } else {
                i = 2131362284;
            }
        } else {
            i = 2131362265;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @DexIgnore
    public static pg5 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    @DexIgnore
    public static pg5 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(2131558848, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    @DexIgnore
    public ConstraintLayout b() {
        return this.f2829a;
    }
}
