package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iz1 extends oz1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ long f1697a;

    @DexIgnore
    public iz1(long j) {
        this.f1697a = j;
    }

    @DexIgnore
    @Override // com.fossil.oz1
    public long a() {
        return this.f1697a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof oz1) {
            return this.f1697a == ((oz1) obj).a();
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.f1697a;
        return ((int) (j ^ (j >>> 32))) ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "LogResponse{nextRequestWaitMillis=" + this.f1697a + "}";
    }
}
