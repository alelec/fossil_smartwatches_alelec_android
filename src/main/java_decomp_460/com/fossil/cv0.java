package com.fossil;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.animation.Interpolator;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cv0 extends RecyclerView.l implements RecyclerView.n {
    @DexIgnore
    public g A;
    @DexIgnore
    public /* final */ RecyclerView.p B; // = new b();
    @DexIgnore
    public Rect C;
    @DexIgnore
    public long D;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<View> f661a; // = new ArrayList();
    @DexIgnore
    public /* final */ float[] b; // = new float[2];
    @DexIgnore
    public RecyclerView.ViewHolder c; // = null;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float k;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public f m;
    @DexIgnore
    public int n; // = 0;
    @DexIgnore
    public int o;
    @DexIgnore
    public List<h> p; // = new ArrayList();
    @DexIgnore
    public int q;
    @DexIgnore
    public RecyclerView r;
    @DexIgnore
    public /* final */ Runnable s; // = new a();
    @DexIgnore
    public VelocityTracker t;
    @DexIgnore
    public List<RecyclerView.ViewHolder> u;
    @DexIgnore
    public List<Integer> v;
    @DexIgnore
    public RecyclerView.h w; // = null;
    @DexIgnore
    public View x; // = null;
    @DexIgnore
    public int y; // = -1;
    @DexIgnore
    public vn0 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            cv0 cv0 = cv0.this;
            if (cv0.c != null && cv0.y()) {
                cv0 cv02 = cv0.this;
                RecyclerView.ViewHolder viewHolder = cv02.c;
                if (viewHolder != null) {
                    cv02.t(viewHolder);
                }
                cv0 cv03 = cv0.this;
                cv03.r.removeCallbacks(cv03.s);
                mo0.d0(cv0.this.r, this);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements RecyclerView.p {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.p
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            int i = 1;
            cv0.this.z.a(motionEvent);
            VelocityTracker velocityTracker = cv0.this.t;
            if (velocityTracker != null) {
                velocityTracker.addMovement(motionEvent);
            }
            if (cv0.this.l != -1) {
                int actionMasked = motionEvent.getActionMasked();
                int findPointerIndex = motionEvent.findPointerIndex(cv0.this.l);
                if (findPointerIndex >= 0) {
                    cv0.this.i(actionMasked, motionEvent, findPointerIndex);
                }
                cv0 cv0 = cv0.this;
                RecyclerView.ViewHolder viewHolder = cv0.c;
                if (viewHolder != null) {
                    if (actionMasked != 1) {
                        if (actionMasked != 2) {
                            if (actionMasked == 3) {
                                VelocityTracker velocityTracker2 = cv0.t;
                                if (velocityTracker2 != null) {
                                    velocityTracker2.clear();
                                }
                            } else if (actionMasked == 6) {
                                int actionIndex = motionEvent.getActionIndex();
                                if (motionEvent.getPointerId(actionIndex) == cv0.this.l) {
                                    if (actionIndex != 0) {
                                        i = 0;
                                    }
                                    cv0.this.l = motionEvent.getPointerId(i);
                                    cv0 cv02 = cv0.this;
                                    cv02.E(motionEvent, cv02.o, actionIndex);
                                    return;
                                }
                                return;
                            } else {
                                return;
                            }
                        } else if (findPointerIndex >= 0) {
                            cv0.E(motionEvent, cv0.o, findPointerIndex);
                            cv0.this.t(viewHolder);
                            cv0 cv03 = cv0.this;
                            cv03.r.removeCallbacks(cv03.s);
                            cv0.this.s.run();
                            cv0.this.r.invalidate();
                            return;
                        } else {
                            return;
                        }
                    }
                    cv0.this.z(null, 0);
                    cv0.this.l = -1;
                }
            }
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.p
        public boolean c(RecyclerView recyclerView, MotionEvent motionEvent) {
            int findPointerIndex;
            h m;
            cv0.this.z.a(motionEvent);
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked == 0) {
                cv0.this.l = motionEvent.getPointerId(0);
                cv0.this.d = motionEvent.getX();
                cv0.this.e = motionEvent.getY();
                cv0.this.u();
                cv0 cv0 = cv0.this;
                if (cv0.c == null && (m = cv0.m(motionEvent)) != null) {
                    cv0 cv02 = cv0.this;
                    cv02.d -= m.i;
                    cv02.e -= m.j;
                    cv02.l(m.e, true);
                    if (cv0.this.f661a.remove(m.e.itemView)) {
                        cv0 cv03 = cv0.this;
                        cv03.m.c(cv03.r, m.e);
                    }
                    cv0.this.z(m.e, m.f);
                    cv0 cv04 = cv0.this;
                    cv04.E(motionEvent, cv04.o, 0);
                }
            } else if (actionMasked == 3 || actionMasked == 1) {
                cv0 cv05 = cv0.this;
                cv05.l = -1;
                cv05.z(null, 0);
            } else {
                int i = cv0.this.l;
                if (i != -1 && (findPointerIndex = motionEvent.findPointerIndex(i)) >= 0) {
                    cv0.this.i(actionMasked, motionEvent, findPointerIndex);
                }
            }
            VelocityTracker velocityTracker = cv0.this.t;
            if (velocityTracker != null) {
                velocityTracker.addMovement(motionEvent);
            }
            return cv0.this.c != null;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.p
        public void e(boolean z) {
            if (z) {
                cv0.this.z(null, 0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends h {
        @DexIgnore
        public /* final */ /* synthetic */ int n;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder o;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(RecyclerView.ViewHolder viewHolder, int i, int i2, float f, float f2, float f3, float f4, int i3, RecyclerView.ViewHolder viewHolder2) {
            super(viewHolder, i, i2, f, f2, f3, f4);
            this.n = i3;
            this.o = viewHolder2;
        }

        @DexIgnore
        @Override // com.fossil.cv0.h
        public void onAnimationEnd(Animator animator) {
            super.onAnimationEnd(animator);
            if (!this.k) {
                if (this.n <= 0) {
                    cv0 cv0 = cv0.this;
                    cv0.m.c(cv0.r, this.o);
                } else {
                    cv0.this.f661a.add(this.o.itemView);
                    this.h = true;
                    int i = this.n;
                    if (i > 0) {
                        cv0.this.v(this, i);
                    }
                }
                cv0 cv02 = cv0.this;
                View view = cv02.x;
                View view2 = this.o.itemView;
                if (view == view2) {
                    cv02.x(view2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ h b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public d(h hVar, int i) {
            this.b = hVar;
            this.c = i;
        }

        @DexIgnore
        public void run() {
            RecyclerView recyclerView = cv0.this.r;
            if (recyclerView != null && recyclerView.isAttachedToWindow()) {
                h hVar = this.b;
                if (!hVar.k && hVar.e.getAdapterPosition() != -1) {
                    RecyclerView.j itemAnimator = cv0.this.r.getItemAnimator();
                    if ((itemAnimator == null || !itemAnimator.isRunning(null)) && !cv0.this.r()) {
                        cv0.this.m.B(this.b.e, this.c);
                    } else {
                        cv0.this.r.post(this);
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements RecyclerView.h {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.h
        public int a(int i, int i2) {
            cv0 cv0 = cv0.this;
            View view = cv0.x;
            if (view == null) {
                return i2;
            }
            int i3 = cv0.y;
            if (i3 == -1) {
                i3 = cv0.r.indexOfChild(view);
                cv0.this.y = i3;
            }
            return i2 == i + -1 ? i3 : i2 >= i3 ? i2 + 1 : i2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f {
        @DexIgnore
        public static /* final */ Interpolator b; // = new a();
        @DexIgnore
        public static /* final */ Interpolator c; // = new b();

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f664a; // = -1;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Interpolator {
            @DexIgnore
            public float getInterpolation(float f) {
                return f * f * f * f * f;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements Interpolator {
            @DexIgnore
            public float getInterpolation(float f) {
                float f2 = f - 1.0f;
                return (f2 * f2 * f2 * f2 * f2) + 1.0f;
            }
        }

        @DexIgnore
        public static int e(int i, int i2) {
            int i3;
            int i4 = i & 789516;
            if (i4 == 0) {
                return i;
            }
            int i5 = i & i4;
            if (i2 == 0) {
                i3 = i4 << 2;
            } else {
                int i6 = i4 << 1;
                i5 |= -789517 & i6;
                i3 = (i6 & 789516) << 2;
            }
            return i5 | i3;
        }

        @DexIgnore
        public static int s(int i, int i2) {
            return i2 << (i * 8);
        }

        @DexIgnore
        public static int t(int i, int i2) {
            return s(0, i2 | i) | s(1, i2) | s(2, i);
        }

        @DexIgnore
        public void A(RecyclerView.ViewHolder viewHolder, int i) {
            if (viewHolder != null) {
                ev0.f990a.b(viewHolder.itemView);
            }
        }

        @DexIgnore
        public abstract void B(RecyclerView.ViewHolder viewHolder, int i);

        @DexIgnore
        public boolean a(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
            return true;
        }

        @DexIgnore
        public RecyclerView.ViewHolder b(RecyclerView.ViewHolder viewHolder, List<RecyclerView.ViewHolder> list, int i, int i2) {
            int i3;
            int i4;
            int i5;
            int bottom;
            int top;
            int left;
            int abs;
            int right;
            int width = viewHolder.itemView.getWidth();
            int height = viewHolder.itemView.getHeight();
            int left2 = i - viewHolder.itemView.getLeft();
            int top2 = i2 - viewHolder.itemView.getTop();
            int size = list.size();
            RecyclerView.ViewHolder viewHolder2 = null;
            int i6 = -1;
            int i7 = 0;
            while (i7 < size) {
                RecyclerView.ViewHolder viewHolder3 = list.get(i7);
                if (left2 <= 0 || (right = viewHolder3.itemView.getRight() - (i + width)) >= 0 || viewHolder3.itemView.getRight() <= viewHolder.itemView.getRight() || (i3 = Math.abs(right)) <= i6) {
                    i3 = i6;
                } else {
                    viewHolder2 = viewHolder3;
                }
                if (left2 < 0 && (left = viewHolder3.itemView.getLeft() - i) > 0 && viewHolder3.itemView.getLeft() < viewHolder.itemView.getLeft() && (abs = Math.abs(left)) > i3) {
                    i3 = abs;
                    viewHolder2 = viewHolder3;
                }
                if (top2 >= 0 || (top = viewHolder3.itemView.getTop() - i2) <= 0 || viewHolder3.itemView.getTop() >= viewHolder.itemView.getTop() || (i4 = Math.abs(top)) <= i3) {
                    i4 = i3;
                } else {
                    viewHolder2 = viewHolder3;
                }
                if (top2 <= 0 || (bottom = viewHolder3.itemView.getBottom() - (i2 + height)) >= 0 || viewHolder3.itemView.getBottom() <= viewHolder.itemView.getBottom() || (i5 = Math.abs(bottom)) <= i4) {
                    i5 = i4;
                    viewHolder3 = viewHolder2;
                }
                i7++;
                i6 = i5;
                viewHolder2 = viewHolder3;
            }
            return viewHolder2;
        }

        @DexIgnore
        public void c(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            ev0.f990a.a(viewHolder.itemView);
        }

        @DexIgnore
        public int d(int i, int i2) {
            int i3;
            int i4 = i & 3158064;
            if (i4 == 0) {
                return i;
            }
            int i5 = i & i4;
            if (i2 == 0) {
                i3 = i4 >> 2;
            } else {
                int i6 = i4 >> 1;
                i5 |= -3158065 & i6;
                i3 = (i6 & 3158064) >> 2;
            }
            return i5 | i3;
        }

        @DexIgnore
        public final int f(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return d(k(recyclerView, viewHolder), mo0.z(recyclerView));
        }

        @DexIgnore
        public long g(RecyclerView recyclerView, int i, float f, float f2) {
            RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
            return itemAnimator == null ? i == 8 ? 200 : 250 : i == 8 ? itemAnimator.getMoveDuration() : itemAnimator.getRemoveDuration();
        }

        @DexIgnore
        public int h() {
            return 0;
        }

        @DexIgnore
        public final int i(RecyclerView recyclerView) {
            if (this.f664a == -1) {
                this.f664a = recyclerView.getResources().getDimensionPixelSize(pu0.item_touch_helper_max_drag_scroll_per_frame);
            }
            return this.f664a;
        }

        @DexIgnore
        public float j(RecyclerView.ViewHolder viewHolder) {
            return 0.5f;
        }

        @DexIgnore
        public abstract int k(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder);

        @DexIgnore
        public float l(float f) {
            return f;
        }

        @DexIgnore
        public float m(RecyclerView.ViewHolder viewHolder) {
            return 0.5f;
        }

        @DexIgnore
        public float n(float f) {
            return f;
        }

        @DexIgnore
        public boolean o(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return (f(recyclerView, viewHolder) & 16711680) != 0;
        }

        @DexIgnore
        public int p(RecyclerView recyclerView, int i, int i2, int i3, long j) {
            float f = 1.0f;
            int i4 = (int) (((float) (i(recyclerView) * ((int) Math.signum((float) i2)))) * c.getInterpolation(Math.min(1.0f, (((float) Math.abs(i2)) * 1.0f) / ((float) i))));
            if (j <= 2000) {
                f = ((float) j) / 2000.0f;
            }
            int interpolation = (int) (b.getInterpolation(f) * ((float) i4));
            return interpolation == 0 ? i2 > 0 ? 1 : -1 : interpolation;
        }

        @DexIgnore
        public boolean q() {
            return true;
        }

        @DexIgnore
        public boolean r() {
            return true;
        }

        @DexIgnore
        public void u(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
            ev0.f990a.d(canvas, recyclerView, viewHolder.itemView, f, f2, i, z);
        }

        @DexIgnore
        public void v(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
            ev0.f990a.c(canvas, recyclerView, viewHolder.itemView, f, f2, i, z);
        }

        @DexIgnore
        public void w(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, List<h> list, int i, float f, float f2) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                h hVar = list.get(i2);
                hVar.e();
                int save = canvas.save();
                u(canvas, recyclerView, hVar.e, hVar.i, hVar.j, hVar.f, false);
                canvas.restoreToCount(save);
            }
            if (viewHolder != null) {
                int save2 = canvas.save();
                u(canvas, recyclerView, viewHolder, f, f2, i, true);
                canvas.restoreToCount(save2);
            }
        }

        @DexIgnore
        public void x(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, List<h> list, int i, float f, float f2) {
            boolean z;
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                h hVar = list.get(i2);
                int save = canvas.save();
                v(canvas, recyclerView, hVar.e, hVar.i, hVar.j, hVar.f, false);
                canvas.restoreToCount(save);
            }
            if (viewHolder != null) {
                int save2 = canvas.save();
                v(canvas, recyclerView, viewHolder, f, f2, i, true);
                canvas.restoreToCount(save2);
            }
            boolean z2 = false;
            int i3 = size - 1;
            while (i3 >= 0) {
                h hVar2 = list.get(i3);
                if (!hVar2.l || hVar2.h) {
                    z = !hVar2.l ? true : z2;
                } else {
                    list.remove(i3);
                    z = z2;
                }
                i3--;
                z2 = z;
            }
            if (z2) {
                recyclerView.invalidate();
            }
        }

        @DexIgnore
        public abstract boolean y(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2);

        @DexIgnore
        public void z(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, int i, RecyclerView.ViewHolder viewHolder2, int i2, int i3, int i4) {
            RecyclerView.m layoutManager = recyclerView.getLayoutManager();
            if (layoutManager instanceof j) {
                ((j) layoutManager).b(viewHolder.itemView, viewHolder2.itemView, i3, i4);
                return;
            }
            if (layoutManager.l()) {
                if (layoutManager.R(viewHolder2.itemView) <= recyclerView.getPaddingLeft()) {
                    recyclerView.scrollToPosition(i2);
                }
                if (layoutManager.U(viewHolder2.itemView) >= recyclerView.getWidth() - recyclerView.getPaddingRight()) {
                    recyclerView.scrollToPosition(i2);
                }
            }
            if (layoutManager.m()) {
                if (layoutManager.V(viewHolder2.itemView) <= recyclerView.getPaddingTop()) {
                    recyclerView.scrollToPosition(i2);
                }
                if (layoutManager.P(viewHolder2.itemView) >= recyclerView.getHeight() - recyclerView.getPaddingBottom()) {
                    recyclerView.scrollToPosition(i2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public boolean b; // = true;

        @DexIgnore
        public g() {
        }

        @DexIgnore
        public void a() {
            this.b = false;
        }

        @DexIgnore
        public boolean onDown(MotionEvent motionEvent) {
            return true;
        }

        @DexIgnore
        public void onLongPress(MotionEvent motionEvent) {
            View n;
            RecyclerView.ViewHolder childViewHolder;
            int i;
            if (this.b && (n = cv0.this.n(motionEvent)) != null && (childViewHolder = cv0.this.r.getChildViewHolder(n)) != null) {
                cv0 cv0 = cv0.this;
                if (cv0.m.o(cv0.r, childViewHolder) && motionEvent.getPointerId(0) == (i = cv0.this.l)) {
                    int findPointerIndex = motionEvent.findPointerIndex(i);
                    float x = motionEvent.getX(findPointerIndex);
                    float y = motionEvent.getY(findPointerIndex);
                    cv0 cv02 = cv0.this;
                    cv02.d = x;
                    cv02.e = y;
                    cv02.i = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    cv02.h = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    if (cv02.m.r()) {
                        cv0.this.z(childViewHolder, 2);
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h implements Animator.AnimatorListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ float f665a;
        @DexIgnore
        public /* final */ float b;
        @DexIgnore
        public /* final */ float c;
        @DexIgnore
        public /* final */ float d;
        @DexIgnore
        public /* final */ RecyclerView.ViewHolder e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ ValueAnimator g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public float i;
        @DexIgnore
        public float j;
        @DexIgnore
        public boolean k; // = false;
        @DexIgnore
        public boolean l; // = false;
        @DexIgnore
        public float m;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements ValueAnimator.AnimatorUpdateListener {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                h.this.c(valueAnimator.getAnimatedFraction());
            }
        }

        @DexIgnore
        public h(RecyclerView.ViewHolder viewHolder, int i2, int i3, float f2, float f3, float f4, float f5) {
            this.f = i3;
            this.e = viewHolder;
            this.f665a = f2;
            this.b = f3;
            this.c = f4;
            this.d = f5;
            ValueAnimator ofFloat = ValueAnimator.ofFloat(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
            this.g = ofFloat;
            ofFloat.addUpdateListener(new a());
            this.g.setTarget(viewHolder.itemView);
            this.g.addListener(this);
            c(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }

        @DexIgnore
        public void a() {
            this.g.cancel();
        }

        @DexIgnore
        public void b(long j2) {
            this.g.setDuration(j2);
        }

        @DexIgnore
        public void c(float f2) {
            this.m = f2;
        }

        @DexIgnore
        public void d() {
            this.e.setIsRecyclable(false);
            this.g.start();
        }

        @DexIgnore
        public void e() {
            float f2 = this.f665a;
            float f3 = this.c;
            if (f2 == f3) {
                this.i = this.e.itemView.getTranslationX();
            } else {
                this.i = f2 + ((f3 - f2) * this.m);
            }
            float f4 = this.b;
            float f5 = this.d;
            if (f4 == f5) {
                this.j = this.e.itemView.getTranslationY();
                return;
            }
            this.j = f4 + ((f5 - f4) * this.m);
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            c(1.0f);
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            if (!this.l) {
                this.e.setIsRecyclable(true);
            }
            this.l = true;
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class i extends f {
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;

        @DexIgnore
        public i(int i, int i2) {
            this.d = i2;
            this.e = i;
        }

        @DexIgnore
        public int C(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return this.e;
        }

        @DexIgnore
        public int D(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return this.d;
        }

        @DexIgnore
        @Override // com.fossil.cv0.f
        public int k(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return f.t(C(recyclerView, viewHolder), D(recyclerView, viewHolder));
        }
    }

    @DexIgnore
    public interface j {
        @DexIgnore
        void b(View view, View view2, int i, int i2);
    }

    @DexIgnore
    public cv0(f fVar) {
        this.m = fVar;
    }

    @DexIgnore
    public static boolean s(View view, float f2, float f3, float f4, float f5) {
        return f2 >= f4 && f2 <= ((float) view.getWidth()) + f4 && f3 >= f5 && f3 <= ((float) view.getHeight()) + f5;
    }

    @DexIgnore
    public final void A() {
        this.q = ViewConfiguration.get(this.r.getContext()).getScaledTouchSlop();
        this.r.addItemDecoration(this);
        this.r.addOnItemTouchListener(this.B);
        this.r.addOnChildAttachStateChangeListener(this);
        B();
    }

    @DexIgnore
    public final void B() {
        this.A = new g();
        this.z = new vn0(this.r.getContext(), this.A);
    }

    @DexIgnore
    public final void C() {
        g gVar = this.A;
        if (gVar != null) {
            gVar.a();
            this.A = null;
        }
        if (this.z != null) {
            this.z = null;
        }
    }

    @DexIgnore
    public final int D(RecyclerView.ViewHolder viewHolder) {
        if (this.n == 2) {
            return 0;
        }
        int k2 = this.m.k(this.r, viewHolder);
        int d2 = (this.m.d(k2, mo0.z(this.r)) & 65280) >> 8;
        if (d2 == 0) {
            return 0;
        }
        int i2 = (k2 & 65280) >> 8;
        if (Math.abs(this.h) > Math.abs(this.i)) {
            int h2 = h(viewHolder, d2);
            if (h2 > 0) {
                return (i2 & h2) == 0 ? f.e(h2, mo0.z(this.r)) : h2;
            }
            int j2 = j(viewHolder, d2);
            if (j2 > 0) {
                return j2;
            }
        } else {
            int j3 = j(viewHolder, d2);
            if (j3 > 0) {
                return j3;
            }
            int h3 = h(viewHolder, d2);
            if (h3 > 0) {
                return (i2 & h3) == 0 ? f.e(h3, mo0.z(this.r)) : h3;
            }
        }
        return 0;
    }

    @DexIgnore
    public void E(MotionEvent motionEvent, int i2, int i3) {
        float x2 = motionEvent.getX(i3);
        float y2 = motionEvent.getY(i3);
        float f2 = x2 - this.d;
        this.h = f2;
        this.i = y2 - this.e;
        if ((i2 & 4) == 0) {
            this.h = Math.max((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2);
        }
        if ((i2 & 8) == 0) {
            this.h = Math.min((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.h);
        }
        if ((i2 & 1) == 0) {
            this.i = Math.max((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.i);
        }
        if ((i2 & 2) == 0) {
            this.i = Math.min((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.i);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.n
    public void b(View view) {
        x(view);
        RecyclerView.ViewHolder childViewHolder = this.r.getChildViewHolder(view);
        if (childViewHolder != null) {
            RecyclerView.ViewHolder viewHolder = this.c;
            if (viewHolder == null || childViewHolder != viewHolder) {
                l(childViewHolder, false);
                if (this.f661a.remove(childViewHolder.itemView)) {
                    this.m.c(this.r, childViewHolder);
                    return;
                }
                return;
            }
            z(null, 0);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.n
    public void d(View view) {
    }

    @DexIgnore
    public final void f() {
        if (Build.VERSION.SDK_INT < 21) {
            if (this.w == null) {
                this.w = new e();
            }
            this.r.setChildDrawingOrderCallback(this.w);
        }
    }

    @DexIgnore
    public void g(RecyclerView recyclerView) {
        RecyclerView recyclerView2 = this.r;
        if (recyclerView2 != recyclerView) {
            if (recyclerView2 != null) {
                k();
            }
            this.r = recyclerView;
            if (recyclerView != null) {
                Resources resources = recyclerView.getResources();
                this.f = resources.getDimension(pu0.item_touch_helper_swipe_escape_velocity);
                this.g = resources.getDimension(pu0.item_touch_helper_swipe_escape_max_velocity);
                A();
            }
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.l
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        rect.setEmpty();
    }

    @DexIgnore
    public final int h(RecyclerView.ViewHolder viewHolder, int i2) {
        int i3 = 8;
        if ((i2 & 12) != 0) {
            int i4 = this.h > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 8 : 4;
            VelocityTracker velocityTracker = this.t;
            if (velocityTracker != null && this.l > -1) {
                velocityTracker.computeCurrentVelocity(1000, this.m.n(this.g));
                float xVelocity = this.t.getXVelocity(this.l);
                float yVelocity = this.t.getYVelocity(this.l);
                if (xVelocity <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    i3 = 4;
                }
                float abs = Math.abs(xVelocity);
                if ((i3 & i2) != 0 && i4 == i3 && abs >= this.m.l(this.f) && abs > Math.abs(yVelocity)) {
                    return i3;
                }
            }
            float width = (float) this.r.getWidth();
            float m2 = this.m.m(viewHolder);
            if ((i2 & i4) != 0 && Math.abs(this.h) > width * m2) {
                return i4;
            }
        }
        return 0;
    }

    @DexIgnore
    public void i(int i2, MotionEvent motionEvent, int i3) {
        RecyclerView.ViewHolder p2;
        int f2;
        if (this.c == null && i2 == 2 && this.n != 2 && this.m.q() && this.r.getScrollState() != 1 && (p2 = p(motionEvent)) != null && (f2 = (this.m.f(this.r, p2) & 65280) >> 8) != 0) {
            float x2 = motionEvent.getX(i3);
            float y2 = motionEvent.getY(i3);
            float f3 = x2 - this.d;
            float f4 = y2 - this.e;
            float abs = Math.abs(f3);
            float abs2 = Math.abs(f4);
            int i4 = this.q;
            if (abs >= ((float) i4) || abs2 >= ((float) i4)) {
                if (abs > abs2) {
                    if (f3 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (f2 & 4) == 0) {
                        return;
                    }
                    if (f3 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (f2 & 8) == 0) {
                        return;
                    }
                } else if (f4 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (f2 & 1) == 0) {
                    return;
                } else {
                    if (f4 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (f2 & 2) == 0) {
                        return;
                    }
                }
                this.i = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                this.h = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                this.l = motionEvent.getPointerId(0);
                z(p2, 1);
            }
        }
    }

    @DexIgnore
    public final int j(RecyclerView.ViewHolder viewHolder, int i2) {
        int i3 = 2;
        if ((i2 & 3) != 0) {
            int i4 = this.i > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 2 : 1;
            VelocityTracker velocityTracker = this.t;
            if (velocityTracker != null && this.l > -1) {
                velocityTracker.computeCurrentVelocity(1000, this.m.n(this.g));
                float xVelocity = this.t.getXVelocity(this.l);
                float yVelocity = this.t.getYVelocity(this.l);
                if (yVelocity <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    i3 = 1;
                }
                float abs = Math.abs(yVelocity);
                if ((i3 & i2) != 0 && i3 == i4 && abs >= this.m.l(this.f) && abs > Math.abs(xVelocity)) {
                    return i3;
                }
            }
            float height = (float) this.r.getHeight();
            float m2 = this.m.m(viewHolder);
            if ((i2 & i4) != 0 && Math.abs(this.i) > height * m2) {
                return i4;
            }
        }
        return 0;
    }

    @DexIgnore
    public final void k() {
        this.r.removeItemDecoration(this);
        this.r.removeOnItemTouchListener(this.B);
        this.r.removeOnChildAttachStateChangeListener(this);
        for (int size = this.p.size() - 1; size >= 0; size--) {
            this.m.c(this.r, this.p.get(0).e);
        }
        this.p.clear();
        this.x = null;
        this.y = -1;
        w();
        C();
    }

    @DexIgnore
    public void l(RecyclerView.ViewHolder viewHolder, boolean z2) {
        for (int size = this.p.size() - 1; size >= 0; size--) {
            h hVar = this.p.get(size);
            if (hVar.e == viewHolder) {
                hVar.k |= z2;
                if (!hVar.l) {
                    hVar.a();
                }
                this.p.remove(size);
                return;
            }
        }
    }

    @DexIgnore
    public h m(MotionEvent motionEvent) {
        if (this.p.isEmpty()) {
            return null;
        }
        View n2 = n(motionEvent);
        for (int size = this.p.size() - 1; size >= 0; size--) {
            h hVar = this.p.get(size);
            if (hVar.e.itemView == n2) {
                return hVar;
            }
        }
        return null;
    }

    @DexIgnore
    public View n(MotionEvent motionEvent) {
        float x2 = motionEvent.getX();
        float y2 = motionEvent.getY();
        RecyclerView.ViewHolder viewHolder = this.c;
        if (viewHolder != null) {
            View view = viewHolder.itemView;
            if (s(view, x2, y2, this.j + this.h, this.k + this.i)) {
                return view;
            }
        }
        for (int size = this.p.size() - 1; size >= 0; size--) {
            h hVar = this.p.get(size);
            View view2 = hVar.e.itemView;
            if (s(view2, x2, y2, hVar.i, hVar.j)) {
                return view2;
            }
        }
        return this.r.findChildViewUnder(x2, y2);
    }

    @DexIgnore
    public final List<RecyclerView.ViewHolder> o(RecyclerView.ViewHolder viewHolder) {
        List<RecyclerView.ViewHolder> list = this.u;
        if (list == null) {
            this.u = new ArrayList();
            this.v = new ArrayList();
        } else {
            list.clear();
            this.v.clear();
        }
        int h2 = this.m.h();
        int round = Math.round(this.j + this.h) - h2;
        int round2 = Math.round(this.k + this.i) - h2;
        int i2 = h2 * 2;
        int width = viewHolder.itemView.getWidth() + round + i2;
        int height = viewHolder.itemView.getHeight() + round2 + i2;
        int i3 = (round + width) / 2;
        int i4 = (round2 + height) / 2;
        RecyclerView.m layoutManager = this.r.getLayoutManager();
        int K = layoutManager.K();
        for (int i5 = 0; i5 < K; i5++) {
            View J = layoutManager.J(i5);
            if (J != viewHolder.itemView && J.getBottom() >= round2 && J.getTop() <= height && J.getRight() >= round && J.getLeft() <= width) {
                RecyclerView.ViewHolder childViewHolder = this.r.getChildViewHolder(J);
                if (this.m.a(this.r, this.c, childViewHolder)) {
                    int abs = Math.abs(i3 - ((J.getLeft() + J.getRight()) / 2));
                    int abs2 = Math.abs(i4 - ((J.getBottom() + J.getTop()) / 2));
                    int i6 = (abs * abs) + (abs2 * abs2);
                    int size = this.u.size();
                    int i7 = 0;
                    int i8 = 0;
                    while (i7 < size && i6 > this.v.get(i7).intValue()) {
                        i7++;
                        i8++;
                    }
                    this.u.add(i8, childViewHolder);
                    this.v.add(i8, Integer.valueOf(i6));
                }
            }
        }
        return this.u;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.l
    public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        float f2;
        float f3;
        this.y = -1;
        if (this.c != null) {
            q(this.b);
            float[] fArr = this.b;
            f3 = fArr[0];
            f2 = fArr[1];
        } else {
            f2 = 0.0f;
            f3 = 0.0f;
        }
        this.m.w(canvas, recyclerView, this.c, this.p, this.n, f3, f2);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.l
    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        float f2;
        float f3;
        if (this.c != null) {
            q(this.b);
            float[] fArr = this.b;
            f3 = fArr[0];
            f2 = fArr[1];
        } else {
            f2 = 0.0f;
            f3 = 0.0f;
        }
        this.m.x(canvas, recyclerView, this.c, this.p, this.n, f3, f2);
    }

    @DexIgnore
    public final RecyclerView.ViewHolder p(MotionEvent motionEvent) {
        View n2;
        RecyclerView.m layoutManager = this.r.getLayoutManager();
        int i2 = this.l;
        if (i2 == -1) {
            return null;
        }
        int findPointerIndex = motionEvent.findPointerIndex(i2);
        float x2 = motionEvent.getX(findPointerIndex);
        float f2 = this.d;
        float y2 = motionEvent.getY(findPointerIndex);
        float f3 = this.e;
        float abs = Math.abs(x2 - f2);
        float abs2 = Math.abs(y2 - f3);
        int i3 = this.q;
        if (abs < ((float) i3) && abs2 < ((float) i3)) {
            return null;
        }
        if (abs > abs2 && layoutManager.l()) {
            return null;
        }
        if ((abs2 <= abs || !layoutManager.m()) && (n2 = n(motionEvent)) != null) {
            return this.r.getChildViewHolder(n2);
        }
        return null;
    }

    @DexIgnore
    public final void q(float[] fArr) {
        if ((this.o & 12) != 0) {
            fArr[0] = (this.j + this.h) - ((float) this.c.itemView.getLeft());
        } else {
            fArr[0] = this.c.itemView.getTranslationX();
        }
        if ((this.o & 3) != 0) {
            fArr[1] = (this.k + this.i) - ((float) this.c.itemView.getTop());
        } else {
            fArr[1] = this.c.itemView.getTranslationY();
        }
    }

    @DexIgnore
    public boolean r() {
        int size = this.p.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (!this.p.get(i2).l) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void t(RecyclerView.ViewHolder viewHolder) {
        if (!this.r.isLayoutRequested() && this.n == 2) {
            float j2 = this.m.j(viewHolder);
            int i2 = (int) (this.j + this.h);
            int i3 = (int) (this.k + this.i);
            if (((float) Math.abs(i3 - viewHolder.itemView.getTop())) >= ((float) viewHolder.itemView.getHeight()) * j2 || ((float) Math.abs(i2 - viewHolder.itemView.getLeft())) >= j2 * ((float) viewHolder.itemView.getWidth())) {
                List<RecyclerView.ViewHolder> o2 = o(viewHolder);
                if (o2.size() != 0) {
                    RecyclerView.ViewHolder b2 = this.m.b(viewHolder, o2, i2, i3);
                    if (b2 == null) {
                        this.u.clear();
                        this.v.clear();
                        return;
                    }
                    int adapterPosition = b2.getAdapterPosition();
                    int adapterPosition2 = viewHolder.getAdapterPosition();
                    if (this.m.y(this.r, viewHolder, b2)) {
                        this.m.z(this.r, viewHolder, adapterPosition2, b2, adapterPosition, i2, i3);
                    }
                }
            }
        }
    }

    @DexIgnore
    public void u() {
        VelocityTracker velocityTracker = this.t;
        if (velocityTracker != null) {
            velocityTracker.recycle();
        }
        this.t = VelocityTracker.obtain();
    }

    @DexIgnore
    public void v(h hVar, int i2) {
        this.r.post(new d(hVar, i2));
    }

    @DexIgnore
    public final void w() {
        VelocityTracker velocityTracker = this.t;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.t = null;
        }
    }

    @DexIgnore
    public void x(View view) {
        if (view == this.x) {
            this.x = null;
            if (this.w != null) {
                this.r.setChildDrawingOrderCallback(null);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0051, code lost:
        if (r4 < 0) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0073, code lost:
        if (r8 < 0) goto L_0x0075;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00e2, code lost:
        if (r4 > 0) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x010a, code lost:
        if (r8 > 0) goto L_0x0075;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean y() {
        /*
        // Method dump skipped, instructions count: 280
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cv0.y():boolean");
    }

    @DexIgnore
    public void z(RecyclerView.ViewHolder viewHolder, int i2) {
        boolean z2;
        boolean z3;
        float signum;
        float f2;
        if (viewHolder != this.c || i2 != this.n) {
            this.D = Long.MIN_VALUE;
            int i3 = this.n;
            l(viewHolder, true);
            this.n = i2;
            if (i2 == 2) {
                if (viewHolder != null) {
                    this.x = viewHolder.itemView;
                    f();
                } else {
                    throw new IllegalArgumentException("Must pass a ViewHolder when dragging");
                }
            }
            RecyclerView.ViewHolder viewHolder2 = this.c;
            if (viewHolder2 != null) {
                if (viewHolder2.itemView.getParent() != null) {
                    int D2 = i3 == 2 ? 0 : D(viewHolder2);
                    w();
                    if (D2 == 1 || D2 == 2) {
                        signum = Math.signum(this.i) * ((float) this.r.getHeight());
                        f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    } else {
                        float signum2 = (D2 == 4 || D2 == 8 || D2 == 16 || D2 == 32) ? Math.signum(this.h) * ((float) this.r.getWidth()) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                        signum = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                        f2 = signum2;
                    }
                    int i4 = i3 == 2 ? 8 : D2 > 0 ? 2 : 4;
                    q(this.b);
                    float[] fArr = this.b;
                    float f3 = fArr[0];
                    float f4 = fArr[1];
                    c cVar = new c(viewHolder2, i4, i3, f3, f4, f2, signum, D2, viewHolder2);
                    cVar.b(this.m.g(this.r, i4, f2 - f3, signum - f4));
                    this.p.add(cVar);
                    cVar.d();
                    z3 = true;
                } else {
                    x(viewHolder2.itemView);
                    this.m.c(this.r, viewHolder2);
                    z3 = false;
                }
                this.c = null;
                z2 = z3;
            } else {
                z2 = false;
            }
            if (viewHolder != null) {
                this.o = (this.m.f(this.r, viewHolder) & ((1 << ((i2 * 8) + 8)) - 1)) >> (this.n * 8);
                this.j = (float) viewHolder.itemView.getLeft();
                this.k = (float) viewHolder.itemView.getTop();
                this.c = viewHolder;
                if (i2 == 2) {
                    viewHolder.itemView.performHapticFeedback(0);
                }
            }
            ViewParent parent = this.r.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(this.c != null);
            }
            if (!z2) {
                this.r.getLayoutManager().v1();
            }
            this.m.A(this.c, this.n);
            this.r.invalidate();
        }
    }
}
