package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ju1 {
    ON,
    OFF,
    TOGGLE;
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final ju1 a(String str) {
            ju1[] values = ju1.values();
            for (ju1 ju1 : values) {
                String a2 = ey1.a(ju1);
                String lowerCase = str.toLowerCase(hd0.y.e());
                pq7.b(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                if (pq7.a(a2, lowerCase)) {
                    return ju1;
                }
            }
            return null;
        }
    }
}
