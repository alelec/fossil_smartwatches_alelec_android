package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class v5 extends Enum<v5> {
    @DexIgnore
    public static /* final */ v5 b;
    @DexIgnore
    public static /* final */ v5 c;
    @DexIgnore
    public static /* final */ v5 d;
    @DexIgnore
    public static /* final */ v5 e;
    @DexIgnore
    public static /* final */ v5 f;
    @DexIgnore
    public static /* final */ v5 g;
    @DexIgnore
    public static /* final */ v5 h;
    @DexIgnore
    public static /* final */ v5 i;
    @DexIgnore
    public static /* final */ v5 j;
    @DexIgnore
    public static /* final */ v5 k;
    @DexIgnore
    public static /* final */ v5 l;
    @DexIgnore
    public static /* final */ v5 m;
    @DexIgnore
    public static /* final */ v5 n;
    @DexIgnore
    public static /* final */ v5 o;
    @DexIgnore
    public static /* final */ /* synthetic */ v5[] p;

    /*
    static {
        v5 v5Var = new v5("CLOSE", 0);
        b = v5Var;
        v5 v5Var2 = new v5("CONNECT", 1);
        c = v5Var2;
        v5 v5Var3 = new v5("DISCONNECT", 2);
        d = v5Var3;
        v5 v5Var4 = new v5("DISCOVER_SERVICE", 3);
        e = v5Var4;
        v5 v5Var5 = new v5("READ_CHARACTERISTIC", 4);
        f = v5Var5;
        v5 v5Var6 = new v5("READ_DESCRIPTOR", 5);
        v5 v5Var7 = new v5("REQUEST_MTU", 6);
        g = v5Var7;
        v5 v5Var8 = new v5("SUBSCRIBE_CHARACTERISTIC", 7);
        h = v5Var8;
        v5 v5Var9 = new v5("WRITE_CHARACTERISTIC", 8);
        i = v5Var9;
        v5 v5Var10 = new v5("WRITE_DESCRIPTOR", 9);
        v5 v5Var11 = new v5("READ_RSSI", 10);
        j = v5Var11;
        v5 v5Var12 = new v5("CREATE_BOND", 11);
        k = v5Var12;
        v5 v5Var13 = new v5("REMOVE_BOND", 12);
        l = v5Var13;
        v5 v5Var14 = new v5("CONNECT_HID", 13);
        m = v5Var14;
        v5 v5Var15 = new v5("DISCONNECT_HID", 14);
        n = v5Var15;
        v5 v5Var16 = new v5("UNKNOWN", 15);
        o = v5Var16;
        p = new v5[]{v5Var, v5Var2, v5Var3, v5Var4, v5Var5, v5Var6, v5Var7, v5Var8, v5Var9, v5Var10, v5Var11, v5Var12, v5Var13, v5Var14, v5Var15, v5Var16};
    }
    */

    @DexIgnore
    public v5(String str, int i2) {
    }

    @DexIgnore
    public static v5 valueOf(String str) {
        return (v5) Enum.valueOf(v5.class, str);
    }

    @DexIgnore
    public static v5[] values() {
        return (v5[]) p.clone();
    }
}
