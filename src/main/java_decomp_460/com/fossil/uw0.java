package com.fossil;

import android.annotation.SuppressLint;
import androidx.lifecycle.LiveData;
import com.fossil.nw0;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uw0<T> extends LiveData<T> {
    @DexIgnore
    public /* final */ qw0 k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ Callable<T> m;
    @DexIgnore
    public /* final */ mw0 n;
    @DexIgnore
    public /* final */ nw0.c o;
    @DexIgnore
    public /* final */ AtomicBoolean p; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ AtomicBoolean q; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicBoolean r; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ Runnable s; // = new a();
    @DexIgnore
    public /* final */ Runnable t; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            boolean z;
            if (uw0.this.r.compareAndSet(false, true)) {
                uw0.this.k.getInvalidationTracker().b(uw0.this.o);
            }
            do {
                if (uw0.this.q.compareAndSet(false, true)) {
                    T t = null;
                    z = false;
                    while (uw0.this.p.compareAndSet(true, false)) {
                        try {
                            try {
                                t = uw0.this.m.call();
                                z = true;
                            } catch (Exception e) {
                                throw new RuntimeException("Exception while computing database live data.", e);
                            }
                        } finally {
                            uw0.this.q.set(false);
                        }
                    }
                    if (z) {
                        uw0.this.l(t);
                    }
                } else {
                    z = false;
                }
                if (!z) {
                    return;
                }
            } while (uw0.this.p.get());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            boolean g = uw0.this.g();
            if (uw0.this.p.compareAndSet(false, true) && g) {
                uw0.this.q().execute(uw0.this.s);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends nw0.c {
        @DexIgnore
        public c(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        @Override // com.fossil.nw0.c
        public void onInvalidated(Set<String> set) {
            bi0.f().b(uw0.this.t);
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public uw0(qw0 qw0, mw0 mw0, boolean z, Callable<T> callable, String[] strArr) {
        this.k = qw0;
        this.l = z;
        this.m = callable;
        this.n = mw0;
        this.o = new c(strArr);
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData
    public void j() {
        super.j();
        this.n.b(this);
        q().execute(this.s);
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData
    public void k() {
        super.k();
        this.n.c(this);
    }

    @DexIgnore
    public Executor q() {
        return this.l ? this.k.getTransactionExecutor() : this.k.getQueryExecutor();
    }
}
