package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.dr4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.watchface.edit.WatchFaceEditActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k87 extends pv5 {
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public m87 h;
    @DexIgnore
    public yg5 i;
    @DexIgnore
    public dr4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final k87 a() {
            return new k87();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements dr4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ k87 f1878a;

        @DexIgnore
        public b(k87 k87) {
            this.f1878a = k87;
        }

        @DexIgnore
        @Override // com.fossil.dr4.a
        public void a(p77 p77) {
            pq7.c(p77, "ring");
            cb7 cb7 = new cb7(p77.d(), p77.f(), p77.c().a());
            gc7 d = hc7.c.d(this.f1878a);
            if (d != null) {
                d.A(cb7);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ k87 f1879a;

        @DexIgnore
        public c(k87 k87) {
            this.f1879a = k87;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            gc7 d = hc7.c.d(this.f1879a);
            if (d != null) {
                d.w(z);
            }
            k87.L6(this.f1879a).o(!z);
            if (z) {
                this.f1879a.S6("");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<List<? extends p77>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ k87 f1880a;

        @DexIgnore
        public d(k87 k87) {
            this.f1880a = k87;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<p77> list) {
            dr4 L6 = k87.L6(this.f1880a);
            pq7.b(list, "it");
            L6.n(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<eb7> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ k87 f1881a;

        @DexIgnore
        public e(k87 k87) {
            this.f1881a = k87;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(eb7 eb7) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceRingFragment", "watchFaceComplicationsLive, value = " + eb7);
            String a2 = eb7.b().a();
            if (eb7.a() == ab7.REMOVED || a2 == null) {
                k87.L6(this.f1881a).q(null);
                this.f1881a.P6();
                return;
            }
            if (!ik5.d.e(a2)) {
                this.f1881a.P6();
            } else {
                this.f1881a.T6();
            }
            this.f1881a.S6(eb7.b().c());
            if (eb7.a() == ab7.ADDED) {
                FlexibleSwitchCompat flexibleSwitchCompat = k87.K6(this.f1881a).e;
                pq7.b(flexibleSwitchCompat, "mBinding.switchRing");
                flexibleSwitchCompat.setChecked(false);
                k87.L6(this.f1881a).o(true);
            } else if (eb7.a() == ab7.SELECTED) {
                boolean d = eb7.b().d();
                FlexibleSwitchCompat flexibleSwitchCompat2 = k87.K6(this.f1881a).e;
                pq7.b(flexibleSwitchCompat2, "mBinding.switchRing");
                flexibleSwitchCompat2.setChecked(d);
                k87.L6(this.f1881a).o(!d);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ yg5 K6(k87 k87) {
        yg5 yg5 = k87.i;
        if (yg5 != null) {
            return yg5;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ dr4 L6(k87 k87) {
        dr4 dr4 = k87.j;
        if (dr4 != null) {
            return dr4;
        }
        pq7.n("mRingAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "WatchFaceRingFragment";
    }

    @DexIgnore
    public final void P6() {
        yg5 yg5 = this.i;
        if (yg5 != null) {
            View view = yg5.h;
            pq7.b(view, "mBinding.vSeparator");
            view.setVisibility(4);
            yg5 yg52 = this.i;
            if (yg52 != null) {
                FlexibleTextView flexibleTextView = yg52.g;
                pq7.b(flexibleTextView, "mBinding.tvSelectedComplication");
                flexibleTextView.setVisibility(4);
                yg5 yg53 = this.i;
                if (yg53 != null) {
                    FlexibleSwitchCompat flexibleSwitchCompat = yg53.e;
                    pq7.b(flexibleSwitchCompat, "mBinding.switchRing");
                    flexibleSwitchCompat.setVisibility(4);
                    yg5 yg54 = this.i;
                    if (yg54 != null) {
                        FlexibleTextView flexibleTextView2 = yg54.f;
                        pq7.b(flexibleTextView2, "mBinding.tvComplicationDetail");
                        flexibleTextView2.setVisibility(4);
                        return;
                    }
                    pq7.n("mBinding");
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        dr4 dr4 = new dr4();
        dr4.p(new b(this));
        this.j = dr4;
        yg5 yg5 = this.i;
        if (yg5 != null) {
            RecyclerView recyclerView = yg5.d;
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
            dr4 dr42 = this.j;
            if (dr42 != null) {
                recyclerView.setAdapter(dr42);
                yg5 yg52 = this.i;
                if (yg52 != null) {
                    yg52.e.setOnCheckedChangeListener(new c(this));
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            } else {
                pq7.n("mRingAdapter");
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void R6() {
        LiveData<eb7> q;
        m87 m87 = this.h;
        if (m87 != null) {
            m87.o().h(getViewLifecycleOwner(), new d(this));
            gc7 d2 = hc7.c.d(this);
            if (d2 != null && (q = d2.q()) != null) {
                q.h(getViewLifecycleOwner(), new e(this));
                return;
            }
            return;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void S6(String str) {
        dr4 dr4 = this.j;
        if (dr4 != null) {
            dr4.q(str);
            dr4 dr42 = this.j;
            if (dr42 != null) {
                int k2 = dr42.k(str);
                if (k2 >= 0) {
                    yg5 yg5 = this.i;
                    if (yg5 != null) {
                        yg5.d.scrollToPosition(k2);
                    } else {
                        pq7.n("mBinding");
                        throw null;
                    }
                }
            } else {
                pq7.n("mRingAdapter");
                throw null;
            }
        } else {
            pq7.n("mRingAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void T6() {
        yg5 yg5 = this.i;
        if (yg5 != null) {
            View view = yg5.h;
            pq7.b(view, "mBinding.vSeparator");
            view.setVisibility(0);
            yg5 yg52 = this.i;
            if (yg52 != null) {
                FlexibleTextView flexibleTextView = yg52.g;
                pq7.b(flexibleTextView, "mBinding.tvSelectedComplication");
                flexibleTextView.setVisibility(0);
                yg5 yg53 = this.i;
                if (yg53 != null) {
                    FlexibleSwitchCompat flexibleSwitchCompat = yg53.e;
                    pq7.b(flexibleSwitchCompat, "mBinding.switchRing");
                    flexibleSwitchCompat.setVisibility(0);
                    yg5 yg54 = this.i;
                    if (yg54 != null) {
                        FlexibleTextView flexibleTextView2 = yg54.f;
                        pq7.b(flexibleTextView2, "mBinding.tvComplicationDetail");
                        flexibleTextView2.setVisibility(0);
                        return;
                    }
                    pq7.n("mBinding");
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        yg5 c2 = yg5.c(layoutInflater);
        pq7.b(c2, "WatchFaceRingFragmentBinding.inflate(inflater)");
        this.i = c2;
        if (c2 != null) {
            return c2.b();
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        PortfolioApp.h0.c().M().e0().a(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WatchFaceEditActivity watchFaceEditActivity = (WatchFaceEditActivity) activity;
            po4 po4 = this.g;
            if (po4 != null) {
                ts0 a2 = vs0.f(watchFaceEditActivity, po4).a(m87.class);
                pq7.b(a2, "ViewModelProviders.of(ac\u2026ingViewModel::class.java)");
                this.h = (m87) a2;
                Q6();
                R6();
                return;
            }
            pq7.n("mViewModelFactory");
            throw null;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.watchface.edit.WatchFaceEditActivity");
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
