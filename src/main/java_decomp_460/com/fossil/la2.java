package com.fossil;

import android.os.DeadObjectException;
import com.fossil.i72;
import com.fossil.l72;
import com.fossil.m62;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class la2<A extends i72<? extends z62, m62.b>> extends z82 {
    @DexIgnore
    public /* final */ A b;

    @DexIgnore
    public la2(int i, A a2) {
        super(i);
        this.b = a2;
    }

    @DexIgnore
    @Override // com.fossil.z82
    public final void b(Status status) {
        this.b.A(status);
    }

    @DexIgnore
    @Override // com.fossil.z82
    public final void c(l72.a<?> aVar) throws DeadObjectException {
        try {
            this.b.y(aVar.R());
        } catch (RuntimeException e) {
            e(e);
        }
    }

    @DexIgnore
    @Override // com.fossil.z82
    public final void d(a82 a82, boolean z) {
        a82.b(this.b, z);
    }

    @DexIgnore
    @Override // com.fossil.z82
    public final void e(Exception exc) {
        String simpleName = exc.getClass().getSimpleName();
        String localizedMessage = exc.getLocalizedMessage();
        StringBuilder sb = new StringBuilder(String.valueOf(simpleName).length() + 2 + String.valueOf(localizedMessage).length());
        sb.append(simpleName);
        sb.append(": ");
        sb.append(localizedMessage);
        this.b.A(new Status(10, sb.toString()));
    }
}
