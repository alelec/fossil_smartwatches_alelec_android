package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q61 implements n61<Uri, Uri> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2924a;

    @DexIgnore
    public q61(Context context) {
        pq7.c(context, "context");
        this.f2924a = context;
    }

    @DexIgnore
    /* renamed from: c */
    public boolean a(Uri uri) {
        pq7.c(uri, "data");
        if (pq7.a(uri.getScheme(), "android.resource")) {
            String authority = uri.getAuthority();
            if (!(authority == null || vt7.l(authority))) {
                List<String> pathSegments = uri.getPathSegments();
                pq7.b(pathSegments, "data.pathSegments");
                if (pathSegments.size() == 2) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: d */
    public Uri b(Uri uri) {
        pq7.c(uri, "data");
        String authority = uri.getAuthority();
        String str = authority != null ? authority : "";
        Resources resourcesForApplication = this.f2924a.getPackageManager().getResourcesForApplication(str);
        List<String> pathSegments = uri.getPathSegments();
        int identifier = resourcesForApplication.getIdentifier(pathSegments.get(1), pathSegments.get(0), str);
        if (identifier != 0) {
            Uri parse = Uri.parse("android.resource://" + str + '/' + identifier);
            pq7.b(parse, "Uri.parse(this)");
            return parse;
        }
        throw new IllegalStateException(("Invalid android.resource URI: " + uri).toString());
    }
}
