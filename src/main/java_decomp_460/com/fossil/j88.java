package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.dl7;
import java.lang.reflect.Method;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j88 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<Throwable, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ Call $this_await$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Call call) {
            super(1);
            this.$this_await$inlined = call;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
            invoke(th);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(Throwable th) {
            this.$this_await$inlined.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements rp7<Throwable, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ Call $this_await$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Call call) {
            super(1);
            this.$this_await$inlined = call;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
            invoke(th);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(Throwable th) {
            this.$this_await$inlined.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements c88<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku7 f1726a;

        @DexIgnore
        public c(ku7 ku7) {
            this.f1726a = ku7;
        }

        @DexIgnore
        @Override // com.fossil.c88
        public void onFailure(Call<T> call, Throwable th) {
            pq7.c(call, "call");
            pq7.c(th, "t");
            ku7 ku7 = this.f1726a;
            dl7.a aVar = dl7.Companion;
            ku7.resumeWith(dl7.m1constructorimpl(el7.a(th)));
        }

        @DexIgnore
        @Override // com.fossil.c88
        public void onResponse(Call<T> call, q88<T> q88) {
            pq7.c(call, "call");
            pq7.c(q88, "response");
            if (q88.e()) {
                T a2 = q88.a();
                if (a2 == null) {
                    Object i = call.c().i(i88.class);
                    if (i != null) {
                        pq7.b(i, "call.request().tag(Invocation::class.java)!!");
                        Method a3 = ((i88) i).a();
                        StringBuilder sb = new StringBuilder();
                        sb.append("Response from ");
                        pq7.b(a3, "method");
                        Class<?> declaringClass = a3.getDeclaringClass();
                        pq7.b(declaringClass, "method.declaringClass");
                        sb.append(declaringClass.getName());
                        sb.append('.');
                        sb.append(a3.getName());
                        sb.append(" was null but response body type was declared as non-null");
                        wk7 wk7 = new wk7(sb.toString());
                        ku7 ku7 = this.f1726a;
                        dl7.a aVar = dl7.Companion;
                        ku7.resumeWith(dl7.m1constructorimpl(el7.a(wk7)));
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                ku7 ku72 = this.f1726a;
                dl7.a aVar2 = dl7.Companion;
                ku72.resumeWith(dl7.m1constructorimpl(a2));
                return;
            }
            ku7 ku73 = this.f1726a;
            g88 g88 = new g88(q88);
            dl7.a aVar3 = dl7.Companion;
            ku73.resumeWith(dl7.m1constructorimpl(el7.a(g88)));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements c88<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku7 f1727a;

        @DexIgnore
        public d(ku7 ku7) {
            this.f1727a = ku7;
        }

        @DexIgnore
        @Override // com.fossil.c88
        public void onFailure(Call<T> call, Throwable th) {
            pq7.c(call, "call");
            pq7.c(th, "t");
            ku7 ku7 = this.f1727a;
            dl7.a aVar = dl7.Companion;
            ku7.resumeWith(dl7.m1constructorimpl(el7.a(th)));
        }

        @DexIgnore
        @Override // com.fossil.c88
        public void onResponse(Call<T> call, q88<T> q88) {
            pq7.c(call, "call");
            pq7.c(q88, "response");
            if (q88.e()) {
                ku7 ku7 = this.f1727a;
                T a2 = q88.a();
                dl7.a aVar = dl7.Companion;
                ku7.resumeWith(dl7.m1constructorimpl(a2));
                return;
            }
            ku7 ku72 = this.f1727a;
            g88 g88 = new g88(q88);
            dl7.a aVar2 = dl7.Companion;
            ku72.resumeWith(dl7.m1constructorimpl(el7.a(g88)));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends qq7 implements rp7<Throwable, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ Call $this_awaitResponse$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(Call call) {
            super(1);
            this.$this_awaitResponse$inlined = call;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
            invoke(th);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(Throwable th) {
            this.$this_awaitResponse$inlined.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements c88<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku7 f1728a;

        @DexIgnore
        public f(ku7 ku7) {
            this.f1728a = ku7;
        }

        @DexIgnore
        @Override // com.fossil.c88
        public void onFailure(Call<T> call, Throwable th) {
            pq7.c(call, "call");
            pq7.c(th, "t");
            ku7 ku7 = this.f1728a;
            dl7.a aVar = dl7.Companion;
            ku7.resumeWith(dl7.m1constructorimpl(el7.a(th)));
        }

        @DexIgnore
        @Override // com.fossil.c88
        public void onResponse(Call<T> call, q88<T> q88) {
            pq7.c(call, "call");
            pq7.c(q88, "response");
            ku7 ku7 = this.f1728a;
            dl7.a aVar = dl7.Companion;
            ku7.resumeWith(dl7.m1constructorimpl(q88));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "retrofit2/KotlinExtensions", f = "KotlinExtensions.kt", l = {100, 102}, m = "yieldAndThrow")
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;

        @DexIgnore
        public g(qn7 qn7) {
            super(qn7);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return j88.d(null, this);
        }
    }

    @DexIgnore
    public static final <T> Object a(Call<T> call, qn7<? super T> qn7) {
        lu7 lu7 = new lu7(xn7.c(qn7), 1);
        lu7.e(new a(call));
        call.D(new c(lu7));
        Object t = lu7.t();
        if (t == yn7.d()) {
            go7.c(qn7);
        }
        return t;
    }

    @DexIgnore
    public static final <T> Object b(Call<T> call, qn7<? super T> qn7) {
        lu7 lu7 = new lu7(xn7.c(qn7), 1);
        lu7.e(new b(call));
        call.D(new d(lu7));
        Object t = lu7.t();
        if (t == yn7.d()) {
            go7.c(qn7);
        }
        return t;
    }

    @DexIgnore
    public static final <T> Object c(Call<T> call, qn7<? super q88<T>> qn7) {
        lu7 lu7 = new lu7(xn7.c(qn7), 1);
        lu7.e(new e(call));
        call.D(new f(lu7));
        Object t = lu7.t();
        if (t == yn7.d()) {
            go7.c(qn7);
        }
        return t;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Object d(java.lang.Exception r5, com.fossil.qn7<?> r6) {
        /*
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.fossil.j88.g
            if (r0 == 0) goto L_0x002d
            r0 = r6
            com.fossil.j88$g r0 = (com.fossil.j88.g) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002d
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x003b
            if (r3 != r4) goto L_0x0033
            java.lang.Object r0 = r0.L$0
            java.lang.Exception r0 = (java.lang.Exception) r0
            boolean r2 = r1 instanceof com.fossil.dl7.b
            if (r2 == 0) goto L_0x004b
            r0 = r1
            com.fossil.dl7$b r0 = (com.fossil.dl7.b) r0
            java.lang.Throwable r0 = r0.exception
            throw r0
        L_0x002d:
            com.fossil.j88$g r0 = new com.fossil.j88$g
            r0.<init>(r6)
            goto L_0x0013
        L_0x0033:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003b:
            boolean r3 = r1 instanceof com.fossil.dl7.b
            if (r3 != 0) goto L_0x004c
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = com.fossil.fy7.b(r0)
            if (r0 != r2) goto L_0x004a
            return r2
        L_0x004a:
            r0 = r5
        L_0x004b:
            throw r0
        L_0x004c:
            com.fossil.dl7$b r1 = (com.fossil.dl7.b) r1
            java.lang.Throwable r0 = r1.exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.j88.d(java.lang.Exception, com.fossil.qn7):java.lang.Object");
    }
}
