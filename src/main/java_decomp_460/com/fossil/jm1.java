package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jm1 extends dm1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<jm1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public jm1 a(Parcel parcel) {
            return new jm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public jm1 createFromParcel(Parcel parcel) {
            return new jm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public jm1[] newArray(int i) {
            return new jm1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ jm1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    public jm1(bt1 bt1) {
        super(fm1.STEPS, bt1, null, null, 12);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ jm1(bt1 bt1, int i, kq7 kq7) {
        this((i & 1) != 0 ? new bt1(false) : bt1);
    }

    @DexIgnore
    public jm1(dt1 dt1, et1 et1, bt1 bt1) {
        super(fm1.STEPS, bt1, dt1, et1);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ jm1(dt1 dt1, et1 et1, bt1 bt1, int i, kq7 kq7) {
        this(dt1, (i & 2) != 0 ? new et1(et1.CREATOR.a()) : et1, (i & 4) != 0 ? new bt1(false) : bt1);
    }

    @DexIgnore
    public final bt1 getDataConfig() {
        at1 at1 = this.c;
        if (at1 != null) {
            return (bt1) at1;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.PercentageCircleComplicationDataConfig");
    }
}
