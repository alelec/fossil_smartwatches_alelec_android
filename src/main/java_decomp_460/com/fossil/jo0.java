package com.fossil;

import android.view.View;
import android.view.ViewTreeObserver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jo0 implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {
    @DexIgnore
    public /* final */ View b;
    @DexIgnore
    public ViewTreeObserver c;
    @DexIgnore
    public /* final */ Runnable d;

    @DexIgnore
    public jo0(View view, Runnable runnable) {
        this.b = view;
        this.c = view.getViewTreeObserver();
        this.d = runnable;
    }

    @DexIgnore
    public static jo0 a(View view, Runnable runnable) {
        if (view == null) {
            throw new NullPointerException("view == null");
        } else if (runnable != null) {
            jo0 jo0 = new jo0(view, runnable);
            view.getViewTreeObserver().addOnPreDrawListener(jo0);
            view.addOnAttachStateChangeListener(jo0);
            return jo0;
        } else {
            throw new NullPointerException("runnable == null");
        }
    }

    @DexIgnore
    public void b() {
        if (this.c.isAlive()) {
            this.c.removeOnPreDrawListener(this);
        } else {
            this.b.getViewTreeObserver().removeOnPreDrawListener(this);
        }
        this.b.removeOnAttachStateChangeListener(this);
    }

    @DexIgnore
    public boolean onPreDraw() {
        b();
        this.d.run();
        return true;
    }

    @DexIgnore
    public void onViewAttachedToWindow(View view) {
        this.c = view.getViewTreeObserver();
    }

    @DexIgnore
    public void onViewDetachedFromWindow(View view) {
        b();
    }
}
