package com.fossil;

import android.location.Location;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.ShareConstants;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.diana.commutetime.Address;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.diana.commutetime.TrafficRequest;
import com.portfolio.platform.data.model.diana.commutetime.TrafficResponse;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cs5 {
    @DexIgnore
    public static cs5 q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public PortfolioApp f645a;
    @DexIgnore
    public ApiServiceV2 b;
    @DexIgnore
    public LocationSource c;
    @DexIgnore
    public UserRepository d;
    @DexIgnore
    public DianaAppSettingRepository e;
    @DexIgnore
    public on5 f;
    @DexIgnore
    public /* final */ Gson g;
    @DexIgnore
    public String h;
    @DexIgnore
    public TrafficResponse i;
    @DexIgnore
    public Address j;
    @DexIgnore
    public AddressWrapper k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public AddressWrapper m;
    @DexIgnore
    public b n;
    @DexIgnore
    public b o;
    @DexIgnore
    public boolean p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final cs5 a() {
            cs5 cs5;
            synchronized (this) {
                if (cs5.q == null) {
                    cs5.q = new cs5(null);
                }
                cs5 = cs5.q;
                if (cs5 == null) {
                    pq7.i();
                    throw null;
                }
            }
            return cs5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements LocationSource.LocationListener {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$Listener$onLocationResult$1", f = "WatchAppCommuteTimeManager.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Location $location;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Location location, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$location = location;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$location, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    cs5.this.v(this.$location.getLatitude(), this.$location.getLongitude());
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.LocationSource.LocationListener
        public void onLocationResult(Location location) {
            pq7.c(location, PlaceFields.LOCATION);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppCommuteTimeManager", "onLocationResult lastLocation=" + location);
            xw7 unused = gu7.d(jv7.a(bw7.a()), bw7.c(), null, new a(this, location, null), 2, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1", f = "WatchAppCommuteTimeManager.kt", l = {112}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $destinationAlias;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ cs5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1$1", f = "WatchAppCommuteTimeManager.kt", l = {114, 134}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x0043  */
            /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r14) {
                /*
                // Method dump skipped, instructions count: 405
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.cs5.c.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(cs5 cs5, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cs5;
            this.$destinationAlias = str;
            this.$serial = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$destinationAlias, this.$serial, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (by7.c(DeviceAppResponse.LIFE_TIME, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends ko7 implements rp7<qn7<? super q88<TrafficResponse>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ qn7 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Location $location$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ TrafficRequest $trafficRequest;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ cs5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(TrafficRequest trafficRequest, qn7 qn7, cs5 cs5, Location location, qn7 qn72) {
            super(1, qn7);
            this.$trafficRequest = trafficRequest;
            this.this$0 = cs5;
            this.$location$inlined = location;
            this.$continuation$inlined = qn72;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new d(this.$trafficRequest, qn7, this.this$0, this.$location$inlined, this.$continuation$inlined);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<TrafficResponse>> qn7) {
            return ((d) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 j = this.this$0.j();
                TrafficRequest trafficRequest = this.$trafficRequest;
                this.label = 1;
                Object trafficStatus = j.getTrafficStatus(trafficRequest, this);
                return trafficStatus == d ? d : trafficStatus;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager", f = "WatchAppCommuteTimeManager.kt", l = {192}, m = "getDurationTime")
    public static final class e extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ cs5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(cs5 cs5, qn7 qn7) {
            super(qn7);
            this.this$0 = cs5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager", f = "WatchAppCommuteTimeManager.kt", l = {145, 165}, m = "getDurationTimeBaseOnLocation")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ cs5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(cs5 cs5, qn7 qn7) {
            super(qn7);
            this.this$0 = cs5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(this);
        }
    }

    @DexIgnore
    public cs5() {
        this.g = new Gson();
        this.n = new b();
        this.o = new b();
        PortfolioApp.h0.c().M().c1(this);
    }

    @DexIgnore
    public /* synthetic */ cs5(kq7 kq7) {
        this();
    }

    @DexIgnore
    public final void g(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppCommuteTimeManager", "getCommuteTimeForWatchApp serial " + str + " destinationAlias " + str2);
        this.h = str;
        this.p = false;
        xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new c(this, str2, str, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071 A[Catch:{ Exception -> 0x02a6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x02a1  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object h(android.location.Location r17, com.fossil.qn7<? super com.fossil.tl7> r18) {
        /*
        // Method dump skipped, instructions count: 693
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cs5.h(android.location.Location, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0167  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01b6  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x01b9  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01bc  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01bf  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x020b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object i(com.fossil.qn7<? super com.fossil.tl7> r15) {
        /*
        // Method dump skipped, instructions count: 596
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cs5.i(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final ApiServiceV2 j() {
        ApiServiceV2 apiServiceV2 = this.b;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        pq7.n("mApiServiceV2");
        throw null;
    }

    @DexIgnore
    public final DianaAppSettingRepository k() {
        DianaAppSettingRepository dianaAppSettingRepository = this.e;
        if (dianaAppSettingRepository != null) {
            return dianaAppSettingRepository;
        }
        pq7.n("mDianaAppSettingRepository");
        throw null;
    }

    @DexIgnore
    public final Float l(Address address, Address address2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppCommuteTimeManager", "getSimpleDistance between " + address + " and " + address2);
        float[] fArr = new float[1];
        Location.distanceBetween(address2.getLat(), address2.getLng(), address.getLat(), address.getLng(), fArr);
        return Float.valueOf(fArr[0]);
    }

    @DexIgnore
    public final String m(String str) {
        switch (str.hashCode()) {
            case -1078030475:
                if (str.equals("medium")) {
                    PortfolioApp portfolioApp = this.f645a;
                    if (portfolioApp != null) {
                        String c2 = um5.c(portfolioApp, 2131887588);
                        pq7.b(c2, "LanguageHelper.getString\u2026ng.traffic_status_medium)");
                        return c2;
                    }
                    pq7.n("mPortfolioApp");
                    throw null;
                }
                break;
            case -284840886:
                if (str.equals("unknown")) {
                    PortfolioApp portfolioApp2 = this.f645a;
                    if (portfolioApp2 != null) {
                        String c3 = um5.c(portfolioApp2, 2131887589);
                        pq7.b(c3, "LanguageHelper.getString\u2026g.traffic_status_unknown)");
                        return c3;
                    }
                    pq7.n("mPortfolioApp");
                    throw null;
                }
                break;
            case 99152071:
                if (str.equals("heavy")) {
                    PortfolioApp portfolioApp3 = this.f645a;
                    if (portfolioApp3 != null) {
                        String c4 = um5.c(portfolioApp3, 2131887586);
                        pq7.b(c4, "LanguageHelper.getString\u2026ing.traffic_status_heavy)");
                        return c4;
                    }
                    pq7.n("mPortfolioApp");
                    throw null;
                }
                break;
            case 102970646:
                if (str.equals("light")) {
                    PortfolioApp portfolioApp4 = this.f645a;
                    if (portfolioApp4 != null) {
                        String c5 = um5.c(portfolioApp4, 2131887587);
                        pq7.b(c5, "LanguageHelper.getString\u2026ing.traffic_status_light)");
                        return c5;
                    }
                    pq7.n("mPortfolioApp");
                    throw null;
                }
                break;
        }
        return "--";
    }

    @DexIgnore
    public final void n(LocationSource.ErrorState errorState) {
        ul5 f2 = ck5.f.f("commute-time");
        if (f2 != null) {
            String str = this.h;
            if (str != null) {
                f2.d(str, false, ErrorCodeBuilder.AppError.NOT_FETCH_CURRENT_LOCATION.getValue());
            } else {
                pq7.i();
                throw null;
            }
        }
        ck5.f.k("commute-time");
    }

    @DexIgnore
    public final void o(String str, String str2, int i2) {
        pq7.c(str, "serial");
        pq7.c(str2, ShareConstants.DESTINATION);
        if (i2 == du1.START.ordinal()) {
            FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "process start commute time");
            g(str, str2);
        } else if (i2 == du1.STOP.ordinal()) {
            FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "process stop commute time");
            JSONObject jSONObject = new JSONObject();
            AddressWrapper addressWrapper = this.m;
            jSONObject.put("des", addressWrapper != null ? addressWrapper.getId() : null);
            jSONObject.put("type", "end");
            jSONObject.put("end_type", "user_exit");
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
            String jSONObject2 = jSONObject.toString();
            pq7.b(jSONObject2, "jsonObject.toString()");
            remote.i(component, session, str, "WatchAppCommuteTimeManager", jSONObject2);
            FLogger.INSTANCE.getRemote().summary(this.p ? 0 : FailureCode.USER_CANCELLED, FLogger.Component.APP, FLogger.Session.DIANA_COMMUTE_TIME, str, "WatchAppCommuteTimeManager");
            this.m = null;
            t();
            u();
        }
    }

    @DexIgnore
    public final void p() {
        String c2;
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "sendArriveToDestinationResponse");
        AddressWrapper addressWrapper = this.m;
        if (addressWrapper != null) {
            try {
                this.p = true;
                int i2 = ds5.f828a[addressWrapper.getType().ordinal()];
                if (i2 == 1) {
                    PortfolioApp portfolioApp = this.f645a;
                    if (portfolioApp != null) {
                        c2 = um5.c(portfolioApp, 2131886353);
                    } else {
                        pq7.n("mPortfolioApp");
                        throw null;
                    }
                } else if (i2 == 2) {
                    PortfolioApp portfolioApp2 = this.f645a;
                    if (portfolioApp2 != null) {
                        c2 = um5.c(portfolioApp2, 2131886351);
                    } else {
                        pq7.n("mPortfolioApp");
                        throw null;
                    }
                } else if (i2 == 3) {
                    hr7 hr7 = hr7.f1520a;
                    PortfolioApp portfolioApp3 = this.f645a;
                    if (portfolioApp3 != null) {
                        String c3 = um5.c(portfolioApp3, 2131886350);
                        pq7.b(c3, "LanguageHelper.getString\u2026Popup___ArrivedToAddress)");
                        c2 = String.format(c3, Arrays.copyOf(new Object[]{addressWrapper.getName()}, 1));
                        pq7.b(c2, "java.lang.String.format(format, *args)");
                    } else {
                        pq7.n("mPortfolioApp");
                        throw null;
                    }
                } else {
                    throw new al7();
                }
                pq7.b(c2, "message");
                CommuteTimeWatchAppMessage commuteTimeWatchAppMessage = new CommuteTimeWatchAppMessage(c2, eu1.END);
                PortfolioApp c4 = PortfolioApp.h0.c();
                String str = this.h;
                if (str != null) {
                    c4.g1(commuteTimeWatchAppMessage, str);
                    JSONObject jSONObject = new JSONObject();
                    AddressWrapper addressWrapper2 = this.m;
                    jSONObject.put("des", addressWrapper2 != null ? addressWrapper2.getId() : null);
                    jSONObject.put("type", "end");
                    jSONObject.put("end_type", "arrived");
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
                    String str2 = this.h;
                    if (str2 != null) {
                        String jSONObject2 = jSONObject.toString();
                        pq7.b(jSONObject2, "jsonObject.toString()");
                        remote.i(component, session, str2, "WatchAppCommuteTimeManager", jSONObject2);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            } catch (IllegalArgumentException e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppCommuteTimeManager", "sendArriveToDestinationResponse exception exception=" + e2.getMessage());
            }
        }
    }

    @DexIgnore
    public final void q(String str, long j2, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppCommuteTimeManager", "sendCommuteTimeAppResponse destination " + str + ", durationInTraffic " + j2 + ", trafficStatus " + str2);
        AddressWrapper addressWrapper = this.m;
        if (addressWrapper == null) {
            return;
        }
        if (addressWrapper == null) {
            pq7.i();
            throw null;
        } else if (!TextUtils.isEmpty(addressWrapper.getName())) {
            int b2 = lr7.b(((float) j2) / 60.0f);
            if (vt7.j(str2, "not_found", true)) {
                PortfolioApp portfolioApp = this.f645a;
                if (portfolioApp != null) {
                    String c2 = um5.c(portfolioApp, 2131886348);
                    pq7.b(c2, "message");
                    CommuteTimeWatchAppMessage commuteTimeWatchAppMessage = new CommuteTimeWatchAppMessage(c2, eu1.END);
                    PortfolioApp c3 = PortfolioApp.h0.c();
                    String str3 = this.h;
                    if (str3 != null) {
                        c3.g1(commuteTimeWatchAppMessage, str3);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.n("mPortfolioApp");
                    throw null;
                }
            } else {
                try {
                    CommuteTimeWatchAppInfo commuteTimeWatchAppInfo = new CommuteTimeWatchAppInfo(str, b2, m(str2));
                    PortfolioApp c4 = PortfolioApp.h0.c();
                    String str4 = this.h;
                    if (str4 != null) {
                        c4.g1(commuteTimeWatchAppInfo, str4);
                        ul5 f2 = ck5.f.f("commute-time");
                        if (f2 != null) {
                            String str5 = this.h;
                            if (str5 != null) {
                                f2.d(str5, this.l, "");
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                        ck5.f.k("commute-time");
                        return;
                    }
                    pq7.i();
                    throw null;
                } catch (IllegalArgumentException e2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("WatchAppCommuteTimeManager", "sendCommuteTimeAppResponse exception exception=" + e2.getMessage());
                }
            }
        }
    }

    @DexIgnore
    public final void r() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "startMonitoringLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            PortfolioApp portfolioApp = this.f645a;
            if (portfolioApp != null) {
                LocationSource.observerLocation$default(locationSource, portfolioApp, this.o, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 8, null);
            } else {
                pq7.n("mPortfolioApp");
                throw null;
            }
        } else {
            pq7.n("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void s() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "startSignificantLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            PortfolioApp portfolioApp = this.f645a;
            if (portfolioApp != null) {
                LocationSource.observerLocation$default(locationSource, portfolioApp, this.n, true, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 8, null);
            } else {
                pq7.n("mPortfolioApp");
                throw null;
            }
        } else {
            pq7.n("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void t() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "stopMonitoringLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            locationSource.unObserverLocation(this.o, false);
        } else {
            pq7.n("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void u() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "stopSignificantLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            locationSource.unObserverLocation(this.n, true);
        } else {
            pq7.n("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void v(double d2, double d3) {
        Float f2;
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "updateObserver updateObserver=" + d2 + ',' + d3);
        AddressWrapper addressWrapper = this.m;
        if (addressWrapper == null) {
            f2 = null;
        } else if (addressWrapper != null) {
            double lat = addressWrapper.getLat();
            AddressWrapper addressWrapper2 = this.m;
            if (addressWrapper2 != null) {
                f2 = l(new Address(lat, addressWrapper2.getLng()), new Address(d2, d3));
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "updateObserver distance=" + f2);
        JSONObject jSONObject = new JSONObject();
        AddressWrapper addressWrapper3 = this.m;
        jSONObject.put("des", addressWrapper3 != null ? addressWrapper3.getId() : null);
        jSONObject.put("type", "loc_update");
        jSONObject.put("dis_to_des", f2);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
        String str = this.h;
        if (str != null) {
            String jSONObject2 = jSONObject.toString();
            pq7.b(jSONObject2, "jsonObject.toString()");
            remote.i(component, session, str, "WatchAppCommuteTimeManager", jSONObject2);
            if (f2 != null) {
                if (f2 == null) {
                    pq7.i();
                    throw null;
                } else if (f2.floatValue() < ((float) 2000)) {
                    if (f2 == null) {
                        pq7.i();
                        throw null;
                    } else if (f2.floatValue() < ((float) 100)) {
                        t();
                        u();
                        p();
                        return;
                    } else {
                        u();
                        r();
                        return;
                    }
                }
            }
            t();
            s();
            return;
        }
        pq7.i();
        throw null;
    }
}
