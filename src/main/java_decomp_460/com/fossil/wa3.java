package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wa3 implements Parcelable.Creator<la3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ la3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 1:
                    z6 = ad2.m(parcel, t);
                    break;
                case 2:
                    z5 = ad2.m(parcel, t);
                    break;
                case 3:
                    z4 = ad2.m(parcel, t);
                    break;
                case 4:
                    z3 = ad2.m(parcel, t);
                    break;
                case 5:
                    z2 = ad2.m(parcel, t);
                    break;
                case 6:
                    z = ad2.m(parcel, t);
                    break;
                default:
                    ad2.B(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new la3(z6, z5, z4, z3, z2, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ la3[] newArray(int i) {
        return new la3[i];
    }
}
