package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class km4 extends rm4 {
    @DexIgnore
    public km4() {
        super(false, 1558, 620, 22, 22, 36, -1, 62);
    }

    @DexIgnore
    @Override // com.fossil.rm4
    public int b(int i) {
        return i <= 8 ? 156 : 155;
    }

    @DexIgnore
    @Override // com.fossil.rm4
    public int f() {
        return 10;
    }
}
