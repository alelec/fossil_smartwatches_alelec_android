package com.fossil;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.view.ActionMode;
import com.fossil.xn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ye0 extends Dialog implements we0 {
    @DexIgnore
    public AppCompatDelegate b;
    @DexIgnore
    public /* final */ xn0.a c; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements xn0.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.xn0.a
        public boolean superDispatchKeyEvent(KeyEvent keyEvent) {
            return ye0.this.c(keyEvent);
        }
    }

    @DexIgnore
    public ye0(Context context, int i) {
        super(context, b(context, i));
        AppCompatDelegate a2 = a();
        a2.F(b(context, i));
        a2.r(null);
    }

    @DexIgnore
    public static int b(Context context, int i) {
        if (i != 0) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(le0.dialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    @DexIgnore
    public AppCompatDelegate a() {
        if (this.b == null) {
            this.b = AppCompatDelegate.h(this, this);
        }
        return this.b;
    }

    @DexIgnore
    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().d(view, layoutParams);
    }

    @DexIgnore
    public boolean c(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }

    @DexIgnore
    public boolean d(int i) {
        return a().A(i);
    }

    @DexIgnore
    public void dismiss() {
        super.dismiss();
        a().s();
    }

    @DexIgnore
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return xn0.e(this.c, getWindow().getDecorView(), this, keyEvent);
    }

    @DexIgnore
    @Override // android.app.Dialog
    public <T extends View> T findViewById(int i) {
        return (T) a().i(i);
    }

    @DexIgnore
    public void invalidateOptionsMenu() {
        a().p();
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        a().o();
        super.onCreate(bundle);
        a().r(bundle);
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        a().x();
    }

    @DexIgnore
    @Override // com.fossil.we0
    public void onSupportActionModeFinished(ActionMode actionMode) {
    }

    @DexIgnore
    @Override // com.fossil.we0
    public void onSupportActionModeStarted(ActionMode actionMode) {
    }

    @DexIgnore
    @Override // com.fossil.we0
    public ActionMode onWindowStartingSupportActionMode(ActionMode.Callback callback) {
        return null;
    }

    @DexIgnore
    @Override // android.app.Dialog
    public void setContentView(int i) {
        a().B(i);
    }

    @DexIgnore
    @Override // android.app.Dialog
    public void setContentView(View view) {
        a().C(view);
    }

    @DexIgnore
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().D(view, layoutParams);
    }

    @DexIgnore
    @Override // android.app.Dialog
    public void setTitle(int i) {
        super.setTitle(i);
        a().G(getContext().getString(i));
    }

    @DexIgnore
    @Override // android.app.Dialog
    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        a().G(charSequence);
    }
}
