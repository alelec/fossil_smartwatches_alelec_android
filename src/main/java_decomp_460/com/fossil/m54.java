package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.IOException;
import java.nio.CharBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m54 {
    @DexIgnore
    public static CharBuffer a() {
        return CharBuffer.allocate(2048);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> T b(Readable readable, r54<T> r54) throws IOException {
        String b;
        i14.l(readable);
        i14.l(r54);
        s54 s54 = new s54(readable);
        do {
            b = s54.b();
            if (b == null) {
                break;
            }
        } while (r54.a(b));
        return r54.getResult();
    }
}
