package com.fossil;

import android.util.Log;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b71 extends x61 {
    @DexIgnore
    public static /* final */ File b; // = new File("/proc/self/fd");
    @DexIgnore
    public static volatile int c;
    @DexIgnore
    public static volatile boolean d; // = true;
    @DexIgnore
    public static /* final */ b71 e; // = new b71();

    @DexIgnore
    public b71() {
        super(null);
    }

    @DexIgnore
    @Override // com.fossil.x61
    public boolean a(f81 f81) {
        pq7.c(f81, "size");
        if (f81 instanceof c81) {
            c81 c81 = (c81) f81;
            if (c81.d() < 100 || c81.c() < 100) {
                return false;
            }
        }
        return b();
    }

    @DexIgnore
    public final boolean b() {
        boolean z;
        boolean z2 = false;
        synchronized (this) {
            int i = c;
            c = i + 1;
            if (i >= 50) {
                c = 0;
                String[] list = b.list();
                if (list == null) {
                    list = new String[0];
                }
                int length = list.length;
                if (length < 750) {
                    z2 = true;
                }
                d = z2;
                if (d && q81.c.a() && q81.c.b() <= 5) {
                    Log.println(5, "LimitedFileDescriptorHardwareBitmapService", "Unable to allocate more hardware bitmaps. Number of used file descriptors: " + length);
                }
            }
            z = d;
        }
        return z;
    }
}
