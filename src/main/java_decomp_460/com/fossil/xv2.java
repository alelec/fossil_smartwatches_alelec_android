package com.fossil;

import android.content.Context;
import android.util.Log;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xv2<T> {
    @DexIgnore
    public static /* final */ Object g; // = new Object();
    @DexIgnore
    public static volatile gw2 h;
    @DexIgnore
    public static lw2 i; // = new lw2(zv2.f4542a);
    @DexIgnore
    public static /* final */ AtomicInteger j; // = new AtomicInteger();

    @DexIgnore
    /* renamed from: a */
    public /* final */ hw2 f4184a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ T c;
    @DexIgnore
    public volatile int d;
    @DexIgnore
    public volatile T e;
    @DexIgnore
    public /* final */ boolean f;

    /*
    static {
        new AtomicReference();
    }
    */

    @DexIgnore
    public xv2(hw2 hw2, String str, T t, boolean z) {
        this.d = -1;
        if (hw2.f1545a != null) {
            this.f4184a = hw2;
            this.b = str;
            this.c = t;
            this.f = z;
            return;
        }
        throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
    }

    @DexIgnore
    public /* synthetic */ xv2(hw2 hw2, String str, Object obj, boolean z, dw2 dw2) {
        this(hw2, str, obj, z);
    }

    @DexIgnore
    public static void g() {
        j.incrementAndGet();
    }

    @DexIgnore
    public static void h(Context context) {
        synchronized (g) {
            gw2 gw2 = h;
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            if (gw2 == null || gw2.a() != context) {
                jv2.d();
                jw2.b();
                sv2.b();
                h = new kv2(context, ww2.a(new aw2(context)));
                j.incrementAndGet();
            }
        }
    }

    @DexIgnore
    public static xv2<Double> i(hw2 hw2, String str, double d2, boolean z) {
        return new fw2(hw2, str, Double.valueOf(d2), true);
    }

    @DexIgnore
    public static xv2<Long> j(hw2 hw2, String str, long j2, boolean z) {
        return new dw2(hw2, str, Long.valueOf(j2), true);
    }

    @DexIgnore
    public static xv2<String> k(hw2 hw2, String str, String str2, boolean z) {
        return new ew2(hw2, str, str2, true);
    }

    @DexIgnore
    public static xv2<Boolean> l(hw2 hw2, String str, boolean z, boolean z2) {
        return new cw2(hw2, str, Boolean.valueOf(z), true);
    }

    @DexIgnore
    public static final /* synthetic */ boolean p() {
        return true;
    }

    @DexIgnore
    public abstract T e(Object obj);

    @DexIgnore
    public final String f(String str) {
        if (str != null && str.isEmpty()) {
            return this.b;
        }
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf(this.b);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    @DexIgnore
    public final String n() {
        return f(this.f4184a.c);
    }

    @DexIgnore
    public final T o() {
        T t;
        T t2;
        Object zza;
        if (!this.f) {
            sw2.h(i.a(this.b), "Attempt to access PhenotypeFlag not via codegen. All new PhenotypeFlags must be accessed through codegen APIs. If you believe you are seeing this error by mistake, you can add your flag to the exemption list located at //java/com/google/android/libraries/phenotype/client/lockdown/flags.textproto. Send the addition CL to ph-reviews@. See go/phenotype-android-codegen for information about generated code. See go/ph-lockdown for more information about this error.");
        }
        int i2 = j.get();
        if (this.d < i2) {
            synchronized (this) {
                if (this.d < i2) {
                    gw2 gw2 = h;
                    sw2.h(gw2 != null, "Must call PhenotypeFlag.init() first");
                    String str = (String) sv2.a(gw2.a()).zza("gms:phenotype:phenotype_flag:debug_bypass_phenotype");
                    if (!(str != null && gv2.c.matcher(str).matches())) {
                        nv2 a2 = this.f4184a.f1545a != null ? vv2.b(gw2.a(), this.f4184a.f1545a) ? jv2.a(gw2.a().getContentResolver(), this.f4184a.f1545a) : null : jw2.a(gw2.a(), null);
                        if (!(a2 == null || (zza = a2.zza(n())) == null)) {
                            t = e(zza);
                        }
                        t = null;
                    } else {
                        if (Log.isLoggable("PhenotypeFlag", 3)) {
                            String valueOf = String.valueOf(n());
                            Log.d("PhenotypeFlag", valueOf.length() != 0 ? "Bypass reading Phenotype values for flag: ".concat(valueOf) : new String("Bypass reading Phenotype values for flag: "));
                        }
                        t = null;
                    }
                    if (t == null) {
                        Object zza2 = sv2.a(gw2.a()).zza(f(this.f4184a.b));
                        t = zza2 != null ? e(zza2) : null;
                        if (t == null) {
                            t = this.c;
                        }
                    }
                    tw2<tv2> zza3 = gw2.b().zza();
                    if (zza3.zza()) {
                        String a3 = zza3.zzb().a(this.f4184a.f1545a, null, this.f4184a.c, this.b);
                        t2 = a3 == null ? this.c : e(a3);
                    } else {
                        t2 = t;
                    }
                    this.e = t2;
                    this.d = i2;
                }
            }
        }
        return this.e;
    }
}
