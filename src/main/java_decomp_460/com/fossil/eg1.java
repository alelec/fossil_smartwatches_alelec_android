package com.fossil;

import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eg1 implements ImageHeaderParser {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ byte[] f936a; // = "Exif\u0000\u0000".getBytes(Charset.forName("UTF-8"));
    @DexIgnore
    public static /* final */ int[] b; // = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8};

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ByteBuffer f937a;

        @DexIgnore
        public a(ByteBuffer byteBuffer) {
            this.f937a = byteBuffer;
            byteBuffer.order(ByteOrder.BIG_ENDIAN);
        }

        @DexIgnore
        @Override // com.fossil.eg1.c
        public int a() throws c.a {
            return (c() << 8) | c();
        }

        @DexIgnore
        @Override // com.fossil.eg1.c
        public int b(byte[] bArr, int i) {
            int min = Math.min(i, this.f937a.remaining());
            if (min == 0) {
                return -1;
            }
            this.f937a.get(bArr, 0, min);
            return min;
        }

        @DexIgnore
        @Override // com.fossil.eg1.c
        public short c() throws c.a {
            if (this.f937a.remaining() >= 1) {
                return (short) (this.f937a.get() & 255);
            }
            throw new c.a();
        }

        @DexIgnore
        @Override // com.fossil.eg1.c
        public long skip(long j) {
            int min = (int) Math.min((long) this.f937a.remaining(), j);
            ByteBuffer byteBuffer = this.f937a;
            byteBuffer.position(byteBuffer.position() + min);
            return (long) min;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ByteBuffer f938a;

        @DexIgnore
        public b(byte[] bArr, int i) {
            this.f938a = (ByteBuffer) ByteBuffer.wrap(bArr).order(ByteOrder.BIG_ENDIAN).limit(i);
        }

        @DexIgnore
        public short a(int i) {
            if (c(i, 2)) {
                return this.f938a.getShort(i);
            }
            return -1;
        }

        @DexIgnore
        public int b(int i) {
            if (c(i, 4)) {
                return this.f938a.getInt(i);
            }
            return -1;
        }

        @DexIgnore
        public final boolean c(int i, int i2) {
            return this.f938a.remaining() - i >= i2;
        }

        @DexIgnore
        public int d() {
            return this.f938a.remaining();
        }

        @DexIgnore
        public void e(ByteOrder byteOrder) {
            this.f938a.order(byteOrder);
        }
    }

    @DexIgnore
    public interface c {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends IOException {
            @DexIgnore
            public static /* final */ long serialVersionUID; // = 1;

            @DexIgnore
            public a() {
                super("Unexpectedly reached end of a file");
            }
        }

        @DexIgnore
        int a() throws IOException;

        @DexIgnore
        int b(byte[] bArr, int i) throws IOException;

        @DexIgnore
        short c() throws IOException;

        @DexIgnore
        long skip(long j) throws IOException;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ InputStream f939a;

        @DexIgnore
        public d(InputStream inputStream) {
            this.f939a = inputStream;
        }

        @DexIgnore
        @Override // com.fossil.eg1.c
        public int a() throws IOException {
            return (c() << 8) | c();
        }

        @DexIgnore
        @Override // com.fossil.eg1.c
        public int b(byte[] bArr, int i) throws IOException {
            int i2 = 0;
            int i3 = 0;
            while (i3 < i) {
                i2 = this.f939a.read(bArr, i3, i - i3);
                if (i2 == -1) {
                    break;
                }
                i3 += i2;
            }
            if (i3 != 0 || i2 != -1) {
                return i3;
            }
            throw new c.a();
        }

        @DexIgnore
        @Override // com.fossil.eg1.c
        public short c() throws IOException {
            int read = this.f939a.read();
            if (read != -1) {
                return (short) read;
            }
            throw new c.a();
        }

        @DexIgnore
        @Override // com.fossil.eg1.c
        public long skip(long j) throws IOException {
            if (j < 0) {
                return 0;
            }
            long j2 = j;
            while (j2 > 0) {
                long skip = this.f939a.skip(j2);
                if (skip <= 0) {
                    if (this.f939a.read() == -1) {
                        break;
                    }
                    skip = 1;
                }
                j2 -= skip;
            }
            return j - j2;
        }
    }

    @DexIgnore
    public static int d(int i, int i2) {
        return i + 2 + (i2 * 12);
    }

    @DexIgnore
    public static boolean g(int i) {
        return (i & 65496) == 65496 || i == 19789 || i == 18761;
    }

    @DexIgnore
    public static int j(b bVar) {
        ByteOrder byteOrder;
        short a2 = bVar.a(6);
        if (a2 == 18761) {
            byteOrder = ByteOrder.LITTLE_ENDIAN;
        } else if (a2 != 19789) {
            if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                Log.d("DfltImageHeaderParser", "Unknown endianness = " + ((int) a2));
            }
            byteOrder = ByteOrder.BIG_ENDIAN;
        } else {
            byteOrder = ByteOrder.BIG_ENDIAN;
        }
        bVar.e(byteOrder);
        int b2 = bVar.b(10) + 6;
        short a3 = bVar.a(b2);
        for (int i = 0; i < a3; i++) {
            int d2 = d(b2, i);
            short a4 = bVar.a(d2);
            if (a4 == 274) {
                short a5 = bVar.a(d2 + 2);
                if (a5 >= 1 && a5 <= 12) {
                    int b3 = bVar.b(d2 + 4);
                    if (b3 >= 0) {
                        if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                            Log.d("DfltImageHeaderParser", "Got tagIndex=" + i + " tagType=" + ((int) a4) + " formatCode=" + ((int) a5) + " componentCount=" + b3);
                        }
                        int i2 = b3 + b[a5];
                        if (i2 <= 4) {
                            int i3 = d2 + 8;
                            if (i3 < 0 || i3 > bVar.d()) {
                                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                                    Log.d("DfltImageHeaderParser", "Illegal tagValueOffset=" + i3 + " tagType=" + ((int) a4));
                                }
                            } else if (i2 >= 0 && i2 + i3 <= bVar.d()) {
                                return bVar.a(i3);
                            } else {
                                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                                    Log.d("DfltImageHeaderParser", "Illegal number of bytes for TI tag data tagType=" + ((int) a4));
                                }
                            }
                        } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                            Log.d("DfltImageHeaderParser", "Got byte count > 4, not orientation, continuing, formatCode=" + ((int) a5));
                        }
                    } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                        Log.d("DfltImageHeaderParser", "Negative tiff component count");
                    }
                } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Got invalid format code = " + ((int) a5));
                }
            }
        }
        return -1;
    }

    @DexIgnore
    @Override // com.bumptech.glide.load.ImageHeaderParser
    public ImageHeaderParser.ImageType a(ByteBuffer byteBuffer) throws IOException {
        ik1.d(byteBuffer);
        return f(new a(byteBuffer));
    }

    @DexIgnore
    @Override // com.bumptech.glide.load.ImageHeaderParser
    public ImageHeaderParser.ImageType b(InputStream inputStream) throws IOException {
        ik1.d(inputStream);
        return f(new d(inputStream));
    }

    @DexIgnore
    @Override // com.bumptech.glide.load.ImageHeaderParser
    public int c(InputStream inputStream, od1 od1) throws IOException {
        ik1.d(inputStream);
        d dVar = new d(inputStream);
        ik1.d(od1);
        return e(dVar, od1);
    }

    @DexIgnore
    public final int e(c cVar, od1 od1) throws IOException {
        try {
            int a2 = cVar.a();
            if (!g(a2)) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Parser doesn't handle magic number: " + a2);
                }
                return -1;
            }
            int i = i(cVar);
            if (i == -1) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Failed to parse exif segment length, or exif segment not found");
                }
                return -1;
            }
            byte[] bArr = (byte[]) od1.g(i, byte[].class);
            try {
                return k(cVar, bArr, i);
            } finally {
                od1.f(bArr);
            }
        } catch (c.a e) {
            return -1;
        }
    }

    @DexIgnore
    public final ImageHeaderParser.ImageType f(c cVar) throws IOException {
        try {
            int a2 = cVar.a();
            if (a2 == 65496) {
                return ImageHeaderParser.ImageType.JPEG;
            }
            int c2 = (a2 << 8) | cVar.c();
            if (c2 == 4671814) {
                return ImageHeaderParser.ImageType.GIF;
            }
            int c3 = (c2 << 8) | cVar.c();
            if (c3 == -1991225785) {
                cVar.skip(21);
                try {
                    return cVar.c() >= 3 ? ImageHeaderParser.ImageType.PNG_A : ImageHeaderParser.ImageType.PNG;
                } catch (c.a e) {
                    return ImageHeaderParser.ImageType.PNG;
                }
            } else if (c3 != 1380533830) {
                return ImageHeaderParser.ImageType.UNKNOWN;
            } else {
                cVar.skip(4);
                if (((cVar.a() << 16) | cVar.a()) != 1464156752) {
                    return ImageHeaderParser.ImageType.UNKNOWN;
                }
                int a3 = (cVar.a() << 16) | cVar.a();
                if ((a3 & -256) != 1448097792) {
                    return ImageHeaderParser.ImageType.UNKNOWN;
                }
                int i = a3 & 255;
                if (i == 88) {
                    cVar.skip(4);
                    return (cVar.c() & 16) != 0 ? ImageHeaderParser.ImageType.WEBP_A : ImageHeaderParser.ImageType.WEBP;
                } else if (i != 76) {
                    return ImageHeaderParser.ImageType.WEBP;
                } else {
                    cVar.skip(4);
                    return (cVar.c() & 8) != 0 ? ImageHeaderParser.ImageType.WEBP_A : ImageHeaderParser.ImageType.WEBP;
                }
            }
        } catch (c.a e2) {
            return ImageHeaderParser.ImageType.UNKNOWN;
        }
    }

    @DexIgnore
    public final boolean h(byte[] bArr, int i) {
        boolean z = bArr != null && i > f936a.length;
        if (z) {
            int i2 = 0;
            while (true) {
                byte[] bArr2 = f936a;
                if (i2 >= bArr2.length) {
                    break;
                } else if (bArr[i2] != bArr2[i2]) {
                    return false;
                } else {
                    i2++;
                }
            }
        }
        return z;
    }

    @DexIgnore
    public final int i(c cVar) throws IOException {
        short c2;
        int a2;
        long j;
        long skip;
        do {
            short c3 = cVar.c();
            if (c3 == 255) {
                c2 = cVar.c();
                if (c2 == 218) {
                    return -1;
                }
                if (c2 != 217) {
                    a2 = cVar.a() - 2;
                    if (c2 == 225) {
                        return a2;
                    }
                    j = (long) a2;
                    skip = cVar.skip(j);
                } else if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
                    return -1;
                } else {
                    Log.d("DfltImageHeaderParser", "Found MARKER_EOI in exif segment");
                    return -1;
                }
            } else if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
                return -1;
            } else {
                Log.d("DfltImageHeaderParser", "Unknown segmentId=" + ((int) c3));
                return -1;
            }
        } while (skip == j);
        if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
            return -1;
        }
        Log.d("DfltImageHeaderParser", "Unable to skip enough data, type: " + ((int) c2) + ", wanted to skip: " + a2 + ", but actually skipped: " + skip);
        return -1;
    }

    @DexIgnore
    public final int k(c cVar, byte[] bArr, int i) throws IOException {
        int b2 = cVar.b(bArr, i);
        if (b2 != i) {
            if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
                return -1;
            }
            Log.d("DfltImageHeaderParser", "Unable to read exif segment data, length: " + i + ", actually read: " + b2);
            return -1;
        } else if (h(bArr, i)) {
            return j(new b(bArr, i));
        } else {
            if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
                return -1;
            }
            Log.d("DfltImageHeaderParser", "Missing jpeg exif preamble");
            return -1;
        }
    }
}
