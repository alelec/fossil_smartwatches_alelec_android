package com.fossil;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ne7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2510a; // = null;
    @DexIgnore
    public String b; // = null;
    @DexIgnore
    public String c; // = "0";
    @DexIgnore
    public long d; // = 0;

    @DexIgnore
    public static ne7 b(String str) {
        ne7 ne7 = new ne7();
        if (se7.f(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (!jSONObject.isNull("ui")) {
                    ne7.f2510a = jSONObject.getString("ui");
                }
                if (!jSONObject.isNull("mc")) {
                    ne7.b = jSONObject.getString("mc");
                }
                if (!jSONObject.isNull("mid")) {
                    ne7.c = jSONObject.getString("mid");
                }
                if (!jSONObject.isNull("ts")) {
                    ne7.d = jSONObject.getLong("ts");
                }
            } catch (JSONException e) {
                Log.w("MID", e);
            }
        }
        return ne7;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            se7.c(jSONObject, "ui", this.f2510a);
            se7.c(jSONObject, "mc", this.b);
            se7.c(jSONObject, "mid", this.c);
            jSONObject.put("ts", this.d);
        } catch (JSONException e) {
            Log.w("MID", e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final String toString() {
        return c().toString();
    }
}
