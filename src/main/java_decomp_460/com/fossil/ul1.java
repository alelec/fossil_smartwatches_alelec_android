package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ul1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ tl1 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ vl1 d;
    @DexIgnore
    public /* final */ wl1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ul1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ul1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                tl1 valueOf = tl1.valueOf(readString);
                int readInt = parcel.readInt();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    pq7.b(readString2, "parcel.readString()!!");
                    vl1 valueOf2 = vl1.valueOf(readString2);
                    String readString3 = parcel.readString();
                    if (readString3 != null) {
                        pq7.b(readString3, "parcel.readString()!!");
                        return new ul1(valueOf, readInt, valueOf2, wl1.valueOf(readString3));
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ul1[] newArray(int i) {
            return new ul1[i];
        }
    }

    @DexIgnore
    public ul1(tl1 tl1, int i, vl1 vl1, wl1 wl1) {
        this.b = tl1;
        this.c = i;
        this.d = vl1;
        this.e = wl1;
    }

    @DexIgnore
    public final byte[] a() {
        byte[] array = ByteBuffer.allocate(5).order(ByteOrder.LITTLE_ENDIAN).put(this.b.a()).putShort((short) this.c).put(this.d.a()).put(this.e.a()).array();
        pq7.b(array, "ByteBuffer.allocate(DATA\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(ul1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ul1 ul1 = (ul1) obj;
            if (this.b != ul1.b) {
                return false;
            }
            if (this.c != ul1.c) {
                return false;
            }
            if (this.d != ul1.d) {
                return false;
            }
            return this.e == ul1.e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.calibration.HandMovingConfig");
    }

    @DexIgnore
    public final int getDegree() {
        return this.c;
    }

    @DexIgnore
    public final tl1 getHandId() {
        return this.b;
    }

    @DexIgnore
    public final vl1 getMovingDirection() {
        return this.d;
    }

    @DexIgnore
    public final wl1 getMovingSpeed() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int i = this.c;
        return (((((hashCode * 31) + i) * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.X0, ey1.a(this.b)), jd0.Y0, Integer.valueOf(this.c)), jd0.Z0, ey1.a(this.d)), jd0.a1, ey1.a(this.e));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
    }
}
