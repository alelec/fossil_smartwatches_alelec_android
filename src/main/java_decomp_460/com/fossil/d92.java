package com.fossil;

import android.os.Bundle;
import com.fossil.m62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface d92 {
    @DexIgnore
    boolean a();

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    void d(int i);

    @DexIgnore
    void e(Bundle bundle);

    @DexIgnore
    Object f();  // void declaration

    @DexIgnore
    void i(z52 z52, m62<?> m62, boolean z);

    @DexIgnore
    <A extends m62.b, T extends i72<? extends z62, A>> T j(T t);

    @DexIgnore
    <A extends m62.b, R extends z62, T extends i72<R, A>> T k(T t);
}
