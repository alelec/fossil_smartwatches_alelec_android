package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import com.fossil.nt6;
import com.fossil.st6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pt6 extends pv5 implements x47 {
    @DexIgnore
    public static String k; // = "";
    @DexIgnore
    public static boolean l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public st6 h;
    @DexIgnore
    public g37<wb5> i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return b();
        }

        @DexIgnore
        public final String b() {
            return pt6.k;
        }

        @DexIgnore
        public final pt6 c() {
            return new pt6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<st6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ pt6 f2869a;

        @DexIgnore
        public b(pt6 pt6) {
            this.f2869a = pt6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(st6.a aVar) {
            String a2;
            if (aVar != null && (a2 = aVar.a()) != null) {
                this.f2869a.N6(a2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pt6 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements nt6.b {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ c f2870a;

            @DexIgnore
            public a(c cVar) {
                this.f2870a = cVar;
            }

            @DexIgnore
            @Override // com.fossil.nt6.b
            public void a(String str) {
                pq7.c(str, "themeName");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("UserCustomizeThemeFragment", "showRenamePresetDialog - themeName=" + str);
                if (!TextUtils.isEmpty(str)) {
                    pt6.K6(this.f2870a.b).i(str);
                }
            }

            @DexIgnore
            @Override // com.fossil.nt6.b
            public void onCancel() {
                FLogger.INSTANCE.getLocal().d("UserCustomizeThemeFragment", "showRenamePresetDialog - onCancel");
            }
        }

        @DexIgnore
        public c(pt6 pt6) {
            this.b = pt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            nt6.h.a("", new a(this)).show(this.b.getChildFragmentManager(), "RenamePresetDialogFragment");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pt6 b;

        @DexIgnore
        public d(pt6 pt6) {
            this.b = pt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ st6 K6(pt6 pt6) {
        st6 st6 = pt6.h;
        if (st6 != null) {
            return st6;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.x47
    public void C3(int i2, int i3) {
        FLogger.INSTANCE.getLocal().d("UserCustomizeThemeFragment", "onColorSelected");
    }

    @DexIgnore
    public final void M6(ViewPager viewPager) {
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        hr4 hr4 = new hr4(childFragmentManager);
        it6 it6 = new it6();
        String string = PortfolioApp.h0.c().getString(2131887582);
        pq7.b(string, "PortfolioApp.instance.getString(R.string.text)");
        hr4.x(it6, string);
        ds6 ds6 = new ds6();
        String string2 = PortfolioApp.h0.c().getString(2131887317);
        pq7.b(string2, "PortfolioApp.instance.getString(R.string.button)");
        hr4.x(ds6, string2);
        xr6 xr6 = new xr6();
        String string3 = PortfolioApp.h0.c().getString(2131887294);
        pq7.b(string3, "PortfolioApp.instance.ge\u2026ring(R.string.background)");
        hr4.x(xr6, string3);
        ws6 ws6 = new ws6();
        String string4 = PortfolioApp.h0.c().getString(2131887553);
        pq7.b(string4, "PortfolioApp.instance.getString(R.string.ring)");
        hr4.x(ws6, string4);
        rr6 rr6 = new rr6();
        String string5 = PortfolioApp.h0.c().getString(2131887264);
        pq7.b(string5, "PortfolioApp.instance.getString(R.string.activity)");
        hr4.x(rr6, string5);
        lr6 lr6 = new lr6();
        String string6 = PortfolioApp.h0.c().getString(2131887263);
        pq7.b(string6, "PortfolioApp.instance.ge\u2026(R.string.active_minutes)");
        hr4.x(lr6, string6);
        fr6 fr6 = new fr6();
        String string7 = PortfolioApp.h0.c().getString(2131887262);
        pq7.b(string7, "PortfolioApp.instance.ge\u2026R.string.active_calories)");
        hr4.x(fr6, string7);
        qs6 qs6 = new qs6();
        String string8 = PortfolioApp.h0.c().getString(2131887422);
        pq7.b(string8, "PortfolioApp.instance.ge\u2026ring(R.string.heart_rate)");
        hr4.x(qs6, string8);
        ct6 ct6 = new ct6();
        String string9 = PortfolioApp.h0.c().getString(2131887572);
        pq7.b(string9, "PortfolioApp.instance.getString(R.string.sleep)");
        hr4.x(ct6, string9);
        ks6 ks6 = new ks6();
        String string10 = PortfolioApp.h0.c().getString(2131887415);
        pq7.b(string10, "PortfolioApp.instance.ge\u2026g(R.string.goal_tracking)");
        hr4.x(ks6, string10);
        is6 is6 = new is6();
        String string11 = PortfolioApp.h0.c().getString(2131887409);
        pq7.b(string11, "PortfolioApp.instance.getString(R.string.font)");
        hr4.x(is6, string11);
        viewPager.setAdapter(hr4);
    }

    @DexIgnore
    public final void N6(String str) {
        pq7.c(str, "name");
        g37<wb5> g37 = this.i;
        if (g37 != null) {
            wb5 a2 = g37.a();
            if (a2 != null) {
                a2.s.setText(str);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        String str;
        pq7.c(layoutInflater, "inflater");
        wb5 wb5 = (wb5) aq0.f(LayoutInflater.from(getContext()), 2131558633, null, false, A6());
        Bundle arguments = getArguments();
        if (arguments == null || (str = arguments.getString("THEME_ID")) == null) {
            str = "";
        }
        k = str;
        Bundle arguments2 = getArguments();
        l = arguments2 != null ? arguments2.getBoolean("THEME_MODE_EDIT") : false;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UserCustomizeThemeFragment", "themeId=" + k + " isModeEdit=" + l);
        PortfolioApp.h0.c().M().d(new rt6()).a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(st6.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026emeViewModel::class.java)");
            st6 st6 = (st6) a2;
            this.h = st6;
            if (st6 != null) {
                st6.f().h(getViewLifecycleOwner(), new b(this));
                st6 st62 = this.h;
                if (st62 != null) {
                    st62.g();
                    this.i = new g37<>(this, wb5);
                    pq7.b(wb5, "binding");
                    return wb5.n();
                }
                pq7.n("mViewModel");
                throw null;
            }
            pq7.n("mViewModel");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<wb5> g37 = this.i;
        if (g37 != null) {
            wb5 a2 = g37.a();
            if (a2 != null) {
                ViewPager viewPager = a2.u;
                pq7.b(viewPager, "it.viewPager");
                M6(viewPager);
                a2.r.I(a2.u, false);
                a2.s.setOnClickListener(new c(this));
                a2.q.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.x47
    public void q3(int i2) {
        FLogger.INSTANCE.getLocal().d("UserCustomizeThemeFragment", "onColorSelected");
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
