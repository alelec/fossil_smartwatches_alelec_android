package com.fossil;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ai5 {
    IMPERIAL("imperial"),
    METRIC("metric");
    
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    public ai5(String str) {
        this.value = str;
    }

    @DexIgnore
    public static ai5 fromString(String str) {
        return (TextUtils.isEmpty(str) || !str.equalsIgnoreCase("imperial")) ? METRIC : IMPERIAL;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }
}
