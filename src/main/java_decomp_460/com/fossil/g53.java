package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g53 implements xw2<f53> {
    @DexIgnore
    public static g53 c; // = new g53();
    @DexIgnore
    public /* final */ xw2<f53> b;

    @DexIgnore
    public g53() {
        this(ww2.b(new i53()));
    }

    @DexIgnore
    public g53(xw2<f53> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static long A() {
        return ((f53) c.zza()).u();
    }

    @DexIgnore
    public static long B() {
        return ((f53) c.zza()).w();
    }

    @DexIgnore
    public static long C() {
        return ((f53) c.zza()).b();
    }

    @DexIgnore
    public static long D() {
        return ((f53) c.zza()).h();
    }

    @DexIgnore
    public static long E() {
        return ((f53) c.zza()).l();
    }

    @DexIgnore
    public static long F() {
        return ((f53) c.zza()).o();
    }

    @DexIgnore
    public static long G() {
        return ((f53) c.zza()).i();
    }

    @DexIgnore
    public static long a() {
        return ((f53) c.zza()).k();
    }

    @DexIgnore
    public static long b() {
        return ((f53) c.zza()).t();
    }

    @DexIgnore
    public static long c() {
        return ((f53) c.zza()).v();
    }

    @DexIgnore
    public static long d() {
        return ((f53) c.zza()).r();
    }

    @DexIgnore
    public static long e() {
        return ((f53) c.zza()).s();
    }

    @DexIgnore
    public static long f() {
        return ((f53) c.zza()).m();
    }

    @DexIgnore
    public static String g() {
        return ((f53) c.zza()).q();
    }

    @DexIgnore
    public static long h() {
        return ((f53) c.zza()).j();
    }

    @DexIgnore
    public static long i() {
        return ((f53) c.zza()).zza();
    }

    @DexIgnore
    public static long j() {
        return ((f53) c.zza()).zzb();
    }

    @DexIgnore
    public static String k() {
        return ((f53) c.zza()).zzc();
    }

    @DexIgnore
    public static String l() {
        return ((f53) c.zza()).zzd();
    }

    @DexIgnore
    public static long m() {
        return ((f53) c.zza()).zze();
    }

    @DexIgnore
    public static long n() {
        return ((f53) c.zza()).zzf();
    }

    @DexIgnore
    public static long o() {
        return ((f53) c.zza()).zzg();
    }

    @DexIgnore
    public static long p() {
        return ((f53) c.zza()).zzh();
    }

    @DexIgnore
    public static long q() {
        return ((f53) c.zza()).g();
    }

    @DexIgnore
    public static long r() {
        return ((f53) c.zza()).a();
    }

    @DexIgnore
    public static long s() {
        return ((f53) c.zza()).p();
    }

    @DexIgnore
    public static long t() {
        return ((f53) c.zza()).zzl();
    }

    @DexIgnore
    public static long u() {
        return ((f53) c.zza()).zzm();
    }

    @DexIgnore
    public static long v() {
        return ((f53) c.zza()).e();
    }

    @DexIgnore
    public static long w() {
        return ((f53) c.zza()).n();
    }

    @DexIgnore
    public static long x() {
        return ((f53) c.zza()).f();
    }

    @DexIgnore
    public static long y() {
        return ((f53) c.zza()).c();
    }

    @DexIgnore
    public static long z() {
        return ((f53) c.zza()).d();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ f53 zza() {
        return this.b.zza();
    }
}
