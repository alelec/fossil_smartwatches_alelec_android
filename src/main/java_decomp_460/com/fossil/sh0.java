package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sh0 extends kh0 {
    @DexIgnore
    public /* final */ WeakReference<Context> b;

    @DexIgnore
    public sh0(Context context, Resources resources) {
        super(resources);
        this.b = new WeakReference<>(context);
    }

    @DexIgnore
    @Override // com.fossil.kh0, android.content.res.Resources
    public Drawable getDrawable(int i) throws Resources.NotFoundException {
        Drawable drawable = super.getDrawable(i);
        Context context = this.b.get();
        if (!(drawable == null || context == null)) {
            jh0.h().x(context, i, drawable);
        }
        return drawable;
    }
}
