package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xc3 extends ds2 implements wc3 {
    @DexIgnore
    public xc3() {
        super("com.google.android.gms.maps.internal.IOnPolygonClickListener");
    }

    @DexIgnore
    @Override // com.fossil.ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        s2(ps2.e(parcel.readStrongBinder()));
        parcel2.writeNoException();
        return true;
    }
}
