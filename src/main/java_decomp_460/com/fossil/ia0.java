package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ia0 extends va0 {
    @DexIgnore
    public static /* final */ ha0 CREATOR; // = new ha0(null);
    @DexIgnore
    public r90[] c;
    @DexIgnore
    public /* final */ v90 d;

    @DexIgnore
    public /* synthetic */ ia0(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = new r90[0];
        this.d = v90.SIMPLE_MOVEMENT;
        Object[] readArray = parcel.readArray(r90.class.getClassLoader());
        if (readArray != null) {
            this.c = (r90[]) readArray;
            return;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.model.microapp.animation.HandAnimation>");
    }

    @DexIgnore
    public ia0(r90[] r90Arr) {
        super(aa0.ANIMATION);
        this.c = new r90[0];
        this.d = v90.SIMPLE_MOVEMENT;
        this.c = r90Arr;
    }

    @DexIgnore
    @Override // com.fossil.va0
    public byte[] a() {
        ByteBuffer order = ByteBuffer.allocate((this.c.length * 3) + 2).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order, "ByteBuffer.allocate(2 + \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.d.b);
        byte b = (byte) 0;
        order.put(b);
        r90[] r90Arr = this.c;
        for (r90 r90 : r90Arr) {
            b = (byte) (b | r90.b.b);
            ByteBuffer order2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN);
            pq7.b(order2, "ByteBuffer.allocate(3)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
            order2.put((byte) (((byte) (((byte) (((byte) (r90.c.b << 6)) | ((byte) (r90.d.b << 5)))) | 0)) | r90.e.b));
            order2.putShort(r90.f);
            byte[] array = order2.array();
            pq7.b(array, "dataBuffer.array()");
            order.put(array);
        }
        order.put(1, b);
        byte[] array2 = order.array();
        pq7.b(array2, "byteBuffer.array()");
        return array2;
    }

    @DexIgnore
    @Override // com.fossil.va0
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.va0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(ia0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.c, ((ia0) obj).c);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.AnimationInstr");
    }

    @DexIgnore
    @Override // com.fossil.va0
    public int hashCode() {
        return this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.va0, com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONArray jSONArray = new JSONArray();
        for (r90 r90 : this.c) {
            jSONArray.put(r90.toJSONObject());
        }
        return g80.k(super.toJSONObject(), jd0.S3, jSONArray);
    }

    @DexIgnore
    @Override // com.fossil.va0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.c, i);
        }
    }
}
