package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mt6 implements Factory<lt6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ThemeRepository> f2417a;

    @DexIgnore
    public mt6(Provider<ThemeRepository> provider) {
        this.f2417a = provider;
    }

    @DexIgnore
    public static mt6 a(Provider<ThemeRepository> provider) {
        return new mt6(provider);
    }

    @DexIgnore
    public static lt6 c(ThemeRepository themeRepository) {
        return new lt6(themeRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public lt6 get() {
        return c(this.f2417a.get());
    }
}
