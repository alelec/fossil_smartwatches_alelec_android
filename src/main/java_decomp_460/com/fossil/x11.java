package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import com.fossil.a21;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x11 implements k11 {
    @DexIgnore
    public static /* final */ String e; // = x01.f("CommandHandler");
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Map<String, k11> c; // = new HashMap();
    @DexIgnore
    public /* final */ Object d; // = new Object();

    @DexIgnore
    public x11(Context context) {
        this.b = context;
    }

    @DexIgnore
    public static Intent a(Context context) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_CONSTRAINTS_CHANGED");
        return intent;
    }

    @DexIgnore
    public static Intent b(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_DELAY_MET");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public static Intent c(Context context, String str, boolean z) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_EXECUTION_COMPLETED");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        intent.putExtra("KEY_NEEDS_RESCHEDULE", z);
        return intent;
    }

    @DexIgnore
    public static Intent e(Context context) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_RESCHEDULE");
        return intent;
    }

    @DexIgnore
    public static Intent f(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_SCHEDULE_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public static Intent g(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_STOP_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public static boolean n(Bundle bundle, String... strArr) {
        if (bundle == null || bundle.isEmpty()) {
            return false;
        }
        for (String str : strArr) {
            if (bundle.get(str) == null) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.k11
    public void d(String str, boolean z) {
        synchronized (this.d) {
            k11 remove = this.c.remove(str);
            if (remove != null) {
                remove.d(str, z);
            }
        }
    }

    @DexIgnore
    public final void h(Intent intent, int i, a21 a21) {
        x01.c().a(e, String.format("Handling constraints changed %s", intent), new Throwable[0]);
        new y11(this.b, i, a21).a();
    }

    @DexIgnore
    public final void i(Intent intent, int i, a21 a21) {
        Bundle extras = intent.getExtras();
        synchronized (this.d) {
            String string = extras.getString("KEY_WORKSPEC_ID");
            x01.c().a(e, String.format("Handing delay met for %s", string), new Throwable[0]);
            if (!this.c.containsKey(string)) {
                z11 z11 = new z11(this.b, i, string, a21);
                this.c.put(string, z11);
                z11.e();
            } else {
                x01.c().a(e, String.format("WorkSpec %s is already being handled for ACTION_DELAY_MET", string), new Throwable[0]);
            }
        }
    }

    @DexIgnore
    public final void j(Intent intent, int i) {
        Bundle extras = intent.getExtras();
        String string = extras.getString("KEY_WORKSPEC_ID");
        boolean z = extras.getBoolean("KEY_NEEDS_RESCHEDULE");
        x01.c().a(e, String.format("Handling onExecutionCompleted %s, %s", intent, Integer.valueOf(i)), new Throwable[0]);
        d(string, z);
    }

    @DexIgnore
    public final void k(Intent intent, int i, a21 a21) {
        x01.c().a(e, String.format("Handling reschedule %s, %s", intent, Integer.valueOf(i)), new Throwable[0]);
        a21.g().t();
    }

    @DexIgnore
    public final void l(Intent intent, int i, a21 a21) {
        String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        x01.c().a(e, String.format("Handling schedule work for %s", string), new Throwable[0]);
        WorkDatabase p = a21.g().p();
        p.beginTransaction();
        try {
            o31 o = p.j().o(string);
            if (o == null) {
                x01 c2 = x01.c();
                String str = e;
                c2.h(str, "Skipping scheduling " + string + " because it's no longer in the DB", new Throwable[0]);
            } else if (o.b.isFinished()) {
                x01 c3 = x01.c();
                String str2 = e;
                c3.h(str2, "Skipping scheduling " + string + "because it is finished.", new Throwable[0]);
                p.endTransaction();
            } else {
                long a2 = o.a();
                if (!o.b()) {
                    x01.c().a(e, String.format("Setting up Alarms for %s at %s", string, Long.valueOf(a2)), new Throwable[0]);
                    w11.c(this.b, a21.g(), string, a2);
                } else {
                    x01.c().a(e, String.format("Opportunistically setting an alarm for %s at %s", string, Long.valueOf(a2)), new Throwable[0]);
                    w11.c(this.b, a21.g(), string, a2);
                    a21.k(new a21.b(a21, a(this.b), i));
                }
                p.setTransactionSuccessful();
                p.endTransaction();
            }
        } finally {
            p.endTransaction();
        }
    }

    @DexIgnore
    public final void m(Intent intent, a21 a21) {
        String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        x01.c().a(e, String.format("Handing stopWork work for %s", string), new Throwable[0]);
        a21.g().y(string);
        w11.a(this.b, a21.g(), string);
        a21.d(string, false);
    }

    @DexIgnore
    public boolean o() {
        boolean z;
        synchronized (this.d) {
            z = !this.c.isEmpty();
        }
        return z;
    }

    @DexIgnore
    public void p(Intent intent, int i, a21 a21) {
        String action = intent.getAction();
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action)) {
            h(intent, i, a21);
        } else if ("ACTION_RESCHEDULE".equals(action)) {
            k(intent, i, a21);
        } else {
            if (!n(intent.getExtras(), "KEY_WORKSPEC_ID")) {
                x01.c().b(e, String.format("Invalid request for %s, requires %s.", action, "KEY_WORKSPEC_ID"), new Throwable[0]);
            } else if ("ACTION_SCHEDULE_WORK".equals(action)) {
                l(intent, i, a21);
            } else if ("ACTION_DELAY_MET".equals(action)) {
                i(intent, i, a21);
            } else if ("ACTION_STOP_WORK".equals(action)) {
                m(intent, a21);
            } else if ("ACTION_EXECUTION_COMPLETED".equals(action)) {
                j(intent, i);
            } else {
                x01.c().h(e, String.format("Ignoring intent %s", intent), new Throwable[0]);
            }
        }
    }
}
