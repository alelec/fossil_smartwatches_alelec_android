package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sq1 extends jq1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<sq1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public sq1 createFromParcel(Parcel parcel) {
            return new sq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public sq1[] newArray(int i) {
            return new sq1[i];
        }
    }

    @DexIgnore
    public sq1(byte b, int i, long j) {
        super(np1.WORKOUT_GPS_DISTANCE, b, i);
        this.e = j;
    }

    @DexIgnore
    public /* synthetic */ sq1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.e = parcel.readLong();
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(sq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.e == ((sq1) obj).e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.WorkoutGPSDistanceRequest");
    }

    @DexIgnore
    public final long getSessionId() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public int hashCode() {
        return (super.hashCode() * 31) + Long.valueOf(this.e).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.ox1, com.fossil.jq1
    public JSONObject toJSONObject() {
        return g80.k(super.toJSONObject(), jd0.H5, Long.valueOf(this.e));
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.e);
        }
    }
}
