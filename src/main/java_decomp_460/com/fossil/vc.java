package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class vc {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f3746a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[lt.values().length];
        f3746a = iArr;
        iArr[lt.JSON_FILE_EVENT.ordinal()] = 1;
        f3746a[lt.HEARTBEAT_EVENT.ordinal()] = 2;
        f3746a[lt.CONNECTION_PARAM_CHANGE_EVENT.ordinal()] = 3;
        f3746a[lt.APP_NOTIFICATION_EVENT.ordinal()] = 4;
        f3746a[lt.MUSIC_EVENT.ordinal()] = 5;
        f3746a[lt.BACKGROUND_SYNC_EVENT.ordinal()] = 6;
        f3746a[lt.SERVICE_CHANGE_EVENT.ordinal()] = 7;
        f3746a[lt.MICRO_APP_EVENT.ordinal()] = 8;
        f3746a[lt.TIME_SYNC_EVENT.ordinal()] = 9;
        f3746a[lt.AUTHENTICATION_REQUEST_EVENT.ordinal()] = 10;
        f3746a[lt.BATTERY_EVENT.ordinal()] = 11;
        f3746a[lt.ENCRYPTED_DATA.ordinal()] = 12;
        int[] iArr2 = new int[do1.values().length];
        b = iArr2;
        iArr2[do1.ACCEPT_PHONE_CALL.ordinal()] = 1;
        b[do1.REJECT_PHONE_CALL.ordinal()] = 2;
        b[do1.DISMISS_NOTIFICATION.ordinal()] = 3;
        b[do1.REPLY_MESSAGE.ordinal()] = 4;
    }
    */
}
