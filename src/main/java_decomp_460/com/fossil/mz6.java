package com.fossil;

import com.portfolio.platform.uirenew.signup.SignUpActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mz6 implements MembersInjector<SignUpActivity> {
    @DexIgnore
    public static void a(SignUpActivity signUpActivity, un5 un5) {
        signUpActivity.B = un5;
    }

    @DexIgnore
    public static void b(SignUpActivity signUpActivity, vn5 vn5) {
        signUpActivity.C = vn5;
    }

    @DexIgnore
    public static void c(SignUpActivity signUpActivity, wn5 wn5) {
        signUpActivity.E = wn5;
    }

    @DexIgnore
    public static void d(SignUpActivity signUpActivity, xn5 xn5) {
        signUpActivity.D = xn5;
    }

    @DexIgnore
    public static void e(SignUpActivity signUpActivity, uz6 uz6) {
        signUpActivity.A = uz6;
    }
}
