package com.fossil;

import android.view.View;
import android.widget.LinearLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ig5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ LinearLayout f1619a;
    @DexIgnore
    public /* final */ LinearLayout b;
    @DexIgnore
    public /* final */ LinearLayout c;

    @DexIgnore
    public ig5(LinearLayout linearLayout, LinearLayout linearLayout2, LinearLayout linearLayout3) {
        this.f1619a = linearLayout;
        this.b = linearLayout2;
        this.c = linearLayout3;
    }

    @DexIgnore
    public static ig5 a(View view) {
        int i;
        LinearLayout linearLayout = (LinearLayout) view.findViewById(2131362802);
        if (linearLayout != null) {
            LinearLayout linearLayout2 = (LinearLayout) view.findViewById(2131362833);
            if (linearLayout2 != null) {
                return new ig5((LinearLayout) view, linearLayout, linearLayout2);
            }
            i = 2131362833;
        } else {
            i = 2131362802;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
