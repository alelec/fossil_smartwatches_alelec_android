package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cu0;
import com.fossil.uu0;
import com.fossil.zu0;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ut0<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ jv0 f3641a;
    @DexIgnore
    public /* final */ uu0<T> b;
    @DexIgnore
    public Executor c; // = bi0.g();
    @DexIgnore
    public /* final */ List<c<T>> d; // = new CopyOnWriteArrayList();
    @DexIgnore
    public boolean e;
    @DexIgnore
    public cu0<T> f;
    @DexIgnore
    public cu0<T> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public cu0.e i; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends cu0.e {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.cu0.e
        public void a(int i, int i2) {
            ut0.this.f3641a.d(i, i2, null);
        }

        @DexIgnore
        @Override // com.fossil.cu0.e
        public void b(int i, int i2) {
            ut0.this.f3641a.b(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.cu0.e
        public void c(int i, int i2) {
            ut0.this.f3641a.c(i, i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ cu0 b;
        @DexIgnore
        public /* final */ /* synthetic */ cu0 c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;
        @DexIgnore
        public /* final */ /* synthetic */ cu0 e;
        @DexIgnore
        public /* final */ /* synthetic */ Runnable f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ zu0.c b;

            @DexIgnore
            public a(zu0.c cVar) {
                this.b = cVar;
            }

            @DexIgnore
            public void run() {
                b bVar = b.this;
                ut0 ut0 = ut0.this;
                if (ut0.h == bVar.d) {
                    ut0.d(bVar.e, bVar.c, this.b, bVar.b.g, bVar.f);
                }
            }
        }

        @DexIgnore
        public b(cu0 cu0, cu0 cu02, int i, cu0 cu03, Runnable runnable) {
            this.b = cu0;
            this.c = cu02;
            this.d = i;
            this.e = cu03;
            this.f = runnable;
        }

        @DexIgnore
        public void run() {
            ut0.this.c.execute(new a(fu0.a(this.b.f, this.c.f, ut0.this.b.b())));
        }
    }

    @DexIgnore
    public interface c<T> {
        @DexIgnore
        void a(cu0<T> cu0, cu0<T> cu02);
    }

    @DexIgnore
    public ut0(RecyclerView.g gVar, zu0.d<T> dVar) {
        this.f3641a = new tu0(gVar);
        this.b = new uu0.a(dVar).a();
    }

    @DexIgnore
    public void a(c<T> cVar) {
        this.d.add(cVar);
    }

    @DexIgnore
    public T b(int i2) {
        cu0<T> cu0 = this.f;
        if (cu0 == null) {
            cu0<T> cu02 = this.g;
            if (cu02 != null) {
                return cu02.get(i2);
            }
            throw new IndexOutOfBoundsException("Item count is zero, getItem() call is invalid");
        }
        cu0.v(i2);
        return this.f.get(i2);
    }

    @DexIgnore
    public int c() {
        cu0<T> cu0 = this.f;
        if (cu0 != null) {
            return cu0.size();
        }
        cu0<T> cu02 = this.g;
        if (cu02 == null) {
            return 0;
        }
        return cu02.size();
    }

    @DexIgnore
    public void d(cu0<T> cu0, cu0<T> cu02, zu0.c cVar, int i2, Runnable runnable) {
        cu0<T> cu03 = this.g;
        if (cu03 == null || this.f != null) {
            throw new IllegalStateException("must be in snapshot state to apply diff");
        }
        this.f = cu0;
        this.g = null;
        fu0.b(this.f3641a, cu03.f, cu0.f, cVar);
        cu0.j(cu02, this.i);
        if (!this.f.isEmpty()) {
            int c2 = fu0.c(cVar, cu03.f, cu02.f, i2);
            cu0<T> cu04 = this.f;
            cu04.v(Math.max(0, Math.min(cu04.size() - 1, c2)));
        }
        e(cu03, this.f, runnable);
    }

    @DexIgnore
    public final void e(cu0<T> cu0, cu0<T> cu02, Runnable runnable) {
        for (c<T> cVar : this.d) {
            cVar.a(cu0, cu02);
        }
        if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void f(cu0<T> cu0) {
        g(cu0, null);
    }

    @DexIgnore
    public void g(cu0<T> cu0, Runnable runnable) {
        if (cu0 != null) {
            if (this.f == null && this.g == null) {
                this.e = cu0.s();
            } else if (cu0.s() != this.e) {
                throw new IllegalArgumentException("AsyncPagedListDiffer cannot handle both contiguous and non-contiguous lists.");
            }
        }
        int i2 = this.h + 1;
        this.h = i2;
        cu0<T> cu02 = this.f;
        if (cu0 != cu02) {
            cu0<T> cu03 = this.g;
            if (cu03 == null) {
                cu03 = cu02;
            }
            if (cu0 == null) {
                int c2 = c();
                cu0<T> cu04 = this.f;
                if (cu04 != null) {
                    cu04.B(this.i);
                    this.f = null;
                } else if (this.g != null) {
                    this.g = null;
                }
                this.f3641a.c(0, c2);
                e(cu03, null, runnable);
            } else if (this.f == null && this.g == null) {
                this.f = cu0;
                cu0.j(null, this.i);
                this.f3641a.b(0, cu0.size());
                e(null, cu0, runnable);
            } else {
                cu0<T> cu05 = this.f;
                if (cu05 != null) {
                    cu05.B(this.i);
                    this.g = (cu0) this.f.D();
                    this.f = null;
                }
                cu0<T> cu06 = this.g;
                if (cu06 == null || this.f != null) {
                    throw new IllegalStateException("must be in snapshot state to diff");
                }
                this.b.a().execute(new b(cu06, (cu0) cu0.D(), i2, cu0, runnable));
            }
        } else if (runnable != null) {
            runnable.run();
        }
    }
}
