package com.fossil;

import com.facebook.internal.FileLruCache;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f58 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ char[] f1056a; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    @DexIgnore
    public static final int c(byte[] bArr, int i) {
        int i2;
        int length = bArr.length;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i5 < length) {
            byte b = bArr[i5];
            if (b >= 0) {
                int i6 = i3 + 1;
                if (i3 == i) {
                    return i4;
                }
                if (!(b == 10 || b == 13)) {
                    if ((b >= 0 && 31 >= b) || (Byte.MAX_VALUE <= b && 159 >= b)) {
                        return -1;
                    }
                }
                if (b == 65533) {
                    return -1;
                }
                int i7 = i4 + (b < 65536 ? 1 : 2);
                i5++;
                while (i5 < length && bArr[i5] >= 0) {
                    byte b2 = bArr[i5];
                    int i8 = i6 + 1;
                    if (i6 == i) {
                        return i7;
                    }
                    if (!(b2 == 10 || b2 == 13)) {
                        if ((b2 >= 0 && 31 >= b2) || (Byte.MAX_VALUE <= b2 && 159 >= b2)) {
                            return -1;
                        }
                    }
                    if (b2 == 65533) {
                        return -1;
                    }
                    i7 += b2 < 65536 ? 1 : 2;
                    i5++;
                    i6 = i8;
                }
                i3 = i6;
                i4 = i7;
            } else {
                if ((b >> 5) == -2) {
                    int i9 = i5 + 1;
                    if (length > i9) {
                        byte b3 = bArr[i5];
                        byte b4 = bArr[i9];
                        if ((b4 & 192) == 128) {
                            int i10 = (b4 ^ 3968) ^ (b3 << 6);
                            if (i10 >= 128) {
                                int i11 = i3 + 1;
                                if (i3 == i) {
                                    return i4;
                                }
                                if (!(i10 == 10 || i10 == 13)) {
                                    if ((i10 >= 0 && 31 >= i10) || (127 <= i10 && 159 >= i10)) {
                                        return -1;
                                    }
                                }
                                if (i10 == 65533) {
                                    return -1;
                                }
                                i4 += i10 < 65536 ? 1 : 2;
                                i5 += 2;
                                i2 = i11;
                            } else if (i3 == i) {
                                return i4;
                            } else {
                                return -1;
                            }
                        } else if (i3 == i) {
                            return i4;
                        } else {
                            return -1;
                        }
                    } else if (i3 == i) {
                        return i4;
                    } else {
                        return -1;
                    }
                } else if ((b >> 4) == -2) {
                    int i12 = i5 + 2;
                    if (length > i12) {
                        byte b5 = bArr[i5];
                        byte b6 = bArr[i5 + 1];
                        if ((b6 & 192) == 128) {
                            byte b7 = bArr[i12];
                            if ((b7 & 192) == 128) {
                                int i13 = ((-123008 ^ b7) ^ (b6 << 6)) ^ (b5 << 12);
                                if (i13 < 2048) {
                                    if (i3 == i) {
                                        return i4;
                                    }
                                    return -1;
                                } else if (55296 > i13 || 57343 < i13) {
                                    i2 = i3 + 1;
                                    if (i3 == i) {
                                        return i4;
                                    }
                                    if (!(i13 == 10 || i13 == 13)) {
                                        if ((i13 >= 0 && 31 >= i13) || (127 <= i13 && 159 >= i13)) {
                                            return -1;
                                        }
                                    }
                                    if (i13 == 65533) {
                                        return -1;
                                    }
                                    i5 += 3;
                                    i4 = (i13 < 65536 ? 1 : 2) + i4;
                                } else if (i3 == i) {
                                    return i4;
                                } else {
                                    return -1;
                                }
                            } else if (i3 == i) {
                                return i4;
                            } else {
                                return -1;
                            }
                        } else if (i3 == i) {
                            return i4;
                        } else {
                            return -1;
                        }
                    } else if (i3 == i) {
                        return i4;
                    } else {
                        return -1;
                    }
                } else if ((b >> 3) == -2) {
                    int i14 = i5 + 3;
                    if (length > i14) {
                        byte b8 = bArr[i5];
                        byte b9 = bArr[i5 + 1];
                        if ((b9 & 192) == 128) {
                            byte b10 = bArr[i5 + 2];
                            if ((b10 & 192) == 128) {
                                byte b11 = bArr[i14];
                                if ((b11 & 192) == 128) {
                                    int i15 = (((3678080 ^ b11) ^ (b10 << 6)) ^ (b9 << 12)) ^ (b8 << DateTimeFieldType.MINUTE_OF_DAY);
                                    if (i15 > 1114111) {
                                        if (i3 == i) {
                                            return i4;
                                        }
                                        return -1;
                                    } else if (55296 <= i15 && 57343 >= i15) {
                                        if (i3 == i) {
                                            return i4;
                                        }
                                        return -1;
                                    } else if (i15 >= 65536) {
                                        i2 = i3 + 1;
                                        if (i3 == i) {
                                            return i4;
                                        }
                                        if (!(i15 == 10 || i15 == 13)) {
                                            if ((i15 >= 0 && 31 >= i15) || (127 <= i15 && 159 >= i15)) {
                                                return -1;
                                            }
                                        }
                                        if (i15 == 65533) {
                                            return -1;
                                        }
                                        i5 += 4;
                                        i4 = (i15 < 65536 ? 1 : 2) + i4;
                                    } else if (i3 == i) {
                                        return i4;
                                    } else {
                                        return -1;
                                    }
                                } else if (i3 == i) {
                                    return i4;
                                } else {
                                    return -1;
                                }
                            } else if (i3 == i) {
                                return i4;
                            } else {
                                return -1;
                            }
                        } else if (i3 == i) {
                            return i4;
                        } else {
                            return -1;
                        }
                    } else if (i3 == i) {
                        return i4;
                    } else {
                        return -1;
                    }
                } else if (i3 == i) {
                    return i4;
                } else {
                    return -1;
                }
                i3 = i2;
            }
        }
        return i4;
    }

    @DexIgnore
    public static final void d(l48 l48, i48 i48, int i, int i2) {
        pq7.c(l48, "$this$commonWrite");
        pq7.c(i48, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        i48.v0(l48.getData$okio(), i, i2);
    }

    @DexIgnore
    public static final int e(char c) {
        char c2 = 'a';
        if ('0' <= c && '9' >= c) {
            return c - '0';
        }
        if ('a' > c || 'f' < c) {
            if ('A' > c || 'F' < c) {
                throw new IllegalArgumentException("Unexpected hex digit: " + c);
            }
            c2 = 'A';
        }
        return (c - c2) + 10;
    }

    @DexIgnore
    public static final char[] f() {
        return f1056a;
    }
}
