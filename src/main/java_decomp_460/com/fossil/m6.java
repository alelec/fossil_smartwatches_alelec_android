package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class m6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2306a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[n6.values().length];
        f2306a = iArr;
        iArr[n6.MODEL_NUMBER.ordinal()] = 1;
        f2306a[n6.SERIAL_NUMBER.ordinal()] = 2;
        f2306a[n6.FIRMWARE_VERSION.ordinal()] = 3;
        f2306a[n6.SOFTWARE_REVISION.ordinal()] = 4;
        f2306a[n6.DC.ordinal()] = 5;
        f2306a[n6.FTC.ordinal()] = 6;
        f2306a[n6.FTD.ordinal()] = 7;
        f2306a[n6.FTD_1.ordinal()] = 8;
        f2306a[n6.AUTHENTICATION.ordinal()] = 9;
        f2306a[n6.ASYNC.ordinal()] = 10;
        int[] iArr2 = new int[n6.values().length];
        b = iArr2;
        iArr2[n6.MODEL_NUMBER.ordinal()] = 1;
        b[n6.SERIAL_NUMBER.ordinal()] = 2;
        b[n6.FIRMWARE_VERSION.ordinal()] = 3;
        b[n6.SOFTWARE_REVISION.ordinal()] = 4;
        b[n6.DC.ordinal()] = 5;
        b[n6.FTC.ordinal()] = 6;
        b[n6.ASYNC.ordinal()] = 7;
        b[n6.FTD.ordinal()] = 8;
        b[n6.FTD_1.ordinal()] = 9;
        b[n6.AUTHENTICATION.ordinal()] = 10;
    }
    */
}
