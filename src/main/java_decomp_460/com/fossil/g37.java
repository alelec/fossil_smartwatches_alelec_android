package com.fossil;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g37<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public T f1255a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends FragmentManager.f {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Fragment f1256a;
        @DexIgnore
        public /* final */ /* synthetic */ FragmentManager b;

        @DexIgnore
        public a(Fragment fragment, FragmentManager fragmentManager) {
            this.f1256a = fragment;
            this.b = fragmentManager;
        }

        @DexIgnore
        @Override // androidx.fragment.app.FragmentManager.f
        public void n(FragmentManager fragmentManager, Fragment fragment) {
            if (Objects.equals(fragment, this.f1256a)) {
                g37.this.f1255a = null;
                this.b.i1(this);
            }
        }
    }

    @DexIgnore
    public g37(Fragment fragment, T t) {
        FragmentManager fragmentManager = fragment.getFragmentManager();
        if (fragmentManager != null) {
            fragmentManager.O0(new a(fragment, fragmentManager), false);
        }
        this.f1255a = t;
    }

    @DexIgnore
    public T a() {
        return this.f1255a;
    }
}
