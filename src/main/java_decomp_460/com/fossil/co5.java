package com.fossil;

import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class co5 implements Factory<bo5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<DianaPresetRepository> f634a;
    @DexIgnore
    public /* final */ Provider<on5> b;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> c;

    @DexIgnore
    public co5(Provider<DianaPresetRepository> provider, Provider<on5> provider2, Provider<WatchFaceRepository> provider3) {
        this.f634a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static co5 a(Provider<DianaPresetRepository> provider, Provider<on5> provider2, Provider<WatchFaceRepository> provider3) {
        return new co5(provider, provider2, provider3);
    }

    @DexIgnore
    public static bo5 c(DianaPresetRepository dianaPresetRepository, on5 on5, WatchFaceRepository watchFaceRepository) {
        return new bo5(dianaPresetRepository, on5, watchFaceRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public bo5 get() {
        return c(this.f634a.get(), this.b.get(), this.c.get());
    }
}
