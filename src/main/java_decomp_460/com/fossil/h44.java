package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h44 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Object[] f1427a; // = new Object[0];

    @DexIgnore
    public static <T> T[] a(T[] tArr, int i) {
        T[] tArr2 = (T[]) f(tArr, i);
        System.arraycopy(tArr, 0, tArr2, 0, Math.min(tArr.length, i));
        return tArr2;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static Object b(Object obj, int i) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException("at index " + i);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static Object[] c(Object... objArr) {
        d(objArr, objArr.length);
        return objArr;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static Object[] d(Object[] objArr, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            b(objArr[i2], i2);
        }
        return objArr;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static Object[] e(Iterable<?> iterable, Object[] objArr) {
        Iterator<?> it = iterable.iterator();
        int i = 0;
        while (it.hasNext()) {
            objArr[i] = it.next();
            i++;
        }
        return objArr;
    }

    @DexIgnore
    public static <T> T[] f(T[] tArr, int i) {
        return (T[]) k44.a(tArr, i);
    }

    @DexIgnore
    public static <T> T[] g(Collection<?> collection, T[] tArr) {
        int size = collection.size();
        if (tArr.length < size) {
            tArr = (T[]) f(tArr, size);
        }
        e(collection, tArr);
        if (tArr.length > size) {
            tArr[size] = null;
        }
        return tArr;
    }
}
