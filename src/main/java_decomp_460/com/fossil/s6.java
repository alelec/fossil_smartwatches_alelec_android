package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s6 {
    @DexIgnore
    public /* synthetic */ s6(kq7 kq7) {
    }

    @DexIgnore
    public final u6 a(UUID uuid) {
        return pq7.a(uuid, hd0.y.a()) ? u6.CLIENT_CHARACTERISTIC_CONFIGURATION : u6.UNKNOWN;
    }

    @DexIgnore
    public final byte[] b() {
        return v6.c;
    }

    @DexIgnore
    public final byte[] c() {
        return v6.b;
    }

    @DexIgnore
    public final byte[] d() {
        return v6.f3722a;
    }
}
