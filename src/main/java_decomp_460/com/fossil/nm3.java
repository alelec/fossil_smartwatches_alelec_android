package com.fossil;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nm3<V> extends FutureTask<V> implements Comparable<nm3<V>> {
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ /* synthetic */ im3 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nm3(im3 im3, Runnable runnable, boolean z, String str) {
        super(runnable, null);
        this.e = im3;
        rc2.k(str);
        long andIncrement = im3.l.getAndIncrement();
        this.b = andIncrement;
        this.d = str;
        this.c = false;
        if (andIncrement == Long.MAX_VALUE) {
            im3.d().F().a("Tasks index overflow");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nm3(im3 im3, Callable<V> callable, boolean z, String str) {
        super(callable);
        this.e = im3;
        rc2.k(str);
        long andIncrement = im3.l.getAndIncrement();
        this.b = andIncrement;
        this.d = str;
        this.c = z;
        if (andIncrement == Long.MAX_VALUE) {
            im3.d().F().a("Tasks index overflow");
        }
    }

    @DexIgnore
    @Override // java.lang.Comparable
    public final /* synthetic */ int compareTo(Object obj) {
        nm3 nm3 = (nm3) obj;
        boolean z = this.c;
        if (z != nm3.c) {
            return z ? -1 : 1;
        }
        long j = this.b;
        long j2 = nm3.b;
        if (j < j2) {
            return -1;
        }
        if (j > j2) {
            return 1;
        }
        this.e.d().G().b("Two tasks share the same index. index", Long.valueOf(this.b));
        return 0;
    }

    @DexIgnore
    public final void setException(Throwable th) {
        this.e.d().F().b(this.d, th);
        super.setException(th);
    }
}
