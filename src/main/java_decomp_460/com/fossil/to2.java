package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class to2 implements qh2 {
    @DexIgnore
    @Override // com.fossil.qh2
    public final t62<Status> a(r62 r62, DataSet dataSet) {
        rc2.l(dataSet, "Must set the data set");
        rc2.o(!dataSet.k().isEmpty(), "Cannot use an empty data set");
        rc2.l(dataSet.A().p0(), "Must set the app package name for the data source");
        return r62.i(new so2(this, r62, dataSet, false));
    }
}
