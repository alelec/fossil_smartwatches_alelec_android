package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yj extends qq7 implements vp7<byte[], n6, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ kk b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public yj(kk kkVar) {
        super(2);
        this.b = kkVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public tl7 invoke(byte[] bArr, n6 n6Var) {
        byte[] bArr2 = bArr;
        n6 n6Var2 = n6Var;
        vp7<? super byte[], ? super n6, tl7> vp7 = this.b.D;
        if (vp7 != null) {
            vp7.invoke(bArr2, n6Var2);
        }
        return tl7.f3441a;
    }
}
