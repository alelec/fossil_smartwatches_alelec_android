package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.os.UserManager;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hv2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static UserManager f1538a;
    @DexIgnore
    public static volatile boolean b; // = (!a());

    @DexIgnore
    public static boolean a() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @DexIgnore
    public static boolean b(Context context) {
        return !a() || d(context);
    }

    @DexIgnore
    @TargetApi(24)
    public static boolean c(Context context) {
        boolean z = true;
        int i = 1;
        while (true) {
            if (i > 2) {
                z = false;
                break;
            }
            if (f1538a == null) {
                f1538a = (UserManager) context.getSystemService(UserManager.class);
            }
            UserManager userManager = f1538a;
            if (userManager == null) {
                break;
            }
            try {
                z = !userManager.isUserUnlocked() ? !userManager.isUserRunning(Process.myUserHandle()) : true;
            } catch (NullPointerException e) {
                Log.w("DirectBootUtils", "Failed to check if user is unlocked.", e);
                f1538a = null;
                i++;
            }
        }
        if (z) {
            f1538a = null;
        }
        return z;
    }

    @DexIgnore
    @TargetApi(24)
    public static boolean d(Context context) {
        boolean z = true;
        if (!b) {
            synchronized (hv2.class) {
                try {
                    if (!b) {
                        z = c(context);
                        if (z) {
                            b = z;
                        }
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return z;
    }
}
