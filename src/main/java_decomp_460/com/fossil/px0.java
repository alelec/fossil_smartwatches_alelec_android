package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface px0 extends nx0 {
    @DexIgnore
    long executeInsert();

    @DexIgnore
    int executeUpdateDelete();
}
