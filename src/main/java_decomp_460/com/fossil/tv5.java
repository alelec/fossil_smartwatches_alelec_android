package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.n04;
import com.fossil.x37;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.HomeActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tv5 extends pv5 implements qv6 {
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public pv6 g;
    @DexIgnore
    public ar4 h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public /* final */ String k; // = qn5.l.a().d("disabledButton");
    @DexIgnore
    public /* final */ String l; // = qn5.l.a().d("primaryColor");
    @DexIgnore
    public g37<h65> m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final tv5 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            tv5 tv5 = new tv5();
            tv5.setArguments(bundle);
            return tv5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements n04.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tv5 f3476a;

        @DexIgnore
        public b(tv5 tv5) {
            this.f3476a = tv5;
        }

        @DexIgnore
        @Override // com.fossil.n04.b
        public final void a(TabLayout.g gVar, int i) {
            pq7.c(gVar, "tab");
            if (!TextUtils.isEmpty(this.f3476a.k) && !TextUtils.isEmpty(this.f3476a.l)) {
                int parseColor = Color.parseColor(this.f3476a.k);
                int parseColor2 = Color.parseColor(this.f3476a.l);
                gVar.o(2131230966);
                if (i == this.f3476a.j) {
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable e2 = gVar.e();
                if (e2 != null) {
                    e2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ h65 f3477a;
        @DexIgnore
        public /* final */ /* synthetic */ tv5 b;

        @DexIgnore
        public c(h65 h65, tv5 tv5) {
            this.f3477a = h65;
            this.b = tv5;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            Drawable e;
            Drawable e2;
            super.b(i, f, i2);
            if (!TextUtils.isEmpty(this.b.l)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ExploreWatchFragment", "set icon color " + this.b.l);
                int parseColor = Color.parseColor(this.b.l);
                TabLayout.g v = this.f3477a.s.v(i);
                if (!(v == null || (e2 = v.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.k) && this.b.j != i) {
                int parseColor2 = Color.parseColor(this.b.k);
                TabLayout.g v2 = this.f3477a.s.v(this.b.j);
                if (!(v2 == null || (e = v2.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.b.j = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tv5 b;

        @DexIgnore
        public d(tv5 tv5) {
            this.b = tv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            tv5.M6(this.b).n();
        }
    }

    @DexIgnore
    public static final /* synthetic */ pv6 M6(tv5 tv5) {
        pv6 pv6 = tv5.g;
        if (pv6 != null) {
            return pv6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "ExploreWatchFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    /* renamed from: P6 */
    public void M5(pv6 pv6) {
        pq7.c(pv6, "presenter");
        this.g = pv6;
    }

    @DexIgnore
    @Override // com.fossil.qv6
    public void f() {
        DashBar dashBar;
        g37<h65> g37 = this.m;
        if (g37 != null) {
            h65 a2 = g37.a();
            if (a2 != null && (dashBar = a2.t) != null) {
                x37.a aVar = x37.f4036a;
                pq7.b(dashBar, "this");
                aVar.a(dashBar, this.i, 500);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qv6
    public void j3() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.B;
            pq7.b(activity, "it");
            HomeActivity.a.b(aVar, activity, null, 2, null);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        g37<h65> g37 = new g37<>(this, (h65) aq0.f(layoutInflater, 2131558554, viewGroup, false, A6()));
        this.m = g37;
        if (g37 != null) {
            h65 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        ar4 ar4 = this.h;
        if (ar4 != null) {
            ar4.i();
            pv6 pv6 = this.g;
            if (pv6 != null) {
                pv6.m();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        pv6 pv6 = this.g;
        if (pv6 != null) {
            pv6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        this.h = new ar4();
        g37<h65> g37 = this.m;
        if (g37 != null) {
            h65 a2 = g37.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.u;
                pq7.b(viewPager2, "binding.vpExplore");
                ar4 ar4 = this.h;
                if (ar4 != null) {
                    viewPager2.setAdapter(ar4);
                    if (a2.u.getChildAt(0) != null) {
                        View childAt = a2.u.getChildAt(0);
                        if (childAt != null) {
                            ((RecyclerView) childAt).setOverScrollMode(2);
                        } else {
                            throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                        }
                    }
                    TabLayout tabLayout = a2.s;
                    ViewPager2 viewPager22 = a2.u;
                    if (viewPager22 != null) {
                        new n04(tabLayout, viewPager22, new b(this)).a();
                        a2.u.g(new c(a2, this));
                        a2.q.setOnClickListener(new d(this));
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.n("mAdapter");
                    throw null;
                }
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                this.i = z;
                pv6 pv6 = this.g;
                if (pv6 != null) {
                    pv6.o(z);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.qv6
    public void z5(List<? extends Explore> list) {
        pq7.c(list, "data");
        ar4 ar4 = this.h;
        if (ar4 != null) {
            ar4.l(list);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }
}
