package com.fossil;

import android.os.DeadObjectException;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface mr2<T extends IInterface> {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    T getService() throws DeadObjectException;
}
