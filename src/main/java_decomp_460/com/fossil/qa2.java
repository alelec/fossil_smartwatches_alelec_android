package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qa2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f2948a;
    @DexIgnore
    public /* final */ z52 b;

    @DexIgnore
    public qa2(z52 z52, int i) {
        rc2.k(z52);
        this.b = z52;
        this.f2948a = i;
    }

    @DexIgnore
    public final z52 a() {
        return this.b;
    }

    @DexIgnore
    public final int b() {
        return this.f2948a;
    }
}
