package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class la0 implements Parcelable.Creator<ma0> {
    @DexIgnore
    public /* synthetic */ la0(kq7 kq7) {
    }

    @DexIgnore
    public ma0 a(Parcel parcel) {
        return new ma0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public ma0 createFromParcel(Parcel parcel) {
        return new ma0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public ma0[] newArray(int i) {
        return new ma0[i];
    }
}
