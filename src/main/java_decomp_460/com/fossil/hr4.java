package com.fossil;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hr4 extends tq0 {
    @DexIgnore
    public /* final */ ArrayList<Fragment> g; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<String> h; // = new ArrayList<>();

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hr4(FragmentManager fragmentManager) {
        super(fragmentManager);
        pq7.c(fragmentManager, "fm");
    }

    @DexIgnore
    @Override // com.fossil.e01
    public int e() {
        return this.g.size();
    }

    @DexIgnore
    @Override // com.fossil.e01
    public CharSequence g(int i) {
        String str = this.h.get(i);
        pq7.b(str, "mFragmentTitleList.get(position)");
        return str;
    }

    @DexIgnore
    @Override // com.fossil.tq0
    public Fragment u(int i) {
        Fragment fragment = this.g.get(i);
        pq7.b(fragment, "mFragmentList.get(position)");
        return fragment;
    }

    @DexIgnore
    public final void x(Fragment fragment, String str) {
        pq7.c(fragment, "fragment");
        pq7.c(str, "title");
        this.g.add(fragment);
        this.h.add(str);
    }
}
