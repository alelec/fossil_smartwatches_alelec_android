package com.fossil;

import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ik7<T> implements Provider<T> {
    @DexIgnore
    public static /* final */ Object c; // = new Object();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public volatile Provider<T> f1641a;
    @DexIgnore
    public volatile Object b; // = c;

    @DexIgnore
    public ik7(Provider<T> provider) {
        this.f1641a = provider;
    }

    @DexIgnore
    public static <P extends Provider<T>, T> Provider<T> a(P p) {
        lk7.b(p);
        return p instanceof ik7 ? p : new ik7(p);
    }

    @DexIgnore
    public static Object b(Object obj, Object obj2) {
        if (!(obj != c) || obj == obj2) {
            return obj2;
        }
        throw new IllegalStateException("Scoped provider was invoked recursively returning different results: " + obj + " & " + obj2 + ". This is likely due to a circular dependency.");
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public T get() {
        T t = (T) this.b;
        if (t == c) {
            synchronized (this) {
                t = this.b;
                if (t == c) {
                    t = this.f1641a.get();
                    b(this.b, t);
                    this.b = t;
                    this.f1641a = null;
                }
            }
        }
        return t;
    }
}
