package com.fossil;

import android.view.View;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class f65 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ RTLImageView B;
    @DexIgnore
    public /* final */ RTLImageView C;
    @DexIgnore
    public /* final */ ConstraintLayout D;
    @DexIgnore
    public /* final */ ScrollView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleEditText s;
    @DexIgnore
    public /* final */ FlexibleEditText t;
    @DexIgnore
    public /* final */ FlexibleEditText u;
    @DexIgnore
    public /* final */ FlexibleEditText v;
    @DexIgnore
    public /* final */ FlexibleButton w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public f65(Object obj, View view, int i, FlexibleButton flexibleButton, ConstraintLayout constraintLayout, FlexibleEditText flexibleEditText, FlexibleEditText flexibleEditText2, FlexibleEditText flexibleEditText3, FlexibleEditText flexibleEditText4, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, RTLImageView rTLImageView, RTLImageView rTLImageView2, ConstraintLayout constraintLayout2, ScrollView scrollView, FlexibleTextView flexibleTextView5) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = constraintLayout;
        this.s = flexibleEditText;
        this.t = flexibleEditText2;
        this.u = flexibleEditText3;
        this.v = flexibleEditText4;
        this.w = flexibleButton2;
        this.x = flexibleTextView;
        this.y = flexibleTextView2;
        this.z = flexibleTextView3;
        this.A = flexibleTextView4;
        this.B = rTLImageView;
        this.C = rTLImageView2;
        this.D = constraintLayout2;
        this.E = scrollView;
        this.F = flexibleTextView5;
    }
}
