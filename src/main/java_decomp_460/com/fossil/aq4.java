package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aq4 implements Factory<wn5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f305a;

    @DexIgnore
    public aq4(uo4 uo4) {
        this.f305a = uo4;
    }

    @DexIgnore
    public static aq4 a(uo4 uo4) {
        return new aq4(uo4);
    }

    @DexIgnore
    public static wn5 c(uo4 uo4) {
        wn5 H = uo4.H();
        lk7.c(H, "Cannot return null from a non-@Nullable @Provides method");
        return H;
    }

    @DexIgnore
    /* renamed from: b */
    public wn5 get() {
        return c(this.f305a);
    }
}
