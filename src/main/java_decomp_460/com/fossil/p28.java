package com.fossil;

import com.fossil.o28;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.Socket;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p28 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ x08 f2770a;
    @DexIgnore
    public o28.a b;
    @DexIgnore
    public x18 c;
    @DexIgnore
    public /* final */ f18 d;
    @DexIgnore
    public /* final */ a18 e;
    @DexIgnore
    public /* final */ m18 f;
    @DexIgnore
    public /* final */ Object g;
    @DexIgnore
    public /* final */ o28 h;
    @DexIgnore
    public int i;
    @DexIgnore
    public l28 j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public s28 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends WeakReference<p28> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Object f2771a;

        @DexIgnore
        public a(p28 p28, Object obj) {
            super(p28);
            this.f2771a = obj;
        }
    }

    @DexIgnore
    public p28(f18 f18, x08 x08, a18 a18, m18 m18, Object obj) {
        this.d = f18;
        this.f2770a = x08;
        this.e = a18;
        this.f = m18;
        this.h = new o28(x08, p(), a18, m18);
        this.g = obj;
    }

    @DexIgnore
    public void a(l28 l28, boolean z) {
        if (this.j == null) {
            this.j = l28;
            this.k = z;
            l28.n.add(new a(this, this.g));
            return;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public void b() {
        s28 s28;
        l28 l28;
        synchronized (this.d) {
            this.m = true;
            s28 = this.n;
            l28 = this.j;
        }
        if (s28 != null) {
            s28.cancel();
        } else if (l28 != null) {
            l28.d();
        }
    }

    @DexIgnore
    public s28 c() {
        s28 s28;
        synchronized (this.d) {
            s28 = this.n;
        }
        return s28;
    }

    @DexIgnore
    public l28 d() {
        l28 l28;
        synchronized (this) {
            l28 = this.j;
        }
        return l28;
    }

    @DexIgnore
    public final Socket e(boolean z, boolean z2, boolean z3) {
        Socket socket;
        if (z3) {
            this.n = null;
        }
        if (z2) {
            this.l = true;
        }
        l28 l28 = this.j;
        if (l28 == null) {
            return null;
        }
        if (z) {
            l28.k = true;
        }
        if (this.n != null) {
            return null;
        }
        if (!this.l && !this.j.k) {
            return null;
        }
        l(this.j);
        if (this.j.n.isEmpty()) {
            this.j.o = System.nanoTime();
            if (z18.f4406a.e(this.d, this.j)) {
                socket = this.j.r();
                this.j = null;
                return socket;
            }
        }
        socket = null;
        this.j = null;
        return socket;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00aa  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.l28 f(int r13, int r14, int r15, int r16, boolean r17) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 330
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.p28.f(int, int, int, int, boolean):com.fossil.l28");
    }

    @DexIgnore
    public final l28 g(int i2, int i3, int i4, int i5, boolean z, boolean z2) throws IOException {
        l28 f2;
        while (true) {
            f2 = f(i2, i3, i4, i5, z);
            synchronized (this.d) {
                if (f2.l != 0 || f2.o()) {
                    if (f2.n(z2)) {
                        break;
                    }
                    j();
                } else {
                    break;
                }
            }
        }
        return f2;
    }

    @DexIgnore
    public boolean h() {
        o28.a aVar;
        return this.c != null || ((aVar = this.b) != null && aVar.b()) || this.h.c();
    }

    @DexIgnore
    public s28 i(OkHttpClient okHttpClient, Interceptor.Chain chain, boolean z) {
        try {
            s28 p = g(chain.f(), chain.a(), chain.b(), okHttpClient.B(), okHttpClient.J(), z).p(okHttpClient, chain, this);
            synchronized (this.d) {
                this.n = p;
            }
            return p;
        } catch (IOException e2) {
            throw new n28(e2);
        }
    }

    @DexIgnore
    public void j() {
        l28 l28;
        Socket e2;
        synchronized (this.d) {
            l28 = this.j;
            e2 = e(true, false, false);
            if (this.j != null) {
                l28 = null;
            }
        }
        b28.h(e2);
        if (l28 != null) {
            this.f.h(this.e, l28);
        }
    }

    @DexIgnore
    public void k() {
        l28 l28;
        Socket e2;
        synchronized (this.d) {
            l28 = this.j;
            e2 = e(false, true, false);
            if (this.j != null) {
                l28 = null;
            }
        }
        b28.h(e2);
        if (l28 != null) {
            z18.f4406a.k(this.e, null);
            this.f.h(this.e, l28);
            this.f.a(this.e);
        }
    }

    @DexIgnore
    public final void l(l28 l28) {
        int size = l28.n.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (l28.n.get(i2).get() == this) {
                l28.n.remove(i2);
                return;
            }
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public Socket m(l28 l28) {
        if (this.n == null && this.j.n.size() == 1) {
            Socket e2 = e(true, false, false);
            this.j = l28;
            l28.n.add(this.j.n.get(0));
            return e2;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public final Socket n() {
        l28 l28 = this.j;
        if (l28 == null || !l28.k) {
            return null;
        }
        return e(false, false, true);
    }

    @DexIgnore
    public x18 o() {
        return this.c;
    }

    @DexIgnore
    public final m28 p() {
        return z18.f4406a.j(this.d);
    }

    @DexIgnore
    public void q(IOException iOException) {
        Socket e2;
        boolean z = false;
        l28 l28 = null;
        synchronized (this.d) {
            if (iOException instanceof p38) {
                d38 d38 = ((p38) iOException).errorCode;
                if (d38 == d38.REFUSED_STREAM) {
                    int i2 = this.i + 1;
                    this.i = i2;
                    if (i2 > 1) {
                        this.c = null;
                    }
                    l28 l282 = this.j;
                    e2 = e(z, false, true);
                    if (this.j == null && this.k) {
                        l28 = l282;
                    }
                } else {
                    if (d38 != d38.CANCEL) {
                        this.c = null;
                    }
                    l28 l2822 = this.j;
                    e2 = e(z, false, true);
                    l28 = l2822;
                }
            } else {
                if (this.j != null && (!this.j.o() || (iOException instanceof c38))) {
                    if (this.j.l == 0) {
                        if (!(this.c == null || iOException == null)) {
                            this.h.a(this.c, iOException);
                        }
                        this.c = null;
                    }
                }
                l28 l28222 = this.j;
                e2 = e(z, false, true);
                l28 = l28222;
            }
            z = true;
            l28 l282222 = this.j;
            e2 = e(z, false, true);
            l28 = l282222;
        }
        b28.h(e2);
        if (l28 != null) {
            this.f.h(this.e, l28);
        }
    }

    @DexIgnore
    public void r(boolean z, s28 s28, long j2, IOException iOException) {
        l28 l28;
        Socket e2;
        boolean z2;
        this.f.p(this.e, j2);
        synchronized (this.d) {
            if (s28 != null) {
                if (s28 == this.n) {
                    if (!z) {
                        this.j.l++;
                    }
                    l28 = this.j;
                    e2 = e(z, false, true);
                    if (this.j != null) {
                        l28 = null;
                    }
                    z2 = this.l;
                }
            }
            throw new IllegalStateException("expected " + this.n + " but was " + s28);
        }
        b28.h(e2);
        if (l28 != null) {
            this.f.h(this.e, l28);
        }
        if (iOException != null) {
            this.f.b(this.e, z18.f4406a.k(this.e, iOException));
        } else if (z2) {
            z18.f4406a.k(this.e, null);
            this.f.a(this.e);
        }
    }

    @DexIgnore
    public String toString() {
        l28 d2 = d();
        return d2 != null ? d2.toString() : this.f2770a.toString();
    }
}
