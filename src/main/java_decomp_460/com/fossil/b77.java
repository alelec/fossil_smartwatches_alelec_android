package com.fossil;

import android.app.Activity;
import com.fossil.a87;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b77 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ b77 f401a; // = new b77();

    @DexIgnore
    public final void a(String str) {
        pq7.c(str, "watchFaceId");
        HashMap hashMap = new HashMap();
        hashMap.put("wf_id", str);
        hashMap.put("event_timestamp", Long.valueOf(System.currentTimeMillis()));
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceAnalytics", "logAppliedWatchFace, value = " + hashMap);
        ck5.f.g().l("wf_apply", hashMap);
    }

    @DexIgnore
    public final void b(int i, Activity activity) {
        String str = "wf_customize_background";
        if (i == 0) {
            str = "wf_customize_text";
        } else if (i == 1) {
            str = "wf_customize_sticker";
        } else if (i != 2) {
            if (i == 3) {
                str = "wf_customize_photo";
            } else if (i == 4) {
                str = "wf_customize_complication";
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceAnalytics", "logCustomizationScreen, screenName = " + str);
        ck5.f.g().m(str, activity);
    }

    @DexIgnore
    public final void c(String str, String str2, String str3) {
        pq7.c(str, "eventName");
        pq7.c(str2, "watchFaceId");
        pq7.c(str3, "watchFaceType");
        HashMap hashMap = new HashMap();
        hashMap.put("wf_id", str2);
        hashMap.put("event_timestamp", Long.valueOf(System.currentTimeMillis()));
        hashMap.put("wf_type", str3);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceAnalytics", "logGalleryEvent, eventName = " + str + ", value = " + hashMap);
        ck5.f.g().l(str, hashMap);
    }

    @DexIgnore
    public final void d(String str) {
        pq7.c(str, "watchFaceId");
        HashMap hashMap = new HashMap();
        hashMap.put("source_wf_id", str);
        hashMap.put("start_timestamp", Long.valueOf(System.currentTimeMillis()));
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceAnalytics", "logNewPreset, value = " + hashMap);
        ck5.f.g().l("wf_myfaces_create_preset", hashMap);
    }

    @DexIgnore
    public final void e(String str) {
        pq7.c(str, "watchFaceId");
        HashMap hashMap = new HashMap();
        hashMap.put("source_wf_id", str);
        hashMap.put("start_timestamp", Long.valueOf(System.currentTimeMillis()));
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceAnalytics", "logNewWatchFace, value = " + hashMap);
        ck5.f.g().l("wf_myfaces_create_face", hashMap);
    }

    @DexIgnore
    public final void f(String str) {
        pq7.c(str, "watchFaceId");
        HashMap hashMap = new HashMap();
        hashMap.put("event_timestamp", Long.valueOf(System.currentTimeMillis()));
        hashMap.put("wf_id", str);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceAnalytics", "logOpenEvent, value = " + hashMap);
        ck5.f.g().l("wf_sharedwf_open_event", hashMap);
    }

    @DexIgnore
    public final void g(ob7 ob7, a87.a aVar) {
        pq7.c(ob7, "theme");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        for (T t : ob7.a()) {
            if (t instanceof mb7) {
                arrayList.add(t.e());
            } else if (t instanceof pb7) {
                arrayList2.add(t.d());
            } else if (t instanceof ib7) {
                arrayList3.add(v87.b(t.getType()));
            }
        }
        sl5 b = ck5.f.b("wf_custom_stats");
        b.a("comp_text", jj5.a(arrayList));
        b.a("comp_sticker", jj5.a(arrayList2));
        b.a("comp_complication", jj5.a(arrayList3));
        if (aVar != null) {
            int i = a77.f209a[aVar.b().ordinal()];
            if (i == 1) {
                b.a("comp_cus_photo", aVar.a());
                b.a("comp_background", "");
            } else if (i == 2) {
                b.a("comp_cus_photo", "");
                b.a("comp_background", aVar.a());
            }
        }
        b.b();
    }

    @DexIgnore
    public final void h(String str, long j, long j2) {
        pq7.c(str, Constants.SESSION);
        HashMap hashMap = new HashMap();
        hashMap.put("start_timestamp", Long.valueOf(j));
        hashMap.put("end_timestamp", Long.valueOf(j2));
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceAnalytics", "logEndSession, value = " + hashMap);
        ck5.f.g().l(str, hashMap);
    }
}
