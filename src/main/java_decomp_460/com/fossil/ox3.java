package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import com.google.android.material.button.MaterialButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ox3 {
    @DexIgnore
    public static /* final */ boolean s; // = (Build.VERSION.SDK_INT >= 21);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MaterialButton f2736a;
    @DexIgnore
    public g04 b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public PorterDuff.Mode i;
    @DexIgnore
    public ColorStateList j;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public ColorStateList l;
    @DexIgnore
    public Drawable m;
    @DexIgnore
    public boolean n; // = false;
    @DexIgnore
    public boolean o; // = false;
    @DexIgnore
    public boolean p; // = false;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public LayerDrawable r;

    @DexIgnore
    public ox3(MaterialButton materialButton, g04 g04) {
        this.f2736a = materialButton;
        this.b = g04;
    }

    @DexIgnore
    public final void A(g04 g04) {
        if (d() != null) {
            d().setShapeAppearanceModel(g04);
        }
        if (l() != null) {
            l().setShapeAppearanceModel(g04);
        }
        if (c() != null) {
            c().setShapeAppearanceModel(g04);
        }
    }

    @DexIgnore
    public void B(int i2, int i3) {
        Drawable drawable = this.m;
        if (drawable != null) {
            drawable.setBounds(this.c, this.e, i3 - this.d, i2 - this.f);
        }
    }

    @DexIgnore
    public final void C() {
        c04 d2 = d();
        c04 l2 = l();
        if (d2 != null) {
            d2.e0((float) this.h, this.k);
            if (l2 != null) {
                l2.d0((float) this.h, this.n ? vx3.c(this.f2736a, jw3.colorSurface) : 0);
            }
        }
    }

    @DexIgnore
    public final InsetDrawable D(Drawable drawable) {
        return new InsetDrawable(drawable, this.c, this.e, this.d, this.f);
    }

    @DexIgnore
    public final Drawable a() {
        c04 c04 = new c04(this.b);
        c04.M(this.f2736a.getContext());
        am0.o(c04, this.j);
        PorterDuff.Mode mode = this.i;
        if (mode != null) {
            am0.p(c04, mode);
        }
        c04.e0((float) this.h, this.k);
        c04 c042 = new c04(this.b);
        c042.setTint(0);
        c042.d0((float) this.h, this.n ? vx3.c(this.f2736a, jw3.colorSurface) : 0);
        if (s) {
            c04 c043 = new c04(this.b);
            this.m = c043;
            am0.n(c043, -1);
            RippleDrawable rippleDrawable = new RippleDrawable(tz3.d(this.l), D(new LayerDrawable(new Drawable[]{c042, c04})), this.m);
            this.r = rippleDrawable;
            return rippleDrawable;
        }
        sz3 sz3 = new sz3(this.b);
        this.m = sz3;
        am0.o(sz3, tz3.d(this.l));
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{c042, c04, this.m});
        this.r = layerDrawable;
        return D(layerDrawable);
    }

    @DexIgnore
    public int b() {
        return this.g;
    }

    @DexIgnore
    public j04 c() {
        LayerDrawable layerDrawable = this.r;
        if (layerDrawable == null || layerDrawable.getNumberOfLayers() <= 1) {
            return null;
        }
        return this.r.getNumberOfLayers() > 2 ? (j04) this.r.getDrawable(2) : (j04) this.r.getDrawable(1);
    }

    @DexIgnore
    public c04 d() {
        return e(false);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    public final c04 e(boolean z) {
        LayerDrawable layerDrawable = this.r;
        if (layerDrawable == null || layerDrawable.getNumberOfLayers() <= 0) {
            return null;
        }
        return s ? (c04) ((LayerDrawable) ((InsetDrawable) this.r.getDrawable(0)).getDrawable()).getDrawable(!z) : (c04) this.r.getDrawable(!z ? 1 : 0);
    }

    @DexIgnore
    public ColorStateList f() {
        return this.l;
    }

    @DexIgnore
    public g04 g() {
        return this.b;
    }

    @DexIgnore
    public ColorStateList h() {
        return this.k;
    }

    @DexIgnore
    public int i() {
        return this.h;
    }

    @DexIgnore
    public ColorStateList j() {
        return this.j;
    }

    @DexIgnore
    public PorterDuff.Mode k() {
        return this.i;
    }

    @DexIgnore
    public final c04 l() {
        return e(true);
    }

    @DexIgnore
    public boolean m() {
        return this.o;
    }

    @DexIgnore
    public boolean n() {
        return this.q;
    }

    @DexIgnore
    public void o(TypedArray typedArray) {
        this.c = typedArray.getDimensionPixelOffset(tw3.MaterialButton_android_insetLeft, 0);
        this.d = typedArray.getDimensionPixelOffset(tw3.MaterialButton_android_insetRight, 0);
        this.e = typedArray.getDimensionPixelOffset(tw3.MaterialButton_android_insetTop, 0);
        this.f = typedArray.getDimensionPixelOffset(tw3.MaterialButton_android_insetBottom, 0);
        if (typedArray.hasValue(tw3.MaterialButton_cornerRadius)) {
            int dimensionPixelSize = typedArray.getDimensionPixelSize(tw3.MaterialButton_cornerRadius, -1);
            this.g = dimensionPixelSize;
            u(this.b.w((float) dimensionPixelSize));
            this.p = true;
        }
        this.h = typedArray.getDimensionPixelSize(tw3.MaterialButton_strokeWidth, 0);
        this.i = kz3.e(typedArray.getInt(tw3.MaterialButton_backgroundTintMode, -1), PorterDuff.Mode.SRC_IN);
        this.j = oz3.a(this.f2736a.getContext(), typedArray, tw3.MaterialButton_backgroundTint);
        this.k = oz3.a(this.f2736a.getContext(), typedArray, tw3.MaterialButton_strokeColor);
        this.l = oz3.a(this.f2736a.getContext(), typedArray, tw3.MaterialButton_rippleColor);
        this.q = typedArray.getBoolean(tw3.MaterialButton_android_checkable, false);
        int dimensionPixelSize2 = typedArray.getDimensionPixelSize(tw3.MaterialButton_elevation, 0);
        int E = mo0.E(this.f2736a);
        int paddingTop = this.f2736a.getPaddingTop();
        int D = mo0.D(this.f2736a);
        int paddingBottom = this.f2736a.getPaddingBottom();
        this.f2736a.setInternalBackground(a());
        c04 d2 = d();
        if (d2 != null) {
            d2.U((float) dimensionPixelSize2);
        }
        mo0.A0(this.f2736a, E + this.c, paddingTop + this.e, D + this.d, paddingBottom + this.f);
    }

    @DexIgnore
    public void p(int i2) {
        if (d() != null) {
            d().setTint(i2);
        }
    }

    @DexIgnore
    public void q() {
        this.o = true;
        this.f2736a.setSupportBackgroundTintList(this.j);
        this.f2736a.setSupportBackgroundTintMode(this.i);
    }

    @DexIgnore
    public void r(boolean z) {
        this.q = z;
    }

    @DexIgnore
    public void s(int i2) {
        if (!this.p || this.g != i2) {
            this.g = i2;
            this.p = true;
            u(this.b.w((float) i2));
        }
    }

    @DexIgnore
    public void t(ColorStateList colorStateList) {
        if (this.l != colorStateList) {
            this.l = colorStateList;
            if (s && (this.f2736a.getBackground() instanceof RippleDrawable)) {
                ((RippleDrawable) this.f2736a.getBackground()).setColor(tz3.d(colorStateList));
            } else if (!s && (this.f2736a.getBackground() instanceof sz3)) {
                ((sz3) this.f2736a.getBackground()).setTintList(tz3.d(colorStateList));
            }
        }
    }

    @DexIgnore
    public void u(g04 g04) {
        this.b = g04;
        A(g04);
    }

    @DexIgnore
    public void v(boolean z) {
        this.n = z;
        C();
    }

    @DexIgnore
    public void w(ColorStateList colorStateList) {
        if (this.k != colorStateList) {
            this.k = colorStateList;
            C();
        }
    }

    @DexIgnore
    public void x(int i2) {
        if (this.h != i2) {
            this.h = i2;
            C();
        }
    }

    @DexIgnore
    public void y(ColorStateList colorStateList) {
        if (this.j != colorStateList) {
            this.j = colorStateList;
            if (d() != null) {
                am0.o(d(), this.j);
            }
        }
    }

    @DexIgnore
    public void z(PorterDuff.Mode mode) {
        if (this.i != mode) {
            this.i = mode;
            if (d() != null && this.i != null) {
                am0.p(d(), this.i);
            }
        }
    }
}
