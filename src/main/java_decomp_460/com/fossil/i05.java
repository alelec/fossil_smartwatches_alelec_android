package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.diana.preset.Data;
import com.portfolio.platform.data.model.diana.preset.MetaData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i05 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<Data> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<Data> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends TypeToken<MetaData> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends TypeToken<MetaData> {
    }

    @DexIgnore
    public final String a(Data data) {
        pq7.c(data, "data");
        String u = new Gson().u(data, new a().getType());
        pq7.b(u, "Gson().toJson(data, type)");
        return u;
    }

    @DexIgnore
    public final Data b(String str) {
        pq7.c(str, "json");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return (Data) new Gson().l(str, new b().getType());
    }

    @DexIgnore
    public final MetaData c(String str) {
        pq7.c(str, "json");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return (MetaData) new Gson().l(str, new c().getType());
    }

    @DexIgnore
    public final String d(MetaData metaData) {
        pq7.c(metaData, "metaData");
        String u = new Gson().u(metaData, new d().getType());
        pq7.b(u, "Gson().toJson(metaData, type)");
        return u;
    }
}
