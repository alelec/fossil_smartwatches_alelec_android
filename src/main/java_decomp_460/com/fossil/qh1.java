package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qh1 implements th1<Bitmap, BitmapDrawable> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Resources f2980a;

    @DexIgnore
    public qh1(Resources resources) {
        ik1.d(resources);
        this.f2980a = resources;
    }

    @DexIgnore
    @Override // com.fossil.th1
    public id1<BitmapDrawable> a(id1<Bitmap> id1, ob1 ob1) {
        return og1.f(this.f2980a, id1);
    }
}
