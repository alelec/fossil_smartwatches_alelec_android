package com.fossil;

import android.os.Bundle;
import android.util.Pair;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.fl5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.service.workout.WorkoutTetherScreenShotManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nm6 extends im6 implements fl5.a {
    @DexIgnore
    public Date e;
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public MutableLiveData<cl7<Date, Date>> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ WorkoutTetherScreenShotManager h;
    @DexIgnore
    public List<DailyHeartRateSummary> i;
    @DexIgnore
    public List<HeartRateSample> j;
    @DexIgnore
    public DailyHeartRateSummary k;
    @DexIgnore
    public List<HeartRateSample> l;
    @DexIgnore
    public ai5 m;
    @DexIgnore
    public LiveData<h47<List<DailyHeartRateSummary>>> n;
    @DexIgnore
    public LiveData<h47<List<HeartRateSample>>> o;
    @DexIgnore
    public Listing<WorkoutSession> p;
    @DexIgnore
    public /* final */ jm6 q;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository r;
    @DexIgnore
    public /* final */ HeartRateSampleRepository s;
    @DexIgnore
    public /* final */ UserRepository t;
    @DexIgnore
    public /* final */ WorkoutSessionRepository u;
    @DexIgnore
    public /* final */ no4 v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<HeartRateSample, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Date date) {
            super(1);
            this.$date = date;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ Boolean invoke(HeartRateSample heartRateSample) {
            return Boolean.valueOf(invoke(heartRateSample));
        }

        @DexIgnore
        public final boolean invoke(HeartRateSample heartRateSample) {
            pq7.c(heartRateSample, "it");
            return lk5.m0(heartRateSample.getDate(), this.$date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$observeWorkoutSessionData$1", f = "HeartRateDetailPresenter.kt", l = {93}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ nm6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements ls0<cu0<WorkoutSession>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ b f2543a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.nm6$b$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$observeWorkoutSessionData$1$1$1", f = "HeartRateDetailPresenter.kt", l = {104}, m = "invokeSuspend")
            /* renamed from: com.fossil.nm6$b$a$a  reason: collision with other inner class name */
            public static final class C0173a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ cu0 $pageList;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.nm6$b$a$a$a")
                /* renamed from: com.fossil.nm6$b$a$a$a  reason: collision with other inner class name */
                public static final class C0174a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0173a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0174a(qn7 qn7, C0173a aVar) {
                        super(2, qn7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        C0174a aVar = new C0174a(qn7, this.this$0);
                        aVar.p$ = (iv7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                        return ((C0174a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    /* JADX WARNING: Removed duplicated region for block: B:25:0x009c  */
                    /* JADX WARNING: Removed duplicated region for block: B:41:0x0014 A[SYNTHETIC] */
                    @Override // com.fossil.zn7
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                        /*
                        // Method dump skipped, instructions count: 319
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nm6.b.a.C0173a.C0174a.invokeSuspend(java.lang.Object):java.lang.Object");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0173a(a aVar, cu0 cu0, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$pageList = cu0;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0173a aVar = new C0173a(this.this$0, this.$pageList, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0173a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        FragmentActivity activity = ((km6) this.this$0.f2543a.this$0.q).getActivity();
                        if (activity != null) {
                            WorkoutTetherScreenShotManager workoutTetherScreenShotManager = this.this$0.f2543a.this$0.h;
                            pq7.b(activity, "it");
                            workoutTetherScreenShotManager.w(activity);
                            dv7 a2 = bw7.a();
                            C0174a aVar = new C0174a(null, this);
                            this.L$0 = iv7;
                            this.L$1 = activity;
                            this.label = 1;
                            if (eu7.g(a2, aVar, this) == d) {
                                return d;
                            }
                        }
                    } else if (i == 1) {
                        FragmentActivity fragmentActivity = (FragmentActivity) this.L$1;
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    jm6 jm6 = this.this$0.f2543a.this$0.q;
                    ai5 ai5 = this.this$0.f2543a.this$0.m;
                    cu0<WorkoutSession> cu0 = this.$pageList;
                    pq7.b(cu0, "pageList");
                    jm6.s(true, ai5, cu0);
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            public a(b bVar) {
                this.f2543a = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(cu0<WorkoutSession> cu0) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HeartRateDetailPresenter", "getWorkoutSessionsPaging observed size = " + cu0.size());
                if (nk5.o.y(PortfolioApp.h0.c().J())) {
                    pq7.b(cu0, "pageList");
                    if (pm7.j0(cu0).isEmpty()) {
                        this.f2543a.this$0.q.s(false, this.f2543a.this$0.m, cu0);
                        return;
                    }
                }
                xw7 unused = gu7.d(this.f2543a.this$0.k(), null, null, new C0173a(this, cu0, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(nm6 nm6, Date date, qn7 qn7) {
            super(2, qn7);
            this.this$0 = nm6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$date, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object workoutSessionsPaging;
            nm6 nm6;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                nm6 nm62 = this.this$0;
                WorkoutSessionRepository workoutSessionRepository = nm62.u;
                Date date = this.$date;
                WorkoutSessionRepository workoutSessionRepository2 = this.this$0.u;
                no4 no4 = this.this$0.v;
                nm6 nm63 = this.this$0;
                this.L$0 = iv7;
                this.L$1 = nm62;
                this.label = 1;
                workoutSessionsPaging = workoutSessionRepository.getWorkoutSessionsPaging(date, workoutSessionRepository2, no4, nm63, this);
                if (workoutSessionsPaging == d) {
                    return d;
                }
                nm6 = nm62;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                nm6 = (nm6) this.L$1;
                workoutSessionsPaging = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            nm6.p = (Listing) workoutSessionsPaging;
            Listing listing = this.this$0.p;
            if (listing != null) {
                LiveData pagedList = listing.getPagedList();
                jm6 jm6 = this.this$0.q;
                if (jm6 != null) {
                    pagedList.h((km6) jm6, new a(this));
                    return tl7.f3441a;
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ nm6 f2544a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$sampleTransformations$1$1", f = "HeartRateDetailPresenter.kt", l = {72, 72}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<HeartRateSample>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Date date, Date date2, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$first, this.$second, qn7);
                aVar.p$ = (hs0) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<HeartRateSample>>> hs0, qn7<? super tl7> qn7) {
                return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r8)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r8)
                    com.fossil.hs0 r0 = r7.p$
                    com.fossil.nm6$c r1 = r7.this$0
                    com.fossil.nm6 r1 = r1.f2544a
                    com.portfolio.platform.data.source.HeartRateSampleRepository r1 = com.fossil.nm6.B(r1)
                    java.util.Date r2 = r7.$first
                    java.util.Date r3 = r7.$second
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getHeartRateSamples(r2, r3, r5, r7)
                    if (r2 != r4) goto L_0x005b
                    r0 = r4
                    goto L_0x0017
                L_0x005b:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.nm6.c.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public c(nm6 nm6) {
            this.f2544a = nm6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<HeartRateSample>>> apply(cl7<? extends Date, ? extends Date> cl7) {
            return or0.c(null, 0, new a(this, (Date) cl7.component1(), (Date) cl7.component2(), null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1", f = "HeartRateDetailPresenter.kt", l = {176}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ nm6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1$1", f = "HeartRateDetailPresenter.kt", l = {176}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Date>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Date> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    PortfolioApp c = PortfolioApp.h0.c();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object n0 = c.n0(this);
                    return n0 == d ? d : n0;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(nm6 nm6, Date date, qn7 qn7) {
            super(2, qn7);
            this.this$0 = nm6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$date, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            nm6 nm6;
            Pair<Date, Date> L;
            cl7 cl7;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (this.this$0.e == null) {
                    nm6 nm62 = this.this$0;
                    dv7 h = nm62.h();
                    a aVar = new a(null);
                    this.L$0 = iv7;
                    this.L$1 = nm62;
                    this.label = 1;
                    g = eu7.g(h, aVar, this);
                    if (g == d) {
                        return d;
                    }
                    nm6 = nm62;
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HeartRateDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.e);
                this.this$0.f = this.$date;
                boolean k0 = lk5.k0(this.this$0.e, this.$date);
                boolean k02 = lk5.k0(new Date(), this.$date);
                Boolean p0 = lk5.p0(this.$date);
                jm6 jm6 = this.this$0.q;
                Date date = this.$date;
                pq7.b(p0, "isToday");
                jm6.j(date, k0, p0.booleanValue(), !k02);
                L = lk5.L(this.$date, this.this$0.e);
                pq7.b(L, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
                cl7 = (cl7) this.this$0.g.e();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("HeartRateDetailPresenter", "setDate - rangeDateValue=" + cl7 + ", newRange=" + new cl7(L.first, L.second));
                if (cl7 != null || !lk5.m0((Date) cl7.getFirst(), (Date) L.first) || !lk5.m0((Date) cl7.getSecond(), (Date) L.second)) {
                    this.this$0.g.l(new cl7(L.first, L.second));
                } else {
                    this.this$0.j0();
                    this.this$0.k0();
                    nm6 nm63 = this.this$0;
                    nm63.h0(nm63.f);
                }
                return tl7.f3441a;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                nm6 = (nm6) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            nm6.e = (Date) g;
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("HeartRateDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.e);
            this.this$0.f = this.$date;
            boolean k03 = lk5.k0(this.this$0.e, this.$date);
            boolean k022 = lk5.k0(new Date(), this.$date);
            Boolean p02 = lk5.p0(this.$date);
            jm6 jm62 = this.this$0.q;
            Date date2 = this.$date;
            pq7.b(p02, "isToday");
            jm62.j(date2, k03, p02.booleanValue(), !k022);
            L = lk5.L(this.$date, this.this$0.e);
            pq7.b(L, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            cl7 = (cl7) this.this$0.g.e();
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            local22.d("HeartRateDetailPresenter", "setDate - rangeDateValue=" + cl7 + ", newRange=" + new cl7(L.first, L.second));
            if (cl7 != null) {
            }
            this.this$0.g.l(new cl7(L.first, L.second));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1", f = "HeartRateDetailPresenter.kt", l = {225}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ nm6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1$summary$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super DailyHeartRateSummary>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super DailyHeartRateSummary> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    nm6 nm6 = this.this$0.this$0;
                    return nm6.e0(nm6.f, this.this$0.this$0.i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(nm6 nm6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = nm6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Integer e;
            Resting resting;
            Integer e2;
            int i = 0;
            Object d = yn7.d();
            int i2 = this.label;
            if (i2 == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 h = this.this$0.h();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(h, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i2 == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) g;
            if (this.this$0.k == null || (!pq7.a(this.this$0.k, dailyHeartRateSummary))) {
                this.this$0.k = dailyHeartRateSummary;
                DailyHeartRateSummary dailyHeartRateSummary2 = this.this$0.k;
                int intValue = (dailyHeartRateSummary2 == null || (resting = dailyHeartRateSummary2.getResting()) == null || (e2 = ao7.e(resting.getValue())) == null) ? 0 : e2.intValue();
                DailyHeartRateSummary dailyHeartRateSummary3 = this.this$0.k;
                if (!(dailyHeartRateSummary3 == null || (e = ao7.e(dailyHeartRateSummary3.getMax())) == null)) {
                    i = e.intValue();
                }
                this.this$0.q.A5(intValue, i);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1", f = "HeartRateDetailPresenter.kt", l = {238, 242, 251, 258}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ nm6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.nm6$f$a$a")
            /* renamed from: com.fossil.nm6$f$a$a  reason: collision with other inner class name */
            public static final class C0175a<T> implements Comparator<T> {
                @DexIgnore
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    return mn7.c(Long.valueOf(t.getStartTimeId().getMillis()), Long.valueOf(t2.getStartTimeId().getMillis()));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    List list = this.this$0.this$0.l;
                    if (list == null) {
                        return null;
                    }
                    if (list.size() > 1) {
                        lm7.r(list, new C0175a());
                    }
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$2", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $listTimeZoneChange;
            @DexIgnore
            public /* final */ /* synthetic */ List $listTodayHeartRateModel;
            @DexIgnore
            public /* final */ /* synthetic */ int $maxHR;
            @DexIgnore
            public /* final */ /* synthetic */ br7 $previousTimeZoneOffset;
            @DexIgnore
            public /* final */ /* synthetic */ long $startOfDay;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(f fVar, long j, br7 br7, List list, List list2, int i, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
                this.$startOfDay = j;
                this.$previousTimeZoneOffset = br7;
                this.$listTimeZoneChange = list;
                this.$listTodayHeartRateModel = list2;
                this.$maxHR = i;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, this.$startOfDay, this.$previousTimeZoneOffset, this.$listTimeZoneChange, this.$listTodayHeartRateModel, this.$maxHR, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                StringBuilder sb;
                char c;
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    List<HeartRateSample> list = this.this$0.this$0.l;
                    if (list == null) {
                        return null;
                    }
                    for (HeartRateSample heartRateSample : list) {
                        long j = (long) 60000;
                        long millis = (heartRateSample.getStartTimeId().getMillis() - this.$startOfDay) / j;
                        long millis2 = (heartRateSample.getEndTime().getMillis() - this.$startOfDay) / j;
                        long j2 = (millis2 + millis) / ((long) 2);
                        if (heartRateSample.getTimezoneOffsetInSecond() != this.$previousTimeZoneOffset.element) {
                            int hourOfDay = heartRateSample.getStartTimeId().getHourOfDay();
                            String c2 = kl5.c(hourOfDay);
                            hr7 hr7 = hr7.f1520a;
                            String format = String.format("%02d:%02d", Arrays.copyOf(new Object[]{ao7.e(Math.abs(heartRateSample.getTimezoneOffsetInSecond() / 3600)), ao7.e(Math.abs((heartRateSample.getTimezoneOffsetInSecond() / 60) % 60))}, 2));
                            pq7.b(format, "java.lang.String.format(format, *args)");
                            List list2 = this.$listTimeZoneChange;
                            Integer e = ao7.e((int) j2);
                            cl7 cl7 = new cl7(ao7.e(hourOfDay), ao7.d(((float) heartRateSample.getTimezoneOffsetInSecond()) / 3600.0f));
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(c2);
                            if (heartRateSample.getTimezoneOffsetInSecond() >= 0) {
                                sb = new StringBuilder();
                                c = '+';
                            } else {
                                sb = new StringBuilder();
                                c = '-';
                            }
                            sb.append(c);
                            sb.append(format);
                            sb2.append(sb.toString());
                            list2.add(new gl7(e, cl7, sb2.toString()));
                            this.$previousTimeZoneOffset.element = heartRateSample.getTimezoneOffsetInSecond();
                        }
                        if (!this.$listTodayHeartRateModel.isEmpty()) {
                            w57 w57 = (w57) pm7.P(this.$listTodayHeartRateModel);
                            if (millis - ((long) w57.a()) > 1) {
                                this.$listTodayHeartRateModel.add(new w57(0, 0, 0, w57.a(), (int) millis, (int) j2));
                            }
                        }
                        int average = (int) heartRateSample.getAverage();
                        if (heartRateSample.getMax() == this.$maxHR) {
                            average = heartRateSample.getMax();
                        }
                        this.$listTodayHeartRateModel.add(new w57(average, heartRateSample.getMin(), heartRateSample.getMax(), (int) millis, (int) millis2, (int) j2));
                    }
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$maxHR$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, qn7);
                cVar.p$ = (iv7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
                return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                int i;
                Object obj2;
                Integer e;
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    List list = this.this$0.this$0.l;
                    if (list != null) {
                        Iterator it = list.iterator();
                        if (!it.hasNext()) {
                            obj2 = null;
                        } else {
                            Object next = it.next();
                            if (it.hasNext()) {
                                Integer e2 = ao7.e(((HeartRateSample) next).getMax());
                                while (true) {
                                    next = it.next();
                                    e2 = ao7.e(((HeartRateSample) next).getMax());
                                    if (e2.compareTo(e2) >= 0) {
                                        e2 = e2;
                                        next = next;
                                    }
                                    if (!it.hasNext()) {
                                        break;
                                    }
                                }
                            }
                            obj2 = next;
                        }
                        HeartRateSample heartRateSample = (HeartRateSample) obj2;
                        if (!(heartRateSample == null || (e = ao7.e(heartRateSample.getMax())) == null)) {
                            i = e.intValue();
                            return ao7.e(i);
                        }
                    }
                    i = 0;
                    return ao7.e(i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$summary$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class d extends ko7 implements vp7<iv7, qn7<? super List<HeartRateSample>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                d dVar = new d(this.this$0, qn7);
                dVar.p$ = (iv7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<HeartRateSample>> qn7) {
                return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    nm6 nm6 = this.this$0.this$0;
                    return nm6.d0(nm6.f, this.this$0.this$0.j);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(nm6 nm6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = nm6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v144, types: [java.util.List] */
        /* JADX WARN: Type inference failed for: r0v147, types: [java.util.List] */
        /* JADX WARN: Type inference failed for: r1v56, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:109:0x03e1  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0137  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x0152  */
        /* JADX WARNING: Removed duplicated region for block: B:51:0x019f  */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x01e1  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x022f  */
        /* JADX WARNING: Unknown variable types count: 3 */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 1000
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.nm6.f.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1", f = "HeartRateDetailPresenter.kt", l = {143}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ nm6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements ls0<h47<? extends List<DailyHeartRateSummary>>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ g f2545a;

            @DexIgnore
            public a(g gVar) {
                this.f2545a = gVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(h47<? extends List<DailyHeartRateSummary>> h47) {
                xh5 xh5 = null;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("start - summaryTransformations -- status=");
                sb.append(h47 != null ? h47.d() : null);
                sb.append(", resource=");
                sb.append(h47 != null ? (List) h47.c() : null);
                sb.append(", status=");
                if (h47 != null) {
                    xh5 = h47.d();
                }
                sb.append(xh5);
                local.d("HeartRateDetailPresenter", sb.toString());
                if (h47 != null && h47.d() != xh5.DATABASE_LOADING) {
                    this.f2545a.this$0.i = (List) h47.c();
                    this.f2545a.this$0.j0();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements ls0<h47<? extends List<HeartRateSample>>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ g f2546a;

            @DexIgnore
            public b(g gVar) {
                this.f2546a = gVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(h47<? extends List<HeartRateSample>> h47) {
                xh5 xh5 = null;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("start - sampleTransformations -- status=");
                sb.append(h47 != null ? h47.d() : null);
                sb.append(", resource=");
                sb.append(h47 != null ? (List) h47.c() : null);
                sb.append(", status=");
                if (h47 != null) {
                    xh5 = h47.d();
                }
                sb.append(xh5);
                local.d("HeartRateDetailPresenter", sb.toString());
                if (h47 != null && h47.d() != xh5.DATABASE_LOADING) {
                    this.f2546a.this$0.j = (List) h47.c();
                    this.f2546a.this$0.k0();
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(nm6 nm6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = nm6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, qn7);
            gVar.p$ = (iv7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object currentUser;
            nm6 nm6;
            String value;
            MFUser.UnitGroup unitGroup;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                nm6 nm62 = this.this$0;
                UserRepository userRepository = nm62.t;
                this.L$0 = iv7;
                this.L$1 = nm62;
                this.label = 1;
                currentUser = userRepository.getCurrentUser(this);
                if (currentUser == d) {
                    return d;
                }
                nm6 = nm62;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                nm6 = (nm6) this.L$1;
                currentUser = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) currentUser;
            if (mFUser == null || (unitGroup = mFUser.getUnitGroup()) == null || (value = unitGroup.getDistance()) == null) {
                value = ai5.METRIC.getValue();
            }
            ai5 fromString = ai5.fromString(value);
            pq7.b(fromString, "Unit.fromString(mUserRep\u2026    ?: Unit.METRIC.value)");
            nm6.m = fromString;
            LiveData liveData = this.this$0.n;
            jm6 jm6 = this.this$0.q;
            if (jm6 != null) {
                liveData.h((km6) jm6, new a(this));
                this.this$0.o.h((LifecycleOwner) this.this$0.q, new b(this));
                return tl7.f3441a;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ nm6 f2547a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$summaryTransformations$1$1", f = "HeartRateDetailPresenter.kt", l = {68, 68}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<DailyHeartRateSummary>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, Date date, Date date2, qn7 qn7) {
                super(2, qn7);
                this.this$0 = hVar;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$first, this.$second, qn7);
                aVar.p$ = (hs0) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<DailyHeartRateSummary>>> hs0, qn7<? super tl7> qn7) {
                return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r8)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r8)
                    com.fossil.hs0 r0 = r7.p$
                    com.fossil.nm6$h r1 = r7.this$0
                    com.fossil.nm6 r1 = r1.f2547a
                    com.portfolio.platform.data.source.HeartRateSummaryRepository r1 = com.fossil.nm6.G(r1)
                    java.util.Date r2 = r7.$first
                    java.util.Date r3 = r7.$second
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getHeartRateSummaries(r2, r3, r5, r7)
                    if (r2 != r4) goto L_0x005b
                    r0 = r4
                    goto L_0x0017
                L_0x005b:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.nm6.h.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public h(nm6 nm6) {
            this.f2547a = nm6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<DailyHeartRateSummary>>> apply(cl7<? extends Date, ? extends Date> cl7) {
            return or0.c(null, 0, new a(this, (Date) cl7.component1(), (Date) cl7.component2(), null), 3, null);
        }
    }

    @DexIgnore
    public nm6(jm6 jm6, HeartRateSummaryRepository heartRateSummaryRepository, HeartRateSampleRepository heartRateSampleRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FileRepository fileRepository, no4 no4) {
        pq7.c(jm6, "mView");
        pq7.c(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        pq7.c(heartRateSampleRepository, "mHeartRateSampleRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(workoutSessionRepository, "mWorkoutSessionRepository");
        pq7.c(fileRepository, "mFileRepository");
        pq7.c(no4, "appExecutors");
        this.q = jm6;
        this.r = heartRateSummaryRepository;
        this.s = heartRateSampleRepository;
        this.t = userRepository;
        this.u = workoutSessionRepository;
        this.v = no4;
        this.h = new WorkoutTetherScreenShotManager(this.u, fileRepository);
        this.i = new ArrayList();
        this.j = new ArrayList();
        this.m = ai5.METRIC;
        LiveData<h47<List<DailyHeartRateSummary>>> c2 = ss0.c(this.g, new h(this));
        pq7.b(c2, "Transformations.switchMa\u2026t, second, true)) }\n    }");
        this.n = c2;
        LiveData<h47<List<HeartRateSample>>> c3 = ss0.c(this.g, new c(this));
        pq7.b(c3, "Transformations.switchMa\u2026st, second, true))}\n    }");
        this.o = c3;
    }

    @DexIgnore
    public final List<HeartRateSample> d0(Date date, List<HeartRateSample> list) {
        ts7 z;
        ts7 h2;
        if (list == null || (z = pm7.z(list)) == null || (h2 = at7.h(z, new a(date))) == null) {
            return null;
        }
        return at7.u(h2);
    }

    @DexIgnore
    @Override // com.fossil.fl5.a
    public void e(fl5.g gVar) {
        pq7.c(gVar, "report");
    }

    @DexIgnore
    public final DailyHeartRateSummary e0(Date date, List<DailyHeartRateSummary> list) {
        T t2;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (lk5.m0(next.getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return t2;
    }

    @DexIgnore
    /* renamed from: f0 */
    public WorkoutTetherScreenShotManager o() {
        return this.h;
    }

    @DexIgnore
    public final String g0(Float f2) {
        if (f2 == null) {
            return "";
        }
        hr7 hr7 = hr7.f1520a;
        String format = String.format("%02d:%02d", Arrays.copyOf(new Object[]{Integer.valueOf((int) Math.abs(f2.floatValue())), Integer.valueOf(((int) Math.abs(f2.floatValue() - (f2.floatValue() % ((float) 10)))) * 60)}, 2));
        pq7.b(format, "java.lang.String.format(format, *args)");
        if (f2.floatValue() >= ((float) 0)) {
            return '+' + format;
        }
        return '-' + format;
    }

    @DexIgnore
    public final void h0(Date date) {
        q();
        xw7 unused = gu7.d(k(), null, null, new b(this, date, null), 3, null);
    }

    @DexIgnore
    public void i0() {
        this.q.M5(this);
    }

    @DexIgnore
    public final xw7 j0() {
        return gu7.d(k(), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final xw7 k0() {
        return gu7.d(k(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        xw7 unused = gu7.d(k(), null, null, new g(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", "stop");
        LiveData<h47<List<HeartRateSample>>> liveData = this.o;
        jm6 jm6 = this.q;
        if (jm6 != null) {
            liveData.n((km6) jm6);
            this.n.n((LifecycleOwner) this.q);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
    }

    @DexIgnore
    @Override // com.fossil.im6
    public ai5 n() {
        return this.m;
    }

    @DexIgnore
    @Override // com.fossil.im6
    public void p(Date date) {
        pq7.c(date, "date");
        h0(date);
    }

    @DexIgnore
    @Override // com.fossil.im6
    public void q() {
        LiveData<cu0<WorkoutSession>> pagedList;
        try {
            Listing<WorkoutSession> listing = this.p;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                jm6 jm6 = this.q;
                if (jm6 != null) {
                    pagedList.n((km6) jm6);
                } else {
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
                }
            }
            this.u.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.e("HeartRateDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.im6
    public void r(Bundle bundle) {
        pq7.c(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.f.getTime());
    }

    @DexIgnore
    @Override // com.fossil.im6
    public void s(Date date) {
        pq7.c(date, "date");
        xw7 unused = gu7.d(k(), null, null, new d(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.im6
    public void t() {
        Date O = lk5.O(this.f);
        pq7.b(O, "DateHelper.getNextDate(mDate)");
        s(O);
    }

    @DexIgnore
    @Override // com.fossil.im6
    public void u() {
        Date P = lk5.P(this.f);
        pq7.b(P, "DateHelper.getPrevDate(mDate)");
        s(P);
    }
}
