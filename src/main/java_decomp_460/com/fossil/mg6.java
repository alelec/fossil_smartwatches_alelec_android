package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mg6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ gg6 f2378a;
    @DexIgnore
    public /* final */ xg6 b;
    @DexIgnore
    public /* final */ rg6 c;

    @DexIgnore
    public mg6(gg6 gg6, xg6 xg6, rg6 rg6) {
        pq7.c(gg6, "mCaloriesOverviewDayView");
        pq7.c(xg6, "mCaloriesOverviewWeekView");
        pq7.c(rg6, "mCaloriesOverviewMonthView");
        this.f2378a = gg6;
        this.b = xg6;
        this.c = rg6;
    }

    @DexIgnore
    public final gg6 a() {
        return this.f2378a;
    }

    @DexIgnore
    public final rg6 b() {
        return this.c;
    }

    @DexIgnore
    public final xg6 c() {
        return this.b;
    }
}
