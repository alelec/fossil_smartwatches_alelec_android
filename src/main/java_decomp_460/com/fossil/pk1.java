package com.fossil;

import com.crashlytics.android.answers.CustomEvent;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pk1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2839a;
    @DexIgnore
    public /* final */ Map<String, Object> b; // = new HashMap();

    @DexIgnore
    public pk1(String str) {
        this.f2839a = str;
    }

    @DexIgnore
    public pk1 a(String str, String str2) {
        this.b.put(str, str2);
        return this;
    }

    @DexIgnore
    public CustomEvent b() {
        CustomEvent customEvent = new CustomEvent(this.f2839a);
        for (String str : this.b.keySet()) {
            Object obj = this.b.get(str);
            if (obj instanceof String) {
                customEvent.putCustomAttribute(str, (String) obj);
            } else if (obj instanceof Number) {
                customEvent.putCustomAttribute(str, (Number) obj);
            }
        }
        return customEvent;
    }
}
