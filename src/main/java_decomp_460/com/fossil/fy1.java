package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fy1 {
    @DexIgnore
    public static final String a(Throwable th) {
        pq7.c(th, "$this$wrappedLocalizedMessage");
        String localizedMessage = th.getLocalizedMessage();
        return localizedMessage != null ? localizedMessage : th.toString();
    }
}
