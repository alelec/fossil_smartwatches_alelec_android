package com.fossil;

import android.graphics.Bitmap;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qb3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ zb3 f2955a;
    @DexIgnore
    public xb3 b;

    @DexIgnore
    public interface a {
        @DexIgnore
        Object onCancel();  // void declaration

        @DexIgnore
        Object onFinish();  // void declaration
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        Object onCameraIdle();  // void declaration
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        Object onCameraMove();  // void declaration
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void onCameraMoveStarted(int i);
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void onCircleClick(de3 de3);
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        void onInfoWindowClick(ke3 ke3);
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void onMapClick(LatLng latLng);
    }

    @DexIgnore
    public interface h {
        @DexIgnore
        Object onMapLoaded();  // void declaration
    }

    @DexIgnore
    public interface i {
        @DexIgnore
        void onMapLongClick(LatLng latLng);
    }

    @DexIgnore
    public interface j {
        @DexIgnore
        boolean onMarkerClick(ke3 ke3);
    }

    @DexIgnore
    public interface k {
        @DexIgnore
        void onMarkerDrag(ke3 ke3);

        @DexIgnore
        void onMarkerDragEnd(ke3 ke3);

        @DexIgnore
        void onMarkerDragStart(ke3 ke3);
    }

    @DexIgnore
    public interface l {
        @DexIgnore
        void onPolygonClick(ne3 ne3);
    }

    @DexIgnore
    public interface m {
        @DexIgnore
        void onPolylineClick(pe3 pe3);
    }

    @DexIgnore
    public interface n {
        @DexIgnore
        void onSnapshotReady(Bitmap bitmap);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o extends ld3 {
        @DexIgnore
        public /* final */ a b;

        @DexIgnore
        public o(a aVar) {
            this.b = aVar;
        }

        @DexIgnore
        @Override // com.fossil.kd3
        public final void onCancel() {
            this.b.onCancel();
        }

        @DexIgnore
        @Override // com.fossil.kd3
        public final void onFinish() {
            this.b.onFinish();
        }
    }

    @DexIgnore
    public qb3(zb3 zb3) {
        rc2.k(zb3);
        this.f2955a = zb3;
    }

    @DexIgnore
    public final void A(d dVar) {
        if (dVar == null) {
            try {
                this.f2955a.B(null);
            } catch (RemoteException e2) {
                throw new se3(e2);
            }
        } else {
            this.f2955a.B(new yf3(this, dVar));
        }
    }

    @DexIgnore
    public final void B(e eVar) {
        if (eVar == null) {
            try {
                this.f2955a.h2(null);
            } catch (RemoteException e2) {
                throw new se3(e2);
            }
        } else {
            this.f2955a.h2(new uf3(this, eVar));
        }
    }

    @DexIgnore
    public final void C(f fVar) {
        if (fVar == null) {
            try {
                this.f2955a.J0(null);
            } catch (RemoteException e2) {
                throw new se3(e2);
            }
        } else {
            this.f2955a.J0(new sf3(this, fVar));
        }
    }

    @DexIgnore
    public final void D(g gVar) {
        if (gVar == null) {
            try {
                this.f2955a.H(null);
            } catch (RemoteException e2) {
                throw new se3(e2);
            }
        } else {
            this.f2955a.H(new bg3(this, gVar));
        }
    }

    @DexIgnore
    public final void E(h hVar) {
        if (hVar == null) {
            try {
                this.f2955a.X(null);
            } catch (RemoteException e2) {
                throw new se3(e2);
            }
        } else {
            this.f2955a.X(new tf3(this, hVar));
        }
    }

    @DexIgnore
    public final void F(i iVar) {
        if (iVar == null) {
            try {
                this.f2955a.H2(null);
            } catch (RemoteException e2) {
                throw new se3(e2);
            }
        } else {
            this.f2955a.H2(new cg3(this, iVar));
        }
    }

    @DexIgnore
    public final void G(j jVar) {
        if (jVar == null) {
            try {
                this.f2955a.D2(null);
            } catch (RemoteException e2) {
                throw new se3(e2);
            }
        } else {
            this.f2955a.D2(new qf3(this, jVar));
        }
    }

    @DexIgnore
    public final void H(k kVar) {
        if (kVar == null) {
            try {
                this.f2955a.R2(null);
            } catch (RemoteException e2) {
                throw new se3(e2);
            }
        } else {
            this.f2955a.R2(new rf3(this, kVar));
        }
    }

    @DexIgnore
    public final void I(l lVar) {
        if (lVar == null) {
            try {
                this.f2955a.S2(null);
            } catch (RemoteException e2) {
                throw new se3(e2);
            }
        } else {
            this.f2955a.S2(new vf3(this, lVar));
        }
    }

    @DexIgnore
    public final void J(m mVar) {
        if (mVar == null) {
            try {
                this.f2955a.R(null);
            } catch (RemoteException e2) {
                throw new se3(e2);
            }
        } else {
            this.f2955a.R(new wf3(this, mVar));
        }
    }

    @DexIgnore
    public final void K(int i2, int i3, int i4, int i5) {
        try {
            this.f2955a.a0(i2, i3, i4, i5);
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final void L(boolean z) {
        try {
            this.f2955a.setTrafficEnabled(z);
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final void M(n nVar) {
        N(nVar, null);
    }

    @DexIgnore
    public final void N(n nVar, Bitmap bitmap) {
        try {
            this.f2955a.I0(new xf3(this, nVar), (tg2) (bitmap != null ? tg2.n(bitmap) : null));
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final de3 a(ee3 ee3) {
        try {
            return new de3(this.f2955a.Q(ee3));
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final ke3 b(le3 le3) {
        try {
            ls2 N2 = this.f2955a.N2(le3);
            if (N2 != null) {
                return new ke3(N2);
            }
            return null;
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final ne3 c(oe3 oe3) {
        try {
            return new ne3(this.f2955a.w1(oe3));
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final pe3 d(qe3 qe3) {
        try {
            return new pe3(this.f2955a.x2(qe3));
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final void e(ob3 ob3) {
        try {
            this.f2955a.j2(ob3.a());
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final void f(ob3 ob3, a aVar) {
        try {
            this.f2955a.U0(ob3.a(), aVar == null ? null : new o(aVar));
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final void g() {
        try {
            this.f2955a.clear();
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final CameraPosition h() {
        try {
            return this.f2955a.p0();
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final float i() {
        try {
            return this.f2955a.z2();
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final float j() {
        try {
            return this.f2955a.z();
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final ub3 k() {
        try {
            return new ub3(this.f2955a.b2());
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final xb3 l() {
        try {
            if (this.b == null) {
                this.b = new xb3(this.f2955a.F1());
            }
            return this.b;
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final boolean m() {
        try {
            return this.f2955a.N1();
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final boolean n() {
        try {
            return this.f2955a.R0();
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final void o(ob3 ob3) {
        try {
            this.f2955a.o0(ob3.a());
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final void p() {
        try {
            this.f2955a.u1();
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final void q(boolean z) {
        try {
            this.f2955a.setBuildingsEnabled(z);
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final boolean r(boolean z) {
        try {
            return this.f2955a.setIndoorEnabled(z);
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final void s(LatLngBounds latLngBounds) {
        try {
            this.f2955a.J(latLngBounds);
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final boolean t(je3 je3) {
        try {
            return this.f2955a.v0(je3);
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final void u(int i2) {
        try {
            this.f2955a.setMapType(i2);
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final void v(float f2) {
        try {
            this.f2955a.Y0(f2);
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final void w(float f2) {
        try {
            this.f2955a.e1(f2);
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final void x(boolean z) {
        try {
            this.f2955a.setMyLocationEnabled(z);
        } catch (RemoteException e2) {
            throw new se3(e2);
        }
    }

    @DexIgnore
    public final void y(b bVar) {
        if (bVar == null) {
            try {
                this.f2955a.x1(null);
            } catch (RemoteException e2) {
                throw new se3(e2);
            }
        } else {
            this.f2955a.x1(new ag3(this, bVar));
        }
    }

    @DexIgnore
    public final void z(c cVar) {
        if (cVar == null) {
            try {
                this.f2955a.a2(null);
            } catch (RemoteException e2) {
                throw new se3(e2);
            }
        } else {
            this.f2955a.a2(new zf3(this, cVar));
        }
    }
}
