package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f77 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f1068a; // = -1;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ d77 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ge5 f1069a;
        @DexIgnore
        public /* final */ d77 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.f77$a$a")
        /* renamed from: com.fossil.f77$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0079a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;
            @DexIgnore
            public /* final */ /* synthetic */ fb7 c;

            @DexIgnore
            public View$OnClickListenerC0079a(a aVar, fb7 fb7, boolean z, int i) {
                this.b = aVar;
                this.c = fb7;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.b.b(this.c, this.b.getAdapterPosition());
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnLongClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public b(a aVar, fb7 fb7, boolean z, int i) {
                this.b = aVar;
            }

            @DexIgnore
            public final boolean onLongClick(View view) {
                this.b.b.a();
                return true;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;
            @DexIgnore
            public /* final */ /* synthetic */ fb7 c;

            @DexIgnore
            public c(a aVar, fb7 fb7, boolean z, int i) {
                this.b = aVar;
                this.c = fb7;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.b.d(this.c);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class d implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public d(a aVar, fb7 fb7, boolean z, int i) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.b.onCancel();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ge5 ge5, d77 d77) {
            super(ge5.n());
            pq7.c(ge5, "binding");
            pq7.c(d77, "photoListener");
            this.f1069a = ge5;
            this.b = d77;
        }

        @DexIgnore
        public final void b(fb7 fb7, int i, boolean z) {
            pq7.c(fb7, "wfWrapper");
            ge5 ge5 = this.f1069a;
            ge5.q.setOnClickListener(new View$OnClickListenerC0079a(this, fb7, z, i));
            ge5.q.setOnLongClickListener(new b(this, fb7, z, i));
            ge5.r.setOnClickListener(new c(this, fb7, z, i));
            ge5.n().setOnClickListener(new d(this, fb7, z, i));
            ImageView imageView = ge5.r;
            pq7.b(imageView, "ivRemove");
            imageView.setVisibility(z ? 0 : 8);
            Drawable e = fb7.e();
            if (e != null) {
                ImageView imageView2 = ge5.q;
                pq7.b(imageView2, "ivBackgroundPreview");
                e51.a(imageView2);
                ge5.q.setImageDrawable(e);
            } else {
                ImageView imageView3 = ge5.q;
                pq7.b(imageView3, "ivBackgroundPreview");
                ty4.a(imageView3, fb7.f(), gl0.f(PortfolioApp.h0.c(), 2131231268));
            }
            if (i == getAdapterPosition()) {
                View view = ge5.t;
                pq7.b(view, "vBackgroundSelected");
                view.setVisibility(0);
                return;
            }
            View view2 = ge5.t;
            pq7.b(view2, "vBackgroundSelected");
            view2.setVisibility(8);
        }
    }

    @DexIgnore
    public f77(int i, d77 d77) {
        pq7.c(d77, "photoListener");
        this.c = i;
        this.d = d77;
    }

    @DexIgnore
    public final void a(int i) {
        this.f1068a = i;
    }

    @DexIgnore
    public final int b() {
        return this.c;
    }

    @DexIgnore
    public final void c(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public boolean d(List<? extends Object> list, int i) {
        pq7.c(list, "items");
        Object obj = list.get(i);
        return (obj instanceof fb7) && ((fb7) obj).a() == l77.BACKGROUND_PHOTO;
    }

    @DexIgnore
    public void e(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        a aVar = null;
        pq7.c(list, "items");
        pq7.c(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof fb7)) {
            obj = null;
        }
        fb7 fb7 = (fb7) obj;
        if (fb7 != null) {
            if (viewHolder instanceof a) {
                aVar = viewHolder;
            }
            a aVar2 = aVar;
            if (aVar2 != null) {
                aVar2.b(fb7, this.f1068a, this.b);
            }
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder f(ViewGroup viewGroup) {
        pq7.c(viewGroup, "parent");
        ge5 z = ge5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemDianaBackgroundBindi\u2026(inflater, parent, false)");
        return new a(z, this.d);
    }
}
