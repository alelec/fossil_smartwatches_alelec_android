package com.fossil;

import android.text.TextUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class va6 extends ra6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public PlacesClient e;
    @DexIgnore
    public WeatherWatchAppSetting f;
    @DexIgnore
    public List<WeatherLocationWrapper> g; // = new ArrayList();
    @DexIgnore
    public /* final */ sa6 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<TResult> implements jt3<FetchPlaceResponse> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ va6 f3741a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.va6$a$a")
        /* renamed from: com.fossil.va6$a$a  reason: collision with other inner class name */
        public static final class C0259a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ LatLng $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.va6$a$a$a")
            /* renamed from: com.fossil.va6$a$a$a  reason: collision with other inner class name */
            public static final class C0260a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0259a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0260a(C0259a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0260a aVar = new C0260a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0260a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        this.this$0.this$0.f3741a.h.h2(this.this$0.this$0.f3741a.g);
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0259a(LatLng latLng, qn7 qn7, a aVar) {
                super(2, qn7);
                this.$it = latLng;
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0259a aVar = new C0259a(this.$it, qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((C0259a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    String c = p47.c(this.this$0.b);
                    List list = this.this$0.f3741a.g;
                    String str = this.this$0.c;
                    LatLng latLng = this.$it;
                    double d2 = latLng.b;
                    double d3 = latLng.c;
                    pq7.b(c, "name");
                    list.add(new WeatherLocationWrapper(str, d2, d3, c, this.this$0.b, false, true, 32, null));
                    jx7 c2 = bw7.c();
                    C0260a aVar = new C0260a(this, null);
                    this.L$0 = iv7;
                    this.L$1 = c;
                    this.label = 1;
                    if (eu7.g(c2, aVar, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    String str2 = (String) this.L$1;
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public a(va6 va6, String str, String str2) {
            this.f3741a = va6;
            this.b = str;
            this.c = str2;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
            this.f3741a.h.a();
            pq7.b(fetchPlaceResponse, "response");
            Place place = fetchPlaceResponse.getPlace();
            pq7.b(place, "response.place");
            LatLng latLng = place.getLatLng();
            if (latLng != null) {
                xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new C0259a(latLng, null, this), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements it3 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ va6 f3742a;

        @DexIgnore
        public b(va6 va6) {
            this.f3742a = va6;
        }

        @DexIgnore
        @Override // com.fossil.it3
        public final void onFailure(Exception exc) {
            pq7.c(exc, "exception");
            this.f3742a.h.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = va6.i;
            local.e(str, "FetchPlaceRequest - exception=" + exc);
            this.f3742a.h.E4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$saveWeatherWatchAppSetting$1", f = "WeatherSettingPresenter.kt", l = {129}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ va6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends WeatherLocationWrapper>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(qn7 qn7, c cVar) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends WeatherLocationWrapper>> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    List list = this.this$0.this$0.g;
                    ArrayList arrayList = new ArrayList();
                    for (Object obj2 : list) {
                        if (ao7.a(((WeatherLocationWrapper) obj2).isEnableLocation()).booleanValue()) {
                            arrayList.add(obj2);
                        }
                    }
                    return arrayList;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(va6 va6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = va6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            WeatherWatchAppSetting weatherWatchAppSetting;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                WeatherWatchAppSetting weatherWatchAppSetting2 = this.this$0.f;
                if (weatherWatchAppSetting2 != null) {
                    dv7 h = this.this$0.h();
                    a aVar = new a(null, this);
                    this.L$0 = iv7;
                    this.L$1 = weatherWatchAppSetting2;
                    this.label = 1;
                    g = eu7.g(h, aVar, this);
                    if (g == d) {
                        return d;
                    }
                    weatherWatchAppSetting = weatherWatchAppSetting2;
                }
                this.this$0.h.a();
                this.this$0.h.x0(true);
                return tl7.f3441a;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
                weatherWatchAppSetting = (WeatherWatchAppSetting) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            weatherWatchAppSetting.setLocations(pm7.j0((List) g));
            this.this$0.h.a();
            this.this$0.h.x0(true);
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = va6.class.getSimpleName();
        pq7.b(simpleName, "WeatherSettingPresenter::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public va6(sa6 sa6, GoogleApiService googleApiService) {
        pq7.c(sa6, "mView");
        pq7.c(googleApiService, "mGoogleApiService");
        this.h = sa6;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        this.e = Places.createClient(PortfolioApp.h0.c());
        this.h.h2(this.g);
        this.h.H(this.e);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        this.e = null;
    }

    @DexIgnore
    @Override // com.fossil.ra6
    public void n(String str, String str2, AutocompleteSessionToken autocompleteSessionToken) {
        pq7.c(str, "address");
        pq7.c(str2, "placeId");
        List<WeatherLocationWrapper> list = this.g;
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            if (t.isEnableLocation()) {
                arrayList.add(t);
            }
        }
        if (arrayList.size() > 1) {
            this.h.G1();
            return;
        }
        this.h.b();
        if (this.e != null) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(Place.Field.ADDRESS);
            arrayList2.add(Place.Field.LAT_LNG);
            FetchPlaceRequest.Builder builder = FetchPlaceRequest.builder(str2, arrayList2);
            pq7.b(builder, "FetchPlaceRequest.builder(placeId, placeFields)");
            builder.setSessionToken(autocompleteSessionToken);
            PlacesClient placesClient = this.e;
            if (placesClient != null) {
                nt3<FetchPlaceResponse> fetchPlace = placesClient.fetchPlace(builder.build());
                pq7.b(fetchPlace, "mPlacesClient!!.fetchPlace(request.build())");
                fetchPlace.f(new a(this, str, str2));
                fetchPlace.d(new b(this));
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ra6
    public WeatherWatchAppSetting o() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.ra6
    public void p() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "saveWeatherWatchAppSetting - mLocationWrappers=" + this.g);
        this.h.b();
        xw7 unused = gu7.d(k(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.ra6
    public void q(int i2, boolean z) {
        List<WeatherLocationWrapper> list = this.g;
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            if (t.isEnableLocation()) {
                arrayList.add(t);
            }
        }
        if (z && arrayList.size() > 1) {
            this.h.G1();
        } else if (i2 < this.g.size()) {
            this.g.get(i2).setEnableLocation(z);
            this.h.h2(this.g);
        }
    }

    @DexIgnore
    public void w(String str) {
        WeatherWatchAppSetting weatherWatchAppSetting;
        pq7.c(str, MicroAppSetting.SETTING);
        try {
            weatherWatchAppSetting = (WeatherWatchAppSetting) new Gson().k(str, WeatherWatchAppSetting.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local.d(str2, "exception when parse weather setting " + e2);
            weatherWatchAppSetting = new WeatherWatchAppSetting();
        }
        this.f = weatherWatchAppSetting;
        if (weatherWatchAppSetting == null) {
            this.f = new WeatherWatchAppSetting();
        }
        WeatherWatchAppSetting weatherWatchAppSetting2 = this.f;
        if (weatherWatchAppSetting2 != null) {
            List<WeatherLocationWrapper> locations = weatherWatchAppSetting2.getLocations();
            ArrayList arrayList = new ArrayList();
            for (T t : locations) {
                T t2 = t;
                if (!TextUtils.isEmpty(t2 != null ? t2.getId() : null)) {
                    arrayList.add(t);
                }
            }
            if (arrayList.isEmpty()) {
                this.f = new WeatherWatchAppSetting();
            }
            this.g.clear();
            WeatherWatchAppSetting weatherWatchAppSetting3 = this.f;
            if (weatherWatchAppSetting3 != null) {
                for (WeatherLocationWrapper weatherLocationWrapper : weatherWatchAppSetting3.getLocations()) {
                    if (weatherLocationWrapper != null) {
                        this.g.add(weatherLocationWrapper);
                    }
                }
                return;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public void x() {
        this.h.M5(this);
    }
}
