package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xj6 implements MembersInjector<SleepOverviewFragment> {
    @DexIgnore
    public static void a(SleepOverviewFragment sleepOverviewFragment, uj6 uj6) {
        sleepOverviewFragment.h = uj6;
    }

    @DexIgnore
    public static void b(SleepOverviewFragment sleepOverviewFragment, fk6 fk6) {
        sleepOverviewFragment.j = fk6;
    }

    @DexIgnore
    public static void c(SleepOverviewFragment sleepOverviewFragment, lk6 lk6) {
        sleepOverviewFragment.i = lk6;
    }
}
