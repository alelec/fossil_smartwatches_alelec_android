package com.fossil;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jp6 implements Factory<hp6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<SummariesRepository> f1788a;
    @DexIgnore
    public /* final */ Provider<SleepSummariesRepository> b;
    @DexIgnore
    public /* final */ Provider<GoalTrackingRepository> c;
    @DexIgnore
    public /* final */ Provider<UserRepository> d;
    @DexIgnore
    public /* final */ Provider<jt5> e;

    @DexIgnore
    public jp6(Provider<SummariesRepository> provider, Provider<SleepSummariesRepository> provider2, Provider<GoalTrackingRepository> provider3, Provider<UserRepository> provider4, Provider<jt5> provider5) {
        this.f1788a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static jp6 a(Provider<SummariesRepository> provider, Provider<SleepSummariesRepository> provider2, Provider<GoalTrackingRepository> provider3, Provider<UserRepository> provider4, Provider<jt5> provider5) {
        return new jp6(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public static hp6 c(SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, jt5 jt5) {
        return new hp6(summariesRepository, sleepSummariesRepository, goalTrackingRepository, userRepository, jt5);
    }

    @DexIgnore
    /* renamed from: b */
    public hp6 get() {
        return c(this.f1788a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }
}
