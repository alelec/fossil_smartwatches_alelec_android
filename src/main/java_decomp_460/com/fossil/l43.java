package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum l43 {
    DOUBLE(s43.DOUBLE, 1),
    FLOAT(s43.FLOAT, 5),
    INT64(s43.LONG, 0),
    UINT64(s43.LONG, 0),
    INT32(s43.INT, 0),
    FIXED64(s43.LONG, 1),
    FIXED32(s43.INT, 5),
    BOOL(s43.BOOLEAN, 0),
    STRING(s43.STRING, 2) {
    },
    GROUP(s43.MESSAGE, 3) {
    },
    MESSAGE(s43.MESSAGE, 2) {
    },
    BYTES(s43.BYTE_STRING, 2) {
    },
    UINT32(s43.INT, 0),
    ENUM(s43.ENUM, 0),
    SFIXED32(s43.INT, 5),
    SFIXED64(s43.LONG, 1),
    SINT32(s43.INT, 0),
    SINT64(s43.LONG, 0);
    
    @DexIgnore
    public /* final */ s43 zzs;
    @DexIgnore
    public /* final */ int zzt;

    @DexIgnore
    public l43(s43 s43, int i) {
        this.zzs = s43;
        this.zzt = i;
    }

    @DexIgnore
    public final s43 zza() {
        return this.zzs;
    }

    @DexIgnore
    public final int zzb() {
        return this.zzt;
    }
}
