package com.fossil;

import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class en7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "kotlin.collections.SlidingWindowKt$windowedIterator$1", f = "SlidingWindow.kt", l = {34, 40, 49, 55, 58}, m = "invokeSuspend")
    public static final class a extends jo7 implements vp7<vs7<? super List<? extends T>>, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator $iterator;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $partialWindows;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $reuseBuffer;
        @DexIgnore
        public /* final */ /* synthetic */ int $size;
        @DexIgnore
        public /* final */ /* synthetic */ int $step;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public int I$2;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public vs7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(int i, int i2, Iterator it, boolean z, boolean z2, qn7 qn7) {
            super(2, qn7);
            this.$size = i;
            this.$step = i2;
            this.$iterator = it;
            this.$reuseBuffer = z;
            this.$partialWindows = z2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.$size, this.$step, this.$iterator, this.$reuseBuffer, this.$partialWindows, qn7);
            aVar.p$ = (vs7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(Object obj, qn7<? super tl7> qn7) {
            return ((a) create(obj, qn7)).invokeSuspend(tl7.f3441a);
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
            	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
            	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
            */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x004e  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0086  */
        /* JADX WARNING: Removed duplicated region for block: B:70:0x017d  */
        /* JADX WARNING: Removed duplicated region for block: B:81:0x0179 A[SYNTHETIC] */
        @Override // com.fossil.zn7
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 435
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.en7.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public static final void a(int i, int i2) {
        String str;
        if (!(i > 0 && i2 > 0)) {
            if (i != i2) {
                str = "Both size " + i + " and step " + i2 + " must be greater than zero.";
            } else {
                str = "size " + i + " must be greater than zero.";
            }
            throw new IllegalArgumentException(str.toString());
        }
    }

    @DexIgnore
    public static final <T> Iterator<List<T>> b(Iterator<? extends T> it, int i, int i2, boolean z, boolean z2) {
        pq7.c(it, "iterator");
        return !it.hasNext() ? qm7.b : ws7.a(new a(i, i2, it, z2, z, null));
    }
}
