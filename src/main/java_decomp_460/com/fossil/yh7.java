package com.fossil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yh7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ExecutorService f4319a;

    @DexIgnore
    public yh7() {
        this.f4319a = null;
        this.f4319a = Executors.newSingleThreadExecutor();
    }

    @DexIgnore
    public void a(Runnable runnable) {
        this.f4319a.execute(runnable);
    }
}
