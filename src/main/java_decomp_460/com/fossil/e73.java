package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e73 implements b73 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f890a;
    @DexIgnore
    public static /* final */ xv2<Boolean> b;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f890a = hw2.d("measurement.collection.efficient_engagement_reporting_enabled_2", true);
        b = hw2.d("measurement.collection.redundant_engagement_removal_enabled", false);
        hw2.b("measurement.id.collection.redundant_engagement_removal_enabled", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.b73
    public final boolean zza() {
        return f890a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.b73
    public final boolean zzb() {
        return b.o().booleanValue();
    }
}
