package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.textfield.TextInputLayout;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t04 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3345a;
    @DexIgnore
    public /* final */ TextInputLayout b;
    @DexIgnore
    public LinearLayout c;
    @DexIgnore
    public int d;
    @DexIgnore
    public FrameLayout e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Animator g;
    @DexIgnore
    public /* final */ float h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public CharSequence k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public TextView m;
    @DexIgnore
    public int n;
    @DexIgnore
    public ColorStateList o;
    @DexIgnore
    public CharSequence p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public TextView r;
    @DexIgnore
    public int s;
    @DexIgnore
    public ColorStateList t;
    @DexIgnore
    public Typeface u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ int f3346a;
        @DexIgnore
        public /* final */ /* synthetic */ TextView b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ TextView d;

        @DexIgnore
        public a(int i, TextView textView, int i2, TextView textView2) {
            this.f3346a = i;
            this.b = textView;
            this.c = i2;
            this.d = textView2;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            t04.this.i = this.f3346a;
            t04.this.g = null;
            TextView textView = this.b;
            if (textView != null) {
                textView.setVisibility(4);
                if (this.c == 1 && t04.this.m != null) {
                    t04.this.m.setText((CharSequence) null);
                }
                TextView textView2 = this.d;
                if (textView2 != null) {
                    textView2.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    this.d.setAlpha(1.0f);
                }
            }
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            TextView textView = this.d;
            if (textView != null) {
                textView.setVisibility(0);
            }
        }
    }

    @DexIgnore
    public t04(TextInputLayout textInputLayout) {
        Context context = textInputLayout.getContext();
        this.f3345a = context;
        this.b = textInputLayout;
        this.h = (float) context.getResources().getDimensionPixelSize(lw3.design_textinput_caption_translate_y);
    }

    @DexIgnore
    public void A(int i2) {
        this.n = i2;
        TextView textView = this.m;
        if (textView != null) {
            this.b.S(textView, i2);
        }
    }

    @DexIgnore
    public void B(ColorStateList colorStateList) {
        this.o = colorStateList;
        TextView textView = this.m;
        if (textView != null && colorStateList != null) {
            textView.setTextColor(colorStateList);
        }
    }

    @DexIgnore
    public void C(int i2) {
        this.s = i2;
        TextView textView = this.r;
        if (textView != null) {
            kp0.q(textView, i2);
        }
    }

    @DexIgnore
    public void D(boolean z) {
        if (this.q != z) {
            g();
            if (z) {
                AppCompatTextView appCompatTextView = new AppCompatTextView(this.f3345a);
                this.r = appCompatTextView;
                appCompatTextView.setId(nw3.textinput_helper_text);
                Typeface typeface = this.u;
                if (typeface != null) {
                    this.r.setTypeface(typeface);
                }
                this.r.setVisibility(4);
                mo0.n0(this.r, 1);
                C(this.s);
                E(this.t);
                d(this.r, 1);
            } else {
                s();
                x(this.r, 1);
                this.r = null;
                this.b.a0();
                this.b.g0();
            }
            this.q = z;
        }
    }

    @DexIgnore
    public void E(ColorStateList colorStateList) {
        this.t = colorStateList;
        TextView textView = this.r;
        if (textView != null && colorStateList != null) {
            textView.setTextColor(colorStateList);
        }
    }

    @DexIgnore
    public final void F(TextView textView, Typeface typeface) {
        if (textView != null) {
            textView.setTypeface(typeface);
        }
    }

    @DexIgnore
    public void G(Typeface typeface) {
        if (typeface != this.u) {
            this.u = typeface;
            F(this.m, typeface);
            F(this.r, typeface);
        }
    }

    @DexIgnore
    public final void H(ViewGroup viewGroup, int i2) {
        if (i2 == 0) {
            viewGroup.setVisibility(8);
        }
    }

    @DexIgnore
    public final boolean I(TextView textView, CharSequence charSequence) {
        return mo0.Q(this.b) && this.b.isEnabled() && (this.j != this.i || textView == null || !TextUtils.equals(textView.getText(), charSequence));
    }

    @DexIgnore
    public void J(CharSequence charSequence) {
        g();
        this.k = charSequence;
        this.m.setText(charSequence);
        if (this.i != 1) {
            this.j = 1;
        }
        L(this.i, this.j, I(this.m, charSequence));
    }

    @DexIgnore
    public void K(CharSequence charSequence) {
        g();
        this.p = charSequence;
        this.r.setText(charSequence);
        if (this.i != 2) {
            this.j = 2;
        }
        L(this.i, this.j, I(this.r, charSequence));
    }

    @DexIgnore
    public final void L(int i2, int i3, boolean z) {
        if (z) {
            AnimatorSet animatorSet = new AnimatorSet();
            this.g = animatorSet;
            ArrayList arrayList = new ArrayList();
            h(arrayList, this.q, this.r, 2, i2, i3);
            h(arrayList, this.l, this.m, 1, i2, i3);
            vw3.a(animatorSet, arrayList);
            animatorSet.addListener(new a(i3, l(i2), i2, l(i3)));
            animatorSet.start();
        } else {
            y(i2, i3);
        }
        this.b.a0();
        this.b.e0(z);
        this.b.g0();
    }

    @DexIgnore
    public void d(TextView textView, int i2) {
        if (this.c == null && this.e == null) {
            LinearLayout linearLayout = new LinearLayout(this.f3345a);
            this.c = linearLayout;
            linearLayout.setOrientation(0);
            this.b.addView(this.c, -1, -2);
            FrameLayout frameLayout = new FrameLayout(this.f3345a);
            this.e = frameLayout;
            this.c.addView(frameLayout, -1, new FrameLayout.LayoutParams(-2, -2));
            this.c.addView(new Space(this.f3345a), new LinearLayout.LayoutParams(0, 0, 1.0f));
            if (this.b.getEditText() != null) {
                e();
            }
        }
        if (u(i2)) {
            this.e.setVisibility(0);
            this.e.addView(textView);
            this.f++;
        } else {
            this.c.addView(textView, i2);
        }
        this.c.setVisibility(0);
        this.d++;
    }

    @DexIgnore
    public void e() {
        if (f()) {
            mo0.A0(this.c, mo0.E(this.b.getEditText()), 0, mo0.D(this.b.getEditText()), 0);
        }
    }

    @DexIgnore
    public final boolean f() {
        return (this.c == null || this.b.getEditText() == null) ? false : true;
    }

    @DexIgnore
    public void g() {
        Animator animator = this.g;
        if (animator != null) {
            animator.cancel();
        }
    }

    @DexIgnore
    public final void h(List<Animator> list, boolean z, TextView textView, int i2, int i3, int i4) {
        if (textView != null && z) {
            if (i2 == i4 || i2 == i3) {
                list.add(i(textView, i4 == i2));
                if (i4 == i2) {
                    list.add(j(textView));
                }
            }
        }
    }

    @DexIgnore
    public final ObjectAnimator i(TextView textView, boolean z) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(textView, View.ALPHA, z ? 1.0f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        ofFloat.setDuration(167L);
        ofFloat.setInterpolator(uw3.f3651a);
        return ofFloat;
    }

    @DexIgnore
    public final ObjectAnimator j(TextView textView) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(textView, View.TRANSLATION_Y, -this.h, 0.0f);
        ofFloat.setDuration(217L);
        ofFloat.setInterpolator(uw3.d);
        return ofFloat;
    }

    @DexIgnore
    public boolean k() {
        return t(this.j);
    }

    @DexIgnore
    public final TextView l(int i2) {
        if (i2 == 1) {
            return this.m;
        }
        if (i2 != 2) {
            return null;
        }
        return this.r;
    }

    @DexIgnore
    public CharSequence m() {
        return this.k;
    }

    @DexIgnore
    public int n() {
        TextView textView = this.m;
        if (textView != null) {
            return textView.getCurrentTextColor();
        }
        return -1;
    }

    @DexIgnore
    public ColorStateList o() {
        TextView textView = this.m;
        if (textView != null) {
            return textView.getTextColors();
        }
        return null;
    }

    @DexIgnore
    public CharSequence p() {
        return this.p;
    }

    @DexIgnore
    public int q() {
        TextView textView = this.r;
        if (textView != null) {
            return textView.getCurrentTextColor();
        }
        return -1;
    }

    @DexIgnore
    public void r() {
        this.k = null;
        g();
        if (this.i == 1) {
            if (!this.q || TextUtils.isEmpty(this.p)) {
                this.j = 0;
            } else {
                this.j = 2;
            }
        }
        L(this.i, this.j, I(this.m, null));
    }

    @DexIgnore
    public void s() {
        g();
        if (this.i == 2) {
            this.j = 0;
        }
        L(this.i, this.j, I(this.r, null));
    }

    @DexIgnore
    public final boolean t(int i2) {
        return i2 == 1 && this.m != null && !TextUtils.isEmpty(this.k);
    }

    @DexIgnore
    public boolean u(int i2) {
        return i2 == 0 || i2 == 1;
    }

    @DexIgnore
    public boolean v() {
        return this.l;
    }

    @DexIgnore
    public boolean w() {
        return this.q;
    }

    @DexIgnore
    public void x(TextView textView, int i2) {
        FrameLayout frameLayout;
        if (this.c != null) {
            if (!u(i2) || (frameLayout = this.e) == null) {
                this.c.removeView(textView);
            } else {
                int i3 = this.f - 1;
                this.f = i3;
                H(frameLayout, i3);
                this.e.removeView(textView);
            }
            int i4 = this.d - 1;
            this.d = i4;
            H(this.c, i4);
        }
    }

    @DexIgnore
    public final void y(int i2, int i3) {
        TextView l2;
        TextView l3;
        if (i2 != i3) {
            if (!(i3 == 0 || (l3 = l(i3)) == null)) {
                l3.setVisibility(0);
                l3.setAlpha(1.0f);
            }
            if (!(i2 == 0 || (l2 = l(i2)) == null)) {
                l2.setVisibility(4);
                if (i2 == 1) {
                    l2.setText((CharSequence) null);
                }
            }
            this.i = i3;
        }
    }

    @DexIgnore
    public void z(boolean z) {
        if (this.l != z) {
            g();
            if (z) {
                AppCompatTextView appCompatTextView = new AppCompatTextView(this.f3345a);
                this.m = appCompatTextView;
                appCompatTextView.setId(nw3.textinput_error);
                Typeface typeface = this.u;
                if (typeface != null) {
                    this.m.setTypeface(typeface);
                }
                A(this.n);
                B(this.o);
                this.m.setVisibility(4);
                mo0.n0(this.m, 1);
                d(this.m, 0);
            } else {
                r();
                x(this.m, 0);
                this.m = null;
                this.b.a0();
                this.b.g0();
            }
            this.l = z;
        }
    }
}
