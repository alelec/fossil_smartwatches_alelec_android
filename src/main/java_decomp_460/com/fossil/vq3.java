package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vq3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ef2 f3806a;
    @DexIgnore
    public long b;

    @DexIgnore
    public vq3(ef2 ef2) {
        rc2.k(ef2);
        this.f3806a = ef2;
    }

    @DexIgnore
    public final void a() {
        this.b = this.f3806a.c();
    }

    @DexIgnore
    public final boolean b(long j) {
        return this.b == 0 || this.f3806a.c() - this.b >= 3600000;
    }

    @DexIgnore
    public final void c() {
        this.b = 0;
    }
}
