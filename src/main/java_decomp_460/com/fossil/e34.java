package com.fossil;

import com.google.j2objc.annotations.Weak;
import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e34<K, V> extends u24<V> {
    @DexIgnore
    @Weak
    public /* final */ a34<K, V> map;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends h54<V> {
        @DexIgnore
        public /* final */ h54<Map.Entry<K, V>> b; // = e34.this.map.entrySet().iterator();

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public V next() {
            return this.b.next().getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends s24<V> {
        @DexIgnore
        public /* final */ /* synthetic */ y24 val$entryList;

        @DexIgnore
        public b(y24 y24) {
            this.val$entryList = y24;
        }

        @DexIgnore
        @Override // com.fossil.s24
        public u24<V> delegateCollection() {
            return e34.this;
        }

        @DexIgnore
        @Override // java.util.List
        public V get(int i) {
            return (V) ((Map.Entry) this.val$entryList.get(i)).getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ a34<?, V> map;

        @DexIgnore
        public c(a34<?, V> a34) {
            this.map = a34;
        }

        @DexIgnore
        public Object readResolve() {
            return this.map.values();
        }
    }

    @DexIgnore
    public e34(a34<K, V> a34) {
        this.map = a34;
    }

    @DexIgnore
    @Override // com.fossil.u24
    public y24<V> asList() {
        return new b(this.map.entrySet().asList());
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean contains(Object obj) {
        return obj != null && p34.f(iterator(), obj);
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean isPartialView() {
        return true;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.u24, com.fossil.u24, java.lang.Iterable
    public h54<V> iterator() {
        return new a();
    }

    @DexIgnore
    public int size() {
        return this.map.size();
    }

    @DexIgnore
    @Override // com.fossil.u24
    public Object writeReplace() {
        return new c(this.map);
    }
}
