package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class yp extends Enum<yp> {
    @DexIgnore
    public static /* final */ yp A;
    @DexIgnore
    public static /* final */ yp A0;
    @DexIgnore
    public static /* final */ yp B;
    @DexIgnore
    public static /* final */ yp B0;
    @DexIgnore
    public static /* final */ yp C;
    @DexIgnore
    public static /* final */ yp C0;
    @DexIgnore
    public static /* final */ yp D;
    @DexIgnore
    public static /* final */ yp D0;
    @DexIgnore
    public static /* final */ yp E;
    @DexIgnore
    public static /* final */ yp E0;
    @DexIgnore
    public static /* final */ yp F;
    @DexIgnore
    public static /* final */ yp F0;
    @DexIgnore
    public static /* final */ yp G;
    @DexIgnore
    public static /* final */ yp G0;
    @DexIgnore
    public static /* final */ yp H;
    @DexIgnore
    public static /* final */ yp H0;
    @DexIgnore
    public static /* final */ yp I;
    @DexIgnore
    public static /* final */ yp I0;
    @DexIgnore
    public static /* final */ yp J;
    @DexIgnore
    public static /* final */ yp J0;
    @DexIgnore
    public static /* final */ yp K;
    @DexIgnore
    public static /* final */ yp K0;
    @DexIgnore
    public static /* final */ yp L;
    @DexIgnore
    public static /* final */ yp L0;
    @DexIgnore
    public static /* final */ yp M;
    @DexIgnore
    public static /* final */ yp M0;
    @DexIgnore
    public static /* final */ yp N;
    @DexIgnore
    public static /* final */ yp N0;
    @DexIgnore
    public static /* final */ yp O;
    @DexIgnore
    public static /* final */ /* synthetic */ yp[] O0;
    @DexIgnore
    public static /* final */ yp P;
    @DexIgnore
    public static /* final */ yp Q;
    @DexIgnore
    public static /* final */ yp R;
    @DexIgnore
    public static /* final */ yp S;
    @DexIgnore
    public static /* final */ yp T;
    @DexIgnore
    public static /* final */ yp U;
    @DexIgnore
    public static /* final */ yp V;
    @DexIgnore
    public static /* final */ yp W;
    @DexIgnore
    public static /* final */ yp X;
    @DexIgnore
    public static /* final */ yp Y;
    @DexIgnore
    public static /* final */ yp Z;
    @DexIgnore
    public static /* final */ yp a0;
    @DexIgnore
    public static /* final */ yp b;
    @DexIgnore
    public static /* final */ yp b0;
    @DexIgnore
    public static /* final */ yp c;
    @DexIgnore
    public static /* final */ yp c0;
    @DexIgnore
    public static /* final */ yp d;
    @DexIgnore
    public static /* final */ yp d0;
    @DexIgnore
    public static /* final */ yp e;
    @DexIgnore
    public static /* final */ yp e0;
    @DexIgnore
    public static /* final */ yp f;
    @DexIgnore
    public static /* final */ yp f0;
    @DexIgnore
    public static /* final */ yp g;
    @DexIgnore
    public static /* final */ yp g0;
    @DexIgnore
    public static /* final */ yp h;
    @DexIgnore
    public static /* final */ yp h0;
    @DexIgnore
    public static /* final */ yp i;
    @DexIgnore
    public static /* final */ yp i0;
    @DexIgnore
    public static /* final */ yp j;
    @DexIgnore
    public static /* final */ yp j0;
    @DexIgnore
    public static /* final */ yp k;
    @DexIgnore
    public static /* final */ yp k0;
    @DexIgnore
    public static /* final */ yp l;
    @DexIgnore
    public static /* final */ yp l0;
    @DexIgnore
    public static /* final */ yp m;
    @DexIgnore
    public static /* final */ yp m0;
    @DexIgnore
    public static /* final */ yp n;
    @DexIgnore
    public static /* final */ yp n0;
    @DexIgnore
    public static /* final */ yp o;
    @DexIgnore
    public static /* final */ yp o0;
    @DexIgnore
    public static /* final */ yp p;
    @DexIgnore
    public static /* final */ yp p0;
    @DexIgnore
    public static /* final */ yp q;
    @DexIgnore
    public static /* final */ yp q0;
    @DexIgnore
    public static /* final */ yp r;
    @DexIgnore
    public static /* final */ yp r0;
    @DexIgnore
    public static /* final */ yp s;
    @DexIgnore
    public static /* final */ yp s0;
    @DexIgnore
    public static /* final */ yp t;
    @DexIgnore
    public static /* final */ yp t0;
    @DexIgnore
    public static /* final */ yp u;
    @DexIgnore
    public static /* final */ yp u0;
    @DexIgnore
    public static /* final */ yp v;
    @DexIgnore
    public static /* final */ yp v0;
    @DexIgnore
    public static /* final */ yp w;
    @DexIgnore
    public static /* final */ yp w0;
    @DexIgnore
    public static /* final */ yp x;
    @DexIgnore
    public static /* final */ yp x0;
    @DexIgnore
    public static /* final */ yp y;
    @DexIgnore
    public static /* final */ yp y0;
    @DexIgnore
    public static /* final */ yp z;
    @DexIgnore
    public static /* final */ yp z0;

    /*
    static {
        yp ypVar = new yp("UNKNOWN", 0);
        b = ypVar;
        yp ypVar2 = new yp("MAKE_DEVICE_READY", 1);
        c = ypVar2;
        yp ypVar3 = new yp("READ_DEVICE_INFO_FILE", 2);
        d = ypVar3;
        yp ypVar4 = new yp("READ_DEVICE_INFO_CHARACTERISTICS", 3);
        e = ypVar4;
        yp ypVar5 = new yp("READ_RSSI", 4);
        f = ypVar5;
        yp ypVar6 = new yp("STREAMING", 5);
        g = ypVar6;
        yp ypVar7 = new yp("OTA", 6);
        h = ypVar7;
        yp ypVar8 = new yp("DISCONNECT", 7);
        i = ypVar8;
        yp ypVar9 = new yp("CLOSE", 8);
        yp ypVar10 = new yp("PUT_FILE", 9);
        j = ypVar10;
        yp ypVar11 = new yp("SET_CONNECTION_PRIORITY", 10);
        yp ypVar12 = new yp("SET_CONNECTION_PARAMS", 11);
        k = ypVar12;
        yp ypVar13 = new yp("GET_CONNECTION_PARAMS", 12);
        yp ypVar14 = new yp("PLAY_ANIMATION", 13);
        l = ypVar14;
        yp ypVar15 = new yp("GET_FILE", 14);
        m = ypVar15;
        yp ypVar16 = new yp("SYNC", 15);
        n = ypVar16;
        yp ypVar17 = new yp("GET_HARDWARE_LOG", 16);
        o = ypVar17;
        yp ypVar18 = new yp("SET_ALARMS", 17);
        p = ypVar18;
        yp ypVar19 = new yp("GET_ALARMS", 18);
        q = ypVar19;
        yp ypVar20 = new yp("REQUEST_HANDS", 19);
        r = ypVar20;
        yp ypVar21 = new yp("RELEASE_HANDS", 20);
        s = ypVar21;
        yp ypVar22 = new yp("MOVE_HANDS", 21);
        t = ypVar22;
        yp ypVar23 = new yp("SET_CALIBRATION_POSITION", 22);
        u = ypVar23;
        yp ypVar24 = new yp("SET_DEVICE_CONFIGS", 23);
        v = ypVar24;
        yp ypVar25 = new yp("GET_DEVICE_CONFIGS", 24);
        w = ypVar25;
        yp ypVar26 = new yp("PUT_JSON_OBJECT", 25);
        yp ypVar27 = new yp("SET_COMPLICATION", 26);
        x = ypVar27;
        yp ypVar28 = new yp("SET_COMPLICATION_CONFIG", 27);
        yp ypVar29 = new yp("SET_WATCH_APP", 28);
        y = ypVar29;
        yp ypVar30 = new yp("SEND_DEVICE_RESPONSE", 29);
        yp ypVar31 = new yp("FIND_THE_FASTEST_CONNECTION_INTERVAL", 30);
        yp ypVar32 = new yp("FIND_MAXIMUM_CONNECTION_INTERVAL", 31);
        yp ypVar33 = new yp("FIND_OPTIMAL_CONNECTION_PARAMETERS", 32);
        yp ypVar34 = new yp("SEND_APP_NOTIFICATION", 33);
        z = ypVar34;
        yp ypVar35 = new yp("SEND_TRACK_INFO", 34);
        A = ypVar35;
        yp ypVar36 = new yp("NOTIFY_MUSIC_EVENT", 35);
        B = ypVar36;
        yp ypVar37 = new yp("ERASE_DATA", 36);
        yp ypVar38 = new yp("SET_FRONT_LIGHT_ENABLE", 37);
        C = ypVar38;
        yp ypVar39 = new yp("SET_BACKGROUND_IMAGE", 38);
        D = ypVar39;
        yp ypVar40 = new yp("GET_BACKGROUND_IMAGE", 39);
        E = ypVar40;
        yp ypVar41 = new yp("PUT_BACKGROUND_IMAGE_DATA", 40);
        F = ypVar41;
        yp ypVar42 = new yp("PUT_BACKGROUND_IMAGE_CONFIG", 41);
        G = ypVar42;
        yp ypVar43 = new yp("GET_NOTIFICATION_FILTER", 42);
        H = ypVar43;
        yp ypVar44 = new yp("SET_NOTIFICATION_FILTER", 43);
        I = ypVar44;
        yp ypVar45 = new yp("GET_CURRENT_WORKOUT_SESSION", 44);
        J = ypVar45;
        yp ypVar46 = new yp("STOP_CURRENT_WORKOUT_SESSION", 45);
        K = ypVar46;
        yp ypVar47 = new yp("CLEAN_UP_DEVICE", 46);
        L = ypVar47;
        yp ypVar48 = new yp("GET_HEARTBEAT_STATISTIC", 47);
        yp ypVar49 = new yp("GET_HEARTBEAT_INTERVAL", 48);
        yp ypVar50 = new yp("SET_HEARTBEAT_INTERVAL", 49);
        yp ypVar51 = new yp("SEND_ASYNC_EVENT_ACK", 50);
        M = ypVar51;
        yp ypVar52 = new yp("FIND_CORRECT_OFFSET", 51);
        N = ypVar52;
        yp ypVar53 = new yp("SYNC_IN_BACKGROUND", 52);
        O = ypVar53;
        yp ypVar54 = new yp("GET_HARDWARE_LOG_IN_BACKGROUND", 53);
        P = ypVar54;
        yp ypVar55 = new yp("SEND_BACKGROUND_SYNC_ACK", 54);
        yp ypVar56 = new yp("START_AUTHENTICATION", 55);
        Q = ypVar56;
        yp ypVar57 = new yp("EXCHANGE_SECRET_KEY", 56);
        R = ypVar57;
        yp ypVar58 = new yp("AUTHENTICATE", 57);
        S = ypVar58;
        yp ypVar59 = new yp("TRY_SET_CONNECTION_PARAMS", 58);
        T = ypVar59;
        yp ypVar60 = new yp("VERIFY_SECRET_KEY", 59);
        U = ypVar60;
        yp ypVar61 = new yp("SEND_DEVICE_DATA", 60);
        V = ypVar61;
        yp ypVar62 = new yp("PUT_NOTIFICATION_ICON", 61);
        W = ypVar62;
        yp ypVar63 = new yp("PUT_NOTIFICATION_FILTER_RULE", 62);
        X = ypVar63;
        yp ypVar64 = new yp("GET_NOTIFICATION_ICON", 63);
        Y = ypVar64;
        yp ypVar65 = new yp("GET_NOTIFICATION_FILTER_RULE", 64);
        Z = ypVar65;
        yp ypVar66 = new yp("SET_DEVICE_PRESET", 65);
        a0 = ypVar66;
        yp ypVar67 = new yp("PUT_PRESET_CONFIG", 66);
        b0 = ypVar67;
        yp ypVar68 = new yp("GET_DEVICE_PRESET", 67);
        c0 = ypVar68;
        yp ypVar69 = new yp("GET_PRESET_CONFIG", 68);
        d0 = ypVar69;
        yp ypVar70 = new yp("GET_CURRENT_PRESET", 69);
        e0 = ypVar70;
        yp ypVar71 = new yp("PUT_LOCALIZATION_FILE", 70);
        f0 = ypVar71;
        yp ypVar72 = new yp("CREATE_BOND", 71);
        g0 = ypVar72;
        yp ypVar73 = new yp("CONFIGURE_MICRO_APP", 72);
        h0 = ypVar73;
        yp ypVar74 = new yp("CONNECT_HID", 73);
        i0 = ypVar74;
        yp ypVar75 = new yp("DISCONNECT_HID", 74);
        j0 = ypVar75;
        yp ypVar76 = new yp("PUT_WATCH_PARAMETERS_FILE", 75);
        k0 = ypVar76;
        yp ypVar77 = new yp("TROUBLESHOOT_DEVICE_BLE", 76);
        yp ypVar78 = new yp("LEGACY_OTA", 77);
        l0 = ypVar78;
        yp ypVar79 = new yp("LEGACY_SYNC", 78);
        m0 = ypVar79;
        yp ypVar80 = new yp("SEND_CUSTOM_COMMAND", 79);
        n0 = ypVar80;
        yp ypVar81 = new yp("SYNC_FLOW", 80);
        o0 = ypVar81;
        yp ypVar82 = new yp("NOTIFY_APP_NOTIFICATION_EVENT", 81);
        p0 = ypVar82;
        yp ypVar83 = new yp("GET_DATA_COLLECTION_FILE", 82);
        q0 = ypVar83;
        yp ypVar84 = new yp("GET_DATA_COLLECTION_FILE_IN_BACKGROUND", 83);
        r0 = ypVar84;
        yp ypVar85 = new yp("CDG_GET_BATTERY", 84);
        s0 = ypVar85;
        yp ypVar86 = new yp("CDG_GET_AVERAGE_RSSI", 85);
        t0 = ypVar86;
        yp ypVar87 = new yp("CDG_VIBE", 86);
        u0 = ypVar87;
        yp ypVar88 = new yp("CDG_SUBSCRIBE_HEART_RATE_CHARACTERISTIC", 87);
        yp ypVar89 = new yp("CDG_UNSUBSCRIBE_HEART_RATE_CHARACTERISTIC", 88);
        yp ypVar90 = new yp("CDG_START_STREAMING_ACCEL", 89);
        v0 = ypVar90;
        yp ypVar91 = new yp("CDG_ABORT_STREAMING_ACCEL", 90);
        w0 = ypVar91;
        yp ypVar92 = new yp("CDG_GET_CHARGING_STATUS", 91);
        yp ypVar93 = new yp("CDG_SUBSCRIBE_ASYNC_CHARACTERISTIC", 92);
        yp ypVar94 = new yp("CDG_UNSUBSCRIBE_ASYNC_CHARACTERISTIC", 93);
        yp ypVar95 = new yp("GET_INSTALLED_UI_PACKAGE", 94);
        x0 = ypVar95;
        yp ypVar96 = new yp("INSTALL_UI_PACKAGE", 95);
        y0 = ypVar96;
        yp ypVar97 = new yp("UNINSTALL_UI_PACKAGE", 96);
        z0 = ypVar97;
        yp ypVar98 = new yp("FETCH_DEVICE_INFORMATION", 97);
        A0 = ypVar98;
        yp ypVar99 = new yp("CONFIRM_AUTHORIZATION", 98);
        B0 = ypVar99;
        yp ypVar100 = new yp("GET_PDK_CONFIG", 99);
        yp ypVar101 = new yp("SEND_ENCRYPTED_DATA", 100);
        C0 = ypVar101;
        yp ypVar102 = new yp("SET_BUDDY_CHALLENGE_FITNESS_DATA", 101);
        yp ypVar103 = new yp("SET_REPLY_MESSAGE", 102);
        D0 = ypVar103;
        yp ypVar104 = new yp("PUT_REPLY_MESSAGE_ICON", 103);
        E0 = ypVar104;
        yp ypVar105 = new yp("PUT_REPLY_MESSAGE_DATA", 104);
        F0 = ypVar105;
        yp ypVar106 = new yp("SET_BUDDY_CHALLENGE_MINIMUM_STEP_THRESHOLD", 105);
        G0 = ypVar106;
        yp ypVar107 = new yp("SET_UP_WATCH_APPS", 106);
        H0 = ypVar107;
        yp ypVar108 = new yp("DELETE_FILE", 107);
        I0 = ypVar108;
        yp ypVar109 = new yp("SET_WORKOUT_ROUTE_IMAGE", 108);
        J0 = ypVar109;
        yp ypVar110 = new yp("PUT_WORKOUT_ROUTE_IMAGE_DATA", 109);
        K0 = ypVar110;
        yp ypVar111 = new yp("PUT_ELABEL_FILE", 110);
        L0 = ypVar111;
        yp ypVar112 = new yp("INSTALL_THEME_PACKAGE", 111);
        M0 = ypVar112;
        yp ypVar113 = new yp("SWITCH_THEME", 112);
        N0 = ypVar113;
        O0 = new yp[]{ypVar, ypVar2, ypVar3, ypVar4, ypVar5, ypVar6, ypVar7, ypVar8, ypVar9, ypVar10, ypVar11, ypVar12, ypVar13, ypVar14, ypVar15, ypVar16, ypVar17, ypVar18, ypVar19, ypVar20, ypVar21, ypVar22, ypVar23, ypVar24, ypVar25, ypVar26, ypVar27, ypVar28, ypVar29, ypVar30, ypVar31, ypVar32, ypVar33, ypVar34, ypVar35, ypVar36, ypVar37, ypVar38, ypVar39, ypVar40, ypVar41, ypVar42, ypVar43, ypVar44, ypVar45, ypVar46, ypVar47, ypVar48, ypVar49, ypVar50, ypVar51, ypVar52, ypVar53, ypVar54, ypVar55, ypVar56, ypVar57, ypVar58, ypVar59, ypVar60, ypVar61, ypVar62, ypVar63, ypVar64, ypVar65, ypVar66, ypVar67, ypVar68, ypVar69, ypVar70, ypVar71, ypVar72, ypVar73, ypVar74, ypVar75, ypVar76, ypVar77, ypVar78, ypVar79, ypVar80, ypVar81, ypVar82, ypVar83, ypVar84, ypVar85, ypVar86, ypVar87, ypVar88, ypVar89, ypVar90, ypVar91, ypVar92, ypVar93, ypVar94, ypVar95, ypVar96, ypVar97, ypVar98, ypVar99, ypVar100, ypVar101, ypVar102, ypVar103, ypVar104, ypVar105, ypVar106, ypVar107, ypVar108, ypVar109, ypVar110, ypVar111, ypVar112, ypVar113};
    }
    */

    @DexIgnore
    public yp(String str, int i2) {
    }

    @DexIgnore
    public static yp valueOf(String str) {
        return (yp) Enum.valueOf(yp.class, str);
    }

    @DexIgnore
    public static yp[] values() {
        return (yp[]) O0.clone();
    }
}
