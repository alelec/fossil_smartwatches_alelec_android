package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.pv4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nv4 extends pv5 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public g37<tc5> g;
    @DexIgnore
    public pv4 h;
    @DexIgnore
    public po4 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return nv4.k;
        }

        @DexIgnore
        public final nv4 b() {
            return new nv4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nv4 b;

        @DexIgnore
        public b(nv4 nv4) {
            this.b = nv4;
        }

        @DexIgnore
        public final void onClick(View view) {
            nv4.M6(this.b).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ nv4 f2578a;

        @DexIgnore
        public c(nv4 nv4) {
            this.f2578a = nv4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            tc5 tc5 = (tc5) nv4.K6(this.f2578a).a();
            if (tc5 != null) {
                pq7.b(bool, "it");
                if (bool.booleanValue()) {
                    ConstraintLayout constraintLayout = tc5.r;
                    pq7.b(constraintLayout, "clError");
                    constraintLayout.setVisibility(8);
                    ProgressBar progressBar = tc5.u;
                    pq7.b(progressBar, "prg");
                    progressBar.setVisibility(0);
                    return;
                }
                ProgressBar progressBar2 = tc5.u;
                pq7.b(progressBar2, "prg");
                progressBar2.setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<pv4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ nv4 f2579a;

        @DexIgnore
        public d(nv4 nv4) {
            this.f2579a = nv4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(pv4.a aVar) {
            tc5 tc5;
            if (aVar.a()) {
                tc5 tc52 = (tc5) nv4.K6(this.f2579a).a();
                if (tc52 != null) {
                    ProgressBar progressBar = tc52.u;
                    pq7.b(progressBar, "prg");
                    progressBar.setVisibility(8);
                    this.f2579a.P6();
                }
            } else if (aVar.b() && (tc5 = (tc5) nv4.K6(this.f2579a).a()) != null) {
                ProgressBar progressBar2 = tc5.u;
                pq7.b(progressBar2, "prg");
                progressBar2.setVisibility(8);
                this.f2579a.Q6();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<Integer> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ nv4 f2580a;

        @DexIgnore
        public e(nv4 nv4) {
            this.f2580a = nv4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Integer num) {
            tc5 tc5 = (tc5) nv4.K6(this.f2580a).a();
            if (tc5 != null) {
                FlexibleTextView flexibleTextView = tc5.t;
                pq7.b(flexibleTextView, "ftvError");
                flexibleTextView.setText(jl5.b.c(num, ""));
                ConstraintLayout constraintLayout = tc5.r;
                pq7.b(constraintLayout, "clError");
                constraintLayout.setVisibility(0);
            }
        }
    }

    /*
    static {
        String simpleName = nv4.class.getSimpleName();
        pq7.b(simpleName, "BCMainFragment::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 K6(nv4 nv4) {
        g37<tc5> g37 = nv4.g;
        if (g37 != null) {
            return g37;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ pv4 M6(nv4 nv4) {
        pv4 pv4 = nv4.h;
        if (pv4 != null) {
            return pv4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return k;
    }

    @DexIgnore
    public final void P6() {
        xq0 j2 = getChildFragmentManager().j();
        g37<tc5> g37 = this.g;
        if (g37 != null) {
            tc5 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "binding.get()!!");
                View n = a2.n();
                pq7.b(n, "binding.get()!!.root");
                j2.b(n.getId(), av4.s.c(), av4.s.b());
                j2.h();
                return;
            }
            pq7.i();
            throw null;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        xq0 j2 = getChildFragmentManager().j();
        g37<tc5> g37 = this.g;
        if (g37 != null) {
            tc5 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "binding.get()!!");
                View n = a2.n();
                pq7.b(n, "binding.get()!!.root");
                j2.s(n.getId(), zw4.m.b(), zw4.m.a());
                j2.h();
                return;
            }
            pq7.i();
            throw null;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final void R6() {
        g37<tc5> g37 = this.g;
        if (g37 != null) {
            tc5 a2 = g37.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new b(this));
                return;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final void S6() {
        pv4 pv4 = this.h;
        if (pv4 != null) {
            pv4.g().h(getViewLifecycleOwner(), new c(this));
            pv4 pv42 = this.h;
            if (pv42 != null) {
                pv42.i().h(getViewLifecycleOwner(), new d(this));
                pv4 pv43 = this.h;
                if (pv43 != null) {
                    pv43.h().h(getViewLifecycleOwner(), new e(this));
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == av4.s.a()) {
            Q6();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().V().a(this);
        FragmentActivity requireActivity = requireActivity();
        po4 po4 = this.i;
        if (po4 != null) {
            ts0 a2 = vs0.f(requireActivity, po4).a(pv4.class);
            pq7.b(a2, "ViewModelProviders.of(re\u2026ainViewModel::class.java)");
            this.h = (pv4) a2;
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        tc5 tc5 = (tc5) aq0.f(layoutInflater, 2131558647, viewGroup, false, A6());
        this.g = new g37<>(this, tc5);
        pq7.b(tc5, "thisBinding");
        return tc5.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        pv4 pv4 = this.h;
        if (pv4 != null) {
            pv4.d();
            R6();
            S6();
            return;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
