package com.fossil;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ge0 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends Binder implements ge0 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ge0$a$a")
        /* renamed from: com.fossil.ge0$a$a  reason: collision with other inner class name */
        public static class C0097a implements ge0 {
            @DexIgnore
            public static ge0 c;
            @DexIgnore
            public IBinder b;

            @DexIgnore
            public C0097a(IBinder iBinder) {
                this.b = iBinder;
            }

            @DexIgnore
            @Override // com.fossil.ge0
            public void T2(int i, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.os.IResultReceiver");
                    obtain.writeInt(i);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (this.b.transact(1, obtain, null, 1) || a.e() == null) {
                        obtain.recycle();
                    } else {
                        a.e().T2(i, bundle);
                    }
                } finally {
                    obtain.recycle();
                }
            }

            @DexIgnore
            public IBinder asBinder() {
                return this.b;
            }
        }

        @DexIgnore
        public a() {
            attachInterface(this, "android.support.v4.os.IResultReceiver");
        }

        @DexIgnore
        public static ge0 d(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("android.support.v4.os.IResultReceiver");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof ge0)) ? new C0097a(iBinder) : (ge0) queryLocalInterface;
        }

        @DexIgnore
        public static ge0 e() {
            return C0097a.c;
        }

        @DexIgnore
        public IBinder asBinder() {
            return this;
        }

        @DexIgnore
        @Override // android.os.Binder
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                parcel.enforceInterface("android.support.v4.os.IResultReceiver");
                T2(parcel.readInt(), parcel.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(parcel) : null);
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("android.support.v4.os.IResultReceiver");
                return true;
            }
        }
    }

    @DexIgnore
    void T2(int i, Bundle bundle) throws RemoteException;
}
