package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Shader f1774a;
    @DexIgnore
    public /* final */ ColorStateList b;
    @DexIgnore
    public int c;

    @DexIgnore
    public jl0(Shader shader, ColorStateList colorStateList, int i) {
        this.f1774a = shader;
        this.b = colorStateList;
        this.c = i;
    }

    @DexIgnore
    public static jl0 a(Resources resources, int i, Resources.Theme theme) throws IOException, XmlPullParserException {
        int next;
        XmlResourceParser xml = resources.getXml(i);
        AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
        do {
            next = xml.next();
            if (next == 2) {
                break;
            }
        } while (next != 1);
        if (next == 2) {
            String name = xml.getName();
            char c2 = '\uffff';
            int hashCode = name.hashCode();
            if (hashCode != 89650992) {
                if (hashCode == 1191572447 && name.equals("selector")) {
                    c2 = 0;
                }
            } else if (name.equals("gradient")) {
                c2 = 1;
            }
            if (c2 == 0) {
                return c(il0.b(resources, xml, asAttributeSet, theme));
            }
            if (c2 == 1) {
                return d(ll0.b(resources, xml, asAttributeSet, theme));
            }
            throw new XmlPullParserException(xml.getPositionDescription() + ": unsupported complex color tag " + name);
        }
        throw new XmlPullParserException("No start tag found");
    }

    @DexIgnore
    public static jl0 b(int i) {
        return new jl0(null, null, i);
    }

    @DexIgnore
    public static jl0 c(ColorStateList colorStateList) {
        return new jl0(null, colorStateList, colorStateList.getDefaultColor());
    }

    @DexIgnore
    public static jl0 d(Shader shader) {
        return new jl0(shader, null, 0);
    }

    @DexIgnore
    public static jl0 g(Resources resources, int i, Resources.Theme theme) {
        try {
            return a(resources, i, theme);
        } catch (Exception e) {
            Log.e("ComplexColorCompat", "Failed to inflate ComplexColor.", e);
            return null;
        }
    }

    @DexIgnore
    public int e() {
        return this.c;
    }

    @DexIgnore
    public Shader f() {
        return this.f1774a;
    }

    @DexIgnore
    public boolean h() {
        return this.f1774a != null;
    }

    @DexIgnore
    public boolean i() {
        ColorStateList colorStateList;
        return this.f1774a == null && (colorStateList = this.b) != null && colorStateList.isStateful();
    }

    @DexIgnore
    public boolean j(int[] iArr) {
        if (i()) {
            ColorStateList colorStateList = this.b;
            int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
            if (colorForState != this.c) {
                this.c = colorForState;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void k(int i) {
        this.c = i;
    }

    @DexIgnore
    public boolean l() {
        return h() || this.c != 0;
    }
}
