package com.fossil;

import com.facebook.GraphRequest;
import com.facebook.stetho.server.http.HttpHeaders;
import com.facebook.stetho.websocket.WebSocketHandler;
import com.fossil.f28;
import com.fossil.p18;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d28 implements Interceptor {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ i28 f724a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements c58 {
        @DexIgnore
        public boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ k48 c;
        @DexIgnore
        public /* final */ /* synthetic */ e28 d;
        @DexIgnore
        public /* final */ /* synthetic */ j48 e;

        @DexIgnore
        public a(d28 d28, k48 k48, e28 e28, j48 j48) {
            this.c = k48;
            this.d = e28;
            this.e = j48;
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.c58, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!this.b && !b28.p(this, 100, TimeUnit.MILLISECONDS)) {
                this.b = true;
                this.d.abort();
            }
            this.c.close();
        }

        @DexIgnore
        @Override // com.fossil.c58
        public long d0(i48 i48, long j) throws IOException {
            try {
                long d0 = this.c.d0(i48, j);
                if (d0 == -1) {
                    if (!this.b) {
                        this.b = true;
                        this.e.close();
                    }
                    return -1;
                }
                i48.C(this.e.d(), i48.p0() - d0, d0);
                this.e.x();
                return d0;
            } catch (IOException e2) {
                if (!this.b) {
                    this.b = true;
                    this.d.abort();
                }
                throw e2;
            }
        }

        @DexIgnore
        @Override // com.fossil.c58
        public d58 e() {
            return this.c.e();
        }
    }

    @DexIgnore
    public d28(i28 i28) {
        this.f724a = i28;
    }

    @DexIgnore
    public static p18 b(p18 p18, p18 p182) {
        p18.a aVar = new p18.a();
        int h = p18.h();
        for (int i = 0; i < h; i++) {
            String e = p18.e(i);
            String i2 = p18.i(i);
            if ((!"Warning".equalsIgnoreCase(e) || !i2.startsWith("1")) && (c(e) || !d(e) || p182.c(e) == null)) {
                z18.f4406a.b(aVar, e, i2);
            }
        }
        int h2 = p182.h();
        for (int i3 = 0; i3 < h2; i3++) {
            String e2 = p182.e(i3);
            if (!c(e2) && d(e2)) {
                z18.f4406a.b(aVar, e2, p182.i(i3));
            }
        }
        return aVar.e();
    }

    @DexIgnore
    public static boolean c(String str) {
        return HttpHeaders.CONTENT_LENGTH.equalsIgnoreCase(str) || GraphRequest.CONTENT_ENCODING_HEADER.equalsIgnoreCase(str) || "Content-Type".equalsIgnoreCase(str);
    }

    @DexIgnore
    public static boolean d(String str) {
        return !WebSocketHandler.HEADER_CONNECTION.equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    @DexIgnore
    public static Response e(Response response) {
        if (response == null || response.a() == null) {
            return response;
        }
        Response.a B = response.B();
        B.b(null);
        return B.c();
    }

    @DexIgnore
    public final Response a(e28 e28, Response response) throws IOException {
        a58 body;
        if (e28 == null || (body = e28.body()) == null) {
            return response;
        }
        a aVar = new a(this, response.a().source(), e28, s48.c(body));
        String j = response.j("Content-Type");
        long contentLength = response.a().contentLength();
        Response.a B = response.B();
        B.b(new x28(j, contentLength, s48.d(aVar)));
        return B.c();
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        i28 i28 = this.f724a;
        Response e = i28 != null ? i28.e(chain.c()) : null;
        f28 c = new f28.a(System.currentTimeMillis(), chain.c(), e).c();
        v18 v18 = c.f1042a;
        Response response = c.b;
        i28 i282 = this.f724a;
        if (i282 != null) {
            i282.b(c);
        }
        if (e != null && response == null) {
            b28.g(e.a());
        }
        if (v18 == null && response == null) {
            Response.a aVar = new Response.a();
            aVar.p(chain.c());
            aVar.n(t18.HTTP_1_1);
            aVar.g(504);
            aVar.k("Unsatisfiable Request (only-if-cached)");
            aVar.b(b28.c);
            aVar.q(-1);
            aVar.o(System.currentTimeMillis());
            return aVar.c();
        } else if (v18 == null) {
            Response.a B = response.B();
            B.d(e(response));
            return B.c();
        } else {
            try {
                Response d = chain.d(v18);
                if (d == null && e != null) {
                }
                if (response != null) {
                    if (d.f() == 304) {
                        Response.a B2 = response.B();
                        B2.j(b(response.l(), d.l()));
                        B2.q(d.L());
                        B2.o(d.F());
                        B2.d(e(response));
                        B2.l(e(d));
                        Response c2 = B2.c();
                        d.a().close();
                        this.f724a.a();
                        this.f724a.f(response, c2);
                        return c2;
                    }
                    b28.g(response.a());
                }
                Response.a B3 = d.B();
                B3.d(e(response));
                B3.l(e(d));
                Response c3 = B3.c();
                if (this.f724a == null) {
                    return c3;
                }
                if (u28.c(c3) && f28.a(c3, v18)) {
                    return a(this.f724a.d(c3), c3);
                }
                if (!v28.a(v18.g())) {
                    return c3;
                }
                try {
                    this.f724a.c(v18);
                    return c3;
                } catch (IOException e2) {
                    return c3;
                }
            } finally {
                if (e != null) {
                    b28.g(e.a());
                }
            }
        }
    }
}
