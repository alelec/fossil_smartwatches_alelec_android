package com.fossil;

import android.content.Intent;
import com.fossil.iq4;
import com.fossil.wq5;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ib6 extends iq4<b, c, a> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public b e;
    @DexIgnore
    public /* final */ d f; // = new d();
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ HybridPresetRepository h;
    @DexIgnore
    public /* final */ MicroAppRepository i;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1604a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public a(int i, ArrayList<Integer> arrayList) {
            pq7.c(arrayList, "mBLEErrorCodes");
            this.f1604a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.f1604a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ HybridPreset f1605a;

        @DexIgnore
        public b(HybridPreset hybridPreset) {
            pq7.c(hybridPreset, "mPreset");
            this.f1605a = hybridPreset;
        }

        @DexIgnore
        public final HybridPreset a() {
            return this.f1605a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements wq5.b {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            if (ib6.this.q()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetHybridPresetToWatchUseCase", "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
                if (communicateMode == CommunicateMode.SET_LINK_MAPPING) {
                    ib6.this.u(false);
                    if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                        FLogger.INSTANCE.getLocal().d("SetHybridPresetToWatchUseCase", "onReceive - success");
                        ib6.this.v();
                        return;
                    }
                    FLogger.INSTANCE.getLocal().d("SetHybridPresetToWatchUseCase", "onReceive - failed");
                    int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>(intExtra2);
                    }
                    ib6.this.i(new a(intExtra2, integerArrayListExtra));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$setPresetToDb$1", f = "SetHybridPresetToWatchUseCase.kt", l = {80}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ib6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ib6 ib6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ib6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            HybridPreset a2;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                a2 = ib6.o(this.this$0).a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetHybridPresetToWatchUseCase", "set to watch success, set preset " + a2 + " to db");
                a2.setActive(true);
                HybridPresetRepository hybridPresetRepository = this.this$0.h;
                this.L$0 = iv7;
                this.L$1 = a2;
                this.label = 1;
                if (hybridPresetRepository.upsertHybridPreset(a2, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                a2 = (HybridPreset) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "Calendar.getInstance()");
            String w0 = lk5.w0(instance.getTime());
            Iterator<HybridPresetAppSetting> it = a2.getButtons().iterator();
            while (it.hasNext()) {
                HybridPresetAppSetting next = it.next();
                if (!fj5.d(next.getSettings())) {
                    ib6 ib6 = this.this$0;
                    String appId = next.getAppId();
                    String settings = next.getSettings();
                    if (settings != null) {
                        ib6.t(appId, settings);
                        MicroAppLastSettingRepository microAppLastSettingRepository = this.this$0.j;
                        String appId2 = next.getAppId();
                        pq7.b(w0, "updatedAt");
                        String settings2 = next.getSettings();
                        if (settings2 != null) {
                            microAppLastSettingRepository.upsertMicroAppLastSetting(new MicroAppLastSetting(appId2, w0, settings2));
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
            this.this$0.j(new c());
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public ib6(DeviceRepository deviceRepository, HybridPresetRepository hybridPresetRepository, MicroAppRepository microAppRepository, MicroAppLastSettingRepository microAppLastSettingRepository) {
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(hybridPresetRepository, "mHybridPresetRepository");
        pq7.c(microAppRepository, "mMicroAppRepository");
        pq7.c(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        this.g = deviceRepository;
        this.h = hybridPresetRepository;
        this.i = microAppRepository;
        this.j = microAppLastSettingRepository;
    }

    @DexIgnore
    public static final /* synthetic */ b o(ib6 ib6) {
        b bVar = ib6.e;
        if (bVar != null) {
            return bVar;
        }
        pq7.n("mRequestValues");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "SetHybridPresetToWatchUseCase";
    }

    @DexIgnore
    public final boolean q() {
        return this.d;
    }

    @DexIgnore
    public final void r() {
        wq5.d.e(this.f, CommunicateMode.SET_LINK_MAPPING);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x007d, code lost:
        if (com.fossil.ao7.f(com.portfolio.platform.PortfolioApp.h0.c().D1(r0, r1)) != null) goto L_0x007f;
     */
    @DexIgnore
    /* renamed from: s */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.ib6.b r6, com.fossil.qn7<java.lang.Object> r7) {
        /*
            r5 = this;
            r4 = 0
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = "SetHybridPresetToWatchUseCase"
            java.lang.String r2 = "executeUseCase"
            r0.d(r1, r2)
            if (r6 == 0) goto L_0x0091
            r5.e = r6
            r0 = 1
            r5.d = r0
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            java.lang.String r0 = r0.J()
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "execute with preset "
            r2.append(r3)
            com.fossil.ib6$b r3 = r5.e
            if (r3 == 0) goto L_0x008b
            com.portfolio.platform.data.model.room.microapp.HybridPreset r3 = r3.a()
            r2.append(r3)
            java.lang.String r3 = "SetHybridPresetToWatchUseCase"
            java.lang.String r2 = r2.toString()
            r1.d(r3, r2)
            com.fossil.ib6$b r1 = r5.e
            if (r1 == 0) goto L_0x0085
            com.portfolio.platform.data.model.room.microapp.HybridPreset r1 = r1.a()
            com.portfolio.platform.data.source.DeviceRepository r2 = r5.g
            com.portfolio.platform.data.source.MicroAppRepository r3 = r5.i
            java.util.List r1 = com.fossil.kj5.k(r1, r0, r2, r3)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "set preset to watch with bleMappings "
            r3.append(r4)
            r3.append(r1)
            java.lang.String r4 = "SetHybridPresetToWatchUseCase"
            java.lang.String r3 = r3.toString()
            r2.d(r4, r3)
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            long r0 = r2.D1(r0, r1)
            java.lang.Long r0 = com.fossil.ao7.f(r0)
            if (r0 == 0) goto L_0x0091
        L_0x007f:
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            return r0
        L_0x0085:
            java.lang.String r0 = "mRequestValues"
            com.fossil.pq7.n(r0)
            throw r4
        L_0x008b:
            java.lang.String r0 = "mRequestValues"
            com.fossil.pq7.n(r0)
            throw r4
        L_0x0091:
            com.fossil.ib6$a r0 = new com.fossil.ib6$a
            r1 = -1
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r0.<init>(r1, r2)
            r5.i(r0)
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ib6.k(com.fossil.ib6$b, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void t(String str, String str2) {
        try {
            if (pq7.a(str, MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                PortfolioApp.h0.c().t1(((SecondTimezoneSetting) new Gson().k(str2, SecondTimezoneSetting.class)).getTimeZoneId());
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("SetHybridPresetToWatchUseCase", "setAutoSettingToWatch exception " + e2);
        }
    }

    @DexIgnore
    public final void u(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final xw7 v() {
        return gu7.d(g(), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final void w() {
        wq5.d.j(this.f, CommunicateMode.SET_LINK_MAPPING);
    }
}
