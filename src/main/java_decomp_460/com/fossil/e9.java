package com.fossil;

import com.fossil.vm1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class e9 extends nq7 implements rp7<byte[], vm1> {
    @DexIgnore
    public e9(vm1.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    @Override // com.fossil.gq7, com.fossil.ds7
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public final fs7 getOwner() {
        return er7.b(vm1.a.class);
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/DailyStepConfig;";
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public vm1 invoke(byte[] bArr) {
        return ((vm1.a) this.receiver).a(bArr);
    }
}
