package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ne0 {
    @DexIgnore
    public static /* final */ int abc_background_cache_hint_selector_material_dark; // = 2131099652;
    @DexIgnore
    public static /* final */ int abc_background_cache_hint_selector_material_light; // = 2131099653;
    @DexIgnore
    public static /* final */ int abc_btn_colored_borderless_text_material; // = 2131099654;
    @DexIgnore
    public static /* final */ int abc_btn_colored_text_material; // = 2131099655;
    @DexIgnore
    public static /* final */ int abc_color_highlight_material; // = 2131099656;
    @DexIgnore
    public static /* final */ int abc_decor_view_status_guard; // = 2131099657;
    @DexIgnore
    public static /* final */ int abc_decor_view_status_guard_light; // = 2131099658;
    @DexIgnore
    public static /* final */ int abc_hint_foreground_material_dark; // = 2131099659;
    @DexIgnore
    public static /* final */ int abc_hint_foreground_material_light; // = 2131099660;
    @DexIgnore
    public static /* final */ int abc_primary_text_disable_only_material_dark; // = 2131099661;
    @DexIgnore
    public static /* final */ int abc_primary_text_disable_only_material_light; // = 2131099662;
    @DexIgnore
    public static /* final */ int abc_primary_text_material_dark; // = 2131099663;
    @DexIgnore
    public static /* final */ int abc_primary_text_material_light; // = 2131099664;
    @DexIgnore
    public static /* final */ int abc_search_url_text; // = 2131099665;
    @DexIgnore
    public static /* final */ int abc_search_url_text_normal; // = 2131099666;
    @DexIgnore
    public static /* final */ int abc_search_url_text_pressed; // = 2131099667;
    @DexIgnore
    public static /* final */ int abc_search_url_text_selected; // = 2131099668;
    @DexIgnore
    public static /* final */ int abc_secondary_text_material_dark; // = 2131099669;
    @DexIgnore
    public static /* final */ int abc_secondary_text_material_light; // = 2131099670;
    @DexIgnore
    public static /* final */ int abc_tint_btn_checkable; // = 2131099671;
    @DexIgnore
    public static /* final */ int abc_tint_default; // = 2131099672;
    @DexIgnore
    public static /* final */ int abc_tint_edittext; // = 2131099673;
    @DexIgnore
    public static /* final */ int abc_tint_seek_thumb; // = 2131099674;
    @DexIgnore
    public static /* final */ int abc_tint_spinner; // = 2131099675;
    @DexIgnore
    public static /* final */ int abc_tint_switch_track; // = 2131099676;
    @DexIgnore
    public static /* final */ int accent_material_dark; // = 2131099679;
    @DexIgnore
    public static /* final */ int accent_material_light; // = 2131099680;
    @DexIgnore
    public static /* final */ int androidx_core_ripple_material_light; // = 2131099687;
    @DexIgnore
    public static /* final */ int androidx_core_secondary_text_default_material_light; // = 2131099688;
    @DexIgnore
    public static /* final */ int background_floating_material_dark; // = 2131099697;
    @DexIgnore
    public static /* final */ int background_floating_material_light; // = 2131099698;
    @DexIgnore
    public static /* final */ int background_material_dark; // = 2131099699;
    @DexIgnore
    public static /* final */ int background_material_light; // = 2131099700;
    @DexIgnore
    public static /* final */ int bright_foreground_disabled_material_dark; // = 2131099708;
    @DexIgnore
    public static /* final */ int bright_foreground_disabled_material_light; // = 2131099709;
    @DexIgnore
    public static /* final */ int bright_foreground_inverse_material_dark; // = 2131099710;
    @DexIgnore
    public static /* final */ int bright_foreground_inverse_material_light; // = 2131099711;
    @DexIgnore
    public static /* final */ int bright_foreground_material_dark; // = 2131099712;
    @DexIgnore
    public static /* final */ int bright_foreground_material_light; // = 2131099713;
    @DexIgnore
    public static /* final */ int button_material_dark; // = 2131099719;
    @DexIgnore
    public static /* final */ int button_material_light; // = 2131099720;
    @DexIgnore
    public static /* final */ int dim_foreground_disabled_material_dark; // = 2131099814;
    @DexIgnore
    public static /* final */ int dim_foreground_disabled_material_light; // = 2131099815;
    @DexIgnore
    public static /* final */ int dim_foreground_material_dark; // = 2131099816;
    @DexIgnore
    public static /* final */ int dim_foreground_material_light; // = 2131099817;
    @DexIgnore
    public static /* final */ int error_color_material_dark; // = 2131099822;
    @DexIgnore
    public static /* final */ int error_color_material_light; // = 2131099823;
    @DexIgnore
    public static /* final */ int foreground_material_dark; // = 2131099825;
    @DexIgnore
    public static /* final */ int foreground_material_light; // = 2131099826;
    @DexIgnore
    public static /* final */ int highlighted_text_material_dark; // = 2131099851;
    @DexIgnore
    public static /* final */ int highlighted_text_material_light; // = 2131099852;
    @DexIgnore
    public static /* final */ int material_blue_grey_800; // = 2131099859;
    @DexIgnore
    public static /* final */ int material_blue_grey_900; // = 2131099860;
    @DexIgnore
    public static /* final */ int material_blue_grey_950; // = 2131099861;
    @DexIgnore
    public static /* final */ int material_deep_teal_200; // = 2131099862;
    @DexIgnore
    public static /* final */ int material_deep_teal_500; // = 2131099863;
    @DexIgnore
    public static /* final */ int material_grey_100; // = 2131099864;
    @DexIgnore
    public static /* final */ int material_grey_300; // = 2131099865;
    @DexIgnore
    public static /* final */ int material_grey_50; // = 2131099866;
    @DexIgnore
    public static /* final */ int material_grey_600; // = 2131099867;
    @DexIgnore
    public static /* final */ int material_grey_800; // = 2131099868;
    @DexIgnore
    public static /* final */ int material_grey_850; // = 2131099869;
    @DexIgnore
    public static /* final */ int material_grey_900; // = 2131099870;
    @DexIgnore
    public static /* final */ int notification_action_color_filter; // = 2131099944;
    @DexIgnore
    public static /* final */ int notification_icon_bg_color; // = 2131099945;
    @DexIgnore
    public static /* final */ int primary_dark_material_dark; // = 2131099970;
    @DexIgnore
    public static /* final */ int primary_dark_material_light; // = 2131099971;
    @DexIgnore
    public static /* final */ int primary_material_dark; // = 2131099972;
    @DexIgnore
    public static /* final */ int primary_material_light; // = 2131099973;
    @DexIgnore
    public static /* final */ int primary_text_default_material_dark; // = 2131099974;
    @DexIgnore
    public static /* final */ int primary_text_default_material_light; // = 2131099975;
    @DexIgnore
    public static /* final */ int primary_text_disabled_material_dark; // = 2131099976;
    @DexIgnore
    public static /* final */ int primary_text_disabled_material_light; // = 2131099977;
    @DexIgnore
    public static /* final */ int ripple_material_dark; // = 2131100329;
    @DexIgnore
    public static /* final */ int ripple_material_light; // = 2131100330;
    @DexIgnore
    public static /* final */ int secondary_text_default_material_dark; // = 2131100332;
    @DexIgnore
    public static /* final */ int secondary_text_default_material_light; // = 2131100333;
    @DexIgnore
    public static /* final */ int secondary_text_disabled_material_dark; // = 2131100334;
    @DexIgnore
    public static /* final */ int secondary_text_disabled_material_light; // = 2131100335;
    @DexIgnore
    public static /* final */ int switch_thumb_disabled_material_dark; // = 2131100343;
    @DexIgnore
    public static /* final */ int switch_thumb_disabled_material_light; // = 2131100344;
    @DexIgnore
    public static /* final */ int switch_thumb_material_dark; // = 2131100345;
    @DexIgnore
    public static /* final */ int switch_thumb_material_light; // = 2131100346;
    @DexIgnore
    public static /* final */ int switch_thumb_normal_material_dark; // = 2131100347;
    @DexIgnore
    public static /* final */ int switch_thumb_normal_material_light; // = 2131100348;
    @DexIgnore
    public static /* final */ int tooltip_background_dark; // = 2131100351;
    @DexIgnore
    public static /* final */ int tooltip_background_light; // = 2131100352;
}
