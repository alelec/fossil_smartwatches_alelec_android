package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vx5 implements Factory<ux5> {
    @DexIgnore
    public static ux5 a(ox5 ox5, String str, ArrayList<Alarm> arrayList, Alarm alarm, yx5 yx5, bk5 bk5, xx5 xx5, UserRepository userRepository) {
        return new ux5(ox5, str, arrayList, alarm, yx5, bk5, xx5, userRepository);
    }
}
