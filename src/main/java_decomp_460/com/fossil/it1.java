package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class it1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ep1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<it1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public it1 createFromParcel(Parcel parcel) {
            fq1 fq1 = (fq1) parcel.readParcelable(fq1.class.getClassLoader());
            nt1 nt1 = (nt1) parcel.readParcelable(nt1.class.getClassLoader());
            Parcelable readParcelable = parcel.readParcelable(ep1.class.getClassLoader());
            if (readParcelable != null) {
                return new it1(fq1, nt1, (ep1) readParcelable);
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public it1[] newArray(int i) {
            return new it1[i];
        }
    }

    @DexIgnore
    public it1(ep1 ep1) {
        this(null, null, ep1);
    }

    @DexIgnore
    public it1(fq1 fq1, ep1 ep1) {
        super(fq1, null);
        this.d = ep1;
    }

    @DexIgnore
    public it1(fq1 fq1, nt1 nt1, ep1 ep1) {
        super(fq1, nt1);
        this.d = ep1;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public JSONObject a() {
        return gy1.c(super.a(), this.d.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        jq1 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("chanceOfRainSSE._.config.info", this.d.a());
        } catch (JSONException e) {
            d90.i.i(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        pq7.b(jSONObject4, "deviceResponseJSONObject.toString()");
        Charset c = hd0.y.c();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(c);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(it1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.d, ((it1) obj).d) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.ChanceOfRainComplicationData");
    }

    @DexIgnore
    public final ep1 getChanceOfRainInfo() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public int hashCode() {
        return (super.hashCode() * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }
}
