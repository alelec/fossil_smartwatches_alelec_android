package com.fossil;

import android.net.Uri;
import com.facebook.internal.Utility;
import com.fossil.af1;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mf1 implements af1<Uri, InputStream> {
    @DexIgnore
    public static /* final */ Set<String> b; // = Collections.unmodifiableSet(new HashSet(Arrays.asList("http", Utility.URL_SCHEME)));

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ af1<te1, InputStream> f2373a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements bf1<Uri, InputStream> {
        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Uri, InputStream> b(ef1 ef1) {
            return new mf1(ef1.d(te1.class, InputStream.class));
        }
    }

    @DexIgnore
    public mf1(af1<te1, InputStream> af1) {
        this.f2373a = af1;
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<InputStream> b(Uri uri, int i, int i2, ob1 ob1) {
        return this.f2373a.b(new te1(uri.toString()), i, i2, ob1);
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
