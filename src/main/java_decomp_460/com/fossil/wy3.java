package com.fossil;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wy3 extends Drawable {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ h04 f4018a; // = new h04();
    @DexIgnore
    public /* final */ Paint b;
    @DexIgnore
    public /* final */ Path c; // = new Path();
    @DexIgnore
    public /* final */ Rect d; // = new Rect();
    @DexIgnore
    public /* final */ RectF e; // = new RectF();
    @DexIgnore
    public /* final */ RectF f; // = new RectF();
    @DexIgnore
    public /* final */ b g; // = new b();
    @DexIgnore
    public float h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public boolean n; // = true;
    @DexIgnore
    public g04 o;
    @DexIgnore
    public ColorStateList p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends Drawable.ConstantState {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return 0;
        }

        @DexIgnore
        public Drawable newDrawable() {
            return wy3.this;
        }
    }

    @DexIgnore
    public wy3(g04 g04) {
        this.o = g04;
        Paint paint = new Paint(1);
        this.b = paint;
        paint.setStyle(Paint.Style.STROKE);
    }

    @DexIgnore
    public final Shader a() {
        Rect rect = this.d;
        copyBounds(rect);
        float height = this.h / ((float) rect.height());
        int e2 = pl0.e(this.i, this.m);
        int e3 = pl0.e(this.j, this.m);
        int e4 = pl0.e(pl0.h(this.j, 0), this.m);
        int e5 = pl0.e(pl0.h(this.l, 0), this.m);
        int e6 = pl0.e(this.l, this.m);
        int e7 = pl0.e(this.k, this.m);
        return new LinearGradient((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) rect.top, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) rect.bottom, new int[]{e2, e3, e4, e5, e6, e7}, new float[]{0.0f, height, 0.5f, 0.5f, 1.0f - height, 1.0f}, Shader.TileMode.CLAMP);
    }

    @DexIgnore
    public RectF b() {
        this.f.set(getBounds());
        return this.f;
    }

    @DexIgnore
    public void c(ColorStateList colorStateList) {
        if (colorStateList != null) {
            this.m = colorStateList.getColorForState(getState(), this.m);
        }
        this.p = colorStateList;
        this.n = true;
        invalidateSelf();
    }

    @DexIgnore
    public void d(float f2) {
        if (this.h != f2) {
            this.h = f2;
            this.b.setStrokeWidth(1.3333f * f2);
            this.n = true;
            invalidateSelf();
        }
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        if (this.n) {
            this.b.setShader(a());
            this.n = false;
        }
        float strokeWidth = this.b.getStrokeWidth() / 2.0f;
        copyBounds(this.d);
        this.e.set(this.d);
        float min = Math.min(this.o.r().a(b()), this.e.width() / 2.0f);
        if (this.o.u(b())) {
            this.e.inset(strokeWidth, strokeWidth);
            canvas.drawRoundRect(this.e, min, min, this.b);
        }
    }

    @DexIgnore
    public void e(int i2, int i3, int i4, int i5) {
        this.i = i2;
        this.j = i3;
        this.k = i4;
        this.l = i5;
    }

    @DexIgnore
    public void f(g04 g04) {
        this.o = g04;
        invalidateSelf();
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        return this.g;
    }

    @DexIgnore
    public int getOpacity() {
        return this.h > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? -3 : -2;
    }

    @DexIgnore
    @TargetApi(21)
    public void getOutline(Outline outline) {
        if (this.o.u(b())) {
            outline.setRoundRect(getBounds(), this.o.r().a(b()));
            return;
        }
        copyBounds(this.d);
        this.e.set(this.d);
        this.f4018a.d(this.o, 1.0f, this.e, this.c);
        if (this.c.isConvex()) {
            outline.setConvexPath(this.c);
        }
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        if (!this.o.u(b())) {
            return true;
        }
        int round = Math.round(this.h);
        rect.set(round, round, round, round);
        return true;
    }

    @DexIgnore
    public boolean isStateful() {
        ColorStateList colorStateList = this.p;
        return (colorStateList != null && colorStateList.isStateful()) || super.isStateful();
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        this.n = true;
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        int colorForState;
        ColorStateList colorStateList = this.p;
        if (!(colorStateList == null || (colorForState = colorStateList.getColorForState(iArr, this.m)) == this.m)) {
            this.n = true;
            this.m = colorForState;
        }
        if (this.n) {
            invalidateSelf();
        }
        return this.n;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        this.b.setAlpha(i2);
        invalidateSelf();
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.b.setColorFilter(colorFilter);
        invalidateSelf();
    }
}
