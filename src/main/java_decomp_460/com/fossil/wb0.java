package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class wb0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f3911a;

    /*
    static {
        int[] iArr = new int[vv1.values().length];
        f3911a = iArr;
        iArr[vv1.WEATHER.ordinal()] = 1;
        f3911a[vv1.STEPS.ordinal()] = 2;
        f3911a[vv1.DATE.ordinal()] = 3;
        f3911a[vv1.CHANCE_OF_RAIN.ordinal()] = 4;
        f3911a[vv1.SECOND_TIMEZONE.ordinal()] = 5;
        f3911a[vv1.ACTIVE_MINUTES.ordinal()] = 6;
        f3911a[vv1.CALORIES.ordinal()] = 7;
        f3911a[vv1.BATTERY.ordinal()] = 8;
        f3911a[vv1.HEART_RATE.ordinal()] = 9;
    }
    */
}
