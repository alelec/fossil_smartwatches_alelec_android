package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mw1 extends gw1 {
    @DexIgnore
    public mw1(byte[] bArr) {
        super("!preview.rle", bArr);
    }

    @DexIgnore
    @Override // com.fossil.fw1, com.fossil.fw1, com.fossil.gw1, com.fossil.gw1, com.fossil.gw1, java.lang.Object
    public mw1 clone() {
        byte[] bitmapImageData = getBitmapImageData();
        byte[] copyOf = Arrays.copyOf(bitmapImageData, bitmapImageData.length);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new mw1(copyOf);
    }
}
