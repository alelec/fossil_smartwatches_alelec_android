package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b02 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ty1 f375a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public b02(ty1 ty1, byte[] bArr) {
        if (ty1 == null) {
            throw new NullPointerException("encoding is null");
        } else if (bArr != null) {
            this.f375a = ty1;
            this.b = bArr;
        } else {
            throw new NullPointerException("bytes is null");
        }
    }

    @DexIgnore
    public byte[] a() {
        return this.b;
    }

    @DexIgnore
    public ty1 b() {
        return this.f375a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b02)) {
            return false;
        }
        b02 b02 = (b02) obj;
        if (this.f375a.equals(b02.f375a)) {
            return Arrays.equals(this.b, b02.b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.f375a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b);
    }

    @DexIgnore
    public String toString() {
        return "EncodedPayload{encoding=" + this.f375a + ", bytes=[...]}";
    }
}
