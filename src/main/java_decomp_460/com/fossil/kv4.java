package com.fossil;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kv4 extends RecyclerView.g<RecyclerView.ViewHolder> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<Object> f2101a; // = new ArrayList();
    @DexIgnore
    public zy4 b; // = new zy4(1);
    @DexIgnore
    public lv4 c;

    @DexIgnore
    public kv4(long j, int i) {
        this.c = new lv4(2, j, i);
    }

    @DexIgnore
    public final void g(List<? extends Object> list) {
        pq7.c(list, "newData");
        this.f2101a.clear();
        this.f2101a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f2101a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        if (this.b.b(this.f2101a, i)) {
            return 1;
        }
        if (this.c.a(this.f2101a, i)) {
            return 2;
        }
        throw new IllegalArgumentException("wrong type");
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        pq7.c(viewHolder, "holder");
        int itemViewType = getItemViewType(i);
        if (itemViewType == 1) {
            this.b.c(this.f2101a, i, viewHolder);
        } else if (itemViewType == 2) {
            this.c.b(this.f2101a, i, viewHolder);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        if (i == 1) {
            return this.b.d(viewGroup);
        }
        if (i == 2) {
            return this.c.c(viewGroup);
        }
        throw new IllegalArgumentException("wrong type: " + i);
    }
}
