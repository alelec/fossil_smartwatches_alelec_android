package com.fossil;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u47 extends nx3 implements DialogInterface.OnKeyListener, View.OnKeyListener {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public int e; // = 80;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public float g; // = 0.3f;
    @DexIgnore
    public DialogInterface.OnDismissListener h;
    @DexIgnore
    public HashMap i;

    /*
    static {
        String simpleName = u47.class.getSimpleName();
        pq7.b(simpleName, "BaseBottomSheetDialogFra\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.nx3, com.fossil.kq0
    public void dismiss() {
        xq0 j2;
        FLogger.INSTANCE.getLocal().d(j, "dismiss");
        FragmentManager fragmentManager = getFragmentManager();
        if (!(fragmentManager == null || (j2 = fragmentManager.j()) == null)) {
            j2.q(this);
            if (j2 != null) {
                j2.i();
            }
        }
        super.dismiss();
    }

    @DexIgnore
    public boolean isActive() {
        return isAdded();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public void onAttach(Context context) {
        pq7.c(context, "context");
        super.onAttach(context);
        Fragment parentFragment = getParentFragment();
        if (!(parentFragment instanceof DialogInterface.OnDismissListener)) {
            parentFragment = null;
        }
        DialogInterface.OnDismissListener onDismissListener = (DialogInterface.OnDismissListener) parentFragment;
        if (onDismissListener == null) {
            onDismissListener = (DialogInterface.OnDismissListener) (!(context instanceof DialogInterface.OnDismissListener) ? null : context);
        }
        this.h = onDismissListener;
    }

    @DexIgnore
    public boolean onBackPressed() {
        dismiss();
        return true;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ck5.f.g();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(j, "onDestroy");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public void onDetach() {
        super.onDetach();
        FLogger.INSTANCE.getLocal().d(j, "onDetach");
    }

    @DexIgnore
    @Override // com.fossil.kq0
    public void onDismiss(DialogInterface dialogInterface) {
        pq7.c(dialogInterface, "dialog");
        super.onDismiss(dialogInterface);
        FLogger.INSTANCE.getLocal().d(j, "onDismiss");
        DialogInterface.OnDismissListener onDismissListener = this.h;
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialogInterface);
        }
    }

    @DexIgnore
    public boolean onKey(DialogInterface dialogInterface, int i2, KeyEvent keyEvent) {
        if (keyEvent == null || keyEvent.getAction() != 1 || i2 != 4) {
            return false;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String simpleName = getClass().getSimpleName();
        pq7.b(simpleName, "javaClass.simpleName");
        local.d(simpleName, "onKey KEYCODE_BACK");
        return onBackPressed();
    }

    @DexIgnore
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent == null || keyEvent.getAction() != 1 || i2 != 4) {
            return false;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String simpleName = getClass().getSimpleName();
        pq7.b(simpleName, "javaClass.simpleName");
        local.d(simpleName, "onKey KEYCODE_BACK");
        return onBackPressed();
    }

    @DexIgnore
    @Override // com.fossil.ze0, com.fossil.kq0
    public void setupDialog(Dialog dialog, int i2) {
        Window window;
        pq7.c(dialog, "dialog");
        if (this.d) {
            Window window2 = dialog.getWindow();
            if (window2 != null) {
                window2.setDimAmount(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        } else {
            Window window3 = dialog.getWindow();
            if (window3 != null) {
                window3.setDimAmount(this.g);
            }
        }
        Window window4 = dialog.getWindow();
        if (window4 != null) {
            window4.setGravity(this.e);
        }
        Window window5 = dialog.getWindow();
        if (window5 != null) {
            window5.setLayout(-1, this.c);
        }
        Window window6 = dialog.getWindow();
        if (window6 != null) {
            window6.setBackgroundDrawableResource(R.color.transparent);
        }
        if (this.f && (window = dialog.getWindow()) != null) {
            window.setFlags(32, 32);
        }
        dialog.setOnKeyListener(this);
    }

    @DexIgnore
    @Override // com.fossil.kq0
    public void show(FragmentManager fragmentManager, String str) {
        pq7.c(fragmentManager, "fragmentManager");
        FLogger.INSTANCE.getLocal().d(str != null ? str : "", "show");
        if (!isActive()) {
            xq0 j2 = fragmentManager.j();
            pq7.b(j2, "fragmentManager.beginTransaction()");
            Fragment Z = fragmentManager.Z(str);
            if (Z != null) {
                j2.q(Z);
            }
            j2.f(null);
            show(j2, str);
            fragmentManager.V();
        }
    }

    @DexIgnore
    public void z6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
