package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k05 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<SleepSessionHeartRate> {
    }

    @DexIgnore
    public final SleepSessionHeartRate a(String str) {
        SleepSessionHeartRate sleepSessionHeartRate;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            sleepSessionHeartRate = (SleepSessionHeartRate) new Gson().l(str, new a().getType());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            e.printStackTrace();
            local.e("SleepSessionHeartRateConverter.toHeartRate()", String.valueOf(tl7.f3441a));
            sleepSessionHeartRate = null;
        }
        return sleepSessionHeartRate;
    }

    @DexIgnore
    public final String b(SleepSessionHeartRate sleepSessionHeartRate) {
        if (sleepSessionHeartRate == null) {
            return null;
        }
        try {
            return jj5.a(sleepSessionHeartRate);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            e.printStackTrace();
            local.e("SleepSessionHeartRateConverter.toString()", String.valueOf(tl7.f3441a));
            return null;
        }
    }
}
