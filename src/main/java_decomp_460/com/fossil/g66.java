package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g66 implements Factory<c66> {
    @DexIgnore
    public static c66 a(e66 e66) {
        c66 c = e66.c();
        lk7.c(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
