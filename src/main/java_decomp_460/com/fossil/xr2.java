package com.fossil;

import android.os.Looper;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xr2 implements da3 {
    @DexIgnore
    @Override // com.fossil.da3
    public final t62<Status> a(r62 r62, ga3 ga3) {
        return r62.j(new zr2(this, r62, ga3));
    }

    @DexIgnore
    @Override // com.fossil.da3
    public final t62<Status> b(r62 r62, LocationRequest locationRequest, ga3 ga3) {
        rc2.l(Looper.myLooper(), "Calling thread must be a prepared Looper thread.");
        return r62.j(new yr2(this, r62, locationRequest, ga3));
    }
}
